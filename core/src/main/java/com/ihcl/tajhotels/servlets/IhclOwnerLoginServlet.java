package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;

/**
 * The Class IHCLOwnerLoginServlet.
 */
@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.paths=" + "/bin/ihcl-security-check",
		"sling.servlet.methods=POST" })
public class IhclOwnerLoginServlet extends SlingAllMethodsServlet {

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	@Reference
	TajApiKeyConfigurationService tajApiKeyConfigurationService;

	private static final Logger LOG = LoggerFactory.getLogger(IhclOwnerLoginServlet.class);

	/**
	 * Do post.
	 *
	 * @param request the request
	 * @param response the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws IOException, ServletException {
		String responseAfterVerification = getResponseAfterVerify(request);
		response.getWriter().write(responseAfterVerification);

	}

	
	/**
	 * Gets the response after verify.
	 *
	 * @param request the request
	 * @return the response after verify
	 */
	private String getResponseAfterVerify(final SlingHttpServletRequest request) {
		String username = request.getParameter("j_username");
		String password = request.getParameter("j_password");
		LOG.debug("Inside Post Method.");
		if (null != tajApiKeyConfigurationService) {
			LOG.debug("Taj API Key is not null.");
			if ((username.equalsIgnoreCase(tajApiKeyConfigurationService.getIhclOwnerUsername()))
					&& (password.equals(tajApiKeyConfigurationService.getIhclOwnerPassword()))) {
				return "valid";
			} else {
				return "Invalid username or password.";
			}
		}
		return "TajApi Key is Null";
	}
}
