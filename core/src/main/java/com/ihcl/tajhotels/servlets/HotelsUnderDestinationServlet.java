package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.search.SearchResult;
import com.ihcl.core.services.search.SearchProvider;


@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/hotelsUnderDestination" })
public class HotelsUnderDestinationServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(HotelsUnderDestinationServlet.class);

    private static final String URL = "url";

    private static final String TAJ_HOTEL_BASE_PATH = "/content/tajhotels/en-in/our-hotels";

    private static final String SELECTION_HOTEL_BASE_PATH = "/content/seleqtions/en-in/our-hotels";

    private static final String VIVANTA_HOTEL_BASE_PATH = "/content/vivanta/en-in/our-hotels";

    private static final String IHCLCB_HOTEL_BASE_PATH = "/content/ihclcb/en-in";

    private static final String IHCLCB_VIVANTA_HOTEL_BASE_PATH = "/content/ihclcb/en-in/our-hotels-vivanta/";

    private static final String IHCLCB_SELEQTIONS_HOTEL_BASE_PATH = "/content/ihclcb/en-in/our-hotel-ihclcb-selections/";

    private static final String IHCLCB_TAJ_HOTEL_BASE_PATH = "/content/ihclcb/en-in/ihclcbtajourhotels/";

    private static final String TEXT_CORPORATE_BOOKING = "/corporate-booking/";

    private static final String TEXT_ROOMS_AND_SUITS = "rooms-and-suites/";

    private static final String TEXT_IHCLCB = "/ihclcb/";

    private static final String CQ_PAGE = "cq:Page";

    private static final String EXTENSION_DOT_HTML = ".html";

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver = null;


    @Reference
    SearchProvider searchProvider;

    JSONArray hotelArray = null;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        LOGGER.trace("Method Entry do get of HotelsUnderDestinationServlet");

        try {
            response.setContentType("application" + "/json" + ";" + "charset=UTF-8");
            hotelArray = new JSONArray();
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            String url = request.getParameter(URL);
            String hotelUrl = url.split("/rooms-and-suites")[0];
            String[] hotelUrlArray = hotelUrl.split("/");
            String hotelName = hotelUrlArray[hotelUrlArray.length - 1];

            HashMap<String, String> predicateMap = new HashMap<>();
            if (url.contains(TEXT_CORPORATE_BOOKING)) {
                predicateMap.put("group.1_path", IHCLCB_HOTEL_BASE_PATH);
            } else {
                predicateMap.put("group.1_path", TAJ_HOTEL_BASE_PATH);
                predicateMap.put("group.2_path", SELECTION_HOTEL_BASE_PATH);
                // predicateMap.put("group.3_path", VIVANTA_HOTEL_BASE_PATH);
                predicateMap.put("group.p.or", "true");
            }
            predicateMap.put("nodename", hotelName);
            predicateMap.put("type", CQ_PAGE);
            predicateMap.put("p.limit", "1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                String[] pathArray = searchResult.getPath().split("/");
                String destinationName = pathArray[5];
                String destinationBasePath = searchResult.getPath().split(destinationName)[0];
                Resource baseHotelResource = resourceResolver.getResource(searchResult.getPath());
                Resource baseHotelJcrResource = baseHotelResource.getChild("jcr:content");
                if (baseHotelJcrResource == null) {
                    continue;
                }
                ModifiableValueMap baseHotelValueMap = baseHotelJcrResource.adaptTo(ModifiableValueMap.class);
                String baseHotelName = baseHotelValueMap.get("hotelName", String.class);
                JSONObject baseHotelNameObject = new JSONObject();
                baseHotelNameObject.put("baseHotelName", baseHotelName);
                hotelArray.put(baseHotelNameObject);
                getAllHotelsUnderDestination(hotelName, destinationBasePath, destinationName, resourceResolver);

            }

            response.getWriter().println(hotelArray.toString());

        } catch (Exception e) {
            LOGGER.error("An error occured while fetching details in HotelsUnderDestinationServlet :: {}",
                    e.getMessage());
            LOGGER.debug("An error occured while fetching details in HotelsUnderDestinationServlet :: {}", e);
        }
    }

    /**
     * @param destinationName
     * @param destinationName
     * @param response
     * @param resourceResolver2
     */
    private void getAllHotelsUnderDestination(String hotelName, String destinationBasePath, String destinationName,
            ResourceResolver resourceResolver) {

        JSONObject destinationObject = new JSONObject();

        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            String strippedDestinationName = destinationName.replace("hotels-in-", "");
            destinationObject.put("destinationName",
                    strippedDestinationName.substring(0, 1).toUpperCase() + strippedDestinationName.substring(1));
            hotelArray.put(destinationObject);
            String destinationPath = destinationBasePath + destinationName;
            if (destinationBasePath.contains(TEXT_IHCLCB)) {
                predicateMap.put("group.1_path", IHCLCB_VIVANTA_HOTEL_BASE_PATH + destinationName);
                predicateMap.put("group.2_path", IHCLCB_SELEQTIONS_HOTEL_BASE_PATH + destinationName);
                predicateMap.put("group.3_path", IHCLCB_TAJ_HOTEL_BASE_PATH + destinationName);
                predicateMap.put("group.p.or", "true");
            } else {
                predicateMap.put("path", destinationPath);
            }
            predicateMap.put("1_property", "sling:resourceType");
            predicateMap.put("1_property.value", "tajhotels/components/structure/hotels-landing-page");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {

                JSONObject hotelObject = new JSONObject();
                Resource hotelResource = resourceResolver.getResource(searchResult.getPath());
                String hotelCrxPath = searchResult.getPath();
                if (hotelResource.getName().equals(hotelName)) {
                    continue;
                }

                Resource hotelBannerResource = hotelResource.getChild("jcr:content/banner_parsys/hotel_banner");

                if (hotelBannerResource != null) {
                    ModifiableValueMap hotelBannerValueMap = hotelBannerResource.adaptTo(ModifiableValueMap.class);
                    hotelObject.put("imagePath", hotelBannerValueMap.get("croppedUrl", String.class));
                    hotelObject.put("hotelName", hotelBannerValueMap.get("hotelName", String.class));
                }
                Resource jcrContent = hotelResource.getChild("jcr:content");

                if (jcrContent != null) {
                    ModifiableValueMap jcrContentValueMap = jcrContent.adaptTo(ModifiableValueMap.class);
                    hotelObject.put("brand", hotelCrxPath.split("/")[6]);
                    hotelObject.put("hotelNodeName", hotelResource.getName());
                    hotelObject.put("hotelArea", jcrContentValueMap.get("hotelArea", String.class));
                    hotelObject.put("hotelPinCode", jcrContentValueMap.get("hotelPinCode", String.class));
                    hotelObject.put("hotelCity", jcrContentValueMap.get("hotelCity", String.class));
                    if (destinationBasePath.contains(TEXT_IHCLCB)) {
                        hotelObject.put("hotelShortUrl",
                                resourceResolver.map(hotelCrxPath + EXTENSION_DOT_HTML) + TEXT_ROOMS_AND_SUITS);
                    }
                }
                hotelArray.put(hotelObject);
            }

        } catch (Exception e) {
            LOGGER.error("An error occured while fetching details in HotelsUnderDestinationServlet.getAllHotelsUnderDestination",e);
        }

    }

}
