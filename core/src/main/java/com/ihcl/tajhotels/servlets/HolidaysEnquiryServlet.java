
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.HolidaysEnquiry;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/HolidaysEnquiryServlet" })
public class HolidaysEnquiryServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(HolidaysEnquiryServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService emailConf;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        HashMap<String, String> dynamicValuemap = new HashMap<>();
        LOG.trace("Method Entry: {}", methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for holidays enquiry is : " + requestUrl);

        try {
            if (validateRequest.validateUrl(requestUrl)) {
                String finalHtmlTemplate = null;
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
                ObjectMapper mapper = new ObjectMapper();
                String enquiryJson = httpRequest.getParameter("holidaysEnquiry");

                if (enquiryJson != null) {
                    LOG.debug("enquiry json : {}", enquiryJson);
                    HolidaysEnquiry enquiry = mapper.readValue(enquiryJson, new TypeReference<HolidaysEnquiry>() {
                    });
                    dynamicValuemap = getDynamicValuesForEmailHolidaysEnquiry(enquiry, dynamicValuemap);

                    enquiry.setEnquiryEmail(emailConf.getEmailFromAddressForHolidaysEnqiry());
                    enquiry.setHolidaysEmail(emailConf.getEmailFromAddressForHolidaysEnqiry());
                    enquiry.setEmailSubject(emailConf.getEmailSubjectForHolidaysEnquiry());
                    enquiry.setEmailBodyTemplate(emailConf.getEmailBodyTemplateForHolidaysEnquiry());
                    enquiry.setDynamicValues(dynamicValuemap);
                    finalHtmlTemplate = StringTokenReplacement.replaceToken(enquiry.getDynamicValues(),
                            emailConf.getEmailBodyTemplateForHolidaysEnquiry());
                    /* send email to submit enquiry to taj hotels */
                    boolean isSendEmailSuccess = emailService.sendPackageEnquiry(enquiry, finalHtmlTemplate);
                    if (isSendEmailSuccess) {
                        LOG.trace("Email has been successfully ");
                        /* send confirmation email to user */
                        enquiry.setEnquiryEmail(enquiry.getUserEmail());
                        enquiry.setHolidaysEmail(emailConf.getEmailFromAddressForHolidaysEnqiry());
                        enquiry.setEmailBodyTemplate(emailConf.getEmailSubjectForEnquiryConfirmation());
                        enquiry.setEmailBodyTemplate(emailConf.getEmailBodyTemplateForEnquiryConfirmation());
                        finalHtmlTemplate = StringTokenReplacement.replaceToken(enquiry.getDynamicValues(),
                                emailConf.getEmailBodyTemplateForEnquiryConfirmation());
                        isSendEmailSuccess = emailService.sendPackageEnquiry(enquiry, finalHtmlTemplate);

                        if (isSendEmailSuccess) {
                            writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                        }
                    } else {
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                        writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                    }

                } else {
                    LOG.error("Enquiry object not found in request");

                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }
        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.debug(httpRequest.getParameter("holidaysEnquiry"));
    }

    private HashMap<String, String> getDynamicValuesForEmailHolidaysEnquiry(HolidaysEnquiry enquiry,
            HashMap<String, String> emailMap) {
        emailMap.put("areaCode", enquiry.getAreaCode());
        emailMap.put("city", enquiry.getCity());
        emailMap.put("country", enquiry.getCountry());
        emailMap.put("gender", enquiry.getGender());
        emailMap.put("contactNum", enquiry.getContactNum());
        emailMap.put("userName", enquiry.getUserName());
        emailMap.put("userEmail", enquiry.getUserEmail());
        emailMap.put("packageName", enquiry.getPackageName());
        emailMap.put("packageDestination", enquiry.getPackageDestination());
        emailMap.put("selectedHotelsWithCity", getHotelsFromArray(enquiry.getSelectedHotelsWithCity()));
        emailMap.put("oncallFlag", getFlagValue(enquiry.isOncallFlag()));
        emailMap.put("fromDate", enquiry.getFromDate());
        emailMap.put("toDate", enquiry.getToDate());

        return emailMap;
    }

    private String getHotelsFromArray(String[] selectedHotelsWithCity) {
        String hotels = "";
        if ((selectedHotelsWithCity != null) && (selectedHotelsWithCity.length < 1)) {
            hotels = "None selected";

        } else {
            for (String hotel : selectedHotelsWithCity) {
                hotels += hotel + ", ";
            }
            hotels.substring(0, hotels.length() - 2);
        }
        return hotels;
    }

    private String getFlagValue(boolean oncallFlag) {
        String result = "";
        if (oncallFlag) {
            result = "Yes";
        } else {
            result = "No";
        }
        return result;
    }

    private void writeJsonToResponse(SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}", methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of Response JSON: {}", jsonString);
        LOG.trace("Method Exit: {}", methodName);

        response.getWriter().write(jsonNode.toString());

    }

    // holiday emails: "holidays@tajhotels.com", "reservations@tajhotels.com"
}
