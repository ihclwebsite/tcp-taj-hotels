
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.AddMultiCitiesTajAir;
import com.ihcl.tajhotels.email.requestdto.ReturnJourneyDetails;
import com.ihcl.tajhotels.email.requestdto.TajAirEmail;

@Component(service = Servlet.class,
immediate = true,
property = { "sling.servlet.paths=" + "/bin/requestquotes" })
public class TajAirBookAppointmentServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(TajAirBookAppointmentServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService tajAirEmailConf;

    HashMap<String, String> dynamicValuemap = new HashMap<>();


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for jiva spa book an appointment is : " + requestUrl);
        try {
            if (validateRequest.validateUrl(requestUrl)) {

                String returnTripTemplate = "&lt;html&gt;&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"2\"&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px\"&gt;USER DETAILS &lt;/div&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Fleet  name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%;\"&gt; Falcon 2000LX&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Full  name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${fullName}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Organization &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${organization}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Country &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${country}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Passengers &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${numberOfGuests}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Email Id &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${emailId}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Mobile Number &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${mobile}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Telephone &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${officePhoneNumber}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Any Non-Indian Passengers &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${anyNonIndianPassengers}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;&lt;table  style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;border-collapse:collapse;width:97%;\"&gt;&lt;tr&gt;&lt;td colspan='6'&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px;margin-top:20px\"&gt;FLIGHT REQUIREMENTS - RETURN TRIP &lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Date &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Time &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure From &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure To &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Return Date &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Return Time &lt;/th&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureDate}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureTime}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureFromCity}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureToCity}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${returnDate}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${returnTime}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width:97%;border-collapse:collapse;margin-top:20px;margin-botton:20px\"&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid ;text-align:left;padding:3px;background-color:#dddddd8c;font-size:12px;width: 11%;\"&gt;You have specified the time as &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${timingStandard}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=\"border:1px solid ;text-align:left;padding:3px;background-color:#dddddd8c;font-size:12px\"&gt;Comments &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${comments}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;";

                String oneWayOrMultiple = "&lt;html&gt;&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"2\"&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px\"&gt;USER DETAILS &lt;/div&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Fleet  name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%;\"&gt; Falcon 2000LX&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Full  name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${fullName}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Organization &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${organization}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Country &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${country}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Passengers &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${numberOfGuests}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Email Id &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${emailId}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Mobile Number &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${mobile}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Telephone &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${officePhoneNumber}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Any Non-Indian Passengers &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${anyNonIndianPassengers}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;";

                String addingTableAndHeadersOneWay = "&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"4\"&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px;margin-top:20px\"&gt;FLIGHT REQUIREMENTS - ONE-WAY TRIP &lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Date &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Time &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure From &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure To &lt;/th&gt;&lt;/tr&gt;";

                String addingTableAndHeadersMultiWay = "&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"4\"&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px;margin-top:20px\"&gt;FLIGHT REQUIREMENTS - MULTI-CITY TRIP&lt;/div&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Date &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure Time &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure From &lt;/th&gt;&lt;th style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;Departure To &lt;/th&gt;&lt;/tr&gt;";

                String oneMoreEntryTemplate = "&lt;tr&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureDate}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureTime}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureFromCity}&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;${departureToCity}&lt;/td&gt;&lt;/tr&gt;";

                String closingTable = "&lt;/table&gt;";

                String lastPartOfPersonalInfo = "&lt;/table&gt;&lt;table style=\"width: 97%;font-family:theantiquasun,Helvetica,Arial,sans-serif;border-collapse:collapse;margin-top:20px;margin-botton:20px\"&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid ;text-align:left;padding:3px;background-color:#dddddd8c;font-size:12px;width:11%;\"&gt;You have specified the time as &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${timingStandard}&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=\"border:1px solid ;text-align:left;padding:3px;background-color:#dddddd8c;font-size:12px\"&gt;Comments &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${comments}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;";

                String finalHtmlTemplate = null;


                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                TajAirEmail requestobj = new TajAirEmail();
                ReturnJourneyDetails returnDetails = new ReturnJourneyDetails();


                // Fields for email template configuration
                if (null != tajAirEmailConf) {
                    requestobj.setFullName(httpRequest.getParameter("fullName"));
                    requestobj.setEmailId(httpRequest.getParameter("emailId"));
                    requestobj.setMobile(httpRequest.getParameter("mobile"));
                    requestobj.setNumberOfGuests(httpRequest.getParameter("numberOfGuests"));
                    requestobj.setOrganization(httpRequest.getParameter("organization"));
                    requestobj.setOfficePhoneNumber(httpRequest.getParameter("officePhoneNumber"));
                    requestobj.setAnyNonIndianPassengers(httpRequest.getParameter("anyNonIndianPassengers"));
                    requestobj.setCountry(httpRequest.getParameter("country"));
                    requestobj.setTimingStandard(httpRequest.getParameter("timingStandard"));
                    requestobj.setComments(httpRequest.getParameter("comments"));

                    if (null != httpRequest.getParameter("returnDate")) {
                        LOG.trace("Its a return trip");
                        returnDetails.setDepartureDate(httpRequest.getParameter("departureDate"));
                        returnDetails.setDepartureTime(httpRequest.getParameter("departureTime"));
                        returnDetails.setDepartureFromCity(httpRequest.getParameter("departureFromCity"));
                        returnDetails.setDepartureToCity(httpRequest.getParameter("departureToCity"));
                        returnDetails.setReturnDate(httpRequest.getParameter("returnDate"));
                        returnDetails.setReturnTime(httpRequest.getParameter("returnTime"));
                        requestobj.setReturnTripDetails(returnDetails);
                        finalHtmlTemplate = returnTripTemplate;
                    } else {
                        requestobj.setAddMultiCities(fetchMultiCityDetailsFromRequest(httpRequest));
                    }

                    if (null != requestobj.getAddMultiCities()) {

                        if (requestobj.isMultiCity()) {
                            for (int i = 0; i < requestobj.getAddMultiCities().size(); i++) {
                                String dataFilled = setTemplateData(requestobj.getAddMultiCities().get(i));
                                if (i == 0) {
                                    oneWayOrMultiple = oneWayOrMultiple.concat(addingTableAndHeadersMultiWay);
                                }
                                oneWayOrMultiple = oneWayOrMultiple.concat(dataFilled);

                                finalHtmlTemplate = oneWayOrMultiple;
                                LOG.trace("One way or multiple str" + oneWayOrMultiple);
                            }
                            finalHtmlTemplate = finalHtmlTemplate.concat(closingTable);
                            finalHtmlTemplate = finalHtmlTemplate.concat(lastPartOfPersonalInfo);

                        } else {
                            oneWayOrMultiple = oneWayOrMultiple.concat(addingTableAndHeadersOneWay);
                            finalHtmlTemplate = oneWayOrMultiple.concat(oneMoreEntryTemplate);
                            finalHtmlTemplate = finalHtmlTemplate.concat(closingTable);
                            finalHtmlTemplate = finalHtmlTemplate.concat(lastPartOfPersonalInfo);
                        }
                    }

                    requestobj.setEmailFromAddress(tajAirEmailConf.getEmailFromAddressForTajAir());
                    requestobj.setEmailToAddress(tajAirEmailConf.getEmailToAddressFotTajAir());
                    requestobj.setEmailSubjectForTajAir(tajAirEmailConf.getEmailSubjectForTajAir());

                    String unescape = StringEscapeUtils.unescapeHtml4(finalHtmlTemplate);

                    LOG.trace("Unescaped string  : " + unescape);
                    dynamicValuemap = getDynamicValuesForEmailTajAir(requestobj);
                    requestobj.setDynamicValuesForEmailTajAir(dynamicValuemap);
                    finalHtmlTemplate = StringTokenReplacement.replaceToken(requestobj.getDynamicValuesForEmailTajAir(),
                            unescape);
                    LOG.trace("Final html template for taj Air email booking confirmation is : " + finalHtmlTemplate);
                } else if (null == tajAirEmailConf) {
                    LOG.warn("Email Configuration for taj Air  is missing");
                }

                boolean isSendEmailSuccess = emailService.sendAppointmentEmailTajAir(requestobj, finalHtmlTemplate);
                finalHtmlTemplate = null;
                if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.trace("This request contain cross site attck characters :");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while Sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param i
     * @return
     */
    private String setTemplateData(AddMultiCitiesTajAir addMultiCitiesTajAir) {
        return "&lt;tr&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;"
                + addMultiCitiesTajAir.getDepartureDate()
                + "&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;"
                + addMultiCitiesTajAir.getDepartureTime()
                + "&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;"
                + addMultiCitiesTajAir.getDepartureFromCity()
                + "&lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px\"&gt;"
                + addMultiCitiesTajAir.getDepartureToCity() + "&lt;/td&gt;&lt;/tr&gt;";
    }

    /**
     * @param queryParamMap
     * @return
     */
    private List<AddMultiCitiesTajAir> fetchMultiCityDetailsFromRequest(SlingHttpServletRequest httpRequest) {
        String methodName = "fetchMultiCityDetailsFromRequest";
        LOG.trace("Method Entry: " + methodName);
        List<AddMultiCitiesTajAir> occupantsList = new ArrayList<>();
        try {

            Object multiCities = httpRequest.getParameter("tripValues");
            LOG.debug("City details are " + multiCities);

            if (multiCities != null) {

                String cityData = multiCities.toString();
                if (!cityData.isEmpty()) {
                    JSONArray jsonArray = new JSONArray(cityData);
                    LOG.debug("Parsed travel details string to json as: " + jsonArray);

                    int numberOfTrips = jsonArray.length();
                    for (int i = 0; i < numberOfTrips; i++) {
                        JSONObject eachTripEntry = jsonArray.getJSONObject(i);

                        AddMultiCitiesTajAir multiCityEntry = new AddMultiCitiesTajAir();

                        multiCityEntry.setDepartureDate(eachTripEntry.get("departureDate").toString());
                        multiCityEntry.setDepartureTime(eachTripEntry.get("departureTime").toString());
                        multiCityEntry.setDepartureFromCity(eachTripEntry.get("departureFromCity").toString());
                        multiCityEntry.setDepartureToCity(eachTripEntry.get("departureToCity").toString());

                        LOG.debug("Adding occupant " + i + " as: " + multiCityEntry);
                        occupantsList.add(multiCityEntry);

                    }
                }
            }
        } catch (JSONException e) {
            LOG.error("An error occured while processing the provided room occupants in the request.", e);
        }

        LOG.debug("Returning occupants list as: " + occupantsList);
        LOG.trace("Method Exit: " + methodName);
        return occupantsList;
    }

    /**
     * Method to set map fields with token names and values
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValuesForEmailTajAir(TajAirEmail request) {
        dynamicValuemap.put("fullName", request.getFullName());
        dynamicValuemap.put("mobile", request.getMobile());
        dynamicValuemap.put("emailId", request.getEmailId());
        dynamicValuemap.put("numberOfGuests", request.getNumberOfGuests());
        dynamicValuemap.put("organization", request.getOrganization());
        dynamicValuemap.put("officePhoneNumber", request.getOfficePhoneNumber());
        dynamicValuemap.put("anyNonIndianPassengers", request.getAnyNonIndianPassengers());
        dynamicValuemap.put("country", request.getCountry());
        dynamicValuemap.put("timingStandard", request.getTimingStandard());
        dynamicValuemap.put("comments", request.getComments());

        // Return details
        if (null != request.getReturnTripDetails()) {
            dynamicValuemap.put("departureDate", request.getReturnTripDetails().getDepartureDate());
            dynamicValuemap.put("departureTime", request.getReturnTripDetails().getDepartureTime());
            dynamicValuemap.put("departureFromCity", request.getReturnTripDetails().getDepartureFromCity());
            dynamicValuemap.put("departureToCity", request.getReturnTripDetails().getDepartureToCity());
            dynamicValuemap.put("returnDate", request.getReturnTripDetails().getReturnDate());
            dynamicValuemap.put("returnTime", request.getReturnTripDetails().getReturnTime());
        }
        if (null != request.getAddMultiCities()) {
            for (int i = 0; i < request.getAddMultiCities().size(); i++) {
                dynamicValuemap.put("departureDate", request.getAddMultiCities().get(i).getDepartureDate());
                dynamicValuemap.put("departureTime", request.getAddMultiCities().get(i).getDepartureTime());
                dynamicValuemap.put("departureFromCity", request.getAddMultiCities().get(i).getDepartureFromCity());
                dynamicValuemap.put("departureToCity", request.getAddMultiCities().get(i).getDepartureToCity());
            }
        }

        return dynamicValuemap;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }

}

