/**
 * 
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.ROOMS_AVAILABILITY_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CALENDAR_AVAILABILITY_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.ERROR_MESSAGE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CALENDAR_AVAILABILITY_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.SYNXIS_MAINTAINENCE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CALENDAR_PRICING_ERROR;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.SYNXIS_ERROR_CODE;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONArray;
import org.json.JSONException;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.GuaranteeCodesConfigurationService;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.core.services.serviceamenity.RatePlanCodeAmenitiesFetcherService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.CalendarAvailabilityResponse;
import com.ihcl.integration.dto.availability.CalendarHotelStayDto;
import com.ihcl.integration.dto.availability.CurrencyCodeDto;
import com.ihcl.integration.dto.availability.PriceDto;
import com.ihcl.integration.dto.availability.RateFilterDto;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.tajhotels.constants.HttpRequestParams;

/**
 * @author TCS
 *
 */
@Component(service = Servlet.class,
			immediate = true,
			property = { "sling.servlet.paths=" + "/bin/calendarAvailability" })
public class CalendarCheckAvailabilityServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(RoomsAvailabilityFetchServlet.class);

    @Reference
    private IRoomAvailabilityService roomAvailabilityService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    @Reference
    private GuaranteeCodesConfigurationService guaranteeCodesConfigurationService;

    @Reference
    private RatePlanCodeAmenitiesFetcherService ratePlanCodeAmenitiesFetcherService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("Method Entry: {}", methodName);

        LOG.debug("Availability searchParams: {}", getQueryParams(httpRequest).toString());

        try {
            LOG.debug("Received room availability service as: {}", roomAvailabilityService);
            LOG.debug("Received rate plan code amenities fetcher as: {}", ratePlanCodeAmenitiesFetcherService);

            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

            List<CalendarAvailabilityResponse> calendarAvailResponse = null;
            if (synixsDowntimeConfigurationService.isForcingToSkipCalls()) {

                LOG.info("isForcingToSkipCalls: {}", synixsDowntimeConfigurationService.isForcingToSkipCalls());
                ObjectMapper mapper = new ObjectMapper();
                ObjectNode jsonNode = mapper.createObjectNode();
                jsonNode.put("serviceError", synixsDowntimeConfigurationService.getDowntimeMessage());
                writeKeyAndMessage(RESPONSE_CODE_FAILURE, SYNXIS_MAINTAINENCE, jsonNode);
                response.setStatus(Integer.valueOf(synixsDowntimeConfigurationService.getServletStatusCode()));
                invalidateCache(response);
                response.getWriter().write(jsonNode.toString());
            } else {
                calendarAvailResponse = fetchAvailabilityFromService(httpRequest);

                LOG.debug("Received room availability response from service as: {}", calendarAvailResponse);
                if (calendarAvailResponse == null || calendarAvailResponse.isEmpty()) {
                    LOG.error("An error occured while fetching room availability due to handled exception.");

                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    invalidateCache(response);

                    writeFailureResponseToJson(response, RESPONSE_CODE_FAILURE, ROOMS_AVAILABILITY_FAILURE);
                    return;
                } else if(calendarAvailResponse.size() == 0 || calendarAvailResponse.get(0).getMessage().equalsIgnoreCase("Failure")) {
                	
                    writeSuccessResponseToJson(httpRequest, response, calendarAvailResponse, RESPONSE_CODE_FAILURE,
                    		CALENDAR_PRICING_ERROR);
                } else {
                	writeSuccessResponseToJson(httpRequest, response, calendarAvailResponse, RESPONSE_CODE_SUCCESS,
                		CALENDAR_AVAILABILITY_SUCCESS);
                }
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability due to thrown exception", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            invalidateCache(response);

            writeFailureResponseToJson(response, RESPONSE_CODE_FAILURE, CALENDAR_AVAILABILITY_FAILURE);
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    private SlingHttpServletResponse invalidateCache(final SlingHttpServletResponse response) {
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        return response;
    }

    /**
     * @param response
     * @param responseCodeFailure
     * @param roomsAvailabilityFailure
     * @throws IOException
     */
    private void writeFailureResponseToJson(SlingHttpServletResponse response, String responseCodeFailure,
            String roomsAvailabilityFailure) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        writeKeyAndMessage(responseCodeFailure, roomsAvailabilityFailure, jsonNode);
        response.getWriter().write(jsonNode.toString());

    }

    /**
     * @param httpRequest
     * @return
     * @throws Exception
     */
    private List<CalendarAvailabilityResponse> fetchAvailabilityFromService(SlingHttpServletRequest httpRequest)
            throws Exception {

        String methodName = "fetchAvailabilityFromService";
        LOG.trace("Method Entry: {}", methodName);

        RoomsAvailabilityRequest availabilityRequest = new RoomsAvailabilityRequest();

        Map<String, String> queryParamMap = getQueryParams(httpRequest);

        availabilityRequest.setRoomOccupants(getRoomOccupantsFromRequest(queryParamMap));

        availabilityRequest.setRateFilters(getRateFiltersFromRequest(queryParamMap));

        availabilityRequest.setRatePlanCodes(getRatePlanCodesFromRequest(queryParamMap));

        availabilityRequest.setCurrencyCode(getCurrenctCodeFromRequest(queryParamMap));

        availabilityRequest.setCheckInDate(queryParamMap.get(HttpRequestParams.CHECK_IN_DATE));

        availabilityRequest.setCheckOutDate(queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE));

        String promoCodeParam = queryParamMap.get(HttpRequestParams.PROMO_CODE);

        String corporateCodeParam = queryParamMap.get(HttpRequestParams.CORPORATE_CODE);
        
        String duration = queryParamMap.get(HttpRequestParams.DURATION);
        
        if(duration != null && !duration.isEmpty()) {
        	availabilityRequest.setDuration(duration);
        } else {
        	availabilityRequest.setDuration("P1N");
        }
        
        String promotionCode = "";
        if (corporateCodeParam != null) {
            promotionCode = corporateCodeParam;
        } else if (promoCodeParam != null && !promoCodeParam.equalsIgnoreCase("null")) {
            promotionCode = promoCodeParam;
        }
        availabilityRequest.setPromoCode(promotionCode);
        
        List<String> hotelIds = getHotelIdsFromRequest(queryParamMap);

        availabilityRequest.setHotelIdentifiers(hotelIds);

        LOG.debug("Attempting to fetch room availability using the request: {}", availabilityRequest);
        List<CalendarAvailabilityResponse> calendarAvailabilityResponse = roomAvailabilityService
                .getCalendarAvailability(availabilityRequest);
        ObjectMapper objectMapper = new ObjectMapper();
        LOG.debug("Received availability response list as: {}",
                objectMapper.writeValueAsString(calendarAvailabilityResponse));

        LOG.trace("Method Exit: {}", methodName);

        return calendarAvailabilityResponse;

    }

    private Map<String, String> getQueryParams(SlingHttpServletRequest httpRequest) {
    	
        String suffixPath = httpRequest.getRequestPathInfo().getSuffix();
        LOG.debug("Received suffix path info: {}", suffixPath);

        String selectorString = suffixPath.substring(suffixPath.lastIndexOf('/') + 1, suffixPath.length() - 1);
        List<String> suffixes = Arrays.asList(suffixPath.split("/"));

        List<String> eachString = Arrays.asList(selectorString.split("\\."));
        Map<String, String> queryParamMap = eachString.stream().map(s -> s.split("=", 2))
                .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));

        queryParamMap.put(HttpRequestParams.HOTEL_IDS, suffixes.get(1));
        queryParamMap.put(HttpRequestParams.CHECK_IN_DATE, suffixes.get(2));
        queryParamMap.put(HttpRequestParams.CHECK_OUT_DATE, suffixes.get(3));
        queryParamMap.put(HttpRequestParams.CURRENCY_SHORT_STRING, suffixes.get(4));
        queryParamMap.put(HttpRequestParams.OCCUPANCY, suffixes.get(5));
        queryParamMap.put(HttpRequestParams.RATE_FILTERS, suffixes.get(6));
        queryParamMap.put(HttpRequestParams.RATE_CODES, suffixes.get(7));
        queryParamMap.put(HttpRequestParams.PROMO_CODE, suffixes.get(8));
        queryParamMap.put(HttpRequestParams.DURATION, suffixes.get(9));
        
        return queryParamMap;
    }

    private List<String> getRatePlanCodesFromRequest(Map<String, String> queryMap) {
        List<String> ratePlanCodes = new ArrayList<>();
        try {
            if (queryMap.containsKey(HttpRequestParams.RATE_CODES)) {
                Object rateCodes = queryMap.get(HttpRequestParams.RATE_CODES);
                LOG.debug("Received rate codes as: {}", rateCodes);

                if (rateCodes != null) {
                    String rateCodesString = rateCodes.toString();
                    if (!rateCodesString.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(rateCodesString);
                        LOG.debug("Parsed rate filters to json array as: {}", jsonArray);
                        int numOfRateCodes = jsonArray.length();

                        for (int i = 0; i < numOfRateCodes; i++) {
                            String rateFilterString = jsonArray.getString(i);

                            ratePlanCodes.add(rateFilterString);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error occurred while parsing rate plan codes from request", e);
        }
        return ratePlanCodes;
    }

    /**
     * @param queryMap
     * @return
     */
    private CurrencyCodeDto getCurrenctCodeFromRequest(Map<String, String> queryMap) {

        Object shortCurrencyString = queryMap.get(HttpRequestParams.CURRENCY_SHORT_STRING);
        Object currencyString = queryMap.get(HttpRequestParams.CURRENCY_STRING);

        return (new CurrencyCodeDto() {

            @Override
            public String getShortCurrencyString() {
                return (String) shortCurrencyString;
            }

            @Override
            public String getCurrencyString() {
                return (String) currencyString;
            }
        });
    }

    private List<RateFilterDto> getRateFiltersFromRequest(Map<String, String> queryMap) {
        String methodName = "getRateFiltersFromRequest";
        LOG.trace("Method Entry: {}", methodName);
        List<RateFilterDto> rateFiltersList = new ArrayList<>();
        try {
            if (queryMap.containsKey(HttpRequestParams.RATE_FILTERS)) {
                Object rateFilters = queryMap.get(HttpRequestParams.RATE_FILTERS);
                LOG.debug("Received rate filters as: {}", rateFilters);
                if (rateFilters != null) {
                    String rateFiltersString = rateFilters.toString();
                    if (!rateFiltersString.isEmpty()) {

                        JSONArray jsonArray = new JSONArray(rateFiltersString);
                        LOG.debug("Parsed rate filters to json array as: {}", jsonArray);
                        int numOfRateFilters = jsonArray.length();

                        for (int i = 0; i < numOfRateFilters; i++) {
                            String rateFilterString = jsonArray.getString(i);

                            LOG.debug("rateFilterString: {}", rateFilterString);
                            RateFilterDto rateFilter = new RateFilterDto() {

                                @Override
                                public String getCode() {
                                    return rateFilterString;
                                }

                                @Override
                                public String getTitle() {
                                    return rateFilterString;
                                }
                            };
                            rateFiltersList.add(rateFilter);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching rate filters from request.", e);
        }
        LOG.debug("Returning rate filters as: " + rateFiltersList);
        LOG.trace("Method Exit: " + methodName);
        return rateFiltersList;

    }

    /**
     * @param queryMap
     * @return
     */
    private List<RoomOccupants> getRoomOccupantsFromRequest(Map<String, String> queryMap) {
        String methodName = "getRoomOccupantsFromRequest";
        LOG.trace("Method Entry: {}", methodName);
        List<RoomOccupants> occupantsList = new ArrayList<>();
        try {

            Object roomOccupantOptions = queryMap.get(HttpRequestParams.OCCUPANCY);
            LOG.debug("Received OCCUPANCY as: {}", roomOccupantOptions);

            if (roomOccupantOptions != null) {

                String roomOccupantsString = '[' + roomOccupantOptions.toString() + ']';
                LOG.debug("roomOccupantsString: {}", roomOccupantsString);
                if (!roomOccupantsString.isEmpty()) {
                    JSONArray jsonArray = new JSONArray(roomOccupantsString);
                    LOG.debug("Parsed occupants string to json as: {}", jsonArray);

                    int numOfOcupancies = jsonArray.length();
                    for (int i = 0; i < numOfOcupancies; i++) {
                        // JSONArray occupancyArray = jsonArray.getJSONArray(i);
                        RoomOccupants roomOccupants = new RoomOccupants();
                        roomOccupants.setNumberOfAdults(jsonArray.optInt(i));
                        roomOccupants.setNumberOfChildren(jsonArray.optInt(i + 1));
                        i++;
                        LOG.debug("room occupancy:-> adults: " + roomOccupants.getNumberOfAdults() + ", children: "
                                + roomOccupants.getNumberOfChildren());
                        occupantsList.add(roomOccupants);
                    }
                }
            }
        } catch (JSONException e) {
            LOG.error("An error occured while processing the provided room occupants in the request.", e);
        }

        LOG.debug("Returning occupants list as: {}", occupantsList);
        LOG.trace("Method Exit: {}", methodName);
        return occupantsList;
    }

    /**
     * @param queryMap
     * @return
     */
    private List<String> getHotelIdsFromRequest(Map<String, String> queryMap) {
        String methodName = "getHotelIdsFromRequest";
        LOG.trace("Method Entry: {}", methodName);

        List<String> hotelIdList = new ArrayList<>();
        Object hotelIds = queryMap.get(HttpRequestParams.HOTEL_IDS);
        LOG.debug("Received hotel ids array from request as: {}", hotelIds);

        if (hotelIds != null && hotelIds instanceof String) {
            hotelIdList.add(hotelIds.toString());
        }

        LOG.debug("Returning hotel ids list as: {}", hotelIdList);
        LOG.trace("Method Exit: {}", methodName);
        return hotelIdList;
    }

    /**
     * @param httpRequest
     * @param response
     * @param roomAvailabilityResponse
     * @param code
     * @param message
     * @throws Exception
     */

    private void writeSuccessResponseToJson(final SlingHttpServletRequest httpRequest,
            final SlingHttpServletResponse response, List<CalendarAvailabilityResponse> calendarAvailResp,
            String code, String message) throws Exception {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}", methodName);

        LOG.debug("\n\n\n\n\n\n-----------------------JSON RETURNED--------------------\n");
        LOG.debug(calendarAvailResp.toString());
        LOG.debug("\n-----------------------JSON END--------------------\n\n\n\n\n\n");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        writeKeyAndMessage(code, message, jsonNode);
        
        if (code.equals(RESPONSE_CODE_SUCCESS)) {
        	//mapCalendarResponse(calendarAvailResp, jsonNode, httpRequest);
        	response.getWriter().write(mapper.writeValueAsString(calendarAvailResp.get(0)));
        	//response.getWriter().write(jsonNode.toString());
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    /**
     * @param code
     * @param message
     * @param jsonNode
     */
    private void writeKeyAndMessage(String code, String message, ObjectNode jsonNode) {

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
    }
    
    private void mapCalendarResponse(List<CalendarAvailabilityResponse> calendarAvailResp, ObjectNode jsonNode, 
    		final SlingHttpServletRequest httpRequest) {
		HashMap<String, List<PriceDto>> calendarMonthsMap = new HashMap<String,List<PriceDto>>();
    	ObjectMapper mapper = new ObjectMapper();
    	ObjectNode calendarNode = mapper.createObjectNode();
    	
    	Map<String, String> queryParamMap = getQueryParams(httpRequest);
    	
    	ArrayList<String> dateString = new ArrayList<>();
		listMonths(queryParamMap.get(HttpRequestParams.CHECK_IN_DATE), 
    				queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE),dateString);
		dateString.add(queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE));
    	
		for(int i=0; i<dateString.size(); i++) {
			String datefromList = dateString.get(i);
			String key = dateKey(datefromList);
			List<PriceDto> priceList = new ArrayList<>();
			calendarMonthsMap.put(key, priceList);
		}
		createMonthWiseNode(calendarMonthsMap, jsonNode);
		
//    	if(calendarAvailResp != null && calendarAvailResp.size() > 0) {
//    		
//    		CalendarAvailabilityResponse calendarResponse = calendarAvailResp.get(0);
//    		jsonNode.put(ERROR_MESSAGE, calendarResponse.getErrorMessage());
//    		jsonNode.put(SYNXIS_ERROR_CODE, calendarResponse.getErrorCode());
//    		
//    		calendarNode.put("hotelCode", calendarResponse.getHotelStays().get(0).getHotelCode());
//    		calendarNode.put("hotelName", calendarResponse.getHotelStays().get(0).getHotelName());
//    		
//			if(calendarResponse.getHotelStays() != null && calendarResponse.getHotelStays().size() > 0) {
//				for(CalendarHotelStayDto hotelStay : calendarResponse.getHotelStays()) {
//					List<PriceDto> prices = hotelStay.getPrices();
//					if(prices != null && prices.size() > 0) {
//						for(PriceDto price : prices) {
//							String monthStartKey = dateKey(price.getStart());
//							String monthEndtKey = dateKey(price.getEnd());
//		    				if(calendarMonthsMap.containsKey(monthStartKey)) {
//		    					List<PriceDto> existingMonthStays =  calendarMonthsMap.get(monthStartKey);
//		    					existingMonthStays.add(price);
//		    					calendarMonthsMap.put(monthStartKey, existingMonthStays);
//		    				}
//		    				else {
//		    					List<PriceDto> priceList = new ArrayList<>();
//		    					priceList.add(price);
//		    					calendarMonthsMap.put(monthEndtKey, priceList);
//		    				}
//		    				if(calendarMonthsMap.containsKey(monthEndtKey)) {
//		    					List<PriceDto> existingMonthStays =  calendarMonthsMap.get(monthEndtKey);
//		    					existingMonthStays.add(price);
//		    					calendarMonthsMap.put(monthEndtKey, existingMonthStays);
//		    				}
//		    				else {
//		    					List<PriceDto> priceList = new ArrayList<>();
//		    					priceList.add(price);
//		    					calendarMonthsMap.put(monthEndtKey, priceList);
//		    				}
//						}
//					}
//				}
//			}
//    		createMonthWiseNode(calendarMonthsMap, calendarNode);
//    	}
    	jsonNode.put("hotelStay", calendarNode);
    }

    private String dateKey(String specifiedDate) {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		String key = "";
		try {
			Date date = sd.parse(specifiedDate);
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			key = localDate.getMonth().toString()+ "-" +localDate.getYear();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return key;
    }
    
    private void listMonths(String startDate, String endDate, ArrayList<String> dateString) {
        DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        Calendar beginCalendar = Calendar.getInstance();
        Calendar finishCalendar = Calendar.getInstance();
        try {
            beginCalendar.setTime(formater.parse(startDate));
            finishCalendar.setTime(formater.parse(endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        while (beginCalendar.before(finishCalendar)) {
            String date = formater.format(beginCalendar.getTime()).toUpperCase();
            dateString.add(date);
            beginCalendar.add(Calendar.MONTH, 1);
        }
    }
    
    private void createMonthWiseNode(HashMap<String, List<PriceDto>> calendarMonthsMap, ObjectNode calendarNode) {
    	ObjectMapper mapper = new ObjectMapper();
		Iterator<Entry<String, List<PriceDto>>> hmIterator = calendarMonthsMap.entrySet().iterator();
    	ObjectNode hotelStayNode = mapper.createObjectNode();
        while (hmIterator.hasNext()) {
        	ArrayNode staysNode = mapper.createArrayNode();
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            LOG.info("key from hashMap" + mapElement.getKey().toString());
            List<PriceDto> calendarStays = (List<PriceDto>) mapElement.getValue();
//            for(PriceDto price : calendarStays) {
//            	stayNode.put("duration", price.getDuration());
//            	stayNode.put("end", price.getEnd());
//            	stayNode.put("start", stays.getStart());
//            	stayNode.put("status", price.getStatus());
//            	stayNode.put("quantity", price.getQuantity());
            	
//            	staysNode.add(stayNode);
//            }
        	ObjectNode stayNode = mapper.createObjectNode();
            mapPrice(stayNode, calendarStays);
            hotelStayNode.put(mapElement.getKey().toString(), staysNode);
        }
        calendarNode.put("calendar", hotelStayNode);
    }
    
    private void mapPrice(ObjectNode stayNode, List<PriceDto> prices) {
    	ObjectMapper mapper = new ObjectMapper();
    	ArrayNode pricesNode = mapper.createArrayNode();
    	for(PriceDto price : prices) {
    		ObjectNode priceNode = mapper.createObjectNode();
    		priceNode.put("duration", price.getDuration());
    		priceNode.put("amountBeforeTax", price.getAmountBeforeTax());
    		priceNode.put("end", price.getEnd());
    		priceNode.put("start", price.getStart());
    		pricesNode.add(priceNode);
    	}
    	stayNode.put("prices", pricesNode);
    }
}
