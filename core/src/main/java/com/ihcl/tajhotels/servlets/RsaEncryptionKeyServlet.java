package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.RsaConfigurationService;
import com.ihcl.loyalty.services.IUserDetailsFilterService;
import com.ihcl.loyalty.services.IUserDetailsService;
import com.ihcl.tajhotels.signin.api.IUserAccount;

@Component(service = Servlet.class, immediate = true, property = {
        "sling.servlet.paths=" + "/bin/SessionRsa", "sling.servlet.methods=" + HttpConstants.METHOD_GET
})

public class RsaEncryptionKeyServlet  extends SlingSafeMethodsServlet{
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(RsaEncryptionKeyServlet.class);

    @Reference
    private IUserAccount userAccount;

    @Reference
    private IUserDetailsService userDetailsService;

    @Reference
    private IUserDetailsFilterService userDetailsFilterService;
	
	@Reference
    RsaConfigurationService config;

	
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
            IOException {
        String methodName = "doGet";
        
        String publicKey = config.getPrivateKey();
        LOG.info("Method Entry: " + methodName);
        JSONObject jsonNode = new JSONObject();
        
        try {
			jsonNode.put("sessionToken", publicKey);
			response.setContentType("application/json");
	        response.getWriter().write(jsonNode.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			response.sendError(500, e.getMessage());
			e.printStackTrace();
		}
        LOG.info("Method Exit: " + methodName);
    }

}
