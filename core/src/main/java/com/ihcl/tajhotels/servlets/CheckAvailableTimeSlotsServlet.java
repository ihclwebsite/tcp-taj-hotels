package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.AVAILABLE_TIMESLOTS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CHECK_AVAILABILITY_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CHECK_AVAILABILITY_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CHECK_AVAILABILITY_TECHNICAL_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.api.ICheckAvailibilityService;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.requestdto.AvailabilityByTimeSlotRequest;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.responsedto.AvailabilityByTimeSlot;

/**
 * Servlet that writes some sample content into the response. It is mounted for all resources of a specific Sling
 * resource type. The {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are idempotent. For write
 * operations use the {@link SlingAllMethodsServlet}.
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/checkAvailableTimeSlot", "sling.servlet.methods=GET",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class CheckAvailableTimeSlotsServlet extends SlingAllMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(CheckAvailableTimeSlotsServlet.class);

    @Reference
    private ICheckAvailibilityService availableTimeSlotsService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("application" + "/json" + ";" + "charset=UTF-8");
            fetchTimeSlotDetailsFromRequest(request, response);
        } catch (Exception e) {
            LOG.error("An error occured while fetching time slots : {}", e.getMessage());
            LOG.debug("An error occured while fetching time slots : {}", e);
            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, CHECK_AVAILABILITY_TECHNICAL_FAILURE,
                    StringUtils.EMPTY);
        }
    }

    private void fetchTimeSlotDetailsFromRequest(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        int sessionValue;
        String dateValue = request.getParameter("dateVal");
        String interfaceId = request.getParameter("interfaceId");
        int peoplecount = Integer.parseInt(request.getParameter("peopleCount"));
        String mealType = request.getParameter("mealType");
        int diningID = Integer.parseInt(request.getParameter("restaurantID"));
        if (mealType.equals("Breakfast")) {
            sessionValue = 1;
        } else if (mealType.equals("Lunch")) {
            sessionValue = 2;
        } else if (mealType.equals("Dinner")) {
            sessionValue = 3;
        } else if (mealType.equals("Brunch")) {
            sessionValue = 4;
        } else {
            sessionValue = 0;
        }
        setTimeSlotRequest(response, peoplecount, dateValue, diningID, sessionValue, interfaceId);
    }


    private void setTimeSlotRequest(SlingHttpServletResponse response, int peoplecount, String dateValue, int diningID,
            int sessionValue, String interfaceId) {
        AvailabilityByTimeSlotRequest requestobj = new AvailabilityByTimeSlotRequest();
        requestobj.setNoOfPeople(peoplecount);
        requestobj.setReservationDate(dateValue);
        requestobj.setRestaurantId(diningID);
        requestobj.setSessionId(sessionValue);
        requestobj.setInterfaceId(interfaceId);
        getTimeSlotsFromService(requestobj, response);
    }

    private void getTimeSlotsFromService(AvailabilityByTimeSlotRequest requestobj, SlingHttpServletResponse response) {
        try {

            AvailabilityByTimeSlot availabilityByTimeSlot = (AvailabilityByTimeSlot) availableTimeSlotsService
                    .getAvailabilityByTimeSlot(requestobj);
            if (availabilityByTimeSlot.isSuccess()) {
                String availableSlots = availabilityByTimeSlot.getTimeValue();
                writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, CHECK_AVAILABILITY_SUCCESS, availableSlots);
            } else {
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, CHECK_AVAILABILITY_FAILURE, StringUtils.EMPTY);
            }
        } catch (IOException e) {
            LOG.error("IOException found in getTimeSlotsFromService :: {}", e.getMessage());
            LOG.debug("IOException found in getTimeSlotsFromService :: {}", e);
        }
    }


    /**
     * @param response
     * @param code
     * @param message
     * @param availableSlots
     * @throws IOException
     */
    private void writeJsonToResponse(SlingHttpServletResponse response, String code, String message,
            String availableSlots) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        jsonNode.put(AVAILABLE_TIMESLOTS, availableSlots);
        response.getWriter().write(jsonNode.toString());
        LOG.trace("The Value of JSON: {}", jsonNode);
    }


}
