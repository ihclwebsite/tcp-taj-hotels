package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.servicehandler.processpayment.PaymentHandler;
import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "= Servlet to initiate payment for purchase tic/epicure",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST, "sling.servlet.paths=" + "/bin/purchase/pay" })
public class PurchasePaymentServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(PurchasePaymentServlet.class);
    private static final long serialVersionUID = 2868486396002288247L;

    @Reference
    private ConfigurationService conf;

    private PaymentHandler paymentHandler = new PaymentHandler();

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String> paymentDetails = buildPaymentDetails(request);
        String paymentParameters = paymentHandler.parsePaymentParameters(paymentDetails);
        String encodedString = paymentHandler.encodePaymentRequest(paymentParameters, conf.getCCAvenueWorkingKey());
        String paymentGatewayResp = paymentHandler.createPaymentForm(conf.getCCAvenueURL(), encodedString,
                conf.getCCAvenueAccessKey());
        response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
        LOG.info("Returning response to page");
        response.getWriter().write(paymentGatewayResp);

    }

    private Map<String, String> buildPaymentDetails(SlingHttpServletRequest request) {


        Map<String, String> paymentDetails = new HashMap<>();
        paymentDetails.put(BookingConstants.CCAVENUE_MERCHANT_ID_NAME, conf.getOnlineMerchantId());
        paymentDetails.put(BookingConstants.CCAVENUE_ORDER_ID_NAME, request.getParameter("MemberNumber"));
        paymentDetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, "INR");
        paymentDetails.put(BookingConstants.CCAVENUE_AMOUNT_NAME, fetchAmount(request));
        paymentDetails.put(BookingConstants.CCAVENUE_REDIRECT_URL_NAME, conf.getPurchasePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_CANCEL_NAME, conf.getPurchasePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_LANGUAGE_NAME, BookingConstants.CCAVENUE_PARAM_LANGUAGE);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_PROMO_CODE_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_TID_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, request.getParameter("MemberNumber"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, request.getParameter("PointType"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, request.getParameter("ProductName"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH4_NAME, request.getParameter("PointsToBuy"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        return paymentDetails;
    }


    public String fetchAmount(SlingHttpServletRequest request) {
        String billAmount = request.getParameter("PointsToBuy");
        int bill = 0;
        if (request.getParameter("PointType").equals("TIC")) {
            bill = Integer.parseInt(billAmount) * 5;
        } else {
            bill = Integer.parseInt(billAmount) * 10;
        }
        return String.valueOf(bill);
    }


}
