package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.ihcl.core.servicehandler.processpayment.PaymentHandler;
import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "= Servlet to initiate payment for campaign epicure",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths=" + "/bin/addon/pay/campaign" })
public class AddOnCampaignPaymentServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AddOnCampaignPaymentServlet.class);
    private static final long serialVersionUID = -3427887814571422899L;

    @Reference
    private ConfigurationService conf;

    private PaymentHandler paymentHandler = new PaymentHandler();

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        Map<String, String> paymentDetails = buildPaymentDetails(request);
        String paymentParameters = paymentHandler.parsePaymentParameters(paymentDetails);
        String purchaseCCAvenueWorkingKey = conf.getPurchaseCCAvenueWorkingKey();
        if (purchaseCCAvenueWorkingKey.equals("")) {
            purchaseCCAvenueWorkingKey = conf.getCCAvenueWorkingKey();
        }
        String ccAvenueAccesskey = conf.getPurchaseCCAvenueAccessKey();
        if (ccAvenueAccesskey.equals("")) {
            ccAvenueAccesskey = conf.getCCAvenueAccessKey();
        }
        String encodedString = paymentHandler.encodePaymentRequest(paymentParameters, purchaseCCAvenueWorkingKey);
        String paymentGatewayResp = paymentHandler.createPaymentForm(conf.getCCAvenueURL(), encodedString,
                ccAvenueAccesskey);
        LOG.debug("print the keys::" + conf.getPurchaseMerchantId() + "access::" + ccAvenueAccesskey + ":working key:"
                + purchaseCCAvenueWorkingKey);
        response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
        LOG.info("Returning response to page");
        response.getWriter().write(paymentGatewayResp);

    }

    private Map<String, String> buildPaymentDetails(SlingHttpServletRequest request) {
        Map<String, String> paymentDetails = new HashMap<>();
        String merchantId = conf.getPurchaseMerchantId();
        if (merchantId.equals("")) {
            merchantId = conf.getOnlineMerchantId();
        }
        paymentDetails.put(BookingConstants.CCAVENUE_MERCHANT_ID_NAME, merchantId);
        paymentDetails.put(BookingConstants.CCAVENUE_ORDER_ID_NAME, "Ep-" + request.getParameter("membershipId"));
        paymentDetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, "INR");
        paymentDetails.put(BookingConstants.CCAVENUE_AMOUNT_NAME, conf.getCamapignEpicureAmount());
        LOG.debug("get the amount::" + conf.getCamapignEpicureAmount());
        paymentDetails.put(BookingConstants.CCAVENUE_REDIRECT_URL_NAME, conf.getEpicurePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_CANCEL_NAME, conf.getEpicurePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_LANGUAGE_NAME, BookingConstants.CCAVENUE_PARAM_LANGUAGE);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_NAME,
                combineMultipleParam(request.getParameter("firstName"), request.getParameter("lastName")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME,
                combineMultipleParam(request.getParameter("address1"), request.getParameter("address2")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME,
                checkOptionalParam(request.getParameter("city")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME,
                checkOptionalParam(request.getParameter("state")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME,
                checkOptionalParam(request.getParameter("pincode")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME,
                checkOptionalParam(request.getParameter("country")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME,
                checkOptionalParam(request.getParameter("mobile")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_PROMO_CODE_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_TID_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, request.getParameter("membershipId"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, request.getParameter("addonName"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, request.getParameter("addonActivity"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH4_NAME,
                checkOptionalParam(request.getParameter("renewCardNumber")));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        return paymentDetails;
    }

    private String checkOptionalParam(String param) {
        if (!Strings.isNullOrEmpty(param)) {
            return param;
        }
        return BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING;
    }

    private String combineMultipleParam(String param1, String param2) {
        if (!Strings.isNullOrEmpty(param1) && !Strings.isNullOrEmpty(param2)) {
            return param1 + " " + param2;
        }
        return BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING;
    }
}
