package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.integration.api.IReadReservationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.RoomsDetails;
import com.ihcl.tajhotels.email.requestdto.SendReservationEmail;


@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/shareOnEmail" })
public class BookHotelSendEmailServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(BookHotelSendEmailServlet.class);

    @Reference
    private IReadReservationService hotelReadReservation;

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService emailConf;

    @Override
    protected void doPost(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doPost";
        LOG.trace("Method Entry: {}", methodName);
        ResourceResolver resolver = null;
        Session session = null;
        try {

            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

            ObjectMapper mapper = new ObjectMapper();
            String recipientEmail = httpRequest.getParameter("recipientEmail");
            String hotelAddress = httpRequest.getParameter(ReservationConstants.HOTEL_ADDRESS);
            String emailAddresss = httpRequest.getParameter(ReservationConstants.EMAIL_ADDRESS);
            String mailToOthersOnly = httpRequest.getParameter("mailToOthers");
            
            resolver = httpRequest.getResourceResolver();
            session = resolver.adaptTo(Session.class);
            FindReservationHelper helper = new FindReservationHelper();
            String bookingJson = helper.getBookingReservationResponse(httpRequest, hotelReadReservation, resolver,
                    session);

            if (bookingJson != null && recipientEmail != null) {
                LOG.info("booking json string : {}", bookingJson);
                BookingObject bookingObjectRequest = mapper.readValue(bookingJson, new TypeReference<BookingObject>() {
                });
//                if (bookingObjectRequest == null || bookingObjectRequest.getGuest() == null
//                        || StringUtils.isEmpty(bookingObjectRequest.getGuest().getEmail())
//                        || !bookingObjectRequest.getGuest().getEmail().equalsIgnoreCase(emailAddresss)) {
//                    throw new HotelBookingException("email not matched with reservation");
//                }
                String bookerEmail = "";
                String adminEmail = "";
                boolean isSendEmailToBooker = false;
                boolean isSendEmailToAdmin = false;
                boolean isSendEmailToRecipient = false;
                
                if(bookingObjectRequest.getGuest().getEmail().contains(",")) {
                	String[] emailList = bookingObjectRequest.getGuest().getEmail().split(",");
                	if(emailList != null && emailList.length > 0) {
                		bookerEmail = emailList[0];
                		adminEmail = emailList[1];
                	}
                	
                }
                if(mailToOthersOnly != null && mailToOthersOnly.equalsIgnoreCase("yes")) {
                	LOG.info("Email is for admin and booker");
                	isSendEmailToRecipient = true;
                } else {
                	isSendEmailToRecipient = sendReservationEmail(recipientEmail, hotelAddress, bookingObjectRequest);
                }
                if(bookerEmail != null && !bookerEmail.isEmpty()) {
                	LOG.info("Booker email is present." + bookerEmail);
                	isSendEmailToBooker = sendReservationEmail(bookerEmail, hotelAddress, bookingObjectRequest);
                }
                if(adminEmail != null && !adminEmail.isEmpty()) {
                	LOG.info("Admin email is present." + adminEmail);
                	isSendEmailToAdmin = sendReservationEmail(adminEmail, hotelAddress, bookingObjectRequest);
                }
                if(!isSendEmailToBooker) {
                	LOG.info("Email not sent to booker");
                }
                if(!isSendEmailToAdmin) {
                	LOG.info("Email not sent to Admin");
                }
                if (isSendEmailToRecipient) {
                    LOG.info("Email has been sent successfully ");
                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    LOG.info("email not sent to recipient");
                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            }
        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
            if (null != session) {
                session.logout();
            }
        }
        LOG.debug(httpRequest.getParameter("bookingData"));
    }

    /**
     * @param recipientEmail
     * @param hotelAddress
     * @param currency
     * @param bookingObjectRequest
     * @return
     */
    protected boolean sendReservationEmail(String recipientEmail, String hotelAddress,
            BookingObject bookingObjectRequest) {
    	LOG.info("inside sendReservationEmail :::: ");
        String roomsEmailSegment = getEmailSegmentForRoomsList(bookingObjectRequest);
        SendReservationEmail emailObject = getReservationEmailObject(bookingObjectRequest, roomsEmailSegment,
                hotelAddress);
        emailObject.setEmailSubject(
                emailConf.getEmailSubjectForReservationEmail() + bookingObjectRequest.getItineraryNumber());
        emailObject.setEmailBodyTemplate(emailConf.getEmailBodyTemplateForReservationEmail());
        emailObject.setUserEmail(recipientEmail.trim());
        String finalEmailBody = StringTokenReplacement.replaceToken(emailObject.getDynamicValues(),
                emailConf.getEmailBodyTemplateForReservationEmail());
        LOG.info("Final Email Body : {}"+ finalEmailBody);
        LOG.info("email subject :: {}", emailObject);
        LOG.info("user email "+ emailObject.getUserEmail());
        return emailService.sendReservationEmail(emailObject, finalEmailBody);
    }

    /**
     * This method creates a dynamic value map for the reservation email and adds it to the SendReservationEmail object
     *
     * @return
     */
    private SendReservationEmail getReservationEmailObject(BookingObject bookingObject, String roomsSegment,
            String hotelAddress) {

        LOG.trace("Method Entry: getReservationEmailObject");
        SendReservationEmail email = new SendReservationEmail();
        HashMap<String, String> emailMap = new HashMap<>();
        String tajSite = "www.tajhotels.com";
        emailMap.put("itineraryNumber", bookingObject.getItineraryNumber());
        emailMap.put("hotelName", bookingObject.getHotel().getHotelName());
        emailMap.put("hotelAddress", hotelAddress);
        emailMap.put("hotelPhone", bookingObject.getHotel().getHotelPhoneNumber());
        emailMap.put("hotelEmail", bookingObject.getHotel().getHotelEmail());
        emailMap.put("tajSite", tajSite);
        emailMap.put("bookedUserFirstName", bookingObject.getGuest().getFirstName());
        emailMap.put("bookedUserLastName", bookingObject.getGuest().getLastName());
        emailMap.put("checkinOn", bookingObject.getCheckInDate());
        emailMap.put("checkoutOn", bookingObject.getCheckOutDate());
        emailMap.put("specialReqests", bookingObject.getGuest().getSpecialRequests());
        emailMap.put("roomsListString", roomsSegment);

        if (hotelAddress != null) {
            emailMap.put("hotelAddress", hotelAddress);
        }

        email.setDynamicValues(emailMap);

        LOG.trace("Method Exit: getReservationEmailObject");
        return email;
    }

    /**
     * This method uses room details template to generate email segment for rooms booked dynamically
     *
     * @param bookingObjectRequest
     * @return HTML segment as String
     */
    private String getEmailSegmentForRoomsList(BookingObject bookingObjectRequest) {

        LOG.trace("Method Entry: getEmailSegmentForRoomsList");
        HashMap<String, String> roomsMap = null;
        List<Room> rooms = bookingObjectRequest.getRoomList();
        RoomsDetails roomForEmail = null;
        int roomCount = 0;
        String roomsSegment = "";
        for (Room room : rooms) {
            roomsMap = new HashMap<>();
            roomCount++;
            roomsMap.put("roomnumber", String.valueOf(roomCount));
            roomsMap.put("confirmationNumber", room.getReservationNumber());
            roomsMap.put("adults", String.valueOf(room.getNoOfAdults()));
            roomsMap.put("children", String.valueOf(room.getNoOfChilds()));
            roomsMap.put("roomType", room.getRoomTypeName());
            roomsMap.put("rateDesc", room.getRateDescription());
            roomsMap.put("rateApplied", room.getRatePlanName());
            roomsMap.put("taxApplied", room.getTaxes().get(0).getTaxAmount());
            roomsMap.put("price", room.getRoomCostAfterTax());
            roomsMap.put("cancelPolicy", room.getCancellationPolicy());
            roomsMap.put("currency", bookingObjectRequest.getCurrencyCode());
            roomForEmail = new RoomsDetails();
            roomForEmail.setDynamicValues(roomsMap);
            String thisRoomString = StringTokenReplacement.replaceToken(roomForEmail.getDynamicValues(),
                    emailConf.getEmailBodyTemplateForReservationEmailRoomsSegment());
            roomsSegment = roomsSegment.concat(thisRoomString);
        }
        LOG.trace("Method Exit: getEmailSegmentForRoomsList");
        return roomsSegment;
    }

    private void writeJsonToResponse(SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}", methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of Response JSON: {}", jsonString);
        LOG.trace("Method Exit: {}", methodName);

        response.getWriter().write(jsonNode.toString());

    }
    
}
