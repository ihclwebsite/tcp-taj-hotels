/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.SafariEnquiryForm;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/safariEnquiry" })
public class SafariEnquiryFormServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(SafariEnquiryFormServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService emailTemplateConf;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        String finalHtmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();
        LOG.trace("Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for safari enquiry form is : " + requestUrl);

        try {
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                SafariEnquiryForm requestobj = new SafariEnquiryForm();

                // Fields for email template configuration
                if (null != emailTemplateConf) {
                    // From html form
                    requestobj.setTitle(httpRequest.getParameter("title"));
                    requestobj.setFirstName(httpRequest.getParameter("firstName"));
                    requestobj.setLastName(httpRequest.getParameter("lastName"));
                    requestobj.setNationality(httpRequest.getParameter("nationality"));
                    requestobj.setEmailId(httpRequest.getParameter("emailId"));
                    requestobj.setAlternateEmail(httpRequest.getParameter("alternateEmail"));
                    requestobj.setContactNumber(httpRequest.getParameter("contactNumber"));
                    requestobj.setComments(httpRequest.getParameter("comments"));
                    requestobj.setTravelPlanFromDate(httpRequest.getParameter("travelPlanFromDate"));
                    requestobj.setTravelPlanToDate(httpRequest.getParameter("travelPlanToDate"));
                    requestobj.setNumberOfAdults(httpRequest.getParameter("numberOfAdults"));
                    requestobj.setNumberOfChildren(httpRequest.getParameter("numberOfChildren"));
                    requestobj.setLodges(httpRequest.getParameter("checkedValue"));
                    requestobj.setSource(httpRequest.getParameter("source"));
                    requestobj.setMedium(httpRequest.getParameter("medium"));
                    requestobj.setCampaign(httpRequest.getParameter("campaign"));

                    requestobj.setDynamicValuesForEmail(dynamicValuemap);
                    LOG.trace("Checked values are :" + httpRequest.getParameter("checkedValue"));

                    // From email template configuration
                    requestobj.setEmailToAddress(emailTemplateConf.getEmailToAddressForSafariEnquiry());
                    requestobj.setEmailSubject(emailTemplateConf.getEmailSubjectForSafariEnquiry());
                    requestobj.setEmailTemplateForBody(emailTemplateConf.getEmailBodyForSafariEnquiry());

                    dynamicValuemap = getDynamicValuesForSafariEnquiryEmail(requestobj, dynamicValuemap);

                    String htmlString = requestobj.getEmailTemplateForBody();
                    String unescape = StringEscapeUtils.unescapeHtml4(htmlString);

                    LOG.trace("Unescaped string  : " + unescape);

                    finalHtmlTemplate = StringTokenReplacement.replaceToken(requestobj.getDynamicValuesForEmail(),
                            unescape);
                    LOG.trace("Final html template for safari enquiry form email is : " + finalHtmlTemplate);
                } else if (null == emailTemplateConf) {
                    LOG.warn("Email Configuration is missing");
                }

                boolean isSendEmailSuccess = emailService.sendSafariEnquiry(requestobj, finalHtmlTemplate);
                if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while Sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * Method to set map fields with token names and values
     *
     * @param dynamicValuemap
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValuesForSafariEnquiryEmail(SafariEnquiryForm requestobj,
            HashMap<String, String> dynamicValuemap) {
        dynamicValuemap.put("title", requestobj.getTitle());
        dynamicValuemap.put("firstName", requestobj.getFirstName());
        dynamicValuemap.put("lastName", requestobj.getLastName());
        dynamicValuemap.put("nationality", requestobj.getNationality());
        dynamicValuemap.put("emailId", requestobj.getEmailId());
        dynamicValuemap.put("alternateEmail", requestobj.getAlternateEmail());
        dynamicValuemap.put("contactNumber", requestobj.getContactNumber());
        dynamicValuemap.put("comments", requestobj.getComments());
        dynamicValuemap.put("travelPlanFromDate", requestobj.getTravelPlanFromDate());
        dynamicValuemap.put("travelPlanToDate", requestobj.getTravelPlanToDate());
        dynamicValuemap.put("numberOfAdults", requestobj.getNumberOfAdults());
        dynamicValuemap.put("numberOfChildren", requestobj.getNumberOfChildren());
        dynamicValuemap.put("lodges", requestobj.getLodges());
        dynamicValuemap.put("source", requestobj.getSource());
        dynamicValuemap.put("medium", requestobj.getMedium());
        dynamicValuemap.put("campaign", requestobj.getCampaign());
        return dynamicValuemap;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }


}
