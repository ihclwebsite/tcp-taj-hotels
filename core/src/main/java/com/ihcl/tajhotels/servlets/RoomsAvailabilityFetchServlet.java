/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.ROOMS_AVAILABILITY_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.ROOMS_AVAILABILITY_SUCCESS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONArray;
import org.json.JSONException;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.serviceamenity.ServiceAmenity;
import com.ihcl.core.services.booking.GuaranteeCodesConfigurationService;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.core.services.serviceamenity.RatePlanCodeAmenitiesFetcherService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.CurrencyCodeDto;
import com.ihcl.integration.dto.availability.HotelAvailabilityResponse;
import com.ihcl.integration.dto.availability.HotelDto;
import com.ihcl.integration.dto.availability.RateFilterDto;
import com.ihcl.integration.dto.availability.RatePlanDto;
import com.ihcl.integration.dto.availability.RoomAvailabilityDto;
import com.ihcl.integration.dto.availability.RoomDto;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.integration.dto.common.DiscountedNightlyRates;
import com.ihcl.integration.dto.common.NightlyRates;
import com.ihcl.integration.dto.common.Taxes;
import com.ihcl.tajhotels.constants.HttpRequestParams;
import com.ihcl.tajhotels.constants.HttpResponseParams;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/room-rates/rooms-availability" })
public class RoomsAvailabilityFetchServlet extends SlingSafeMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(RoomsAvailabilityFetchServlet.class);

    @Reference
    private IRoomAvailabilityService roomAvailabilityService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    @Reference
    private GuaranteeCodesConfigurationService guaranteeCodesConfigurationService;

    @Reference
    private RatePlanCodeAmenitiesFetcherService ratePlanCodeAmenitiesFetcherService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("Method Entry: {}", methodName);

        LOG.debug("Availability searchParams: {}", getQueryParams(httpRequest).toString());

        try {
            LOG.debug("Received room availability service as: {}", roomAvailabilityService);
            LOG.debug("Received rate plan code amenities fetcher as: {}", ratePlanCodeAmenitiesFetcherService);

            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

            List<HotelAvailabilityResponse> roomAvailabilityResponse = null;
            if (synixsDowntimeConfigurationService.isForcingToSkipCalls()) {

                LOG.info("isForcingToSkipCalls: {}", synixsDowntimeConfigurationService.isForcingToSkipCalls());
                ObjectMapper mapper = new ObjectMapper();
                ObjectNode jsonNode = mapper.createObjectNode();
                jsonNode.put("serviceError", synixsDowntimeConfigurationService.getDowntimeMessage());
                writeKeyAndMessage(RESPONSE_CODE_FAILURE, ROOMS_AVAILABILITY_FAILURE, jsonNode);
                response.setStatus(Integer.valueOf(synixsDowntimeConfigurationService.getServletStatusCode()));
                invalidateCache(response);
                response.getWriter().write(jsonNode.toString());
            } else {
                roomAvailabilityResponse = fetchAvailabilityFromService(httpRequest);

                LOG.debug("Received room availability response from service as: {}", roomAvailabilityResponse);
                if (roomAvailabilityResponse.isEmpty()) {
                    LOG.error("An error occured while fetching room availability due to handled exception.");

                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    invalidateCache(response);

                    writeFailureResponseToJson(response, RESPONSE_CODE_FAILURE, ROOMS_AVAILABILITY_FAILURE);
                    return;
                } else {
                    boolean isSuccess = checkForResults(roomAvailabilityResponse);
                    if (!isSuccess) {
                        invalidateCache(response);
                    }
                }

                writeSuccessResponseToJson(httpRequest, response, roomAvailabilityResponse, RESPONSE_CODE_SUCCESS,
                        ROOMS_AVAILABILITY_SUCCESS);
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability due to thrown exception", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            invalidateCache(response);

            writeFailureResponseToJson(response, RESPONSE_CODE_FAILURE, ROOMS_AVAILABILITY_FAILURE);
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    private boolean checkForResults(List<HotelAvailabilityResponse> roomAvailabilityResponse) {
        HotelAvailabilityResponse response = roomAvailabilityResponse.get(0);
        if (response.isSuccess()) {
            LOG.info("Rate fetched successfuly");
            return true;
        } else {
            boolean isAnySuccess = false;

            isAnySuccess = isAnySuccess || !response.getRoomAvailabilityWithRateFilters().isEmpty();
            if (isAnySuccess) {
                LOG.info("Response available for starndard filters");
            } else {
                LOG.info("Response not available for starndard filters");
            }
            isAnySuccess = isAnySuccess || !response.getRoomAvailabiilityForOffers().isEmpty();
            if (isAnySuccess) {
                LOG.info("Response available for offer code");
            } else {
                LOG.info("Response not available for offer code");
            }
            isAnySuccess = isAnySuccess || !response.getRoomAvailabiilityForPromoCode().isEmpty();
            if (isAnySuccess) {
                LOG.info("Response available for promo code");
            } else {
                LOG.info("Response not available for promo code");
            }
            return isAnySuccess;
        }

    }

    private SlingHttpServletResponse invalidateCache(final SlingHttpServletResponse response) {
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        return response;
    }

    /**
     * @param response
     * @param responseCodeFailure
     * @param roomsAvailabilityFailure
     * @throws IOException
     */
    private void writeFailureResponseToJson(SlingHttpServletResponse response, String responseCodeFailure,
            String roomsAvailabilityFailure) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        writeKeyAndMessage(responseCodeFailure, roomsAvailabilityFailure, jsonNode);
        response.getWriter().write(jsonNode.toString());

    }

    /**
     * @param httpRequest
     * @return
     * @throws Exception
     */
    private List<HotelAvailabilityResponse> fetchAvailabilityFromService(SlingHttpServletRequest httpRequest)
            throws Exception {

        String methodName = "fetchAvailabilityFromService";
        LOG.trace("Method Entry: {}", methodName);

        RoomsAvailabilityRequest availabilityRequest = new RoomsAvailabilityRequest();

        Map<String, String> queryParamMap = getQueryParams(httpRequest);

        availabilityRequest.setRoomOccupants(getRoomOccupantsFromRequest(queryParamMap));

        availabilityRequest.setRateFilters(getRateFiltersFromRequest(queryParamMap));

        availabilityRequest.setRatePlanCodes(getRatePlanCodesFromRequest(queryParamMap));

        availabilityRequest.setCurrencyCode(getCurrenctCodeFromRequest(queryParamMap));

        availabilityRequest.setCheckInDate(queryParamMap.get(HttpRequestParams.CHECK_IN_DATE));

        availabilityRequest.setCheckOutDate(queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE));

        String promoCodeParam = queryParamMap.get(HttpRequestParams.PROMO_CODE);

        String corporateCodeParam = queryParamMap.get(HttpRequestParams.CORPORATE_CODE);

        String promotionCode = "";
        if (corporateCodeParam != null) {
            promotionCode = corporateCodeParam;
        } else if (promoCodeParam != null) {
            promotionCode = promoCodeParam;
        }
        availabilityRequest.setPromoCode(promotionCode);

        List<String> hotelIds = getHotelIdsFromRequest(queryParamMap);

        availabilityRequest.setHotelIdentifiers(hotelIds);

        LOG.debug("Attempting to fetch room availability using the request: {}", availabilityRequest);
        List<HotelAvailabilityResponse> roomAvailabilityResponse = roomAvailabilityService
                .getAvailabilityWithDifferentRooms(availabilityRequest);
        ObjectMapper objectMapper = new ObjectMapper();
        LOG.debug("Received availability response list as: {}",
                objectMapper.writeValueAsString(roomAvailabilityResponse));

        LOG.trace("Method Exit: {}", methodName);

        return roomAvailabilityResponse;

    }

    private Map<String, String> getQueryParams(SlingHttpServletRequest httpRequest) {
        // String selectorString = httpRequest.getRequestPathInfo().getSelectorString();
        // assert selectorString != null;
        // List<String> eachString = Arrays.asList(selectorString.split("\\."));
        // Map<String, String> queryParamMap = eachString.stream().map(s -> s.split("=",
        // 2))
        // .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));

        String suffixPath = httpRequest.getRequestPathInfo().getSuffix();
        LOG.debug("Received suffix path info: {}", suffixPath);

        String selectorString = suffixPath.substring(suffixPath.lastIndexOf('/') + 1, suffixPath.length() - 1);
        List<String> suffixes = Arrays.asList(suffixPath.split("/"));

        List<String> eachString = Arrays.asList(selectorString.split("\\."));
        Map<String, String> queryParamMap = eachString.stream().map(s -> s.split("=", 2))
                .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));

        queryParamMap.put(HttpRequestParams.HOTEL_IDS, suffixes.get(1));
        queryParamMap.put(HttpRequestParams.CHECK_IN_DATE, suffixes.get(2));
        queryParamMap.put(HttpRequestParams.CHECK_OUT_DATE, suffixes.get(3));
        queryParamMap.put(HttpRequestParams.CURRENCY_SHORT_STRING, suffixes.get(4));
        queryParamMap.put(HttpRequestParams.OCCUPANCY, suffixes.get(5));
        queryParamMap.put(HttpRequestParams.RATE_FILTERS, suffixes.get(6));
        queryParamMap.put(HttpRequestParams.RATE_CODES, suffixes.get(7));

        return queryParamMap;
    }

    private List<String> getRatePlanCodesFromRequest(Map<String, String> queryMap) {
        List<String> ratePlanCodes = new ArrayList<>();
        try {
            if (queryMap.containsKey(HttpRequestParams.RATE_CODES)) {
                Object rateCodes = queryMap.get(HttpRequestParams.RATE_CODES);
                LOG.debug("Received rate codes as: {}", rateCodes);

                if (rateCodes != null) {
                    String rateCodesString = rateCodes.toString();
                    if (!rateCodesString.isEmpty()) {
                        JSONArray jsonArray = new JSONArray(rateCodesString);
                        LOG.debug("Parsed rate filters to json array as: {}", jsonArray);
                        int numOfRateCodes = jsonArray.length();

                        for (int i = 0; i < numOfRateCodes; i++) {
                            String rateFilterString = jsonArray.getString(i);

                            ratePlanCodes.add(rateFilterString);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error occurred while parsing rate plan codes from request", e);
        }
        return ratePlanCodes;
    }

    /**
     * @param queryMap
     * @return
     */
    private CurrencyCodeDto getCurrenctCodeFromRequest(Map<String, String> queryMap) {

        Object shortCurrencyString = queryMap.get(HttpRequestParams.CURRENCY_SHORT_STRING);
        Object currencyString = queryMap.get(HttpRequestParams.CURRENCY_STRING);

        return (new CurrencyCodeDto() {

            @Override
            public String getShortCurrencyString() {
                return (String) shortCurrencyString;
            }

            @Override
            public String getCurrencyString() {
                return (String) currencyString;
            }
        });
    }

    private List<RateFilterDto> getRateFiltersFromRequest(Map<String, String> queryMap) {
        String methodName = "getRateFiltersFromRequest";
        LOG.trace("Method Entry: {}", methodName);
        List<RateFilterDto> rateFiltersList = new ArrayList<>();
        try {
            if (queryMap.containsKey(HttpRequestParams.RATE_FILTERS)) {
                Object rateFilters = queryMap.get(HttpRequestParams.RATE_FILTERS);
                LOG.debug("Received rate filters as: {}", rateFilters);
                if (rateFilters != null) {
                    String rateFiltersString = rateFilters.toString();
                    if (!rateFiltersString.isEmpty()) {

                        JSONArray jsonArray = new JSONArray(rateFiltersString);
                        LOG.debug("Parsed rate filters to json array as: {}", jsonArray);
                        int numOfRateFilters = jsonArray.length();

                        for (int i = 0; i < numOfRateFilters; i++) {
                            String rateFilterString = jsonArray.getString(i);

                            LOG.debug("rateFilterString: {}", rateFilterString);
                            RateFilterDto rateFilter = new RateFilterDto() {

                                @Override
                                public String getCode() {
                                    return rateFilterString;
                                }

                                @Override
                                public String getTitle() {
                                    return rateFilterString;
                                }
                            };
                            rateFiltersList.add(rateFilter);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching rate filters from request.", e);
        }
        LOG.debug("Returning rate filters as: " + rateFiltersList);
        LOG.trace("Method Exit: " + methodName);
        return rateFiltersList;

    }

    /**
     * @param queryMap
     * @return
     */
    private List<RoomOccupants> getRoomOccupantsFromRequest(Map<String, String> queryMap) {
        String methodName = "getRoomOccupantsFromRequest";
        LOG.trace("Method Entry: {}", methodName);
        List<RoomOccupants> occupantsList = new ArrayList<>();
        try {

            Object roomOccupantOptions = queryMap.get(HttpRequestParams.OCCUPANCY);
            LOG.debug("Received OCCUPANCY as: {}", roomOccupantOptions);

            if (roomOccupantOptions != null) {

                String roomOccupantsString = '[' + roomOccupantOptions.toString() + ']';
                LOG.debug("roomOccupantsString: {}", roomOccupantsString);
                if (!roomOccupantsString.isEmpty()) {
                    JSONArray jsonArray = new JSONArray(roomOccupantsString);
                    LOG.debug("Parsed occupants string to json as: {}", jsonArray);

                    int numOfOcupancies = jsonArray.length();
                    for (int i = 0; i < numOfOcupancies; i++) {
                        // JSONArray occupancyArray = jsonArray.getJSONArray(i);
                        RoomOccupants roomOccupants = new RoomOccupants();
                        roomOccupants.setNumberOfAdults(jsonArray.optInt(i));
                        roomOccupants.setNumberOfChildren(jsonArray.optInt(i + 1));
                        i++;
                        LOG.debug("room occupancy:-> adults: " + roomOccupants.getNumberOfAdults() + ", children: "
                                + roomOccupants.getNumberOfChildren());
                        occupantsList.add(roomOccupants);
                    }
                }
            }
        } catch (JSONException e) {
            LOG.error("An error occured while processing the provided room occupants in the request.", e);
        }

        LOG.debug("Returning occupants list as: {}", occupantsList);
        LOG.trace("Method Exit: {}", methodName);
        return occupantsList;
    }

    /**
     * @param queryMap
     * @return
     */
    private List<String> getHotelIdsFromRequest(Map<String, String> queryMap) {
        String methodName = "getHotelIdsFromRequest";
        LOG.trace("Method Entry: {}", methodName);

        List<String> hotelIdList = new ArrayList<>();
        Object hotelIds = queryMap.get(HttpRequestParams.HOTEL_IDS);
        LOG.debug("Received hotel ids array from request as: {}", hotelIds);

        if (hotelIds != null && hotelIds instanceof String) {
            hotelIdList.add(hotelIds.toString());
        }

        LOG.debug("Returning hotel ids list as: {}", hotelIdList);
        LOG.trace("Method Exit: {}", methodName);
        return hotelIdList;
    }

    /**
     * @param httpRequest
     * @param response
     * @param roomAvailabilityResponse
     * @param code
     * @param message
     * @throws Exception
     */

    private void writeSuccessResponseToJson(final SlingHttpServletRequest httpRequest,
            final SlingHttpServletResponse response, List<HotelAvailabilityResponse> roomAvailabilityResponse,
            String code, String message) throws Exception {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}", methodName);

        LOG.debug("\n\n\n\n\n\n-----------------------JSON RETURNED--------------------\n");
        LOG.debug(roomAvailabilityResponse.toString());
        LOG.debug("\n-----------------------JSON END--------------------\n\n\n\n\n\n");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        writeKeyAndMessage(code, message, jsonNode);

        if (code.equals(RESPONSE_CODE_SUCCESS)) {
            processListAvailabilityResponse(httpRequest, response, jsonNode, roomAvailabilityResponse);
            response.getWriter().write(jsonNode.toString());
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    /**
     * @param code
     * @param message
     * @param jsonNode
     */
    private void writeKeyAndMessage(String code, String message, ObjectNode jsonNode) {

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
    }

    /**
     * @param httpRequest
     * @param response
     * @param jsonNode
     * @param roomAvailabilityResponses
     * @throws Exception
     */
    private void processListAvailabilityResponse(SlingHttpServletRequest httpRequest, SlingHttpServletResponse response,
            ObjectNode jsonNode, List<HotelAvailabilityResponse> roomAvailabilityResponses) throws Exception {

        String methodName = "processListAvailabilityResponse";
        LOG.trace("Method Entry: {}", methodName);

        List<String> hotelIdsList = getHotelIdsFromRequest(getQueryParams(httpRequest));
        LOG.debug("Received hotel ids from request are: {}", hotelIdsList);

        int sizeOfRoomAvailabilityResponse = roomAvailabilityResponses.size();
        LOG.debug("Size of Room Availability Response is: {}", sizeOfRoomAvailabilityResponse);
        for (int i = 0; i < sizeOfRoomAvailabilityResponse; i++) {
            HotelAvailabilityResponse roomAvailabilityResponse = roomAvailabilityResponses.get(i);
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode roomJsonNode = mapper.createObjectNode();

            for (String hotelId : hotelIdsList) {
                Map<RateFilterDto, Map<String, HotelDto>> rateFilterRoomsAvailability = roomAvailabilityResponse
                        .getRoomAvailabilityWithRateFilters();
                Map<String, HotelDto> rateCodeRoomsAvailability = roomAvailabilityResponse
                        .getRoomAvailabiilityForOffers();
                Map<String, HotelDto> promoCodeRoomsAvailability = roomAvailabilityResponse
                        .getRoomAvailabiilityForPromoCode();
                if (rateFilterRoomsAvailability != null) {
                    roomJsonNode.put("rateFilters", buildRateFilters(response, rateFilterRoomsAvailability, hotelId));
                }
                if (rateCodeRoomsAvailability != null) {
                    roomJsonNode.put("rateCodes", buildRateCodes(response, rateCodeRoomsAvailability, hotelId));
                }
                if (promoCodeRoomsAvailability != null) {
                    roomJsonNode.put("promoCode", buildPromoCode(response, promoCodeRoomsAvailability, hotelId));
                }
            }
            LOG.debug("Inserting availability response fo{}r room number: {}", i);
            jsonNode.put((i + 1) + "", roomJsonNode);
            LOG.debug("Root JSON node after insertion is: {}", jsonNode);
        }

        LOG.trace("Method Exit: {}", methodName);
    }

    /**
     * Build rateCodes json node.
     *
     * @param response
     *
     * @param roomsAvailability
     * @param hotelId
     * @return
     */
    private JsonNode buildRateCodes(SlingHttpServletResponse response, Map<String, HotelDto> roomsAvailability,
            String hotelId) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        for (Map.Entry<String, HotelDto> entry : roomsAvailability.entrySet()) {
            node.put(entry.getKey(), buildHotelJson(response, entry.getValue()));
        }
        return node;
    }

    private JsonNode buildPromoCode(SlingHttpServletResponse response, Map<String, HotelDto> roomsAvailability,
            String hotelId) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        for (Map.Entry<String, HotelDto> entry : roomsAvailability.entrySet()) {
            node.put(entry.getKey(), buildHotelJson(response, entry.getValue()));
        }
        return node;
    }

    /**
     * /
     *
     * @param response
     *            **
     *
     * @param hotelId
     * @return JsonNode
     */
    private JsonNode buildRateFilters(SlingHttpServletResponse response,
            Map<RateFilterDto, Map<String, HotelDto>> roomsAvailability, String hotelId) {
        String methodName = "buildRateFilters";
        LOG.trace("Method Entry: {}", methodName);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        for (Map.Entry<RateFilterDto, Map<String, HotelDto>> entry : roomsAvailability.entrySet()) {
            RateFilterDto rateFilter = entry.getKey();
            Collection<Entry<String, HotelDto>> entries = entry.getValue().entrySet();
            for (Entry<String, HotelDto> hotel : entries) {
                if (hotel.getKey() == null) {
                    continue;
                } else {
                    node.put(rateFilter.getCode(), buildRateFilterPlan(response, entry.getValue(), hotelId));
                }
            }

        }
        LOG.debug("Returning rate filters node as: {}", node);
        LOG.trace("Method Exit: {}", methodName);
        return node;
    }

    /**
     * /
     *
     * @param response
     *            **
     *
     * @param hotelId
     * @param roomsAvailability
     * @return JsonNode
     */
    private JsonNode buildRateFilterPlan(SlingHttpServletResponse response, Map<String, HotelDto> roomsAvailability,
            String hotelId) {
        String methodName = "buildRateFilterPlan";
        LOG.debug("\n\n*********************************\n\n");
        LOG.debug("\n\nTHe hotel id in the request: {}", hotelId);
        LOG.debug("\n\n*********************************\n\n");
        LOG.trace("Method Entry: {}", methodName);
        LOG.debug("Received rooms availability to build rate filter plan as: {}", roomsAvailability);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();

        HotelDto hotel = roomsAvailability.get(hotelId);
        if (hotel != null) {
            node.put(hotelId, buildHotelJson(response, hotel));
        }
        LOG.trace("Method Exit: {}", methodName);
        return node;
    }

    /**
     * @param response
     * @param hotel
     * @return
     */
    private JsonNode buildHotelJson(SlingHttpServletResponse response, HotelDto hotel) {
        String methodName = "buildHotelJson";
        LOG.trace("Method Entry: {}", methodName);
        LOG.debug("Received IHotel object to build hotel json as: {}", hotel);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode hotelJsonNode = mapper.createObjectNode();

        List<String> warnings = new ArrayList<String>();
        List<String> errors = new ArrayList<String>();

        String hotelCode = hotel.getHotelCode();
        hotelJsonNode.put(HttpResponseParams.HOTEL_CODE, hotelCode);

        float lowestDiscountedPrice = hotel.getLowestDiscountedPrice();
        hotelJsonNode.put(HttpResponseParams.LOWEST_DISCOUNTED_PRICE, lowestDiscountedPrice);

        float lowestPrice = hotel.getLowestPrice();
        hotelJsonNode.put(HttpResponseParams.LOWEST_PRICE, lowestPrice);

        float lowestPriceTax = hotel.getLowestPriceTax();
        hotelJsonNode.put(HttpResponseParams.LOWEST_PRICE_TAX, lowestPriceTax);

        if (hotel.getCurrencyCode() != null) {
            String currencyString = hotel.getCurrencyCode().getCurrencyString();
            hotelJsonNode.put(HttpResponseParams.CURRENCY_STRING, currencyString);
        } else {
            hotelJsonNode.put(HttpResponseParams.CURRENCY_STRING, "");
        }

        if (null != hotel.getErrors()) {
            errors = hotel.getErrors();
            ArrayNode errorResponse = hotelJsonNode.putArray(HttpResponseParams.HOTEL_ERRORS);
            if (!errors.isEmpty()) {
                invalidateCache(response);
            }
            for (String error : errors) {
                errorResponse.add(error);
            }
        }

        if (null != hotel.getWarnings()) {
            warnings = hotel.getWarnings();
            if (!warnings.isEmpty()) {
                invalidateCache(response);
            }
            ArrayNode warningResponse = hotelJsonNode.putArray(HttpResponseParams.HOTEL_WARNINGS);
            for (String warning : warnings) {
                warningResponse.add(warning);
            }
        }

        Map<String, RoomAvailabilityDto> availableRooms = hotel.getAvailableRooms();
        hotelJsonNode.put(HttpResponseParams.HOTEL_ROOMS_AVAILABILITY, buildRoomsAvailabilityJson(availableRooms));

        LOG.trace("Method Exit: {}", methodName);
        return hotelJsonNode;
    }

    /**
     * @param availableRooms
     * @return
     */
    private JsonNode buildRoomsAvailabilityJson(Map<String, RoomAvailabilityDto> availableRooms) {
        String methodName = "buildRoomsAvailabilityJson";
        LOG.trace("Method Entry: {}", methodName);
        LOG.debug("Received available rooms to build json as: {}", availableRooms);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode roomSAvailabilityJsonNode = mapper.createObjectNode();
        if (null != availableRooms) {
            Set<String> keys = availableRooms.keySet();
            for (String key : keys) {
                roomSAvailabilityJsonNode.put(key, buildRoomAvailabilityJson(availableRooms.get(key)));
            }
        }
        LOG.trace("Method Exit: {}", methodName);
        return roomSAvailabilityJsonNode;
    }

    /**
     * @param roomAvailability
     * @return
     */
    private JsonNode buildRoomAvailabilityJson(RoomAvailabilityDto roomAvailability) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode roomAvailabilityJsonNode = mapper.createObjectNode();

        float lowestDiscountedPrice = roomAvailability.getLowestDiscountedPrice();
        roomAvailabilityJsonNode.put(HttpResponseParams.LOWEST_DISCOUNTED_PRICE, lowestDiscountedPrice);

        float lowestPrice = roomAvailability.getLowestPrice();
        roomAvailabilityJsonNode.put(HttpResponseParams.LOWEST_PRICE, lowestPrice);

        float lowestPriceTax = roomAvailability.getLowestPriceTax();
        roomAvailabilityJsonNode.put(HttpResponseParams.LOWEST_PRICE_TAX, lowestPriceTax);

        if (roomAvailability.getCurrencyCode() != null) {
            String currencyString = roomAvailability.getCurrencyCode().getCurrencyString();
            roomAvailabilityJsonNode.put(HttpResponseParams.CURRENCY_STRING, currencyString);
        } else {
            roomAvailabilityJsonNode.put(HttpResponseParams.CURRENCY_STRING, "");
        }

        int availableUnits = roomAvailability.getAvailableUnits();
        roomAvailabilityJsonNode.put(HttpResponseParams.ROOMS_AVAILABLE_UNITS, availableUnits);

        String availabilityExpression = roomAvailability.getAvailabilityExpression();
        roomAvailabilityJsonNode.put(HttpResponseParams.ROOMS_AVAILABLITY_EXPRESSION, availabilityExpression);

        RoomDto availableRooms = roomAvailability.getRoom();
        roomAvailabilityJsonNode.put(HttpResponseParams.ROOMS_AVAILABLE, buildRoomsJson(availableRooms));

        return roomAvailabilityJsonNode;
    }

    /**
     * @param availableRooms
     * @return ArrayNode
     */

    private ArrayNode buildRoomsJson(RoomDto availableRooms) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode node = mapper.createArrayNode();

        List<RatePlanDto> ratePlanList = availableRooms.getRatePlans();

        for (RatePlanDto ratePlan : ratePlanList) {

            ObjectMapper mappers = new ObjectMapper();
            ObjectNode ratePlanNode = mappers.createObjectNode();

            String ratePlanCode = ratePlan.getCode();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_CODE, ratePlanCode);

            float avgTax = ratePlan.getAvgTax();
            ratePlanNode.put(HttpResponseParams.AVERAGE_TAX, avgTax);

            String ratePlanTitle = ratePlan.getTitle();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_TITLE, ratePlanTitle);

            float lowestPrice = ratePlan.getDiscountedPrice();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_DISCOUNTED_PRICE, lowestPrice);

            float price = ratePlan.getPrice();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_PRICE, price);

            String ratePlanDescription = ratePlan.getDescription();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_DESCRIPTION, ratePlanDescription);

            String cancellationPolicy = ratePlan.getCancellationPolicy();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_CANCELLATION_POLICY, cancellationPolicy);

            float tax = ratePlan.getTax();
            ratePlanNode.put(HttpResponseParams.TAX, tax);

            float totalPrice = ratePlan.getTotalPrice();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_TOTAL_PRICE, totalPrice);

            float totalDiscountedPrice = ratePlan.getTotalDiscountedPrice();
            ratePlanNode.put(HttpResponseParams.RATE_PLAN_TOTAL_DISCOUNTED_PRICE, totalDiscountedPrice);

            if (ratePlan.getCurrencyCode() != null) {
                String currencyString = ratePlan.getCurrencyCode().getCurrencyString();
                ratePlanNode.put(HttpResponseParams.CURRENCY_STRING, currencyString);
            } else {
                ratePlanNode.put(HttpResponseParams.CURRENCY_STRING, "");
            }

            if (ratePlan.getNightlyRates() != null) {
                ratePlanNode.put(HttpResponseParams.NIGHTLY_RATES, fetchNightlyRatesList(ratePlan.getNightlyRates()));
            }

            if (ratePlan.getDiscountedNightlyRates() != null) {
                ratePlanNode.put(HttpResponseParams.DISCOUNTED_NIGHTLY_RATES,
                        fetchDiscountedNightlyRatesList(ratePlan.getDiscountedNightlyRates()));
            }

            if (ratePlan.getTaxes() != null) {
                ratePlanNode.put(HttpResponseParams.TAXES, fetchTaxesList(ratePlan.getTaxes()));
            }

            ratePlanNode.put(HttpResponseParams.GUARANTEE_CODE, ratePlan.getGuaranteeCode());

            ratePlanNode.put(HttpResponseParams.CREDIT_CARD_REQUIRED, isCreditCardRequired(ratePlan));

            float guaranteeAmount = ratePlan.getGuaranteeAmount();
            ratePlanNode.put(HttpResponseParams.GUARANTEE_AMOUNT, guaranteeAmount);

            ratePlanNode.put(HttpResponseParams.GUARANTEE_PERCENTAGE, ratePlan.getGuaranteePercentage());

            ratePlanNode.put(HttpResponseParams.GUARANTEE_DESCRIPTION, ratePlan.getGuaranteeDescription());

            ratePlanNode.put(HttpResponseParams.GUARANTEE_AMOUNT_TAX_INCLUSIVE,
                    ratePlan.isGuaranteeAmountTaxInclusive());


            ratePlanNode.put(HttpResponseParams.RATE_PLAN_AMENITIES, fetchAmenitiesForRatePlan(ratePlanCode));

            node.add(ratePlanNode);

            LOG.trace("Displaying node values: {}", node);
        }
        return node;
    }

    /**
     * @param ratePlan
     * @return
     */
    private boolean isCreditCardRequired(RatePlanDto ratePlan) {
        boolean creditCardCheck = true;
        if (guaranteeCodesConfigurationService.getGuaranteeCodes() != null) {
            for (String guaranteeCode : guaranteeCodesConfigurationService.getGuaranteeCodes()) {
                if (ratePlan.getGuaranteeCode().equalsIgnoreCase(guaranteeCode)) {
                    creditCardCheck = false;
                }
            }
        }
        return creditCardCheck;
    }

    private ArrayNode fetchTaxesList(List<Taxes> taxesList) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode node = mapper.createArrayNode();

        for (Taxes taxes : taxesList) {

            ObjectMapper mappers = new ObjectMapper();
            ObjectNode taxNode = mappers.createObjectNode();

            String taxName = taxes.getTaxName();
            taxNode.put(HttpResponseParams.TAX_NAME, taxName);

            String taxDescription = taxes.getTaxDescription();
            taxNode.put(HttpResponseParams.TAX_DESCRIPTION, taxDescription);

            String taxAmount = taxes.getTaxAmount();
            taxNode.put(HttpResponseParams.TAX_AMOUNT, taxAmount);

            node.add(taxNode);
            LOG.trace("Displaying node values: {}", node);
        }
        return node;

    }

    private ArrayNode fetchDiscountedNightlyRatesList(List<DiscountedNightlyRates> iDiscountedNightlyRatesList) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode node = mapper.createArrayNode();

        for (DiscountedNightlyRates iDiscountedNightlyRates : iDiscountedNightlyRatesList) {

            ObjectMapper mappers = new ObjectMapper();
            ObjectNode nightlyRateNode = mappers.createObjectNode();

            String date = iDiscountedNightlyRates.getDate();
            nightlyRateNode.put(HttpResponseParams.DATE, date);

            String price = iDiscountedNightlyRates.getPrice();
            nightlyRateNode.put(HttpResponseParams.RATE_PLAN_PRICE, price);

            String priceWithFeeAndTax = iDiscountedNightlyRates.getPriceWithFeeAndTax();
            nightlyRateNode.put(HttpResponseParams.PRICE_WITH_FEE_AND_TAX, priceWithFeeAndTax);

            String tax = iDiscountedNightlyRates.getTax();
            nightlyRateNode.put(HttpResponseParams.TAX, tax);

            node.add(nightlyRateNode);
            LOG.trace("Displaying node values: {}", node);
        }
        return node;

    }

    private ArrayNode fetchNightlyRatesList(List<NightlyRates> nightlyRateList) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode node = mapper.createArrayNode();

        for (NightlyRates nightlyRate : nightlyRateList) {

            ObjectMapper mappers = new ObjectMapper();
            ObjectNode nightlyRateNode = mappers.createObjectNode();

            String date = nightlyRate.getDate();
            nightlyRateNode.put(HttpResponseParams.DATE, date);

            String price = nightlyRate.getPrice();
            nightlyRateNode.put(HttpResponseParams.RATE_PLAN_PRICE, price);

            String priceWithFeeAndTax = nightlyRate.getPriceWithFeeAndTax();
            nightlyRateNode.put(HttpResponseParams.PRICE_WITH_FEE_AND_TAX, priceWithFeeAndTax);

            String tax = nightlyRate.getTax();
            nightlyRateNode.put(HttpResponseParams.TAX, tax);

            node.add(nightlyRateNode);
            LOG.trace("Displaying node values: {}", node);
        }
        return node;
    }

    private JsonNode fetchAmenitiesForRatePlan(String ratePlanCode) {
        String methodName = "buildRateFilterPlan";
        LOG.trace("Method Entry: {}", methodName);
        ObjectMapper mappers = new ObjectMapper();
        ArrayNode ratePlanNode = mappers.createArrayNode();

        List<ServiceAmenity> amenities = ratePlanCodeAmenitiesFetcherService.getMappedAmenitiesFor(ratePlanCode);

        for (ServiceAmenity amenity : amenities) {
            ObjectNode amenityNode = mappers.createObjectNode();
            amenityNode.put(HttpResponseParams.RATE_PLAN_AMENITIY_NAME, amenity.getName());
            amenityNode.put(HttpResponseParams.RATE_PLAN_AMENITIY_ICON_PATH, amenity.getImage());
            ratePlanNode.add(amenityNode);
        }

        LOG.debug("Returning amenities for rate plan with code: " + ratePlanCode + " as: " + ratePlanNode);
        LOG.trace("Method Exit: {}", methodName);
        return ratePlanNode;
    }

}
