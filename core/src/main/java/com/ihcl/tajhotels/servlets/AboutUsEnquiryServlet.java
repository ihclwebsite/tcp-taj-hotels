/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.AboutUsEnquiry;

/**
 * @author Gayathri
 *
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/aboutusenquiry" })
public class AboutUsEnquiryServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AboutUsEnquiryServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    TajApiKeyConfigurationService tajApiKeyConfigurationService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("\n\n In AboutUsEnquirServlet Class-> Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for about us enquiry is : " + requestUrl);

        try {
            if (tajApiKeyConfigurationService != null) {
                LOG.trace("tajApiKeyConfigurationService is not null : "
                        + tajApiKeyConfigurationService.getTajAdminEmailId());
            } else if (tajApiKeyConfigurationService == null) {
                LOG.trace("tajApiKeyConfigurationService is null : ");
            }
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                AboutUsEnquiry requestobj = setEnquiryDetails(httpRequest);

                boolean isSendEmailSuccess = emailService.sendAboutUsEnquiryEmail(requestobj);
                if (isSendEmailSuccess) {

                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private AboutUsEnquiry setEnquiryDetails(SlingHttpServletRequest httpRequest) throws Exception {
        String methodName = "setEmailRequest";
        LOG.trace("Method Entry: " + methodName);
        AboutUsEnquiry requestobj = new AboutUsEnquiry();
        LOG.trace("emailID value -" + httpRequest.getParameter("emailId"));
        if (tajApiKeyConfigurationService.getTajAdminEmailId() != null) {
            requestobj.setAdminEmailId(tajApiKeyConfigurationService.getDevelopmentAdminEmailId());
            LOG.trace("\n\nadmin mail id from osgi is " + tajApiKeyConfigurationService.getDevelopmentAdminEmailId());
        } else
            LOG.trace("\n\nadmin mail id from osgi config is null ");
        requestobj.setFirstName(httpRequest.getParameter("firstName"));
        requestobj.setLastName(httpRequest.getParameter("lastName"));
        requestobj.setEmail(httpRequest.getParameter("emailId"));
        requestobj.setPhoneNumber(httpRequest.getParameter("phoneNumber"));
        requestobj.setCompanyName(httpRequest.getParameter("companyName"));
        requestobj.setCompanyType(httpRequest.getParameter("companyType"));
        requestobj.setRegion(httpRequest.getParameter("region"));
        requestobj.setProjectType(httpRequest.getParameter("projectType"));
        requestobj.setOwnershipType(httpRequest.getParameter("ownerShipType"));
        requestobj.setDescription(httpRequest.getParameter("description"));
        LOG.trace("Method Exit: " + methodName);
        return requestobj;
    }

    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }
}
