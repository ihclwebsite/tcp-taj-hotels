/**
 * <pre>
 * CheckHotelOfferAvailability Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :12-Sep-2019
 * @ClassName : CheckHotelOfferAvailability.java
 * @Description : tajhotels-core -CheckHotelOfferAvailability.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  12-Sep-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
package com.ihcl.tajhotels.servlets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.hotels.SearchBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.core.util.URLMapUtil;

/**
 * <pre>
 * CheckOfferHotelAvailabilityServlet Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :12-Sep-2019
 * @ClassName : CheckOfferHotelAvailabilityServlet.java
 * @Description : com.ihcl.tajhotels.servlets -CheckOfferHotelAvailabilityServlet.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  12-Sep-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/checkHotelsForOffers", "sling.servlet.methods=GET" })
public class CheckOfferHotelAvailabilityServlet extends SlingSafeMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = -8568698008344511392L;

    private static final Logger LOG = LoggerFactory.getLogger(CheckOfferHotelAvailabilityServlet.class);

    @Reference
    TajhotelsHotelsBeanService hotelBeanService;

    @Reference
    SearchProvider searchProvider;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        ResourceResolver resolver = null;
        try {
            resolver = request.getResourceResolver();
            Map<String, Object> categorizedResponse = createSearchResultForOffers(resolver, request);
            ObjectMapper objectMapper = new ObjectMapper();
            response.setContentType("application/json");
            response.getWriter().println(objectMapper.writeValueAsString(categorizedResponse));
        } catch (Exception e) {
            LOG.error("Exception occured while getting the resourceResolver :: {}", e.getMessage());
            LOG.debug("Exception occured while getting the resourceResolver :: {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
    }

    /**
     * @param resolver
     * @param request
     * @return
     */
    private Map<String, Object> createSearchResultForOffers(ResourceResolver resolver,
            SlingHttpServletRequest request) {
        Map<String, Object> hotels = new HashMap<>();
        String suffixPath = request.getRequestPathInfo().getSuffix();
        LOG.debug("Received suffix path info: {}", suffixPath);
        List<String> suffixes = Arrays.asList(suffixPath.split("/"));
        String searchKeyInReq = suffixes.get(3);
        String websitePath = suffixes.get(1).replaceAll(":", "/content/");
        if (StringUtils.isBlank(websitePath)) {
            websitePath = SearchServiceConstants.PATH.TAJ_ROOT;
        }
        String otherWebsitePaths = suffixes.get(2).replaceAll(":", "/content/").replaceAll("_", ",");
        LOG.debug(searchKeyInReq + "|" + websitePath + "|" + otherWebsitePaths);
        String searchKey = searchKeyInReq + "*";
        SearchResult otherWebsiteSearchResult = null;
        if (StringUtils.isNotBlank(otherWebsitePaths) && otherWebsitePaths.contains(",")) {
            String[] paths = otherWebsitePaths.split(",");
            otherWebsiteSearchResult = getSearchResult(searchKey, null, paths);
            ArrayList<Object> otherHotels = buildHotelResult(otherWebsiteSearchResult, resolver, request);
            hotels.put("others", otherHotels);
        }
        SearchResult websiteSearchResults = getSearchResult(searchKey, websitePath, null);
        ArrayList<Object> websiteHotels = buildHotelResult(websiteSearchResults, resolver, request);
        hotels.put("websites", websiteHotels);

        return hotels;
    }

    /**
     * @param otherWebsiteSearchResult
     * @param resolver
     * @param request
     */
    private ArrayList<Object> buildHotelResult(SearchResult searchResult, ResourceResolver resolver,
            SlingHttpServletRequest request) {
        ArrayList<Object> hotels = new ArrayList<>();
        for (Hit hit : searchResult.getHits()) {
            try {
                Resource resultResource = resolver.getResource(hit.getPath());
                String[] arr = resultResource.getPath().split("/jcr:content");
                ValueMap vm = resultResource.adaptTo(ValueMap.class);
                if (null != vm) {
                    String hotelTitle = vm.get("hotelName", String.class);
                    String hotelId = vm.get("hotelId", String.class);
                    String path = getMappedPath(arr[0], resolver, request);
                    SearchBean searchBean = new SearchBean(hotelTitle, path, hotelId, null, null);
                    hotels.add(searchBean);
                }
            } catch (Exception e) {
                LOG.error("Exception found while getting the hotel details :: {}", e.getMessage());
                LOG.debug("Exception found while getting the hotel details :: {}", e);
            }

        }
        return hotels;
    }

    /**
     * @param searchKey
     * @param object
     * @param paths
     * @return
     */
    private SearchResult getSearchResult(String searchKey, String contentRootPath, String[] paths) {
        HashMap<String, String> predicateMap = new HashMap<>();

        try {
            predicateMap.put("fulltext", searchKey);
            if (null != paths && null == contentRootPath) {
                predicateMap.put("1_group.p.or", "true");
                for (int i = 0; i < paths.length; i++) {
                    predicateMap.put("1_group." + i + "_path", paths[i]);
                }
            } else {
                predicateMap.put("path", contentRootPath);
            }
            predicateMap.put("2_property", "sling:resourceType");
            predicateMap.put("2_property.value", SearchServiceConstants.RESOURCETYPE.HOTELS);
            predicateMap.put("3_property", "noReservationAvailable");
            predicateMap.put("3_property.operation", "exists");
            predicateMap.put("3_property.value", "false");
            predicateMap.put("orderby", "@jcr:score");
            predicateMap.put("orderby.sort", "desc");
            predicateMap.put("p.limit", "-1");

        } catch (Exception e) {
            LOG.error("Error while searching for text= {} --> {}", searchKey, e.getMessage());
            LOG.debug("Error while searching for text= {} --> {}", searchKey, e);
        }
        return searchProvider.getQueryResult(predicateMap);

    }


    /**
     * Gets the mapped path.
     *
     * @param url
     *            the url
     * @param resourceResolver
     *            the resource resolver
     * @return the mapped path
     */
    private String getMappedPath(String url, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(request, URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }

}
