package com.ihcl.tajhotels.servlets;


import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.TABLE_CANCELLATION_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.TABLE_CANCELLATION_SUCCESS;


import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.requestdto.UpdateReservationRequest;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.responsedto.UpdateReservation;
import com.ihcl.tajhotels.integration.tableReservations.updateReservation.api.IUpdateReservationService;

/**
 * Servlet that writes some sample content into the response. It is mounted for all resources of a specific Sling
 * resource type. The {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are idempotent. For write
 * operations use the {@link SlingAllMethodsServlet}.
 */

@Component(
        service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/cancelTableReservation", "sling.servlet.methods=GET",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class CancelTableReseervation extends SlingAllMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(TableBookReservation.class);

    @Reference
    private IUpdateReservationService updateReservationService;
    int status;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("Method Entry: {}" , methodName);   
       
       
        try {
            LOG.debug("Cancellation of Table reservation service : {}" , updateReservationService);
            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
            setCancellationDetails(request);
            
            writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, TABLE_CANCELLATION_SUCCESS);
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability.", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, TABLE_CANCELLATION_FAILURE);
        }
        LOG.trace("Method Exit: {}" , methodName);
    }

    private void setCancellationDetails(SlingHttpServletRequest request) throws Exception {
        String methodName = "setCancellationDetails";
        
        int restaurantID=Integer.parseInt(request.getParameter("restaurantID"));
        String reservationID=request.getParameter("reservationID");
        int customerID=Integer.parseInt(request.getParameter("customerID"));
        UpdateReservationRequest requestobj = new UpdateReservationRequest();
        requestobj.setRestaurantId(restaurantID);
        requestobj.setReservationId(reservationID);
        requestobj.setCustomerId(customerID);
        LOG.trace("Customer ID :: {}", customerID);
        LOG.trace("RestaurantId :: {} ReservationId :: {}", restaurantID, reservationID);
        /*
        The Status Code for following action
        Confirmed : 1
         Seated : 5
        Cancelled : 8
        Waitlisted : 7
        Vacant : 9 */
       requestobj.setStatus(8);
       
       UpdateReservation cancelReservation = (UpdateReservation) updateReservationService.updateReservation(requestobj);
       String cancellationStatus = cancelReservation.getMessage();
       LOG.debug("Table Reservation Status response from service as: {}" , cancellationStatus);
       LOG.trace("Method Exit: {}" , methodName);
    }

    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}" , methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        
        response.getWriter().write(jsonNode.toString());
        LOG.trace("The Value of JSON: {}" , jsonNode);
        LOG.trace("Method Exit: {}" , methodName);
    }
}

