package com.ihcl.tajhotels.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.hotels.SearchBean;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.core.util.URLMapUtil;

/**
 * @author Titus
 *
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/venueSearch", "sling.servlet.methods=GET" })
public class SearchAllVenueServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 9022344985091162836L;

    private static final Logger LOG = LoggerFactory.getLogger(SearchAllVenueServlet.class);

    @Reference
    SearchProvider searchProvider;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        ResourceResolver resolver = null;
        try {
            resolver = request.getResourceResolver();
            Map<String, Object> categorizedResponse = createSearchResultForVenues(resolver, request);
            ObjectMapper objectMapper = new ObjectMapper();
            response.setContentType("application/json");
            response.getWriter().println(objectMapper.writeValueAsString(categorizedResponse));
        } catch (Exception e) {
            LOG.error("Exception occured while getting the resourceResolver :: {}", e.getMessage());
            LOG.debug("Exception occured while getting the resourceResolver :: {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
    }


    private Map<String, Object> createSearchResultForVenues(ResourceResolver resolver,
            SlingHttpServletRequest request) {
        Map<String, Object> hotels = new HashMap<>();
        String searchKeyInReq = request.getParameter("searchText");
        String searchKey = searchKeyInReq + "*";
        SearchResult websiteSearchResults = getSearchResult(searchKey, "/content", null);
        ArrayList<Object> websiteHotels = buildHotelResult(websiteSearchResults, resolver, request);
        TreeSet<String> destinationList = buildDestinationResult(websiteSearchResults, resolver, request);
        hotels.put("websites", websiteHotels);
        hotels.put("destination", destinationList);
        return hotels;
    }

    private ArrayList<Object> buildHotelResult(SearchResult searchResult, ResourceResolver resolver,
            SlingHttpServletRequest request) {
        ArrayList<Object> hotels = new ArrayList<>();
        for (Hit hit : searchResult.getHits()) {
            try {
                String hotelPath = hit.getPath().split("/meetings-and-events/jcr:content")[0];
                Resource resultResource = resolver.getResource(hotelPath + "/jcr:content");
                ValueMap vm = resultResource.adaptTo(ValueMap.class);
                if (null != vm) {
                    String hotelTitle = vm.get("hotelName", String.class);
                    String hotelId = vm.get("hotelId", String.class);
                    String path = getMappedPath(hotelPath, resolver, request);
                    SearchBean searchBean = new SearchBean(hotelTitle, path, hotelId, null, null);
                    hotels.add(searchBean);
                }

            } catch (Exception e) {
                LOG.error("Exception found while getting the hotel details :: {}", e.getMessage());
                LOG.debug("Exception found while getting the hotel details :: {}", e);
            }

        }
        return hotels;
    }

    private TreeSet<String> buildDestinationResult(SearchResult searchResult, ResourceResolver resolver,
            SlingHttpServletRequest request) {
        TreeSet<String> destinations = new TreeSet<String>();
        for (Hit hit : searchResult.getHits()) {
            try {
                String hotelPath = hit.getPath().split("/meetings-and-events/jcr:content")[0];
                Resource hotelResource = resolver.getResource(hotelPath);
                Resource destinationResource = hotelResource.getParent().getParent().getChild("jcr:content");
                ValueMap vm = destinationResource.adaptTo(ValueMap.class);
                if (null != vm) {
                    String destinationName = vm.get("jcr:title", String.class);
                    destinations.add(destinationName);
                }

            } catch (Exception e) {
                LOG.error("Exception found while getting the destination details :: {}", e.getMessage());
                LOG.debug("Exception found while getting the destination details :: {}", e);
            }

        }
        return destinations;
    }


    private SearchResult getSearchResult(String searchKey, String contentRootPath, String[] paths) {
        HashMap<String, String> predicateMap = new HashMap<>();

        try {
            predicateMap.put("fulltext", searchKey);
            if (null != paths && null == contentRootPath) {
                predicateMap.put("1_group.p.or", "true");
                for (int i = 0; i < paths.length; i++) {
                    predicateMap.put("1_group." + i + "_path", paths[i]);
                }
            } else {
                predicateMap.put("path", contentRootPath);
            }
            predicateMap.put("2_property", "sling:resourceType");
            predicateMap.put("2_property.value", SearchServiceConstants.RESOURCETYPE.MEETING_VENUES);
            predicateMap.put("orderby", "@jcr:score");
            predicateMap.put("orderby.sort", "desc");
            predicateMap.put("p.limit", "-1");

        } catch (Exception e) {
            LOG.error("Error while searching for text= {} --> {}", searchKey, e.getMessage());
            LOG.debug("Error while searching for text= {} --> {}", searchKey, e);
        }
        return searchProvider.getQueryResult(predicateMap);

    }


    private String getMappedPath(String url, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(request, URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }
}
