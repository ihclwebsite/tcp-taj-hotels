package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.models.HotelDetailsFetcher;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.core.util.ObjectConverter;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.HotelAvailabilityResponse;
import com.ihcl.integration.dto.availability.HotelDto;
import com.ihcl.integration.dto.availability.RoomAvailabilityDto;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Flight;
import com.ihcl.integration.dto.details.Guest;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.integration.dto.details.Payments;
import com.ihcl.integration.dto.details.ResStatus;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.integration.dto.details.TicPoints;
import com.ihcl.integration.exception.ServiceException;
import com.ihcl.loyalty.config.ISibelConfiguration;
import com.ihcl.loyalty.dto.IRedemptionTransactionsRequest;
import com.ihcl.loyalty.dto.IRedemptionTransactionsResponse;
import com.ihcl.loyalty.dto.impl.RedemptionTransactionsRequestImpl;
import com.ihcl.loyalty.services.IRedemptionTransactionsService;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.loyalty.dto.IRedemptionTransactionsResponse;
import com.ihcl.loyalty.dto.ITdlRedemTransactionRequestDto;
import com.ihcl.loyalty.services.IRedemptionTransactionsService;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/postPaymentServlet", "sling.servlet.methods={GET,POST}",
                "sling.servlet.paths=" + BookingConstants.TIC_POST_PAYMENT_SERVLET_URL,
                "sling.servlet.paths=" + BookingConstants.CORPORATE_POST_PAYMENT_SERVLET_URL,
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class BookHotelConfirmServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(BookHotelConfirmServlet.class);

    private static final String TAPP_ME = "TAPPMe";

    private static final String TAP = "TAP";

    private static final String TIC = "TIC";

    @Reference
    private IRoomAvailabilityService roomAvailabilityService;

    @Reference
    private IRedemptionTransactionsService redemptionTransactionsService;

    @Reference
    private IBookingRequestHandler bookingHandler;

    @Reference
    private TajHotelsBrandsETCService tajHotelsBrandsETCService;

    @Reference
    private ISibelConfiguration iSibelConfiguration;
    
    @Reference
    private IRedemptionTransactionsService iRedeem;

    private final String EPICURE = "EPICURE";

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String methodName = "doPost";
        LOG.trace("Method Entry: {}", methodName);
        try {

            LOG.info("BookHotelConfirmServlet Started in doPost");
            bookHotelConfirmationMethod(request, response);
        } catch (Exception e) {
            LOG.debug("Exception at doPost of BookHotelConfirmServlet :: {} ", e.getMessage());
            LOG.error("Exception at doPost of BookHotelConfirmServlet", e);
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    private void bookHotelConfirmationMethod(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String methodName = "bookHotelConfirmationMethod";
        LOG.trace("Method Entry: {}", methodName);
        try {

            StringBuffer requestURL = request.getRequestURL();
            LOG.info("request.getRequestURL(): {}", requestURL);

            String websiteId = request.getServerName();
            LOG.info("request.getServerName(): {}", websiteId);

            RequestPathInfo requestPathInfo = request.getRequestPathInfo();
            String resourcePath = requestPathInfo.getResourcePath();
            LOG.info("request.getRequestPathInfo().getResourcePath(): {}", resourcePath);

            websiteId = setDefaultWebsiteId(websiteId);

            TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = tajHotelsBrandsETCService.getEtcConfigBean();
            LOG.debug("Received etcAllBrandsConfig as: {}", etcAllBrandsConfig);

            Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean = etcAllBrandsConfig.getEtcConfigBean();
            LOG.debug("Received etcConfigBean as: {}", etcConfigBean);

            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcConfigBean.get(websiteId);
            LOG.info("Received individualWebsiteAllConfig for website id: {} as: {}", websiteId,
                    individualWebsiteAllConfig);

            Map<String, TajHotelBrandsETCConfigurations> etcWebsiteConfigBean = individualWebsiteAllConfig
                    .getEtcConfigBean();
            LOG.debug("Received individualWebsiteAllConfigBean as: {}", etcWebsiteConfigBean);

            String ccAvenueType = identifyCcAvenueType(resourcePath);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = etcWebsiteConfigBean.get(ccAvenueType);
            String configLoggerString = "websiteId: " + websiteId + ", ccAvenueType: " + ccAvenueType
                    + ", etcWebsiteConfig: " + etcWebsiteConfig;
            LOG.info("Final CCAvenue ETC Configuration: {}", configLoggerString);


            BookingObject bookingObjectRequest = getBookingObjectFromRequest(request, etcWebsiteConfig);

            String redirectUrl = etcWebsiteConfig.getPaymentConfirmUrl();
            if (null != bookingObjectRequest) {
                checkCorporateBooking(bookingObjectRequest, ccAvenueType);
                PrintWriter responseWriter = response.getWriter();
                handlePaymentResponse(request, response, individualWebsiteAllConfig, etcWebsiteConfig,
                        bookingObjectRequest, redirectUrl, responseWriter);
            }

        } catch (Exception e) {
            LOG.debug("IOException found in bookHotelConfirmationMethod :: {}", e.getMessage());
            LOG.error("IOException found in bookHotelConfirmationMethod", e);
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    private void checkCorporateBooking(BookingObject bookingObjectRequest, String ccAvenueType) {
        if (ccAvenueType.equals(ReservationConstants.CORPORATE_BOOKING_CCAVENUE)) {
            bookingObjectRequest.setCorporateBooking(true);
        }
    }

    private String identifyCcAvenueType(String resourcePath) {
        String ccAvenueType = "";
        if (BookingConstants.TIC_POST_PAYMENT_SERVLET_URL.equalsIgnoreCase(resourcePath)) {
            ccAvenueType = ReservationConstants.TIC_CCAVENUE;
        } else if (BookingConstants.CORPORATE_POST_PAYMENT_SERVLET_URL.equalsIgnoreCase(resourcePath)) {
            ccAvenueType = ReservationConstants.CORPORATE_BOOKING_CCAVENUE;
        } else {
            ccAvenueType = ReservationConstants.BOOKING_CCAVENUE;
        }
        LOG.info("Identified the ccAvenue type as: {}", ccAvenueType);
        return ccAvenueType;
    }

    private void handlePaymentResponse(SlingHttpServletRequest request, SlingHttpServletResponse response,
            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig, TajHotelBrandsETCConfigurations etcWebsiteConfig,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter) throws IOException {
        boolean paymentStatus = bookingObjectRequest.isPaymentStatus();
        LOG.info("PaymentStatus(): {}", paymentStatus);
        if (paymentStatus) {
            handlePaymentSuccess(request, response, individualWebsiteAllConfig, etcWebsiteConfig, bookingObjectRequest,
                    redirectUrl, responseWriter);
        } else {
            handleFailure(response, etcWebsiteConfig, bookingObjectRequest, redirectUrl, responseWriter);

        }
    }

    private void handlePaymentSuccess(SlingHttpServletRequest request, SlingHttpServletResponse response,
            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig, TajHotelBrandsETCConfigurations etcWebsiteConfig,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectConverter converter = new ObjectConverter();


        BookingObject bookingObjectResponse = buildResponseForHotelConfirmation(bookingObjectRequest, objectMapper,
                request);
        String responseString = objectMapper.writeValueAsString(bookingObjectResponse);
        LOG.info("Response received : {}", responseString);

        if (null != bookingObjectResponse) {
            bookingObjectResponse.setCorporateBooking(bookingObjectRequest.isCorporateBooking());
            if (bookingObjectResponse.isSuccess()) {
                String returnMessage = null;
                bookingObjectResponse = setAdditionalDetailsToResponse(request, bookingObjectResponse,
                        bookingObjectRequest, converter, etcWebsiteConfig.getContentRootPath(),
                        individualWebsiteAllConfig.isAmaBooking());
                returnMessage = objectMapper.writeValueAsString(bookingObjectResponse);
                handleReservationSuccess(response, returnMessage, redirectUrl, responseWriter, bookingObjectResponse);

            } else {
                handleBookingFailed(response, objectMapper, bookingObjectRequest, redirectUrl, responseWriter,
                        bookingObjectResponse);

            }
        } else {
            handleFailure(response, etcWebsiteConfig, bookingObjectRequest, redirectUrl, responseWriter);
        }
    }

    private void handleBookingFailed(SlingHttpServletResponse response, ObjectMapper objectMapper,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter,
            BookingObject bookingObjectResponse) throws IOException {
        String returnMessage;
        String confirmationStatus;
        LOG.info("BookingObject.Success:false or All rooms are unavailable");
        bookingObjectRequest.setSuccess(false);
        // [TIC-FLOW]
        if (null != bookingObjectResponse.getPayments() && null != bookingObjectResponse.getPayments().getTicpoints()
                && bookingObjectResponse.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()) {
            bookingObjectRequest.setErrorMessage(bookingObjectResponse.getErrorMessage());
        }
        returnMessage = objectMapper.writeValueAsString(bookingObjectRequest);
        ignoreBooking(bookingObjectRequest);
        confirmationStatus = BookingConstants.CONFIRM_BOOKING_FAILED;
        bookingObjectRequest.setResStatus(ResStatus.Ignore);
        if (bookingObjectRequest.getPayments().isPayOnlineNow()) {
            writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
        } else if (bookingObjectRequest.getPayments().isPayAtHotel()) {
            response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            responseWriter.write(returnMessage);
            responseWriter.close();
        }
    }

    private void handleReservationSuccess(SlingHttpServletResponse response, String returnMessage, String redirectUrl,
            PrintWriter responseWriter, BookingObject bookingObjectResponse) {
        String confirmationStatus;
        if (bookingObjectResponse.getPayments().isPayOnlineNow()) {
            LOG.info("Processing redirect response for Online payment");
            LOG.info("All Rooms are available - Pay online selected");
            confirmationStatus = BookingConstants.CONFIRM_BOOKING_ALL_BOOKED;
            writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
        } else if (bookingObjectResponse.getPayments().isPayAtHotel()) {
            LOG.debug("Processing response for Pay at Hotel");
            LOG.info("All Rooms are available - Pay at Hotel selected");
            LOG.debug("-----BookHotelConfirmServlet : Leaving doGet() method");
            LOG.debug("Final Return message: {}", returnMessage);
            response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            responseWriter.write(returnMessage);
            responseWriter.close();
        }
    }

    private String setDefaultWebsiteId(String websiteId) {
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "www.tajhotels.com";
        }
        return websiteId;
    }

    private void handleFailure(SlingHttpServletResponse response, TajHotelBrandsETCConfigurations etcWebsiteConfig,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter) {
        String confirmationStatus = null;
        String returnMessage = null;
        if (bookingObjectRequest.getPayments() != null && bookingObjectRequest.getPayments().isPayOnlineNow()) {
            if (!bookingObjectRequest.isPaymentStatus()) {
                LOG.info("Payment is canceled, Redirecting to Cancel Url : {}",
                        etcWebsiteConfig.getCcAvenueCancelUrl());
                confirmationStatus = BookingConstants.TRANSACTION_CANCELED;
                redirectUrl = etcWebsiteConfig.getCcAvenueCancelUrl();
            }
            writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
        } else if (bookingObjectRequest.getPayments() != null && bookingObjectRequest.getPayments().isPayAtHotel()) {

            LOG.debug("-----BookHotelConfirmServlet - Exception : Leaving doGet() method");
            response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            responseWriter.write("ExceptionFound");
            responseWriter.close();
        }
    }

    /**
     * @param bookingObjectResponse
     * @param bookingObjectRequest
     * @return
     */
    private BookingObject setFailureStatusToResponse(BookingObject bookingObjectRequest,
            BookingObject bookingObjectResponse) {
        if (bookingObjectRequest.getPayments().isPayOnlineNow()) {
            LOG.info("All rooms are unavailable - Pay online selected");
            LOG.info("Sending Ignore Request : {}", bookingObjectResponse);
            initIgnoreBooking(bookingObjectRequest, bookingObjectResponse);

        } else if (bookingObjectRequest.getPayments().isPayAtHotel()) {
            LOG.info("All rooms are unavailable - Pay at Hotel selected");
            initIgnoreBooking(bookingObjectRequest, bookingObjectRequest);
        }
        return bookingObjectRequest;

    }

    private void initIgnoreBooking(BookingObject bookingObjectRequest, BookingObject bookingObjectResponse) {
        ignoreBooking(bookingObjectResponse);
        bookingObjectRequest.setResStatus(ResStatus.Ignore);
    }

    private void ignoreBooking(BookingObject bookingObjectResponse) {
        boolean ignoreStatus = sendIgnoreRequest(bookingObjectResponse);
        if (ignoreStatus) {
            LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_SUCCESSFUL);
        } else {
            LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_FAILED);
        }
    }


    /**
     * @param bookingObjectResponseStringa
     * @param confirmationStatus
     */
    private void writeResponseOnServer(String bookingObjectResponseStringa, SlingHttpServletResponse response,
            String confirmationStatus, String redirectUrl) {
        try {
            PrintWriter out = response.getWriter();
            LOG.info("redirecting using hidden form");
            LOG.info("Final BookingObject String : {}", bookingObjectResponseStringa);
            LOG.info("RedirectURL : {}", redirectUrl);
            response.setContentType(BookingConstants.CONTENT_TYPE_HTML);
            bookingObjectResponseStringa = StringEscapeUtils.escapeEcmaScript(bookingObjectResponseStringa);
            LOG.debug("Final response which is setting as SessionData : {}", bookingObjectResponseStringa);

            out.print("<form id='confirmbooking' action='" + redirectUrl + BookingConstants.PAGE_EXTENSION_HTML
                    + "' method='get'>");
            out.print("<input type='hidden' name='" + BookingConstants.CONFIRMATION_STATUS_NAME + "' value='"
                    + confirmationStatus + "'>");

            out.print("</form>");
            out.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                    + "sessionStorage.setItem('bookingDetailsRequest', '" + bookingObjectResponseStringa + "');"
                    + "sessionStorage.setItem('status', '" + confirmationStatus + "');"
                    + "sessionStorage.setItem('paymentType', 'payOnline');"
                    + "document.getElementById(\"confirmbooking\").submit()" + "}" + "</script>");
            LOG.info("end of output stream");
            out.close();

        } catch (Exception e) {
            LOG.error("Error while submitting form : {}", e.getMessage());
            LOG.debug("error while submitting form : {}", e);
        }

    }

    /**
     * @param bookingObjectResponse
     * @param request
     * @param bookingObjectRequest
     * @param converter
     * @param rootPath
     */
    private BookingObject setAdditionalDetailsToResponse(SlingHttpServletRequest request,
            BookingObject bookingObjectResponse, BookingObject bookingObjectRequest, ObjectConverter converter,
            String rootPath, boolean amaBooking) {


        try {
            HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();
            if (null == bookingObjectResponse.getPayments()) {
                LOG.info("Resetting BookingObjectResponse.Payments with BookingObjectRequest.Payments.");
                bookingObjectResponse.setPayments(bookingObjectRequest.getPayments());

                if (bookingObjectRequest.getPayments().getTicpoints() != null
                        && bookingObjectRequest.getPayments().getTicpoints().isPointsPlusCash()) {
                    LOG.info("Setting Payments.isPayGuaranteeAmount: {}", false);
                    bookingObjectResponse.getPayments().setPayGuaranteeAmount(false);
                } else if (bookingObjectResponse.getPayments().isPayOnlineNow() && Float.compare(
                        (Math.round(Float.parseFloat(bookingObjectResponse.getTotalPaidAmount()) * 100) / 100),
                        (Math.round(Float.parseFloat(bookingObjectResponse.getTotalAmountAfterTax()) * 100)
                                / 100)) != 0) {
                    LOG.info("Setting Payments.isPayGuaranteeAmount: {}", true);
                    bookingObjectResponse.getPayments().setPayGuaranteeAmount(true);
                }
            }

            LOG.debug("Status of Commited Status {} ", bookingObjectResponse.isSuccess());
            LOG.debug("Itinerary Number for commit : {}", bookingObjectResponse.getItineraryNumber());

            if (StringUtils.isNotBlank(bookingObjectResponse.getCurrencyCode())) {
                LOG.info("Setting Currency code from bookingObject's Request -> Response ::");
                bookingObjectResponse.setCurrencyCode(bookingObjectRequest.getCurrencyCode());
            }
            bookingObjectResponse = setFlightDetailsToSpecialRequests(bookingObjectResponse, converter);
            String roomTypeCode = "";
            if (amaBooking) {
                roomTypeCode = bookingObjectResponse.getRoomList().get(0).getRoomTypeCode();
                LOG.info("roomTypeCode: {}", roomTypeCode);
            }
            fetchHotelDetailsResponse(request, bookingObjectResponse, rootPath, fetchDetails, roomTypeCode);

            ResourceResolver resolver = request.getResourceResolver();
            setRoomDetailsToResponse(bookingObjectResponse, fetchDetails, resolver);


        } catch (Exception e) {
            LOG.error("Exception found while trying to get the bedType and roomName:: {}", e.getMessage());
            LOG.debug("Exception found while trying to get the bedType and roomName:: {}", e);
        }

        return bookingObjectResponse;
    }

    private void fetchHotelDetailsResponse(SlingHttpServletRequest request, BookingObject bookingObjectResponse,
            String rootPath, HotelDetailsFetcher fetchDetails, String roomTypeCode) {
        try {
            LOG.info("getting response for hotel details. hotel id : {}", bookingObjectResponse.getHotelId());
            Hotel hoteldetails = fetchDetails.getHotelDetails(request, bookingObjectResponse.getHotelId(), rootPath,
                    roomTypeCode);
            bookingObjectResponse.setHotel(hoteldetails);
        } catch (RepositoryException e1) {
            LOG.error("error while getting hotel details : {}", e1.getMessage());
            LOG.debug("error while getting hotel details : {}", e1);
        }
    }

    private void setRoomDetailsToResponse(BookingObject bookingObjectResponse, HotelDetailsFetcher fetchDetails,
            ResourceResolver resolver) throws RepositoryException {
        Session session = resolver.adaptTo(Session.class);
        String hotelPath = bookingObjectResponse.getHotel().getHotelResourcePath();
        List<Room> rooms = bookingObjectResponse.getRoomList();
        if (rooms != null && !rooms.isEmpty() && StringUtils.isNotBlank(hotelPath)) {
            for (Room room : rooms) {
                Iterator<Node> iteratorNode = queryBedType(fetchDetails, resolver, session, hotelPath, room);
                if (iteratorNode != null) {
                    setBedType(room, iteratorNode);
                }
            }
        }
    }

    private void setBedType(Room room, Iterator<Node> iteratorNode) throws RepositoryException {
        while (iteratorNode.hasNext()) {
            Node currentNode = iteratorNode.next();
            if (null != currentNode) {
                if (currentNode.hasProperty("bedType")) {
                    room.setBedType(currentNode.getProperty("bedType").getValue().getString());
                }
                Node parentNode = currentNode.getParent().getParent();
                if (null != parentNode && parentNode.hasProperty("roomTitle")) {
                    String roomTitle = parentNode.getProperty("roomTitle").getValue().getString();
                    if (StringUtils.isNotBlank(roomTitle)) {
                        room.setRoomTypeName(roomTitle);
                    }
                }
            }
        }
    }

    private Iterator<Node> queryBedType(HotelDetailsFetcher fetchDetails, ResourceResolver resolver, Session session,
            String hotelPath, Room room) {
        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("path", hotelPath);
        queryParam.put("type", ReservationConstants.NT_UNSTRUCTURED);
        String roomCode = room.getRoomTypeCode();
        queryParam.put("1_property", ReservationConstants.ROOM_CODE);
        queryParam.put("1_property.value", roomCode);
        return fetchDetails.searchRoomType(queryParam, session, resolver);
    }


    /**
     * @param bookingObjectResponse
     * @param converter
     */
    private BookingObject setFlightDetailsToSpecialRequests(BookingObject bookingObjectResponse,
            ObjectConverter converter) {

        Guest responseGuestObject = bookingObjectResponse.getGuest();
        LOG.info("Checking if flight details are present :");
        try {
            if (responseGuestObject != null && responseGuestObject.getSpecialRequests() != null
                    && responseGuestObject.getSpecialRequests().contains("Arrival Flight:")) {
                Flight flightDetails;
                LOG.debug("Found flight details in response. Setting Flight details object.");
                responseGuestObject.setAirportPickup(true);

                flightDetails = converter.getFlightDetails(responseGuestObject.getSpecialRequests());

                bookingObjectResponse.setFlight(flightDetails);
                LOG.info("Removing flight string from Special requests");
                String specialRequests = responseGuestObject.getSpecialRequests();
                String newRequests = getFlightDetails(specialRequests);
                if (newRequests.equals("")) {
                    newRequests = newRequests.substring(0, newRequests.length() - 1);
                    responseGuestObject.setSpecialRequests(newRequests);
                }
                LOG.info("New Special requests string : {}", responseGuestObject.getSpecialRequests());
            }

            splitSpecialRequest(responseGuestObject);
        } catch (IOException e) {
            LOG.error("IOException found in setFlightDetailsToSpecialRequests :: {}", e.getMessage());
            LOG.debug("IOException found in setFlightDetailsToSpecialRequests :: {}", e);
        } catch (Exception e) {
            LOG.error("Exception found in setFlightDetailsToSpecialRequests :: {}", e.getMessage());
            LOG.debug("Exception found in setFlightDetailsToSpecialRequests :: {}", e);
        }
        return bookingObjectResponse;

    }

    private void splitSpecialRequest(Guest responseGuestObject) {
        String guestGstNumber = "guestGSTNumber:";
        if (null != responseGuestObject && StringUtils.isNotBlank(responseGuestObject.getSpecialRequests())
                && responseGuestObject.getSpecialRequests().contains(guestGstNumber)) {

            LOG.info("Special request contain GST Number : {}", responseGuestObject.getSpecialRequests());
            String special = responseGuestObject.getSpecialRequests();
            if (special.contains(",guestGSTNumber:")) {
                special = special.split(",guestGSTNumber:")[0];
            } else if (special.contains(guestGstNumber)) {
                special = special.split(guestGstNumber)[0];
            }
            responseGuestObject.setSpecialRequests(special);
            LOG.info("After removing GST from Special Request : {}", responseGuestObject.getSpecialRequests());
        }
    }

    private String getFlightDetails(String specialRequests) {
        StringTokenizer token = new StringTokenizer(specialRequests, ",");
        String newRequests = "";
        while (token.hasMoreTokens()) {
            String tokenString = token.nextToken();
            if (!tokenString.contains("Arrival Flight:")) {
                LOG.debug("Token : {}", tokenString);
                newRequests = newRequests + tokenString + ",";
            }
        }
        return newRequests;
    }


    /**
     * @param bookingObjectRequest
     * @param objectMapper
     * @param request
     * @return
     */
    private BookingObject buildResponseForHotelConfirmation(BookingObject bookingObjectRequest,
            ObjectMapper objectMapper, SlingHttpServletRequest request) {


        BookingObject bookingObjectResponse = null;
        LOG.info("Creating requests for each room and passing to Booking bundle with Commit status.");
        try {
            Payments guestPayment = bookingObjectRequest.getPayments();
            bookingObjectRequest.setResStatus(ResStatus.Commit);
            if (guestPayment.isPayAtHotel()) {
                guestPayment.setCardNumber(null);
                guestPayment.setCardType(null);
                guestPayment.setExpiryMonth(null);
                guestPayment.setNameOnCard(null);
            }

            if (isRedemptionBooking(guestPayment)) {
                //IRedemptionTransactionsRequest redemptionTransactionsRequest = buildRedemptionRequest(
                      //  bookingObjectRequest);
                //IRedemptionTransactionsResponse redeemResp = null;
               	
               	ITdlRedemTransactionRequestDto redemReq = buildTdRedemptionRequest(bookingObjectRequest);
//                if(bookingObjectRequest.getPayments() != null && 
//                        bookingObjectRequest.getPayments().getTicpoints() != null && 
//                        bookingObjectRequest.getPayments().getTicpoints().isPointsPlusCash()) {
//                        redemptionTransactionsRequest.setPaymentType("Points + Pay");
//                        } else {
//                        redemptionTransactionsRequest.setPaymentType("Points");
//                        }
                IRedemptionTransactionsRequest redemptionTransactionsRequest = new RedemptionTransactionsRequestImpl();
                fetchHotelDetails(bookingObjectRequest, request, redemptionTransactionsRequest);

//                bookingObjectResponse = confirmRedemptionBooking(bookingObjectRequest, guestPayment, objectMapper,
//                        redemptionTransactionsRequest);
                bookingObjectResponse = confirmTdRedemptionBooking(bookingObjectRequest, guestPayment, objectMapper,
                		redemReq);

            } else {
                String debugBookingObjectRequest = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.debug("--> TIC SIBEL CALL :OUTSIDE OF THE FLOW  <-- ");
                LOG.info("Final Booking Commit Request String : {}", debugBookingObjectRequest);
                LOG.debug("-----Passing on bookingDetails object to the booking handler-----");
                bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                debugBookingObjectRequest = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received Booking Commit Response String : {}", debugBookingObjectRequest);
            }
            LOG.debug("--> bookingObjectRes : Received Response from the booking handler <--");
            return bookingObjectResponse;
        } catch (IOException e) {
            LOG.error("Exception occured in buildResponseForHotelConfirmation", e);
        } catch (ServiceException se) {
            LOG.error("Exception occured in parsing TIC Sible call: {}", se.getMessage());
            LOG.error("Exception occured in TIC Sible call:: {}", se);

        } catch (Exception e) {
            LOG.error("Exception occured in buildResponseForHotelConfirmation", e);
        }
        return bookingObjectResponse;

    }

    private boolean isRedemptionBooking(Payments guestPayment) {
        return null != guestPayment && guestPayment.getTicpoints() != null
                && guestPayment.getTicpoints().getPointsType() != null;
    }

    private IRedemptionTransactionsRequest buildRedemptionRequest(BookingObject bookingObjectRequest)
            throws ParseException {
        SimpleDateFormat sdfSiebel = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat sdfSynxis = new SimpleDateFormat("yyyy-MM-dd");
        Date checkInDate = sdfSynxis.parse(bookingObjectRequest.getCheckInDate());
        String checkInSiebelDate = sdfSiebel.format(checkInDate);
        Date checkOutDate = sdfSynxis.parse(bookingObjectRequest.getCheckOutDate());
        String checkOutSiebelDate = sdfSiebel.format(checkOutDate);

        IRedemptionTransactionsRequest redemptionTransactionsRequest = new RedemptionTransactionsRequestImpl();
        redemptionTransactionsRequest.setProductName("Stay");
        redemptionTransactionsRequest.setTransactionChannel(iSibelConfiguration.getTransactionChannel());
        redemptionTransactionsRequest.setMemberNumber(bookingObjectRequest.getGuest().getMembershipId());
        redemptionTransactionsRequest.setCheckInDate(checkInSiebelDate);
        redemptionTransactionsRequest.setCheckOutDate(checkOutSiebelDate);
        LOG.trace("--PaymentType--{}",redemptionTransactionsRequest.getPaymentType());
        LOG.info("--PaymentType--{}",redemptionTransactionsRequest.getPaymentType());
     // Adding extra parameter for booking through redemption
        if(bookingObjectRequest.getPayments() != null) {
        if(bookingObjectRequest.getPayments().getTrackingId() != null && 
        !bookingObjectRequest.getPayments().getTrackingId().isEmpty()) {
                redemptionTransactionsRequest.setPaymentTxnId(bookingObjectRequest.getPayments().getTrackingId());
        }
        if(bookingObjectRequest.getPayments().getAmount() != null &&
        !bookingObjectRequest.getPayments().getAmount().isEmpty()) {
                redemptionTransactionsRequest.setCashPaidAmount(bookingObjectRequest.getPayments().getAmount());
        }
        }
        if(bookingObjectRequest.getRoomList()!= null && bookingObjectRequest.getRoomList().size() > 0) {
        int roomlistSize = bookingObjectRequest.getRoomList().size();
        String comment = roomlistSize + " " + "Room Booking.";
            redemptionTransactionsRequest.setTransactionComments(comment);
        }
        // Called same function with bookingObjectResponse replaced from bookingObjectRequest
        setReservationNumberToRedemption(bookingObjectRequest, redemptionTransactionsRequest);
        return redemptionTransactionsRequest;
    }
    
    private ITdlRedemTransactionRequestDto  buildTdRedemptionRequest(BookingObject bookingObjectRequest)
            throws ParseException {
    	ITdlRedemTransactionRequestDto redemReq = new ITdlRedemTransactionRequestDto();
		
		redemReq.setCustomerHash(bookingObjectRequest.getGuest().getCustomerHash());
		LOG.info("CustomerHash {}",bookingObjectRequest.getGuest().getCustomerHash());
		redemReq.setExternalReferenceNumber(bookingObjectRequest.getItineraryNumber());
		LOG.info("Reference Number {}",bookingObjectRequest.getItineraryNumber());
		//redemReq.setNotes(notes);
		redemReq.setPointsRedeemed(Integer.toString(bookingObjectRequest.getPayments().getTicpoints().getNoTicPoints()));
		LOG.info("Points Redeemed {}",Integer.toString(bookingObjectRequest.getPayments().getTicpoints().getNoTicPoints()));
		redemReq.setTransactionNumber(bookingObjectRequest.getItineraryNumber());
		LOG.info("Transaction Number {}",bookingObjectRequest.getItineraryNumber());
        return redemReq;
    }

    private BookingObject confirmRedemptionBooking(BookingObject bookingObjectRequest, Payments guestPayment,
            ObjectMapper objectMapper, IRedemptionTransactionsRequest redemptionTransactionsRequest) throws Exception {
        String methodName = "confirmRedemptionBooking";
        LOG.trace("Method Entry: {}", methodName);
        LOG.debug("--> TIC SIBEL CALL :INSIDE THE FLOW  <-- ");
        BookingObject bookingObjectResponse;
        String specialRequestsTic = buildTicSpecialRequest(guestPayment, redemptionTransactionsRequest);

        LOG.debug("--> TIC SIBEL CALL INTIATED  <--");
        LOG.debug("--> TIC SIBEL CALL ALL REQUEST  <-- {}", redemptionTransactionsRequest);

        if (validateRedemptionRequest(bookingObjectRequest, redemptionTransactionsRequest)) {
        	LOG.info("redemptionTransactionsRequest {}",redemptionTransactionsRequest);
            LOG.info("redemptionTransactionsRequest.getPaymentType {}",redemptionTransactionsRequest.getPaymentType());
            IRedemptionTransactionsResponse redemptionTransactionsResponse = redemptionTransactionsService
                    .redeemTransaction(redemptionTransactionsRequest);
            LOG.info("redemptionTransactionsResponse {}",redemptionTransactionsResponse);
            LOG.info("redemptionTransactionsResponse.getPointsShortage {}",redemptionTransactionsResponse.getPointsShortage());
            LOG.info("redemptionTransactionsResponse.getErrorMsg {}",redemptionTransactionsResponse.getErrorMsg());
            LOG.info("redemptionTransactionsResponse.getResponse {}",redemptionTransactionsResponse.getResponse());
            LOG.debug("--> TIC SIBEL CALL ALL RESPONSE  <-- {}", redemptionTransactionsResponse);
            String redemptionTransactionsResponseString = redemptionTransactionsResponse.getResponse();
            LOG.debug("--> TIC SIBEL CALL Success RESPONSE  <-- {}", redemptionTransactionsResponseString);
            if (null != redemptionTransactionsResponse && null != redemptionTransactionsResponseString
                    && redemptionTransactionsResponseString.equals("Success")) {

                LOG.debug("--> TIC SIBEL CALL Success  <--");

                setSpecialRequest(bookingObjectRequest, specialRequestsTic, redemptionTransactionsResponse);

                String bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.info("Final Booking Commit Request String : {}", bookingObjectLoggerString);
                LOG.debug("-----Passing on bookingDetails object to the booking handler-----");

                bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received Booking Commit Response String : {}", bookingObjectLoggerString);

                setReservationNumberToRedemption(bookingObjectResponse, redemptionTransactionsRequest);
            } else {
                bookingObjectResponse = setPointRedemptionFailed(bookingObjectRequest,
                        redemptionTransactionsResponse.getErrorMsg());
            }
        } else {
            bookingObjectResponse = setPointRedemptionFailed(bookingObjectRequest,
                    "Validation failed for number of points required to book the selected room.");
        }

        LOG.trace("Method Exit: {}", methodName);
        return bookingObjectResponse;
    }
    
    private BookingObject confirmTdRedemptionBooking(BookingObject bookingObjectRequest, Payments guestPayment,
            ObjectMapper objectMapper, ITdlRedemTransactionRequestDto  redemReq) throws Exception {
        String methodName = "confirmTdRedemptionBooking";
        LOG.trace("Method Entry: {}", methodName);
        IRedemptionTransactionsResponse redeemResp = null;
        String authToken = bookingObjectRequest.getGuest().getAuthToken();
        LOG.info("authToken {}",authToken);
        LOG.debug("--> TIC TD CALL :INSIDE THE FLOW  <-- ");
        BookingObject bookingObjectResponse;
      //  String specialRequestsTic = buildTicSpecialRequest(guestPayment, redemptionTransactionsRequest);

        LOG.debug("--> TIC TD CALL INTIATED  <--");
        LOG.debug("--> TIC TD CALL ALL REQUEST  <-- {}", redemReq);

       
        	LOG.info("redemptionTransactionsRequest {}",redemReq);
          //  LOG.info("redemptionTransactionsRequest.getPaymentType {}",redemptionTransactionsRequest.getPaymentType());
//            IRedemptionTransactionsResponse redemptionTransactionsResponse = redemptionTransactionsService
//                    .redeemTransaction(redemptionTransactionsRequest);
        	
        	redeemResp = iRedeem.redeemTransactionTdl(redemReq, authToken, null);
        	
        	
            LOG.info("redemptionTransactionsResponse {}",redeemResp);
            LOG.info("redemptionTransactionsResponse.getPointsShortage {}",redeemResp.getPointsShortage());
            LOG.info("redemptionTransactionsResponse.getErrorMsg {}",redeemResp.getErrorMsg());
            LOG.info("redemptionTransactionsResponse.getResponse {}",redeemResp.getResponse());
            LOG.debug("--> TIC TD CALL ALL RESPONSE  <-- {}", redeemResp);
            String redemptionTransactionsResponseString = redeemResp.getResponse();
            LOG.debug("--> TIC TD CALL Success RESPONSE  <-- {}", redemptionTransactionsResponseString);
            if (null != redeemResp && null != redemptionTransactionsResponseString
                    && redemptionTransactionsResponseString.equals("Success")) {

                LOG.debug("--> TIC TD CALL Success  <--");

                //setSpecialRequest(bookingObjectRequest, specialRequestsTic, redemptionTransactionsResponse);

                String bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.info("Final Booking Commit Request String : {}", bookingObjectLoggerString);
                LOG.debug("-----Passing on bookingDetails object to the booking handler-----");

                bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received Booking Commit Response String : {}", bookingObjectLoggerString);

                //setReservationNumberToRedemption(bookingObjectResponse, redemptionTransactionsRequest);
            } else {
                bookingObjectResponse = setPointRedemptionFailed(bookingObjectRequest,
                		redeemResp.getErrorMsg());
            }
       
        LOG.trace("Method Exit: {}", methodName);
        return bookingObjectResponse;
    }

    private boolean validateRedemptionRequest(BookingObject bookingObjectRequest,
            IRedemptionTransactionsRequest redemptionRequest) {
        String methodName = "validateRedemptionRequest";
        LOG.trace("Method Entry: {}", methodName);
        boolean isValidBooking = false;
        RoomsAvailabilityRequest availabilityRequest = buildRoomAvailabilityRequestFrom(bookingObjectRequest);
        List<HotelAvailabilityResponse> availabilityResponse = roomAvailabilityService
                .getAvailabilityWithDifferentRooms(availabilityRequest);

        int pointsRequired = getPointsRequiredForBooking(availabilityResponse, bookingObjectRequest);
        int redemptionRequestPoints = Integer.parseInt(redemptionRequest.getRedeemPoints());
        LOG.info("Points required for booking: {} and points mentioned in redemption request: {}", pointsRequired,
                redemptionRequestPoints);
        if (redemptionRequestPoints < pointsRequired) {
            int pointsDifference = redemptionRequestPoints - pointsRequired;
            isValidBooking = validateCashPayment(bookingObjectRequest, pointsDifference);
        } else {
            isValidBooking = true;
        }
        LOG.trace("Method Exit: {}", methodName);
        return true;
    }
    

    private boolean validateCashPayment(BookingObject bookingObjectRequest, int pointsDifference) {
        Payments payments = bookingObjectRequest.getPayments();
        String strAmount = payments.getAmount();
        int amount = getIntegerOf(strAmount);
        LOG.info("An expected amount of {} was to be paid online and found that an amount of {} was paid.",
                pointsDifference, amount);
        return Math.abs(amount - pointsDifference) < 5;
    }

    private int getIntegerOf(String strAmount) {
        int amount = -1;
        try {
            amount = Integer.parseInt(strAmount);
        } catch (NumberFormatException e) {
            LOG.error("An error occurred while parsing the amount paid.", e);
        }
        return amount;
    }

    private int getPointsRequiredForBooking(List<HotelAvailabilityResponse> availabilityResponse,
            BookingObject bookingObjectRequest) {
        String methodName = "getPointsRequiredForBooking";
        LOG.trace("Method Entry: {}", methodName);
        int pointsRequired = 0;

        List<Room> roomList = bookingObjectRequest.getRoomList();
        int size = roomList.size();

        for (int i = 0; i < size; i++) {
            Room room = roomList.get(i);
            String roomTypeCode = room.getRoomTypeCode();
            pointsRequired += getPointsRequiredForRoom(bookingObjectRequest.getHotelId(), roomTypeCode,
                    availabilityResponse);
        }
        LOG.info("Points required for booking: {} is {}", bookingObjectRequest, pointsRequired);
        LOG.trace("Method Exit: {}", methodName);
        return pointsRequired;
    }

    private int getPointsRequiredForRoom(String hotelId, String roomTypeCode,
            List<HotelAvailabilityResponse> availabilityResponse) {
        String methodName = "getPointsRequiredForBooking";
        LOG.trace("Method Entry: {}", methodName);
        int pointsRequired = 0;
        int size = availabilityResponse.size();
        for (int i = 0; i < size; i++) {
            HotelAvailabilityResponse availability = availabilityResponse.get(i);
            Map<String, HotelDto> roomAvailability = availability.getRoomAvailabiilityForOffers();
            HotelDto hotelDto = roomAvailability.get(hotelId);
            if (hotelDto != null) {
                Map<String, RoomAvailabilityDto> availableRooms = hotelDto.getAvailableRooms();
                RoomAvailabilityDto roomAvailabilityDto = availableRooms.get(roomTypeCode);
                if (roomAvailabilityDto != null) {
                    float lowestPrice = roomAvailabilityDto.getLowestPrice();
                    float lowestPriceTax = roomAvailabilityDto.getLowestPriceTax();
                    pointsRequired = (int) (lowestPrice + lowestPriceTax);
                    break;
                }
            }
        }
        LOG.trace("Method Exit: {}", methodName);
        return pointsRequired;
    }


    private RoomsAvailabilityRequest buildRoomAvailabilityRequestFrom(BookingObject bookingObjectRequest) {
        String methodName = "buildRoomAvailabilityRequestFrom";
        LOG.trace("Method Entry: {}", methodName);

        RoomsAvailabilityRequest roomsAvailabilityRequest = new RoomsAvailabilityRequest();

        String checkInDate = bookingObjectRequest.getCheckInDate();
        roomsAvailabilityRequest.setCheckInDate(checkInDate);

        String checkOutDate = bookingObjectRequest.getCheckOutDate();
        roomsAvailabilityRequest.setCheckOutDate(checkOutDate);

        String hotelId = bookingObjectRequest.getHotelId();
        List<String> hotelIds = new ArrayList<>();
        hotelIds.add(hotelId);
        roomsAvailabilityRequest.setHotelIds(hotelIds);

        List<RoomOccupants> occupants = new ArrayList<>();

        List<Room> roomList = bookingObjectRequest.getRoomList();

        List<String> ratePlanCodes = new ArrayList<>();
        for (int i = 0; i < roomList.size(); i++) {
            Room room = roomList.get(i);

            RoomOccupants occupant = getRoomOccupantsFrom(room);
            occupants.add(occupant);

            addRatePlanCode(ratePlanCodes, room);
        }

        roomsAvailabilityRequest.setRoomOccupants(occupants);

        roomsAvailabilityRequest.setRatePlanCodes(ratePlanCodes);

        LOG.debug("Constructed a roomAvailabilityRequest as: {}", roomsAvailabilityRequest);
        LOG.trace("Method Exit: {}", methodName);
        return roomsAvailabilityRequest;

    }

    private void addRatePlanCode(List<String> ratePlanCodes, Room room) {
        String ratePlanCode = room.getRatePlanCode();
        if (!ratePlanCodes.contains(ratePlanCode)) {
            ratePlanCodes.add(ratePlanCode);
        }
    }

    private RoomOccupants getRoomOccupantsFrom(Room room) {
        RoomOccupants occupant = new RoomOccupants();
        int noOfAdults = room.getNoOfAdults();
        occupant.setNumberOfAdults(noOfAdults);

        int noOfChilds = room.getNoOfChilds();
        occupant.setNumberOfChildren(noOfChilds);
        return occupant;
    }

    private void fetchHotelDetails(BookingObject bookingObjectRequest, SlingHttpServletRequest request,
            IRedemptionTransactionsRequest redemptionTransactionsRequest) throws RepositoryException {

        String hotelId = bookingObjectRequest.getHotelId();
        LOG.debug("checking with hotel code inside tajhotels: hotelcode:{}", hotelId);
        HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();

        Hotel hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/tajhotels/en-in/our-hotels", "");
        if (hotel.getPropertyCode() == null || hotel.getHotelName() == null) {// checking under seleqtions
            LOG.debug("checking with hotel code inside seleqtions");
            hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/seleqtions/en-in/our-hotels", "");
        }
        if (hotel.getPropertyCode() == null || hotel.getHotelName() == null) {// checking under vivanta
            LOG.debug("checking with hotel code inside vivanta");
            hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/vivanta/en-in/our-hotels", "");
        }
        LOG.debug("HotelName: {} PropertyCode: {}", hotel.getHotelName(), hotel.getPropertyCode());


        if (null != hotel.propertyCode) {
            redemptionTransactionsRequest.setPropertyCode(hotel.propertyCode);
        }
    }

    private void setSpecialRequest(BookingObject bookingObjectRequest, String specialRequestsTic,
            IRedemptionTransactionsResponse redemptionTransactionsResponse) {
        String specialRequests = "Sibel TIC Transcation ID:" + redemptionTransactionsResponse.getTransactionId()
                + ", Points Redeemed : " + specialRequestsTic + ", "
                + bookingObjectRequest.getGuest().getSpecialRequests();
        bookingObjectRequest.getGuest().setSpecialRequests(specialRequests);
        LOG.debug("TIC SIBEL CALL Special Request to Synxis  : {}", specialRequests);
    }

    private BookingObject setPointRedemptionFailed(BookingObject bookingObjectRequest, String errorMessage) {
        LOG.debug("--> TIC SIBEL CALL FAILED  <--");
        BookingObject bookingObjectResponse = new BookingObject();
        sendIgnoreRequest(bookingObjectRequest);
        // Failure Scenario of TIC FLOW
        if (null != errorMessage) {
            bookingObjectResponse.setSuccess(false);
            TicPoints ticPoints = new TicPoints();
            ticPoints.setRedeemTicOrEpicurePoints(true);
            Payments payments = new Payments();
            payments.setTicpoints(ticPoints);
            bookingObjectResponse.setPayments(payments);
            bookingObjectResponse.setErrorMessage(errorMessage);
        }
        return bookingObjectResponse;
    }

    private void setReservationNumberToRedemption(BookingObject bookingObjectResponse,
            IRedemptionTransactionsRequest redemptionTransactionsRequest) {
        StringBuilder reservationNumber = new StringBuilder();
        if (bookingObjectResponse.getRoomList().size() == 1) {
        	if(bookingObjectResponse.getRoomList().get(0).getReservationNumber()!=null) {
        		reservationNumber = new StringBuilder(bookingObjectResponse.getRoomList().get(0).getReservationNumber());
        	}
        } else {
            for (Room room : bookingObjectResponse.getRoomList()) {
            	if(room.getReservationNumber()!=null) {
	                reservationNumber.append(room.getReservationNumber());
	                reservationNumber.append("-");
            	}
            }

            reservationNumber = reservationNumber.deleteCharAt(reservationNumber.length() - 1);
        }
        LOG.debug("print the value of reservationNumber:: {}", reservationNumber);
        redemptionTransactionsRequest.setRegisterNumber(reservationNumber.toString());
    }

    private String buildTicSpecialRequest(Payments guestPayment,
            IRedemptionTransactionsRequest redemptionTransactionsRequest) {
        String specialRequestTic = "";
        TicPoints ticpoints = guestPayment.getTicpoints();
        String pointsType = ticpoints.getPointsType();
        if (pointsType.equals(TIC)) {
            redemptionTransactionsRequest.setPointsRedeemedType(TIC);
            redemptionTransactionsRequest.setRedeemPoints(String.valueOf(ticpoints.getNoTicPoints()));
            specialRequestTic = ticpoints.getNoTicPoints() + " " + TIC;
        } else if (pointsType.equals(EPICURE)) {
            redemptionTransactionsRequest.setPointsRedeemedType("Epicure");
            redemptionTransactionsRequest.setRedeemPoints(String.valueOf(ticpoints.getNoEpicurePoints()));
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + EPICURE;
        } else if (pointsType.equals(TAP)) {
            redemptionTransactionsRequest.setPointsRedeemedType(TAP);
            redemptionTransactionsRequest.setRedeemPoints(String.valueOf(ticpoints.getNoTicPoints()));
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + TAP;
        } else if (pointsType.equals(TAPP_ME)) {
            redemptionTransactionsRequest.setPointsRedeemedType(TAPP_ME);
            redemptionTransactionsRequest.setRedeemPoints(String.valueOf(ticpoints.getNoTicPoints()));
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + "TAPP Me";
        }
        LOG.debug("--> TIC SIBEL CALL specialRequestTic  <-- {}", specialRequestTic);
        return specialRequestTic;
    }


    /**
     * @param request
     * @param etcWebsiteConfig
     * @return
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws ParseException
     */
    private BookingObject getBookingObjectFromRequest(SlingHttpServletRequest request,
            TajHotelBrandsETCConfigurations etcWebsiteConfig) throws IOException, ParseException {
        BookingObject bookObjectRequest = null;
        if (null == request.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME)
                && null != request.getParameter(BookingConstants.BOOKING_OBJECT_REQUEST)) {

            String payatHotelReq = request.getParameter(BookingConstants.BOOKING_OBJECT_REQUEST);
            try {
                bookObjectRequest = processPayAtHotelData(payatHotelReq);
            } catch (Exception e) {
                LOG.error("Exception occured : {}", e.getMessage());
                LOG.debug("Exception occured : {}", e);
            }

        } else if (null != request.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME)
                && null == request.getParameter(BookingConstants.BOOKING_OBJECT_REQUEST)) {
            String encResp = request.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME);

            bookObjectRequest = processPayOnlineData(encResp, etcWebsiteConfig.getCcAvenueWorkingKey());
            if (!bookObjectRequest.isPaymentStatus()) {
                LOG.info("Payment failed : {}", bookObjectRequest.isPaymentStatus());
                // TODO: Check why this assignment was made.
                // confirmationStatus = BookingConstants.CONFIRM_BOOKING_FAILED;
            }

        }
        return bookObjectRequest;
    }


    /**
     * @param payatHotelRequest
     * @return
     */
    private BookingObject processPayAtHotelData(String payatHotelRequest) {

        LOG.info("Inside processPayatHotelData()");
        LOG.info("processPayAtHotelData:- Received Json : {}", payatHotelRequest);
        ObjectMapper objectMapper = new ObjectMapper();
        BookingObject bookPayatHotelRequest = new BookingObject();
        try {
            bookPayatHotelRequest = objectMapper.readValue(payatHotelRequest, new TypeReference<BookingObject>() {
            });
            bookPayatHotelRequest.setPaymentStatus(true);
        } catch (JsonParseException jpe) {
            LOG.error("Error occured while parsiong json : {}", jpe.getMessage());
            LOG.debug("Error occured while parsiong json : {}", jpe);
            bookPayatHotelRequest.setPaymentStatus(false);
        } catch (JsonMappingException jme) {
            LOG.error("Error Occured while Json Mapping to bookingdetailsrequest Object : {}", jme.getMessage());
            LOG.debug("Error Occured while Json Mapping to bookingdetailsrequest Object : {}", jme);
            bookPayatHotelRequest.setPaymentStatus(false);
        } catch (IOException ioe) {
            LOG.error("Input Outpu Exception : {}", ioe.getMessage());
            LOG.debug("Input Outpu Exception : {}", ioe);
            bookPayatHotelRequest.setPaymentStatus(false);
        } catch (Exception e) {
            LOG.error("Generic exception caught : {}", e.getMessage());
            LOG.debug("Generic exception caught", e);
            bookPayatHotelRequest.setPaymentStatus(false);
        }
        LOG.info("Leaving processPayatHotelData()");
        return bookPayatHotelRequest;
    }

    /**
     * @param encResp
     * @param workingKey
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws ParseException
     */
    private BookingObject processPayOnlineData(String encResp, String workingKey) throws IOException, ParseException {

        LOG.info("Inside processpayOnlineData()");
        Map<String, String> responseMap = new LinkedHashMap<>();
        BookingObject bookOnlineRequestbasic = new BookingObject();
        ObjectMapper objectMapper = new ObjectMapper();
        AesCryptUtil aesUtil = new AesCryptUtil(workingKey);
        String decResp = aesUtil.decrypt(encResp);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");

        createCcAvenueResponseMap(responseMap, tokenizer);

        Guest guestData = new Guest();
        String merchParamResNumbers = "";
        Payments paymentsDetails = buildPaymentData(responseMap);
        handleMerchantParams(responseMap, bookOnlineRequestbasic, objectMapper, paymentsDetails, guestData,
                merchParamResNumbers);

        if (responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS)
                .equalsIgnoreCase(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE)) {
            bookOnlineRequestbasic.setCurrencyCode(responseMap.get(BookingConstants.CCAVENUE_CURRENCY_NAME));
            LOG.debug("Adding ORDER_ID/CC_AVENUE_REF_NUMBER/CRS_NUMBER/AMOUNT_COLLECTED");
            String specialRequestWithRefNumber = responseMap.get(BookingConstants.CCAVENUE_ORDER_ID_NAME) + "/"
                    + responseMap.get(BookingConstants.CCAVENUE_TRACKING_ID) + "/";
            LOG.debug("ORDER_ID/CC_AVENUE_REF_NUMBER/ {}", specialRequestWithRefNumber);
            guestData.setSpecialRequests(specialRequestWithRefNumber);
            bookOnlineRequestbasic.setGuest(guestData);
            bookOnlineRequestbasic.setPaymentStatus(true);
            bookOnlineRequestbasic.setPayments(paymentsDetails);
            if (StringUtils.isNotBlank(responseMap.get(BookingConstants.CCAVENUE_AMOUNT_NAME))) {
                bookOnlineRequestbasic.setTotalPaidAmount(responseMap.get(BookingConstants.CCAVENUE_AMOUNT_NAME));
                LOG.info("Payment Amount : {}", responseMap.get(BookingConstants.CCAVENUE_AMOUNT_NAME));
            }
            LOG.info("Payment status : {}", responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS));
        } else {
            LOG.info("Payment status : {}", responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS));
            LOG.error("Payment failed. Sending Ignore Request.");
            bookOnlineRequestbasic.setPaymentStatus(false);
            ignoreBooking(bookOnlineRequestbasic);
        }
        LOG.info("Leaving processpayOnlineData():");
        return bookOnlineRequestbasic;
    }

    private Payments buildPaymentData(Map<String, String> responseMap) {
        Payments payments = new Payments();
        payments.setOrderId(responseMap.get("order_id"));
        payments.setTrackingId(responseMap.get("tracking_id"));
        payments.setBankRefNum(responseMap.get("bank_ref_no"));
        payments.setPaymentMode(responseMap.get("payment_mode"));
        payments.setCardName(responseMap.get("card_name"));
        payments.setAmount(responseMap.get("amount"));
        return payments;
    }

    private void handleMerchantParams(Map<String, String> responseMap, BookingObject bookOnlineRequestbasic,
            ObjectMapper objectMapper, Payments paymentsDetails, Guest guestData, String merchParamResNumbers)
            throws IOException, ParseException {
        String mapValue;
        for (String mapKey : responseMap.keySet()) {
            String str = mapKey + " ::" + responseMap.get(mapKey);
            LOG.info("for loop : {}", str);
            mapValue = responseMap.get(mapKey);
            if (mapKey.equals("merchant_param1")) {
                LOG.debug("merchant_param1 : {}", mapValue);
                String[] resNumSplitArray = mapValue.split(" - ");
                if (resNumSplitArray.length > 1 && resNumSplitArray[1] != null) {
                    merchParamResNumbers = resNumSplitArray[1];
                }

            }
            handleMerchantParam5(bookOnlineRequestbasic, objectMapper, paymentsDetails, guestData, merchParamResNumbers,
                    mapValue, mapKey);
            if (mapKey.equals("merchant_param3")) {
                // 21 Jun 2019 - 22 Jun 2019
                LOG.debug("merchant_param3:- CheckIn and CheckOut Dates: {}", mapValue);
                String[] tokenArray = mapValue.split(" - ");

                SimpleDateFormat sdfCCAvenue = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat sdfSynxis = new SimpleDateFormat("yyyy-MM-dd");

                LOG.debug("tokenArray[0]: {}", tokenArray[0]);
                Date checkInDate = sdfCCAvenue.parse(tokenArray[0]);
                String checkInSynxisDate = sdfSynxis.format(checkInDate);
                bookOnlineRequestbasic.setCheckInDate(checkInSynxisDate);
                LOG.debug(" CheckIn Date: {}", checkInSynxisDate);

                LOG.debug("tokenArray[1]: {}", tokenArray[1]);
                Date checkOutDate = sdfCCAvenue.parse(tokenArray[1]);
                String checkOutSiebelDate = sdfSynxis.format(checkOutDate);
                bookOnlineRequestbasic.setCheckOutDate(checkOutSiebelDate);
                LOG.debug("CheckOut Date:: {}", checkOutSiebelDate);
            }
            if (mapKey.equals("sub_account_id")) {
                LOG.debug("sub_account_id : {}", mapValue);
            }
            if (mapKey.equals(BookingConstants.CCAVENUE_ORDER_ID_NAME)) {
                LOG.debug(BookingConstants.CCAVENUE_ORDER_ID_NAME, mapValue);
                bookOnlineRequestbasic.setItineraryNumber(mapValue);

                paymentsDetails.setCardNumber(null);
                paymentsDetails.setCardType(null);
                paymentsDetails.setExpiryMonth(null);
                paymentsDetails.setNameOnCard(null);
                paymentsDetails.setPayAtHotel(false);
                paymentsDetails.setPayOnlineNow(true);
                paymentsDetails.setPayUsingCerditCard(false);
                paymentsDetails.setPayUsingGiftCard(false);
                paymentsDetails.setTicpoints(null);
                bookOnlineRequestbasic.setPayments(paymentsDetails);
            }
        }
    }

    private void handleMerchantParam5(BookingObject bookOnlineRequestbasic, ObjectMapper objectMapper,
            Payments paymentsDetails, Guest guestData, String merchParamResNumbers, String mapValue, String mapKey)
            throws IOException {
        if (mapKey.equals("merchant_param5")) {
            LOG.debug("merchant_param5 : {}", mapValue);
            String[] merchParam5StringArray = mapValue.split("_-_");
            if (merchParam5StringArray[0] != null) {
                String[] tokenArray = fetchHotelChainCode(merchParam5StringArray, "Hotel and Chain codes: {}", 0, 2);
                bookOnlineRequestbasic.setHotelId(tokenArray[0]);
                LOG.debug("tokenArray[0]: {}", tokenArray[0]);
                bookOnlineRequestbasic.setChainCode(tokenArray[1]);
                LOG.debug("tokenArray[1]: {}", tokenArray[1]);
            }
            if (merchParam5StringArray.length > 1 && merchParam5StringArray[1] != null) {
                LOG.debug("RatePlan and Promo codes: {}", merchParam5StringArray[1]);

                String[] ratePlanAndPromoCodesArray = merchParam5StringArray[1].split("_");
                String[] resNumArray = merchParamResNumbers.split("_");
                List<Room> roomList = new ArrayList<>();
                splitPromoCode(objectMapper, ratePlanAndPromoCodesArray, resNumArray, roomList);

                bookOnlineRequestbasic.setRoomList(roomList);
            }
            fetchPointsFromMerchantParam(bookOnlineRequestbasic, paymentsDetails, guestData, merchParam5StringArray);

        }
    }

    private void fetchPointsFromMerchantParam(BookingObject bookOnlineRequestbasic, Payments paymentsDetails,
            Guest guestData, String[] merchParam5StringArray) {
        if (merchParam5StringArray.length > 2 && merchParam5StringArray[2] != null) {
            String[] tokenArray = fetchHotelChainCode(merchParam5StringArray, "TIC/EPICURE Details: {}", 2, 3);
            guestData.setMembershipId(tokenArray[2]);
            TicPoints ticPoints = new TicPoints();
            ticPoints.setPointsType(tokenArray[0]);
            if (tokenArray[0].equals(TIC)) {
                ticPoints.setPointsType(TIC);
                ticPoints.setNoTicPoints(Integer.parseInt(tokenArray[1]));
            } else if (tokenArray[0].equals(EPICURE)) {
                ticPoints.setPointsType(EPICURE);
                ticPoints.setNoEpicurePoints(Integer.parseInt(tokenArray[1]));
            } else if (tokenArray[0].equals(TAP)) {
                ticPoints.setPointsType(TAP);
                ticPoints.setNoTicPoints(Integer.parseInt(tokenArray[1]));
            } else if (tokenArray[0].equals(TAPP_ME)) {
                ticPoints.setPointsType(TAPP_ME);
                ticPoints.setNoTicPoints(Integer.parseInt(tokenArray[1]));
            }
            ticPoints.setPointsPlusCash(true);
            paymentsDetails.setTicpoints(ticPoints);
            bookOnlineRequestbasic.setGuest(guestData);
            bookOnlineRequestbasic.setPayments(paymentsDetails);
        }
    }

    private void splitPromoCode(ObjectMapper objectMapper, String[] ratePlanAndPromoCodesArray, String[] resNumArray,
            List<Room> roomList) throws IOException {
        for (int k = 0; ratePlanAndPromoCodesArray.length > k; k++) {
            LOG.debug("ratePlanAndPromoCode: {}", ratePlanAndPromoCodesArray[k]);
            String[] splitedArray = ratePlanAndPromoCodesArray[k].split("-");
            Room room = new Room();
            if (StringUtils.isNotBlank(resNumArray[k])) {
                LOG.debug("resNumber: {}", resNumArray[k]);
                room.setReservationNumber(resNumArray[k]);
                room.setReservationSubString(resNumArray[k].substring(resNumArray[k].length() - 5));
            }
            if (StringUtils.isNotBlank(splitedArray[0])) {
                LOG.debug("ratePlanCode: {}", splitedArray[0]);
                room.setRatePlanCode(splitedArray[0]);
            }
            if (splitedArray.length == 2) {
                LOG.debug("PromoCode: {}", splitedArray[1]);
                room.setPromoCode(splitedArray[1]);
            }
            String debugString = "After adding resNum, RPC and PromoCode final " + k + " room is:"
                    + objectMapper.writeValueAsString(room);
            LOG.debug(debugString);
            roomList.add(room);
        }
    }

    private void createCcAvenueResponseMap(Map<String, String> responseMap, StringTokenizer tokenizer) {
        String pair;
        String key;
        String value;
        while (tokenizer.hasMoreTokens()) {
            pair = tokenizer.nextToken();
            if (pair != null) {
                StringTokenizer strTok = new StringTokenizer(pair, "=");
                if (strTok.hasMoreTokens()) {
                    key = strTok.nextToken();
                    if (strTok.hasMoreTokens()) {
                        value = strTok.nextToken();
                        try {
                            responseMap.put(key, URLDecoder.decode(value, BookingConstants.CHARACTER_ENCOADING));
                        } catch (UnsupportedEncodingException usee) {
                            LOG.debug("Exception occured while decoding merchant_param2: {}", usee.getMessage());
                            LOG.error("Exception occured while decoding merchant_param2: {}", usee);
                        }
                    }
                }
            }
        }
    }

    private String[] fetchHotelChainCode(String[] merchParam5StringArray, String s, int i2, int i3) {
        LOG.debug(s, merchParam5StringArray[i2]);
        StringTokenizer tokenbreak = new StringTokenizer(merchParam5StringArray[i2], "-");
        String[] tokenArray = new String[i3];
        int i = 0;
        while (tokenbreak.hasMoreTokens()) {
            tokenArray[i] = tokenbreak.nextToken();
            i++;
        }
        return tokenArray;
    }

    /**
     * @param bookingObjectReqest
     */
    private boolean sendIgnoreRequest(BookingObject bookingObjectReqest) {
        LOG.info("Inside sendIgnoreRequest method");

        boolean isSuccess = true;
        BookingObject bookingObjectResponse = new BookingObject();

        setIgnoreTo(bookingObjectReqest);

        LOG.info("-----Passing on ignore request to the booking handler-----");
        try {
            bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectReqest);
        } catch (Exception e) {
            LOG.error("Ignore exception: {}", e.getMessage());
            LOG.error("Ignore exception: {}", e);
            isSuccess = false;
        }

        LOG.info("-----Received Response from the booking handler-----");

        if (bookingObjectResponse.isSuccess()) {
            LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_SUCCESSFUL);
            bookingObjectResponse.setResStatus(ResStatus.Ignore);
        } else {
            isSuccess = false;
            LOG.debug("Error while booking Room:  - {}", bookingObjectResponse.getErrorMessage());
        }
        return isSuccess;
    }


    private void setIgnoreTo(BookingObject bookingObjectReqest) {
        bookingObjectReqest.setResStatus(ResStatus.Ignore);
        bookingObjectReqest.getPayments().setCardNumber(null);
        bookingObjectReqest.getPayments().setCardType(null);
        bookingObjectReqest.getPayments().setExpiryMonth(null);
        bookingObjectReqest.getPayments().setNameOnCard(null);
        bookingObjectReqest.getPayments().setTicpoints(null);
        bookingObjectReqest.setGuest(null);
        bookingObjectReqest.setFlight(null);
    }
}