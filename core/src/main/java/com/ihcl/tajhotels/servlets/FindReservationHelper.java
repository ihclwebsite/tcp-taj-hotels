/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.GsonBuilder;
import com.ihcl.core.util.ReservationServletUtil;
import com.ihcl.integration.api.IReadReservationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.reservation.ReadRequest;
import com.ihcl.tajhotels.constants.ReservationConstants;

/**
 * <pre>
* FindReservationHelper.java Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :Dec 18, 2018
 * @ClassName : FindReservationHelper.java
 * @Description : tajhotels-core -FindReservationHelper.java
 * @Modification Information
 *
 *               <pre>
*
*     since            author               description
*  ===========     ==============   =========================
*  Dec 18, 2018     Neha Priyanka            Create
 *
 *               </pre>
 */
public class FindReservationHelper {

    private ReadRequest readRequestObject;

    private BookingObject bookingObject;

    private Session aemSession;

    private ResourceResolver resourceResolver;


    private static final Logger LOG = LoggerFactory.getLogger(FindReservationHelper.class);

    /**
     * Gets the booking reservation response.
     *
     * @param request
     *            the request
     * @param hotelReadReservation
     *            the hotel read reservation
     * @param session
     * @param resolver
     * @return the booking reservation response
     */
    public String getBookingReservationResponse(SlingHttpServletRequest request,
            IReadReservationService hotelReadReservation, ResourceResolver resolver, Session session) {
        String responseFromServer = null;
        try {
            aemSession = session;
            resourceResolver = resolver;
            responseFromServer = getReservationDetailsFromRequest(request, hotelReadReservation);
            if (StringUtils.isBlank(responseFromServer)) {
                responseFromServer = "Reservation Not Found";
            }
        } catch (Exception e) {
            LOG.error("Exception found while trying to get the response from service :: {}", e.getMessage());
            LOG.debug("Exception found while trying to get the response from service :: {}", e);
        }
        return responseFromServer;
    }


    /**
     * Gets the reservation details from request.
     *
     * @param request
     *            the request
     * @param hotelReadReservationService
     *            the hotel read reservation service
     * @return the reservation details from request
     */
    private String getReservationDetailsFromRequest(SlingHttpServletRequest request,
            IReadReservationService hotelReadReservationService) {
        String confirmationNo = request.getParameter(ReservationConstants.CONFIRMATION_NUMBER);
        String emailAddresss = request.getParameter(ReservationConstants.EMAIL_ADDRESS);
        String hotelChainCode = request.getParameter(ReservationConstants.CHAIN_CODE);
        String cdmReferenceId = request.getParameter(ReservationConstants.CDM_REFERENCE_ID);

        readRequestObject = new ReadRequest();
        readRequestObject.setChainCode(hotelChainCode);
        readRequestObject.setEmailAddress(emailAddresss);
        return getBookingObjectResponse(readRequestObject, confirmationNo, cdmReferenceId, hotelReadReservationService,
                hotelChainCode, request);

    }


    /**
     * Gets the booking object response.
     *
     * @param readRequestObject
     *            the read request object
     * @param confirmationNo
     *            the confirmation no
     * @param cdmReferenceId
     *            the cdm reference id
     * @param hotelReadReservationService
     *            the hotel read reservation service
     * @param request
     * @return the booking object response
     */
    private String getBookingObjectResponse(ReadRequest readRequestObject, String confirmationNo, String cdmReferenceId,
            IReadReservationService hotelReadReservationService, String hotelChainCode,
            SlingHttpServletRequest request) {

        String responseString;
        String contentRootPath = request.getParameter(ReservationConstants.CONTENT_ROOT_PATH);
        if (StringUtils.isBlank(contentRootPath)) {
            contentRootPath = ReservationConstants.DEFAULT_ROOT_PATH;
        }
        if (StringUtils.isNotBlank(cdmReferenceId)) {
            readRequestObject.setPinNumber(cdmReferenceId);
            return getBookingListFromResponse(readRequestObject, hotelReadReservationService, request, contentRootPath);
        } else {
            List<String> reservationList = new ArrayList<>();
            int hotelSubstring = Integer.parseInt(request.getParameter("hotelSubstring"));
            String substring = confirmationNo.substring(0, hotelSubstring);
            if (substring.equalsIgnoreCase(hotelChainCode)) {
                LOG.debug("Setting Itinerary number for read reservation");
                readRequestObject.setItineraryNumber(confirmationNo);
            } else {
                LOG.debug("Setting confirmation number for read reservation");
                reservationList.add(confirmationNo);
                readRequestObject.setReservationNumber(reservationList);
            }
            bookingObject = hotelReadReservationService.getReservationDetails(readRequestObject);
            BookingObject bookingResponseJson = ReservationServletUtil.getFinalJsonStructure(request, bookingObject,
                    aemSession, resourceResolver, contentRootPath);
            if (null != bookingResponseJson) {
                responseString = new GsonBuilder().setPrettyPrinting().create().toJson(bookingResponseJson);
                return responseString;
            }
        }
        return StringUtils.EMPTY;
    }


    /**
     * Gets the booking list from response.
     *
     * @param readRequestObject
     *            the read request object
     * @param hotelReadReservationService
     *            the hotel read reservation service
     * @param request
     * @param contentRootPath
     * @return the booking list from response
     */
    private String getBookingListFromResponse(ReadRequest readRequestObject,
            IReadReservationService hotelReadReservationService, SlingHttpServletRequest request,
            String contentRootPath) {
        List<BookingObject> bookingList = hotelReadReservationService.getReservationsFrom(readRequestObject);
        List<BookingObject> responseObjectList = new ArrayList<>();
        for (BookingObject object : bookingList) {
            BookingObject responseObject = ReservationServletUtil.getFinalJsonStructure(request, object, aemSession,
                    resourceResolver, contentRootPath);
            if (null != responseObject) {
                responseObjectList.add(responseObject);
            }
        }
        return new GsonBuilder().setPrettyPrinting().create().toJson(responseObjectList);
    }

}
