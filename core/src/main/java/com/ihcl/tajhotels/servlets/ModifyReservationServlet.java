package com.ihcl.tajhotels.servlets;


import java.io.IOException;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihcl.core.util.ReservationServletUtil;
import com.ihcl.integration.api.IModifyReservationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.tajhotels.constants.ReservationConstants;

/**
 * The Class ModifyReservationServlet.
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/modifyReservation", "sling.servlet.methods=GET" })
public class ModifyReservationServlet extends SlingSafeMethodsServlet {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(ModifyReservationServlet.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The hotel read reservation.
     */
    @Reference
    private IModifyReservationService modifyService;

    /** The config service. */
    @Reference
    com.ihcl.integration.config.ConfigurationService configService;

    Session session;

    ResourceResolver resolver;

    /*
     * (non-Javadoc)
     *
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling. api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        try {
            BookingObject bookingObj = getModifyObjectFromRequest(request);
            resolver = request.getResourceResolver();
            session = resolver.adaptTo(Session.class);
            String responseFromServer = null;
            if (null != bookingObj) {
                BookingObject responseBookingObj = modifyService.modifyExistingReservation(bookingObj, configService);
                if (null != responseBookingObj && responseBookingObj.isSuccess()) {
                    String contentRootPath = request.getParameter(ReservationConstants.CONTENT_ROOT_PATH);
                    responseBookingObj = ReservationServletUtil.getFinalJsonStructure(request, responseBookingObj,
                            session, resolver, contentRootPath);
                    responseFromServer = new GsonBuilder().setPrettyPrinting().create().toJson(responseBookingObj);
                } else if (null != responseBookingObj && !responseBookingObj.isSuccess()) {
                    responseFromServer = responseBookingObj.getErrorMessage();
                }
                if (StringUtils.isNotBlank(responseFromServer)) {
                    LOG.debug("Response From ReadReservation Api{}", responseFromServer);
                    response.getWriter().write(responseFromServer);
                } else {
                    response.getWriter().write("Service not active");
                }

            } else {
                response.getWriter().write("Service not active");
            }

        } catch (IOException e) {
            LOG.debug("Exception found in doGet :: {}", e);
            LOG.error(" Exception found in doGet :: {}", e.getMessage());
        } catch (Exception e) {
            LOG.debug("Exception found in doGet :: {}", e);
            LOG.error(" Exception found in doGet :: {}", e.getMessage());
        }
    }


    /**
     * Gets the modify object from request.
     *
     * @param request
     *            the request
     * @return the modify object from request
     */
    private BookingObject getModifyObjectFromRequest(SlingHttpServletRequest request) {
        String modifyRequestJson = request.getParameter("modifyJson");
        Gson gson = new Gson();
        return gson.fromJson(modifyRequestJson, BookingObject.class);
    }


}
