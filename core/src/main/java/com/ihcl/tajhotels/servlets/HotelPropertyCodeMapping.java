/**
 * class to create a mapping of hotel name and property code for tic stay redemptions
 */
package com.ihcl.tajhotels.servlets;


import java.util.HashMap;
import java.util.Map;

/**
 * @author Nutan
 *
 */
public class HotelPropertyCodeMapping {

    public static Map<String,String> hotelPropertyCode=new HashMap<String,String>();

    static {

        hotelPropertyCode.put("Ama Plantation Trails","C2");
        hotelPropertyCode.put("Ambassador, New Delhi","40");
        hotelPropertyCode.put("Baghvan A Taj Safari","B2");
        hotelPropertyCode.put("Banjaar Tola Kanha","B4");
        hotelPropertyCode.put("Blue Diamond Pune","20");
        hotelPropertyCode.put("Cidade De Goa","C1");
        hotelPropertyCode.put("Jai Mahal Palace","22");
        hotelPropertyCode.put("Mahua Kothi, Bandhavgarh","B3");
        hotelPropertyCode.put("Meghauli Serai Chitwan","TS1");
        hotelPropertyCode.put("Pashan Garh Panna","B5");
        hotelPropertyCode.put("Pratap Mahal, Ajmer","A1");
        hotelPropertyCode.put("President Mumbai","07");
        hotelPropertyCode.put("Rambagh Palace","21");
        hotelPropertyCode.put("SMS Hotel","83");
        hotelPropertyCode.put("Savoy Ooty","42");
        hotelPropertyCode.put("St James Court A Taj Hotel","50");
        hotelPropertyCode.put("Taj 51 Buckingham Gate London","51");
        hotelPropertyCode.put("Taj Aravali Resort & Spa","B8");
        hotelPropertyCode.put("Taj Bangalore Bengaluru","A3");
        hotelPropertyCode.put("Taj Banjara Hyderabad","11");
        hotelPropertyCode.put("Taj Bekal Resort and Spa","54");
        hotelPropertyCode.put("Taj Bengal","03");
        hotelPropertyCode.put("Taj Bentota Resort & Spa","55");
        hotelPropertyCode.put("Taj Boston","85");
        hotelPropertyCode.put("Taj Campton Place","87");
        hotelPropertyCode.put("Taj Cape Town","88");
        hotelPropertyCode.put("Taj Chandigarh Chandigarh","79");
        hotelPropertyCode.put("Taj City Centre","57");
        hotelPropertyCode.put("Taj Club House Chennai","94");
        hotelPropertyCode.put("Taj Connemara, Chennai","08");
        hotelPropertyCode.put("Taj Coral Reef Maldives","A5");
        hotelPropertyCode.put("Taj Corbett Resort & Spa","A6");
        hotelPropertyCode.put("Taj Coromandel Chennai","04");
        hotelPropertyCode.put("Taj Deccan Hyderabad","10");
        hotelPropertyCode.put("Taj Dubai","14");
        hotelPropertyCode.put("Taj Exotica Resort & Spa","A9");
        hotelPropertyCode.put("Taj Exotica Resort Maldives","70");
        hotelPropertyCode.put("Taj Exotica Resort and Spa Goa","27");
        hotelPropertyCode.put("Taj Falaknuma Palace","90");
        hotelPropertyCode.put("Taj Fishermans Cove Resort&Spa","28");
        hotelPropertyCode.put("Taj Fort Aguada Resort and Spa","25");
        hotelPropertyCode.put("Taj Ganges Varanasi","36");
        hotelPropertyCode.put("Taj Green Cove Resort & Spa","78");
        hotelPropertyCode.put("Taj Hari Mahal Jodhpur","38");
        hotelPropertyCode.put("Taj Holiday Village Resort Spa","26");
        hotelPropertyCode.put("Taj Hotel & Convention Centre Agra","C3");
        hotelPropertyCode.put("Taj Krishna Hyderabad","06");
        hotelPropertyCode.put("Taj Kumarakom Resort and Spa","31");
        hotelPropertyCode.put("Taj Lake Palace Udaipur","69");
        hotelPropertyCode.put("Taj Lands End","64");
        hotelPropertyCode.put("Taj M G Road Bengaluru","09");
        hotelPropertyCode.put("Taj Madikeri Resort and Spa","47");
        hotelPropertyCode.put("Taj Mahal Hotel New Delhi","01");
        hotelPropertyCode.put("Taj Mahal Lucknow","18");
        hotelPropertyCode.put("Taj Mahal Tower","00");
        hotelPropertyCode.put("Taj Malabar Resort and Spa","39");
        hotelPropertyCode.put("Taj Nadesar Palace","dummy");
        hotelPropertyCode.put("Taj Palace New Delhi","02");
        hotelPropertyCode.put("Taj Pamodzi Lusaka","61");
        hotelPropertyCode.put("Taj Rishikesh Resort & Spa","93");
        hotelPropertyCode.put("Taj Samudra","52");
        hotelPropertyCode.put("Taj Santacruz Mumbai","A4");
        hotelPropertyCode.put("Taj Swarna Amritsar","A7");
        hotelPropertyCode.put("Taj Tashi Bhutan","93");
        hotelPropertyCode.put("Taj Theog Resort and Spa","A8");
        hotelPropertyCode.put("Taj Usha Kiran Palace","73");
        hotelPropertyCode.put("Taj Wellington Mews Mumbai","77");
        hotelPropertyCode.put("Taj West End Bengaluru","05");
        hotelPropertyCode.put("Taj Yeshwantpur Bengaluru","91");
        hotelPropertyCode.put("Tajview, Agra","35");
        hotelPropertyCode.put("The Gateway Hotel Ambad Nashik","16");
        hotelPropertyCode.put("The Gateway Hotel Athwalines","84");
        hotelPropertyCode.put("The Gateway Hotel Balaghat Rd","62");
        hotelPropertyCode.put("The Gateway Hotel Calicut","17");
        hotelPropertyCode.put("The Gateway Hotel Chikmagalur","34");
        hotelPropertyCode.put("The Gateway Hotel Colombo","53");
        hotelPropertyCode.put("The Gateway Hotel Coonoor","30");
        hotelPropertyCode.put("The Gateway Hotel EM Bypass","33");
        hotelPropertyCode.put("The Gateway Hotel Gir Forest","63");
        hotelPropertyCode.put("The Gateway Hotel M G Road","89");
        hotelPropertyCode.put("The Gateway Hotel Marine Drive","13");
        hotelPropertyCode.put("The Gateway Hotel Old Port Rd","41");
        hotelPropertyCode.put("The Gateway Hotel Pasumalai","29");
        hotelPropertyCode.put("The Gateway Hotel Residency Rd","48");
        hotelPropertyCode.put("The Gateway Hotel Varkala","32");
        hotelPropertyCode.put("The Gateway Hotel Vizag","66");
        hotelPropertyCode.put("The Gateway Resort Damdama","43");
        hotelPropertyCode.put("The Pierre - A Taj Hotel","81");
        hotelPropertyCode.put("The Taj Mahal Palace","B7");
        hotelPropertyCode.put("Umaid Bhawan Palace","82");
        hotelPropertyCode.put("Vivanta Aurangabad Maharashtra","15");
        hotelPropertyCode.put("Vivanta Bengaluru Whitefield","96");
        hotelPropertyCode.put("Vivanta By Taj Trivandrum","95");
        hotelPropertyCode.put("Vivanta Chennai IT Expressway","65");
        hotelPropertyCode.put("Vivanta Coimbatore","45");
        hotelPropertyCode.put("Vivanta Dal View","92");
        hotelPropertyCode.put("Vivanta Goa Panaji","97");
        hotelPropertyCode.put("Vivanta Guwahati","A2");
        hotelPropertyCode.put("Vivanta Hyderabad Begumpet","80");
        hotelPropertyCode.put("Vivanta New Delhi Dwarka","44");
        hotelPropertyCode.put("Vivanta Pune Hinjawadi","A0");
        hotelPropertyCode.put("Vivanta Rebak Island Langkawi","86");
        hotelPropertyCode.put("Vivanta Sawai Madhopur Lodge","56");
        hotelPropertyCode.put("Vivanta Surajkund NCR","49");
        hotelPropertyCode.put("Vivanta Vadodara","67");

    }

}
