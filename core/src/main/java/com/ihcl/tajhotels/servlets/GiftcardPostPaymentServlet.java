/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.ws.rs.ProcessingException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.GiftcardRequest;
import com.ihcl.core.servicehandler.giftcard.IGiftcardRequestHandler;
import com.ihcl.core.util.IGiftcardBatchNumberFetcher;
import com.ihcl.integration.giftcard.config.ConfigurationService;
import com.ihcl.integration.giftcard.dto.ApiWebProperties;
import com.ihcl.integration.giftcard.dto.CreateAndIssueRequest;
import com.ihcl.integration.giftcard.dto.GiftcardResponse;
import com.ihcl.integration.giftcard.dto.details.Customer;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.EGiftcardRequest;

/**
 * @author Vijay Chikkani
 *
 */
@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/giftcardPostPaymentServlet", "sling.servlet.methods={GET,POST}",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class GiftcardPostPaymentServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftcardPostPaymentServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private ConfigurationService configurationService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private IGiftcardRequestHandler iGiftcardRequestHandler;

    @Reference
    private IGiftcardBatchNumberFetcher iGiftcardBatchNumberFetcher;


    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started GiftcardPostPaymentServlet.doPost().");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String paymentStatus = "";
        String redirectUrl = "";
        try {

            TajHotelBrandsETCConfigurations etcWebsiteConfig = fetchGiftcardETCConfigurations(slingHttpServletRequest,
                    objectMapper);

            String encResp = slingHttpServletRequest.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME);

            if (StringUtils.isNotBlank(encResp)) {

                CCAvenueParams ccAvenueParams = processCCavenuedata(encResp, etcWebsiteConfig.getCcAvenueWorkingKey());
                objectNode.put("ccAvenueTrackingId", ccAvenueParams.getTrackingId());
                objectNode.put("paymentStatus", ccAvenueParams.getPaymentstatus());
                if (ccAvenueParams.getPaymentstatus()) {
                    paymentStatus = "Success";
                    redirectUrl = etcWebsiteConfig.getPaymentConfirmUrl();
                    proceedForGiftcardCreation(slingHttpServletRequest, slingHttpServletResponse, objectMapper,
                            objectNode, ccAvenueParams);
                } else {
                    paymentStatus = "Failed";
                    redirectUrl = etcWebsiteConfig.getCcAvenueCancelUrl();
                }

            } else {
                LOGGER.info("Response from CCAvenue getting null/empty. (encResp).");
            }

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, hbe.getMessage());
            LOGGER.debug("Missing information in giftcard creation : {}", hbe.getMessage());
            LOGGER.error("Missing information in giftcard creation : {}", hbe);

        } catch (Exception e) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, "Giftcard creation got interrupted.");
            LOGGER.debug("Exception while giftcard creation : {}", e.getMessage());
            LOGGER.error("Exception while giftcard creation : {}", e);

        } finally {
            LOGGER.debug("End of GiftcardPostPaymentServlet.doPost().");
            writeResponseOnServer(slingHttpServletResponse, objectNode.toString(), paymentStatus, redirectUrl);
        }
    }

    /**
     * @param slingHttpServletRequest
     * @param slingHttpServletResponse
     * @param objectMapper
     * @param objectNode
     * @param ccAvenueParams
     * @return
     * @throws RepositoryException
     * @throws Exception
     */
    private ObjectNode proceedForGiftcardCreation(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse, ObjectMapper objectMapper, ObjectNode objectNode,
            CCAvenueParams ccAvenueParams) throws Exception {

        LOGGER.debug("Starting proceedForGiftcardCreation()");
        LOGGER.debug("Giftcard creation sarted after CCAvenue ::: ->");
        GiftcardRequest giftcardRequest = buildGiftcardRequest(ccAvenueParams);
        ApiWebProperties apiWebProperties = buildApiWebProperties(slingHttpServletRequest, configurationService);
        LOGGER.debug("apiWebProperties from Request: {}", apiWebProperties);
        LOGGER.debug("ccAvenueParamsData from Request: {}", ccAvenueParams);
        LOGGER.debug("giftcardCustomerDetails from Request: {}", giftcardRequest);
        LOGGER.debug("End of proceedForGiftcardCreation()");

        return createGiftcardService(slingHttpServletRequest, slingHttpServletResponse, objectMapper, objectNode,
                ccAvenueParams, giftcardRequest, apiWebProperties);
    }

    /**
     * @param slingHttpServletRequest
     * @param objectMapper
     * @throws HotelBookingException
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    private TajHotelBrandsETCConfigurations fetchGiftcardETCConfigurations(
            SlingHttpServletRequest slingHttpServletRequest, ObjectMapper objectMapper)
            throws HotelBookingException, IOException {
        LOGGER.debug("Starting fetchGiftcardETCConfigurations()");
        LOGGER.info("request.getRequestURL(): {}", slingHttpServletRequest.getRequestURL());
        LOGGER.info("request.getServerName(): {}", slingHttpServletRequest.getServerName());

        String websiteId = slingHttpServletRequest.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "tajhotels";
        }

        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFTCARD_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(ReservationConstants.GIFTCARD_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.GIFTCARD_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.GIFTCARD_CONFIG_EXCEPTION);
        }
        String debugString = "WebsiteId: " + websiteId + ", ConfigurationType: "
                + ReservationConstants.GIFTCARD_CCAVENUE + ", Configurations :-> "
                + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        LOGGER.debug("End of fetchGiftcardETCConfigurations()");

        return etcWebsiteConfig;
    }

    /**
     * @param shServletRequest
     * @param configService
     * @return apiWebProperties
     * @throws RepositoryException
     */
    private ApiWebProperties buildApiWebProperties(SlingHttpServletRequest shServletRequest,
            ConfigurationService configService) throws RepositoryException {
        LOGGER.debug("Started buildApiWebProperties()");
        ApiWebProperties apiWebProperties = new ApiWebProperties();
        apiWebProperties.setTerminalId(configService.getTerminalId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        apiWebProperties.setDateAtClient(sdf.format(new Date()));
        apiWebProperties.setUserName(configService.getUserName());
        apiWebProperties.setPassword(configService.getPassword());
        apiWebProperties.setForwardingEntityId(configService.getForwardingEntityId());
        apiWebProperties.setForwardingEntityPassword(configService.getForwardingEntityPassword());
        apiWebProperties.setCurrentBatchNumber(
                Integer.valueOf(iGiftcardBatchNumberFetcher.getCurrentBatchNumber(shServletRequest, configService)));
        apiWebProperties.setAcquirerId(null);
        apiWebProperties.setMerchantOutletName(configService.getMerchantOutletName());
        apiWebProperties.setOrganizationName(configService.getOrganizationName());
        apiWebProperties.setPosEntryMode(2);
        apiWebProperties.setPosName(null);
        apiWebProperties.setPosTypeId(1);
        apiWebProperties.setTermAppVersion(null);
        LOGGER.debug("End of buildApiWebProperties()");
        return apiWebProperties;
    }

    /**
     * @param ccAvenueParams
     * @return giftcardRequest
     */
    private GiftcardRequest buildGiftcardRequest(CCAvenueParams ccAvenueParams) {

        LOGGER.debug("Started buildGiftcardRequest()");
        GiftcardRequest giftcardRequest = new GiftcardRequest();
        giftcardRequest.setEmail(ccAvenueParams.getBillingEmail());
        String[] billingNameArray = ccAvenueParams.getBillingName().split(" ");
        giftcardRequest.setFirstName(billingNameArray[0]);
        giftcardRequest.setLastName(billingNameArray[1]);
        giftcardRequest.setPhoneNumber(ccAvenueParams.getBillingTel());
        giftcardRequest.setCurrenyType(ccAvenueParams.getCurrency());
        LOGGER.info("ccAvenueParams.getMerchantParam5():- {}", ccAvenueParams.getMerchantParam5());
        String[] giftcardAmountArray = ccAvenueParams.getMerchantParam5().split("_");
        String payableAmount = giftcardAmountArray[0].split("-")[1];
        String giftcardAmount = giftcardAmountArray[1].split("-")[1];
        LOGGER.info("PayableAmount: {}", payableAmount);
        LOGGER.info("GiftcardAmount: {}", giftcardAmount);
        if (giftcardAmountArray[0].split("-")[0].equalsIgnoreCase("GCPA")) {
            giftcardRequest.setPayableAmount(payableAmount);
        }
        if (giftcardAmountArray[1].split("-")[0].equalsIgnoreCase("GCA")) {
            giftcardRequest.setAmount(giftcardAmount);
        }
        LOGGER.debug("End of buildGiftcardRequest()");
        return giftcardRequest;
    }

    /**
     * @param slingHttpServletRequest
     * @param slingHttpServletResponse
     * @param objectMapper
     * @param objectNode
     * @param ccAvenueParams
     * @param giftcardRequest
     * @param apiWebProperties
     * @return ObjectNode
     * @throws Exception
     */
    private ObjectNode createGiftcardService(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse, ObjectMapper objectMapper, ObjectNode objectNode,
            CCAvenueParams ccAvenueParams, GiftcardRequest giftcardRequest, ApiWebProperties apiWebProperties)
            throws Exception {
        LOGGER.debug("Starting createGiftcardService()");
        CreateAndIssueRequest caiRequest = buildCreationRequest(ccAvenueParams, giftcardRequest, apiWebProperties);
        LOGGER.debug("Calling create giftcard service with currentBatchNumber is :- {}",
                caiRequest.getApiWebProperties().getCurrentBatchNumber());
        String connectionTimeout = configurationService.getInitialServiceTimeout();
        int i = 0;
        GiftcardResponse giftcardResponse = null;
        do {
            LOGGER.debug("connectionTimeout: {}", connectionTimeout);
            LOGGER.info("Giftcard CreateAndIssueRequest: {}", objectMapper.writeValueAsString(caiRequest));
            giftcardResponse = getGiftcardResponse(caiRequest, connectionTimeout);
            if (giftcardResponse != null) {
                i = 3;
            }
            ++i;
            connectionTimeout = configurationService.getRestAllServiceTimeout();
        } while (i < 3);

        if (giftcardResponse == null) {
            LOGGER.debug("Response getting from createGiftcard method is null.");
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, "Giftcard Response getting null from Giftcard Backend");
        } else if (Integer.valueOf(giftcardResponse.getResponseCode()).equals(0)) {
            LOGGER.info("************************************************");
            LOGGER.info("Created giftcardResponse: {}", objectMapper.writeValueAsString(giftcardResponse));
            LOGGER.info("************************************************");
            sendEmail(giftcardResponse, caiRequest, configurationService, giftcardRequest);
            objectNode.put(BookingConstants.STATUS, true);
            objectNode.put(BookingConstants.MESSAGE,
                    "Giftcard created Successfully. You will receive Gift card through email.");
        } else if (Integer.valueOf(giftcardResponse.getResponseCode()).equals(10019)
                || Integer.valueOf(giftcardResponse.getResponseCode()).equals(10064)) {

            GiftcardResponse giftcardIitializeResponse = iGiftcardRequestHandler.initializeGiftcard("0");
            if (Integer.valueOf(giftcardIitializeResponse.getResponseCode()).equals(0)) {
                LOGGER.debug("Settting currenctBatchNumber with new initialized number: {}",
                        giftcardIitializeResponse.getApiWebProperties().getCurrentBatchNumber());
                boolean isNewBatchNumberUpdated = iGiftcardBatchNumberFetcher.setCurrentBatchNumber(
                        slingHttpServletRequest, configurationService,
                        String.valueOf(giftcardIitializeResponse.getApiWebProperties().getCurrentBatchNumber()));
                if (isNewBatchNumberUpdated) {
                    LOGGER.info("New GiftcardBatchNumner updated in content.");
                } else {
                    LOGGER.info("New GiftcardBatchNumner not updated in content.");
                }

                createGiftcardService(slingHttpServletRequest, slingHttpServletResponse, objectMapper, objectNode,
                        ccAvenueParams, giftcardRequest, giftcardIitializeResponse.getApiWebProperties());
            } else {
                LOGGER.info(" Found an error while initializing Gift card.");
                throw new HotelBookingException(" Found an error while initializing Gift card. Response code: "
                        + giftcardIitializeResponse.getResponseCode() + ", Message: "
                        + giftcardIitializeResponse.getResponseMessage());
            }
        } else {

            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE,
                    "Giftcard creation failed. Please contact support team for refund.");
            LOGGER.info("Giftcard Bundle got error with " + giftcardResponse.getResponseCode() + " "
                    + giftcardResponse.getResponseMessage());
        }
        LOGGER.debug("End of createGiftcardService()");
        return objectNode;
    }

    /**
     * @param caiRequest
     * @return GiftcardResponse or null
     * @throws Exception
     */
    private GiftcardResponse getGiftcardResponse(CreateAndIssueRequest caiRequest, String connectionTimeout)
            throws Exception {
        try {
            return iGiftcardRequestHandler.createGiftcard(caiRequest, connectionTimeout);

        } catch (ProcessingException ste) {
            LOGGER.debug("ProcessingException: {}", ste.getMessage());
            return null;
        }
    }

    public boolean sendEmail(GiftcardResponse giftcardCreationResponse, CreateAndIssueRequest createAndIssueRequest,
            ConfigurationService configurationService, GiftcardRequest giftcardRequest) throws Exception {
        LOGGER.debug("Starting sendEmail()");
        LOGGER.debug("Preparing sending eGiftcard to customer mailId.");
        boolean emailStatus = sendEGiftcardViaEmail(giftcardCreationResponse, createAndIssueRequest,
                configurationService, giftcardRequest);

        if (emailStatus) {
            LOGGER.info("eGiftcard sent through email.");
        } else {
            LOGGER.info("Issue with mailing eGiftcard.");
        }
        LOGGER.debug("End of sendEmail() with status: {}", emailStatus);
        return emailStatus;
    }

    private boolean sendEGiftcardViaEmail(GiftcardResponse giftcardResponse,
            CreateAndIssueRequest createAndIssueRequest, ConfigurationService configurationService,
            GiftcardRequest giftcardRequest) throws Exception {
        this.configurationService = configurationService;
        LOGGER.debug("Started sendEGiftcardViaEmail()");

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = dateFormat1.parse(giftcardResponse.getCardExpiry());

        EGiftcardRequest eGiftcardRequest = new EGiftcardRequest();
        eGiftcardRequest.setAmount(Integer.parseInt(giftcardRequest.getAmount()));
        eGiftcardRequest.setCardNumber(giftcardResponse.getCardNumber());
        eGiftcardRequest.setCardPIN(giftcardResponse.getCardPIN());
        eGiftcardRequest.setEmailSubject(configurationService.getEGiftcardMailSubject());
        eGiftcardRequest.setEmailBody(configurationService.getEGiftcardMailBodyTemplate());
        eGiftcardRequest.setExpiry(dateFormat2.format(date));
        eGiftcardRequest.setInvoiceNumber(giftcardResponse.getInvoiceNumber());
        eGiftcardRequest.setReceiverName(createAndIssueRequest.getCustomer().getFirstname());
        eGiftcardRequest.setSenderName(createAndIssueRequest.getCustomer().getFirstname());
        eGiftcardRequest.setToMail(createAndIssueRequest.getCustomer().getEmail());

        boolean emailStatus = iEmailService.sendEGiftcardThroughEmail(eGiftcardRequest);
        LOGGER.debug("End of sendEGiftcardViaEmail() with emailStatus of {}", emailStatus);

        return emailStatus;
    }

    /**
     * @param customerDetails
     * @param apiWebProperties
     * @param objectMapper
     * @throws IOException
     * @throws HotelBookingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    private CreateAndIssueRequest buildCreationRequest(CCAvenueParams ccAvenueParams, GiftcardRequest giftcardRequest,
            ApiWebProperties apiWebProperties) throws HotelBookingException {
        LOGGER.debug("Starting buildCreationRequest()");
        CreateAndIssueRequest createRequest = new CreateAndIssueRequest();

        createRequest.setApiWebProperties(apiWebProperties);
        LOGGER.debug("giftcardRequest.getFirstName(): {}", giftcardRequest.getFirstName());
        LOGGER.debug("giftcardRequest.getLastName(): {}", giftcardRequest.getLastName());
        LOGGER.debug("giftcardRequest.getEmail(): {}", giftcardRequest.getEmail());
        LOGGER.debug("giftcardRequest.getPhoneNumber() {}", giftcardRequest.getPhoneNumber());
        LOGGER.debug("giftcardRequest.getPayableAmount() {}", giftcardRequest.getPayableAmount());
        Customer customer = new Customer();
        customer.setFirstname(giftcardRequest.getFirstName());
        customer.setLastName(giftcardRequest.getLastName());
        customer.setEmail(giftcardRequest.getEmail());
        customer.setPhoneNumber(giftcardRequest.getPhoneNumber());
        createRequest.setCustomer(customer);

        int payableAmountInt = (int) Float.parseFloat(giftcardRequest.getPayableAmount());
        int payedAtCCAvenue = (int) Float.parseFloat(ccAvenueParams.getAmount());
        int amountInt = (int) Float.parseFloat(giftcardRequest.getAmount());
        LOGGER.debug("payableAmountInt: {}", payableAmountInt);
        if (Integer.compare(payableAmountInt, payedAtCCAvenue) == 0) {
            createRequest.setAmount(amountInt);
        } else {
            String exceptionString = "Payed amount not matching with discounted amount showing in Giftcard popup."
                    + "Please contact admin to refund if you paid. Your tracking id is "
                    + ccAvenueParams.getTrackingId();
            throw new HotelBookingException(exceptionString);
        }
        int transactionId = (int) (new Date().getTime() / 1000);
        createRequest.setTransactionId(transactionId);
        createRequest.setInvoiceNumber("WEB-" + ccAvenueParams.getTrackingId());
        createRequest.setIdempotencyKey(ccAvenueParams.getOrderId());

        LOGGER.debug("End of buildCreationRequest()");
        return createRequest;
    }

    /**
     * @param redirectUrl
     * @param paymentStatus
     * @param string
     * @param slingHttpServletResponse
     * @param giftcardResponse
     * @param response
     * @param giftcardStatus
     * @param redirectUrl
     */
    private void writeResponseOnServer(SlingHttpServletResponse slingHttpServletResponse, String responseString,
            String paymentStatus, String redirectUrl) {
        try {
            LOGGER.debug("Starting writeResponseOnServer()");
            PrintWriter printWriter = slingHttpServletResponse.getWriter();
            LOGGER.info("redirecting using hidden form");
            LOGGER.debug("Final giftcardResponse : {}", responseString);
            LOGGER.debug("RedirectURL : {}", redirectUrl);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_HTML);
            responseString = StringEscapeUtils.escapeEcmaScript(responseString);
            LOGGER.info("Final giftcardResponseString which is setting as SessionData : {}", responseString);
            printWriter.print("<form id='giftcardCreation' action='" + redirectUrl
                    + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
            printWriter.print("<input type='hidden' name='paymentStatus' value='" + paymentStatus + "'>");
            printWriter.print("</form>");
            printWriter.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                    + "sessionStorage.setItem('giftcardResponseString', '" + responseString + "');"
                    + "sessionStorage.setItem('paymentStatus', '" + paymentStatus + "');"
                    + "document.getElementById(\"giftcardCreation\").submit()" + "}" + "</script>");
            LOGGER.info("end of output stream");
            LOGGER.debug("End of writeResponseOnServer()");
            printWriter.close();

        } catch (Exception e) {
            LOGGER.debug("Error while submitting form : {}", e.getMessage());
            LOGGER.debug("error while submitting form : {}", e);
        }

    }

    /**
     * @param encResp
     * @param ccAvenueWorkingKey
     * @return CCAvenueParams
     * @throws UnsupportedEncodingException
     */
    private CCAvenueParams processCCavenuedata(String encResp, String ccAvenueWorkingKey)
            throws UnsupportedEncodingException {
        LOGGER.debug("Starting processCCavenuedata()");
        Map<String, String> responseMap = new LinkedHashMap<>();
        AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueWorkingKey);
        String decResp = aesUtil.decrypt(encResp);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
        String pair = null;
        String key = "";
        String value = "";
        while (tokenizer.hasMoreTokens()) {
            pair = tokenizer.nextToken();
            if (pair != null) {
                StringTokenizer strTok = new StringTokenizer(pair, "=");
                if (strTok.hasMoreTokens()) {
                    key = strTok.nextToken();
                    if (strTok.hasMoreTokens()) {
                        value = strTok.nextToken();
                        try {
                            responseMap.put(key, URLDecoder.decode(value, BookingConstants.CHARACTER_ENCOADING));
                        } catch (UnsupportedEncodingException usee) {
                            LOGGER.error("** Error occured while decoding CCAvenueParmas **");
                            throw usee;
                        }
                    }
                }
            }
        }
        LOGGER.debug("End of processCCavenuedata()");
        return extractingCCAvenueParams(responseMap);
    }

    /**
     * @param responseMap
     * @return ccavenueParams
     */
    private CCAvenueParams extractingCCAvenueParams(Map<String, String> responseMap) {

        LOGGER.debug("Starting extractingCCAvenueParams()");
        CCAvenueParams ccavenueParams = new CCAvenueParams();

        for (Map.Entry<String, String> entry : responseMap.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue();
            String str = mapKey + " ::: " + mapValue;
            LOGGER.info("CCAvenue params: {}", str);
        }

        ccavenueParams.setPaymentstatus(responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS)
                .equalsIgnoreCase(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE));

        ccavenueParams.setTrackingId(responseMap.get(BookingConstants.CCAVENUE_TRACKING_ID));
        ccavenueParams.setOrderId(responseMap.get("order_id"));
        ccavenueParams.setCurrency(responseMap.get("currency"));
        ccavenueParams.setAmount(responseMap.get("amount"));
        ccavenueParams.setBillingName(responseMap.get("billing_name"));
        ccavenueParams.setBillingTel(responseMap.get("billing_tel"));
        ccavenueParams.setBillingEmail(responseMap.get("billing_email"));

        ccavenueParams.setMerchantParam1(responseMap.get("merchant_param1"));
        ccavenueParams.setMerchantParam2(responseMap.get("merchant_param2"));
        ccavenueParams.setMerchantParam3(responseMap.get("merchant_param3"));
        ccavenueParams.setMerchantParam4(responseMap.get("merchant_param4"));
        ccavenueParams.setMerchantParam5(responseMap.get("merchant_param5"));

        ccavenueParams.setSubAccountId(responseMap.get("sub_account_id"));
        LOGGER.debug("End of extractingCCAvenueParams()");

        return ccavenueParams;
    }
}
