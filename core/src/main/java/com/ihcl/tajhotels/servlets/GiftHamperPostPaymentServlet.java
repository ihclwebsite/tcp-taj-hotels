/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.HotelDetailsFetcher;
import com.ihcl.core.services.config.GiftHamperConfigurationService;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;

/**
 * @author Vijay Chikkani
 *
 */
@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/giftHamperPostPaymentServlet", "sling.servlet.methods={GET,POST}",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class GiftHamperPostPaymentServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftHamperPostPaymentServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private GiftHamperConfigurationService giftHamperConfigurationService;

    @Reference
    private SearchProvider searchProvider;

    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started GiftHamperPostPaymentServlet.doPost().");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String paymentStatus = "";
        try {

            Map<String, Object> objectMap = fetchgETCConfigurations(slingHttpServletRequest, objectMapper);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = (TajHotelBrandsETCConfigurations) objectMap
                    .get("etcWebsiteConfig");

            String encResp = slingHttpServletRequest.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME);

            if (StringUtils.isNotBlank(encResp)) {

                CCAvenueParams ccAvenueParams = processCCavenuedata(encResp, etcWebsiteConfig.getCcAvenueWorkingKey());
                objectNode.put("ccAvenueTrackingId", ccAvenueParams.getTrackingId());
                objectNode.put("paymentStatus", ccAvenueParams.getPaymentstatus());
                String loggerString = "ccAvenueTrackingId: " + ccAvenueParams.getTrackingId() + "; paymentStatus: "
                        + ccAvenueParams.getPaymentstatus();
                LOGGER.info(loggerString);

                String merchantParam5 = ccAvenueParams.getMerchantParam5();
                LOGGER.info("merchantParam5: {}", merchantParam5);
                // mechParam5Info:- 1.HotelId, 2.GiftHamperCode, 3.CollectionType, 4.SubTotalAmount and 5.ShippingAmount
                String[] merchantParam5Array = merchantParam5.split("_");
                String hotelId = merchantParam5Array[0];
                String giftHamperCode = merchantParam5Array[1];
                String collectionType = merchantParam5Array[2];

                String contentRootPath = (String) objectMap.get("contentRootPath");

                Map<String, Object> hotelAndHamperDetailsMap = getHotelAndHamperDetails(slingHttpServletRequest,
                        objectMapper, contentRootPath, hotelId, giftHamperCode);
                String giftHamperPagePath = (String) hotelAndHamperDetailsMap.get("giftHamperPath");
                if (StringUtils.isBlank(giftHamperPagePath)) {
                    throw new HotelBookingException("giftHamperPagePath not found");
                }

                String subTotalAmount = merchantParam5Array[3];
                String shippingAmount = "0";
                if (5 < merchantParam5Array.length) {
                    shippingAmount = merchantParam5Array[4];
                }

                int totalAmount = Integer.parseInt(subTotalAmount) + Integer.parseInt(shippingAmount);
                float ccAvenueAmount = Float.valueOf(ccAvenueParams.getAmount());
                if (totalAmount != (int) ccAvenueAmount) {
                    LOGGER.info(
                            "Issue found while comparing of totalPayedAmount and ( shippingAmount + subTtotalAmount ).");
                }

                if (ccAvenueParams.getPaymentstatus()) {
                    LOGGER.info("Paymentstatus: {}", ccAvenueParams.getPaymentstatus());
                    paymentStatus = "Success";
                    Hotel hotelDetails = (Hotel) hotelAndHamperDetailsMap.get("hotelDetails");
                    objectNode.put(BookingConstants.STATUS, true);
                    String formSessionAdditionalDetails = fetchCookieFromRequest(slingHttpServletRequest);
                    sendEmailToCustomerAndHotelAuthority(ccAvenueParams, hotelDetails, giftHamperConfigurationService,
                            formSessionAdditionalDetails);
                } else {
                    paymentStatus = "Failed";
                }

            } else {
                LOGGER.info("Response from CCAvenue getting null/empty. (encResp).");
            }

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, hbe.getMessage());
            LOGGER.debug("Missing information in gift hamper creation : {}", hbe.getMessage());
            LOGGER.error("Missing information in gift hamper creation : {}", hbe);

        } catch (Exception e) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, "Gift hamper creation got interrupted.");
            LOGGER.debug("Exception while gift hamper creation : {}", e.getMessage());
            LOGGER.error("Exception while gift hamper creation : {}", e);

        } finally {
            writeResponseOnServer(slingHttpServletResponse, objectNode.toString(), paymentStatus);
        }
    }

    /**
     * @param slingHttpServletRequest
     * @return
     */
    private String fetchCookieFromRequest(SlingHttpServletRequest slingHttpServletRequest) {
        String cookies = "";
        String formSessionAdditionalDetails = "";
        if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("cookie"))) {
            cookies = slingHttpServletRequest.getHeader("cookie");
            LOGGER.info("Cookies header name : cookie");
        } else if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("Cookie"))) {
            cookies = slingHttpServletRequest.getHeader("Cookie");
            LOGGER.info("Cookies header name : Cookie");
        }
        if (StringUtils.isNotBlank(cookies)) {
            String[] cookieParamsArray = cookies.split("; ");
            for (String cookieParams : cookieParamsArray) {
                LOGGER.info("cookieParams: {}", cookieParams);
                String[] cookieNameAndValuePair = cookieParams.split("=");
                LOGGER.info("cookieNameAndValuePair[0]: {}", cookieNameAndValuePair[0]);
                if (cookieNameAndValuePair[0].equalsIgnoreCase("formadditionaldetails")) {
                    LOGGER.info("cookieNameAndValuePair[1]: {}", cookieNameAndValuePair[1]);
                    formSessionAdditionalDetails = cookieNameAndValuePair[1];
                }
            }
        }

        LOGGER.info("formSessionAdditionalDetails: {}", formSessionAdditionalDetails);
        return formSessionAdditionalDetails;
    }

    private void sendEmailToCustomerAndHotelAuthority(CCAvenueParams ccAvenueParams, Hotel hotelDetails,
            GiftHamperConfigurationService giftHamperConfigurationService, String formSessionAdditionalDetails) {

        LOGGER.info("Sending Email to hotel authority: {}", hotelDetails.getGiftHamperAdminMailId());
        sendMailWithGiftHamper(ccAvenueParams, hotelDetails.getGiftHamperAdminMailId(),
                giftHamperConfigurationService.getHotelEmailBodyTemplate(),
                giftHamperConfigurationService.getHotelEmailSubject(), formSessionAdditionalDetails,
                hotelDetails.getHotelName());
        LOGGER.info(" Sending Email to Sender reference: {}", ccAvenueParams.getBillingEmail());
        sendMailWithGiftHamper(ccAvenueParams, ccAvenueParams.getBillingEmail(),
                giftHamperConfigurationService.getCustomerEmailBodyTemplate(),
                giftHamperConfigurationService.getCustomerEmailSubject(), formSessionAdditionalDetails,
                hotelDetails.getHotelName());
    }

    private void sendMailWithGiftHamper(CCAvenueParams ccAvenueParams, String toMail, String emailBodyTemplate,
            String mailSubject, String formSessionAdditionalDetails, String hotelName) {

        String giftHamperMailBody = emailBodyTemplate;

        giftHamperMailBody = giftHamperMailBody.replace("$HOTELNAME$", hotelName);

        giftHamperMailBody = giftHamperMailBody.replace("$CCAVENUETRACKINGID$", ccAvenueParams.getTrackingId());
        giftHamperMailBody = giftHamperMailBody.replace("$CCAVENUEORDERID$", ccAvenueParams.getOrderId());

        giftHamperMailBody = giftHamperMailBody.replace("$SENDERNAME$", ccAvenueParams.getBillingName());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDEREMAIL$", ccAvenueParams.getBillingEmail());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDERPHONENUMBER$", ccAvenueParams.getBillingTel());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDERAMOUNT$", ccAvenueParams.getAmount());

        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGNAME$", ccAvenueParams.getMerchantParam1());
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGPHONENUMBER$", ccAvenueParams.getMerchantParam2());
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGADDRESS$", ccAvenueParams.getMerchantParam3());

        // shippingCity() + "_" + shippingPincode() + "_"+ shippingState() + "_"
        // +shippingCountry()
        String[] merchantParam4Array = ccAvenueParams.getMerchantParam4().split("_");
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGCITY$", merchantParam4Array[0]);
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGPINCODE$", merchantParam4Array[1]);
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGSTATE$", merchantParam4Array[2]);
        giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGCOUNTRY$", merchantParam4Array[3]);

        // mechParam5Info:- 1.HotelId, 2.GiftHamperCode, 3.CollectionType, 4.SubTotalAmount and 5.ShippingAmount
        String[] merchantParam5Array = ccAvenueParams.getMerchantParam5().split("_");
        giftHamperMailBody = giftHamperMailBody.replace("$HOTELID$", merchantParam5Array[0]);
        giftHamperMailBody = giftHamperMailBody.replace("$GIFTHAMPERCODE$", merchantParam5Array[1]);
        giftHamperMailBody = giftHamperMailBody.replace("$GIFTHAMPERTYPE$", merchantParam5Array[2]);
        giftHamperMailBody = giftHamperMailBody.replace("$SUBTOTALAMOUNT$", merchantParam5Array[3]);
        if (5 < merchantParam5Array.length) {
            giftHamperMailBody = giftHamperMailBody.replace("$SHIPPINGAMOUNT$", merchantParam5Array[4]);
        }

        if (null != formSessionAdditionalDetails
                && formSessionAdditionalDetails.contains("_giftHamperAdditionalMessage_")) {
            String data = formSessionAdditionalDetails.replace("_giftHamperAdditionalMessage_", "");
            if (StringUtils.isNotBlank(data)) {
                String[] additionalDetails = formSessionAdditionalDetails.split("_giftHamperAdditionalMessage_");
                String specialInstructions = StringUtils.isNotBlank(additionalDetails[0]) ? additionalDetails[0] : "NA";
                String yourMessage = StringUtils.isNotBlank(additionalDetails[1]) ? additionalDetails[1] : "NA";
                giftHamperMailBody = giftHamperMailBody.replace("$SPECIALINSTRUCTIONS$", specialInstructions);
                giftHamperMailBody = giftHamperMailBody.replace("$YOURMESSAGE$", yourMessage);
            }
        }
        boolean status = iEmailService.sendEMail(mailSubject, giftHamperMailBody, toMail, null, null, null);
        if (status) {
            LOGGER.info("Gift Hamper conformation sent through email. Mail status: {}", status);
        } else {
            LOGGER.info("Issue found while mailing Gift Hamper. Mail status: {}", status);
        }

    }

    private Map<String, Object> getHotelAndHamperDetails(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper, String rootPath, String hotelId, String giftHamperCode) throws IOException {

        Map<String, Object> hotelAndHamperDetailsMap = new HashMap<>();
        try {
            HotelDetailsFetcher hotelDetailsFetcher = new HotelDetailsFetcher();
            Hotel hoteldetails = hotelDetailsFetcher.getHotelDetails(slingHttpServletRequest, hotelId, rootPath, "");
            String hoteldata = hotelId + "'s hoteldetails: " + objectMapper.writeValueAsString(hoteldetails);
            LOGGER.info(hoteldata);
            hotelAndHamperDetailsMap.put("hotelDetails", hoteldetails);
            HashMap<String, String> query = new HashMap<>();
            query.put("path", hoteldetails.hotelResourcePath);
            query.put("1_property", "sling:resourceType");
            query.put("1_property.value", "tajhotels/components/content/tic/redeem-card-details");
            query.put("2_property", "giftHamperCode");
            query.put("2_property.value", giftHamperCode);
            SearchResult giftHamperPageResult = searchProvider.getQueryResultWithoutClosing(query);
            List<Hit> resultHit = giftHamperPageResult.getHits();

            for (Hit gifthamperHit : resultHit) {
                LOGGER.info("gifthamperHit.getPath(): {}", gifthamperHit.getPath());
                hotelAndHamperDetailsMap.put("giftHamperPath",
                        gifthamperHit.getPath().replace("/jcr:content/root/redeem_card_details", ""));
            }
        } catch (Exception e) {
            LOGGER.debug("Exception while getting giftHamperPagePath: {}", e.getMessage());
            LOGGER.error("Exception while getting giftHamperPagePath: {}", e);
        }
        LOGGER.info("hotelAndHamperDetailsMap: {}", objectMapper.writeValueAsString(hotelAndHamperDetailsMap));
        return hotelAndHamperDetailsMap;

    }

    private void writeResponseOnServer(SlingHttpServletResponse slingHttpServletResponse, String responseString,
            String paymentStatus) {
        String methodName = "writeResponseOnServer";
        LOGGER.trace("Method Entry: " + methodName);
        try {
            PrintWriter printWriter = slingHttpServletResponse.getWriter();
            LOGGER.info("redirecting using hidden form");
            LOGGER.info("Final giftHamperResponse : {}", responseString);
            String redirectUrl = giftHamperConfigurationService.getConfirmationPagePath();
            if (paymentStatus == null || !paymentStatus.equalsIgnoreCase("success")) {
                redirectUrl = giftHamperConfigurationService.getFailurePagePath();
            }
            LOGGER.info("Received redirection page path from configuration as: {}", redirectUrl);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_HTML);
            responseString = StringEscapeUtils.escapeEcmaScript(responseString);
            LOGGER.info("Final giftHamperResponseString which is setting as SessionData : {}", responseString);
            printWriter.print("<form id='giftHamperCreation' action='" + redirectUrl
                    + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
            printWriter.print("<input type='hidden' name='paymentStatus' value='" + paymentStatus + "'>");
            printWriter.print("</form>");
            printWriter.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                    + "sessionStorage.setItem('giftHamperResponseString', '" + responseString + "');"
                    + "sessionStorage.setItem('paymentStatus', '" + paymentStatus + "');"
                    + "document.getElementById(\"giftHamperCreation\").submit()" + "}" + "</script>");
            LOGGER.info("end of output stream");
            printWriter.close();

        } catch (Exception e) {
            LOGGER.error("Error while submitting form : {}", e.getMessage());
            LOGGER.debug("error while submitting form : {}", e);
        }
        LOGGER.trace("Method Exit: " + methodName);

    }

    private Map<String, Object> fetchgETCConfigurations(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper) throws HotelBookingException, IOException {

        LOGGER.info("request.getRequestURL(): {}", slingHttpServletRequest.getRequestURL());
        LOGGER.info("request.getServerName(): {}", slingHttpServletRequest.getServerName());

        String websiteId = slingHttpServletRequest.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "tajhotels";
        }
        Map<String, Object> objectMap = new HashMap<>();
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(ReservationConstants.GIFT_HAMPER_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.GIFT_HAMPER_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        objectMap.put("contentRootPath", etcWebsiteConfig.getContentRootPath());
        String debugString = "WebsiteId: " + websiteId + ", ConfigurationType: "
                + ReservationConstants.GIFT_HAMPER_CCAVENUE + ", Configurations :-> "
                + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        objectMap.put("etcWebsiteConfig", etcWebsiteConfig);
        return objectMap;
    }

    private CCAvenueParams processCCavenuedata(String encResp, String ccAvenueWorkingKey)
            throws UnsupportedEncodingException {
        Map<String, String> responseMap = new LinkedHashMap<>();
        AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueWorkingKey);
        String decResp = aesUtil.decrypt(encResp);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
        String pair = null;
        String key = "";
        String value = "";
        while (tokenizer.hasMoreTokens()) {
            pair = tokenizer.nextToken();
            if (pair != null) {
                StringTokenizer strTok = new StringTokenizer(pair, "=");
                if (strTok.hasMoreTokens()) {
                    key = strTok.nextToken();
                    if (strTok.hasMoreTokens()) {
                        value = strTok.nextToken();
                        try {
                            responseMap.put(key, URLDecoder.decode(value, BookingConstants.CHARACTER_ENCOADING));
                        } catch (UnsupportedEncodingException usee) {
                            LOGGER.error("** Error occured while decoding CCAvenueParmas **");
                            throw usee;
                        }
                    }
                }
            }
        }
        return extractingCCAvenueParams(responseMap);
    }

    private CCAvenueParams extractingCCAvenueParams(Map<String, String> responseMap) {
        CCAvenueParams ccavenueParams = new CCAvenueParams();

        for (Map.Entry<String, String> entry : responseMap.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue();
            String str = mapKey + " ::: " + mapValue;
            LOGGER.info("CCAvenue params: {}", str);
        }

        ccavenueParams.setPaymentstatus(responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS)
                .equalsIgnoreCase(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE));

        ccavenueParams.setTrackingId(responseMap.get(BookingConstants.CCAVENUE_TRACKING_ID));
        ccavenueParams.setOrderId(responseMap.get("order_id"));
        ccavenueParams.setCurrency(responseMap.get("currency"));
        ccavenueParams.setAmount(responseMap.get("amount"));

        ccavenueParams.setBillingName(responseMap.get("billing_name"));
        ccavenueParams.setBillingTel(responseMap.get("billing_tel"));
        ccavenueParams.setBillingEmail(responseMap.get("billing_email"));
        ccavenueParams.setBillingAddress(responseMap.get("billing_address"));
        ccavenueParams.setBillingCity(responseMap.get("billing_city"));
        ccavenueParams.setBillingZip(responseMap.get("billing_zip"));
        ccavenueParams.setBillingState(responseMap.get("billing_state"));
        ccavenueParams.setBillingCountry(responseMap.get("billing_country"));

        ccavenueParams.setMerchantParam1(responseMap.get("merchant_param1"));
        ccavenueParams.setMerchantParam2(responseMap.get("merchant_param2"));
        ccavenueParams.setMerchantParam3(responseMap.get("merchant_param3"));
        ccavenueParams.setMerchantParam4(responseMap.get("merchant_param4"));
        ccavenueParams.setMerchantParam5(responseMap.get("merchant_param5"));

        ccavenueParams.setSubAccountId(responseMap.get("sub_account_id"));
        return ccavenueParams;
    }

}
