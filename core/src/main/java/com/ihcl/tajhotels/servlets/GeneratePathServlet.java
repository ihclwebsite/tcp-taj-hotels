package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.HotelsSearchService;

@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.paths=" + "/bin/resource/urlPath",
		"sling.servlet.methods=GET", "sling.servlet.resourceTypes=services/powerproxy",
		"sling.servlet.selectors=groups", })
public class GeneratePathServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(GeneratePathServlet.class);

	@Reference
	private HotelsSearchService hotelsearchService;

	@Reference
	private ResourceResolverFactory resolverFactory;

	private Session session;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		try {
			List<String> hotelPagePath = hotelsearchService.getAllHotelResourcePath();

			ResourceResolver resourceResolver = request.getResourceResolver();
			session = resourceResolver.adaptTo(Session.class);

			// Create parent nodes
			String scheme = request.getScheme();
			int port = request.getServerPort();
			String serverName = request.getServerName();

			Node beginNode = session.getRootNode().getNode("etc/map");

			// Create http or https node
			if (doesNodeExist(beginNode, scheme) == -1) {
				beginNode.addNode(scheme, "sling:Folder");
				session.save();
			}
			Node schemeNode = session.getRootNode().getNode("etc/map/" + scheme);

			String urlServerName = null;
			if (port != 80)
				urlServerName = serverName + "." + port;
			else
				urlServerName = serverName;

			// Create website url server node
			if (doesNodeExist(schemeNode, urlServerName) == -1) {
				LOG.debug("server page creation started.");
				schemeNode.addNode(urlServerName, "sling:Mapping");
				LOG.debug("server page property creation completed.");
				session.save();
			}

			Node serverNode = session.getRootNode().getNode("etc/map/" + scheme + "/" + urlServerName);
			// serverNode.setProperty("sling:match", "[^/]+");

			// Create en-in node
			if (doesNodeExist(serverNode, "en-in") == -1) {
				Node newNode = serverNode.addNode("en-in", "sling:Mapping");
				newNode.setProperty("sling:match", "en-in");
				session.save();
			}

			String beginNodePath = "etc/map/" + scheme + "/" + urlServerName + "/en-in";

			// Creating Hotel Page Nodes
			for (String hotelPath : hotelPagePath) {
				String[] hotelPathArr = hotelPath.substring(1).split("/");
				String hotelDestinationName = hotelPathArr[4];
				int startIndexOfActualPath = hotelPath.indexOf(hotelDestinationName);
				createHotelPageNodes(hotelPath, startIndexOfActualPath, beginNodePath);
			}

			LOG.debug("all hotel page creation completed.");

			session.logout();
		} catch (Exception e) {
			LOG.error("Error after creating hotel nodes first time " + e.getMessage());
		}
	}

	private void createHotelPageNodes(String hotelPath, int startIndexOfActualPath2, String beginNodePath) {
		try {
			Map<String, String> urlListMap = new HashMap<>();
			String[] distinctNodes = hotelPath.substring(startIndexOfActualPath2).split("/");
			// added below if condition to restrict data set to pages under brands and not about / exp /rest pages of destination
			if (distinctNodes[1].equals("taj") || distinctNodes[1].equals("gateway")
					|| distinctNodes[1].equals("vivanta")) {
				StringBuilder hotelURLPrefix = new StringBuilder(
						"/content/tajhotels/en-in/our-hotels/" + distinctNodes[0]);
				LOG.debug("Array is" + distinctNodes);

				for (int i = 1; i < distinctNodes.length; i++) {
					urlListMap.put(distinctNodes[i], hotelURLPrefix.append("/" + distinctNodes[i]).toString());
				}
				if (distinctNodes.length > 2) {
					LOG.debug("distinctNodes length  is" + distinctNodes.length);
					urlListMap.put(distinctNodes[2], urlListMap.get(distinctNodes[2]));
					LOG.debug("map second element" + urlListMap.get(distinctNodes[2]));
				}

				

				StringBuilder currentNodePath = new StringBuilder(beginNodePath);
				Node dynamicTajNode = session.getRootNode().getNode(currentNodePath.toString());

				for (int i = 1; i < distinctNodes.length; i++) {
					if (doesNodeExist(dynamicTajNode, distinctNodes[i]) == -1) {
						Node newNode = dynamicTajNode.addNode(distinctNodes[i], "sling:Mapping");
						newNode.setProperty("sling:match", newNode.getName());
						newNode.setProperty("sling:internalRedirect", urlListMap.get(distinctNodes[i]) + ".html");
						LOG.debug("map random element " + urlListMap.get(distinctNodes[i]));
						session.save();
					}
					dynamicTajNode = session.getRootNode()
							.getNode(currentNodePath.append("/" + distinctNodes[i]).toString());
				}
			}
		} catch (Exception e) {
			LOG.error("Error in creating hotel nodes " + e.getMessage());
		}
	}

	private int doesNodeExist(Node tajNode, String node) {
		try {
			int childRecs = 0;
			java.lang.Iterable<Node> nodeList = JcrUtils.getChildNodes(tajNode, node);
			Iterator it = nodeList.iterator();

			// only going to be 1 content/customer node if it exists
			if (it.hasNext()) {
				// Count the number of child nodes to customer
				Node nodeRoot = tajNode.getNode(node);
				Iterable itNode = JcrUtils.getChildNodes(nodeRoot);
				Iterator childNodeIt = itNode.iterator();

				// Count the number of customer child nodes
				while (childNodeIt.hasNext()) {
					childRecs++;
					childNodeIt.next();
				}
				return childRecs;
			} else
				return -1; // node does not exist
		} catch (Exception e) {
			// e.printStackTrace();
			LOG.error("Error in checking nodes existance " + e.getMessage());
		}
		return 0;
	}
}
