package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.DonationRequest;
import com.ihcl.core.servicehandler.processpayment.CCAvenuePaymentHandler;
import com.ihcl.core.services.config.DonationConfigurationService;
import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.Quote;


//RSA imports
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Component(service = Servlet.class,
	immediate = true,
	property = { "sling.servlet.paths=" + "/bin/Donation" ,"sling.servlet.methods={GET,POST}",
	        "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })

public class DonationServlet extends SlingAllMethodsServlet{
	
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(DonationServlet.class);

	@Reference
    private IEmailService emailService;
    
    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private EmailTemplateConfigurationService requestQuoteService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    ByteArrayOutputStream outputStream = null;
    
    @Reference
    DonationConfigurationService config;
	
	@Override
    protected void doPost(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
		String methodName = "doPost";
		response.setHeader("Cache-Control","no-cache, no-store, must-revalidate, max-age=0, s-manage=0");
		response.setHeader("Expires", "0");
        String htmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String returnMessage = "{\"firstName\":\"Mahesh\",\"lastName\":\"Prajapati\",\"email\":\"maheshprajapati1718@gmail.com\",\"city\":\"Thane\",\"pincode\":\"123123\",\"country\":\"India\",\"donation\":\"1\",\"panCardNumber\":\"qwert1234q\",\"address1\":\"Vartaknagar\",\"address2\":\"test\",\"isGDPRCompliance\":true}";
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode((config.publicKey()).getBytes()));
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode((config.privateKey()).getBytes()));
		
        //Public and Private Key generation
//        	try {
//    			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
//    			kpg.initialize(2048);
//    			KeyPair kp = kpg.generateKeyPair();
//    			PublicKey pub = kp.getPublic();(config.publicKey())
//    			PrivateKey prv= kp.getPrivate();config.privateKey())
//        		String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgFGVfrY4jQSoZQWWygZ83roKXWD4YeT2x2p41dGkPixe73rT2IW04glagN2vgoZoHuOPqa5and6kAmK2ujmCHu6D1auJhE2tXP+yLkpSiYMQucDKmCsWMnW9XlC5K7OSL77TXXcfvTvyZcjObEz6LIBRzs6+FqpFbUO9SJEfh6wIDAQAB";
//        	    String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAKAUZV+tjiNBKhlBZbKBnzeugpdYPhh5PbHanjV0aQ+LF7vetPYhbTiCVqA3a+Chmge44+prlqd3qQCYra6OYIe7oPVq4mETa1c/7IuSlKJgxC5wMqYKxYydb1eULkrs5IvvtNddx+9O/JlyM5sTPosgFHOzr4WqkVtQ71IkR+HrAgMBAAECgYAkQLo8kteP0GAyXAcmCAkA2Tql/8wASuTX9ITD4lsws/VqDKO64hMUKyBnJGX/91kkypCDNF5oCsdxZSJgV8owViYWZPnbvEcNqLtqgs7nj1UHuX9S5yYIPGN/mHL6OJJ7sosOd6rqdpg6JRRkAKUV+tmN/7Gh0+GFXM+ug6mgwQJBAO9/+CWpCAVoGxCA+YsTMb82fTOmGYMkZOAfQsvIV2v6DC8eJrSa+c0yCOTa3tirlCkhBfB08f8U2iEPS+Gu3bECQQCrG7O0gYmFL2RX1O+37ovyyHTbst4s4xbLW4jLzbSoimL235lCdIC+fllEEP96wPAiqo6dzmdH8KsGmVozsVRbAkB0ME8AZjp/9Pt8TDXD5LHzo8mlruUdnCBcIo5TMoRG2+3hRe1dHPonNCjgbdZCoyqjsWOiPfnQ2Brigvs7J4xhAkBGRiZUKC92x7QKbqXVgN9xYuq7oIanIM0nz/wq190uq0dh5Qtow7hshC/dSK3kmIEHe8z++tpoLWvQVgM538apAkBoSNfaTkDZhFavuiVl6L8cWCoDcJBItip8wKQhXwHp0O3HLg10OEd14M58ooNfpgt+8D8/8/2OOFaR0HzA+2Dm";
//            	LOG.error("publicKey " + config.publicKey()+" privateKey "+config.privateKey());
//    			LOG.error("publicKeySpec "+publicKeySpec);
//    			LOG.error("privateKeySpec "+privateKeySpec);
//    			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//    			PublicKey pub = keyFactory.generatePublic(publicKeySpec);
//    			LOG.error("Public key generated" + pub);
//    			PrivateKey prv = keyFactory.generatePrivate(privateKeySpec);
//    			LOG.error("Private key generated");
//    			LOG.error("Private key val" + prv);
//    			
//    			LOG.error("pub " + pub);
//    			LOG.error("prv " + prv);
//    			
//    			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//    	        cipher.init(Cipher.ENCRYPT_MODE, pub);
//    	        byte[] encryptData = cipher.doFinal(returnMessage.getBytes());
//    	        
//    	        LOG.error("Text to be encrypted " + returnMessage);
//    	        
//    	        LOG.error("encryptData " + encryptData);
//    	        
//    	        cipher.init(Cipher.DECRYPT_MODE, prv);
//    	        String decryptData = new String(cipher.doFinal(encryptData));
//    	        
//    	        LOG.error("decryptData " + decryptData);
//    	        
//    		} catch (NoSuchAlgorithmException e1) {
//    			LOG.error("NoSuchAlgorithmException");
//    			e1.printStackTrace();
//    		} catch (NoSuchPaddingException e) {
//    			LOG.error("NoSuchPaddingException");
//    			e.printStackTrace();
//    		} catch (InvalidKeyException e) {
//    			LOG.error("InvalidKeyException");
//    			e.printStackTrace();
//    		} catch (IllegalBlockSizeException e) {
//    			LOG.error("IllegalBlockSizeException");
//    			e.printStackTrace();
//    		} catch (BadPaddingException e) {
//    			LOG.error("BadPaddingException");
//    			e.printStackTrace();
//    		} catch (InvalidKeySpecException e) {
//    			LOG.error("InvalidKeySpecException"+ e);
//				e.printStackTrace();
//			}
        
        

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.error("Request URL for Donation is : " + requestUrl);
        LOG.error("Method Entry: " + methodName);
        try {
        	
        	String websiteId = httpRequest.getServerName();
        	//String websiteId = "www.ihcltata.com";
            if (StringUtils.isBlank(websiteId)) {
                websiteId = "tajhotels";
            }

           TajHotelBrandsETCConfigurations etcWebsiteConfig = extractCCAvenueConfigurations(objectMapper, websiteId);
           
           LOG.error("etcWebsiteConfig value: " + etcWebsiteConfig);
           LOG.error("websiteId: " + websiteId);
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
                LOG.error("inside validateUrl: " + methodName);
//                Quote requestobj = setEmailRequest(httpRequest);
//                
                String senderDetailsString = httpRequest.getParameter("senderDetails");
                senderDetailsString = senderDetailsString.replaceAll(" ", "+");
                senderDetailsString = senderDetailsString.replaceAll("  ", "+");
                LOG.debug("senderDetailsString: {}", senderDetailsString.trim());
                //PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode((config.privateKey()).getBytes()));
    			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PrivateKey prv = keyFactory.generatePrivate(privateKeySpec);     
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");    	        
    	        cipher.init(Cipher.DECRYPT_MODE, prv);
                byte[] encryptData = Base64.getDecoder().decode(senderDetailsString.getBytes());
    	        LOG.error("encryptData " + encryptData);
    	        String decryptData = new String(cipher.doFinal(encryptData));
    	        LOG.error("decryptData " + decryptData);
                DonationRequest senderDetails =
                        objectMapper.readValue(decryptData, new TypeReference<DonationRequest>() {
                        });
                
                nullCheckForSenderDetails(senderDetails);
                
                Date d = new Date();
                long diff = d.getTime() - senderDetails.getTimeStamp();
                LOG.error("d.getTime():"+d.getTime()+" - senderDetails.getTimeStamp():"+senderDetails.getTimeStamp()+" < 10000 Diff : " + diff);
                //if(d.getTime() - senderDetails.getTimeStamp() < 10000) {
                	
                	LOG.error("valid request");
              //CCAvenue code starts here
                // Required params for ccavenue call needs to be added
                String giftHamperOrderId = "" + (int) (new Date().getTime() / 1000);
                Cookie domainCookie = new Cookie("session", giftHamperOrderId);
                domainCookie.setMaxAge(60*60);
        		response.addCookie(domainCookie);
        		
                CCAvenueParams ccAvenueParams = injectCCAvenueParmas(etcWebsiteConfig, senderDetails,giftHamperOrderId);

                LOG.info("ccAvenueParams: {}", objectMapper.writeValueAsString(ccAvenueParams));               
                CCAvenuePaymentHandler ccAvenuePaymentHandler = new CCAvenuePaymentHandler();
                objectNode.put("CCAvenueForm", ccAvenuePaymentHandler.getCCAvenueDetails(ccAvenueParams));

                returnMessage = writeJsonToResponse(objectNode, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS,true);
//                }else {
//                	LOG.error("Not a valid request");
//                	String errorMessage = "Could not process the request please try again"; 
//                	returnMessage =  writeJsonToResponse(objectNode, RESPONSE_CODE_FAILURE, errorMessage,false);
//                }
            }
            } catch (Exception e) {
                LOG.error("An error occured while sending Email.", e);

                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                returnMessage =  writeJsonToResponse(objectNode, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE,false);
                
            } finally {
            	response.getWriter().write(returnMessage);
            }
	}
	private Quote setEmailRequest(SlingHttpServletRequest httpRequest) throws Exception {
        String methodName = "setEmailRequest";
        LOG.error("Method Entry: " + methodName);
        Quote requestobj = new Quote();
        requestobj.setVenue("venueName");
        requestobj.setHotelEmailId("hotelEmailId");
        LOG.trace("Method Exit: " + methodName);
        return requestobj;
    }
    private String writeJsonToResponse(final ObjectNode objectNode, String code, String message,boolean status)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        jsonNode.put("status", status);
        jsonNode.put("CCAvenueData", objectNode);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

//        response.getWriter().write(jsonNode.toString());
        
        return jsonNode.toString();
    }
    private HashMap<String, String> getDynamicValuesForEmailMeetings(Quote requestobj,
            HashMap<String, String> dynamicValuemap) {
        dynamicValuemap.put("hotelName", requestobj.getHotelName());
        dynamicValuemap.put("venueName", requestobj.getVenue());
//        dynamicValuemap.put("guestEmailId", requestobj.getEmailId());
//        dynamicValuemap.put("bookingDate", requestobj.getPreferredDate());
//        dynamicValuemap.put("guest", requestobj.getNoOfGuests());
//        dynamicValuemap.put("name", requestobj.getFullName());
        return dynamicValuemap;
    }
    
    private void fetchValueMapFromObj(ModifiableValueMap valueMap, Quote requestobj, String timeSubmitted) {
        valueMap.put("hotelName", requestobj.getHotelName());
        valueMap.put("bookingArrivalDate", requestobj.getArrivalDate());
        valueMap.put("bookingDepartureDate", requestobj.getDepartureDate());
        valueMap.put("guestEmailId", requestobj.getEmailId());
        valueMap.put("guest", requestobj.getNoOfGuests());
        valueMap.put("name", requestobj.getFullName());
        valueMap.put("phoneNumber", requestobj.getMobile());
        valueMap.put("noOfRooms", requestobj.getNoOfRooms());
        valueMap.put("singleRooms", requestobj.getNoOfSingleRooms());
        valueMap.put("doubleRooms", requestobj.getNoOfDoubleRooms());
        valueMap.put("tripleRooms", requestobj.getNoOfTripleRooms());
        valueMap.put("timeOfTheEvent", requestobj.getTimeOfTheEvent());
        valueMap.put("purposeOfTheEvent", requestobj.getPurposeOfTheEvent());
        valueMap.put("tentativeBudget", requestobj.getBudget());
        valueMap.put("specialRequests", requestobj.getSpecialComments());
        valueMap.put("timeSubmitted", timeSubmitted);
    }
    
   //Code to get Time Stamp
//    private String fetchCurrentTimeStamp() {
//        Date date = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSS");
//        String timeStamp = sdf.format(date);
//        LOG.trace("Current Timestamp {}", timeStamp);
//        return timeStamp;
//    }
    
    private TajHotelBrandsETCConfigurations extractCCAvenueConfigurations(ObjectMapper objectMapper, String websiteId)
            throws HotelBookingException, IOException {
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        LOG.info("etcAllBrandsConfig {}",etcAllBrandsConfig);
        LOG.info("etcConfigService.getEtcConfigBean() {}",etcConfigService.getEtcConfigBean());
        if (etcAllBrandsConfig == null) {
        	LOG.info("etcAllBrandsConfig getting null, Please check etc configurations");
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig =
                etcAllBrandsConfig.getEtcConfigBean().get(websiteId);
        LOG.info("individualWebsiteAllConfig {}",individualWebsiteAllConfig);
        if (individualWebsiteAllConfig == null) {
        	LOG.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig =
                individualWebsiteAllConfig.getEtcConfigBean().get("donationCCAvenue");
        LOG.info("individualWebsiteAllConfig.getEtcConfigBean() {}",individualWebsiteAllConfig.getEtcConfigBean());
        LOG.info("etcWebsiteConfig {}",etcWebsiteConfig);
        if (etcWebsiteConfig == null) {
        	LOG.info("etcWebsiteConfig getting null, for configurationType: {}",
        			"donationCCAvenue");
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        String debugString =
                "WebsiteId: " + websiteId + ", configurationType: " + "donationCCAvenue" +
                        ", configurations :-> " + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOG.info(debugString);
        return etcWebsiteConfig;
    }
    private CCAvenueParams injectCCAvenueParmas(TajHotelBrandsETCConfigurations etcWebsiteConfig,DonationRequest senderDetails, String giftHamperOrderId) {

        LOG.debug("Started injectCCAvenueParmas()");
        CCAvenueParams ccAvenueParams = new CCAvenueParams();
        ccAvenueParams.setMerchantId(etcWebsiteConfig.getPaymentMerchantId());
        ccAvenueParams.setCcAvenueAccessKey(etcWebsiteConfig.getCcAvenueAccessKey());
        ccAvenueParams.setCcAvenueWorkingKey(etcWebsiteConfig.getCcAvenueWorkingKey());
        ccAvenueParams.setCcAvenueUrl(etcWebsiteConfig.getCcAvenueUrl());
        ccAvenueParams.setRedirectUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());
        ccAvenueParams.setCancelUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());

        
        ccAvenueParams.setOrderId("DO"+giftHamperOrderId);
        ccAvenueParams.setCurrency(BookingConstants.CCAVENUE_PARAM_CURRENCY);
        ccAvenueParams.setAmount(senderDetails.getDonation());
        ccAvenueParams.setBillingName(senderDetails.getFirstName()+" "+senderDetails.getLastName());
        ccAvenueParams.setBillingZip(senderDetails.getPincode());
        ccAvenueParams.setBillingEmail(senderDetails.getEmail());
        ccAvenueParams.setBillingCity(senderDetails.getCity());
        ccAvenueParams.setBillingState(senderDetails.getAddress2());
        ccAvenueParams.setBillingAddress(senderDetails.getAddress1());
        ccAvenueParams.setBillingCountry(senderDetails.getCountry());
        LOG.debug("Updated ccAvenueParams {}",ccAvenueParams);
        LOG.debug("End of injectCCAvenueParmas()");
        return ccAvenueParams;
    }
    private void nullCheckForSenderDetails(DonationRequest senderDetails) throws HotelBookingException {
        if (StringUtils.isBlank(senderDetails.getFirstName())) {
            throw new HotelBookingException("Sender FirstName not found");
        }
        if (StringUtils.isBlank(senderDetails.getLastName())) {
            throw new HotelBookingException("Sender LastName not found");
        }
        if (StringUtils.isBlank(senderDetails.getEmail())) {
            throw new HotelBookingException("Sender Email not found");
        }
        if (StringUtils.isBlank(senderDetails.getDonation())) {
            throw new HotelBookingException("Sender Donation amount not found");
        }
    }
}