package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.EventBookingRequest;
import com.ihcl.core.servicehandler.processpayment.CCAvenuePaymentHandler;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/eventBookingServlet", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })

public class TajEventBookingServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TajEventBookingServlet.class);

    private static final String SUBACCOUNT_ID = "subAccountId";

    private static final String EVENT_PRICE = "eventPrice";

    private static final String HOTEL_NAME = "hotelName";

    @Reference
    private IEmailService iEmailService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private SearchProvider searchProvider;

    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started TajEventBookingServlet.doPost()");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String returnMessage = "";


        try {
            String websiteId = slingHttpServletRequest.getServerName();
            if (StringUtils.isBlank(websiteId)) {
                websiteId = "tajhotels";
            }
            String paramMapString = objectMapper.writeValueAsString(slingHttpServletRequest.getParameterMap());
            LOGGER.info("ServletRequest.ParameterMap: {}", paramMapString);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = extractCCAvenueConfigurations(objectMapper, websiteId);

            String formDataString = slingHttpServletRequest.getParameter("eventDetailsData");
            LOGGER.debug("formDataString: {}", formDataString);

            EventBookingRequest eventBookingRequest = objectMapper.readValue(formDataString,
                    new TypeReference<EventBookingRequest>() {
                    });

            String hotelIdString = slingHttpServletRequest.getParameter("hotelId");
            LOGGER.debug("hotelIdString: {}", hotelIdString);

            String subAccountIdString = slingHttpServletRequest.getParameter(SUBACCOUNT_ID);
            LOGGER.debug("subAccountId: {}", subAccountIdString);

            if (StringUtils.isBlank(hotelIdString)) {
                throw new HotelBookingException("Issue found while fetching hotelId.");
            }

            String contentRootPath = etcWebsiteConfig.getContentRootPath();

            Map<String, Object> eventDetailsMap = getEventDetailsMap(slingHttpServletRequest, eventBookingRequest,
                    contentRootPath);
            String hotelNameFromResource = (String) eventDetailsMap.get(HOTEL_NAME);
            String subAccountIdFromResource = (String) eventDetailsMap.get(SUBACCOUNT_ID);
            String eventPriceFromResource = (String) eventDetailsMap.get(EVENT_PRICE);

            validateParameters(eventBookingRequest, subAccountIdString, subAccountIdFromResource,
                    eventPriceFromResource);

            CCAvenueParams ccAvenueParams = injectCCAvenueParmas(etcWebsiteConfig, eventBookingRequest, hotelIdString,
                    subAccountIdString, hotelNameFromResource);

            LOGGER.info("ccAvenueParams: {}", objectMapper.writeValueAsString(ccAvenueParams));
            CCAvenuePaymentHandler ccAvenuePaymentHandler = new CCAvenuePaymentHandler();
            objectNode.put("CCAvenueForm", ccAvenuePaymentHandler.getCCAvenueDetails(ccAvenueParams));

            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, true);
            returnMessage = objectNode.toString();

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, false);
            objectNode.put("message", hbe.getMessage());
            LOGGER.debug("TajEventBookingServlet throwing custom exception : {}", hbe.getMessage());
            LOGGER.error("TajEventBookingServlet throwing custom exception : {}", hbe);
            returnMessage = objectNode.toString();

        } catch (Exception e) {
            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, false);
            objectNode.put("message", BookingConstants.EVENT_EXCEPTION_MESSAGE);
            LOGGER.debug("Exception occured while booking event : {}", e.getMessage());
            LOGGER.error("Exception occured while booking event : {}", e);
            returnMessage = objectNode.toString();

        } finally {
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);

            LOGGER.info("Final returnMessage : {}", returnMessage);
            LOGGER.debug("End of TajEventBookingServlet.doPost().");
            LOGGER.info("Returning response to page");

            slingHttpServletResponse.getWriter().write(returnMessage);
            slingHttpServletResponse.getWriter().close();
        }
    }


    /**
     * @param eventBookingRequest
     * @param subAccountIdFromResource
     * @param subAccountIdString
     * @param eventPriceFromResource
     */
    private void validateParameters(EventBookingRequest eventBookingRequest, String subAccountIdString,
            String subAccountIdFromResource, String eventPriceFromResource) throws HotelBookingException {

        if (StringUtils.isBlank(eventBookingRequest.getCustomerName())) {
            throw new HotelBookingException("Customer Name not found");
        }
        if (StringUtils.isBlank(eventBookingRequest.getCustomerEmail())) {
            throw new HotelBookingException("Customer Email not found");
        }
        if (StringUtils.isBlank(eventBookingRequest.getPhoneNumber())) {
            throw new HotelBookingException("Customer PhoneNumber not found");
        }
        if (StringUtils.isBlank(eventBookingRequest.getEventDate())) {
            throw new HotelBookingException("Event Date not found");
        }
        if (StringUtils.isBlank(eventBookingRequest.getCurrency())) {
            throw new HotelBookingException("Curreny Sumbol Not found");
        }
        if (!eventBookingRequest.getCurrency().equalsIgnoreCase(BookingConstants.CCAVENUE_PARAM_CURRENCY)) {
            throw new HotelBookingException("This currency is not allowed for event booking");
        }
        if (StringUtils.isBlank(eventBookingRequest.getTickets())) {
            throw new HotelBookingException("Event Tickets not found");
        }
        if (StringUtils.isBlank(eventBookingRequest.getAvailableSeats())) {
            throw new HotelBookingException("Event AvailableSeats not found");
        }
        if (Integer.valueOf(eventBookingRequest.getAvailableSeats()) < Integer
                .valueOf(eventBookingRequest.getTickets())) {
            throw new HotelBookingException("Required no of seats are not available.");
        }
        if (!StringUtils.isBlank(eventBookingRequest.getTickets())
                && !StringUtils.isBlank(eventBookingRequest.getTotalAmount())) {
            Integer tickets = Integer.parseInt(eventBookingRequest.getTickets());
            Integer amount = Integer.parseInt(eventBookingRequest.getTotalAmount());
            Integer ticketPrice = amount / tickets;
            if (Integer.parseInt(eventPriceFromResource) != ticketPrice) {
                throw new HotelBookingException("Event tickets prices are not matching.");
            }
        }
        if (!StringUtils.equals(subAccountIdString, subAccountIdFromResource)) {
            throw new HotelBookingException("Event Account details are not matching.");
        }
    }


    /**
     * @param objectMapper
     * @param websiteId
     * @return etcWebsiteConfig
     * @throws HotelBookingException
     * @throws IOException
     */
    private TajHotelBrandsETCConfigurations extractCCAvenueConfigurations(ObjectMapper objectMapper, String websiteId)
            throws HotelBookingException, IOException {
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        if (etcAllBrandsConfig == null) {
            LOGGER.info("etcAllBrandsConfig getting null, Please check etc configurations");
            throw new HotelBookingException(ReservationConstants.EVENT_CONFIG_EXCEPTION);
        }
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.EVENT_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(ReservationConstants.EVENT_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.EVENT_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.EVENT_CONFIG_EXCEPTION);
        }
        String debugString = "WebsiteId: " + websiteId + ", configurationType: " + ReservationConstants.EVENT_CCAVENUE
                + ", configurations :-> " + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        return etcWebsiteConfig;
    }

    /**
     * @param slingHttpServletRequest
     * @param request
     * @param contentRootPath
     * @return eventDetailsMap
     * @throws IOException
     */
    private Map<String, Object> getEventDetailsMap(SlingHttpServletRequest slingHttpServletRequest,
            EventBookingRequest request, String contentRootPath) throws IOException {

        Map<String, Object> eventDetailsMap = new HashMap<>();
        try {
            ResourceResolver resolver = slingHttpServletRequest.getResourceResolver();
            HashMap<String, String> query = new HashMap<>();
            query.put("path", contentRootPath);
            query.put("1_property", "sling:resourceType");
            query.put("1_property.value", "tajhotels/components/content/taj-events");
            query.put("2_property", "productName");
            query.put("2_property.value", request.getProductName());
            SearchResult eventResult = searchProvider.getQueryResultWithoutClosing(query);
            List<Hit> resultHit = eventResult.getHits();
            for (Hit hit : resultHit) {
                Resource resultResource = resolver.getResource(hit.getPath());
                if (null != resultResource) {
                    ValueMap eventMap = resultResource.getValueMap();
                    String eventPrice = null != eventMap.get(EVENT_PRICE, String.class)
                            ? eventMap.get(EVENT_PRICE, String.class)
                            : "";
                    String hotelName = null != eventMap.get(HOTEL_NAME, String.class)
                            ? eventMap.get(HOTEL_NAME, String.class)
                            : "";
                    String subAccountId = null != eventMap.get(SUBACCOUNT_ID, String.class)
                            ? eventMap.get(SUBACCOUNT_ID, String.class)
                            : "";
                    eventDetailsMap.put(EVENT_PRICE, eventPrice);
                    eventDetailsMap.put(HOTEL_NAME, hotelName);
                    eventDetailsMap.put(SUBACCOUNT_ID, subAccountId);
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Exception while getting eventPath: {}", e.getMessage());
            LOGGER.error("Exception while getting eventPath: {}", e);
        }
        return eventDetailsMap;
    }

    /**
     * @param etcWebsiteConfig
     * @param shippingDetails
     * @param subAccountID
     * @return ccAvenueParams
     */
    private CCAvenueParams injectCCAvenueParmas(TajHotelBrandsETCConfigurations etcWebsiteConfig,
            EventBookingRequest eventBookingRequest, String hotelId, String subAccountID, String hotelName) {

        LOGGER.debug("Started injectCCAvenueParmas()");
        CCAvenueParams ccAvenueParams = new CCAvenueParams();
        ccAvenueParams.setMerchantId(etcWebsiteConfig.getPaymentMerchantId());
        ccAvenueParams.setCcAvenueAccessKey(etcWebsiteConfig.getCcAvenueAccessKey());
        ccAvenueParams.setCcAvenueWorkingKey(etcWebsiteConfig.getCcAvenueWorkingKey());
        ccAvenueParams.setCcAvenueUrl(etcWebsiteConfig.getCcAvenueUrl());
        ccAvenueParams.setRedirectUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());
        ccAvenueParams.setCancelUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());

        String eventBookingId = "EBO" + (int) (new Date().getTime() / 1000);
        ccAvenueParams.setOrderId(eventBookingId);
        ccAvenueParams.setCurrency(BookingConstants.CCAVENUE_PARAM_CURRENCY);
        ccAvenueParams.setAmount(eventBookingRequest.getTotalAmount());

        ccAvenueParams.setBillingName(eventBookingRequest.getCustomerName());
        ccAvenueParams.setBillingTel(eventBookingRequest.getPhoneNumber());
        ccAvenueParams.setBillingEmail(eventBookingRequest.getCustomerEmail());

        String eventDetails = eventBookingRequest.getEventName() + "," + eventBookingRequest.getEventVenue() + " "
                + eventBookingRequest.getEventDate();
        ccAvenueParams.setMerchantParam1(hotelId);
        ccAvenueParams.setMerchantParam2(eventBookingRequest.getTickets());
        ccAvenueParams.setMerchantParam3(eventDetails);
        ccAvenueParams.setMerchantParam4(eventBookingRequest.getEventPath());
        ccAvenueParams.setMerchantParam5(hotelName);

        ccAvenueParams.setSubAccountId(subAccountID);

        LOGGER.debug("End of injectCCAvenueParmas()");
        return ccAvenueParams;
    }
}
