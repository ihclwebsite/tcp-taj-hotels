package com.ihcl.tajhotels.servlets;

import com.ihcl.core.services.TriggerDAMWorkflow;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.jsoup.Jsoup;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.paths=" + "/bin/renderDam",
		"sling.servlet.methods=GET", "sling.servlet.resourceTypes=services/powerproxy",
		"sling.servlet.selectors=groups", })

public class GenerateDAMRendtion extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 6271281340150971901L;
	@Reference
	private TriggerDAMWorkflow triggerDamWorkflow;

	private static final String workflowName = "/etc/workflow/models/rendition_update/jcr:content/model";
	org.jsoup.nodes.Document jsoupDoc;
	org.jsoup.select.Elements jsoupElements;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		StringBuffer resp= new StringBuffer();
		String url = request.getParameter("url");
		jsoupDoc = Jsoup.connect(url).validateTLSCertificates(false).get();
		jsoupElements = jsoupDoc.select("img");
		for (org.jsoup.nodes.Element element : jsoupElements) {
			String imagePath = element.attr("src");
			if (!imagePath.contains("/style-icons") && !imagePath.contains("pagespeed")
					&& !imagePath.contains("data:image") &&!imagePath.contains("/social-icons")  ) {
				resp.append(triggerDamWorkflow.startWorkflow(workflowName, imagePath));
			}
			
			  response.getWriter().write(resp.toString());
		}
	}

}
