package com.ihcl.tajhotels.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.ClaimFormSubmit;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/claimFormSubmit" })
public class ClaimFormSubmitServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1438264192759342553L;

    private static final Logger LOG = LoggerFactory.getLogger(ClaimFormSubmitServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService claimFormEmailConf;


    @Override
    protected void doPost(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doPost";
        String finalHtmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for cliam form submit servlet is : " + requestUrl);

        LOG.trace("Method Entry: " + methodName);
        try {
            String firstName = httpRequest.getParameter("firstName");
            String lastName = httpRequest.getParameter("lastName");
            String ticNumber = httpRequest.getParameter("ticNumber");
            String email = httpRequest.getParameter("email");
            String preferredModeOfContact = httpRequest.getParameter("preferredModeToContact");
            String bookingId = httpRequest.getParameter("bookingId");
            String hotelName = httpRequest.getParameter("hotelName");
            String arrivalDate = httpRequest.getParameter("arrivalDate");
            String departureDate = httpRequest.getParameter("departureDate");
            String specialTerms = httpRequest.getParameter("specialTerms");
            String bookingRate = httpRequest.getParameter("bookingRate");
            String numberOfRooms = httpRequest.getParameter("numberOfRooms");
            String adultsPerRoom = httpRequest.getParameter("adultsPerRoom");
            String childrenPerRoom = httpRequest.getParameter("childrenPerRoom");
            String currencySelected = httpRequest.getParameter("currencySelected");
            String lowerPrice = httpRequest.getParameter("lowerPrice");
            String lowerPriceCurrency = httpRequest.getParameter("lowerPriceCurrency");
            String approxDate = httpRequest.getParameter("approximateDate");
            String approxTime = httpRequest.getParameter("approximateTime");
            String websiteName = httpRequest.getParameter("websiteName");
            String websiteUrl = httpRequest.getParameter("websiteUrl");
            String additionalInfo = httpRequest.getParameter("additionalInfo");

            StringBuilder complateRequest = new StringBuilder(requestUrl);
            complateRequest.append(firstName);
            complateRequest.append(lastName);
            complateRequest.append(ticNumber);
            complateRequest.append(email);
            complateRequest.append(preferredModeOfContact);
            complateRequest.append(bookingId);
            complateRequest.append(hotelName);
            complateRequest.append(arrivalDate);
            complateRequest.append(departureDate);
            complateRequest.append(specialTerms);
            complateRequest.append(bookingRate);
            complateRequest.append(numberOfRooms);
            complateRequest.append(adultsPerRoom);
            complateRequest.append(childrenPerRoom);
            complateRequest.append(currencySelected);
            complateRequest.append(lowerPrice);
            complateRequest.append(lowerPriceCurrency);
            complateRequest.append(approxDate);
            complateRequest.append(approxTime);
            complateRequest.append(websiteName);
            complateRequest.append(websiteUrl);
            complateRequest.append(additionalInfo);


            if (validateRequest.validateUrl(complateRequest.toString())) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                ClaimFormSubmit requestobj = new ClaimFormSubmit();

                RequestParameter filePart = null;
                File attachmentFile = null;

                if (null != claimFormEmailConf) {
                    requestobj.setFirstName(firstName);
                    requestobj.setLastName(lastName);
                    requestobj.setTicNumber(ticNumber);
                    requestobj.setEmail(email);
                    requestobj.setPreferredModeToContact(preferredModeOfContact);

                    // Booking info
                    requestobj.setBookingId(bookingId);
                    requestobj.setHotelName(hotelName);
                    requestobj.setArrivalDate(arrivalDate);
                    requestobj.setDepartureDate(departureDate);
                    requestobj.setSpecialTerms(specialTerms);
                    requestobj.setBookingRate(bookingRate);
                    requestobj.setNumberOfRooms(numberOfRooms);
                    requestobj.setAdultsPerRoom(adultsPerRoom);
                    requestobj.setChildrenPerRoom(childrenPerRoom);
                    requestobj.setCurrencySelected(currencySelected);

                    // Lower price info
                    requestobj.setLowerPrice(lowerPrice);
                    requestobj.setLowerPriceCurrency(lowerPriceCurrency);
                    requestobj.setApproximateDate(approxDate);
                    requestobj.setApproximateTime(approxTime);
                    requestobj.setWebsiteName(websiteName);
                    requestobj.setWebsiteUrl(websiteUrl);
                    requestobj.setFileName(httpRequest.getParameter("fileName"));

                    requestobj.setScreenshot(httpRequest.getRequestParameter("screenshot"));

                    requestobj.setAdditionalInfo(additionalInfo);

                    // Data from email configuration
                    requestobj.setEmailToAddress(claimFormEmailConf.getEmailToAddressForClaimForm());
                    requestobj.setEmailSubject(claimFormEmailConf.getEmailSubjectForClaimForm());
                    requestobj.setEmailTemplateForBody(claimFormEmailConf.getEmailBodyForClaimForm());

                    LOG.trace("Reading attachment :");
                    String filename = requestobj.getFileName();

                    attachmentFile = new File(filename);
                    filePart = httpRequest.getRequestParameter("screenshot");

                    String htmlString = claimFormEmailConf.getEmailBodyForClaimForm();
                    String unescape = StringEscapeUtils.unescapeHtml4(htmlString);

                    dynamicValuemap = getDynamicValuesForClaimFormEmail(requestobj, dynamicValuemap);
                    requestobj.setDynamicValuesForEmail(dynamicValuemap);
                    finalHtmlTemplate = StringTokenReplacement.replaceToken(requestobj.getDynamicValuesForEmail(),
                            unescape);
                } else if (null == claimFormEmailConf) {
                    LOG.warn("Email Configuration for claim form submission is missing");
                }
                copyToFile(filePart.getInputStream(), attachmentFile);
                boolean isSendEmailSuccess = emailService.sendClaimFormEmail(requestobj, finalHtmlTemplate,
                        attachmentFile);
                if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully ");

                    response.setStatus(HttpServletResponse.SC_OK);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }

        } catch (Exception e) {
            LOG.error("An error occured while Sending Email.", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * Method to convert input stream to File.
     *
     * @param inputStream
     * @param file
     * @throws IOException
     */
    public void copyToFile(InputStream inputStream, File file) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException ioException) {
            LOG.trace("Unable to read input stream and write it to file");
        }
    }

    /**
     * Method to set map fields with token names and values
     *
     * @param dynamicValuemap
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValuesForClaimFormEmail(ClaimFormSubmit requestobj,
            HashMap<String, String> dynamicValuemap) {

        dynamicValuemap.put("firstName", requestobj.getFirstName());
        dynamicValuemap.put("lastName", requestobj.getLastName());
        dynamicValuemap.put("ticNumber", requestobj.getTicNumber());
        dynamicValuemap.put("email", requestobj.getEmail());
        dynamicValuemap.put("preferredModeToContact", requestobj.getPreferredModeToContact());

        // Booking info
        dynamicValuemap.put("bookingId", requestobj.getBookingId());
        dynamicValuemap.put("hotelName", requestobj.getHotelName());
        dynamicValuemap.put("arrivalDate", requestobj.getArrivalDate());
        dynamicValuemap.put("departureDate", requestobj.getDepartureDate());
        dynamicValuemap.put("specialTerms", requestobj.getSpecialTerms());
        dynamicValuemap.put("bookingRate", requestobj.getBookingRate());
        dynamicValuemap.put("numberOfRooms", requestobj.getNumberOfRooms());
        dynamicValuemap.put("adultsPerRoom", requestobj.getAdultsPerRoom());
        dynamicValuemap.put("childrenPerRoom", requestobj.getChildrenPerRoom());
        dynamicValuemap.put("currencySelected", requestobj.getCurrencySelected());

        // Lower price info
        dynamicValuemap.put("lowerPrice", requestobj.getLowerPrice());
        dynamicValuemap.put("lowerPriceCurrency", requestobj.getLowerPriceCurrency());
        dynamicValuemap.put("approximateDate", requestobj.getApproximateDate());
        dynamicValuemap.put("approximateTime", requestobj.getApproximateTime());
        dynamicValuemap.put("websiteName", requestobj.getWebsiteName());
        dynamicValuemap.put("websiteUrl", requestobj.getWebsiteUrl());
        dynamicValuemap.put("additionalInfo", requestobj.getAdditionalInfo());

        return dynamicValuemap;
    }
}
