/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.CartDetails;
import com.ihcl.integration.dto.availability.HotelAvailabilityResponse;
import com.ihcl.integration.dto.availability.HotelDto;
import com.ihcl.integration.dto.availability.RatePlanDto;
import com.ihcl.integration.dto.availability.RoomAvailabilityDto;
import com.ihcl.integration.dto.availability.RoomDto;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/bookCartAvailabilityServlet", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class BookCartAvailabilityServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(BookCartAvailabilityServlet.class);

    @Reference
    IRoomAvailabilityService iRoomAvailabilityService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    @Override
    protected void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
            throws IOException {
        LOGGER.debug("BookCartAvailabilityServlet.doPost() :: Started");
        String returnMessage = "";
        try {
            if (synixsDowntimeConfigurationService.isForcingToSkipCalls()) {
                throw new HotelBookingException(synixsDowntimeConfigurationService.getDowntimeMessage());
            }
            RoomsAvailabilityRequest roomsAvailabilityRequest = new RoomsAvailabilityRequest();
            roomsAvailabilityRequest.setCheckInDate(servletRequest.getParameter("checkInDate"));
            roomsAvailabilityRequest.setCheckOutDate(servletRequest.getParameter("checkOutDate"));
            String hotelIds = servletRequest.getParameter("hotelId");
            if (StringUtils.isNotBlank(hotelIds)) {
                List<String> hotelIdsList = new ArrayList<>();
                hotelIdsList.add(hotelIds);
                roomsAvailabilityRequest.setHotelIds(hotelIdsList);
            }
            String roomDetails = servletRequest.getParameter("roomDetailsList");
            ObjectMapper objectMapper = new ObjectMapper();

            LOGGER.info("request.getRequestURL(): {}", servletRequest.getRequestURL().toString());
            LOGGER.info("request.getRequestURI(): {}", servletRequest.getServerName());

            List<CartDetails> cartDetailsList = objectMapper.readValue(roomDetails,
                    new TypeReference<List<CartDetails>>() {
                    });
            roomsAvailabilityRequest.setCartDetails(cartDetailsList);

            LOGGER.debug("Final roomsAvailabilityRequest : {}",
                    objectMapper.writeValueAsString(roomsAvailabilityRequest));
            List<HotelAvailabilityResponse> hotelAvailabilityResponseList = iRoomAvailabilityService
                    .getCartPageResponse(roomsAvailabilityRequest);
            LOGGER.debug("Received response from Backend, hotelAvailabilityResponseList: {}",
                    objectMapper.writeValueAsString(hotelAvailabilityResponseList));
            returnMessage = buildResponceJSON(roomsAvailabilityRequest, hotelAvailabilityResponseList);

        } catch (HotelBookingException hbe) {
            LOGGER.debug("HotelBookingException occured : {}", hbe.getMessage());
            returnMessage = hbe.getMessage();
        } catch (IOException ioe) {
            LOGGER.debug("Exception occured : {}", ioe.getMessage());
            LOGGER.error("Exception occured : {}", ioe);
            returnMessage = "Service Call Failed. Please try again";
        } finally {
            LOGGER.debug("Final returnMessage : {}", returnMessage);
            servletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            servletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            servletResponse.getWriter().write(returnMessage);
            LOGGER.info("Returning response to page");
            servletResponse.getWriter().close();
        }
    }

    /**
     * @param roomsAvailabilityRequest
     * @param hotelAvailabilityResponseList
     * @return
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    private String buildResponceJSON(RoomsAvailabilityRequest roomsAvailabilityRequest,
            List<HotelAvailabilityResponse> hotelAvailabilityResponseList) throws IOException {
        LOGGER.debug("Started: buildResponceJSON()");
        ObjectMapper objectMapper = new ObjectMapper();
        List<ObjectNode> objectNodeJsonList = new ArrayList<>();
        if (hotelAvailabilityResponseList != null) {
            for (int b = 0; b < hotelAvailabilityResponseList.size(); b++) {

                HotelAvailabilityResponse hotelAvailabilityResponse = hotelAvailabilityResponseList.get(b);
                CartDetails requestCartDetails = roomsAvailabilityRequest.getCartDetails().get(b);
                String loggerString = b + ". roomsAvailabilityRequest: "
                        + objectMapper.writeValueAsString(roomsAvailabilityRequest);
                LOGGER.debug(loggerString);
                loggerString = b + ". hotelAvailabilityResponse: "
                        + objectMapper.writeValueAsString(hotelAvailabilityResponse);
                LOGGER.debug(loggerString);
                loggerString = b + ". requestCartDetails: " + objectMapper.writeValueAsString(requestCartDetails);
                LOGGER.debug(loggerString);
                ObjectNode objectNode = createCustomResponseList(roomsAvailabilityRequest, objectMapper,
                        hotelAvailabilityResponse, requestCartDetails);
                loggerString = b + ". Adding object to List: " + objectMapper.writeValueAsString(objectNode);
                LOGGER.debug(loggerString);
                objectNodeJsonList.add(objectNode);
            }
        } else {
            LOGGER.debug("Maybe Response's requestHotelItList == null");
        }
        LOGGER.debug("Ended: buildResponceJSON()");
        return objectMapper.writeValueAsString(objectNodeJsonList);
    }

    /**
     * @param roomsAvailabilityRequest
     * @param objectMapper
     * @param objectNodeJsonList
     * @param hotelAvailabilityResponse
     * @param requestCartDetails
     * @return
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    private ObjectNode createCustomResponseList(RoomsAvailabilityRequest roomsAvailabilityRequest,
            ObjectMapper objectMapper, HotelAvailabilityResponse hotelAvailabilityResponse,
            CartDetails requestCartDetails) {
        LOGGER.debug("Started: createCustomResponseList()");
        ObjectNode newJsonNode = objectMapper.createObjectNode();
        newJsonNode.put("success", hotelAvailabilityResponse.isSuccess());
        Map<String, HotelDto> hotelDtoMap = hotelAvailabilityResponse.getRoomAvailability();
        List<String> requestHotelItList = roomsAvailabilityRequest.getHotelIds();
        if (requestHotelItList != null && !requestHotelItList.isEmpty()) {
            HotelDto hotelDto = hotelDtoMap.get(requestHotelItList.get(0));
            if (hotelDto.getHotelCode().equalsIgnoreCase(requestHotelItList.get(0))) {
                LOGGER.debug("Ended: createCustomResponseList()");
                return injectHotelDetailsIntoJSONNode(newJsonNode, requestCartDetails, hotelDto);
            } else {
                LOGGER.debug("Response and Request hotelIds are not equals");
                return newJsonNode;
            }
        } else {
            LOGGER.debug("Maybe Response's requestHotelItList == null or empty");
        }
        LOGGER.debug("Ended: createCustomResponseList()");
        return newJsonNode;
    }

    /**
     * @param newJsonNode
     * @param requestCartDetails
     * @param hotelDto
     * @return
     */
    private ObjectNode injectHotelDetailsIntoJSONNode(ObjectNode newJsonNode, CartDetails requestCartDetails,
            HotelDto hotelDto) {

        LOGGER.debug("Started: injectHotelDetailsIntoJSONNode()");
        newJsonNode.put("hotelId", hotelDto.getHotelCode());
        Map<String, RoomAvailabilityDto> roomAvailabilityDtoMap = hotelDto.getAvailableRooms();
        if (roomAvailabilityDtoMap != null && !roomAvailabilityDtoMap.isEmpty()
                && roomAvailabilityDtoMap.get(requestCartDetails.getRoomTypeCode()) != null) {

            RoomAvailabilityDto roomAvailabilityDto = roomAvailabilityDtoMap.get(requestCartDetails.getRoomTypeCode());
            newJsonNode.put("AvailableUnits", roomAvailabilityDto.getAvailableUnits());
            RoomDto roomDto = roomAvailabilityDto.getRoom();
            if (roomDto.getRoomTypeCode().equalsIgnoreCase(requestCartDetails.getRoomTypeCode())) {
                newJsonNode.put("roomTypeCode", roomDto.getRoomTypeCode());
                RatePlanDto ratePlanDto = roomDto.getRatePlans().get(0);
                if (ratePlanDto.getCode().equalsIgnoreCase(requestCartDetails.getRatePlanCode())) {
                    LOGGER.debug("Ended: injectHotelDetailsIntoJSONNode()");
                    return injectRatePlanDetailsIntoJSONNode(newJsonNode, ratePlanDto);
                } else {
                    LOGGER.debug("Response and Request ratePlanCodes are not equals");
                }
            } else {
                LOGGER.debug("Response and Request roomTypeCodes are not equals");
            }
        } else {
            LOGGER.debug(
                    "Maybe Response's roomAvailabilityDtoMap == null or empty or it don't have request's roomTypeCode");
        }

        LOGGER.debug("Ended: injectHotelDetailsIntoJSONNode()");
        return newJsonNode;
    }

    /**
     * @param newJsonNode
     * @param requestCartDetails
     * @param ratePlanDto
     * @return
     */
    private ObjectNode injectRatePlanDetailsIntoJSONNode(ObjectNode newJsonNode, RatePlanDto ratePlanDto) {

        LOGGER.debug("Started: injectRatePlanDetailsIntoJSONNode()");
        newJsonNode.put("ratePlanCode", ratePlanDto.getCode());
        newJsonNode.put("ratePlanTitle", ratePlanDto.getTitle());
        newJsonNode.put("currencyString", ratePlanDto.getCurrencyCode().getCurrencyString());
        newJsonNode.put("averageDiscountedPrice", ratePlanDto.getDiscountedPrice());
        newJsonNode.put("averagePrice", ratePlanDto.getPrice());
        newJsonNode.put("averageTax", ratePlanDto.getAvgTax());
        newJsonNode.put("totalDiscountedPrice", ratePlanDto.getTotalDiscountedPrice());
        newJsonNode.put("totalPrice", ratePlanDto.getTotalPrice());
        newJsonNode.put("totalTax", ratePlanDto.getTax());
        newJsonNode.putPOJO("discountedNightlyRates", ratePlanDto.getDiscountedNightlyRates());
        newJsonNode.putPOJO("nightlyRates", ratePlanDto.getNightlyRates());
        newJsonNode.putPOJO("taxes", ratePlanDto.getTaxes());
        newJsonNode.put("guaranteeAmount", ratePlanDto.getGuaranteeAmount());
        newJsonNode.put("guaranteeCode", ratePlanDto.getGuaranteeCode());
        newJsonNode.put("guaranteeDescription", ratePlanDto.getGuaranteeDescription());
        newJsonNode.put("guaranteePercentage", ratePlanDto.getGuaranteePercentage());
        newJsonNode.put("isguaranteeAmountTaxInclusive", ratePlanDto.isGuaranteeAmountTaxInclusive());

        LOGGER.debug("Ended: injectRatePlanDetailsIntoJSONNode()");
        return newJsonNode;
    }

}
