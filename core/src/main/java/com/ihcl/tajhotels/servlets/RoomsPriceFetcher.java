package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.CHECK_AVAILABILITY_DESTINATION_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.CHECK_AVAILABILITY_DESTINATION_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONArray;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.hotels.BrandDetailsBean;
import com.ihcl.core.hotels.DestinationHotelBean;
import com.ihcl.core.hotels.DestinationHotelMapBean;
import com.ihcl.core.hotels.HotelDetailsBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.CurrencyCodeDto;
import com.ihcl.integration.dto.availability.DestinationPagePriceResponse;
import com.ihcl.integration.dto.availability.HotelDto;
import com.ihcl.integration.dto.availability.RateFilterDto;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.tajhotels.constants.HttpRequestParams;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/fetch/rooms-prices",
                "sling.servlet.paths=" + HttpRequestParams.DESTINATION_HOTEL_RATES })
public class RoomsPriceFetcher extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(RoomsPriceFetcher.class);

    @Reference
    private IRoomAvailabilityService roomAvailabilityService;

    @Reference
    private TajhotelsHotelsBeanService tajhotelsHotelsBeanService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        List<HotelDto> hotels = null;
        LOG.trace("Method Entry: {}", methodName);

        try {

            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
            if (synixsDowntimeConfigurationService.isForcingToSkipCalls()) {

                LOG.info("isForcingToSkipCalls: {}", synixsDowntimeConfigurationService.isForcingToSkipCalls());
                response.setStatus(Integer.valueOf(synixsDowntimeConfigurationService.getServletStatusCode()));
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE,
                        synixsDowntimeConfigurationService.getDowntimeMessage(), hotels);
            } else {
                List<Object> result = fetchAvailabilityFromService(httpRequest, hotels);
                DestinationPagePriceResponse roomAvailabilityResponse = (DestinationPagePriceResponse) result.get(0);
                hotels = (List<HotelDto>) result.get(1);

                LOG.debug("Received room availability response from service as: {}", roomAvailabilityResponse);
                writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, CHECK_AVAILABILITY_DESTINATION_SUCCESS, hotels);
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, CHECK_AVAILABILITY_DESTINATION_FAILURE, hotels);
        }
        LOG.trace("Method Exit: {}", methodName);
    }

    private List<Object> fetchAvailabilityFromService(SlingHttpServletRequest httpRequest, List<HotelDto> hotels)
            throws Exception {

        String methodName = "fetchAvailabilityFromService";
        LOG.trace("Method Entry: {}", methodName);
        ObjectMapper objectMapper = new ObjectMapper();
        RoomsAvailabilityRequest availabilityRequest = new RoomsAvailabilityRequest();

        LOG.debug("SlingHttpServletRequest.getRequestPathInfo().getResourcePath(): {}",
                httpRequest.getRequestPathInfo().getResourcePath());
        Map<String, String> queryParamMap = null;
        String hotelIdString = "";
        int noOfAdults = 0;
        int noOfChildren = 0;
        List<String> hotelIDList = new ArrayList<>();
        String rateFilter = "";
        String promoCode = "";
        if (HttpRequestParams.DESTINATION_HOTEL_RATES.equals(httpRequest.getRequestPathInfo().getResourcePath())) {

            String suffixPath = httpRequest.getRequestPathInfo().getSuffix();
            LOG.debug("Received suffix path info: {}", suffixPath);

            String selectorString = suffixPath.substring(suffixPath.lastIndexOf('/') + 1, suffixPath.length() - 1);
            List<String> suffixes = Arrays.asList(suffixPath.split("/"));

            List<String> eachString = Arrays.asList(selectorString.split("\\."));
            queryParamMap = eachString.stream().map(s -> s.split("=", 2))
                    .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));

            queryParamMap.put(HttpRequestParams.WEB_SITE_ID, suffixes.get(1));
            queryParamMap.put(HttpRequestParams.DESTINATION_TAG, suffixes.get(2));
            queryParamMap.put(HttpRequestParams.CHECK_IN_DATE, suffixes.get(3));
            queryParamMap.put(HttpRequestParams.CHECK_OUT_DATE, suffixes.get(4));
            queryParamMap.put(HttpRequestParams.OCCUPANCY, suffixes.get(5));
            queryParamMap.put(HttpRequestParams.RATE_FILTERS, suffixes.get(6));
            queryParamMap.put(HttpRequestParams.PROMO_CODE, suffixes.get(7));

            String destinationTag = queryParamMap.get(HttpRequestParams.DESTINATION_TAG);
            LOG.debug("destinationTag: {}", destinationTag);
            destinationTag = destinationTag.replace("+", "/");
            LOG.debug("replaceing + with / : {}", destinationTag);
            Map<String, DestinationHotelMapBean> websiteHotelsMap = tajhotelsHotelsBeanService
                    .getWebsiteSpecificHotelsBean().getWebsiteHotelsMap();
            for (Entry<String, DestinationHotelMapBean> destinationHotelMapBeanEntry : websiteHotelsMap.entrySet()) {
                if (destinationHotelMapBeanEntry.getValue() != null
                        && destinationHotelMapBeanEntry.getValue().getDestinationMap() != null) {
                    for (Entry<String, DestinationHotelBean> destinationHotelBeanEntry : destinationHotelMapBeanEntry
                            .getValue().getDestinationMap().entrySet()) {
                        if (destinationHotelBeanEntry.getValue().getDestinationDetails().getDestinationCityTag()
                                .equalsIgnoreCase(destinationTag)) {
                            for (Entry<String, BrandDetailsBean> brandDetailsBeanEntry : destinationHotelBeanEntry
                                    .getValue().getBrandHotels().entrySet()) {
                                List<HotelDetailsBean> hotelDetailsBeanList = brandDetailsBeanEntry.getValue()
                                        .getHotelsBean().getHotelsList();
                                for (HotelDetailsBean hotelDetailsBean : hotelDetailsBeanList) {
                                    hotelIDList.add(hotelDetailsBean.getHotelID());
                                }
                            }
                            break;
                        }
                    }
                }
            }
            if (hotelIDList.isEmpty()) {
                throw new HotelBookingException(
                        "While fetching hotelIds using destinationTag with tajhotelsHotelsBeanService, hotelIDList getting empty");
            }
            LOG.debug("hotelIdsList: {}", objectMapper.writeValueAsString(hotelIDList));

            String roomOccupantsString = '[' + queryParamMap.get(HttpRequestParams.OCCUPANCY).toString() + ']';
            if (!roomOccupantsString.isEmpty()) {
                JSONArray jsonArray = new JSONArray(roomOccupantsString);
                noOfAdults = jsonArray.optInt(0);
                noOfChildren = jsonArray.optInt(1);
            }

            rateFilter = queryParamMap.get(HttpRequestParams.RATE_FILTERS);
            promoCode = queryParamMap.get(HttpRequestParams.PROMO_CODE);

        } else {
            String selectorString = httpRequest.getRequestPathInfo().getSelectorString();
            assert selectorString != null;
            LOG.debug("selectorString: {}", selectorString);
            List<String> eachString = Arrays.asList(selectorString.split("\\."));
            queryParamMap = eachString.stream().map(s -> s.split("=", 2))
                    .collect(Collectors.toMap(a -> a[0], a -> a.length > 1 ? a[1] : ""));
            LOG.debug("queryParamMap: {}", objectMapper.writeValueAsString(queryParamMap));

            String roomDetailString = queryParamMap.get(HttpRequestParams.ROOM_DETAILS);
            LOG.trace("The Room String {}", roomDetailString);
            List<RoomOccupants> roomDetailsList = objectMapper.readValue(roomDetailString,
                    new TypeReference<List<RoomOccupants>>() {
                    });
            LOG.trace("The Roomlist Size: {}", roomDetailsList.size());
            noOfAdults = roomDetailsList.get(0).getNumberOfAdults();
            noOfChildren = roomDetailsList.get(0).getNumberOfChildren();

            hotelIdString = queryParamMap.get(HttpRequestParams.HOTEL_ID_STRING);

            LOG.trace("HotelIdString: {}", hotelIdString);
            hotelIDList = objectMapper.readValue(hotelIdString, new TypeReference<List<String>>() {
            });

            rateFilter = "STD";

        }

        LOG.debug("hotelIdsCount: {}", hotelIDList.size());

        LOG.debug("After setting  rooms noOfAdults: " + noOfAdults + ", noOfChildren: " + noOfChildren);
        RoomOccupants roomOccupants = new RoomOccupants();
        roomOccupants.setNumberOfAdults(noOfAdults);
        roomOccupants.setNumberOfChildren(noOfChildren);
        availabilityRequest.setHotelIdentifiers(hotelIDList);
        availabilityRequest.setRoomOccupants(Collections.singletonList(roomOccupants));
        availabilityRequest.setCheckInDate(queryParamMap.get(HttpRequestParams.CHECK_IN_DATE));
        LOG.trace("The CheckIN: {}", queryParamMap.get(HttpRequestParams.CHECK_IN_DATE));
        availabilityRequest.setCheckOutDate(queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE));
        LOG.trace("The Check OUT: {}", queryParamMap.get(HttpRequestParams.CHECK_OUT_DATE));
        /*
         * availabilityRequest.setQuantity(Integer.parseInt(queryParamMap.get( HttpRequestParams.ROOM_COUNT)));
         */
        availabilityRequest.setQuantity(1);
        availabilityRequest.setDestinationPageListing(true);
        CurrencyCodeDto currencyCodeImpl = new CurrencyCodeDto();
        currencyCodeImpl.setCurrencyString("INR");
        availabilityRequest.setCurrencyCode(currencyCodeImpl);
        if (StringUtils.isNotBlank(rateFilter)) {
            List<RateFilterDto> rateFilters = new ArrayList<>();
            RateFilterDto rateFilterDto = new RateFilterDto();
            rateFilterDto.setCode(rateFilter);
            rateFilters.add(rateFilterDto);
            availabilityRequest.setRateFilters(rateFilters);
        }
        if (StringUtils.isNotBlank(promoCode)) {
            availabilityRequest.setPromoCode(promoCode);
        }
        LOG.trace("The Request from Destination Page: {}", objectMapper.writeValueAsString(availabilityRequest));
        DestinationPagePriceResponse roomAvailabilityResponse = roomAvailabilityService
                .getAveragePriceForDestinationPage(availabilityRequest);

        roomAvailabilityResponse.getDestinationPagePricing();
        LOG.trace("list size must be 2 : {}", roomAvailabilityResponse.getHotelPricesList().size());
        // LOG.trace("The response Value: {}", roomAvailabilityResponse.getHotelPricesList().get(0).getHotelCode());

        hotels = roomAvailabilityResponse.getHotelPricesList();
        for (HotelDto hotelinfo : hotels) {
            LOG.trace("The response Value hotelId: {}", hotelinfo.getHotelCode());
            LOG.trace("The response Value price : {}", hotelinfo.getLowestPrice());
        }

        LOG.trace("Method Exit: {}", methodName);

        List<Object> result = new ArrayList<>();
        result.add(roomAvailabilityResponse);
        result.add(hotels);

        return result;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @param hotels
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message,
            List<HotelDto> hotels) throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: {}", methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        if (hotels != null) {
            String arrayToJson = mapper.writeValueAsString(hotels);
            jsonNode.put("hotelDetails", arrayToJson);
        }
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: {}", jsonString);
        LOG.trace("Method Exit: {}", methodName);

        response.getWriter().write(jsonNode.toString());
    }
}
