package com.ihcl.tajhotels.servlets;

import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.search.SearchProvider;


@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/genericVenueSearch", "sling.servlet.methods=GET" })
public class GenericVenueSearchServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 9022344985091162836L;

    private static final Logger LOG = LoggerFactory.getLogger(GenericVenueSearchServlet.class);

    @Reference
    SearchProvider searchProvider;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        ResourceResolver resolver = null;
        try {
            resolver = request.getResourceResolver();
            HashMap<String, HashMap<String, Object>> categorizedResponse = buildSearchResults(resolver, request);
            ObjectMapper objectMapper = new ObjectMapper();
            response.setContentType("application/json");
            response.getWriter().write(objectMapper.writeValueAsString(categorizedResponse));
        } catch (Exception e) {
            LOG.error("Exception occured while getting the resourceResolver :: {}", e.getMessage());
            LOG.debug("Exception occured while getting the resourceResolver :: {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
    }

    private HashMap<String, HashMap<String, Object>> buildSearchResults(ResourceResolver resolver,
            SlingHttpServletRequest request) {
        String searchKey = "*";
        SearchResult websiteSearchResults = getSearchResult(searchKey);
        LOG.debug("Hit Count :: {}", websiteSearchResults.getHits().size());
        HashMap<String, HashMap<String, Object>> mapNode = buildDestinationResultsNode(websiteSearchResults, resolver);
        return mapNode;
    }


    private HashMap<String, HashMap<String, Object>> buildDestinationResultsNode(SearchResult searchResult,
            ResourceResolver resolver) {


        HashMap<String, HashMap<String, Object>> resultSet = new HashMap<String, HashMap<String, Object>>();


        String destinationName = null;
        String hotelName = null;
        String hotelEmail = null;
        String destinationMails = null;

        for (Hit hit : searchResult.getHits()) {
            try {

                String hotelPath = hit.getPath().split("/meetings-and-events/")[0];

                Resource hotelResource = resolver.getResource(hotelPath);

                if (null != hotelResource) {
                    Resource destinationResource = hotelResource.getParent().getParent().getChild("jcr:content");
                    destinationMails = getAllHotelEmails(hotelResource);
                    if (null != destinationResource) {
                        ValueMap vm = destinationResource.adaptTo(ValueMap.class);
                        if (null != vm) {
                            destinationName = vm.get("jcr:title", String.class);
                        }
                    }
                    Resource resultResource = resolver.getResource(hotelPath + "/jcr:content");
                    if (null != resultResource) {
                        ValueMap vm = resultResource.adaptTo(ValueMap.class);
                        if (null != vm) {
                            hotelName = vm.get("hotelName", String.class);
                            hotelEmail = null != vm.get("requestQuoteEmail", String.class)
                                    ? vm.get("requestQuoteEmail", String.class) : vm.get("hotelEmail", String.class);
                        }
                    }
                }

                if (resultSet.containsKey(destinationName)) {
                    HashMap<String, Object> vm = resultSet.get(destinationName);
                    HashMap<String, String> hotelMap = (HashMap<String, String>) vm.get("hotels");
                    hotelMap.put(hotelName, hotelEmail);
                    vm.replace("hotels", hotelMap);
                    resultSet.replace(destinationName, vm);
                } else {
                    HashMap<String, Object> vm = new HashMap<String, Object>();
                    HashMap<String, String> hotelMap = new HashMap<String, String>();
                    hotelMap.put(hotelName, hotelEmail);
                    vm.put("destinationMail", destinationMails);
                    vm.put("hotels", hotelMap);
                    resultSet.put(destinationName, vm);
                }
            } catch (Exception e) {
                LOG.error("Exception found while getting the destination details :: {}", e.getMessage());
                LOG.debug("Exception found while getting the destination details :: {}", e);
            }
        }
        return resultSet;
    }

    private String getAllHotelEmails(Resource hotelResource) {
        String hotelEmail = "";
        try {
            Resource destinationResource = hotelResource.getParent().getParent();
            if (null != destinationResource) {
                Iterator<Resource> brandItr = destinationResource.listChildren();
                while (brandItr.hasNext()) {
                    Resource brandResource = brandItr.next();
                    if (null != brandResource && !brandResource.getPath().contains("jcr:content")) {
                        Iterator<Resource> itr = brandResource.listChildren();
                        while (itr.hasNext()) {
                            Resource currentHotelResource = itr.next();
                            if (null != currentHotelResource && currentHotelResource.hasChildren()) {
                                Resource hotelChild = currentHotelResource.getChild("jcr:content");
                                if (null != hotelChild) {
                                    ValueMap vm = hotelChild.adaptTo(ValueMap.class);
                                    if (null != vm) {
                                        String currentHotelEmail = null != vm.get("requestQuoteEmail", String.class)
                                                ? vm.get("requestQuoteEmail", String.class)
                                                : vm.get("hotelEmail", String.class);
                                        if (null != currentHotelEmail && !hotelEmail.contains(currentHotelEmail)) {
                                            hotelEmail = hotelEmail.concat(currentHotelEmail + ",");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error while getting emails : {}", e.getMessage());
            LOG.debug("Error while getting emails : {}", e);
        }
        return hotelEmail;
    }

    private SearchResult getSearchResult(String searchKey) {
        HashMap<String, String> predicateMap = new HashMap<>();

        try {
            predicateMap.put("fulltext", searchKey);
            predicateMap.put("1_group.1_path", "/content/tajhotels/en-in/our-hotels");
            predicateMap.put("1_group.2_path", "/content/vivanta/en-in/our-hotels");
            predicateMap.put("1_group.3_path", "/content/seleqtions/en-in/our-hotels");
            predicateMap.put("1_group.p.or", "true");
            predicateMap.put("2_property", "sling:resourceType");
            predicateMap.put("2_property.value", "tajhotels/components/content/meeting-event-details");
            predicateMap.put("p.limit", "-1");

        } catch (Exception e) {
            LOG.error("Error while searching for text= {} --> {}", searchKey, e.getMessage());
            LOG.debug("Error while searching for text= {} --> {}", searchKey, e);
        }
        return searchProvider.getQueryResult(predicateMap);
    }
}
