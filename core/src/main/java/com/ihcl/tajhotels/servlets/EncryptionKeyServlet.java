package com.ihcl.tajhotels.servlets;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.ihcl.core.services.config.DonationConfigurationService;
import com.ihcl.loyalty.dto.IUserDetails;
import com.ihcl.loyalty.services.IUserDetailsFilterService;
import com.ihcl.loyalty.services.IUserDetailsService;
import com.ihcl.tajhotels.signin.api.IUserAccount;
import com.ihcl.tajhotels.signin.dto.EmailResponseValue;
import com.ihcl.tajhotels.signin.dto.ForgotPasswordInput;
import com.ihcl.tajhotels.signin.dto.ForgotPasswordResponse;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static com.ihcl.integration.Constants.REQUEST_KEY_MEMBERSHIP_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;

@Component(service = Servlet.class, immediate = true, property = {
        "sling.servlet.paths=" + "/bin/Session", "sling.servlet.methods=" + HttpConstants.METHOD_GET
})
public class EncryptionKeyServlet extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(EncryptionKeyServlet.class);

    @Reference
    private IUserAccount userAccount;

    @Reference
    private IUserDetailsService userDetailsService;

    @Reference
    private IUserDetailsFilterService userDetailsFilterService;
    
    @Reference
    DonationConfigurationService config;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException,
            IOException {
        String methodName = "doGet";
        
        String publicKey = config.publicKey();
        LOG.trace("Method Entry: " + methodName);
        JSONObject jsonNode = new JSONObject();
        
        try {
			jsonNode.put("sessionToken", publicKey);
			response.setContentType("application/json");
	        response.getWriter().write(jsonNode.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			response.sendError(500, e.getMessage());
			e.printStackTrace();
		}
        LOG.trace("Method Exit: " + methodName);
    }

}
