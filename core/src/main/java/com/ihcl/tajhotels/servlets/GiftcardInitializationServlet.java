/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.GiftcardRequest;
import com.ihcl.core.servicehandler.giftcard.IGiftcardRequestHandler;
import com.ihcl.core.servicehandler.processpayment.CCAvenuePaymentHandler;
import com.ihcl.core.util.IGiftcardBatchNumberFetcher;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.integration.giftcard.config.ConfigurationService;
import com.ihcl.integration.giftcard.dto.ApiWebProperties;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;

/**
 * @author Vijay Chikkani
 *
 */

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/giftcardInitializeServlet", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class GiftcardInitializationServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftcardInitializationServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private ConfigurationService configurationService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private IGiftcardRequestHandler iGiftcardRequestHandler;

    @Reference
    private IGiftcardBatchNumberFetcher iGiftcardBatchNumberFetcher;

    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started GiftcardInitializationServlet.doPost().");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(slingHttpServletRequest);
        LOGGER.trace("Request URL for contact us in taj air servlet is : {}", requestUrl);

        try {

            String giftcardRequestString = slingHttpServletRequest.getParameter("giftcardCustomerDetails");
            LOGGER.trace("giftcardRequestString: {}", giftcardRequestString);
            if (null != giftcardRequestString) {
                requestUrl = requestUrl.concat(giftcardRequestString);
            }

            if (validateRequest.validateUrl(requestUrl)) {
                LOGGER.info("request.getRequestURL(): {}", slingHttpServletRequest.getRequestURL());
                LOGGER.info("request.getServerName(): {}", slingHttpServletRequest.getServerName());

                String websiteId = slingHttpServletRequest.getServerName();
                if (StringUtils.isBlank(websiteId)) {
                    websiteId = "tajhotels";
                }

                GiftcardRequest giftcardRequest = captureRequestParams(slingHttpServletRequest, objectMapper);
                ApiWebProperties apiWebProperties = setApiWebProperties(slingHttpServletRequest, configurationService);

                String apiWebPropertiesString = objectMapper.writeValueAsString(apiWebProperties);
                LOGGER.debug("apiWebPropertiesString: {}", apiWebPropertiesString);

                CCAvenuePaymentHandler ccAvenuePaymentHandler = new CCAvenuePaymentHandler();

                TajHotelBrandsETCConfigurations etcWebsiteConfig = fetchEtcWebsiteConfig(objectMapper, websiteId);

                CCAvenueParams ccAvenueParams = injectCCAvenueParmas(etcWebsiteConfig, giftcardRequest);
                LOGGER.info("ccAvenueParams: {}", objectMapper.writeValueAsString(ccAvenueParams));

                sendingMailAfterPayment(ccAvenueParams, giftcardRequest, configurationService);

                objectNode.put("CCAvenueForm", ccAvenuePaymentHandler.getCCAvenueDetails(ccAvenueParams));

                objectNode.put("status", true);
                String returnMessage = objectNode.toString();
                LOGGER.info("Final returnMessage : {}", returnMessage);
                slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
                slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);

                LOGGER.debug("End of GiftcardInitializationServlet.doPost().");
                LOGGER.info("Returning response to page");
                slingHttpServletResponse.getWriter().write(returnMessage);
            } else {
                LOGGER.info("This request contains cross site attck characters");
                slingHttpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }

        } catch (HotelBookingException hbe) {
            objectNode.put("status", false);
            objectNode.put("message", hbe.getMessage());
            LOGGER.debug("Throwing Initilizing giftcard exception : {}", hbe.getMessage());
            LOGGER.error("Throwing Initilizing giftcard exception : {}", hbe);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            slingHttpServletResponse.getWriter().write(objectNode.toString());

        } catch (Exception e) {

            objectNode.put("status", false);
            objectNode.put("message", BookingConstants.GIFTCARD_EXCEPTION_MESSAGE);
            LOGGER.debug("Exception occured while Initilizing giftcard : {}", e.getMessage());
            LOGGER.error("Exception occured while Initilizing giftcard : {}", e);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            slingHttpServletResponse.getWriter().write(objectNode.toString());

        } finally {
            LOGGER.debug("End of GiftcardInitializationServlet.doPost().");
            slingHttpServletResponse.getWriter().close();
        }
    }

    /**
     * @param objectMapper
     * @param websiteId
     * @return
     * @throws HotelBookingException
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    protected TajHotelBrandsETCConfigurations fetchEtcWebsiteConfig(ObjectMapper objectMapper, String websiteId)
            throws HotelBookingException, IOException, JsonGenerationException, JsonMappingException {
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        if (etcAllBrandsConfig == null) {
            LOGGER.info("etcAllBrandsConfig getting null, Please check etc configurations");
            throw new HotelBookingException(ReservationConstants.GIFTCARD_CONFIG_EXCEPTION);
        }
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFTCARD_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(ReservationConstants.GIFTCARD_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.GIFTCARD_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.GIFTCARD_CONFIG_EXCEPTION);
        }
        String debugString = "WebsiteId: " + websiteId + ", configurationType: "
                + ReservationConstants.GIFTCARD_CCAVENUE + ", configurations :-> "
                + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        return etcWebsiteConfig;
    }

    private ApiWebProperties setApiWebProperties(SlingHttpServletRequest shServletRequest,
            ConfigurationService configurationService)
            throws RepositoryException, PersistenceException, LoginException {
        LOGGER.debug("Started settingApiWebProperties()");
        ApiWebProperties apiWebProperties = new ApiWebProperties();
        apiWebProperties.setTerminalId(configurationService.getTerminalId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        apiWebProperties.setDateAtClient(sdf.format(new Date()));
        apiWebProperties.setUserName(configurationService.getUserName());
        apiWebProperties.setPassword(configurationService.getPassword());
        apiWebProperties.setForwardingEntityId(configurationService.getForwardingEntityId());
        apiWebProperties.setForwardingEntityPassword(configurationService.getForwardingEntityPassword());
        apiWebProperties.setCurrentBatchNumber(Integer
                .valueOf(iGiftcardBatchNumberFetcher.getCurrentBatchNumber(shServletRequest, configurationService)));
        apiWebProperties.setAcquirerId(null);
        apiWebProperties.setMerchantOutletName(configurationService.getMerchantOutletName());
        apiWebProperties.setOrganizationName(configurationService.getOrganizationName());
        apiWebProperties.setPosEntryMode(2);
        apiWebProperties.setPosName(null);
        apiWebProperties.setPosTypeId(1);
        apiWebProperties.setTermAppVersion(null);

        LOGGER.debug("End of settingApiWebProperties()");
        return apiWebProperties;
    }

    /**
     * @param slingHttpServletRequest
     * @param objectMapper
     * @throws HotelBookingException
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    private GiftcardRequest captureRequestParams(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper) throws HotelBookingException, IOException {

        LOGGER.debug("Starting captureRequestParams()");
        String giftcardRequestString = slingHttpServletRequest.getParameter("giftcardCustomerDetails");
        LOGGER.debug("giftcardRequestString: {}", giftcardRequestString);

        GiftcardRequest giftcardRequestObj = objectMapper.readValue(giftcardRequestString,
                new TypeReference<GiftcardRequest>() {
                });
        checkManditaryData(giftcardRequestObj);

        LOGGER.debug("End of captureRequestParams()");
        return giftcardRequestObj;
    }

    /**
     * @param giftcardRequestObj
     * @throws HotelBookingException
     */
    private void checkManditaryData(GiftcardRequest giftcardRequestObj) throws HotelBookingException {

        LOGGER.debug("Starting checkManditaryData()");
        if (StringUtils.isBlank(giftcardRequestObj.getTitle())) {
            throw new HotelBookingException("Please enter valid title.");
        } else if (StringUtils.isBlank(giftcardRequestObj.getFirstName())) {
            throw new HotelBookingException("Please enter valid first name.");
        } else if (StringUtils.isBlank(giftcardRequestObj.getLastName())) {
            throw new HotelBookingException("Please enter valid last name.");
        } else if (StringUtils.isBlank(giftcardRequestObj.getEmail())) {
            throw new HotelBookingException("Please enter valid email.");
        } else if (StringUtils.isBlank(giftcardRequestObj.getCurrenyType())
                || !giftcardRequestObj.getCurrenyType().equalsIgnoreCase(BookingConstants.CCAVENUE_PARAM_CURRENCY)) {
            throw new HotelBookingException("Currency value should be INR only");
        } else if (StringUtils.isBlank(giftcardRequestObj.getAmount())) {
            throw new HotelBookingException("Please enter valid amount.");
        } else if ((int) Float.parseFloat(giftcardRequestObj.getAmount()) < 1000) {
            throw new HotelBookingException("Minimum card value should be more than Rs.1000");
        } else if ((int) Float.parseFloat(giftcardRequestObj.getAmount()) > 100000) {
            throw new HotelBookingException("Maximum card value should be less than Rs.100000");
        } else if (StringUtils.isBlank(giftcardRequestObj.getPayableAmount())) {
            throw new HotelBookingException("Please enter valid payable amount.");
        } else if (StringUtils.isBlank(giftcardRequestObj.getItineraryNumber())) {
            throw new HotelBookingException("Itinerary number not found");
        }

        LOGGER.debug("End of checkManditaryData()");
    }

    /**
     * @param ccAvenueParams
     * @param giftcardRequest
     * @param configurationService2
     *
     */
    private void sendingMailAfterPayment(CCAvenueParams ccAvenueParams, GiftcardRequest giftcardRequest,
            ConfigurationService configurationService) {

        LOGGER.debug("Starting sendingMailAfterPayment()");
        String eCardPaymentMailBody = configurationService.getEGiftcardPaymentMailBodyTemplate();
        LocalDate localDate = LocalDate.now();

        eCardPaymentMailBody = eCardPaymentMailBody.replace("$GCORDERNO$", ccAvenueParams.getOrderId());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$CURRENTDATE$",
                DateTimeFormatter.ofPattern("dd MMM yyyy").format(localDate));
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$BUYERNAME$", ccAvenueParams.getBillingName());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$BUYEREMAIL$", ccAvenueParams.getBillingEmail());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$BUYERPHONENO$", ccAvenueParams.getBillingTel());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$GCEACHUNITCOST$", giftcardRequest.getAmount());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$GCQTY$", "1");
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$EACHGCAMOUNT$ ", giftcardRequest.getAmount());
        eCardPaymentMailBody = eCardPaymentMailBody.replace("$ALLGCAMOUNT$", giftcardRequest.getAmount());

        boolean status = iEmailService.sendEMail(configurationService.getEGiftcardPaymentMailSubject(),
                eCardPaymentMailBody, ccAvenueParams.getBillingEmail(), null, null, null);
        if (status) {
            LOGGER.info("eGiftcard payment status mail sent. Status: {}", status);
        } else {
            LOGGER.info("Issue with mailing eGiftcard payment. Status: {}", status);
        }
        LOGGER.debug("End of sendingMailAfterPayment()");
    }

    /**
     * @param etcWebsiteConfig
     * @param giftcardRequest
     * @return ccAvenueParams
     */
    private CCAvenueParams injectCCAvenueParmas(TajHotelBrandsETCConfigurations etcWebsiteConfig,
            GiftcardRequest giftcardRequest) {

        LOGGER.debug("Started injectCCAvenueParmas()");

        CCAvenueParams ccAvenueParams = new CCAvenueParams();
        ccAvenueParams.setMerchantId(etcWebsiteConfig.getPaymentMerchantId());
        ccAvenueParams.setCcAvenueAccessKey(etcWebsiteConfig.getCcAvenueAccessKey());
        ccAvenueParams.setCcAvenueWorkingKey(etcWebsiteConfig.getCcAvenueWorkingKey());
        ccAvenueParams.setCcAvenueUrl(etcWebsiteConfig.getCcAvenueUrl());
        ccAvenueParams.setRedirectUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());
        ccAvenueParams.setCancelUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());

        String giftcardOrderId = "GCO" + (int) (new Date().getTime() / 1000);
        ccAvenueParams.setOrderId(giftcardOrderId);
        ccAvenueParams.setCurrency(BookingConstants.CCAVENUE_PARAM_CURRENCY);
        ccAvenueParams.setAmount(giftcardRequest.getPayableAmount());

        ccAvenueParams.setBillingName(giftcardRequest.getFirstName() + " " + giftcardRequest.getLastName());
        ccAvenueParams.setBillingTel(giftcardRequest.getPhoneNumber());
        ccAvenueParams.setBillingEmail(giftcardRequest.getEmail());

        ccAvenueParams.setMerchantParam1(giftcardRequest.getItineraryNumber());
        ccAvenueParams.setMerchantParam2(BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        ccAvenueParams.setMerchantParam3(BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        ccAvenueParams.setMerchantParam4(BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        ccAvenueParams.setMerchantParam5(
                "GCPA-" + giftcardRequest.getPayableAmount() + "_GCA-" + giftcardRequest.getAmount());

        ccAvenueParams.setSubAccountId(BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);

        LOGGER.debug("End of injectCCAvenueParmas()");
        return ccAvenueParams;
    }

}
