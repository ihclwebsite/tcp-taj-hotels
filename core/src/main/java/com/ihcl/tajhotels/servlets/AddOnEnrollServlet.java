package com.ihcl.tajhotels.servlets;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONException;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.integration.exception.ServiceException;
import com.ihcl.integration.rest.service.api.IRestService;
import com.ihcl.loyalty.dto.AddonEnrollInput;
import com.ihcl.loyalty.dto.AddonEnrollResponse;
import com.ihcl.loyalty.services.IAddonService;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.integration.dto.details.OpelBookingResponse;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "= Servlet to invoke add on enrollment",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/addon/enroll" })
public class AddOnEnrollServlet extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AddOnEnrollServlet.class);
    private static final long serialVersionUID = -2969119479209546052L;
    private String serviceURL = "https://sandbox.juspay.in";
    private final String apiKey = "4C2884A74544E68915C7A244211AE4";

    @Reference
    private ConfigurationService conf;

    @Reference
    private IAddonService addonService;

    private ObjectMapper objectMapper;
    
    @Reference
    private IRestService restService;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        objectMapper = new ObjectMapper();
        objectMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
    	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String orderId = request.getParameter("order_id");
        LOG.info("orderId" +orderId);
        
        String status = request.getParameter("status");
        
        RequestPathInfo requestPathInfo = request.getRequestPathInfo();
        String resourcePath = requestPathInfo.getResourcePath();

        String detailsresponse;	
     /*   String encResp = request.getParameter(CCAVENUE_RESPONSE_PARAM_NAME);
        String purchaseCCAvenueWorkingKey = conf.getPurchaseCCAvenueWorkingKey();
        if (purchaseCCAvenueWorkingKey.equals("")) {
            purchaseCCAvenueWorkingKey = conf.getCCAvenueWorkingKey();
        }
        AesCryptUtil aesUtil = new AesCryptUtil(purchaseCCAvenueWorkingKey);
        String decResp = aesUtil.decrypt(encResp);
        URLMapUtil urlMapUtil = new URLMapUtil();
        Map<String, List<String>> paymentDetailsMap = urlMapUtil.splitQuery(decResp); */
       // BookingConfirmServlet bookEpicureConfirm = new BookingConfirmServlet();
        if(status.equalsIgnoreCase("CHARGED")) {
        	LOG.info("inside if condtion for passing status charged");
    		detailsresponse = fetchPaymentJusPay(serviceURL, orderId);
    		/* URLMapUtil urlMapUtil = new URLMapUtil();
             Map<String, List<String>> juspayResponseMap = urlMapUtil.splitQuery(detailsresponse);*/
	 if(detailsresponse != null){
        try {
        	LOG.info("Inside try block");
        	OpelBookingResponse opelBookingResponse = objectMapper.readValue(detailsresponse, OpelBookingResponse.class);
            String statusFromOpelResp = opelBookingResponse.getStatus();
            String statusIdFromOpelRsp = opelBookingResponse.getStatus_id();
           
         //   LOG.info("juspayResponseMap "+juspayResponseMap);
            LOG.info("status " + opelBookingResponse.getStatus());
        	LOG.info("status_id " + opelBookingResponse.getStatus_id());
        	if(opelBookingResponse.getStatus_id() != null && 
        			Integer.compare(Integer.parseInt(opelBookingResponse.getStatus_id()) ,21) == 0 && 
					opelBookingResponse.getStatus() != null &&
        			opelBookingResponse.getStatus().equalsIgnoreCase("CHARGED")) {
        		LOG.info("Inside if block");
        		AddonEnrollResponse addonEnrollResponse = addonService
                        .enrollAddon(buildServiceInput(opelBookingResponse));
        		String parsedAddonResp = objectMapper.writeValueAsString(addonEnrollResponse);
        		 response.setContentType(BookingConstants.CONTENT_TYPE_HTML);
        		 String escapedAddonResp = StringEscapeUtils.escapeEcmaScript(parsedAddonResp);
        		 buildFormOutput(response, escapedAddonResp, statusFromOpelResp, opelBookingResponse.getPayment_gateway_response().getRrn());
        	}
        	else {
        		LOG.info("Inside else condition");
        		buildFormOutput(response, null, status, null);
        	}
    	    
    		/*commenting epicure changes for opel integration*/
           /* String status = paymentDetailsMap.get(CCAVENUE_RESPONSE_ORDER_STATUS).get(0);
            if (status.equals(CCAVENUE_RESPONSE_ORDER_STATUS_VALUE)) {
                AddonEnrollResponse addonEnrollResponse = addonService
                        .enrollAddon(buildServiceInput(paymentDetailsMap));
                String parsedAddonResp = objectMapper.writeValueAsString(addonEnrollResponse);
                response.setContentType(BookingConstants.CONTENT_TYPE_HTML);
                String escapedAddonResp = StringEscapeUtils.escapeEcmaScript(parsedAddonResp);
                buildFormOutput(response, escapedAddonResp, status, paymentDetailsMap.get(CCAVENUE_RESPONSE_ID).get(0));
            } else {
                buildFormOutput(response, null, status, null);
            }*/
        } catch (ServiceException | JSONException e) {
            LOG.info(e.getMessage(), e);
            response.sendError(500, e.getMessage());
        }
	 }
    }
    }

    private AddonEnrollInput buildServiceInput(OpelBookingResponse opelBookingResponse) throws JSONException{
    	LOG.info("Inside buildServiceInput method");
       AddonEnrollInput addonEnrollInput = new AddonEnrollInput();
       String paymentMethod = "";
       if(opelBookingResponse.getPayment_method_type() != null) {
       	
       	if(opelBookingResponse.getPayment_method_type().equalsIgnoreCase("CARD")) {
           	paymentMethod = "Credit Card";
           }else if(opelBookingResponse.getPayment_method_type().equalsIgnoreCase("NB")) {
           	paymentMethod = "Net Banking";
           }else if(opelBookingResponse.getPayment_method_type().equalsIgnoreCase("UPI") || opelBookingResponse.getPayment_method_type().equalsIgnoreCase("WALLET")) {
           	paymentMethod = "Partner Payment";
           }
       }
       addonEnrollInput.setAddOnName(opelBookingResponse.getUdf9());
        LOG.info("AddonNAme "+addonEnrollInput.getAddOnName());
        addonEnrollInput.setMembershipId(opelBookingResponse.getUdf7());
        LOG.info("MembershipID "+addonEnrollInput.getMembershipId());  
        addonEnrollInput.setPaymentMethod(paymentMethod);
        LOG.info("paymentMethod "+addonEnrollInput.getPaymentMethod());
        addonEnrollInput.setAddress1(opelBookingResponse.getUdf1());
        LOG.info("Address "+addonEnrollInput.getAddress1());
        addonEnrollInput.setCity(opelBookingResponse.getUdf4());
        LOG.info("City "+addonEnrollInput.getCity());
        addonEnrollInput.setState(opelBookingResponse.getUdf5());
        LOG.info("State "+addonEnrollInput.getState());
        addonEnrollInput.setCountry(opelBookingResponse.getUdf3());
        LOG.info("Country "+addonEnrollInput.getCountry());
        addonEnrollInput.setZipCode(opelBookingResponse.getUdf6());
        LOG.info("Pincode "+addonEnrollInput.getZipCode());
        addonEnrollInput.setPaymentId(opelBookingResponse.getPayment_gateway_response().getRrn());
        LOG.info("PAymentID "+addonEnrollInput.getPaymentId());
        /*  addonEnrollInput.setPhoneNum(opelBookingResponse.getCustomer_phone());
        LOG.info("Phone number "+addonEnrollInput.getPhoneNum());
       addonEnrollInput.setAddOnActivity(opelBookingResponse.getUdf8());
        LOG.info("Add on activity "+addonEnrollInput.getAddOnActivity());*/
       /* addonEnrollInput.setAddOnName(responseMap.get(CCAVENUE_MERCH2_NAME).get(0));
        addonEnrollInput.setMembershipId(responseMap.get(CCAVENUE_MERCH1_NAME).get(0));
        addonEnrollInput.setPaymentMethod(responseMap.get(CCAVENUE_RESPONSE_PAY_MODE).get(0));
        addonEnrollInput.setAddress1(responseMap.get(CCAVENUE_BILLING_ADDRESS_NAME).get(0));
        addonEnrollInput.setCity(responseMap.get(CCAVENUE_BILLING_CITY_NAME).get(0));
        addonEnrollInput.setState(responseMap.get(CCAVENUE_BILLING_STATE_NAME).get(0));
        addonEnrollInput.setCountry(responseMap.get(CCAVENUE_BILLING_COUNTRY_NAME).get(0));
        addonEnrollInput.setZipCode(responseMap.get(CCAVENUE_BILLING_ZIP_NAME).get(0));
        addonEnrollInput.setPaymentId(responseMap.get(CCAVENUE_RESPONSE_ID).get(0));*/
     //   LOG.info("addonEnrollInput "+addonEnrollInput.toString());

        return addonEnrollInput;
    }

    private void buildFormOutput(SlingHttpServletResponse response, String escapedResponse, String status,
            String transcationId) throws IOException {
    	LOG.info("Inside build form method");
    	String redirecTionUrl = "https://author-taj-stage63.adobecqms.net/content/tajhotels/en-in/taj-inner-circle/epicure-confirm";
        PrintWriter out = response.getWriter();
        out.print("<form id='addonEnrollConfirm' action='" + redirecTionUrl + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
        out.print("</form>");
        out.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                + "sessionStorage.setItem('addonEnrollResponse', '" + escapedResponse + "');"
                + "sessionStorage.setItem('transcationId', '" + transcationId + "');"
                + "sessionStorage.setItem('paymentStatus', '" + status + "');"
                + "document.getElementById(\"addonEnrollConfirm\").submit()" + "}" + "</script>");
        out.close();
    }
    
    private String fetchPaymentJusPay(String serviceURL, String orderid ) {
        byte[] authEncBytes = Base64.encodeBase64(apiKey.getBytes());
        String authStringEnc = new String(authEncBytes);
        String detailsresponsejson = null;
        LOG.info("Entered readJsonFromUrl method and authEnc is = "+authStringEnc);
        String endpoint = "/orders/" + orderid;

        try {
            MultivaluedMap<String, String> mMap = new MultivaluedHashMap<String, String>();
            mMap.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=utf-8");
            mMap.add("Authorization", "Basic " +authStringEnc);
            
        	detailsresponsejson = restService.consumeRestGetAPI(serviceURL, endpoint, "application/json", null, mMap, "30000");
        	
        } catch (Exception e) {
            LOG.info(e.getMessage(), e);
        }
        LOG.info("detailsresponsejson "+ detailsresponsejson);
        return detailsresponsejson;
        
    }
}
