/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.DestinationSearchService;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.core.util.ValidateRequestUrlInServlet;

/**
 * @author Gayathri
 *
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/contactUs" })
public class ContactUsAllHotelServlet extends SlingSafeMethodsServlet {

    /**
     * Added generated serial version id from eclipse.
     */

    private static final long serialVersionUID = -4451114132969057306L;

    @Reference
    HotelsSearchService hotelSearchService;

    @Reference
    DestinationSearchService destinationSearchService;


    ResourceResolver resourceResolver;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    private static final Logger LOG = LoggerFactory.getLogger(ContactUsAllHotelServlet.class);

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("Entry -> Method[ Name : doGet( )] in ContactUsServlet");
        LOG.trace("hotelSearchService Hash code : " + hotelSearchService.hashCode());

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(request);
        LOG.trace("Request URL for cliam form submit servlet is : " + requestUrl);
        if (validateRequest.validateUrl(requestUrl)) {
            try {
                resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
                if (null != resourceResolver)
                    LOG.trace("ContactUsAllHotelServlet - THE resourceResolver - " + resourceResolver.getUserID());
                else
                    LOG.trace("ContactUsAllHotelServlet - THE resourceResolver is null - " + resourceResolver);
            } catch (org.apache.sling.api.resource.LoginException e1) {
                LOG.error("Exception found in doGet of ContactUsAllHotelServlets :: {}", e1.getMessage());
                LOG.debug("Exception found in doGet of ContactUsAllHotelServlets :: {}", e1);
            }
            JSONObject jsonArrayDetails = null;
            String requestType = request.getParameter("type");
            String requestPath = StringUtils.isNotBlank(request.getParameter("hotelRootPath"))
                    ? request.getParameter("hotelRootPath")
                    : SearchServiceConstants.PATH.HOTELROOT;
            LOG.trace("request param value {}", requestType);
            List<String> hotelList;
            try {
                if (requestType.equals("hotels")) {
                    hotelList = hotelSearchService.getAllHotels(requestPath);
                    LOG.trace("Before calling getJsonforHotels method()");
                    jsonArrayDetails = getJsonforHotels(hotelList);
                    LOG.trace("jsonArrayHotelDetails - -  in getJsonforHotels " + jsonArrayDetails);

                    LOG.trace("After calling getJsonforHotels method()");
                } else if (requestType.equals("sales")) {
                    LOG.trace("Before calling getGlobalSalesOfficeList method()");
                    jsonArrayDetails = getSalesCorporateOfficeList("sales");
                    LOG.trace("After calling getGlobalSalesOfficeList method()");
                } else if (requestType.equals("corporates")) {
                    LOG.trace("Before calling getCorporateOfficeList method()");
                    jsonArrayDetails = getSalesCorporateOfficeList("corporates");
                    LOG.trace("After calling getCorporateOfficeList method()");
                } else if (requestType.equalsIgnoreCase("vivanta")) {
                    LOG.trace("Before calling getBrandSpecificHotels method()");
                    hotelList = hotelSearchService.getBrandSpecificHotels("vivanta");
                    jsonArrayDetails = getJsonforHotels(hotelList);
                    LOG.trace("After calling getBrandSpecificHotels method()");

                } else if (requestType.equalsIgnoreCase("seleqtions")) {
                    LOG.trace("Before calling getBrandSpecificHotels method()");
                    hotelList = hotelSearchService.getBrandSpecificHotels("seleqtions");
                    jsonArrayDetails = getJsonforHotels(hotelList);
                    LOG.trace("After calling getBrandSpecificHotels method()");

                } else if (requestType.equalsIgnoreCase("gateway")) {
                    LOG.trace("Before calling getBrandSpecificHotels method()");
                    hotelList = hotelSearchService.getBrandSpecificHotels("gateway");
                    jsonArrayDetails = getJsonforHotels(hotelList);
                    LOG.trace("After calling getBrandSpecificHotels method()");
                } else if (requestType.equalsIgnoreCase("taj")) {
                    LOG.trace("Before calling getBrandSpecificHotels method()");
                    hotelList = hotelSearchService.getBrandSpecificHotels("taj");
                    jsonArrayDetails = getJsonforHotels(hotelList);
                    LOG.trace("After calling getBrandSpecificHotels method()");
                }
                if (jsonArrayDetails != null) {
                    response.getWriter().write(jsonArrayDetails.toString());
                }
            } catch (Exception e) {
                LOG.error("Exception found in doGet :: {}", e.getMessage());
                LOG.debug("Exception found in doGet :: {}", e);
            } finally {
                LOG.trace("Exiting ContactUsAllHotelServlet");
                // request.getResourceResolver().adaptTo(Session.class);
            }
        } else {
            LOG.info("This request contains cross site attack characters");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    public JSONObject getJsonforHotels(List<String> hotelList) throws JSONException {
        JSONObject jsonHotelObject = new JSONObject();
        JSONArray jsonArrayHotelDetails = new JSONArray();
        for (String item : hotelList) {
            Resource resourceItem = resourceResolver.getResource(item);
            resourceItem = resourceItem.getChild("jcr:content");
            ValueMap valueMap = resourceItem.adaptTo(ValueMap.class);
            String hotelName = valueMap.get("hotelName", String.class);
            String hotelCity = valueMap.get("hotelCity", String.class);
            String countryName = valueMap.get("countryName", String.class);
            /*
             * if (null != countryName && countryName.equals("India") || countryName.equals("INDIA") ||
             * countryName.equals("india")) { countryName = "india"; }
             */
            LOG.trace("resourceItem FOR HOTELS " + resourceItem.getPath());
            String streetAddress = valueMap.get("streetAddress", String.class);
            String imagePath = valueMap.get("imagePath", String.class);
            String postalCode = valueMap.get("postalCode", String.class);
            String hotelPhoneNumber = valueMap.get("hotelPhoneNumber", String.class);
            String hotelId = valueMap.get("hotelId", String.class);
            String hotelLatitude = valueMap.get("hotelLatitude", String.class);
            String hotelLongitude = valueMap.get("hotelLongitude", String.class);
            String countryCode = valueMap.get("countryCode", String.class);
            String hotelEmail = valueMap.get("hotelEmail", String.class);
            // forming JSON
            JSONObject jsonObjhotelDetails = new JSONObject();
            JSONObject jsonObjCoordinates = new JSONObject();
            LOG.trace("LATITUDE - " + hotelLatitude);
            LOG.trace("LONGITUDE - " + hotelLongitude);
            if (null != hotelLatitude && !hotelLatitude.equals(""))
                jsonObjCoordinates.put("lat", Float.parseFloat(hotelLatitude));
            else
                jsonObjCoordinates.put("lat", 00.00);
            if (null != hotelLongitude && !hotelLongitude.equals(""))
                jsonObjCoordinates.put("lng", Float.parseFloat(hotelLongitude));
            else
                jsonObjCoordinates.put("lng", 00.00);

            jsonObjhotelDetails.put("city", hotelCity);
            jsonObjhotelDetails.put("name", hotelName);
            jsonObjhotelDetails.put("hotelEmail", hotelEmail);
            jsonObjhotelDetails.put("address",
                    streetAddress + ", " + hotelCity + ", " + countryName + ", " + postalCode);
            jsonObjhotelDetails.put("coordinates", jsonObjCoordinates);
            if (null != valueMap.get("areaCode", String.class)) {
                String areaCode = valueMap.get("areaCode", String.class);
                jsonObjhotelDetails.put("phone", countryCode + " " + areaCode + " " + hotelPhoneNumber);
            } else {
                jsonObjhotelDetails.put("phone", countryCode + " " + hotelPhoneNumber);
            }
            String faxPhone = valueMap.get("faxPhone", String.class);
            if (null != faxPhone && !faxPhone.equals("")) {
                LOG.trace("inside Hotel data - fax - " + faxPhone + " CITY NAME - " + hotelCity);
                jsonObjhotelDetails.put("fax", countryCode + " " + faxPhone);
            } else {
                LOG.trace("inside Hotel data fax is null, CITY NAME - " + hotelCity);
                jsonObjhotelDetails.put("fax", countryCode + " " + "null");
            }
            jsonObjhotelDetails.put("imagePath", imagePath);
            jsonObjhotelDetails.put("hotelPath", getMappedPath(resourceItem.getParent().getPath()));
            jsonObjhotelDetails.put("type", "hotels");
            jsonObjhotelDetails.put("cityId", hotelCity);
            jsonObjhotelDetails.put("hotelId", hotelId);
            jsonObjhotelDetails.put("country", countryName);
            LOG.trace("CountryName ---- - " + countryName);
            /*
             * if (countryName.equals("india") || countryName.equals("India") || countryName.equals("INDIA"))
             * jsonObjhotelDetails.put("countryType", "india"); else
             */
            jsonObjhotelDetails.put("countryType", "outside-india");

            jsonArrayHotelDetails.put(jsonObjhotelDetails);
            jsonHotelObject.put("data", jsonArrayHotelDetails);
        }
        LOG.trace("JSONArray in contactus page - - - " + jsonArrayHotelDetails);

        return jsonHotelObject;
    }

    public JSONObject getSalesCorporateOfficeList(String officeType) throws JSONException {
        JSONObject jsonSalesObject = new JSONObject();
        LOG.trace("in global sales list method");
        JSONArray jsonArraySalesDetails = new JSONArray();
        Resource resourceItem = null;
        if (officeType.equals("sales")) {
            resourceItem = resourceResolver.getResource(
                    "/content/shared-content/global-shared-content/shared-content/sales-office/jcr:content/root");
        } else if (officeType.equals("corporates")) {
            resourceItem = resourceResolver.getResource(
                    "/content/shared-content/global-shared-content/shared-content/corporate-office/jcr:content/root");
        }
        LOG.trace(" line 171 resourceItem- " + resourceItem);

        Iterable<Resource> resource = resourceItem.getChildren();
        for (

        Resource rec : resource) {
            LOG.trace("in getglobalsales list resource - " + rec.getName());
            ValueMap valueMap = rec.adaptTo(ValueMap.class);
            LOG.trace("Value map for sales office list - " + valueMap);
            String cityName = valueMap.get("cityName", String.class);
            String countryCode = valueMap.get("countryCode", String.class);
            LOG.trace("\n\n cityName - " + cityName + " countryCode - " + countryCode);
            String countryName = valueMap.get("countryName", String.class);
            String hotelAddress = valueMap.get("hotelAddress", String.class);
            String hotelName = valueMap.get("hotelName", String.class);
            String phoneNumber = valueMap.get("phoneNumber", String.class);
            String postalCode = valueMap.get("postalCode", String.class);
            String imagePath = valueMap.get("imagePath", String.class);

            JSONObject jsonObjSalesDetails = new JSONObject();
            String salesLatitude = valueMap.get("latitude", String.class);
            String salesLongitude = valueMap.get("longitude", String.class);
            LOG.trace("\n\n salesLatitude - " + salesLatitude + " salesLongitude - " + salesLongitude);
            // forming JSON
            jsonObjSalesDetails.put("city", cityName);
            jsonObjSalesDetails.put("name", hotelName);
            jsonObjSalesDetails.put("address", hotelAddress);
            JSONObject jsonObjCoordinates = new JSONObject();
            jsonObjCoordinates.put("lat", Float.parseFloat(salesLatitude));
            jsonObjCoordinates.put("lng", Float.parseFloat(salesLongitude));
            LOG.trace("Sales jsonObjCoordinates - " + jsonObjCoordinates);
            jsonObjSalesDetails.put("coordinates", jsonObjCoordinates);
            jsonObjSalesDetails.put("phone", countryCode + " " + phoneNumber);
            String faxPhone = valueMap.get("faxPhone", String.class);
            if (null != faxPhone && !faxPhone.equals("")) {
                LOG.trace("inside sales data - fax - " + faxPhone + " CITY NAME - " + cityName);
                jsonObjSalesDetails.put("fax", countryCode + " " + faxPhone);
            } else {
                LOG.trace("inside sales data fax is null city name - " + cityName);
                jsonObjSalesDetails.put("fax", countryCode + " " + "null");
            }
            if (officeType.equals("sales")) {
                jsonObjSalesDetails.put("type", "sales");
            } else if (officeType.equals("corporates")) {
                jsonObjSalesDetails.put("type", "hotels");
            }
            jsonObjSalesDetails.put("cityId", cityName);
            jsonObjSalesDetails.put("hotelId", cityName);
            jsonObjSalesDetails.put("country", countryName);
            jsonObjSalesDetails.put("imagePath", imagePath);
            if (null != countryName && countryName.equals("india") || countryName.equals("India")
                    || countryName.equals("INDIA"))
                jsonObjSalesDetails.put("countryType", "india");
            else
                jsonObjSalesDetails.put("countryType", "outside-india");
            LOG.trace("Sales jsonObjSalesDetails - " + jsonObjSalesDetails);
            jsonArraySalesDetails.put(jsonObjSalesDetails);
            jsonSalesObject.put("data", jsonArraySalesDetails);
            // JSONObject jsonCountryObj = getAllCountryList("taj:destination");
            JSONArray countryValue = getAllCountryList("taj:destination");
            jsonSalesObject.put("country", countryValue);
            LOG.trace("JSONArray in contactus page with country - - " + jsonSalesObject);
        }
        LOG.trace("JSONArray in sales details contactus page - - - " + jsonArraySalesDetails);
        return jsonSalesObject;
    }

    public JSONArray getAllCountryList(String countryTag) throws JSONException {
        HashMap<String, String> allCountry = destinationSearchService.getDestinationByCountry(countryTag);
        LOG.trace("inside getAllCountryList - " + allCountry);
        LOG.trace("get title of all country" + allCountry.values());
        LOG.trace("all country - " + allCountry);
        JSONObject jsonCountryObj = new JSONObject();
        JSONArray jsoncountryarray = new JSONArray();

        /*
         * for (Map.Entry<String, String> entry : allCountry.entrySet()) { String key = entry.getKey(); String value =
         * entry.getValue(); LOG.trace("MAP ENTRY VALUE - " + value); }
         */
        /*
         * jsonCountryObj.put(allCountry.values()); LOG.trace("Country JSON - " + jsonCountryObj); return
         * jsonCountryObj;
         */
        jsoncountryarray.put(allCountry.values());
        LOG.trace("jsoncountryarray - " + jsoncountryarray);
        return jsoncountryarray;
    }

    private String getMappedPath(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            // return URLMapUtil.getPathFromURL(resolvedURL);
            return resolvedURL;
        }
        return url;
    }
}
