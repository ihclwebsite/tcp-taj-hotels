/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.exception.TajHotelException;
import com.ihcl.core.services.ImageCropperService;

/**
 * @author Srikanta.moonraft
 *
 */
@Component(
        service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/crop/requestedImage" })
public class ImageCropperServlet extends SlingSafeMethodsServlet {

    /**
     * added generated serialVersionUID
     */
    private static final long serialVersionUID = 714808103493465434L;

    private static final Logger LOG = LoggerFactory.getLogger(ImageCropperServlet.class);

    @Reference
    private ImageCropperService imageCropper;

    private String responseMassageStatus = "";

    /*
     * (non-Javadoc)
     *
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("Entry -> Method[ Name : doGet( )]");

        String incomingImageUrl = null;
        String jsonString = null;
        int x = 20, y = 20, width = 200, height = 200;
        String croppedImageUrl = null;

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode responseNode = mapper.createObjectNode();

        if (imageCropper != null) {
            LOG.debug("Image cropper is not null & it's Hashcode : " + imageCropper.hashCode());
            incomingImageUrl = request.getParameter("incomingImageUrl");
            x = Integer.parseInt(request.getParameter("x"));
            y = Integer.parseInt(request.getParameter("y"));
            width = Integer.parseInt(request.getParameter("width"));
            height = Integer.parseInt(request.getParameter("height"));
            LOG.debug("Ajax incomingImageUrl before processing: " + incomingImageUrl + "x, y, width, Height : " + x
                    + ", " + y + ", " + width + ", " + height);

            responseNode.put("incomingImageUrl", incomingImageUrl);
            responseNode.put("x", x);
            responseNode.put("y", y);
            responseNode.put("width", width);
            responseNode.put("height", height);

            if (incomingImageUrl == null || incomingImageUrl.equals("") || incomingImageUrl.length() < 4) {
                responseMassageStatus += "'Invalid incomingImageUrl' ";
                if (x < 0 || y < 0 || width < 0 || height < 0)
                    responseMassageStatus += "'Invalid dimension, check(x, y, width, height)' ";
            }
            // crop the image
            croppedImageUrl = imageCropper.cropImageFromUrl(incomingImageUrl, x, y, width, height);
            if (croppedImageUrl != null) {
                responseMassageStatus = "Image has been cropped successfully";

                responseNode.put("incomingImageUrl", incomingImageUrl);
                responseNode.put("x", x);
                responseNode.put("y", y);
                responseNode.put("width", width);
                responseNode.put("height", height);
                responseNode.put("croppedImageUrl", croppedImageUrl);
                responseNode.put("responseMassageStatus", responseMassageStatus);

                jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseNode);

                // returning as JSON

            } else if (croppedImageUrl == null) {
                new TajHotelException("Image can't be cropped. Cropped image Url is null");
                LOG.error("Cropped URL is null after cropping.",
                        new TajHotelException("Image can't be cropped. Cropped image Url is null"));
            }
        } else if (imageCropper == null) {
            LOG.debug("Image cropper is null : Can't process the image.");
            responseMassageStatus += "'Image cropper is null : Can't process the image.'";
        } else {
            responseMassageStatus += "Failed to process cropping the image.";
            new TajHotelException("Failed to process cropping the image.");
        }
        LOG.trace("Exit -> Method[ Name : doGet( )]");
        responseNode.put("responseMassageStatus", responseMassageStatus);
        prepareResponse(response, jsonString);
    }

    private void prepareResponse(SlingHttpServletResponse response, String jsonString) {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(jsonString);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            LOG.error(e.getMessage(), e);
        }
    }
}
