package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_AMOUNT_NAME;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_MERCH1_NAME;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_MERCH2_NAME;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_MERCH3_NAME;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_MERCH4_NAME;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE;
import static com.ihcl.tajhotels.constants.BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.integration.exception.ServiceException;
import com.ihcl.loyalty.dto.PurchasePointInput;
import com.ihcl.loyalty.dto.PurchaseResponse;
import com.ihcl.loyalty.services.IPurchasePointService;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "= Servlet to invoke add on enrollment",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths=" + "/bin/purchase/redirect" })
public class PurchasePointPostPaymentServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(PurchasePointPostPaymentServlet.class);
    private static final long serialVersionUID = 3982582391923505051L;

    @Reference
    private ConfigurationService conf;

    @Reference
    private IPurchasePointService iPurchasePointService;

    private ObjectMapper objectMapper;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        objectMapper = new ObjectMapper();
        String encResp = request.getParameter(CCAVENUE_RESPONSE_PARAM_NAME);
        AesCryptUtil aesUtil = new AesCryptUtil(conf.getCCAvenueWorkingKey());
        String decResp = aesUtil.decrypt(encResp);
        URLMapUtil urlMapUtil = new URLMapUtil();
        Map<String, List<String>> paymentDetailsMap = urlMapUtil.splitQuery(decResp);
        try {
            String status = paymentDetailsMap.get(CCAVENUE_RESPONSE_ORDER_STATUS).get(0);
            if (status.equals(CCAVENUE_RESPONSE_ORDER_STATUS_VALUE)) {
                PurchaseResponse purchaseResponse = iPurchasePointService
                        .purchasepoints(buildServiceInput(paymentDetailsMap));
                String parsedAddonResp = objectMapper.writeValueAsString(purchaseResponse);
                response.setContentType(BookingConstants.CONTENT_TYPE_HTML);
                JSONObject jsonObj = new JSONObject();

                jsonObj.put("membershipNumber", paymentDetailsMap.get(CCAVENUE_MERCH1_NAME).get(0));
                jsonObj.put("pointsToRedeem", paymentDetailsMap.get(CCAVENUE_MERCH4_NAME).get(0));
                String escapedAddonResp = StringEscapeUtils.escapeEcmaScript(parsedAddonResp);
                buildFormOutput(response, parsedAddonResp, status, jsonObj);
            } else {
                buildFormOutput(response, null, status, null);
            }
        } catch (ServiceException | JSONException e) {
            LOG.error(e.getMessage(), e);
            response.sendError(500, e.getMessage());
        }
    }

    private PurchasePointInput buildServiceInput(Map<String, List<String>> responseMap) {
        PurchasePointInput purchasePointInput = new PurchasePointInput();
        purchasePointInput.setMembershipNumber(responseMap.get(CCAVENUE_MERCH1_NAME).get(0));
        purchasePointInput.setBillAmount(responseMap.get(CCAVENUE_AMOUNT_NAME).get(0));
        purchasePointInput.setPointType(responseMap.get(CCAVENUE_MERCH2_NAME).get(0));
        purchasePointInput.setPointsToBuy(responseMap.get(CCAVENUE_MERCH4_NAME).get(0));
        purchasePointInput.setProductName(responseMap.get(CCAVENUE_MERCH3_NAME).get(0));

        return purchasePointInput;
    }

    private void buildFormOutput(SlingHttpServletResponse response, String escapedResponse, String status,
            JSONObject json) throws IOException {
        PrintWriter out = response.getWriter();
        out.print("<form id='purchaseConfirm' action='" + conf.getPurchasePaymentSuccess()
                + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
        out.print("</form>");
        out.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                + "sessionStorage.setItem('purchaseResponse', '" + escapedResponse + "');"
                + "sessionStorage.setItem('paymentStatus', '" + status + "');"
                + "sessionStorage.setItem('memberDetails', '" + json + "');"
                + "document.getElementById(\"purchaseConfirm\").submit()" + "}" + "</script>");
        out.close();
    }
}
