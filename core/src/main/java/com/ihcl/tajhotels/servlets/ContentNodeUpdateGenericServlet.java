/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.search.SearchProvider;

/**
 * <pre>
* ContentNodeUpdateGenericServlet Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :Feb 11, 2019
 * @ClassName : ContentNodeUpdateGenericServlet.java
 * @Description : com.ihcl.tajhotels.servlets
 * @Modification Information
 *
 *               <pre>
*
*     since            author               description
*  ===========     ==============   =========================
*  Feb 11, 2019     Neha Priyanka            Create
 *
 *               </pre>
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/updateTajhotelContent", "sling.servlet.methods=GET" })

public class ContentNodeUpdateGenericServlet extends SlingSafeMethodsServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentNodeUpdateGenericServlet.class);

    private static final long serialVersionUID = -4542206988616094897L;

    @Reference
    SearchProvider searchProvider;

    @Reference
    ResourceResolverFactory resourceResolverFactory;


    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        ResourceResolver resourceResolver = null;
        Session session = null;

        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            session = resourceResolver.adaptTo(Session.class);
            boolean status = getPropertiesFromAuthor(request, resourceResolver, session);
            response.getWriter().write("Node updated :: " + status);
        } catch (Exception e) {
            LOGGER.error("Exception Occurred in doGet of ContentNodeUpdateGenericServlet :: {}", e.getMessage());
            LOGGER.debug("Exception Occurred in doGet of ContentNodeUpdateGenericServlet :: {}", e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
            if (null != session) {
                session.logout();
            }
        }


    }


    /**
     * @param request
     * @param resourceResolver
     * @return
     */
    private boolean getPropertiesFromAuthor(SlingHttpServletRequest request, ResourceResolver resourceResolver,
            Session session) {
        String contentPath = request.getParameter("path");
        String propertyName = request.getParameter("propertyName");
        String propertyValue = request.getParameter("propertyValue");
        String propertyText = request.getParameter("propertyText");
        String updatedPropertyText = request.getParameter("updatedPropertyText");
        try {
            HashMap<String, String> predicateMap = new HashMap<>();
            predicateMap.put("path", contentPath);
            predicateMap.put("property", "sling:resourceType");
            predicateMap.put("property.value", propertyValue);
            SearchResult searchResults = getQueryResult(predicateMap, resourceResolver, session);
            if (null != searchResults) {
                return updateNodeProperties(searchResults, propertyText, updatedPropertyText, propertyName, session);
            }
        } catch (Exception e) {
            LOGGER.error("Exception occurred at getPropertiesFromAuthor {}", e.getMessage());
            LOGGER.debug("Exception occurred at getPropertiesFromAuthor {}", e);
        }
        return false;
    }


    /**
     * @param searchResults
     * @param propertyText
     * @param updatedPropertyText
     * @param propertyName
     * @param resourceResolver
     * @param session
     * @param session
     * @return
     */
    private boolean updateNodeProperties(SearchResult searchResults, String propertyText, String updatedPropertyText,
            String propertyName, Session session) {
        boolean nodeUpdated = false;
        try {
            int count = 0;
            Iterator<Node> itrSearchNode = searchResults.getNodes();
            LOGGER.debug("{} Number of nodes found for update", searchResults.getHits().size());
            while (itrSearchNode.hasNext()) {
                count++;
                Node propertyNode = itrSearchNode.next();
                if (propertyNode.hasProperty(propertyName)) {
                    String existingPropertyValue = propertyNode.getProperty(propertyName).getValue().getString();
                    if (existingPropertyValue.contains(propertyText)) {
                        String newPropertyValue = existingPropertyValue.replace(propertyText, updatedPropertyText);
                        propertyNode.setProperty(propertyName, newPropertyValue);
                        session.save();
                        nodeUpdated = true;
                    }
                }
            }
            LOGGER.debug("{} number of Nodes updated", count);
        } catch (RepositoryException e) {
            LOGGER.error("Exception occurred at updateNodeProperties {}", e.getMessage());
            LOGGER.debug("Exception occurred at updateNodeProperties {}", e);
        }
        return nodeUpdated;
    }

    public SearchResult getQueryResult(Map<String, String> predicates, ResourceResolver resourceResolver,
            Session session) {
        QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
        final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        query.setHitsPerPage(99999);
        return query.getResult();
    }

}
