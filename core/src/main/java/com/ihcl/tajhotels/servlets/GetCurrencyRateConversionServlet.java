package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/getCurrencyRateConversion", "sling.servlet.methods=GET" })
public class GetCurrencyRateConversionServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final String PATH_TO_CURRENCY_CONVERSION = "/content/shared-content/global-shared-content/currency-conversion";

    private static final Logger LOG = LoggerFactory.getLogger(GetCurrencyRateConversionServlet.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    Map<String, String> currencyArray = null;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        LOG.debug("Inside the GetCurrencyRateConversionServlet");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        currencyArray = new HashMap<>();

        ResourceResolver resourceResolver;
        try {
        	LOG.debug("Attempting to get currency conversion values");
        	
        	resourceResolver = resolverFactory.getServiceResourceResolver(null);
            Resource currencyConversionResource = resourceResolver.getResource(PATH_TO_CURRENCY_CONVERSION);
            if (currencyConversionResource != null) {
				for (Resource currencyConversionNode : currencyConversionResource.getChildren()) {
					currencyArray.put(currencyConversionNode.getName(),
							currencyConversionNode.getValueMap().get("conversionRate", String.class));
				}
				out.println(new JSONObject(currencyArray));
			}else {
				LOG.error("Currency node is null. No data available to send for currency conversion");
			}
        } catch (LoginException e) {
            LOG.error("Error Occured in  GetCurrencyRateConversionServlet : {}", e.getMessage());
        }
    }
}
