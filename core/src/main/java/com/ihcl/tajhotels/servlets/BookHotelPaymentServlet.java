package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.servicehandler.processpayment.PaymentHandler;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/paymentProcessing", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=buildpayment", })
public class BookHotelPaymentServlet extends SlingAllMethodsServlet {


    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(BookHotelPaymentServlet.class);

    @Reference
    TajHotelsBrandsETCService etcConfigService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        LOG.debug("PaymentServlet : Started");
        LOG.debug("PaymentServlet conf test :: ");

        ObjectMapper objectMapper = new ObjectMapper();
        String returnMessage = null;
        String bookingObjectFromParam = request.getParameter(BookingConstants.FORM_BOOKING_REQUEST);

        LOG.info("request.getRequestURL(): {}", request.getRequestURL());
        LOG.info("request.getRequestURI(): {}", request.getServerName());

        String websiteId = request.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "www.tajhotels.com";
        }

        LOG.debug("BookingObject String from getParams :: {}", bookingObjectFromParam);
        BookingObject bookingObject = objectMapper.readValue(bookingObjectFromParam,
                new TypeReference<BookingObject>() {
                });
        LOG.debug("Itinerary Number string :: {}", bookingObject.getItineraryNumber());

        TajHotelsAllBrandsConfigurationsBean etcConfigBean = etcConfigService.getEtcConfigBean();
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcConfigBean.getEtcConfigBean().get(websiteId);

        String idForCCAvenueConfig = ReservationConstants.BOOKING_CCAVENUE;
        if (bookingObject.isCorporateBooking()) {
            LOG.info("Inside corporate booking and calling CCAvenue with corporate MerchantId ");
            idForCCAvenueConfig = ReservationConstants.CORPORATE_BOOKING_CCAVENUE;
        } else if (bookingObject.getPayments() != null && bookingObject.getPayments().getTicpoints() != null
                && bookingObject.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                && bookingObject.getPayments().getTicpoints().isPointsPlusCash()) {
            LOG.info("Inside Tic domain and tic points and calling CCAvenue with tic MerchantId ");
            idForCCAvenueConfig = ReservationConstants.TIC_CCAVENUE;
        }

        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(idForCCAvenueConfig);
        String configString = "websiteId: " + websiteId + ", idForCCAvenueConfig: " + idForCCAvenueConfig
                + ", etcWebsiteConfig: " + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOG.info(configString);

        if (bookingObject.getPayments().isPayAtHotel()) {
            LOG.debug("PaymentServlet----- : Ignoring payment as pay at hotel is selected -----");
        } else {
            PaymentHandler paymentHandeler = new PaymentHandler();
            try {
                String bookingObjectString = "Final BookingObject before passing to PaymentHandler: "
                        + objectMapper.writeValueAsString(bookingObject);
                LOG.info(bookingObjectString);
                returnMessage = paymentHandeler.getCCAvenueDetails(bookingObject.getItineraryNumber(), bookingObject,
                        etcWebsiteConfig);
            } catch (ParseException e) {
                LOG.debug("Exception occured while Booking : {}", e.getMessage());
                LOG.error("Exception occured while Booking : {}", e);
            }
        }

        LOG.debug("Returning Payment data to page");
        response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
        response.getWriter().write(returnMessage);
        response.getWriter().close();

    }

}
