package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.HotelDetailsFetcher;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.core.servicehandler.processpayment.PaymentHandler;
import com.ihcl.core.services.booking.PaymentCardDetailsConfigurationService;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.core.util.ObjectConverter;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Coupon;
import com.ihcl.integration.dto.details.Flight;
import com.ihcl.integration.dto.details.Guest;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.integration.dto.details.Payments;
import com.ihcl.integration.dto.details.ResStatus;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/bookHotelInitiateServlet", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class BookHotelInitiateServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(BookHotelInitiateServlet.class);

    @Reference
    private IBookingRequestHandler bookingHandler;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private TajHotelsBrandsETCService tajHotelsBrandsETCService;

    @Reference
    private PaymentCardDetailsConfigurationService paymentCardDetailsService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    @Override
    protected void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
            throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        String returnMessage = "";
        Map<String, String> ignoreDetailsMap = new HashMap<>();
        try {
            if (synixsDowntimeConfigurationService.isForcingToSkipCalls()) {
                LOG.info("isForcingToSkipCalls: {}", synixsDowntimeConfigurationService.isForcingToSkipCalls());
                throw new HotelBookingException(synixsDowntimeConfigurationService.getDowntimeMessage());

            } else if (validateURLRequest(servletRequest)) {

                LOG.debug("Validation completed. Now processing Request to access data from it.");
                BookingObject bookingObjectRequest = buildingBookingRequestFromServletRequest(servletRequest,
                        objectMapper);

                String bookingRequestString = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.info("Sending bookingObjectRequest to Booking-Backend: {}", bookingRequestString);

                BookingObject bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                String bookingResponseString = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received bookingObjectResponse from Booking-Backend: {}", bookingResponseString);

                ignoreDetailsMap.put(BookingConstants.ITINERARY_NUMBER, bookingObjectResponse.getItineraryNumber());
                ignoreDetailsMap.put(BookingConstants.FORM_HOTEL_ID, bookingObjectResponse.getHotelId());
                 returnMessage = handlingBookingResponse(objectMapper, servletRequest, bookingObjectRequest,
                        bookingObjectResponse);
               // returnMessage = objectMapper.writeValueAsString(bookingObjectResponse);
            } else {
                LOG.info("This request contains cross site attack characters");
                returnMessage = "Service Call Failed. Please try Again";
                servletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        } catch (HotelBookingException hbe) {
            LOG.info("Throws custom  HotelBookingException : {}", hbe.getMessage());
            LOG.error("Throws custom HotelBookingException : {}", hbe);
            ignoreBooking(ignoreDetailsMap);
            returnMessage = hbe.getMessage();
        } catch (Exception e) {
            LOG.info("Exception occured while Booking : {}", e.getMessage());
            LOG.error("Exception occured while Booking : {}", e);
            ignoreBooking(ignoreDetailsMap);
            returnMessage = "Service Call Failed. Please try Again";
        } finally {
            LOG.info("Final returnMessage : {}", returnMessage);
            servletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            servletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            servletResponse.getWriter().write(returnMessage);
            LOG.info("Returning response to page");
            servletResponse.getWriter().close();
        }
    }

    /**
     * @param request
     * @return boolean
     */
    private boolean validateURLRequest(SlingHttpServletRequest request) {
        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(request);
        LOG.trace("Request URL for Booking room initiate servlet is : {}", requestUrl);

        // Fetching data from request parameters.. Need to remove after Vijay's code
        String guestString = request.getParameter(BookingConstants.FORM_GUEST_DETAILS);
        String paymentsString = request.getParameter(BookingConstants.FORM_PAYMENT_DETAILS);
        String roomString = request.getParameter(BookingConstants.FORM_ROOM_DETAILS);
        String flightDetailsString = request.getParameter(BookingConstants.FORM_FLIGHT_DETAILS);
        String chainCode = request.getParameter(BookingConstants.FORM_HOTEL_CHAIN_CODE);
        String hotelId = request.getParameter(BookingConstants.FORM_HOTEL_ID);
        String hotelLocationId = request.getParameter(BookingConstants.HOTEL_LOCATION_ID);
        String checkInDate = request.getParameter(BookingConstants.FORM_CHECK_IN_DATE);
        String checkOutDate = request.getParameter(BookingConstants.FORM_CHECKOUT_DATE);
        String promotionCode = request.getParameter(BookingConstants.FORM_PROMO_CODE);
        String couponCode = request.getParameter(BookingConstants.FORM_COUPON_CODE);
        String loggedInUserString = request.getParameter(BookingConstants.LOGGED_IN_USER_DETAILS);

        StringBuilder completeRequest = new StringBuilder();
        completeRequest.append(guestString);
        completeRequest.append(paymentsString);
        completeRequest.append(roomString);
        completeRequest.append(flightDetailsString);
        completeRequest.append(chainCode);
        completeRequest.append(hotelId);
        completeRequest.append(hotelLocationId);
        completeRequest.append(checkInDate);
        completeRequest.append(checkOutDate);
        completeRequest.append(promotionCode);
        completeRequest.append(couponCode);
        completeRequest.append(loggedInUserString);

        LOG.info("Complete Request for booking room initiate is : {}", completeRequest);

        return validateRequest.validateUrl(completeRequest.toString());
    }

    /**
     * @param request
     * @param objectMapper
     * @return bookingObjectRequest
     * @throws IOException
     * @throws HotelBookingException
     */
    private BookingObject buildingBookingRequestFromServletRequest(SlingHttpServletRequest request,
            ObjectMapper objectMapper) throws IOException, HotelBookingException {

        LOG.debug("Starting: buildingBookingObjectFromServletRequest()");
        Map<String, String> individualParamsMap = getIndividualParamsFromRequest(request);

        BookingObject bookingObjectRequest = injectingObjectStringFromServletRequest(request, objectMapper);
        String bookingObjectRequestString = objectMapper.writeValueAsString(bookingObjectRequest);
        LOG.debug("After injecting object String From ServletRequest to bookingObjectRequest: {}",
                bookingObjectRequestString);

        injectingIndvParamsToBookingObject(bookingObjectRequest, individualParamsMap);
        bookingObjectRequestString = objectMapper.writeValueAsString(bookingObjectRequest);
        LOG.debug("After injecting individual params From ServletRequest to bookingObjectRequest: {}",
                bookingObjectRequestString);

        checkForBookingMandatoryFields(bookingObjectRequest);

        String specialRequest = addingExtraInfoToSpecialRequest(bookingObjectRequest);
        bookingObjectRequest.getGuest().setSpecialRequests(specialRequest);

        bookingObjectRequest.setResStatus(ResStatus.Initiate);

        LOG.debug("Ending: buildingBookingObjectFromServletRequest()");
        return bookingObjectRequest;
    }

    /**
     * @param request
     * @return
     */
    private Map<String, String> getIndividualParamsFromRequest(SlingHttpServletRequest request) {

        LOG.debug("Starting: getRequestIndividualParams()");
        String hotelId = request.getParameter(BookingConstants.FORM_HOTEL_ID);
        String chainCode = request.getParameter(BookingConstants.FORM_HOTEL_CHAIN_CODE);
        String checkInDate = request.getParameter(BookingConstants.FORM_CHECK_IN_DATE);
        String checkOutDate = request.getParameter(BookingConstants.FORM_CHECKOUT_DATE);
        String promotionCode = request.getParameter(BookingConstants.FORM_PROMO_CODE);
        String itineraryNumber = request.getParameter(BookingConstants.ITINERARY_NUMBER);
        String corporateBooking = request.getParameter(BookingConstants.CORPORATE_BOOKING);

        String indParamLog = "HotelId: " + hotelId + ", chainCode: " + chainCode + ", checkInDate: " + checkInDate
                + ", checkOutDate: " + checkOutDate + ", promotionCode: " + promotionCode + ", itineraryNumber: "
                + itineraryNumber + ", corporateBooking: " + corporateBooking;
        LOG.info("IndividualParamsFromRequest:- {}", indParamLog);

        return settingIndivisualParamsIntoMap(hotelId, chainCode, checkInDate, checkOutDate, promotionCode,
                itineraryNumber, corporateBooking);
    }

    /**
     * @param hotelId
     * @param chainCode
     * @param checkInDate
     * @param checkOutDate
     * @param promotionCode
     * @param itineraryNumber
     * @param corporateBooking
     * @return
     */
    private Map<String, String> settingIndivisualParamsIntoMap(String hotelId, String chainCode, String checkInDate,
            String checkOutDate, String promotionCode, String itineraryNumber, String corporateBooking) {

        Map<String, String> indvParamsStringMap = new HashMap<>();
        indvParamsStringMap.put(BookingConstants.FORM_HOTEL_ID, hotelId);
        indvParamsStringMap.put("chainCode", chainCode);
        indvParamsStringMap.put("checkInDate", checkInDate);
        indvParamsStringMap.put("checkOutDate", checkOutDate);
        indvParamsStringMap.put("promotionCode", promotionCode);
        indvParamsStringMap.put(BookingConstants.ITINERARY_NUMBER, itineraryNumber);
        indvParamsStringMap.put("corporateBooking", corporateBooking);

        LOG.debug("Ending: getRequestIndividualParams()");
        return indvParamsStringMap;
    }

    /**
     * @param slingHttpServletRequest
     * @param objectMapper
     * @return
     * @throws IOException
     */
    private BookingObject injectingObjectStringFromServletRequest(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper) throws IOException {

        LOG.debug("Starting: gettingObjectStringParamsFromRequest()");
        String guestString = slingHttpServletRequest.getParameter(BookingConstants.FORM_GUEST_DETAILS);
        String paymentsString = slingHttpServletRequest.getParameter(BookingConstants.FORM_PAYMENT_DETAILS);
        String roomString = slingHttpServletRequest.getParameter(BookingConstants.FORM_ROOM_DETAILS);
        String flightDetailsString = slingHttpServletRequest.getParameter(BookingConstants.FORM_FLIGHT_DETAILS);
        String loggedInUserString = slingHttpServletRequest.getParameter(BookingConstants.LOGGED_IN_USER_DETAILS);
        String couponCode = slingHttpServletRequest.getParameter(BookingConstants.FORM_COUPON_CODE);
        String hotelLocationId = slingHttpServletRequest.getParameter(BookingConstants.HOTEL_LOCATION_ID);

        loggingObjectStringRequestParms(guestString, paymentsString, roomString, flightDetailsString,
                loggedInUserString, couponCode, hotelLocationId);

        BookingObject bookingObject = injectingGuestPaymentAndRoomObjectString(objectMapper, guestString,
                paymentsString, roomString);

        injectingRemainingObjectStrings(bookingObject, objectMapper, flightDetailsString, loggedInUserString,
                couponCode, hotelLocationId);

        return bookingObject;
    }

    /**
     * @param objectMapper
     * @param guestString
     * @param paymentsString
     * @param roomString
     * @return
     * @throws IOException
     */
    private BookingObject injectingGuestPaymentAndRoomObjectString(ObjectMapper objectMapper, String guestString,
            String paymentsString, String roomString) throws IOException {

        BookingObject bookingObject = new BookingObject();

        Guest guest = objectMapper.readValue(guestString, new TypeReference<Guest>() {
        });
        bookingObject.setGuest(guest);

        Payments payments = objectMapper.readValue(paymentsString, new TypeReference<Payments>() {
        });

        if (payments.isCreditCardRequired() && (payments.isPayOnlineNow() || (payments.isPayAtHotel()
                && payments.getTicpoints() != null && payments.getTicpoints().isRedeemTicOrEpicurePoints()))) {
            setDefaultCreditCardDetails(payments);
            LOG.info("Settting Dummy card details in payments");
        }
        bookingObject.setPayments(payments);

        List<Room> roomList = objectMapper.readValue(roomString, new TypeReference<List<Room>>() {
        });
        bookingObject.setRoomList(roomList);

        if (bookingObject.getGuest().equals(guest) && bookingObject.getPayments().equals(payments)
                && bookingObject.getRoomList().equals(roomList)) {
            LOG.info("BookHotelInitiateServlet : Data objects created successfully");
        } else {
            LOG.info("BookHotelInitiateServlet : Data objects creation failed");
        }

        return bookingObject;

    }

    /**
     * @param payments
     */
    private void setDefaultCreditCardDetails(Payments payments) {
        payments.setNameOnCard(paymentCardDetailsService.getCardName());
        payments.setExpiryMonth(paymentCardDetailsService.getExpiryDate());
        payments.setCardNumber(paymentCardDetailsService.getCardNumber());
        payments.setCardType(paymentCardDetailsService.getCardType());
    }

    /**
     * @param bookingObject
     * @param objectMapper
     * @param flightDetailsString
     * @param loggedInUserString
     * @param couponCode
     * @param hotelLocationId
     * @return
     * @throws IOException
     */
    private BookingObject injectingRemainingObjectStrings(BookingObject bookingObject, ObjectMapper objectMapper,
            String flightDetailsString, String loggedInUserString, String couponCode, String hotelLocationId)
            throws IOException {

        LOG.debug("Starting: settingBookingObjectFromObjectStrings()");
        Flight flight = objectMapper.readValue(flightDetailsString, new TypeReference<Flight>() {
        });
        if (StringUtils.isNotBlank(flight.getArrivalFlightNo()) || StringUtils.isNotBlank(flight.getFlightArrivalTime())
                || StringUtils.isNotBlank(flight.getArrivalAirportName())) {
            bookingObject.setFlight(flight);
        }

        if (StringUtils.isNotBlank(loggedInUserString) && !loggedInUserString.equalsIgnoreCase("undefined")) {
            LOG.debug("loggedInUserString converting to Guest Obj: {}", loggedInUserString);
            Guest loggedInUser = objectMapper.readValue(loggedInUserString, new TypeReference<Guest>() {
            });
            bookingObject.setLoggedInUser(loggedInUser);
            String loggedInUserD = objectMapper.writeValueAsString(loggedInUser);
           LOG.debug("LoggedIn user objeect " +loggedInUserD);
        }

        if (StringUtils.isNotBlank(hotelLocationId)) {
            Hotel hotel = new Hotel();
            hotel.setHotelLocationId(hotelLocationId);
            bookingObject.setHotel(hotel);
        }

        if (StringUtils.isNotBlank(couponCode)) {
            Coupon coupon = new Coupon();
            coupon.setAppliedCoupon(couponCode);
            bookingObject.setCoupon(coupon);
        }

        LOG.debug("Ending: settingBookingObjectFromObjectStrings()");
        return bookingObject;
    }

    /**
     * @param bookingObjectRequest
     * @param individualParamsMap
     * @return
     */
    private BookingObject injectingIndvParamsToBookingObject(BookingObject bookingObjectRequest,
            Map<String, String> individualParamsMap) {

        LOG.debug("Starting: injectingIndvParamsToBookingObject()");
        bookingObjectRequest.setChainCode(individualParamsMap.get("chainCode"));
        bookingObjectRequest.setHotelId(individualParamsMap.get(BookingConstants.FORM_HOTEL_ID));
        bookingObjectRequest.setCheckInDate(individualParamsMap.get("checkInDate"));
        bookingObjectRequest.setCheckOutDate(individualParamsMap.get("checkOutDate"));
        bookingObjectRequest.setPromotionCode(individualParamsMap.get("promotionCode"));
        bookingObjectRequest.setCorporateBooking(Boolean.valueOf(individualParamsMap.get("corporateBooking")));

        String itineraryNumber = individualParamsMap.get(BookingConstants.ITINERARY_NUMBER);
        if (StringUtils.isNotBlank(itineraryNumber)) {
            bookingObjectRequest.setItineraryNumber(itineraryNumber);
        }
        LOG.debug("Ending: injectingIndvParamsToBookingObject()");

        return bookingObjectRequest;
    }

    /**
     * @param bookingObjectRequest
     * @return
     * @throws IOException
     */
    private String addingExtraInfoToSpecialRequest(BookingObject bookingObjectRequest) throws IOException {

        LOG.debug("Starting: addingExtraInfoToSpecialRequest()");
        String specialRequest = "";
        String gstNumber = "";

        if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getGstNumber())) {
            gstNumber = BookingConstants.GUEST_GST_NUMBER.concat(bookingObjectRequest.getGuest().getGstNumber());
        }

        if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getSpecialRequests())) {
            specialRequest = bookingObjectRequest.getGuest().getSpecialRequests();
        }

        String flightData = flightDetailStringForSpecialRequest(bookingObjectRequest);
        if (StringUtils.isNotBlank(specialRequest) && StringUtils.isNotBlank(gstNumber)) {
            specialRequest += ", ".concat(gstNumber);
        } else if (StringUtils.isBlank(specialRequest) && StringUtils.isNotBlank(gstNumber)) {
            specialRequest += gstNumber;
        }

        if (StringUtils.isNotBlank(specialRequest) && StringUtils.isNotBlank(flightData)) {
            specialRequest += ", ".concat(flightData);
        } else if (StringUtils.isBlank(specialRequest) && StringUtils.isNotBlank(flightData)) {
            specialRequest += flightData;
        }

        LOG.debug("Ending: addingExtraInfoToSpecialRequest():- {}", specialRequest);
        return specialRequest;
    }

    /**
     * @param bookingObjectRequest
     * @return
     * @throws IOException
     */
    private String flightDetailStringForSpecialRequest(BookingObject bookingObjectRequest) throws IOException {
        ObjectConverter converter = new ObjectConverter();
        if (bookingObjectRequest.getFlight() != null
                && StringUtils.isNotBlank(bookingObjectRequest.getFlight().getFlightArrivalTime())
                && StringUtils.isNotBlank(bookingObjectRequest.getFlight().getArrivalFlightNo())) {

            String flightString = converter.setFlightDetails(bookingObjectRequest.getFlight());
            LOG.debug("flight string {}", flightString);
            if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getSpecialRequests())
                    && StringUtils.isNotBlank(bookingObjectRequest.getFlight().getArrivalAirportName())) {
                return flightString;
            } else {
                return StringUtils.EMPTY;
            }

        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * @param objectMapper
     * @param slingHttpServletRequest
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @return
     * @throws ParseException
     * @throws IOException
     * @throws RepositoryException
     * @throws HotelBookingException
     * @throws LoginException
     * @throws Exception
     */
    private String handlingBookingResponse(ObjectMapper objectMapper, SlingHttpServletRequest slingHttpServletRequest,
            BookingObject bookingObjectRequest, BookingObject bookingObjectResponse)
            throws HotelBookingException, RepositoryException, IOException, ParseException, LoginException {
        /*
         * Checking bookingObjectRes.isSuccess or bookingObjectRes.isPartialBooking, If any one of this is true, Setting
         * bookingObjectRes.seccess is true
         */
        if (bookingObjectResponse.isSuccess() || bookingObjectResponse.isPartialBooking()) {
            bookingObjectResponse.setSuccess(true);

            // [TIC-FLOW]
            if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getMembershipId())) {
                // since membership id gets deleted after the call, temp fix
                bookingObjectResponse.getGuest().setMembershipId(bookingObjectRequest.getGuest().getMembershipId());
               bookingObjectResponse.getGuest().setAuthToken(bookingObjectRequest.getLoggedInUser().getAuthToken());
                bookingObjectResponse.getGuest().setCustomerHash(bookingObjectRequest.getLoggedInUser().getCustomerHash());
            }

            return handlingBookingSuccess(objectMapper, slingHttpServletRequest, bookingObjectRequest,
                    bookingObjectResponse);
        } else {
            LOG.debug("All rooms are unavailable");
            return objectMapper.writeValueAsString(bookingObjectResponse);
        }
    }

    /**
     * @param objectMapper
     * @param slingHttpServletRequest
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @return
     * @throws HotelBookingException
     * @throws IOException
     * @throws RepositoryException
     * @throws ParseException
     * @throws LoginException
     * @throws Exception
     */
    private String handlingBookingSuccess(ObjectMapper objectMapper, SlingHttpServletRequest slingHttpServletRequest,
            BookingObject bookingObjectRequest, BookingObject bookingObjectResponse)
            throws HotelBookingException, RepositoryException, IOException, ParseException, LoginException {

        LOG.info("Booking Done: Success");

        setInfoFromRequestToResponse(bookingObjectRequest, bookingObjectResponse);
        String bookingResponseString = objectMapper.writeValueAsString(bookingObjectResponse);
        LOG.debug("After setting info from bookingRequest to BookingResponse {}", bookingResponseString);

        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = fetchIndividualWebsiteAllConfig(
                slingHttpServletRequest);

        TajHotelBrandsETCConfigurations etcWebsiteConfig = getEtcWebsiteConfig(objectMapper, bookingObjectResponse,
                individualWebsiteAllConfig);

        bookingObjectResponse.setHotel(
                fetchHotelDetails(objectMapper, slingHttpServletRequest, bookingObjectRequest, bookingObjectResponse,
                        etcWebsiteConfig.getContentRootPath(), individualWebsiteAllConfig.isAmaBooking()));

        if (bookingObjectResponse.getPayments().isCreditCardRequired()
                && bookingObjectResponse.getPayments().isPayAtHotel()) {
            setPaymentCardNumber(bookingObjectRequest, bookingObjectResponse);
        }

        if (!bookingObjectResponse.isPartialBooking()) {
            LOG.info("BookHotelInitiateServlet : While booking  all rooms are available");
            // [TIC-FLOW]
            if (bookingObjectRequest.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                    && bookingObjectRequest.getPayments().getTicpoints().isPointsPlusCash()) {
            	LOG.info("set tic points in booking object reponse");
            	//bookingObjectResponse.getPayments().getTicpoints().setRedeemTicOrEpicurePoints(true);
            	bookingObjectResponse.getPayments().getTicpoints().setNoTicPoints(bookingObjectRequest.getPayments().getTicpoints().getNoTicPoints());
            }
            return handlingAllRoomsBooked(bookingObjectResponse, objectMapper, etcWebsiteConfig);
        } else {
            LOG.info("BookHotelInitiateServlet : While booking  some rooms Unavailable Found");
            if (StringUtils.isNotBlank(bookingObjectRequest.getItineraryNumber())
                    && bookingObjectRequest.getItineraryNumber().equals(bookingObjectResponse.getItineraryNumber())) {
                bookingObjectResponse.setItineraryNumber("");
            }
            return objectMapper.writeValueAsString(bookingObjectResponse);
        }
    }

    /**
     * @param objectMapper
     * @param bookingObjectResponse
     * @param individualWebsiteAllConfig
     * @return
     * @throws HotelBookingException
     * @throws IOException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    protected TajHotelBrandsETCConfigurations getEtcWebsiteConfig(ObjectMapper objectMapper,
            BookingObject bookingObjectResponse, TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig)
            throws HotelBookingException, IOException {

        String idForCCAvenueConfig = "";
        if (bookingObjectResponse.isCorporateBooking()) {
            LOG.info("Inside corporate booking and calling CCAvenue with corporate MerchantId ");
            idForCCAvenueConfig = ReservationConstants.CORPORATE_BOOKING_CCAVENUE;
        } else if (bookingObjectResponse.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                && bookingObjectResponse.getPayments().getTicpoints().isPointsPlusCash()) {
            LOG.info("Inside Tic domain and tic points and calling CCAvenue with tic MerchantId ");
          //  idForCCAvenueConfig = ReservationConstants.TIC_CCAVENUE; 
            idForCCAvenueConfig = ReservationConstants.BOOKING_CCAVENUE;    //For opel configuartion points+cash
        } else {
            LOG.info("Inside default booking and calling CCAvenue with default booking MerchantId");
            idForCCAvenueConfig = ReservationConstants.BOOKING_CCAVENUE;
        }

        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(idForCCAvenueConfig);
        if (etcWebsiteConfig == null) {
            LOG.info("etcWebsiteConfig getting null, for configurationType: {}", idForCCAvenueConfig);
            throw new HotelBookingException(BookingConstants.CONFIG_EXCEPTION_MESSAGE);
        }
        String debugString = "configurationType: " + idForCCAvenueConfig + ", configurations :-> "
                + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOG.info(debugString);
        return etcWebsiteConfig;
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @throws HotelBookingException
     */
    protected void setPaymentCardNumber(BookingObject bookingObjectRequest, BookingObject bookingObjectResponse)
            throws HotelBookingException {
        LOG.info("Partial availability - Pay at hotel selected");
        if (bookingObjectRequest.getPayments() != null && bookingObjectRequest.getPayments().getTicpoints() != null
                && bookingObjectRequest.getPayments().getTicpoints().getPointsType() != null
                && bookingObjectRequest.getPayments().getTicpoints().getPointsType().length() > 0) {
            bookingObjectResponse.getPayments()
                    .setCardNumber(changeCardNumber(bookingObjectResponse.getPayments().getCardNumber(),
                            bookingObjectRequest.getPayments().getTicpoints().getPointsType()));
        } else {
            bookingObjectResponse.getPayments()
                    .setCardNumber(changeCardNumber(bookingObjectResponse.getPayments().getCardNumber(), "NAN"));
        }
        LOG.debug("changing card number to last four digits for security purpose: {}",
                bookingObjectResponse.getPayments().getCardNumber());
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     */
    protected void setInfoFromRequestToResponse(BookingObject bookingObjectRequest,
            BookingObject bookingObjectResponse) {

        if (bookingObjectResponse.getPayments() == null) {
            bookingObjectResponse.setPayments(bookingObjectRequest.getPayments());
        }

        bookingObjectResponse.setCorporateBooking(bookingObjectRequest.isCorporateBooking());

        if (bookingObjectRequest.getFlight() != null) {
            bookingObjectResponse.setFlight(bookingObjectRequest.getFlight());
        }
        if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getGstNumber())) {
            bookingObjectResponse.getGuest().setGstNumber(bookingObjectRequest.getGuest().getGstNumber());
        }
        if (StringUtils.isNotBlank(bookingObjectRequest.getGuest().getSpecialRequests())) {
            bookingObjectResponse.getGuest().setSpecialRequests(bookingObjectRequest.getGuest().getSpecialRequests());
        }

        bookingObjectResponse
                .setRoomList(addingBookingFailedRoomsToResponse(bookingObjectRequest, bookingObjectResponse));
    }

    /**
     * @param request
     * @return
     * @throws HotelBookingException
     */
    private TajhotelsBrandAllEtcConfigBean fetchIndividualWebsiteAllConfig(SlingHttpServletRequest request)
            throws HotelBookingException {
        LOG.info("request.getRequestURL(): {}", request.getRequestURL());
        LOG.info("request.getRequestURI(): {}", request.getServerName());

        String websiteId = request.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "www.tajhotels.com";
        }

        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = tajHotelsBrandsETCService.getEtcConfigBean();
        if (etcAllBrandsConfig == null) {
            LOG.info(
                    "etcAllBrandsConfig getting null. \n  ****  Please verify CCAvenue configaration are proper or not.  ****  ");
            throw new HotelBookingException(BookingConstants.CONFIG_EXCEPTION_MESSAGE);
        }
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOG.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(BookingConstants.CONFIG_EXCEPTION_MESSAGE);
        }
        return individualWebsiteAllConfig;
    }

    /**
     * @param objectMapper
     * @param slingHttpServletRequest
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @param hotelContentRootPath
     * @param isAmaBooking
     * @return
     * @throws RepositoryException
     * @throws IOException
     */
    private Hotel fetchHotelDetails(ObjectMapper objectMapper, SlingHttpServletRequest slingHttpServletRequest,
            BookingObject bookingObjectRequest, BookingObject bookingObjectResponse, String hotelContentRootPath,
            boolean isAmaBooking) throws RepositoryException, IOException {

        HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();

        LOG.info("getting response for hotel details. hotel id : {}", bookingObjectResponse.getHotelId());

        String roomTypeCode = "";

        if (isAmaBooking) {
            roomTypeCode = bookingObjectResponse.getRoomList().get(0).getRoomTypeCode();
        }

        String inputsToGetHotelDetails = "isAmaBooking: " + isAmaBooking + ", ContentRootPath: " + hotelContentRootPath
                + ", HotelId: " + bookingObjectResponse.getHotelId() + ", roomTypeCode: " + roomTypeCode;
        LOG.info(inputsToGetHotelDetails);

        Hotel hoteldetails = fetchDetails.getHotelDetails(slingHttpServletRequest, bookingObjectResponse.getHotelId(),
                hotelContentRootPath, roomTypeCode);

        String hotelDetailsString = objectMapper.writeValueAsString(hoteldetails);
        LOG.info("HotelDetails {}", hotelDetailsString);

        if (bookingObjectRequest != null && bookingObjectRequest.getHotel() != null
                && StringUtils.isNotBlank(bookingObjectRequest.getHotel().getHotelLocationId())) {
            hoteldetails.setHotelLocationId(bookingObjectRequest.getHotel().getHotelLocationId());
        }
        return hoteldetails;
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @return
     */
    private List<Room> addingBookingFailedRoomsToResponse(BookingObject bookingObjectRequest,
            BookingObject bookingObjectResponse) {
        if (bookingObjectResponse.getRoomList() != null && bookingObjectRequest.getRoomList() != null) {

            bookingObjectRequest
                    .setRoomList(settingStatusTRUEIfRoomBooked(bookingObjectRequest, bookingObjectResponse));

            for (int k = 0; k < bookingObjectRequest.getRoomList().size(); k++) {
                if (!bookingObjectRequest.getRoomList().get(k).isBookingStatus()) {
                    bookingObjectResponse.getRoomList().add(bookingObjectRequest.getRoomList().get(k));
                }
            }
            LOG.debug("After adding missing rooms in response: {}", bookingObjectResponse);
        }
        return bookingObjectResponse.getRoomList();
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @return
     */
    private List<Room> settingStatusTRUEIfRoomBooked(BookingObject bookingObjectRequest,
            BookingObject bookingObjectResponse) {

        LOG.debug("Starting: settingStatusTRUEIfRoomBooked()");
        for (int i = 0; i < bookingObjectResponse.getRoomList().size(); i++) {
            boolean checkingValues = true;
            for (int j = 0; j < bookingObjectRequest.getRoomList().size(); j++) {
                if (!bookingObjectRequest.getRoomList().get(j).isBookingStatus() && checkingValues
                        && (bookingObjectRequest.getRoomList().get(j).getRoomTypeCode()
                                .equalsIgnoreCase(bookingObjectResponse.getRoomList().get(i).getRoomTypeCode()))
                        && (bookingObjectRequest.getRoomList().get(j).getRatePlanCode()
                                .equalsIgnoreCase(bookingObjectResponse.getRoomList().get(i).getRatePlanCode()))
                        && (bookingObjectRequest.getRoomList().get(j).getNoOfAdults() == bookingObjectResponse
                                .getRoomList().get(i).getNoOfAdults())
                        && (bookingObjectRequest.getRoomList().get(j).getNoOfChilds() == bookingObjectResponse
                                .getRoomList().get(i).getNoOfChilds())) {
                    checkingValues = addingPromocodeAndStatus(bookingObjectRequest, bookingObjectResponse, i, j);
                }
            }
        }

        LOG.debug("bookingObjectRequest RoomList: {}", bookingObjectRequest.getRoomList());
        LOG.debug("Ending: settingStatusTRUEIfRoomBooked()");

        return bookingObjectRequest.getRoomList();
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @param i
     * @param j
     * @return
     */
    protected boolean addingPromocodeAndStatus(BookingObject bookingObjectRequest, BookingObject bookingObjectResponse,
            int i, int j) {
        boolean checkingValues;
        checkingValues = false;
        bookingObjectRequest.getRoomList().get(j).setBookingStatus(true);
        bookingObjectResponse.getRoomList().get(i)
                .setRoomTypeName(bookingObjectRequest.getRoomList().get(j).getRoomTypeName());
        if (StringUtils.isNotBlank(bookingObjectRequest.getRoomList().get(j).getPromoCode())) {
            bookingObjectResponse.getRoomList().get(i)
                    .setPromoCode(bookingObjectRequest.getRoomList().get(j).getPromoCode());
        } else {
            bookingObjectResponse.getRoomList().get(i).setPromoCode("");
        }
        return checkingValues;
    }

    /**
     * @param bookingObjectRequest
     * @param bookingObjectResponse
     * @param objectMapper
     * @param etcWebsiteConfig
     * @return
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     * @throws HotelBookingException
     * @throws ParseException
     * @throws LoginException
     * @throws Exception
     */
    private String handlingAllRoomsBooked(BookingObject bookingObjectResponse, ObjectMapper objectMapper,
            TajHotelBrandsETCConfigurations etcWebsiteConfig)
            throws IOException, ParseException, LoginException, HotelBookingException {
        if (bookingObjectResponse.getPayments().isPayAtHotel()) {
            LOG.debug("All Rooms are available - PayAtHotel selected");
            return objectMapper.writeValueAsString(bookingObjectResponse);
        } else {
            LOG.debug("All Rooms are available - Pay online selected");

            PaymentHandler paymentHandler = new PaymentHandler();
            // [TIC-FLOW]
            if (bookingObjectResponse.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                    && bookingObjectResponse.getPayments().getTicpoints().isPointsPlusCash()) {
                LOG.info("Inside Tic domain and tic points");
                
                ticOrEpicurePointsCalculation(bookingObjectResponse);
            }

            String bookingResponseString = objectMapper.writeValueAsString(bookingObjectResponse);
            LOG.info("Final bookingResponse Before passing to PaymentHandler: {}", bookingResponseString);
            return paymentHandler.getCCAvenueDetails(bookingObjectResponse.getItineraryNumber(), bookingObjectResponse,
                    etcWebsiteConfig);
        }

    }

    /**
     * @param bookingObjectResponse
     * @param bookingObjectRequest
     * @return
     * @throws LoginException
     * @throws HotelBookingException
     */
    private BookingObject ticOrEpicurePointsCalculation(BookingObject bookingObjectResponse)
            throws LoginException, HotelBookingException {
    	
    	LOG.info("bookingObjectResponse.getTotalAmountAfterTax() : {}", bookingObjectResponse.getTotalAmountAfterTax());
    	LOG.info("bookingObjectResponse.getTotalPaidAmount() : {}", bookingObjectResponse.getTotalPaidAmount());
    	LOG.info("bookingObjectResponse.getNoOfTicPoints() : {}", bookingObjectResponse.getPayments().getTicpoints().getNoTicPoints());
        int totalTicCartPrice = currencyConversionToINR(bookingObjectResponse.getCurrencyCode(),
                bookingObjectResponse.getTotalAmountAfterTax());
        if (totalTicCartPrice == 0) {
            throw new HotelBookingException("Issue with TIC or Epicure points conversion.");
        }

        int totalAfterRedemption = 0;

        if (!bookingObjectResponse.getPayments().getTicpoints().getCurrencyConversionRate().isEmpty()) {
            bookingObjectResponse.setCurrencyCode("INR");
            bookingObjectResponse.getPayments().setCurrencyCode("INR");
        }

        if (bookingObjectResponse.getPayments().getTicpoints().getPointsType().equals("TIC")) {
            int ticPoints = bookingObjectResponse.getPayments().getTicpoints().getNoTicPoints();
            if (((totalTicCartPrice * 50) / 100) > ticPoints) {
                LOG.info("((totalTicCartPrice * 50) / 100) : {}", ((totalTicCartPrice * 50) / 100));
                LOG.info("ticPoints : {}", ticPoints);
                throw new HotelBookingException("Minimum 50% TIC Points redemption required to do this reservation.");
            }
            totalAfterRedemption = totalTicCartPrice - ticPoints;
            bookingObjectResponse.setTotalAmountAfterTax(String.valueOf(totalAfterRedemption));
        } else if (bookingObjectResponse.getPayments().getTicpoints().getPointsType().equals("EPICURE")) {
            int epicurePoints = bookingObjectResponse.getPayments().getTicpoints().getNoEpicurePoints();
            double epicurePointsCart = (double) totalTicCartPrice / 2;
            totalAfterRedemption = (int) Math.ceil(epicurePointsCart) - epicurePoints;
            bookingObjectResponse.setTotalAmountAfterTax(String.valueOf(totalAfterRedemption * 10));
        } else if (bookingObjectResponse.getPayments().getTicpoints().getPointsType().equals("TAP")
                || bookingObjectResponse.getPayments().getTicpoints().getPointsType()
                        .equals(BookingConstants.TIC_POINTS_TAPME)) {
            int ticPoints = bookingObjectResponse.getPayments().getTicpoints().getNoTicPoints();
            totalAfterRedemption = totalTicCartPrice - ticPoints;
            bookingObjectResponse.setTotalAmountAfterTax(String.valueOf(totalAfterRedemption * 10));
        }

        return bookingObjectResponse;

    }

    private Map<String, String> getCurrencyConversionRate() throws LoginException {
        LOG.info("Method Started: getCurrencyConversionRate()");
        ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
        Resource currencyConversionResource = resourceResolver
                .getResource("/content/shared-content/global-shared-content/currency-conversion");
        Map<String, String> currencyMap = new HashMap<>();
        if (currencyConversionResource != null) {
            for (Resource currencyConversionNode : currencyConversionResource.getChildren()) {
                currencyMap.put(currencyConversionNode.getName(),
                        currencyConversionNode.getValueMap().get("conversionRate", String.class));
            }
        } else {
            LOG.error("Currency node is null. No data available to send for currency conversion");
        }
        return currencyMap;
    }

    private int currencyConversionToINR(String currencyCode, String totalAmountAfterTax) throws LoginException {

        Map<String, String> currencyConversionMap = getCurrencyConversionRate();
        String currencyConversionRate = currencyConversionMap.get(currencyCode + "_INR");
        LOG.info("currencyCode : {}", currencyCode);
        LOG.info("totalAmountAfterTax : {}", totalAmountAfterTax);
        LOG.info("currencyConversionRate : {}", currencyConversionRate);

        if (StringUtils.isNotBlank(currencyConversionRate)) {
            return Math.round(Float.valueOf(currencyConversionRate) * Float.valueOf(totalAmountAfterTax));
        }
        return 0;

    }

    /**
     * @param cardNumber
     * @param cardType
     * @return
     * @throws HotelBookingException
     */
    public String changeCardNumber(String cardNumber, String cardType) throws HotelBookingException {

        LOG.info("Inside method changeCardNumber()");
        String newCardNumber = null;
        if (cardType != null
                && (cardType.compareToIgnoreCase("TAP") == 0 || cardType.compareToIgnoreCase("TAPPMe") == 0)) {
            LOG.debug("Membership login card found");
        } else {
            if (cardNumber.length() < 15) {
                throw new HotelBookingException("Cardnumber length is invalid");
            } else {
                newCardNumber = (cardNumber.substring(0, cardNumber.length() - 4).replaceAll("[0-9]", "*"))
                        .concat(cardNumber.substring(cardNumber.length() - 4));
            }

        }

        LOG.debug("New Card number : {}", newCardNumber);
        LOG.info("Leaving method changeCardNumber()");
        return newCardNumber;
    }

    /**
     * @param bookingObjectRequest
     * @throws HotelBookingException
     */
    private void checkForBookingMandatoryFields(BookingObject bookingObjectRequest) throws HotelBookingException {

        LOG.debug("Started check for booking mandatory fields");
        if (StringUtils.isBlank(bookingObjectRequest.getCheckInDate())) {
            throw new HotelBookingException("CheckIn date not Found");
        }
        if (StringUtils.isBlank(bookingObjectRequest.getCheckOutDate())) {
            throw new HotelBookingException("CheckOut date not Found");
        }
        if (StringUtils.isBlank(bookingObjectRequest.getHotelId())) {
            throw new HotelBookingException("HotelID not Found");
        }
        if (StringUtils.isBlank(bookingObjectRequest.getChainCode())) {
            throw new HotelBookingException("Chain id not Found");
        }
        if (bookingObjectRequest.getGuest() == null) {
            throw new HotelBookingException("Guest details not Found");
        } else {
            checkForMandatoryGuestDetails(bookingObjectRequest.getGuest());
        }
        if (bookingObjectRequest.getPayments() == null) {
            throw new HotelBookingException("Payment details not Found");

        } else if (bookingObjectRequest.getPayments().isCreditCardRequired()
                && bookingObjectRequest.getPayments().isPayAtHotel()) {
            checkForMandatoryPaymentDetails(bookingObjectRequest.getPayments());
        }
        LOG.debug("Check for booking mandatory fields successfully.");
    }

    /**
     * @param payments
     * @throws HotelBookingException
     */
    private void checkForMandatoryPaymentDetails(Payments payments) throws HotelBookingException {

        LOG.debug("Started check for booking payment mandatory fields");
        if (payments.getTicpoints() != null && payments.getTicpoints().getPointsType() != null
                && payments.getTicpoints().getPointsType().length() > 0
                && (payments.getTicpoints().getPointsType().compareToIgnoreCase("TAP") == 0
                        || payments.getTicpoints().getPointsType().compareToIgnoreCase("TAPPMe") == 0)) {
            LOG.debug("Skiping card validation for Membership user");
        } else {
            if (StringUtils.isBlank(payments.getNameOnCard())) {
                throw new HotelBookingException("Name on card not Found");
            }
            if (StringUtils.isBlank(payments.getCardNumber())) {
                throw new HotelBookingException("Card number not Found");
            }
            if (StringUtils.isBlank(payments.getExpiryMonth())) {
                throw new HotelBookingException("Card expiry details not Found");
            }
            if (StringUtils.isBlank(payments.getCardType())) {
                throw new HotelBookingException("Card type validation failed. Please reenter card number");
            }
        }
        LOG.debug("Check for booking payment mandatory fields successfully.");
    }

    /**
     * @param guest
     * @throws HotelBookingException
     */
    private void checkForMandatoryGuestDetails(Guest guest) throws HotelBookingException {

        LOG.debug("Started check for booking guest mandatory fields");
        if (StringUtils.isBlank(guest.getFirstName())) {
            throw new HotelBookingException("Guest firstname not Found");
        }
        if (StringUtils.isBlank(guest.getLastName())) {
            throw new HotelBookingException("Guest lastname not Found");
        }
        if (StringUtils.isBlank(guest.getEmail())) {
            throw new HotelBookingException("Guest email not Found");
        }
        if (StringUtils.isBlank(guest.getPhoneNumber())) {
            throw new HotelBookingException("Guest phone number not Found");
        }
        LOG.debug("Check for booking guest mandatory fields successfully.");
    }

    /**
     * @param guestString
     * @param paymentsString
     * @param roomString
     * @param flightDetailsString
     * @param loggedInUserString
     * @param couponCode
     * @param hotelLocationId
     */
    private void loggingObjectStringRequestParms(String guestString, String paymentsString, String roomString,
            String flightDetailsString, String loggedInUserString, String couponCode, String hotelLocationId) {
        LOG.debug("--------------> Printing ObjectStringRequestParms <--------------");
        LOG.debug("guestString : {}", guestString);
        LOG.debug("paymentsString : {}", paymentsString);
        LOG.debug("roomString : {}", roomString);
        LOG.debug("flightDetailsString : {}", flightDetailsString);
        LOG.debug("loggedInUser : {}", loggedInUserString);
        LOG.debug("Coupon code : {}", couponCode);
        LOG.debug("hotelLocationId : {}", hotelLocationId);
    }

    /**
     * @param ignoreDetailsRequestMap
     */
    private void ignoreBooking(Map<String, String> ignoreDetailsRequestMap) {
        if (ignoreDetailsRequestMap != null
                && StringUtils.isNotBlank(ignoreDetailsRequestMap.get(BookingConstants.ITINERARY_NUMBER))
                && StringUtils.isNotBlank(ignoreDetailsRequestMap.get(BookingConstants.FORM_HOTEL_ID))) {
            LOG.info("Started ignoreBooking() for ItineraryNumber: {}", ignoreDetailsRequestMap);
            BookingObject bookingIgnoreRequest = new BookingObject();
            bookingIgnoreRequest.setItineraryNumber(ignoreDetailsRequestMap.get(BookingConstants.ITINERARY_NUMBER));
            bookingIgnoreRequest.setHotelId(ignoreDetailsRequestMap.get(BookingConstants.FORM_HOTEL_ID));
            bookingIgnoreRequest.setResStatus(ResStatus.Ignore);
            BookingObject bookingIgnoreResponse = new BookingObject();

            LOG.info("-----Passing on ignore request to the booking handler-----");
            try {
                bookingIgnoreResponse = bookingHandler.createRoomReservation(bookingIgnoreRequest);
            } catch (Exception e) {
                LOG.error("Ignore exception: {}", e.getMessage());
                LOG.error("Ignore exception: {}", e);
            }

            LOG.info("-----Received Response from the booking handler-----");

            if (bookingIgnoreResponse.isSuccess()) {
                LOG.info("Booking got Ignored Successfully: {}", bookingIgnoreResponse.getItineraryNumber());
            } else {
                LOG.info("Error while ignoring:  - {}", bookingIgnoreResponse.getErrorMessage());
            }
        } else {
            LOG.info("Aborting ignoreBooking(), Because Reservation not yet Initiated");
        }
    }
}
