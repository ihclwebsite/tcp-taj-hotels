package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import com.ihcl.tajhotels.servlets.DonationServlet;
import com.sun.mail.iap.ByteArray;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
//import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.pdfbox.io.IOUtils;
//import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.jcr.Binary;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.filelibrary.client.api.Asset;
import com.ccavenue.security.AesCryptUtil;
//import com.day.cq.search.result.Hit;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.DonationRequest;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.config.ConfigurationService;
import com.ihcl.core.services.config.DonationConfirmationConfigurationService;


/**
 * @author TCS
 *
 */

@Component(service = Servlet.class,
property = { "sling.servlet.paths=" + "/bin/donationConfirmation", "sling.servlet.methods={GET,POST}",
        "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class DonationConfirmationServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = -187125409109376508L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DonationConfirmationServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;
    
    @Reference
    private DonationConfirmationConfigurationService DonationConfirmation;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
	@Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {
		slingHttpServletResponse.setHeader("Cache-Control","no-cache, no-store, must-revalidate, max-age=0, s-manage=0");
		slingHttpServletResponse.setHeader("Expires", "0");
        LOGGER.debug("Started DonationConfirmationServlet.doPost().");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String paymentStatus = "";
        try {

            Map<String, Object> objectMap = fetchgETCConfigurations(slingHttpServletRequest, objectMapper);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = (TajHotelBrandsETCConfigurations) objectMap
                    .get("etcWebsiteConfig");

            String encResp = slingHttpServletRequest.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME);

            
            if (StringUtils.isNotBlank(encResp)) {

                CCAvenueParams ccAvenueParams = processCCavenuedata(encResp, etcWebsiteConfig.getCcAvenueWorkingKey());
                objectNode.put("ccAvenueTrackingId", ccAvenueParams.getTrackingId());
                objectNode.put("paymentStatus", ccAvenueParams.getPaymentstatus());
                String loggerString = "ccAvenueTrackingId: " + ccAvenueParams.getTrackingId() + "; paymentStatus: "
                        + ccAvenueParams.getPaymentstatus();
                LOGGER.info(loggerString);
                
                LOGGER.trace("ccAvenueParams ", ccAvenueParams);
                
                if (ccAvenueParams.getPaymentstatus()) {
                    LOGGER.info("Paymentstatus: {}", ccAvenueParams.getPaymentstatus());
                    paymentStatus = "Success";
//                    Hotel hotelDetails = (Hotel) hotelAndHamperDetailsMap.get("hotelDetails");
                    objectNode.put(BookingConstants.STATUS, true);
                    String orderId = "DO"+fetchCookieOrderId(slingHttpServletRequest);
                    String formSessionDetails = fetchCookieFromRequest(slingHttpServletRequest); 
                    Base64.Decoder decoder = Base64.getDecoder();
                    String formSessionAdditionalDetails = new String(decoder.decode(formSessionDetails)); 
                    LOGGER.info("formSessionAdditionalDetails: {}", formSessionAdditionalDetails);
//                  To send Email.
                    if(orderId.equalsIgnoreCase(ccAvenueParams.getOrderId())) {
                    	LOGGER.info("ccAvenueParams.getOrderId(): {} orderId :{}", ccAvenueParams.getOrderId(),orderId);
	                    ObjectMapper objMapper = new ObjectMapper(); 
	                    DonationRequest user = objMapper.readValue(formSessionAdditionalDetails, DonationRequest.class);
	                    LOGGER.info("user Firstname: {}", user.getFirstName());
	//                    int receiptNumber = (int)(10000000.0 * Math.random());
	                    user.setOrderId(ccAvenueParams.getOrderId());
	        			user.setReceiptNumber(ccAvenueParams.getOrderId().substring(2));
	        			user.setOrderIdMismatch("false");
	                    captureUserData(user);
	                    sendEmailToCustomerAndHotelAuthority(ccAvenueParams, DonationConfirmation, user);
                    }else {
                    	LOGGER.info("Different Order Id, Sending sendEmailPaymentInterrupted");
                    	ObjectMapper objMapper = new ObjectMapper(); 
	                    DonationRequest user = objMapper.readValue(formSessionAdditionalDetails, DonationRequest.class);
                    	paymentStatus = "false";
                    	user.setMailStatus(false);
                    	user.setOrderIdMismatch("true");
                    	captureUserData(user);
                    	sendEmailPaymentInterrupted(ccAvenueParams, user);
                    }
                } else {
                    LOGGER.info("Paymentstatus: {}", ccAvenueParams.getPaymentstatus());
                    paymentStatus = "Failed";
//                    Hotel hotelDetails = (Hotel) hotelAndHamperDetailsMap.get("hotelDetails");
                    objectNode.put(BookingConstants.STATUS, true);
                    String formSessionDetails = fetchCookieFromRequest(slingHttpServletRequest); 
                    Base64.Decoder decoder = Base64.getDecoder();
                    String formSessionAdditionalDetails = new String(decoder.decode(formSessionDetails)); 
                    LOGGER.info("formSessionAdditionalDetails: {}", formSessionAdditionalDetails);
                    ObjectMapper objMapper = new ObjectMapper();
                    DonationRequest user = objMapper.readValue(formSessionAdditionalDetails, DonationRequest.class);
                    LOGGER.info("user Firstname: {}", user.getFirstName());
                    captureUserData(user);
                }

            } else {
                LOGGER.info("Response from CCAvenue getting null/empty. (encResp).");
            }

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, hbe.getMessage());
            LOGGER.debug("Missing information in gift hamper creation : {}", hbe.getMessage());
            LOGGER.error("Missing information in gift hamper creation : {}", hbe);

        } catch (Exception e) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, "Gift hamper creation got interrupted.");
            LOGGER.debug("Exception while gift hamper creation : {}", e.getMessage());
            LOGGER.error("Exception while gift hamper creation : {}", e);

        } finally {
            writeResponseOnServer(slingHttpServletResponse, objectNode.toString(), paymentStatus);
        }
    }

    private boolean sendEmailPaymentInterrupted(CCAvenueParams ccAvenueParams, DonationRequest user) {
    	LOGGER.info("inside sendEmailToCustomerAndHotelAuthority ", ccAvenueParams);
        LOGGER.info(" Sending Email to Sender reference: {}", user.getEmail());
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
//        String currentDate = date.getDate()+"/"+date.getMonth()+"/"+date.getYear(); 
        String subject = "IHCL website donation transaction security issue";
        String htmlMessage = "<table>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td height=\"10\"></td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> Dear Team, </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 10px;\"> This is a system generated mail </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> There was an occurance of faulty transaction, which was identified. The\r\n" + 
        		"                amount deducted should be reimbursed to the below customer </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <td style=\"font-size: 14px;\"> Transaction details </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> CCavenue orderId : "+ ccAvenueParams.getOrderId()+"</td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> CCavenue trackingId  : "+ ccAvenueParams.getTrackingId()+"</td>\r\n" + 
        		"        </tr>\r\n" +
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> Email :"+user.getEmail()+" </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> Amount : "+user.getDonation() +"Rs </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> First name : "+user.getFirstName() +"</td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> Last name : "+user.getLastName() +"</td>\r\n" + 
        		"        </tr>\r\n" + 
        		"        <tr>\r\n" + 
        		"            <td style=\"font-size: 14px;\"> Time : "+dateFormat.format(date)+"</td>\r\n" + 
        		"        </tr>\r\n" + 
        		"    </table>";
		boolean isSendEmailSuccess = iEmailService.donationInterruptedMail(subject, htmlMessage);

		LOGGER.error("SendQuoteEmail response: " + isSendEmailSuccess);
        if (isSendEmailSuccess) {
        	LOGGER.error("Email has been successfully ");
//          returnMessage = writeJsonToResponse(objectNode, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS,true);
        } else {
//          response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//          returnMessage = writeJsonToResponse(objectNode, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE,false);
        }
        return isSendEmailSuccess;
//		return false;
	}

	/**
     * @param slingHttpServletRequest
     * @return
     */
    private String fetchCookieFromRequest(SlingHttpServletRequest slingHttpServletRequest) {
        String cookies = "";
        String formSessionAdditionalDetails = "";
        if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("cookie"))) {
            cookies = slingHttpServletRequest.getHeader("cookie");
            LOGGER.info("Cookies header name : cookie {}", cookies);
        } else if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("Cookie"))) {
            cookies = slingHttpServletRequest.getHeader("Cookie");
            LOGGER.info("Cookies header name : Cookie {}", cookies);
        }
        if (StringUtils.isNotBlank(cookies)) {
            String[] cookieParamsArray = cookies.split("; ");
            for (String cookieParams : cookieParamsArray) {
                LOGGER.info("cookieParams: {}", cookieParams);
                String[] cookieNameAndValuePair = cookieParams.split("=");
                LOGGER.info("cookieNameAndValuePair[0]: {}", cookieNameAndValuePair[0]);
                if (cookieNameAndValuePair[0].equalsIgnoreCase("senderDetails")) {
                    LOGGER.info("cookieNameAndValuePair[1]: {}", cookieNameAndValuePair[1]);
                    formSessionAdditionalDetails = cookieNameAndValuePair[1];
                }
            }
        }

        LOGGER.info("formSessionAdditionalDetails: {}", formSessionAdditionalDetails);
        return formSessionAdditionalDetails;
    }
    
    private String fetchCookieOrderId(SlingHttpServletRequest slingHttpServletRequest) {
        String cookies = "";
        String formSessionAdditionalDetails = "";
        if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("cookie"))) {
            cookies = slingHttpServletRequest.getHeader("cookie");
            LOGGER.info("Cookies header name : cookie {}", cookies);
        } else if (StringUtils.isNotBlank(slingHttpServletRequest.getHeader("Cookie"))) {
            cookies = slingHttpServletRequest.getHeader("Cookie");
            LOGGER.info("Cookies header name : Cookie {}", cookies);
        }
        if (StringUtils.isNotBlank(cookies)) {
            String[] cookieParamsArray = cookies.split("; ");
            for (String cookieParams : cookieParamsArray) {
                LOGGER.info("cookieParams: {}", cookieParams);
                String[] cookieNameAndValuePair = cookieParams.split("=");
                LOGGER.info("cookieNameAndValuePair[0]: {}", cookieNameAndValuePair[0]);
                if (cookieNameAndValuePair[0].equalsIgnoreCase("session")) {
                    LOGGER.info("cookieNameAndValuePair[1]: {}", cookieNameAndValuePair[1]);
                    formSessionAdditionalDetails = cookieNameAndValuePair[1];
                }
            }
        }

        LOGGER.info("formSessionAdditionalDetails: {}", formSessionAdditionalDetails);
        return formSessionAdditionalDetails;
    }

    private boolean sendEmailToCustomerAndHotelAuthority(CCAvenueParams ccAvenueParams,
    		DonationConfirmationConfigurationService giftHamperConfigurationService, DonationRequest user) throws IOException,LoginException,PathNotFoundException,ValueFormatException,RepositoryException {

        LOGGER.info("inside sendEmailToCustomerAndHotelAuthority ", ccAvenueParams);
        LOGGER.info(" Sending Email to Sender reference: {}", user.getEmail());
        String subject = "Donation successful";
        String htmlMessage = getHtmlMessage(user);
//		String to = ccAvenueParams.getBillingEmail();
        String to = user.getEmail();
		String cc = "";
		String bcc = "";
		PDDocument doc = createPDDocument(user,ccAvenueParams);
//		boolean isSendEmailSuccess = iEmailService.sendEMail(subject, htmlMessage, to, cc, bcc, file);
		boolean isSendEmailSuccess = iEmailService.sendPdfMail(subject, htmlMessage, to, cc, bcc, doc,user.getFirstName()+user.getLastName()+user.getOrderId());
		LOGGER.error("SendQuoteEmail response: " + isSendEmailSuccess);
        if (isSendEmailSuccess) {
        	LOGGER.error("Email has been successfully ");
//          returnMessage = writeJsonToResponse(objectNode, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS,true);
        } else {
//          response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//          returnMessage = writeJsonToResponse(objectNode, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE,false);
        }
        return isSendEmailSuccess;
    }

    private PDDocument createPDDocument(DonationRequest user, CCAvenueParams ccAvenueParams) throws IOException, LoginException, RepositoryException {
    	String timeStamp = fetchCurrentTimeStamp();
        String[] splitTimeStamp = timeStamp.split(" ");
        String[] splitYearMonthDate = splitTimeStamp[0].split("/");
        String currentYear = splitYearMonthDate[0];
        String currentMonth = splitYearMonthDate[1];
        String currentDay = splitYearMonthDate[2];
		PDDocument doc = new PDDocument();
		try {
			LOGGER.error("Inside createPDDocument");
			PDPage page = new PDPage();
			doc.addPage(page);
			PDFont font = PDType1Font.HELVETICA_BOLD;
			PDImageXObject pdImage = PDImageXObject.createFromFile("/mnt/forms/Taj-welfare-logo.png",doc);
			PDImageXObject footerImage = PDImageXObject.createFromFile("/mnt/forms/Footer.png",doc);
			PDImageXObject signatureImage = PDImageXObject.createFromFile("/mnt/forms/signature.png",doc);
			PDPageContentStream contents = new PDPageContentStream(doc, page);
			contents.drawImage(pdImage, -50, 640);
			contents.drawImage(footerImage, 0, 0);
			contents.drawImage(signatureImage, 370, 270);
			addText(font, 15, 200, 650, "Taj Public Service Welfare Trust", contents);
			addText(font, 10, 250, 630, "2nd Floor, Mandlik House", contents);
			addText(font, 10, 280, 610, "Mandlik Road,", contents);
			addText(font, 10, 275, 590, "Colaba, Mumbai", contents);
			addText(font, 10, 60, 560, "Date: "+currentDay+"/"+currentMonth+"/"+currentYear, contents);
			addText(font, 10, 425, 560, "Receipt No : "+ user.getReceiptNumber(), contents);
			addText(font, 10, 60, 540, "Place: Mumbai", contents);
			addText(font, 15, 290, 500, "Receipt", contents);
			addText(font, 10, 60, 450, "Received from "+user.getFirstName()+" "+user.getLastName()+" [ PAN : "+ user.getPanCardNumber() +" ] an amount of Rs "+ user.getDonation(), contents);
			addText(font, 10, 60, 435, "(Rupees "+user.getDonation()+" only) on "+currentDay+"/"+currentMonth+"/"+currentYear+" towards objects of the Trust.  ", contents);
			addText(font, 8, 60, 400, "PAN: AABTT6081F", contents);
			addText(font, 8, 60, 390, "Registration No: E-25354 Mumbai dated 23-12-2008 ", contents);
			addText(font, 8, 60, 380, "Approval U/s 80G of the Income Tax Act, 1961 vide", contents);
			addText(font, 8, 60, 370, "Order No. DIT(E)/MC/80G/1431/2008/2008-09 ", contents);
			addText(font, 8, 60, 360, "Dated: 24-02-2009", contents);
			addText(font, 8, 425, 270, "Authorised Signatory", contents);
			addText(font, 8, 390, 255, "TAJ PUBLIC SERVICE WELFARE TRUST", contents);
			addText(font, 7, 60, 230, "In case of any questions/queries, please do write to us at tajtrust@ihcltata.com", contents);
			contents.close();
			doc.save("/mnt/forms/"+user.getFirstName()+user.getLastName()+user.getOrderId()+"Donation.pdf");
		} finally {
			doc.close();
		}
		return doc;
	}
    
    public static void addText(PDFont font, int fontSize, int offsetY, int offsetX, String text, PDPageContentStream contents) throws IOException {
		contents.beginText();
		contents.setFont(font, fontSize);
		contents.newLineAtOffset(offsetY, offsetX);
		contents.showText(text);
		contents.endText();
	}

	private void sendMailWithGiftHamper(CCAvenueParams ccAvenueParams, String toMail, String emailBodyTemplate,
            String mailSubject, String formSessionAdditionalDetails) {

        String giftHamperMailBody = emailBodyTemplate;

        giftHamperMailBody = giftHamperMailBody.replace("$CCAVENUETRACKINGID$", ccAvenueParams.getTrackingId());
        giftHamperMailBody = giftHamperMailBody.replace("$CCAVENUEORDERID$", ccAvenueParams.getOrderId());

        giftHamperMailBody = giftHamperMailBody.replace("$SENDERNAME$", ccAvenueParams.getBillingName());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDEREMAIL$", ccAvenueParams.getBillingEmail());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDERPHONENUMBER$", ccAvenueParams.getBillingTel());
        giftHamperMailBody = giftHamperMailBody.replace("$SENDERAMOUNT$", ccAvenueParams.getAmount());
        boolean status = iEmailService.sendEMail(mailSubject, giftHamperMailBody, toMail, null, null, null);
        if (status) {
            LOGGER.info("Gift Hamper conformation sent through email. Mail status: {}", status);
        } else {
            LOGGER.info("Issue found while mailing Gift Hamper. Mail status: {}", status);
        }

    }


    private void writeResponseOnServer(SlingHttpServletResponse slingHttpServletResponse, String responseString,
            String paymentStatus) {
        String methodName = "writeResponseOnServer";
        LOGGER.info("Method Entry: " + methodName);
        try {
            PrintWriter printWriter = slingHttpServletResponse.getWriter();
            LOGGER.info("redirecting using hidden form");
            LOGGER.info("Final giftHamperResponse : {}", responseString);
            String redirectUrl = DonationConfirmation.getConfirmationPagePath();
            if (paymentStatus == null || !paymentStatus.equalsIgnoreCase("success")) {
                redirectUrl = DonationConfirmation.getFailurePagePath();
            }
            LOGGER.info("Received redirection page path from configuration as: {}", redirectUrl);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_HTML);
            responseString = StringEscapeUtils.escapeEcmaScript(responseString);
            LOGGER.info("Final giftHamperResponseString which is setting as SessionData : {}", responseString);
            printWriter.print("<form id='giftHamperCreation' action='" + redirectUrl
                    + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
            printWriter.print("<input type='hidden' name='paymentStatus' value='" + paymentStatus + "'>");
            printWriter.print("</form>");
            printWriter.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                    + "sessionStorage.setItem('giftHamperResponseString', '" + responseString + "');"
                    + "sessionStorage.setItem('paymentStatus', '" + paymentStatus + "');"
                    + "document.getElementById(\"giftHamperCreation\").submit()" + "}" + "</script>");
            LOGGER.info("end of output stream");
            printWriter.close();

        } catch (Exception e) {
            LOGGER.error("Error while submitting form : {}", e.getMessage());
            LOGGER.debug("error while submitting form : {}", e);
        }
        LOGGER.info("Method Exit: " + methodName);

    }

    private Map<String, Object> fetchgETCConfigurations(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper) throws HotelBookingException, IOException {

        LOGGER.info("request.getRequestURL(): {}", slingHttpServletRequest.getRequestURL());
        LOGGER.info("request.getServerName(): {}", slingHttpServletRequest.getServerName());

        String websiteId = slingHttpServletRequest.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "tajhotels";
        }
        Map<String, Object> objectMap = new HashMap<>();
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get("donationCCAvenue");
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    "donationCCAvenue");
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        objectMap.put("contentRootPath", etcWebsiteConfig.getContentRootPath());
        String debugString = "WebsiteId: " + websiteId + ", ConfigurationType: "
                + "donationCCAvenue" + ", Configurations :-> "
                + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        objectMap.put("etcWebsiteConfig", etcWebsiteConfig);
        return objectMap;
    }

    private CCAvenueParams processCCavenuedata(String encResp, String ccAvenueWorkingKey)
            throws UnsupportedEncodingException {
        Map<String, String> responseMap = new LinkedHashMap<>();
        AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueWorkingKey);
        String decResp = aesUtil.decrypt(encResp);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
        String pair = null;
        String key = "";
        String value = "";
        while (tokenizer.hasMoreTokens()) {
            pair = tokenizer.nextToken();
            if (pair != null) {
                StringTokenizer strTok = new StringTokenizer(pair, "=");
                if (strTok.hasMoreTokens()) {
                    key = strTok.nextToken();
                    if (strTok.hasMoreTokens()) {
                        value = strTok.nextToken();
                        try {
                            responseMap.put(key, URLDecoder.decode(value, BookingConstants.CHARACTER_ENCOADING));
                        } catch (UnsupportedEncodingException usee) {
                            LOGGER.error("** Error occured while decoding CCAvenueParmas **");
                            throw usee;
                        }
                    }
                }
            }
        }
        return extractingCCAvenueParams(responseMap);
    }

    private CCAvenueParams extractingCCAvenueParams(Map<String, String> responseMap) {
        CCAvenueParams ccavenueParams = new CCAvenueParams();

        for (Map.Entry<String, String> entry : responseMap.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue();
            String str = mapKey + " ::: " + mapValue;
            LOGGER.info("CCAvenue params: {}", str);
        }

        ccavenueParams.setPaymentstatus(responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS)
                .equalsIgnoreCase(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE));

        ccavenueParams.setTrackingId(responseMap.get(BookingConstants.CCAVENUE_TRACKING_ID));
        ccavenueParams.setOrderId(responseMap.get("order_id"));
        ccavenueParams.setCurrency(responseMap.get("currency"));
        ccavenueParams.setAmount(responseMap.get("amount"));

        ccavenueParams.setBillingName(responseMap.get("billing_name"));
        ccavenueParams.setBillingTel(responseMap.get("billing_tel"));
        ccavenueParams.setBillingEmail(responseMap.get("billing_email"));
        
        return ccavenueParams;
    }
    private String getHtmlMessage(DonationRequest user) {
    	
    	String HtmlTemplate = "<body style=\"padding:0; margin:20px 0 0 0\">\r\n" + 
        		"    <table border=\"1\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"><tr>\r\n" + 
        		"            <td width=\"600\" valign=\"top\" style=\"border-top: 1px solid #aaa; border-right: 1px solid #aaa; border-left: 1px solid #aaa;background-color:\"#FFFFFF\";\">\r\n" + 
        		"                    <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\r\n" + 
        		"                        <tr><td valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"560\"><tr>\r\n" + 
        		"                            <td align=\"left\"><img src=\"https://www.tajhotels.com/content/dam/logos/ihcl-logo.png\" alt=\"Logo\" border=\"0\" style=\"display:block; width:75px\" /></td>\r\n" + 
        		"                            <td align=\"right\">\r\n" + 
        		"                                <a href=\"https://www.tajhotels.com/\" style=\"color:#d29751; text-decoration:none;\">tajhotels.com</a> \r\n" + 
        		"                            </td></tr></table>\r\n" + 
        		"                                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"560\">\r\n" + 
        		"                                    <tr>\r\n" + 
        		"                                        <td align=\"center\" style=\"color: ##d29751; font-size:24px;\">\r\n" + 
        		"                                            <table>\r\n" + 
        		"                                                <tr>\r\n" +
        		"                                                </tr>\r\n" + 
        		"                                                <tr><td height=\"10\" ></td></tr>\r\n" + 
        		"                                                <tr>\r\n" + 
        		"                                                    <td style=\"font-size: 14px;\">\r\n" + 
        		"                                                        Dear "+ user.getFirstName()+" "+user.getLastName()+",\r\n" + 
        		"                                                    </td>\r\n" + 
        		"                                                </tr>\r\n" + 
        		"                                                <tr>\r\n" + 
        		"                                                    <td style=\"font-size: 14px;\">\r\n" + 
        		"                                                            Please accept our heartfelt gratitude for your generous contribution towards the current objective to combat COVID19. Your financial support will help us continue our mission & aid the brave hearts.\r\n" + 
        		"                                                    </td>\r\n" + 
        		"                                                </tr>\r\n" + 
        		"                                            </table>\r\n" + 
        		"                                            <table border=\"0\" cellspacing=\"0\">\r\n" +
        		"                                                <tr>\r\n" + 
        		"                                                    <td align=\"center\" style=\"color:#aaa; font-size:16px;height: 20px;\">\r\n" + 
        		"															\r\n" + 
        		"                                                    </td>\r\n" + 
        		"                                                </tr>\r\n" +
        		"												 <tr>\r\n" + 
        		"        											<td style=\"font-size: 14px;\">\r\n" + 
        		"            											In case of any questions/queries, please do write to us at tajtrust@ihcltata.com \r\n" + 
        		"        											</td>\r\n" + 
        		"                                                </tr>\r\n" +
        		"												 <tr>\r\n" + 
        		"        											<td style=\"font-size: 14px;\">\r\n" + 
        		"            											*Your receipt is attached with the mail" + 
        		"        											</td>\r\n" + 
        		"                                                </tr>\r\n" + 
        		"                                            </table>\r\n" + 
        		"                                        </td>\r\n" + 
        		"                                    </tr>\r\n" + 
        		"                                    <tr>\r\n" + 
        		"                                        <td height=\"15\"></td>\r\n" + 
        		"                                    </tr>\r\n" + 
        		"                                </table>\r\n" + 
        		"                            </td>\r\n" + 
        		"                        </tr>\r\n" + 
        		"                    </table>\r\n" + 
        		"            </td>\r\n" + 
        		"        </tr>\r\n" + 

        		"        <tr>\r\n" + 
        		"            <td>\r\n" + 
        		"                <table bgcolor=\"#efeeee\" width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\r\n" + 
        		"                    <tr>\r\n" + 
        		"                        <td valign=\"top\" style=\"border-bottom: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;background-color:\"#efeeee\";\">\r\n" + 
        		"                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\r\n" + 
        		"                                <tr>\r\n" + 
        		"                                    <td height=\"20\" style=\"font-size:14px;\">\r\n" + 
        		"                                        Our Brands:\r\n" + 
        		"                                    </td>\r\n" + 
        		"                                </tr>\r\n" + 
        		"                                <tr align=\"left\">\r\n" + 
        		"                                    <td align=\"left\" width=\"50px\">\r\n" + 
        		"                                        <a href=\"https://www.tajhotels.com/\">\r\n" + 
        		"                                            <img width=\"40px\" src=\"https://www.tajhotels.com/content/dam/tajhotels/icons/style-icons/logo--Taj.png\" alt=\"Logo\" style=\"vertical-align: bottom;\" border=\"0\"/></a>\r\n" + 
        		"                                        <a href=\"https://www.seleqtionshotels.com/\">\r\n" + 
        		"                                            <img width=\"60px\" src=\"https://www.tajhotels.com/content/dam/tajhotels/icons/style-icons/seleqtions-brand-icon.png\" style=\"vertical-align: middle;\" alt=\"Logo\" border=\"0\"/></a>\r\n" + 
        		"                                        <a href=\"https://www.vivantahotels.com/\">\r\n" + 
        		"                                            <img width=\"60px\" src=\"https://www.tajhotels.com/content/dam/tajhotels/ihcl/Logos/icon-vivanta.png\" style=\"vertical-align: middle;\" alt=\"Logo\" border=\"0\"/></a>\r\n" + 
        		"                                        <a href=\"https://www.gingerhotels.com/\">\r\n" + 
        		"                                            <img width=\"60px\" src=\"https://www.tajhotels.com/content/dam/tajhotels/ihcl/Logos/Ginger-icon.png\" style=\"vertical-align: middle;\" alt=\"Logo\" border=\"0\"/></a>\r\n" + 
        		"										 <a href=\"https://www.tajhotels.com/en-in/\">\r\n" + 
                		"                                    <img width=\"60px\" src=\"https://www.tajhotels.com/content/dam/tajhotels/ihcl/Logos/Expressions-icon.png\" style=\"vertical-align: middle;\" alt=\"Logo\" border=\"0\"/></a>\r\n" + 
        		"                                        <a href=\"https://www.tajhotels.com/en-in/\">\r\n" + 
        		"                                            <img width=\"40px\" src=\"https://www.tajhotels.com/content/dam/logos/taj_sats-01.png/_jcr_content/renditions/cq5dam.web.756.756.png\" style=\"vertical-align: middle;\" alt=\"Logo\" border=\"0\"/></a>\r\n" + 
        		"                                    </td>\r\n" + 
        		"                                </tr>\r\n" + 
        		"                            </table>\r\n" + 
        		"                            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"560\">\r\n" + 
        		"                                <tr>\r\n" + 
        		"                                    <tr height=\"5\"></tr>\r\n" + 
        		"                                </tr>\r\n" +
        		"                                <tr height=\"5\"></tr>\r\n" + 
        		"                                <tr>\r\n" + 
        		"                                    <td style=\"color: #000; font-size:24px;\">\r\n" + 
        		"                                        <table border=\"0\" cellspacing=\"0\">\r\n" + 
        		"                                            <tr>\r\n" + 
        		"                                                <td style=\"color:#4a4a4a; font-size:12px;\">\r\n" + 
        		"                                                        ©2020 The Indian Hotels Company Limited. All Rights Reserved.    \r\n" + 
        		"                                                </td>\r\n" + 
        		"                                            </tr>\r\n" + 
        		"                                        </table>\r\n" + 
        		"                                    </td>\r\n" + 
        		"                                </tr>\r\n" + 
        		"                            </table>\r\n" + 
        		"                        </td>\r\n" + 
        		"                    </tr>\r\n" + 
        		"                </table>\r\n" + 
        		"            </td>\r\n" + 
        		"        </tr>\r\n" + 
        		"    </table>\r\n" + 
        		"</body>";
		return HtmlTemplate;
    }
    private void captureUserData(DonationRequest user)
            throws RepositoryException, PersistenceException, LoginException {
        LOGGER.error("Inside captureUserData Method{}");
        final String dataStorePath = "/content/usergenerated/donation-data";
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        LOGGER.error("Obtained Resource Resovler as {}", resourceResolver);
        Resource dataStorePathResource = resourceResolver.getResource(dataStorePath);
        LOGGER.error("Obtained dataStorePathResource as {}", dataStorePathResource);
        String timeStamp = fetchCurrentTimeStamp();
        String[] splitTimeStamp = timeStamp.split(" ");
        String[] splitYearMonthDate = splitTimeStamp[0].split("/");
        String currentYear = splitYearMonthDate[0];
        String currentMonth = splitYearMonthDate[1];
        String currentDay = splitYearMonthDate[2];
        String time = splitTimeStamp[1].replaceAll(":", "").replaceAll("\\.", "");
        LOGGER.trace("UserData id {}", time);
        if (null != dataStorePathResource) {
            Node dateStoreNode = dataStorePathResource.adaptTo(Node.class);
            LOGGER.trace("Obtained Data Store Node as {}", dateStoreNode);
            Session session = dateStoreNode.getSession();

            if (dateStoreNode.hasNode(currentYear)) {
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                if (currentyearNode.hasNode(currentMonth)) {
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    if (currentMonthNode.hasNode(currentDay)) {
                    	Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        addDataToNode(userNode,user);
//                        Node data = userNode.addNode("data");
//                        data.setProperty("username", "To be Fetched from form");
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
//                        fetchValueMapFromObj(userDataMap, userData, timeStamp);
                        resourceResolver.commit();
                        resourceResolver.close();
                    } else {
                    	currentMonthNode.addNode(currentDay, "nt:unstructured");
                        session.save();
                        Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        addDataToNode(userNode,user);
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
//                        fetchValueMapFromObj(userDataMap, userData, timeStamp);
                        resourceResolver.commit();
                        resourceResolver.close();
                    }

                } else {
                	currentyearNode.addNode(currentMonth, "nt:unstructured");
                    session.save();
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    currentMonthNode.addNode(currentDay, "nt:unstructured");
                    session.save();
                    Node currentDayNode = currentMonthNode.getNode(currentDay);
                    currentDayNode.addNode(time, "nt:unstructured");
                    session.save();
                    Node userNode = currentDayNode.getNode(time);
                    addDataToNode(userNode,user);
                    Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                    ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
//                    fetchValueMapFromObj(userDataMap, userData, timeStamp);
                    resourceResolver.commit();
                    resourceResolver.close();
                }
            } else {
            	dateStoreNode.addNode(currentYear, "nt:unstructured");
                session.save();
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                currentyearNode.addNode(currentMonth, "nt:unstructured");
                session.save();
                Node currentMonthNode = currentyearNode.getNode(currentMonth);
                currentMonthNode.addNode(currentDay, "nt:unstructured");
                session.save();
                Node currentDayNode = currentMonthNode.getNode(currentDay);
                currentDayNode.addNode(time, "nt:unstructured");
                session.save();
                Node userNode = currentDayNode.getNode(time);
                addDataToNode(userNode,user);
                Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
//                fetchValueMapFromObj(userDataMap, userData, timeStamp);
                resourceResolver.commit();
                resourceResolver.close();
            }
        }
    }
    
    private String fetchCurrentTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSS");
        String timeStamp = sdf.format(date);
        LOGGER.trace("Current Timestamp {}", timeStamp);
        return timeStamp;
    }

    private void addDataToNode(Node userNode ,DonationRequest user) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
    	
    	String nodeName = user.getFirstName()+user.getLastName()+user.getOrderId();
    	Node data = userNode.addNode(nodeName);
    	LOGGER.info("user.getFirstName() = {}", user.getFirstName());
        data.setProperty("FirstName", user.getFirstName());
        data.setProperty("LastName", user.getLastName());
        data.setProperty("Email", user.getEmail());
        data.setProperty("PAN", user.getPanCardNumber());
        data.setProperty("Donation amount", user.getDonation());
        data.setProperty("City", user.getCity());
        data.setProperty("Country", user.getCountry());
        data.setProperty("Pincode", user.getPincode());
        data.setProperty("AddressLineOne", user.getAddress1());
        data.setProperty("State", user.getAddress2());
        data.setProperty("Gdpr compliance", user.getIsGDPRCompliance());
        data.setProperty("Receipt No", user.getReceiptNumber());
        data.setProperty("Order ID", user.getOrderId());
        if(user.getOrderIdMismatch()!=null) {
        	data.setProperty("OrderIdMismatch", user.getOrderIdMismatch());
        }
    }
}
