package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestDispatcherOptions;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;

@Component(service = Servlet.class, immediate = true, property = { "sling.servlet.paths=" + "/bin/fetch/alloffers" })
public class AllOffersFilterServlet extends SlingAllMethodsServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8362773045503357999L;

	@Reference
	private RequestResponseFactory requestResponseFactory;

	@Reference
	private SlingRequestProcessor requestProcessor;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {

		try {

			String resourcePath = "content/tajhotels/en-in/offers.html";
			ResourceResolver resourceResolver = request.getResourceResolver();
			Resource resource = resourceResolver.resolve(resourcePath);
			request.getRequestDispatcher(resource).forward(request, response);

			response.getOutputStream().toString();

		} catch (Exception e) {
		}
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {

		try {
			RequestDispatcherOptions options = new RequestDispatcherOptions();
			options.setReplaceSelectors("");
			request.getRequestDispatcher("content/tajhotels/en-in/all-offers.html").include(request, response);

		} catch (Exception e) {
		}
	}
}
