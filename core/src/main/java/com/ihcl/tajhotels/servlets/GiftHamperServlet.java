/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.models.GiftHamperRequest;
import com.ihcl.core.servicehandler.processpayment.CCAvenuePaymentHandler;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;

/**
 * @author Vijay Chikkani
 *
 */
@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/giftHamperServlet", "sling.servlet.methods=post",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })

public class GiftHamperServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GiftHamperServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started GiftHamperServlet.doPost()");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String returnMessage = "";

        try {
            String websiteId = slingHttpServletRequest.getServerName();
            if (StringUtils.isBlank(websiteId)) {
                websiteId = "tajhotels";
            }

            TajHotelBrandsETCConfigurations etcWebsiteConfig = extractCCAvenueConfigurations(objectMapper, websiteId);

            String senderDetailsString = slingHttpServletRequest.getParameter("senderDetails");
            LOGGER.debug("senderDetailsString: {}", senderDetailsString);

            GiftHamperRequest senderDetails =
                    objectMapper.readValue(senderDetailsString, new TypeReference<GiftHamperRequest>() {
                    });

            nullCheckForSenderDetails(senderDetails);

            String shippingDetailsString = slingHttpServletRequest.getParameter("shippingDetails");
            LOGGER.debug("shippingDetailsString: {}", shippingDetailsString);

            GiftHamperRequest shippingDetails =
                    objectMapper.readValue(shippingDetailsString, new TypeReference<GiftHamperRequest>() {
                    });

            nullCheckForShippingDetails(shippingDetails);

            String hotelIdString = slingHttpServletRequest.getParameter("hotelId");
            LOGGER.debug("hotelIdString: {}", hotelIdString);

            String subAccountIdString = slingHttpServletRequest.getParameter("subAccountID");
            LOGGER.debug("subAccountID: {}", subAccountIdString);

            if (StringUtils.isBlank(hotelIdString) || StringUtils.isBlank(subAccountIdString)) {
                throw new HotelBookingException("Issue found while fetching hotelId || subAccountID.");
            }


            if ((Integer.parseInt(senderDetails.getSubTotalAmount()) !=
                    Integer.parseInt(shippingDetails.getSubTotalAmount())) ||
                    (StringUtils.isNotBlank(senderDetails.getShippingAmount()) &&
                            StringUtils.isNotBlank(shippingDetails.getShippingAmount()) &&
                            (Integer.parseInt(senderDetails.getShippingAmount()) !=
                                    Integer.parseInt(shippingDetails.getShippingAmount())))) {
                throw new HotelBookingException("Issue found while fetching Amount details.");
            }

            if (StringUtils.isNotBlank(senderDetails.getShippingAmount())) {
                String totalAmount = String.valueOf(Integer.parseInt(senderDetails.getSubTotalAmount()) +
                        Integer.parseInt(senderDetails.getShippingAmount()));
                senderDetails.setTotalAmount(totalAmount);
                shippingDetails.setTotalAmount(totalAmount);
                LOGGER.info("Setting TotalAmount as sum of SubTotalAmount and ShippingAmount");
            } else {
                senderDetails.setTotalAmount(senderDetails.getSubTotalAmount());
                shippingDetails.setTotalAmount(shippingDetails.getSubTotalAmount());
                LOGGER.info("Setting TotalAmount as SubTotalAmount because there is no ShippingAmount");
            }

            CCAvenueParams ccAvenueParams =
                    injectCCAvenueParmas(etcWebsiteConfig, senderDetails, shippingDetails, hotelIdString,
                            subAccountIdString);

            LOGGER.info("ccAvenueParams: {}", objectMapper.writeValueAsString(ccAvenueParams));
            CCAvenuePaymentHandler ccAvenuePaymentHandler = new CCAvenuePaymentHandler();
            objectNode.put("CCAvenueForm", ccAvenuePaymentHandler.getCCAvenueDetails(ccAvenueParams));

            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, true);
            returnMessage = objectNode.toString();

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, false);
            objectNode.put("message", hbe.getMessage());
            LOGGER.debug("GiftHamperServlet throwing custom exception : {}", hbe.getMessage());
            LOGGER.error("GiftHamperServlet throwing custom exception : {}", hbe);
            returnMessage = objectNode.toString();

        } catch (Exception e) {
            objectNode.put(BookingConstants.CONFIRMATION_STATUS_NAME, false);
            objectNode.put("message", BookingConstants.GIFT_HAMPER_EXCEPTION_MESSAGE);
            LOGGER.debug("Exception occured while ordering Gift Hamper : {}", e.getMessage());
            LOGGER.error("Exception occured while ordering Gift Hamper : {}", e);
            returnMessage = objectNode.toString();

        } finally {
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);

            LOGGER.info("Final returnMessage : {}", returnMessage);
            LOGGER.debug("End of GiftHamperServlet.doPost().");
            LOGGER.info("Returning response to page");

            slingHttpServletResponse.getWriter().write(returnMessage);
            slingHttpServletResponse.getWriter().close();
        }
    }

    /**
     * @param shippingDetails
     * @throws HotelBookingException
     */
    private void nullCheckForShippingDetails(GiftHamperRequest shippingDetails) throws HotelBookingException {
        if (StringUtils.isBlank(shippingDetails.getFirstName())) {
            throw new HotelBookingException("Shipping FirstName not found");
        }
        if (StringUtils.isBlank(shippingDetails.getLastName())) {
            throw new HotelBookingException("Shipping LastName not found");
        }
        if (StringUtils.isBlank(shippingDetails.getPhoneNumber())) {
            throw new HotelBookingException("Shipping PhoneNumber not found");
        }
        if (StringUtils.isBlank(shippingDetails.getAddress())) {
            throw new HotelBookingException("Shipping Address not found");
        }
        if (StringUtils.isBlank(shippingDetails.getCity())) {
            throw new HotelBookingException("Shipping City not found");
        }
        if (StringUtils.isBlank(shippingDetails.getPincode())) {
            throw new HotelBookingException("Shipping Pincode not found");
        }
    }

    /**
     * @param senderDetails
     * @throws HotelBookingException
     */
    private void nullCheckForSenderDetails(GiftHamperRequest senderDetails) throws HotelBookingException {
        if (StringUtils.isBlank(senderDetails.getFirstName())) {
            throw new HotelBookingException("Sender FirstName not found");
        }
        if (StringUtils.isBlank(senderDetails.getLastName())) {
            throw new HotelBookingException("Sender LastName not found");
        }
        if (StringUtils.isBlank(senderDetails.getEmail())) {
            throw new HotelBookingException("Sender Email not found");
        }
        if (StringUtils.isBlank(senderDetails.getPhoneNumber())) {
            throw new HotelBookingException("Sender PhoneNumber not found");
        }
    }

    /**
     * @param objectMapper
     * @param websiteId
     * @return etcWebsiteConfig
     * @throws HotelBookingException
     * @throws IOException
     */
    private TajHotelBrandsETCConfigurations extractCCAvenueConfigurations(ObjectMapper objectMapper, String websiteId)
            throws HotelBookingException, IOException {
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        if (etcAllBrandsConfig == null) {
            LOGGER.info("etcAllBrandsConfig getting null, Please check etc configurations");
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig =
                etcAllBrandsConfig.getEtcConfigBean().get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig =
                individualWebsiteAllConfig.getEtcConfigBean().get(ReservationConstants.GIFT_HAMPER_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.GIFT_HAMPER_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.GIFT_HAMPER_CONFIG_EXCEPTION);
        }
        String debugString =
                "WebsiteId: " + websiteId + ", configurationType: " + ReservationConstants.GIFT_HAMPER_CCAVENUE +
                        ", configurations :-> " + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        return etcWebsiteConfig;
    }

    /**
     * @param etcWebsiteConfig
     * @param shippingDetails
     * @param subAccountID
     * @return ccAvenueParams
     */
    private CCAvenueParams injectCCAvenueParmas(TajHotelBrandsETCConfigurations etcWebsiteConfig,
            GiftHamperRequest senderDetails, GiftHamperRequest shippingDetails, String hotelId, String subAccountID) {

        LOGGER.debug("Started injectCCAvenueParmas()");
        CCAvenueParams ccAvenueParams = new CCAvenueParams();
        ccAvenueParams.setMerchantId(etcWebsiteConfig.getPaymentMerchantId());
        ccAvenueParams.setCcAvenueAccessKey(etcWebsiteConfig.getCcAvenueAccessKey());
        ccAvenueParams.setCcAvenueWorkingKey(etcWebsiteConfig.getCcAvenueWorkingKey());
        ccAvenueParams.setCcAvenueUrl(etcWebsiteConfig.getCcAvenueUrl());
        ccAvenueParams.setRedirectUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());
        ccAvenueParams.setCancelUrl(etcWebsiteConfig.getCcAvenueRedirectUrl());

        String giftHamperOrderId = "GHO" + (int) (new Date().getTime() / 1000);
        ccAvenueParams.setOrderId(giftHamperOrderId);
        ccAvenueParams.setCurrency(BookingConstants.CCAVENUE_PARAM_CURRENCY);
        ccAvenueParams.setAmount(senderDetails.getTotalAmount());

        ccAvenueParams.setBillingName(senderDetails.getFirstName() + " " + senderDetails.getLastName());

        ccAvenueParams.setBillingTel(senderDetails.getPhoneNumber());
        ccAvenueParams.setBillingEmail(senderDetails.getEmail());


        ccAvenueParams.setMerchantParam1(shippingDetails.getFirstName() + "_" + shippingDetails.getLastName());
        ccAvenueParams.setMerchantParam2(shippingDetails.getPhoneNumber());
        ccAvenueParams.setMerchantParam3(shippingDetails.getAddress());
        ccAvenueParams.setMerchantParam4(
                shippingDetails.getCity() + "_" + shippingDetails.getPincode() + "_" + shippingDetails.getState() +
                        "_" + shippingDetails.getCountry());

        String collectionType = "";
        if (shippingDetails.isDelivery()) {
            collectionType = "DELIVERY";
        } else if (shippingDetails.isPickup()) {
            collectionType = "PICKUP";
        }
        String mechParam5 = hotelId + "_" + senderDetails.getCode() + "_" + collectionType + "_" +
                shippingDetails.getSubTotalAmount() + "_" + shippingDetails.getShippingAmount();
        ccAvenueParams.setMerchantParam5(mechParam5);

        ccAvenueParams.setSubAccountId(subAccountID);

        LOGGER.debug("End of injectCCAvenueParmas()");
        return ccAvenueParams;
    }
}
