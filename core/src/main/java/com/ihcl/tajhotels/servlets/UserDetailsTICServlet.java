/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.integration.Constants.REQUEST_KEY_AUTH_TOKEN;
import static com.ihcl.integration.Constants.RESPONSE_KEY_DATA;
import static com.ihcl.integration.Constants.RESPONSE_KEY_MESSAGE;
import static com.ihcl.integration.Constants.RESPONSE_VALUE_FETCH_MEMBER_DETAILS_SUCCESS;
import static com.ihcl.integration.Constants.RESPONSE_VALUE_NO_MEMBERSHIP_ID_MESSAGE;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.signin.api.IUserAccount;
import com.ihcl.tajhotels.signin.dto.User;
import com.ihcl.tajhotels.signin.dto.UserDetailsResponse;

/**
 * @author user
 *
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/fetch-user-details-tic", "sling.servlet.methods=POST" })
public class UserDetailsTICServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID */
    private static final long serialVersionUID = 1L;

    @Reference
    IUserAccount userAccountService;

    private static final Logger LOG = LoggerFactory.getLogger(UserDetailsTICServlet.class);

    @Override
    protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        String authToken = req.getParameter(REQUEST_KEY_AUTH_TOKEN);
        LOG.debug("Modified authToken as: " + authToken);
        UserDetailsResponse userDetails = null;

        String responseMessage;
        try {
            if (StringUtils.isNotBlank(authToken)) {
                userDetails = userAccountService.fetchDetails(authToken);
                jsonNode.put(RESPONSE_KEY_DATA, getJsonOf(userDetails));
                responseMessage = RESPONSE_VALUE_FETCH_MEMBER_DETAILS_SUCCESS;
            } else {
                responseMessage = RESPONSE_VALUE_NO_MEMBERSHIP_ID_MESSAGE;
            }
            jsonNode.put(RESPONSE_KEY_MESSAGE, responseMessage);

            resp.setContentType("application/json");
            resp.getWriter().write(jsonNode.toString());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            resp.sendError(500, e.getMessage());
        }
    }

    private JsonNode getJsonOf(UserDetailsResponse userDetails) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        User user = userDetails.getUserDetails();
        String name = user.getFirstName() + " " + user.getLastName();
        jsonNode.put("name", name);
        jsonNode.put("firstName", user.getFirstName());
        jsonNode.put("lastName", user.getLastName());
        jsonNode.put("email", user.getEmail());
        jsonNode.put("mobile", user.getMobile());
        jsonNode.put("cdmReferenceId", user.getCdmReferenceId());
        jsonNode.put("membershipId", user.getMembershipId());
        jsonNode.put("googleLinked", user.getGoogleLinked());
        jsonNode.put("facebookLinked", user.getFacebookLinked());
        jsonNode.put("title", user.getTitle());
        return jsonNode;
    }

}
