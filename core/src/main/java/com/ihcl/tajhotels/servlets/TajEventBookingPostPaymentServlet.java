package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.core.services.config.TajEventBookingConfigurationService;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.tajhotels.email.api.IEmailService;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/eventBookingPostPaymentServlet", "sling.servlet.methods={GET,POST}",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class TajEventBookingPostPaymentServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TajEventBookingPostPaymentServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private TajHotelsBrandsETCService etcConfigService;

    @Reference
    private TajEventBookingConfigurationService eventConfigurationService;

    @Reference
    private SearchProvider searchProvider;

    @Override
    protected void doPost(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        LOGGER.debug("Started TajEventBookingPostPaymentServlet.doPost().");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        String paymentStatus = "";
        try {

            Map<String, Object> objectMap = fetchgETCConfigurations(slingHttpServletRequest, objectMapper);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = (TajHotelBrandsETCConfigurations) objectMap
                    .get("etcWebsiteConfig");

            String encResp = slingHttpServletRequest.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME);

            if (StringUtils.isNotBlank(encResp)) {

                CCAvenueParams ccAvenueParams = processCCavenuedata(encResp, etcWebsiteConfig.getCcAvenueWorkingKey());
                objectNode.put("ccAvenueTrackingId", ccAvenueParams.getTrackingId());
                objectNode.put("paymentStatus", ccAvenueParams.getPaymentstatus());
                String loggerString = "ccAvenueTrackingId: " + ccAvenueParams.getTrackingId() + "; paymentStatus: "
                        + ccAvenueParams.getPaymentstatus();
                LOGGER.info(loggerString);

                String hotelId = ccAvenueParams.getMerchantParam1();
                String eventPath = ccAvenueParams.getMerchantParam4();

                if (StringUtils.isBlank(eventPath)) {
                    throw new HotelBookingException("eventPath not found");
                }

                if (ccAvenueParams.getPaymentstatus()) {
                    LOGGER.info("Paymentstatus: {}", ccAvenueParams.getPaymentstatus());
                    paymentStatus = "Success";
                    objectNode.put(BookingConstants.STATUS, true);
                    sendEmailToCustomerAndHotelAuthority(ccAvenueParams, eventConfigurationService, hotelId);
                } else {
                    paymentStatus = "Failed";
                }

            } else {
                LOGGER.info("Response from CCAvenue getting null/empty. (encResp).");
            }

        } catch (HotelBookingException hbe) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, hbe.getMessage());
            LOGGER.debug("Missing information in event booking : {}", hbe.getMessage());
            LOGGER.error("Missing information in event booking : {}", hbe);

        } catch (Exception e) {
            objectNode.put(BookingConstants.STATUS, false);
            objectNode.put(BookingConstants.MESSAGE, "Event booking got interrupted.");
            LOGGER.debug("Exception while event booking : {}", e.getMessage());
            LOGGER.error("Exception while event booking : {}", e);

        } finally {
            writeResponseOnServer(slingHttpServletResponse, objectNode.toString(), paymentStatus);
        }
    }

    private void sendEmailToCustomerAndHotelAuthority(CCAvenueParams ccAvenueParams,
            TajEventBookingConfigurationService eventConfigurationService, String hotelId) {

        LOGGER.info("Sending Email to hotel authority: {}", eventConfigurationService.getEventAdminEmailId());
        sendMailWithGiftHamper(ccAvenueParams, eventConfigurationService.getEventAdminEmailId(),
                eventConfigurationService.getHotelEmailBodyTemplate(), eventConfigurationService.getHotelEmailSubject(),
                hotelId);
        LOGGER.info(" Sending Email to Sender reference: {}", ccAvenueParams.getBillingEmail());
        sendMailWithGiftHamper(ccAvenueParams, ccAvenueParams.getBillingEmail(),
                eventConfigurationService.getCustomerEmailBodyTemplate(),
                eventConfigurationService.getCustomerEmailSubject(), hotelId);
    }

    private void sendMailWithGiftHamper(CCAvenueParams ccAvenueParams, String toMail, String emailBodyTemplate,
            String mailSubject, String hotelId) {

        String eventBookingMailBody = emailBodyTemplate;

        eventBookingMailBody = eventBookingMailBody.replace("$HOTELNAME$", ccAvenueParams.getMerchantParam5());

        eventBookingMailBody = eventBookingMailBody.replace("$CCAVENUETRACKINGID$", ccAvenueParams.getTrackingId());
        eventBookingMailBody = eventBookingMailBody.replace("$CCAVENUEORDERID$", ccAvenueParams.getOrderId());

        eventBookingMailBody = eventBookingMailBody.replace("$SENDERNAME$", ccAvenueParams.getBillingName());
        eventBookingMailBody = eventBookingMailBody.replace("$SENDEREMAIL$", ccAvenueParams.getBillingEmail());
        eventBookingMailBody = eventBookingMailBody.replace("$SENDERPHONENUMBER$", ccAvenueParams.getBillingTel());
        eventBookingMailBody = eventBookingMailBody.replace("$SENDERAMOUNT$", ccAvenueParams.getAmount());

        eventBookingMailBody = eventBookingMailBody.replace("$HOTELID$", hotelId);
        eventBookingMailBody = eventBookingMailBody.replace("$TICKETNO$", ccAvenueParams.getMerchantParam2());
        eventBookingMailBody = eventBookingMailBody.replace("$EVENTDETAILS$", ccAvenueParams.getMerchantParam3());

        boolean status = iEmailService.sendEMail(mailSubject, eventBookingMailBody, toMail, null, null, null);
        if (status) {
            LOGGER.info("Gift Hamper conformation sent through email. Mail status: {}", status);
        } else {
            LOGGER.info("Issue found while mailing Gift Hamper. Mail status: {}", status);
        }

    }

    private void writeResponseOnServer(SlingHttpServletResponse slingHttpServletResponse, String responseString,
            String paymentStatus) {
        String methodName = "writeResponseOnServer";
        LOGGER.trace("Method Entry: " + methodName);
        try {
            PrintWriter printWriter = slingHttpServletResponse.getWriter();
            LOGGER.info("redirecting using hidden form");
            LOGGER.info("Final eventBookingResponse : {}", responseString);
            String redirectUrl = eventConfigurationService.getConfirmationPagePath();
            if (paymentStatus == null || !paymentStatus.equalsIgnoreCase("success")) {
                redirectUrl = eventConfigurationService.getFailurePagePath();
            }
            LOGGER.info("Received redirection page path from configuration as: {}", redirectUrl);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_HTML);
            responseString = StringEscapeUtils.escapeEcmaScript(responseString);
            LOGGER.info("Final eventBookingResponseString which is setting as SessionData : {}", responseString);
            printWriter.print("<form id='eventBookingCreation' action='" + redirectUrl
                    + BookingConstants.PAGE_EXTENSION_HTML + "' method='get'>");
            printWriter.print("<input type='hidden' name='paymentStatus' value='" + paymentStatus + "'>");
            printWriter.print("</form>");
            printWriter.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                    + "sessionStorage.setItem('eventBookingResponseString', '" + responseString + "');"
                    + "sessionStorage.setItem('paymentStatus', '" + paymentStatus + "');"
                    + "document.getElementById(\"eventBookingCreation\").submit()" + "}" + "</script>");
            LOGGER.info("end of output stream");
            printWriter.close();

        } catch (Exception e) {
            LOGGER.error("Error while submitting form : {}", e.getMessage());
            LOGGER.debug("error while submitting form : {}", e);
        }
        LOGGER.trace("Method Exit: " + methodName);

    }

    private Map<String, Object> fetchgETCConfigurations(SlingHttpServletRequest slingHttpServletRequest,
            ObjectMapper objectMapper) throws HotelBookingException, IOException {

        LOGGER.info("request.getRequestURL(): {}", slingHttpServletRequest.getRequestURL());
        LOGGER.info("request.getServerName(): {}", slingHttpServletRequest.getServerName());

        String websiteId = slingHttpServletRequest.getServerName();
        if (StringUtils.isBlank(websiteId)) {
            websiteId = "tajhotels";
        }
        Map<String, Object> objectMap = new HashMap<>();
        TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = etcConfigService.getEtcConfigBean();
        TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcAllBrandsConfig.getEtcConfigBean()
                .get(websiteId);
        if (individualWebsiteAllConfig == null) {
            LOGGER.info("individualWebsiteAllConfig getting null, for websiteId: {}", websiteId);
            throw new HotelBookingException(ReservationConstants.EVENT_CONFIG_EXCEPTION);
        }
        TajHotelBrandsETCConfigurations etcWebsiteConfig = individualWebsiteAllConfig.getEtcConfigBean()
                .get(ReservationConstants.EVENT_CCAVENUE);
        if (etcWebsiteConfig == null) {
            LOGGER.info("etcWebsiteConfig getting null, for configurationType: {}",
                    ReservationConstants.EVENT_CCAVENUE);
            throw new HotelBookingException(ReservationConstants.EVENT_CONFIG_EXCEPTION);
        }
        objectMap.put("contentRootPath", etcWebsiteConfig.getContentRootPath());
        String debugString = "WebsiteId: " + websiteId + ", ConfigurationType: " + ReservationConstants.EVENT_CCAVENUE
                + ", Configurations :-> " + objectMapper.writeValueAsString(etcWebsiteConfig);
        LOGGER.info(debugString);
        objectMap.put("etcWebsiteConfig", etcWebsiteConfig);
        return objectMap;
    }

    private CCAvenueParams processCCavenuedata(String encResp, String ccAvenueWorkingKey)
            throws UnsupportedEncodingException {
        Map<String, String> responseMap = new LinkedHashMap<>();
        AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueWorkingKey);
        String decResp = aesUtil.decrypt(encResp);
        StringTokenizer tokenizer = new StringTokenizer(decResp, "&");
        String pair = null;
        String key = "";
        String value = "";
        while (tokenizer.hasMoreTokens()) {
            pair = tokenizer.nextToken();
            if (pair != null) {
                StringTokenizer strTok = new StringTokenizer(pair, "=");
                if (strTok.hasMoreTokens()) {
                    key = strTok.nextToken();
                    if (strTok.hasMoreTokens()) {
                        value = strTok.nextToken();
                        try {
                            responseMap.put(key, URLDecoder.decode(value, BookingConstants.CHARACTER_ENCOADING));
                        } catch (UnsupportedEncodingException usee) {
                            LOGGER.error("** Error occured while decoding CCAvenueParmas **");
                            throw usee;
                        }
                    }
                }
            }
        }
        return extractingCCAvenueParams(responseMap);
    }

    private CCAvenueParams extractingCCAvenueParams(Map<String, String> responseMap) {
        CCAvenueParams ccavenueParams = new CCAvenueParams();

        for (Map.Entry<String, String> entry : responseMap.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue();
            String str = mapKey + " ::: " + mapValue;
            LOGGER.info("CCAvenue params: {}", str);
        }

        ccavenueParams.setPaymentstatus(responseMap.get(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS)
                .equalsIgnoreCase(BookingConstants.CCAVENUE_RESPONSE_ORDER_STATUS_VALUE));

        ccavenueParams.setTrackingId(responseMap.get(BookingConstants.CCAVENUE_TRACKING_ID));
        ccavenueParams.setOrderId(responseMap.get("order_id"));
        ccavenueParams.setCurrency(responseMap.get("currency"));
        ccavenueParams.setAmount(responseMap.get("amount"));

        ccavenueParams.setBillingName(responseMap.get("billing_name"));
        ccavenueParams.setBillingTel(responseMap.get("billing_tel"));
        ccavenueParams.setBillingEmail(responseMap.get("billing_email"));
        ccavenueParams.setBillingAddress(responseMap.get("billing_address"));
        ccavenueParams.setBillingCity(responseMap.get("billing_city"));
        ccavenueParams.setBillingZip(responseMap.get("billing_zip"));
        ccavenueParams.setBillingState(responseMap.get("billing_state"));
        ccavenueParams.setBillingCountry(responseMap.get("billing_country"));

        ccavenueParams.setMerchantParam1(responseMap.get("merchant_param1"));
        ccavenueParams.setMerchantParam2(responseMap.get("merchant_param2"));
        ccavenueParams.setMerchantParam3(responseMap.get("merchant_param3"));
        ccavenueParams.setMerchantParam4(responseMap.get("merchant_param4"));
        ccavenueParams.setMerchantParam5(responseMap.get("merchant_param5"));

        ccavenueParams.setSubAccountId(responseMap.get("sub_account_id"));
        return ccavenueParams;
    }

}
