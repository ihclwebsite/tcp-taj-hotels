package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.NEWSLETTER_SUBSCRIPTION_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.services.config.TajApiKeyConfigurationService;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.integration.exception.ServiceException;
import com.ihcl.integration.newsletter.api.INewsletterService;
import com.ihcl.integration.newsletter.dto.ManageUserSubscriptionResponse;
import com.ihcl.integration.newsletter.dto.NewsletterRequest;
import com.ihcl.tajhotels.email.api.IEmailService;


@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/newsletterSubscribe" })
public class NewsletterSubscribeServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(NewsletterSubscribeServlet.class);

    @Reference
    private IEmailService iEmailService;

    @Reference
    private INewsletterService iNewsletterService;

    @Reference
    TajApiKeyConfigurationService tajApiKeyConfigurationService;

    @Reference
    private EmailTemplateConfigurationService emailTemplateConfigService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        String requestFrom = httpRequest.getParameter("sourceBrand");
        LOG.trace("This newsletter subscription is from " + requestFrom + "domain");
        LOG.trace("\n\n In NewsletterSubscribeServlet Class-> Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for news letter subscription is : " + requestUrl);

        try {
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");


                ManageUserSubscriptionResponse serviceResponse = fetchFromNewsLetterService(httpRequest);
                String SERVELET_RESPONSE = "";
                String isSubscribeFlag = httpRequest.getParameter("isSubscribe");
                if (isSubscribeFlag.equals("true")) {
                    SERVELET_RESPONSE = "Email Subscription Successfull";
                } else if (isSubscribeFlag.equals("false")) {
                    SERVELET_RESPONSE = "Email Unsubscription Successfull";
                }

                if (serviceResponse.getCode().equals("200")) {
                    LOG.trace("Susbscription Successfull ");
                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, SERVELET_RESPONSE);

                } else {
                    LOG.trace("Error Message : " + serviceResponse.getStatus());
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, SERVELET_RESPONSE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, NEWSLETTER_SUBSCRIPTION_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, NEWSLETTER_SUBSCRIPTION_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }


    private ManageUserSubscriptionResponse fetchFromNewsLetterService(SlingHttpServletRequest httpRequest)
            throws ServiceException {

        NewsletterRequest requestBody = getRequestParametersFrom(httpRequest);
        return iNewsletterService.manageUserSubscription(requestBody);
    }

    private NewsletterRequest getRequestParametersFrom(SlingHttpServletRequest httpRequest) {

        NewsletterRequest requestBody = new NewsletterRequest();
        String isSubscribe = httpRequest.getParameter("isSubscribe");
        requestBody.setTitle(httpRequest.getParameter("title"));
        requestBody.setFirstName(httpRequest.getParameter("firstName"));
        requestBody.setLastName(httpRequest.getParameter("lastName"));
        requestBody.setGender(httpRequest.getParameter("gender"));
        requestBody.setEmail(httpRequest.getParameter("emailId"));
        requestBody.setRecordName(httpRequest.getParameter("emailId"));
        requestBody.setCity(httpRequest.getParameter("city"));
        requestBody.setCountry(httpRequest.getParameter("country"));
        if (isSubscribe.equals("true")) {
            requestBody.setSubscribeUnsubscribeValue("Subscribe");
            requestBody.setSubscribedDate(httpRequest.getParameter("currentDate"));
            requestBody.setUnsubscibedDate("");
        } else if (isSubscribe.equals("false")) {
            requestBody.setSubscribeUnsubscribeValue("Unsubscribe");
            requestBody.setSubscribedDate("");
            requestBody.setUnsubscibedDate(httpRequest.getParameter("currentDate"));
        }
        requestBody.setAgreeToPrivacyPolicy(true);
        requestBody.setPrivacyPolicy(true);
        return requestBody;
    }

    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }
}
