package com.ihcl.tajhotels.servlets;


import static com.ihcl.tajhotels.constants.HttpResponseMessage.CUSTOMER_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.DATE_VALUE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.FIRST_NAME;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.PEOPLE_COUNT;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESERVATION_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESERVATION_STATUS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESERVATION_TIME;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESTAURANT_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.TABLE_RESERVATION_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.TABLE_RESERVATION_IS_FAILURE;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.requestdto.MakeReservationRequest;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.responsedto.MakeReservation;
import com.ihcl.tajhotels.integration.tableReservations.makeReservation.api.IMakeReservationService;

/**
 * Servlet that writes some sample content into the response. It is mounted for all resources of a specific Sling
 * resource type. The {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are idempotent. For write
 * operations use the {@link SlingAllMethodsServlet}.
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/tableBookingReservation", "sling.servlet.methods=GET",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class TableBookReservation extends SlingAllMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(TableBookReservation.class);

    @Reference
    private IMakeReservationService makeReservationService;


    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
    	 ObjectMapper mapperObj = new ObjectMapper();
        String methodName = "doGet";
        MakeReservationRequest requestobj = null;
        MakeReservation makeReservation = null;
        LOG.trace("Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(request);
        LOG.trace("Request URL for table booking is : " + requestUrl);
        try {
            if (validateRequest.validateUrl(requestUrl)) {
                LOG.debug("Received make table reservation service as: " + makeReservationService);
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                requestobj = setReservationDetails(request);
                String reqObjString = mapperObj.writeValueAsString(requestobj);
                LOG.debug("requestobj "+reqObjString);
                makeReservation = (MakeReservation) makeReservationService.makeReservation(requestobj);
                boolean reservationSuccess = makeReservation.isSuccess();
                LOG.debug("Reservation status received from the service :" + reservationSuccess);
                if (reservationSuccess) {
                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, makeReservation.getMessage(),
                            reservationSuccess, requestobj, makeReservation);
                } else {
                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, makeReservation.getMessage(),
                            reservationSuccess, requestobj, makeReservation);
                }
            } else {
                LOG.trace("This request contain cross site attck characters :");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE,
                        TABLE_RESERVATION_IS_FAILURE, requestobj, makeReservation);
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability.", e);
            // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, TABLE_RESERVATION_FAILURE,
                    TABLE_RESERVATION_IS_FAILURE, requestobj, makeReservation);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private MakeReservationRequest setReservationDetails(SlingHttpServletRequest request) throws Exception {
        String methodName = "setReservationDetails";
        LOG.trace("Method Entry: " + methodName);
        MakeReservationRequest requestobj = new MakeReservationRequest();
        requestobj.setRestaurantId(Integer.parseInt(request.getParameter("restaurantID")));
        requestobj.setReservationDate(request.getParameter("dateVal"));
        requestobj.setReservationTime(request.getParameter("reservationTime"));
        requestobj.setNoOfPeople(request.getParameter("peopleCount"));
        requestobj.setCustomerId(0);
        requestobj.setFirstName(request.getParameter("firstName"));
        requestobj.setMobile(request.getParameter("mobileNumber"));
        requestobj.setOccasion(request.getParameter("eventName"));
        requestobj.setEmail(request.getParameter("emailID"));
        if(request.getParameter("offerTitle") != null && request.getParameter("offerId") != null){
        requestobj.setOfferName(request.getParameter("offerTitle"));
        requestobj.setOfferId(request.getParameter("offerId"));
        }
        else{
        requestobj.setOfferName("");
        requestobj.setOfferId("");    
        }
        LOG.trace("requestobj "+requestobj.toString());
        return requestobj;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @param requestobj
     * @param makeReservation
     * @throws IOException
     */
    private void writeJsonToResponse(SlingHttpServletResponse response, String code, String message,
            boolean reservationSuccess, MakeReservationRequest requestobj, MakeReservation makeReservation)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        if (reservationSuccess) {
            jsonNode = populateJson(jsonNode, requestobj, makeReservation);
        }
        response.getWriter().write(jsonNode.toString());
        LOG.trace("The Value of JSON: " + jsonNode);
        LOG.trace("Method Exit: " + methodName);
    }

    private ObjectNode populateJson(ObjectNode jsonNode, MakeReservationRequest requestobj,
            MakeReservation makeReservation) {
        String methodName = "populateJson";
        LOG.trace("Method Entry: " + methodName);
        jsonNode.put(RESERVATION_STATUS, makeReservation.getStatus());
        jsonNode.put(RESERVATION_ID, makeReservation.getReservationID());
        jsonNode.put(DATE_VALUE, makeReservation.getReservationDate());
        jsonNode.put(RESERVATION_TIME, makeReservation.getReservationTime());
        jsonNode.put(PEOPLE_COUNT, makeReservation.getNoOfPeople());
        jsonNode.put(EMAIL_ID, makeReservation.getEmail());
        jsonNode.put(FIRST_NAME, makeReservation.getFirstName());
        jsonNode.put(CUSTOMER_ID, makeReservation.getCustomerId());
        jsonNode.put(RESTAURANT_ID, makeReservation.getRestaurantId());
        jsonNode.put("OfferName", makeReservation.getOfferName());
        jsonNode.put("OfferId", makeReservation.getOfferId());

        LOG.trace("The Value of JSON: " + jsonNode);
        LOG.trace("Method Exit: " + methodName);
        return jsonNode;
    }


    public IMakeReservationService getMakeReservationService() {
        return makeReservationService;
    }


}