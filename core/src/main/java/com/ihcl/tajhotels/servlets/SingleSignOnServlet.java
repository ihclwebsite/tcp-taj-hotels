/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Servlet;
import javax.servlet.http.Cookie;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.glassfish.jersey.client.ClientProperties;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.SingleSignOnConfigurationService;
import com.ihcl.tajhotels.constants.BookingConstants;

/**
 * @author apple
 *
 */
@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/singleSignOnServlet",
                "sling.servlet.methods={GET}", "sling.servlet.resourceTypes=services/powerproxy",
                "sling.servlet.selectors=groups", })
public class SingleSignOnServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 9876543210L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SingleSignOnServlet.class);

    @Reference
    private SingleSignOnConfigurationService singleSignOnConfigurationService;

    @Override
    protected void doGet(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();

        String reqAndResMediaType = BookingConstants.CONTENT_TYPE_TEXT;
        String paramMapString = objectMapper.writeValueAsString(slingHttpServletRequest.getParameterMap());
        LOGGER.info("ServletRequest.ParameterMap: {}", paramMapString);
        String cookieValue = slingHttpServletRequest
                .getParameter(singleSignOnConfigurationService.getSingleSignOnCookieName());

        String setCookieForOthers = slingHttpServletRequest.getParameter("setCookieForOthers");
        String setCookieForOtherDomains = slingHttpServletRequest.getParameter("setCookieForOtherDomains");
        String setCookieForCurrentDomain = slingHttpServletRequest.getParameter("setCookieForCurrenctDomain");
        String secureString = slingHttpServletRequest.getParameter("secure");

        String loggerString = "Received Params from request: "
                + singleSignOnConfigurationService.getSingleSignOnCookieName() + ": " + cookieValue + ", secure: "
                + secureString + ", setCookieForOthers: " + setCookieForOthers + ", setCookieForCurrentDomain: "
                + setCookieForCurrentDomain;
        LOGGER.info(loggerString);

        if (StringUtils.isBlank(secureString)) {
            secureString = "false";
        }

        String ecodedCookieValue = URLEncoder.encode(cookieValue, "UTF-8");
        LOGGER.info("ecodedValue: {}", ecodedCookieValue);
        //String cookiePath = singleSignOnConfigurationService.getSingleSignOnCookiePath();
        String cookiePath = "/";
        int cookieMaxAge = getCookieMaxAgeFromConfig();
        String cookieName = singleSignOnConfigurationService.getSingleSignOnCookieName();
        boolean isSecureCookie = Boolean.parseBoolean(secureString);

        String cookieDomain = slingHttpServletRequest.getServerName();
        slingHttpServletResponse.addCookie(addCookieToServletResponse(objectMapper, cookieDomain, cookiePath,
                cookieName, ecodedCookieValue, cookieMaxAge, isSecureCookie));
        objectNode.put(cookieDomain, "{\"" + cookieName + "\":\"" + ecodedCookieValue + "\"}");

        if (StringUtils.isNotBlank(setCookieForOtherDomains) && getBoolean(setCookieForOtherDomains)) {
            String[] singleSignOnCookieDomainNamesArray = singleSignOnConfigurationService
                    .getSingleSignOnCookieDomainNames();
            for (String cookieDomainName : singleSignOnCookieDomainNamesArray) {
                slingHttpServletResponse.addCookie(addCookieToServletResponse(objectMapper, cookieDomainName,
                        cookiePath, cookieName, ecodedCookieValue, cookieMaxAge, isSecureCookie));
                objectNode.put(cookieDomainName, "{\"" + cookieName + "\":\"" + ecodedCookieValue + "\"}");
            }
        }

        if (StringUtils.isNotBlank(setCookieForOthers) && getBoolean(setCookieForOthers)) {
            setCookieForConfiguredDomains(objectMapper, reqAndResMediaType,
                    singleSignOnConfigurationService.getDomainConnectionTimeout(), ecodedCookieValue, objectNode);
        }

        String returnMessage = objectNode.toString();
        LOGGER.info("Final returnMessage : {}", returnMessage);
        slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);

        slingHttpServletResponse.getWriter().write(returnMessage);
        LOGGER.info("End of SingleSignInServlet.doPost().");
        slingHttpServletResponse.getWriter().close();
    }

    /**
     * @param slingHttpServletRequest
     * @param slingHttpServletResponse
     * @param objectMapper
     * @param ecodedCookieValue
     * @param isSecureCookie
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    private Cookie addCookieToServletResponse(ObjectMapper objectMapper, String cookieDomain, String cookiePath,
            String cookieName, String ecodedCookieValue, int cookieMaxAge, boolean isSecureCookie) throws IOException {

        String logString = "Cookie raw details:- CookieName: " + cookieName + ", ecodedCookieValue: "
                + ecodedCookieValue + ", MaxAge: " + cookieMaxAge + ", CookiePath" + cookiePath;
        LOGGER.info(logString);

        Cookie cookie = new Cookie(cookieName, ecodedCookieValue);
        cookie.setSecure(isSecureCookie);
        cookie.setDomain(cookieDomain);
        cookie.setPath(cookiePath);
        cookie.setMaxAge(cookieMaxAge);

        String cookieString = objectMapper.writeValueAsString(cookie);
        LOGGER.info("Final cookieString: {}", cookieString);
        return cookie;
    }

    /**
     * @return true/false
     */
    private boolean getBoolean(String booleanString) {
        return booleanString.equalsIgnoreCase("true");
    }

    /**
     * @param objectMapper
     * @param reqAndResMediaType
     * @param connectionTimeout
     * @param authTokenString
     * @param objectNode
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    protected void setCookieForConfiguredDomains(ObjectMapper objectMapper, String reqAndResMediaType,
            String connectionTimeout, String authTokenString, ObjectNode objectNode) throws IOException {
        String[] singleSignOnDomainsArray = singleSignOnConfigurationService.getSingleSignOnDomains();
        for (String singleSignOnDomain : singleSignOnDomainsArray) {
            String apiUrl = singleSignOnDomain + BookingConstants.SINGLE_SIGN_ON_SERVLET_URL;
            String requestJson = singleSignOnConfigurationService.getSingleSignOnCookieName() + "=" + authTokenString;
            JsonNode rootNode = consumeAPI(objectMapper, reqAndResMediaType, connectionTimeout, apiUrl, requestJson);
            objectNode.put(singleSignOnDomain, objectMapper.writeValueAsString(rootNode));
        }
    }

    /**
     * @param objectMapper
     * @param reqAndResMediaType
     * @param connectionTimeout
     * @param apiUrl
     * @param requestJson
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonGenerationException
     * @throws JsonMappingException
     */
    protected JsonNode consumeAPI(ObjectMapper objectMapper, String reqAndResMediaType, String connectionTimeout,
            String apiUrl, String requestJson) throws IOException {
        String methodParams = "APIUrl: " + apiUrl + ", ReqAndResMediaType: " + reqAndResMediaType + ", RequestJson: "
                + requestJson;
        LOGGER.info("methodParams: {}", methodParams);

        Client client = ClientBuilder.newClient().property(ClientProperties.CONNECT_TIMEOUT, connectionTimeout)
                .property(ClientProperties.READ_TIMEOUT, connectionTimeout);
        Response response = client.target(apiUrl).request(reqAndResMediaType).post(Entity.text(requestJson),
                Response.class);

        LOGGER.info("response.getStatus(): " + response.getStatus() + ", response.getMediaType(): "
                + response.getMediaType());
        LOGGER.info("response.getHeaders(): {}", response.getHeaders());

        String responseReadEntityString = response.readEntity(String.class);
        LOGGER.info("responseEntityString: {}", responseReadEntityString);

        JsonFactory factory = new JsonFactory();

        ObjectMapper mapper = new ObjectMapper(factory);
        JsonNode rootNode = mapper.readTree(responseReadEntityString);

        String rootNodeString = objectMapper.writeValueAsString(rootNode);
        LOGGER.info("rootNodeString: {}", rootNodeString);
        return rootNode;
    }

    /**
     * @return MaxAge form configuration or 900 as default value
     */
    private int getCookieMaxAgeFromConfig() {
        try {
            return Integer.valueOf(singleSignOnConfigurationService.getSingleSignOnCookieMaxAge());
        } catch (NumberFormatException numberFormatException) {
            LOGGER.debug("Exception occured while converting string to integer: {}",
                    numberFormatException.getMessage());
            LOGGER.error("Exception occured while converting string to integer: {}", numberFormatException);
            return 900;
        }
    }

}
