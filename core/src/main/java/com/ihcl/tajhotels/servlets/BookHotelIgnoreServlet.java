package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.exception.HotelBookingException;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.ResStatus;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/bookHotelIgnoreServlet", "sling.servlet.methods={GET,POST}",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=ignorestatus", })
public class BookHotelIgnoreServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(BookHotelIgnoreServlet.class);

    @Reference
    IBookingRequestHandler bookingHandler;


    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        LOG.info("BookHotelIgnoreServlet : Started in doPost");
        LOG.info("Forwarding the request to doGet() method.");
        doGet(request, response);
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        LOG.info("BookHotelIgnoreServlet : Started in doGet");
        ObjectMapper objectMapper = new ObjectMapper();
        String bookingObjectString = null;

        try {
            bookingObjectString = request.getParameter(BookingConstants.FORM_BOOKING_REQUEST);
            if (bookingObjectString == null) {
                throw new HotelBookingException("Missing booking request details from request");
            } else {
                LOG.info("Request received for Ignore : {}", bookingObjectString);
                BookingObject bookingObjectReq = objectMapper.readValue(bookingObjectString,
                        new TypeReference<BookingObject>() {
                        });
                LOG.debug("Booking object as received : {}", objectMapper.writeValueAsString(bookingObjectReq));
                LOG.info("Creating requests for each room and passing to Booking bundle with Ignore status.");
                if (getResponseFromServer(bookingObjectReq, objectMapper)) {
                    LOG.info("All requests ignored successfully");
                    response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
                    response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
                    response.getWriter().write(BookingConstants.IGNORE_STATUS_SUCCESS);
                    response.getWriter().close();
                } else {
                    LOG.info("All requests could not be ignored");
                    response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
                    response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
                    response.getWriter().write(BookingConstants.IGNORE_STATUS_FAILED);
                    response.getWriter().close();
                }

            }
        } catch (HotelBookingException hbe) {
            LOG.error("Booking Request details not found in request");
            response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            response.getWriter().write(BookingConstants.IGNORE_STATUS_FAILED);
            response.getWriter().close();
        } catch (Exception e) {
            LOG.debug("ignore exception: {}", e.getMessage());
            LOG.error("ignore exception: {}", e);
            response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            response.getWriter().write("Unable to call Service.");
            response.getWriter().close();
        }

    }

    /**
     * @param bookingObjectReq
     * @param objectMapper
     * @return
     */
    private boolean getResponseFromServer(BookingObject bookingObjectReq, ObjectMapper objectMapper) {
        boolean allRoomsIgnored = true;
        try {
            bookingObjectReq.setResStatus(ResStatus.Ignore);
            bookingObjectReq.getPayments().setCardNumber(null);
            bookingObjectReq.getPayments().setCardType(null);
            bookingObjectReq.getPayments().setExpiryMonth(null);
            bookingObjectReq.getPayments().setNameOnCard(null);
            bookingObjectReq.getPayments().setTicpoints(null);
            bookingObjectReq.setGuest(null);
            bookingObjectReq.setFlight(null);

            LOG.debug("Booking request : {}", objectMapper.writeValueAsString(bookingObjectReq));
            LOG.info("-----Passing on ignore request to the booking handler-----");
            BookingObject bookingObjectResponse;
            bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectReq);
            LOG.debug("Booking response : {}", objectMapper.writeValueAsString(bookingObjectResponse));
            LOG.info("-----Received Response from the booking handler-----");

            if (bookingObjectResponse.isSuccess()) {
                LOG.info("Ignore request Successful");
            } else {
                allRoomsIgnored = false;
                LOG.debug("Error while booking Room: - {}", bookingObjectResponse.getErrorMessage());
            }

        } catch (Exception e) {
            LOG.debug("ignore exception in getResponseFromServer: {}", e.getMessage());
            LOG.error("ignore exception in getResponseFromServer: {}", e);
        }
        return allRoomsIgnored;
    }
}

