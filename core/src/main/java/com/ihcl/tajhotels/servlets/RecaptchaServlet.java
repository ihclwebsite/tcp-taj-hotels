package com.ihcl.tajhotels.servlets;

import com.ihcl.core.services.booking.RecaptchaConfigurationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


@Component(service = Servlet.class, immediate = true, property = {
        "sling.servlet.paths=" + "/bin/verify/recaptcha"})
public class RecaptchaServlet extends SlingAllMethodsServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecaptchaServlet.class);
    private static final long serialVersionUID = -7854944135605319462L;
    @Reference
    private RecaptchaConfigurationService service;

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
        if (!request.getRequestParameterList().isEmpty()) {
            String urlPath = "https://www.google.com/recaptcha/api/siteverify";
            URL url = new URL(urlPath);
            LOGGER.info("URL set");
            String urlParameters = "secret=" + service.getSecretKey() +
                    "&response=" + request.getRequestParameter("response");
            byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setUseCaches(false);
            LOGGER.info("Request type is set");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
                LOGGER.info("Got response in the reader object");
                StringBuilder sb = new StringBuilder();
                for (int c; (c = in.read()) >= 0; ) {
                    sb.append((char) c);
                }
                String apiResponse = sb.toString();
                LOGGER.info("API Response : " + apiResponse);
                JSONObject myResponse = new JSONObject(apiResponse);
                Object status = myResponse.get("success");
                LOGGER.info("status returned in the response : " + status);
                if (status.equals(true)) {
                    LOGGER.info("Success has been written to output");
                    response.getWriter().write("Success");
                } else {
                    LOGGER.info("Failure has been written to output");
                    response.getWriter().write("Failure");
                }
            } catch (IOException | JSONException e) {
                LOGGER.error("Exception has been caught while parsing recaptcha response : " + e);
            }
        } else {
            String siteKey = service.getSiteKey();
            response.getWriter().write(siteKey);
        }
    }

}
