/**
 * 
 */
package com.ihcl.tajhotels.servlets;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Value;
import javax.servlet.Servlet;

import java.io.IOException;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.json.simple.JSONArray;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.models.RestaurantDataBean;;
/**
 * @author TCS
 *
 */
@Component(service = { Servlet.class },
property = { Constants.SERVICE_DESCRIPTION + "=Servlet to fetch all restaurants data",
        "sling.servlet.paths=" + "/bin/all-restaurant" })
public class AllRestaurantServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = 5158455376139083860L;
	
	private final Logger LOGGER = LoggerFactory.getLogger(AllRestaurantServlet.class);
	
    @Override
    protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse resp) throws IOException {
        try {
            resp.getWriter().write(getRestaurantDetails(req, resp));
        }
        catch(Exception e) {
        	LOGGER.error("error in getting data: "+ e.getMessage());
        	LOGGER.error("" + e);
        }
    }
    
    private String getRestaurantDetails (SlingHttpServletRequest req, SlingHttpServletResponse resp) throws JsonGenerationException, JsonMappingException, IOException {
    	ObjectMapper objectMapper = new ObjectMapper();
//    	JSONObject restaurant = new JSONObject();
    	Map<String, JSONArray> restaurant = new HashMap<>();
    	Map<String, Map<String, JSONArray>> categorizedResponse = new HashMap<>();
    	JSONArray array ;
    	
    	resp.setContentType("application/json");

        ResourceResolver resourceResolver = req.getResourceResolver();

        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page currentPage ;
     
        currentPage = pageManager.getPage("/content/tajhotels/en-in/our-hotels");
        array =  getRestaurants(currentPage , resourceResolver,"taj");   
        if(array != null && !array.isEmpty()){ 
        	restaurant.put("Taj",array);
        }
      
        currentPage = pageManager.getPage("/content/seleqtions/en-in/our-hotels");
        array =  getRestaurants(currentPage , resourceResolver,"seleqtions");   
        if(array != null && !array.isEmpty()){ 
        	restaurant.put("Seleqtions",array);
        }
      
        currentPage = pageManager.getPage("/content/vivanta/en-in/our-hotels");
        array =  getRestaurants(currentPage , resourceResolver,"vivanta");   
        if(array != null && !array.isEmpty()){ 
        	restaurant.put("Vivanta",array);
        }

        currentPage = pageManager.getPage("/content/ama/en-in/our-hotels");
        array =  getRestaurants(currentPage , resourceResolver,"ama");   
        if(array != null && !array.isEmpty()){ 
        	restaurant.put("ama",array);
        }
  
        categorizedResponse.put("restaurant", restaurant);
        return objectMapper.writeValueAsString(categorizedResponse);
        
   }
    
    
    public JSONArray getRestaurants (Page page ,ResourceResolver resourceResolver ,String brandName) {   	 
	    JSONObject  destobj , hotelobj;
	    JSONArray array =new JSONArray();
	    JSONArray hotelarray;
	    String DestName = "";
	    String hotelName = "";
	    String hotelId = "";
	    String hotelLatitude = "";
	    String hotelLongitude = "";
		
	    try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page currentPage = page ;
            Node content = currentPage.getContentResource().adaptTo(Node.class);
            
            Iterator<Page> itrd = currentPage.listChildren();
            while(itrd.hasNext()) {
            	destobj = new JSONObject();
            	hotelobj = new JSONObject();
            	currentPage = itrd.next();
            	
            	try {
            		content = currentPage.getContentResource().adaptTo(Node.class);
            		
            		if(content.hasProperty("city")) {
                	   Value[] values = content.getProperty("city").getValues();
            			DestName = values[0].getString().substring(values[0].getString().lastIndexOf("/") + 1);
                	}
            		else if(content.hasProperty("jcr:title")) {
            			DestName = content.getProperty("jcr:title").getValue().getString();
            	
            		}
            	}
            	catch(Exception e) {
            		LOGGER.error("Error while searching for jcr:title or city property ", e);
            	}
            	
            	try
            	{
            	    currentPage = pageManager.getPage(currentPage.getPath() + "/" + brandName);
            	    Iterator<Page> itr = currentPage.listChildren();
                
            	    while(itr.hasNext()) {
                	   hotelarray = new JSONArray();
                	   currentPage = itr.next();
                   	   content = currentPage.getContentResource().adaptTo(Node.class);
              	  
                   	   try {
                   		   if(content.hasProperty("hotelName")){
   		           			   hotelName = content.getProperty("hotelName").getValue().getString();
   		    	           }
                   		   else if(content.hasProperty("jcr:title")) {
                   			   hotelName = content.getProperty("jcr:title").getValue().getString();
                   		   }
                   		   if(content.hasProperty("hotelId")) {
                   			   hotelId = content.getProperty("hotelId").getValue().getString();
                   		   }
                   		   if(content.hasProperty("hotelLatitude")) {
                   			hotelLatitude = content.getProperty("hotelLatitude").getValue().getString();
                   		   }
                   		   if(content.hasProperty("hotelLongitude")) {
                   			hotelLongitude = content.getProperty("hotelLongitude").getValue().getString();
                   		   }
                   		   
               		   }
   		    		  catch(Exception e) {
   		    			LOGGER.error("Error while searching for hotelName property ", e);
   		    		  }

                   	   try {
              	           currentPage =currentPage.getPageManager().getPage(currentPage.getPath() + "/restaurants");
                       	   Iterator<Page> itra = currentPage.listChildren();
     	    	  
                       	   while(itra.hasNext()) {
     	    		         currentPage = itra.next();
     	    		  
     	    		         try {
     	    		             content = currentPage.getContentResource().adaptTo(Node.class);
     	    		             if(content.getProperty("cq:lastReplicationAction").getValue().getString().toLowerCase().equals("activate"))
     	    		             {
     	    		            	RestaurantDataBean restaurant = new RestaurantDataBean();
 			                     
     	    			          String pageTitle = getPageTitle(content);
 			          	          String areaCode = getAreaCode(content);
 			          	          String jcr_desc = getJcrDesc(content);
 			          	          String jcr_title = getJcrTitle(content);
 			          	          String navTitle = getNavTitle(content);
 			          	          String ogSitename = getOgSitename(content);
 			          	          String rateReview = getRateReview(content);
 			          	          String restaurantId = getRestaurantId(content);
 			          	          String searchBoost = getSearchBoost(content);
 			          	          String seoDescription = getSeoDescription(content);
 			          	          String seoKeywords = getSeoKeywords(content);
 			          	          String servesCuisine = getServesCuisine(content);
 			          	          String jcr_canonical = getJcrCanonical(content);
 			          	          String restaurantPath = currentPage.getPath().toString();
     	    		 
 			          	          restaurant.setPageTitle(pageTitle);
 			          	          restaurant.setAreaCode(areaCode);
 			          	          restaurant.setJcr_description(jcr_desc);
 			          	          restaurant.setJcr_title(jcr_title);
 			          	          restaurant.setNavTitle(navTitle);
 			          	          restaurant.setOgSitename(ogSitename);
 			          	          restaurant.setRateReview(rateReview);
 			          	          restaurant.setRestaurantId(restaurantId);
 			          	          restaurant.setSearchBoost(searchBoost);
 			          	          restaurant.setSeoDescription(seoDescription);
 			          	          restaurant.setSeoKeywords(seoKeywords);
 			          	          restaurant.setServesCuisine(servesCuisine);
 			          	          restaurant.setHotelName(hotelName);
 			          	          restaurant.setHotelId(hotelId);
 			          	          restaurant.setJcr_canonical(jcr_canonical);
 			          	          restaurant.setHotelLatitude(hotelLatitude);
 			          	          restaurant.setHotelLongitude(hotelLongitude);	
 			          	          restaurant.setRestaurantPath(restaurantPath);
 			          	          
 			          	
 			          	          try {
 			          	 
 			          	        	  Node root = content.getNode("root");

 		   	    	                  Node diningDetails =null;
	 		   	    	                  try {
	 		   	    	                	diningDetails = root.getNode("dining_details");
	 			  	                      }
	 			  	                      catch(Exception e) {
	 			  	                    	  LOGGER.error("Error while fetching root node's child nodes ", e);
	 			  	                      }
 		   	    	                  
 				                      String averagePrice = getAveragePrice(diningDetails);
 				                      String currencySymbol = getCurrencySymbol(diningDetails);
 				                      String diningDescription = getDiningDescription(diningDetails);
 				                      String diningID = getDiningID(diningDetails);
 				                      String diningImage = getDiningImage(diningDetails);
 				                      String diningLongDescription = getDiningLongDescription(diningDetails);
 				                      String diningName = getDiningName(diningDetails);
 				                      String diningPhoneNumber = getDiningPhoneNumber(diningDetails);
	 				                  String dinnerTime = getDinnerTime(diningDetails);
	 				                  String dressCode = getDressCode(diningDetails);
	 				                  String lunchTime = getLunchTime(diningDetails);
	 				                  String pathToDownloadFrom = getPathToDownloadFrom(diningDetails);
		 				              String timingslabel1 = getTimingslabel1(diningDetails);
		 				              String timingsvalue1 = getTimingsvalue1(diningDetails);
		 				              String diningCuisine = getDiningCuisine(diningDetails);
 				               
		 				             restaurant.setAveragePrice(averagePrice);
		 				             restaurant.setCurrencySymbol(currencySymbol);
		 				             restaurant.setDiningDescription(diningDescription);
		 				             restaurant.setDiningID(diningID);
		 				             restaurant.setDiningImage(diningImage);
		 				             restaurant.setDiningLongDescription(diningLongDescription);
		 				             restaurant.setDiningName(diningName);
		 				             restaurant.setDiningPhoneNumber(diningPhoneNumber);
		 				             restaurant.setDinnerTime(dinnerTime);
		 				             restaurant.setDressCode(dressCode);
		 				             restaurant.setLunchTime(lunchTime);
		 				             restaurant.setPathToDownloadFrom(pathToDownloadFrom);
		 				             restaurant.setTimingslabel1(timingslabel1);
		 				             restaurant.setTimingsvalue1(timingsvalue1);
		 				             restaurant.setDiningCuisine(diningCuisine);
		 				             
 			          	          }
 			          	
 			          	          catch(Exception e) {
 			          	        	LOGGER.error("Error while fetching root node ", e);
 			          	          }
			          	
 			          	          //JSONObject Jobj = offer.offerJson();
			          	          hotelarray.add(restaurant);
     	    		  
     	    		             }
     	    		         }
     	    		        catch(Exception e) {
     	    		        	LOGGER.error("Error while searching for lastReplicationAction property or jcr:content resource ", e);
                  	         }     	    	
     	    		      }
                          if(hotelarray != null && !hotelarray.isEmpty()){
                             hotelobj.put(hotelName,hotelarray);
                          }
      	               }
                       catch(Exception e) {
                    	   LOGGER.error("Error while fetching offers-and-promotions node ",hotelName, e);
                       }
                   }
                   if(!hotelobj.isEmpty()) {
                      destobj.put(DestName,hotelobj);
                   }
                   if (!destobj.isEmpty()){
                      array.add(destobj);
                   }
                 }
            	 catch(Exception e) {
            		 LOGGER.error("Error while fetching brandName node  ", e);   
 		    	 }
              }
		   }
		   catch(Exception e) {
			   LOGGER.error("Error while fetching argument page and its jcr:content ", e);
		   }
		return array;
	
    }
	
	public String getPageTitle(Node node){
		String title = "" ;
		try {
			if(node.hasProperty("pageTitle")){   
				 title = node.getProperty("pageTitle").getValue().getString();
			}
		}
		catch (Exception e){
			LOGGER.error("Error while searching for pageTitle ", e);
		}		
		return  title ;
	}
	public String getSeoKeywords(Node node){
		String seoKeywords = "";
		try {
			if(node.hasProperty("seoKeywords")){
				seoKeywords = node.getProperty("seoKeywords").getValue().getString();
			} 
		}
		catch (Exception e){
			LOGGER.error("Error while searching for seoKeywords ", e);
		}		
		return  seoKeywords ;
	}
	
	public String getServesCuisine(Node node) {
		String servesCuisine = "";
		try {
			if(node.hasProperty("servesCuisine")){
				servesCuisine = node.getProperty("servesCuisine").getValue().getString();
			}
		}
		catch (Exception e){
			LOGGER.error("Error while searching for servesCuisine ", e);
		}		
		return  servesCuisine ;
	}
	
	public String getAreaCode(Node node)
	{
		String offercode = "" ;
		try {
		    if(node.hasProperty("areaCode"))
		    {
			   offercode = node.getProperty("areaCode").getValue().getString();
		    }
	
		}
		catch(Exception e){
			LOGGER.error("Error while searching for areaCode ", e);	 
		}		
		return offercode;
	}
	
	public String getJcrTitle(Node node){
		String jcr_title = "" ;
		try {
			if(node.hasProperty("jcr:title")){
				jcr_title = node.getProperty("jcr:title").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for getJcrTitle ", e); 
		}		
		return jcr_title;
	}		

	public String getSearchBoost(Node node)
	{
		String searchBoost = "";
		try {
			if(node.hasProperty("searchBoost"))
			{
				searchBoost =  node.getProperty("searchBoost").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for searchBoost ", e);	 
		}		
		return searchBoost;
	}
	
	public String getSeoDescription(Node node)
	{
		String seoDescription = "";
		try {
		if(node.hasProperty("seoDescription"))
		{
			seoDescription =  node.getProperty("seoDescription").getValue().getString();
		}
		
		}
		catch(Exception e){
			LOGGER.error("Error while searching for seoDescription ", e);
		}		
		return seoDescription;
	}
	public String getAveragePrice(Node node){
		String averagePrice = "";
		try {
		if(node.hasProperty("averagePrice")){
			averagePrice = node.getProperty("averagePrice").getValue().getString();
		}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for averagePrice ", e);
		}		
		return averagePrice;
	}
	
	public String getJcrDesc(Node node){
		String jcr_description = "";
		try {
			if(node.hasProperty("jcr:description")){
				jcr_description = node.getProperty("jcr:description").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for jcr description ", e);
		}		
		return jcr_description;
	}
	public String getNavTitle(Node node){
		String navTitle = "";
		try {
			if(node.hasProperty("navTitle")){
				navTitle = node.getProperty("navTitle").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for navTitle", e);
		}		
		return navTitle;
	}
	public String getOgSitename(Node node){
		String ogSitename = "";
		try {
			if(node.hasProperty("ogSitename")){
				ogSitename = node.getProperty("ogSitename").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for ogSitename ", e);
		}		
		return ogSitename;
	}
	public String getRateReview(Node node){
		String rateReview = "";
		try {
			if(node.hasProperty("rateReview")){
				rateReview = node.getProperty("rateReview").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for rateReview ", e);
		}		
		return rateReview;
	}
	public String getRestaurantId(Node node){
		String restaurantId = "";
		try {
			if(node.hasProperty("restaurantId")){
				restaurantId = node.getProperty("restaurantId").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for restaurantId", e);
		}		
		return restaurantId;
	}
	public String getCurrencySymbol(Node node){
		String currencySymbol = "";
		try {
			if(node.hasProperty("currencySymbol")){
				currencySymbol = node.getProperty("currencySymbol").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for currencySymbol", e);
		}		
		return currencySymbol;
	}
	public String getDiningDescription(Node node){
		String diningDescription = "";
		try {
			if(node.hasProperty("diningDescription")){
				diningDescription = node.getProperty("diningDescription").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningDescription", e);
		}
		return diningDescription;
	}
	public String getDiningID(Node node){
		String diningID = "";
		try {
			if(node.hasProperty("diningID")){
				diningID = node.getProperty("diningID").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningDescription", e);
		}
		return diningID;
	}
	public String getDiningImage(Node node){
		String diningImage = "";
		try {
			if(node.hasProperty("diningImage")){
				diningImage = node.getProperty("diningImage").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningImage", e);
		}
		return diningImage;
	}
	public String getDiningLongDescription(Node node){
		String diningLongDescription = "";
		try {
			if(node.hasProperty("diningLongDescription")){
				diningLongDescription = node.getProperty("diningLongDescription").getValue().getString();
				diningLongDescription = StringEscapeUtils.escapeHtml4(diningLongDescription);
				diningLongDescription = Jsoup.parse(diningLongDescription).text().replaceAll("\\<.*?>","");
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningLongDescription", e);
		}
		return diningLongDescription;
	}
	public String getDiningName(Node node){
		String diningName = "";
		try {
			if(node.hasProperty("diningName")){
				diningName = node.getProperty("diningName").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningName", e);
		}
		return diningName;
	}
	public String getDiningPhoneNumber(Node node){
		String diningPhoneNumber = "";
		try {
			if(node.hasProperty("diningPhoneNumber")){
				diningPhoneNumber = node.getProperty("diningPhoneNumber").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningPhoneNumber ", e);
		}
		return diningPhoneNumber;
	}
	public String getDinnerTime(Node node){
		String dinnerTime = "";
		try {
			if(node.hasProperty("dinnerTime")){
				dinnerTime = node.getProperty("dinnerTime").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for dinnerTime", e);
		}
		return dinnerTime;
	}
	public String getDressCode(Node node){
		String dressCode = "";
		try {
			if(node.hasProperty("dressCode")){
				dressCode = node.getProperty("dressCode").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for dressCode", e);
		}
		return dressCode;
	}
	public String getLunchTime(Node node){
		String lunchTime = "";
		try {
			if(node.hasProperty("lunchTime")){
				lunchTime = node.getProperty("lunchTime").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for lunchTime", e);
		}
		return lunchTime;
	}
	public String getPathToDownloadFrom(Node node){
		String pathToDownloadFrom = "";
		try {
			if(node.hasProperty("pathToDownloadFrom")){
				pathToDownloadFrom = node.getProperty("pathToDownloadFrom").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for pathToDownloadFrom", e);
		}
		return pathToDownloadFrom;
	}
	public String getTimingslabel1(Node node){
		String timingslabel1 = "";
		try {
			if(node.hasProperty("timingslabel1")){
				timingslabel1 = node.getProperty("timingslabel1").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for timingslabel1", e);
		}
		return timingslabel1;
	}
	public String getTimingsvalue1(Node node){
		String timingsvalue1 = "";
		try {
			if(node.hasProperty("timingsvalue1")){
				timingsvalue1 = node.getProperty("timingsvalue1").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for timingsvalue1", e);
		}
		return timingsvalue1;
	}
	public String getDiningCuisine(Node node){
		String diningCuisine = "";
		try {
			if(node.hasProperty("diningCuisine")){
				diningCuisine = node.getProperty("diningCuisine").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for diningCuisine", e);
		}
		return diningCuisine;
	}
	public String getJcrCanonical(Node node){
		String jcr_canonical = "";
		try {
			if(node.hasProperty("jcr:canonical")){
				jcr_canonical = node.getProperty("jcr:canonical").getValue().getString();
			}
		}
		catch(Exception e){
			LOGGER.error("Error while searching for jcr_canonical", e);
		}
		return jcr_canonical;
	}
	
}
