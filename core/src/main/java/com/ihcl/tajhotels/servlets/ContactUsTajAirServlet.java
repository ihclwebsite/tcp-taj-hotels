/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.TajAirContactUs;

@Component(service = Servlet.class,
immediate = true,
property = { "sling.servlet.paths=" + "/bin/contactustajair" })
public class ContactUsTajAirServlet extends SlingSafeMethodsServlet {

    String contactUsTemplate = "&lt;html&gt;&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"2\"&gt;&lt;div style=\"background:#896633;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px\"&gt;USER DETAILS &lt;/div&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%;\"&gt; ${fullName} &lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Organization &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${organization}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Email &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${emailId}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Telephone &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${phoneNumber}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Country &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${country}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Subject &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${subject}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Comments &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${comments}&lt;/td&gt;";

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(ContactUsEnquiryFeedbackServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService emailTemplateConf;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        String finalHtmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();

        LOG.trace("Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for contact us in taj air servlet is : " + requestUrl);

        try {
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                TajAirContactUs requestobj = new TajAirContactUs();

                // Fields for email template configuration
                if (null != emailTemplateConf) {
                    // Data from form data.
                    requestobj.setFullName(httpRequest.getParameter("fullName"));
                    requestobj.setPhoneNumber(httpRequest.getParameter("phoneNumber"));
                    requestobj.setEmail(httpRequest.getParameter("emailId"));
                    requestobj.setSubject(httpRequest.getParameter("subject"));
                    requestobj.setCountry(httpRequest.getParameter("country"));
                    requestobj.setOrganization(httpRequest.getParameter("organization"));
                    requestobj.setComments(httpRequest.getParameter("comments"));

                    // Data from osgi configuration.
                    requestobj.setEmailToAddress(emailTemplateConf.getEmailToAddressForTajAirContactUs());
                    requestobj.setEmailFromAddress(emailTemplateConf.getEmailFromAddressForTajAirContactUs());
                    requestobj.setEmailSubject(emailTemplateConf.getEmailSubjectForTajAirContactUs());
                    // requestobj.setEmailTemplateForBody(emailTemplateConf.getEmailBodyTemplateForTajAirContactUs());

                    dynamicValuemap = getDynamicValues(requestobj, dynamicValuemap);

                    //String htmlString = emailTemplateConf.getEmailBodyTemplateForTajAirContactUs();
                    String unescape = StringEscapeUtils.unescapeHtml4(contactUsTemplate);

                    LOG.trace("Unescaped string : " + unescape);

                    requestobj.setDynamicValues(dynamicValuemap);
                    finalHtmlTemplate = StringTokenReplacement.replaceToken(requestobj.getDynamicValues(), unescape);
                    LOG.trace("Final html template for contact us email confirmation is : " + finalHtmlTemplate);
                } else if (null == emailTemplateConf) {
                    LOG.warn("Email Configuration for is missing");
                }

                boolean isSendEmailSuccess = emailService.sendContactUsEmailTajAir(requestobj, finalHtmlTemplate);
                if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }


        } catch (Exception e) {
            LOG.error("An error occured while Sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);

    }

    /**
     * Method to set map fields with token names and values
     *
     * @param dynamicValuemap
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValues(TajAirContactUs requestobj,
            HashMap<String, String> dynamicValuemap) {
        dynamicValuemap.put("fullName", requestobj.getFullName());
        dynamicValuemap.put("phoneNumber", requestobj.getPhoneNumber());
        dynamicValuemap.put("emailId", requestobj.getEmail());
        dynamicValuemap.put("subject", requestobj.getSubject());
        dynamicValuemap.put("country", requestobj.getCountry());
        dynamicValuemap.put("organization", requestobj.getOrganization());
        dynamicValuemap.put("comments", requestobj.getComments());
        return dynamicValuemap;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }

}
