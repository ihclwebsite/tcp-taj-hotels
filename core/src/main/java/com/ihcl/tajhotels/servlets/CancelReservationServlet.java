/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihcl.integration.api.ICancelReservationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.ResStatus;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.integration.dto.reservation.CancelObject;
import com.ihcl.integration.dto.reservation.CancelResponse;
import com.ihcl.integration.dto.reservation.ReadRequest;
import com.ihcl.tajhotels.constants.ReservationConstants;


/**
 * The Class CancelReservationServlet.
 *
 * @author nehapriyanka
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/cancelReservation", "sling.servlet.methods=POST" })

public class CancelReservationServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelReservationServlet.class);

    @Reference
    private ICancelReservationService cancelService;

    private List<CancelResponse> cancelResponseList;


    /* (non-Javadoc)
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest, org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {

		BookingObject cancelledResponse = cancelReservation(request);
		response.setContentType(ReservationConstants.CONTENT_TYPE);
		response.getWriter().write(new GsonBuilder().setPrettyPrinting().create().toJson(cancelledResponse));

	}

    
    /**
     * Cancel reservation.
     *
     * @param request the request
     * @return the booking object
     */
    private BookingObject cancelReservation(SlingHttpServletRequest request) {
    	
    	BookingObject finalCancelResponse = new BookingObject();
    	CancelObject cancelObj = null;
		try {
			ReadRequest requestObj = new ReadRequest();
			String mailId = request.getParameter(ReservationConstants.EMAIL_ADDRESS);
			String hotelId = request.getParameter(ReservationConstants.HOTEL_ID);
			LOGGER.info("mailId "+mailId);
			LOGGER.info("hotelId "+hotelId);
			String itineraryNumber = request.getParameter("itineraryNumber");
			LOGGER.info("itineraryNumber "+itineraryNumber);
			if (StringUtils.isNotBlank(itineraryNumber)) {
				requestObj.setItineraryNumber(itineraryNumber);
			} else {
				String[] reservations = request.getParameterValues(ReservationConstants.CHECKED_ROOMS);
				List<String> reservationList = Arrays.asList(reservations);
				requestObj.setReservationNumber(reservationList);
			}
			String bookingRequestJson = request.getParameter("cancelJson");                           //(ReservationConstants.CANCEL_JSON);
           LOGGER.info("bookingRequestJson "+bookingRequestJson);
			requestObj.setEmailAddress(mailId);
			requestObj.setHotelID(hotelId);

			cancelObj = cancelService.cancelReservation(requestObj);
			if (null != cancelObj) {
				cancelResponseList = cancelObj.getCancelResponseList();
				LOGGER.info("cancelResponseList "+cancelResponseList);
				CancelResponse cancelResponse = cancelResponseList.get(0);
				if (cancelObj.getResResponseType().equalsIgnoreCase("Cancelled")) {
					finalCancelResponse = getCancelJsonResponse(bookingRequestJson, cancelResponse, cancelObj.getResResponseType());
				    LOGGER.info("finalCancelResponse "+finalCancelResponse);
				}
			}
		} catch (Exception e) {
            LOGGER.info("Exception Occured while cancelling the reservation :: {}", e.getMessage());
            LOGGER.info("Exception Occured while cancelling the reservation :: {}", e);
        }
		LOGGER.info("cancelObj "+cancelObj);
        finalCancelResponse.setCancelObject(cancelObj);
        return finalCancelResponse;
    }

	
	/**
	 * Gets the cancel json response.
	 *
	 * @param bookingRequestJson the booking request json
	 * @param cancelResponse the cancel response
	 * @return Booking Object
	 */
	private BookingObject getCancelJsonResponse(String bookingRequestJson, CancelResponse cancelResponse, String resType) {
		Gson gson = new Gson();
        BookingObject cancelObject =  gson.fromJson(bookingRequestJson, BookingObject.class);
        List<Room> roomList = cancelObject.getRoomList();
        for (Room room : roomList) {
        	String reservationNumber = room.getReservationNumber();
        	List<String> cancelledReservations = cancelResponse.getReservationNumber();
        	for (String cancelRes : cancelledReservations) {
				if (cancelRes.equals(reservationNumber)) {
					room.setResStatus(ResStatus.Cancelled);
				}
        	}
        }
        if(resType != null && resType.equalsIgnoreCase("Cancelled")) {
        	
        	cancelObject.setSuccess(true);
        } else {
        	cancelObject.setSuccess(false);
        }
       return cancelObject; 
	}

}
