package com.ihcl.tajhotels.servlets;


import static com.ihcl.tajhotels.constants.HttpResponseMessage.CUSTOMER_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.DATE_VALUE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.FIRST_NAME;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MODIFY_RESERVATION_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.PEOPLE_COUNT;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESERVATION_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESERVATION_TIME;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESTAURANT_ID;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.TABLE_RESERVATION_IS_FAILURE;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.requestdto.ModifyReservationRequest;
import com.ihcl.tajhotels.integration.tableReservations.checkAvailibility.responsedto.MakeReservation;
import com.ihcl.tajhotels.integration.tableReservations.updateReservation.api.IModifyReservationService;

/**
 * Servlet that writes some sample content into the response. It is mounted for all resources of a specific Sling
 * resource type. The {@link SlingSafeMethodsServlet} shall be used for HTTP methods that are idempotent. For write
 * operations use the {@link SlingAllMethodsServlet}.
 */

@Component(
        service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/modifyTableBookingReservation", "sling.servlet.methods=GET",
                "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=groups", })
public class ModifyTableReservation extends SlingAllMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(TableBookReservation.class);

    @Reference
    private IModifyReservationService modifyReservationService;

    private String responseMessage;


    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("Method Entry: " + methodName);
        try {

            response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
            ModifyReservationRequest requestobj = setReservationDetails(request);
            MakeReservation updateReservation = (MakeReservation) modifyReservationService
                    .modifyReservation(requestobj);
            boolean reservationSuccess = updateReservation.isSuccess();
            LOG.debug("Reservation status received from the service :" + reservationSuccess);
            if (reservationSuccess) {
                LOG.debug("Update Table Reservation Status response from service as: " + responseMessage);
                writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, updateReservation,
                        reservationSuccess);
            } else {
                LOG.debug("Update Table Reservation Status response from service as: " + responseMessage);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, updateReservation,
                        reservationSuccess);
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching room availability.", e.getMessage());
            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, null,
                    TABLE_RESERVATION_IS_FAILURE);
        }
        LOG.trace("Method Exit: {}" , methodName);
    }

    /**
     * @param request
     */
    private ModifyReservationRequest setReservationDetails(SlingHttpServletRequest request) {
    	String sessionValue;
        int restaurantID = Integer.parseInt(request.getParameter("restaurantID"));
        String reservationId = request.getParameter("reservationID");
        int customerID = Integer.parseInt(request.getParameter("customerID"));
        String dateValue = request.getParameter("dateVal");
        String peoplecount = request.getParameter("peopleCount");
        String mealType = request.getParameter("mealType");
        String firstName = request.getParameter("firstName");
        String emailID = request.getParameter("emailID");
        String mobileNumber = request.getParameter("mobileNumber");
        String reservationTime = request.getParameter("reservationTime");
        String eventName = request.getParameter("eventName");
        if (mealType.equals("Breakfast")) {
            sessionValue = "1";

        } else if (mealType.equals("Lunch")) {
            sessionValue = "2";
        } else if (mealType.equals("Dinner")) {
            sessionValue = "3";
        } else if (mealType.equals("Brunch")) {
            sessionValue = "4";
        } else {
            sessionValue = "0";
        }
        ModifyReservationRequest requestobj = new ModifyReservationRequest();
        requestobj.setRestaurantId(restaurantID);
        requestobj.setReservationId(reservationId);
        requestobj.setCustomerId(customerID);
        requestobj.setFirstName(firstName);
        requestobj.setEmail(emailID);
        requestobj.setMobile(mobileNumber);
        requestobj.setOccasion(eventName);
        requestobj.setSessionName(sessionValue);
        requestobj.setNoOfPeople(peoplecount);
        requestobj.setReservationDate(dateValue);
        requestobj.setReservationTime(reservationTime);
        LOG.trace("The Restaurant ID: " + restaurantID);
        LOG.trace("The Reservation ID: " + reservationId);
        LOG.trace("The Customer ID: " + customerID);
        LOG.trace("The final date: " + dateValue);
        LOG.trace("The People count: " + peoplecount);
        LOG.trace("The MealType: " + mealType);
        LOG.trace("The First Name: " + firstName);
        LOG.trace("The Email ID " + emailID);
        LOG.trace("The Mobile Number: " + mobileNumber);
        LOG.trace("The reservation Time " + reservationTime);
        LOG.trace("The Event Name " + eventName);
       
        LOG.trace("The Session Value: " + sessionValue);
        return requestobj;
    }


    /*private void fetchReservationDetails(MakeReservation updateReservation) {
        String methodName = "fetchReservationDetails";
        LOG.trace("Method Entry: " + methodName);
        reservationId = updateReservation.getReservationID();
        customerID = updateReservation.getCustomerId();

        dateValue = updateReservation.getReservationDate();

        peoplecount = updateReservation.getNoOfPeople();

        firstName = updateReservation.getFirstName();

        emailID = updateReservation.getEmail();

        reservationTime = updateReservation.getReservationTime();
        LOG.trace("The Reservation ID:: " + reservationId);
        LOG.trace("The Customer ID : " + customerID);
        LOG.trace("Method Exit: " + methodName);

    }*/

    /**
     * @param response
     * @param code
     * @param updateReservation 
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code,
            MakeReservation updateReservation, boolean reservationSuccess) throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        if (null == updateReservation) {
        	jsonNode.put(MESSAGE_KEY, MODIFY_RESERVATION_FAILURE);
        } else {
        	jsonNode.put(MESSAGE_KEY, updateReservation.getMessage());
        }
        jsonNode.put(RESPONSE_CODE_KEY, code);
        if (reservationSuccess && null != updateReservation) {
            jsonNode = populateJson(jsonNode, updateReservation);
        }

        response.getWriter().write(jsonNode.toString());
        LOG.trace("The Value of JSON: " + jsonNode);
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @param jsonNode
     * @param updateReservation 
     * @return
     */
    private ObjectNode populateJson(ObjectNode jsonNode, MakeReservation updateReservation) {
    	
        String methodName = "populateJson";
        LOG.trace("Method Entry: " + methodName);
        jsonNode.put(RESERVATION_ID, updateReservation.getReservationID());
        jsonNode.put(CUSTOMER_ID, updateReservation.getCustomerId());
        jsonNode.put(DATE_VALUE, updateReservation.getReservationDate());
        jsonNode.put(PEOPLE_COUNT, updateReservation.getNoOfPeople());
        jsonNode.put(FIRST_NAME, updateReservation.getFirstName());
        jsonNode.put(EMAIL_ID, updateReservation.getEmail());
        jsonNode.put(RESERVATION_TIME, updateReservation.getReservationTime());
        jsonNode.put(RESTAURANT_ID, updateReservation.getRestaurantId());
        LOG.trace("Method Exit: " + methodName);
        return jsonNode;
    }

    public IModifyReservationService getModifyReservationService() {
        return modifyReservationService;
    }

}
