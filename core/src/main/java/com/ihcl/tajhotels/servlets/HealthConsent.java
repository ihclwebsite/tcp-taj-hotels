package com.ihcl.tajhotels.servlets;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.HealthConsentBean;
import com.ihcl.tajhotels.email.api.IEmailService;


/**
 * @author TCS
 *
 */
@Component(service = Servlet.class,
immediate = true,
property = { "sling.servlet.paths=" + "/bin/health-consent" ,"sling.servlet.methods={GET,POST}",
        "sling.servlet.resourceTypes=services/powerproxy", "sling.servlet.selectors=commitstatus", })
public class HealthConsent extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Reference
    private IEmailService iEmailService;

    private static final Logger LOG = LoggerFactory.getLogger(HealthConsent.class);
    
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private ObjectMapper objectMapper;

    @Override
    protected void doPost(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws IOException, ServletException {
        String methodName = "doPost";
        LOG.trace("Method entry: " + methodName);
        objectMapper = new ObjectMapper();
        JSONObject dataJson = new JSONObject();
        try {
        	resp.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");
        	String consentDataString = req.getParameter("consentData");
        	LOG.info("consentData : "+ consentDataString);
        	if(consentDataString != null) 
        	{
	        	HealthConsentBean consentDetails = objectMapper.readValue(consentDataString, HealthConsentBean.class);
	        	long timestamp = (new Date().getTime() / 1000);
	        	Date date = new Date();
	        	SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	            String submissionTimeStamp = sdf.format(date);
//	        	captureHealthConsentData(consentDetails,timestamp);
	        	boolean isEmailSuccess = sendEmailToCustomerAndHotelAuthority(consentDetails,timestamp,submissionTimeStamp);
	        	JSONObject responseJson = new JSONObject();
	        	if(isEmailSuccess)
	        	{
		            responseJson.put("StatusCode", 200);
		            responseJson.put("message", "Email sent successfully");
		        	dataJson.put("firstName", consentDetails.getFirstName());
		        	dataJson.put("lastName", consentDetails.getLastName());
		        	dataJson.put("title", consentDetails.getTitle());
		        	responseJson.put("data", dataJson);
		        	String responseString = responseJson.toString();
		            resp.getWriter().println(responseString);
	        	}else {
	        		responseJson.put("status", 500);
	        		responseJson.put("message", "Unable to send mail");
	        	}
        	}
        	else 
        	{
        		resp.sendError(400, "Please provide form details.");
        	}
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            resp.sendError(500, e.getMessage());
        }
        LOG.trace("Method exit: " + methodName);
    }
    
    private void captureHealthConsentData(HealthConsentBean healthConsentData, long timestamp)
            throws RepositoryException, PersistenceException, LoginException {
    	LOG.debug("Inside captureHealthConsentData method");
        final String dataStorePath = "/content/usergenerated/health-consent-data";
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        Resource dataStorePathResource = resourceResolver.getResource(dataStorePath);
        String timeStamp = fetchCurrentTimeStamp();
        String[] splitTimeStamp = timeStamp.split(" ");
        String[] splitYearMonthDate = splitTimeStamp[0].split("/");
        String currentYear = splitYearMonthDate[0];
        String currentMonth = splitYearMonthDate[1];
        String currentDay = splitYearMonthDate[2];
        String time = splitTimeStamp[1].replaceAll(":", "").replaceAll("\\.", "");
        LOG.trace("UserData id {}", time);
        if (null != dataStorePathResource) {
            Node dateStoreNode = dataStorePathResource.adaptTo(Node.class);
            LOG.trace("Obtained Data Store Node as {}", dateStoreNode);
            Session session = dateStoreNode.getSession();

            if (dateStoreNode.hasNode(currentYear)) {
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                if (currentyearNode.hasNode(currentMonth)) {
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    if (currentMonthNode.hasNode(currentDay)) {
                    	Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        addDataToNode(userNode, healthConsentData, timestamp);
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                        resourceResolver.commit();
                        resourceResolver.close();
                    } else {
                    	currentMonthNode.addNode(currentDay, "nt:unstructured");
                        session.save();
                        Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        addDataToNode(userNode,healthConsentData,timestamp);
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                        resourceResolver.commit();
                        resourceResolver.close();
                    }

                } else {
                	currentyearNode.addNode(currentMonth, "nt:unstructured");
                    session.save();
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    currentMonthNode.addNode(currentDay, "nt:unstructured");
                    session.save();
                    Node currentDayNode = currentMonthNode.getNode(currentDay);
                    currentDayNode.addNode(time, "nt:unstructured");
                    session.save();
                    Node userNode = currentDayNode.getNode(time);
                    addDataToNode(userNode,healthConsentData,timestamp);
                    Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                    ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                    resourceResolver.commit();
                    resourceResolver.close();
                }
            } else {
            	dateStoreNode.addNode(currentYear, "nt:unstructured");
                session.save();
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                currentyearNode.addNode(currentMonth, "nt:unstructured");
                session.save();
                Node currentMonthNode = currentyearNode.getNode(currentMonth);
                currentMonthNode.addNode(currentDay, "nt:unstructured");
                session.save();
                Node currentDayNode = currentMonthNode.getNode(currentDay);
                currentDayNode.addNode(time, "nt:unstructured");
                session.save();
                Node userNode = currentDayNode.getNode(time);
                addDataToNode(userNode,healthConsentData,timestamp);
                Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                resourceResolver.commit();
                resourceResolver.close();
            }
        }
    }
    
    private String fetchCurrentTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSS");
        String timeStamp = sdf.format(date);
        LOG.trace("Current Timestamp {}", timeStamp);
        return timeStamp;
    }
    
    private void addDataToNode(Node healthConsentlNode ,HealthConsentBean consentDetail, long timestamp) throws ItemExistsException, PathNotFoundException, VersionException, ConstraintViolationException, LockException, RepositoryException {
    	String nodeName = consentDetail.getFirstName()+"-"+consentDetail.getLastName()+ "_" + timestamp;
    	Node data = healthConsentlNode.addNode(nodeName);
    	LOG.info("node name "+ nodeName);
        data.setProperty("FirstName", consentDetail.getFirstName());
        data.setProperty("LastName", consentDetail.getLastName());
        data.setProperty("Email", consentDetail.getEmail());
        data.setProperty("Mobile Number", consentDetail.getPhoneNumber());
        data.setProperty("Address Line", consentDetail.getAddress());
        data.setProperty("City", consentDetail.getCity());
        data.setProperty("Pincode", consentDetail.getPincode());
        data.setProperty("State", consentDetail.getState());
        data.setProperty("Country", consentDetail.getCountry());
        data.setProperty("passport No", consentDetail.getPassportNo());
        data.setProperty("Organization", consentDetail.getOrganization());
        data.setProperty("Close Relative’s Name", consentDetail.getRelativesName());
        data.setProperty("Relative Contact No", consentDetail.getRelativePhoneNumber());
        data.setProperty("Arriving from", consentDetail.getArrivingFrom());
        data.setProperty("Arrival Date in Hotel", consentDetail.getArrivalDateInHotel());
        data.setProperty("Arrival Date in India", consentDetail.getArrivalDateInIndia());
        data.setProperty("Departure To", consentDetail.getDepartingTo());
        data.setProperty("Departure Date", consentDetail.getDepartureDate());
        
    }
    
    private boolean sendEmailToCustomerAndHotelAuthority(HealthConsentBean user, long timestamp, String submissionTimeStamp) throws IOException,LoginException,PathNotFoundException,ValueFormatException,RepositoryException {
        String subject = "Health Report - "+user.getFirstName()+" "+user.getLastName()+" - "+user.getHotelName()+", "+user.getHotelCity();
        String htmlMessage = getHtmlMessage(user,submissionTimeStamp);
        String to = "";
        if(user.getMailToSelf().equalsIgnoreCase("yes")) {
        	to = user.getEmail()+","+user.getHotelEmail();
        }
        else {
        	to = user.getHotelEmail();
        }
		String cc = "";
		String bcc = "";
		PDDocument doc = createPDDocument(user,timestamp,submissionTimeStamp);
		boolean isSendEmailSuccess = iEmailService.sendHealthConsentMail(subject, htmlMessage, to, cc, bcc, doc,user.getFirstName()+user.getLastName()+timestamp);
		LOG.error("SendQuoteEmail response: " + isSendEmailSuccess);
        if (isSendEmailSuccess) {
        	LOG.trace("Email has been successfully ");
        } else {
        	LOG.error("Email has not sent");
        }
        return isSendEmailSuccess;
    }
//    
    private PDDocument createPDDocument(HealthConsentBean user, long timestamp, String submissionTimeStamp) throws IOException, LoginException, RepositoryException {
    	PDDocument doc = new PDDocument();
		try {
			System.out.println("Inside createPDDocument");
			String title = (user.getHotelName()+", "+user.getHotelCity()).toUpperCase();
			int start = (int) Math.round(300 - (title.length()/2)*11.8);
			doc = PDDocument.load(new File("/mnt/GuestHealthReports/HealthReportTemplate.pdf"));
//			doc = PDDocument.load(new File("/HealthReportTemplate.pdf"));
			PDPage page = (PDPage) doc.getDocumentCatalog().getPages().get(0);
			PDFont font = PDType1Font.HELVETICA_BOLD;
			PDFont font2 = PDType1Font.HELVETICA;
			PDPageContentStream contents = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.APPEND, true);
			contents.setNonStrokingColor(Color.BLACK);
			addText(font, 18, start, 750, title, contents);
			addText(font2, 8, 47, 660, user.getFirstName()+" "+user.getLastName(), contents);
			addText(font2, 8, 332, 660, user.getCountry(), contents);
			addText(font2, 8, 449, 660, user.getPhoneNumber(), contents);
			addText(font2, 8, 47, 630, user.getAddress(), contents);
			addText(font2, 8, 332, 630, user.getPassportNo(), contents);
			addText(font2, 8, 47, 600, user.getOrganization(), contents);
			addText(font2, 8, 332, 600, user.getRelativesName(), contents);
			addText(font2, 8, 449, 600, user.getRelativePhoneNumber(), contents);
			addText(font2, 8, 47, 557, user.getArrivingFrom(), contents);
			addText(font2, 8, 152, 557, user.getArrivalDateInHotel(), contents);
			addText(font2, 8, 242, 557, user.getArrivalDateInIndia(), contents);
			addText(font2, 8, 332, 557, user.getDepartingTo(), contents);
			addText(font2, 8, 449, 557, user.getDepartureDate(), contents);
			if(user.getListedSymptoms().length()>12) {
				addText(font2, 8, 150, 500, user.getListedSymptoms(), contents);
			}
			else {
				addText(font2, 10, 485, 507, user.getListedSymptoms(), contents);
			}
			addText(font2, 10, 485, 482, user.getContactWithPatient(), contents);
			addText(font2, 10, 485, 452, user.getQuarantine(), contents);
			addText(font2, 10, 485, 427, user.getConfirmedPatient(), contents);
			addText(font2, 10, 485, 409, user.getBeenToRedZone(), contents);
			addText(font2, 8, 465, 100, submissionTimeStamp, contents);
			addText(font, 8, 70, 60, user.getHotelName()+", "+user.getHotelArea()+", "+user.getHotelCity()+" - "+user.getHotelPin(), contents);
			addText(font, 8, 70, 50, "Tel: +"+user.getHotelStd()+" "+user.getHotelPhone()+" Fax: +"+user.getHotelStd()+" "+user.getHotelFax()+" "+user.getHotelEmail()+" "+user.getBrandSite(), contents);
			addText(font, 8, 70, 40, "CIN No: L74999MH1902PLC000183", contents);
			
//			String signatureString = "iVBORw0KGgoAAAANSUhEUgAAAMgAAAA/CAYAAAClz4c/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAHYYAAB2GAV2iE4EAACFlSURBVHhe7X0HmBzVle7fE3pSTw6aHDTKQkZCSEgDBgMmGYMNC5+9z4DXsMZ+ttfvrff79q1t1sa79nq9i9fmw8/f8vbZ7zljE01Yk7QYJIJQAhRGYTSSJufcPZ37nXNv3e7q6qpO0yOJt/PP1FTVrXNPuufcUF1dYwsRcM6gRNu0PcOsbAnvR/h8fhzYexonjs+hvrYJ7auK0NyWp12NtK8IQfq1ZckyfUjawmTnJh6ytP05AhttNNysbAnvR7z6Yj9+/ZMxDPVl4fdPjOP5Z6aAQDDSB+qha3JOEJkkRGtKHA19Qo2NjOFMdy+Cfq9WIhHhmRrOcYKkAuUs3i/hfIfH7cSZrll86ENtuPSKagwOulBSkgtkZ8f0fzZtmAgGZdvyudw4PBN3mPrAf+rxV/Hg91/C8SPj8Hh8FDHy2nmXIOkoEx/spEzztIZqrHSRefvPLlK3nwMwKDZGdlY2Ltpais7jvfjBA10oKbahvtGHkaHZGN/wOSdEVpYMR5UgySQHQ9VjHZbVrkJF1RZMT+fTKBKpzTQRuuSxaGsQZfT5Cr3ZZnpygCRyaDwbz3f7EyE5/dmHiiZIPmNf8sZ+C2L/26fxwHd64Q3mYdvWcvgDfvT1zeGa66pw9fV1cBQXyqpiVpB68JrB5fTD7bahsNCGvDxKNG1dky7OyiJdLyL9oEnPiVaBzjopvayuG3UNcQRQFVu2pA8STVaS9ih+ZnzflyA7eLSw0UhhhHNuHq++NEDbCOqbl+GWP61Fc0s+pibceOQXPXjx+T4sb8/D9R9rxsYLS1FWno+cXDvVzEySRKBP4PRwVhIkyAszAquqgis1yPrpODCTAckJwt7KCtugXMf8OWDoyEKWStRYfdg24Rlxdj4jFAzA4w0gPz+HzmJ1drvdOPjOMF54fghDZwpw4021+OA1ZTRScPBLBPw+7N8zhldeHsKpnnEUEKvK6hysWV2FxmYHquscqKgsQElZYcLOx+jLROfpIEGC8KWFCWBwcNBgJznxkGfK1qiGnkBdW7guYSiz2YF8nIIj/dTIo/2jyMnJRsWySmTnUCtTbxokNuZJwCIiZfI4hLHRSQqoSRQVOrBxczXs+bG98blFAK45J9mXjYG+ebzx2jScLi9KS20oq8iGozQLRUX5mJvzo7dnBiePzaH3TAir11bgxpubsHodT6HM/epx03pkeA5HDs3gVNccxkY86O+dwcioE3d8uhU33roSdnskscxgnB2Ez1XbMhY3QQK0sYB0en1r8NSEBMsTMkAGDp9TkNE+KyxzYcYZoXdoMMC28RRJ3lVRTojX47jn3eg8PIIJatiiwiDqWypo4VmNHHsuXQ1ziJbDWSNsosQh62xa+eBAL751/3NwTa9DaVEZtm6rxm13VqGggHmdewT8Xrz0wnE89eQgNm5swFu7htHeXoMtW2swNjaNwcExzLu9yKOkDiIX9txcNNZX4OJLqrB8ZQH5MZWYCSFAs4yek7M4dGgCF15UgebWUipPs/3J/0HqsEQbpKRHLJIYQRiZDVQGi1WiI72AXpXMy9SDg5ghhnHawudhXaJx/NhpPPbbE3DPOnDNtc3Y3EHBXGjnqoRYXYV9Gk8eNTlBgjRFyeLbnIT3DpzEwz/qxJ999nKq7cGvftJPc/UGXH5VtbieMlge7aQ6C/Pd8PAUnv39Gex8ZRKH3gtizZpqbNpYjC/8VQPZLBPY46bRxRlAgGyyUwdRUsrTrtQQCgVwsmsC/T2TWN5SiqYVbPvCAlpA+EKN2AvzRQJtkhegAixZsPKsP081IlDyjDL1NJmBlC8UiD43wO2ap2DZhR/98A3yewnu+Mw6bL+yHoVFeRp9bB0GJ0eQpmKcKJweTKZGD7/fTVOSaVx37Xps2VaCTRcXo6Q8C2+/NQevR45sqYLl8KjINVXHkyqCIS/p0IPv3P8eDhzw4b/cuQrr1vtx/HgfOq4oDScHg0eO8ko7qqppvZBycgTQdWIIv/1lJ5558gympoDymhIqz0ByMERbMi/ztkkFZ2WRTq7X9gzroLIG10+hHicrOyhpcqmffiSbd7oxOuLG7jfHMTExiG0dK2nor9WupweRLNR4p04P4aEH9uFTd3Rg87ZyuhLEf/xhAL//3SS+cl87WtoLqCw1H4lm5M8gyG6ZuAHMzngo6G1wFNnFOsIKwZAfb+3sw78/24fZ6Sx86Oo2XPfRZSikJcSxzjGMjwSwaQuPmGpxnh48Hg86j4zj5Rf6MD0BXLS5Fh+6pg7lFZmbVuqnt5nA+yJBxO3EVOaSIlhIRpJVVOCKY9J1sH8a/adm6CQf+cVFNKcupMWoZJaJBti/rxs//N4pfOnLW7D1Mu45KRCPDOOBvx/FvV9qxpZLi6kk/UDkRHnnwCm8tsMJe7YD13+sBq3tDu1iNFwuF559qhtPPTGCyy9vxg031aKlzZw2XbhcTuzdM4g91NlMTuRj7foKbO+oIL8WaRSLB33bpoPMpVpcsIL6LTWknMLkEF7uG2E1DWQH8i3MicFZnKJF4tRwAE3N1dhIveaGjUXh5GAsxNkK42NuOJ354lEIhZLSXKHz6BiXRdZn8WBF03loBE89MonB3kJaO01i394eTIzPa1cVgjTNGcZPHz6JXa/O4lN3rcCff4FGrzSSw+jXYMBN/vRganoWL/yhC9//9jH85qfDKCqoxJ/fu5qmbk1nJTkYC+3/z9IIsjCwigsNTGWmkU8o6MPklBtzFLQhTzYKivNQWVuI7PyFJ4IVnnv2EP71R9P42tfWYPvllaJscnQGf3/fED58QyU+8nFZlghmfjl2tBdPP3EEzslKjE/4xRO0g2d8aG4uRl1LENW1dsw6Pdi/9wz6zuTRNGc5jRrVaGrhaV368Hl9GBicolHrFOZnAXteEd475ML8XA62XVKHa2+sQl1z6gv5c42zNIKkBm54fd6mmhz6ugrMI5pPECPDE9i7ux8nO6eRm5+PptWlqGktWtTkYNB4RZsfubkR9/NjGLm5c7Dbk1+km/nl3585hp4zIYxMTlJy2OH3hlDXUI7LrqqBh47ffKMf+/ZNobikGvfcuwF33dOQdHKY+VUiiCOHhvDdb76H5x4LYt/uXPT3Aldd3YCv3b8Gd36u5n2ZHIzzcgRRKqWaGArGdQLz0/Nyu7zoPDyGzvcm0NRaRgvlWhQW8SJ2cRNDYcdLnfjBPw/gvvs2YdvlFaLsdNcQvvX1o7jr7nW48jq+3ZmOLl7cfefjOPwusG17A0oqc1DmKMOdn2lDTR0nAd/pYt/aIJ8QYRnyPBHUNCpq/RWuGsJA3ywOvetEYUEh6hvz0NiaQ8m+OD5V7Wts18XAeTmCsNGJDGfnsKPM8tuqbiBAU4t9XfjZwydw7N0AtmxrpZ61kZJjYXdnzGCml0KxoxBZ/jxMTfJoIXHy5AzmXCFULuMvFKWuy0DfBH7xf47A7axBbX01TaNCqKmpxMdub6TkiHxJiR+Tycpm/kpGcrLM2iTSBqCkKKFpVB35sxTLV+VTciTvU+YTz19GcHLo2z5e3VR5G3FeJkhCkL2hQAABr4c9oBVGYGxIPj/R1YsfP7QT//itvRSYQVx+XRlWri+iaxrRWURpSQFyQQkyptzvx/79syivdqCpOdW1QBAvPd+FH3zvIA6+48dX/sfF+PG/deD+b2/GvZ9fiRUrU/98wSygTBNEDCHm9IsFNZIpfRaaAInw/kwQaif+0M2Wo90/J/9YOWlm2oVdr3TjZ/+ri3pXWqg2tmDK6UWBg01fvOwwBpMe5VV5yC9x4nS3U5yf7BrD7jencdWH61DkiHyqH4uIjV6fF8c6h/HDf96Ppx6dwgevXIVv/cMmXHxJKRqaCtHc6kBefnrNG+VLPrTwLffkUVOuNGGWfFY+UPL0CRJPBzPeqeC8XYMkY5RQXWtA3mXxg5DhegHseWsAb78xiaqqUmy7tBqOEje++tUX0dSwFl+970Ko/Drb8HmdeOC7B3CqqxTfe7AVv/z5fuzcEcItt9fQYt2DnJw8VNfYUduQj5aWShQXy1ElEPBhdMSJ7pOT2LXrFIb7bWhpbMCtn2xFY0v8B/sYqqlVYCUVOByn/AFknKewY3ixmCRYvx+QMEHEZfpd6BdPUkHSjUcQj9Jr9OpR+qHBKex7cwSdnT6aYpTj6muXobgsG6+/fhD//S9exzf+9mbcdEu9oF0w9N7TVObeL37PGsDTT3TioX+ZpaRw4LWdXVjVtoZ6fT+a2xykvweHDw/C559BXU0JKqur4aXjuVkPRgZ9mJnLwoYNjei4rALrLqgUj30kA/Yrb6ybUUdLnYV9HAAptD/3/gt6kuH8gWmCqCLV0yRKEKZJNqAzDlZVE+2cm8MrL3Xj3T0zaGmrx8c/0UqjhnT6xPgUvvfdXXjn7Wp85x8vwMUd1o9iK0QajujIRrJSnEtTtbr8tC77iBtXkEWCMB4GB6bw7W+cwO7dw7hwswP2HDduuHElbr6lXaMAJYMLR4+OYnjAS0kxjbLSQhpRatC+yoHyynyNyhyptIloYw1WdRRNMjz5OTT13Fm6SEX/xURGplgcSGzMYhokZPCPSaIGaeqxf38PXnj+FM1fSnHt9a34wEUVWs/K5tmw8z9O4+UXh9A3YMM996xDxxX8OEd8RIKC/pB8fkyfS7LFSBUbAEyvGjaRLyYmJnD/N94ivYJobLaJO2wP/OAqGhHKNIqFwWpEUPolBWE+20Q+SGEGkZIMC2SCRyawsDTXwA3BBmUSUfzoWH2bz4jZmTk88qvD+N0v+9BQ24Iv/reN2NJRrZt2SCc7CnNxSUclAiEP8nRPpcZDJNBp44cAs7LJ1si9fX58/XT3BIb650nHSCehb1gzv0xNzuAnDx9EwJ+LL//lBVi/thF/dvdGrFkrn8vKBPQ6KLAuSh/9sRX4OxU8QurrJAMz2akiEzwygYyMIMwiVYOUWKt6iXjOzbmw8zVahNNao625Clde04KmVnW/3wTU2Pv39dLi+Di++XcfxOr18acoMSB9WGOlUyjkx44X38Wjv3Hhwg+04TOfq0OB+LAxGkY7es4M4ec/7cTUdC6+9OUNaF1eKnTjBIyHdHysh7F+Iv8z9HXO53XCYiIpa5UzjVDlvLeiSQR9Pf2xseH0106fGsb/fKgTu9/y4NJL2/CJu5abJwfrJdYIdEwByI0cCgD2XP4QK8IvGbB8/nag4EeYnJjFjpfGMTrmQL4j33KhrOyYm5vFM08fxN9+/Q3MOrPw+S+sk8nBEMkRX590/atgrM96xUsOhv56MvT/PyLuCMKX2Clqb4S+3GrOmwj6nslKjoLf58Zzzx3DKy9MYc36NvzJ7ctQvUzd3jSpRz0zsxd60eVdrx7Hjx88iQcfuhbVDayrtSwj+G4Z65fNvGg+/uTju/HoI3MI+qqxtSMHt95SK+6U2fNzSZ4MpplpJ86cnsTRY4PY/fokTanyccWVK3DL7U0o4LcVpCBf7xs+ZsTzlYKqF8+36bbd2ca50DNugrBC7NRkGiJV6BssXuNJhDA0OIpnnjyBzqM+XH/dalxzQ12iWYmA3oY/PHMYv/p5P/713z4MRxnLM5ep9FGuodqRYwp+n3caf/PXu9B5qBA1tQVoa8tBa7MdXSdH4PbyIn6eHJsPXyCLRqs8FDtyxIsMOjqaUNuQ/IOBVj7RdyqJYMUnHv9zibCfzxOdEyYII6NZq4kTL24gY5Mx+J39p/H0Y6dQXlaBj97Wjrbl6X2h58nfduLNN/34px9u0EqMYN0iL13g6RT/2EKkp3h+SaK76zjuvfcdDA9WYs3qQtxxVz0uvawSLpePFuB+8EtO+Bt8eTRKFDvs4rvr/CIz5m0GJU8PszKFeEGUCPrmTrd+KvXi2WEG5s9bRmNuAbCFAn5eIpJCcg69UAfGg3CW4ElbkqxnZybx8IOdyMutxB2fbUVZpV2sIzhwFcQdLt7TH78/ILZg0AbPvA+uOQ+mppwYGBjH0093EVETbr2tWDz6XVycR4Gbh7LyItTXF6O4xORBQW4w2glf0ML84Dt9+On/7qb65WhqcWDDBWX4yEcrkZUTv0FTCaxUgzAeUk0+PZiOoadNpm5iGsmXOGv7CMxk6qHiM9OxaQVbKBgQItVXWhczQdKB1+3Bgd1jOHJoCgUUwO1tpeLbd4GgB67ZAGbm5jE5PUMuz6VADlIyTME174d3PhtOZ4gSK5eSoARZNO157fUutDQ3omNbOdW3ie9j8PuZenrdcDsDuOvu9bRw5s9HIretIz4I4vlnj+GJx4dR31CNz39xFSoqaM2Rx9d5E15kwgUjboBZiMlkUiksXjCqGIvlm0jm4ulkjozc5k0EY6+QTC+kRygQwuysB243qcra+kOUBB5MT/kwNj4Hl8cnvsFWWAQ4ikJwlBSiIM+O/IIs8XK3/AI7rWHG8C/fP4jbPrER27bm4fDBSYyPheDzezE6OoFf/N9O3HN3Bz756RUkINr54+OjeOx3J/DqDjduuGEVPnprDcrLEz/7lC64SSwDgH3JnZnhMk8Hz+bjQP9ZQAkSpBmKbBCzRkk1mJNB3ABYJOzd3Y1f/WwcV1xdiJ07+1FSVI1ly/Ip0Wbxxx3dmJnxYuOmevGGjYqqYlRXF9Gow18jnaE10BjKyorw2c9txvoNmfmkO13o+zM+Dnc6/EwaudSqrTLt88WKCzH7IjWTSfazEUeUIAGRIKyVWZKkooQwkLDYSluDv4DEsmMb7pFf7sOTjzlpeubD1u0N+NitK5Btc+PJx7txtNNN65JmLKvNwdioF9OTNHWbccPldlHPnIOGphJsvKgSVVXqbeTmONv269tGyWaYybcKaDHyMP3ZUTkuxK102ng9l8yzXIuRpEaIEYR9y05igbznAhasFLB0rq6BGEynEE/xxTNMBUlsa3/9r3fgjy/7cdPNrfibb65Eb98IJUwP/L58fPzWVixfEf2YB9vGfpFvXtQKrcBibfIzF4byGftG+ZMOiIQIMmC38DPpxG9rNOqmbxOrYz04KLN4ypaGWvJ/gbCN7AArBpKGO+GEMURswq8MtYA+dsJ1uYNfpOllzBpEnbJgoUCI9mQkZ7TxGm9hZbVGUzTqrli6ELJ1DWo8TwhSg+8/cR0PTaPuvusFVFWsxm2fbKSFOb8KZxgr19bgxptakZdnpivbES1fb28UhM1CYLhWiOQqfUVA0wVRN4EJydjJNMyPW4blMMx8lVRHpBROA8IuQlYWM7Fq78gzdBEV+UCeGHVMZH/MdeYt78OK30wjhiULVwqIvfg1VzjGEM0TVvQLATtGBEay0JE653yYmrHDlp2Pzk4Xhoe9uOX21TRytFFymHtVTjsj0PslBsJPcpM+i6YVdbmHixQJ6G1Kxb6wLrSpevr6aq/XwRJJkMSDlBEvMqWu0S/+i9jJ1XkkSlbnmOs0enFN/qt4ZBKJc07YL5WShsYeM8SckZzAI0fsWxB5bSB7GyuIzzJ40wxVvFUvpZdlCb1/WB1y3sjwFH796/cw2O9BdVUOOi5z4FOfXo1Vayq0nsucr9kiMb4OdI2vq43AdrD+yh51rMBl+nO93QnBZETL9GwH11W8FI+keaUI1SYMKSNajv66vKau8z7a51yfWej9YEQ0v1jwSzR5lhOHRdrI6G1eNsR8SFcGml2TEGpomnCSJJwamIHrC98H0N8/iZ2vjuLEUTcGBjwodjjwxb/g997yU7yRBrKE0EVjGIec9bYKRPaHuEZsOOGsaFUTWPFhROpKWlYqnuzFgl7Xcy1fDxV7mdYpowlippxcnCnDIqOOpSFEFm+hZp2EEn29Y3h79wSOHB5DcUkptm9rQHNzAcmzoaY2B9k5Smas7CidWF3Wmc/p10xfLuPNSh9LGw1QPSTTWtFLXnxEtEE6sAoGKhMK028iX1mB+TLMdAnL5HZi/nG+q74YsNJN6RVjM5GLOhq5mU3xoCWIFCqRGoP4UHwlTxH44WN5jXtWGze4GGV4r82raXqUbOPOzrpwtLMfb+7ilyNnYcXKKmzd3oiVq5J7kYFyWryA0l/jYwbXMzo8moe0Ua1nVLlwOYHrKvn6MoZRl8i5pJO+IhBvLpGzWipj3XT10oVRvimEYHl4/iIy9WSk9BJ0gpYgam5MDc4WGxo9NUSUEcrRj83yDocOmnwFUU8XQLEIYXhoBp1HRsT/vHPO5qC1rRwdly/D8hUOizqxSCoQDFAON5MR0ZdpJB2PXgxFzzL5mDdr+6JhSUfl/COaLYkl5bmA8pdCInuVrcn6xgi9PHkLWp0zr7QShFwsd8SC/hhv0XKHmRRfVkSOBHwkkk1sIThnPJic9GBq0oWZqVlk52ajsNCOmroyVFcXkkjzJIp1kh/d3ePY+9Y4TnfzG8tt2HJxPT5wURkqqvK0xbWUHg/RToxPmx6Yv9Ijmn+sTRHoE5bpJK3S0bwOI51EPxtQNuhhpaeyQdks/iOXoJX0ydrIdAz2mc1mDF5rH5oheg3Ch6J+hIkQRsVmc81ohQPi8xoewmRiBDA2PIPDBwew/0AvpidyaU1QhJLSLOTYfPD5gwj4/dTzB1BdVYbLrmxB+1rzfz/G/y9vbNSJzsOTeGfvFDxuP9pX1GDthkraikkHpmIz9MZHzIoul9A3nPigTCAUnvopu2RjxdY3gutFpo/RQc5IJwnl1IxGG9rJ28TWPLktZEBYy1G2JBtoElKWmQ/NINec0bTJ+JBpeIvWSwZ6dIAboNVjLfV15RMC2kkC2fFgukjnImWQumxloJ5WYWJ8gqY9PXjjjTFkZxdh9epyrL2gAmXlBSgqykUejR5+Sg6vx4eRgSkc2NNLCQDc/IkLdf/oJQS3x4tjR2kadXgEp09Oo7ioBBdsqEH7yiI0tqr/L8H6memmnMuIdrDSOWybNgViXiJB6FQ52xh4+rqyTLmP9uJ+I4+edE0LEtHBEJif0Vdh+QbeCrIuTZy4SBRH6BhGXrxZBb7+Ou/1deODdWB5kRGe6zOYh/5Y0jFieZvJtOaTPORHAxJRtoebn64aEjYVJEyQRND3RvPOWbz6x6PYt2cEdlsJOq5YhQ9srkBxcfy3iLhnZvDEo0cxN12IP7lzBSWGB3veHMFgTwBeWsDXNhRRYpRi5eoi2O3RAWDdG+rNkrYou2LqEKnsg5hO1tPbr6dXPCI+Ui+g1uqHtEDSqiv3msnVX2NEeEowPcPcvlgY6+shZTMNy+OS5HhKsB7meisdxW1sopNUsdNlM9sVHyv79HU0Uk33CJTNRv7hc65o4ZNkYJogKYGmE/ydis7Dfdi1qxduZx4u2daIzdvqtLemJ8a804l9b/XiyUdHkVfgQH1DIUrL87FyVSklRjEcZebrk1TBTmNnGoMoUWOpRjCHcp/amzd2IsSXYYVIWqu/1mBKaSdv0s70dDWD1CRZXeJDtYX0Bx3TuZhtEvTtE023OFh4ggS9OPB2D959bw4ty6uwadMylFWql0oTawvlPV4XznTPYaDHi9FhJ/p65tHTO4uK8jLc9dnlaGguRE74M4vMwNjLMNh83pST9c7Wl+uPzaHcmIiGN6aJpjPTLTH4n7ZRPRHosTyN4B6e14eRZtHoleoKqjiOzYn9EQuuw0hUL5qORig6DScg/197DewzRup+Sx6pJQhTEjnvWHfXrIsW4cNwTmZh0/Y6lFZavZcqJEaZOacHQ30uHD00hfEJJ61DgLpllVjeXo72VQU4c2oAL+8Yx+1/ugb1jZFHy42NYRVMYoTgH6s5p3Ao1dNVVU5mxOud9DKN+sRDNH+aCnBd3mQB/THnY5TPiC3j6Z0sY6P4a8ZygazKGIqW0yNAVBxg+ikiX6I2DRAvOhdlCezkct5SDUzli1TqRetANoibAJH6kieVceLEqipgZUcySDpBBBlT0l7syKG9Zwbw1O960dpSj4/c1gx7HjUSXZ93eWnaFIBzhjbnPGam/ejvcWFq0oPC/CLUNeehYpkddQ0OVNdE3vIx2j+J3zxyFKtWNeL6m5q00uQN5G8exv2AUVmqY8UOVryNMpRrzMqTdXh0gqjA40QmxPnQyiyYuIxlZ4dviYuWkIfhrOfgjdgUKY8G85K8SQ4FnXgam865g+F9IkTqmyMVH8WD4sN7daznK/1EbW7Qm2kZTJtI13hILUHkAalDyoT8mJucw7t7JjA8FEJNdQGV+sVTEJNTs3C6QvC5s5GdlYMKuta2qpj2djgctJXKXiwM5k2/Po9XfO+797QX//UrF0M3miYHwSa1hlFON4N0PvudnKu8lDxrAeah+EfkMDPtmA7FNMmQLPoGVuAy3mIbW8ePjmVAqHPzwIjYTTYq2wSoTJOZyDdWQWfUU9wCJz5cpmDF1wqqrrFeuFz8iVzT0/NxqvIU4ieIjA+C7F1ihIpLIcxM0dSpf168+ID1sBfloqTCLr7SavW2j0jwUQU+FPsAerpHsOMPg9i8vRnrLixDjngLYvzGSg9Gs2MdyffS+SaEtJcK+BIdR2hkIIiYEuexPlLHcSHsl4dSkNROVktQV4do/Zkpc7HuZUz14yKtXN1ATdj7EpnsNokP/apbr+IJb+4gJBWHCoH+kDzmmbR/Fgglh5GqLBsFaii6Ejc6XQibxZBGsSDeohzGstOwTygt5GgMmD/9BH0+dB8bx0DfONrXVdJivVYYFa/HSgUsg3/lJ6wKbADLiLZNJAgRC/k6JwsacU768p6SW35IGO0jS38xYnwmL/D/a2ff85lcS8XazDzNGjpSrmyTOplB6caI1U/TRfwlDiayokDixIejPAoSKbeVqEF/2IfSX/yrcSS7+Lr4HgifpvDlOqWz0CmsoLa3gOqMGVG2JgFDgrAVPM9lA6hMNFA0jI2zoMBlA4XBvCl55Ebi6Zn3isdRsnNzqVgGXsKGMgHzEtW48UhM0EaLUdFo2nUqY74yYPjt7dG2Mdg+1TAMQUHnnD/Mh6nUFEnyiSRF2D+CiAv4l/4QE/0n+GIj34vpiChjAsNUlMD8WN/4viA+JILFMmcZjtH0Sk9GYn4SenoFDnr+oJUTRIsaWSZs0xbVTKidK3BqsKuFfmE/SLAcMxkMlsNQPhXTU83XVlC2JmunHoYpFh0KBTSBSTotVaFWWFCyWUA0EKtHOsoRQpTyHwFZxgnCZ+zAiHzlmhj7tHJ2Fb9xUe8DY53wufhDf+mUG1yWKb5cwmVaOdXhcJO+YBpZzsfJ+VsFBFvKAar4SIT9rOmi56nnrXQ3g6JTvJhW8RFXtOvikQ91zgcsk/nyKZXpZStExQGRiiTQYlL6SdZVvAQfrZxh5Mcwk5MMTNYgfJo6o4yCVAprkYZR8WEwNwYpyMuoq8z0WghzPb9M+zAFKDXOoQoLQdJ3sZawhP+M0MaxJSxhCWZYSpAlLCEOlhJkCUuIg6UEWcIS4mApQZawBEsA/w9J3nBCXjtmfAAAAABJRU5ErkJggg==";
			String signatureString = user.getSignature();
			LOG.trace("signatureString {}", signatureString);
			byte[] signatureByte = Base64.getDecoder().decode(signatureString.getBytes());
			PDImageXObject signature = PDImageXObject.createFromByteArray(doc, signatureByte, "Signature");
//			contents.drawImage(signature, 0, 280);
			contents.drawImage(signature, 45, 280,70,70);
			
			contents.close();
			doc.save("/mnt/GuestHealthReports/"+user.getFirstName()+user.getLastName()+timestamp+".pdf");
			doc.close();
		} finally {
			doc.close();
		}
    	return doc;
    }
	   public static void addText(PDFont font, int fontSize, int offsetY, int offsetX, String text, PDPageContentStream contents) throws IOException {
			contents.beginText();
			contents.setFont(font, fontSize);
			contents.newLineAtOffset(offsetY, offsetX);
			contents.showText(text);
			contents.endText();
	   }
    
	    private String getHtmlMessage(HealthConsentBean healthConsent, String submissionTimeStamp) {
	    	String HtmlTemplate = "<body style=\"padding:0; margin:20px 0 0 0\">\r\n" + 
	    			"        <table width=\"600\" cellspacing=\"0\" align=\"center\" bgcolor=\"#efeeee\" cellpadding=\"10\">\r\n" + 
	    			"            <tbody><tr class=\"first-cell\">\r\n" + 
	    			"                <td align=\"center\">\r\n" + 
	    			"                    <table width=\"480\" cellpadding=\"6\">\r\n" + 
	    			"                        <tbody>\r\n" + 
	    			"                        <tr>\r\n" + 
	    			"                            <td align=\"center\" style=\"font-size:35px; font-weight:bold\">"+ healthConsent.getHotelName()+", "+healthConsent.getHotelCity()+"</td>\r\n" + 
	    			"                        </tr>\r\n" + 
	    			"                        \r\n" + 
	    			"                        <tr>\r\n" + 
	    			"                            <td align=\"center\" style=\"font-size:18px; font-weight:bold\">Health Screening Form for Guest</td>\r\n" + 
	    			"                        </tr>\r\n" + 
	    			"                    </tbody></table>\r\n" + 
	    			"                </td>\r\n" + 
	    			"            </tr>\r\n" + 
	    			"            <tr class=\"second-cell\">\r\n" + 
	    			"                \r\n" + 
	    			"                <td align=\"center\">\r\n" + 
	    			"                    <table width=\"460\" bgcolor=\"white\" cellpadding=\"7\">\r\n" + 
	    			"                        <tbody><tr class=\"form-subtitle\">\r\n" + 
	    			"                            <td>\r\n" + 
	    			"                                <table cellpadding=\"7\">\r\n" + 
	    			"                                    <tbody><tr><td style=\"font-size: 20px;\">Greetings,</td></tr>\r\n" + 
	    			"                                    \r\n" + 
	    			"                                    <tr><td style=\"font-size: 16px;\">Attached is the self declared form:</td></tr>\r\n" + 
	    			"                                </tbody></table>\r\n" + 
	    			"                            </td>\r\n" + 
	    			"                        </tr>\r\n" + 
	    			"                        <tr class=\"userdata\">\r\n" + 
	    			"                            <td>\r\n" + 
	    			"                                <table>\r\n" + 
	    			"                                    <tbody><tr class=\"user-details\"><td>\r\n" + 
	    			"                                        <table>\r\n" + 
	    			"                                            <tbody><tr>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Name:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+ healthConsent.getFirstName()+" "+healthConsent.getLastName()+"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Guest Nationality:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+ healthConsent.getCountry() +"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Personal Contact No:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+healthConsent.getPhoneNumber()+"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                            </tr>\r\n" + 
	    			"                                        </tbody></table>\r\n" + 
	    			"                                    </td></tr>\r\n" + 
	    			"                                    <tr class=\"user-dates\"><td>\r\n" + 
	    			"                                        <table>\r\n" + 
	    			"                                            <tbody><tr>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Arriving from:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+healthConsent.getArrivingFrom()+"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Arrival Date in India:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+healthConsent.getArrivalDateInIndia()+"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                                <td width=\"170\">\r\n" + 
	    			"                                                    <table width=\"170\">\r\n" + 
	    			"                                                        <tbody><tr><td style=\"font-weight: bold; color: gray;\">Submission Date Time:</td></tr>\r\n" + 
	    			"                                                        <tr><td style=\"color: darkgrey; font-size: 14px;\">"+submissionTimeStamp+"</td></tr>\r\n" + 
	    			"                                                    </tbody></table>\r\n" + 
	    			"                                                </td>\r\n" + 
	    			"                                            </tr>\r\n" + 
	    			"                                        </tbody></table>\r\n" + 
	    			"                                    </td></tr>\r\n" + 
	    			"                                </tbody></table>\r\n" + 
	    			"                            </td>\r\n" + 
	    			"                        </tr>\r\n" + 
	    			"                        <tr class=\"dates\">\r\n" + 
	    			"                            <td>\r\n" + 
	    			"                                <table>\r\n" + 
	    			"                                    <tbody><tr>\r\n" + 
	    			"                                        <td>\r\n" + 
	    			"                                            <table width=\"250\" bgcolor=\"#e3e3e3\" cellpadding=\"3\">\r\n" + 
	    			"                                                <tbody><tr><td style=\"font-weight: bold; color: gray; font-size: 16px;\">Arrival Date in Hotel</td></tr>\r\n" + 
	    			"                                                <tr><td style=\"color: darkgrey;\">"+healthConsent.getArrivalDateInHotel()+"</td></tr>\r\n" + 
	    			"                                            </tbody></table>\r\n" + 
	    			"                                        </td>\r\n" + 
	    			"                                        <td width=\"1\"></td>\r\n" + 
	    			"                                        <td>\r\n" + 
	    			"                                            <table width=\"250\" bgcolor=\"#f5f5f5\" cellpadding=\"3\">\r\n" + 
	    			"                                                <tbody><tr><td style=\"font-weight: bold; color:gray; font-size: 16px;\">Departure Date</td></tr>\r\n" + 
	    			"                                                <tr><td style=\"color: darkgrey;\">"+healthConsent.getDepartureDate()+"</td></tr>\r\n" + 
	    			"                                            </tbody></table>\r\n" + 
	    			"                                        </td>\r\n" + 
	    			"                                    </tr>\r\n" + 
	    			"                                </tbody></table>\r\n" + 
	    			"                            </td>\r\n" + 
	    			"                            \r\n" + 
	    			"                        </tr>\r\n" + 
	    			"                    </tbody></table>\r\n" + 
	    			"                </td>\r\n" + 
	    			"                \r\n" + 
	    			"            </tr>\r\n" + 
	    			"        </tbody>\r\n" + 
	    			"    </table>\r\n" + 
	    			"</body>";
			return HtmlTemplate;
	    }
    
    
}


