package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.HttpHeaders;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;


import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import org.apache.sling.api.resource.ResourceResolver;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.osgi.service.component.annotations.Reference;

import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.models.HotelDetailsFetcher;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.core.util.ObjectConverter;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.HotelAvailabilityResponse;
import com.ihcl.integration.dto.availability.HotelDto;
import com.ihcl.integration.dto.availability.RoomAvailabilityDto;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Flight;
import com.ihcl.integration.dto.details.Guest;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.integration.dto.details.Payments;
import com.ihcl.integration.dto.details.ResStatus;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.integration.dto.details.TicPoints;
import com.ihcl.integration.exception.ServiceException;
import com.ihcl.integration.rest.service.api.IRestService;
import com.ihcl.loyalty.config.ISibelConfiguration;
import com.ihcl.loyalty.dto.IRedemptionTransactionsRequest;
import com.ihcl.loyalty.dto.IRedemptionTransactionsResponse;
import com.ihcl.loyalty.dto.ITdlRedemTransactionRequestDto;
import com.ihcl.loyalty.services.IRedemptionTransactionsService;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;
import com.ihcl.integration.dto.details.OpelBookingResponse;




@Component(service = Servlet.class,
property = { 
		"sling.servlet.paths=" + "/bin/postPaymentServletOpel", 
		"sling.servlet.methods=get",
	    "sling.servlet.extensions=html",
		"sling.servlet.selectors=groups", 
		})
public class BookingConfirmServlet extends SlingSafeMethodsServlet{

	/**
	 * 
	 */
	   @Reference
	    private IRoomAvailabilityService roomAvailabilityService;

	    @Reference
	    private IRedemptionTransactionsService redemptionTransactionsService;

	    @Reference
	    private IBookingRequestHandler bookingHandler;

	    @Reference
	    private TajHotelsBrandsETCService tajHotelsBrandsETCService;

	    @Reference
	    private ISibelConfiguration iSibelConfiguration;

	    @Reference
	    private IRestService restService;
	    
		private static final long serialVersionUID = 1L;
		
		private static final String TAPP_ME = "TAPPMe";
	
	    private static final String TAP = "TAP";
	
	    private static final String TIC = "TIC";
	    
	    private final String EPICURE = "EPICURE";
		
	    private String serviceURL = "https://sandbox.juspay.in";
	
	    private final String apiKey = "ED099A96D81489BAB1EB3D31673C98";
		
		private static final Logger LOG = LoggerFactory.getLogger(BookingConfirmServlet.class);
		
	 @Override
	    protected void doGet(final SlingHttpServletRequest request,final SlingHttpServletResponse response) throws ServletException, IOException {
        String methodName = "doGet";
        LOG.info("Method Entry: {}", methodName);
        try {
            LOG.info("BookHotelConfirmServlet Started in doGet");
            bookHotelConfirmationMethod(request, response);
        } catch (Exception e) {
            LOG.info("Exception at doGet of BookHotelConfirmServlet :: {} ", e.getMessage());
            LOG.info("Exception at doGet of BookHotelConfirmServlet", e);
        }
        LOG.info("Method Exit: {}", methodName);
    }
    

    private void bookHotelConfirmationMethod(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String methodName = "bookHotelConfirmationMethod";
        LOG.info("Method Entry: {}", methodName);
        try {

        	ObjectMapper objectMapper = new ObjectMapper();
        	objectMapper.configure(Feature.AUTO_CLOSE_SOURCE, true);
        	objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            PrintWriter responseWriter = response.getWriter();
            
            String orderId = request.getParameter("order_id");
            LOG.info("orderId" +orderId);
            
            String status = request.getParameter("status");

            String websiteId = request.getServerName();
            
            RequestPathInfo requestPathInfo = request.getRequestPathInfo();
            String resourcePath = requestPathInfo.getResourcePath();
            
            websiteId = setDefaultWebsiteId(websiteId);

            TajHotelsAllBrandsConfigurationsBean etcAllBrandsConfig = tajHotelsBrandsETCService.getEtcConfigBean();
            Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean = etcAllBrandsConfig.getEtcConfigBean();

            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig = etcConfigBean.get(websiteId);

            Map<String, TajHotelBrandsETCConfigurations> etcWebsiteConfigBean = individualWebsiteAllConfig
                    .getEtcConfigBean();

            String bookingType = identifyBookingType(resourcePath);

            TajHotelBrandsETCConfigurations etcWebsiteConfig = etcWebsiteConfigBean.get(bookingType);
            String configLoggerString = "websiteId: " + websiteId + ", bookingType: " + bookingType
                    + ", etcWebsiteConfig: " + etcWebsiteConfig;
            LOG.info("Final ETC Configuration: {}", configLoggerString);

            String detailsresponse;
        	if(status.equalsIgnoreCase("CHARGED")) {
        		
        		LOG.info("inside if condtion for passing status charged");
        		detailsresponse = fetchPaymentJusPay(serviceURL, orderId);
        		
                if(detailsresponse != null){
                    
                	OpelBookingResponse opelBookingResponse = objectMapper.readValue(detailsresponse, OpelBookingResponse.class);
                	
                	BookingObject bookingObjectRequest = getBookingObjectFromOpelResponse(opelBookingResponse);

                	LOG.info("status_id " + opelBookingResponse.getStatus_id());
                	LOG.info("getStatus " + opelBookingResponse.getStatus());
                	
                	String redirectUrl = etcWebsiteConfig.getPaymentConfirmUrl();
                	if(opelBookingResponse.getStatus_id() != null && 
                			Integer.compare(Integer.parseInt(opelBookingResponse.getStatus_id()) ,21) == 0 && 
        					opelBookingResponse.getStatus() != null &&
                			opelBookingResponse.getStatus().equalsIgnoreCase("CHARGED")) {
                		
                        checkCorporateBooking(bookingObjectRequest, bookingType);
                        handlePaymentResponse(request, response, individualWebsiteAllConfig, etcWebsiteConfig,
                                bookingObjectRequest, redirectUrl, responseWriter, opelBookingResponse);
                   }
                   else{
                	   ignoreBooking(bookingObjectRequest);
                   }
	        	}
	        	else {
	                LOG.debug("detailsresponse is null");
	                ObjectNode responseNode = objectMapper.createObjectNode();
	                responseNode.put("message", "Can not confirm payment details");
	                responseWriter.write(responseNode.toString());
	        	}
            }
            else {
            	LOG.debug("payment status is not charged");
                ObjectNode responseNode = objectMapper.createObjectNode();
                responseNode.put("message", "Payment Failed!");
                responseWriter.write(responseNode.toString());
            }
                
        } catch (Exception e) {
            LOG.debug("IOException found in bookHotelConfirmationMethod :: {}", e.getMessage());
            LOG.error("IOException found in bookHotelConfirmationMethod", e);
            response.sendError(500, "Servers are not running at this movement. Please try again after sometime");
        }
        LOG.info("Method Exit: {}", methodName);
    }
    
    private String fetchPaymentJusPay(String serviceURL, String orderid ) {
        byte[] authEncBytes = Base64.encodeBase64(apiKey.getBytes());
        String authStringEnc = new String(authEncBytes);
        String detailsresponsejson = null;
        LOG.info("Entered readJsonFromUrl method and authEnc is = "+authStringEnc);
        String endpoint = "/orders/" + orderid;

        try {
            MultivaluedMap<String, String> mMap = new MultivaluedHashMap<String, String>();
            mMap.add(HttpHeaders.CONTENT_TYPE, "application/json;charset=utf-8");
            mMap.add("Authorization", "Basic " +authStringEnc);
            
        	detailsresponsejson = restService.consumeRestGetAPI(serviceURL, endpoint, "application/json", null, mMap, "30000");
        	
        } catch (Exception e) {
            LOG.info(e.getMessage(), e);
        }
        LOG.info("detailsresponsejson "+ detailsresponsejson);
        return detailsresponsejson;
        
    }
    
    private BookingObject getBookingObjectFromOpelResponse(OpelBookingResponse opelBookingResponse) {
    	LOG.info("Start: get BookingObject FromOpelResponse");
    	BookingObject bookingObject = new BookingObject();
    	Guest guest = new Guest() ;
    	Payments payments = new Payments() ;
    	TicPoints ticpoints = new TicPoints();
    	Guest loggedInUser  = new Guest();
    	guest.setEmail(opelBookingResponse.getCustomer_email());
    	guest.setPhoneNumber(opelBookingResponse.getCustomer_phone());
    	
    	if(opelBookingResponse.getUdf9() != null && !opelBookingResponse.getUdf9().isEmpty()) {
    		guest.setMembershipId(opelBookingResponse.getUdf9());
    		loggedInUser.setMembershipId(opelBookingResponse.getUdf9());
    	}
    	
    	ticpoints.setPointsType("TIC");
    	if(opelBookingResponse.getUdf10() != null && !opelBookingResponse.getUdf10().isEmpty()) {
    		ticpoints.setNoTicPoints(Integer.parseInt(opelBookingResponse.getUdf10()));
    	}
    	
    	payments.setCreditCardRequired(true);
    	if(opelBookingResponse.getCard() != null && opelBookingResponse.getCard().getName_on_card() != null) {
	    	payments.setNameOnCard(opelBookingResponse.getCard().getName_on_card());
    	}
    	if(opelBookingResponse.getCard() != null && opelBookingResponse.getCard().getExpiry_month() != null) {
    		payments.setExpiryMonth(opelBookingResponse.getCard().getExpiry_month());
    	}
	    if(opelBookingResponse.getStatus_id() != null && 
	    		Integer.compare(Integer.parseInt(opelBookingResponse.getStatus_id()) ,21) == 0) {
	    	bookingObject.setSuccess(true);
	    }
	    
    	payments.setPayOnlineNow(true);
    	//payments.setPayGuaranteeAmount(opelBookingResponse.getCard());
    	payments.setTicpoints(ticpoints);
    	
    	bookingObject.setItineraryNumber(opelBookingResponse.getOrder_id());
    	bookingObject.setHotelId(opelBookingResponse.getUdf5());
    	bookingObject.setGuest(guest);
    	bookingObject.setPayments(payments);
    	bookingObject.setChainCode(opelBookingResponse.getUdf6());
    	bookingObject.setCheckInDate(opelBookingResponse.getUdf3());
    	bookingObject.setCheckOutDate(opelBookingResponse.getUdf3());
    	
    	if(!StringUtils.isEmpty(opelBookingResponse.getUdf4())) {
    		loggedInUser.setCustomerHash(opelBookingResponse.getUdf4());
    	}
    	if(!StringUtils.isEmpty(opelBookingResponse.getUdf8())) {
    		loggedInUser.setAuthToken(opelBookingResponse.getUdf8());
    	}
    	bookingObject.setLoggedInUser(loggedInUser);
    	bookingObject.setPaymentStatus(true);

    	LOG.info("Exit : get BookingObject FromOpelResponse");
    	return bookingObject;
    }

    
    private void handlePaymentResponse(SlingHttpServletRequest request, SlingHttpServletResponse response,
            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig, TajHotelBrandsETCConfigurations etcWebsiteConfig,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter, 
            OpelBookingResponse opelBookingResponse) throws IOException {
        boolean paymentStatus = bookingObjectRequest.isPaymentStatus();
        LOG.info("PaymentStatus(): {}", paymentStatus);
        if (paymentStatus) {
            handlePaymentSuccess(request, response, individualWebsiteAllConfig, etcWebsiteConfig, bookingObjectRequest,
                    redirectUrl, responseWriter, opelBookingResponse);
        } else {
            handleFailure(response, etcWebsiteConfig, bookingObjectRequest, redirectUrl, responseWriter);

        }
    }
    
    private void checkCorporateBooking(BookingObject bookingObjectRequest, String ccAvenueType) {
        if (ccAvenueType.equals(ReservationConstants.CORPORATE_BOOKING_CCAVENUE)) {
            bookingObjectRequest.setCorporateBooking(true);
        }
    }


    private void handlePaymentSuccess(SlingHttpServletRequest request, SlingHttpServletResponse response,
            TajhotelsBrandAllEtcConfigBean individualWebsiteAllConfig, TajHotelBrandsETCConfigurations etcWebsiteConfig,
            BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter, 
            OpelBookingResponse opelBookingResponse) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectConverter converter = new ObjectConverter();


        BookingObject bookingObjectResponse = buildResponseForHotelConfirmation(bookingObjectRequest, objectMapper,
                request, opelBookingResponse);
        String responseString = objectMapper.writeValueAsString(bookingObjectResponse);
        LOG.info("Response received : {}", responseString);

        if (null != bookingObjectResponse) {
            bookingObjectResponse.setCorporateBooking(bookingObjectRequest.isCorporateBooking());
            LOG.info("here ::::::::::::: " + bookingObjectResponse.isSuccess());
            if (bookingObjectResponse.isSuccess()) {
                String returnMessage = null;
                bookingObjectResponse = setAdditionalDetailsToResponse(request, bookingObjectResponse,
                        bookingObjectRequest, converter, etcWebsiteConfig.getContentRootPath(),
                        individualWebsiteAllConfig.isAmaBooking());
                if(isRedemptionBooking(bookingObjectRequest)) {
                	TicPoints ticpointDto = bookingObjectResponse.getPayments().getTicpoints();
                	ticpointDto.setRedeemTicOrEpicurePoints(true);
                	ticpointDto.setPointsPlusCash(true);
                	Payments redemPayment = bookingObjectResponse.getPayments();
                	redemPayment.setTicpoints(ticpointDto);
                	bookingObjectResponse.setPayments(redemPayment);
                }
                returnMessage = objectMapper.writeValueAsString(bookingObjectResponse);
                handleReservationSuccess(response, returnMessage, redirectUrl, responseWriter, bookingObjectResponse);

            } else {
                handleBookingFailed(response, objectMapper, bookingObjectRequest, redirectUrl, responseWriter,
                        bookingObjectResponse);

            }
        } else {
            handleFailure(response, etcWebsiteConfig, bookingObjectRequest, redirectUrl, responseWriter);
        }
    }
    
    private BookingObject buildResponseForHotelConfirmation(BookingObject bookingObjectRequest,
            ObjectMapper objectMapper, SlingHttpServletRequest request, OpelBookingResponse opelBookingResponse) {


        BookingObject bookingObjectResponse = null;
        LOG.info("Creating requests for each room and passing to Booking bundle with Commit status.");
        try {
            Payments guestPayment = bookingObjectRequest.getPayments();
            bookingObjectRequest.setResStatus(ResStatus.Commit);
            if (guestPayment.isPayAtHotel()) {
                guestPayment.setCardNumber(null);
                guestPayment.setCardType(null);
                guestPayment.setExpiryMonth(null);
                guestPayment.setNameOnCard(null);
            }
            if (isRedemptionBooking(bookingObjectRequest)) {
            	
            	ITdlRedemTransactionRequestDto redemReq = buildRedemptionRequest(bookingObjectRequest, opelBookingResponse);

                //fetchHotelDetails(bookingObjectRequest, request, redemptionTransactionsRequest);

                bookingObjectResponse = confirmRedemptionBooking(bookingObjectRequest, guestPayment, objectMapper,
                		redemReq);

            } 
            else {
                String debugBookingObjectRequest = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.debug("--> TIC TD CALL :OUTSIDE OF THE FLOW  <-- ");
                LOG.info("Final Booking Commit Request String : {}", debugBookingObjectRequest);
                LOG.debug("-----Passing on bookingDetails object to the booking handler-----");
                bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                debugBookingObjectRequest = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received Booking Commit Response String : {}", debugBookingObjectRequest);
            }
            LOG.debug("--> bookingObjectRes : Received Response from the booking handler <--");
            return bookingObjectResponse;
        } catch (IOException e) {
            LOG.error("Exception occured in buildResponseForHotelConfirmation", e);
        } catch (ServiceException se) {
            LOG.error("Exception occured in parsing TIC Sible call: {}", se.getMessage());
            LOG.error("Exception occured in TIC Sible call:: {}", se);

        } catch (Exception e) {
            LOG.error("Exception occured in buildResponseForHotelConfirmation", e);
        }
        return bookingObjectResponse;

    }

    
    private ITdlRedemTransactionRequestDto buildRedemptionRequest(BookingObject bookingObjectRequest, OpelBookingResponse opelBookingResponse)
            throws ParseException {
    	ITdlRedemTransactionRequestDto tdlRedemReq = new ITdlRedemTransactionRequestDto();
    	
    	tdlRedemReq.setCustomerHash(bookingObjectRequest.getLoggedInUser().getCustomerHash());
    	tdlRedemReq.setExternalReferenceNumber(opelBookingResponse.getTxn_uuid());
    	tdlRedemReq.setPointsRedeemed(String.valueOf(bookingObjectRequest.getPayments().getTicpoints().getNoTicPoints()));
    	tdlRedemReq.setTransactionNumber(bookingObjectRequest.getItineraryNumber());
    	tdlRedemReq.setNotes("Points + Pay");
    	
        return tdlRedemReq;
    }
    
    private boolean isRedemptionBooking(BookingObject bookingObjectRequest) {
    	boolean isRedemption  = false;
    	if(bookingObjectRequest != null && bookingObjectRequest.getPayments() != null &&
    			bookingObjectRequest.getPayments().getTicpoints() != null && 
    			bookingObjectRequest.getPayments().getTicpoints().getNoTicPoints() > 0 && 
    			bookingObjectRequest.getGuest() != null && !StringUtils.isEmpty(bookingObjectRequest.getGuest().getMembershipId())) {
    		isRedemption = true;
    	}
    	return isRedemption;
    }
    
    private BookingObject confirmRedemptionBooking(BookingObject bookingObjectRequest, Payments guestPayment,
            			ObjectMapper objectMapper, ITdlRedemTransactionRequestDto redemptionTransactionsRequest) throws Exception {
        String methodName = "confirmRedemptionBooking";
        LOG.trace("Method Entry: {}", methodName);
        LOG.debug("--> TIC TD CALL :INSIDE THE FLOW  <-- ");
        BookingObject bookingObjectResponse;
        String specialRequestsTic = buildTicSpecialRequest(guestPayment, redemptionTransactionsRequest);

        LOG.debug("--> TIC TD CALL INTIATED  <--");
        LOG.debug("--> TIC TD CALL ALL REQUEST  <-- {}", redemptionTransactionsRequest);

//        if (validateRedemptionRequest(bookingObjectRequest, redemptionTransactionsRequest)) {
        	
        	String authToken = bookingObjectRequest.getLoggedInUser().getAuthToken();
        	
        	IRedemptionTransactionsResponse redemptionTransactionsResponse = redemptionTransactionsService
        				.redeemTransactionTdl(redemptionTransactionsRequest, authToken, null);
             
            LOG.debug("--> TIC TD CALL ALL RESPONSE  <-- {}", redemptionTransactionsResponse);
            String redemptionTransactionsResponseString = redemptionTransactionsResponse.getResponse();
            LOG.debug("--> TIC TD CALL Success RESPONSE  <-- {}", redemptionTransactionsResponseString);
            if (null != redemptionTransactionsResponse && null != redemptionTransactionsResponseString
                    && redemptionTransactionsResponseString.equalsIgnoreCase("Success")) {

                LOG.debug("--> TIC TD CALL Success  <--");

                setSpecialRequest(bookingObjectRequest, specialRequestsTic, redemptionTransactionsResponse);

                String bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectRequest);
                LOG.info("Final Booking Commit Request String : {}", bookingObjectLoggerString);
                LOG.debug("-----Passing on bookingDetails object to the booking handler-----");

                bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectRequest);

                bookingObjectLoggerString = objectMapper.writeValueAsString(bookingObjectResponse);
                LOG.info("Received Booking Commit Response String : {}", bookingObjectLoggerString);


                //setReservationNumberToRedemption(bookingObjectResponse, redemptionTransactionsRequest);
            } else {
                bookingObjectResponse = setPointRedemptionFailed(bookingObjectRequest,
                        redemptionTransactionsResponse.getErrorMsg());
            }
//        } else {
//            bookingObjectResponse = setPointRedemptionFailed(bookingObjectRequest,
//                    "Validation failed for number of points required to book the selected room.");
//        }

        LOG.trace("Method Exit: {}", methodName);
        return bookingObjectResponse;
    }
    
//    private void fetchHotelDetails(BookingObject bookingObjectRequest, SlingHttpServletRequest request,
//            IRedemptionTransactionsRequest redemptionTransactionsRequest) throws RepositoryException {
//
//        String hotelId = bookingObjectRequest.getHotelId();
//        LOG.debug("checking with hotel code inside tajhotels: hotelcode:{}", hotelId);
//        HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();
//
//        Hotel hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/tajhotels/en-in/our-hotels", "");
//        if (hotel.getPropertyCode() == null || hotel.getHotelName() == null) {// checking under seleqtions
//            LOG.debug("checking with hotel code inside seleqtions");
//            hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/seleqtions/en-in/our-hotels", "");
//        }
//        if (hotel.getPropertyCode() == null || hotel.getHotelName() == null) {// checking under vivanta
//            LOG.debug("checking with hotel code inside vivanta");
//            hotel = fetchDetails.getHotelDetails(request, hotelId, "/content/vivanta/en-in/our-hotels", "");
//        }
//        LOG.debug("HotelName: {} PropertyCode: {}", hotel.getHotelName(), hotel.getPropertyCode());
//
//        if (null != hotel.propertyCode) {
//            redemptionTransactionsRequest.setPropertyCode(hotel.propertyCode);
//        }
//    }
    
    private String buildTicSpecialRequest(Payments guestPayment,
    		ITdlRedemTransactionRequestDto redemReq) {
        String specialRequestTic = "";
        TicPoints ticpoints = guestPayment.getTicpoints();
        String pointsType = ticpoints.getPointsType();
        if (pointsType.equals(TIC)) {
            specialRequestTic = ticpoints.getNoTicPoints() + " " + TIC;
        } else if (pointsType.equals(EPICURE)) {
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + EPICURE;
        } else if (pointsType.equals(TAP)) {
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + TAP;
        } else if (pointsType.equals(TAPP_ME)) {
            specialRequestTic = ticpoints.getNoEpicurePoints() + " " + "TAPP Me";
        }
        LOG.debug("--> TIC TD CALL specialRequestTic  <-- {}", specialRequestTic);
        return specialRequestTic;
    }
    
    private boolean validateRedemptionRequest(BookingObject bookingObjectRequest,
            IRedemptionTransactionsRequest redemptionRequest) {
        String methodName = "validateRedemptionRequest";
        LOG.trace("Method Entry: {}", methodName);
        boolean isValidBooking = false;
        RoomsAvailabilityRequest availabilityRequest = buildRoomAvailabilityRequestFrom(bookingObjectRequest);
        List<HotelAvailabilityResponse> availabilityResponse = roomAvailabilityService
                .getAvailabilityWithDifferentRooms(availabilityRequest);

        int pointsRequired = getPointsRequiredForBooking(availabilityResponse, bookingObjectRequest);
        int redemptionRequestPoints = Integer.parseInt(redemptionRequest.getRedeemPoints());
        LOG.info("Points required for booking: {} and points mentioned in redemption request: {}", pointsRequired,
                redemptionRequestPoints);
        if (redemptionRequestPoints < pointsRequired) {
            int pointsDifference = redemptionRequestPoints - pointsRequired;
            isValidBooking = validateCashPayment(bookingObjectRequest, pointsDifference);
        } else {
            isValidBooking = true;
        }
        LOG.trace("Method Exit: {}", methodName);
        return isValidBooking;
    }
    
    private void setSpecialRequest(BookingObject bookingObjectRequest, String specialRequestsTic,
            IRedemptionTransactionsResponse redemptionTransactionsResponse) {
        String specialRequests = "TD TIC Transcation ID:" + redemptionTransactionsResponse.getTransactionId()
                + ", Points Redeemed : " + specialRequestsTic + ", "
                + bookingObjectRequest.getGuest().getSpecialRequests();
        bookingObjectRequest.getGuest().setSpecialRequests(specialRequests);
        LOG.debug("TIC TD CALL Special Request to Synxis  : {}", specialRequests);
    }
    
//    private void setReservationNumberToRedemption(BookingObject bookingObjectResponse,
//            IRedemptionTransactionsRequest redemptionTransactionsRequest) {
//        StringBuilder reservationNumber = new StringBuilder();
//        if (bookingObjectResponse.getRoomList().size() == 1) {
//            reservationNumber = new StringBuilder(bookingObjectResponse.getRoomList().get(0).getReservationNumber());
//        } else {
//            for (Room room : bookingObjectResponse.getRoomList()) {
//
//                reservationNumber.append(room.getReservationNumber());
//                reservationNumber.append("-");
//            }
//
//            reservationNumber = reservationNumber.deleteCharAt(reservationNumber.length() - 1);
//        }
//        LOG.debug("print the value of reservationNumber:: {}", reservationNumber);
//        redemptionTransactionsRequest.setRegisterNumber(reservationNumber.toString());
//    }
    
    private BookingObject setPointRedemptionFailed(BookingObject bookingObjectRequest, String errorMessage) {
        LOG.debug("--> TIC TD CALL FAILED  <--");
        BookingObject bookingObjectResponse = new BookingObject();
        sendIgnoreRequest(bookingObjectRequest);
        // Failure Scenario of TIC FLOW
        if (null != errorMessage) {
            bookingObjectResponse.setSuccess(false);
            TicPoints ticPoints = new TicPoints();
            ticPoints.setRedeemTicOrEpicurePoints(true);
            Payments payments = new Payments();
            payments.setTicpoints(ticPoints);
            bookingObjectResponse.setPayments(payments);
            bookingObjectResponse.setErrorMessage(errorMessage);
        }
        return bookingObjectResponse;
    }
    private RoomsAvailabilityRequest buildRoomAvailabilityRequestFrom(BookingObject bookingObjectRequest) {
        String methodName = "buildRoomAvailabilityRequestFrom";
        LOG.trace("Method Entry: {}", methodName);

        RoomsAvailabilityRequest roomsAvailabilityRequest = new RoomsAvailabilityRequest();

        String checkInDate = bookingObjectRequest.getCheckInDate();
        roomsAvailabilityRequest.setCheckInDate(checkInDate);

        String checkOutDate = bookingObjectRequest.getCheckOutDate();
        roomsAvailabilityRequest.setCheckOutDate(checkOutDate);

        String hotelId = bookingObjectRequest.getHotelId();
        List<String> hotelIds = new ArrayList<>();
        hotelIds.add(hotelId);
        roomsAvailabilityRequest.setHotelIds(hotelIds);

        List<RoomOccupants> occupants = new ArrayList<>();

        List<Room> roomList = bookingObjectRequest.getRoomList();

        List<String> ratePlanCodes = new ArrayList<>();
        for (int i = 0; i < roomList.size(); i++) {
            Room room = roomList.get(i);

            RoomOccupants occupant = getRoomOccupantsFrom(room);
            occupants.add(occupant);

            addRatePlanCode(ratePlanCodes, room);
        }

        roomsAvailabilityRequest.setRoomOccupants(occupants);

        roomsAvailabilityRequest.setRatePlanCodes(ratePlanCodes);

        LOG.debug("Constructed a roomAvailabilityRequest as: {}", roomsAvailabilityRequest);
        LOG.trace("Method Exit: {}", methodName);
        return roomsAvailabilityRequest;

    }
       private int getPointsRequiredForBooking(List<HotelAvailabilityResponse> availabilityResponse,
            BookingObject bookingObjectRequest) {
        String methodName = "getPointsRequiredForBooking";
        LOG.trace("Method Entry: {}", methodName);
        int pointsRequired = 0;

        List<Room> roomList = bookingObjectRequest.getRoomList();
        int size = roomList.size();

        for (int i = 0; i < size; i++) {
            Room room = roomList.get(i);
            String roomTypeCode = room.getRoomTypeCode();
            pointsRequired += getPointsRequiredForRoom(bookingObjectRequest.getHotelId(), roomTypeCode,
                    availabilityResponse);
        }
        LOG.info("Points required for booking: {} is {}", bookingObjectRequest, pointsRequired);
        LOG.trace("Method Exit: {}", methodName);
        return pointsRequired;
    }

      private boolean validateCashPayment(BookingObject bookingObjectRequest, int pointsDifference) {
        Payments payments = bookingObjectRequest.getPayments();
        String strAmount = payments.getAmount();
        int amount = getIntegerOf(strAmount);
        LOG.info("An expected amount of {} was to be paid online and found that an amount of {} was paid.",
                pointsDifference, amount);
        return Math.abs(amount - pointsDifference) < 5;
    }
      private boolean sendIgnoreRequest(BookingObject bookingObjectReqest) {
          LOG.info("Inside sendIgnoreRequest method");

          boolean isSuccess = true;
          BookingObject bookingObjectResponse = new BookingObject();

          setIgnoreTo(bookingObjectReqest);

          LOG.info("-----Passing on ignore request to the booking handler-----");
          try {
              bookingObjectResponse = bookingHandler.createRoomReservation(bookingObjectReqest);
          } catch (Exception e) {
              LOG.error("Ignore exception: {}", e.getMessage());
              LOG.error("Ignore exception: {}", e);
              isSuccess = false;
          }

          LOG.info("-----Received Response from the booking handler-----");

          if (bookingObjectResponse.isSuccess()) {
              LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_SUCCESSFUL);
              bookingObjectResponse.setResStatus(ResStatus.Ignore);
          } else {
              isSuccess = false;
              LOG.debug("Error while booking Room:  - {}", bookingObjectResponse.getErrorMessage());
          }
          return isSuccess;
      }
      
      private void setIgnoreTo(BookingObject bookingObjectReqest) {
          bookingObjectReqest.setResStatus(ResStatus.Ignore);
          bookingObjectReqest.getPayments().setCardNumber(null);
          bookingObjectReqest.getPayments().setCardType(null);
          bookingObjectReqest.getPayments().setExpiryMonth(null);
          bookingObjectReqest.getPayments().setNameOnCard(null);
          bookingObjectReqest.getPayments().setTicpoints(null);
          bookingObjectReqest.setGuest(null);
          bookingObjectReqest.setFlight(null);
      }
      
      private String setDefaultWebsiteId(String websiteId) {
          if (StringUtils.isBlank(websiteId)) {
              websiteId = "www.tajhotels.com";
          }
          return websiteId;
      }
      
      private void handleReservationSuccess(SlingHttpServletResponse response, String returnMessage, String redirectUrl,
              PrintWriter responseWriter, BookingObject bookingObjectResponse) {
          String confirmationStatus;
          if (bookingObjectResponse.getPayments().isPayOnlineNow()) {
              LOG.info("Processing redirect response for Online payment");
              LOG.info("All Rooms are available - Pay online selected");
              confirmationStatus = BookingConstants.CONFIRM_BOOKING_ALL_BOOKED;
              writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
          } 
      }
      
      
      private void writeResponseOnServer(String bookingObjectResponseStringa, SlingHttpServletResponse response,
              String confirmationStatus, String redirectUrl) {
          try {
              PrintWriter out = response.getWriter();
              LOG.info("redirecting using hidden form");
              LOG.info("Final BookingObject String : {}", bookingObjectResponseStringa);
              LOG.info("RedirectURL : {}", redirectUrl);
              response.setContentType(BookingConstants.CONTENT_TYPE_HTML);
              bookingObjectResponseStringa = StringEscapeUtils.escapeEcmaScript(bookingObjectResponseStringa);
              LOG.debug("Final response which is setting as SessionData : {}", bookingObjectResponseStringa);

              out.print("<form id='confirmbooking' action='" + redirectUrl + BookingConstants.PAGE_EXTENSION_HTML
                      + "' method='get'>");
              out.print("<input type='hidden' name='" + BookingConstants.CONFIRMATION_STATUS_NAME + "' value='"
                      + confirmationStatus + "'>");

              out.print("</form>");
              out.println("<script type=\"text/javascript\">" + "window.onload = function(){"
                      + "sessionStorage.setItem('bookingDetailsRequest', '" + bookingObjectResponseStringa + "');"
                      + "sessionStorage.setItem('status', '" + confirmationStatus + "');"
                      + "sessionStorage.setItem('paymentType', 'payOnline');"
                      + "document.getElementById(\"confirmbooking\").submit()" + "}" + "</script>");
              LOG.info("end of output stream");
              out.close();

          } catch (Exception e) {
              LOG.error("Error while submitting form : {}", e.getMessage());
              LOG.debug("error while submitting form : {}", e);
          }

      }
      
      private void handleBookingFailed(SlingHttpServletResponse response, ObjectMapper objectMapper,
              BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter,
              BookingObject bookingObjectResponse) throws IOException {
          String returnMessage;
          String confirmationStatus;
          LOG.info("BookingObject.Success:false or All rooms are unavailable");
          bookingObjectRequest.setSuccess(false);
          // [TIC-FLOW]
          if (null != bookingObjectResponse.getPayments() && null != bookingObjectResponse.getPayments().getTicpoints()
                  && bookingObjectResponse.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()) {
              bookingObjectRequest.setErrorMessage(bookingObjectResponse.getErrorMessage());
          }
          returnMessage = objectMapper.writeValueAsString(bookingObjectRequest);
          ignoreBooking(bookingObjectRequest);
          confirmationStatus = BookingConstants.CONFIRM_BOOKING_FAILED;
          bookingObjectRequest.setResStatus(ResStatus.Ignore);
          if (bookingObjectRequest.getPayments().isPayOnlineNow()) {
              writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
          } else if (bookingObjectRequest.getPayments().isPayAtHotel()) {
              response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
              response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
              responseWriter.write(returnMessage);
              responseWriter.close();
          }
      }
      private void ignoreBooking(BookingObject bookingObjectResponse) {
          boolean ignoreStatus = sendIgnoreRequest(bookingObjectResponse);
          if (ignoreStatus) {
              LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_SUCCESSFUL);
          } else {
              LOG.info(BookingConstants.BOOKING_IGNORE_REQUEST_FAILED);
          }
      }
    
      
      private void handleFailure(SlingHttpServletResponse response, TajHotelBrandsETCConfigurations etcWebsiteConfig,
              BookingObject bookingObjectRequest, String redirectUrl, PrintWriter responseWriter) {
          String confirmationStatus = null;
          String returnMessage = null;
          if (bookingObjectRequest.getPayments() != null && bookingObjectRequest.getPayments().isPayOnlineNow()) {
              if (!bookingObjectRequest.isPaymentStatus()) {
                  LOG.info("Payment is canceled, Redirecting to Cancel Url : {}",
                          etcWebsiteConfig.getCcAvenueCancelUrl());
                  confirmationStatus = BookingConstants.TRANSACTION_CANCELED;
                  redirectUrl = etcWebsiteConfig.getCcAvenueCancelUrl();
              }
              writeResponseOnServer(returnMessage, response, confirmationStatus, redirectUrl);
          } else if (bookingObjectRequest.getPayments() != null && bookingObjectRequest.getPayments().isPayAtHotel()) {

              LOG.debug("-----BookHotelConfirmServlet - Exception : Leaving doGet() method");
              response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
              response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
              responseWriter.write("ExceptionFound");
              responseWriter.close();
          }
      }
      private String identifyBookingType(String resourcePath) {
          String bookingType = "";
          if (BookingConstants.TIC_POST_PAYMENT_SERVLET_URL.equalsIgnoreCase(resourcePath)) {
              bookingType = ReservationConstants.TIC_CCAVENUE;
          } else if (BookingConstants.CORPORATE_POST_PAYMENT_SERVLET_URL.equalsIgnoreCase(resourcePath)) {
              bookingType = ReservationConstants.CORPORATE_BOOKING_CCAVENUE;
          } else {
              bookingType = ReservationConstants.BOOKING_CCAVENUE;
          }
          LOG.info("Identified the booking type as: {}", bookingType);
          return bookingType;
      }
      
      private RoomOccupants getRoomOccupantsFrom(Room room) {
          RoomOccupants occupant = new RoomOccupants();
          int noOfAdults = room.getNoOfAdults();
          occupant.setNumberOfAdults(noOfAdults);

          int noOfChilds = room.getNoOfChilds();
          occupant.setNumberOfChildren(noOfChilds);
          return occupant;
      }

      private void addRatePlanCode(List<String> ratePlanCodes, Room room) {
          String ratePlanCode = room.getRatePlanCode();
          if (!ratePlanCodes.contains(ratePlanCode)) {
              ratePlanCodes.add(ratePlanCode);
          }
      }
      private int getPointsRequiredForRoom(String hotelId, String roomTypeCode,
              List<HotelAvailabilityResponse> availabilityResponse) {
          String methodName = "getPointsRequiredForBooking";
          LOG.trace("Method Entry: {}", methodName);
          int pointsRequired = 0;
          int size = availabilityResponse.size();
          for (int i = 0; i < size; i++) {
              HotelAvailabilityResponse availability = availabilityResponse.get(i);
              Map<String, HotelDto> roomAvailability = availability.getRoomAvailabiilityForOffers();
              HotelDto hotelDto = roomAvailability.get(hotelId);
              if (hotelDto != null) {
                  Map<String, RoomAvailabilityDto> availableRooms = hotelDto.getAvailableRooms();
                  RoomAvailabilityDto roomAvailabilityDto = availableRooms.get(roomTypeCode);
                  if (roomAvailabilityDto != null) {
                      float lowestPrice = roomAvailabilityDto.getLowestPrice();
                      float lowestPriceTax = roomAvailabilityDto.getLowestPriceTax();
                      pointsRequired = (int) (lowestPrice + lowestPriceTax);
                      break;
                  }
              }
          }
          LOG.trace("Method Exit: {}", methodName);
          return pointsRequired;
      } 

      private int getIntegerOf(String strAmount) {
          int amount = -1;
          try {
              amount = Integer.parseInt(strAmount);
          } catch (NumberFormatException e) {
              LOG.error("An error occurred while parsing the amount paid.", e);
          }
          return amount;
      }
      private BookingObject setAdditionalDetailsToResponse(SlingHttpServletRequest request,
              BookingObject bookingObjectResponse, BookingObject bookingObjectRequest, ObjectConverter converter,
              String rootPath, boolean amaBooking) {


          try {
              HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();
              if (null == bookingObjectResponse.getPayments()) {
                  LOG.info("Resetting BookingObjectResponse.Payments with BookingObjectRequest.Payments.");
                  bookingObjectResponse.setPayments(bookingObjectRequest.getPayments());

                  if (bookingObjectRequest.getPayments().getTicpoints() != null
                          && bookingObjectRequest.getPayments().getTicpoints().isPointsPlusCash()) {
                      LOG.info("Setting Payments.isPayGuaranteeAmount: {}", false);
                      bookingObjectResponse.getPayments().setPayGuaranteeAmount(false);
                  } else if (bookingObjectResponse.getPayments().isPayOnlineNow() && Float.compare(
                          (Math.round(Float.parseFloat(bookingObjectResponse.getTotalPaidAmount()) * 100) / 100),
                          (Math.round(Float.parseFloat(bookingObjectResponse.getTotalAmountAfterTax()) * 100)
                                  / 100)) != 0) {
                      LOG.info("Setting Payments.isPayGuaranteeAmount: {}", true);
                      bookingObjectResponse.getPayments().setPayGuaranteeAmount(true);
                  }
              }

              LOG.debug("Status of Commited Status {} ", bookingObjectResponse.isSuccess());
              LOG.debug("Itinerary Number for commit : {}", bookingObjectResponse.getItineraryNumber());

              if (StringUtils.isNotBlank(bookingObjectResponse.getCurrencyCode())) {
                  LOG.info("Setting Currency code from bookingObject's Request -> Response ::");
                  bookingObjectResponse.setCurrencyCode(bookingObjectRequest.getCurrencyCode());
              }
              bookingObjectResponse = setFlightDetailsToSpecialRequests(bookingObjectResponse, converter);
              String roomTypeCode = "";
              if (amaBooking) {
                  roomTypeCode = bookingObjectResponse.getRoomList().get(0).getRoomTypeCode();
                  LOG.info("roomTypeCode: {}", roomTypeCode);
              }
              fetchHotelDetailsResponse(request, bookingObjectResponse, rootPath, fetchDetails, roomTypeCode);

              ResourceResolver resolver = request.getResourceResolver();
              setRoomDetailsToResponse(bookingObjectResponse, fetchDetails, resolver);


          } catch (Exception e) {
              LOG.error("Exception found while trying to get the bedType and roomName:: {}", e.getMessage());
              LOG.debug("Exception found while trying to get the bedType and roomName:: {}", e);
          }

          return bookingObjectResponse;
      }

      private BookingObject setFlightDetailsToSpecialRequests(BookingObject bookingObjectResponse,
              ObjectConverter converter) {

          Guest responseGuestObject = bookingObjectResponse.getGuest();
          LOG.info("Checking if flight details are present :");
          try {
              if (responseGuestObject != null && responseGuestObject.getSpecialRequests() != null
                      && responseGuestObject.getSpecialRequests().contains("Arrival Flight:")) {
                  Flight flightDetails;
                  LOG.debug("Found flight details in response. Setting Flight details object.");
                  responseGuestObject.setAirportPickup(true);

                  flightDetails = converter.getFlightDetails(responseGuestObject.getSpecialRequests());

                  bookingObjectResponse.setFlight(flightDetails);
                  LOG.info("Removing flight string from Special requests");
                  String specialRequests = responseGuestObject.getSpecialRequests();
                  String newRequests = getFlightDetails(specialRequests);
                  if (newRequests.equals("")) {
                      newRequests = newRequests.substring(0, newRequests.length() - 1);
                      responseGuestObject.setSpecialRequests(newRequests);
                  }
                  LOG.info("New Special requests string : {}", responseGuestObject.getSpecialRequests());
              }

              splitSpecialRequest(responseGuestObject);
          } catch (IOException e) {
              LOG.error("IOException found in setFlightDetailsToSpecialRequests :: {}", e.getMessage());
              LOG.debug("IOException found in setFlightDetailsToSpecialRequests :: {}", e);
          } catch (Exception e) {
              LOG.error("Exception found in setFlightDetailsToSpecialRequests :: {}", e.getMessage());
              LOG.debug("Exception found in setFlightDetailsToSpecialRequests :: {}", e);
          }
          return bookingObjectResponse;

      }
      private void splitSpecialRequest(Guest responseGuestObject) {
          String guestGstNumber = "guestGSTNumber:";
          if (null != responseGuestObject && StringUtils.isNotBlank(responseGuestObject.getSpecialRequests())
                  && responseGuestObject.getSpecialRequests().contains(guestGstNumber)) {

              LOG.info("Special request contain GST Number : {}", responseGuestObject.getSpecialRequests());
              String special = responseGuestObject.getSpecialRequests();
              if (special.contains(",guestGSTNumber:")) {
                  special = special.split(",guestGSTNumber:")[0];
              } else if (special.contains(guestGstNumber)) {
                  special = special.split(guestGstNumber)[0];
              }
              responseGuestObject.setSpecialRequests(special);
              LOG.info("After removing GST from Special Request : {}", responseGuestObject.getSpecialRequests());
          }
      }
      private void fetchHotelDetailsResponse(SlingHttpServletRequest request, BookingObject bookingObjectResponse,
              String rootPath, HotelDetailsFetcher fetchDetails, String roomTypeCode) {
          try {
              LOG.info("getting response for hotel details. hotel id : {}", bookingObjectResponse.getHotelId());
              Hotel hoteldetails = fetchDetails.getHotelDetails(request, bookingObjectResponse.getHotelId(), rootPath,
                      roomTypeCode);
              bookingObjectResponse.setHotel(hoteldetails);
          } catch (RepositoryException e1) {
              LOG.error("error while getting hotel details : {}", e1.getMessage());
              LOG.debug("error while getting hotel details : {}", e1);
          }
      }
      private void setRoomDetailsToResponse(BookingObject bookingObjectResponse, HotelDetailsFetcher fetchDetails,
              ResourceResolver resolver) throws RepositoryException {
          Session session = resolver.adaptTo(Session.class);
          String hotelPath = bookingObjectResponse.getHotel().getHotelResourcePath();
          List<Room> rooms = bookingObjectResponse.getRoomList();
          if (rooms != null && !rooms.isEmpty() && StringUtils.isNotBlank(hotelPath)) {
              for (Room room : rooms) {
                  Iterator<Node> iteratorNode = queryBedType(fetchDetails, resolver, session, hotelPath, room);
                  if (iteratorNode != null) {
                      setBedType(room, iteratorNode);
                  }
              }
          }
      }
      private void setBedType(Room room, Iterator<Node> iteratorNode) throws RepositoryException {
          while (iteratorNode.hasNext()) {
              Node currentNode = iteratorNode.next();
              if (null != currentNode) {
                  if (currentNode.hasProperty("bedType")) {
                      room.setBedType(currentNode.getProperty("bedType").getValue().getString());
                  }
                  Node parentNode = currentNode.getParent().getParent();
                  if (null != parentNode && parentNode.hasProperty("roomTitle")) {
                      String roomTitle = parentNode.getProperty("roomTitle").getValue().getString();
                      if (StringUtils.isNotBlank(roomTitle)) {
                          room.setRoomTypeName(roomTitle);
                      }
                  }
              }
          }
      }
      
      private String getFlightDetails(String specialRequests) {
          StringTokenizer token = new StringTokenizer(specialRequests, ",");
          String newRequests = "";
          while (token.hasMoreTokens()) {
              String tokenString = token.nextToken();
              if (!tokenString.contains("Arrival Flight:")) {
                  LOG.debug("Token : {}", tokenString);
                  newRequests = newRequests + tokenString + ",";
              }
          }
          return newRequests;
      }
      private Iterator<Node> queryBedType(HotelDetailsFetcher fetchDetails, ResourceResolver resolver, Session session,
              String hotelPath, Room room) {
          Map<String, String> queryParam = new HashMap<>();
          queryParam.put("path", hotelPath);
          queryParam.put("type", ReservationConstants.NT_UNSTRUCTURED);
          String roomCode = room.getRoomTypeCode();
          queryParam.put("1_property", ReservationConstants.ROOM_CODE);
          queryParam.put("1_property.value", roomCode);
          return fetchDetails.searchRoomType(queryParam, session, resolver);
      }
}