/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.SingleSignOnConfigurationService;
import com.ihcl.tajhotels.constants.BookingConstants;

/**
 * @author Vijay Chikkani
 *
 */
@Component(service = Servlet.class,
        property = { "sling.servlet.paths=" + "/bin/singleSignOutServlet",
                "sling.servlet.methods={GET}", "sling.servlet.resourceTypes=services/powerproxy",
                "sling.servlet.selectors=groups", })
public class SingleSignOutServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 9876543210L;

    private static final Logger logger = LoggerFactory.getLogger(SingleSignOutServlet.class);

    @Reference
    private SingleSignOnConfigurationService singleSignOnConfigurationService;

    @Override
    protected void doGet(SlingHttpServletRequest slingHttpServletRequest,
            SlingHttpServletResponse slingHttpServletResponse) throws IOException {

        logger.info("Started doGet method");
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();
        try {
            String cookieName = singleSignOnConfigurationService.getSingleSignOnCookieName();
//            String cookiePath = singleSignOnConfigurationService.getSingleSignOnCookiePath();
            String cookiePath = "/";
            String cookieDomain = slingHttpServletRequest.getServerName();

            String loggerString = "Deleting cookieName: " + cookieName + ", cookieDomain: " + cookieDomain
                    + ", cookiePath: " + cookiePath;
            logger.info(loggerString);

            Cookie cookie = addCookieToServletResponse(objectMapper, cookieDomain, cookiePath, cookieName, "", 0,
                    false);
            slingHttpServletResponse.addCookie(cookie);
            objectNode.put("SUCCESS", true);

        } catch (Exception exception) {
            logger.info("Excetion occured while fetching User Details: {}", exception.getMessage());
            logger.error("Excetion occured while fetching User Details: {}", exception);
            objectNode.put("error", exception.getMessage());
            objectNode.put("SUCCESS", false);

        } finally {
            String returnMessage = objectNode.toString();
            logger.info("Final returnMessage : {}", returnMessage);
            slingHttpServletResponse.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
            slingHttpServletResponse.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
            slingHttpServletResponse.getWriter().write(returnMessage);
            logger.info("End of SingleSignInServlet.doPost().");
            slingHttpServletResponse.getWriter().close();
        }
    }

    /**
     * @param slingHttpServletRequest
     * @param slingHttpServletResponse
     * @param objectMapper
     * @param ecodedCookieValue
     * @param isSecureCookie
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonGenerationException
     */
    private Cookie addCookieToServletResponse(ObjectMapper objectMapper, String cookieDomain, String cookiePath,
            String cookieName, String ecodedCookieValue, int cookieMaxAge, boolean isSecureCookie) throws IOException {

        String logString = "Cookie raw details:- CookieName: " + cookieName + ", ecodedCookieValue: "
                + ecodedCookieValue + ", MaxAge: " + cookieMaxAge;
        logger.info(logString);

        Cookie cookie = new Cookie(cookieName, ecodedCookieValue);
        cookie.setSecure(isSecureCookie);
        cookie.setDomain(cookieDomain);
        cookie.setPath(cookiePath);
        cookie.setMaxAge(cookieMaxAge);

        String cookieString = objectMapper.writeValueAsString(cookie);
        logger.info("Final cookieString: {}", cookieString);
        return cookie;
    }

}
