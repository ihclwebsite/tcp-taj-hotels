package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.FlushCacheDispatcherConfigurationService;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/refreshRateCache", "sling.servlet.methods=GET" })
public class PurgeRateCacheServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 7011999851200697629L;

    private static final Logger log = LoggerFactory.getLogger(PurgeRateCacheServlet.class);

    private static final String HOTEL_ID = "hotelId";

    @Reference
    private FlushCacheDispatcherConfigurationService flushCacheConfig;

    private static final String DEFAULT_FLUSH_CACHE_URL = "https://author-taj-prod63.adobecqms.net/bin/flushcache";


    @Override
    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {

        log.info("Method Entry doGet of PurgeRateCacheServlet");
        try {
            response.setContentType("text/html");

            String FLUSH_RATE_CACHE_URL = DEFAULT_FLUSH_CACHE_URL;
            if (null != flushCacheConfig) {
                FLUSH_RATE_CACHE_URL = flushCacheConfig.getRateRefreshServletUrl();
            }

            String hotelId = request.getParameter(HOTEL_ID);
            log.trace("HotelId from request paramter " + hotelId);

            String finalURL = FLUSH_RATE_CACHE_URL + "?" + HOTEL_ID + "=" + hotelId;
            log.info("Final URL for rate flush " + finalURL);

            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet get = new HttpGet(finalURL);
            CloseableHttpResponse httpResponse = client.execute(get);

            String stringResponse = EntityUtils.toString(httpResponse.getEntity());
            log.info("Obtained Response as " + stringResponse);
            httpResponse.close();

            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().write(stringResponse);
        } catch (Exception e) {
            log.error("RateRefresh Failed , Exception Caught: " + e.getMessage());
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }

}
