/**
 * 
 */
package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.HotelAvailabilityResponse;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import com.ihcl.tajhotels.constants.HttpRequestParams;
import com.ihcl.tajhotels.signin.dto.ErrorResponse;

/**
 * @author TCS
 *
 */
@Component(service = Servlet.class,
immediate = true,
property = { "sling.servlet.paths=" + "/bin/hotel-rates/rooms-availability",
			 "sling.servlet.methods={POST}",
			 "sling.servlet.resourceTypes=services/powerproxy"})
public class HotelRoomsAvailabiltyServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1704348532999251380L;

	private static final Logger LOG = LoggerFactory.getLogger(HotelRoomsAvailabiltyServlet.class);
	
    @Reference
    private IRoomAvailabilityService roomAvailabilityService;

    @Reference
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;
    
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        String methodName = "HotelRoomsAvailabiltyServlet doPost";
        LOG.trace("Method Entry: {}", methodName);        
    	PrintWriter responseWriter = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        
        try {
        	
            List<HotelAvailabilityResponse> hotelAvailabilityResp = fetchHotelRooms(request, response);
        	
        	responseWriter.write(mapper.writeValueAsString(hotelAvailabilityResp));
            
        } catch (Exception e) {
            LOG.debug("Exception at doPost of HotelRoomsAvailabiltyServlet :: {} ", e.getMessage());
            LOG.error("Exception at doPost of HotelRoomsAvailabiltyServlet", e);
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setError(e.getMessage());
            response.setStatus(500);
            responseWriter.write(mapper.writeValueAsString(errorResponse));
        }
        LOG.trace("Method Exit: {}", methodName);
    }
    
    private List<HotelAvailabilityResponse> fetchHotelRooms(SlingHttpServletRequest request, SlingHttpServletResponse response) 
    		throws JSONException, JsonGenerationException, JsonMappingException, IOException {
    	
        List<RoomsAvailabilityRequest> roomsAvailabilityRequest = new ArrayList<>();
    	ObjectMapper mapper = new ObjectMapper();
    	
    	ArrayNode hotelList = mapper.createArrayNode();
        
    	String hotelStringList = request.getParameter("hotelList");
    	String ratePlanString = request.getParameter(HttpRequestParams.RATE_CODES);
    	String roomOccupancy = request.getParameter(HttpRequestParams.OCCUPANCY);
    	
    	try {
    		hotelList =  mapper.readValue(hotelStringList,ArrayNode.class);
		} catch (IOException e) {
			LOG.error("Exception in parsing fetch hotel rooms list", e);
			e.printStackTrace();
		}
    	
    	createAvailabilityReq(roomsAvailabilityRequest, hotelList, ratePlanString, roomOccupancy);
    	
    	String requestStr = mapper.writeValueAsString(roomsAvailabilityRequest);
    	LOG.debug("requestStr "+ requestStr);
    	
    	
    	List<HotelAvailabilityResponse> hotelAvailabilityResp =  fetchAvailabilityResponse(roomsAvailabilityRequest);
    	
    	return hotelAvailabilityResp;

    }
    
    
    private void createAvailabilityReq(List<RoomsAvailabilityRequest> roomsAvailabilityRequest, ArrayNode hotelList,
    									String ratePlanString, String roomOccupancy) throws JSONException {
    	
    	if(hotelList != null && hotelList.size() > 0) {
    		int listSize = hotelList.size();
    		for(int i = 0; i < listSize; i++) {
    			
    			RoomsAvailabilityRequest roomRequest = new RoomsAvailabilityRequest();
    			List<String> hotelIdsList = new ArrayList<>();
    			List<String> ratePlanCodes = new ArrayList<>();
    			List<RoomOccupants> roomOccupants = new ArrayList<>();
    			RoomOccupants roomOccupant = new RoomOccupants();
    			
    			String hotelId = hotelList.get(i).get(HttpRequestParams.HOTEL_IDS).toString()
    												.replaceAll("&quot;", "").replaceAll("\"", "");
    			String checkInDate = hotelList.get(i).get(HttpRequestParams.CHECK_IN_DATE).toString()
    												.replaceAll("&quot;", "").replaceAll("\"", "");
    			String checkOutDate = hotelList.get(i).get(HttpRequestParams.CHECK_OUT_DATE).toString()
    												.replaceAll("&quot;", "").replaceAll("\"", "");
    			String[] peopleCountList = roomOccupancy.split(",");
    			
    			hotelIdsList.add(hotelId);
    			ratePlanCodes.add(ratePlanString);
    			roomOccupant.setNumberOfAdults(Integer.parseInt(peopleCountList[0]));
    			roomOccupant.setNumberOfChildren(Integer.parseInt(peopleCountList[1]));
    			roomOccupants.add(roomOccupant);
    			
    			roomRequest.setHotelIds(hotelIdsList);
    			roomRequest.setCheckInDate(checkInDate);
    			roomRequest.setCheckOutDate(checkOutDate);
    			roomRequest.setRatePlanCodes(ratePlanCodes);
    			roomRequest.setRoomOccupants(roomOccupants);
    			
    			roomsAvailabilityRequest.add(roomRequest);
    		}
    		
    	}
    }
    
    private List<HotelAvailabilityResponse> fetchAvailabilityResponse(List<RoomsAvailabilityRequest> roomsAvailabilityRequest) throws JsonGenerationException, JsonMappingException, IOException {
    	
    	List<HotelAvailabilityResponse> availabilityResp = roomAvailabilityService.getHotelsRoomList(roomsAvailabilityRequest);
    	
    	List<HotelAvailabilityResponse> filteredResp = new ArrayList<>();
    	
    	if(availabilityResp != null) {
	    	for(HotelAvailabilityResponse availability : availabilityResp) {
	    		if(availability.isSuccess()) {
	    			filteredResp.add(availability);
	    		}
	    	}
    	}
    	return availabilityResp;
    	
    }
}
