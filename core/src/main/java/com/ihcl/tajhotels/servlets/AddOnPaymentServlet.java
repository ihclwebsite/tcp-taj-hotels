package com.ihcl.tajhotels.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.ihcl.core.servicehandler.processpayment.OpelGenerateSignatureImpl;
import com.ihcl.core.servicehandler.processpayment.PaymentHandler;
import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.tajhotels.constants.BookingConstants;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "= Servlet to initiate payment for add-ons like epicure",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST, "sling.servlet.paths=" + "/bin/addon/pay" })
public class AddOnPaymentServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AddOnPaymentServlet.class);
    private static final long serialVersionUID = 4404889252577037993L;

    @Reference
    private ConfigurationService conf;

    private PaymentHandler paymentHandler = new PaymentHandler();

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        String paymentDetails = buildPaymentDetails(request);
        LOG.info("Payment details "+paymentDetails);
        ObjectMapper mapperObj = new ObjectMapper();
        String epicureJuspayPaymentDetails ="";
        response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
        LOG.info("Returning response to page");
        response.getWriter().write(paymentDetails);
        LOG.info("Returning response to page");
        response.getWriter().close();
     /*  String paymentParameters = paymentHandler.parsePaymentParameters(paymentDetails);
       
        String purchaseCCAvenueWorkingKey = conf.getPurchaseCCAvenueWorkingKey();
        if (purchaseCCAvenueWorkingKey.equals("")) {
            purchaseCCAvenueWorkingKey = conf.getCCAvenueWorkingKey();
        }
        String ccAvenueAccesskey = conf.getPurchaseCCAvenueAccessKey();
        if (ccAvenueAccesskey.equals("")) {
            ccAvenueAccesskey = conf.getCCAvenueAccessKey();
        }
        LOG.info("print the merchant id::" + conf.getPurchaseMerchantId() + ":access key:" + ccAvenueAccesskey
                + "working key::" + purchaseCCAvenueWorkingKey);
        String encodedString = paymentHandler.encodePaymentRequest(paymentParameters, purchaseCCAvenueWorkingKey);
        String paymentGatewayResp = paymentHandler.createPaymentForm(conf.getCCAvenueURL(), encodedString,
                ccAvenueAccesskey);
        response.setContentType(BookingConstants.CONTENT_TYPE_TEXT);
        response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
        LOG.info("Returning response to page");
        response.getWriter().write(paymentGatewayResp);*/

    }

    private String buildPaymentDetails(SlingHttpServletRequest request) {
    	LOG.info("Inside buildpaymanet details method");
        Map<String, String> juspayPaymentDetails = new HashMap<>();
        ObjectMapper mapperObj = new ObjectMapper();
        String epicureJuspayPayloadString ="";
        String signature = "";
        OpelGenerateSignatureImpl opelsignedpayload = new OpelGenerateSignatureImpl();
        Date date1 = new Date();
   	    long timestamp = date1.getTime() / 1000L; 	
    	String timestampString = Long.toString(timestamp);
   	    Random rnum = new Random();
    	int randomNumber = rnum.nextInt(99);
    	int orderId = rnum.nextInt(999999999);
   	    String randomNo = Integer.toString(randomNumber);
   	    String uniqueID = randomNo+timestampString;
   	    String membershipId = request.getParameter("membershipId");
   	    String orderIdString = Integer.toString(orderId);
   	    juspayPaymentDetails.put(BookingConstants.OPEL_ORDER_ID, orderIdString);
        juspayPaymentDetails.put(BookingConstants.OPEL_MERCHANT_ID, BookingConstants.OPEL_EPICURE_MERCHANT_ID);
        juspayPaymentDetails.put(BookingConstants.OPEL_CUSTOMERID, uniqueID);
        juspayPaymentDetails.put(BookingConstants.OPEL_AMOUNT, "1");
        juspayPaymentDetails.put(BookingConstants.OPEL_RETUEN_URL, "http://author-taj-stage63.adobecqms.net/bin/addon/enroll");
        juspayPaymentDetails.put(BookingConstants.OPEL_TIMESTAMP, timestampString);
        juspayPaymentDetails.put(BookingConstants.OPEL_CUSTOMER_PHONE_NO, request.getParameter("mobile"));
        juspayPaymentDetails.put(BookingConstants.OPEL_CUSTOMER_EMAIL, request.getParameter("email"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF1, request.getParameter("address1"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF2, request.getParameter("address2"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF3, request.getParameter("country"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF4, request.getParameter("city"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF5, request.getParameter("state"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF6, request.getParameter("pincode"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF7, request.getParameter("membershipId"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF8, request.getParameter("addonActivity"));
        juspayPaymentDetails.put(BookingConstants.OPEL_UDF9, request.getParameter("addonName"));
        juspayPaymentDetails.put(BookingConstants.OPEL_MERCHANT_PARAM1, request.getParameter("membershipId"));
        juspayPaymentDetails.put(BookingConstants.OPEL_MERCHANT_PARAM2, request.getParameter("addonName"));
        juspayPaymentDetails.put(BookingConstants.OPEL_MERCHANT_PARAM3, request.getParameter("addonActivity"));
        juspayPaymentDetails.put(BookingConstants.OPEL_MERCHANT_PARAM4, checkOptionalParam(request.getParameter("renewCardNumber")));
        
      /*  String merchantId = conf.getPurchaseMerchantId();    //commenting it for opel epicure integration
        if (merchantId.equals("")) {
            merchantId = conf.getOnlineMerchantId();
        }
        
        paymentDetails.put(BookingConstants.CCAVENUE_MERCHANT_ID_NAME, merchantId);
        paymentDetails.put(BookingConstants.CCAVENUE_ORDER_ID_NAME, "Ep-" + request.getParameter("membershipId"));
        paymentDetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, "INR");
        paymentDetails.put(BookingConstants.CCAVENUE_AMOUNT_NAME, conf.getepicureEnrollAmount());
        paymentDetails.put(BookingConstants.CCAVENUE_REDIRECT_URL_NAME, conf.getEpicurePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_CANCEL_NAME, conf.getEpicurePaymentRedirectUrl());
        paymentDetails.put(BookingConstants.CCAVENUE_LANGUAGE_NAME, BookingConstants.CCAVENUE_PARAM_LANGUAGE);
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_NAME,
                combineMultipleParam(request.getParameter("firstName"), request.getParameter("lastName")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME,
                combineMultipleParam(request.getParameter("address1"), request.getParameter("address2")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME,
                checkOptionalParam(request.getParameter("city")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME,
                checkOptionalParam(request.getParameter("state")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME,
                checkOptionalParam(request.getParameter("pincode")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME,
                checkOptionalParam(request.getParameter("country")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME,
                checkOptionalParam(request.getParameter("mobile")));
        paymentDetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_PROMO_CODE_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_TID_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, request.getParameter("membershipId"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, request.getParameter("addonName"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, request.getParameter("addonActivity"));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH4_NAME,
                checkOptionalParam(request.getParameter("renewCardNumber")));
        paymentDetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);*/
        try {
        	
			 epicureJuspayPayloadString = mapperObj.writeValueAsString(juspayPaymentDetails);
			 LOG.info("Ccavuene req object "+epicureJuspayPayloadString);
			 LOG.info("calling generatesignature method");
				signature = opelsignedpayload.generateSignatureForPayload(epicureJuspayPayloadString, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.info("inside catch block");
			e.printStackTrace();
		}
        LOG.info("Epicure Payment req object "+signature);
        return signature;
    }

    private String checkOptionalParam(String param) {
        if (!Strings.isNullOrEmpty(param)) {
            return param;
        }
        return BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING;
    }

    private String combineMultipleParam(String param1, String param2) {
        if (!Strings.isNullOrEmpty(param1) && !Strings.isNullOrEmpty(param2)) {
            return param1 + " " + param2;
        }
        return BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING;
    }
}
