/**
 * 
 */
package com.ihcl.tajhotels.servlets;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.ihcl.core.services.NodeOperationsService;
import com.ihcl.core.services.search.OffersSearchService;
import com.ihcl.tajhotels.constants.CrxConstants;

/**
 * <pre>
 * UpdateGlobalOfferServlet Class
 * </pre>.
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see 
 * @since :21-May-2019
 * @ClassName : UpdateGlobalOfferServlet.java
 * @Description : com.ihcl.tajhotels.servlets -UpdateGlobalOfferServlet.java
 * @Modification Information
 * 
 *               <pre>
 * 
 *     since            author               description
 *  ===========     ==============   =========================
 *  21-May-2019     Neha Priyanka            Create
 * 
 *               </pre>
 */

@Component(service = Servlet.class,
immediate = true,
property = { "sling.servlet.paths=" + "/bin/updateGlobalOffers", "sling.servlet.methods=GET" })

public class UpdateGlobalOfferServlet extends SlingAllMethodsServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6905347958642442994L;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(UpdateGlobalOfferServlet.class);
	
	@Reference
	OffersSearchService offersSearchService;
	@Reference
	NodeOperationsService nodeOperationService;




    /*
     * (non-Javadoc)
     *
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling. api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
    	try {
    	String offerPath = request.getParameter("offerPath");
    	ResourceResolver resolver = request.getResourceResolver();
    	Resource offerResource = resolver.getResource(offerPath + "/jcr:content/root/participating_hotels");
    	List<String> hotelsPath = new ArrayList<>();
		if (null != offerResource) {
			Node offerNode = offerResource.adaptTo(Node.class);
			Value []values = null;
			try {
				values = offerNode.getProperty("pages").getValues();
				for (Value value : values) {
					hotelsPath.add(value.getString());
				}
			} catch (RepositoryException e) {
				LOG.error("Exception found in doGet :: {}", e.getMessage());
				LOG.debug("Exception found in doGet :: {}", e);
			}
		}
		createLocalOffers(offerPath, hotelsPath, resolver);
    	} catch (Exception e) {
    		LOG.error("Exception while creating the offer :: {}", e.getMessage());
    		LOG.debug("Exception while creating the offer :: {}", e);
    	}
    	
    }




	/**
	 * Creates the local offers.
	 *
	 * @param offerPath the offer path
	 * @param hotelsPath the hotels path
	 * @param resolver 
	 */
	private void createLocalOffers(String offerPath, List<String> hotelsPath, ResourceResolver resolver) {
		for (String hotelPath : hotelsPath) {
			String createdPath = createLocalOffer(offerPath, hotelPath, resolver);
			updateLocalOffer(offerPath, createdPath, resolver);
		}
	}
	
	
	
	/**
	 * @param offerPath
	 * @param createdPath
	 * @param resolver
	 */
	private void updateLocalOffer(String offerPath, String createdPath, ResourceResolver resourceResolver) {
		try {
			Resource localOffer = resourceResolver.resolve(createdPath);
			Resource globalOffer = resourceResolver.resolve(offerPath);
			copyContentResourcePropertiesFromGlobal(localOffer, globalOffer);
			replaceOfferDetails(localOffer, globalOffer);
			replaceOfferInclusions(localOffer, globalOffer);
			replaceOfferTerms(localOffer, globalOffer);
			addGlobalOfferPathProperty(localOffer, offerPath);
		} catch (Exception e) {
			LOG.error("Exception found in updateLocalOffer :: {}", e.getMessage());
			LOG.debug("Exception found in updateLocalOffer :: {}", e);
		}

	}




	/**
	 * Creates the local offer.
	 *
	 * @param globalOfferPath the global offer path
	 * @param hotelPath the hotel path
	 * @param resourceResolver 
	 * @return the string
	 */
	private String createLocalOffer(String globalOfferPath, String hotelPath, ResourceResolver resourceResolver) {
		String newLocalOfferPath = null;
		String offerTemplatePath = findLocalOfferTemplate(hotelPath);
		if (StringUtils.isNotBlank(offerTemplatePath)) {
			try {
				PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
				Page localOfferPageTemplate = pageManager.getContainingPage(offerTemplatePath);
				Page localOffer = pageManager.copy(localOfferPageTemplate,
						localOfferPageTemplate.getParent().getPath() + getOfferName(globalOfferPath), null, false,
						false, true);
				newLocalOfferPath = localOffer.getPath();
			} catch (WCMException e) {
				LOG.error(
						"Failed to create local offer page at {} for the global offer :: {}", hotelPath, globalOfferPath);
			}

		}
		return newLocalOfferPath;
	}




	/**
	 * @param globalOfferPath
	 * @return
	 */
	private String getOfferName(String globalOfferPath) {
		return globalOfferPath.substring(globalOfferPath.lastIndexOf('/'));
	}




	/**
	 * @param hotelPath
	 * @return
	 */
	private String findLocalOfferTemplate(String hotelPath) {
		return offersSearchService.getLocalOfferTemplate(hotelPath);

	}

	private void addGlobalOfferPathProperty(Resource localOffer, String globalOfferPath) {
		LOG.info("addGlobalOfferPathProperty");
		Node localOfferRoot = localOffer.adaptTo(Node.class);
		try {
			Node localJcrContent = localOfferRoot.getNode("jcr:content");
			Session session = localJcrContent.getSession();
			localJcrContent.setProperty(CrxConstants.GLOBALPATH_IN_LOCALOFFER_PROPETY_NAME, globalOfferPath);
			session.save();
		} catch (PathNotFoundException e) {
			LOG.error("Failed to set the global path in the local offer :: {}", e.getMessage());
			LOG.debug("Failed to set the global path in the local offer :: {}", e);
		} catch (RepositoryException e) {
			LOG.error("Failed to set the global path in the local offer {}", e.getMessage());
		}

	}

	private void replaceOfferDetails(Resource localOffer, Resource globalOffer) {
		LOG.info("Copy Offer Details");
		Node localOfferRoot = localOffer.adaptTo(Node.class);
		Node globalOfferRoot = globalOffer.adaptTo(Node.class);
		try {
			Node localOfferDetails = localOfferRoot.getNode("jcr:content/root/offerdetails");
			Node globalOfferDetails = globalOfferRoot.getNode("jcr:content/root/offerdetails");
			nodeOperationService.replaceNode(globalOfferDetails, localOfferDetails);

		} catch (PathNotFoundException e) {
			LOG.error("Failed to copy properties from between jcr content {} :: {}" , localOffer.getName()
					, globalOffer.getName());
		} catch (RepositoryException e) {
			LOG.error("Failed to copy properties from between jcr content {} :: {}", localOffer.getName()
					, globalOffer.getName());
		}
	}

	private void replaceOfferInclusions(Resource localOffer, Resource globalOffer) {
		LOG.info("replaceOfferInclusions");
		Node localOfferRoot = localOffer.adaptTo(Node.class);
		Node globalOfferRoot = globalOffer.adaptTo(Node.class);
		try {
			Node localOfferDetails = localOfferRoot.getNode("jcr:content/root/offerinclusion");
			Node globalOfferDetails = globalOfferRoot.getNode("jcr:content/root/offerinclusion");
			nodeOperationService.replaceNode(globalOfferDetails, localOfferDetails);
		} catch (PathNotFoundException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		} catch (RepositoryException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		}
	}

	private void replaceOfferTerms(Resource localOffer, Resource globalOffer) {
		LOG.info("replaceOfferTerms");
		Node localOfferRoot = localOffer.adaptTo(Node.class);
		Node globalOfferRoot = globalOffer.adaptTo(Node.class);
		try {
			Node localOfferDetails = localOfferRoot.getNode("jcr:content/root/offer_terms_conditions");
			Node globalOfferDetails = globalOfferRoot.getNode("jcr:content/root/offer_terms_conditions");
			nodeOperationService.replaceNode(globalOfferDetails, localOfferDetails);
		} catch (PathNotFoundException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		} catch (RepositoryException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		}
	}

	private void copyContentResourcePropertiesFromGlobal(Resource localOffer, Resource globalOffer) {
		LOG.info("CcopyContentResourcePropertiesFromGlobal");
		Node localOfferRoot = localOffer.adaptTo(Node.class);
		Node globalOfferRoot = globalOffer.adaptTo(Node.class);
		try {
			ArrayList<String> ignoreProperties = new ArrayList<String>();
			ignoreProperties.add("navTitle");
			ignoreProperties.add("sling:resourceType");
			ignoreProperties.add("cq:template");
			Node localJcrContent = localOfferRoot.getNode("jcr:content");
			Node globalJcrContent = globalOfferRoot.getNode("jcr:content");
			nodeOperationService.copyProperties(globalJcrContent, localJcrContent, ignoreProperties);

		} catch (PathNotFoundException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		} catch (RepositoryException e) {
			LOG.error("Failed to copy properties from between jcr content" + localOffer.getName()
					+ globalOffer.getName());
		}
	}


}
