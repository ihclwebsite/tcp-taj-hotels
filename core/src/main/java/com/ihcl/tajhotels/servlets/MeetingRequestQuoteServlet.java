package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.Quote;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/requestQuote" })
public class MeetingRequestQuoteServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(MeetingRequestQuoteServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService requestQuoteService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        String htmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for meeting request quote is : " + requestUrl);

        LOG.trace("Method Entry: " + methodName);
        try {
            if (validateRequest.validateUrl(requestUrl)) {
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                Quote requestobj = setEmailRequest(httpRequest);
                String captureDataFlag = httpRequest.getParameter("isGeneric");

                if (null != requestQuoteService) {
                    requestobj.setHotelName(httpRequest.getParameter("hotelName"));
                    if (null != httpRequest.getParameter("venueName")) {
                        requestobj.setVenue(httpRequest.getParameter("venueName"));
                    }
                    requestobj.setHotelEmailId(httpRequest.getParameter("hotelEmailId"));
                    if (null != httpRequest.getParameter("bookingDate")) {
                        requestobj.setPreferredDate(httpRequest.getParameter("bookingDate"));
                    } else {
                        requestobj.setArrivalDate(httpRequest.getParameter("bookingArrivalDate"));
                        requestobj.setDepartureDate(httpRequest.getParameter("bookingDepartureDate"));
                    }
                    requestobj.setNoOfGuests(httpRequest.getParameter("noOfGuests"));
                    requestobj.setFullName(httpRequest.getParameter("name"));
                    requestobj.setMobile(httpRequest.getParameter("phoneNumber"));
                    requestobj.setEmailId(httpRequest.getParameter("guestEmailId"));
                    requestobj.setTimeOfTheEvent(httpRequest.getParameter("timeOfTheEvent"));
                    requestobj.setPurposeOfTheEvent(httpRequest.getParameter("purposeOfTheEvent"));
                    requestobj.setEmailFromAddress(requestQuoteService.getEmailFromAddressForMeetingRequestQuote());
                    requestobj.setEmailSubject(requestQuoteService.getEmailSubjectForMeetingRequestQuote());
                    if (null != captureDataFlag && captureDataFlag.equalsIgnoreCase("true")) {
                        requestobj.setBudget(httpRequest.getParameter("tentativeBudget"));
                        requestobj.setNoOfRooms(httpRequest.getParameter("noOfRooms"));
                        requestobj.setNoOfDoubleRooms(httpRequest.getParameter("doubleRooms"));
                        requestobj.setNoOfSingleRooms(httpRequest.getParameter("singleRooms"));
                        requestobj.setNoOfTripleRooms(httpRequest.getParameter("tripleRooms"));
                        requestobj.setSpecialComments(httpRequest.getParameter("specialRequests"));
                    }

                    dynamicValuemap = getDynamicValuesForEmailMeetings(requestobj, dynamicValuemap);

                    String htmlBody = null;
                    if (null != captureDataFlag && captureDataFlag.equalsIgnoreCase("true")) {
                        htmlBody = requestQuoteService.getEmailBodyTemplateForGenericRequestQuote();
                    } else {
                        htmlBody = requestQuoteService.getEmailBodyTemplateForMeetingRequestQuote();
                    }
                    String unescapedStringBody = StringEscapeUtils.unescapeHtml4(htmlBody);
                    requestobj.setEmailTemplateForBody(unescapedStringBody);
                    requestobj.setDynamicValuesForEmailMeetings(dynamicValuemap);

                    htmlTemplate = StringTokenReplacement.replaceToken(requestobj.getDynamicValuesForEmailMeetings(),
                            unescapedStringBody);
                }

                if (null != captureDataFlag && captureDataFlag.equalsIgnoreCase("true")) {
                    LOG.trace("Capturing User data & storing the same in a node");
                    captureUserData(httpRequest, requestobj);
                }

                boolean isSendEmailSuccess = emailService.sendQuoteEmail(requestobj, htmlTemplate);
                if (isSendEmailSuccess) {

                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * Method to set map fields with token names and values
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValuesForEmailMeetings(Quote requestobj,
            HashMap<String, String> dynamicValuemap) {
        dynamicValuemap.put("hotelName", requestobj.getHotelName());
        dynamicValuemap.put("venueName", requestobj.getVenue());
        dynamicValuemap.put("guestEmailId", requestobj.getEmailId());
        dynamicValuemap.put("bookingDate", requestobj.getPreferredDate());
        dynamicValuemap.put("guest", requestobj.getNoOfGuests());
        dynamicValuemap.put("name", requestobj.getFullName());
        dynamicValuemap.put("phoneNumber", requestobj.getMobile());
        dynamicValuemap.put("timeOfTheEvent", requestobj.getTimeOfTheEvent());
        dynamicValuemap.put("purposeOfTheEvent", requestobj.getPurposeOfTheEvent());
        dynamicValuemap.put("bookingArrivalDate", requestobj.getArrivalDate());
        dynamicValuemap.put("bookingDepartureDate", requestobj.getDepartureDate());
        dynamicValuemap.put("noOfRooms", requestobj.getNoOfRooms());
        dynamicValuemap.put("singleRooms", requestobj.getNoOfSingleRooms());
        dynamicValuemap.put("doubleRooms", requestobj.getNoOfDoubleRooms());
        dynamicValuemap.put("tripleRooms", requestobj.getNoOfTripleRooms());
        dynamicValuemap.put("tentativeBudget", requestobj.getBudget());
        dynamicValuemap.put("specialRequests", requestobj.getSpecialComments());
        return dynamicValuemap;
    }

    /**
     * @param httpRequest
     */

    private Quote setEmailRequest(SlingHttpServletRequest httpRequest) throws Exception {
        String methodName = "setEmailRequest";
        LOG.trace("Method Entry: " + methodName);
        Quote requestobj = new Quote();
        requestobj.setVenue(httpRequest.getParameter("venueName"));
        requestobj.setHotelEmailId(httpRequest.getParameter("hotelEmailId"));
        requestobj.setPreferredDate(httpRequest.getParameter("bookingDate"));
        requestobj.setArrivalDate(httpRequest.getParameter("bookingArrivalDate"));
        requestobj.setDepartureDate(httpRequest.getParameter("bookingDepartureDate"));
        requestobj.setNoOfGuests(httpRequest.getParameter("guest"));
        requestobj.setFullName(httpRequest.getParameter("name"));
        requestobj.setMobile(httpRequest.getParameter("phoneNumber"));
        requestobj.setEmailId(httpRequest.getParameter("guestEmailId"));
        requestobj.setTimeOfTheEvent(httpRequest.getParameter("timeOfTheEvent"));
        requestobj.setPurposeOfTheEvent(httpRequest.getParameter("purposeOfTheEvent"));
        requestobj.setBudget(httpRequest.getParameter("tentativeBudget"));
        requestobj.setNoOfRooms(httpRequest.getParameter("noOfRooms"));
        requestobj.setNoOfDoubleRooms(httpRequest.getParameter("doubleRooms"));
        requestobj.setNoOfSingleRooms(httpRequest.getParameter("singleRooms"));
        requestobj.setNoOfTripleRooms(httpRequest.getParameter("tripleRooms"));
        requestobj.setSpecialComments(httpRequest.getParameter("specialRequests"));
        LOG.trace("Method Exit: " + methodName);
        return requestobj;
    }


    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }

    private void captureUserData(SlingHttpServletRequest httpRequest, Quote userData)
            throws RepositoryException, PersistenceException, LoginException {

        LOG.trace("Inside captureUserData Mathod{}");
        final String dataStorePath = "/content/usergenerated/meeting-data";
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        LOG.trace("Obtained Resource Resovler as {}", resourceResolver);
        Resource dataStorePathResource = resourceResolver.getResource(dataStorePath);
        LOG.trace("Obtained dataStorePathResource as {}", dataStorePathResource);

        String timeStamp = fetchCurrentTimeStamp();
        String[] splitTimeStamp = timeStamp.split(" ");
        String[] splitYearMonthDate = splitTimeStamp[0].split("/");
        String currentYear = splitYearMonthDate[0];
        String currentMonth = splitYearMonthDate[1];
        String currentDay = splitYearMonthDate[2];
        String time = splitTimeStamp[1].replaceAll(":", "").replaceAll("\\.", "");
        LOG.trace("UserData id {}", time);
        if (null != dataStorePathResource) {
            Node dateStoreNode = dataStorePathResource.adaptTo(Node.class);
            LOG.trace("Obtained Data Store Node as {}", dateStoreNode);
            Session session = dateStoreNode.getSession();

            if (dateStoreNode.hasNode(currentYear)) {
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                if (currentyearNode.hasNode(currentMonth)) {
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    if (currentMonthNode.hasNode(currentDay)) {
                        Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                        fetchValueMapFromObj(userDataMap, userData, timeStamp);
                        resourceResolver.commit();

                    } else {
                        currentMonthNode.addNode(currentDay, "nt:unstructured");
                        session.save();
                        Node currentDayNode = currentMonthNode.getNode(currentDay);
                        currentDayNode.addNode(time, "nt:unstructured");
                        session.save();
                        Node userNode = currentDayNode.getNode(time);
                        Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                        ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                        fetchValueMapFromObj(userDataMap, userData, timeStamp);
                        resourceResolver.commit();
                    }

                } else {
                    currentyearNode.addNode(currentMonth, "nt:unstructured");
                    session.save();
                    Node currentMonthNode = currentyearNode.getNode(currentMonth);
                    currentMonthNode.addNode(currentDay, "nt:unstructured");
                    session.save();
                    Node currentDayNode = currentMonthNode.getNode(currentDay);
                    currentDayNode.addNode(time, "nt:unstructured");
                    session.save();
                    Node userNode = currentDayNode.getNode(time);
                    Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                    ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                    fetchValueMapFromObj(userDataMap, userData, timeStamp);
                    resourceResolver.commit();
                }
            } else {
                dateStoreNode.addNode(currentYear, "nt:unstructured");
                session.save();
                Node currentyearNode = dateStoreNode.getNode(currentYear);
                currentyearNode.addNode(currentMonth, "nt:unstructured");
                session.save();
                Node currentMonthNode = currentyearNode.getNode(currentMonth);
                currentMonthNode.addNode(currentDay, "nt:unstructured");
                session.save();
                Node currentDayNode = currentMonthNode.getNode(currentDay);
                currentDayNode.addNode(time, "nt:unstructured");
                session.save();
                Node userNode = currentDayNode.getNode(time);
                Resource userNodeResrouce = resourceResolver.getResource(userNode.getPath());
                ModifiableValueMap userDataMap = userNodeResrouce.adaptTo(ModifiableValueMap.class);
                fetchValueMapFromObj(userDataMap, userData, timeStamp);
                resourceResolver.commit();
            }
        }
    }

    private void fetchValueMapFromObj(ModifiableValueMap valueMap, Quote requestobj, String timeSubmitted) {
        valueMap.put("hotelName", requestobj.getHotelName());
        valueMap.put("bookingArrivalDate", requestobj.getArrivalDate());
        valueMap.put("bookingDepartureDate", requestobj.getDepartureDate());
        valueMap.put("guestEmailId", requestobj.getEmailId());
        valueMap.put("guest", requestobj.getNoOfGuests());
        valueMap.put("name", requestobj.getFullName());
        valueMap.put("phoneNumber", requestobj.getMobile());
        valueMap.put("noOfRooms", requestobj.getNoOfRooms());
        valueMap.put("singleRooms", requestobj.getNoOfSingleRooms());
        valueMap.put("doubleRooms", requestobj.getNoOfDoubleRooms());
        valueMap.put("tripleRooms", requestobj.getNoOfTripleRooms());
        valueMap.put("timeOfTheEvent", requestobj.getTimeOfTheEvent());
        valueMap.put("purposeOfTheEvent", requestobj.getPurposeOfTheEvent());
        valueMap.put("tentativeBudget", requestobj.getBudget());
        valueMap.put("specialRequests", requestobj.getSpecialComments());
        valueMap.put("timeSubmitted", timeSubmitted);
    }

    private String fetchCurrentTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSS");
        String timeStamp = sdf.format(date);
        LOG.trace("Current Timestamp {}", timeStamp);
        return timeStamp;
    }

}
