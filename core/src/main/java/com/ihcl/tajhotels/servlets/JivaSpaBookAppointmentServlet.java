
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfigurationService;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.Appointment;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/bookAppointment" })
public class JivaSpaBookAppointmentServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(JivaSpaBookAppointmentServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    private EmailTemplateConfigurationService jivaSpaEmailConf;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for jiva spa book an appointment is : " + requestUrl);

        String finalHtmlTemplate = null;
        HashMap<String, String> dynamicValuemap = new HashMap<>();
        dynamicValuemap = getDynamicValuesForEmailJivaSpa(httpRequest, dynamicValuemap);
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.trace("Calling URL validate method" + validateRequest);
            if (validateRequest.validateUrl(requestUrl)) {
                LOG.trace("URL is secured ");

                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                Appointment requestobj = new Appointment();

                // Fields for email template configuration
                if (null != jivaSpaEmailConf) {
                    requestobj.setEmailId(httpRequest.getParameter("guestEmailId"));
                    requestobj.setBestTimeToContact(httpRequest.getParameter("timeToContact"));
                    requestobj.setHotelEmailId(httpRequest.getParameter("hotelEmailId"));
                    requestobj.setEmailFromAddress(jivaSpaEmailConf.getEmailFromAddressForJivaSpa());
                    requestobj.setEmailSubject(jivaSpaEmailConf.getEmailSubjectForJivaSpa());
                    requestobj.setEmailTemplateForBody(jivaSpaEmailConf.getEmailBodyTemplateForJivaSpa());
                    String htmlString = jivaSpaEmailConf.getEmailBodyTemplateForJivaSpa();
                    String unescape = StringEscapeUtils.unescapeHtml4(htmlString);

                    LOG.trace("Unescaped string  : " + unescape);

                    requestobj.setDynamicValuesForEmailJivaSpa(dynamicValuemap);
                    finalHtmlTemplate = StringTokenReplacement
                            .replaceToken(requestobj.getDynamicValuesForEmailJivaSpa(), unescape);
                    LOG.trace("Final html template for jiva spa email booking confirmation is : " + finalHtmlTemplate);
                } else if (null == jivaSpaEmailConf) {
                    LOG.warn("Email Configuration for jiva spa is missing");
                }

                boolean isSendEmailSuccess = emailService.sendAppointmentEmail(requestobj, finalHtmlTemplate);
                if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.trace("This request contain cross site attck characters :");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while Sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);


    }

    /**
     * Method to set map fields with token names and values
     *
     * @param dynamicValuemap
     *
     * @return map
     */
    private HashMap<String, String> getDynamicValuesForEmailJivaSpa(SlingHttpServletRequest httpRequest,
            HashMap<String, String> dynamicValuemap) {
        dynamicValuemap.put("hotelName", httpRequest.getParameter("hotelName"));
        dynamicValuemap.put("jivaSpaName", httpRequest.getParameter("jivaSpaName"));
        dynamicValuemap.put("hotelEmailId", httpRequest.getParameter("hotelEmailId"));
        dynamicValuemap.put("bookingDate", httpRequest.getParameter("bookingDate"));
        dynamicValuemap.put("city", httpRequest.getParameter("city"));
        dynamicValuemap.put("name", httpRequest.getParameter("name"));
        dynamicValuemap.put("gender", httpRequest.getParameter("gender"));
        dynamicValuemap.put("phoneNumber", httpRequest.getParameter("phoneNumber"));
        dynamicValuemap.put("guestEmailId", httpRequest.getParameter("guestEmailId"));
        dynamicValuemap.put("spaDuration", httpRequest.getParameter("spaDuration"));
        dynamicValuemap.put("spaAmount", httpRequest.getParameter("spaAmount"));
        dynamicValuemap.put("timeToContact", httpRequest.getParameter("timeToContact"));
        dynamicValuemap.put("spaStartTime", httpRequest.getParameter("spaStartTime"));
        return dynamicValuemap;
    }

    /**
     * @param response
     * @param code
     * @param message
     * @throws IOException
     */
    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }


}

