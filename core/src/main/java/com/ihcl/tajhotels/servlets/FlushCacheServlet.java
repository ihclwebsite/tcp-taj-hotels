package com.ihcl.tajhotels.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.DispatcherRateFlushService;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/flushcache", "sling.servlet.methods=GET" })

public class FlushCacheServlet extends SlingSafeMethodsServlet {

    private static final String HTTP = "http";

    private static final String HOTEL_ID = "hotelId";

    private static final String DESTINATION_TAG = "destinationTag";

    private static final String SERVER = "server";

    private static final String HANDLE = "handle";

    private static final long serialVersionUID = -2818846349871249343L;

    static final String SERVLET_PATH = "sling.servlet.paths";

    @Reference
    private DispatcherRateFlushService rateFlushService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        logger.info("Inside get method of FlushCache");
        response.setContentType("text/html");
        String handle = request.getParameter(HANDLE);
        String server = request.getParameter(SERVER);
        String hotelId = request.getParameter(HOTEL_ID);
        String destinationTag = request.getParameter(DESTINATION_TAG);
        try {
            if (hotelId != null && !hotelId.isEmpty()) {
                String fullResposnse = rateFlushService.clearHotelRateCache(hotelId);
                if (fullResposnse == DispatcherRateFlushService.ERROR) {
                    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, fullResposnse);
                } else {
                    response.getWriter().write(fullResposnse.replaceAll("\n", "<br>"));
                }
                return;
            } else if (StringUtils.isNotBlank(destinationTag)) {
                String fullResposnse = rateFlushService.clearDestinationRatesCache(destinationTag);
                if (fullResposnse == DispatcherRateFlushService.ERROR) {
                    response.sendError(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR, fullResposnse);
                } else {
                    response.getWriter().write(fullResposnse.replaceAll("\n", "<br>"));
                }
                return;
            }
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().write(rateFlushService.flushCache(handle, server, HTTP).replaceAll("\n", "<br>"));
        } catch (IOException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

}
