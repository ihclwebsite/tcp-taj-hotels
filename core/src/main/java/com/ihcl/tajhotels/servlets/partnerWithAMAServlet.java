package com.ihcl.tajhotels.servlets;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.ihcl.core.ama.PartnerAmaResponse;
import com.ihcl.core.ama.configuration.IAmaConfiguration;
import com.ihcl.core.util.StringTokenReplacement;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.PartnerWithAMA;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(service = Servlet.class,immediate = true,property = { "sling.servlet.methods=POST","sling.servlet.paths=" + "/bin/partnerwithama" })
public class partnerWithAMAServlet extends SlingAllMethodsServlet {

	 @Reference
	 private IEmailService emailService;

    @Reference
    private IAmaConfiguration emailConf;
	    
	String finalHtmlTemplate = "&lt;html&gt;&lt;table style=\"font-family:theantiquasun,Helvetica,Arial,sans-serif;width: 97%;border-collapse:collapse\"&gt;&lt;tr&gt;&lt;td colspan=\"2\"&gt;&lt;div style=\"background:#c25031;color:#fff;padding:4px;font-family:theantiquasun,Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:20px\"&gt; DETAILS &lt;/div&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; Name &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%;\"&gt; ${name} &lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Mobile Number &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${mobile}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Email Id &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${emailId}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Location &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${location}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Region &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${region}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Area Of House &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${areaOfHouse}  Sq.M &lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"background-color:#dddddd8c;font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Number Of Bedroom &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${numberOfBedroom}&lt;/td&gt;&lt;/tr&gt;&lt;tr style=\"font-size:12px\"&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt;Comments &lt;/td&gt;&lt;td style=\"border:1px solid #000;text-align:left;padding:3px;width:50%\"&gt; ${comments}&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;";	
	
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(partnerWithAMAServlet.class);

    HashMap<String, String> dynamicValuemap = new HashMap<>();

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("Method Entry: {}", "doGet");
        try {
        	
        	ObjectMapper objectMapper = new ObjectMapper();

            PartnerWithAMA partnerWithAMA = new PartnerWithAMA();
            
            partnerWithAMA.setName(request.getParameter("name"));            
            partnerWithAMA.setMobile(request.getParameter("mobile"));
            partnerWithAMA.setEmailId(request.getParameter("email"));
            partnerWithAMA.setLocation(request.getParameter("loc"));
            partnerWithAMA.setRegion(request.getParameter("region"));
            partnerWithAMA.setAreaOfHouse(request.getParameter("area"));
            partnerWithAMA.setNumberOfBedroom(request.getParameter("noOfBedrooms"));
            partnerWithAMA.setComments(request.getParameter("msg"));
            
             
            if (partnerWithAMA.getEmailId() != null && partnerWithAMA.getMobile() != null) {
            	getDynamicValues(partnerWithAMA); 
            	partnerWithAMA.setDynamicValues(dynamicValuemap);
            	
            	partnerWithAMA.setEmailToAddress(emailConf.getEmailToAddress());
            	partnerWithAMA.setEmailSubject(emailConf.getEmailSubject());
            	
            	
            	String unescape = StringEscapeUtils.unescapeHtml4(finalHtmlTemplate);
            	LOG.trace("Unescaped string  : " + unescape);
            	
            	finalHtmlTemplate = StringTokenReplacement.replaceToken(partnerWithAMA.getDynamicValues(), unescape);
            	
            	boolean isSendEmailSuccess = emailService.sendEmailPartnerWithAMA(partnerWithAMA, finalHtmlTemplate);
            	PartnerAmaResponse partnerAmaResponse=new PartnerAmaResponse();
            	if (isSendEmailSuccess) {
                    LOG.trace("Email has been successfully");
                    
                    partnerAmaResponse.setSuccessMsg("true");
                } else {
                	partnerAmaResponse.setErrorMsg("Error in Applying for AMA property");
                }
            	response.getWriter().println(objectMapper.writeValueAsString(partnerAmaResponse));
            	
            }
        } catch (Exception e) {
            LOG.error("An error occured while sending Email: ", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

        }
    }

    private void getDynamicValues(PartnerWithAMA partnerWithAMA) {
    	
        dynamicValuemap.put("name", partnerWithAMA.getName());
        dynamicValuemap.put("mobile", partnerWithAMA.getMobile());
        dynamicValuemap.put("emailId", partnerWithAMA.getEmailId());
        dynamicValuemap.put("location", partnerWithAMA.getLocation());
        dynamicValuemap.put("region", partnerWithAMA.getRegion());
        dynamicValuemap.put("areaOfHouse", partnerWithAMA.getAreaOfHouse());
        dynamicValuemap.put("numberOfBedroom", partnerWithAMA.getNumberOfBedroom());
        dynamicValuemap.put("comments", partnerWithAMA.getComments());
    }
    
    
}
