package com.ihcl.tajhotels.servlets;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.xml.bind.DatatypeConverter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = {Servlet.class}, immediate = true, property = {"sling.servlet.paths=/bin/signedPayload", "sling.servlet.methods=GET"})
public class JuspaySignatureServlet extends SlingAllMethodsServlet {
  private static final long serialVersionUID = 1L;
  
  private static final Logger LOG = LoggerFactory.getLogger(JuspaySignatureServlet.class);
  
  String fileContent = "-----BEGIN RSA PRIVATE KEY-----\r\nMIIEpQIBAAKCAQEA2R+5+b1HZSNNMotnr7Z/5By4NSTZW4dDMGDy2huOSLn1EF6v\r\n75sssdY5kRkFoihLIeNQA+yzsi0kzpz3rCCCmo1DJwgoqA48JVQwwBjZ9SHeC0nE\r\n66VODmMJJGNWe1quHWQb3otIzS+U+rtd1Alzo9up8u8e+FrecyjO6fBMZfd32iO7\r\nqPtExtA1XDtKMqoRbHMiAz940xA5+BLmJC+gp1IYsVce2KA5BW1laPxbku42aQR7\r\neZipSa3BYRY8m964Aj6vLj4kTeTbrc4OH7yatRdWbVbrwVWpg936g8Q3Qf3jQY+H\r\nMu76l1WeXK4GkPkA+oJXY6ag1XhhqtOrLw3rJwIDAQABAoIBAQCjy2thG4lgouD5\r\n4HC3/dU9IO1WKhZPFht5w6lxIJiWBLL7RnMzLrzo69NBwr6dNgh36CPU0hw9rhC2\r\nTXQKRfxA25BtQZpqLVLyVjDwuc6zPnljyqLjojDgaZXb/ZSgOihfw8XCfRDOubaJ\r\n8A84hmjWlEABJKMYeHSYK5Dsqnr37/Oj4OT2NWLaRx8Kk0HPuv/bxx3MHurIHRtG\r\n514UJZcOfN8Ti69/DoYbtb/Mpg/djXr5s47TafxPa8jyT2E8nWboPvYPDQcdu2CI\r\nE0mbrj2C0Ak7g20ZYzm9HCbJHVG+2rPSjVgXKW2ZgXoZflte+G5vRfDrZH+TZuo+\r\nja0pVlIBAoGBAP4ZEtdpRJp5kvj7bUIkXd5E6lrxd2M0VprfMhHk0i+Z1p1nF8St\r\nF6p9uIGuRIEthvdRZVy56fWCHXOfmquRJDHazQZVnnXcRaqhuMYdZoJ3i1KkmSL4\r\nX/SdBsB4rY4FQZWNtwKoSeDugQeJzf4bhyn0iZGbWPq+XO70MxXya8CfAoGBANq/\r\nzL6ul+G6i/nXrfzwUr83EtXh6Zoj51YBK4g3ZIIuWkWvbo9NguV3p9KmeRKMWwYO\r\nRHC7aVwpHdjzOyzmSFdmC+5dqVe6rkdl9AzxpKt0p0rOznmZUhDcdElCk0p6pC5R\r\nQDAt2PA4aR3kT+9z2dPV0IHsUGiouF/LtmTmdCB5AoGAIShUdRefhCjpLORiVYc5\r\nWI/VpRhtY9yokH0fo4Ygh2Wjw9Z4G4oa1HyjXwjGl7TBL/THLVp1VTwta7EgFdNS\r\nzc6ngnQZwXeE/8cqvW+IuO2wmJAyC4Ytv1XeU69rtmSpMkLT5tzfByMYY0twPgCJ\r\nmsf2S7Hh4paEugnTwMFpnjECgYEAoTqc/i5RY97LLOr7ImM/mhBNobdRJnswFwPl\r\nwhCR1CG2B4a2Rokq4VbAK1LoCfPJYz1A1JZNoc/sX+tmwkE5MLHWOWpvVmoR6i4L\r\nIz83z+e7JjgnlxialDLowtZ/GXYrbLgWR2yDaQsq7w1InYUWGDyP4jL7USiKPJE5\r\nbkUtcoECgYEAwhbb1NxzteIr8zlytMj52sgeiJPQRjbU5CoMAJuiHvYHT8jQwso7\r\nlfbz+fXdQamU29v1Hdhc2JR1xWxrTz4bAt1l9lWK8zQBTK3SOlhyvrvNkKtTwjan\r\nsR6+uwB9KY5mrF++pRA8IL2f0yhx2uqwDkX/Og6ZnFHJn3BvQM/DWPg=\r\n-----END RSA PRIVATE KEY-----\r\n";
  
  protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
    String methodName = "doPost";
    LOG.info("Method Entry: " + methodName);
  //  String custmobile = request.getParameter("custMobile");
    //LOG.info("custmobileno " + custmobile);
   // String custemail = request.getParameter("custemailid");
   // String customerId = request.getParameter("customerid");
   // String merchantid = request.getParameter("merchantid");
   // String returnurl = request.getParameter("returnurl");
    //String timestamp = request.getParameter("timestamp");
   // String orderid = request.getParameter("itineraryNumber");
   // String amnt = request.getParameter("totalcartPrice");
    String orderpayload = request.getParameter("obj");
    
    LOG.info("metadata  " +orderpayload);
    
  //  JSONObject jObj = null;;
	//JSONObject CCAVENUE_V2 =null;
  /*  JSONObject CCAVENUE_V2 = null;
	try {
		//CCAVENUE_V2 = jObj.getJSONObject(request.getParameter("reqMetadata"));
		// CCAVENUE_V2 = new JSONObject(request.getParameter("reqMetadata")); 
		//LOG.info("CCAVENUE_V2 "+CCAVENUE_V2);
	} catch (JSONException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}*/
   
	//JuspayOrderPayload jsupayOrder = new JuspayOrderPayload(orderid, amnt, customerId, merchantid, custmobile, custemail, timestamp, returnurl,metadata);
    Gson gson = new Gson();
   String json = gson.toJson(orderpayload);
     json = json.replaceAll("\\\\", "");
    LOG.info("Jsona order payload is  " + json);
   // String data = "{\"order_id\":\"987654321abcd\",\"amount\":\"1.00\",\"customer_id\":\"987654321\",\"merchant_id\":\"ihcl\",\"customer_phone\":\"9970235674\",\"customer_email\":\"sonal123@gmail.com\",\"timestamp\":\"1571922200845\",\"return_url\":\"https://author-taj-dev63.adobecqms.net/content/tajhotels/en-in/booking-confirmation.html\"}";
  /*  Map<String, String> orderPayload = new LinkedHashMap<>();
    orderPayload.put("order_id", orderid);
    orderPayload.put("amount", amnt);
    orderPayload.put("customer_id", customerId);
    orderPayload.put("merchant_id", merchantid);
    orderPayload.put("customer_phone", custmobile);
    orderPayload.put("customer_email", custemail);
    orderPayload.put("timestamp", timestamp);
    orderPayload.put("return_url", returnurl);
    LOG.info("orderPayload " + orderPayload);
    String orderPayloadString = parseOrderParameters(orderPayload);
    LOG.info(orderPayloadString);*/
    JSONObject jsonNode = new JSONObject();
    ResourceResolver resourceResolver = request.getResourceResolver();
    try {
      LOG.info("Inside try block, calling sign method");
      sign(orderpayload, fileContent, Boolean.valueOf(true), response);
    } catch (Exception e) {
      e.printStackTrace();
    } 
  }
  
/*  public String parseOrderParameters(Map<String, String> orderDetails) throws UnsupportedEncodingException {
    String paymentString = "";
    LOG.debug("--------------------Setting request String--------------------");
    Iterator<Map.Entry<String, String>> iterator = orderDetails.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, String> entry = iterator.next();
      paymentString = paymentString.concat((String)entry.getKey() + ":" + (String)entry
          .getValue() + ",");
    } 
    return paymentString;
  }*/
  
  public static void sign(String data, String keyContentOrFile, Boolean urlEncodeSignature, SlingHttpServletResponse resp) throws Exception {
    Signature rsa = Signature.getInstance("SHA256withRSA");
    PrivateKey privateKey = null;
    data = URLDecoder.decode(data);
    if (keyContentOrFile.startsWith("--")) {
      privateKey = readPrivateKey(keyContentOrFile);
    } else {
      privateKey = readPrivateKeyFromFile(keyContentOrFile);
    } 
    rsa.initSign(privateKey);
    rsa.update(data.getBytes());
    byte[] sign = rsa.sign();
    if (urlEncodeSignature.booleanValue()) {
      String p1 = URLEncoder.encode(DatatypeConverter.printBase64Binary(sign), "UTF-8");
      LOG.info("Signature value:- " + p1);
      LOG.info("Order PAyload value:- " + data);
      JSONObject jsonNode = new JSONObject();
      try {
        jsonNode.put("sessionToken", p1);
        jsonNode.put("OrderPayload", data);
        resp.setContentType("application/json");
        resp.getWriter().write(jsonNode.toString());
      } catch (JSONException e) {
        e.printStackTrace();
      } 
    } else {
      String p1 = DatatypeConverter.printBase64Binary(sign);
      LOG.info("Signature value" + p1);
    } 
  }
  
  public static PrivateKey readPrivateKeyFromFile(String filename) throws Exception {
    File keyFile = new File(filename);
    BufferedReader reader = new BufferedReader(new FileReader(keyFile));
    StringBuffer fileContent = new StringBuffer();
    Boolean isPkcs1Content = Boolean.valueOf(false);
    String line;
    while ((line = reader.readLine()) != null) {
      if (!line.startsWith("--")) {
        fileContent.append(line).append("\n");
        continue;
      } 
      if (!isPkcs1Content.booleanValue() && line.startsWith("-----BEGIN RSA PRIVATE KEY-----"))
        isPkcs1Content = Boolean.valueOf(true); 
    } 
    byte[] keyBytes = DatatypeConverter.parseBase64Binary(fileContent.toString());
    return generatePrivateKey(keyBytes, isPkcs1Content);
  }
  
  public static PrivateKey readPrivateKey(String content) throws Exception {
    LOG.info("Inside readPrivateKey method");
    Boolean isPkcs1Content = Boolean.valueOf(false);
    if (content.startsWith("-----BEGIN RSA PRIVATE KEY-----"))
      isPkcs1Content = Boolean.valueOf(true); 
    content = content.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "");
    content = content.replaceAll("-----BEGIN PRIVATE KEY-----", "");
    content = content.replaceAll("-----END RSA PRIVATE KEY-----", "");
    content = content.replaceAll("-----END PRIVATE KEY-----", "");
    byte[] keyBytes = DatatypeConverter.parseBase64Binary(content.toString());
    return generatePrivateKey(keyBytes, isPkcs1Content);
  }
  
  private static PrivateKey generatePrivateKey(byte[] keyBytes, Boolean isPkcs1Content) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
    LOG.info("Inside generatePrivateKey method");
    PrivateKey privateKey = null;
    if (isPkcs1Content.booleanValue()) {
      RSAPrivateCrtKeySpec keySpec = getRSAKeySpec(keyBytes);
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      privateKey = keyFactory.generatePrivate(keySpec);
    } else {
      PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
      KeyFactory kf = KeyFactory.getInstance("RSA");
      privateKey = kf.generatePrivate(spec);
    } 
    LOG.info("privateKey value" + privateKey);
    return privateKey;
  }
  
  private static RSAPrivateCrtKeySpec getRSAKeySpec(byte[] keyBytes) throws IOException {
    System.out.println("Inside rsaKeySpec method");
    DerParser parser = new DerParser(keyBytes);
    Asn1Object sequence = parser.read();
    if (sequence.getType() != 16)
      throw new IOException("Invalid DER: not a sequence"); 
    parser = sequence.getParser();
    parser.read();
    BigInteger modulus = parser.read().getInteger();
    BigInteger publicExp = parser.read().getInteger();
    BigInteger privateExp = parser.read().getInteger();
    BigInteger prime1 = parser.read().getInteger();
    BigInteger prime2 = parser.read().getInteger();
    BigInteger exp1 = parser.read().getInteger();
    BigInteger exp2 = parser.read().getInteger();
    BigInteger crtCoef = parser.read().getInteger();
    RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(modulus, publicExp, privateExp, prime1, prime2, exp1, exp2, crtCoef);
    LOG.info("RSAPrivateCrtKeySpec value" + keySpec);
    return keySpec;
  }
}

class DerParser {

	  // Classes
	  public final static int UNIVERSAL = 0x00;
	  public final static int APPLICATION = 0x40;
	  public final static int CONTEXT = 0x80;
	  public final static int PRIVATE = 0xC0;

	  // Constructed Flag
	  public final static int CONSTRUCTED = 0x20;

	  // Tag and data types
	  public final static int ANY = 0x00;
	  public final static int BOOLEAN = 0x01;
	  public final static int INTEGER = 0x02;
	  public final static int BIT_STRING = 0x03;
	  public final static int OCTET_STRING = 0x04;
	  public final static int NULL = 0x05;
	  public final static int OBJECT_IDENTIFIER = 0x06;
	  public final static int REAL = 0x09;
	  public final static int ENUMERATED = 0x0a;
	  public final static int RELATIVE_OID = 0x0d;

	  public final static int SEQUENCE = 0x10;
	  public final static int SET = 0x11;

	  public final static int NUMERIC_STRING = 0x12;
	  public final static int PRINTABLE_STRING = 0x13;
	  public final static int T61_STRING = 0x14;
	  public final static int VIDEOTEX_STRING = 0x15;
	  public final static int IA5_STRING = 0x16;
	  public final static int GRAPHIC_STRING = 0x19;
	  public final static int ISO646_STRING = 0x1A;
	  public final static int GENERAL_STRING = 0x1B;

	  public final static int UTF8_STRING = 0x0C;
	  public final static int UNIVERSAL_STRING = 0x1C;
	  public final static int BMP_STRING = 0x1E;

	  public final static int UTC_TIME = 0x17;
	  public final static int GENERALIZED_TIME = 0x18;

	  protected InputStream in;

	  /**
	   * Create a new DER decoder from an input stream.
	   * 
	   * @param in
	   *            The DER encoded stream
	   */
	  public DerParser(InputStream in) throws IOException {
	    this.in = in;
	  }

	  /**
	   * Create a new DER decoder from a byte array.
	   * 
	   * @param The
	   *            encoded bytes
	   * @throws IOException 
	   */
	  public DerParser(byte[] bytes) throws IOException {
	    this(new ByteArrayInputStream(bytes));
	  }

	  /**
	   * Read next object. If it's constructed, the value holds
	   * encoded content and it should be parsed by a new
	   * parser from <code>Asn1Object.getParser</code>.
	   * 
	   * @return A object
	   * @throws IOException
	   */
	  public Asn1Object read() throws IOException {
	    int tag = in.read();

	    if (tag == -1)
	      throw new IOException("Invalid DER: stream too short, missing tag"); //$NON-NLS-1$

	    int length = getLength();

	    byte[] value = new byte[length];
	    int n = in.read(value);
	    if (n < length)
	      throw new IOException("Invalid DER: stream too short, missing value"); //$NON-NLS-1$

	    Asn1Object o = new Asn1Object(tag, length, value);

	    return o;
	  }

	  /**
	   * Decode the length of the field. Can only support length
	   * encoding up to 4 octets.
	   * 
	   * <p/>In BER/DER encoding, length can be encoded in 2 forms,
	   * <ul>
	   * <li>Short form. One octet. Bit 8 has value "0" and bits 7-1
	   * give the length.
	     * <li>Long form. Two to 127 octets (only 4 is supported here). 
	     * Bit 8 of first octet has value "1" and bits 7-1 give the 
	     * number of additional length octets. Second and following 
	     * octets give the length, base 256, most significant digit first.
	   * </ul>
	   * @return The length as integer
	   * @throws IOException
	   */
	  private int getLength() throws IOException {

	    int i = in.read();
	    if (i == -1)
	      throw new IOException("Invalid DER: length missing"); //$NON-NLS-1$

	    // A single byte short length
	    if ((i & ~0x7F) == 0)
	      return i;

	    int num = i & 0x7F;

	    // We can't handle length longer than 4 bytes
	    if ( i >= 0xFF || num > 4) 
	      throw new IOException("Invalid DER: length field too big (" //$NON-NLS-1$
	          + i + ")"); //$NON-NLS-1$

	    byte[] bytes = new byte[num];     
	    int n = in.read(bytes);
	    if (n < num)
	      throw new IOException("Invalid DER: length too short"); //$NON-NLS-1$

	    return new BigInteger(1, bytes).intValue();
	  }

	} 

class Asn1Object {

	  protected final int type;
	  protected final int length;
	  protected final byte[] value;
	  protected final int tag;

	  /**
	   * Construct a ASN.1 TLV. The TLV could be either a
	   * constructed or primitive entity.
	   * 
	   * <p/>The first byte in DER encoding is made of following fields,
	   * <pre>
	   *-------------------------------------------------
	     *|Bit 8|Bit 7|Bit 6|Bit 5|Bit 4|Bit 3|Bit 2|Bit 1|
	     *-------------------------------------------------
	     *|  Class    | CF  |     +      Type             |
	     *-------------------------------------------------
	   * </pre>
	   * <ul>
	   * <li>Class: Universal, Application, Context or Private
	   * <li>CF: Constructed flag. If 1, the field is constructed.
	   * <li>Type: This is actually called tag in ASN.1. It
	   * indicates data type (Integer, String) or a construct
	   * (sequence, choice, set).
	   * </ul>
	   * 
	   * @param tag Tag or Identifier
	   * @param length Length of the field
	   * @param value Encoded octet string for the field.
	   */
	  public Asn1Object(int tag, int length, byte[] value) {
	    this.tag = tag;
	    this.type = tag & 0x1F;
	    this.length = length;
	    this.value = value;
	  }

	  public int getType() {
	    return type;
	  }

	  public int getLength() {
	    return length;
	  }

	  public byte[] getValue() {
	    return value;
	  }

	  public boolean isConstructed() {
	    return  (tag & DerParser.CONSTRUCTED) == DerParser.CONSTRUCTED;
	  }

	  /**
	   * For constructed field, return a parser for its content.
	   * 
	   * @return A parser for the construct.
	   * @throws IOException
	   */
	  public DerParser getParser() throws IOException {
	    if (!isConstructed()) 
	      throw new IOException("Invalid DER: can't parse primitive entity"); //$NON-NLS-1$

	    return new DerParser(value);
	  }

	  /**
	   * Get the value as integer
	   * 
	   * @return BigInteger
	   * @throws IOException
	   */
	  public BigInteger getInteger() throws IOException {
	      if (type != DerParser.INTEGER)
	        throw new IOException("Invalid DER: object is not integer"); //$NON-NLS-1$

	      return new BigInteger(value);
	  }

	  /**
	   * Get value as string. Most strings are treated
	   * as Latin-1.
	   * 
	   * @return Java string
	   * @throws IOException
	   */
	  public String getString() throws IOException {

	    String encoding;

	    switch (type) {

	    // Not all are Latin-1 but it's the closest thing
	    case DerParser.NUMERIC_STRING:
	    case DerParser.PRINTABLE_STRING:
	    case DerParser.VIDEOTEX_STRING:
	    case DerParser.IA5_STRING:
	    case DerParser.GRAPHIC_STRING:
	    case DerParser.ISO646_STRING:
	    case DerParser.GENERAL_STRING:
	      encoding = "ISO-8859-1"; //$NON-NLS-1$
	      break;

	    case DerParser.BMP_STRING:
	      encoding = "UTF-16BE"; //$NON-NLS-1$
	      break;

	    case DerParser.UTF8_STRING:
	      encoding = "UTF-8"; //$NON-NLS-1$
	      break;

	    case DerParser.UNIVERSAL_STRING:
	      throw new IOException("Invalid DER: can't handle UCS-4 string"); //$NON-NLS-1$

	    default:
	      throw new IOException("Invalid DER: object is not a string"); //$NON-NLS-1$
	    }

	    return new String(value, encoding);
	  }
}
