/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.EMAIL_SERVICE_SUCCESS;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.MESSAGE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_FAILURE;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_KEY;
import static com.ihcl.tajhotels.constants.HttpResponseMessage.RESPONSE_CODE_SUCCESS;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;
import com.ihcl.core.util.ValidateRequestUrlInServlet;
import com.ihcl.tajhotels.email.api.IEmailService;
import com.ihcl.tajhotels.email.requestdto.WriteToUs;

/**
 * @author Gayathri
 *
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/contactus" })
public class ContactUsEnquiryFeedbackServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(ContactUsEnquiryFeedbackServlet.class);

    @Reference
    private IEmailService emailService;

    @Reference
    TajApiKeyConfigurationService tajApiKeyConfigurationService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        String methodName = "doGet";
        LOG.trace("\n\n In AboutUsEnquirServlet Class-> Method Entry: " + methodName);

        ValidateRequestUrlInServlet validateRequest = new ValidateRequestUrlInServlet();
        String requestUrl = validateRequest.getRequestUrl(httpRequest);
        LOG.trace("Request URL for contact us enquiry feedback servlet is : " + requestUrl);


        try {
            if (validateRequest.validateUrl(requestUrl)) {
                if (tajApiKeyConfigurationService != null) {
                    LOG.trace("tajApiKeyConfigurationService is not null : "
                            + tajApiKeyConfigurationService.getTajAdminEmailId());
                } else if (tajApiKeyConfigurationService == null) {
                    LOG.trace("tajApiKeyConfigurationService is null : ");
                }
                response.setContentType("application" + "/" + "json" + ";" + "charset=UTF-8");

                WriteToUs requestobj = setEnquiryFeedbackDetails(httpRequest);

                boolean isSendEmailSuccess = emailService.sendContactUsWriteToUs(requestobj);
                if (isSendEmailSuccess) {

                    LOG.trace("Email has been successfully ");

                    writeJsonToResponse(response, RESPONSE_CODE_SUCCESS, EMAIL_SERVICE_SUCCESS);
                } else {
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

                    writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
                }
            } else {
                LOG.info("This request contains cross site attack characters");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
            }

        } catch (Exception e) {
            LOG.error("An error occured while sending Email.", e);

            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            writeJsonToResponse(response, RESPONSE_CODE_FAILURE, EMAIL_SERVICE_FAILURE);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private WriteToUs setEnquiryFeedbackDetails(SlingHttpServletRequest httpRequest) throws Exception {
        String methodName = "setEnquiryFeedbackDetails";
        LOG.trace("Method Entry: " + methodName);
        WriteToUs requestobj = new WriteToUs();
        LOG.trace("emailID value -" + httpRequest.getParameter("emailId"));
        if (tajApiKeyConfigurationService.getTajAdminEmailId() != null) {
            requestobj.setAdminMailId(tajApiKeyConfigurationService.getIHCLWriteToUsAdminEmailId());
            LOG.trace("\n\nadmin mail id from osgi is " + tajApiKeyConfigurationService.getIHCLWriteToUsAdminEmailId());
        } else
            LOG.trace("\n\nadmin mail id from osgi config is null ");
        requestobj.setFullName(httpRequest.getParameter("fullName"));
        requestobj.setMobileNo(httpRequest.getParameter("mobile"));
        requestobj.setEmailId(httpRequest.getParameter("emailId"));
        requestobj.setFeedback(httpRequest.getParameter("feedback"));
        LOG.trace("\n\nfull name - " + httpRequest.getParameter("fullName"));
        LOG.trace("\n\nmobile no. - " + httpRequest.getParameter("mobile"));
        LOG.trace("\n\nemail id - " + httpRequest.getParameter("emailId"));
        LOG.trace("\n\nfeedback - " + httpRequest.getParameter("feedback"));

        /*
         * These additional parameters are added as part of TajHotels Feedback Form there is a check box at cqDialog
         * level which enables these 4 parameters to be rendered at UI level by-default these parameters would to empty
         * from client side.
         */

        String hotelName = httpRequest.getParameter("hotelname");
        String city = httpRequest.getParameter("city");
        String checkInDate = httpRequest.getParameter("checkInDate");
        String checkOutDate = httpRequest.getParameter("checkOutdate");

        LOG.trace("HotelName == > " + hotelName);
        LOG.trace("City == > " + city);
        LOG.trace("checkInDate == > " + checkInDate);
        LOG.trace("checkOutDate == > " + checkOutDate);

        requestobj.setHotelName(hotelName);
        requestobj.setCity(city);
        requestobj.setCheckInDate(checkInDate);
        requestobj.setCheckOutDate(checkOutDate);

        LOG.trace("Method Exit: " + methodName);
        return requestobj;
    }

    private void writeJsonToResponse(final SlingHttpServletResponse response, String code, String message)
            throws IOException {
        String methodName = "writeJsonToResponse";
        LOG.trace("Method Entry: " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();

        jsonNode.put(MESSAGE_KEY, message);
        jsonNode.put(RESPONSE_CODE_KEY, code);
        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        LOG.trace("The Value of JSON: " + jsonString);
        LOG.trace("Method Exit: " + methodName);

        response.getWriter().write(jsonNode.toString());
    }
}
