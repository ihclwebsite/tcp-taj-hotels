/**
 *
 */
package com.ihcl.tajhotels.servlets;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.integration.api.IReadReservationService;

/**
 * The Class FindReservationServlet.
 */
@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/findReservation", "sling.servlet.methods=GET" })

public class FindReservationServlet extends SlingSafeMethodsServlet {


    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(FindReservationServlet.class);


    @Reference
    private IReadReservationService hotelReadReservation;


    /*
     * (non-Javadoc)
     *
     * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling. api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        ResourceResolver resolver = null;
        Session session = null;
        try {
            resolver = request.getResourceResolver();
            session = resolver.adaptTo(Session.class);
            FindReservationHelper helper = new FindReservationHelper();
            String responseFromServer = helper.getBookingReservationResponse(request, hotelReadReservation, resolver,
                    session);
            response.getWriter().write(responseFromServer);
        } catch (Exception e) {
            LOG.debug("Exception found in doGet :: {}", e);
            LOG.error(" Exception found in doGet :: {}", e.getMessage());
        } finally {
            if (null != resolver) {
                resolver.close();
            }
            if (null != session) {
                session.logout();
            }
        }
    }
}
