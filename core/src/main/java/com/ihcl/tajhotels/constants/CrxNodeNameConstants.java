package com.ihcl.tajhotels.constants;

/**
 * Constants to map to the values that appear in crx.
 */
public interface CrxNodeNameConstants {


    String SERVICE_AMENITIES = "serviceAmenities";
}
