package com.ihcl.tajhotels.constants;

public interface HttpResponseMessage {

    String MESSAGE_KEY = "message";

    String ROOMS_AVAILABILITY_SUCCESS = "Fetched room availability successfully";

    String ROOMS_AVAILABILITY_FAILURE = "Unable to fetch rooms availability";

    String RESPONSE_CODE_KEY = "responseCode";

    String RESPONSE_CODE_SUCCESS = "SUCCESS";

    String RESPONSE_CODE_FAILURE = "FAIL";

    String AVAILABLE_TIMESLOTS = "AvailableSlots";

    String RESERVATION_STATUS = "ReservationStatus";

    String RESERVATION_ID = "ReservationId";

    String TABLE_RESERVATION_SUCCESS = "Table Booking Successful";

    String TABLE_RESERVATION_FAILURE = "Unable To Book A Table";

    String DATE_VALUE = "dateValue";

    String FIRST_NAME = "firstName";

    String RESERVATION_TIME = "reservationTIme";

    String EMAIL_ID = "emailID";

    String PEOPLE_COUNT = "peopleCount";

    String CHECK_AVAILABILITY_FAILURE = "No Slots are Available";

    String CHECK_AVAILABILITY_TECHNICAL_FAILURE = "Technical Error Occured. Please Try again later";

    String CHECK_AVAILABILITY_SUCCESS = "Time Slots are Available";

    String CUSTOMER_ID = "CustomerID";

    String TABLE_CANCELLATION_SUCCESS = "Booking has been cancelled";

    String TABLE_CANCELLATION_FAILURE = "Table Cancellation was unsucessful";

    String RESTAURANT_ID = "restaurantID";

    String MODIFY_RESERVATION_SUCCESS = "Updated Successful";

    String MODIFY_RESERVATION_FAILURE = "Unable To Modify Reservation";

    boolean TABLE_RESERVATION_IS_FAILURE = false;

    String CHECK_AVAILABILITY_DESTINATION_SUCCESS = "Sucessfully Fetched the Price";

    String CHECK_AVAILABILITY_DESTINATION_FAILURE = "Unable to Fetch the Price";

    String EMAIL_SERVICE_SUCCESS = "Email has been sent Sucessfully";

    String EMAIL_SERVICE_FAILURE = "There was problem in sending in Email";

    String NEWSLETTER_SUBSCRIPTION_FAILURE = "Newsletter Subscription/Unsubscription Failed";
    
    String CALENDAR_AVAILABILITY_SUCCESS = "Calendar availability fetched successfully.";
    
    String ERROR_MESSAGE = "errorMessage";
    
    String CALENDAR_AVAILABILITY_FAILURE = "Unable to fetch availability.";

    String SYNXIS_MAINTAINENCE = "Servers are down due to maintenance.";
    
    String CALENDAR_PRICING_ERROR = "Unable to fetch calendar pricing.";
    
    String SYNXIS_ERROR_CODE = "errorCode";
}
