package com.ihcl.tajhotels.constants;

public interface HttpResponseParams {

    String HOTEL_CODE = "hotelCode";

    String NIGHTLY_RATES = "nightlyRates";

    String LOWEST_DISCOUNTED_PRICE = "lowestDiscountedPrice";

    String LOWEST_PRICE = "lowestPrice";

    String AVERAGE_TAX = "avgTax";

    String LOWEST_PRICE_TAX = "lowestPriceTax";

    String HOTEL_WARNINGS = "warning";

    String HOTEL_ERRORS = "error";

    String HOTEL_ROOMS_AVAILABILITY = "roomsAvailability";

    String ROOMS_AVAILABLE_UNITS = "availableUnits";

    String ROOMS_AVAILABLITY_EXPRESSION = "availabilityExpression";

    String ROOMS_AVAILABLE = "rooms";

    String RATE_PLAN_CODE = "ratePlanCode";

    String RATE_PLAN_TITLE = "ratePlanTitle";

    String RATE_PLAN_PRICE = "price";

    String RATE_PLAN_DISCOUNTED_PRICE = "discountedPrice";

    String RATE_PLAN_CANCELLATION_POLICY = "cancellationPolicy";

    String RATE_PLAN_DESCRIPTION = "rateDescription";

    String RATE_PLAN_AVAILABLITY_EXPRESSION = "availabilityExpression";

    String TAX = "tax";

    String DATE = "date";

    String PRICE_WITH_FEE_AND_TAX = "priceWithFeeAndTax";

    String DISCOUNTED_NIGHTLY_RATES = "discountedNightlyRates";

    String TAXES = "taxes";

    String TAX_NAME = "taxName";

    String TAX_DESCRIPTION = "taxDescription";

    String TAX_AMOUNT = "taxAmount";

    String RATE_PLAN_TOTAL_PRICE = "totalPrice";

    String RATE_PLAN_TOTAL_DISCOUNTED_PRICE = "totalDiscountedPrice";

    String RATE_PLAN_AMENITIES = "ratePlanAmenities";

    String RATE_PLAN_AMENITIY_NAME = "amenityName";
    
    String GUARANTEE_CODE = "guaranteeCode";

    String CREDIT_CARD_REQUIRED = "creditCardRequired";

    String GUARANTEE_DESCRIPTION = "guaranteeDescription";

    String GUARANTEE_AMOUNT = "guaranteeAmount";

    String GUARANTEE_AMOUNT_TAX_INCLUSIVE = "guaranteeAmountTaxInclusive";

    String GUARANTEE_PERCENTAGE = "guaranteePercentage";

    String CURRENCY_STRING = "currencyString";

    String RATE_PLAN_AMENITIY_ICON_PATH = "amenityIconPath";
}
