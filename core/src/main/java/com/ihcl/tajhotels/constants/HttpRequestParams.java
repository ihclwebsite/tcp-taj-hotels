package com.ihcl.tajhotels.constants;

public interface HttpRequestParams {

    String HOTEL_IDS = "hotelIds";

    String CHECK_IN_DATE = "checkInDate";

    String CHECK_OUT_DATE = "checkOutDate";

    String OCCUPANCY = "occupancy";

    String CURRENCY_SHORT_STRING = "shortCurrencyString";

    String CURRENCY_STRING = "currencyString";

    String HOTEL_ID_STRING = "hotelIds";

    String ROOM_DETAILS = "roomDetails";

    String ROOM_COUNT = "roomCount";

    String RATE_FILTERS = "rateFilters";

    String PROMO_CODE = "promoCode";

    String CORPORATE_CODE = "corporateCode";

    String RATE_CODES = "rateCodes";

    String DESTINATION_HOTEL_RATES = "/bin/hotel-rates/destination-hotel-rates";

    String WEB_SITE_ID = "websiteId";

    String DESTINATION_TAG = "destinationTag";
    
    String DURATION = "duration";
}
