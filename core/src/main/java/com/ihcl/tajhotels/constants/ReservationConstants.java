/**
 *
 */
package com.ihcl.tajhotels.constants;


/**
 * @author moonraft
 *
 */
public class ReservationConstants {

    public static final String EMAIL_ADDRESS = "emailAddress";

    public static final String CHECKED_ROOMS = "checkedRooms";

    public static final String HOTEL_ADDRESS = "hotelAddress";

    public static final String CONFIRMATION_NUMBER = "confirmationNo";

    public static final String CHAIN_CODE = "chainCode";

    public static final String CONTENT_TYPE = "application/json; charset=UTF-8";

    public static final String NT_UNSTRUCTURED = "nt:unstructured";

    public static final String TRUE = "true";

    public static final String ROOM_CODE = "roomCode";

    public static final String ROOM_CODE_NODE = "roomCodes";

    public static final String HOTEL_ID = "hotelID";

    public static final String REASONS = "reasons";

    public static final String REASON = "reason";

    public static final String PAID_ONLINE = "PayOnlineInfo";

    public static final String COLON = ":";

    public static final String COMMA = ",";

    public static final String GST_NUMBER = "guestGSTNumber";

    public static final String BEDTYPE = "bedType";

    public static final String ROOM_TITLE = "roomTitle";

    public static final String CDM_REFERENCE_ID = "cdmReferenceId";

    public static final String CANCEL_JSON = "cancelJson";

    public static final String BRAND_ID = "jcr:content/id";

    public static final String CONTENT_ROOT_PATH_ID = "contentRootPath";

    public static final String IS_AMA_BOOKNG = "jcr:content/amaBooking";

    public static final String CC_AVENUE_ACCESS_KEY = "ccAvenueAccessKey";

    public static final String CC_AVENUE_CANCEL_URL = "ccAvenueCancelUrl";

    public static final String CC_AVENUE_REDIRECT_URL = "ccAvenueRedirectUrl";

    public static final String CC_AVENUE_URL = "ccAvenueUrl";

    public static final String CC_AVENUE_WORKING_KEY = "ccAvenueWorkingKey";

    public static final String PAYMENT_CONFIRM_URL = "paymentConfirmUrl";

    public static final String PAYMENT_MERCHANT_ID = "paymentMerchantId";

    public static final String CONTENT_ROOT_PATH = "contentRootPath";

    public static final String FUNCTION_ID = "functionId";

    public static final String DEFAULT_ROOT_PATH = "/content/tajhotels";

    public static final String BOOKING_CCAVENUE = "bookingCCAvenue";

    public static final String TIC_CCAVENUE = "ticBookingCCAvenue";

    public static final String CORPORATE_BOOKING_CCAVENUE = "corporateBookingCCAvenue";

    public static final String GIFTCARD_CCAVENUE = "giftcardCCAvenue";

    public static final String GIFT_HAMPER_CCAVENUE = "giftHamperCCAvenue";

    public static final String EVENT_CCAVENUE = "eventCCAvenue";

    public static final String CONFIG_NODE = "jcr:content/configurations";

    public static final String GIFTCARD_CONFIG_EXCEPTION = "Configuration issue occured, While ordering Taj Experiences eCard. Please try after some time.";

    public static final String GIFT_HAMPER_CONFIG_EXCEPTION = "Configuration issue occured, While ordering Gift Hamper. Please try after some time.";

    public static final String EVENT_CONFIG_EXCEPTION = "Configuration issue occured, while booking Event. Please try after some time.";

    private ReservationConstants() {
    }

}
