/**
 *
 */
package com.ihcl.tajhotels.constants;


/**
 * <pre>
* SearchConstants Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :Feb 8, 2019
 * @ClassName : SearchConstants.java
 * @Description : com.ihcl.tajhotels.constants
 * @Modification Information
 *
 *               <pre>
*
*     since            author               description
*  ===========     ==============   =========================
*  Feb 8, 2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class SearchConstants {

    public static final String PATH = "path";

    public static final String TRUE = "true";

    public static final String PROPERTY = "property";

    public static final String PROPERTY_VALUE = "property.value";

    public static final String P_LIMIT = "p.limit";

    public static final String ORDER_BY = "orderby";

    public static final String JCR_CONTENT = "jcr:content";

    public static final String ORDER_BY_SORT = "orderby.sort";


}
