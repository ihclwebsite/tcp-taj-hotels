package com.ihcl.tajhotels.constants;

/**
 * Constants to map to the values that appear in crx.
 */
public interface CrxPropertyKeyConstants {


    String RATE_PLAN_CODES = "ratePlanCodes";

    String SERVICE_AMENITY_NAME = "name";

    String SERVICE_AMENITY_GROUP = "group";

    String SERVICE_AMENITY_IMAGE =  "image";

    String SERVICE_AMENITY_DESCRIPTION = "description";

    String SERVICE_AMENITY_PATH = "path";
}
