package com.ihcl.tajhotels.constants;

public interface BookingConstants {

    String HOTEL_LOCATION_ID = "hotelLocationId";

    String ROOM_STATUS_AVAILABLE = "AVAILABLE";

    String ROOM_STATUS_UNAVAILABLE = "UNAVAILABLE";

    String ROOM_STATUS_IGNORED = "IGNORED";

    String BOOKING_STATUS_ALL_ROOMS_UNAVAILABLE = "All Rooms are unavailable.";

    String BOOKING_STATUS_ALL_ROOMS_AVAILABLE_CONFIRMED = "All Rooms Successfully booked";

    String BOOKING_OBJECT_REQUEST = "bookingObjectRequest";

    String BOOKING_NO_AVAILABILITY = "No Availability found";

    String FORM_ERROR = "Form Error: ";

    String CONTENT_TYPE_TEXT = "text/plain";

    String CONTENT_TYPE_HTML = "text/html";

    String CHARACTER_ENCOADING = "UTF-8";

    String WEBSITE_ID = "websiteId";

    // String CCAVENUE_URL =
    // "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction";

    String CCAVENUE_URL_PARAM_ACCESS_CODE = "access_code";

    String CCAVENUE_URL_PARAM_ENC_REQ = "encRequest";

    String CCAVENUE_MERCHANT_ID_NAME = "merchant_id";

    String CCAVENUE_ORDER_ID_NAME = "order_id";

    String CCAVENUE_TRACKING_ID = "tracking_id";

    String CCAVENUE_CURRENCY_NAME = "currency";

    String CCAVENUE_AMOUNT_NAME = "amount";

    String CCAVENUE_REDIRECT_URL_NAME = "redirect_url";

    String CCAVENUE_CANCEL_NAME = "cancel_url";

    String CCAVENUE_LANGUAGE_NAME = "language";

    String CCAVENUE_BILLING_NAME = "billing_name";

    String CCAVENUE_BILLING_ADDRESS_NAME = "billing_address";

    String CCAVENUE_BILLING_CITY_NAME = "billing_city";

    String CCAVENUE_BILLING_STATE_NAME = "billing_state";

    String CCAVENUE_BILLING_ZIP_NAME = "billing_zip";

    String CCAVENUE_BILLING_COUNTRY_NAME = "billing_country";

    String CCAVENUE_BILLING_TEL_NAME = "billing_tel";

    String CCAVENUE_BILLING_EMAIL_NAME = "billing_email";

    String CCAVENUE_DELIVERY_NAME_NAME = "delivery_name";

    String CCAVENUE_DELIVERY_ADDRESS_NAME = "delivery_address";

    String CCAVENUE_DELIVERY_CITY_NAME = "delivery_city";

    String CCAVENUE_DELIVERY_STATE_NAME = "delivery_state";

    String CCAVENUE_DELIVERY_ZIP_NAME = "delivery_zip";

    String CCAVENUE_DELIVERY_COUNTRY_NAME = "delivery_country";

    String CCAVENUE_DELIVERY_TEL_NAME = "delivery_tel";

    String CCAVENUE_PROMO_CODE_NAME = "promo_code";

    String CCAVENUE_TID_NAME = "tid";

    String CCAVENUE_MERCH1_NAME = "merchant_param1";

    String CCAVENUE_MERCH2_NAME = "merchant_param2";

    String CCAVENUE_MERCH3_NAME = "merchant_param3";

    String CCAVENUE_MERCH4_NAME = "merchant_param4";

    String CCAVENUE_MERCH5_NAME = "merchant_param5";

    String CCAVENUE_SUB_ACCOUNT_ID = "sub_account_id";

    String CCAVENUE_PARAM_MERCHANT_ID = "83198";

    String CCAVENUE_PARAM_LANGUAGE = "EN";

    String CCAVENUE_PARAM_CURRENCY = "INR";

    String CCAVENUE_PARAM_DEFAULT_STRING = "NULL";

    String CCAVENUE_RESPONSE_ORDER_STATUS = "order_status";

    String CCAVENUE_RESPONSE_ORDER_STATUS_VALUE = "Success";

    String CCAVENUE_RESPONSE_PARAM_NAME = "encResp";

    String CCAVENUE_RESPONSE_PAY_MODE = "payment_mode";

    String CCAVENUE_RESPONSE_ID = "tracking_id";

    String CONFIRM_BOOKING_OBJECT_NAME = "bookingresponse";

    String PAGE_EXTENSION_HTML = ".html";

    String CONFIRMATION_STATUS_NAME = "status";

    String CONFIRM_BOOKING_ALL_BOOKED = "allbooked";

    String CONFIRM_BOOKING_PARTIALLY_BOOKED = "partiallybooked";

    String CONFIRM_BOOKING_FAILED = "bookingfailed";

    String PARAM_EQUAL_ASSIGN_OPERATOR = "=";

    String PARAM_AMPERSAND_OPERATOR = "&";

    String REPLACE_OPENING_CURLY_BRACE = "(";

    String JSON_OPENING_CURLY_BRACE_NOESCAPING = "{";

    String JSON_OPENING_CURLY_BRACE = "\\{";

    String REPLACE_CLOSING_CURLY_BRACE = ")";

    String JSON_CLOSING_CURLY_BRACE_NOESCAPING = "}";

    String JSON_CLOSING_CURLY_BRACE = "\\}";

    String REPLACE_COLON = "-";

    String JSON_COLON = ":";

    String REPLACE_QUOTES = "_";

    String JSON_QUOTES = "\"";

    String REPLACE_OPENING_SQ_BRACE = "#";

    String JSON_OPENING_SQ_BRACE = "\\[";

    String JSON_OPENING_SQ_BRACE_NOESCAPING = "[";

    String REPLACE_CLOSING_SQ_BRACE = "|";

    String JSON_CLOSING_SQ_BRACE = "\\]";

    String JSON_CLOSING_SQ_BRACE_NOESCAPING = "]";

    String FORM_BOOKING_REQUEST = "bookingDetailsRequest";

    String FORM_GUEST_NAME = "guestName";

    String FORM_HOTEL_ID = "hotelId";

    String FORM_HOTEL_CHAIN_CODE = "hotelChainCode";

    String FORM_NO_OF_CHILDREN = "noOfChilds";

    String FORM_NO_OF_ADULTS = "noOfAdults";

    String FORM_RATE_PLAN_CODE = "ratePlanCode";

    String FORM_ROOM_TYPE_CODE = "roomTypeCode";

    String FORM_CHECK_IN_DATE = "checkInDate";

    String FORM_CHECKOUT_DATE = "checkOutDate";

    String FORM_NO_OF_UNITS = "noOfUnits";

    String BOOKING_ID = "bookingId";

    String FORM_GUEST_COUNTRY = "guestCountry";

    String FORM_GUEST_EMAIL = "guestEmail";

    String FORM_MEMBERSHIP_NO = "guestMembershipNumber";

    String FORM_GUEST_PHONENO = "guestPhoneNumber";

    String FORM_GUEST_TITLE = "guestTitle";

    String FORM_SPECIAL_REQUESTS = "specialRequests";

    String FORM_BILER_NAME = "billerName";

    String FORM_BILLER_PHONE_NO = "billerPhoneNumber";

    String FORM_BILLER_EMAIL = "billerEmail";

    String FORM_BILLER_GST_NO = "billerGstNumber";

    String FORM_AUTO_ENROLL_TIC = "autoEnrollTic";

    String FORM_GDPR = "gdpr";

    String FORM_ARRIVAL_FLIGHT_NO = "arrivalFlightNo";

    String FORM_DEPARTURE_FLIGHT_NO = "depatureFlightNo";

    String FORM_ARRIVAL_FLIGHT_TIME = "flightArrivalTime";

    String FORM_DEPARTURE_FLIGHT_TIME = "flightDepatureTime";

    String FORM_PAY_AT_HOTEL = "payAtHotel";

    String FORM_NAME_ON_CARD = "nameOnCard";

    String FORM_CARD_NUMBER = "cardNumber";

    String FORM_EXPIRY_MONTH = "expiryMonth";

    String FORM_PAY_ONLIE_NOW = "payOnlineNow";

    String FORM_PAY_USING_GIFT_CARD = "payUsingGiftCard";

    String FORM_PAY_USING_CREDITCARD = "payUsingCerditCard";

    String FORM_PAYABLE_AMOUNT = "payableamount";

    String FORM_TIC_POINTS = "ticpoints";

    String FORM_CARD_TYPE = "cardType";

    String FORM_BILLER_DETAILS = "billerDetails";

    String FORM_GUEST_DETAILS = "guestDetails";

    String FORM_COMPANY_DETAILS = "companyDetails";

    String FORM_PAYMENT_DETAILS = "paymentsDetails";

    String FORM_ROOM_DETAILS = "roomDetails";

    String FORM_FLIGHT_DETAILS = "flightDetails";

    String LOGGED_IN_USER_DETAILS = "loggedInUser";

    String FORM_PROMO_CODE = "promotionCode";

    String FORM_COUPON_CODE = "appliedCoupon";

    String TOTAL_CART_PRICE = "totalCartPrice";

    String IGNORE_STATUS_SUCCESS = "Ignore Request Successfull";

    String IGNORE_STATUS_FAILED = "Ignore Request Failed";

    String BUNDLE_UNREACHABLE = "Booking bundle unreachable";

    String TRANSACTION_CANCELED = "Transaction_Cancelled";

    String TRANSACTION_COMPLETED = "Transaction_Completed";

    String BOOKING_OBJECT_STRING_CONVERSION_EXCEPTION = "Exception while converting BookingObject to String : {}";

    String BOOKING_IGNORE_REQUEST_SUCCESSFUL = "Ignore Request successful";

    String BOOKING_IGNORE_REQUEST_FAILED = "Ignore Request failed";

    String ITINERARY_NUMBER = "itineraryNumber";

    String GIFTCARD_EXCEPTION_MESSAGE = "Unable to Initialize Giftcard. Please try after sometime.";

    String GIFT_HAMPER_EXCEPTION_MESSAGE = "Unable to order Gift Hamper. Please try after sometime.";

    String EVENT_EXCEPTION_MESSAGE = "Unable to book Event. Please try after sometime.";

    String TIC_POST_PAYMENT_SERVLET_URL = "/bin/ticPostPaymentServlet";

    String STATUS = "status";

    String MESSAGE = "message";

    String GUEST_GST_NUMBER = "guestGSTNumber:";

    String CONFIG_EXCEPTION_MESSAGE = "While booking configaration issue occured. Please try after some time.";

    Object TIC_POINTS_TAPME = "TAPPMe";

    String CORPORATE_BOOKING = "corporateBooking";

    String CORPORATE_POST_PAYMENT_SERVLET_URL = "/bin/corporatePostPaymentServlet";
    
    String SINGLE_SIGN_ON_SERVLET_URL = "/bin/singleSignOnServlet";

    String SINGLE_SIGN_OUT_SERVLET_URL = "/bin/singleSignOutServlet";
    
    String ORDER_STATUS = "status";

	String OPEL_AMOUNT = "amount";

	String OPEL_ORDER_ID = "order_id";

	String OPEL_CUSTOMERID = "customer_id";

	String OPEL_CUSTOMER_PHONE_NO = "customer_phone";

	String OPEL_CUSTOMER_EMAIL = "customer_email";

	String OPEL_MERCHANT_ID = "merchant_id";

	String OPEL_RETUEN_URL = "return_url";

	String OPEL_TIMESTAMP = "timestamp";

	String OPEL_MERCHANT_PARAM1 = "metadata.CCAVENUE_V2:merchant_param1";
	
	String OPEL_MERCHANT_PARAM2 = "metadata.CCAVENUE_V2:merchant_param2";
	
	String OPEL_MERCHANT_PARAM3 = "metadata.CCAVENUE_V2:merchant_param3";
	
	String OPEL_MERCHANT_PARAM4 = "metadata.CCAVENUE_V2:merchant_param4";
	
	String OPEL_MERCHANT_PARAM5 = "metadata.CCAVENUE_V2:merchant_param5";

	String OPEL_SUB_ACCOUNT_ID = "metadata.CCAVENUE_V2:sub_account_id";

	String OPEL_UDF1 = "udf1";
	
	String OPEL_UDF2 = "udf2";
	
	String OPEL_UDF3 = "udf3";
	
	String OPEL_UDF4 = "udf4";
	
	String OPEL_UDF5 = "udf5";

	String OPEL_UDF6 = "udf6";

	String OPEL_UDF7 = "udf7";

	String OPEL_UDF8 = "udf8";

	String OPEL_UDF9 = "udf9";
	
	String OPEL_UDF10 = "udf10";

	String OPEL_OFFER_TITLE = "offer_applied";

	String OPEL_PAYMENT_METHOD_TYPE = "payment_method_type";

	String OPEL_PAYMENT_GETWAY_ID = "gateway_id";

	String OPEL_EPICURE_MERCHANT_ID = "epicure";

	String OPEL_PRODUCT_ID = "product_id";
}
