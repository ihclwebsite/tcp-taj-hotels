package com.ihcl.core.transport.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.AgentConfig;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationResult;
import com.day.cq.replication.ReplicationTransaction;
import com.day.cq.replication.TransportContext;
import com.day.cq.replication.TransportHandler;
import com.ihcl.core.services.config.CustomTransportHandlerConfigurationService;
import com.ihcl.core.util.URLMapUtil;


@Component(service = TransportHandler.class,
        name = "Custom Transport Handler",
        immediate = true,
        property = { "name=label", "value=Dispatcher Purge Agent" })
public class DispatcherTransportHandler implements TransportHandler {

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    CustomTransportHandlerConfigurationService transportHandlerConfig;

    ResourceResolver resourceResolver;

    private static final Logger LOG = LoggerFactory.getLogger(DispatcherTransportHandler.class);

    private static final List<String> domainList = new ArrayList<String>(Arrays.asList("www.tajhotels.com",
            "www.vivantahotels.com", "www.seleqtionshotels.com", "www.benareshotelslimited.com"));

    private static final String PRIMARY_TYPE_PAGE = "cq:Page";

    private static final String PRIMARY_TYPE_COMPONENT = "cq:Component";

    private static final String HOST = "Host";

    private static final String CQ_HANDLE = "CQ-Handle";

    private static final String CQ_ACTION = "CQ-Action";

    private static final String CQ_PATH = "CQ-Path";

    private static final String APPS = "/apps";

    private static final String ETC_CLIENTLIBS = "/etc.clientlibs";

    @Override
    public boolean canHandle(AgentConfig config) {
        String uri = config == null ? null : config.getTransportURI();
        LOG.info("Inside canHandle mehtod " + config.getTransportURI());
        LOG.info("CustomProtocol for canHandle : " + transportHandlerConfig.getCusotmProtocol());
        return (uri != null) && ((uri.startsWith(transportHandlerConfig.getCusotmProtocol() + "://")));
    }

    @Override
    public ReplicationResult deliver(TransportContext ctx, ReplicationTransaction tx) throws ReplicationException {
        String methodName = "deliver";
        LOG.info("MethodEntry :" + methodName);
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.info("Obtained resourceResolver as " + resourceResolver);
            final ReplicationActionType replicationType = tx.getAction().getType();
            LOG.info(methodName + " -> ReplicationActionType " + replicationType);
            if (replicationType == ReplicationActionType.TEST) {
                return doTest(ctx, tx);
            } else if (replicationType == ReplicationActionType.ACTIVATE
                    || replicationType == ReplicationActionType.DEACTIVATE || replicationType == ReplicationActionType.DELETE) {

                LOG.info("CQ-Action || replication Type : " + replicationType);
                LOG.info("Transport URI : " + ctx.getConfig().getTransportURI());
                LOG.info("CQ-Path : " + tx.getAction().getPath());
                LOG.info("calling the transport URI");
                return doActivate(ctx, tx);
            } else {
                throw new ReplicationException("Replication action type " + replicationType + " not supported.");
            }
        } catch (ClientProtocolException e) {
            LOG.error("Exception occured while sending the Http request for flushing  " + e.getMessage());
        } catch (IOException e) {
            LOG.error("IO Exception occured while sending the Http request for flushing  " + e.getMessage());
        } catch (LoginException e) {
            LOG.error("Exception occured while obtaining ServiceResourceResolver from resourceResolverFactory "
                    + e.getMessage());
        } finally {
            if (resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }
        return new ReplicationResult(false, 0, "Replication failed");
    }


    private ReplicationResult doActivate(TransportContext ctx, ReplicationTransaction tx)
            throws IOException {

        String methodName = "doActivate";
        LOG.info("MethodEntry => " + methodName);
        LOG.info("Transport URI " + ctx.getConfig().getTransportURI());
        final String transportURI = ctx.getConfig().getTransportURI()
                .replaceAll(transportHandlerConfig.getCusotmProtocol(), "http");
        LOG.info("Updated TransportURI => " + transportURI);
        String unmappedPath = getPagePathOfNode(tx.getAction().getPath());
        final String mappedPath = getMappedPath(unmappedPath);
        if (null != mappedPath) {
            LOG.info("Reverse Mapped Path : " + mappedPath);
        } else {
            LOG.info("MappedPath is Null, returning Replication Failure message");
            return new ReplicationResult(false, 0, "Replication failed");
        }
        String cqAction = tx.getAction().getType().toString();
        if (validateToFlushFromEveryDomain(tx.getAction().getPath())) {
            unmappedPath = refactorIfAppsPath(tx.getAction().getPath());
            LOG.info("Flushing from all domains");
            for (String domainHost : domainList) {
                LOG.info("Attempting to Flush the path " + unmappedPath + "from Domain -> " + domainHost);
                ReplicationResult result = initiateInvalidateFlushHTTPcall(transportURI, cqAction, domainHost,
                        unmappedPath);
            }
            return ReplicationResult.OK;
        } else {
            String hostAddress = getHostAddressFromURL(tx.getAction().getPath());
            if (null != hostAddress) {
                LOG.info("Obtained HostAddress as " + hostAddress);
                return initiateInvalidateFlushHTTPcall(transportURI, cqAction, hostAddress, mappedPath);
            } else {
                LOG.info("HostAddress is Null, returning Replication Failure message");
                return new ReplicationResult(false, 0, "Replication failed");
            }
        }
    }

    private ReplicationResult doTest(TransportContext ctx, ReplicationTransaction tx) throws ReplicationException {
        LOG.info("Inside the doTest Method of the Dispatcher Transport handler");
        LOG.info("Obtained CustomTransportHandlerConfigurationService as " + transportHandlerConfig);
        LOG.info("Custom Protocol defined by user " + transportHandlerConfig.getCusotmProtocol());
        LOG.info("Transport URI : " + ctx.getConfig().getTransportURI());
        LOG.info("CQ-Path : " + tx.getAction().getPath());
        LOG.info("Reverse Mapped Path " + getMappedPath(tx.getAction().getPath()));
        LOG.info("Test completed");
        return ReplicationResult.OK;
    }

    private ReplicationResult initiateInvalidateFlushHTTPcall(String transportURI, String cqAction, String hostAddress,
            String flushPath) throws IOException {

        String methodName = "initiateInvalidateFlushHTTPcall";
        LOG.info("MethodEntry => " + methodName);
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(transportURI);
        post.setHeader(CQ_HANDLE, flushPath);
        post.setHeader(CQ_PATH, flushPath);
        post.setHeader(CQ_ACTION, cqAction);
        post.setHeader(HOST, hostAddress);
        client.execute(post);
        CloseableHttpResponse httpResponse = client.execute(post);
        client.close();
        if (httpResponse != null) {
            final int statusCode = httpResponse.getStatusLine().getStatusCode();
            LOG.info("Response : " + httpResponse.toString());
            if (statusCode == HttpStatus.SC_CREATED) {
                LOG.info(methodName + " exited with HTTP Status Code " + HttpStatus.SC_CREATED);
                LOG.info("MethodExit => " + methodName);
                return ReplicationResult.OK;
            }
            LOG.info(methodName + " exited with HTTP Status Code " + statusCode);
            LOG.info("MethodExit => " + methodName);
            return ReplicationResult.OK;
        }
        LOG.info("HTTP Response is Null");
        LOG.info("MethodExit => " + methodName);
        return new ReplicationResult(false, 0, "Replication failed");
    }

    private boolean validateToFlushFromEveryDomain(String path) {

        String updatedPath = getPagePathOfNode(path);
        if (updatedPath.equalsIgnoreCase(path)) {
            Resource pathResource = resourceResolver.getResource(updatedPath);
            if (null != pathResource) {
                if (pathResource.getResourceType().equals(PRIMARY_TYPE_PAGE)) {
                    LOG.info("Found page Path, now this path would be resolved");
                    return false;
                }
                LOG.info("This is not a page path, hence flushing from all domains");
            }
            return true;
        }
        LOG.info("Its a page path, this path would be resolved and flushed from the domain ,Path :> " + updatedPath);
        return false;
    }

    private String getPagePathOfNode(String path) {
        String pathBeforeJcrContent = path.split("/jcr:content")[0];
        Resource pathResource = resourceResolver.getResource(pathBeforeJcrContent);
        if (null != pathResource) {
            if (pathResource.getResourceType().equals(PRIMARY_TYPE_PAGE)) {
                LOG.info("Found page Path, now this path would be resolved");
                return pathBeforeJcrContent;
            } else {
                LOG.info("Not a page returning the path");
                return pathBeforeJcrContent;
            }
        }
        LOG.info("pathResource is null ,returning the default path");
        return path;
    }

    private String refactorIfAppsPath(String path) {
        String updatedPath = path;
        if (path.startsWith(APPS)) {
            updatedPath = refactorToComponentPath(path);
            LOG.info("Refactored Component Path : " + updatedPath);
            updatedPath = updatedPath.replaceAll(APPS, ETC_CLIENTLIBS);
            LOG.info("Updated Refactored Path : " + updatedPath);
            return updatedPath;
        }
        return path;
    }

    private String refactorToComponentPath(String path) {
        Resource componentResource = resourceResolver.getResource(path);
        if (null != componentResource) {
            if (componentResource.getResourceType().equals(PRIMARY_TYPE_COMPONENT)) {
                return path;
            }
            LOG.info("Couldnt find the component , hence calling the same function with path "
                    + path.substring(0, path.lastIndexOf("/")));
            return refactorToComponentPath(path.substring(0, path.lastIndexOf("/")));
        }
        return path;
    }

    private String getMappedPath(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return URLMapUtil.getPathFromURL(resolvedURL);
        }
        return url;
    }

    private String getHostAddressFromURL(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return URLMapUtil.getHostAddressFromURL(resolvedURL);
        }
        return url;
    }
}
