package com.ihcl.core.event.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChange.ChangeType;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.ihcl.core.services.config.TajApiKeyConfigurationService;

@Component(service = ResourceChangeListener.class,
        property = {
                // filter the notifications by path in the repo. Can be array and supports globs
                ResourceChangeListener.PATHS + "=" + "/content/tajhotels/en-in/our-hotels",
                // The type of change you want to listen to.
                // Possible values at
                // https://sling.apache.org/apidocs/sling9/org/apache/sling/api/resource/observation/ResourceChange.ChangeType.html.
                ResourceChangeListener.CHANGES + "=" + "ADDED", ResourceChangeListener.CHANGES + "=" + "REMOVED",
                ResourceChangeListener.CHANGES + "=" + "CHANGED"

        // PS: If you want to declare multiple values for a prop, you repeat it in OSGI R6 annotations.
        // https://stackoverflow.com/questions/41243873/osgi-r6-service-component-annotations-property-list#answer-41248826
        })
public class HotelDesintationNameChangeListener implements ResourceChangeListener {

    @Reference
    private ResourceResolverFactory resolverFactory;


    @Reference
    private SlingSettingsService settings;

    @Reference
    QueryBuilder builder;

    @Reference
    TajApiKeyConfigurationService etcMapKey;

    public static final Logger LOG = LoggerFactory.getLogger(HotelDesintationNameChangeListener.class);

    @Override
    public void onChange(List<ResourceChange> resourceChangeList) {
        for (ResourceChange resourceChange : resourceChangeList) {
            String resourcePath = resourceChange.getPath();
            LOG.info(resourceChange.getType() + ":::::::::" + resourcePath);
            try {
                String[] pathContents = resourcePath.split("/");
                String brand = pathContents[6];
                String hotelName = pathContents[7];
                LOG.info("brand of the hotel =" + brand);
                LOG.info("Name of the hotel =" + hotelName);
                LOG.info("maplocation from configured key " + etcMapKey.getEtcMapLocation());
                String mapLocation = etcMapKey.getEtcMapLocation();
                if (resourceChange.getType() == ChangeType.REMOVED) {
                    List<Hit> nodesTobeReomved = isMapPresent(mapLocation + "/" + brand, hotelName);
                    for (Hit hit : nodesTobeReomved) {
                        LOG.trace("inside remove section =" + hit.getPath());
                        hit.getNode().remove();
                        hit.getNode().getSession().save();
                    }
                }
                ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
                Resource resource = resourceResolver.getResource(resourcePath);
                if (resource != null) {
                    LOG.info("resource deatails " + resource);
                    Resource hotelResource = resource.getParent();
                    String hotelPath = hotelResource.getPath();
                    LOG.info("hotel path =" + hotelPath);
                    LOG.info("resource type of node =" + resource.getResourceType());
                    Node node = resource.adaptTo(Node.class);
                    if (node.hasProperty("sling:resourceType")) {
                        String propertyValue = node.getProperty("sling:resourceType").getValue().getString();
                        if (propertyValue != null && propertyValue
                                .equalsIgnoreCase("tajhotels/components/structure/hotels-landing-page")) {
                            LOG.info("resourve type of node is =" + propertyValue);
                            LOG.info("location of map=" + mapLocation);
                            List<Hit> pathsOfNodes = isMapPresent(mapLocation + "/" + brand, hotelName);
                            if (!pathsOfNodes.isEmpty()) {
                                updateMap(pathsOfNodes, hotelPath);
                            } else {
                                addMap(mapLocation, hotelPath);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                LOG.error("Error/Exception is thrown while reading Result Set" + e.toString(), e);
            }

        }
    }

    List<Hit> isMapPresent(String mapPath, String nodeName) throws RepositoryException, LoginException {
        LOG.info("Entering isMapPresent method");
        LOG.info("mapPath for queybuilder " + mapPath);
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", mapPath);// replace with mapPath
        queryMap.put("group.1_nodename", nodeName);
        queryMap.put("group.2_nodename", nodeName + "-" + "reverse");
        queryMap.put("group.p.or", "true");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        List<String> paths = new ArrayList<String>();
        for (Hit hit : query.getResult().getHits()) {
            paths.add(hit.getPath().trim());
            LOG.info("path of map where this old nodename is present " + hit.getPath().trim());
        }
        queryMap.clear();
        LOG.info("Exiting isMapPresent method");
        return query.getResult().getHits();

    }

    public void updateMap(List<Hit> hits, String hotelPath) throws RepositoryException {
        LOG.info("Entering updateMap method");
        for (Hit hit : hits) {
            String pagePath = hit.getPath();
            ValueMap pageProperties = hit.getProperties();
            String nodeName = hit.getNode().getName();
            String redirectPath = String.valueOf(pageProperties.get("sling:internalRedirect"));
            String newRedirectPath = "";
            if (redirectPath.indexOf(hotelPath) == -1) {
                LOG.info("node name inside updateMap method " + nodeName);
                if (nodeName.contains("reverse")) {
                    newRedirectPath = hotelPath + "(.*)" + "." + "html";
                } else {
                    newRedirectPath = hotelPath + "$1" + "." + "html";
                }
                hit.getNode().setProperty("sling:internalRedirect", newRedirectPath);
                hit.getNode().getSession().save();
            }

        }
        LOG.info("Exiting updateMap method");
    }

    public void addMap(String mapLocation, String path) throws LoginException {
        LOG.info("Entering addMap method");
        String[] pathContents;
        String brandName = "";
        String hotelName = "";
        String slingMatch = "";
        String slngInterRedirect = "";
        String reverseNodeName = "";
        String rvsSlingMatch = "";
        String rvsSlngRedirect = "";
        if (path != null) {
            pathContents = path.split("/");
            brandName = pathContents[6];
            hotelName = pathContents[7];
            slingMatch = pathContents[7] + "(.*)";
            slngInterRedirect = path + "$1" + "." + "html";
            LOG.info("inside addMap brandName =" + brandName + " and hotelName =" + hotelName);
            try {

                // ResourceResolver resourceResolver = resolverFactory.getResourceResolver(null);
                // session = resourceResolver.adaptTo(Session.class);
                ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
                Session session = resourceResolver.adaptTo(Session.class);
                mapLocation = mapLocation + "/" + brandName;
                mapLocation = mapLocation.replaceFirst("/", "");
                LOG.info("after removing / in maplocation " + mapLocation);
                Node brandNode = session.getRootNode().getNode(mapLocation);
                LOG.info("node name " + brandNode.getName());
                // Node brandNode = res.adaptTo(Node.class);
                // forward mapping

                Node newNode = brandNode.addNode(hotelName, "sling:Mapping");
                // newNode.setProperty("jcr:primaryType", "sling:Mapping");
                newNode.setProperty("sling:match", slingMatch);
                newNode.setProperty("sling:internalRedirect", slngInterRedirect);
                newNode.getSession().save();

                // reverse mapping
                reverseNodeName = hotelName + "-" + "reverse";
                rvsSlingMatch = hotelName + "$1/";
                rvsSlngRedirect = path + "(.*)" + "." + "html";
                Node reverseNode = brandNode.addNode(reverseNodeName, "sling:Mapping");
                // reverseNode.setProperty("jcr:primaryType", "sling:Mapping");
                reverseNode.setProperty("sling:match", rvsSlingMatch);
                reverseNode.setProperty("sling:internalRedirect", rvsSlngRedirect);
                reverseNode.getSession().save();

            } catch (Exception e) {
                LOG.error("Error/Exception is thrown  " + e.toString(), e);
            }

        }
        LOG.info("Exiting addMap method");

    }

}
