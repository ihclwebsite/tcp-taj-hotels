package com.ihcl.core.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.NonExistingResource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.EngineConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;

@Component(service = Filter.class,
        property = { Constants.SERVICE_DESCRIPTION + "=Get Offer pages URL",
                EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
                Constants.SERVICE_RANKING + ":Integer=-800"

        })

public class OfferFilters implements Filter {

    Logger log = LoggerFactory.getLogger(OfferFilters.class);

    @Reference
    SlingSettingsService slingSettingsService;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    TajApiKeyConfigurationService config;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        // Only run the filter if it is publish box
        if (slingSettingsService.getRunModes().contains("publish")) {
            String[] urlsMatch = config.getUrlsToBeRedirected();
            final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
            final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;

            String path = slingRequest.getRequestURI();
            StringBuffer url = slingRequest.getRequestURL();
            log.info("REQUEST URL" + url);
            // checking if this is a valid offer page and isExist otherwise internal
            // redirect to FALLBACK_PATH.
            for (String match : urlsMatch) {
                ResourceResolver resourceResolver;
                try {
                    resourceResolver = resolverFactory.getServiceResourceResolver(null);
                    log.trace("value from configMgr =" + match);
                    log.trace(
                            "resource from url " + resourceResolver.resolve(slingRequest, slingRequest.getPathInfo()));
                    if (path.matches(match) && resourceResolver.resolve(slingRequest,
                            slingRequest.getPathInfo()) instanceof NonExistingResource) {
                        log.trace("url content being matched =" + path);
                        String finalRedirectPath = path.substring(0, path.lastIndexOf("/") + 1);
                        log.info("final redirected url  =" + finalRedirectPath);
                        // Setting status to 301 -Moved Permanently
                        slingResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
                        slingResponse.setHeader("Location", "");
                        slingResponse.sendRedirect(finalRedirectPath);
                    }
                } catch (LoginException | IllegalStateException e) {
                    log.error("Failed to create resource resolver" + e.getMessage());
                    chain.doFilter(request, response);
                }
            }
            chain.doFilter(request, response);
        } else {
            log.info("THIS IS AUTHOR");
            // To proceed with the rest of the Filter chain
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
