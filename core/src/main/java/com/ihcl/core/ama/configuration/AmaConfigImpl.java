/**
 *
 */
package com.ihcl.core.ama.configuration;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Nutan
 *
 */

@Component(immediate = true,
        service = IAmaConfiguration.class)
@Designate(ocd = AmaConfigParams.class)
public class AmaConfigImpl implements IAmaConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(AmaConfigImpl.class);

    private String activityPath;
    private String emailToAddress;
    private String emailSubject;
    private String emailBodyTemplate;


    @Activate
    @Modified
    private void activate(AmaConfigParams config) {
        String methodName = "activate";
        LOG.debug("Method Entry: " + methodName);
        activityPath = config.activityPath();
        LOG.debug("activityPath: " + activityPath);
        
        emailToAddress = config.emailToAddress();
        emailSubject = config.emailSubject();
        emailBodyTemplate = config.emailBodyTemplate();
        LOG.debug("Method Exit: " + methodName);

    }

    @Override
    public String activityPath() {
        return activityPath;
    }

   

	@Override
	public String getEmailToAddress() {
		return emailToAddress;
	}

	@Override
	public String getEmailSubject() {
		return emailSubject;
	}

	@Override
	public String getEmailBodyTemplate() {
		return emailBodyTemplate;
	}
}
