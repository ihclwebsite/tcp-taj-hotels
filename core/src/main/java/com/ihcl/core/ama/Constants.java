/**
 *
 */
package com.ihcl.core.ama;


/**
 * @author Nutan
 *
 */
public class Constants {

    public interface activity {

        String TITLE = "title";

        String DESCRIPTION = "description";

        String CQ_TAGS = "cq:tags";

        String IMAGE = "image";

        String VIEW_DETAIL = "buttonLabel";

        String VIEW_URL = "buttonUrl";

    }

}
