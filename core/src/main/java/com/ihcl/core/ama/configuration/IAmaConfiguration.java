/**
 *
 */
package com.ihcl.core.ama.configuration;


/**
 * @author Nutan
 *
 */
public interface IAmaConfiguration {

    String activityPath();
    

    String getEmailToAddress();
    
    String getEmailSubject();
    
    String getEmailBodyTemplate();
}
