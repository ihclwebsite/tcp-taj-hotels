/**
 *
 */
package com.ihcl.core.ama;

import java.util.List;

/**
 * @author Nutan
 *
 */
public class ActivityBean {

    String image;

    String title;

    String description;

    String viewDetails;

    List<String> tags;

    String url;


    public String getUrl() {
        return url;
    }


    public void setUrl(String url) {
        this.url = url;
    }


    public String getImage() {
        return image;
    }


    public void setImage(String image) {
        this.image = image;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getViewDetails() {
        return viewDetails;
    }


    public void setViewDetails(String viewDetails) {
        this.viewDetails = viewDetails;
    }


    public List<String> getTags() {
        return tags;
    }

    public ActivityBean(String image, String title, String description, String viewDetails, List<String> tags,
            String url) {
        super();
        this.image = image;
        this.title = title;
        this.description = description;
        this.viewDetails = viewDetails;
        this.tags = tags;
        this.url = url;
    }

}
