/**
 *
 */
package com.ihcl.core.ama.configuration;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Nutan
 *
 */

@ObjectClassDefinition(name = "Ama Configuration Parameters",
        description = "")
public @interface AmaConfigParams {

    @AttributeDefinition(name = "Activity Content Path",
            description = "")
    String activityPath() default "/content/ama/en-in";

    @AttributeDefinition(name = "To address", description = "When user will fill form to become partner with ama, all form data will mailed to this mail id.")
    String emailToAddress() default "anil.yadav@moonraft.com";

    @AttributeDefinition(name = "Subject of Email", description = "Subject of Email")
    String emailSubject() default "Partner With amã Stays & Trails Application";
    
    @AttributeDefinition(name = "Email body template", description = "Email body template for Partner With AMA")
    String emailBodyTemplate() default "template";

}
