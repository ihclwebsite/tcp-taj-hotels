/**
 *
 */
package com.ihcl.core.jsonresponses.sealapi;


/**
 * @author moonraft Vijay Pal
 *
 *
 */
public class Response {


    public String name;

    private float score;

    public Integer sources_count;

    public Integer reviews_count;

    public String ty_id;

    public String score_description;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public float getScore() {
        score = score / 20;
        return score;
    }


    public void setScore(float score) {
        this.score = score;
    }


    public Integer getSources_count() {
        return sources_count;
    }


    public void setSources_count(Integer sources_count) {
        this.sources_count = sources_count;
    }


    public Integer getReviews_count() {
        return reviews_count;
    }


    public void setReviews_count(Integer reviews_count) {
        this.reviews_count = reviews_count;
    }


    public String getTy_id() {
        return ty_id;
    }


    public void setTy_id(String ty_id) {
        this.ty_id = ty_id;
    }


    public String getScore_description() {
        return score_description;
    }


    public void setScore_description(String score_description) {
        this.score_description = score_description;
    }


}
