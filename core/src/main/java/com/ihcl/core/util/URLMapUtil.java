package com.ihcl.core.util;

import com.google.common.base.Strings;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

public class URLMapUtil {

	public static String appendHTMLExtension(String path) {
		return path.concat(".html");

	}

	public static String getPathFromURL(String url) {
		try {
			URL pathURL = new URL(url);
			return pathURL.getPath();
		} catch (MalformedURLException e) {
			return url;
		}
	}

	/**
	 * Method takes a query string and converts it into a key value map
	 * param1=value1&param2=&param3=value3&param3
	 * => {param1=["value1"], param2=[null], param3=["value3", null]}
	 *
	 * Taken from : https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
	 *
	 * @param queryString
	 * @return Map
	 */
	public Map<String, List<String>> splitQuery(String queryString) {
		if (Strings.isNullOrEmpty(queryString)) {
			return Collections.emptyMap();
		}
		return Arrays.stream(queryString.split("&"))
				.map(this::splitQueryParameter)
				.collect(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry::getKey, LinkedHashMap::new, mapping(Map.Entry::getValue, toList())));
	}

	private AbstractMap.SimpleImmutableEntry<String, String> splitQueryParameter(String it) {
		final int idx = it.indexOf("=");
		final String key = idx > 0 ? it.substring(0, idx) : it;
		final String value = idx > 0 && it.length() > idx + 1 ? it.substring(idx + 1) : null;
		return new AbstractMap.SimpleImmutableEntry<>(key, value);
	}

	public static String getHostAddressFromURL(String url) {
		try {
			URL pathURL = new URL(url);
			return pathURL.getHost();
		} catch (MalformedURLException e) {
			return url;
		}
	}
}
