package com.ihcl.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidateRequestUrlInServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ValidateRequestUrlInServlet.class);

    Boolean isValid = Boolean.TRUE;


    public boolean validateUrl(String url) {
        url = url.replaceAll("%3C", "<");
        url = url.replaceAll("%3E", ">");
        Pattern scriptPattern = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
        Matcher m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a javascript: tag in it");
            isValid = Boolean.FALSE;
        }
        scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a script tag in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a script tag in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a script tag in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a script tag in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("eval\\((.*?)\\)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a eval(...) in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("expression\\((.*?)\\)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a expression(...) in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a vbscript: in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a onload= in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("<html>(.*?)</html>", Pattern.CASE_INSENSITIVE);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a html tag in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("</html>", Pattern.CASE_INSENSITIVE);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a html in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("<html(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a html in it");
            isValid = Boolean.FALSE;
        }

        scriptPattern = Pattern.compile("<img(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        m = scriptPattern.matcher(url);
        if (m.find()) {
            LOG.trace("This request has a image tag in it");
            isValid = Boolean.FALSE;
        }

        return isValid;
    }

    public String getRequestUrl(SlingHttpServletRequest httpRequest) {
        return httpRequest.getScheme() + "://" + httpRequest.getServerName()
                + ("http".equals(httpRequest.getScheme()) && httpRequest.getServerPort() == 80
                        || "https".equals(httpRequest.getScheme()) && httpRequest.getServerPort() == 443 ? ""
                                : ":" + httpRequest.getServerPort())
                + httpRequest.getRequestURI()
                + (httpRequest.getQueryString() != null ? "?" + httpRequest.getQueryString() : "");
    }

    public String getRequestMethod(SlingHttpServletRequest httpRequest) {
        return httpRequest.getMethod();
    }
}
