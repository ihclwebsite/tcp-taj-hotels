/**
 *
 */
package com.ihcl.core.util;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;

import com.ihcl.integration.giftcard.config.ConfigurationService;

/**
 * @author Vijay Chikkani
 *
 */
public interface IGiftcardBatchNumberFetcher {

    String getCurrentBatchNumber(SlingHttpServletRequest request, ConfigurationService configurationService)
            throws RepositoryException;

    boolean setCurrentBatchNumber(SlingHttpServletRequest request, ConfigurationService configurationService,
            String newBatchNumber) throws RepositoryException, LoginException, PersistenceException;
}
