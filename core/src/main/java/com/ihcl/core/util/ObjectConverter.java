/**
 *
 */
package com.ihcl.core.util;

import java.io.IOException;
import java.util.StringTokenizer;

import com.ihcl.integration.dto.details.Flight;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author moonraft
 *
 */
public class ObjectConverter {

    private ObjectMapper objectMapper;

    private static final Logger LOG = LoggerFactory.getLogger(ObjectConverter.class);

    public String setFlightDetails(Flight flight)
            throws IOException {

        LOG.info("Inside method setFlightDetails()");
        objectMapper = new ObjectMapper();
        LOG.debug("Flight object received :" + objectMapper.writeValueAsString(flight));

        String flightDetailsString = null;
        flightDetailsString = "Arrival Flight:" + flight.getArrivalAirportName() + "#"
                + flight.getArrivalFlightNo() + "#" + flight.getFlightArrivalTime();

        LOG.debug("Flight string set :" + flightDetailsString);
        LOG.info("Leaving mehod setFlightDetails()");

        return flightDetailsString;
    }

    public Flight getFlightDetails(String flightComments)
            throws IOException {

        LOG.info("Inside method getFlightDetails()");
        LOG.debug("Flight string received :" + flightComments);

        Flight flight = new Flight();
        objectMapper = new ObjectMapper();
        String[] details = new String[3];
        String flightToken = null, rqToken = null;
        int i = 0;
        StringTokenizer requestToken = new StringTokenizer(flightComments, ",");
        while (requestToken.hasMoreTokens()) {
            rqToken = requestToken.nextToken();
            if (rqToken.contains("Arrival Flight:")) {
                flightToken = rqToken;
            }
        }
        if (flightToken != null) {
            LOG.debug("Flight Inforamtion found: " + flightToken);
            flightToken = flightToken.replace("Arrival Flight:", "");
            StringTokenizer token = new StringTokenizer(flightToken, "#");
            LOG.debug("Flight final token string: " + flightToken);
            while (token.hasMoreTokens()) {
                details[i] = token.nextToken();
                i++;
            }
            flight.setArrivalAirportName(details[0]);
            flight.setArrivalFlightNo(details[1]);
            flight.setFlightArrivalTime(details[2]);
        }

        LOG.debug("Flight Object set :" + objectMapper.writeValueAsString(flight));
        LOG.info("Leaving method getFlightDetails()");

        return flight;

    }
}
