/**
 *
 */
package com.ihcl.core.util.services;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

/**
 * @author Srikanta.moonraft
 *
 */
public interface TajCommonsFetchResource {

    /**
     * @param currentResource
     * @param parentResourceName
     * @return
     */
    Resource getParentUpto(Resource currentResource, String parentResourceName);

    /**
     * @param cuurentResource
     * @param childResourceName
     * @return
     */
    Resource getChildUpto(Resource cuurentResource, String childResourceName);

    /**
     * @param currentResource
     * @param childResourceName
     * @return
     */
    ValueMap getChildAsValueMapUpto(Resource currentResource, String childResourceName);

    /**
     * @param currentResource
     * @param childResourceName
     * @return
     */
    ValueMap getChildTypeAsValueMapUpto(Resource currentResource, String childResourceName, String childResourceType);


}

