/**
 *
 */
package com.ihcl.core.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.HotelDetailsFetcher;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.integration.dto.details.Payments;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.tajhotels.constants.ReservationConstants;

/**
 * <pre>
* ReservationServletUtil.java Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :Dec 19, 2018
 * @ClassName : ReservationServletUtil.java
 * @Description : tajhotels-core -ReservationServletUtil.java
 * @Modification Information
 *
 *               <pre>
*
*     since            author               description
*  ===========     ==============   =========================
*  Dec 19, 2018     Neha Priyanka            Create
 *
 *               </pre>
 */
public class ReservationServletUtil {


    private static final Logger LOG = LoggerFactory.getLogger(ReservationServletUtil.class);


    /**
     * Sets the GST number to booking object.
     *
     * @param specialRequest
     *            the new GST number to booking object
     * @param bookingObject
     */
    public static void setGSTNumberToBookingObject(String specialRequest, BookingObject bookingObject) {
        String gstNumber = "";
        String specialRequestsResponse = "";
        if (StringUtils.isNotBlank(specialRequest) && specialRequest.contains(ReservationConstants.GST_NUMBER)) {
            String[] specialRequestArray = specialRequest
                    .split(ReservationConstants.COMMA + ReservationConstants.GST_NUMBER + ReservationConstants.COLON);
            gstNumber = specialRequestArray[1];
            specialRequestsResponse = specialRequestArray[0];
            bookingObject.getGuest().setGstNumber(gstNumber);
            bookingObject.getGuest().setSpecialRequests(specialRequestsResponse);
        }
    }

    /**
     * Gets the payment details from special request.
     *
     * @param specialRequest
     *            the special request
     * @return the payment details from special request
     */
    public static Payments getPaymentDetailsFromSpecialRequest(String specialRequest, BookingObject bookingObject) {
        Payments paymentObj = new Payments();
        if (StringUtils.isNotBlank(specialRequest) && specialRequest.contains(ReservationConstants.PAID_ONLINE)) {
            String[] specialRequestArray = specialRequest
                    .split(ReservationConstants.COMMA + ReservationConstants.PAID_ONLINE + ReservationConstants.COLON);
            specialRequest = specialRequestArray[0];
            paymentObj.setPayAtHotel(false);
            paymentObj.setPayOnlineNow(true);
            bookingObject.getGuest().setSpecialRequests(specialRequest);
        } else {
            paymentObj.setPayAtHotel(true);
            paymentObj.setPayOnlineNow(false);
        }
        paymentObj.setCardNumber("");
        paymentObj.setCardType("");
        paymentObj.setExpiryMonth("");
        paymentObj.setNameOnCard("");
        return paymentObj;
    }


    /**
     * Gets the final json structure.
     *
     * @param request
     *            the request
     * @param bookObject
     *            the book object
     * @param resourceResolver
     * @param aemSession
     * @return the final json structure
     */
    public static BookingObject getFinalJsonStructure(SlingHttpServletRequest request, BookingObject bookObject,
            Session aemSession, ResourceResolver resourceResolver, String contentRootPath) {

        try {
            HotelDetailsFetcher fetchDetails = new HotelDetailsFetcher();
            if (null != bookObject) {
                String hotelID = bookObject.getHotelId();
                if (StringUtils.isNotBlank(hotelID)) {
                    Hotel hotelDetails = fetchDetails.getHotelDetails(request, hotelID, contentRootPath, "");
                    bookObject.setHotel(hotelDetails);
                }
                String specialRequest = bookObject.getGuest().getSpecialRequests();
                Payments paymentObj = getPaymentDetailsFromSpecialRequest(specialRequest, bookObject);
                bookObject.setPayments(paymentObj);
                setGSTNumberToBookingObject(specialRequest, bookObject);
                String hotelPath = bookObject.getHotel().getHotelResourcePath();
                List<Room> rooms = bookObject.getRoomList();
                setRoomDetailsFromNodes(hotelPath, rooms, fetchDetails, aemSession, resourceResolver);
            }

        } catch (RepositoryException e) {
            LOG.error("Exception found in getFinalJsonStructure :: {}", e.getMessage());
            LOG.debug(" Exception found in getFinalJsonStructure :: {}", e);
        } catch (Exception e) {
            LOG.error("Exception found in getFinalJsonStructure :: {}", e.getMessage());
            LOG.debug(" Exception found in getFinalJsonStructure :: {}", e);
        }
        return bookObject;
    }


    /**
     * Gets the room details from nodes.
     *
     * @param hotelPath
     *            the hotel path
     * @param rooms
     *            the rooms
     * @param fetchDetails
     *            the fetch details
     * @param resourceResolver
     * @param aemSession
     * @return the room details from nodes
     */
    public static void setRoomDetailsFromNodes(String hotelPath, List<Room> rooms, HotelDetailsFetcher fetchDetails,
            Session aemSession, ResourceResolver resourceResolver) {
        try {
            if (StringUtils.isNotBlank(hotelPath) && null != rooms) {
                for (Room room : rooms) {
                    Map<String, String> queryParam = new HashMap<>();
                    queryParam.put("path", hotelPath);
                    queryParam.put("type", ReservationConstants.NT_UNSTRUCTURED);
                    String roomCode = room.getRoomTypeCode();
                    queryParam.put("1_property", ReservationConstants.ROOM_CODE);
                    queryParam.put("1_property.value", roomCode);
                    Iterator<Node> iteratorNode = fetchDetails.searchRoomType(queryParam, aemSession, resourceResolver);
                    while (null != iteratorNode && iteratorNode.hasNext()) {
                        Node currentNode = iteratorNode.next();
                        if (null != currentNode) {
                            if (currentNode.hasProperty(ReservationConstants.BEDTYPE)) {
                                room.setBedType(
                                        currentNode.getProperty(ReservationConstants.BEDTYPE).getValue().getString());
                            }
                            Node parentNode = currentNode.getParent().getParent();
                            if (null != parentNode && parentNode.hasProperty(ReservationConstants.ROOM_TITLE)) {
                                String roomTitle = parentNode.getProperty(ReservationConstants.ROOM_TITLE).getValue()
                                        .getString();
                                if (StringUtils.isNotBlank(roomTitle)) {
                                    room.setRoomTypeName(roomTitle);
                                }
                            }
                        }
                    }
                }
            }
        } catch (IllegalStateException | RepositoryException e) {
            LOG.debug("Exception found in getRoomDetailsFromNodes :: {} ", e);
            LOG.error("Exception found in getRoomDetailsFromNodes :: {} ", e.getMessage());

        } catch (Exception e) {
            LOG.debug("Exception found in getRoomDetailsFromNodes :: {} ", e);
            LOG.error("Exception found in getRoomDetailsFromNodes :: {} ", e.getMessage());
        }
    }


}
