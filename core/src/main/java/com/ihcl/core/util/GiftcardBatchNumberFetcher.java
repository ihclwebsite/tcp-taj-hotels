/**
 *
 */
package com.ihcl.core.util;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.integration.giftcard.config.ConfigurationService;

/**
 * @author Vijay Chikkani
 *
 */
@Component(immediate = true,
        service = IGiftcardBatchNumberFetcher.class)
public class GiftcardBatchNumberFetcher implements IGiftcardBatchNumberFetcher {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    Resource resultResource;

    ResourceResolver resourceResolver;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public String getCurrentBatchNumber(SlingHttpServletRequest request, ConfigurationService configurationService)
            throws RepositoryException {
        LOG.debug("Starting getCurrentBatchNumber()");
        ResourceResolver resolver = request.getResourceResolver();
        Session session = resolver.adaptTo(Session.class);
        if (session != null) {
            Node giftcardNode = session.getNode(configurationService.getGiftcardCurrentBatchNumberLocation());
            Property batchProperty = giftcardNode.getProperty("currentBatchNumber");
            LOG.debug("End of getCurrentBatchNumber()");
            return batchProperty.getValue().getString();
        }
        LOG.info("While getting Giftcard Batch number, sessioin getting null ");
        LOG.debug("End of getCurrentBatchNumber()");
        return "";
    }

    @Override
    public boolean setCurrentBatchNumber(SlingHttpServletRequest request, ConfigurationService configurationService,
            String newBatchNumber) throws RepositoryException, LoginException, PersistenceException {
        LOG.debug("Starting setCurrentBatchNumber()");
        resourceResolver = resolverFactory.getServiceResourceResolver(null);
        resultResource = resourceResolver.getResource(configurationService.getGiftcardCurrentBatchNumberLocation());
        if (resultResource != null) {
            Node logNode = resultResource.adaptTo(Node.class);
            if (logNode != null) {
                logNode.setProperty("currentBatchNumber", newBatchNumber);
                resourceResolver.commit();
                LOG.info("New GiftcardBatchnumber commited in content");
                LOG.debug("End of setCurrentBatchNumber()");
                return true;
            }
            LOG.info("While setting GiftcardBatchnumber in content, logNode getting null");
            LOG.debug("End of setCurrentBatchNumber()");
            return false;
        }
        LOG.info("While setting GiftcardBatchnumber in content, resultResource getting null");
        LOG.debug("End of setCurrentBatchNumber()");

        return false;

        /*
         * ResourceResolver resolver = request.getResourceResolver(); Session session = resolver.adaptTo(Session.class);
         * Node giftcardNode = session.getNode(configurationService.getGiftcardCurrentBatchNumberLocation());
         * giftcardNode.setProperty("currentBatchNumber", newBatchNumber); session.save();
         */
    }

}
