package com.ihcl.core.util;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.servlets.JivaSpaBookAppointmentServlet;

public class StringTokenReplacement {

    private static final Logger LOG = LoggerFactory.getLogger(JivaSpaBookAppointmentServlet.class);

    /**
     * This method takes strings containing tokens and a token map and replaces tokens in string with its value in token
     * map.
     *
     * @param tokenMap
     *            Eg. tokenMap = {"token1": "value1"}
     * @param text
     *            Eg. text = "${token1} across Taj"
     * @return String Eg. "value1 across Taj"
     */
    public static String replaceToken(HashMap<String, String> tokenMap, String text) {
        LOG.trace("Input values for replace token method are : " + tokenMap + " and " + text);
        StringBuffer result = new StringBuffer();
        try {
            Pattern p = Pattern.compile("\\$\\{([\\w\\.]+)\\}");
            Matcher m = p.matcher(text);
            while (m.find()) {
                String token = m.group().replaceAll("[${}]", "");
                // Adding null check for token value. Appending empty string if the value is null.
                if (null != tokenMap.get(token)) {
                    m.appendReplacement(result, tokenMap.get(token));
                } else {
                    m.appendReplacement(result, "");
                }
            }
            m.appendTail(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.trace("Output value from replace token method  : " + result);
        return result.toString();
    }
}
