/**
 *
 */
package com.ihcl.core.util.services.impl;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.util.services.TajCommonsFetchResource;

/**
 * @author Srikanta.moonraft
 *
 */
@Component(
        immediate = true)
public class TajCommonsFetchResourceImpl implements TajCommonsFetchResource {

    private final Logger LOG = LoggerFactory.getLogger(TajCommonsFetchResourceImpl.class);

    @Override
    public Resource getChildUpto(Resource currentResource, String childResourceName) {
        LOG.trace("Entry > [Method : getChildUpto(currentResource, childResourceName) ] :: [ Annotations : @Override");
        if (currentResource != null) {
            if (currentResource.getName().contains(childResourceName)) {
                LOG.debug("Returning > [child > path :" + currentResource.getPath() + " ]");
                return currentResource;
            }
            if (currentResource.hasChildren()) {
                for (Resource child : currentResource.getChildren()) {
                    return getChildUpto(child, childResourceName);
                }
            }
            LOG.warn("Returning > [child > as null : Can not find the resource in the Hirarchy ]");
            return null;
        }
        LOG.warn("Returning > [child > as null : condition failed(currentResource is null ]");
        LOG.trace("Exit > [Method : getChildUpto(currentResource, childResourceName) ] :: [ Annotations : @Override ]");

        return null;
    }

    @Override
    public ValueMap getChildAsValueMapUpto(Resource currentResource, String childResourceName) {
        LOG.trace(
                "Entry > [Method : getChildAsValueMapUpto(currentResource, childResourceName) ] :: [ Annotations : @Override");
        ValueMap map = null;
        map = getChildUpto(currentResource, childResourceName).adaptTo(ValueMap.class);
        LOG.debug("Returning > [child > as null : condition failed(currentResource is null ]");
        LOG.trace(
                "Exit > [Method : getChildAsValueMapUpto(currentResource, childResourceName) ] :: [ Annotations : @Override ]");
        return map;
    }

    @Override
    public ValueMap getChildTypeAsValueMapUpto(Resource currentResource, String childResourceName,
            String childResourceType) {
        LOG.trace(
                "Entry > [Method : getChildAsValueMapUpto(currentResource, childResourceName) ] :: [ Annotations : @Override");
        if (currentResource != null) {
            if (currentResource.getName().contains(childResourceName)
                    && currentResource.isResourceType(childResourceType)) {
                LOG.debug("Returning > [child > path :" + currentResource.getPath() + " ]");
                return currentResource.adaptTo(ValueMap.class);
            }
            if (currentResource.hasChildren()) {
                for (Resource child : currentResource.getChildren()) {
                    return getChildTypeAsValueMapUpto(child, childResourceName, childResourceType);
                }
            }
            LOG.warn("Returning > [child > as null : Can not find the resource in the Hirarchy ]");
            return null;
        }
        LOG.warn("Returning > [child > as null : condition failed(currentResource is null ]");
        LOG.trace("Exit > [Method : getChildUpto(currentResource, childResourceName) ] :: [ Annotations : @Override ]");
        return null;
    }

    @Override
    public Resource getParentUpto(Resource currentResource, String parentResourceName) {
        LOG.trace(
                "Entry > [Method : getParentUpto(currentResource, parentResourceName) ] :: [ Annotations : @Override ]");
        Resource result = currentResource;
        while (result != null && result.getParent() != null) {
            Resource parent = result.getParent();
            if (parent.getName().equals(parentResourceName)) {
                return parent;
                // result= child;
            } else {
                result = result.getParent();
            }
            return result;
        }
        LOG.trace(
                "Exit > [Method : getParentUpto(currentResource, parentResourceName) ] :: [ Annotations : @Override ]");
        return result;
    }

}
