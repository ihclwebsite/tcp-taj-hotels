package com.ihcl.core.offerdata.incremental;

import java.util.*;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.Component;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import javax.jcr.Value;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.util.Iterator;
import java.lang.Iterable;
import java.io.IOException;
import javax.jcr.Node; 

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.offerdata.incremental.Offer;
import com.ihcl.core.offerdata.incremental.GlobalOfferService;



@Component(service = HotelOfferService.class)
public class HotelOfferService {
	
    @Reference
    private ResourceResolverFactory resolverFactory;
	
    @Reference
    private GlobalOfferService globalOfferService;
    
    private final Logger LOG = LoggerFactory.getLogger(getClass());
    
	public ArrayList<Object> getOffers (Page page ,ResourceResolver resourceResolver ,String brandName)
	{

		HashMap<String, HashMap<String, ArrayList<Object>>>  destobj ;
		HashMap<String, ArrayList<Object>> hotelobj;
		ArrayList<Object> array =new ArrayList<>();
		ArrayList<Object> hotelarray;
	    String DestName = "";
	    String hotelName = "";
	    String hotelId = "";
	    Date ref_date = null;
	    
	    try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page currentPage = page ;
            Node content = currentPage.getContentResource().adaptTo(Node.class);
            
            Node ref_node = resourceResolver.getResource("/content/Offers-data").adaptTo(Node.class);
            try {
                ref_date = ref_node.getProperty("SchedulerDate").getValue().getDate().getTime();
            	
            }
            catch(Exception e) {
            	
            }
            
            Iterator<Page> itrd = currentPage.listChildren();
            while(itrd.hasNext()) {
            	destobj = new HashMap<>();
            	hotelobj = new HashMap<>();
            	currentPage = itrd.next();
            	
            	try {
            	
            		content = currentPage.getContentResource().adaptTo(Node.class);
            		
            		if(content.hasProperty("city")) 
            		{
                	   Value[] values = content.getProperty("city").getValues();
            			DestName = values[0].getString().substring(values[0].getString().lastIndexOf("/") + 1);
                	
                	}
            		else if(content.hasProperty("jcr:title")) {
            	    DestName = content.getProperty("jcr:title").getValue().getString();
            	
            		}
            		
            	}
            	catch(Exception e) {
            		LOG.error("Error while searching for jcr:title or city property ", e);
            	}
            	
            	try
            	{
            	    currentPage = pageManager.getPage(currentPage.getPath() + "/" + brandName);
            	    Iterator<Page> itr = currentPage.listChildren();
                
            	    while(itr.hasNext()) {
                	   hotelarray = new ArrayList<>();
                	   hotelName = "";
                 	   hotelId = "";
                	   currentPage = itr.next();
                   	   content = currentPage.getContentResource().adaptTo(Node.class);
              	  
                   	   try {
              	          
                   		   if(content.hasProperty("hotelName"))
   		    	           {
   		           			   hotelName = content.getProperty("hotelName").getValue().getString();
   		    	           }
                   		   else if(content.hasProperty("jcr:title")) {
                   			hotelName = content.getProperty("jcr:title").getValue().getString();
                        	
                   		}
                   		hotelId = content.getProperty("hotelId").getValue().getString();
                   		   }
   		    		  catch(Exception e) {
   		    			LOG.error("Error while searching for hotelName property ", e);
   		    		  }

                   	   try {
              	           currentPage =currentPage.getPageManager().getPage(currentPage.getPath() + "/offers-and-promotions");
                       	   Iterator<Page> itra = currentPage.listChildren();
     	    	  
                       	   while(itra.hasNext()) {
     	    		         currentPage = itra.next();
     	    		  
     	    		         try {
     	    		             content = currentPage.getContentResource().adaptTo(Node.class);
     	    		             if(content.getProperty("cq:lastReplicated").getValue().getDate().getTime().compareTo(ref_date) >= 0)
     	    		             {
     	    			          Offer offer = new Offer();
 			                     
     	    			          String title = globalOfferService.getTitle(content);
 			          	          String offerCode = globalOfferService.getOfferCode(content);
 			          	          String roundOffer = globalOfferService.getRoundTheYear(content);
 			          	          String imagePath = globalOfferService.getImagePath(content);
 			          	          String lastRepAction = globalOfferService.getLastRepAction(content);
 			          	          String redirectionURL = globalOfferService.getRedirectionURL(content);
 			          	          String offerStarts = globalOfferService.getOfferStarts(content);
 			          	          String offerEnds = globalOfferService.getOfferEnds(content);
     	    		 
                                  offer.settitle(title);
 			               	      offer.setcode(offerCode);
 			          	          offer.setroundYear(roundOffer);
 			          	          offer.setimagePath(imagePath);
 			          	          offer.setlastRepAction(lastRepAction);
 			          	          offer.setstart(offerStarts);
 			          	          offer.setend(offerEnds);
 			          	          offer.setredirectionURL(redirectionURL);
 			          	          offer.sethotelId(hotelId);
 			          	          try {
 			          	 
 			          	        	  Node root = content.getNode("root");
 			          	   
 			          	              Node currentNode = null;
 			                          boolean reference =false;
 			                          Iterator<Node> itrn = root.getNodes();
 			          	
 		   	    	                  while(itrn.hasNext()) {
 		   	    		                currentNode = itrn.next();
 		   	    		
 		   	    		                if(currentNode.getName().length() > 10 && currentNode.getName().substring(0,10).equals("reference_"))
 		   	    		                {
 		   	    		                offer = getReferencePackage(offer,currentNode,resourceResolver);
 		   	    		                reference =  true;
 		   	    		                }
 		   	    	   
 		   	    	                  }
 			          	   
 		   	    	                  if(!reference) {
 		   	    	                  Node offerdetails =null;
 		   	    	                  Node offerinclusion =null;
 		   	    	                  Node offerTC =null;
 		   	    	                  try {
 				                      offerdetails = root.getNode("offerdetails");
 			  	                      }
 			  	                      catch(Exception e) {
 			  	                    	
 			  	                      }
 			  	                      try {
 				                      offerinclusion = root.getNode("offerinclusion");
 			  	                      }
 			  	                      catch(Exception e) {
 			  	                    	  
			  	                      }
 				                      try {
 				                      offerTC = root.getNode("offer_terms_conditions");	   
 				                      }
 				                     catch(Exception e) {
 				                    	
			  	                      }
 				                      String description = globalOfferService.getDescription(offerdetails);
 				                      String validity = globalOfferService.getValidity(offerdetails);
 				                     HashMap<String, String> inclusion = globalOfferService.getInclusions(offerinclusion);
 				                      String termsConditions = globalOfferService.getTermsConditions(offerTC);
 				        
 				                      offer.setdescription(description);
 				                      offer.setvalidity(validity);
 				                      offer.settermsConditions(termsConditions);
 				                      offer.setinclusion(inclusion);
 			          	
 		   	    	                  }
 			          	
 			          	          }
 			          	
 			          	          catch(Exception e) {
 			          	        	LOG.error("Error while fetching root node and its child nodes ", e);
 			          	          }
			          	
 			          	       HashMap<String, Object> Jobj = offer.offerJson();
			          	          hotelarray.add(Jobj);
     	    		  
     	    		             }
     	    		         }
     	    		        catch(Exception e) {
     	    		        	LOG.error("Error while searching for lastReplicationAction property or jcr:content resource ", e);
                  	         }     	    	
     	    		      }
                          if(hotelarray != null && !hotelarray.isEmpty())
                          {
                             hotelobj.put(hotelName,hotelarray);
                          }
      	               }
                       catch(Exception e) {
                    	   LOG.error("Error while fetching offers-and-promotions node ",hotelName, e);
                       }
                   }
                   if(!hotelobj.isEmpty())
                   {
                      destobj.put(DestName,hotelobj);
                   }
                   if (!destobj.isEmpty())
                   {
                      array.add(destobj);
                   }
                 }
            	 catch(Exception e) {
            		 LOG.error("Error while fetching brandName node  ", e);   
 		    	  }
              }
		   }
		   catch(Exception e) {
			   LOG.error("Error while fetching argument page and its jcr:content ", e);
		   }
		return array;
	}
	
	public Offer getReferencePackage(Offer offer,Node node,ResourceResolver resourceResolver)
    {
		try {
		
		   String ref_path =node.getProperty("path").getValue().getString();
		   String identifier = ref_path.substring(ref_path.lastIndexOf("/") + 1);
		   Node ref_node = resourceResolver.getResource(ref_path).adaptTo(Node.class);
		
		   if (identifier.equals("holidays_hotel_package"))
			{
			   String title = globalOfferService.getTitle(ref_node);
         	   String offerCode = globalOfferService.getOfferCode(ref_node);
         	
         	   String offerStarts = globalOfferService.getOfferStarts(ref_node);
         	   String offerEnds = globalOfferService.getOfferEnds(ref_node); 
	           String description = globalOfferService.getDescription(ref_node);
	           String validity = globalOfferService.getValidity(ref_node);
	           HashMap<String, String> inclusion = getInclusions(ref_node);
	           String termsConditions = globalOfferService.getTermsConditions(ref_node);
	        
	           offer.setdescription(description);
	           offer.setvalidity(validity);
	           offer.settermsConditions(termsConditions);
	           offer.setinclusion(inclusion);
	           offer.settitle(title);
         	   offer.setcode(offerCode);
			
			}
		   
		   if (identifier.equals("offerdetails"))
			{
			 String description = globalOfferService.getDescription(ref_node);
	         String validity = globalOfferService.getValidity(ref_node);
	         
	         offer.setdescription(description);
	         offer.setvalidity(validity);
	         
			}
		
		   if (identifier.equals("offerinclusion"))
			{
			   HashMap<String, String> inclusion = globalOfferService.getInclusions(ref_node);
			 offer.setinclusion(inclusion);
			
			}
		
		   if (identifier.equals("offer_terms_conditions"))
			{
			   String termsConditions = globalOfferService.getTermsConditions(ref_node);
			      
		       offer.settermsConditions(termsConditions);
			}
		
		}
		catch(Exception e) {
			LOG.error("Error while fetching path property or node specified by path property ", e);
		}
		return offer ;
 
    }
	
	public HashMap<String, String> getInclusions(Node node)
	{ 
		HashMap<String, String> inclusion = new HashMap<>();
    	String type ="";
    	String details ="";
    	try {
    		Node inclusions = node.getNode("inclusions");	
    		Iterator<Node> itr = inclusions.getNodes();
	    	while(itr.hasNext()) {
	    		
	    		Node currentNode = itr.next();
	    		if(currentNode.hasProperty("inclusionImageMap"))
	    		{
	    		type = currentNode.getProperty("inclusionImageMap").getValue().getString();
	    		type = type.substring(0,type.indexOf(':'));
	    		
	    		}
	    		if(currentNode.hasProperty("inclusionDescription"))
	    		{
	    		details = currentNode.getProperty("inclusionDescription").getValue().getString();
	    		
	    		}
	    		inclusion.put(type, details);
	    	}
    	}
    	catch(Exception e) {
    		LOG.error("Error while fetching inclusions node ", e);
    	}
    			
		return inclusion;
	}

	
}

	
