/**
 * 
 */
package com.ihcl.core.offerdata.incremental;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Session;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.offerdata.incremental.GlobalOfferService;
import com.ihcl.core.offerdata.incremental.Offer;
import com.ihcl.core.offerdata.incremental.DestinationOfferService;



@Designate(ocd=DataScheduler.Config.class)
@Component(service=Runnable.class)
public class DataScheduler implements Runnable{

	private static final Logger EXCEPTION_LOGGER = LoggerFactory.getLogger(DataScheduler.class);
	private boolean SchedulerEnabled;
  	
 	@Reference
	GlobalOfferService globalOfferService;
	
	@Reference
	HotelOfferService hotelOfferService;
	
	@Reference
	DestinationOfferService destinationOfferService;
	
	@Reference
	ResourceResolverFactory resourceResolverFactory;

	
	@Override
	public void run() {
		if (SchedulerEnabled) {
			EXCEPTION_LOGGER.info("DataScheduler started");
			ResourceResolver resourceResolver = null;
			Node node = null;
			
			Date date = new Date();
			Calendar calendar = Calendar.getInstance();
	   	    calendar.setTime(date);
	   	 
	   	    ObjectMapper objectMapper = new ObjectMapper(); 
	   	    HashMap<String, HashMap<String, ArrayList<Object>>> offerList = new HashMap<>();
			
	   	    HashMap<String, Object> param = new HashMap<>();
            param.put(resourceResolverFactory.SUBSERVICE,"DataSchedulerService");
        
            try {
        	
                resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);
        	
                node = resourceResolver.getResource("/content/Offers-data").adaptTo(Node.class);
           
      	        HashMap<String, ArrayList<Object>> Global = new HashMap<>();
      	        HashMap<String, ArrayList<Object>> Hotel = new HashMap<>();
      	        HashMap<String, ArrayList<Object>> Destination = new HashMap<>();
      	        ArrayList<Object> array;
      	 
      	        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
                Page currentPage ;
     
                currentPage = pageManager.getPage("/content/tajhotels/en-in/offers");
                array =  globalOfferService.getOffers(currentPage, resourceResolver);   
                if(array != null && !array.isEmpty()) 
                {
                	Global.put("Taj",array);
                }
     
                currentPage = pageManager.getPage("/content/seleqtions/en-in/offers");
                array =  globalOfferService.getOffers(currentPage, resourceResolver);   
                if(array != null && !array.isEmpty()) 
                {
                	Global.put("Seleqtions",array);
                }
     
                currentPage = pageManager.getPage("/content/vivanta/en-in/offers");
                array =  globalOfferService.getOffers(currentPage, resourceResolver);   
                if(array != null && !array.isEmpty()) 
                {
                	Global.put("Vivanta",array);
                }
     
                currentPage = pageManager.getPage("/content/ama/en-in/offers-and-promotions");
                array =  globalOfferService.getOffers(currentPage, resourceResolver);   
                if(array != null && !array.isEmpty()) 
                {
                	Global.put("ama",array);
                }
     
     
                currentPage = pageManager.getPage("/content/tajhotels/en-in/our-hotels");
                array =  hotelOfferService.getOffers(currentPage , resourceResolver,"taj");   
                if(array != null && !array.isEmpty())
                { 
                	Hotel.put("Taj",array);
                }
     
                currentPage = pageManager.getPage("/content/seleqtions/en-in/our-hotels");
                array =  hotelOfferService.getOffers(currentPage , resourceResolver,"seleqtions");   
                if(array != null && !array.isEmpty())
                { 
                	Hotel.put("Seleqtions",array);
                }
     
                currentPage = pageManager.getPage("/content/vivanta/en-in/our-hotels");
                array =  hotelOfferService.getOffers(currentPage , resourceResolver,"vivanta");   
                if(array != null && !array.isEmpty())
                { 
                	Hotel.put("Vivanta",array);
                }

                currentPage = pageManager.getPage("/content/ama/en-in/our-hotels");
                array =  hotelOfferService.getOffers(currentPage , resourceResolver,"ama");   
                if(array != null && !array.isEmpty())
                { 
                	Hotel.put("ama",array);
                }

     
                currentPage = pageManager.getPage("/content/tajhotels/en-in/taj-holidays/destinations");
                array =  destinationOfferService.getOffers(currentPage , resourceResolver);   
                if(array != null && !array.isEmpty()) 
                {
                	Destination.put("Taj",array);
                }
     
                if(Global != null && !Global.isEmpty()) 
                {
                	offerList.put("Global offers" , Global);
                }
                if(Hotel != null && !Hotel.isEmpty()) 
                {
                	offerList.put("Hotel offers" , Hotel);
                }
                if(Destination != null && !Destination.isEmpty()) 
                {
                	offerList.put("Destination offers" , Destination);
                }
       
       
                try {
                	node.setProperty("Data",objectMapper.writeValueAsString(offerList));
                	node.setProperty("SchedulerDate", calendar);
          
                	Session session = resourceResolver.adaptTo(Session.class);
                	session.save();
                }
                catch(Exception e) {
                	EXCEPTION_LOGGER.error("Problem while setting the property ",e);
                }
			
				} 
            	catch (Exception e) {
            		EXCEPTION_LOGGER.error("Problem getting resource",e);
				} 
            	finally {
            		if (null != resourceResolver) {
					resourceResolver.close();
            		}
            		EXCEPTION_LOGGER.info("Scheduler Ended");
            	}
			}
		}
	 
	
	@Activate
	@Modified
	public void activate(Config config) {
		SchedulerEnabled = config.enableScheduler();
		run();
		}
	
	@ObjectClassDefinition(name="Data Scheduler",
            description = "This scheduler would loop through offers and return modified offers")
	public @interface Config {
		
		@AttributeDefinition(name = "Cron expression defining when this Scheduled Service will run", 
			description = "[every minute = 0 * * * * ?], [01:00am daily = 0 0 1 * * ?]")
		String scheduler_expression() default "0 */15 * ? * *";
		
		 @AttributeDefinition(name = "Concurrent task",
             description = "Whether or not to schedule this task concurrently")
		 boolean scheduler_concurrent() default false;
		
		@AttributeDefinition(name = "Enable Scheduler", description = "ex: true or false")
		boolean enableScheduler() default true;
		

		
		
	}

}