
package com.ihcl.core.offerdata.incremental;

import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.resource.ValueMap;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.*;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.jcr.Node; 
import javax.jcr.Session;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.ihcl.core.offerdata.GlobalOfferService;
import com.ihcl.core.offerdata.Offer;
import com.ihcl.core.offerdata.DestinationOfferService;
/**
 * Servlet that writes incremental offer details into the response.
 */

@Component(service = Servlet.class,
property = { 
		"sling.servlet.paths=" + "/bin/incrementaloffer", 
		"sling.servlet.methods=get",
	    "sling.servlet.extensions=json",
		"sling.servlet.selectors=groups", 
		})
 public class IncrementalOfferServlet extends SlingAllMethodsServlet {
	
    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {

    	ResourceResolver resourceResolver=null;
    	Node node  = null;
 
    	JSONObject offerList=new JSONObject();
    	Date date = new Date();
    	Calendar calendar = Calendar.getInstance();
   	    calendar.setTime(date);
   	    String data = "" ;
      
   	    try{
    	
   	    	resourceResolver =req.getResourceResolver();
      
   	    	node = resourceResolver.getResource("/content/Offers-data").adaptTo(Node.class);
            
   	    	try {
			
   	    		data = node.getProperty("Data").getValue().getString();
//   	    		node.setProperty("Date", calendar);
//           
//   	    		Session session = resourceResolver.adaptTo(Session.class);
//		        session.save();
		        }
		
   	    	catch(Exception e) {
	      		}
   	    }
   	    catch(Exception e) {
   	    }	
   	    
   	    resp.getWriter().print(data);
    } 
}