package com.ihcl.core.offerdata;

import java.util.*;
import java.text.SimpleDateFormat;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.util.Iterator;
import java.lang.Iterable;
import java.io.IOException;
import javax.jcr.Node; 
import javax.jcr.Value;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.offerdata.Offer;


@Component(service = GlobalOfferService.class)
public class GlobalOfferService {
	
    @Reference
    private ResourceResolverFactory resolverFactory;
    
    private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	public JSONArray getOffers (Page page ,ResourceResolver resourceResolver, boolean isAuthor)
	{
		Page currentPage;
		Offer offer ;
		JSONArray array = new JSONArray();
		boolean loopOffer = true;
		
		try {
		Node content = page.getContentResource().adaptTo(Node.class);
		Iterator<Page> it = page.listChildren(); 
		
		while(it.hasNext()) {
		      currentPage = it.next();
		      content = currentPage.getContentResource().adaptTo(Node.class);
		      loopOffer = true;
		      if(content.hasProperty("showInGlobalOffers") && content.getProperty("showInGlobalOffers").getValue().getBoolean() == true)
	          {
		    	  
		    	if  (isAuthor) {
		    	if(!content.getProperty("cq:lastReplicationAction").getValue().getString().toLowerCase().equals("activate"))
		    		loopOffer = false;
		    	}
	        	if(loopOffer) 
	        	{	
	          	offer = new Offer();
	          	
	          	String title = getTitle(content);
	          	String offerCode = getOfferCode(content);
	          	String roundOffer = getRoundTheYear(content);
	          	String imagePath = getImagePath(content);
	          	String lastRepAction = getLastRepAction(content);
	          	String redirectionURL = getRedirectionURL(content);
	          	String offerStarts = getOfferStarts(content);
	          	String offerEnds = getOfferEnds(content);
	       
	          	
	          	try {
	          	   Node root = content.getNode("root");
	  	         
		           Node offerdetails = root.getNode("offerdetails");
		           Node offerinclusion = root.getNode("offerinclusion");
		           Node offerTC = root.getNode("offer_terms_conditions");	   
		         
		           String description = getDescription(offerdetails);
		           String validity = getValidity(offerdetails);
		           JSONObject inclusion = getInclusions(offerinclusion);
		           String termsConditions = getTermsConditions(offerTC);
		        
		           offer.setdescription(description);
		           offer.setvalidity(validity);
		           offer.settermsConditions(termsConditions);
		           offer.setinclusion(inclusion);
		           
		           if(root.hasNode("participating_hotels"))
		           {
		        	   try {
		        	   Node participating_hotels_node = root.getNode("participating_hotels");
		        	   boolean global = participating_hotels_node.getProperty("global").getValue().getBoolean();
		        	   if(!global)
		        	   {
		        		   JSONObject participating_hotels = getHotels(participating_hotels_node, resourceResolver);
		        		   offer.sethotels(participating_hotels);
		        	   }
		        	   }
		        	   catch(Exception e) {
		        		   LOG.error("Error while searching for participating hotels", e);
		        	   }
		           }
	          	}
	          	
	          	catch(Exception e) {
	          		LOG.error("Error while searching for root child nodes of =", title, e);
	          	}
	          	offer.settitle(title);
	          	offer.setcode(offerCode);
	          	offer.setroundYear(roundOffer);
	          	offer.setimagePath(imagePath);
	          	offer.setlastRepAction(lastRepAction);
	          	offer.setstart(offerStarts);
	          	offer.setend(offerEnds);
	          	offer.setredirectionURL(redirectionURL);
	          	offer.sethotelId("");
	          	
	          	
	          	JSONObject Jobj = offer.offerJson();
	          	array.add(Jobj);
	        	}
	       }
		      
		}
		}
		catch(Exception e) {
			LOG.error("Error while searching for showInGlobalOffers or lastReplicationAction property ", e);
		}
		return array ;
	}


		
		public String getTitle(Node node)
		{
			String title = "" ;
			try {
			if(node.hasProperty("offerTitle"))
			{   
				 title = node.getProperty("offerTitle").getValue().getString();
			}
			else if(node.hasProperty("packageTitle"))
			{   
				 title = node.getProperty("packageTitle").getValue().getString();
			}
		
			}
			catch (Exception e){
				LOG.error("Error while searching for Title property ", e);
			}		
			return  title ;
		}
		
		public String getOfferStarts(Node node)
		{
			Date start = null ;
			String startDate = "";
			try {
				if(node.hasProperty("packageValidityStartDate"))
				{
					startDate = node.getProperty("packageValidityStartDate").getValue().getString();
					startDate = convertDate(startDate, "start");
				}
				
				else if(node.hasProperty("offerStartsFrom"))
			    {   
				
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				
		            start = node.getProperty("offerStartsFrom").getValue().getDate().getTime();
		        
		            startDate = formatter.format(start); 
				
			    }
		
			}
			catch (Exception e){
				LOG.error("Error while searching for OfferStartsFrom property ", e);
			}		
			return  startDate ;
		}
		
		public String getOfferEnds(Node node)
		{
			Date end = null ;
			String endDate = "";
			try {
				if(node.hasProperty("packageValidityEndDate"))
				{
					endDate = node.getProperty("packageValidityEndDate").getValue().getString();
					endDate = convertDate(endDate, "end");
				}
				
				else if(node.hasProperty("offerEndsOn"))
			    {   
				
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				
		            end = node.getProperty("offerEndsOn").getValue().getDate().getTime();
		        
		            endDate = formatter.format(end); 
				
			    }
		
			}
			catch (Exception e){
				LOG.error("Error while searching for OfferEndsOn property ", e);
			}		
			return  endDate ;
		}
		
		public String getOfferCode(Node node)
		{
			String offercode = "" ;
			try {
			    if(node.hasProperty("offerRateCode"))
			    {
				   offercode = node.getProperty("offerRateCode").getValue().getString();
			    }
		
			}
			catch(Exception e){
				LOG.error("Error while searching for OfferCode property ", e);	 
			}		
			return offercode;
		}
		
		public String getRoundTheYear(Node node)
		{
			String roundyear = "" ;
			try {
			if(node.hasProperty("roundTheYearOffer"))
			{
				roundyear = node.getProperty("roundTheYearOffer").getValue().getString();
			}
		
			}
			catch(Exception e){
				LOG.error("Error while searching for roundTheYear property ", e); 
			}		
			return roundyear;
		}
		
		public String getImagePath(Node node)
		{
			String path = "";
			try {
			if(node.hasProperty("listImage"))
			{
				path =  node.getProperty("listImage").getValue().getString();
			}
			else if(node.hasProperty("packageImagePath"))
			{
				path =  node.getProperty("packageImagePath").getValue().getString();
			}
			
			}
			catch(Exception e){
				LOG.error("Error while searching for ImagePath property ", e);	 
			}		
			return path;
		}		

		public String getLastRepAction(Node node)
		{
			String action = "";
			try {
			if(node.hasProperty("cq:lastReplicationAction"))
			{
				action =  node.getProperty("cq:lastReplicationAction").getValue().getString();
			}
			
			}
			catch(Exception e){
				LOG.error("Error while searching for lastReplicationAction property ", e);	 
			}		
			return action;
		}
		
		public String getRedirectionURL(Node node)
		{
			String url = "";
			try {
			if(node.hasProperty("jcr:canonical"))
			{
				url =  node.getProperty("jcr:canonical").getValue().getString();
			}
			
			}
			catch(Exception e){
				LOG.error("Error while searching for jcr:canonical property ", e);
			}		
			return url;
		}
		
		
		public String getDescription(Node node)
		{
			String description = "";
			try {
			if(node.hasProperty("offerDescription"))
			{
				description = node.getProperty("offerDescription").getValue().getString();
			}
			else if (node.getParent().getParent().hasProperty("offerShortDesc"))
			{
				description = node.getParent().getParent().getProperty("offerShortDesc").getValue().getString();
			}
			else if(node.hasProperty("packageDescription"))
			{
				description = node.getProperty("packageDescription").getValue().getString();
			}
			}
			catch(Exception e){
				LOG.error("Error while searching for Description property ", e);
			}		
			return description;
		}
		
		public String getValidity(Node node)
		{
			String validity = "";
			try {
			if(node.hasProperty("validity"))
			{
				validity = node.getProperty("validity").getValue().getString();
			
				if (node.hasProperty("validity Text"))
			    {
				   validity = node.getProperty("validity Text").getValue().getString() + validity;
			    }
				else if (node.getParent().getParent().hasProperty("validity Text"))
			    {
				   validity = node.getParent().getParent().getProperty("validity Text").getValue().getString() + validity;
			    }
			}
			
			}
			catch(Exception e){
				LOG.error("Error while searching for Validity property ", e);
			}		
			return validity;
		}
		
		public String getTermsConditions(Node node)
		{
			String terms = "";
			try {
			if(node.hasProperty("termsDescription"))
			{
				terms = node.getProperty("termsDescription").getValue().getString().replaceAll("\\<.*?>"," ");
			}
			else if(node.hasProperty("termsAndCondition"))
			{
				terms = node.getProperty("termsAndCondition").getValue().getString().replaceAll("\\<.*?>"," ");
			}
			
			}
			catch(Exception e){
				LOG.error("Error while searching for T&C property ", e);
			}		
			return terms;
		}
		
		public JSONObject getInclusions(Node node)
		{
			JSONObject inclusion = new JSONObject();
			try {
			if(node.hasProperty("convenience"))
			{
				if(!(node.getProperty("convenience").getValue().getString().equals("") || node.getProperty("convenience").getValue().getString().equals(" ")))
				{
					inclusion.put("convenience",node.getProperty("convenience").getValue().getString()) ;
				}
			}
			
			if(node.hasProperty("dining"))
			{
				if(!(node.getProperty("dining").getValue().getString().equals("") || node.getProperty("dining").getValue().getString().equals(" ")))
				{
				inclusion.put("dining",node.getProperty("dining").getValue().getString()) ;
			
				}
			}
			
			if(node.hasProperty("stay"))
			{
				if(!(node.getProperty("stay").getValue().getString().equals("") || node.getProperty("stay").getValue().getString().equals(" ")))
				{
				inclusion.put("stay",node.getProperty("stay").getValue().getString()) ;
			
				}
			}
				
			if(node.hasProperty("spa"))
			{
				if(!(node.getProperty("spa").getValue().getString().equals("") || node.getProperty("spa").getValue().getString().equals(" ")))
				{
				inclusion.put("spa",node.getProperty("spa").getValue().getString()) ;
			
				}
			}
			if(node.hasProperty("sightseeing"))
			{
				if(!(node.getProperty("sightseeing").getValue().getString().equals("") || node.getProperty("sightseeing").getValue().getString().equals(" ")))
				{
				inclusion.put("sightseeing",node.getProperty("sightseeing").getValue().getString()) ;
			
				}
			}
			}
			catch(Exception e){
				LOG.error("Error while searching for Inclusions property ", e);
			}		
			return inclusion;
		}
		
		public JSONObject getHotels(Node node, ResourceResolver resourceResolver)
		{
			Page hotelPage = null;
			Node content = null;
			JSONObject hotel_obj = new JSONObject();
			
			try {
			Value[] values = node.getProperty("pages").getValues();
			
			for(Value value : values)
			{
				try {
				hotelPage = resourceResolver.getResource(value.getString()).adaptTo(Page.class);
				content = hotelPage.getContentResource().adaptTo(Node.class);;
				String hotelName = content.getProperty("hotelName").getValue().getString();
				String hotelId = content.getProperty("hotelId").getValue().getString();
				hotel_obj.put(hotelName, hotelId);
				}
				catch(Exception e) {
					
				}
			}
			}
			catch(Exception e) {
				
			}
			return hotel_obj;
    	
		}
		
		public String convertDate(String date, String type) {
			 String modified ="";
		   	 Calendar cl = Calendar. getInstance();
		   	 
			List<SimpleDateFormat> knownPatterns = new ArrayList<SimpleDateFormat>();
			knownPatterns.add(new SimpleDateFormat("dd MMMM yyyy"));
			knownPatterns.add(new SimpleDateFormat("dd MMM yyyy"));
			knownPatterns.add(new SimpleDateFormat("d MMMM yyyy"));
			knownPatterns.add(new SimpleDateFormat("d MMM yyyy"));
			knownPatterns.add(new SimpleDateFormat("dd/MM/yyyy"));
            knownPatterns.add(new SimpleDateFormat("dd MMM yy"));
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			
			date = date.replace("1st","1");
		  	date = date.replace("2nd","2");
		  	date = date.replace("3rd","3");
		  	date = date.replace("th "," ");
            date = date.replace("'"," ");  
			
			for (SimpleDateFormat pattern : knownPatterns) {
			    try {
			    	Date dateTest = pattern.parse(date); 
			    	if(type.equals("end")) {
			    		cl.setTime(dateTest);
				        cl. add(Calendar.HOUR, 23);
				        cl. add(Calendar.MINUTE, 59);
				        cl. add(Calendar.SECOND, 59);
				        dateTest = cl.getTime();
			    		
			    	}
			    	
			        modified =  formatter.format(dateTest);
			       
			        dateTest =  formatter.parse(modified);
			        
			        modified =  formatter2.format(formatter.parse(modified));
			        date = modified;
			        return date;
			    	
			    	// Take a try
			       

			    } catch (Exception pe) {
			        // Loop on
			    }
			}
			return date;
			
		}
}	