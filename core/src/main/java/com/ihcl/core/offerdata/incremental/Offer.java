package com.ihcl.core.offerdata.incremental;

import java.util.*;
import java.lang.String;
// import org.json.simple.JSONObject;
import java.util.HashMap;

public class Offer {
   private String title ;
   private String description ;
   private String imagePath ;
   private String validity ;
   private String lastRepAction ;
   private HashMap<String, String> inclusion ; 
   private String termsConditions ;
   private String redirectionURL ;
   private String offerCode ;
   private String roundYear ;
   private String offerStarts ;
   private String offerEnds ;
   private String hotelId;
   private HashMap<String, String> participating_hotels ;
   
   public void settitle(String title) {
		this.title = title;
	}
	public String gettitle() {
		return title;
	}
   public void setdescription(String description) {
        this.description = description;
	}
	public String getdescription() {
		return description;
	}
   public void setimagePath(String imagePath) {
        this.imagePath = imagePath;
	}
	public String getimagePath() {
		return imagePath;
	}
   public void setvalidity(String validity) {
        this.validity = validity;
	}
	public String getvalidity() {
		return validity;
	}	
   public void setlastRepAction(String lastRepAction) {
        this.lastRepAction = lastRepAction;
	}
	public String getlastRepAction() {
		return lastRepAction;
	}
   public void setinclusion(HashMap<String, String> inclusion) {
        this.inclusion = inclusion;
	}
	public HashMap<String, String> getinclusion() {
		return inclusion;
	}	
   public void settermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
	}
	public String gettermsConditions() {
		return termsConditions;
	}
	
   public void setredirectionURL(String redirectionURL) {
        this.redirectionURL = redirectionURL;
	}
	public String getredirectionURL() {
		return redirectionURL;
	}
	
   public void setcode(String offerCode) {
        this.offerCode = offerCode;
	}
	public String getcode() {
		return offerCode;
	}
		
   public void setroundYear(String roundYear) {
        this.roundYear = roundYear;
	}
	public String getroundYear() {
		return roundYear;
	}
	
    public void setstart(String offerStarts) {
	    this.offerStarts = offerStarts;
	}
	public String getstart() {
		return offerStarts;
	}
		
	public void setend(String offerEnds) {
	    this.offerEnds = offerEnds;
	}
	public String getend() {
	     return offerEnds;
	}		
	
	public void sethotels(HashMap<String, String> participating_hotels) {
        this.participating_hotels = participating_hotels;
	}
	public HashMap<String, String> gethotels() {
		return participating_hotels;
	}

	
	public void sethotelId(String hotelId) {
	    this.hotelId = hotelId;
	}
	public String gethotelId() {
	     return hotelId;
	}
	

			public HashMap<String, Object> offerJson ()
			{
				HashMap<String, Object> obj = new HashMap<>();
				if(!(this.gettitle().equals("") || this.gettitle().equals(" ")))
				{
					obj.put("Title",this.gettitle());	
				}
				if(!(this.getdescription().equals("") || this.getdescription().equals(" ")))
				{
					obj.put("Description",this.getdescription());	
				}
				if(!(this.getimagePath().equals("") || this.getimagePath().equals(" ")))
				{
					obj.put("Offer Image",this.getimagePath());	
				}
				if(!(this.getvalidity().equals("") || this.getvalidity().equals(" ")))
				{
					obj.put("Validity",this.getvalidity());	
				}
				if(!(this.getlastRepAction().equals("") || this.getlastRepAction().equals(" ")))
				{
					obj.put("Last Replication Action",this.getlastRepAction());	
				}
				if(!(this.gettermsConditions().equals("") || this.gettermsConditions().equals(" ")))
				{
					obj.put("Terms and Conditions",this.gettermsConditions());	
				}
				if(!(this.getredirectionURL().equals("") || this.getredirectionURL().equals(" ")))
				{
					obj.put("Redirection URL",this.getredirectionURL());	
				}
				if(!(this.getcode().equals("") || this.getcode().equals(" ")))
				{
					obj.put("Offer Rate Code",this.getcode());	
				}
				if(!(this.getroundYear().equals("") || this.getroundYear().equals(" ")))
				{
					obj.put("Round the year",this.getroundYear());	
				}
				if(this.getinclusion() != null && !this.getinclusion().isEmpty())
				{
					obj.put("Inclusions",this.getinclusion());	
				}
				
					obj.put("Offer Starts",this.getstart());	
				
					obj.put("Offer Ends",this.getend());	
				if(!(this.gethotelId().equals("") || this.gethotelId().equals(" ")))
				{
					obj.put("Hotel ID",this.gethotelId());	
				}
				if(this.gethotels() != null && !this.gethotels().isEmpty())
				{
					obj.put("Participating Hotels",this.gethotels());	
				}
				
					return obj;
			}
			
			
			
			
			
			
			
			
			
}