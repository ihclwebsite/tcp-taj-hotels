package com.ihcl.core.offerdata;

import java.util.*;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.Component;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.util.Iterator;
import java.lang.Iterable;
import java.io.IOException;
import javax.jcr.Node; 

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.offerdata.Offer;
import com.ihcl.core.offerdata.GlobalOfferService;



@Component(service = DestinationOfferService.class)
public class DestinationOfferService {
	
    @Reference
    private ResourceResolverFactory resolverFactory;
	
    @Reference
    private GlobalOfferService globalOfferService;
    
    private final Logger LOG = LoggerFactory.getLogger(getClass());
    
	public JSONArray getOffers (Page page, ResourceResolver resourceResolver, boolean isAuthor)
	{
	
		JSONObject offerList=new JSONObject();
	    JSONObject  destobj , hotelobj;
	    JSONArray array =new JSONArray();
	    JSONArray hotelarray;
	    String DestName = "";
	    String hotelName = "";
	    String hotelId = "";
	    String pageName = "";
	    boolean loopOffer = true;
	        
		try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page currentPage = page ;
            Node content = currentPage.getContentResource().adaptTo(Node.class);
            
            Iterator<Page> itrd = currentPage.listChildren();
            while(itrd.hasNext()) {
            	destobj = new JSONObject();
            	hotelobj = new JSONObject();
            	
            	currentPage = itrd.next();
            	
            	try {
            	
            		content = currentPage.getContentResource().adaptTo(Node.class);
            	    DestName = content.getProperty("destinationName").getValue().getString();
            	
            	}
            	catch(Exception e) {
            		LOG.error("Error while searching for destinationName property ", e);
            	}
            	
            	 Iterator<Page> itrp = currentPage.listChildren();
            	 
            	 while(itrp.hasNext()) {
            		 currentPage = itrp.next();
            		 
            		 try {
            		 pageName = currentPage.getName();
            		 }
            		 catch(Exception e) {
            			 LOG.error("Error while extracting pageName  ", e);
            		 }
            		 
            		 if(pageName.substring(pageName.lastIndexOf('-') + 1).equals("packages"))
            		 { 
            			 Iterator<Page> itr = currentPage.listChildren();
                     
            			 while(itr.hasNext()) {
            				 currentPage = itr.next();
            				 content = currentPage.getContentResource().adaptTo(Node.class);
            				 hotelName = "";
                       	     hotelId = "";
            				 hotelarray = new JSONArray();
                         	 
                      	     try {
                      	        if(content.hasProperty("holidayHotelName"))
           		    	           {
           		    		
                      	        	hotelName = content.getProperty("holidayHotelName").getValue().getString();
           		    	  	       }
                      	        Node hotel_detail = content.getNode("root").getNode("holidays_hotel_detail");
                      	        String hotelPath = hotel_detail.getProperty("hotelPath").getValue().getString();
                      	        Page hotelPage = resourceResolver.getResource(hotelPath).adaptTo(Page.class);
              				    content = hotelPage.getContentResource().adaptTo(Node.class);;
              				    hotelId = content.getProperty("hotelId").getValue().getString();
                      	        }
                      	      catch(Exception e) {
                      	    	LOG.error("Error while searching for holidayHotelName property under ",DestName, e);
           		    		  }
           		    
                      	     Iterator<Page> itra = currentPage.listChildren();
             	    	     while(itra.hasNext()) {
             	    		  currentPage = itra.next();
             	    		  loopOffer = true;
             	    		 
             	    		  try {
             	    		     content = currentPage.getContentResource().adaptTo(Node.class);
             	    		     
             	    		    if  (isAuthor) {
             	   		    	if(!content.getProperty("cq:lastReplicationAction").getValue().getString().toLowerCase().equals("activate"))
             	   		    		loopOffer = false;
             	   		    	}
             	    		     
             	    		     if(loopOffer)
             	    		        {
             	    			     Offer offer = new Offer();
         			          	
             	    			     String roundOffer = globalOfferService.getRoundTheYear(content);
         			          	     String imagePath = globalOfferService.getImagePath(content);
         			          	     String lastRepAction = globalOfferService.getLastRepAction(content);
         			          	     String redirectionURL = globalOfferService.getRedirectionURL(content);
         			          	     
         			          	     offer.setroundYear(roundOffer);
         			          	     offer.setimagePath(imagePath);
         			          	     offer.setlastRepAction(lastRepAction);
            			             offer.setredirectionURL(redirectionURL);
            			             offer.sethotelId(hotelId);
             	    			  	
            			             try {
         			          	       Node root = content.getNode("root");
         			  	         
         				               Node packagedetails = root.getNode("holidays_hotel_package");
         				           	   
         				               String title = globalOfferService.getTitle(packagedetails);
           			          	       String offerCode = globalOfferService.getOfferCode(packagedetails);
           			          	       String offerStarts = globalOfferService.getOfferStarts(packagedetails);
           			                   String offerEnds = globalOfferService.getOfferEnds(packagedetails); 
         				               String description = globalOfferService.getDescription(packagedetails);
         				               String validity = globalOfferService.getValidity(packagedetails);
         				               JSONObject inclusion = getInclusions(packagedetails);
         				               String termsConditions = globalOfferService.getTermsConditions(packagedetails);
         				        
         				               offer.setdescription(description);
         				               offer.setvalidity(validity);
         				               offer.settermsConditions(termsConditions);
         				               offer.setinclusion(inclusion);
         				               offer.settitle(title);
           			          	       offer.setcode(offerCode);
            			               offer.setstart(offerStarts);
             			               offer.setend(offerEnds);
         			          	
             	    			  	}
         			          	
             	    			  	catch(Exception e) {
             	    			  		LOG.error("Error while searching for holidays_hotel_package node ", e);
             	    			  	}
         			          	
            			             JSONObject Jobj = offer.offerJson();
        			          	     hotelarray.add(Jobj);
             	    		  
             	    		        }
             	    		  }
             	    		  
             	    		 catch(Exception e) {
             	    			LOG.error("Error while searching for lastReplicationAction property or getting jcr:content node", e);
                          	  }
             	    		 }
            
             	    	     if(hotelarray != null && !hotelarray.isEmpty())
                              {
                                 hotelobj.put(hotelName,hotelarray);
                              }
                     
            			 }
            		 }
            	 }
            	 
            	 if(!hotelobj.isEmpty())
                 {
                 destobj.put(DestName,hotelobj);
                 }
                 
            	 if (!destobj.isEmpty())
                 {
                 array.add(destobj);
                 }
            }

		}
		catch(Exception e) {
			LOG.error("Error while fetching jcr:content node of argument page ", e);
		}
		return array;
	}
	
	    public JSONObject getInclusions(Node node)
	    { 
    		JSONObject inclusion = new JSONObject();
    	    String type ="";
    	    String details ="";
    	    try {
    		    Node inclusions = node.getNode("inclusions");	
    		    Iterator<Node> itr = inclusions.getNodes();
	    
    		    while(itr.hasNext()) {
	    		
    		    	Node currentNode = itr.next();
	    		    
    		    	if(currentNode.hasProperty("inclusionImageMap"))
	    		    {
	    		      type = currentNode.getProperty("inclusionImageMap").getValue().getString();
	    		      type = type.substring(0,type.indexOf(':'));
	    		
	    		    }
	    		    if(currentNode.hasProperty("inclusionDescription"))
	    		    {
	    		      details = currentNode.getProperty("inclusionDescription").getValue().getString();
	    		
	    		    }
	    		    inclusion.put(type, details);
	    	 
    		    }
    	
    	    }
    	    catch(Exception e) {
    	    	LOG.error("Error while searching for inclusions node ", e);
    	    }
    			
		    return inclusion;
	    }

	
}

	
