
package com.ihcl.core.offerdata;

import org.apache.sling.api.servlets.ServletResolverConstants;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.util.Iterator;
import java.lang.Iterable;
import java.io.IOException;
import javax.jcr.Node; 

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import com.ihcl.core.offerdata.GlobalOfferService;
import com.ihcl.core.offerdata.DestinationOfferService;
import com.ihcl.core.offerdata.HotelOfferService;
/**
 * Servlet that writes offer details into the response.
 */

@Component(service = Servlet.class,
property = { 
		"sling.servlet.paths=" + "/bin/offerservlet", 
		"sling.servlet.methods=get",
	    "sling.servlet.extensions=json",
		"sling.servlet.selectors=groups", 
		})
 public class Offerservlet extends SlingAllMethodsServlet {
	
  	@Reference
	GlobalOfferService globalOfferService;
	
	@Reference
	HotelOfferService hotelOfferService;
	
	@Reference
	DestinationOfferService destinationOfferService;
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
    @Override
    protected void doGet(final SlingHttpServletRequest req,
            final SlingHttpServletResponse resp) throws ServletException, IOException {

    	JSONObject offerList = new JSONObject();
    	JSONObject Global = new JSONObject() ;
    	JSONObject Hotel = new JSONObject() ;
    	JSONObject Destination = new JSONObject() ;
    	JSONArray array ;
    	
    	resp.setContentType("application/json");

    	String instance = req.getParameter("instance");
    	boolean isAuthor = false;
    	if(instance != null && instance.equalsIgnoreCase("author")) {
    		isAuthor = true ;
    	}
    	
    	ResourceResolver resourceResolver = req.getResourceResolver();

        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page currentPage ;
      
     
        currentPage = pageManager.getPage("/content/tajhotels/en-in/offers");
        array =  globalOfferService.getOffers(currentPage,resourceResolver, isAuthor);   
        if(array != null && !array.isEmpty())
        {
        Global.put("taj",array);
        }
      
        currentPage = pageManager.getPage("/content/seleqtions/en-in/offers");
        array =  globalOfferService.getOffers(currentPage,resourceResolver, isAuthor);   
        if(array != null && !array.isEmpty())
        {
        Global.put("seleqtions",array);
        }
      
        currentPage = pageManager.getPage("/content/vivanta/en-in/offers");
        array =  globalOfferService.getOffers(currentPage ,resourceResolver, isAuthor);   
        if(array != null && !array.isEmpty())
        {
        Global.put("vivanta",array);
        }
      
        currentPage = pageManager.getPage("/content/ama/en-in/offers-and-promotions");
        array =  globalOfferService.getOffers(currentPage,resourceResolver, isAuthor);   
        if(array != null && !array.isEmpty())
        {
        Global.put("ama",array);
        }
      
      
        currentPage = pageManager.getPage("/content/tajhotels/en-in/our-hotels");
        array =  hotelOfferService.getOffers(currentPage , resourceResolver,"taj",isAuthor);   
        if(array != null && !array.isEmpty())
        { 
    	  Hotel.put("Taj",array);
        }
      
        currentPage = pageManager.getPage("/content/seleqtions/en-in/our-hotels");
        array =  hotelOfferService.getOffers(currentPage , resourceResolver,"seleqtions",isAuthor);   
        if(array != null && !array.isEmpty())
        { 
    	  Hotel.put("Seleqtions",array);
        }
      
        currentPage = pageManager.getPage("/content/vivanta/en-in/our-hotels");
        array =  hotelOfferService.getOffers(currentPage , resourceResolver,"vivanta",isAuthor);   
        if(array != null && !array.isEmpty())
        { 
    	  Hotel.put("Vivanta",array);
        }

        currentPage = pageManager.getPage("/content/ama/en-in/our-hotels");
        array =  hotelOfferService.getOffers(currentPage , resourceResolver,"ama",isAuthor);   
        if(array != null && !array.isEmpty())
        { 
    	  Hotel.put("ama",array);
        }
 
      
        currentPage = pageManager.getPage("/content/tajhotels/en-in/taj-holidays/destinations");
        array =  destinationOfferService.getOffers(currentPage , resourceResolver,isAuthor);   
        if(array != null && !array.isEmpty())
        {
        Destination.put("Taj",array);
        }
      
        if(!Global.isEmpty())
        {
        	offerList.put("Global offers" , Global);
        }
        
        if(!Hotel.isEmpty())
       	{
        	offerList.put("Hotel offers" , Hotel);
       	}
        if(!Destination.isEmpty())
        {
        	offerList.put("Destination offers" , Destination);
        }
   
        resp.getWriter().print(offerList);

        
   }
}
        