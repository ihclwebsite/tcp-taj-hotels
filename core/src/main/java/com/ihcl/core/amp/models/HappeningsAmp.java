/**
 *
 */
package com.ihcl.core.amp.models;

import java.util.Iterator;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.shared.services.ResourceFetcherService;

/**
 * @author Nutan
 *
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/happenings/amp" })

public class HappeningsAmp extends SlingSafeMethodsServlet {


    private static final long serialVersionUID = 1L;

    private String TAJHOTELS_CONTENT_PATH_ROOT_PATH = "/content/tajhotels/en-in/jcr:content/root";

    private String VIVANTA_CONTENT_PATH_ROOT_PATH = "/content/vivanta/en-in/jcr:content/root";

    private String SELECTIONS_CONTENT_PATH_ROOT_PATH = "/content/seleqtions/en-in/jcr:content/root";

    private static final Logger LOG = LoggerFactory.getLogger(HappeningsAmp.class);

    @Reference
    ResourceFetcherService resourceFetcherService;

    private String HAPPENINGS_RESOURCETYPE = "tajhotels/components/content/upcoming-events";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private String VIVANTA_DOMAIN = "www.vivantahotels.com";

    private String SELECTIONS_DOMAIN = "www.seleqtionshotels.com";


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response) {


        ResourceResolver resourceResolver;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            String path = TAJHOTELS_CONTENT_PATH_ROOT_PATH;
            if (httpRequest.getServerName().equals(VIVANTA_DOMAIN)) {
                path = VIVANTA_CONTENT_PATH_ROOT_PATH;
            }
            if (httpRequest.getServerName().equals(SELECTIONS_DOMAIN)) {
                path = SELECTIONS_CONTENT_PATH_ROOT_PATH;
            }
            Resource rootResource = resourceResolver.getResource(path);
            Resource happeningResourcePath = resourceFetcherService.getChildrenOfType(rootResource,
                    HAPPENINGS_RESOURCETYPE);
            LOG.debug("The happenings content path is ::" + happeningResourcePath.getPath());
            String eventsResourcePath = happeningResourcePath.getPath() + "/events";
            Resource eventsResource = resourceResolver.getResource(eventsResourcePath);
            Iterator<Resource> iter = eventsResource.listChildren();
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayNode upcomgEventsNode = objectMapper.createArrayNode();

            while (iter.hasNext()) {
                Resource upcomingEventsResource = iter.next();
                ValueMap jcrValueMap = upcomingEventsResource.adaptTo(ValueMap.class);
                objectMapper = new ObjectMapper();
                ObjectNode JsonNode = objectMapper.createObjectNode();
                JsonNode.put("eventImage", getProperty(jcrValueMap, "eventImage", String.class, ""));
                JsonNode.put("eventLinkText", getProperty(jcrValueMap, "eventLinkText", String.class, ""));
                JsonNode.put("eventLinkUrl", getProperty(jcrValueMap, "eventLinkUrl", String.class, ""));
                JsonNode.put("eventOpeningDateDesk",
                        getProperty(jcrValueMap, "eventOpeningDateDesk", String.class, ""));
                JsonNode.put("eventOpeningDateMob", getProperty(jcrValueMap, "eventOpeningDateMob", String.class, ""));
                JsonNode.put("eventTitle", getProperty(jcrValueMap, "eventTitle", String.class, ""));
                JsonNode.put("eventLocation", getProperty(jcrValueMap, "eventLocation", String.class, ""));
                upcomgEventsNode.add(JsonNode);
            }

            ObjectNode finalEventsNode = objectMapper.createObjectNode();
            if (null != upcomgEventsNode) {
                finalEventsNode.put("UpcomingEvents", upcomgEventsNode);
            } else {
                finalEventsNode.put("UpcomingEvents", "No upcoming events");
            }
            response.getWriter().write(finalEventsNode.toString());

        } catch (Exception e) {
            LOG.error("exception caught in doGet method of HappeningsAmp Class " + e.getMessage());
        }

    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }


}
