/**
 *
 */
package com.ihcl.core.amp.models;

import java.util.Iterator;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.jsoup.Jsoup;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.shared.services.ResourceFetcherService;

/**
 * @author Nutan
 *
 */

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/brands/amp" })

public class BrandsAmp extends SlingSafeMethodsServlet {


    private static final long serialVersionUID = 1L;

    private String TAJHOTELS_CONTENT_PATH_ROOT_PATH = "/content/tajhotels/en-in/jcr:content/root";

    private String VIVANTA_CONTENT_PATH_ROOT_PATH = "/content/vivanta/en-in/jcr:content/root";

    private String SELECTIONS_CONTENT_PATH_ROOT_PATH = "/content/seleqtions/en-in/jcr:content/root";

    private static final Logger LOG = LoggerFactory.getLogger(BrandsAmp.class);

    @Reference
    ResourceFetcherService resourceFetcherService;

    private String BRAND_RESOURCETYPE = "tajhotels/components/content/brands-list";

    private String CUSTOM_IMAGE_RESOURCETYPE = "tajhotels/components/content/customimage";

    private String VIVANTA_DOMAIN = "www.vivantahotels.com";

    private String SELECTIONS_DOMAIN = "www.seleqtionshotels.com";


    @Reference
    private ResourceResolverFactory resourceResolverFactory;


    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response) {


        ResourceResolver resourceResolver;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("print server name" + httpRequest.getServerName());
            String path = TAJHOTELS_CONTENT_PATH_ROOT_PATH;
            if (httpRequest.getServerName().equals(VIVANTA_DOMAIN)) {
                path = VIVANTA_CONTENT_PATH_ROOT_PATH;
            }
            if (httpRequest.getServerName().equals(SELECTIONS_DOMAIN)) {
                path = SELECTIONS_CONTENT_PATH_ROOT_PATH;
            }
            Resource rootResource = resourceResolver.getResource(path);
            Resource brandResourcePath = resourceFetcherService.getChildrenOfType(rootResource, BRAND_RESOURCETYPE);
            LOG.debug("The brand content path is ::" + brandResourcePath.getPath());
            Iterator<Resource> iter = brandResourcePath.listChildren();
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayNode brandArrNode = objectMapper.createArrayNode();

            while (iter.hasNext()) {
                Resource brandResource = iter.next();
                if (!brandResource.isResourceType(CUSTOM_IMAGE_RESOURCETYPE))
                    continue;
                ValueMap jcrValueMap = brandResource.adaptTo(ValueMap.class);

                objectMapper = new ObjectMapper();
                ObjectNode JsonNode = objectMapper.createObjectNode();
                JsonNode.put("bgImage", getProperty(jcrValueMap, "bgImage", String.class, ""));
                JsonNode.put("bgImageAlt", getProperty(jcrValueMap, "bgImageAlt", String.class, ""));
                JsonNode.put("brandPath", getProperty(jcrValueMap, "brandPath", String.class, ""));
                String htmlDescription = html2text(getProperty(jcrValueMap, "description", String.class, ""));
                JsonNode.put("description", htmlDescription);
                JsonNode.put("image", getProperty(jcrValueMap, "image", String.class, ""));
                JsonNode.put("imageMob", getProperty(jcrValueMap, "imageMob", String.class, ""));
                JsonNode.put("imageMobAlt", getProperty(jcrValueMap, "imageMobAlt", String.class, ""));
                JsonNode.put("overlay", getProperty(jcrValueMap, "overlay", String.class, ""));
                JsonNode.put("overlayAlt", getProperty(jcrValueMap, "overlayAlt", String.class, ""));
                JsonNode.put("overlayImgCssClass", getProperty(jcrValueMap, "overlayImgCssClass", String.class, ""));

                brandArrNode.add(JsonNode);
            }

            ObjectNode finalBrandsNode = objectMapper.createObjectNode();
            if (null != brandArrNode) {
                finalBrandsNode.put("Brands", brandArrNode);
            } else {
                finalBrandsNode.put("Brands", "No brands available");
            }
            response.getWriter().write(finalBrandsNode.toString());

        } catch (Exception e) {
            LOG.error("exception caught in doGet method of BrandsAmp Class " + e.getMessage());
        }

    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

}
