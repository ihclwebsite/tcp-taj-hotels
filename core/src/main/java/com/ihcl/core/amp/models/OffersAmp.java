

package com.ihcl.core.amp.models;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.ihcl.core.services.search.OffersSearchService;
import com.ihcl.core.shared.services.ResourceFetcherService;

@Component(service = { Servlet.class },
        immediate = true,
        property = { "sling.servlet.paths=/bin/offers/amp" })
public class OffersAmp extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(OffersAmp.class);;

    @Reference
    OffersSearchService offersSearchService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    ResourceFetcherService resourceFetcherService;

    String OFFERS_CONTENT_PATH = "/content/tajhotels/en-in/offers";


    @Override
    protected void doGet(SlingHttpServletRequest httpRequest, SlingHttpServletResponse response) {
        try {
            ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            List<Page> offerList = new ArrayList<Page>();
            List<String> offersPathList = offersSearchService.getAllOffersByRank(OFFERS_CONTENT_PATH + "/");

            LOG.debug("The offers content path is ::" + OFFERS_CONTENT_PATH);
            try {
                for (String path : offersPathList) {
                    offerList.add(pageManager.getContainingPage(path));
                }
                ObjectMapper objectMapper = new ObjectMapper();
                ArrayNode offersArrNode = objectMapper.createArrayNode();
                ObjectNode offersJsonNode = objectMapper.createObjectNode();
                offersArrNode = getOffersArrayNode(offerList, resourceResolver);
                if (null != offersArrNode) {
                    offersJsonNode.put("Offers", offersArrNode);
                } else {
                    offersJsonNode.put("Offers", "No offers available");
                }
                response.getWriter().write(offersJsonNode.toString());
            } catch (Exception e) {
                LOG.error("Exception found while getting the offers :: {} for the list :: {}", e.getMessage(),
                        offersPathList);
                LOG.debug("Exception found while getting the offers :: {} for the list :: {}", e, offersPathList);
            }
        } catch (Exception e) {
            LOG.error("exception caught in doGet method of BrandsAmp Class " + e.getMessage());
        }
    }

    private ArrayNode getOffersArrayNode(List<Page> offerList, ResourceResolver resourceResolver) {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode offersArrNode = objectMapper.createArrayNode();
        LOG.debug("print the size " + offerList.size());
        ObjectNode offersJsonNode = objectMapper.createObjectNode();
        for (Page page : offerList) {
            String offerPath = page.getPath() + "/jcr:content";
            LOG.info("print the offer path " + offerPath);
            Resource offerImageResource = resourceResolver.getResource(offerPath + "/banner_parsys/offerbanner");
            ValueMap imageValueMap = offerImageResource.adaptTo(ValueMap.class);
            objectMapper = new ObjectMapper();
            offersJsonNode = objectMapper.createObjectNode();
            Resource offerRes = resourceResolver.getResource(offerPath);
            ValueMap valueMap = offerRes.adaptTo(ValueMap.class);
            if (valueMap.containsKey("roundTheYearOffer")) {
                offersJsonNode.put("roundTheYearOffer", "Round the year");
            }
            offersJsonNode.put("offerTitle", getProperty(valueMap, "offerTitle", String.class, ""));
            offersJsonNode.put("offerShortDesc", getProperty(valueMap, "offerShortDesc", String.class, ""));
            offersJsonNode.put("bannerImage", getProperty(imageValueMap, "bannerImage", String.class, ""));
            offersJsonNode.put("viewDetails", "VIEW DETAILS");
            offersJsonNode.put("RoundTheYear", "Round the year");

            offersArrNode.add(offersJsonNode);
        }
        return offersArrNode;
    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }


}
