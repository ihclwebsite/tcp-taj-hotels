package com.ihcl.core.amp.models;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.DestinationSearchService;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.tajhotels.constants.CrxConstants;

@Component(service = Servlet.class,
        immediate = true,
        property = { "sling.servlet.paths=" + "/bin/amp/popularDestinations" })
public class PopularDestinationsAmp extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(PopularDestinationsAmp.class);

    private static final long serialVersionUID = -2821052337244401575L;

    @Reference
    private DestinationSearchService destinationSearchService;

    @Override
    protected void doGet(final SlingHttpServletRequest httpRequest, final SlingHttpServletResponse response)
            throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        try {
            List<String> destinationList = destinationSearchService.getDestinationsByRank();
            if (null != destinationList) {
                jsonNode.put("destinations", buildDestionationArrayNode(destinationList, httpRequest));
            } else {
                jsonNode.put("ERROR", "Null Destination List");
            }
        } catch (Exception e) {
            jsonNode.put("ERROR", e.getMessage());
            e.printStackTrace();
        }
        response.getWriter().write(jsonNode.toString());
    }

    private JsonNode buildDestionationArrayNode(List<String> destinationList, SlingHttpServletRequest httpRequest) {

        ResourceResolver resolver = httpRequest.getResourceResolver();
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();
        for (String destinationPath : destinationList) {
            arrayNode.add(buildDestinationNode(destinationPath, resolver));
        }
        return arrayNode;
    }

    private JsonNode buildDestinationNode(String destinationPath, ResourceResolver resolver) {

        String jcrPath = destinationPath + "/jcr:content";
        Resource jcrResource = resolver.getResource(jcrPath);
        LOG.trace("Obtained jcr resource as {}", jcrResource);
        ValueMap jcrValueMap = jcrResource.adaptTo(ValueMap.class);
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        node.put("name", getProperty(jcrValueMap, "jcr:title", String.class, ""));
        node.put("imagePath", getBannerImage(jcrResource));
        node.put("path", getMappedPath(destinationPath, resolver));
        return node;
    }

    private String getBannerImage(Resource jcrResource) {
        Resource bannerParsys = jcrResource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                return String.valueOf(hotelBanner.getValueMap().get("bannerImage"));
            }
        }
        return null;
    }

    private String getMappedPath(String url, ResourceResolver resourceResolver) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }
}
