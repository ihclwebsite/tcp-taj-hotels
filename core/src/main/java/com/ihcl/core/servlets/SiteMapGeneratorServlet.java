/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ihcl.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.SiteMapGeneratorService;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "=Servlet to create a Sitexml",
                "sling.servlet.paths=" + "/bin/generate/sitemap"

        })
public class SiteMapGeneratorServlet extends SlingSafeMethodsServlet {

    private static final Logger logger = LoggerFactory.getLogger(SiteMapGeneratorServlet.class);

    private static final long serialVersionUID = 1L;

    @Reference
    SiteMapGeneratorService siteMapGeneratorService;

    SlingHttpServletRequest request;


    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
            throws ServletException, IOException {

        PrintWriter pw = resp.getWriter();

        try {
            String type = req.getParameter("type");
            String brands = req.getParameter("brandNames");
            String contentRootPath = req.getParameter("contentRootPath");
            logger.info("type of request=" + type + " brands=" + brands + " contentRootPath=" + contentRootPath);

            brands = brands.substring(0, brands.lastIndexOf(","));
            request = req;
            resp.setContentType("text/html; charset=UTF-8");

            String hostString = getHostString();
            List<String> brandNames = new ArrayList<String>();

            for (String brand : brands.split(",")) {
                brandNames.add(brand);
                logger.info("brand name=" + brand);
            }

            Boolean result = true;
            if (!brandNames.isEmpty()) {
                if (type != null && type.equalsIgnoreCase("sitemap")) {
                    if (brandNames.contains("master")) {
                        brandNames.remove("master");
                        if (siteMapGeneratorService.generateGenericSiteMap(hostString)) {
                            result = true;
                        } else {
                            result = false;
                            pw.write("Error Occured while creating master SiteMap");
                        }

                    }

                    if (!brandNames.contains("master") && brandNames.size() > 0) {
                        if (siteMapGeneratorService.generateSectionalSiteMap(hostString, brandNames, contentRootPath)) {
                            result = true;
                        } else {
                            result = false;
                            pw.write("Error Occured while creating SiteMap");
                        }
                    }
                    logger.info("inside sitemap =" + result);

                }


                if (type != null && type.equalsIgnoreCase("imagemap")) {
                    if (brandNames.contains("master")) {
                        brandNames.remove("master");
                        if (siteMapGeneratorService.generateGenericImageSiteMap()) {
                            result = true;
                        } else {
                            result = false;
                            pw.write("Error Occured while creating master Image-SiteMap");
                        }

                    }

                    if (!brandNames.contains("master") && brandNames.size() > 0) {
                        if (siteMapGeneratorService.generateSectionalImageSiteMap(brandNames, contentRootPath)) {
                            result = true;
                            logger.trace("inside image brands result =" + result);
                        } else {
                            result = false;
                            pw.write("Error Occured while creating Image-SiteMap");
                        }
                    }
                    logger.trace("inside image =" + result);

                }
            } else {
                pw.write("Please select one of the values from given checkboxes");
            }
            if (result) {
                logger.trace("inside success condition =" + result);
                pw.write("SiteMap generation was successfull");
            }
        } catch (Exception ex) {
            logger.error("Error/Exception is thrown " + ex.toString(), ex);
            pw.write("Error Occured while creating Image-SiteMap");
        }


        /*
         * if (siteMapGeneratorService.generateSiteMap(hostString)) {
         * pw.write("SiteMap generation was successfull.<br>"); pw.write(
         * "<a href='/content/dam/tajhotels/en-in/our-hotels/bangalore/taj-bangalore/sitemap.xml' target='_blank'>Click to Open</a> <br>"
         * ); // siteMapGeneratorService.generateSectionalSiteMap(hostString);
         *
         *
         * if (siteMapGeneratorService.generateSectionalImageSiteMap()) {
         * pw.write("Image SiteMap generation was successfull.<br>"); pw.write(
         * "<a href='/content/dam/tajhotels/en-in/our-hotels/bangalore/taj-bangalore/image-sitemap.xml' target='_blank'>Click to Open</a> <br>"
         * ); } else { pw.write("Error Occured while creating Image-SiteMap"); }
         *
         *
         * } else { pw.write("Error Occured while creating Link-SiteMap"); }
         */

    }

    private String getHostString() {
        String hostString = request.getScheme() + "://" + request.getServerName();
        if ("localhost".equalsIgnoreCase(request.getServerName()) && request.getServerPort() != 0) {
            hostString += ":" + request.getServerPort();
        }
        return hostString;
    }
}
