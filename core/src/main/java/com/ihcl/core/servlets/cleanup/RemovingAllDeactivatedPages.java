package com.ihcl.core.servlets.cleanup;

import java.io.IOException;
import java.util.HashMap;
import javax.jcr.Node;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Servlet to Remove Deactived Pages",
		"sling.servlet.paths=" + "/bin/remove/deactivated-pages" })
public class RemovingAllDeactivatedPages extends SlingAllMethodsServlet {
	private static final Logger logger = LoggerFactory.getLogger(RemovingAllDeactivatedPages.class);
	private static final long serialVersionUID = 6810672448455086165L;
	private static String DESTINATION_PATH = "/content/tajhotels/en-in/our-hotels/";

	@Reference
	QueryBuilder queryBuilder;

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		ResourceResolver resourceResolver = request.getResourceResolver();
		SearchResult overviews = getPagesWithResourceType(DESTINATION_PATH,
				"cq:Page", request);
		
		for (Hit overview : overviews.getHits()) {
			try {
				String overviewPath = overview.getPath();
				if(resourceResolver.getResource(overviewPath)!=null) {
					ModifiableValueMap modifiableValueMap = resourceResolver.getResource(overviewPath+"/jcr:content").adaptTo(ModifiableValueMap.class);
					if(modifiableValueMap.containsKey("cq:lastReplicationAction")) {
						if(modifiableValueMap.get("cq:lastReplicationAction").equals("Deactivate")) {
							response.getWriter().write("Path Removed : " + overviewPath + "<br>");
							resourceResolver.getResource(overviewPath).adaptTo(Node.class).remove();
							resourceResolver.commit();
						}
					}
				}
			} catch (Exception e) {
				logger.error("Exception : " + e);
				response.getWriter().write("Exception : " + e + "<br>");
			}
		}
	}

	private SearchResult getPagesWithResourceType(String hotelPath, String resourceType,
			SlingHttpServletRequest request) {
		HashMap<String, String> predicates = new HashMap<String, String>();
		predicates.put("path", hotelPath);
		predicates.put("type", "cq:Page");
		predicates.put("p.limit", "-1");
		Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
				request.getResourceResolver().adaptTo(Session.class));
		return query.getResult();
	}

}
