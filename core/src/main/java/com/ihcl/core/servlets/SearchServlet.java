package com.ihcl.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.SpellCheck;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;

@Component(service = { Servlet.class },
        property = { Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword search",
                "sling.servlet.paths=" + "/bin/search", "sling.servlet.paths=" + "GET" })
public class SearchServlet extends SlingAllMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = -286015220226678427L;

    private static final Logger LOG = LoggerFactory.getLogger(SearchServlet.class);

    private static final String REQ_PARAMETER_SEARCH_KEY = "searchText";

    private static final String RESP_JSON_TYPE = "application/json";

    @Reference
    private SearchProvider searchProvider;
    
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    SpellCheck spellCheckService;

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response)
            throws ServletException, IOException {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            String suffixPath = request.getRequestPathInfo().getSuffix();
            LOG.trace("Received suffix path info: {}", suffixPath);
            List<String> suffixes = Arrays.asList(suffixPath.split("/"));
            String searchKeyInReq = suffixes.get(3);
            String contentRootPath = suffixes.get(1).replaceAll(":", "/content/");
			if (StringUtils.isBlank(contentRootPath)) {
				contentRootPath = SearchServiceConstants.PATH.TAJ_ROOT;
			}
            String appendDestName = StringUtils.equals(suffixes.get(4), "result") ? null : suffixes.get(4);
            String otherWebsitesPath = suffixes.get(2).replaceAll(":", "/content/").replaceAll("_", ",");
            LOG.trace(searchKeyInReq + "|" + contentRootPath + "|" + otherWebsitesPath);
            List<String> correctedWords = spellCheckService.performSpellCheck(searchKeyInReq);
            LOG.trace("Corrceted wrds :: {}", correctedWords);
            SearchServletHelper helper = new SearchServletHelper();
            Map<String, Object> categorizedResponse = helper.createSearchResult(resourceResolver, searchKeyInReq, contentRootPath, appendDestName, otherWebsitesPath, correctedWords, searchProvider, request);
            ObjectMapper objectMapper = new ObjectMapper();
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
            response.setContentType(RESP_JSON_TYPE);
            response.getWriter().println(objectMapper.writeValueAsString(categorizedResponse));
        } catch (Exception e) {
            LOG.error("Exception found while getting the search Result :: {}", e.getMessage());
            LOG.debug("Exception found while getting the search Result :: {}", e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
    }

    

}