package com.ihcl.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.SpellCheck;
import com.ihcl.core.services.search.SearchProvider;

@Component(
        service = {Servlet.class},
        property = {
                Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword search",
                "sling.servlet.paths="+ "/bin/investors-search",
                "sling.servlet.paths="+ "GET"
        })
public class InvestorsReportSearchServlet extends SlingAllMethodsServlet {
	
	private static final long serialVersionUID = -8677785166429578481L;

	private static final Logger LOG = LoggerFactory.getLogger(InvestorsReportSearchServlet.class);
	
	private static final String REQ_PARAMETER_SEARCH_KEY = "searchText";
	
	private static final String REQ_PARAMETER_DATE_FILTER = "dateFilters";
	
	private static final String REQ_PARAMETER_DAM_PATHS="damPaths";

    private static final String RESP_JSON_TYPE = "application/json";

    @Reference
    private SearchProvider searchProvider;

    @Reference
    private SpellCheck spellChecker;
    
    private boolean dateFilterFlag = false;
    
    private List<String> dateFilters = new ArrayList<String>();
    
    private List<String> damPaths = new ArrayList<String>();
    
    private ResourceResolver resolver;
    
    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response)
            throws ServletException, IOException {
    	
        String searchKeyInReq =  request.getParameter(REQ_PARAMETER_SEARCH_KEY);
        dateFilters = getDateFiltersFromRequest(request);
        damPaths=getDamPathsFromRequest(request);
        LOG.trace("length of dateFilters from request :: " + dateFilters.size());
        LOG.trace("length of damPaths from request :: " + damPaths.size());
        String searchKey = searchKeyInReq + "*";
        LOG.debug("search key from request :: " + searchKey);
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, ArrayList<Object>> categorizedResponse = new HashMap<>();
        ArrayList<Object> reportsSearchResults = new ArrayList<>();
        List<String> correctedWords = spellChecker.performSpellCheck(searchKeyInReq);

        if (correctedWords.size() > 0) {
            searchKey = correctedWords.get(0)+"*";
        }
        LOG.info("InvestorsReportSearchServlet searchKey = " + searchKey );
        
        response.setContentType(RESP_JSON_TYPE); 
        
        if(0!=damPaths.size()) {
        	for (String damPath : damPaths) {
        		LOG.debug("Dam Path :: " + damPath);
        		if(dateFilters.size()== 0) {
                	String date="";
                	dateFilterFlag=false;
                	LOG.trace("No dateFilters passed, hence setting DateFilterFlag as false");
                	processAndFormResultFromDamPaths(searchKey,damPath,reportsSearchResults,request,date);
        		}
        		else {	
                	dateFilterFlag=true;
                	LOG.trace("DateFilters passed, hence setting DateFilterFlag as true");
                	for (int i=0;i<dateFilters.size();i++) {
                		String date = dateFilters.get(i);
                		processAndFormResultFromDamPaths(searchKey,damPath,reportsSearchResults,request,date);
                	}
                }
        	}
        }
        categorizedResponse.put("reportsSearchResults", reportsSearchResults);
        response.getWriter().println(objectMapper.writeValueAsString(categorizedResponse));   
    }
    
    private List<String> getDateFiltersFromRequest(SlingHttpServletRequest httpRequest) {
    	
    	List<String> dateFilters = new ArrayList<String>();
    	Object dateLs = httpRequest.getParameter(REQ_PARAMETER_DATE_FILTER);
    	LOG.debug("Received dates as: " + dateLs);
    	try {
			if (dateLs != null) {
				String datesString = dateLs.toString();
				if (!datesString.isEmpty()) {
					JSONArray jsonArray = new JSONArray(datesString);
					LOG.debug("Parsed dates to json array as: " + jsonArray);
					int numOfDates = jsonArray.length();
					for (int i = 0; i < numOfDates; i++) {
						String dateFilterString = jsonArray.getString(i);
						dateFilters.add(dateFilterString);
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error occurred while parsing dateFilters from request", e);
		}
		return dateFilters; 	
    }
    
    private List<String> getDamPathsFromRequest(SlingHttpServletRequest httpRequest) {
    	
    	List<String> damPaths = new ArrayList<String>();
    	Object damPathLs = httpRequest.getParameter(REQ_PARAMETER_DAM_PATHS);
    	LOG.debug("Received dam path list as: " + damPathLs);
    	try {
			if (damPathLs != null) {
				String damPathLsString = damPathLs.toString();
				if (!damPathLsString.isEmpty()) {
					JSONArray jsonArray = new JSONArray(damPathLsString);
					LOG.debug("Parsed dam paths to json array as: " + jsonArray);
					int numOfdamPaths = jsonArray.length();
					for (int i = 0; i < numOfdamPaths; i++) {
						String damPathString = jsonArray.getString(i);
						damPaths.add(damPathString);
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error occurred while parsing dampaths from request", e.getMessage());
		}
		return damPaths; 	
    }
    
    private void processAndFormResultFromDamPaths(String searchKey,String damPath,ArrayList<Object> reportsSearchResults,SlingHttpServletRequest request,String date) throws IOException {
    		
    	String updatedDamPath;
    	try {
            HashMap<String, String> predicateMap = new HashMap<>();
            predicateMap.put("fulltext", searchKey);
            if(dateFilterFlag)
            	updatedDamPath=damPath+date;
            else
            	updatedDamPath= damPath;
            LOG.info("InvestorsReportSearchServlet ::: updatedDamPath" + updatedDamPath);
            predicateMap.put("path", updatedDamPath);
            predicateMap.put("1_property", "jcr:primaryType");
            predicateMap.put("1_property.value","dam:Asset");
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            for(Hit hit: searchResult.getHits()) {
            	HashMap<String, String> resultObj = new HashMap<>();
            	   resolver = request.getResourceResolver();
            	   Resource pdfPathResource = resolver.getResource(hit.getPath());
	    		   pdfPathResource=pdfPathResource.getChild("jcr:content").getChild("metadata");
	    		   ValueMap vMap=pdfPathResource.adaptTo(ValueMap.class);
	    		String pdfTitle = vMap.get("jcr:title",String.class);   
            	resultObj.put("name", pdfTitle);
            	resultObj.put("path",hit.getPath());
            	reportsSearchResults.add(resultObj);
            }
        } catch (Exception e) {
            LOG.error("Error while searching for reports with text=", searchKey, e);
        }
    }
}
