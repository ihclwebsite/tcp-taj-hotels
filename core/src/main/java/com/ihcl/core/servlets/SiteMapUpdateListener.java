package com.ihcl.core.servlets;

import java.util.List;

import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Component;


@Component(service = ResourceChangeListener.class,
        property = {
                // filter the notifications by path in the repo. Can be array and supports globs
                ResourceChangeListener.PATHS + "=" + "/content/tajhotels/en-in/our-hotels",
                // The type of change you want to listen to.
                // Possible values at
                // https://sling.apache.org/apidocs/sling9/org/apache/sling/api/resource/observation/ResourceChange.ChangeType.html.
                ResourceChangeListener.CHANGES + "=" + "ADDED", ResourceChangeListener.CHANGES + "=" + "REMOVED",
                ResourceChangeListener.CHANGES + "=" + "CHANGED"

        // PS: If you want to declare multiple values for a prop, you repeat it in OSGI R6 annotations.
        // https://stackoverflow.com/questions/41243873/osgi-r6-service-component-annotations-property-list#answer-41248826
        })
public class SiteMapUpdateListener implements ResourceChangeListener {

    @Override
    public void onChange(List<ResourceChange> resourceChangeList) {

        for (ResourceChange resourceChange : resourceChangeList) {
            String resourcePath = resourceChange.getPath();
            resourcePath.toCharArray();
        }
    }

}
