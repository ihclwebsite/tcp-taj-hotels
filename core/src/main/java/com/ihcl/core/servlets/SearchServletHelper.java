package com.ihcl.core.servlets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.ihcl.core.hotels.SearchBean;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.core.util.URLMapUtil;

public class SearchServletHelper {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(SearchServletHelper.class);


    /**
     * Creates the search result.
     *
     * @param resourceResolver
     *            the resource resolver
     * @param searchKeyInReq
     *            the search key in req
     * @param contentRootPath
     *            the content root path
     * @param appendDestName
     *            the append dest name
     * @param otherWebsitesPath
     *            the other websites path
     * @param correctedWords
     *            the corrected words
     * @param searchProvider
     *            the search provider
     * @return the map
     */
    public Map<String, Object> createSearchResult(ResourceResolver resourceResolver, String searchKeyInReq,
            String contentRootPath, String appendDestName, String otherWebsitesPath, List<String> correctedWords,
            SearchProvider searchProvider, SlingHttpServletRequest request) {
        try {
            String searchKey = searchKeyInReq + "*";
            SearchResult otherWebsiteSearchResult = null;
            if (StringUtils.isNotBlank(otherWebsitesPath) && otherWebsitesPath.contains(",")) {
                String[] paths = otherWebsitesPath.split(",");

                otherWebsiteSearchResult = getSearchResult(searchKey, null, paths, searchProvider);
                if (otherWebsiteSearchResult.getHits().isEmpty() && correctedWords.size() > 0) {
                    LOG.trace("Corrected search key : {}", correctedWords.get(0) + "*");
                    otherWebsiteSearchResult = getSearchResult(correctedWords.get(0) + "*", null, paths,
                            searchProvider);
                    LOG.trace("Others Result with corrected key : {}", otherWebsiteSearchResult.getHits().size());
                }
            }
            SearchResult searchResult = getSearchResult(searchKey, contentRootPath, null, searchProvider);
            if (searchResult.getHits().isEmpty() && correctedWords.size() > 0) {
                LOG.trace("Corrected search key : {}", correctedWords.get(0) + "*");
                searchResult = getSearchResult(correctedWords.get(0) + "*", contentRootPath, null, searchProvider);
                LOG.trace("Result with corrected key : {}", searchResult.getHits().size());
            }
            return getSearchResultsForWebsite(otherWebsiteSearchResult, searchResult, appendDestName, resourceResolver,
                    request);
        } catch (Exception e) {
            LOG.error("Exception occured while getting the searchResult :: {}", e.getMessage());
            LOG.debug("Exception occured while getting the searchResult :: {}", e);
        }
        return null;

    }


    /**
     * Gets the search results for website.
     *
     * @param otherWebsiteSearchResult
     *            the other website search result
     * @param searchResult
     *            the search result
     * @param appendDestName
     *            the append dest name
     * @param resourceResolver
     *            the resource resolver
     * @return the search results for website
     */
    private Map<String, Object> getSearchResultsForWebsite(SearchResult otherWebsiteSearchResult,
            SearchResult searchResult, String appendDestName, ResourceResolver resourceResolver,
            SlingHttpServletRequest request) {
        try {
            Map<String, Object> hotels = new HashMap<>();
            ArrayList<Object> destinations = new ArrayList<>();
            Map<String, Object> restaurants = new HashMap<>();
            Map<String, Object> experiences = new HashMap<>();
            ArrayList<Object> holidayDestination = new ArrayList<>();
            ArrayList<Object> holidayHotel = new ArrayList<>();
            LOG.trace("otherWebsiteSearchResult "+ otherWebsiteSearchResult);
            if (null != searchResult) {
                getAllResultsObject(hotels, destinations, restaurants, experiences, holidayDestination, holidayHotel,
                        searchResult, appendDestName, "website", resourceResolver, request);
            }
            if (null != otherWebsiteSearchResult) {
                getAllResultsObject(hotels, destinations, restaurants, experiences, holidayDestination, holidayHotel,
                        otherWebsiteSearchResult, appendDestName, "others", resourceResolver, request);
            }
            Map<String, Object> categorizedResponse = new HashMap<>();
            ArrayList<Object> suggestions = new ArrayList<>();
            categorizedResponse.put("suggestions", suggestions);
            categorizedResponse.put("destinations", destinations);
            categorizedResponse.put("hotels", hotels);
            categorizedResponse.put("restaurants", restaurants);
            categorizedResponse.put("experiences", experiences);
            categorizedResponse.put("holidays", holidayDestination);
            categorizedResponse.put("holidayHotels", holidayHotel);
            return categorizedResponse;
        } catch (Exception e) {
            LOG.error("Exception in getSearchResultsForWebsite :: {}", e.getMessage());
            LOG.debug("Exception in getSearchResultsForWebsite :: {}", e);
        }
        return null;
    }

    /**
     * Gets the all results object.
     *
     * @param hotelsMap
     *            the hotels map
     * @param destinations
     *            the destinations
     * @param restaurantsMap
     *            the restaurants map
     * @param experiencesMap
     *            the experiences map
     * @param holidayDestination
     *            the holiday destination
     * @param holidayHotel
     *            the holiday hotel
     * @param searchResult
     *            the search result
     * @param appendDestName
     *            the append dest name
     * @param key
     *            the key
     * @param resourceResolver
     *            the resource resolver
     * @return the all results object
     */
    private void getAllResultsObject(Map<String, Object> hotelsMap, ArrayList<Object> destinations,
            Map<String, Object> restaurantsMap, Map<String, Object> experiencesMap,
            ArrayList<Object> holidayDestination, ArrayList<Object> holidayHotel, SearchResult searchResult,
            String appendDestName, String key, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
        ArrayList<Object> hotels = new ArrayList<>();
        ArrayList<Object> restaurants = new ArrayList<>();
        ArrayList<Object> experiences = new ArrayList<>();
        LOG.trace("Result size : {}", searchResult.getHits().size());
        for (Hit hit : searchResult.getHits()) {
            try {
                Resource resultResource = resourceResolver.getResource(hit.getPath());
                String resultResourceType = resultResource.getResourceType();
                String[] arr = resultResource.getPath().split("/jcr:content");
                ValueMap vm = resultResource.adaptTo(ValueMap.class);
                switch (resultResourceType) {
                    case SearchServiceConstants.RESOURCETYPE.HOTELS: {
                    	
                        try {
                            String hotelTitle;
                            if (StringUtils.isBlank(appendDestName) || appendDestName.equals("false")) {
                                hotelTitle = vm.get("navTitle", String.class);
                            } else {
                                hotelTitle = vm.get("hotelName", String.class) + ", "
                                        + vm.get("hotelCity", String.class);
                            }
                            String hotelId = vm.get("hotelId", String.class);
                            String maximumGuests = vm.get("maximumNoOfGuests", String.class);
                            String maximumBeds = vm.get("numberOfBeds", String.class);
                            String path = getMappedPath(arr[0], resourceResolver, request);
                            //added for same day checkout
                            String sameDayCheckout = vm.get("SameDayCheckOut", String.class);
                            String isOnlyBungalowPage = vm.get("isOnlyBungalowPage", String.class);
                            
                            LOG.error("sameDayCheckout ONE::: "+ sameDayCheckout);
                            SearchBean searchBean = new SearchBean(hotelTitle, path, hotelId, maximumGuests,
                                    maximumBeds, sameDayCheckout, isOnlyBungalowPage);
                            LOG.trace("serachBean :: {} ", searchBean);
                            hotels.add(searchBean);
                        } catch (Exception e) {
                            LOG.error("Error while adapting or resolving hotel properties, " + "resource="
                                    + resultResource.getPath());
                        }
                        break;
                    }
                    case SearchServiceConstants.RESOURCETYPE.DESTINATION: {
                        try {
                            if (key.equals("website")) {
                                String title = StringUtils.isNotBlank(vm.get("navTitle", String.class))
                                        ? vm.get("navTitle", String.class)
                                        : vm.get("jcr:title", String.class);
                                String path = getMappedPath(arr[0], resourceResolver, request);
                                SearchBean searchBean = null;
                                if(arr != null && arr[0] != null && arr[0].split("/")[4] != null) {
                                    searchBean = new SearchBean(title, path, null, arr[0].split("/")[4]);
                                } else {
                                    searchBean = new SearchBean(title, path, null, null, null);
                                }
                                destinations.add(searchBean);
                            }
                        } catch (Exception e) {
                            LOG.error("Error while adapting or resolving destination properties, " + "resource="
                                    + resultResource.getPath());
                        }
                        break;
                    }
                    case SearchServiceConstants.RESOURCETYPE.RESTAURANTS: {
                        try {
                            ValueMap restaurantVm = resultResource.getParent().getParent().getParent()
                                    .getChild("jcr:content").getValueMap();
                            String title = vm.get("root/dining_details/diningName", String.class);
                            String navTitle = restaurantVm.get("navTitle", String.class);
                            title = title + ", " + navTitle;
                            String path = getMappedPath(arr[0], resourceResolver, request);
                            SearchBean searchBean = new SearchBean(title, path, null, null, null);
                            restaurants.add(searchBean);
                        } catch (Exception e) {
                            LOG.error("Error while adapting or resolving restaurant properties, " + "resource="
                                    + resultResource.getPath());
                        }
                        break;
                    }
                    case SearchServiceConstants.RESOURCETYPE.EXPERIENCES: {
                        try {
                            ValueMap experienceVm = resultResource.getParent().getParent().getParent().getParent()
                                    .getParent().getChild("jcr:content").getValueMap();
                            String title = vm.get("experienceName", String.class);
                            String navTitle = experienceVm.get("navTitle", String.class);
                            title = title + ", " + navTitle;
                            String path = getMappedPath(arr[0], resourceResolver, request);
                            SearchBean searchBean = new SearchBean(title, path, null, null, null);
                            experiences.add(searchBean);
                        } catch (Exception e) {
                            LOG.error("Error while adapting or resolving experience properties, "
                                    + "resource= {} ---> {}", resultResource.getPath(), e.getMessage());
                        }
                        break;
                    }
                    case SearchServiceConstants.RESOURCETYPE.HOLIDAY_DESTINATION: {
                        try {
                            if (key.equals("website")) {
                                ValueMap holidayVm = resultResource.getParent().getParent().getChild("jcr:content")
                                        .getValueMap();
                                String path = null;
                                String title = holidayVm.get("destinationName", String.class);
                                if (StringUtils.contains(arr[0], "packages")) {
                                    Resource holidayRes = resourceResolver.getResource(arr[0]);
                                    Page resourcePage = holidayRes.adaptTo(Page.class);
                                    Iterator<Page> children = resourcePage.listChildren();
                                    if (children.hasNext()) {
                                        path = getMappedPath(arr[0], resourceResolver, request);
                                    }
                                }
                                if (null != path) {
                                    SearchBean searchBean = new SearchBean(title, path, null, null, null);
                                    holidayDestination.add(searchBean);
                                }
                            }
                        } catch (Exception e) {
                            LOG.error("Error while adapting or resolving holiday destination properties, "
                                    + "resource= {} --> {}", resultResource.getPath(), e.getMessage());
                        }
                        break;
                    }
                    case SearchServiceConstants.RESOURCETYPE.HOLIDAY_HOTELS: {
                        try {
                            if (key.equals("website")) {
                                Resource holidayHotelRes = resourceResolver.getResource(arr[0]);
                                String path = getMappedPath(holidayHotelRes.getParent().getPath(), resourceResolver,
                                        request);
                                String title = vm.get("holidayHotelName", String.class);
                                SearchBean searchBean = new SearchBean(title, path, null, null, null);
                                holidayHotel.add(searchBean);
                            }
                        } catch (Exception e) {
                            LOG.error("Error while resolving holiday hotel properties, " + "resource= {} --> {}",
                                    resultResource.getPath(), e.getMessage());
                        }
                        break;
                    }
                }
            } catch (RepositoryException e) {
                LOG.error("Error while fetching resource {} {}", hit, e.getMessage());
                LOG.debug("Error while fetching resource {} {}", hit, e);
            } catch (Exception e) {
                LOG.error("Exception while getting the resources :: {}", e.getMessage());
                LOG.debug("Exception while getting the resources :: {}", e);
            }
        }
        hotelsMap.put(key, hotels);
        restaurantsMap.put(key, filterTopResults(restaurants, 15));
        experiencesMap.put(key, filterTopResults(experiences, 15));
    }


    /**
     * Filter top results.
     *
     * @param inputList
     *            the input list
     * @param count
     *            the count
     * @return the array list
     */
    private ArrayList<Object> filterTopResults(ArrayList<Object> inputList, int count) {

        if (null != inputList) {
            if (inputList.size() <= count) {
                return inputList;
            } else {
                ArrayList<Object> shortenedList = new ArrayList<>();
                for (int i = 0; i < count; i++) {
                    shortenedList.add(inputList.get(i));
                }
                return shortenedList;
            }
        }
        return null;

    }

    /**
     * Gets the mapped path.
     *
     * @param url
     *            the url
     * @param resourceResolver
     *            the resource resolver
     * @return the mapped path
     */
    private String getMappedPath(String url, ResourceResolver resourceResolver, SlingHttpServletRequest request) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(request, URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }

    /**
     * Gets the search result.
     *
     * @param searchKey
     *            the search key
     * @param contentRootPath
     *            the content root path
     * @param paths
     *            the paths
     * @param searchProvider
     * @return the search result
     */
    private SearchResult getSearchResult(String searchKey, String contentRootPath, String[] paths,
            SearchProvider searchProvider) {
        HashMap<String, String> predicateMap = new HashMap<>();

        try {
            predicateMap.put("fulltext", searchKey);
            if (null != paths) {
                predicateMap.put("1_group.p.or", "true");
                for (int i = 0; i < paths.length; i++) {
                    predicateMap.put("1_group." + i + "_path", paths[i]);
                }

            } else {
                predicateMap.put("path", contentRootPath);
            }
            predicateMap.put("property", "sling:resourceType");
            predicateMap.put("property.1_value", SearchServiceConstants.RESOURCETYPE.HOTELS);
            predicateMap.put("property.2_value", SearchServiceConstants.RESOURCETYPE.DESTINATION);
            predicateMap.put("property.3_value", SearchServiceConstants.RESOURCETYPE.RESTAURANTS);
            predicateMap.put("property.4_value", SearchServiceConstants.RESOURCETYPE.EXPERIENCES);
            predicateMap.put("property.5_value", SearchServiceConstants.RESOURCETYPE.HOLIDAY_DESTINATION);
            predicateMap.put("property.6_value", SearchServiceConstants.RESOURCETYPE.HOLIDAY_HOTELS);
            predicateMap.put("orderby", "@jcr:score");
            predicateMap.put("orderby.sort", "desc");
            predicateMap.put("p.limit", "-1");

        } catch (Exception e) {
            LOG.error("Error while searching for text= {} --> {}", searchKey, e.getMessage());
            LOG.debug("Error while searching for text= {} --> {}", searchKey, e);
        }
        return searchProvider.getQueryResult(predicateMap);

    }


}
