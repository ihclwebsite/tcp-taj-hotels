package com.ihcl.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.services.SpellCheck;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.core.services.search.SearchProvider;

@Component(service = { Servlet.class },
        property = { Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword search",
                "sling.servlet.paths=" + "/bin/hotel-search", "sling.servlet.paths=" + "GET" })
public class HotelSearchServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(HotelSearchServlet.class);

    private static final String REQ_PARAMETER_SEARCH_KEY = "searchText";

    private static final String RESP_JSON_TYPE = "application/json";
    private static final long serialVersionUID = 4096780316336259764L;

    @Reference
    private SearchProvider searchProvider;

    @Reference
    private SpellCheck spellChecker;

    @Reference
    HotelsSearchService hotelSearchService;

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response)
            throws ServletException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, ArrayList<Object>> categorizedResponse = new HashMap<>();
        ArrayList<Object> hotels = new ArrayList<>();
        String searchKeyInReq = request.getParameter(REQ_PARAMETER_SEARCH_KEY);
        String searchKey = searchKeyInReq + "*";

        List<String> correctedWords = spellChecker.performSpellCheck(searchKeyInReq);

        // Adding wildcard to search key
        if (correctedWords.size() > 0) {
            searchKey = correctedWords.get(0) + "*";
        }

        response.setContentType(RESP_JSON_TYPE);
        /**
         * fulltext=mumbai* path=/content/tajhotels orderby=@jcr:score orderby.sort=desc
         * property=jcr:content/sling:resourceType property.2_value=hotelLandingPage
         */
        try {
            /*
             * HashMap<String, String> predicateMap = new HashMap<>(); predicateMap.put("fulltext", searchKey);
             * predicateMap.put("path", SearchServiceConstants.PATH.TAJ_ROOT); predicateMap.put("property",
             * "sling:resourceType"); predicateMap.put("property.1_value", SearchServiceConstants.RESOURCETYPE.HOTELS);
             * predicateMap.put("orderby", "@jcr:score"); predicateMap.put("orderby.sort", "desc"); SearchResult
             * searchResult = searchProvider.getQueryResult(predicateMap);
             */
            com.day.cq.search.result.SearchResult searchResult = hotelSearchService.getHotelsByKey(searchKey);
            for (Hit hit : searchResult.getHits()) {
                HashMap<String, String> resultObj = new HashMap<>();
                Resource resultResource = hit.getResource();
                ParticipatingHotel resultHotel = resultResource.adaptTo(ParticipatingHotel.class);
                resultObj.put("title", resultHotel.getHotelName());
                resultObj.put("path", resultHotel.getHotelPath());
                resultObj.put("city", resultHotel.getHotelCity());
                resultObj.put("id", resultHotel.getHotelId());
                if (null != resultHotel.getJivaSpaEmail()) {
                    resultObj.put("spaEmailId", resultHotel.getJivaSpaEmail());
                    resultObj.put("hotelEmail", resultHotel.getHotelEmail());
                } else {
                    resultObj.put("spaEmailId", resultHotel.getHotelEmail());
                }

                resultObj.put("requestQuoteEmailId", resultHotel.getRequestQuoteEmail());
                hotels.add(resultObj);
            }
        } catch (Exception e) {
            LOG.error("Error while searching for hotels with text=", searchKeyInReq, e);
        }
        categorizedResponse.put("hotels", hotels);
        response.getWriter().println(objectMapper.writeValueAsString(categorizedResponse));
    }
}
