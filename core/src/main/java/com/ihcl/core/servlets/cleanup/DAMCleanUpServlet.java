package com.ihcl.core.servlets.cleanup;

import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword searcb",
		"sling.servlet.paths=" + "/bin/clusterdamImages", "sling.servlet.paths=" + "POST" })
public class DAMCleanUpServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 7133736952581543320L;
	String imageMap = "";

	private static final String rootPath = "content/dam/tajhotels/hotels";

	private static final String[] imagefolders = { "4x3", "16x5", "16x7", "16x9", "3x2" };
	private static final String[] roomMatches = { "rooms_&_suites", "rooms-&_suites", "rooms", "suites", "r&s",
			"bedroom" };
	private static final String[] diningMatches = { "dining", "e&d", "restaurant" };
	private static final String[] meetingsMatches = { "meetings_&_events", "m&c", "m&e", "meeting", "events",
			"ballroom" };
	private static final String[] experienceMatches = { "signature_experiences", "se_" };
	private static final String[] aagMatches = { "aag_" };

	private static final String[] ignoreHotels_lux = { "jcr:content", "hotel-name", "old-wellington-mews",
			"TajWestEnd,Bangalore", "jai-mahal-palace,jaipur", "taj-palace-delhi", "Taj_Safaris", "taj-bangalore",
			"Taj_Amritsar", "taj-exotica-andamans", "taj-city-center-gurugram", "taj-theog", "taj-rishikesh" };

	private static final String[] ignoreHotels_luxury_alreadyStructured = { "taj-bekal-kerala", "TAJ-Bekal-Kerala",
			"old-Goa-Taj-Fort-Aguada", "Old-Taj_Fort_Aguada_Goa", "Taj-Fort_Aguada_Goa_06Feb2016",
			"old-taj-holiday-village_goa", "Taj_Holiday_Village_Goa", "Taj_Holiday_Village_05Feb2016",
			"taj-madikeri-coorg", "taj-kumarakom-kerala", "old-taj-malabar-cochin", "Old-taj-green-cove-kovalam",
			"Taj_GreenCove_Kovalam_06Feb2016", "old-Taj-Yeshwantpur", "old-mg-road,bangalore" };

	private static final String[] imageCategs = { "rooms-and-suites", "restaurants", "signature-experiences", "gallery",
			"meetings-and-events", "offers-and-promotions", "misc", "aag" };
	private static final String ROOMS_SUITS = "rooms-and-suites";
	private static final String DINING = "restaurants";
	private static final String EXPERIENCES = "signature-experiences";
	private static final String GALLERY = "gallery";
	private static final String MEETINGS_AND_EVENTS = "meetings-and-events";
	private static final String OFFERS = "offers-and-promotions";
	private static final String AWARDS = "awards";
	private static final String AAG = "aag";
	private static final String MISCELLANEOUS = "misc";
	ArrayList<String> childNodes = new ArrayList<String>();
	Session session;
	Workspace workspace;
	Logger logger;

	@Override
	protected void doPost(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response)
			throws ServletException, IOException {
		ResourceResolver resourceResolver = request.getResourceResolver();
		session = resourceResolver.adaptTo(Session.class);
		workspace = session.getWorkspace();
		logger = LoggerFactory.getLogger(DAMCleanUpServlet.class);
		try {
			Node rootNode = session.getRootNode().getNode(request.getParameter("damPath"));
			logger.debug("-----------------------------------------");
			logger.debug("START OF A FRESH CLEAN UP RUN");
			logger.debug("-----------------------------------------");
			Iterable it = JcrUtils.getChildNodes(rootNode);
			NodeIterator iterator = (NodeIterator) it.iterator();
			while (iterator.hasNext()) {
				Node hotelNode = iterator.nextNode();
				logger.debug("-----------------------------------------");
				logger.debug("Current Hotel being extracted:");
				logger.debug(hotelNode.getPath());
				logger.debug("-----------------------------------------");
				if (!ignoreHotel(hotelNode.getName())) {
					try {
						Node hotelImageNode = hotelNode.getNode("images");
						extractHotelAssets(hotelImageNode);
					} catch (PathNotFoundException e) {
						logger.error("Could not find image folder in the path" + hotelNode.getPath(), e);
					}

				}
			}
		} catch (PathNotFoundException e) {
			logger.error("Repository Operation Failed", e);
			// response.getWriter().println(e.getMessage());
		} catch (RepositoryException e) {
			logger.error("Repository Operation Failed", e);
			// response.getWriter().println(e.getStackTrace());
		}
		response.getWriter().println(imageMap);
	}

	private Boolean ignoreHotel(String hotelName) {
		for (String hotel : ignoreHotels_lux) {
			if (hotelName.equals(hotel)) {
				return true;
			}
		}
		for (String hotel : ignoreHotels_luxury_alreadyStructured) {
			if (hotelName.equals(hotel)) {
				return true;
			}
		}

		return false;
	}

	private void extractHotelAssets(Node damNodeForHotelImage) throws RepositoryException {
		Iterable it = JcrUtils.getChildNodes(damNodeForHotelImage);
		NodeIterator iterator = (NodeIterator) it.iterator();
		while (iterator.hasNext()) {
			Node c = (Node) iterator.next();
			childNodes.add(c.getPath());
			for (String imageFoldername : imagefolders) {
				if (c.getPath().contains(imageFoldername)) {
					logger.debug("-----------------------------------------");
					logger.debug("Creating New folders in the path" + c.getPath());
					logger.debug("-----------------------------------------");
					createNewFolders(c, imageFoldername);
					logger.debug("-----------------------------------------");
					logger.debug("Clustering images in " + c.getPath());
					logger.debug("-----------------------------------------");
					clusterImages(c);
				}
			}
		}

	}

	private void createNewFolders(Node currentNode, String imageFolderName)
			throws RepositoryException {
		for (String categ : imageCategs) {
			createFolder(currentNode, categ, imageFolderName);
		}
	}

	private void createFolder(Node currentNode, String categ, String imageFolderName) {
		try {
			Node categNode = JcrUtils.getOrAddFolder(currentNode.getParent(), categ);
			Node newFolder_rooms = JcrUtils.getOrAddFolder(categNode, imageFolderName);
			session.save();
		} catch (RepositoryException e) {
			logger.error("Failed on createFolder" + e);
			e.printStackTrace();
		}
	}

	private void copyImage(String oldPath, String newPath, String imageName) {
		try {
			appendImageMap(oldPath + "=" + newPath + "/" + imageName);
			workspace.copy(oldPath, newPath + "/" + imageName);
			logger.debug("Found a Rooms and Suites Image");
			logger.debug("Old Path:" + oldPath);
			logger.debug("New Path:" + newPath);
			logger.debug("___________________________");
			session.save();
		} catch (RepositoryException e) {

			e.printStackTrace();
			// ignoring images that are already present in the new folder. Could be due to
			// partial migration
		}
	}

	private void clusterImages(Node c) throws RepositoryException {
		Iterable it = JcrUtils.getChildNodes(c);
		NodeIterator iterator = (NodeIterator) it.iterator();
		while (iterator.hasNext()) {
			Node image = (Node) iterator.next();
			childNodes.add(image.getPath());
			logger.debug("Processing Image:" + image.getPath());
			if (isRoomsAndSuitesImage(StringUtils.lowerCase(image.getName()))) {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + ROOMS_SUITS + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			} else if (isDiningImage(StringUtils.lowerCase(image.getName()))) {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + DINING + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			} else if (isMeetingEventsImage(StringUtils.lowerCase(image.getName()))) {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + MEETINGS_AND_EVENTS + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			} else if (isExperiencesImage(StringUtils.lowerCase(image.getName()))) {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + EXPERIENCES + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			} else if (isAAGImage(StringUtils.lowerCase(image.getName()))) {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + AAG + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			} else {
				String newPathforImage = image.getParent().getParent().getPath() + "/" + MISCELLANEOUS + "/"
						+ image.getParent().getName();
				copyImage(image.getPath(), newPathforImage, image.getName());
			}

		}
	}

	private boolean isRoomsAndSuitesImage(String imageName) {
		for (String roomMatch : roomMatches) {
			if (imageName.contains(roomMatch)) {
				return true;
			}
		}
		return false;
	}

	private boolean isDiningImage(String imageName) {
		for (String diningMatch : diningMatches) {
			if (imageName.contains(diningMatch)) {
				return true;
			}
		}
		return false;
	}

	private boolean isMeetingEventsImage(String imageName) {
		for (String meetingMatch : meetingsMatches) {
			if (imageName.contains(meetingMatch)) {
				return true;
			}
		}
		return false;
	}

	private boolean isExperiencesImage(String imageName) {
		for (String expMatch : experienceMatches) {
			if (imageName.contains(expMatch)) {
				return true;
			}
		}
		return false;
	}

	private boolean isAAGImage(String imageName) {
		for (String aagMatch : aagMatches) {
			if (imageName.contains(aagMatch)) {
				return true;
			}
		}
		return false;
	}

	private void appendImageMap(String appendString) {
		imageMap = imageMap + "\n" + appendString;
	}

}
