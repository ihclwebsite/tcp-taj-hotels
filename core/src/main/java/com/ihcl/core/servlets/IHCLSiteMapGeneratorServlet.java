/**
 *
 */
package com.ihcl.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.IHCLSiteMapGeneratorService;


@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "=Servlet to create a ihcl Sitexml",
                "sling.servlet.paths=" + "/bin/generate/sitemap/ihcl"

        })
public class IHCLSiteMapGeneratorServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;


    private static final Logger logger = LoggerFactory.getLogger(SiteMapGeneratorServlet.class);


    @Reference
    IHCLSiteMapGeneratorService siteMapGeneratorService;

    SlingHttpServletRequest request;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
            throws ServletException, IOException {


        PrintWriter pw = resp.getWriter();
        try {
            request = req;
            resp.setContentType("text/html; charset=UTF-8");
            String hostString = getHostString();
            if (siteMapGeneratorService.generateSiteMap(hostString)) {
                pw.write("SiteMap generation was successfull");
            } else {
                pw.write("Error Occured while creating SiteMap");
            }

            if (siteMapGeneratorService.generateImageSiteMap()) {
                pw.write("SiteMap generation was successfull");
            } else {
                pw.write("Error Occured while creating SiteMap");
            }
        } catch (Exception e) {
            logger.error("Error/Exception is thrown " + e.toString(), e);
            pw.write("Error Occured while creating SiteMap");
        }
    }


    private String getHostString() {
        String hostString = request.getScheme() + "://" + request.getServerName();
        if ("localhost".equalsIgnoreCase(request.getServerName()) && request.getServerPort() != 0) {
            hostString += ":" + request.getServerPort();
        }
        return hostString;
    }


}
