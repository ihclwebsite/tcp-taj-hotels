package com.ihcl.core.servicehandler.processpayment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.RsaConfigurationService;
import com.ihcl.core.services.config.RsaServiceConfiguration;

public class OpelGenerateSignatureImpl {
	

	
	 private static final Logger LOG = LoggerFactory.getLogger(OpelGenerateSignatureImpl.class);
	 public String keyContent1 = "-----BEGIN RSA PRIVATE KEY-----\r\nMIIEpQIBAAKCAQEA2R+5+b1HZSNNMotnr7Z/5By4NSTZW4dDMGDy2huOSLn1EF6v\r\n75sssdY5kRkFoihLIeNQA+yzsi0kzpz3rCCCmo1DJwgoqA48JVQwwBjZ9SHeC0nE\r\n66VODmMJJGNWe1quHWQb3otIzS+U+rtd1Alzo9up8u8e+FrecyjO6fBMZfd32iO7\r\nqPtExtA1XDtKMqoRbHMiAz940xA5+BLmJC+gp1IYsVce2KA5BW1laPxbku42aQR7\r\neZipSa3BYRY8m964Aj6vLj4kTeTbrc4OH7yatRdWbVbrwVWpg936g8Q3Qf3jQY+H\r\nMu76l1WeXK4GkPkA+oJXY6ag1XhhqtOrLw3rJwIDAQABAoIBAQCjy2thG4lgouD5\r\n4HC3/dU9IO1WKhZPFht5w6lxIJiWBLL7RnMzLrzo69NBwr6dNgh36CPU0hw9rhC2\r\nTXQKRfxA25BtQZpqLVLyVjDwuc6zPnljyqLjojDgaZXb/ZSgOihfw8XCfRDOubaJ\r\n8A84hmjWlEABJKMYeHSYK5Dsqnr37/Oj4OT2NWLaRx8Kk0HPuv/bxx3MHurIHRtG\r\n514UJZcOfN8Ti69/DoYbtb/Mpg/djXr5s47TafxPa8jyT2E8nWboPvYPDQcdu2CI\r\nE0mbrj2C0Ak7g20ZYzm9HCbJHVG+2rPSjVgXKW2ZgXoZflte+G5vRfDrZH+TZuo+\r\nja0pVlIBAoGBAP4ZEtdpRJp5kvj7bUIkXd5E6lrxd2M0VprfMhHk0i+Z1p1nF8St\r\nF6p9uIGuRIEthvdRZVy56fWCHXOfmquRJDHazQZVnnXcRaqhuMYdZoJ3i1KkmSL4\r\nX/SdBsB4rY4FQZWNtwKoSeDugQeJzf4bhyn0iZGbWPq+XO70MxXya8CfAoGBANq/\r\nzL6ul+G6i/nXrfzwUr83EtXh6Zoj51YBK4g3ZIIuWkWvbo9NguV3p9KmeRKMWwYO\r\nRHC7aVwpHdjzOyzmSFdmC+5dqVe6rkdl9AzxpKt0p0rOznmZUhDcdElCk0p6pC5R\r\nQDAt2PA4aR3kT+9z2dPV0IHsUGiouF/LtmTmdCB5AoGAIShUdRefhCjpLORiVYc5\r\nWI/VpRhtY9yokH0fo4Ygh2Wjw9Z4G4oa1HyjXwjGl7TBL/THLVp1VTwta7EgFdNS\r\nzc6ngnQZwXeE/8cqvW+IuO2wmJAyC4Ytv1XeU69rtmSpMkLT5tzfByMYY0twPgCJ\r\nmsf2S7Hh4paEugnTwMFpnjECgYEAoTqc/i5RY97LLOr7ImM/mhBNobdRJnswFwPl\r\nwhCR1CG2B4a2Rokq4VbAK1LoCfPJYz1A1JZNoc/sX+tmwkE5MLHWOWpvVmoR6i4L\r\nIz83z+e7JjgnlxialDLowtZ/GXYrbLgWR2yDaQsq7w1InYUWGDyP4jL7USiKPJE5\r\nbkUtcoECgYEAwhbb1NxzteIr8zlytMj52sgeiJPQRjbU5CoMAJuiHvYHT8jQwso7\r\nlfbz+fXdQamU29v1Hdhc2JR1xWxrTz4bAt1l9lWK8zQBTK3SOlhyvrvNkKtTwjan\r\nsR6+uwB9KY5mrF++pRA8IL2f0yhx2uqwDkX/Og6ZnFHJn3BvQM/DWPg=\r\n-----END RSA PRIVATE KEY-----\r\n";
  
	// String keyContent = rsaconfig.privateKey();
		
 	 public String generateSignatureForPayload(String orderPayloadString,
			Boolean urlEncodeSignature) throws Exception{
		// TODO Auto-generated method stub
 	//	String keyContent = config.privateKey();
		 LOG.debug("KeyCOntent "+keyContent1);
		JSONObject jsonNode = new JSONObject();
		 Signature rsa = Signature.getInstance("SHA256withRSA");
		    PrivateKey privateKey = null;
		    orderPayloadString = URLDecoder.decode(orderPayloadString);
		    if (keyContent1.startsWith("--")) {
		      privateKey = readPrivateKey(keyContent1);
		    } else {
		      privateKey = readPrivateKey(keyContent1);
		    } 
		    rsa.initSign(privateKey);
		    rsa.update(orderPayloadString.getBytes());
		    byte[] sign = rsa.sign();
		    if (urlEncodeSignature.booleanValue()) {
		      String signature = URLEncoder.encode(DatatypeConverter.printBase64Binary(sign), "UTF-8");
		      LOG.debug("Signature value:- " + signature);
		      LOG.debug("Order PAyload value:- " + orderPayloadString);
		      orderPayloadString = orderPayloadString.replaceAll("\\\\", "");
		      jsonNode.put("signature", signature);
		      jsonNode.put("OrderPayload", orderPayloadString);
		      
		    }
		
		return jsonNode.toString();
	}

		public String generateSignatureForPayload(String orderPayloadString, String keyContent, Boolean urlEncodeSignature) throws Exception {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
		 	//	String keyContent = config.privateKey();
				 LOG.debug("KeyCOntent "+keyContent);
				JSONObject jsonNode = new JSONObject();
				 Signature rsa = Signature.getInstance("SHA256withRSA");
				    PrivateKey privateKey = null;
				    orderPayloadString = URLDecoder.decode(orderPayloadString);
				    if (keyContent.startsWith("--")) {
				      privateKey = readPrivateKey(keyContent);
				    } else {
				      privateKey = readPrivateKey(keyContent);
				    } 
				    rsa.initSign(privateKey);
				    rsa.update(orderPayloadString.getBytes());
				    byte[] sign = rsa.sign();
				    if (urlEncodeSignature.booleanValue()) {
				      String signature = URLEncoder.encode(DatatypeConverter.printBase64Binary(sign), "UTF-8");
				      LOG.debug("Signature value:- " + signature);
				      LOG.debug("Order PAyload value:- " + orderPayloadString);
				      orderPayloadString = orderPayloadString.replaceAll("\\\\", "");
				      jsonNode.put("signature", signature);
				      jsonNode.put("OrderPayload", orderPayloadString);
				      
				    }
				
				return jsonNode.toString();
		}
	
	  public static PrivateKey readPrivateKeyFromFile(String filename) throws Exception {
		    File keyFile = new File(filename);
		    BufferedReader reader = new BufferedReader(new FileReader(keyFile));
		    StringBuffer fileContent = new StringBuffer();
		    Boolean isPkcs1Content = Boolean.valueOf(false);
		    String line;
		    while ((line = reader.readLine()) != null) {
		      if (!line.startsWith("--")) {
		        fileContent.append(line).append("\n");
		        continue;
		      } 
		      if (!isPkcs1Content.booleanValue() && line.startsWith("-----BEGIN RSA PRIVATE KEY-----"))
		        isPkcs1Content = Boolean.valueOf(true); 
		    } 
		    byte[] keyBytes = DatatypeConverter.parseBase64Binary(fileContent.toString());
		    return generatePrivateKey(keyBytes, isPkcs1Content);
		  }
		  
		  public static PrivateKey readPrivateKey(String content) throws Exception {
		    LOG.info("Inside readPrivateKey method");
		    Boolean isPkcs1Content = Boolean.valueOf(false);
		    if (content.startsWith("-----BEGIN RSA PRIVATE KEY-----"))
		      isPkcs1Content = Boolean.valueOf(true); 
		    content = content.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "");
		    content = content.replaceAll("-----BEGIN PRIVATE KEY-----", "");
		    content = content.replaceAll("-----END RSA PRIVATE KEY-----", "");
		    content = content.replaceAll("-----END PRIVATE KEY-----", "");
		    byte[] keyBytes = DatatypeConverter.parseBase64Binary(content.toString());
		    return generatePrivateKey(keyBytes, isPkcs1Content);
		  }
		  
		  private static PrivateKey generatePrivateKey(byte[] keyBytes, Boolean isPkcs1Content) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
			    LOG.info("Inside generatePrivateKey method");
			    PrivateKey privateKey = null;
			    if (isPkcs1Content.booleanValue()) {
			      RSAPrivateCrtKeySpec keySpec = getRSAKeySpec(keyBytes);
			      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			      privateKey = keyFactory.generatePrivate(keySpec);
			    } else {
			      PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
			      KeyFactory kf = KeyFactory.getInstance("RSA");
			      privateKey = kf.generatePrivate(spec);
			    } 
			    LOG.info("privateKey value" + privateKey);
			    return privateKey;
			  }
		  
		  private static RSAPrivateCrtKeySpec getRSAKeySpec(byte[] keyBytes) throws IOException {
			    LOG.info("Inside rsaKeySpec method");
			    DerParser parser = new DerParser(keyBytes);
			    Asn1Object sequence = parser.read();
			    if (sequence.getType() != 16)
			      throw new IOException("Invalid DER: not a sequence"); 
			    parser = sequence.getParser();
			    parser.read();
			    BigInteger modulus = parser.read().getInteger();
			    BigInteger publicExp = parser.read().getInteger();
			    BigInteger privateExp = parser.read().getInteger();
			    BigInteger prime1 = parser.read().getInteger();
			    BigInteger prime2 = parser.read().getInteger();
			    BigInteger exp1 = parser.read().getInteger();
			    BigInteger exp2 = parser.read().getInteger();
			    BigInteger crtCoef = parser.read().getInteger();
			    RSAPrivateCrtKeySpec keySpec = new RSAPrivateCrtKeySpec(modulus, publicExp, privateExp, prime1, prime2, exp1, exp2, crtCoef);
			    LOG.info("RSAPrivateCrtKeySpec value" + keySpec);
			    return keySpec;
			  }



}
