/**
 *
 */
package com.ihcl.core.servicehandler.processpayment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
//import com.ihcl.core.services.config.OpelPaymentConfigurationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Guest;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.integration.dto.details.Room;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.core.servicehandler.processpayment.OpelGenerateSignatureImpl;
import com.ihcl.core.services.config.RsaConfigurationService;
import com.ihcl.core.services.config.RsaServiceConfiguration;

/**
 * This class has the utilities to handle payment processing while reserving a room
 *
 */
public class PaymentHandler {
	
//	 @Reference
	  //  OpelPaymentConfigurationService config;

	 @Reference
	 RsaConfigurationService config;
	 
    private static final Logger LOG = LoggerFactory.getLogger(PaymentHandler.class);

   // public String privateKey = config.getPrivateKey();
   
    public String getPayementDetails(String itineraryNumber, BookingObject bookingObject,
            TajHotelBrandsETCConfigurations etcWebsiteConfig) throws ParseException {

        LOG.debug("---> Inside PaymentHandler.getPayementDetails method <---");
        LOG.info("PaymentHandler getPayementDetails method  : conf test :: {}",
                etcWebsiteConfig.getCcAvenueWorkingKey());
        LOG.info("PaymentHandler getPayementDetails method  : conf test :: {}",
                etcWebsiteConfig.getPaymentMerchantId());
        String paymentString = "";
        Map<String, String> paymentdetails = new LinkedHashMap<>();
        String hotelAndChain = null;
        String ticRoomRedemption = "";
        if (bookingObject.getPayments().getTicpoints() != null
                && bookingObject.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                && bookingObject.getPayments().getTicpoints().getPointsType().equals("TIC")) {
            ticRoomRedemption = ticRoomRedemption + "TIC-"
                    + bookingObject.getPayments().getTicpoints().getNoTicPoints();
            if (bookingObject.getGuest() != null && bookingObject.getGuest().getMembershipId() != null
                    && !bookingObject.getGuest().getMembershipId().isEmpty()) {
                ticRoomRedemption = ticRoomRedemption + "-" + bookingObject.getGuest().getMembershipId();
            }

        } else if (bookingObject.getPayments().getTicpoints() != null
                && bookingObject.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()
                && bookingObject.getPayments().getTicpoints().getPointsType().equals("EPICURE")) {
            ticRoomRedemption = ticRoomRedemption + "EPICURE-"
                    + bookingObject.getPayments().getTicpoints().getNoEpicurePoints();
            if (bookingObject.getGuest() != null && bookingObject.getGuest().getMembershipId() != null
                    && !bookingObject.getGuest().getMembershipId().isEmpty()) {
                ticRoomRedemption = ticRoomRedemption + "-" + bookingObject.getGuest().getMembershipId();
            }

        } else if (bookingObject != null && bookingObject.getRoomList() != null && bookingObject.getPayments() != null
                && bookingObject.getPayments().isPayGuaranteeAmount()) {
            float totalGuaranteeAmount = 0;
            for (Room room : bookingObject.getRoomList()) {
                if (room.getGuarantee() != null && room.getGuarantee().getAmount() != null
                        && room.getGuarantee().getPercentage() != null) {
                    if (room.getGuarantee().getPercentage().equalsIgnoreCase("100")) {
                        totalGuaranteeAmount = totalGuaranteeAmount + Float.valueOf(room.getRoomCostAfterTax());
                    } else {
                        totalGuaranteeAmount = totalGuaranteeAmount + Float.valueOf(room.getGuarantee().getAmount());
                    }
                }
            }
            bookingObject.setTotalAmountAfterTax(String.valueOf(totalGuaranteeAmount));
        }

        hotelAndChain = bookingObject.getHotelId() + "-" + bookingObject.getChainCode();
        paymentdetails.put(BookingConstants.CCAVENUE_MERCHANT_ID_NAME, etcWebsiteConfig.getPaymentMerchantId());
        paymentdetails.put(BookingConstants.CCAVENUE_ORDER_ID_NAME, itineraryNumber);
        paymentdetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, bookingObject.getCurrencyCode());
        paymentdetails.put(BookingConstants.CCAVENUE_AMOUNT_NAME, bookingObject.getTotalAmountAfterTax());
        paymentdetails.put(BookingConstants.CCAVENUE_REDIRECT_URL_NAME, etcWebsiteConfig.getCcAvenueRedirectUrl());
        paymentdetails.put(BookingConstants.CCAVENUE_CANCEL_NAME, etcWebsiteConfig.getCcAvenueRedirectUrl());
        paymentdetails.put(BookingConstants.CCAVENUE_LANGUAGE_NAME, BookingConstants.CCAVENUE_PARAM_LANGUAGE);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME,
                BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_PROMO_CODE_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_TID_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);

        paymentdetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, addingTwoStringsForMerchantParams("WEB",
                addingMultipleReservationNumbers(bookingObject), " - ", false));

        paymentdetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, addingGuestFirstAndLastName(bookingObject));

        paymentdetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, addingTwoStringsForMerchantParams(
                bookingObject.getCheckInDate(), bookingObject.getCheckOutDate(), " - ", true));

        paymentdetails.put(BookingConstants.CCAVENUE_MERCH4_NAME, addingRatePlanCodes(bookingObject));

        String merch5Data = addingTwoStringsForMerchantParams(hotelAndChain,
                addingAllRoomsRatePlanCodeAndPromoCode(bookingObject), "_-_", false);
        if (bookingObject.getGuest() != null && bookingObject.getGuest().getMembershipId() != null
                && !bookingObject.getGuest().getMembershipId().isEmpty()) {
            merch5Data = addingTwoStringsForMerchantParams(merch5Data, ticRoomRedemption, "_-_", false);
        }
        paymentdetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, merch5Data);

        if (null != bookingObject.getHotel() && StringUtils.isNotBlank(bookingObject.getHotel().getHotelLocationId())) {
            paymentdetails.put(BookingConstants.CCAVENUE_SUB_ACCOUNT_ID, bookingObject.getHotel().getHotelLocationId());
        }


        paymentString = parsePaymentParameters(paymentdetails);
        LOG.debug("hotelnchainid: {}", hotelAndChain);
        LOG.debug("The final redirect request String: {}", paymentString);
        LOG.debug("PaymentHandler : Leaving getPaymentDeatils");

        return paymentString;
    }

    public String parsePaymentParameters(Map<String, String> paymentDetails) {
        String paymentString = "";

        LOG.debug("--------------------Setting request String--------------------");
        Iterator<Map.Entry<String, String>> iterator = paymentDetails.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            try {
                paymentString = paymentString.concat(entry.getKey() + "="
                        + URLEncoder.encode(entry.getValue(), BookingConstants.CHARACTER_ENCOADING) + "&");
                LOG.debug("Key : " +entry.getKey()+" Value " + entry.getValue());
                LOG.error("Key : " +entry.getKey()+" Value" + entry.getValue());
                LOG.trace("Key : " + entry.getKey()+" Value" + entry.getValue());
                LOG.info("Key : " + entry.getKey()+" Value" + entry.getValue());
                LOG.debug("paymentString" + paymentString);
                LOG.error("paymentString" + paymentString);
                LOG.trace("paymentString" + paymentString);
                LOG.info("paymentString" + paymentString);
            } catch (UnsupportedEncodingException e) {
                LOG.error("Error occurred while converting the payment param to string : ", e);
            }
        }
        return paymentString;
    }

    private String addingRatePlanCodes(BookingObject bookingObject) throws ParseException {
        String ratePlanCodes = "";
        if (bookingObject != null && bookingObject.getRoomList() != null) {
            for (int m = 0; m < bookingObject.getRoomList().size(); m++) {
                if (StringUtils.isNotBlank(bookingObject.getRoomList().get(m).getReservationNumber())) {
                    if (StringUtils.isNotBlank(ratePlanCodes)) {
                        ratePlanCodes = addingTwoStringsForMerchantParams(ratePlanCodes,
                                bookingObject.getRoomList().get(m).getRatePlanCode(), " - ", false);
                    } else {
                        ratePlanCodes = bookingObject.getRoomList().get(m).getRatePlanCode();
                    }
                }
            }
        }
        return ratePlanCodes;

    }

    private String addingMultipleReservationNumbers(BookingObject bookingObject) throws ParseException {
        String mutiResvationNumberString = "";
        for (int g = 0; g < bookingObject.getRoomList().size(); g++) {
            if (StringUtils.isNotBlank(bookingObject.getRoomList().get(g).getReservationNumber())) {
                if (StringUtils.isNotBlank(mutiResvationNumberString)) {
                    mutiResvationNumberString = addingTwoStringsForMerchantParams(mutiResvationNumberString,
                            bookingObject.getRoomList().get(g).getReservationNumber(), "_", false);
                } else {
                    mutiResvationNumberString = bookingObject.getRoomList().get(g).getReservationNumber();
                }
            }
        }
        return mutiResvationNumberString;

    }

    private String addingGuestFirstAndLastName(BookingObject bookingObject) {
        String fullName = "";
        if (bookingObject.getGuest() != null && StringUtils.isNotBlank(bookingObject.getGuest().getFirstName())) {
            fullName = bookingObject.getGuest().getFirstName();
        }
        if (bookingObject.getGuest() != null && StringUtils.isNotBlank(bookingObject.getGuest().getLastName())) {
            if (StringUtils.isNotBlank(fullName)) {
                return fullName.concat(" ").concat(bookingObject.getGuest().getLastName());
            } else {
                return bookingObject.getGuest().getLastName();
            }
        }
        return fullName;

    }

    private String changingDateStringFormat(String dateString) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
        Date date = format1.parse(dateString);
        return format2.format(date);
    }

    private String addingTwoStringsForMerchantParams(String string1, String String2, String stringSeperater,
            boolean isITDate) throws ParseException {
        if (isITDate)
            return changingDateStringFormat(string1) + stringSeperater + changingDateStringFormat(String2);
        return string1 + stringSeperater + String2;

    }

    private String addingAllRoomsRatePlanCodeAndPromoCode(BookingObject bObject) throws ParseException {
        String roomRPCandPC = "";
        for (int g = 0; g < bObject.getRoomList().size(); g++) {
            if (StringUtils.isNotBlank(bObject.getRoomList().get(g).getReservationNumber())) {
                String addedString = addingTwoStringsForMerchantParams(bObject.getRoomList().get(g).getRatePlanCode(),
                        bObject.getRoomList().get(g).getPromoCode(), "-", false);
                if (StringUtils.isNotEmpty(roomRPCandPC)) {
                    roomRPCandPC = addingTwoStringsForMerchantParams(roomRPCandPC, addedString, "_", false);
                } else {
                    roomRPCandPC = addedString;
                }
            }

        }
        return roomRPCandPC;

    }

    public String getCCAvenueDetails(String resNumString, BookingObject bookingObject,
            TajHotelBrandsETCConfigurations etcWebsiteConfig) throws ParseException {

        LOG.info("---> Inside PaymentHandler.getCCAvenueSetails method <---");
        String paymentParameter = getPayementDetails(resNumString, bookingObject, etcWebsiteConfig);

        LOG.info("paymentParameter : " +paymentParameter);
        LOG.error("paymentParameter : " +paymentParameter);
        LOG.trace("paymentParameter : " +paymentParameter);
        LOG.info("paymentParameter : " +paymentParameter);
        LOG.debug("---> Encrypting information <---");
        LOG.debug("working key : {}", etcWebsiteConfig.getCcAvenueWorkingKey());
        LOG.debug("Access Code: {}", etcWebsiteConfig.getCcAvenueAccessKey());

        String encRequest = encodePaymentRequest(paymentParameter, etcWebsiteConfig.getCcAvenueWorkingKey());
        LOG.debug("Encrypted encRequest:---> {}", encRequest);
        LOG.debug("---> Passing control to Payment processing through ajax <---");
        return generateSignedPayload(bookingObject,resNumString,etcWebsiteConfig);
       // return createPaymentForm(etcWebsiteConfig.getCcAvenueUrl(), encRequest,
             //   etcWebsiteConfig.getCcAvenueAccessKey());
    //    commenting create payment form method for opel integration
       
    }

    private String generateSignedPayload(BookingObject bookingObject, String resNumString, TajHotelBrandsETCConfigurations etcWebsiteConfig){
		// TODO Auto-generated method stub
    	LOG.info("Inside generate signed payload method");
    	 Map<String, String> orderPayloadDetails = new LinkedHashMap<>();
    	 ObjectMapper mapperObj = new ObjectMapper();
        String orderPayloadString = "";
        String signature = "";
        String promoCode = "";
        String authCode ="";
        String custmHash ="";
       // String merchantParam2 ="";
        OpelGenerateSignatureImpl opelsignedpayload = new OpelGenerateSignatureImpl();
    	String reservationNumber = null;
    //	String orderId = bookingObject.getItineraryNumber();
    //	String amount = bookingObject.getTotalAmountAfterTax();
    	String hotelId = bookingObject.getHotelId();
    	String chaninCode = bookingObject.getChainCode();
    	String chkInDate = bookingObject.getCheckInDate();
    	String checkOutDate = bookingObject.getCheckOutDate();
    	LOG.info("checkinDat "+checkOutDate);
    	String ratePlanCode = null;
    	List<Room> rooms = bookingObject.getRoomList();
    	 for (Room room : rooms) {
    		  reservationNumber = room.getReservationNumber();
    		  ratePlanCode = room.getRatePlanCode();
    		  promoCode = room.getPromoCode();
    		
    	 }
  //  	 String phonenumber = bookingObject.getHotel().getHotelPhoneNumber();
    	 String hotelLocId = bookingObject.getHotel().getHotelLocationId();
    	 Guest guest = bookingObject.getGuest();
    //	 String custm_phone = guest.getPhoneNumber();
    //	 String email = guest.getEmail();
    	 String firstName = guest.getFirstName();
    	 String lastName = guest.getLastName();
    	 String name =firstName.concat(" ").concat(lastName); 
    	 LOG.debug("name "+name);
    	//hgv.contains(reservationNumber)
    	//hgv.getReservationNumber()
    	 String chkdate = "(".concat(chkInDate).concat(",").concat(checkOutDate).concat(")");
    	 String udfCheckIncheckOutDate = chkInDate.concat(" ").concat(checkOutDate);
    	 String confirmationNumber = "WEB".concat(" ").concat(reservationNumber);
    	 String merchantParam = "(".concat(hotelId).concat(",").concat(chaninCode).concat(",").concat(ratePlanCode).concat(")");
    	 String redirectUrl = "https://author-taj-stage63.adobecqms.net/bin/postPaymentServletOpel";  //"http://localhost:4502/bin/postPaymentServletOpel";
    	 Date date1 = new Date();
    	 long timestamp = date1.getTime() / 1000L; 	
    	 String timestampString = Long.toString(timestamp);
    	 //Random rnum = new Random();
    	// int randomNumber = rnum.nextInt(99);
    	// String randomNo = Integer.toString(randomNumber);
    	// String uniqueID = randomNo+timestampString;
    	 LOG.debug("name "+name);
    	 if(null != bookingObject.getPayments() && null != bookingObject.getPayments().getTicpoints() && bookingObject.getPayments().getTicpoints().isRedeemTicOrEpicurePoints()) {
    		int ticPoint= bookingObject.getPayments().getTicpoints().getNoTicPoints();
    		String noOfTICPoints =Integer.toString(ticPoint);
    		 orderPayloadDetails.put(BookingConstants.OPEL_UDF10, noOfTICPoints);
    	 }
    		//LOG.info("loggebd in user data "+bookingObject.getLoggedInUser().toString());
    	 LOG.info("authtoken "+bookingObject.getGuest().getAuthToken());
    	 LOG.debug("cstmhash "+bookingObject.getGuest().getCustomerHash());
    	  if (bookingObject.getGuest() != null && bookingObject.getGuest().getMembershipId() != null
                  && !bookingObject.getGuest().getMembershipId().isEmpty() && bookingObject.getGuest().getAuthToken() != null && bookingObject.getGuest().getCustomerHash() != null) {
    	
    		  authCode = bookingObject.getGuest().getAuthToken();
    		  custmHash = bookingObject.getGuest().getCustomerHash();
    		 // merchantParam2 = firstName.concat(" ").concat(lastName).concat("-").concat(custmHash);
    	  }
    	  
    	//LOG.info("merchantParam2 "+merchantParam2);
    	 
    	 orderPayloadDetails.put(BookingConstants.OPEL_ORDER_ID, bookingObject.getItineraryNumber());
    	 orderPayloadDetails.put(BookingConstants.OPEL_AMOUNT, "1");
    	 orderPayloadDetails.put(BookingConstants.OPEL_CUSTOMERID, custmHash);
    	 orderPayloadDetails.put(BookingConstants.OPEL_CUSTOMER_PHONE_NO, bookingObject.getGuest().getPhoneNumber());
    	 orderPayloadDetails.put(BookingConstants.OPEL_CUSTOMER_EMAIL, bookingObject.getGuest().getEmail());
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_ID, "ihcl");
    	 orderPayloadDetails.put(BookingConstants.OPEL_RETUEN_URL, redirectUrl);
    	 orderPayloadDetails.put(BookingConstants.OPEL_TIMESTAMP, timestampString);
    	 orderPayloadDetails.put(BookingConstants.OPEL_PRODUCT_ID, hotelLocId);
    	 orderPayloadDetails.put(BookingConstants.OPEL_OFFER_TITLE, promoCode);
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_PARAM1, confirmationNumber);
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_PARAM2, name);
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_PARAM3, chkdate);
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_PARAM4, ratePlanCode);
    	 orderPayloadDetails.put(BookingConstants.OPEL_MERCHANT_PARAM5, merchantParam);
    	 orderPayloadDetails.put(BookingConstants.OPEL_SUB_ACCOUNT_ID, bookingObject.getHotel().hotelLocationId);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF1, confirmationNumber);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF2, name);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF3, udfCheckIncheckOutDate);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF4, custmHash);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF5, hotelId);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF6, chaninCode);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF7, ratePlanCode);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF8, authCode);
    	 orderPayloadDetails.put(BookingConstants.OPEL_UDF9, bookingObject.getGuest().getMembershipId());
    	 
    	 
    	 try {
			//orderPayloadString = parseOrderPayloadParameters(orderPayloadDetails);
    		 orderPayloadString = mapperObj.writeValueAsString(orderPayloadDetails);
			LOG.debug("orderPayloadString "+orderPayloadString);
			String privateKey = config.getPrivateKey();
			
	    	LOG.debug("privateKey "+privateKey);
			 LOG.debug("calling generatesignature method");
				signature = opelsignedpayload.generateSignatureForPayload(orderPayloadString, privateKey, true);
				//return signature; 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.debug("Inside catch block "+e);
		
			e.printStackTrace();
		}
   /* 	 String privateKey = config.privateKey();
    	 LOG.info("privateKey "+privateKey);
    	 
    try {
    	 LOG.info("calling generatesignature method");
		signature = opelsignedpayload.generateSignatureForPayload(orderPayloadString, privateKey, true);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		LOG.info("Error in exception");
		e.printStackTrace();
	}*/
//	return signature;
	return signature;   	 

    
	}
    

	public String encodePaymentRequest(String paymentParameters, String encodingKey) {
        AesCryptUtil aesUtil = new AesCryptUtil(encodingKey);
        return aesUtil.encrypt(paymentParameters);
    }

    public String createPaymentForm(String paymentServiceUrl, String encodedRequest, String accessKey) {

        return "<form action=\"" + paymentServiceUrl + "\" method=\"post\">" + "<input type=\"hidden\" name=\""
                + BookingConstants.CCAVENUE_URL_PARAM_ENC_REQ + "\" value=\"" + encodedRequest + "\" />"
                + "<input type=\"hidden\" name=\"" + BookingConstants.CCAVENUE_URL_PARAM_ACCESS_CODE + "\" value=\""
                + accessKey + "\" />" + "</form>";
    }

}
