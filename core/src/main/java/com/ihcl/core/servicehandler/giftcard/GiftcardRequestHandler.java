/**
 *
 */
package com.ihcl.core.servicehandler.giftcard;

import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.integration.giftcard.api.IGiftcartService;
import com.ihcl.integration.giftcard.dto.CreateAndIssueRequest;
import com.ihcl.integration.giftcard.dto.GiftcardResponse;

/**
 * @author admin
 *
 */

@Component(service = IGiftcardRequestHandler.class,
        immediate = true)
public class GiftcardRequestHandler implements IGiftcardRequestHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GiftcardRequestHandler.class);

    @Reference
    IGiftcartService iGiftcartService;

    @Override
    public GiftcardResponse initializeGiftcard(String connectionTimeout) throws Exception {

        LOG.debug("Started giftcardInitialization() in GiftcardRequestHandler.");

        GiftcardResponse guiftcardResponse = iGiftcartService.initializeGiftcard(connectionTimeout);

        LOG.debug("Ended giftcardInitialization() in GiftcardRequestHandler.");

        return guiftcardResponse;
    }

    @Override
    public GiftcardResponse createGiftcard(CreateAndIssueRequest createAndIssueRequest, String connectionTimeout)
            throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        LOG.debug("Started createGiftcard() in GiftcardRequestHandler.");
        LOG.debug("Forwording request to Giftcard bundle: {}", objectMapper.writeValueAsString(createAndIssueRequest));

        GiftcardResponse guiftcardResponse = iGiftcartService.createGiftcard(createAndIssueRequest, connectionTimeout);

        LOG.debug("Received response from Giftcard bundle: {}", objectMapper.writeValueAsString(guiftcardResponse));
        LOG.debug("Ended createGiftcard() in GiftcardRequestHandler.");

        return guiftcardResponse;
    }

}
