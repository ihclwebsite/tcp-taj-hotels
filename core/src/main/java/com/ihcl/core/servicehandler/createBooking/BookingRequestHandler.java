package com.ihcl.core.servicehandler.createBooking;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.integration.api.ICreateReservationService;
import com.ihcl.integration.dto.details.BookingObject;

/**
 * This class handles Create reservation Initiate request
 *
 * @author moonraft
 */

@Component(service = IBookingRequestHandler.class)
public class BookingRequestHandler implements IBookingRequestHandler {

    private static final Logger LOG = LoggerFactory.getLogger(BookingRequestHandler.class);

    @Reference
    ICreateReservationService reservationHandler;

    @Override
    public BookingObject createRoomReservation(BookingObject bookingObjectReq) throws Exception {

        LOG.debug("BookingRequestHandler :Inside Method BookingRequestHandler");

        LOG.debug("----------------------Sending request to Booking Bundle------------------------");

        BookingObject bookingObjectRes = reservationHandler.createReservation(bookingObjectReq);

        LOG.debug("----------------------Received response from Booking Bundle------------------------");

        LOG.debug("Reservation status: {}", bookingObjectRes.isSuccess());

        if (bookingObjectRes.isSuccess()) {
            LOG.debug("ItineraryNumber: {}", bookingObjectRes.getItineraryNumber());
        } else {
            LOG.debug("Received error while reserving rooms as : {}", bookingObjectRes.getErrorMessage());
        }
        LOG.debug("BookingRequestHandler :Exiting Method BookingRequestHandler");

        return bookingObjectRes;
    }
}
