/**
 *
 */
package com.ihcl.core.servicehandler.createBooking;

import com.ihcl.integration.dto.details.BookingObject;

/**
 * @author moonraft
 */
public interface IBookingRequestHandler {

    BookingObject createRoomReservation(BookingObject bookingObjectReq) throws Exception;
}
