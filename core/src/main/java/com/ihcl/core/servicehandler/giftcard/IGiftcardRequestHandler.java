/**
 *
 */
package com.ihcl.core.servicehandler.giftcard;

import java.io.IOException;

import com.ihcl.integration.giftcard.dto.CreateAndIssueRequest;
import com.ihcl.integration.giftcard.dto.GiftcardResponse;

/**
 * @author admin
 *
 */
public interface IGiftcardRequestHandler {

    /**
     * This servet initializeHitCount
     * @return
     * @throws IOException
     */
    GiftcardResponse initializeGiftcard(String connectionTimeout) throws Exception;

    /**
     * @return
     * @throws IOException
     */
    GiftcardResponse createGiftcard(CreateAndIssueRequest createAndIssueRequest, String connectionTimeout)
            throws Exception;

}
