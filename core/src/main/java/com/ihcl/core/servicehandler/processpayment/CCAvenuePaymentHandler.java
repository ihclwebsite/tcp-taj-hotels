/**
 *
 */
package com.ihcl.core.servicehandler.processpayment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ccavenue.security.AesCryptUtil;
import com.ihcl.core.models.CCAvenueParams;
import com.ihcl.tajhotels.constants.BookingConstants;

/**
 * This class has the utilities to handle payment processing while reserving a room
 *
 */
public class CCAvenuePaymentHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CCAvenuePaymentHandler.class);

    public String getPayementDetails(CCAvenueParams ccAvenueParams) throws ParseException {

        LOG.debug("---> Inside CCAvenuePaymentHandler.getPayementDetails method <---");
        LOG.debug("CcAvenueWorkingKey :: {}", ccAvenueParams.getCcAvenueWorkingKey());
        LOG.debug("CcAvenue MerchantId :: {}", ccAvenueParams.getMerchantId());

        String paymentString = "";
        Map<String, String> paymentdetails = new LinkedHashMap<>();
        paymentdetails.put(BookingConstants.CCAVENUE_MERCHANT_ID_NAME, ccAvenueParams.getMerchantId());
        paymentdetails.put(BookingConstants.CCAVENUE_ORDER_ID_NAME, ccAvenueParams.getOrderId());

        if (StringUtils.isNotBlank(ccAvenueParams.getCurrency())) {
            paymentdetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, ccAvenueParams.getCurrency());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_CURRENCY_NAME, BookingConstants.CCAVENUE_PARAM_CURRENCY);
        }

        paymentdetails.put(BookingConstants.CCAVENUE_AMOUNT_NAME, ccAvenueParams.getAmount());
        paymentdetails.put(BookingConstants.CCAVENUE_REDIRECT_URL_NAME, ccAvenueParams.getRedirectUrl());
        paymentdetails.put(BookingConstants.CCAVENUE_CANCEL_NAME, ccAvenueParams.getCancelUrl());
        paymentdetails.put(BookingConstants.CCAVENUE_LANGUAGE_NAME, BookingConstants.CCAVENUE_PARAM_LANGUAGE);

        // Injecting billing details into map which will pass to CCAvenue
        injectBillingDetails(ccAvenueParams, paymentdetails);

        // Injecting delivery details into map which will pass to CCAvenue
        injectDeliveryDetails(ccAvenueParams, paymentdetails);

        // Injecting merchant params into map which will pass to CCAvenue
        injectMerchantParams(ccAvenueParams, paymentdetails);

        paymentdetails.put(BookingConstants.CCAVENUE_PROMO_CODE_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        paymentdetails.put(BookingConstants.CCAVENUE_TID_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);

        if (StringUtils.isNotBlank(ccAvenueParams.getSubAccountId())) {
            paymentdetails.put(BookingConstants.CCAVENUE_SUB_ACCOUNT_ID, ccAvenueParams.getSubAccountId());
        }

        LOG.debug("--------------------Setting request String--------------------");

        Iterator<Map.Entry<String, String>> iterator = paymentdetails.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            try {
                paymentString = paymentString.concat(entry.getKey() + "="
                        + URLEncoder.encode(entry.getValue(), BookingConstants.CHARACTER_ENCOADING) + "&");
            } catch (UnsupportedEncodingException e) {
                LOG.debug("Error occured while converting the payment param to string : ", e);
            }
        }

        LOG.debug("The final redirect request String : {}", paymentString);
        LOG.debug("PaymentHandler : Leaving getPaymentDeatils");

        return paymentString;
    }


    /**
     * @param ccAvenueParams
     * @param paymentdetails
     */
    private void injectDeliveryDetails(CCAvenueParams ccAvenueParams, Map<String, String> paymentdetails) {
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryName())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME, ccAvenueParams.getDeliveryName());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_NAME_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryAddress())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME, ccAvenueParams.getDeliveryAddress());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ADDRESS_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryCity())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME, ccAvenueParams.getDeliveryCity());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_CITY_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryState())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME, ccAvenueParams.getDeliveryState());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_STATE_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryZip())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME, ccAvenueParams.getDeliveryZip());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_ZIP_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryCountry())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME, ccAvenueParams.getDeliveryCountry());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_COUNTRY_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getDeliveryTel())) {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME, ccAvenueParams.getDeliveryTel());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_DELIVERY_TEL_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
    }


    /**
     * @param ccAvenueParams
     * @param paymentdetails
     */
    private void injectMerchantParams(CCAvenueParams ccAvenueParams, Map<String, String> paymentdetails) {
        if (StringUtils.isNotBlank(ccAvenueParams.getMerchantParam1())) {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, ccAvenueParams.getMerchantParam1());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH1_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getMerchantParam2())) {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, ccAvenueParams.getMerchantParam2());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH2_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getMerchantParam3())) {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, ccAvenueParams.getMerchantParam3());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH3_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getMerchantParam4())) {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH4_NAME, ccAvenueParams.getMerchantParam4());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH4_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getMerchantParam5())) {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, ccAvenueParams.getMerchantParam5());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_MERCH5_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
    }


    /**
     * @param ccAvenueParams
     * @param paymentdetails
     */
    private void injectBillingDetails(CCAvenueParams ccAvenueParams, Map<String, String> paymentdetails) {

        if (StringUtils.isNotBlank(ccAvenueParams.getBillingName())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_NAME, ccAvenueParams.getBillingName());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_NAME, BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingAddress())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME, ccAvenueParams.getBillingAddress());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ADDRESS_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingCity())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME, ccAvenueParams.getBillingCity());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_CITY_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingState())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME, ccAvenueParams.getBillingState());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_STATE_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingZip())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME, ccAvenueParams.getBillingZip());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_ZIP_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);

        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingCountry())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME, ccAvenueParams.getBillingCountry());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_COUNTRY_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }

        // Injecting billing tel and email details
        injectBillingTelANDEmail(ccAvenueParams, paymentdetails);
    }


    /**
     * @param ccAvenueParams
     * @param paymentdetails
     */
    private void injectBillingTelANDEmail(CCAvenueParams ccAvenueParams, Map<String, String> paymentdetails) {
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingTel())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME, ccAvenueParams.getBillingTel());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_TEL_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
        if (StringUtils.isNotBlank(ccAvenueParams.getBillingEmail())) {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME, ccAvenueParams.getBillingEmail());
        } else {
            paymentdetails.put(BookingConstants.CCAVENUE_BILLING_EMAIL_NAME,
                    BookingConstants.CCAVENUE_PARAM_DEFAULT_STRING);
        }
    }


    public String getCCAvenueDetails(CCAvenueParams ccAvenueParams) throws ParseException {

        LOG.debug("---> Inside PaymentHandler.getCCAvenueSetails method <---");
        String paymentParameter = getPayementDetails(ccAvenueParams);

        LOG.debug("---> Encrypting information <---");
        LOG.debug("working key : {}", ccAvenueParams.getCcAvenueWorkingKey());
        LOG.debug("Access Code: {}", ccAvenueParams.getCcAvenueAccessKey());

        AesCryptUtil aesUtil = new AesCryptUtil(ccAvenueParams.getCcAvenueWorkingKey());
        String encRequest = aesUtil.encrypt(paymentParameter);

        LOG.debug("Encrypted encRequest:---> {}", encRequest);
        LOG.debug("---> Passing control to Payment processing through ajax <---");


        return "<form action=\"" + ccAvenueParams.getCcAvenueUrl() + "\" method=\"post\">"
                + "<input type=\"hidden\" name=\"" + BookingConstants.CCAVENUE_URL_PARAM_ENC_REQ + "\" value=\""
                + encRequest + "\" />" + "<input type=\"hidden\" name=\""
                + BookingConstants.CCAVENUE_URL_PARAM_ACCESS_CODE + "\" value=\""
                + ccAvenueParams.getCcAvenueAccessKey() + "\" />" + "</form>";
    }

}
