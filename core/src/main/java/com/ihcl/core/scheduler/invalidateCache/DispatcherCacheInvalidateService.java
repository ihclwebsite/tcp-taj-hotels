package com.ihcl.core.scheduler.invalidateCache;

import com.ihcl.core.scheduler.conf.DispatcherURLConfiguration;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Dictionary;

@Component(service = Runnable.class,
        immediate = true, configurationPid = DispatcherCacheInvalidateService.PID, configurationPolicy = ConfigurationPolicy.REQUIRE
)
@Designate(ocd = DispatcherURLConfiguration.class)

public class DispatcherCacheInvalidateService implements Runnable {

    static final String PID = "com.ihcl.core.scheduler.invalidateCache.DispatcherCacheInvalidateService";

    private static final String CACHE_HANDLE = "cacheHandle";
    private static final String DISPATCHER_URL = "dispatcherUrl";
    private static final String DISPATCHER_PORT = "dispatcherPort";
    private static final String CACHE_ACTION = "cacheAction";
    private static final String SCHEDULER_EXPRESSION = "schedulerExpression";
    private static final String ENABLED = "enabled";
    private static final String CONCURRENT = "concurrent";
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private String dispatcherUrl;
    private String dispatcherPort;
    private String cacheAction;
    private String[] cacheHandle;
    private String schedulerExpression;
    private boolean enabled;
    private boolean concurrent;

    @org.osgi.service.component.annotations.Reference
    private Scheduler scheduler;

    @Override
    public void run() {

        log.info("Executing a periodic job");
        if (enabled && !concurrent) {
            try {
                this.concurrent = true;
                String url = dispatcherUrl + ":" + dispatcherPort + "/dispatcher/invalidate.cache";
                URL obj = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                conn.setRequestProperty("CQ-Action", cacheAction);
                String[] handleProperties = cacheHandle;
                for (String handleProperty : handleProperties) {
                    conn.setRequestProperty("CQ-Handle", handleProperty);
                }
                conn.setRequestProperty("Content-Length", "0");
                conn.setRequestProperty("Content-Type", "application/octet-stream");

                conn.setDoOutput(false);
                log.info("Final Curl request method " + conn.getURL());
                conn.getInputStream();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.concurrent = false;
        }
    }

    @Activate
    protected void activate(final ComponentContext componentContext) throws Exception {
        log.info("cache invalidate component is activated");
        final Dictionary<String, Object> properties = componentContext.getProperties();
        this.cacheAction = (String) properties.get(CACHE_ACTION);
        this.cacheHandle = (String[]) properties.get(CACHE_HANDLE);
        this.dispatcherPort = (String) properties.get(DISPATCHER_PORT);
        this.dispatcherUrl = (String) properties.get(DISPATCHER_URL);
        this.schedulerExpression = (String) properties.get(SCHEDULER_EXPRESSION);
        this.enabled = (boolean) properties.get(ENABLED);
        try {
            this.scheduler.schedule(this, scheduler.EXPR(schedulerExpression).name("DispatcherCacheInvalidator"));
            log.info("Scheduler has been activated with the cron expression : " + schedulerExpression);
        } catch (Exception e) {
            log.error("Exception caught, executing job");
            log.error(e.getMessage());
            this.run();
        }
    }

    @Deactivate
    protected void deactivate(ComponentContext ctx) {
        log.info("cache invalidate component is deactivated");
    }
}
