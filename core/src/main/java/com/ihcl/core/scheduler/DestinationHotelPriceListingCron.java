package com.ihcl.core.scheduler;

import java.util.HashMap;

import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.scheduler.conf.DestinationListingConfiguration;

/**
 * @author Ravindar Dev
 *
 */
@Component(configurationPolicy = ConfigurationPolicy.REQUIRE,
        configurationPid = DestinationHotelPriceListingCron.PID)
@Designate(ocd = DestinationListingConfiguration.class)
public class DestinationHotelPriceListingCron implements Runnable {

    public static final String PID = "com.ihcl.core.scheduler.DestinationHotelPriceListingCron";

    private static final Logger log = LoggerFactory.getLogger(DestinationHotelPriceListingCron.class);

    @Reference
    private JobManager jobManager;

    @Override
    public void run() {
        log.info(">>>>>>>>>>INSIDE THE CRON JOB");
        jobManager.addJob("tajhotels/destinationHotelPriceListing", new HashMap<String, Object>());
    }

}
