package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Nutan
 *
 */
@ObjectClassDefinition(name = "Gift Card Excel Config",
        description = "Gift Card Excel Config File")
public @interface GiftExcelGenConfiguration {

    @AttributeDefinition(name = "Expression",
            description = "Cron-job expression. Default: run every week on MON. 00 hrs",
            type = AttributeType.STRING)
    String scheduler_expression() default "0 0 0 ? * MON *";

    @AttributeDefinition(name = "Concurrent",
            description = "Schedule task concurrently",
            type = AttributeType.BOOLEAN)
    boolean scheduler_concurrent() default true;

    @AttributeDefinition(name = "Email Id To configuration",
            description = "Please add the email Id in comma separated values for eg. xyz@moonraft.com,abc@moonraft.com")
    String getEmailIdTo() default "nutan@moonraft.com";

    @AttributeDefinition(name = "Email Id Cc configuration",
            description = "Please add the email Id in comma separated values for eg. xyz@moonraft.com,abc@moonraft.com")
    String getEmailIdCc() default "nutan@moonraft.com";

}
