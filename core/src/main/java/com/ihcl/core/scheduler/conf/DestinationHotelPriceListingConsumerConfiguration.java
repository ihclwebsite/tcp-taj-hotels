package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Ravindar Dev
 *
 */
@ObjectClassDefinition(name = "Taj Destination Hotel Price Listing Consumer Configuration", description = "Configure the Destination Hotel Price Consumer Listing")
public @interface DestinationHotelPriceListingConsumerConfiguration {
	
    @AttributeDefinition(
            name = "Base Path",
            description = "Root base path of page where all the hotels are present",
            type = AttributeType.STRING
        )
    String getBasePath() default "/content/tajhotels/en-in/our-hotels";
}
