package com.ihcl.core.scheduler.jobs;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.gift.excel.IGiftExcelService;
import com.ihcl.core.scheduler.conf.GiftExcelGenConfiguration;

/**
 * @author Nutan
 *
 */

@Component(service = JobConsumer.class,
        immediate = true,
        configurationPid = GiftExcelGenerationConsumer.PID,
        property = { JobConsumer.PROPERTY_TOPICS + "=tajhotels/giftcardExcelGenerationConsumer" })
@Designate(ocd = GiftExcelGenConfiguration.class)
public class GiftExcelGenerationConsumer implements JobConsumer {

    private static final Logger log = LoggerFactory.getLogger(GiftExcelGenerationConsumer.class);

    public static final String PID = "com.ihcl.core.scheduler.jobs.GiftExcelGenerationConsumer";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private ResourceResolver resourceResolver;


    @Reference
    private IGiftExcelService iGiftExcelService;

    private String emailAddressTo;

    private String emailAddressCc;

    private static final String PATH_TO_CREATE_EXCEL_FROM = "/content/taj-inner-circle/en-in/book-and-redeem/gift-card/redemptions";


    @Activate
    @Modified
    protected void activate(final GiftExcelGenConfiguration configuration) {
        log.debug("cron job gift excel generation is activated");
        emailAddressTo = configuration.getEmailIdTo();
        emailAddressCc = configuration.getEmailIdCc();

    }

    @Override
    public JobResult process(Job job) {
        log.info(">>>>>>INSIDE GiftExcelGenConfiguration PROCESS ::::");
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource giftExcelResource = resourceResolver.getResource(PATH_TO_CREATE_EXCEL_FROM);
            Node giftExcelNode = giftExcelResource.adaptTo(Node.class);
            if (giftExcelNode.isNode()) {
                iGiftExcelService.generateExcelFromGiftNode(giftExcelResource, emailAddressTo, emailAddressCc,
                        resourceResolver);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return JobResult.FAILED;
        }

        return JobResult.OK;
    }
}
