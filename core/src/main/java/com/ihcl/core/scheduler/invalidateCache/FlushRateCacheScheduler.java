package com.ihcl.core.scheduler.invalidateCache;

import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.DispatcherRateFlushService;
import com.ihcl.core.services.config.RateCacheSchedulerConfig;

/**
 * OSGI R6 Annotation Scheduler Example
 * 
 * @author aahlawat
 *
 */
@Component(service = Runnable.class, immediate = true, configurationPid = FlushRateCacheScheduler.PID, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = RateCacheSchedulerConfig.class)

public class FlushRateCacheScheduler implements Runnable {

	static final String PID = "com.ihcl.tajhotels.schedulers.FlushRateCacheScheduler";

	@Reference
	private Scheduler scheduler;

	@Reference
	private DispatcherRateFlushService rateFlushService;

	private int schedulerID;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Activate
	protected void activate(RateCacheSchedulerConfig config) {
		schedulerID = config.schedulerName().hashCode();
	}

	@Modified
	protected void modified(RateCacheSchedulerConfig config) {
		removeScheduler();
		schedulerID = config.schedulerName().hashCode(); // update schedulerID
		addScheduler(config);
	}

	@Deactivate
	protected void deactivate(RateCacheSchedulerConfig config) {
		removeScheduler();
	}

	/**
	 * Remove a scheduler based on the scheduler ID
	 */
	private void removeScheduler() {
		logger.debug("Removing Scheduler Job '{}'", schedulerID);
		scheduler.unschedule(String.valueOf(schedulerID));
	}

	/**
	 * Add a scheduler based on the scheduler ID
	 */
	private void addScheduler(RateCacheSchedulerConfig config) {
		if (config.serviceEnabled()) {
			ScheduleOptions sopts = scheduler.EXPR(config.schedulerExpression());
			sopts.name(String.valueOf(schedulerID));
			sopts.canRunConcurrently(false);
			scheduler.schedule(this, sopts);
			logger.debug("Scheduler {} added succesfully", schedulerID);
		} else {
			logger.debug("Scheduler is Disabled, no scheduler job created");
		}
	}

	@Override
	public void run() {
		logger.debug("Inside Scheduler run Method");
		rateFlushService.clearHotelRateCache("");
	}
}