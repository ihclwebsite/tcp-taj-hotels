/**
 * 
 */
package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author TCS
 *
 */

@ObjectClassDefinition(	name = "Donation Excel Config",
						description = "Donation Excel Config File")
public @interface DonationExcelGenConfiguration {

    @AttributeDefinition(name = "Expression",
            description = "Cron-job expression. Default: run every week on MON. 00 hrs",
            type = AttributeType.STRING)
    String scheduler_expression() default "0 0 0 ? * MON *";

    @AttributeDefinition(name = "Concurrent",
            description = "Schedule task concurrently",
            type = AttributeType.BOOLEAN)
    boolean scheduler_concurrent() default true;

    @AttributeDefinition(name = "Email Id To configuration",
            description = "Please add the email Id in comma separated values for eg. xyz@tcs.com,abc@tcs.com")
    String getEmailIdTo() default "maheshprajapati1718@gmail.com";

    @AttributeDefinition(name = "Email Id Cc configuration",
            description = "Please add the email Id in comma separated values for eg. xyz@tcs.com,abc@tcs.com")
    String getEmailIdCc() default "harishdwngn8@gmail.com";
	
}
