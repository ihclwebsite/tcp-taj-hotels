package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Ravindar Dev
 *
 */
@ObjectClassDefinition(name = "Currency Rate Convertion Config",
        description = "Currency Rate Convertion Config File")
public @interface CurrencyRateListingConfiguration {

    @AttributeDefinition(name = "Expression",
            description = "Cron-job expression. Default: run every week on MON. 00 hrs",
            type = AttributeType.STRING)
    String scheduler_expression() default "0 0 0 ? * MON *";

    @AttributeDefinition(name = "Concurrent",
            description = "Schedule task concurrently",
            type = AttributeType.BOOLEAN)
    boolean scheduler_concurrent() default true;

}
