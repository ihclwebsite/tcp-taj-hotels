package com.ihcl.core.scheduler.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import com.ihcl.core.scheduler.conf.DestinationHotelPriceListingConsumerConfiguration;
import com.ihcl.integration.dto.availability.CurrencyCodeDto;
import com.ihcl.integration.dto.availability.DestinationPagePriceResponse;
import com.ihcl.integration.dto.availability.RoomOccupants;
import com.ihcl.integration.dto.availability.RoomsAvailabilityRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.destination.DestinationHotel;
import com.ihcl.integration.api.IRoomAvailabilityService;

/**
 * @author Ravindar Dev
 *
 */

@Component(service = JobConsumer.class,immediate = true,configurationPid = DestinationHotelPriceListingConsumer.PID,
		   property = {
			JobConsumer.PROPERTY_TOPICS + "=tajhotels/destinationHotelPriceListing"
})
@Designate(ocd = DestinationHotelPriceListingConsumerConfiguration.class)
public class DestinationHotelPriceListingConsumer implements JobConsumer {

	private static final Logger log = LoggerFactory.getLogger(DestinationHotelPriceListingConsumer.class);
	
	public static final String PID = "com.ihcl.core.scheduler.jobs.DestinationHotelPriceListingConsumer";
	private String basePath;
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;
	
	@Reference
	QueryBuilder queryBuilder;
	
    @Reference
    private IRoomAvailabilityService hotelAvailabilityService;
    
	private ResourceResolver resourceResolver;
    
    Map<String,DestinationHotel> destinationHotels;
    
    @Activate @Modified
	protected void activate(final DestinationHotelPriceListingConsumerConfiguration configuration) {
    	this.basePath = configuration.getBasePath();
    	log.info(">>>>>>BASE PATH :::: {}", basePath);
	}
    
	@Override
	public JobResult process(Job job) {
		log.info(">>>>>>INSIDE PROCESS ::::");
		try {
			resourceResolver =  resourceResolverFactory.getServiceResourceResolver(null);
			
			//Read all the pages which has hotelId tag in them using QueryBuilder
			SearchResult searchResult = getAllPages(basePath);
			
			destinationHotels = new HashMap<>(); 
			
			//Lets add new Pages if any to the SiteMap
			for (Hit hit : searchResult.getHits()) {
					String pagePath = hit.getPath();
					log.info(">>>>>>PAGE PATH :::: {}", pagePath);
					ValueMap pageProperties = hit.getProperties();
					String hotelId = pageProperties.get("hotelId",String.class);
					DestinationHotel destinationHotel = new DestinationHotel();
					destinationHotel.setJcrPath(pagePath);
					destinationHotels.put(hotelId, destinationHotel);
			}
			
			DestinationPagePriceResponse destinationPagePriceResponse = fetchAvailabilityFromService();
			
			 Set<String> destinationHotelIdKeySet = destinationPagePriceResponse.getDestinationPagePricing().keySet();
	            
	         for(String destinationHotelIdKey : destinationHotelIdKeySet){
	        	 
	        	 if(destinationHotels.containsKey(destinationHotelIdKey)){
	        		 DestinationHotel destinationHotel = destinationHotels.get(destinationHotelIdKey);
	        		 String path = destinationHotel.getJcrPath();
	        		 if(resourceResolver != null){
		        		 ModifiableValueMap hotelValueMap = resourceResolver.getResource(path+"/jcr:content").adaptTo(ModifiableValueMap.class);
		        		 if(hotelValueMap != null){
			        		 hotelValueMap.put("amountAfterTax",destinationPagePriceResponse.getDestinationPagePricing().get(destinationHotelIdKey).getAmountAfterTax());
			        		 hotelValueMap.put("amountBeforeTax",destinationPagePriceResponse.getDestinationPagePricing().get(destinationHotelIdKey).getAmountBeforeTax());
			        		 resourceResolver.commit();
		        		 }
	        		 }
	        	 }
	        	
	         }
		
			
			return JobResult.OK;
			
		}catch(RepositoryException e) {
			log.info("Error/Exception occured in DestinationHotelPriceListingConsumer while reading Result Set {}",e.getMessage());
		} catch (LoginException e) {
			log.info("Error/Exception occured in DestinationHotelPriceListingConsumer while getting Resource Resolver {}",e.getMessage());
		}catch (Exception e) {
			log.info("Error/Exception occured in DestinationHotelPriceListingConsumer {} ",e.getMessage());
		}
		
		return JobResult.FAILED;
	}
	
    private SearchResult getAllPages(String path) {
		//Object Creation for QueryBuilder
		Map<String, String> queryMap = new TreeMap<>();
		queryMap.put("path",path);
		queryMap.put("type", "cq:Page");
		queryMap.put("property", "jcr:content/hotelId");
		queryMap.put("property.operation", "exists");
		queryMap.put("p.hits", "all");
		queryMap.put("p.limit", "-1");

		Query query = queryBuilder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
		// Execute the query and get the results ...
		return query.getResult();
		
	}
    
	 /**
     * @return
     * @throws Exception
     */
    private DestinationPagePriceResponse fetchAvailabilityFromService() throws Exception {
		RoomsAvailabilityRequest availabilityRequest = new RoomsAvailabilityRequest();

        availabilityRequest.setCurrencyCode(new CurrencyCodeDto() {

            @Override
            public String getShortCurrencyString() {
                return "INR";
            }

            @Override
            public String getCurrencyString() {
                return "INR";
            }
        });
        
        List<String> hotelIds = new ArrayList<>();
        
        List<RoomOccupants> occupantsList = new ArrayList<>();
        
        Set<String> hotelsKeySet = destinationHotels.keySet();
        for(String hotelId: hotelsKeySet) {
	        hotelIds.add(hotelId);
	        RoomOccupants roomOccupants = new RoomOccupants();
	        roomOccupants.setNumberOfAdults(1);
	        roomOccupants.setNumberOfChildren(0);
	        occupantsList.add(roomOccupants);
	        
	    }
        availabilityRequest.setRoomOccupants(occupantsList);
        availabilityRequest.setHotelIdentifiers(hotelIds);
        return hotelAvailabilityService.getAveragePriceForDestinationPage(availabilityRequest);
    }
    
}
