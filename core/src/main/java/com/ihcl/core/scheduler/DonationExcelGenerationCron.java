/**
 * 
 */
package com.ihcl.core.scheduler;

import java.util.HashMap;

import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.scheduler.conf.DonationExcelGenConfiguration;

/**
 * @author TCS
 *
 */

@Component( configurationPolicy = ConfigurationPolicy.REQUIRE,
			configurationPid = DonationExcelGenerationCron.PID)
@Designate(ocd = DonationExcelGenConfiguration.class)
public class DonationExcelGenerationCron implements Runnable {

    public static final String PID = "com.ihcl.core.scheduler.DonationExcelGenerationCron";

    private static final Logger log = LoggerFactory.getLogger(DonationExcelGenerationCron.class);

    @Reference
    private JobManager jobManager;

    @Override
    public void run() {
        log.info(">>>>>>>>>>INSIDE THE DonationExcelGenerationCron JOB");
        jobManager.addJob("tajhotels/donationExcelGenerationConsumer", new HashMap<String, Object>());
    }
	
}
