package com.ihcl.core.scheduler;

import java.util.HashMap;

import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.scheduler.conf.CurrencyRateListingConfiguration;

/**
 * @author Ravindar Dev
 *
 */
@Component(configurationPolicy = ConfigurationPolicy.REQUIRE,
        configurationPid = CurrencyRateListingCron.PID)
@Designate(ocd = CurrencyRateListingConfiguration.class)
public class CurrencyRateListingCron implements Runnable {

    public static final String PID = "com.ihcl.core.scheduler.CurrencyRateListingCron";

    private static final Logger log = LoggerFactory.getLogger(CurrencyRateListingCron.class);

    @Reference
    private JobManager jobManager;

    @Override
    public void run() {
        log.info(">>>>>>>>>>INSIDE THE CurrencyRateListingCron CRON JOB");
        jobManager.addJob("tajhotels/currencyRateListingConsumer", new HashMap<String, Object>());
    }

}
