package com.ihcl.core.scheduler.jobs;

import java.util.Calendar;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.currency.ICurrencyFetcher;
import com.ihcl.core.scheduler.conf.CurrencyRateListingConfiguration;
import com.ihcl.loyalty.dto.CurrencyResponsePojo;

/**
 * @author Ravindar Dev
 */

@Component(service = JobConsumer.class,
        immediate = true,
        configurationPid = CurrencyRateListingConsumer.PID,
        property = { JobConsumer.PROPERTY_TOPICS + "=tajhotels/currencyRateListingConsumer" })
@Designate(ocd = CurrencyRateListingConfiguration.class)
public class CurrencyRateListingConsumer implements JobConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(CurrencyRateListingConsumer.class);

    public static final String PID = "com.ihcl.core.scheduler.jobs.CurrencyRateListingConsumer";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private ResourceResolver resourceResolver;


    @Reference
    private ICurrencyFetcher iCurrencyFetcher;

    private static final String PATH_TO_CURRENCY_CONVERSION =
            "/content/shared-content/global-shared-content/currency-conversion";


    @Activate
    @Modified
    protected void activate(final CurrencyRateListingConfiguration configuration) {
        LOG.debug("Activating the cron job for currency conversion rate fetch.");
    }

    @Override
    public JobResult process(Job job) {
        String methodName = "process";
        LOG.trace("Method Entry: " + methodName);
        JobResult result = JobResult.OK;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource currencyConversionResource = resourceResolver.getResource(PATH_TO_CURRENCY_CONVERSION);

            Node currencyConversionNode = currencyConversionResource.adaptTo(Node.class);

            String[] currencyArray = { "USD", "MYR", "INR", "ZAR", "AED", "GBP", "EUR", "£", "$", "RM" };

            for (String currency : currencyArray) {
                String currencySymbol = currency;
                if (currency.equals("$")) {
                    currency = "USD";
                } else if (currency.equals("£")) {
                    currency = "EUR";
                } else if (currency.equals("RM")) {
                    currency = "MYR";
                }

                CurrencyResponsePojo currencyResponsePojo = iCurrencyFetcher.convertCurrency(currency);
                String rate = currencyResponsePojo.getExchange_spcRate();

                LOG.debug("The conversion rate received for currency: " + currency + " is: " + rate);

                Node currencyNode = getNodeForCurrency(currencyConversionNode, currencySymbol);
                LOG.info("Attempting to update the node at: " + currencyConversionNode);

                currencyNode.setProperty("conversionRate", rate);
                currencyNode.setProperty("cq:lastModified", Calendar.getInstance());
                resourceResolver.commit();
                LOG.info("Update of currency conversion rate node completed successfully");
            }
        } catch (Exception e) {
            LOG.error("An exception occurred while updating currency conversion rates", e);
            result = JobResult.FAILED;
        }
        LOG.trace("Method Exit: " + methodName);
        return result;
    }

    private Node getNodeForCurrency(Node currencyConversionNode, String currencySymbol) throws RepositoryException {
        Node currencyNode;
        String currencyNodeName = currencySymbol + "_INR";
        if (currencyConversionNode.hasNode(currencyNodeName)) {
            currencyNode = currencyConversionNode.getNode(currencyNodeName);

        } else {
            currencyNode = currencyConversionNode.addNode(currencyNodeName);
            currencyNode.setPrimaryType("nt:unstructured");
        }
        return currencyNode;
    }
}
