/**
 * 
 */
package com.ihcl.core.scheduler.jobs;

import java.io.File;
import java.util.Date;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.scheduler.conf.HealthReportGenConfiguration;


/**
 * @author TCS
 *
 */

@Component(service = JobConsumer.class,
immediate = true,
configurationPid = HealthReportGenerationConsumer.PID,
property = { JobConsumer.PROPERTY_TOPICS + "=tajhotels/healthReportGenerationConsumer" })
@Designate(ocd = HealthReportGenConfiguration.class)

public class HealthReportGenerationConsumer implements JobConsumer {


    private static final Logger log = LoggerFactory.getLogger(HealthReportGenerationConsumer.class);

    public static final String PID = "com.ihcl.core.scheduler.jobs.HealthReportGenerationConsumer";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;


    @Activate
    @Modified
    protected void activate(final HealthReportGenConfiguration configuration) {
        log.debug("cron job donation excel generation is activated");
//        emailAddressTo = configuration.getEmailIdTo();
//        emailAddressCc = configuration.getEmailIdCc();
    }

    @Override
    public JobResult process(Job job) {
        log.debug(">>>>>>INSIDE HealthReportGenConfiguration PROCESS ::::");
        try {
        	Date date = new Date();
    		long time = date.getTime();
    		File file = new File("/mnt/GuestHealthReports/");
            File[] files = file.listFiles();
            for(File f: files){
                System.out.println(f.getName());
                if(f.getName().equalsIgnoreCase("HealthReportTemplate.pdf")) {
                	log.debug("Template Found "+f.getName());
                	}
                else {
                	log.debug("Last Modified Time"+f.lastModified());
                	log.debug("Current Time"+time);
                	log.debug("Difference "+ (time - f.lastModified()));
                	if((time - f.lastModified()) > 604800000) {
                		log.debug("Deleting File");
                		f.delete();
                	}
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            return JobResult.FAILED;
        }

        return JobResult.OK;
    }

}
