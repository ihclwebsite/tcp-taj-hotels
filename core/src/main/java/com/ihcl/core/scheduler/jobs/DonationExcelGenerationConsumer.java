/**
 * 
 */
package com.ihcl.core.scheduler.jobs;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.donation.excel.IDonationExcelService;
import com.ihcl.core.scheduler.conf.DonationExcelGenConfiguration;


/**
 * @author TCS
 *
 */

@Component(service = JobConsumer.class,
immediate = true,
configurationPid = DonationExcelGenerationConsumer.PID,
property = { JobConsumer.PROPERTY_TOPICS + "=tajhotels/donationExcelGenerationConsumer" })
@Designate(ocd = DonationExcelGenConfiguration.class)

public class DonationExcelGenerationConsumer implements JobConsumer {


    private static final Logger log = LoggerFactory.getLogger(DonationExcelGenerationConsumer.class);

    public static final String PID = "com.ihcl.core.scheduler.jobs.DonationExcelGenerationConsumer";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private ResourceResolver resourceResolver;


    @Reference
    private IDonationExcelService iDonationExcelService;

    private String emailAddressTo;

    private String emailAddressCc;

    private static final String PATH_TO_CREATE_EXCEL_FROM = "/content/usergenerated/donation-data";


    @Activate
    @Modified
    protected void activate(final DonationExcelGenConfiguration configuration) {
        log.debug("cron job donation excel generation is activated");
        emailAddressTo = configuration.getEmailIdTo();
        emailAddressCc = configuration.getEmailIdCc();
    }

    @Override
    public JobResult process(Job job) {
        log.info(">>>>>>INSIDE DonationExcelGenConfiguration PROCESS ::::");
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource donationExcelResource = resourceResolver.getResource(PATH_TO_CREATE_EXCEL_FROM);
            Node donationExcelNode = donationExcelResource.adaptTo(Node.class);
            if (donationExcelNode.isNode()) {
                iDonationExcelService.generateExcelFromDonationNode(donationExcelResource, emailAddressTo, emailAddressCc,
                        resourceResolver);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return JobResult.FAILED;
        }

        return JobResult.OK;
    }

}
