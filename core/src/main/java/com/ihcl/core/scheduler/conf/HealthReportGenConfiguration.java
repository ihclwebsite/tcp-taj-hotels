/**
 * 
 */
package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author TCS
 *
 */

@ObjectClassDefinition(	name = "Health Report Config",
						description = "Health Report Config File")
public @interface HealthReportGenConfiguration {

    @AttributeDefinition(name = "Expression",
            description = "Cron-job expression. Default: run every week on MON. 00 hrs",
            type = AttributeType.STRING)
    String scheduler_expression() default "0 0 0 ? * MON *";

}
