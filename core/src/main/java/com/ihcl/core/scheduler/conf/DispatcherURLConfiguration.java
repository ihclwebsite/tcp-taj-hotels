package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Dispatcher Url Config", description = "Dispatcher Url Configuration")
public @interface DispatcherURLConfiguration {
    @AttributeDefinition(name = "dispatcherUrl", description = "Host of the dispatcher", type = AttributeType.STRING)
    String dispatcherUrl() default "http://10.44.33.27";

    @AttributeDefinition(name = "Dispatcher Port", description = "port number of dispatcher", type = AttributeType.STRING)
    String dispatcherPort() default "4503";

    @AttributeDefinition(name = "Cache action", description = "Action to ber performed on the cache", type = AttributeType.STRING)
    String cacheAction() default "flush";

    @AttributeDefinition(name = "Dispatcher cache handle", description = "Dispatcher cache handle", type = AttributeType.STRING)
    String[] cacheHandle() default {"/content", "/bin"};

    @AttributeDefinition(name = "scheduler.expression", description = "Scheduler Expression", type = AttributeType.STRING)
    String schedulerExpression() default "0 0 0/6 * * ?";

    @AttributeDefinition(name = "Service Enabled", description = "Is Service Enabled?", type = AttributeType.BOOLEAN)
    boolean enabled() default false;
}
