package com.ihcl.core.scheduler.conf;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Ravindar Dev
 *
 */
@ObjectClassDefinition(name = "Taj Destination Hotel Price Listing Configuration", description = "Configure the Destination Hotel Price Listing")
public @interface DestinationListingConfiguration {
	
    @AttributeDefinition(
            name = "Expression",
            description = "Cron-job expression. Default: run every minute.",
            type = AttributeType.STRING
        )
    String scheduler_expression() default "0 * * * * ?";
    
	@AttributeDefinition(
            name = "Concurrent",
            description = "Schedule task concurrently",
            type = AttributeType.BOOLEAN
        )
	boolean scheduler_concurrent() default true;

}
