package com.ihcl.core.scheduler;

import java.util.HashMap;

import org.apache.sling.event.jobs.JobManager;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.scheduler.conf.GiftExcelGenConfiguration;

/**
 * @author Nutan
 *
 */
@Component(configurationPolicy = ConfigurationPolicy.REQUIRE,
        configurationPid = GiftExcelGenerationCron.PID)
@Designate(ocd = GiftExcelGenConfiguration.class)
public class GiftExcelGenerationCron implements Runnable {

    public static final String PID = "com.ihcl.core.scheduler.GiftExcelGenerationCron";

    private static final Logger log = LoggerFactory.getLogger(GiftExcelGenerationCron.class);

    @Reference
    private JobManager jobManager;

    @Override
    public void run() {
        log.info(">>>>>>>>>>INSIDE THE GiftExcelGenConfigurationCron JOB");
        jobManager.addJob("tajhotels/giftcardExcelGenerationConsumer", new HashMap<String, Object>());
    }

}
