package com.ihcl.core.services.leadcapture;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Runnable.class)
@Designate(ocd = LeadCaptureScheduler.Config.class)
public class LeadCaptureScheduler implements Runnable {

    @Reference
    private ILeadCaptureService leadCaptureService;

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Activate
    @Modified
    protected void activate(Config config) {
        run();
    }

    @Override
    public void run() {
        LOG.trace("Lead Capture Scheduler Triggered");
        leadCaptureService.triggerExcelGenerationForCapturedData();
    }

    @ObjectClassDefinition(name = "Tajhotels - Lead Capture Scheduler Configuration",
            description = "This job upon trigger will generate a CSV and send an Email to with the CSV as an attachment to the Admins")
    public @interface Config {

        @AttributeDefinition(name = "Scheduler Name",
                description = "Name for the scheduler")
        String schedulerName() default "Lead Capture Scheduler Configurations";

        @AttributeDefinition(name = "Enabled",
                description = "Enable Scheduler")
        boolean serviceEnabled() default true;

        @AttributeDefinition(name = "Cron Expression",
                description = "Cron-job expression. Default: run once every week on Monday at 12:00 AM.")
        String scheduler_expression() default "0 0 0 ? * MON *";

        @AttributeDefinition(name = "Concurrent task",
                description = "Whether or not to schedule this task concurrently")
        boolean scheduler_concurrent() default false;

    }
}

