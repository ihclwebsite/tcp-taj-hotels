package com.ihcl.core.services.leadcapture.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.leadcapture.ILeadCaptureService;
import com.ihcl.core.services.leadcapture.LeadCaptureConfigurationService;
import com.ihcl.core.services.leadcapture.LeadCaptureModel;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.email.api.IEmailService;


@Component(immediate = true,
        service = ILeadCaptureService.class)
public class LeadCaptureServiceImpl implements ILeadCaptureService {


    public static final String PREFIX = "LeadCaptureUserData";

    public static final String SUFFIX = ".csv";

    private static final String CSV_SEPARATOR = ",";

    private final Logger LOG = LoggerFactory.getLogger(LeadCaptureServiceImpl.class);

    @Reference
    private ResourceFetcherService resourceFetcherService;

    @Reference
    private IEmailService emailService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private ResourceResolver resourceResolver;

    @Reference
    private LeadCaptureConfigurationService config;


    @Override
    public void triggerExcelGenerationForCapturedData() {

        LOG.trace("Obtained Lead Capture Node data from LeadCaptureConfigurationService as {}",
                config.getLeadCaptureUserDataNodePath());

        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.trace("Obtained resourceResolver as {}", resourceResolver);
            if (null != resourceResolver) {
                Resource userDataNodeResource = resourceResolver.getResource(config.getLeadCaptureUserDataNodePath());
                final String FILE_NAME = PREFIX + "-" + fetchCurrentDate() + "-";
                final File tempFile = File.createTempFile(FILE_NAME, SUFFIX);

                LOG.trace("File name & absolute path of the file {}", tempFile.getAbsolutePath());

                List<LeadCaptureModel> userData = prepareDataForCSV(userDataNodeResource);
                if (!userData.isEmpty()) {
                    LOG.trace("Creating the CSV..");

                    generateCSV(userData, tempFile);

                    LOG.trace("CSV file has been created & initiating the transmission of the file to the admins");

                    emailService.sendLeadCaptureExcel(tempFile, config.getAdminMailId(), config.getAdminMailIdCC());

                    LOG.trace("Email Transmission is completed");
                } else {
                    LOG.trace("User Data is empty for the time period ,hence CSV generation and mail trigger halted..");
                }
            }
        } catch (Exception e) {
            LOG.trace("Exception occured on method triggerExcelGenerationForCapturedData {}", e.getStackTrace());
        } finally {
            resourceResolver.close();
        }

    }

    private List<LeadCaptureModel> prepareDataForCSV(Resource userDataNodeResource) {

        List<LeadCaptureModel> data = new ArrayList<LeadCaptureModel>();
        final String[] lastSevenDays = fetchLastSevenDays();
        try {
            for (String date : lastSevenDays) {
                String[] splitDate = date.split("-");
                String currentYear = splitDate[0];
                String currentMonth = splitDate[1];
                String currentDay = splitDate[2];
                Node userDataNode = userDataNodeResource.adaptTo(Node.class);
                if (userDataNode.hasNode(currentYear)) {
                    Node yearNode = userDataNode.getNode(currentYear);
                    if (yearNode.hasNode(currentMonth)) {
                        Node monthNode = yearNode.getNode(currentMonth);
                        if (monthNode.hasNode(currentDay)) {
                            Node dayNode = monthNode.getNode(currentDay);
                            if (dayNode.hasNodes()) {
                                Resource dayNodeResource = resourceResolver.getResource(dayNode.getPath());
                                if (null != dayNodeResource) {
                                    Iterable<Resource> userDataNodeResources = dayNodeResource.getChildren();
                                    for (Resource child : userDataNodeResources) {
                                        LeadCaptureModel model = new LeadCaptureModel();
                                        ValueMap map = child.adaptTo(ValueMap.class);
                                        model.setUniqueIdentifier(child.getName());
                                        model.setTimeSubmitted(getProperty(map, "timeSubmitted", String.class, "NA"));
                                        model.setConatctNumber(getProperty(map, "phoneNumber", String.class, "NA"));
                                        model.setFullName(getProperty(map, "name", String.class, "NA"));
                                        model.setGuestEmailId(getProperty(map, "guestEmailId", String.class, "NA"));
                                        model.setNoOfGuests(getProperty(map, "guest", String.class, "NA"));
                                        model.setPurposeOfTheEvent(
                                                getProperty(map, "purposeOfTheEvent", String.class, "NA"));
                                        model.setTimeOfTheEvent(getProperty(map, "timeOfTheEvent", String.class, "NA"));

                                        model.setArrivalDate(
                                                getProperty(map, "bookingArrivalDate", String.class, "NA"));
                                        model.setDepartureDate(
                                                getProperty(map, "bookingDepartureDate", String.class, "NA"));
                                        model.setBudget(getProperty(map, "tentativeBudget", String.class, "NA"));
                                        model.setNoOfRooms(getProperty(map, "noOfRooms", String.class, "NA"));
                                        model.setNoOfSingleRooms(getProperty(map, "singleRooms", String.class, "NA"));
                                        model.setNoOfDoubleRooms(getProperty(map, "doubleRooms", String.class, "NA"));
                                        model.setNoOfTripleRooms(getProperty(map, "tripleRooms", String.class, "NA"));
                                        model.setSpecialRequests(
                                                getProperty(map, "specialRequests", String.class, "NA"));
                                        model.setLocation(getProperty(map, "hotelName", String.class, "NA"));
                                        data.add(model);
                                    }
                                }
                            } else {
                                LOG.trace("Day node was created , user data was deleted manually for {}", currentDay);
                            }
                        } else {
                            LOG.trace("No user form submission during this particular day {}", currentDay);
                        }
                    } else {
                        LOG.trace("No form submission yet during the month {}", currentMonth);
                    }
                } else {
                    LOG.trace("No form submission yet during begining of the year {}", currentYear);
                }
            }
        } catch (Exception e) {
            LOG.trace("Exception occured on method prepareDataForCSV {}", e.getStackTrace());
        }
        return data;
    }


    private void generateCSV(List<LeadCaptureModel> userDataList, File csvFile) {

        try {
            FileWriter fileWriter = new FileWriter(csvFile);
            // CSV Headers
            String[] headers = { "Id", "Form Submission Date", "Full Name", "Email-Id", "Contact Number",
                    "Venue Location", "Arrival Date", "Departure Date", "No of Guests", "No of Rooms",
                    "No of Single Rooms", "No of Double Rooms", "No of Triple Rooms", "Purpose of Event",
                    "Time Slot of Event", "Tentative Budget", "Special Request" };


            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.append(Arrays.toString(headers).replaceAll("\\[|\\]", ""));
            bw.newLine();

            for (LeadCaptureModel userData : userDataList) {
                StringBuffer sbf = new StringBuffer();

                sbf.append(surroundQuotesWithNullCheck(userData.getUniqueIdentifier()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getTimeSubmitted()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getFullName()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getGuestEmailId()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getConatctNumber()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getLocation()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getArrivalDate()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getDepartureDate()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getNoOfGuests()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getNoOfRooms()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getNoOfSingleRooms()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getNoOfDoubleRooms()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getNoOfTripleRooms()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getPurposeOfTheEvent()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getTimeOfTheEvent()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getBudget()));
                sbf.append(CSV_SEPARATOR);
                sbf.append(surroundQuotesWithNullCheck(userData.getSpecialRequests()));
                sbf.append(CSV_SEPARATOR);

                bw.write(sbf.toString());
                bw.newLine();
            }
            bw.flush();
            bw.close();

        } catch (Exception e) {
            LOG.trace("Exception occured in generateCSV method {}", e.getStackTrace());
        }

    }

    private String surroundQuotesWithNullCheck(String string) {
        String surroundedString = string;
        if (null != string) {
            if (string.contains(",")) {
                if (string.contains("\"")) {
                    string.replaceAll("\"", "\"\"");
                }
                surroundedString = "\"".concat(string).concat("\"");
            }
        } else {
            surroundedString = "";
        }
        return surroundedString;
    }

    private String[] fetchLastSevenDays() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        String[] days = new String[7];
        days[0] = sdf.format(date);

        for (int i = 0; i < 7; i++) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            date = cal.getTime();
            days[i] = sdf.format(date);
        }
        return days;
    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }


    private String fetchCurrentDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String timeStamp = sdf.format(date);
        LOG.trace("Current Date {}", timeStamp);
        return timeStamp;
    }

}
