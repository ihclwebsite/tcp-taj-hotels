package com.ihcl.core.services.search;

import java.util.HashMap;
import java.util.List;

public interface OffersSearchService {

    List<String> getAllOffers(String homePagePath);

    List<String> getAllOffersByRank(String offerPath);

    List<String> getSelectedOffersByRank(List<String> pathsToLookupOffers);

    List<String> getAllOffersForCountry(String countryName);

    List<String> getAllOffersForHotel(List<String> hotelNames);

    List<String> getAllOffersForHotel(String hotelPath);

    List<String> getAllOffersForHotelPath(String hotelPath);

    List<String> getCorporateOffersForHotelPath(String hotelPath);

    HashMap<String, String> getOfferCategories();

    String fetchOfferFromUUID(String uuid);

    String getLocalOfferTemplate(String hotelPath);

    String findLocalOfferForGlobalOfferName(String hotelPath,String globalOfferPath);
}
