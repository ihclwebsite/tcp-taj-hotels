package com.ihcl.core.services.serviceamenity;

/**
 * Service to fetch group icon from the given group name.
 * 
 * @author moonraft
 *
 */
public interface AmenityGroupIconMapper {

	/**
	 * Method to get the group icon path for the given group name.
	 * 
	 * @param groupName
	 * @return groupIconPath
	 */
	String getIconPath(String groupName);
	
}
