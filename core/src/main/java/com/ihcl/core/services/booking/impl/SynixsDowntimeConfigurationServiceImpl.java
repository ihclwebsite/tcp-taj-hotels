/**
 *
 */
package com.ihcl.core.services.booking.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.SynixsDowntimeConfiguration;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;

/**
 * @author Vijay Chikkani
 *
 */
@Component(immediate = true,
        service = SynixsDowntimeConfigurationService.class)
@Designate(ocd = SynixsDowntimeConfiguration.class)
public class SynixsDowntimeConfigurationServiceImpl implements SynixsDowntimeConfigurationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SynixsDowntimeConfigurationServiceImpl.class);

    private SynixsDowntimeConfiguration synixsDowntimeConfiguration;

    @Reference
    private SlingSettingsService slingSettingsService;

    @Activate
    public void activate(SynixsDowntimeConfiguration synixsDowntimeConfiguration) {

        String methodName = "activate";
        LOGGER.trace("Method Entry ::: {}", methodName);

        this.synixsDowntimeConfiguration = synixsDowntimeConfiguration;

        LOGGER.trace(
                "Properties Fetched from com.ihcl.core.services.booking.impl.SynixsDowntimeConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOGGER.trace("Method Exit ::: {}", methodName);
    }


    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOGGER.trace("Method Entry ::: {}", methodName);
        LOGGER.trace("Present Run Mode ==> {}", slingSettingsService.getRunModes().toString());

        String env = "";

        if (slingSettingsService.getRunModes().contains("author"))
            env = "LOCAL";
        else if (slingSettingsService.getRunModes().contains("dev"))
            env = "DEV";
        else if (slingSettingsService.getRunModes().contains("stage"))
            env = "STAGING";

        LOGGER.trace("present ENV ==> " + env + "getDowntimeStarts :: > " + getDowntimeStarts());
        LOGGER.trace("Method Exit ::: {}", methodName);
    }

    @Override
    public boolean isForcingToSkipCalls() {
        return synixsDowntimeConfiguration.forcingToSkipCalls();
    }

    @Override
    public String getDowntimeStarts() {
        return synixsDowntimeConfiguration.downtimeStarts();
    }

    @Override
    public String getDowntimeEnds() {
        return synixsDowntimeConfiguration.downtimeEnds();
    }

    @Override
    public String getServletStatusCode() {
        return synixsDowntimeConfiguration.servletStatusCode();
    }

    @Override
    public String getDowntimeMessage() {
        return synixsDowntimeConfiguration.downtimeMessage();
    }

    @Override
    public String getRedirectPagePath() {
        return synixsDowntimeConfiguration.getRedirectPagePath();
    }

}
