package com.ihcl.core.services.config;


public interface TajEventBookingConfigurationService {

    String getEventAdminEmailId();

    String getCustomerEmailSubject();

    String getCustomerEmailBodyTemplate();

    String getHotelEmailSubject();

    String getHotelEmailBodyTemplate();

    String getConfirmationPagePath();

    String getFailurePagePath();

}
