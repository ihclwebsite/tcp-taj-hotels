package com.ihcl.core.services.leadcapture.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;


@ObjectClassDefinition(name = "Taj Lead Capture Configuration",
        description = "Configure the Lead Capture Admin and path from which CSV data needs to be fetched")
public @interface LeadCaptureConfiguration {

    @AttributeDefinition(name = "Expression",
            description = "Path where user data is captured and stored")
    String userDataNodePath() default "/content/usergenerated/meeting-data";

    @AttributeDefinition(name = "Admin Mail Id",
            description = "Admin Mail Id, to whom the Coalated CSV data needs to be sent")
    String adminMailId() default "titus@moonraft.com";

    @AttributeDefinition(name = "Admin CC Mail Id",
            description = "CC Admin Mail Ids , to whom the Coalated CSV data needs to be sent")
    String adminMailIdCC() default "shwetha@moonraft.com";

}
