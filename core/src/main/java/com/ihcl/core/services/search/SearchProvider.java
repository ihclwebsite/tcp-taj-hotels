package com.ihcl.core.services.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.facets.Facet;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

@Component(service = SearchProvider.class)
public class SearchProvider {

    private static final Logger LOG = LoggerFactory.getLogger(SearchProvider.class);

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    private QueryBuilder queryBuilder;

    public List<com.ihcl.core.models.search.SearchResult> executeQuery(HashMap<String, String> predicates) {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
                    resourceResolver.adaptTo(Session.class));
            SearchResult queryResult = query.getResult();
            LOG.trace("Received query result as: " + queryResult);
            return buildSearchResults(queryResult);
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return null;
    }

    public Map<String, Facet> fetchFacets(HashMap<String, String> predicates) throws RepositoryException {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
                    resourceResolver.adaptTo(Session.class));
            return query.getResult().getFacets();
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return null;
    }

    private List<com.ihcl.core.models.search.SearchResult> buildSearchResults(SearchResult result) {
        final List<com.ihcl.core.models.search.SearchResult> queryResults = new ArrayList<com.ihcl.core.models.search.SearchResult>();
        LOG.debug(String.valueOf(result.getHits().size()));
        for (Hit hit : result.getHits()) {
            try {

                Resource resource = hit.getResource();
                LOG.debug("Attempting to adapt the resource: " + resource);
                final com.ihcl.core.models.search.SearchResult searchResult = resource
                        .adaptTo(com.ihcl.core.models.search.SearchResult.class);
                LOG.debug("Adapted the received resource to search result as: " + searchResult);

                List<String> excerpts = new ArrayList<>();
                excerpts.add(hit.getExcerpt());
                queryResults.add(searchResult);
            } catch (RepositoryException e) {
                LOG.error("Unable to adapt this hit's resource to a Search Result :: {}", e);
            }
        }
        return queryResults;

    }

    public SearchResult getQueryResult(HashMap<String, String> predicates) {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
                    resourceResolver.adaptTo(Session.class));
            return query.getResult();
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e.getMessage());
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return null;
    }

    public SearchResult getQueryResultWithoutClosing(HashMap<String, String> predicates) {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
                    resourceResolver.adaptTo(Session.class));
            return query.getResult();
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e.getMessage());
        }
        return null;
    }

}
