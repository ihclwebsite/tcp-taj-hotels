/**
 *
 */
package com.ihcl.core.services.booking.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.GiftcardConfiguration;
import com.ihcl.core.services.booking.GiftcardConfigurationService;

@Component(immediate = true,
        service = GiftcardConfigurationService.class)
@Designate(ocd = GiftcardConfiguration.class)
public class GiftcardConfigurationServiceImpl implements GiftcardConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(GiftcardConfigurationServiceImpl.class);

    private GiftcardConfiguration giftcardConfiguration;

    @Reference
    private SlingSettingsService slingSettingsService;


    @Activate
    public void activate(GiftcardConfiguration giftcardConfiguration) {

        String methodName = "activate";
        LOG.trace("Method Entry ::: {}", methodName);

        this.giftcardConfiguration = giftcardConfiguration;

        LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.GIftcardConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOG.trace("Method Exit ::: {}", methodName);
    }

    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOG.trace("Method Entry ::: {}", methodName);
        LOG.trace("Present Run Mode ==> {}", slingSettingsService.getRunModes().toString());

        String env = "";

        if (slingSettingsService.getRunModes().contains("author"))
            env = "LOCAL";
        else if (slingSettingsService.getRunModes().contains("dev"))
            env = "DEV";
        else if (slingSettingsService.getRunModes().contains("stage"))
            env = "STAGING";

        LOG.trace("present ENV ==> " + env + "Sample env url :: > " + getCCAvenueRedirectURL());
        LOG.trace("Method Exit ::: {}", methodName);
    }

    @Override
    public String getCCAvenueURL() {

        return giftcardConfiguration.ccAvenueURL();
    }

    @Override
    public String getCCAvenueWorkingKey() {

        return giftcardConfiguration.ccAvenueWorkingKey();
    }

    @Override
    public String getCCAvenueAccessKey() {

        return giftcardConfiguration.ccAvenueAccessKey();
    }

    @Override
    public String getCCAvenueRedirectURL() {

        return giftcardConfiguration.ccAvenueReDirectURL();
    }

    @Override
    public String getGiftcardFailedURL() {

        return giftcardConfiguration.giftcardFailedURL();
    }

    @Override
    public String getGiftcardConfirmationURL() {

        return giftcardConfiguration.giftcardConfirmationURL();
    }

    @Override
    public String getOnlineMerchantId() {
        LOG.debug("Value of config paymentMerchantID: {}", giftcardConfiguration.paymentMerchantID());
        return giftcardConfiguration.paymentMerchantID();
    }
}
