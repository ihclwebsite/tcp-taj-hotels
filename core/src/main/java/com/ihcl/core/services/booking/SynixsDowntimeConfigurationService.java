/**
 *
 */
package com.ihcl.core.services.booking;


/**
 * @author Vijay Chikkani
 *
 */
public interface SynixsDowntimeConfigurationService {

    boolean isForcingToSkipCalls();

    String getDowntimeStarts();

    String getDowntimeEnds();

    String getServletStatusCode();

    String getDowntimeMessage();

    String getRedirectPagePath();

}
