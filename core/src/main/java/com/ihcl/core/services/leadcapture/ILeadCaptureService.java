package com.ihcl.core.services.leadcapture;


public interface ILeadCaptureService {

    void triggerExcelGenerationForCapturedData();

}
