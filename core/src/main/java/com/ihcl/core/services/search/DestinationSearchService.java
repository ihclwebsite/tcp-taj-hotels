package com.ihcl.core.services.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ihcl.core.models.Destination;

public interface DestinationSearchService {

    List<String> getAllDestinations(String destinationPath);

    List<String> getDestinationsByRank();

    HashMap<String, String> getDestinationByCountry(String countryTagId);

    List<String> getDestinationByType(String destinationType);

    List<String> getDestinationForHotels(List<String> hotelNames);

    List<String> getDestinationForHotel(String hotelName);

    List<Destination> getAllDestinationModels(String destinationPath);

    Map<String, List<Destination>> getDestinationModelMap(String destinationPath);

    List<String> getAllHolidayDestinations();

    List<String> getAllHolidayHotels();

    List<String> getAllHolidayHotelOffers(String HotelPath);
}
