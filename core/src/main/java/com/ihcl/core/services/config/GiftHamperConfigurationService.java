/**
 *
 */
package com.ihcl.core.services.config;


/**
 * @author Vijay Chikkani
 *
 */
public interface GiftHamperConfigurationService {

    String getCustomerEmailSubject();

    String getCustomerEmailBodyTemplate();

    String getHotelEmailSubject();

    String getHotelEmailBodyTemplate();

    String getConfirmationPagePath();

    String getFailurePagePath();

}
