package com.ihcl.core.services.search;

import java.util.List;
import java.util.Map;

public interface GlobalSearchService {
	
	Map<String, String> fetchAllCountries();

	Map<String, String> fetchAllHotelTypes();
	
	List<String> fetchAllOfferTypes();
}
