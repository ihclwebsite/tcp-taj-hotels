/**
 *
 */
package com.ihcl.core.services;

/**
 * @author Srikanta.moonraft
 *
 */
public interface ImageCropperService {

    /**
     * @param incomingImageUrl
     * @param x
     * @param y
     * @param width
     * @param height
     * @return
     */
    String cropImageFromUrl(String incomingImageUrl, int x, int y, int width, int height);
}
