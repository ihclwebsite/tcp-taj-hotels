package com.ihcl.core.services.booking.impl;

import com.ihcl.core.services.booking.RecaptchaConfiguration;
import com.ihcl.core.services.booking.RecaptchaConfigurationService;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true,
        service = RecaptchaConfigurationService.class)
@Designate(ocd = RecaptchaConfiguration.class)
public class RecaptchaConfigurationServiceImpl implements RecaptchaConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(RecaptchaConfigurationServiceImpl.class);

    private RecaptchaConfiguration config;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(RecaptchaConfiguration config) {

        String methodName = "activate";
        this.config = config;
        LOG.trace("Method Entry ::: " + methodName);
    }

    @Override
    public String getSiteKey() {
        return config.sitekey();
    }

    @Override
    public String getSecretKey() {
        return config.secretkey();
    }
}
