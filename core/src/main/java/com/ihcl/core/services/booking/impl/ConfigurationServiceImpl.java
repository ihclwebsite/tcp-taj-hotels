package com.ihcl.core.services.booking.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.ConfigurationService;
import com.ihcl.core.services.booking.ServiceConfiguration;

@Component(immediate = true,
        service = ConfigurationService.class)
@Designate(ocd = ServiceConfiguration.class)
public class ConfigurationServiceImpl implements ConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceImpl.class);

    private ServiceConfiguration config;

    @Reference
    private SlingSettingsService settings;


    @Activate
    public void activate(ServiceConfiguration config) {

        String methodName = "activate";
        LOG.trace("Method Entry ::: " + methodName);

        this.config = config;

        LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.ConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOG.trace("Method Exit ::: " + methodName);
    }

    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOG.trace("Method Entry ::: " + methodName);
        LOG.trace("Present Run Mode ==> " + settings.getRunModes().toString());

        String env = "";

        if (settings.getRunModes().contains("author"))
            env = "LOCAL";
        else if (settings.getRunModes().contains("dev"))
            env = "DEV";
        else if (settings.getRunModes().contains("stage"))
            env = "STAGING";

        LOG.trace("present ENV ==> " + env + "Sample env url :: > " + getCCAvenueRedirectURL());
        LOG.trace("Method Exit ::: " + methodName);
    }

    @Override
    public String getCCAvenueURL() {

        return config.ccAvenueURL();
    }

    @Override
    public String getCCAvenueWorkingKey() {

        return config.ccAvenueWorkingKey();
    }

    @Override
    public String getCCAvenueAccessKey() {

        return config.ccAvenueAccessKey();
    }

    @Override
    public String getCCAvenueRedirectURL() {

        return config.ccAvenueReDirectURL();
    }

    @Override
    public String getBookingFailedURL() {

        return config.bookingFailedURL();
    }

    @Override
    public String getBookingConfirmationURL() {

        return config.bookingConfirmationURL();
    }

    @Override
    public String getOnlineMerchantId() {
        LOG.debug("Value of config paymentMerchantID: " + config.paymentMerchantID());
        return config.paymentMerchantID();
    }

    @Override
    public String getEpicurePaymentRedirectUrl() {
        return config.epicurePaymentRedirectUrl();
    }

    @Override
    public String getEpicureEnrollRedirectUrl() {
        return config.epicureEnrollRedirectUrl();
    }


    @Override
    public String getPurchasePaymentRedirectUrl() {
        return config.purchasePaymentRedirectUrl();
    }

    @Override
    public String getPurchasePaymentSuccess() {
        return config.getPurchasePaymentSuccess();
    }

    @Override
    public String getepicureEnrollAmount() {
        return config.epicureEnrollAmount();
    }

    @Override
    public String getPurchaseMerchantId() {
        return config.getPurchaseMerchantId();
    }


    @Override
    public String getPurchaseCCAvenueWorkingKey() {
        return config.getPurchaseCCAvenueWorkingKey();
    }


    @Override
    public String getPurchaseCCAvenueAccessKey() {
        return config.getPurchaseCCAvenueAccessKey();
    }

    @Override
    public String getCamapignEpicureAmount() {
        return config.epicureCampaignEnrollAmount();
    }


}
