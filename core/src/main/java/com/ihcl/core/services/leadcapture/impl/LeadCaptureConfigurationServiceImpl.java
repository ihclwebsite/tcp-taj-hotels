/**
 *
 */
package com.ihcl.core.services.leadcapture.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.leadcapture.LeadCaptureConfigurationService;
import com.ihcl.core.services.leadcapture.config.LeadCaptureConfiguration;


@Component(immediate = true,
        service = LeadCaptureConfigurationService.class)
@Designate(ocd = LeadCaptureConfiguration.class)
public class LeadCaptureConfigurationServiceImpl implements LeadCaptureConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(LeadCaptureConfigurationServiceImpl.class);

    private LeadCaptureConfiguration config;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(LeadCaptureConfiguration config) {

        LOG.trace("Acivate method of LeadCaptureConfigurationServiceImpl");
        LOG.trace("current  ENV. {}", settings.getRunModes());
        this.config = config;
    }

    @Override
    public String getLeadCaptureUserDataNodePath() {
        return config.userDataNodePath();
    }

    @Override
    public String getAdminMailId() {
        return config.adminMailId();
    }

    @Override
    public String getAdminMailIdCC() {
        return config.adminMailIdCC();
    }

}
