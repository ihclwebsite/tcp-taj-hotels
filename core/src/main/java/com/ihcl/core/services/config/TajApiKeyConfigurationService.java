/**
 *
 */
package com.ihcl.core.services.config;


/**
 * @author Srikanta.moonraft
 *
 */
public interface TajApiKeyConfigurationService {

    String getOpenWeatherMapApiKey();

    String getGoogleMapApiKey();

    String getHotelChainCode();

    String getTajAdminEmailId();

    String getDevelopmentAdminEmailId();

    String getIHCLWriteToUsAdminEmailId();

    String getEtcMapLocation();

    String[] getUrlsToBeRedirected();
    
    String getIhclOwnerUsername();
    
    String getIhclOwnerPassword();

	String getChatBotId();


}
