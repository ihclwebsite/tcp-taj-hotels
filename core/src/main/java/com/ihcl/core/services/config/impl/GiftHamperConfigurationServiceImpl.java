/**
 *
 */
package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.ServiceConfiguration;
import com.ihcl.core.services.config.GiftHamperConfiguration;
import com.ihcl.core.services.config.GiftHamperConfigurationService;

@Component(immediate = true,
        service = GiftHamperConfigurationService.class)
@Designate(ocd = GiftHamperConfiguration.class)
public class GiftHamperConfigurationServiceImpl implements GiftHamperConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(GiftHamperConfigurationServiceImpl.class);

    private GiftHamperConfiguration giftHamperConfiguration;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(GiftHamperConfiguration giftHamperConfiguration) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> {}", settings.getRunModes());
        this.giftHamperConfiguration = giftHamperConfiguration;

        LOG.trace("Method Exit ::: activate()");
    }

    @Override
    public String getCustomerEmailSubject() {
        return giftHamperConfiguration.customerEmailSubject();
    }

    @Override
    public String getCustomerEmailBodyTemplate() {
        return giftHamperConfiguration.customerEmailBodyTemplate();
    }

    @Override
    public String getHotelEmailSubject() {
        return giftHamperConfiguration.hotelEmailSubject();
    }

    @Override
    public String getHotelEmailBodyTemplate() {
        return giftHamperConfiguration.hotelEmailBodyTemplate();
    }

    @Override
    public String getConfirmationPagePath() {
        return giftHamperConfiguration.getConfirmationPagePath();
    }

    @Override
    public String getFailurePagePath() {
        return giftHamperConfiguration.getFailurePagePath();
    }

}
