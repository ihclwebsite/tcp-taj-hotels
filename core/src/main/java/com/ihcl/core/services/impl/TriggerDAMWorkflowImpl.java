package com.ihcl.core.services.impl;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.model.WorkflowModel;
import com.ihcl.core.services.TriggerDAMWorkflow;

@Component(service = TriggerDAMWorkflow.class, immediate = true)
public class TriggerDAMWorkflowImpl implements TriggerDAMWorkflow {
	@Reference
	private WorkflowService workflowService;

	private Session session;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Override
	public String startWorkflow(String workflowName, String workflowContent) {

		try {
			// Invoke the adaptTo method to create a Session
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
			session = resourceResolver.adaptTo(Session.class);

			// Create a workflow session
			WorkflowSession wfSession = workflowService.getWorkflowSession(session);

			// Get the workflow model
			WorkflowModel wfModel = wfSession.getModel(workflowName);

			// Get the workflow data
			// The first param in the newWorkflowData method is the payloadType. Just a
			// fancy name to let it know what type of workflow it is working with.
			WorkflowData wfData = wfSession.newWorkflowData("JCR_PATH", workflowContent);

			// Run the Workflow.
			wfSession.startWorkflow(wfModel, wfData);

			return workflowName + " has been successfully invoked on this content: " + workflowContent;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
