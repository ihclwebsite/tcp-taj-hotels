package com.ihcl.core.services.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.ihcl.core.models.search.SearchResult;
import com.ihcl.core.services.search.SearchServiceConstants.PATH;
import com.ihcl.core.services.search.SearchServiceConstants.RESOURCETYPE;

@Component(service = HotelsSearchService.class)
public class HotelSearchImpl implements HotelsSearchService {

    private static final Logger LOG = LoggerFactory.getLogger(HotelSearchImpl.class);

    @Reference
    SearchProvider searchProvider;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Override
    public Map<String, String> getHotelByDestination(String destinationTagPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        Map<String, String> hotelList = new HashMap<>();
        predicates.put("path", PATH.HOTELROOT);
        predicates.put("tagid.property", "locations");
        predicates.put("tagid", destinationTagPath);
        predicates.put("2_property", "sling:resourceType");
        predicates.put("2_property.value", RESOURCETYPE.HOTELS);
        predicates.put("p.limit=", "-1");
        List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
        for (SearchResult searchResult : searchResults) {
            ValueMap valueMap = resourceResolverFactory.getThreadResourceResolver().getResource(
                    searchResult.getPath() + "/jcr:content").adaptTo(ValueMap.class);
            String hotelName = valueMap.get("hotelName", String.class);
            hotelList.put(hotelName, searchResult.getPath());
        }
        return hotelList;
    }

    @Override
    public List<String> getHotelPathsByDestination(String destinationTagPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        List<String> hotelList = new ArrayList<String>();
        predicates.put("path", PATH.HOTELROOT);
        predicates.put("tagid.property", "locations");
        predicates.put("tagid", destinationTagPath);
        predicates.put("2_property", "sling:resourceType");
        predicates.put("2_property.value", RESOURCETYPE.HOTELS);
        predicates.put("p.limit=", "-1");
        List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
        for (SearchResult searchResult : searchResults) {
            hotelList.add(searchResult.getPath());
        }
        return hotelList;
    }

    @Override
    public List<String> getHotelsByCountry(String countryTagPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        ArrayList<String> hotelList = new ArrayList<String>();
        predicates.put("path", PATH.HOTELROOT);
        predicates.put("1_property", "sling:resourceType");
        predicates.put("1_property.value", RESOURCETYPE.HOTELS);
        predicates.put("tagid.property", "locations");
        predicates.put("tagid", countryTagPath);
        predicates.put("p.limit", "-1");
        List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
        for (SearchResult searchResult : searchResults) {
            hotelList.add(searchResult.getPath());
        }
        return hotelList;
    }

    @Override
    public List<String> getHotelsByType(String hotelType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getHotelPathForName(String hotelName) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        String hotelPath = "";
        predicates.put("path", PATH.HOTELROOT);
        predicates.put("1_property", "jcr:title");
        predicates.put("1_property.value", hotelName);
        predicates.put("2_property", "sling:resourceType");
        predicates.put("2_property.value", RESOURCETYPE.HOTELS);
        predicates.put("p.limit", "-1");
        List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
        if (searchResults != null && searchResults.size() > 0) {
            hotelPath = searchResults.get(0).getPath();
        }
        return hotelPath;
    }

    @Override
    public HashMap<String, String> getHotelTypes() {
        HashMap<String, String> hotelTypes = new HashMap<String, String>();
        ResourceResolver resourceResolver;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource tagRoot = resourceResolver.getResource("/etc/tags/taj/hotels/types");
            Iterable<Resource> childTags = tagRoot.getChildren();
            for (Resource resource : childTags) {
                Tag tag = resource.adaptTo(Tag.class);
                hotelTypes.put(tag.getTagID(), tag.getTitle());
            }
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance");
        }
        return hotelTypes;
    }

    @Override
    public List<String> getHotelPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "jcr:content/sling:resourceType");
            predicateMap.put("property.value", RESOURCETYPE.HOTELS);
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }

    @Override
    public List<String> getHotelIdsByDestinationPath(String ourHotelsPath, String destinationTag) {
        List<String> hotelIds = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        predicateMap.put("path", ourHotelsPath);
        predicateMap.put("tagid.property", "locations");
        predicateMap.put("tagid", destinationTag);
        predicateMap.put("2_property", "sling:resourceType");
        predicateMap.put("2_property.value", RESOURCETYPE.HOTELS);
        List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
        LOG.debug("searchResults.size(): {}", searchResults.size());
        for (SearchResult searchResult : searchResults) {
            LOG.debug("searchResult.getPath(): {}", searchResult.getPath());
            ResourceResolver resourceResolver = null;
            try {
                Map<String, Object> param = new HashMap<>();
                param.put(ResourceResolverFactory.SUBSERVICE, "getResourceResolver");
                resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);
                Resource hotelResource = resourceResolver.getResource(searchResult.getPath() + "/jcr:content");
                ValueMap valueMap = hotelResource.adaptTo(ValueMap.class);
                if (valueMap.get("cq:lastReplicationAction", String.class).equalsIgnoreCase("Activate")) {
                    String hotelId = valueMap.get("hotelId", String.class);
                    LOG.debug("hotelId: {}", hotelId);
                    hotelIds.add(hotelId);
                }
            } catch (Exception exception) {
                LOG.debug("Exception occured while getting hotelIds using destination path or id: {}",
                        exception.getMessage());
                LOG.trace("Exception occured while getting hotelIds using destination path or id: {}", exception);
            } finally {
                if (resourceResolver != null && resourceResolver.isLive()) {
                    resourceResolver.close();
                }
            }
        }
        return hotelIds;
    }

    /**
     * Takes a String and an array of substrings to search substring inside the string Returns index of the substring
     * from the array of substrings
     */
    private int stringContainsItemFromList(String inputStr, String[] items) {
        for (int i = 0; i < items.length; i++) {
            if (inputStr.contains(items[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public List<String> getAllHotelResourcePath() {
        List<String> AllHotelResourcePath = new ArrayList<>();

        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("type", "cq:Page");
            predicateMap.put("path", "/content/tajhotels/en-in/our-hotels");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                AllHotelResourcePath.add(searchResult.getPath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AllHotelResourcePath;
    }

    @Override
    public List<String> getAllHotels(String hotelSearchPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        ArrayList<String> hotelList = new ArrayList<String>();
        predicates.put("path", hotelSearchPath);
        predicates.put("property", "sling:resourceType");
        predicates.put("property.value", RESOURCETYPE.HOTELS);
        predicates.put("p.limit", "-1");
        predicates.put("orderby", "@listingRanker");
        predicates.put("orderby.sort", "asc");
        //predicates.put("orderby.case", "ignore");
        List<SearchResult> searchResults = searchProvider.executeQuery(predicates);

        LOG.info("-------------list-------------------");
        for (SearchResult searchResult : searchResults) {
            LOG.info(searchResult.getPath());
            hotelList.add(searchResult.getPath());
        }
        LOG.info("-------------list-------------------");
        return hotelList;
    }

    @Override
    public Set<String> getAllHotelsInsideIndia() {
        LOG.trace("inside getAllHotelsInsideIndia");
        int i;
        Set<String> hotelList = new HashSet<String>();
        HashMap<String, String> predicates = new HashMap<String, String>();
        try {
            predicates.put("path", "/content/tajhotels/en-in/our-hotels");
            // predicates.put("locations", "taj:destination/india/bangalore");
            predicates.put("1_property", "sling:resourceType");
            predicates.put("1_property.value", "tajhotels/components/structure/hotels-landing-page");
            predicates.put("2_property", "countryName");
            predicates.put("2_property.1_value", "India");
            predicates.put("2_property.2_value", "INDIA");
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            LOG.trace("searchResults -- -- " + searchResults.size());

            for (SearchResult searchResult : searchResults) {
                hotelList.add(searchResult.getPath());
                LOG.trace("-- -- -- -- -- -- -- --searchResult.getPath() -- " + searchResult.getPath());
                LOG.trace("-- -- -- -- -- -- -- --searchResult.getTitle() -- " + searchResult.getTitle());
            }
            LOG.trace("hotelList SIZE - " + hotelList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.trace("exit getAllHotelsInsideIndia");
        return hotelList;
    }

    @Override
    public Set<String> getAllHotelsOutsideIndia() {
        LOG.trace("Outside getAllHotelsInsideIndia");
        int i;
        Set<String> hotelList = new HashSet<String>();
        HashMap<String, String> predicates = new HashMap<String, String>();
        try {
            predicates.put("path", "/content/tajhotels/en-in/our-hotels");
            // predicates.put("locations", "taj:destination/india/bangalore");
            predicates.put("1_property", "sling:resourceType");
            predicates.put("1_property.value", "tajhotels/components/structure/hotels-landing-page");
            predicates.put("2_property", "countryName");
            predicates.put("2_property.1_value", "India");
            predicates.put("2_property.operation", "unequals");
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            LOG.trace("searchResults -- -- " + searchResults.size());

            for (SearchResult searchResult : searchResults) {
                hotelList.add(searchResult.getPath());
                LOG.trace("-- -- -- -- -- -- -- --searchResult.getPath() -- " + searchResult.getPath());
                LOG.trace("-- -- -- -- -- -- -- --searchResult.getTitle() -- " + searchResult.getTitle());
            }
            LOG.trace("hotelList SIZE - " + hotelList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.trace("exit getAllHotelsInsideIndia");
        return hotelList;
    }

    @Override
    public com.day.cq.search.result.SearchResult getHotelsByKey(String searchKey) {
        HashMap<String, String> predicateMap = new HashMap<>();
        predicateMap.put("fulltext", searchKey);
        predicateMap.put("path", SearchServiceConstants.PATH.TAJ_ROOT);
        predicateMap.put("property", "sling:resourceType");
        predicateMap.put("property.1_value", SearchServiceConstants.RESOURCETYPE.HOTELS);
        predicateMap.put("orderby", "@jcr:score");
        predicateMap.put("orderby.sort", "desc");
        com.day.cq.search.result.SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
        return searchResult;
    }

    @Override
    public List<String> getBrandSpecificHotels(String brandName) {
        ArrayList<String> hotelList = new ArrayList<String>();
        try {
            HashMap<String, String> predicates = new HashMap<String, String>();
            String brandPath = "taj:hotels/brands/" + brandName;
            LOG.trace("inside the getBrandSpecificHotels for the type " + brandName);

            predicates.put("1_group.1_path", "/content/tajhotels/en-in/our-hotels");
            predicates.put("1_group.2_path", "/content/vivanta/en-in/our-hotels");
            predicates.put("1_group.3_path", "/content/seleqtions/en-in/our-hotels");
            predicates.put("1_group.p.or", "true");

            predicates.put("2_property", "sling:resourceType");
            predicates.put("2_property.value", RESOURCETYPE.HOTELS);
            predicates.put("3_property", "brand");
            predicates.put("3_property.value", brandPath);
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            for (SearchResult searchResult : searchResults) {
                hotelList.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Specific Hotel Brand Error - " + e);
        }
        LOG.trace("\nINSIDE the get brand specific hotels SIZE" + hotelList.size());
        return hotelList;
    }

    @Override
    public com.day.cq.search.result.SearchResult getHotelsByTeatments(String homePagePath) {
        String methodName = "getHotelsByTeatments";
        LOG.trace("Method Entry: " + methodName);
        HashMap<String, String> predicateMap = new HashMap<>();
        predicateMap.put("path", homePagePath + SearchServiceConstants.PATH.OUR_HOTELS);
        predicateMap.put("1_property", "sling:resourceType");
        predicateMap.put("1_property.value", SearchServiceConstants.RESOURCETYPE.HOTELS);
        predicateMap.put("2_property", "cq:tags");
        predicateMap.put("2_property.value", "taj:treatment-and-therapies");
        predicateMap.put("p.limit", "-1");
        LOG.debug("Predicate value received as :" + predicateMap);
        com.day.cq.search.result.SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
        LOG.trace("Method Exit: " + methodName);
        return searchResult;
    }

    @Override
    public List<String> getTicHotelPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "jcr:content/sling:resourceType");
            predicateMap.put("property.value", RESOURCETYPE.HOTELS);
            predicateMap.put("boolproperty", "jcr:content/noTic");
            predicateMap.put("boolproperty.value", "false");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }


    @Override
    public List<String> getRoomRedemptionTicHotelPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "jcr:content/sling:resourceType");
            predicateMap.put("property.value", RESOURCETYPE.HOTELS);
            predicateMap.put("boolproperty", "jcr:content/roomRedemptionTIC");
            predicateMap.put("boolproperty.value", "false");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }

    @Override
    public List<String> getTapHotelPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "cq:tags");
            predicateMap.put("property.value", "taj:tap");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }

    @Override
    public List<String> getTappmeHotelPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "cq:tags");
            predicateMap.put("property.value", "taj:tappme");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }

    @Override
    public List<String> getTajHolidaysRedemptionHotelsPathsByDestinationPath(String destinationPath) {
        List<String> hotels = new ArrayList<>();
        List<String> sortedHotels = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", destinationPath);
            predicateMap.put("property", "cq:tags");
            predicateMap.put("property.value", "taj:tic-room-redemption/taj-holidays-redemption");
            predicateMap.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        sortedHotels = hotels.stream().sorted((a, b) -> Integer
                .compare(stringContainsItemFromList(a, SearchServiceConstants.brandsSortOrder),
                        stringContainsItemFromList(b, SearchServiceConstants.brandsSortOrder))).collect(
                Collectors.toList());
        return sortedHotels;
    }
}