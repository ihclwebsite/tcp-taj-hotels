package com.ihcl.core.services.booking;

public interface RecaptchaConfigurationService {
    String getSiteKey();

    String getSecretKey();
}
