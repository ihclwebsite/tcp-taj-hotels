package com.ihcl.core.services;

public interface GoogleMapService  {
	
	
	String[]  getLatLong(String address);
	
	String getAddressFromLatLng(String apiKey, String latitude, String longitude);
}
