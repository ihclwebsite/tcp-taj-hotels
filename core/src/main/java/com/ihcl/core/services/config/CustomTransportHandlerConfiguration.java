package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Custom Transport Handler Configuration", description = "Taj TransportHandler Configurations")
public @interface CustomTransportHandlerConfiguration {
	
	@AttributeDefinition(name = "Dispatcher Flush Agent Protocol", description = "Custom protocol needs to be defined to trigger the CustomTransportHandler and the same has to configured or prepended at the publisher flush agents")
	String customProtocol() default "cdpa";

}
