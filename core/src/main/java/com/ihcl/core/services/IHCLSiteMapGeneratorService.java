/**
 *
 */
package com.ihcl.core.services;


public interface IHCLSiteMapGeneratorService {

    Boolean generateSiteMap(String hostString);

    Boolean generateImageSiteMap();
}
