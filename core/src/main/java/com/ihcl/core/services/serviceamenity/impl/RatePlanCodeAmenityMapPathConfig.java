package com.ihcl.core.services.serviceamenity.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Rate Plan Code to Service Amenity Map Path Configuration",
        description = "")
public @interface RatePlanCodeAmenityMapPathConfig {

    @AttributeDefinition(name = "Rate Plan Code Amenity Map Page Path",
            description = "Specify the page path at which the mapping for rate plan codes and service amenities is specified")
    String getRatePlanCodeAmenitiesMapPagePath() default "/content/tajhotels/en-in/shared-content/rate-code-amenity-map-page";
}
