package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "IHCLCB API Configuration",
        description = "IHCL CB Configuration")
public @interface IhclcbApiConfiguration {

	@AttributeDefinition(name = "IHCLCB Api Host",
            description = "Enter ihclcb Api host Url",
            type = AttributeType.STRING)
    String ihclcbApiHostUrl() default "https://tajprodics-a455764.integration.us2.oraclecloud.com";
	
    @AttributeDefinition(name = "B2B Corporate Booking URL",
            description = "Enter the B2B entity details Api url",
            type = AttributeType.STRING)
    String entityDetailsFetchApiUrl() default "/integration/flowapi/rest/CORP_BOOKING_TOOL/v01/contact-partyid";

    @AttributeDefinition(name = "B2C Corporate Booking URL",
            description = "Enter the B2C guest details Api URL",
            type = AttributeType.STRING)
    String guestDetailsFetchApiUrl() default "/integration/flowapi/rest/GUESTDETAILSONMEM/v01/GuestDetails";
 
    @AttributeDefinition(name = "IHCLCB Bookings Api URL",
            description = "Enter the IHCLCB Bookings Api URL",
            type = AttributeType.STRING)
    String ihclcbBookingApiUrl() default "/integration/flowapi/rest/MDMFETCHRESERVATION/v01/FetchReservation?q=ContactName_Id_c=";

}
