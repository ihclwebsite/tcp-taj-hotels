package com.ihcl.core.services.config;

public interface FlushCacheDispatcherConfigurationService {

    String[] getDispatcherIps();

    String getProtocol();

    String getDispatcherPort();

    String getRateCachePath();

    String getDestinationRatesCachePath();

    String[] getFlushHosts();

    String getRateRefreshServletUrl();


}
