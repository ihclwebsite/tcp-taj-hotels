package com.ihcl.core.services;

import java.util.List;

/**
 * Service performs a spell check and returns a list of possible correct words using lucene spellcheck index.
 * If the text is correct, no results are returned.
 */
public interface SpellCheck {
    List<String> performSpellCheck(String text);
}
