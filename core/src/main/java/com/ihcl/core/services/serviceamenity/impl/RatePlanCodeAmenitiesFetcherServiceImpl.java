package com.ihcl.core.services.serviceamenity.impl;

import com.day.crx.JcrConstants;
import com.ihcl.core.models.serviceamenity.ServiceAmenity;
import com.ihcl.core.services.serviceamenity.RatePlanCodeAmenitiesFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;
import com.ihcl.tajhotels.constants.CrxNodeNameConstants;
import com.ihcl.tajhotels.constants.CrxPropertyKeyConstants;
import org.apache.sling.api.resource.*;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Component(immediate = true, service = RatePlanCodeAmenitiesFetcherService.class)
@Designate(ocd = RatePlanCodeAmenityMapPathConfig.class)
public class RatePlanCodeAmenitiesFetcherServiceImpl implements RatePlanCodeAmenitiesFetcherService {

	private static final Logger LOG = LoggerFactory.getLogger(RatePlanCodeAmenitiesFetcherServiceImpl.class);

	private String ratePlanCodeAmenitiesMapPagePath;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Activate
	@Modified
	protected void activate(RatePlanCodeAmenityMapPathConfig config) {
		String methodName = "activate";
		LOG.trace("Method Entry: " + methodName);
		ratePlanCodeAmenitiesMapPagePath = config.getRatePlanCodeAmenitiesMapPagePath();
		LOG.trace("Method Exit: " + methodName);
	}

	@Override
	public List<ServiceAmenity> getMappedAmenitiesFor(String ratePlanCode) {
		String methodName = "getMappedAmenitiesFor";
		LOG.trace("Method Entry: " + methodName);
		Set<ServiceAmenity> mappedAmenities = new HashSet<>();

		try {
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
			Resource ratePlanCodeAmenitiesMapPage = resourceResolver.getResource(ratePlanCodeAmenitiesMapPagePath);
			LOG.debug("Received rate plan code amenities map page as: " + ratePlanCodeAmenitiesMapPage);
			Resource rootNodeResource = null;
			if (ratePlanCodeAmenitiesMapPage != null) {
				rootNodeResource = ratePlanCodeAmenitiesMapPage.getChild(JcrConstants.JCR_CONTENT + "/root");
			}
			LOG.debug("Received root node under rate plan code and amenities map page path as: " + rootNodeResource);
			if (rootNodeResource != null && rootNodeResource.hasChildren()) {
				LOG.debug("Attempting to iterate through children of root node.");
				for (Resource child : rootNodeResource.getChildren()) {
					if (child.isResourceType(CrxConstants.RATE_CODE_AMENITY_MAP_COMPONENT_RESOURCETYPE)) {
						if (isRatePlanCodeMapped(child, ratePlanCode)) {
							LOG.debug("Found a mapping entry for rate plan code: " + ratePlanCode);
							List<ServiceAmenity> selectedAmenities = getSelectedServiceAmenitiesUnder(child);
							LOG.debug("Attempting to add selected amenities: " + selectedAmenities + " to the set.");
							mappedAmenities.addAll(selectedAmenities);
							LOG.debug("Mapped amenities set after addition: " + mappedAmenities);
						}
					}
				}
			}
		} catch (LoginException e) {
			LOG.error(
					"An error occured while logging in to fetch page that contains rate plan code to amenity mapping.",
					e);
		}

		ArrayList<ServiceAmenity> amenitiesList = new ArrayList<>(mappedAmenities);
		LOG.debug("Returning mapped amenities for rate plan code: " + ratePlanCode + " as: " + amenitiesList);
		LOG.trace("Method Exit: " + methodName);
		return amenitiesList;
	}

	private List<ServiceAmenity> getSelectedServiceAmenitiesUnder(Resource resource) {
		String methodName = "getSelectedServiceAmenitiesUnder";
		LOG.trace("Method Entry: " + methodName);
		List<ServiceAmenity> amenities = new ArrayList<>();
		try {
			LOG.debug("Attempting to fetch amenities under: " + resource);

			Resource amenitiesResource = resource.getChild(CrxNodeNameConstants.SERVICE_AMENITIES);
			if (amenitiesResource != null) {
				ValueMap valueMap = amenitiesResource.adaptTo(ValueMap.class);
				LOG.debug("Adapted service amenities resource to value map: " + valueMap);
				Set<String> keys = valueMap.keySet();
				for (String key : keys) {
					if (key.equalsIgnoreCase(JcrConstants.JCR_PRIMARYTYPE)) {
						continue;
					}
					String[] amenitiesPathArray = valueMap.get(key, String[].class);
					int numOfAmenities = amenitiesPathArray.length;
					LOG.debug("Number of amenities under key: " + key + " are: " + numOfAmenities);
					ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
					for (int i = 0; i < numOfAmenities; i++) {
						String amenityResourcePath = amenitiesPathArray[i];
						LOG.debug("Attempting to fetch service amenity resource at: " + amenityResourcePath);
						Resource serviceAmenityResource = resourceResolver.getResource(amenityResourcePath);
						if (serviceAmenityResource != null) {
							ServiceAmenity amenity = getAmenityAt(serviceAmenityResource);
							amenities.add(amenity);
						}
					}

				}
			}
		} catch (LoginException e) {
			LOG.error("An error occured while logging in to fetch service amenity details.", e);
		}

		LOG.debug("Returning amenities under: " + resource + " as: " + amenities);
		LOG.trace("Method Exit: " + methodName);
		return amenities;
	}

	private ServiceAmenity getAmenityAt(Resource serviceAmenityResource) {
		String methodName = "getAmenityAt";
		LOG.trace("Method Entry: " + methodName);
		ValueMap valueMap = serviceAmenityResource.adaptTo(ValueMap.class);
		LOG.debug("Adapted service amenity resource to value map as: " + valueMap);
		String name = valueMap.get(CrxPropertyKeyConstants.SERVICE_AMENITY_NAME, String.class);
		String group = valueMap.get(CrxPropertyKeyConstants.SERVICE_AMENITY_GROUP, String.class);
		String image = valueMap.get(CrxPropertyKeyConstants.SERVICE_AMENITY_IMAGE, String.class);
		String description = valueMap.get(CrxPropertyKeyConstants.SERVICE_AMENITY_DESCRIPTION, String.class);
		String path = serviceAmenityResource.getPath();
		ServiceAmenity amenity = new ServiceAmenity(name, image, description, group, path);

		LOG.debug("Returning amenity as: " + amenity + " for the resource: " + serviceAmenityResource);

		LOG.trace("Method Exit: " + methodName);
		return amenity;
	}

	private boolean isRatePlanCodeMapped(Resource child, String ratePlanCode) {
		String methodName = "isRatePlanCodeMapped";
		LOG.trace("Method Entry: " + methodName);
		boolean ratePlanCodeMapped = false;

		ValueMap valueMap = child.adaptTo(ValueMap.class);
		if (valueMap.containsKey(CrxPropertyKeyConstants.RATE_PLAN_CODES)) {
			String[] ratePlanCodes = valueMap.get(CrxPropertyKeyConstants.RATE_PLAN_CODES, String[].class);
			ratePlanCodeMapped = Arrays.asList(ratePlanCodes).contains(ratePlanCode);
		}

		LOG.trace("Method Exit: " + methodName);
		return ratePlanCodeMapped;
	}
}
