package com.ihcl.core.services.config;

public interface DonationConfigurationService {
	
	String publicKey();

	String privateKey();

}
