package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Flush Rate Cache Scheduler Configuration", description = "Configure the rate cache clear on dispatcher scheduler here")
public @interface RateCacheSchedulerConfig {

	@AttributeDefinition(name = "Scheduler Name", description = "Name for the scheduler")
	String schedulerName() default "Rate Cache Flush Scheduler";

	@AttributeDefinition(name = "Enabled", description = "Enable Scheduler")
	boolean serviceEnabled() default false;

	@AttributeDefinition(name = "Expression", description = "Cron-job expression. Default: run every 3 hours.")
	String schedulerExpression() default "0 0 0/3 ? * * *";

}
