package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Booking Service Configuration",
        description = "Service Configuration")
public @interface ServiceConfiguration {

    @AttributeDefinition(name = "Online Payment Merchant ID",
            description = "Enter the Online Payment Merchant ID",
            type = AttributeType.STRING)
    String paymentMerchantID() default "4";

    @AttributeDefinition(name = "CCAvenue URL",
            description = "Enter the CCAvenue URL",
            type = AttributeType.STRING)
    String ccAvenueURL() default "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction";

    @AttributeDefinition(name = "CCAvenue Access Key",
            description = "Enter the CCAvenue Access Key",
            type = AttributeType.STRING)
    String ccAvenueAccessKey() default "AVQF01FG85AS47FQSA";

    @AttributeDefinition(name = "CCAvenue Working Key",
            description = "Enter the CCAvenue Working Key ",
            type = AttributeType.STRING)
    String ccAvenueWorkingKey() default "E0AF518195DD811AE2526EFF2571F380";

    @AttributeDefinition(name = "CCAvenue Re-direct URL",
            description = "Enter the CCAvenue Re-direct URL ",
            type = AttributeType.STRING)
    String ccAvenueReDirectURL() default "http://localhost:4502/bin/postPaymentServlet";

    @AttributeDefinition(name = "Booking Cancel/Failure URL",
            description = "Enter Booking Cancel/Failure URL ",
            type = AttributeType.STRING)
    String bookingFailedURL() default "http://localhost:4502/content/tajhotels/en-in/booking-cart.html";

    @AttributeDefinition(name = "Booking Confirmation URL",
            description = "Enter Booking Confirmation Page URL",
            type = AttributeType.STRING)
    String bookingConfirmationURL() default "http://localhost:4502/content/tajhotels/en-in/booking-checkout/booking-confirmation.html";
    
    @AttributeDefinition(name = "Epicure Payment Gateway Redirect Url",
            description = "Redirect after payment gateway call",
            type = AttributeType.STRING)
    String epicurePaymentRedirectUrl() default "http://localhost:4502/bin/addon/enroll";

    @AttributeDefinition(name = "Epicure Enrollment Redirect Url",
            description = "Redirect after invoking epicure enrollment service",
            type = AttributeType.STRING)
    String epicureEnrollRedirectUrl() default "";

    @AttributeDefinition(name = "Epicure Enrollment Purchase amount",
            description = "eg 15930.0",
            type = AttributeType.STRING)
    String epicureEnrollAmount() default "15930.0";

    @AttributeDefinition(name = "purchase Payment Gateway Redirect Url",
            description = "Redirect after payment gateway call",
            type = AttributeType.STRING)
    String purchasePaymentRedirectUrl() default "http://localhost:4502/bin/purchase/redirect";

    @AttributeDefinition(name = "purchase Payment Gateway Success Url",
            description = "purchase Payment Gateway Success Url",
            type = AttributeType.STRING)
    String getPurchasePaymentSuccess() default "http://localhost:4502/content/taj-inner-circle/purchase-success";

    @AttributeDefinition(name = "Online Purchase Payment Merchant ID",
            description = "Enter the Online Purchase Payment Merchant ID",
            type = AttributeType.STRING)
    String getPurchaseMerchantId() default "4";


    @AttributeDefinition(name = "Purchase CCAvenue Access Key",
            description = "Enter the Purchase CCAvenue Access Key",
            type = AttributeType.STRING)
    String getPurchaseCCAvenueAccessKey() default "AVQF01FG85AS47FQSA";

    @AttributeDefinition(name = "Purchase CCAvenue Working Key",
            description = "Enter the Purchase CCAvenue Working Key ",
            type = AttributeType.STRING)
    String getPurchaseCCAvenueWorkingKey() default "E0AF518195DD811AE2526EFF2571F380";

    @AttributeDefinition(name = "Campaign Epicure Enrollment Purchase amount",
            description = "eg 9500",
            type = AttributeType.STRING)
    String epicureCampaignEnrollAmount() default "9500";

}
