package com.ihcl.core.services.config;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(immediate = true, service = DonationConfigurationService.class)
@Designate(ocd = DonationServiceConfiguration.class)

public class DonationConfigurationServiceImpl implements DonationConfigurationService {

	private DonationServiceConfiguration config;
	private static final Logger LOG = LoggerFactory.getLogger(DonationConfigurationServiceImpl.class);

	@Activate
	public void activate(DonationServiceConfiguration config) {

		String methodName = "activate";
		LOG.trace("Method Entry ::: " + methodName);

		this.config = config;

		LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.ConfigurationServiceImpl ");

		LOG.trace("Method Exit ::: " + methodName);
	}

	@Override
	public String publicKey() {
		return config.publicKey();
	}

	@Override
	public String privateKey() {
		return config.privateKey();
	}


}
