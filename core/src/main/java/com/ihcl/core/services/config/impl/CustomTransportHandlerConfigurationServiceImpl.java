package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.CustomTransportHandlerConfiguration;
import com.ihcl.core.services.config.CustomTransportHandlerConfigurationService;


@Component(immediate = true, service = CustomTransportHandlerConfigurationService.class)
@Designate(ocd = CustomTransportHandlerConfiguration.class)
public class CustomTransportHandlerConfigurationServiceImpl implements CustomTransportHandlerConfigurationService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomTransportHandlerConfigurationServiceImpl.class);
	
	private CustomTransportHandlerConfiguration config;
	
	@Reference
	private SlingSettingsService settings;
	
	@Activate
	public void activate(CustomTransportHandlerConfiguration config) {
		String methodName="CustomTransportHandlerConfigurationServiceImpl.activate";
		LOG.trace("Method Entry : "+ methodName);
		LOG.trace("present ENV : " + settings.getRunModes());
		this.config = config;
		LOG.trace("Method Exit : "+ methodName);
	}
	
	@Override
	public String getCusotmProtocol() {
		return config.customProtocol();
	}

}
