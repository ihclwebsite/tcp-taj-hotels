package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.PointRedemptionConfiguration;
import com.ihcl.core.services.config.PointRedemptionConfigurationService;

@Component(immediate = true,
		service = PointRedemptionConfigurationService.class)
@Designate(ocd = PointRedemptionConfiguration.class)
public class PointRedemptionConfigurationServiceImpl implements PointRedemptionConfigurationService {
	
	private static final Logger LOG = LoggerFactory.getLogger(PointRedemptionConfigurationServiceImpl.class);
	
	private PointRedemptionConfiguration messageConfig;

    @Reference
    private SlingSettingsService settings;


    @Activate
    public void activate(PointRedemptionConfiguration config) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> " + settings.getRunModes());
        this.messageConfig = config;

        LOG.trace("Method Exit ::: activate()");
    }

	@Override
	public String getmessageForRoomAndSuitesPointRedemption() {
		
		return messageConfig.messageForRoomAndSuitesPointRedemption();
	}

}
