package com.ihcl.core.services.config;

public interface RsaConfigurationService {

	String getPublicKey();

	String getPrivateKey();

	
}
