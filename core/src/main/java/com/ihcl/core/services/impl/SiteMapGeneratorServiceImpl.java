package com.ihcl.core.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.jsoup.Jsoup;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.dam.api.Rendition;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.ihcl.core.models.sitemap.SiteMap;
import com.ihcl.core.services.SiteMapGeneratorService;

@Component(service = SiteMapGeneratorService.class,
        immediate = true)
public class SiteMapGeneratorServiceImpl implements SiteMapGeneratorService {

    private static final Logger logger = LoggerFactory.getLogger(SiteMapGeneratorServiceImpl.class);

    // private static final String BASE_PATH_FOR_SITEMAP = "/content/tajhotels/en-in/our-hotels";

	private static final String GENERIC_PATH = "/content/tajhotels/en-in";

	//private static final String GENERIC_PATH = "/content";

    // private static final String GENERIC_PATH = "/content";

    private static final String SITEMAP_INDEX_PATH = "/content/dam/tajhotels/sitemaps/sitemap-index.xml";

    private static final String ALL_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/sitemap.xml";

    private static final String GENERIC_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/generic-sitemap.xml";

    private static final String GENERIC_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/image-generic-sitemap.xml";

    private static final String ALL_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/image-sitemap.xml";

    private static final String PATH_FOR_BRANDS = "/etc/tags/taj/hotels/brands";

    private static final String BASE_PATH_FOR_HOLIDAY_SITEMAP = "/content/tajhotels/en-in/taj-holidays";

    private static final String HOLIDAY_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/holidays-sitemap.xml";

    private static final String HOLIDAY_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/holiday-image-sitemap.xml";

    private static final String SELEQTIONS_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/seleqtions-sitemap.xml";

    private static final String SELEQTIONS_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/seleqtions-image-sitemap.xml";

    private static final String VIVANTA_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/vivanta-sitemap.xml";

    private static final String VIVANTA_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/vivanta-image-sitemap.xml";

    private static final String BASE_PATH_FOR_TIC = "/content/taj-inner-circle";

    private static final String TIC_SITEMAP_PATH = "/content/dam/tajhotels/sitemaps/tic-sitemap.xml";

    private static final String TIC_SITEMAP_IMAGE_PATH = "/content/dam/tajhotels/sitemaps/tic-image-sitemap.xml";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    QueryBuilder builder;

    AssetManager assetMgr;

    Resource resource;

    ResourceResolver resourceResolver;

    InputStream inputStream;

    ByteArrayOutputStream outputStream;

    DocumentBuilder docBuilder;

    DocumentBuilderFactory docFactory;

    Document document;

    ArrayList<SiteMap> siteMapObject;

    ArrayList<SiteMap> indexSiteMapObject;

    List<Hit> allHits;

    List<Hit> holidayHits;

    List<Hit> genericHits;

    List<Hit> brandsHits;

    List<Hit> hits;

    String hostString;

    Map<String, String> brandToSiteMapPath;

    Map<String, String> siteMapToImagePathMap;

    @Override
    public Boolean generateSiteMap(String hostString, String rootPath) {
        try {
            logger.info("Starting to generate site map");
            this.hostString = hostString;
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();

            readXmlFromDam(ALL_SITEMAP_PATH, resourceResolver);
            // read all the pages using QueryBuilder
            getAllPages(rootPath + "/en-in/our-hotels");

            // Now we have got the updated sitemap from DAM, Lets add or delete if anything
            // has changed
            updateSiteMapEntries(allHits);

            updateExistingSiteMap(ALL_SITEMAP_PATH);

            return true;

        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }

        return false;
    }

    private void updateExistingSiteMap(String path) throws Exception {
        logger.trace("inside updateExistingSiteMap");
        // Lets create a new sitemap.XML file with freshly update data
        document = docBuilder.newDocument();
        Element urlset = document.createElement("urlset");

        Attr attr = document.createAttribute("xmlns");
        attr.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");
        urlset.setAttributeNode(attr);

        document.appendChild(urlset);

        for (SiteMap siteMapEntry : siteMapObject) {
            logger.trace("inside updateExistingSiteMap loc " + siteMapEntry.getLoc());
            Element url = document.createElement("url");
            urlset.appendChild(url);

            // loc elements
            Element loc = document.createElement("loc");
            loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
            url.appendChild(loc);

            // lastmod elements
            Element lastmod = document.createElement("lastmod");
            lastmod.appendChild(document.createTextNode(siteMapEntry.getLastmod()));
            url.appendChild(lastmod);

            // changefreq elements
            Element changefreq = document.createElement("changefreq");
            changefreq.appendChild(document.createTextNode(siteMapEntry.getChangefreq()));
            url.appendChild(changefreq);

            // priority elements
            Element priority = document.createElement("priority");
            priority.appendChild(document.createTextNode(siteMapEntry.getPriority()));
            url.appendChild(priority);
        }

        // write the content into new generated xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(document);
        Result outputTarget = new StreamResult(outputStream);

        transformer.transform(xmlSource, outputTarget);
        inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        writeXmlToDam(inputStream, resourceResolver, path);

    }

    private void getAllPages(String path) {
        // Object Creation for QueryBuilder
        allHits = new ArrayList<Hit>();
        /*
         * Map<String, String> queryMap = new TreeMap<>(); queryMap.put("path", path); queryMap.put("type", "cq:Page");
         * // queryMap.put("property", "jcr:content/cq:lastReplicationAction"); // queryMap.put("property.value",
         * "Activate"); queryMap.put("p.hits", "all"); queryMap.put("p.limit", "-1");
         *
         * Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
         */
        getPropertyLevelPages(null, "master");
        getJivaSpaListPages(null, "master");
        getDestinationPages(path);

    }

    private void getAllPages(String path, String brand) throws RepositoryException {
        // Object Creation for QueryBuilder
        allHits = new ArrayList<Hit>();
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/brand");
        queryMap.put("property.value", brand);
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        List<String> paths = new ArrayList<String>();
        for (Hit hit : query.getResult().getHits()) {
            paths.add(hit.getPath().trim());
            allHits.add(hit);
        }
        queryMap.clear();
        getAllBrandPages(paths);

    }

    private void updateSiteMapEntries(List<Hit> allHits) throws ParseException {
        logger.trace("inside updatesitemap entries");
        // Lets add new Pages if any to the SiteMap
        siteMapObject = new ArrayList<>();
        for (Hit hit : allHits) {
            try {
                String pagePath = hit.getPath();
                ValueMap pageProperties = hit.getProperties();

                boolean newPageFlag = true;
                /*
                 * for (SiteMap siteMapEntry : siteMapObject) { if
                 * (siteMapEntry.getLoc().equalsIgnoreCase(resourceResolver.map(pagePath + ".html"))) { logger.trace(
                 * "inside updatesitemap entries old page " + resourceResolver.map(pagePath + ".html")); // converting
                 * the date from GregorianCalendar to String Calendar cal = (Calendar)
                 * pageProperties.get("cq:lastModified"); DateFormat date = new SimpleDateFormat("yyyy-MM-dd"); if (cal
                 * != null) { siteMapEntry.setLastmod(date.format(cal.getTime())); } else {
                 * siteMapEntry.setLastmod(" "); }
                 * siteMapEntry.setChangefreq(String.valueOf(pageProperties.get("changefreq")));
                 * siteMapEntry.setPriority(String.valueOf(pageProperties.get("priority")));
                 * siteMapEntry.setIsPagePresent(true); newPageFlag = false; break; } }
                 */
                if (newPageFlag) {
                    logger.trace("inside updatesitemap entries new page " + pagePath);

                    SiteMap siteMapEntry = new SiteMap();
                    siteMapEntry.setLoc(resourceResolver.map(pagePath + ".html"));
                    /*
                     * logger.info("hoststring = " + hostString + "  pagePath  =" + pagePath + " after mapping =" +
                     * resourceResolver.map(pagePath + ".html"));
                     */
                    // converting the date from GregorianCalendar to String
                    if (pageProperties != null) {
                        if (pageProperties.containsKey("jcr:created")) {
                            Calendar cal = (Calendar) pageProperties.get("jcr:created");
                            DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                            if (cal != null) {
                                siteMapEntry.setLastmod(date.format(cal.getTime()));
                            } else {
                                siteMapEntry.setLastmod(" ");
                                logger.info("Page name=" + hit.getPath());
                            }
                        } else {
                            siteMapEntry.setLastmod(" ");
                        }

                        if (pageProperties.containsKey("changefreq")) {
                            siteMapEntry.setChangefreq(String.valueOf(pageProperties.get("changefreq")));
                        } else {
                            siteMapEntry.setChangefreq("weekly");
                        }
                        if (pageProperties.containsKey("priority")) {
                            siteMapEntry.setPriority(String.valueOf(pageProperties.get("priority")));
                        } else {
                            siteMapEntry.setPriority("0.9");
                        }
                    } else {
                        siteMapEntry.setLastmod(" ");
                        siteMapEntry.setChangefreq("weekly");
                        siteMapEntry.setPriority("0.9");
                    }

                    siteMapEntry.setIsPagePresent(true);
                    siteMapObject.add(siteMapEntry);
                }

            } catch (RepositoryException e) {
                logger.error("Error/Exception is thrown while reading Result Set" + e.toString(), e);
            }
        }

        // Lets Delete any old pages which are not there anymore(not-activated).
        for (int i = 0; i < siteMapObject.size(); i++) {
            if (!siteMapObject.get(i).getIsPagePresent()) {
                logger.info("url removed from existing sitemap is " + siteMapObject.get(i).getLoc());
                siteMapObject.remove(i);
                logger.info("after removing url from existing sitemap is ");
            }
        }
    }

    private void getHolidayPages(String path) throws RepositoryException {
        // Object Creation for QueryBuilder
        holidayHits = new ArrayList<Hit>();
        Map<String, Object> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        queryMap.put("property", "jcr:content/sling:resourceType");
        queryMap.put("property.value", "tajhotels/components/structure/tajholidays-destination-hotel-page");
        queryMap.put("property.operation", "unequals");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        List<String> allPaths = new ArrayList<String>();
        logger.trace("Hits size : " + query.getResult().getHits().size());
        for (Hit hit : query.getResult().getHits()) {
            if (!(hit.getPath().contains("itineraries"))) {
                allPaths.add(hit.getPath().trim());
                holidayHits.add(hit);
            }
        }
    }

    @Override
    public Boolean generateSectionalSiteMap(String hostString, List<String> brandNames, String rootPath) {
        try {
            logger.info("Starting to generate sectional site map");

            this.hostString = hostString;
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();

            if (brandNames.contains("holidays")) {
                logger.trace("inside for loop generatesectional sitemap");
                getHolidayPages(BASE_PATH_FOR_HOLIDAY_SITEMAP);
                logger.trace("after get holiday pages");
                updateSiteMapEntries(holidayHits);
                updateExistingSiteMap(HOLIDAY_SITEMAP_PATH);

            } else if (rootPath.contains("seleqtions") && brandNames.contains("seleqtions")) {
                logger.trace("inside for loop generatesectional sitemap");
                getBrandPages(rootPath);
                logger.trace("after get seleqtion brand pages");
                updateSiteMapEntries(brandsHits);
                updateExistingSiteMap(SELEQTIONS_SITEMAP_PATH);

            } else if (rootPath.contains("vivanta") && brandNames.contains("vivanta")) {
                logger.trace("inside for loop generatesectional sitemap");
                getBrandPages(rootPath);
                logger.trace("after get vivanta brand pages");
                updateSiteMapEntries(brandsHits);
                updateExistingSiteMap(VIVANTA_SITEMAP_PATH);
            } else if (brandNames.contains("tic")) {
                logger.trace("inside tic sitemap generator");
                String ticPath = BASE_PATH_FOR_TIC;
                getPages(ticPath);
                updateSiteMapEntries(hits);
                updateExistingSiteMap(TIC_SITEMAP_PATH);
            }

            else {

                // getBrandNames();
                mappingBrandPaths(brandNames);

                for (Map.Entry<String, String> entry : brandToSiteMapPath.entrySet()) {
                    logger.trace("inside for loop generatesectional sitemap");
                    // readXmlFromDam(entry.getValue(), resourceResolver);
                    // read all the pages for taj hotels using QueryBuilder
                    getAllPages(rootPath + "/en-in/our-hotels", entry.getKey());
                    logger.trace("after get allpages");
                    // Now we have got the updated sitemap from DAM, Lets add or delete if anything
                    // // has changed
                    updateSiteMapEntries(allHits);
                    updateExistingSiteMap(entry.getValue());
                    // updateIndexSitemap(entry.getValue());
                }
            }
            return true;
        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }

        return false;
    }

    private void readXmlFromDam(String path, ResourceResolver resourceResolver) throws Exception {
        logger.trace("inside readXmlFromDam");
        document = docBuilder.newDocument();
        // checking if sitemap.xml is present at root node.
        resource = resourceResolver.getResource(path);
        siteMapObject = new ArrayList<>();

        if (resource != null) {
            boolean isAssest = DamUtil.isAsset(resource);
            if (isAssest) {
                Asset asset = resource.adaptTo(Asset.class);
                List<Rendition> renditions = asset.getRenditions();
                for (Rendition rendition : renditions) {
                    String name = rendition.getName();
                    if (name.equals("original")) {
                        inputStream = rendition.getStream();
                        break;
                    }
                }
            }

            document = docBuilder.parse(inputStream);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("url");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node urlNode = nodeList.item(i);
                if (urlNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) urlNode;
                    element.getElementsByTagName("loc").item(0).getTextContent();

                    SiteMap siteMapEntry = new SiteMap();
                    siteMapEntry.setLoc(element.getElementsByTagName("loc").item(0).getTextContent());
                    siteMapEntry.setLastmod(element.getElementsByTagName("lastmod").item(0).getTextContent());
                    siteMapEntry.setChangefreq(element.getElementsByTagName("changefreq").item(0).getTextContent());
                    siteMapEntry.setPriority(element.getElementsByTagName("priority").item(0).getTextContent());
                    logger.trace("inside readXmlFromDam location " + siteMapEntry.getLoc());
                    siteMapObject.add(siteMapEntry);
                }
            }
        }
    }

    private void writeXmlToDam(InputStream inputStream, ResourceResolver resourceResolver, String path) {
        try {
            // Use AssetManager to place the file into the AEM DAM
            logger.trace("inside writeToDam");
            assetMgr = resourceResolver.adaptTo(AssetManager.class);
            if (assetMgr != null) {
                logger.info("inisde asset mgr path of node to be removed is" + path);
                resource = resourceResolver.getResource(path);
                if (resource != null) {
                    javax.jcr.Node node = resource.adaptTo(javax.jcr.Node.class);
                    node.remove();
                    node.getSession().save();
                    logger.info("node has removed successfully");
                }
                assetMgr.createAsset(path, inputStream, "application/xml", true);
            }

        } catch (Exception e) {
            logger.error("Error/Exception while writing file to DAM" + e.toString(), e);
        }
    }

    @Override
    public Boolean generateImageSiteMap() {
        try {
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();
            readXmlFromDam(ALL_SITEMAP_PATH, resourceResolver);
            updateExistingImageSiteMap(ALL_SITEMAP_IMAGE_PATH);
            return true;

        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }

        return false;
    }

    @Override
    public Boolean generateGenericImageSiteMap() {
        try {
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();
            readXmlFromDam(GENERIC_SITEMAP_PATH, resourceResolver);
            updateExistingImageSiteMap(GENERIC_SITEMAP_IMAGE_PATH);
            return true;
        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }

        return false;
    }

    private void updateExistingImageSiteMap(String path) throws Exception {
        logger.trace("inside updateExistingImageSiteMap");
        // Lets create a new sitemap.XML file with freshly update data
        document = docBuilder.newDocument();
        Element urlset = document.createElement("urlset");

        Attr xmlns = document.createAttribute("xmlns");
        xmlns.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");

        Attr xmlnsImage = document.createAttribute("xmlns:image");
        xmlnsImage.setValue("http://www.google.com/schemas/sitemap-image/1.1");

        urlset.setAttributeNode(xmlns);
        urlset.setAttributeNode(xmlnsImage);

        document.appendChild(urlset);

        org.jsoup.nodes.Document jsoupDocument;
        org.jsoup.select.Elements jsoupElements;
        try {
            for (SiteMap siteMapEntry : siteMapObject) {
                Element url = document.createElement("url");
                urlset.appendChild(url);
                logger.trace("inside updateExistingImageSiteMap" + siteMapEntry.getLoc());
                // loc elements
                Element loc = document.createElement("loc");
                loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
                url.appendChild(loc);

                Element changeFreq = document.createElement("changefreq");
                changeFreq.appendChild(document.createTextNode(siteMapEntry.getChangefreq()));
                url.appendChild(changeFreq);

                Element priority = document.createElement("priority");
                priority.appendChild(document.createTextNode(siteMapEntry.getPriority()));
                url.appendChild(priority);
                try {
                    logger.info("URL of page =" + siteMapEntry.getLoc());
                    jsoupDocument = Jsoup.connect(siteMapEntry.getLoc()).get();
                    jsoupElements = jsoupDocument.select("img");
                    for (org.jsoup.nodes.Element element : jsoupElements) {
                        if (element.hasAttr("src")) {
                            if (element.absUrl("src") != null && !element.absUrl("src").contains("style-icons")) {
                                Element parentImageTag = document.createElement("image:image");
                                url.appendChild(parentImageTag);
                                Element imageLoc = document.createElement("image:loc");
                                imageLoc.appendChild(document.createTextNode(element.absUrl("src")));
                                parentImageTag.appendChild(imageLoc);
                                logger.trace("inside image of cureent page and loc is" + element.absUrl("src"));
                                if (element.hasAttr("title")) {
                                    Element imageTitle = document.createElement("image:title");
                                    imageTitle.appendChild(document.createTextNode(element.attr("title")));
                                    parentImageTag.appendChild(imageTitle);
                                }
                                if (element.hasAttr("alt")) {
                                    Element imageCaption = document.createElement("image:caption");
                                    imageCaption.appendChild(document.createTextNode(element.attr("alt")));
                                    parentImageTag.appendChild(imageCaption);
                                }
                            }

                        }

                    }
                } catch (Exception e) {
                    logger.error("Error/Exception is thrown while crawling the page" + e.toString(), e);
                }
            }

            // write the content into new generated xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            outputStream = new ByteArrayOutputStream();
            Source xmlSource = new DOMSource(document);
            Result outputTarget = new StreamResult(outputStream);

            transformer.transform(xmlSource, outputTarget);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());

            writeXmlToDam(inputStream, resourceResolver, path);

        } catch (Exception e) {
            logger.error("Error/Exception is thrown while crawling the page" + e.toString(), e);
        }

    }

    @Override
    public Boolean generateSectionalImageSiteMap(List<String> brandNames, String rootPath) {
        try {
            // Inject a ResourceResolver
            logger.trace("inside generateSectionalImageSiteMap");
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();

            if (brandNames.contains("holidays")) {
                logger.trace("inside generateSectionalImageSiteMap inside forloop");
                readXmlFromDam(HOLIDAY_SITEMAP_PATH, resourceResolver);
                updateExistingImageSiteMap(HOLIDAY_SITEMAP_IMAGE_PATH);

            } else if (rootPath.contains("seleqtions") && brandNames.contains("seleqtions")) {
                logger.trace("inside generateSectionalImageSiteMap inside forloop");
                readXmlFromDam(SELEQTIONS_SITEMAP_PATH, resourceResolver);
                updateExistingImageSiteMap(SELEQTIONS_SITEMAP_IMAGE_PATH);

            } else if (rootPath.contains("vivanta") && brandNames.contains("vivanta")) {
                logger.trace("inside generateSectionalImageSiteMap inside forloop");
                readXmlFromDam(VIVANTA_SITEMAP_PATH, resourceResolver);
                updateExistingImageSiteMap(VIVANTA_SITEMAP_IMAGE_PATH);
            } else if (brandNames.contains("tic")) {
                logger.trace("inside tic image sitemap ");
                readXmlFromDam(TIC_SITEMAP_PATH, resourceResolver);
                updateExistingImageSiteMap(TIC_SITEMAP_IMAGE_PATH);
            } else {

                mappingBrandPaths(brandNames);
                for (Map.Entry<String, String> entry : siteMapToImagePathMap.entrySet()) {
                    logger.trace("inside generateSectionalImageSiteMap inside forloop");
                    readXmlFromDam(entry.getKey(), resourceResolver);
                    updateExistingImageSiteMap(entry.getValue());
                }
            }
            return true;

        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }
        return false;
    }

    public void printMap(Map<String, String> maps) {
        logger.info("Maps items are as below");
        for (Entry<String, String> item : maps.entrySet()) {
            logger.info(item.getKey() + " : " + item.getValue());
        }
    }

    public void printList(List<String> list) {
        for (String item : list) {
            logger.info(item + " :");
        }
    }

    public void getBrandNames() throws RepositoryException {
        Map<String, String> queryMap = new TreeMap<>();
        brandToSiteMapPath = new HashMap<String, String>();
        siteMapToImagePathMap = new HashMap<String, String>();
        queryMap.put("path", PATH_FOR_BRANDS);
        queryMap.put("type", "cq:tag");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");

        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        String brandName;
        for (Hit hit : query.getResult().getHits()) {
            brandName = hit.getNode().getName();
            /*
             * logger.info("brand node name=" + hit.getNode().getName()); logger.info("brand path=" + hit.getPath());
             * logger.info("brand title=" + hit.getTitle());
             */
            brandToSiteMapPath.put("taj:hotels/brands/" + brandName,
                    "/content/dam/tajhotels/sitemap_" + brandName + ".xml");
            siteMapToImagePathMap.put("/content/dam/tajhotels/sitemap_" + brandName + ".xml",
                    "/content/dam/tajhotels/image-sitemap_" + brandName + ".xml");
        }
    }

    public void mappingBrandPaths(List<String> brandNames) {
        logger.trace("inside mappingBrandPaths ");
        brandToSiteMapPath = new HashMap<String, String>();
        siteMapToImagePathMap = new HashMap<String, String>();
        if (brandNames != null) {
            brandNames.remove("master");
        }
        for (String brandName : brandNames) {
            logger.trace("inside mappingBrandPaths " + brandName);
            brandToSiteMapPath.put("taj:hotels/brands/" + brandName,
                    "/content/dam/tajhotels/sitemaps/sitemap_" + brandName + ".xml");
            siteMapToImagePathMap.put("/content/dam/tajhotels/sitemaps/sitemap_" + brandName + ".xml",
                    "/content/dam/tajhotels/sitemaps/image-sitemap_" + brandName + ".xml");
        }
    }

    private void getDestinationPages(String path) {
        // Object Creation for QueryBuilder
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/sling:resourceType");
        queryMap.put("property.1_value", "tajhotels/components/structure/destination-landing-page");
        queryMap.put("property.2_value", "tajhotels/components/structure/about-destination-page");
        queryMap.put("property.3_value", "tajhotels/components/structure/dining-list-destination-page");
        queryMap.put("property.4_value", "tajhotels/components/structure/all-experience-destination-page");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        genericHits.addAll(query.getResult().getHits());

    }

    private void getJivaSpaListPages(List<String> paths, String type) {
        // Object Creation for QueryBuilder
        Map<String, String> queryMap = new TreeMap<>();
        if (type != null && type.equalsIgnoreCase("brand")) {
            queryMap.put("group.p.or", "true");
            int i = 1;
            for (String pathElement : paths) {
                queryMap.put("group." + i + "_path", pathElement);
                // queryMap.put("group." + i + "_path.self", "true");
                i++;
            }
        } else {
            queryMap.put("path", "/content/tajhotels/en-in/our-hotels");
        }

        queryMap.put("type", "cq:Page");
        queryMap.put("1_property", "jcr:content/sling:resourceType");
        queryMap.put("1_property.value", "tajhotels/components/structure/jiva-spa-list-page");
        queryMap.put("2_property", "jcr:content/cq:template");
        queryMap.put("2_property.value", "/conf/tajhotels/settings/wcm/templates/jiva-spa-list");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        allHits.addAll(query.getResult().getHits());

    }

    private void getPropertyLevelPages(List<String> paths, String type) {
        // Object Creation for QueryBuilder
        Map<String, String> queryMap = new TreeMap<>();
        if (type != null && type.equalsIgnoreCase("brand")) {
            queryMap.put("group.p.or", "true");
            int i = 1;
            for (String pathElement : paths) {
                queryMap.put("group." + i + "_path", pathElement);
                // queryMap.put("group." + i + "_path.self", "true");
                i++;
            }
        } else {
            queryMap.put("path", "/content/tajhotels/en-in/our-hotels");
        }

        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/sling:resourceType");
        queryMap.put("property.1_value", "tajhotels/components/structure/dining-details-page");
        queryMap.put("property.2_value", "tajhotels/components/structure/dining-list-page");
        queryMap.put("property.3_value", "tajhotels/components/structure/all-offers-page");
        queryMap.put("property.4_value", "tajhotels/components/structure/offer-details-page");
        queryMap.put("property.5_value", "tajhotels/components/structure/experiences-list-page");
        queryMap.put("property.6_value", "tajhotels/components/structure/local-area-list-page");
        queryMap.put("property.7_value", "tajhotels/components/structure/tajhotels-meetings-page");
        queryMap.put("property.8_value", "tajhotels/components/structure/inspiration-gallery-page");
        queryMap.put("property.9_value", "tajhotels/components/structure/fitness-page");
        queryMap.put("property.10_value", "tajhotels/components/structure/tajhotels-rooms-listing-page");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        allHits.addAll(query.getResult().getHits());
        query.getResult().getHits().size();

    }

    private void getAllBrandPages(List<String> paths) {
        // Object Creation for QueryBuilder
        Map<String, Object> queryMap = new TreeMap<>();
        queryMap.put("group.p.or", "true");
        int i = 1;
        for (String pathElement : paths) {
            queryMap.put("group." + i + "_path", pathElement);
            // queryMap.put("group." + i + "_path.self", "true");
            i++;
        }
        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/isCrawlabel");
        queryMap.put("property.value", "true");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        allHits.addAll(query.getResult().getHits());
    }

    @Override
    public Boolean generateGenericSiteMap(String hostString) {
        try {
            genericHits = new ArrayList<Hit>();
            logger.info("Starting to generate Generic site map");
            this.hostString = hostString;
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();

            // readXmlFromDam(GENERIC_SITEMAP_PATH, resourceResolver);
            // read all the pages using QueryBuilder
            getGenericPages();

            // Now we have got the updated sitemap from DAM, Lets add or delete if anything
            // has changed
            updateSiteMapEntries(genericHits);

            updateExistingSiteMap(GENERIC_SITEMAP_PATH);

            // updateIndexSitemap(GENERIC_SITEMAP_PATH);
            return true;

        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }

        return false;
    }

    private void getGenericPages() throws RepositoryException {

        List<Hit> allPages = new ArrayList<Hit>();
        List<Hit> hotelPages = new ArrayList<Hit>();
		//String genericPath = GENERIC_PATH;
		//String ourHotelsPath = GENERIC_PATH + "/our-hotels";
        // method call to get All crawlabel pages under /en-in
		allPages = getGivenPathpages(GENERIC_PATH);
        // method call to get All crawlabel pages under /en-in/our-hotels
        hotelPages = getGivenPathpages(GENERIC_PATH + "/our-hotels");
        Boolean isPresent = false;
        for (Hit hit : allPages) {
            isPresent = false;
            for (Hit hit1 : hotelPages) {

                if (hit1.getPath().equalsIgnoreCase(hit.getPath())) {
                    isPresent = true;
                }

            }
            if (!isPresent) {
                genericHits.add(hit);
            }
            /*
             * if (hotelPages.contains(hit)) { logger.info("inside contains logic"); } else { genericHits.add(hit); }
             */
        }

        /*
         * for (Hit hit : hotelPages) {
         *
         * logger.info("inside hotel pages loop " + hit.getPath());
         *
         * }
         */
        getDestinationPages(GENERIC_PATH + "/our-hotels");

    }

    private List<Hit> getGivenPathpages(String path) {
        Map<String, Object> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        if (path.equalsIgnoreCase(GENERIC_PATH)) {
            queryMap.put("path.self", "true");
        }
        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/isCrawlabel");
        queryMap.put("property.value", "true");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        return query.getResult().getHits();
    }

    private void updateIndexSitemap(String path) {
        try {
            readExistingIndexSitemap(SITEMAP_INDEX_PATH, resourceResolver);
            modifyIndexSitemap(path);
        } catch (Exception ex) {
            // TODO Auto-generated catch block
            logger.error("Error/Exception is thrown inside updateIndexSitemap" + ex.toString(), ex);
        }

    }

    private void readExistingIndexSitemap(String path, ResourceResolver resourceResolver) throws Exception {
        logger.trace("inside readExistingIndexSitemap");
        document = docBuilder.newDocument();
        // checking if sitemap.xml is present at root node.
        resource = resourceResolver.getResource(path);
        indexSiteMapObject = new ArrayList<>();

        if (resource != null) {
            boolean isAssest = DamUtil.isAsset(resource);
            if (isAssest) {
                Asset asset = resource.adaptTo(Asset.class);
                List<Rendition> renditions = asset.getRenditions();
                for (Rendition rendition : renditions) {
                    String name = rendition.getName();
                    if (name.equals("original")) {
                        inputStream = rendition.getStream();
                        break;
                    }
                }
            }

            document = docBuilder.parse(inputStream);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("sitemap");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node urlNode = nodeList.item(i);
                if (urlNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) urlNode;
                    element.getElementsByTagName("loc").item(0).getTextContent();

                    SiteMap siteMapEntry = new SiteMap();
                    siteMapEntry.setLoc(element.getElementsByTagName("loc").item(0).getTextContent());
                    siteMapEntry.setLastmod(element.getElementsByTagName("lastmod").item(0).getTextContent());
                    logger.trace("inside readXmlFromDam location " + siteMapEntry.getLoc());
                    indexSiteMapObject.add(siteMapEntry);
                }
            }
        }
    }

    private void modifyIndexSitemap(String path) throws Exception {
        logger.trace("inside modifyIndexSitemap");
        // Lets create a new sitemap.XML file with freshly update data
        document = docBuilder.newDocument();
        Element urlset = document.createElement("sitemapindex");

        Attr attr = document.createAttribute("xmlns");
        attr.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");
        urlset.setAttributeNode(attr);

        document.appendChild(urlset);

        for (SiteMap siteMapEntry : indexSiteMapObject) {
            logger.trace("inside modifyIndexSitemap loc " + siteMapEntry.getLoc());
            Element url = document.createElement("sitemap");
            urlset.appendChild(url);

            logger.trace("inside modifyIndexSitemap loc after map " + resourceResolver.map(path));
            if (siteMapEntry.getLoc().equalsIgnoreCase(resourceResolver.map(path))) {
                logger.trace("inside if stmt  modifyIndexSitemap");
                // loc elements
                Element loc = document.createElement("loc");
                loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
                url.appendChild(loc);

                // lastmod elements
                Date today = new Date();
                DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                Element lastmod = document.createElement("lastmod");
                lastmod.appendChild(document.createTextNode(date.format(today)));
                url.appendChild(lastmod);

            } else {
                logger.trace("inside else stmt  modifyIndexSitemap");
                // loc elements
                Element loc = document.createElement("loc");
                loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
                url.appendChild(loc);

                // lastmod elements
                Element lastmod = document.createElement("lastmod");
                lastmod.appendChild(document.createTextNode(siteMapEntry.getLastmod()));
                url.appendChild(lastmod);
            }

        }

        // write the content into new generated xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(document);
        Result outputTarget = new StreamResult(outputStream);

        transformer.transform(xmlSource, outputTarget);
        inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        writeXmlToDam(inputStream, resourceResolver, SITEMAP_INDEX_PATH);

    }

    private void getBrandPages(String path) throws RepositoryException {
        // Object Creation for QueryBuilder
        brandsHits = new ArrayList<Hit>();
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        queryMap.put("property", "jcr:content/isCrawlabel");
        queryMap.put("property.value", "true");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        logger.trace("Hits size : " + query.getResult().getHits().size());
        brandsHits.addAll(query.getResult().getHits());

    }

    private void getPages(String path) {
        logger.trace("inside getPages() method");
        hits = new ArrayList<Hit>();
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");
        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        logger.trace("size of the result of getPages " + query.getResult().getHits().size());
        hits.addAll(query.getResult().getHits());

    }

}
