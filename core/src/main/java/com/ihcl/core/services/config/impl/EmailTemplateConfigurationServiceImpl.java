package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.EmailTemplateConfiguration;
import com.ihcl.core.services.config.EmailTemplateConfigurationService;

@Component(immediate = true,
        service = EmailTemplateConfigurationService.class)
@Designate(ocd = EmailTemplateConfiguration.class)
public class EmailTemplateConfigurationServiceImpl implements EmailTemplateConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(EmailTemplateConfigurationServiceImpl.class);

    private EmailTemplateConfiguration emailConfig;

    @Reference
    private SlingSettingsService settings;


    @Activate
    public void activate(EmailTemplateConfiguration config) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> " + settings.getRunModes());
        this.emailConfig = config;

        LOG.trace("Method Exit ::: activate()");
    }


    @Override
    public String getEmailFromAddressForNewsLetter() {
        return emailConfig.emailFromAddressForNewsLetter();
    }

    @Override
    public String getEmailSubjectForNewsLetter() {
        return emailConfig.emailSubjectForNewsLetter();
    }

    @Override
    public String getEmailBodyTemplateForNewsLetterIhcl() {
        return emailConfig.emailBodyTemplateForNewsLetterIhcl();
    }

    @Override
    public String getContactNumberForNewsLetter() {
        return emailConfig.contactNumberForNewsLetter();
    }

    @Override
    public String getUrlIhclForNewsLetter() {
        return emailConfig.urlIhclForNewsLetter();
    }

    @Override
    public String getEmailBodyTemplateForNewsLetterTajHotels() {
        return emailConfig.emailBodyTemplateForNewsLetterTajHotels();
    }

    @Override
    public String getEmailFromAddressForJivaSpa() {
        return emailConfig.emailFromAddressForJivaSpa();
    }

    @Override
    public String getEmailSubjectForJivaSpa() {
        return emailConfig.emailSubjectForJivaSpa();
    }

    @Override
    public String getEmailBodyTemplateForJivaSpa() {
        return emailConfig.emailBodyTemplateForJivaSpa();
    }

    @Override
    public String getEmailFromAddressForMeetingRequestQuote() {
        return emailConfig.emailFromAddressForMeetingRequestQuote();
    }

    @Override
    public String getEmailSubjectForMeetingRequestQuote() {
        return emailConfig.emailSubjectForMeetingRequestQuote();
    }

    @Override
    public String getEmailBodyTemplateForMeetingRequestQuote() {
        return emailConfig.emailBodyTemplateForMeetingRequestQuote();
    }

    @Override
    public String getEmailBodyTemplateForGenericRequestQuote() {
        return emailConfig.emailBodyTemplateForGenericRequestQuote();
    }

    @Override
    public String getEmailFromAddressForHolidaysEnqiry() {
        return emailConfig.emailFromAddressForHolidaysEnqiry();
    }


    @Override
    public String getEmailSubjectForHolidaysEnquiry() {
        return emailConfig.emailSubjectForHolidaysEnquiry();
    }


    @Override
    public String getEmailBodyTemplateForHolidaysEnquiry() {
        return emailConfig.emailBodyTemplateForHolidaysEnquiry();
    }


    @Override
    public String getEmailHolidaysEmail() {
        return emailConfig.emailHolidaysEmail();
    }

    @Override
    public String getContactNumberForNewsLetterTajHotels() {
        return emailConfig.contactNumberForNewsLetterTajHotels();
    }

    @Override
    public String getUrlForNewsLetterTajHotels() {
        return emailConfig.urlForNewsLetterTajHotels();
    }


    @Override
    public String getEmailSubjectForEnquiryConfirmation() {
        return emailConfig.emailSubjectForEnquiryConfirmation();
    }


    @Override
    public String getEmailBodyTemplateForEnquiryConfirmation() {
        return emailConfig.emailBodyTemplateForEnquiryConfirmation();
    }

    @Override
    public String getEmailToAddressForClaimForm() {
        return emailConfig.emailToAddressForClaimForm();
    }

    @Override
    public String getEmailSubjectForClaimForm() {
        return emailConfig.emailSubjectForClaimForm();
    }

    @Override
    public String getEmailBodyForClaimForm() {
        return emailConfig.emailBodyTemplateForClaimForm();
    }

    @Override
    public String getEmailToAddressForSafariEnquiry() {
        return emailConfig.emailToAddressForSafariEnquiry();
    }

    @Override
    public String getEmailSubjectForSafariEnquiry() {
        return emailConfig.emailSubjectForSafariEnquiry();
    }

    @Override
    public String getEmailBodyForSafariEnquiry() {
        return emailConfig.emailBodySafariEnquiry();
    }

    @Override
    public String getEmailFromAddressForTajAir() {
        return emailConfig.emailFromAddressForTajAir();
    }

    @Override
    public String getEmailBodyTemplateForTajAir() {
        return emailConfig.emailBodyTemplateForTajAir();
    }

    @Override
    public String getEmailToAddressFotTajAir() {
        return emailConfig.emailToAddressForTajAir();
    }

    @Override
    public String getEmailSubjectForTajAir() {
        return emailConfig.emailSubjectForTajAir();
    }

    @Override
    public String getEmailToAddressForTajAirContactUs() {
        return emailConfig.emailToAddressForTajAirContactUs();
    }

    @Override
    public String getEmailSubjectForTajAirContactUs() {
        return emailConfig.emailSubjectForTajAirContactUs();
    }

    @Override
    public String getEmailFromAddressForTajAirContactUs() {
        return emailConfig.emailFromAddressForTajAirContactUs();
    }

    @Override
    public String getEmailBodyTemplateForTajAirContactUs() {
        return emailConfig.emailBodyTemplateForTajAirContactUs();
    }


    @Override
    public String getEmailBodyTemplateForReservationEmailRoomsSegment() {
        return emailConfig.emailBodyTemplateForReservationEmailRoomsSegment();
    }

    @Override
    public String getEmailSubjectForReservationEmail() {
        return emailConfig.emailSubjectForReservationEmail();
    }


    @Override
    public String getEmailBodyTemplateForReservationEmail() {
        return emailConfig.emailBodyTemplateForReservationEmail();
    }

    @Override
    public String getBccAddressForNewsletterSubscription() {
        return emailConfig.bccAddressForNewsletterSubscription();
    }


}
