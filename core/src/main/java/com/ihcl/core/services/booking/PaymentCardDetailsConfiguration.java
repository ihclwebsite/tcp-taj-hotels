package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Payment details card configuration", description = "Payment details card configuration")
public @interface PaymentCardDetailsConfiguration {

    @AttributeDefinition(name = "Card Name", description = "Payment card name", type = AttributeType.STRING)
    String cardName() default "test";

    @AttributeDefinition(name = "Expiry Date", description = "CC card expiry date", type = AttributeType.STRING)
    String expityDate() default "12/25";

    @AttributeDefinition(name = "Card Number", description = "Payment card number", type = AttributeType.STRING)
    String cardNumber() default "4111111111111111";

    @AttributeDefinition(name = "Card Number", description = "Payment card number", type = AttributeType.STRING)
    String cardType() default "VI";
}
