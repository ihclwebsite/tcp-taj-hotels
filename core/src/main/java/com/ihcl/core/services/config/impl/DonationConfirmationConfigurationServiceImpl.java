/**
 * 
 */
package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.DonationConfirmationConfigurationService;
import com.ihcl.core.services.config.DonationConfirmationConfiguration;
/**
 * @author TCS
 *
 */
@Component(	immediate = true,
			service = DonationConfirmationConfigurationService.class)
@Designate(ocd = DonationConfirmationConfiguration.class)

public class DonationConfirmationConfigurationServiceImpl implements DonationConfirmationConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(DonationConfirmationConfigurationServiceImpl.class);

    private DonationConfirmationConfiguration donationConfirmation;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(DonationConfirmationConfiguration donationConfirmationConfiguration) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> {}", settings.getRunModes());
        this.donationConfirmation = donationConfirmationConfiguration;

        LOG.trace("Method Exit ::: activate()");
    }

    @Override
    public String getCustomerEmailSubject() {
        return donationConfirmation.customerEmailSubject();
    }

    @Override
    public String getCustomerEmailBodyTemplate() {
        return donationConfirmation.customerEmailBodyTemplate();
    }

    @Override
    public String getConfirmationPagePath() {
        return donationConfirmation.getConfirmationPagePath();
    }

    @Override
    public String getFailurePagePath() {
        return donationConfirmation.getFailurePagePath();
    }


}
