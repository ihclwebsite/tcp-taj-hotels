package com.ihcl.core.services.search;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.ihcl.core.models.offers.Offer;
import com.ihcl.core.models.search.SearchResult;
import com.ihcl.core.services.search.SearchServiceConstants.PATH;
import com.ihcl.core.services.search.SearchServiceConstants.RESOURCETYPE;
import com.ihcl.tajhotels.constants.CrxConstants;

@Component(service = OffersSearchService.class)
public class OfferSearchImpl implements OffersSearchService {

    private static final String OFFER_LIST_LENGTH = "8";

    private static final Logger LOG = LoggerFactory.getLogger(OfferSearchImpl.class);

    @Reference
    SearchProvider searchProvider;

    @Reference
    HotelsSearchService hotelSearchService;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Override
    public List<String> getAllOffers(String homePagePath) {

        HashMap<String, String> predicates = new HashMap<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        List<String> offerList = new ArrayList<>();
        try {
        	if (homePagePath.contains("ama")) {
        		predicates.put("path", homePagePath + "/offers-and-promotions");
			} else {
				predicates.put("path", homePagePath + "/offers");
			}
            predicates.put("property", "sling:resourceType");
            predicates.put("property.value", RESOURCETYPE.GLOBAL_OFFERS);
            predicates.put("1_property", "showInGlobalOffers");
            predicates.put("1_property.value", "true");
            predicates.put("group.p.or", "true");
            predicates.put("group.1_daterange.property", "offerEndsOn");
            predicates.put("group.1_daterange.lowerBound", sdf.format(new Date()));
            predicates.put("group.2_property", "roundTheYearOffer");
            predicates.put("group.2_property.operation", "exists");
            predicates.put("group.2_property.value", "true");
            predicates.put("p.limit", "-1");
            predicates.put("orderby", "@rank");
            predicates.put("orderby.sort", "desc");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            for (SearchResult searchResult : searchResults) {
                offerList.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Exception found while getting all offers :: {}", e.getMessage());
        }
        return offerList;
    }

    @Override
    public List<String> getAllOffersByRank(String offerPath) {
        HashMap<String, String> predicates = new HashMap<>();
        List<String> offerList = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            predicates.put("path", offerPath);
            predicates.put("path.flat", "true");
            predicates.put("property", "jcr:content/sling:resourceType");
            predicates.put("property.value", RESOURCETYPE.GLOBAL_OFFERS);
            predicates.put("1_property", "jcr:content/showInGlobalOffers");
            predicates.put("1_property.value", "true");
            predicates.put("group.p.or", "true");
            predicates.put("group.1_daterange.property", "jcr:content/offerEndsOn");
            predicates.put("group.1_daterange.lowerBound", sdf.format(new Date()));
            predicates.put("group.2_property", "jcr:content/offerEndsOn");
            predicates.put("group.2_property.operation", "exists");
            predicates.put("group.2_property.value", "false");
            predicates.put("p.limit", OFFER_LIST_LENGTH);
            predicates.put("orderby", "@jcr:content/rank");
            predicates.put("orderby.sort", "desc");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            for (SearchResult searchResult : searchResults) {
                offerList.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Exception in getting allOffersByBank :: {}", e.getMessage());
        }
        return offerList;
    }

    @Override
    public List<String> getSelectedOffersByRank(List<String> pathsToLookupOffers) {
        LOG.trace("Method Entry : getSelectedOffersByRank(List<String> pathsToLookupOffers)");
        HashMap<String, String> predicates = new HashMap<String, String>();
        List<String> offerList = new ArrayList<String>();
        try {
            if (pathsToLookupOffers != null && pathsToLookupOffers.size() > 0) {
                for (int index = 0; index < pathsToLookupOffers.size(); index++) {
                    predicates.put("1_group." + index + "_group.path", pathsToLookupOffers.get(index));
                    predicates.put("1_group." + index + "_group.property", "jcr:content/sling:resourceType");
                    predicates.put("1_group." + index + "_group.property.value", RESOURCETYPE.LOCAL_OFFERS);

                }
                predicates.put("group.p.or", "true");
                predicates.put("p.limit", "-1");
                predicates.put("orderby", "@jcr:content/rank");
                predicates.put("orderby.sort", "desc");
            }
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            LOG.trace("prepared Predicates : " + predicates);
            for (SearchResult searchResult : searchResults) {
                offerList.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Exception while getting the selected offers by bank :: {}", e.getMessage());
        }
        LOG.trace("Method Exit : getSelectedOffersByRank(List<String> pathsToLookupOffers)");
        return offerList;
    }

    @Override
    public List<String> getAllOffersForCountry(String countryName) {
        return null;
    }

    @Override
    public List<String> getAllOffersForHotel(List<String> hotelNames) {
        List<String> offerList = new ArrayList<String>();
        offerList.addAll(getCorporateOffersForHotels(hotelNames));
        return offerList;
    }

    /**
     * Getting global offers filtered as per hotel path
     *
     * @param hotelPaths
     * @return List of offer paths
     */
    private List<String> getCorporateOffersForHotels(List<String> hotelPaths) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        List<String> offerList = new ArrayList<String>();
        try {
            predicates.put("path", PATH.OFFERS_ROOT);
            predicates.put("1_property", "sling:resourceType");
            predicates.put("1_property.value", "tajhotels/components/content/participating-hotels");
            predicates.put("2_property", "pages");
            for (int i = 0; i < hotelPaths.size(); i++) {
                predicates.put("2_property." + i + "_value", hotelPaths.get(i));
            }
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            for (SearchResult searchResult : searchResults) {
                offerList.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Could not get the resourceResolver :: {}", e.getMessage());
        }
        return offerList;
    }

    /**
     * Getting offers local to a hotel
     *
     * @param hotelPath
     * @return List of offer paths
     */
    private List<String> getLocalOffersForHotel(String hotelPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        List<String> offerList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		try {
			predicates.put("path", hotelPath);
			predicates.put("1_property", "@jcr:content/sling:resourceType");
			predicates.put("1_property.value", RESOURCETYPE.LOCAL_OFFERS);
			predicates.put("group.p.or", "true");
			predicates.put("group.1_daterange.property", "jcr:content/offerEndsOn");
			predicates.put("group.1_daterange.lowerBound", sdf.format(new Date()));
			predicates.put("group.2_property", "jcr:content/offerEndsOn");
			predicates.put("group.2_property.operation", "exists");
			predicates.put("group.2_property.value", "false");
			predicates.put("p.limit", "-1");
			predicates.put("orderby", "@jcr:content/rank");
			predicates.put("orderby.sort", "desc");
			List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
			for (SearchResult searchResult : searchResults) {
				offerList.add(searchResult.getPath());
			}
		} catch (Exception e) {
            LOG.error("Could not get the resourceResolver :: {}", e.getMessage());
        }
        return offerList;
    }

    @Override
    public List<String> getAllOffersForHotel(String hotelName) {
        String hotelPath = hotelSearchService.getHotelPathForName(hotelName);
        ArrayList<String> offerList = new ArrayList<String>();
        ArrayList<String> hotelList = new ArrayList<String>();
        hotelList.add(hotelPath);
        offerList.addAll(getCorporateOffersForHotels(hotelList));
        // offerList.addAll(getLocalOffersForHotel(hotelList));
        return offerList;
    }

    @Override
    public List<String> getAllOffersForHotelPath(String hotelPath) {
        ArrayList<String> hotelList = new ArrayList<>();
        hotelList.add(hotelPath);
        List<String> finalOfferList = new ArrayList<String>();
        List<String> localOfferList = getLocalOffersForHotel(hotelPath);
        List<String> corpOfferList = getCorporateOffersForHotels(hotelList);
        HashMap<String, String> consolidatedOfferMap = new HashMap<String, String>();
        finalOfferList.addAll(localOfferList);

        return finalOfferList;
    }

    private List<String> removeDuplicateCorporateOffer(List<String> localOfferList, List<String> corpOfferList) {
        List<String> localOfferCodes = new ArrayList<String>();
        Map<String, String> corporateOfferMaps = new HashMap<String, String>();
        ResourceResolver resolver = null;
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            for (String localOfferPath : localOfferList) {
                Resource res = resolver.getResource(localOfferPath + "/jcr:content");
                if (res != null) {
                    Offer offer = res.adaptTo(Offer.class);
                    String rateCode = offer.getOfferRateCode();
                    localOfferCodes.add(rateCode);
                }
            }
            for (String corporateOfferPath : corpOfferList) {
                Resource res = resolver.getResource(corporateOfferPath + "/jcr:content");
                if (res != null) {
                    Offer offer = res.adaptTo(Offer.class);
                    String rateCode = offer.getOfferRateCode();
                    corporateOfferMaps.put(rateCode, corporateOfferPath);
                }
            }

            for (String localOfferCode : localOfferCodes) {
                String duplicateCode = corporateOfferMaps.get(localOfferCode);
                if (duplicateCode != null && !duplicateCode.isEmpty()) {
                    corporateOfferMaps.remove(localOfferCode);
                }

            }

            return getOfferListFromMap(corporateOfferMaps);

        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance");
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return null;
    }

    private void addOfferListInMap(Map<String, String> offerMap, List<String> offerList) {
        ResourceResolver resolver = null;
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            for (String offerPath : offerList) {
                Resource res = resolver.getResource(offerPath + "/jcr:content");
                if (res != null) {
                    Offer offer = res.adaptTo(Offer.class);
                    String rateCode = offer.getOfferRateCode();
                    if (rateCode != null && !rateCode.isEmpty()) {
                        offerMap.put(offer.getOfferRateCode(), offerPath);
                    }
                }
            }
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e.getMessage());
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
    }

    private List<String> getOfferListFromMap(Map<String, String> offerMap) {
        ArrayList<String> finalOfferList = new ArrayList<String>();
        for (Entry entry : offerMap.entrySet()) {
            finalOfferList.add((String) entry.getValue());
        }
        return finalOfferList;

    }

    @Override
    public List<String> getCorporateOffersForHotelPath(String hotelPath) {
        ArrayList<String> offerList = new ArrayList<>();
        ArrayList<String> hotelList = new ArrayList<>();
        hotelList.add(hotelPath);
        // offerList.addAll(getLocalOffersForHotel(hotelList));
        offerList.addAll(getCorporateOffersForHotels(hotelList));

        return offerList;
    }

    @Override
    public HashMap<String, String> getOfferCategories() {
        HashMap<String, String> offerTypes = new HashMap<String, String>();
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource tagRoot = resourceResolver.getResource("/etc/tags/taj/offers");
            if (tagRoot != null) {
                Iterable<Resource> childTags = tagRoot.getChildren();
                for (Resource resource : childTags) {
                    Tag tag = resource.adaptTo(Tag.class);
                    offerTypes.put(tag.getTagID(), tag.getTitle());
                }
            }
            Resource tagHolidaysRoot = resourceResolver.getResource("/etc/tags/taj/holidays");
            if (tagRoot != null) {
                Iterable<Resource> childTags = tagHolidaysRoot.getChildren();
                for (Resource resource : childTags) {
                    Tag tag = resource.adaptTo(Tag.class);
                    offerTypes.put(tag.getTagID(), tag.getTitle());
                }
            }
        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e.getMessage());
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return offerTypes;
    }

    @Override
    public String fetchOfferFromUUID(String uuid) {
        String offerPath = null;
        try {
            HashMap<String, String> predicates = new HashMap<String, String>();
            predicates.put("path", PATH.OFFERS_ROOT);
            predicates.put("1_property", "jcr:uuid");
            predicates.put("1_property.value", uuid);
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            for (SearchResult searchResult : searchResults) {
                offerPath = searchResult.getPath();
            }
        } catch (Exception e) {
            LOG.error("Could not get the resourceResolver :: {}", e.getMessage());
        }
        return offerPath;
    }

    @Override
    public String getLocalOfferTemplate(String hotelPath) {
        String offerPath = null;
        try {
            HashMap<String, String> predicates = new HashMap<String, String>();
            predicates.put("path", hotelPath);
            predicates.put("1_property", "sling:resourceType");
            predicates.put("1_property.value", CrxConstants.HOTEL_SPECIFIC_OFFER_RESOURCE_TYPE);
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            if (searchResults != null && !searchResults.isEmpty()) {
                offerPath = searchResults.get(0).getPath();
            }
        } catch (Exception e) {
            LOG.error("Could not get the resourceResolver :: {}", e.getMessage());
        }
        return offerPath;
    }

    @Override
    public String findLocalOfferForGlobalOfferName(String hotelPath, String globalOfferPath) {
        HashMap<String, String> predicates = new HashMap<String, String>();
        String offerPath = null;
        try {
            predicates.put("path", hotelPath);
            predicates.put("1_property", "sling:resourceType");
            predicates.put("1_property.value", CrxConstants.HOTEL_SPECIFIC_OFFER_RESOURCE_TYPE);
            predicates.put("2_property", CrxConstants.GLOBALPATH_IN_LOCALOFFER_PROPETY_NAME);
            predicates.put("2_property.value", globalOfferPath);
            predicates.put("p.limit", "-1");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
            if (searchResults != null && !searchResults.isEmpty()) {
                offerPath = searchResults.get(0).getPath();
            }
        } catch (Exception e) {
            LOG.error("Could not get the resourceResolver :: {}", e.getMessage());
        }
        return offerPath;
    }
    

}
