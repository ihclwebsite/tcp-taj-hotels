package com.ihcl.core.services.impl;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.ihcl.core.services.NodeOperationsService;

@Component(service = NodeOperationsService.class)
public class NodeOperationsImpl implements NodeOperationsService {

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	public static final Logger LOG = LoggerFactory.getLogger(NodeOperationsImpl.class);

	@Override
	public Node copyProperties(Node sourceNode, Node targetNode, List<String> ignoreProperties) {
		LOG.info("Inside method :copyProperties(Node sourceNode, Node targetNode, List<String> ignoreProperties)");
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			Session session = sourceNode.getSession();
			LOG.debug("Copying properties from " + sourceNode.getName() + "to the node" + targetNode.getName());
			PropertyIterator iterator = sourceNode.getProperties();
			while (iterator.hasNext()) {
				Property property = (Property) iterator.next();
				if (!ignoreProperties.contains(property.getName()) && !property.getDefinition().isProtected()) {
					targetNode.setProperty(property.getName(), property.getValue());
				}
			}
			session.save();
		} catch (RepositoryException e) {
			LOG.error("Failed to copy properties" + e);
			e.printStackTrace();
		} catch (LoginException e) {
			LOG.error("Failed to copy properties" + e);
			e.printStackTrace();
		}
		resourceResolver.close();
		return targetNode;
	}

	@Override
	public boolean replaceNode(Node sourceNode, Node replaceNode) {
		try {
				Session session = sourceNode.getSession();
				Node parentNode= session.getNode(replaceNode.getParent().getPath());
				replaceNode.remove();
				session.save();
				JcrUtil.copy(sourceNode, parentNode, sourceNode.getName());
				JcrUtil.setChildNodeOrder(parentNode, "breadcrumb,reference,offerdetails,offerinclusion,offer_terms_conditions,book_now_button");
				session.save();
		} catch (RepositoryException e) {
			LOG.error("Failed to  replace Node" + e);
		}
		return false;
	}

}
