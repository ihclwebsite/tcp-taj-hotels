/**
 *
 */
package com.ihcl.core.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.jsoup.Jsoup;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.dam.api.Rendition;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.sitemap.SiteMap;
import com.ihcl.core.services.IHCLSiteMapGeneratorService;

@Component(service = IHCLSiteMapGeneratorService.class,
        immediate = true)
public class IHCLSiteMapGeneratorServiceImpl implements IHCLSiteMapGeneratorService {

    private static final Logger logger = LoggerFactory.getLogger(IHCLSiteMapGeneratorServiceImpl.class);

    private static final String BASE_PATH_FOR_SITEMAP = "/content/tajhotels/ihcl";

    private static final String SITEMAP_PATH = "/content/dam/ihcl/sitemaps/sitemap-ihcl.xml";

    private static final String SITEMAP_IMAGE_PATH = "/content/dam/ihcl/sitemaps/image-sitemap-ihcl.xml";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    QueryBuilder builder;

    AssetManager assetMgr;

    Resource resource;

    ResourceResolver resourceResolver;

    InputStream inputStream;

    ByteArrayOutputStream outputStream;

    DocumentBuilder docBuilder;

    DocumentBuilderFactory docFactory;

    Document document;

    ArrayList<SiteMap> siteMapObject;

    String hostString;

    @Override
    public Boolean generateSiteMap(String hostString) {

        try {

            logger.info("Starting to generate site map");
            this.hostString = hostString;
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();

            readXmlFromDam(SITEMAP_PATH, resourceResolver);
            // read all the pages using QueryBuilder
            SearchResult result = getAllPages(BASE_PATH_FOR_SITEMAP);

            // Now we have got the updated sitemap from DAM, Lets add or delete if anything
            // has changed
            updateSiteMapEntries(siteMapObject, result);

            updateExistingSiteMap(SITEMAP_PATH);

            return true;
        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }
        return null;
    }


    @Override
    public Boolean generateImageSiteMap() {

        try {
            // Inject a ResourceResolver
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();
            readXmlFromDam(SITEMAP_PATH, resourceResolver);
            updateExistingImageSiteMap(SITEMAP_IMAGE_PATH);
            return true;

        } catch (Exception ex) {
            logger.error("Error/Exception is thrown while fetching Result Set or Writing a XML File" + ex.toString(),
                    ex);
        }
        return null;
    }

    private void readXmlFromDam(String path, ResourceResolver resourceResolver) throws Exception {
        logger.trace("inside readXmlFromDam");
        document = docBuilder.newDocument();
        // checking if sitemap.xml is present at root node.
        resource = resourceResolver.getResource(path);
        siteMapObject = new ArrayList<>();

        if (resource != null) {
            boolean isAssest = DamUtil.isAsset(resource);
            if (isAssest) {
                Asset asset = resource.adaptTo(Asset.class);
                List<Rendition> renditions = asset.getRenditions();
                for (Rendition rendition : renditions) {
                    String name = rendition.getName();
                    if (name.equals("original")) {
                        inputStream = rendition.getStream();
                        break;
                    }
                }
            }

            document = docBuilder.parse(inputStream);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("url");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node urlNode = nodeList.item(i);
                if (urlNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) urlNode;
                    element.getElementsByTagName("loc").item(0).getTextContent();

                    SiteMap siteMapEntry = new SiteMap();
                    siteMapEntry.setLoc(element.getElementsByTagName("loc").item(0).getTextContent());
                    siteMapEntry.setLastmod(element.getElementsByTagName("lastmod").item(0).getTextContent());
                    siteMapEntry.setChangefreq(element.getElementsByTagName("changefreq").item(0).getTextContent());
                    siteMapEntry.setPriority(element.getElementsByTagName("priority").item(0).getTextContent());
                    logger.trace("inside readXmlFromDam location " + siteMapEntry.getLoc());
                    siteMapObject.add(siteMapEntry);
                }
            }
        }
    }

    private SearchResult getAllPages(String path) {
        // Object Creation for QueryBuilder
        Map<String, String> queryMap = new TreeMap<>();
        queryMap.put("path", path);
        queryMap.put("type", "cq:Page");
        // queryMap.put("property", "jcr:content/cq:lastReplicationAction");
        // queryMap.put("property.value", "Activate");
        queryMap.put("p.hits", "all");
        queryMap.put("p.limit", "-1");

        Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
        // Execute the query and get the results ...
        return query.getResult();

    }

    private void updateSiteMapEntries(ArrayList<SiteMap> siteMapObject, SearchResult searchResult)
            throws ParseException {
        logger.trace("inside updatesitemap entries");
        // Lets add new Pages if any to the SiteMap
        for (Hit hit : searchResult.getHits()) {
            try {
                String pagePath = hit.getPath();
                ValueMap pageProperties = hit.getProperties();

                boolean newPageFlag = true;
                for (SiteMap siteMapEntry : siteMapObject) {
                    if (siteMapEntry.getLoc().equalsIgnoreCase(pagePath)) {
                        logger.trace("inside updatesitemap entries old page");
                        // converting the date from GregorianCalendar to String
                        Calendar cal = (Calendar) pageProperties.get("cq:lastModified");
                        DateFormat date = new SimpleDateFormat("dd/MM/yyyy");
                        siteMapEntry.setLastmod(date.format(cal.getTime()));
                        siteMapEntry.setChangefreq(String.valueOf(pageProperties.get("changefreq")));
                        siteMapEntry.setPriority(String.valueOf(pageProperties.get("priority")));
                        siteMapEntry.setIsPagePresent(true);
                        newPageFlag = false;
                        break;
                    }
                }
                if (newPageFlag) {
                    logger.trace("inside updatesitemap entries new page " + pagePath);

                    SiteMap siteMapEntry = new SiteMap();
                    siteMapEntry.setLoc(resourceResolver.map(hostString + pagePath + ".html"));
                    /*
                     * logger.info("hoststring = " + hostString + "  pagePath  =" + pagePath + " after mapping =" +
                     * resourceResolver.map(pagePath + ".html"));
                     */
                    // converting the date from GregorianCalendar to String
                    Calendar cal = (Calendar) pageProperties.get("cq:lastModified");
                    DateFormat date = new SimpleDateFormat("dd/MM/yyyy");
                    if (cal != null) {
                        siteMapEntry.setLastmod(date.format(cal.getTime()));
                    } else {
                        siteMapEntry.setLastmod(" ");
                        logger.info("Page name=" + hit.getPath());
                    }
                    if (pageProperties.containsKey("changefreq")) {
                        siteMapEntry.setChangefreq(String.valueOf(pageProperties.get("changefreq")));
                    } else {
                        siteMapEntry.setChangefreq("weekly");
                    }
                    if (pageProperties.containsKey("priority")) {
                        siteMapEntry.setPriority(String.valueOf(pageProperties.get("priority")));
                    } else {
                        siteMapEntry.setPriority("0.9");
                    }
                    siteMapEntry.setIsPagePresent(true);
                    siteMapObject.add(siteMapEntry);
                }

            } catch (RepositoryException e) {
                logger.error("Error/Exception is thrown while reading Result Set" + e.toString(), e);
            }
        }

        // Lets Delete any old pages which are not there anymore(not-activated).
        for (int i = 0; i < siteMapObject.size(); i++) {
            if (!siteMapObject.get(i).getIsPagePresent()) {
                siteMapObject.remove(i);
            }
        }
    }

    private void updateExistingSiteMap(String path) throws Exception {
        logger.trace("inside updateExistingSiteMap");
        // Lets create a new sitemap.XML file with freshly update data
        document = docBuilder.newDocument();
        Element urlset = document.createElement("urlset");

        Attr attr = document.createAttribute("xmlns");
        attr.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");
        urlset.setAttributeNode(attr);

        document.appendChild(urlset);

        for (SiteMap siteMapEntry : siteMapObject) {
            logger.trace("inside updateExistingSiteMap loc " + siteMapEntry.getLoc());
            Element url = document.createElement("url");
            urlset.appendChild(url);

            // loc elements
            Element loc = document.createElement("loc");
            loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
            url.appendChild(loc);

            // lastmod elements
            Element lastmod = document.createElement("lastmod");
            lastmod.appendChild(document.createTextNode(siteMapEntry.getLastmod()));
            url.appendChild(lastmod);

            // changefreq elements
            Element changefreq = document.createElement("changefreq");
            changefreq.appendChild(document.createTextNode(siteMapEntry.getChangefreq()));
            url.appendChild(changefreq);

            // priority elements
            Element priority = document.createElement("priority");
            priority.appendChild(document.createTextNode(siteMapEntry.getPriority()));
            url.appendChild(priority);
        }

        // write the content into new generated xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(document);
        Result outputTarget = new StreamResult(outputStream);

        transformer.transform(xmlSource, outputTarget);
        inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        writeXmlToDam(inputStream, resourceResolver, path);

    }

    private void writeXmlToDam(InputStream inputStream, ResourceResolver resourceResolver, String path) {
        try {
            // Use AssetManager to place the file into the AEM DAM
            logger.trace("inside writeToDam");
            assetMgr = resourceResolver.adaptTo(AssetManager.class);
            if (assetMgr != null) {
                assetMgr.createAsset(path, inputStream, "application/xml", true);
            }

        } catch (Exception e) {
            logger.error("Error/Exception while writing file to DAM" + e.toString(), e);
        }
    }

    private void updateExistingImageSiteMap(String path) throws Exception {
        logger.trace("inside updateExistingImageSiteMap");
        // Lets create a new sitemap.XML file with freshly update data
        document = docBuilder.newDocument();
        Element urlset = document.createElement("urlset");

        Attr xmlns = document.createAttribute("xmlns");
        xmlns.setValue("http://www.sitemaps.org/schemas/sitemap/0.9");

        Attr xmlnsImage = document.createAttribute("xmlns:image");
        xmlnsImage.setValue("http://www.google.com/schemas/sitemap-image/1.1");

        urlset.setAttributeNode(xmlns);
        urlset.setAttributeNode(xmlnsImage);

        document.appendChild(urlset);

        org.jsoup.nodes.Document jsoupDocument;
        org.jsoup.select.Elements jsoupElements;
        try {
            for (SiteMap siteMapEntry : siteMapObject) {
                Element url = document.createElement("url");
                urlset.appendChild(url);
                logger.trace("inside updateExistingImageSiteMap" + siteMapEntry.getLoc());
                // loc elements
                Element loc = document.createElement("loc");
                loc.appendChild(document.createTextNode(siteMapEntry.getLoc()));
                url.appendChild(loc);

                Element changeFreq = document.createElement("changefreq");
                changeFreq.appendChild(document.createTextNode(siteMapEntry.getChangefreq()));
                url.appendChild(changeFreq);

                Element priority = document.createElement("priority");
                priority.appendChild(document.createTextNode(siteMapEntry.getPriority()));
                url.appendChild(priority);
                try {
                    logger.info("URL of page =" + siteMapEntry.getLoc());
                    jsoupDocument = Jsoup.connect(siteMapEntry.getLoc()).validateTLSCertificates(false).get();
                    jsoupElements = jsoupDocument.select("img");
                    for (org.jsoup.nodes.Element element : jsoupElements) {
                        if (element.hasAttr("src")) {
                            if (element.absUrl("src") != null && !element.absUrl("src").contains("style-icons")) {
                                Element parentImageTag = document.createElement("image:image");
                                url.appendChild(parentImageTag);
                                Element imageLoc = document.createElement("image:loc");
                                imageLoc.appendChild(document.createTextNode(element.absUrl("src")));
                                parentImageTag.appendChild(imageLoc);
                                logger.trace("inside image of cureent page " + element.absUrl("src"));
                                if (element.hasAttr("title")) {
                                    Element imageTitle = document.createElement("image:title");
                                    imageTitle.appendChild(document.createTextNode(element.attr("title")));
                                    parentImageTag.appendChild(imageTitle);
                                }
                                if (element.hasAttr("alt")) {
                                    Element imageCaption = document.createElement("image:caption");
                                    imageCaption.appendChild(document.createTextNode(element.attr("alt")));
                                    parentImageTag.appendChild(imageCaption);
                                }
                            }

                        }

                    }
                } catch (Exception e) {
                    logger.error("Error/Exception is thrown while crawling the page" + e.toString(), e);
                }
            }

            // write the content into new generated xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            outputStream = new ByteArrayOutputStream();
            Source xmlSource = new DOMSource(document);
            Result outputTarget = new StreamResult(outputStream);

            transformer.transform(xmlSource, outputTarget);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());

            writeXmlToDam(inputStream, resourceResolver, path);

        } catch (Exception e) {
            logger.error("Error/Exception is thrown while crawling the page" + e.toString(), e);
        }

    }

}
