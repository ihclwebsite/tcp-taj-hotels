package com.ihcl.core.services.booking.impl;

import com.ihcl.core.services.booking.PaymentCardDetailsConfiguration;
import com.ihcl.core.services.booking.PaymentCardDetailsConfigurationService;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true,
        service = PaymentCardDetailsConfigurationService.class)
@Designate(ocd = PaymentCardDetailsConfiguration.class)
public class PaymentCardDetailsConfigurationServiceImpl implements PaymentCardDetailsConfigurationService {


    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceImpl.class);

    private PaymentCardDetailsConfiguration config;

    @Reference
    private SlingSettingsService settings;


    @Activate
    public void activate(PaymentCardDetailsConfiguration config) {

        String methodName = "activate";
        LOG.trace("Method Entry ::: " + methodName);

        this.config = config;

        LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.ConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOG.trace("Method Exit ::: " + methodName);
    }

    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOG.trace("Method Entry ::: " + methodName);
        LOG.trace("Present Run Mode ==> " + settings.getRunModes().toString());

        String env = "";

        if (settings.getRunModes().contains("author"))
            env = "LOCAL";
        else if (settings.getRunModes().contains("dev"))
            env = "DEV";
        else if (settings.getRunModes().contains("stage"))
            env = "STAGING";
        LOG.trace("Method Exit ::: " + methodName);
    }

    @Override
    public String getCardNumber() {
        return config.cardNumber();
    }

    @Override
    public String getCardName() {
        return config.cardName();
    }

    @Override
    public String getExpiryDate() {
        return config.expityDate();
    }

    @Override
    public String getCardType() {
        return config.cardType();
    }
}
