package com.ihcl.core.services.serviceamenity.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.serviceamenity.ServiceAmenity;
import com.ihcl.core.services.serviceamenity.AvailableServiceAmenityPathFetcherConfig;
import com.ihcl.core.services.serviceamenity.IAllAvailableServiceAmenitiesFetcher;

@Component(immediate = true,
        service = IAllAvailableServiceAmenitiesFetcher.class)
@Designate(ocd = AvailableServiceAmenityPathFetcherConfig.class)
public class AllAvailableServiceAmenitiesFetcherImpl implements IAllAvailableServiceAmenitiesFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(AllAvailableServiceAmenitiesFetcherImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    private String serviceAmenitiesPagePath;

    @Activate
    @Modified
    protected void activate(AvailableServiceAmenityPathFetcherConfig config) {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        serviceAmenitiesPagePath = config.getAvailableServiceAmenitiesPagePath();
        LOG.debug("Configured serviceAmenitiesPagePath as: " + serviceAmenitiesPagePath);
        LOG.trace("Method Exit: " + methodName);
    }

    protected List<ServiceAmenity> fetchAvailableServiceAmenities() {
        String methodName = "fetchAvailableServiceAmenities";
        LOG.trace("Method Entry: " + methodName);
        List<ServiceAmenity> availableServiceAmenities = new ArrayList<>();
        try {
            ResourceResolver resourceResolver;
            LOG.debug("Attempting to fetch service resolver");
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resourceResolver as: " + resourceResolver);
            Resource resource = resourceResolver.getResource(serviceAmenitiesPagePath + "/jcr:content/root");
            LOG.debug("Received root node under service amenity page path as: " + resource);
            if (resource != null && resource.hasChildren()) {
                LOG.debug("resource is not null: proceeding further");
                for (Resource rootNodeResource : resource.getChildren()) {
                    availableServiceAmenities.add(getServiceAmenityAt(rootNodeResource));
                }
            }
        } catch (LoginException e) {
            LOG.error("An error occured while logging in.", e);
        }
        LOG.trace("Method Exit: " + methodName);
        return availableServiceAmenities;
    }

    public ServiceAmenity getServiceAmenityAt(Resource resource) {
        String methodName = "getServiceAmenityAt";
        LOG.trace("Method Entry: " + methodName);
        ServiceAmenity amenity = null;
        String resourceType = resource.getResourceType();
        if (resourceType.equals("tajhotels/components/content/service-amenity")) {
            ValueMap valueMap = resource.getValueMap();
            if (valueMap.containsKey("name")) {
                String serviceName = valueMap.get("name", String.class);
                String imagePath = valueMap.get("image", String.class);
                String description = valueMap.get("description", String.class);
                String group = valueMap.get("group", String.class);
                String path = resource.getPath();
                amenity = new ServiceAmenity(serviceName, imagePath, description, group, path);
                amenity.setPath(resource.getPath());
            }
        }
        LOG.trace("Method Exit: " + methodName);
        return amenity;
    }

    @Override
    public List<ServiceAmenity> getServiceAmenities() {
        return fetchAvailableServiceAmenities();
    }

}

