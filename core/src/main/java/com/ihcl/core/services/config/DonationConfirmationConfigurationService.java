/**
 * 
 */
package com.ihcl.core.services.config;

/**
 * @author TCS
 *
 */
public interface DonationConfirmationConfigurationService {
	
    String getCustomerEmailSubject();

    String getCustomerEmailBodyTemplate();

    String getConfirmationPagePath();

    String getFailurePagePath();
}
