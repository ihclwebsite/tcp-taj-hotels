package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajEventBookingConfigurationService;
import com.ihcl.core.services.config.TajEventsBookingConfiguration;

@Component(immediate = true,
        service = TajEventBookingConfigurationService.class)
@Designate(ocd = TajEventsBookingConfiguration.class)
public class TajEventBookingConfigurationServiceImpl implements TajEventBookingConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(TajEventBookingConfigurationServiceImpl.class);

    private TajEventsBookingConfiguration eventConfiguration;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(TajEventsBookingConfiguration eventConfiguration) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> {}", settings.getRunModes());
        this.eventConfiguration = eventConfiguration;

        LOG.trace("Method Exit ::: activate()");
    }

    @Override
    public String getEventAdminEmailId() {
        return eventConfiguration.getEventAdminEmailId();
    }

    @Override
    public String getCustomerEmailSubject() {
        return eventConfiguration.customerEmailSubject();
    }

    @Override
    public String getCustomerEmailBodyTemplate() {
        return eventConfiguration.customerEmailBodyTemplate();
    }

    @Override
    public String getHotelEmailSubject() {
        return eventConfiguration.hotelEmailSubject();
    }

    @Override
    public String getHotelEmailBodyTemplate() {
        return eventConfiguration.hotelEmailBodyTemplate();
    }

    @Override
    public String getConfirmationPagePath() {
        return eventConfiguration.getConfirmationPagePath();
    }

    @Override
    public String getFailurePagePath() {
        return eventConfiguration.getFailurePagePath();
    }
}
