package com.ihcl.core.services;

public interface DispatcherRateFlushService {

    String ERROR = "ERROR";

    String clearHotelRateCache(String hotelId);

    String clearDestinationRatesCache(String destinationTag);

    String flushCache(String handle, String server, String protocol);

}
