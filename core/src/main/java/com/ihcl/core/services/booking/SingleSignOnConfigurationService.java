/**
 *
 */
package com.ihcl.core.services.booking;


/**
 * @author admin
 *
 */
public interface SingleSignOnConfigurationService {

    String[] getSingleSignOnDomains();

    String[] getSingleSignOnCookieDomainNames();

    String getSingleSignOnCookiePath();

    String getDomainConnectionTimeout();

    String getSingleSignOnCookieName();

    String getSingleSignOnCookieMaxAge();


}
