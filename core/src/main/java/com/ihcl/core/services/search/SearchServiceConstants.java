package com.ihcl.core.services.search;

public class SearchServiceConstants {

    public static class RESOURCETYPE {

        public static final String DESTINATION = "tajhotels/components/structure/destination-landing-page";

        public static final String LOCAL_OFFERS = "tajhotels/components/structure/hotel-specific-offer-details-page";

        public static final String GLOBAL_OFFERS = "tajhotels/components/structure/offer-details-page";

        public static final String HOTELS = "tajhotels/components/structure/hotels-landing-page";

        public static final String RESTAURANTS = "tajhotels/components/structure/dining-details-page";

        public static final String JIVA_SPA = "tajhotels/components/content/jiva-spa-details";

        public static final String EXPERIENCES = "tajhotels/components/content/experience-details";

        public static final String HOLIDAY_DESTINATION = "tajhotels/components/structure/tajholidays-destination-landing-page";

        public static final String HOLIDAY_HOTELS = "tajhotels/components/structure/tajholidays-destination-hotel-page";

        public static final String MEETING_VENUES = "tajhotels/components/structure/tajhotels-meetings-page";

    }

    public static class PATH {

        public static final String TAJ_ROOT = "/content/tajhotels";

        public static final String EN = "/en-in";

        public static final String HOTELROOT = "/content/tajhotels/en-in/our-hotels/";

        public static final String OUR_HOTELS = "/our-hotels/";

        public static final String OFFERS_ROOT = "/content/tajhotels/en-in/offers/";

        public static final String COUNTRY_TAG_ROOT = "/etc/tags/taj/destination";

        public static final String DESTINATION_TAG_ROOT = "/etc/tags/taj/offers";

        public static final String HOTEL_TYPE_TAG_ROOT = "/etc/tags/taj/hotels/types";

        public static final String JIVA_SPA_ROOT = "/content/tajhotels/en-in/jiva-spa";

        public static final String JIVA_SPA_PATH = "/en-in/jiva-spa";

    }

    public static String[] brandsSortOrder = new String[] { "/taj/", "/vivanta/", "/gateway/" };
}
