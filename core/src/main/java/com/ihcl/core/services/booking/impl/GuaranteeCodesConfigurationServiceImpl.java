/**
 *
 */
package com.ihcl.core.services.booking.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.GuaranteeCodesConfiguration;
import com.ihcl.core.services.booking.GuaranteeCodesConfigurationService;

/**
 * @author Vijay Chikkani
 *
 */
@Component(immediate = true,
        service = GuaranteeCodesConfigurationService.class)
@Designate(ocd = GuaranteeCodesConfiguration.class)
public class GuaranteeCodesConfigurationServiceImpl implements GuaranteeCodesConfigurationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuaranteeCodesConfigurationServiceImpl.class);

    private GuaranteeCodesConfiguration guaranteeCodesConfiguration;

    @Reference
    private SlingSettingsService slingSettingsService;

    @Activate
    public void activate(GuaranteeCodesConfiguration guaranteeCodesConfiguration) {

        String methodName = "activate";
        LOGGER.trace("Method Entry ::: {}", methodName);

        this.guaranteeCodesConfiguration = guaranteeCodesConfiguration;

        LOGGER.trace(
                "Properties Fetched from com.ihcl.core.services.booking.impl.GuaranteeCodesConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOGGER.trace("Method Exit ::: {}", methodName);
    }


    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOGGER.trace("Method Entry ::: {}", methodName);
        LOGGER.trace("Present Run Mode ==> {}", slingSettingsService.getRunModes().toString());

        String env = "";

        if (slingSettingsService.getRunModes().contains("author"))
            env = "LOCAL";
        else if (slingSettingsService.getRunModes().contains("dev"))
            env = "DEV";
        else if (slingSettingsService.getRunModes().contains("stage"))
            env = "STAGING";

        LOGGER.trace("present ENV ==> " + env + "getDowntimeStarts :: > " + getGuaranteeCodes().toString());
        LOGGER.trace("Method Exit ::: {}", methodName);
    }

    @Override
    public String[] getGuaranteeCodes() {
        return guaranteeCodesConfiguration.getGuaranteeCodes();
    }

}
