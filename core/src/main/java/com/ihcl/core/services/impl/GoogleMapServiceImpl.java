package com.ihcl.core.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.ihcl.core.services.GoogleMapService;


public class GoogleMapServiceImpl implements GoogleMapService {
	
       private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GoogleMapServiceImpl.class);
	
	String myJSON="" ;
	String keyValue;
	String sourceLatLong;
	String strLatitude;
    String strLongtitude;
	
	@Override
	public String[] getLatLong(String address) throws RuntimeException {
		
	
		try
		{
				int responseCode = 0;
			    String api = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + URLEncoder.encode(address, "UTF-8") + "&sensor=true";
			    LOG.debug("The api value: "+api);
			    URL url = new URL(api);
			    HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			    httpConnection.connect();
			    responseCode = httpConnection.getResponseCode();
			    if(responseCode == 200)
			    {
			    	LOG.debug("Inside Response");
			      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = builder.parse(httpConnection.getInputStream());
			      XPathFactory xPathfactory = XPathFactory.newInstance();
			      XPath xpath = xPathfactory.newXPath();
			      XPathExpression expr = xpath.compile("/GeocodeResponse/status");
			      String status = (String)expr.evaluate(document, XPathConstants.STRING);
			      
			      if(status.equals("OK"))
			      {
			         expr = xpath.compile("//geometry/location/lat");
			         String latitude = (String)expr.evaluate(document, XPathConstants.STRING);
			         LOG.debug(" Latitude :"+latitude);
			         expr = xpath.compile("//geometry/location/lng");
			         String longitude = (String)expr.evaluate(document, XPathConstants.STRING);
			         LOG.debug(" Longitude :"+longitude);
			        String[] str={latitude, longitude};
			        return str;
			        	     
			      }
			      else
			      {
			         throw new Exception("Error from the API - response status: "+status);
			      }
			    }

		}
		catch(Exception e)
		{
			e.printStackTrace();
			}
				    return null;
			  }

	@Override
	public String getAddressFromLatLng(String apiKey,String latitude,String longitude)  {
		
		String methodName ="getAddressFromLatLng";
		LOG.trace("Method Entry :" +methodName);
		
		String latlngString =latitude+","+longitude;
		LOG.trace("Api key : " + apiKey +" latlngString : " +latlngString);
		String addressFromLatLng="";
		
		try {
			
		String api = "https://maps.googleapis.com/maps/api/geocode/json?key="+apiKey+"&latlng=" + URLEncoder.encode(latlngString, "UTF-8") + "&sensor=true";	
		String jsonResponseString=readJsonFromUrl(api);
	    JSONObject jsonObj = new JSONObject(jsonResponseString);
	    if(jsonObj.get("status").equals("OK")) {
	    JSONArray resultsArray =(JSONArray) jsonObj.get("results");
	    for(int i = 0; i < resultsArray.length(); i++)
	    {
	          String addressString =resultsArray.getJSONObject(i).getString("formatted_address");
	          if(!addressString.contains("Unnamed Road")) {
		    		addressFromLatLng=addressString;
		    		break;
		    	}
	    }
	    LOG.trace("AddressFromLatLng:: "+ addressFromLatLng);
	    }
		}catch(Exception e) {
	    	LOG.error("Exception occured while fetching address" + e.getMessage());
	    }
		LOG.trace("Method Exit :" +methodName);
		return addressFromLatLng;
		}
	
	private String readAll(Reader reader) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            stringBuilder.append((char) cp);
        }
        return stringBuilder.toString();
    }

    private String readJsonFromUrl(String url) {
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        String jsonText = null;
        try {
            inputStream = new URL(url).openStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            jsonText = readAll(bufferedReader);
            bufferedReader.close();
            inputStream.close();
        } catch (Exception e) {
            
        }
        return jsonText;
    }

	}
	
	
	
