package com.ihcl.core.services.impl;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.DispatcherRateFlushService;
import com.ihcl.core.services.config.FlushCacheDispatcherConfigurationService;

@Component(immediate = true,
        service = DispatcherRateFlushService.class)

public class DispatcherRateFlushServiceImpl implements DispatcherRateFlushService {

    private static final String DELETE = "Delete";

    private static final String HOST = "Host";

    private static final String CQ_HANDLE = "CQ-Handle";

    private static final String CQ_ACTION = "CQ-Action";

    @Reference
    private FlushCacheDispatcherConfigurationService flushCacheConfig;

    private Logger logger = LoggerFactory.getLogger(DispatcherRateFlushServiceImpl.class);

    @Override
    public String clearHotelRateCache(String hotelId) {
        logger.info("Inside method clearHotelRateCache");
        if (null != flushCacheConfig) {
            String handle = flushCacheConfig.getRateCachePath() + hotelId;
            return prepareFlushCacheFor(handle);
        }
        return ERROR;
    }

    @Override
    public String clearDestinationRatesCache(String destinationTag) {
        logger.info("Inside method clearDestinationRatesCache");
        if (null != flushCacheConfig) {
            String handle = flushCacheConfig.getDestinationRatesCachePath() + destinationTag;
            return prepareFlushCacheFor(handle);
        }
        return ERROR;
    }

    public String prepareFlushCacheFor(String handle) {

        String[] dispatchers = flushCacheConfig.getDispatcherIps();
        String responseString = "";
        StringBuffer responseStr = new StringBuffer();
        for (String dispatcher : dispatchers) {
            responseString = "";
            logger.info("dispatcher port flag: " + flushCacheConfig.getDispatcherPort().isEmpty());
            dispatcher = dispatcher + (!flushCacheConfig.getDispatcherPort().isEmpty() ? ":" : "")
                    + flushCacheConfig.getDispatcherPort();
            responseString = flushCache(handle, dispatcher, flushCacheConfig.getProtocol());
            responseStr.append("Dispatcher ").append(dispatcher).append(" has returned ").append(System.lineSeparator())
                    .append(System.lineSeparator()).append(responseString).append(System.lineSeparator())
                    .append(System.lineSeparator());
        }
        return responseStr.toString();
    }

    @Override
    public String flushCache(String handle, String server, String protocol) {
        StringBuffer responseStr = new StringBuffer();
        for (String hostName : this.flushCacheConfig.getFlushHosts()) {
            String responseStringFromHttpCall = flushCacheForHostName(handle, server, protocol, hostName);

            responseStr.append("For Host : ").append(hostName)
                    .append((responseStringFromHttpCall != ERROR ? (",  The path  \"" + handle + "\" has been cleared")
                            : ("  Error occured while clearing \"" + handle + " \"")))
                    .append(System.lineSeparator());
        }
        return responseStr.toString();
    }

    private String flushCacheForHostName(String handle, String server, String protocol, String hostName) {
        try {
            String uri = protocol + "://" + server + "/dispatcher/invalidate.cache";
            logger.info("Final URL " + uri);
            logger.info("Flush Path " + handle);
            logger.info("Clearing Cache for Host {}", hostName);

            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(uri);
            post.setHeader(CQ_ACTION, DELETE);
            post.setHeader(CQ_HANDLE, handle);

            post.setHeader(HOST, hostName);
            client.execute(post);
            CloseableHttpResponse httpResponse = client.execute(post);

            logger.info("Flushcache finished invoking the URL");
            String stringResponse = EntityUtils.toString(httpResponse.getEntity());
            logger.info("Flushcache dispatcher http response:" + stringResponse);
            httpResponse.close();

            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                logger.info("Flushcache dispatcher request was a success");
                return stringResponse;
            } else {
                logger.info("Flushcache dispatcher request failed");
                return ERROR;
            }
        } catch (Exception e) {
            logger.error("Flushcache exception: " + e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }
}
