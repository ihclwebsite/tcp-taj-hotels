/**
 *
 */
package com.ihcl.core.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.dam.api.Rendition;
import com.ihcl.core.exception.TajHotelException;
import com.ihcl.core.services.ImageCropperService;

/**
 * @author Srikanta.moonraft
 *
 */
@Component(
        service = ImageCropperService.class,
        immediate = true)
public class ImageCropperServiceImpl implements ImageCropperService {

    private static final Logger LOG = LoggerFactory.getLogger(ImageCropperServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    private ResourceResolver resourceResolver;

    String incomingImageUrl;


    String incommigImageMimeType = null;

    String incommigImageFileFormat = null;

    Asset imageAsset = null;

    Rendition originalRendition = null;

    String croppedImageUrl = null;

    int croppedImageWidth;

    int croppedImageHeight;

    Rendition newRendition = null;

    String newRenditionName = null;

    @Override
    public String cropImageFromUrl(String incomingImageUrl, int x, int y, int width, int height) {
        LOG.trace(
                "Entry > [Method : cropImageFromUrl(incomingImageUrl, x, y, width, height)] :: [Annotations : @Override");
        this.incomingImageUrl = incomingImageUrl;
        LOG.debug("incomingImageUrl: " + incomingImageUrl);
        try {
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            if (resourceResolver != null) {
                LOG.debug("Resolver is not Null & User ID : " + resourceResolver.getUserID());

                // read the image from the incoming URL
                BufferedImage incomingImage = readImageFromUrl(incomingImageUrl, originalRendition);
                LOG.debug("Read as Buffered image & its Width : " + incomingImage.getWidth());

                // crop the image
                BufferedImage croppedImage = cropImage(incomingImage, x, y, width, height);
                croppedImageWidth = croppedImage.getWidth();
                croppedImageHeight = croppedImage.getHeight();
                LOG.debug("generated croppedImage & its Width : " + croppedImageWidth);

                if (croppedImageWidth > 0 && croppedImageHeight > 0) {
                    newRenditionName = "cq5dam.cropped." + croppedImageWidth + "." + croppedImageHeight + "."
                            + incommigImageFileFormat;
                    LOG.debug("created new rendition Name as : " + newRenditionName);

                    // store the cropped image to the rendition URL
                    LOG.debug("converting --> 'buffered image to --> input stream'");
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    ImageIO.write(croppedImage, incommigImageFileFormat, byteArrayOutputStream);
                    InputStream is = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                    LOG.debug("converted --> 'buffered image to --> input stream'");

                    // Adding rendition to existing asset
                    LOG.debug("generating --> 'new rendition inside the existing image'");
                    newRendition = imageAsset.addRendition(newRenditionName, is, incommigImageMimeType);
                    LOG.debug("image rendition Path: " + newRendition.getPath());
                    croppedImageUrl = newRendition.getPath();
                } else {
                    LOG.debug("croppedImage has invalid width and Height. Error in cropping. ");
                    LOG.error("croppedImage has invalid width and Height. Error in cropping. ",
                            new TajHotelException("Invalid width and height in cropped image"));
                }

                // LOG.debug("skipping --> 'create a new image with multiple rendition'");
                // croppedImageUrl = writeToDam(is, incomingImageUrl);
                LOG.debug("croppedImageUrl : " + croppedImageUrl);
            } else if (resourceResolver == null) {
                LOG.debug("resourceResolver is null, Can't proceed further. ");
            }
        } catch (LoginException | IOException e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.debug("Returning cropped image URL as : " + croppedImageUrl);
        LOG.trace(
                "Exit > [Method : cropImageFromUrl(incomingImageUrl, x, y, width, height) ] :: [ Annotations : @Override");
        return croppedImageUrl;
    }

    private BufferedImage readImageFromUrl(String incommingUrl, Rendition originalRendition) {
        LOG.trace("Entry > [Method : readImageFromUrl(url) ] ");
        BufferedImage bufferedImage = null;
        try {
            LOG.debug("URL for reading : " + incommingUrl);

            // getting the image as resource
            Resource imageResource = resourceResolver.getResource(incommingUrl);
            LOG.debug("getting the image as resource & its Path : " + imageResource.getPath());
            imageAsset = imageResource.adaptTo(Asset.class);
            LOG.debug("Image Adapted as >> Asset and its Path : " + imageAsset.getPath() + ", MimeType : "
                    + imageAsset.getMimeType());
            originalRendition = imageAsset.getOriginal();
            incommigImageMimeType = originalRendition.getMimeType();
            incommigImageFileFormat = incommigImageMimeType.substring(6);
            LOG.debug("Got the Original rendition and its Size : " + originalRendition.getSize() + "Mime Type : "
                    + incommigImageMimeType);
            if (incommigImageMimeType.substring(0, 5).contains("image")) {
                LOG.debug("image Format  : " + incommigImageFileFormat);
            } else {
                LOG.debug("image Format  Not supprted : " + incommigImageFileFormat);
                LOG.debug("image part : " + incommigImageMimeType.substring(1, 5));
                LOG.error("image Format  Not supprted : ",
                        new TajHotelException(incommigImageFileFormat + " Format not supprted"));
            }
            // converting Rendition to Buffered Image
            LOG.debug("getting RenditionStream as >> Buffered image ");
            bufferedImage = ImageIO.read(originalRendition.getStream());
            LOG.debug("converted RenditionStream as >> Buffered image & its Type : " + bufferedImage.getType());

        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.debug("returning Buffered image & it's width is : " + bufferedImage.getWidth());
        LOG.trace("Exit > [Method : readImageFromUrl(url) ] ");
        return bufferedImage;
    }

    /**
     * Crops an image to the specified region
     *
     * @param bufferedImage
     *            the image that will be crop
     * @param x
     *            the upper left x coordinate that this region will start
     * @param y
     *            the upper left y coordinate that this region will start
     * @param width
     *            the width of the region that will be crop
     * @param height
     *            the height of the region that will be crop
     * @return the image that was cropped.
     */
    private BufferedImage cropImage(BufferedImage bufferedImage, int x, int y, int width, int height) {
        LOG.trace("Entry > [Method : cropImage(bufferedImage, x, y, width, height) ] ");
        BufferedImage croppedImage = bufferedImage.getSubimage(x, y, width, height);
        LOG.debug("Cropped Image -> Width :" + croppedImage.getWidth() + ", Height :" + croppedImage.getHeight()
                + ", Type : " + croppedImage.getType());


        LOG.trace("Exit > [Method : cropImage(bufferedImage, x, y, width, height) ] ");
        return croppedImage;
    }


    // Save the uploaded file into the AEM DAM using AssetManager API
    private String writeToDam(InputStream is, String oldFilePath) {
        LOG.trace("Entry > [Method : writeToDam(InputStream, oldFilePath) ] ");
        try {
            // Use AssetManager to place the file into the AEM DAM
            AssetManager assetMgr = resourceResolver.adaptTo(AssetManager.class);
            // String newFile = "/content/dam/travel/" + fileName;
            String newImageUrl = incomingImageUrl + "/jcr:content/renditions/author_cropped";
            LOG.debug("newImageUrl" + newImageUrl);
            assetMgr.createAsset(newImageUrl, is, incommigImageMimeType, true);

            // Return the path to the file was stored
            LOG.trace("Exit > [Method : writeToDam(InputStream, oldFilePath) ] ");
            return newImageUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOG.trace("Exit > [Method : writeToDam(InputStream, oldFilePath) ] ");
        return null;
    }
}
