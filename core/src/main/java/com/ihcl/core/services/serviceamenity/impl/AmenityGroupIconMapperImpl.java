package com.ihcl.core.services.serviceamenity.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.serviceamenity.AmenityGroupIconFetcherConfig;
import com.ihcl.core.services.serviceamenity.AmenityGroupIconMapper;

@Component(immediate = true, service = AmenityGroupIconMapper.class)
@Designate(ocd = AmenityGroupIconFetcherConfig.class)
public class AmenityGroupIconMapperImpl implements AmenityGroupIconMapper {

	private static final Logger LOG = LoggerFactory.getLogger(AllAvailableServiceAmenitiesFetcherImpl.class);

	private String[] amenityGroupIconPaths;
	private String iconPath = "";

	@Activate
	@Modified
	protected void activate(AmenityGroupIconFetcherConfig config) {
		String methodName = "activate";
		LOG.trace("Method Entry: " + methodName);
		amenityGroupIconPaths = config.getAmenityGroupIconPaths();
		LOG.debug("Configured serviceAmenitiesPagePath as: " + amenityGroupIconPaths);
		LOG.trace("Method Exit: " + methodName);
	}

	@Override
	public String getIconPath(String groupName) {
		String[] values;
		LOG.debug("Group name is : " + groupName);
		if (amenityGroupIconPaths != null && amenityGroupIconPaths.length != 0) {
			for (int i = 0; i < amenityGroupIconPaths.length; i++) {
				values = amenityGroupIconPaths[i].split("=");
				if ( values.length>1 && values[0].equals(groupName)) {
					iconPath = values[1];
					break;
				}
			}
		}
		LOG.debug("Group icon path is : " + iconPath);
		return iconPath;
	}
}
