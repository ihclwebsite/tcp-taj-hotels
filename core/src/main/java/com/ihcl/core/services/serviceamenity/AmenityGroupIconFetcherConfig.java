package com.ihcl.core.services.serviceamenity;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Amenity Group Icon Fetcher Configuration",
        description = "The configuration to fetch the amenity group icon.")
public @interface AmenityGroupIconFetcherConfig {

    @AttributeDefinition(name = "Amenity Group Icon Path",
            description = "Path for the given amenity group")
    String[] getAmenityGroupIconPaths() default "";
}
