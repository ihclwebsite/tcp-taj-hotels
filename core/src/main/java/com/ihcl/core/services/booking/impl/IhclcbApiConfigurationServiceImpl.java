package com.ihcl.core.services.booking.impl;


import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.GiftcardConfiguration;
import com.ihcl.core.services.booking.IhclcbApiConfiguration;
import com.ihcl.core.services.booking.IhclcbApiConfigurationService;

@Component(immediate = true,
        service = IhclcbApiConfigurationService.class)
@Designate(ocd = IhclcbApiConfiguration.class)



public class IhclcbApiConfigurationServiceImpl implements IhclcbApiConfigurationService {
	
	private static final Logger LOG = LoggerFactory.getLogger(IhclcbApiConfigurationServiceImpl.class);

    private IhclcbApiConfiguration ihclcbApiConfiguration;
    
    
    @Reference
    private SlingSettingsService slingSettingsService;


    @Activate
    public void activate(IhclcbApiConfiguration ihclcbApiConfiguration) {

        String methodName = "activate";
        LOG.trace("Method Entry ::: {}", methodName);

        this.ihclcbApiConfiguration = ihclcbApiConfiguration;

        LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.IhclcbApiConfigurationServiceImpl ");
        getWorkingEnvironment();

        LOG.trace("Method Exit ::: {}", methodName);
    }

    private void getWorkingEnvironment() {

        String methodName = "getWorkingEnvironment";
        LOG.trace("Method Entry ::: {}", methodName);
        LOG.trace("Present Run Mode ==> {}", slingSettingsService.getRunModes().toString());

        String env = "";

        if (slingSettingsService.getRunModes().contains("author"))
            env = "LOCAL";
        else if (slingSettingsService.getRunModes().contains("dev"))
            env = "DEV";
        else if (slingSettingsService.getRunModes().contains("stage"))
            env = "STAGING";
       }
    
    @Override
    public String getGuestDetailsFetchApiUrl() {

        return ihclcbApiConfiguration.guestDetailsFetchApiUrl();
    }

    @Override
    public String getEntityDetailsFetchApiUrl() {

        return ihclcbApiConfiguration.entityDetailsFetchApiUrl();
    }
 
    @Override
    public String getIhclcbBookingApiUrl() {

        return ihclcbApiConfiguration.ihclcbBookingApiUrl();
    }
    
    @Override
    public String getIhclcbApiHostUrl() {

        return ihclcbApiConfiguration.ihclcbApiHostUrl();
    }
}
