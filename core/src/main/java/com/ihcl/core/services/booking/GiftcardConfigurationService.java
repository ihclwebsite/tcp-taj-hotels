/**
 *
 */
package com.ihcl.core.services.booking;


/**
 * @author admin
 *
 */
public interface GiftcardConfigurationService {

    String getCCAvenueURL();

    String getCCAvenueWorkingKey();

    String getCCAvenueAccessKey();

    String getCCAvenueRedirectURL();

    String getGiftcardFailedURL();

    String getGiftcardConfirmationURL();

    String getOnlineMerchantId();

}
