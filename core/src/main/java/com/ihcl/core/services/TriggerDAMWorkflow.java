package com.ihcl.core.services;

public interface TriggerDAMWorkflow {

    String startWorkflow(String workflowName, String workflowContent);
}
