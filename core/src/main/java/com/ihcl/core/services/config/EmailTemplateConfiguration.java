package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Email template configuration",
        description = "Configuring email templates")
public @interface EmailTemplateConfiguration {

    // For NewsLetter
    @AttributeDefinition(name = "From address for Newsletter",
            description = "From address for newsletter")
    String emailFromAddressForNewsLetter() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Subject for newsletter response email",
            description = "Subject for newsletter email response")
    String emailSubjectForNewsLetter() default "Newsletter Subscription Completion";

    @AttributeDefinition(name = "Body template for newsletter in IHCL",
            description = "HTML template for newsletter email body in IHCL")
    String emailBodyTemplateForNewsLetterIhcl() default "<html><div>Your request for Newsletter subscription is submitted.</div></html>";

    @AttributeDefinition(name = "Contact number for newsletter in IHCL ",
            description = "Contact number to be displayed in News letter subscription maill")
    String contactNumberForNewsLetter() default "+9102261371710";

    @AttributeDefinition(name = "IHCL URL in newsletter",
            description = "IHCL final url in news letter subscription mail")
    String urlIhclForNewsLetter() default "https://www.theindianhotels.com/";

    @AttributeDefinition(name = "Body template for newsletter in TajHotels",
            description = "HTML template for email body in Taj hotels")
    String emailBodyTemplateForNewsLetterTajHotels() default "<html><div>Your request for Newsletter subscription is submitted through Taj Hotels</div></html>";

    // For JivaSpa booking an appointment request
    @AttributeDefinition(name = "From address for Jiva spa",
            description = "From address for jiva spa")
    String emailFromAddressForJivaSpa() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Subject for jiva spa response email",
            description = "Subject for jiva spa email response")
    String emailSubjectForJivaSpa() default "Your request for jiva spa appointment is submitted";

    @AttributeDefinition(name = "Body template for Jiva spa",
            description = "HTML template for jiva spa email body")
    String emailBodyTemplateForJivaSpa() default "<html><div><h2>Appointment Details</h2></div></div><div> The Appointment details are as below:</div></div></div><ul><li><div>Hotel name : ${hotelName}</div></li><li><div>Spa name : ${jivaSpaName}</div></li><li><div>Spa Duration : ${spaDuration}</div></li><li><div>Spa amount : ${spaAmount}</div></li><li><div>Hotel Email Id : ${hotelEmailId}</div></li><li><div>Spa Booking date : ${bookingDate}</div></li><li><div>Spa Preffered time : ${spaStartTime}</div></li><li><div>City : ${city}</div></li><li><div>Full name : ${name}</div></li><li><div>Gender : ${gender}</div></li><li><div>Phone number : ${phoneNumber}</div></li><li><div>Guest email id : ${guestEmailId}</div></li><li><div>Best time to contact : ${timeToContact}</div></li></ul></html>";

    // For request quote in meetings
    @AttributeDefinition(name = "From address for request quote in meetings",
            description = "From address for request quote in meetings")
    String emailFromAddressForMeetingRequestQuote() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Subject for request quote email in meetings",
            description = "Subject for request quote email in meetings")
    String emailSubjectForMeetingRequestQuote() default "Your request for meeting quote is submitted";

    @AttributeDefinition(name = "Body template for request quote email in meetings",
            description = "HTML template for request quote email body in meetings")
    String emailBodyTemplateForMeetingRequestQuote() default "<html><div>Your request quote for meetings is submitted.</div></html>";

    @AttributeDefinition(name = "Body template for generic request quote email",
            description = "HTML template for generic quote email body")
    String emailBodyTemplateForGenericRequestQuote() default "<html><div><h2>Quotation Details</h2></div><div>Dear Guest, Thank you for your interest in hosting an event with us. We will get back to you with in 48 hours</div><div>The Quotation details are as below:</div><ul><li><div>Location : ${hotelName}</div></li><li><div>Arrival Date : ${bookingArrivalDate}</div></li><li><div>Departure Date : ${bookingDepartureDate}</div></li><li><div>Purpose of the Event : ${purposeOfTheEvent}</div></li><li><div>Special Comments : ${specialRequests}</div></li><li><div>No of Rooms : ${noOfRooms}</div></li><li><div>Number of guests : ${guest}</div></li><li><div>Full Name : ${name}</div></li><li><div>Phone number : ${phoneNumber}</div></li><li><div>Email id : ${guestEmailId}</div></li></ul></html>";

    // For holiday package enquiry
    @AttributeDefinition(name = "From address for Holidays Enquiry",
            description = "From address for Holidays Enquiry")
    String emailFromAddressForHolidaysEnqiry() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Subject for Holidays Enquiry",
            description = "Subject for Holidays Enquiry")
    String emailSubjectForHolidaysEnquiry() default "Enquiry for Holiday Package.";

    @AttributeDefinition(name = "Body template for Holidays Enquiry",
            description = "HTML template for Holidays Enquiry")
    String emailBodyTemplateForHolidaysEnquiry() default "<html>	<div class=\"callout large primary\"><article class=\"grid-container\">"
            + "<div class=\"callout large primary\"><article class=\"grid-container\">"
            + "<div class=\"grid-x\"><div class=\"medium-8 cell\"><h2>Enquiry Details</h2>"
            + "<h3>Package selected for booking</h3><p class=\"subheader\">1. Package : "
            + "${packageName} , ${packageDestination} </p>" + "<p class=\"subheader\">2. Period : ${fromDate} to "
            + "${toDate} </p> <p class=\"subheader\">3. Selected hotels : " + "${selectedHotelsWithCity} </p>"
            + "<p class=\"subheader\">4. Selected location : ${country} ${city}  ${areaCode}"
            + "</p><h3>User Details</h3>" + "<p class=\"subheader\">Name : ${userName} </p>"
            + "<p class=\"subheader\">Email : ${userEmail} </p>" + "<p class=\"subheader\">phone : ${contactNum} </p>"
            + "<p class=\"subheader\">selected for on call response : " + "${oncallFlag} </p>"
            + "</div></div><br></article></div></article></div></html>";

    @AttributeDefinition(name = "Email address for Holidays",
            description = "Email address for Holidays")
    String emailHolidaysEmail() default "holidays@tajhotels.com";

    @AttributeDefinition(name = "Subject for Taj Holidays Enquiry confirmation Email",
            description = "Taj Holidays Enquiry confirmation Email")
    String emailSubjectForEnquiryConfirmation() default "Enquiry submitted successfully";

    @AttributeDefinition(name = "Body template for Enquiry Confirmation",
            description = "HTML template for Enquiry Confirmation")
    String emailBodyTemplateForEnquiryConfirmation() default "<html> <div class=\"callout large primary\">"
            + "<article class=\"grid-container\"><html> <div class=\"callout large primary\">"
            + "<article class=\"grid-container\"><div class=\"callout large primary\">"
            + "<article class=\"grid-container\"><div class=\"grid-x\"><div class=\"medium-8 cell\">"
            + "<h4>Your Enquiry for the ${packageName} package has been submitted successfully. Our team will "
            + "contact you to help you in booking the best package of your choice.</h4></div></div><br></article>"
            + "</div></article></div></html>";

    // For newsletter subscription email contact information in taj hotels
    @AttributeDefinition(name = "Contact number for newsletter in Taj hotels ",
            description = "Contact number to be displayed in News letter subscription mail in taj hotels")
    String contactNumberForNewsLetterTajHotels() default "+91 22 6601 1825";

    @AttributeDefinition(name = "Taj Hotels URL in newsletter",
            description = "Taj Hotels final url in news letter subscription mail")
    String urlForNewsLetterTajHotels() default "https://www.tajhotels.com";

    // For request quote in claim form
    @AttributeDefinition(name = "To address for claim form in champaign",
            description = "To address for claim form")
    String emailToAddressForClaimForm() default "";

    @AttributeDefinition(name = "Subject for claim form in champaign",
            description = "Subject for claim form in champaign")
    String emailSubjectForClaimForm() default "claim form submitted";

    @AttributeDefinition(name = "Body template for claim form",
            description = "Body template for claim form")
    String emailBodyTemplateForClaimForm() default "<html><div>Claim form submitted.</div></html>";

    @AttributeDefinition(name = "To address for safari enquiry form submission",
            description = "To address for safari enquiry form submission")
    String emailToAddressForSafariEnquiry() default "";

    @AttributeDefinition(name = "Email subject for safari enquiry form",
            description = "Email subject for safari enquiry form")
    String emailSubjectForSafariEnquiry() default "Your Safari enquiry form is submitted";

    @AttributeDefinition(name = "Email body tempalte for safari enquiry form",
            description = "Email body tempalte for safari enquiry form")
    String emailBodySafariEnquiry() default "<html><div>Your Safari enquiry form is submitted</div></html>";

    @AttributeDefinition(name = "Body template for Taj Air",
            description = "HTML template for  Taj Air email body")
    String emailBodyTemplateForTajAir() default "<html><div>Your request for Taj Air appointment is submitted.</div></html>";

    @AttributeDefinition(name = "From address for TajAir",
            description = "From address for Taj Air")
    String emailFromAddressForTajAir() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "To address for TajAir",
            description = "To address for Taj Air")
    String emailToAddressForTajAir() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Subject for Taj Air request quote",
            description = "Subject for Taj Air request quote")
    String emailSubjectForTajAir() default "Taj Air   enrollment completed successfully";

    @AttributeDefinition(name = "Email from address from taj air contact us",
            description = "Email from address from taj air contact us")
    String emailFromAddressForTajAirContactUs() default "leadgen@tajhotels.com";

    @AttributeDefinition(name = "Email from address to taj air contact us",
            description = "Email from address to taj air contact us")
    String emailToAddressForTajAirContactUs() default "abhilasha@moonraft.com";

    @AttributeDefinition(name = "Subject for Taj Air request quote",
            description = "Subject for Taj Air request quote")
    String emailSubjectForTajAirContactUs() default "Taj Air - Contact Us";

    @AttributeDefinition(name = "Email body template for taj air contact us",
            description = "Email body template for taj air contact us")
    String emailBodyTemplateForTajAirContactUs() default "<html><div>Your request for Taj Air enquiry is submitted.</div></html>";

    @AttributeDefinition(name = "Email body template for Rooms List segment in Reservation Email",
            description = "Email body template for Rooms List segment in Reservation Email")
    String emailBodyTemplateForReservationEmailRoomsSegment() default "<div class=\"meduim-12 cell\"><div><p>Room ${roomnumber} : Confirmation Number : ${confirmationNumber}</p><p>Number of Guests  : ${adults} Adults, ${children} Children</p><p>Room Type : ${roomType}</p><p>Rate Description : ${rateDesc}</p><p>Rate Applicable : ${rateApplied}</p><p>Taxes : ${currency} ${taxApplied}</p><p>Total Price: ${currency} ${price}</p><p>Cancellation Policy : ${cancelPolicy}</p></div><hr></div>";

    @AttributeDefinition(name = "Email Subject for Reservation Email",
            description = "Email body subject for Reservation Email")
    String emailSubjectForReservationEmail() default "Your Reservation is Confirmed! #";

    @AttributeDefinition(name = "Email Body Template for Reservation Email",
            description = "Email body template for Reservation Email")
    String emailBodyTemplateForReservationEmail() default "<html><div class=\"callout large primary\"><article class=\"grid-container\"><div class=\"callout large primary\"><article class=\"grid-container\"><div class=\"grid-x\"><div class=\"medium-8 cell\"><span><img src=https://www.tajhotels.com/content/dam/tajhotels/icons/style-icons/logo-taj@2x.png alt=\"Brand Logo\"></span><h2 style=\" display: inline; padding-left: 1rem; font-size: 1.6rem;\">Reservation Confirmation</h2><hr><p>Confirmation Number : ${itineraryNumber}</p><p>Hotel name : ${hotelName}</p><p>Guest Name : ${bookedUserFirstName} ${bookedUserLastName}</p><p class=\\\"subheader\\\">Check in date : ${checkinOn}</p><p class=\\\"subheader\\\">Check out date : ${checkoutOn}</p><h4>Thank you for choosing Taj as your next travel destination. You have our commitment to provide a curated stay experience that eliminates distractions and the unnecessary - so you have the time and space you can call your own. The detailed information below confirms your reservation.</h4><h4>${hotelName}, ${hotelAddress}</h4><hr><h3>Reserved Rooms Details</h3><div>${roomsListString}</div><div class=\"medium-8 cell\"><p>Comments : ${specialReqests}</p><h5>Hotel Details : ${hotelName}, ${hotelAddress}, Phone: ${hotelPhone}, Email: ${hotelEmail}. You can also reach us at ${tajSite}</h5><h5>For all reservation related queries, please email us at reservations@tajhotels.com. You can call our Taj Reservation World Wide (24x7) on the following numbers:</h5><h5>India : +91 22 66011825 | 1 800 111 825 (Toll Free)</h5><h5>USA and Canada : 1 866 969 1 825 | UAE : 800 035 702 467 | Other Countries : 00 800 4 588 1 825</h5></div></div><br></article></div></article></div></html>";

    @AttributeDefinition(name = "BCC address for newsletter subscription",
            description = "BCC address for newsletter subscription")
    String bccAddressForNewsletterSubscription() default "";

}
