package com.ihcl.core.services.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface HotelsSearchService {

    Map<String, String> getHotelByDestination(String destinationName);

    List<String> getHotelPathsByDestination(String destinationTag); // taj:destination/india/mumbai

    List<String> getHotelsByCountry(String countryName);

    List<String> getHotelsByType(String hotelType);

    String getHotelPathForName(String hotelName);

    HashMap<String, String> getHotelTypes();

    List<String> getAllHotelResourcePath();

    List<String> getHotelPathsByDestinationPath(String destinationPath);

    List<String> getHotelIdsByDestinationPath(String ourHotelsPath, String destinationTag);

    List<String> getTicHotelPathsByDestinationPath(String destinationPath);
    
    List<String> getRoomRedemptionTicHotelPathsByDestinationPath(String destinationPath);

    List<String> getAllHotels(String hotelSearchPath);

    Set<String> getAllHotelsInsideIndia();

    Set<String> getAllHotelsOutsideIndia();

    com.day.cq.search.result.SearchResult getHotelsByKey(String searchKey);

    List<String> getBrandSpecificHotels(String brandName);

    com.day.cq.search.result.SearchResult getHotelsByTeatments(String homePagePath);

    List<String> getTapHotelPathsByDestinationPath(String destinationPath);

    List<String> getTappmeHotelPathsByDestinationPath(String destinationPath);

    List<String> getTajHolidaysRedemptionHotelsPathsByDestinationPath(String destinationPath);


}
