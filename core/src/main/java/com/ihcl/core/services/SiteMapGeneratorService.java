package com.ihcl.core.services;

import java.util.List;

public interface SiteMapGeneratorService {

    Boolean generateSiteMap(String hostString, String rootPath);

    Boolean generateGenericSiteMap(String hostString);

    Boolean generateSectionalSiteMap(String path, List<String> brandNames, String rootPath);

    Boolean generateImageSiteMap();

    Boolean generateSectionalImageSiteMap(List<String> brandNames, String rootPath);

    Boolean generateGenericImageSiteMap();

}
