package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Room And Suites Point Redemption message configuration",
description = "Configuring room redemption point message")
public @interface PointRedemptionConfiguration {
	
	@AttributeDefinition(name = "Message for Room And Suites Point Redemption",
            description = "Message for Room And Suites Point Redemption")
    String messageForRoomAndSuitesPointRedemption() default "* Points + cash options available during checkout";

}
