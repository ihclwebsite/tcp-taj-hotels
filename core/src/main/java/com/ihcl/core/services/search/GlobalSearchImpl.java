package com.ihcl.core.services.search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.ihcl.core.services.search.SearchServiceConstants.PATH;

@Component(service = GlobalSearchService.class)
public class GlobalSearchImpl implements GlobalSearchService {

	private static final Logger LOG = LoggerFactory.getLogger(GlobalSearchService.class);
	@Reference
	SearchProvider searchProvider;

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	private ResourceResolver resourceResolver;

	@Override
	public Map<String, String> fetchAllCountries() {
		HashMap<String, String> countryMap = new HashMap<String, String>();
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			Resource tagRoot = resourceResolver.getResource(PATH.COUNTRY_TAG_ROOT);
			if(tagRoot != null) {
				Iterable<Resource> childTags = tagRoot.getChildren();
				for (Resource resource : childTags) {
					Tag tag = resource.adaptTo(Tag.class);
					countryMap.put(tag.getTagID(), tag.getTitle());
				}
			}
			
		} catch (LoginException e) {
			LOG.error(
					"Could not invoked search provider service. Please check service user configuration in the instance");
		}
		return countryMap;
	}

	@Override
	public Map<String, String> fetchAllHotelTypes() {
		HashMap<String, String> hotelTypes = new HashMap<String, String>();
		ResourceResolver resourceResolver;
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			Resource tagRoot = resourceResolver.getResource(PATH.HOTEL_TYPE_TAG_ROOT);
			Iterable<Resource> childTags = tagRoot.getChildren();
			for (Resource resource : childTags) {
				Tag tag = resource.adaptTo(Tag.class);
				hotelTypes.put(tag.getTagID(), tag.getTitle());
			}
		} catch (LoginException e) {
			LOG.error(
					"Could not invoked search provider service. Please check service user configuration in the instance");
		}
		return hotelTypes;
	}

	@Override
	public List<String> fetchAllOfferTypes() {
		// TODO Auto-generated method stub
		return null;
	}
}
