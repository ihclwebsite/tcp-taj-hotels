package com.ihcl.core.services.serviceamenity;

import java.util.List;

import com.ihcl.core.models.serviceamenity.ServiceAmenity;

/**
 *
 */
public interface IAllAvailableServiceAmenitiesFetcher {

    List<ServiceAmenity> getServiceAmenities();

}
