package com.ihcl.core.services.config;

public interface EmailTemplateConfigurationService {

    String getEmailFromAddressForNewsLetter();

    String getEmailSubjectForNewsLetter();

    String getEmailBodyTemplateForNewsLetterIhcl();

    String getContactNumberForNewsLetter();

    String getUrlIhclForNewsLetter();

    String getEmailBodyTemplateForNewsLetterTajHotels();

    String getEmailFromAddressForJivaSpa();

    String getEmailSubjectForJivaSpa();

    String getEmailBodyTemplateForJivaSpa();

    String getEmailFromAddressForMeetingRequestQuote();

    String getEmailSubjectForMeetingRequestQuote();

    String getEmailBodyTemplateForMeetingRequestQuote();

    String getEmailBodyTemplateForGenericRequestQuote();

    String getEmailFromAddressForHolidaysEnqiry();

    String getEmailSubjectForHolidaysEnquiry();

    String getEmailBodyTemplateForHolidaysEnquiry();

    String getEmailBodyTemplateForEnquiryConfirmation();

    String getEmailHolidaysEmail();

    String getContactNumberForNewsLetterTajHotels();

    String getUrlForNewsLetterTajHotels();

    String getEmailSubjectForEnquiryConfirmation();

    String getEmailToAddressForClaimForm();

    String getEmailSubjectForClaimForm();

    String getEmailBodyForClaimForm();

    String getEmailToAddressForSafariEnquiry();

    String getEmailSubjectForSafariEnquiry();

    String getEmailBodyForSafariEnquiry();

    String getEmailBodyTemplateForTajAir();

    String getEmailFromAddressForTajAir();

    String getEmailToAddressFotTajAir();

    String getEmailSubjectForTajAir();

    String getEmailToAddressForTajAirContactUs();

    String getEmailSubjectForTajAirContactUs();

    String getEmailFromAddressForTajAirContactUs();

    String getEmailBodyTemplateForTajAirContactUs();

    String getEmailBodyTemplateForReservationEmailRoomsSegment();

    String getEmailSubjectForReservationEmail();

    String getEmailBodyTemplateForReservationEmail();

    String getBccAddressForNewsletterSubscription();
}
