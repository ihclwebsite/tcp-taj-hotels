package com.ihcl.core.services.serviceamenity;

import com.ihcl.core.models.serviceamenity.ServiceAmenity;

import java.util.List;

public interface RatePlanCodeAmenitiesFetcherService {

    List<ServiceAmenity> getMappedAmenitiesFor(String ratePlanCode);

}
