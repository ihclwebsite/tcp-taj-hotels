/**
 *
 */
package com.ihcl.core.services.booking;


/**
 * @author admin
 *
 */
public interface GuaranteeCodesConfigurationService {

    String[] getGuaranteeCodes();

}
