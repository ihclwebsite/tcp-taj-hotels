package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Single Sign On Configuration",
        description = "Single Sign On Configuration")
public @interface SingleSignOnConfiguration {

    @AttributeDefinition(name = "Single Sign On Domains",
            description = "Enter Single Sign On Domains",
            type = AttributeType.STRING)
    String[] getSingleSignOnDomains();

    @AttributeDefinition(name = "Single Sign On Cookie Domain Names",
            description = "Enter Single Sign On Cookie Domain Names",
            type = AttributeType.STRING)
    String[] getSingleSignOnCookieDomainNames();

    @AttributeDefinition(name = "Single Sign On Cookie Path",
            description = "Enter Single Sign On Cookie Path",
            type = AttributeType.STRING)
    String getSingleSignOnCookiePath();

    @AttributeDefinition(name = "Domain Connection timed out",
            description = "Enter Domain Connection timed out",
            type = AttributeType.STRING)
    String getDomainConnectionTimeout() default "0";

    @AttributeDefinition(name = "Single Sign On Cookie Name",
            description = "Enter Single Sign On Cookie Name",
            type = AttributeType.STRING)
    String getSingleSignOnCookieName() default "ihcl-sso-token";

    @AttributeDefinition(name = "Single Sign On Cookie Max Age",
            description = "Enter Single Sign On Cookie Max Age",
            type = AttributeType.STRING)
    String getSingleSignOnCookieMaxAge() default "604800";


}
