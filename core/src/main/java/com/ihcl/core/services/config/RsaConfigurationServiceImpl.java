package com.ihcl.core.services.config;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, service = RsaConfigurationService.class)
@Designate(ocd = RsaServiceConfiguration.class)

public class RsaConfigurationServiceImpl implements RsaConfigurationService{

	private RsaServiceConfiguration config;
	private static final Logger LOG = LoggerFactory.getLogger(RsaConfigurationServiceImpl.class);

	@Activate
	public void activate(RsaServiceConfiguration config) {

		String methodName = "activate";
		LOG.trace("Method Entry ::: " + methodName);

		this.config = config;

		LOG.trace("Properties Fetched from com.ihcl.core.services.booking.impl.ConfigurationServiceImpl ");

		LOG.trace("Method Exit ::: " + methodName);
	}

	
	




	@Override
	public String getPublicKey() {
		// TODO Auto-generated method stub
		return config.publicKey();
	}



	@Override
	public String getPrivateKey() {
		// TODO Auto-generated method stub
		return config.privateKey();
	}
	
}