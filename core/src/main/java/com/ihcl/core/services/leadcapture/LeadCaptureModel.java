package com.ihcl.core.services.leadcapture;


public class LeadCaptureModel {

    private String uniqueIdentifier;

    private String location;

    private String arrivalDate;

    private String departureDate;

    private String budget;

    private String specialRequests;

    private String noOfRooms;

    private String noOfSingleRooms;

    private String noOfDoubleRooms;

    private String noOfTripleRooms;

    private String guestEmailId;

    private String noOfGuests;

    private String fullName;

    private String conatctNumber;

    private String timeOfTheEvent;

    private String purposeOfTheEvent;

    private String timeSubmitted;


    /**
     * Getter for the field location
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }


    /**
     * Setter for the field location
     *
     * @param location
     *            the location to set
     */

    public void setLocation(String location) {
        this.location = location;
    }


    /**
     * Getter for the field arrivalDate
     *
     * @return the arrivalDate
     */
    public String getArrivalDate() {
        return arrivalDate;
    }


    /**
     * Setter for the field arrivalDate
     *
     * @param arrivalDate
     *            the arrivalDate to set
     */

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }


    /**
     * Getter for the field departureDate
     *
     * @return the departureDate
     */
    public String getDepartureDate() {
        return departureDate;
    }


    /**
     * Setter for the field departureDate
     *
     * @param departureDate
     *            the departureDate to set
     */

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }


    /**
     * Getter for the field budget
     *
     * @return the budget
     */
    public String getBudget() {
        return budget;
    }


    /**
     * Setter for the field budget
     *
     * @param budget
     *            the budget to set
     */

    public void setBudget(String budget) {
        this.budget = budget;
    }


    /**
     * Getter for the field specialComments
     *
     * @return the specialComments
     */
    public String getSpecialRequests() {
        return specialRequests;
    }


    /**
     * Setter for the field specialComments
     *
     * @param specialComments
     *            the specialComments to set
     */

    public void setSpecialRequests(String specialRequests) {
        this.specialRequests = specialRequests;
    }


    /**
     * Getter for the field noOfRooms
     *
     * @return the noOfRooms
     */
    public String getNoOfRooms() {
        return noOfRooms;
    }


    /**
     * Setter for the field noOfRooms
     *
     * @param noOfRooms
     *            the noOfRooms to set
     */

    public void setNoOfRooms(String noOfRooms) {
        this.noOfRooms = noOfRooms;
    }


    /**
     * Getter for the field noOfSingleRooms
     *
     * @return the noOfSingleRooms
     */
    public String getNoOfSingleRooms() {
        return noOfSingleRooms;
    }


    /**
     * Setter for the field noOfSingleRooms
     *
     * @param noOfSingleRooms
     *            the noOfSingleRooms to set
     */

    public void setNoOfSingleRooms(String noOfSingleRooms) {
        this.noOfSingleRooms = noOfSingleRooms;
    }


    /**
     * Getter for the field noOfDoubleRooms
     *
     * @return the noOfDoubleRooms
     */
    public String getNoOfDoubleRooms() {
        return noOfDoubleRooms;
    }


    /**
     * Setter for the field noOfDoubleRooms
     *
     * @param noOfDoubleRooms
     *            the noOfDoubleRooms to set
     */

    public void setNoOfDoubleRooms(String noOfDoubleRooms) {
        this.noOfDoubleRooms = noOfDoubleRooms;
    }


    /**
     * Getter for the field noOfTripleRooms
     *
     * @return the noOfTripleRooms
     */
    public String getNoOfTripleRooms() {
        return noOfTripleRooms;
    }


    /**
     * Setter for the field noOfTripleRooms
     *
     * @param noOfTripleRooms
     *            the noOfTripleRooms to set
     */

    public void setNoOfTripleRooms(String noOfTripleRooms) {
        this.noOfTripleRooms = noOfTripleRooms;
    }


    /**
     * Getter for the field guestEmailId
     *
     * @return the guestEmailId
     */
    public String getGuestEmailId() {
        return guestEmailId;
    }


    /**
     * Setter for the field guestEmailId
     *
     * @param guestEmailId
     *            the guestEmailId to set
     */

    public void setGuestEmailId(String guestEmailId) {
        this.guestEmailId = guestEmailId;
    }


    /**
     * Getter for the field noOfGuests
     *
     * @return the noOfGuests
     */
    public String getNoOfGuests() {
        return noOfGuests;
    }


    /**
     * Setter for the field noOfGuests
     *
     * @param noOfGuests
     *            the noOfGuests to set
     */

    public void setNoOfGuests(String noOfGuests) {
        this.noOfGuests = noOfGuests;
    }


    /**
     * Getter for the field fullName
     *
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }


    /**
     * Setter for the field fullName
     *
     * @param fullName
     *            the fullName to set
     */

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    /**
     * Getter for the field conatctNumber
     *
     * @return the conatctNumber
     */
    public String getConatctNumber() {
        return conatctNumber;
    }


    /**
     * Setter for the field conatctNumber
     *
     * @param conatctNumber
     *            the conatctNumber to set
     */

    public void setConatctNumber(String conatctNumber) {
        this.conatctNumber = conatctNumber;
    }


    /**
     * Getter for the field timeOfTheEvent
     *
     * @return the timeOfTheEvent
     */
    public String getTimeOfTheEvent() {
        return timeOfTheEvent;
    }


    /**
     * Setter for the field timeOfTheEvent
     *
     * @param timeOfTheEvent
     *            the timeOfTheEvent to set
     */

    public void setTimeOfTheEvent(String timeOfTheEvent) {
        this.timeOfTheEvent = timeOfTheEvent;
    }


    /**
     * Getter for the field purposeOfTheEvent
     *
     * @return the purposeOfTheEvent
     */
    public String getPurposeOfTheEvent() {
        return purposeOfTheEvent;
    }


    /**
     * Setter for the field purposeOfTheEvent
     *
     * @param purposeOfTheEvent
     *            the purposeOfTheEvent to set
     */

    public void setPurposeOfTheEvent(String purposeOfTheEvent) {
        this.purposeOfTheEvent = purposeOfTheEvent;
    }


    /**
     * Getter for the field timeSubmitted
     *
     * @return the timeSubmitted
     */
    public String getTimeSubmitted() {
        return timeSubmitted;
    }


    /**
     * Setter for the field timeSubmitted
     *
     * @param timeSubmitted
     *            the timeSubmitted to set
     */

    public void setTimeSubmitted(String timeSubmitted) {
        this.timeSubmitted = timeSubmitted;
    }


    /**
     * Getter for the field uniqueIdentifier
     *
     * @return the uniqueIdentifier
     */
    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }


    /**
     * Setter for the field uniqueIdentifier
     *
     * @param uniqueIdentifier
     *            the uniqueIdentifier to set
     */

    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }


}
