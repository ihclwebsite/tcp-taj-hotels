package com.ihcl.core.services.serviceamenity;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Available Service Amenity Fetcher Configuration",
        description = "The configuration for the hello world component.")
public @interface AvailableServiceAmenityPathFetcherConfig {

    @AttributeDefinition(name = "Service Amenity Page Path",
            description = "Specify the page path in which available service amenity components are added.")
    String getAvailableServiceAmenitiesPagePath() default "/content/shared-content/global-shared-content/shared-content/available-service-amenities";
}
