package com.ihcl.core.services.booking;

public interface IhclcbApiConfigurationService {

    String getIhclcbApiHostUrl();

    String getEntityDetailsFetchApiUrl();

    String getGuestDetailsFetchApiUrl();

    String getIhclcbBookingApiUrl();


}
