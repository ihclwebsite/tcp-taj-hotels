package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.FlushCacheDispatcherConfiguration;
import com.ihcl.core.services.config.FlushCacheDispatcherConfigurationService;

@Component(immediate = true,
        service = FlushCacheDispatcherConfigurationService.class)
@Designate(ocd = FlushCacheDispatcherConfiguration.class)
public class FlushCacheDispatcherConfigurationServiceImpl implements FlushCacheDispatcherConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(FlushCacheDispatcherConfigurationServiceImpl.class);

    private FlushCacheDispatcherConfiguration config;

    @Reference
    private SlingSettingsService settings;

    @Activate
    public void activate(FlushCacheDispatcherConfiguration config) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> " + settings.getRunModes());
        this.config = config;

        LOG.trace("Method Exit ::: activate()");
    }

    @Override
    public String[] getDispatcherIps() {
        return config.dispatcherIps();
    }

    @Override
    public String getProtocol() {
        return config.protocol();
    }

    @Override
    public String getDispatcherPort() {
        return config.dispatcherPort();
    }

    @Override
    public String getRateCachePath() {
        return config.rateCachePath();
    }

    @Override
    public String getDestinationRatesCachePath() {
        return config.destinationRatesCachePath();
    }

    @Override
    public String[] getFlushHosts() {
        return config.flushHosts();
    }


    @Override
    public String getRateRefreshServletUrl() {
        return config.rateRefreshServletUrl();
    }

}
