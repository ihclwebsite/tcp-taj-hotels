package com.ihcl.core.services.impl;

import com.ihcl.core.services.SpellCheck;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;
import java.util.ArrayList;
import java.util.List;

@Component(service = SpellCheck.class)
public class SpellCheckImpl implements SpellCheck {

    private static final Logger LOG = LoggerFactory.getLogger(SpellCheckImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public List<String> performSpellCheck(String text) {
        List<String> spellchecks = new ArrayList<>();
        try {
            ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager qm = session.getWorkspace().getQueryManager();
            String xpath = "/jcr:root/content/tajhotels//*[rep:spellcheck('" + text + "')]/(rep:spellcheck())";
            QueryResult result = qm.createQuery(xpath, Query.XPATH).execute();
            RowIterator it = result.getRows();
            while (it.hasNext()) {
                Row row = it.nextRow();
                spellchecks.add(row.getValue("rep:spellcheck()").getString());
            }
        } catch (Exception e) {
            LOG.error("Exception while performing spell check, text=", text, e);
        }
        return spellchecks;
    }
}
