/**
 *
 */
package com.ihcl.core.services.booking.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.SingleSignOnConfiguration;
import com.ihcl.core.services.booking.SingleSignOnConfigurationService;

@Component(immediate = true,
        service = SingleSignOnConfigurationService.class)
@Designate(ocd = SingleSignOnConfiguration.class)
public class SingleSignOnConfigurationServiceImpl implements SingleSignOnConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(SingleSignOnConfigurationServiceImpl.class);

    private SingleSignOnConfiguration singleSignOnConfiguration;

    @Reference
    private SlingSettingsService slingSettingsService;


    @Activate
    public void activate(SingleSignOnConfiguration singleSignOnConfiguration) {

        String methodName = "activate";
        LOG.trace("Method Entry ::: {}", methodName);

        this.singleSignOnConfiguration = singleSignOnConfiguration;

        LOG.trace("Method Exit ::: {}", methodName);
    }

    @Override
    public String[] getSingleSignOnDomains() {
        return singleSignOnConfiguration.getSingleSignOnDomains();
    }

    @Override
    public String[] getSingleSignOnCookieDomainNames() {
        return singleSignOnConfiguration.getSingleSignOnCookieDomainNames();
    }

    @Override
    public String getSingleSignOnCookiePath() {
        return singleSignOnConfiguration.getSingleSignOnCookiePath();
    }

    @Override
    public String getDomainConnectionTimeout() {
        return singleSignOnConfiguration.getDomainConnectionTimeout();
    }

    @Override
    public String getSingleSignOnCookieName() {
        return singleSignOnConfiguration.getSingleSignOnCookieName();
    }

    @Override
    public String getSingleSignOnCookieMaxAge() {
        return singleSignOnConfiguration.getSingleSignOnCookieMaxAge();
    }

}
