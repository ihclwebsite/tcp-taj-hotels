package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Giftcard Configuration",
        description = "Giftcard Configuration")
public @interface GiftcardConfiguration {

    @AttributeDefinition(name = "Online Payment Merchant ID",
            description = "Enter the Online Payment Merchant ID",
            type = AttributeType.STRING)
    String paymentMerchantID() default "4";

    @AttributeDefinition(name = "CCAvenue URL",
            description = "Enter the CCAvenue URL",
            type = AttributeType.STRING)
    String ccAvenueURL() default "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction";

    @AttributeDefinition(name = "CCAvenue Access Key",
            description = "Enter the CCAvenue Access Key",
            type = AttributeType.STRING)
    String ccAvenueAccessKey() default "AVQF01FG85AS47FQSA";

    @AttributeDefinition(name = "CCAvenue Working Key",
            description = "Enter the CCAvenue Working Key ",
            type = AttributeType.STRING)
    String ccAvenueWorkingKey() default "E0AF518195DD811AE2526EFF2571F380";

    @AttributeDefinition(name = "CCAvenue Re-direct URL",
            description = "Enter the CCAvenue Re-direct URL ",
            type = AttributeType.STRING)
    String ccAvenueReDirectURL() default "http://localhost:4502/bin/giftcardPostPaymentServlet";

    @AttributeDefinition(name = "Giftcard Cancel/Failure URL",
            description = "Enter Giftcard Cancel/Failure URL ",
            type = AttributeType.STRING)
    String giftcardFailedURL() default "http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html";

    @AttributeDefinition(name = "Giftcard Confirmation URL",
            description = "Enter Giftcard Confirmation Page URL",
            type = AttributeType.STRING)
    String giftcardConfirmationURL() default "http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html";

}
