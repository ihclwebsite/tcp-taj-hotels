package com.ihcl.core.services.booking;

public interface ConfigurationService {

    String getCCAvenueURL();

    String getCCAvenueWorkingKey();

    String getCCAvenueAccessKey();

    String getCCAvenueRedirectURL();

    String getBookingFailedURL();

    String getBookingConfirmationURL();

    String getOnlineMerchantId();
	
	

    String getEpicurePaymentRedirectUrl();

    String getEpicureEnrollRedirectUrl();

    String getPurchasePaymentRedirectUrl();

    String getPurchasePaymentSuccess();

    String getepicureEnrollAmount();

    String getPurchaseMerchantId();

    String getPurchaseCCAvenueWorkingKey();

    String getPurchaseCCAvenueAccessKey();

    String getCamapignEpicureAmount();



}
