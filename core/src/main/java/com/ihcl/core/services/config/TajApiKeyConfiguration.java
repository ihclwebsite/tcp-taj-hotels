/**
 *
 */
package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Srikanta.moonraft
 *
 */
@ObjectClassDefinition(name = "Taj API Key & Admin MailID Configuration", description = "All the 3rd party API keys & Admin EmailID can be configured here")
public @interface TajApiKeyConfiguration {

	@AttributeDefinition(name = "Open Weather API key", description = "Please add the api key from [https://home.openweathermap.org/api_keys] ")
	String openWeatherMapApiKey() default "52b091023d529bf406ae711fb80733f1";

	@AttributeDefinition(name = "Google Map API key", description = "Please add the api key from [https://developers.google.com/maps/]")
	String googleMapApiKey() default "AIzaSyB7HtopLBECeEWREu2jwu80j0lSydqsCDI";

	@AttributeDefinition(name = "Hotel chain code", description = "Please add the hotel chain code")
	String hotelChainCode() default "21305";

	@AttributeDefinition(name = "Taj Admin Email-Id for newsletter subscription", description = "Please add the Admin Email ID ")
	String tajAdminEmailId() default "shwetha@moonraft.com";

	@AttributeDefinition(name = "IHCL Developement Admin Email-Id", description = "Please add the Admin Email ID ")
	String developmentAdminEmailId() default "development@tajhotels.com";

	@AttributeDefinition(name = "IHCL Write To Us Admin Email-Id", description = "Please add the Admin Email ID ")
	String IHCLwrtieToUsAdminEmailId() default "shwetha@moonraft.com";

	@AttributeDefinition(name = "/etc/map Location", description = "etc/map location of different environment can be accessed through this key")
	String etcMapLocation() default "/etc/map.publish.prod/https/beta.tajhotels.com/en-in";

	@AttributeDefinition(name = "urls to be modified", description = "enter urls to be redirected to the main page if it does not exist")
	String[] urlsToBeRedirected() default { "(.*)(/offers/)([A-Za-z0-9/]+)",
			"(.*)(/offers-and-promotions/)([A-Za-z0-9/]+)" };

	@AttributeDefinition(name = "Ihcl Owner Username", description = "enter Ihcl Owner Username")
	String ihclOwnerUsername() default "ihclprivilegesplus@tajhotels.com";

	@AttributeDefinition(name = "Ihcl Owner Password", description = "enter Ihcl Owner Password")
	String ihclOwnerPassword() default "Taj@123";

	@AttributeDefinition(name = "Yellow Messenger ChatBot ID", description = "Yellow Messenger ChatBot ID")
	String getChatBotId() default "x1594291984708";

}
