/**
 *
 */
package com.ihcl.core.services;


/**
 * @author moonraft
 *
 */
public interface CurrencyFetcherService {

    String getCurrencySymbolValue(String currencyValue);

}
