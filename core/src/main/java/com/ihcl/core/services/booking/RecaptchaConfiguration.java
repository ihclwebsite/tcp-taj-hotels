package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Recaptcha Service Configuration", description = "Service Configuration")
public @interface RecaptchaConfiguration {
    @AttributeDefinition(name = "Online Payment Merchant ID", description = "Enter the Online Payment Merchant ID", type=AttributeType.STRING)
//    String sitekey() default "6Ldyg3MUAAAAACV1dWM6Sd7VTr2Y_itn7a6W9RX3";
    String sitekey() default "6LfODuoUAAAAAJt3mzcgzQkcPB4Z_VeVlsUXh63t";

    @AttributeDefinition(name = "Online Payment Merchant ID", description = "Enter the Online Payment Merchant ID", type=AttributeType.STRING)
//    String secretkey() default "6Ldyg3MUAAAAAER60qd2hquq34twwB10KUkKYJkB";
    String secretkey() default "6LfODuoUAAAAACZePh9vSz_uuhqYWEBDWhJHHBHe";
}
