package com.ihcl.core.services.leadcapture;


public interface LeadCaptureConfigurationService {

    String getLeadCaptureUserDataNodePath();

    String getAdminMailId();

    String getAdminMailIdCC();

}
