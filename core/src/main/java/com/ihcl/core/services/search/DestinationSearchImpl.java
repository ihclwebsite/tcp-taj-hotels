package com.ihcl.core.services.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.models.Destination;
import com.ihcl.core.models.search.SearchResult;
import com.ihcl.tajhotels.constants.SearchConstants;

/**
 * The service provides a query builder based integration of search services related to Destinations
 */

@Component(service = DestinationSearchService.class)
public class DestinationSearchImpl implements DestinationSearchService {

    private static final Logger LOG = LoggerFactory.getLogger(DestinationSearchImpl.class);

    private static final String COUNTRY_TYPE_ALL = "all";

    private static final String COUNTRY_TYPE_INDIA = "India";

    private static final String COUNTRY_TYPE_EXCEPT_INDIA = "except_india";

    private static final String DESTINATIONS_LENGTH = "8";

    @Reference
    SearchProvider searchProvider;

    @Reference
    ResourceResolverFactory resourceResolverFactory;


    /*
     * Method to fetch all the destinations available across all the countries
     */
    @Override
    public List<String> getAllDestinations(String hotelRootPath) {
        List<String> destinations = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put(SearchConstants.PATH,
                    hotelRootPath + SearchServiceConstants.PATH.EN + SearchServiceConstants.PATH.OUR_HOTELS);
            predicateMap.put("path.flat", "true");
            predicateMap.put(SearchConstants.PROPERTY, "jcr:content/sling:resourceType");
            predicateMap.put(SearchConstants.PROPERTY_VALUE, SearchServiceConstants.RESOURCETYPE.DESTINATION);
            predicateMap.put(SearchConstants.P_LIMIT, "-1");
            predicateMap.put(SearchConstants.ORDER_BY, "@jcr:content/jcr:title");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                destinations.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all destinations :: {}", e.getMessage());
            LOG.debug("Error while fetching all destinations :: {}", e);
        }
        return destinations;
    }

    @Override
    public List<Destination> getAllDestinationModels(String destinationPath) {
        List<String> destinationPaths = null;
        Resource destResource = null;
        Destination destinationModel = null;
        List<Destination> destinationsList = new ArrayList<>();
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            destinationPaths = getAllDestinations(destinationPath);

            for (String rPath : destinationPaths) {
                destResource = resourceResolver.getResource(rPath);
                LOG.trace("destination Res ValueMap : {}", destResource);
                destinationModel = destResource.getChild("jcr:content").adaptTo(Destination.class);
                destinationsList.add(destinationModel);
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all destinationModels :: {}", e.getMessage());
            LOG.debug("Error while fetching all destinationModels :: {}", e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return destinationsList;
    }

    @Override
    public Map<String, List<Destination>> getDestinationModelMap(String destinationPath) {
        Map<String, List<Destination>> countryMap = new HashMap<>();
        List<Destination> indianDestinationsList = new ArrayList<>();
        List<Destination> nonIndianDestinationsList = new ArrayList<>();

        List<Destination> allDestinationsList = getAllDestinationModels(destinationPath);
        countryMap.put(COUNTRY_TYPE_ALL, allDestinationsList);
        try {
            for (Destination child : allDestinationsList) {
                if (child.getCountry().equals(COUNTRY_TYPE_INDIA)) {
                    indianDestinationsList.add(child);
                } else {
                    nonIndianDestinationsList.add(child);
                }
            }
            countryMap.put(COUNTRY_TYPE_INDIA, indianDestinationsList);
            countryMap.put(COUNTRY_TYPE_EXCEPT_INDIA, nonIndianDestinationsList);
            LOG.trace("built the country wise destinatio Map and returning it");
        } catch (Exception e) {
            LOG.debug("Error while fetching all destinationModels :: {}", e.getMessage());
            LOG.error("Error while fetching all destinationModels :: {}", e);
        }
        return countryMap;
    }

    @Override
    public HashMap<String, String> getDestinationByCountry(String countryTagId) {
        HashMap<String, String> destinationMap = new HashMap<>();
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            Tag tagRoot = tagManager.resolve(countryTagId);
            Resource tagRootRes = tagRoot.adaptTo(Resource.class);
            Iterable<Resource> childTags = tagRootRes.getChildren();
            for (Resource resource : childTags) {
                Tag tag = resource.adaptTo(Tag.class);
                destinationMap.put(tag.getTagID(), tag.getTitle());
            }

        } catch (LoginException e) {
            LOG.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e.getMessage());
            LOG.debug(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}",
                    e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
        return destinationMap;
    }


    @Override
    public List<String> getDestinationsByRank() {
        List<String> destinationList = new ArrayList<>();
		try {
			HashMap<String, String> predicates = new HashMap<>();
			predicates.put(SearchConstants.PATH, SearchServiceConstants.PATH.HOTELROOT);
			predicates.put("path.flat", "true");
			predicates.put(SearchConstants.PROPERTY, "jcr:content/sling:resourceType");
			predicates.put(SearchConstants.PROPERTY_VALUE, SearchServiceConstants.RESOURCETYPE.DESTINATION);
			predicates.put(SearchConstants.P_LIMIT, DESTINATIONS_LENGTH);
			predicates.put(SearchConstants.ORDER_BY, "@jcr:content/rank");
			predicates.put(SearchConstants.ORDER_BY_SORT, "desc");
			List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
			for (SearchResult searchResult : searchResults) {
				destinationList.add(searchResult.getPath());
			}
			LOG.trace("destinationList-> {}", destinationList);
		} catch (Exception e) {
        	LOG.error("Exception found in getDestinationsByRank :: {} ", e.getMessage());
        }
        return destinationList;
    }

    /**
     * Fetch all holiday destination Paths
     *
     * @return: String list
     */
    @Override
    public List<String> getAllHolidayDestinations() {

        List<String> destinations = new ArrayList<>();
        String destinationRoot = "/content/tajhotels/en-in/taj-holidays/destinations/";
        String searchKeyValue = "aboutDestination";
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put(SearchConstants.PATH, destinationRoot);
            predicateMap.put(SearchConstants.PROPERTY, "searchKey");
            predicateMap.put(SearchConstants.PROPERTY_VALUE, searchKeyValue);
            predicateMap.put(SearchConstants.P_LIMIT, "-1");
            predicateMap.put(SearchConstants.ORDER_BY, "@destinationName");
            predicateMap.put(SearchConstants.ORDER_BY_SORT, "asc");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                destinations.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all destinations :: {}", e.getMessage());
            LOG.debug("Error while fetching all destinations :: {}", e);
        }
        return destinations;
    }

    /**
     * Fetch all holiday hotels path.
     *
     * @return String list
     */
    @Override
    public List<String> getAllHolidayHotels() {

        List<String> hotels = new ArrayList<>();
        String destinationRoot = "/content/tajhotels/en-in/taj-holidays/destinations/";
        String searchKeyValue = "holidayHotel";
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put(SearchConstants.PATH, destinationRoot);
            predicateMap.put(SearchConstants.PROPERTY, "searchKey");
            predicateMap.put(SearchConstants.PROPERTY_VALUE, searchKeyValue);
            predicateMap.put(SearchConstants.P_LIMIT, "-1");
            predicateMap.put(SearchConstants.ORDER_BY, "@jcr:title");
            predicateMap.put(SearchConstants.ORDER_BY_SORT, "asc");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                hotels.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all hotels :: {}", e.getMessage());
            LOG.debug("Error while fetching all hotels :: {}", e);
        }
        return hotels;
    }

    /**
     * Fetch all holiday offers pages paths
     *
     * @return String list
     */
    @Override
    public List<String> getAllHolidayHotelOffers(String hotelPath) {
        List<String> offers = new ArrayList<>();
        String searchKeyValue = "holidayPackages";
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put(SearchConstants.PATH, hotelPath);
            predicateMap.put(SearchConstants.PROPERTY, "searchKey");
            predicateMap.put(SearchConstants.PROPERTY_VALUE, searchKeyValue);
            predicateMap.put(SearchConstants.P_LIMIT, "-1");
            predicateMap.put(SearchConstants.ORDER_BY, "@jcr:title");
            predicateMap.put(SearchConstants.ORDER_BY_SORT, "asc");
            List<SearchResult> searchResults = searchProvider.executeQuery(predicateMap);
            for (SearchResult searchResult : searchResults) {
                offers.add(searchResult.getPath());
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all offers :: {}", e.getMessage());
            LOG.debug("Error while fetching all offers :: {}", e);
        }
        return offers;
    }


    @Override
    public List<String> getDestinationByType(String destinationType) {
        return null;
    }

    @Override
    public List<String> getDestinationForHotels(List<String> hotelNames) {
        return null;
    }

    @Override
    public List<String> getDestinationForHotel(String hotelName) {
        return null;
    }

}
