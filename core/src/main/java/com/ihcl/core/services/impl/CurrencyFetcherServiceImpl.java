/**
 *
 */
package com.ihcl.core.services.impl;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.CurrencyFetcherService;

@Component(immediate = true,
        service = CurrencyFetcherService.class)
public class CurrencyFetcherServiceImpl implements CurrencyFetcherService {

    private final Logger LOG = LoggerFactory.getLogger(CurrencyFetcherServiceImpl.class);

    @Override
    public String getCurrencySymbolValue(String currencyValue) {
        String methodName = "getCurrencySymbolValue";
        LOG.trace("Method Entry: " + methodName);
        String currencySymbol = null;
        try {
            if (currencyValue.toLowerCase().equals("usd")) {
                String symbolValue = "\u0024";
                currencySymbol = getSymbolValue(symbolValue);
            } else if (currencyValue.toLowerCase().equals("gdp")) {
                String symbolValue = "\u00A3";
                currencySymbol = getSymbolValue(symbolValue);
            } else if (currencyValue.toLowerCase().equals("inr")) {
                String symbolValue = "\u20B9";
                currencySymbol = getSymbolValue(symbolValue);
            } else if (currencyValue.toLowerCase().equals("myr")) {
                currencySymbol = "MYR ";
            } else if (currencyValue.equalsIgnoreCase("aed")) {
                currencySymbol = "AED";
            } else if (currencyValue.equalsIgnoreCase("lrk")) {
                currencySymbol = "LRK";
            } else {
                currencySymbol = currencyValue;
            }
        } catch (Exception e) {
            LOG.error("Error/Exception is thrown while fetching  currency value :" + e.toString(), e);
        }
        LOG.trace("Received Currency Symbol as:" + currencySymbol);
        LOG.trace("Method Exit: " + methodName);
        return currencySymbol;
    }

    /**
     * @param symbolValue
     */
    private String getSymbolValue(String symbolValue) throws UnsupportedEncodingException {
        String methodName = "getSymbolValue";
        LOG.trace("Method Entry: " + methodName);
        byte[] utf8 = symbolValue.getBytes(StandardCharsets.UTF_8);
        symbolValue = new String(utf8, StandardCharsets.UTF_8);
        LOG.trace("Method Exit: " + methodName);
        return symbolValue;
    }


}
