/**
 *
 */
package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Vijay Chikkani
 *
 */
@ObjectClassDefinition(name = "Guarantee Codes Configuration",
        description = "Guarantee Codes Configuration")
public @interface GuaranteeCodesConfiguration {

    @AttributeDefinition(name = "Guarantee Codes Configuration",
            description = "These Guarantee policy codes are not required credit card detail.",
            type = AttributeType.STRING)
    String[] getGuaranteeCodes();
}
