package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Taj Flush Cache Dispatcher Service Configuration",
        description = "Configuring Dispatchers")
public @interface FlushCacheDispatcherConfiguration {

    @AttributeDefinition(name = "dispatcherIps",
            description = "Ips of dispatcher")
    String[] dispatcherIps() default "[54.169.9.103]";

    @AttributeDefinition(name = "dispacherPort",
            description = "Dispatcher Port")
    String dispatcherPort() default "80";

    @AttributeDefinition(name = "protocol",
            description = "Protocol")
    String protocol() default "http";

    @AttributeDefinition(name = "Rate Cache Path",
            description = "Path to rate cache")
    String rateCachePath() default "/bin/fetch/rooms-availability.json/";

    @AttributeDefinition(name = "Destination Rate Cache Path",
            description = "Path to destination rate cache")
    String destinationRatesCachePath() default "/bin/hotel-rates/destination-hotel-rates";

    @AttributeDefinition(name = "Rate Cache Path",
            description = "Path to rate cache")
    String[] flushHosts() default "[www.tajhotels.com]";

    @AttributeDefinition(name = "Rate Refresh Servlet URL",
            description = "Rate Refresh Servlet URL which would be invoked if price disparity exists in Boking Cart Page")
    String rateRefreshServletUrl() default "https://author-taj-stage63.adobecqms.net/bin/flushcache";
}
