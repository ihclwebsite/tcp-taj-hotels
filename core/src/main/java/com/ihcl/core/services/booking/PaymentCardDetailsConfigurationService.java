package com.ihcl.core.services.booking;

public interface PaymentCardDetailsConfigurationService {

    String getCardNumber();

    String getCardName();

    String getExpiryDate();

    String getCardType();
}
