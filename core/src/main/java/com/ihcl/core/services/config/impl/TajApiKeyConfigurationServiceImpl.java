/**
 *
 */
package com.ihcl.core.services.config.impl;

import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfiguration;
import com.ihcl.core.services.config.TajApiKeyConfigurationService;

/**
 * @author Srikanta.moonraft
 *
 */
@Component(immediate = true,
        service = TajApiKeyConfigurationService.class)
@Designate(ocd = TajApiKeyConfiguration.class)
public class TajApiKeyConfigurationServiceImpl implements TajApiKeyConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(TajApiKeyConfigurationServiceImpl.class);

    private TajApiKeyConfiguration globalConfig;

    @Reference
    private SlingSettingsService settings;


    @Activate
    public void activate(TajApiKeyConfiguration config) {

        LOG.trace("Method Entry ::: activate()");
        LOG.trace("present ENV. ==> " + settings.getRunModes());
        this.globalConfig = config;

        LOG.trace("Method Exit ::: activate()");
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.GlobalConfigurationService#getOpenWeatherMapApiKey()
     */
    @Override
    public String getOpenWeatherMapApiKey() {
        // TODO Auto-generated method stub
        return globalConfig.openWeatherMapApiKey();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.GlobalConfigurationService#getGoogleMapApiKey()
     */
    @Override
    public String getGoogleMapApiKey() {
        return globalConfig.googleMapApiKey();
    }

    @Override
    public String getTajAdminEmailId() {
        return globalConfig.tajAdminEmailId();
    }


    @Override
    public String getHotelChainCode() {
        return globalConfig.hotelChainCode();
    }


    @Override
    public String getDevelopmentAdminEmailId() {
        return globalConfig.developmentAdminEmailId();
    }


    @Override
    public String getIHCLWriteToUsAdminEmailId() {
        return globalConfig.IHCLwrtieToUsAdminEmailId();
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.TajApiKeyConfigurationService#getEtcMapLocation()
     */
    @Override
    public String getEtcMapLocation() {

        return globalConfig.etcMapLocation();
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.TajApiKeyConfigurationService#getUrlsToBeRedirected()
     */
    @Override
    public String[] getUrlsToBeRedirected() {
        // TODO Auto-generated method stub
        return globalConfig.urlsToBeRedirected();
    }


	@Override
	public String getIhclOwnerUsername() {
		// TODO Auto-generated method stub
		return globalConfig.ihclOwnerUsername();
	}


	@Override
	public String getIhclOwnerPassword() {
		// TODO Auto-generated method stub
		return globalConfig.ihclOwnerPassword();
	}


	@Override
	public String getChatBotId() {
		// TODO Auto-generated method stub
		return globalConfig.getChatBotId();
	}


}
