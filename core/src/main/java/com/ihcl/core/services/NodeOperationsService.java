package com.ihcl.core.services;

import java.util.List;

import javax.jcr.Node;

public interface NodeOperationsService {

	Node copyProperties(Node sourceNode, Node targetNode, List<String> ignoreProperties);

	boolean replaceNode(Node sourceNode, Node replaceNode);
}
