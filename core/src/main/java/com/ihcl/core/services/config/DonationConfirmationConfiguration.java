/**
 * 
 */
package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author TCS
 *
 */
@ObjectClassDefinition(	name = "Taj Donation Service Configuration",
						description = "Configuring Donation Details")
public @interface DonationConfirmationConfiguration {

    @AttributeDefinition(name = "Customer Email Subject",
            description = "Customer Email Subject")
    String customerEmailSubject() default "Donation Details";

    @AttributeDefinition(name = "Donation Customer Email Body",
            description = "Donation Email Body")
    String customerEmailBodyTemplate() default "GIFTHAMPERCODE:- $GIFTHAMPERCODE$, SENDERAMOUNT:- $SENDERAMOUNT$ || CCAVENUETRACKINGID:- $CCAVENUETRACKINGID$ || SENDERNAME:- $SENDERNAME$";

    @AttributeDefinition(name = "Donation Confirmation Page Path",
            description = "URL page path to which the user has to be redirected after payment success")
    String getConfirmationPagePath() default "/IHCL-donation-success";

    @AttributeDefinition(name = "Donation Failure Page Path",
            description = "URL page path to which the user has to be redirected after payment failure")
    String getFailurePagePath() default "/IHCL-donation-error";


}
