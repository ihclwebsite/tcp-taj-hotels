/**
 *
 */
package com.ihcl.core.services.booking;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Vijay Chikkani
 *
 */
@ObjectClassDefinition(name = "Synixs Downtime Configuration",
        description = "Synixs Downtime Configuration")
public @interface SynixsDowntimeConfiguration {

    @AttributeDefinition(name = "Forcing to skip synixs call",
            description = "If it enable forcefully to skip synixs call",
            type = AttributeType.BOOLEAN)
    boolean forcingToSkipCalls() default false;

    @AttributeDefinition(name = "Downtime starts from",
            description = "Downtime starts from (yyyy-MM-dd'T'HH:mm:ss Z)",
            type = AttributeType.STRING)
    String downtimeStarts() default "";

    @AttributeDefinition(name = "Downtime Ends at",
            description = "Downtime ends at (yyyy-MM-dd'T'HH:mm:ss Z)",
            type = AttributeType.STRING)
    String downtimeEnds() default "";

    @AttributeDefinition(name = "Downtime servlet status code",
            description = "Downtime servlet status code (Must be an Integer)",
            type = AttributeType.STRING)
    String servletStatusCode() default "500";

    @AttributeDefinition(name = "Downtime Popup Message",
            description = "Downtime Popup Message",
            type = AttributeType.STRING)
    String downtimeMessage() default "Synixs under downtime. Please try after some time.";

    @AttributeDefinition(name = "Redirect Page Path",
            description = "Content path of the page to which browser needs to be redirected in case of error",
            type = AttributeType.STRING)
    String getRedirectPagePath() default "/content/tajhotels/en-in/synxis-downtime";
}
