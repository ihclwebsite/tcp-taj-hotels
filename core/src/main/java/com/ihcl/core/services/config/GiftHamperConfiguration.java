/**
 *
 */
package com.ihcl.core.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author Vijay Chikkani
 *
 */
@ObjectClassDefinition(name = "Taj Gift Hamper Service Configuration",
        description = "Configuring Gift Hamper Details")
public @interface GiftHamperConfiguration {

    @AttributeDefinition(name = "Customer Email Subject",
            description = "Customer Email Subject")
    String customerEmailSubject() default "Gift Hamper Order Details";

    @AttributeDefinition(name = "Gift Hamper Customer Email Body",
            description = "Gift Hamper Customer Email Body")
    String customerEmailBodyTemplate() default "GIFTHAMPERCODE:- $GIFTHAMPERCODE$, SENDERAMOUNT:- $SENDERAMOUNT$ || CCAVENUETRACKINGID:- $CCAVENUETRACKINGID$ || SENDERNAME:- $SENDERNAME$";

    @AttributeDefinition(name = "Gift Hamper Hotel Email Subject",
            description = "Gift Hamper Hotel Email Subject")
    String hotelEmailSubject() default "Gift Hamper Order Details";

    @AttributeDefinition(name = "Gift Hamper Hotel Email Body",
            description = "Gift Hamper Hotel Email Body")
    String hotelEmailBodyTemplate() default "GIFTHAMPERCODE:- $GIFTHAMPERCODE$, SENDERAMOUNT:- $SENDERAMOUNT$ || CCAVENUETRACKINGID:- $CCAVENUETRACKINGID$ || SHIPPINGNAME:- $SHIPPINGNAME$, SHIPPINGPHONENUMBER:- $SHIPPINGPHONENUMBER$, SHIPPINGADDRESS:- $SHIPPINGADDRESS$, SHIPPINGCITY:- $SHIPPINGCITY$, SHIPPINGPINCODE:- $SHIPPINGPINCODE$, SHIPPINGSTATE:- $SHIPPINGSTATE$, SHIPPINGCOUNTRY:- $SHIPPINGCOUNTRY$";

    @AttributeDefinition(name = "Gift Hamper Confirmation Page Path",
            description = "URL page path to which the user has to be redirected after payment success")
    String getConfirmationPagePath() default "/en-in/gift-hamper-payment-success";

    @AttributeDefinition(name = "Gift Hamper Failure Page Path",
            description = "URL page path to which the user has to be redirected after payment failure")
    String getFailurePagePath() default "/en-in/gift-hamper-payment-fail";
}
