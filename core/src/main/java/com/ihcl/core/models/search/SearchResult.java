package com.ihcl.core.models.search;

import java.util.Collection;
import java.util.List;


public interface SearchResult {
	int DESCRIPTION_MAX_LENGTH = 320;

	String DESCRIPTION_ELLIPSIS = " ... ";

	enum ContentType {
		PAGE, ASSET
	}

	ContentType getContentType();

	List<String> getTagIds();

	String getURL();

	String getPath();

	String getTitle();

	String getDescription();
	
	String getResourceType();

	void setExcerpts(Collection<String> excerpt);

}
