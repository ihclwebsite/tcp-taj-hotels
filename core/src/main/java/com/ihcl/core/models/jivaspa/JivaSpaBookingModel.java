package com.ihcl.core.models.jivaspa;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = JivaSpaListModel.class)
public class JivaSpaBookingModel {

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Inject
	SlingHttpServletRequest request;

	@Inject
	private Page currentPage;

	@Inject
	Resource resource;
	
	/*From details page*/
	private String spaName;
	private String spaType;
	private String amount;
	
	/*From booking page*/
	private String hotel;
	private String fullName;
	private String email;
	private String preferredDate;
	private String gender;
	private String mobileNumber;
	private String availability;
	/**
	 * @return the spaName
	 */
	public String getSpaName() {
		return spaName;
	}
	/**
	 * @return the spaType
	 */
	public String getSpaType() {
		return spaType;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @return the hotel
	 */
	public String getHotel() {
		return hotel;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the preferredDate
	 */
	public String getPreferredDate() {
		return preferredDate;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @return the availability
	 */
	public String getAvailability() {
		return availability;
	}
	
	
}
