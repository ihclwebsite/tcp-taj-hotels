/**
 *
 */
package com.ihcl.core.models.meeting;

import static com.ihcl.tajhotels.constants.CrxConstants.MEETING_EVENT_DETAILS_COMPONENT_RESOURCETYPE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.shared.services.ResourceFetcherService;

/**
 * <pre>
 * MeetingEventModel.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :30-Aug-2019
 * @ClassName : MeetingEventModel.java
 * @Description : com.ihcl.core.models.meeting -MeetingEventModel.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  30-Aug-2019     Subharun Mukherjee            Create
 *
 *               </pre>
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = MeetingEventModel.class)
public class MeetingEventModel {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(MeetingEventModel.class);

    /** The Constant CONSTANT_STRING_NA. */
    private static final String CONSTANT_STRING_NA = "NA";

    /** The resource. */
    @Inject
    private Resource resource;

    /** The resolver factory. */
    @Inject
    private ResourceResolverFactory resolverFactory;

    /** The resource fetcher service. */
    @Inject
    private ResourceFetcherService resourceFetcherService;

    /** The gallery image fetcher service. */
    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;


    /**
     * Gets the meeting details.
     *
     * @return the meeting details
     */
    public MeetingDetailsBean getMeetingDetails() {
        ResourceResolver resolver = null;
        try {
            resolver = resolverFactory.getServiceResourceResolver(null);
            Resource eventDetailsResource = resourceFetcherService.getChildrenOfType(resource,
                    MEETING_EVENT_DETAILS_COMPONENT_RESOURCETYPE);
            if (null != eventDetailsResource) {
                Resource eventRoomResource = eventDetailsResource.getParent().getParent();

                ValueMap valueMap = eventDetailsResource.getValueMap();
                String roomTitle = getValueMapProperty(valueMap, "roomTitle");
                String requestQuotePagePath = getValueMapProperty(valueMap, "requestQuotePagePath");
                String roomLongDescription = getValueMapProperty(valueMap, "roomLongDescription");
                String roomBuilt = getValueMapProperty(valueMap, "roomBuilt");
                String roomRenovated = getValueMapProperty(valueMap, "roomRenovated");
                String airportDistance = getValueMapProperty(valueMap, "airportDistance");
                String roomDimension = getValueMapProperty(valueMap, "roomDimension");
                String roomArea = getValueMapProperty(valueMap, "roomArea");
                String roomHeight = getValueMapProperty(valueMap, "roomHeight");
                String guestEntry = getValueMapProperty(valueMap, "guestEntry");
                String seatingStyle = getValueMapProperty(valueMap, "seatingStyle");
                String theatreCapacity = getCapacity(valueMap, "theatreCapacity");
                String uCapacity = getCapacity(valueMap, "uCapacity");
                String classroomCapacity = getCapacity(valueMap, "classroomCapacity");
                String circularCapacity = getCapacity(valueMap, "circularCapacity");
                String boardroomCapacity = getCapacity(valueMap, "boardroomCapacity");
                String reception = getCapacity(valueMap, "reception");
                String roomCapacity = getValueMapProperty(valueMap, "roomCapacity");
                String roomTheme = getValueMapProperty(valueMap, "roomTheme");
                String meetingImage = getValueMapProperty(valueMap, "meetingImage");

                String capacity = null;
                if (null == roomCapacity) {
                    capacity = getMaxCapacity(theatreCapacity, uCapacity, classroomCapacity, circularCapacity,
                            boardroomCapacity, reception);
                }

                List<String> venueEventTypes = new ArrayList<>();
                String requestQuoteEmailId = null;
                String hotelName = null;
                String location = null;
                if (null != eventRoomResource) {
                    ValueMap hotelValueMap = eventRoomResource.getParent().getParent().getParent()
                            .getChild("jcr:content").getValueMap();
                    venueEventTypes = getVenueTypes(eventRoomResource, resolver);
                    requestQuoteEmailId = hotelValueMap.get("requestQuoteEmail") != null
                            ? hotelValueMap.get("requestQuoteEmail").toString()
                            : hotelValueMap.get("hotelEmail").toString();
                    hotelName = getValueMapProperty(hotelValueMap, "hotelName");
                    location = (getValueMapProperty(hotelValueMap, "locations") + " " + hotelName + " " + roomTitle)
                            .toLowerCase();
                }

                List<JSONObject> galleryImagePaths = new ArrayList<>();
                Resource galleryResource = eventDetailsResource.getChild("gallery");
                if (galleryResource != null) {
                    galleryImagePaths = galleryImageFetcherService.getGalleryImagePathsAt(galleryResource);
                }

                return new MeetingDetailsBean(roomTitle, requestQuotePagePath, roomCapacity, roomLongDescription,
                        roomBuilt, roomRenovated, airportDistance, roomDimension, roomArea, roomHeight, guestEntry,
                        seatingStyle, theatreCapacity, uCapacity, classroomCapacity, circularCapacity,
                        boardroomCapacity, reception, roomTheme, capacity, meetingImage, galleryImagePaths, location,
                        hotelName, venueEventTypes, requestQuoteEmailId);
            }

        } catch (Exception e) {
            LOG.error("Exception while getting the event details :: {}", e.getMessage());
            LOG.debug("Exception while getting the event details :: {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return null;
    }

    /**
     * Gets the capacity.
     *
     * @param valueMap
     *            the value map
     * @param string
     *            the string
     * @return the capacity
     */
    private String getCapacity(ValueMap valueMap, String string) {
        Object value = valueMap.get(string);
        if (null != value || value == "") {
            return valueMap.get(string).toString();
        } else {
            return CONSTANT_STRING_NA;
        }
    }

    /**
     * Gets the value map property.
     *
     * @param valueMap
     *            the value map
     * @param string
     *            the string
     * @return the value map property
     */
    private String getValueMapProperty(ValueMap valueMap, String string) {
        return valueMap.get(string) != null ? valueMap.get(string).toString() : null;
    }

    /**
     * Gets the max capacity.
     *
     * @param theatreCapacity
     *            the theatre capacity
     * @param uCapacity
     *            the u capacity
     * @param classroomCapacity
     *            the classroom capacity
     * @param circularCapacity
     *            the circular capacity
     * @param boardroomCapacity
     *            the boardroom capacity
     * @param reception
     *            the reception
     * @return the max capacity
     */
    private String getMaxCapacity(String theatreCapacity, String uCapacity, String classroomCapacity,
            String circularCapacity, String boardroomCapacity, String reception) {
        String maxCapacity = null;
        int theatre = (StringUtils.equals(theatreCapacity, CONSTANT_STRING_NA)) ? Integer.parseInt(theatreCapacity) : 0;
        int u = (StringUtils.equals(uCapacity, CONSTANT_STRING_NA)) ? Integer.parseInt(uCapacity) : 0;
        int classroom = (StringUtils.equals(classroomCapacity, CONSTANT_STRING_NA))
                ? Integer.parseInt(classroomCapacity)
                : 0;
        int circular = (StringUtils.equals(circularCapacity, CONSTANT_STRING_NA)) ? Integer.parseInt(circularCapacity)
                : 0;
        int boardroom = (StringUtils.equals(boardroomCapacity, CONSTANT_STRING_NA))
                ? Integer.parseInt(boardroomCapacity)
                : 0;
        int receptionNum = (StringUtils.equals(reception, CONSTANT_STRING_NA)) ? Integer.parseInt(reception) : 0;
        List<Integer> list = Arrays.asList(theatre, u, classroom, circular, boardroom, receptionNum);
        maxCapacity = Collections.max(list).toString();
        return maxCapacity;
    }

    /**
     * Gets the venue types.
     *
     * @param resource
     *            the resource
     * @param resolver
     *            the resolver
     * @return the venue types
     */
    private List<String> getVenueTypes(Resource resource, ResourceResolver resolver) {
        List<String> venueEventTypes = new ArrayList<>();
        try {
            ValueMap typesValueMap = resource.getValueMap();
            String[] events = typesValueMap.get("venueEventTypes", String[].class);
            if (null != events && events.length > 0) {
                for (String event : events) {
                    TagManager tagMgr = resolver.adaptTo(TagManager.class);
                    Tag eventTag = tagMgr.resolve(event);
                    if (null != eventTag) {
                        venueEventTypes.add(eventTag.getTitle());
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Exception while getting the event types :: {}", e.getMessage());
            LOG.debug("Exception while getting the event types :: {}", e);
        }
        return venueEventTypes;
    }

}
