/**
 *
 */
package com.ihcl.core.models.rateandreview;

import org.codehaus.jackson.annotate.JsonProperty;

import com.ihcl.core.models.Meta;

public class SealResponse {

    @JsonProperty("meta")
    private Meta meta;

    @JsonProperty("response")
    private Response response;


    public Meta getMeta() {
        return meta;
    }


    public void setMeta(Meta meta) {
        this.meta = meta;
    }


    public Response getResponse() {
        return response;
    }


    public void setResponse(Response response) {
        this.response = response;
    }


}
