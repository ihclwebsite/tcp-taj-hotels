package com.ihcl.core.models;


public class NavigationTabBean {

    public String tabtext;

    public String tabtextlink;

    public String getTabtext() {
        return tabtext;
    }

    public void setTabtext(String tabtext) {
        this.tabtext = tabtext;
    }

    public String getTabtextlink() {
        return tabtextlink;
    }

    public void setTabtextlink(String tabtextlink) {
        this.tabtextlink = tabtextlink;
    }


}
