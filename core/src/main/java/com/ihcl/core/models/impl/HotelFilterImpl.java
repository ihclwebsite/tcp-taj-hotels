package com.ihcl.core.models.impl;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.ihcl.core.models.HotelFilter;
import com.ihcl.core.services.search.HotelsSearchService;

@Model(adaptables = Resource.class, adapters = HotelFilter.class)
public class HotelFilterImpl implements HotelFilter {

	private HashMap<String, String> hotelTypes;

	@Self
	Resource resource;

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	HotelsSearchService hotelSearchService;

	@PostConstruct
	public void init() {
		hotelTypes = hotelSearchService.getHotelTypes();

	}

	@Override
	public HashMap<String, String> getHotelTypes() {
		return hotelTypes;
	}

}
