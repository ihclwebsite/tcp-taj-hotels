/**
 *
 */
package com.ihcl.core.models.serviceamenity.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.serviceamenity.GroupedServiceAmenitiesFetcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sampathkumar
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = GroupedServiceAmenitiesFetcher.class)
public class GroupedServiceAmenitiesFetcherImpl implements GroupedServiceAmenitiesFetcher {

	private static final Logger LOG = LoggerFactory.getLogger(GroupedServiceAmenitiesFetcherImpl.class);

	private List<String> allAmenities;

	private String[] bedAndBathAmenities;

	private String[] entertainment;

	private String[] otherConveniences;

	private String[] wellnessAmenities;

	private String[] suiteFeatures;

	private String[] hotelFacilities;

	int sizeOfAllAmenities;

	int sizeOfBedAndBathAmenities;

	int sizeOfEntertainment;

	int sizeOfOtherConveniences;

	int sizeOfWellnessAmenities;

	int sizeOfSuiteFeatures;

	int sizeOfHotelFacilities;

	@Inject
	@Default(values = "")
	private Resource resource;

	@Inject
	@Default(values = "")
	private String componentPath;

	@PostConstruct
	protected void init() {
		String methodName = "init";
		LOG.trace("Method Entry: " + methodName);
		LOG.debug("Received resource as: " + resource);
		LOG.debug("Received componentPath as: " + componentPath);
		fetchGroupedServiceAmenities(resource);
		LOG.trace("Method Exit: " + methodName);
	}

	private void fetchGroupedServiceAmenities(Resource resource) {
		String methodName = "fetchGroupedServiceAmenities";
		LOG.trace("Method Entry: " + methodName);
		try {
			if (null != componentPath && !"".equals(componentPath)) {
				Resource amenitiesComponentResource = getAmenitiesDetailsComponentUnder(resource);
				if (amenitiesComponentResource != null) {
					fetchServiceAmenities(amenitiesComponentResource);
				}
			} else {
				fetchServiceAmenities(resource);
			}
		} catch (Throwable e) {
			LOG.error("An error occured while fetching grouped service amenities.", e);
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private void fetchServiceAmenities(Resource resource) {
		String methodName = "fetchServiceAmenities";
		LOG.trace("Method Entry: " + methodName);
		Resource serviceAmenitiesNode = resource.getChild("serviceAmenities");
		try {
			if (serviceAmenitiesNode != null) {
				LOG.debug("Found service amenities node at: " + serviceAmenitiesNode);
				ValueMap valueMap = serviceAmenitiesNode.adaptTo(ValueMap.class);
				bedAndBathAmenities = getProperty(valueMap, "bedAndBathExperience", String[].class);
				entertainment = getProperty(valueMap, "entertainment", String[].class);
				otherConveniences = getProperty(valueMap, "otherConveniences", String[].class);
				wellnessAmenities = getProperty(valueMap, "wellnessAmenities", String[].class);
				suiteFeatures = getProperty(valueMap, "suiteFeatures", String[].class);
				hotelFacilities = getProperty(valueMap, "hotelFacilities", String[].class);

				sizeOfOtherConveniences = otherConveniences == null ? 0 : otherConveniences.length;
				sizeOfBedAndBathAmenities = bedAndBathAmenities == null ? 0 : bedAndBathAmenities.length;
				sizeOfEntertainment = entertainment == null ? 0 : entertainment.length;
				sizeOfHotelFacilities = hotelFacilities == null ? 0 : hotelFacilities.length;
				sizeOfSuiteFeatures = suiteFeatures == null ? 0 : suiteFeatures.length;
				sizeOfWellnessAmenities = wellnessAmenities == null ? 0 : wellnessAmenities.length;

				populateAllAmenities();
			}
		} catch (Throwable e) {
			LOG.error("Exception : ", e);
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private void populateAllAmenities() {
		String methodName = "populateAllAmenities";
		LOG.trace("Method Entry: " + methodName);
		allAmenities = new ArrayList<>();

		addToAllAmenities(bedAndBathAmenities);
		addToAllAmenities(entertainment);
		addToAllAmenities(suiteFeatures);
		addToAllAmenities(hotelFacilities);
		addToAllAmenities(otherConveniences);
		addToAllAmenities(wellnessAmenities);

		LOG.debug("Populated all amenities as: " + allAmenities);
		LOG.trace("Method Exit: " + methodName);
	}

	private void addToAllAmenities(String[] amenities) {
		String methodName = "addToAllAmenities";
		LOG.trace("Method Entry: " + methodName);
		if (amenities != null) {
			int amenitiesLength = amenities.length;
			LOG.debug("Attempting to add the given amenities array with length: " + amenitiesLength
					+ " to all amenities");
			if (amenitiesLength > 0) {
				allAmenities.addAll(Arrays.asList(amenities));
			}
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private Resource getAmenitiesDetailsComponentUnder(Resource resource) {
		String methodName = "getAmenitiesDetailsComponentUnder";
		LOG.trace("Method Entry: " + methodName);
		Resource root = resource.getChild("jcr:content/root");
		Resource amenitiesComponentResource = null;
		LOG.debug("Attempting to iterate over children of: " + root);
		if (root != null) {
			Iterable<Resource> children = root.getChildren();
			for (Resource child : children) {
				LOG.debug("Iterating over child: " + child);
				if (child.getResourceType().equals(componentPath)) {
					LOG.debug("Found amenities component at child: " + child);
					amenitiesComponentResource = child;
					break;
				}
			}
		}
		LOG.trace("Method Exit: " + methodName);
		return amenitiesComponentResource;
	}

	/**
	 * @param valueMap
	 * @param key
	 * @param type
	 */
	private <T> T getProperty(ValueMap valueMap, String key, Class<T> type) {
		T value = null;
		if (valueMap.containsKey(key)) {
			value = valueMap.get(key, type);
		}
		return value;
	}

	@Override
	public List<String> getAllAmenities() {
		return allAmenities;
	}

	@Override
	public String[] getBedAndBathAmenities() {
		return bedAndBathAmenities;
	}

	@Override
	public String[] getEntertainment() {
		return entertainment;
	}

	@Override
	public String[] getOtherConveniences() {
		return otherConveniences;
	}

	@Override
	public String[] getWellnessAmenities() {
		return wellnessAmenities;
	}

	@Override
	public String[] getSuiteFeatures() {
		return suiteFeatures;
	}

	@Override
	public String[] getHotelFacilities() {
		return hotelFacilities;
	}

	public int getSizeOfAllAmenities() {
		return sizeOfAllAmenities;
	}

	public int getSizeOfBedAndBathAmenities() {
		return sizeOfBedAndBathAmenities;
	}

	public int getSizeOfEntertainment() {
		return sizeOfEntertainment;
	}

	public int getSizeOfOtherConveniences() {
		return sizeOfOtherConveniences;
	}

	public int getSizeOfWellnessAmenities() {
		return sizeOfWellnessAmenities;
	}

	public int getSizeOfSuiteFeatures() {
		return sizeOfSuiteFeatures;
	}

	public int getSizeOfHotelFacilities() {
		return sizeOfHotelFacilities;
	}

}
