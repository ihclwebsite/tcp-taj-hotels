package com.ihcl.core.models.TajAirCardsList;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.TajAirCardsListModel;
import com.ihcl.core.util.services.TajCommonsFetchResource;

@Model(adaptables = SlingHttpServletRequest.class,
        adapters = TajAirCardsListModel.class)
public class TajAirCardsListModelImpl implements TajAirCardsListModel {

    private static final Logger LOG = LoggerFactory.getLogger(TajAirCardsListModelImpl.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private Resource resource;

    ValueMap valueMap;


    @PostConstruct
    public void init() {

        LOG.trace(" Method Entry : init()");
        resource = request.getResource();
        LOG.trace("resource Path In corporate list cards  Model : " + resource.getPath());
        if (tajCommonsFetchResource != null) {
            valueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "taj_air_offer_detail",
                    "tajhotels/components/content/taj-air-offer-details");
            LOG.trace("valuemap from TAj Air cards list  :" + valueMap);

        }
        LOG.trace(" Method Exit : init()");
    }

    @Override
    public ValueMap getValueMap() {
        LOG.trace(" --------------------------->Method Entry : getvalueMap()");
        return valueMap;
    }

}
