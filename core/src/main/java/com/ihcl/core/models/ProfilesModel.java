/**
 *
 */
package com.ihcl.core.models;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author Vijay.Pal
 *
 */
@ConsumerType
public interface ProfilesModel {

    /**
     * @return
     */
    ValueMap getValueMap();


}
