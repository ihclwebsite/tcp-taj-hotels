package com.ihcl.core.models;

public class TitleBean {
	
	private String titleFormItemLabel;

	public String getTitleFormItemLabel() {
		return titleFormItemLabel;
	}

	public void setTitleFormItemLabel(String titleFormItemLabel) {
		this.titleFormItemLabel = titleFormItemLabel;
	}

}
