package com.ihcl.core.models.reviewsPojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Review_list {

    private String ty_id;

    private String text;

    private String source_id;

    private int score;

    private String lang;

    private String author_nationality;

    private String url;

    private String author;

    private String title;

    private String review_id;

    private String author_age;

    private String[] category_list;

    private String created;

    private String[] response_list;

    private String trip_type;

    private String[] category_score_list;

    private String source_name;

    public String getTy_id() {
        return ty_id;
    }

    public void setTy_id(String ty_id) {
        this.ty_id = ty_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSource_id() {
        return source_id;
    }

    public void setSource_id(String source_id) {
        this.source_id = source_id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getAuthor_nationality() {
        return author_nationality;
    }

    public void setAuthor_nationality(String author_nationality) {
        this.author_nationality = author_nationality;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getAuthor_age() {
        return author_age;
    }

    public void setAuthor_age(String author_age) {
        this.author_age = author_age;
    }

    public String[] getCategory_list() {
        return category_list;
    }

    public void setCategory_list(String[] category_list) {
        this.category_list = category_list;
    }

    public String getCreated() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String sysTime = formatter.format(date);
        long dayDifference = getDateDifference(this.created, sysTime);
        created = getApproximateDiff(dayDifference);
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String[] getResponse_list() {
        return response_list;
    }

    public void setResponse_list(String[] response_list) {
        this.response_list = response_list;
    }

    public String getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(String trip_type) {
        this.trip_type = trip_type;
    }

    public String[] getCategory_score_list() {
        return category_score_list;
    }

    public void setCategory_score_list(String[] category_score_list) {
        this.category_score_list = category_score_list;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    @Override
    public String toString() {
        return "ClassPojo [ty_id = " + ty_id + ", text = " + text + ", source_id = " + source_id + ", score = " + score
                + ", lang = " + lang + ", author_nationality = " + author_nationality + ", url = " + url + ", author = "
                + author + ", title = " + title + ", review_id = " + review_id + ", author_age = " + author_age
                + ", category_list = " + category_list + ", created = " + created + ", response_list = " + response_list
                + ", trip_type = " + trip_type + ", category_score_list = " + category_score_list + ", source_name = "
                + source_name + "]";
    }

    public long getDateDifference(String inputDate1, String inputDate2) {

        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
        long daydifferences = 0;
        try {
            Date date1 = myFormat.parse(inputDate1);
            Date date2 = myFormat.parse(inputDate2);
            long diff = date2.getTime() - date1.getTime();
            daydifferences = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return daydifferences;
    }

    public String getApproximateDiff(Long dayDifference) {
        String response = "";

        if (dayDifference >= 366)
            response = dayDifference / 366 + " years ago";
        else if (dayDifference >= 30)
            response = dayDifference / 30 + " months ago";
        else
            response = dayDifference + " days ago";
        return response;
    }
}
