package com.ihcl.core.models.room;

import java.io.IOException;
import java.util.List;

import org.json.JSONObject;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface RoomPropertiesFetcher {

    String getPath();

    String getMaxOccupancyDetails();

    String getDescription();

    String getSize();

    String getSignatureFeature();

    String getRoomTypeName();

    String[] getServiceAmenities();

    /**
     * @return
     */
    List<String> getAllAmenities();

    /**
     * @return
     */
    String[] getBedAndBathAmenities();

    /**
     * @return
     */
    String[] getEntertainment();

    /**
     * @return
     */
    String[] getOtherConveniences();

    /**
     * @return
     */
    String[] getWellnessAmenities();

    /**
     * @return
     */
    String[] getSuiteFeatures();

    /**
     * @return
     */
    String[] getHotelFacilities();

    /**
     * @return
     */
    String[] getBedAndBathAemenities();

    /**
     * @return
     */
    String[] getEntertainmentAemenities();

    /**
     * @return
     */
    List<JSONObject> getGalleryImagePaths();

    String getHighightImagePath();

    String getDraggedImagePath();

    String getRoomCodeBedTypeMapString() throws IOException;

    /**
     * Method to get area of the room
     *
     * @return String - Area
     */
    String getArea();

    /**
     * Method to get wifi availability of the room
     *
     * @return String - wifi availability
     */
    String getWifi();

    /**
     * Method to get maximum occupancy in the room.
     *
     * @return String - maximum occupancy
     */
    String getMaxOccupancy();

    /**
     * Method to get bed type available in room
     *
     * @return String - bed type
     */
    String getBedType();

    /**
     * Method to get the signature feature of the room
     *
     * @returnString - Signature feature of the room
     */
    String getSignatureFeatureOfRoom();

    /**
     * Method to return list of signature features
     *
     * @return - list of signature features.
     */
    String[] getSignatureFeaturesList();

    List<String> getSignatureList();

    /**
     * Method to return whether the room is configured for online booking
     *
     * @return
     */
    boolean getBookableOnline();

    /**
     * Returns the minimum number of days that has to be selected to book the room
     *
     * @return
     */
    int getMinimumDaysSelectionForBooking();

    /**
     * Method to return if online booking disabled reason or message
     * 
     * @return
     */
    String getOnlineUnavailableMessage();
    
    String getAreaImage();
    String getWifiImage();
    String getMaxOccupancyImage();
    String getBedTypeImage();
    String getOtherAmenity();
    String getOtherAmenityImage();
}
