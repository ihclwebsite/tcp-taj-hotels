package com.ihcl.core.models.metaReviewsPojo;

public class Language_meta_review_list {

    private Summary summary;

    private Good_to_know_list[] good_to_know_list;

    private Category_list[] category_list;

    private String reviews_percent;

    private Filter filter;

    private Trip_type_meta_review_list[] trip_type_meta_review_list;

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public Good_to_know_list[] getGood_to_know_list() {
        return good_to_know_list;
    }

    public void setGood_to_know_list(Good_to_know_list[] good_to_know_list) {
        this.good_to_know_list = good_to_know_list;
    }

    public Category_list[] getCategory_list() {
        return category_list;
    }

    public void setCategory_list(Category_list[] category_list) {
        this.category_list = category_list;
    }

    public String getReviews_percent() {
        return reviews_percent;
    }

    public void setReviews_percent(String reviews_percent) {
        this.reviews_percent = reviews_percent;
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public Trip_type_meta_review_list[] getTrip_type_meta_review_list() {
        return trip_type_meta_review_list;
    }

    public void setTrip_type_meta_review_list(Trip_type_meta_review_list[] trip_type_meta_review_list) {
        this.trip_type_meta_review_list = trip_type_meta_review_list;
    }

    @Override
    public String toString() {
        return "ClassPojo [summary = " + summary + ", good_to_know_list = " + good_to_know_list + ", category_list = "
                + category_list + ", reviews_percent = " + reviews_percent + ", filter = " + filter
                + ", trip_type_meta_review_list = " + trip_type_meta_review_list + "]";
    }
}
