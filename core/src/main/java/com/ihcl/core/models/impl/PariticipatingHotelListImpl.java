package com.ihcl.core.models.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import com.ihcl.core.models.ParticipatingHotelList;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.core.services.search.SearchServiceConstants;

@Model(adaptables = Resource.class, adapters = ParticipatingHotelList.class)
public class PariticipatingHotelListImpl implements ParticipatingHotelList {

	@Inject
	HotelsSearchService hotelSearchService;
	private List<String> allHotels;

	@PostConstruct
	public void init() {
		allHotels = hotelSearchService.getAllHotels(SearchServiceConstants.PATH.HOTELROOT);
	}

	@Override
	public List<String> getAllHotels() {
		return allHotels;
	}

}
