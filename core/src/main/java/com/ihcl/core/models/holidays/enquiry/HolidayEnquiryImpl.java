package com.ihcl.core.models.holidays.enquiry;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = IHolidayEnquiry.class)
public class HolidayEnquiryImpl implements IHolidayEnquiry {

	private static final Logger LOG = LoggerFactory.getLogger(HolidayEnquiryImpl.class);

	private static final String USER_DETAILS_COMPONENT_NAME = "userDetails";
	private static final String GENDER_DROPDOWN_COMPONENT_NAME = "genderDropDown";
	private static final String PROPERTY_FORM_LABEL = "userFormItemLabel";
	private static final String PROPERTY_FORM_FIELD = "userFormItemField";
	private static final String PROPERTY_GENDER_OPTION = "genderOption";
	@Inject
	Resource resource;

	@Inject
	ResourceResolverFactory resourceResolverFactory;

	@Inject
	SearchProvider searchProvider;

	private HolidayEnquiryModel formFields;
	
	@PostConstruct
	protected void init() throws IOException {

		LOG.debug("Inside init method of class IHolidayEnquiryImpl");
		LOG.debug("Initial resource path: {}", resource.getPath());

		formFields= new HolidayEnquiryModel();
		List<HolidayMultiFieldForm> listOfFields = new LinkedList<>();
		Iterable<Resource> detailsNode = resource.getChildren();
		List<String> genderOptions = new LinkedList<>();
		try {
			for (Resource child : detailsNode) {
				LOG.debug("current child name : {}", child.getName());

				if (child.getName().equals(USER_DETAILS_COMPONENT_NAME)) {

					Iterable<Resource> items = child.getChildren();
					for (Resource item : items) {
						HolidayMultiFieldForm formfield = new HolidayMultiFieldForm();
						ValueMap valueMap = item.adaptTo(ValueMap.class);
						formfield.setUserFormItemLabel(valueMap.get(PROPERTY_FORM_LABEL, String.class));
						formfield.setUserFormItemField(valueMap.get(PROPERTY_FORM_FIELD, String.class));
						listOfFields.add(formfield);
					}
					formFields.setListOfFields(listOfFields);
				} else if (child.getName().equals(GENDER_DROPDOWN_COMPONENT_NAME)) {

					Iterable<Resource> options = child.getChildren();
					for (Resource option : options) {
						ValueMap valueMap = option.adaptTo(ValueMap.class);
						genderOptions.add(valueMap.get(PROPERTY_GENDER_OPTION, String.class));
					}
					formFields.setGenderOptions(genderOptions);
				}
			}
		} catch (Exception e) {
			LOG.error("error occured while getting fields data : {}", e.getMessage());
			e.printStackTrace();
		}

		LOG.debug("Leaving init method of class IHolidayEnquiryImpl");
	}

	@Override
	public HolidayEnquiryModel getFormFields() {
		
		LOG.debug("returning : " +formFields.hashCode() );
		return formFields;
	}

}
