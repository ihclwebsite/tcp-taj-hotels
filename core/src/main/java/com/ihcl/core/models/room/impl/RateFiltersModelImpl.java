package com.ihcl.core.models.room.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.room.RateFiltersModel;


@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = RateFiltersModel.class)
public class RateFiltersModelImpl implements RateFiltersModel {

    private static final Logger LOG = LoggerFactory.getLogger(RateFiltersModelImpl.class);

    @Inject
    @Default(values = "")
    private Resource resource;

    private List<RateFilterBean> rateFilters;


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received resource as: " + resource);
        rateFilters = new ArrayList<>();
        fetchRateFilters();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */
    private void fetchRateFilters() {
        String methodName = "fetchRateFilters";
        LOG.trace("Method Entry: " + methodName);
        Resource rateFiltersNode = resource.getChild("rateFilters");
        if (rateFiltersNode != null) {
            Iterable<Resource> children = rateFiltersNode.getChildren();
            for (Resource child : children) {
                fetchRateFilterAt(child);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @param child
     */
    private void fetchRateFilterAt(Resource resource) {
        String methodName = "fetchRateFilterAt";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = resource.getValueMap();
        RateFilterBean bean = new RateFilterBean();
        String displayName = getProperty(valueMap, "rateFilterName", String.class, "");
        String rateFilterCode = getProperty(valueMap, "rateFilterCode", String.class, "");
        bean.setRateFilterCode(rateFilterCode);
        bean.setRateFilterDisplayName(displayName);
        rateFilters.add(bean);
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param valueMap
     * @param key
     * @param type
     * @param defaultValue
     *
     */
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        String methodName = "getProperty";
        LOG.trace("Method Entry: " + methodName);
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        LOG.trace("Value found for key: " + key + " : " + value);
        LOG.trace("Method Exit: " + methodName);
        return value;
    }

    @Override
    public List<RateFilterBean> getRateFilters() {
        return rateFilters;
    }


}
