package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(
        adaptables = Resource.class)
public class ContactUsMiscellaneousModel {


    protected static final Logger LOG = LoggerFactory.getLogger(ContactUsMiscellaneousModel.class);

    @Inject
    @Optional
    @Named("misc")
    private List<MiscellaneousBean> misc;

    @PostConstruct
    public void init() {
        String methodName = "init";
        LOG.info("Method Entry of ContactUsMiscellaneousModel :: " + methodName);
        LOG.info("Method Exit of ContactUsMiscellaneousModel :: " + methodName);
    }

    public List<MiscellaneousBean> getMisc() {
        return misc;
    }

}
