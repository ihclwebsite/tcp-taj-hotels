package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class HotelAwards extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<AwardMultifieldBean> ls = new ArrayList<AwardMultifieldBean>();

    @Override
    public void activate() throws Exception {

        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();

        while (ni.hasNext()) {

            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setMultiFieldItems(ni2);
        }
    }

    public void setMultiFieldItems(NodeIterator ni2) {
        try {
            while (ni2.hasNext()) {
                String awardImagePath;
                String awardTitle;
                String awardDesc;

                AwardMultifieldBean obj = new AwardMultifieldBean();
                Node grandChild = ni2.nextNode();
                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
                if (grandChild.hasProperty("awardImagePath")) {
                    log.trace("Property value is : " + grandChild.getProperty("awardImagePath"));
                    awardImagePath = grandChild.getProperty("awardImagePath").getString();
                    obj.setAwardImagePath(awardImagePath);
                } else {
                    obj.setAwardImagePath("");
                }
                if (grandChild.hasProperty("awardTitle")) {
                    awardTitle = grandChild.getProperty("awardTitle").getString();
                    obj.setAwardTitle(awardTitle);
                } else {
                    obj.setAwardTitle("");
                }
                if (grandChild.hasProperty("awardDesc")) {
                    awardDesc = grandChild.getProperty("awardDesc").getString();
                    obj.setAwardDesc(awardDesc);
                } else {
                    obj.setAwardDesc("");
                }

                ls.add(obj);

            }
        } catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }
    }

    public List<AwardMultifieldBean> getMultiFieldItems() {
        /*
         * for (int i = 0; i < ls.size(); i++) ls.get(i).setIndex(i);
         */
        log.info("Result -----> " + ls);
        return ls;
    }
}
