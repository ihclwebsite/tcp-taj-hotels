package com.ihcl.core.models;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.hotels.DestinationHotelBean;
import com.ihcl.core.hotels.DestinationHotelMapBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;

/**
 * The Class Footer.
 */
@Model(adaptables = SlingHttpServletRequest.class, adapters=Footer.class)
public class Footer {

    /** The Constant LOG. */
    protected static final Logger LOG = LoggerFactory.getLogger(Footer.class);
    

    /** The footer links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> footerLinks;

    /** The policy links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> policyLinks;

    /** The brand links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> brandLinks;

    /** The our brand links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> ourBrandLinks;

    /** The company links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> companyLinks;

    /** The development links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> developmentLinks;

    /** The responsibility links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> responsibilityLinks;

    /** The career links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> careerLinks;

    /** The news room links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> newsRoomLinks;

    /** The home links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> homeLinks;

    /** The contact links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> contactLinks;

    /** The investors links. */
    @Inject @Via("resource")
    @Optional
    private List<FooterLink> investorsLinks;


    /** The destinations. */
    private TreeMap<String, String> destinations;

    
    /** The tajhotels hotels bean service. */
    @Inject
    private TajhotelsHotelsBeanService tajhotelsHotelsBeanService;

    /** The root path. */
    @Inject
    @Default(values="/content/tajhotels")
    private String rootPath;

    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
        try {
            LOG.trace("Method entry -> init()");
            destinations = getAllDestinations();
            LOG.trace("Method exit -> init()");
        } catch (Exception e) {
            LOG.error("Exception occured at Post Construct method :: {}", e.getMessage());
            LOG.debug("Exception occured at Post Construct method :: {}", e);
        }
    }
    
    /**
     * Gets the all destinations.
     *
     * @return the all destinations
     */
    private TreeMap<String, String> getAllDestinations() {
		destinations = new TreeMap<>();
		Map<String, DestinationHotelMapBean> websitesBean = tajhotelsHotelsBeanService.getWebsiteSpecificHotelsBean().getWebsiteHotelsMap();
            	Map<String, DestinationHotelBean> destinationMap = websitesBean.get(rootPath+"/en-in").getDestinationMap();
            	for (Entry<String, DestinationHotelBean> entry : destinationMap.entrySet()) {
            		destinations.put(entry.getValue().getDestinationDetails().getDestinationTitle(), entry.getValue().getDestinationDetails().getDestinationPath());
            	}
				return destinations;
	}

    /**
     * Gets the footer links.
     *
     * @return the footer links
     */
    public List<FooterLink> getFooterLinks() {
        return footerLinks;
    }

    /**
     * Gets the policy links.
     *
     * @return the policy links
     */
    public List<FooterLink> getPolicyLinks() {
        return policyLinks;
    }

    /**
     * Gets the brand links.
     *
     * @return the brand links
     */
    public List<FooterLink> getBrandLinks() {
        return brandLinks;
    }

    /**
     * Gets the destinations.
     *
     * @return the destinations
     */
    public Map<String, String> getDestinations() {
        return destinations;
    }

    /**
     * Gets the log.
     *
     * @return the log
     */
    public static Logger getLog() {
        return LOG;
    }

    /**
     * Gets the our brand links.
     *
     * @return the our brand links
     */
    public List<FooterLink> getOurBrandLinks() {
        return ourBrandLinks;
    }

    /**
     * Gets the company links.
     *
     * @return the company links
     */
    public List<FooterLink> getCompanyLinks() {
        return companyLinks;
    }

    /**
     * Gets the development links.
     *
     * @return the development links
     */
    public List<FooterLink> getDevelopmentLinks() {
        return developmentLinks;
    }

    /**
     * Gets the responsibility links.
     *
     * @return the responsibility links
     */
    public List<FooterLink> getResponsibilityLinks() {
        return responsibilityLinks;
    }

    /**
     * Gets the career links.
     *
     * @return the career links
     */
    public List<FooterLink> getCareerLinks() {
        return careerLinks;
    }

    /**
     * Gets the news room links.
     *
     * @return the news room links
     */
    public List<FooterLink> getNewsRoomLinks() {
        return newsRoomLinks;
    }

    /**
     * Gets the home links.
     *
     * @return the home links
     */
    public List<FooterLink> getHomeLinks() {
        return homeLinks;
    }

    /**
     * Gets the contact links.
     *
     * @return the contact links
     */
    public List<FooterLink> getContactLinks() {
        return contactLinks;
    }

    /**
     * Gets the investors links.
     *
     * @return the investors links
     */
    public List<FooterLink> getInvestorsLinks() {
        return investorsLinks;
    }

}
