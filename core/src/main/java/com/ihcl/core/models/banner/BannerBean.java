package com.ihcl.core.models.banner;

import java.util.List;

public class BannerBean {

    private String bannerDescDesk;

    private String bannerDescMob;

    private String bannerImage;

    private String bannerTitlePosition;

    private String bannerLinkText;

    private String bannerLinkUrl;

    private String bannerSubTitle;

    private String bannerSubTitlePosition;

    private String bannerDescPosition;

    private String bannerTitleFont;

    private String bannerTitleFontSize;

    private String bannerTitle;

    private String mobileImage;

    private String tintOpacity;

    private String bannerSubtitleFont;

    private String bannerSubtitleFontSize;

    private String bannerDescFont;

    private String bannerDescFontSize;

    private String bannerButtonLayout;

    private List<String> context;
    
    private String bannerRichText;


    public String getBannerDescDesk() {
        return bannerDescDesk;
    }


    public void setBannerDescDesk(String bannerDescDesk) {
        this.bannerDescDesk = bannerDescDesk;
    }


    public String getBannerDescMob() {
        return bannerDescMob;
    }


    public void setBannerDescMob(String bannerDescMob) {
        this.bannerDescMob = bannerDescMob;
    }


    public String getBannerImage() {
        return bannerImage;
    }


    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }


    public String getBannerTitlePosition() {
        return bannerTitlePosition;
    }


    public void setBannerTitlePosition(String bannerTitlePosition) {
        this.bannerTitlePosition = bannerTitlePosition;
    }


    public String getBannerLinkText() {
        return bannerLinkText;
    }


    public void setBannerLinkText(String bannerLinkText) {
        this.bannerLinkText = bannerLinkText;
    }


    public String getBannerLinkUrl() {
        return bannerLinkUrl;
    }


    public void setBannerLinkUrl(String bannerLinkUrl) {
        this.bannerLinkUrl = bannerLinkUrl;
    }


    public String getBannerSubTitle() {
        return bannerSubTitle;
    }


    public void setBannerSubTitle(String bannerSubTitle) {
        this.bannerSubTitle = bannerSubTitle;
    }


    public String getBannerSubTitlePosition() {
        return bannerSubTitlePosition;
    }


    public void setBannerSubTitlePosition(String bannerSubTitlePosition) {
        this.bannerSubTitlePosition = bannerSubTitlePosition;
    }


    public String getBannerDescPosition() {
        return bannerDescPosition;
    }


    public void setBannerDescPosition(String bannerDescPosition) {
        this.bannerDescPosition = bannerDescPosition;
    }


    public String getBannerTitleFont() {
        return bannerTitleFont;
    }


    public void setBannerTitleFont(String bannerTitleFont) {
        this.bannerTitleFont = bannerTitleFont;
    }


    public String getBannerTitleFontSize() {
        return bannerTitleFontSize;
    }


    public void setBannerTitleFontSize(String bannerTitleFontSize) {
        this.bannerTitleFontSize = bannerTitleFontSize;
    }


    public String getBannerTitle() {
        return bannerTitle;
    }


    public void setBannerTitle(String bannerTitle) {
        this.bannerTitle = bannerTitle;
    }


    public String getMobileImage() {
        return mobileImage;
    }


    public void setMobileImage(String mobileImage) {
        this.mobileImage = mobileImage;
    }


    public String getTintOpacity() {
        return tintOpacity;
    }


    public void setTintOpacity(String tintOpacity) {
        this.tintOpacity = tintOpacity;
    }


    public String getBannerSubtitleFont() {
        return bannerSubtitleFont;
    }


    public void setBannerSubtitleFont(String bannerSubtitleFont) {
        this.bannerSubtitleFont = bannerSubtitleFont;
    }


    public String getBannerSubtitleFontSize() {
        return bannerSubtitleFontSize;
    }


    public void setBannerSubtitleFontSize(String bannerSubtitleFontSize) {
        this.bannerSubtitleFontSize = bannerSubtitleFontSize;
    }


    public String getBannerDescFont() {
        return bannerDescFont;
    }


    public void setBannerDescFont(String bannerDescFont) {
        this.bannerDescFont = bannerDescFont;
    }


    public String getBannerDescFontSize() {
        return bannerDescFontSize;
    }


    public void setBannerDescFontSize(String bannerDescFontSize) {
        this.bannerDescFontSize = bannerDescFontSize;
    }


    public String getBannerButtonLayout() {
        return bannerButtonLayout;
    }


    public void setBannerButtonLayout(String bannerButtonLayout) {
        this.bannerButtonLayout = bannerButtonLayout;
    }


    public List<String> getContext() {
        return context;
    }


    public void setContext(List<String> context) {
        this.context = context;
    }


    public BannerBean(String bannerDescDesk, String bannerDescMob, String bannerImage, String bannerTitlePosition,
            String bannerLinkText, String bannerLinkUrl, String bannerSubTitle, String bannerSubTitlePosition,
            String bannerDescPosition, String bannerTitleFont, String bannerTitleFontSize, String bannerTitle,
            String mobileImage, String tintOpacity, String bannerSubtitleFont, String bannerSubtitleFontSize,
            String bannerDescFont, String bannerDescFontSize, String bannerButtonLayout, String bannerRichText, List<String> context) {
        super();
        this.bannerDescDesk = bannerDescDesk;
        this.bannerDescMob = bannerDescMob;
        this.bannerImage = bannerImage;
        this.bannerTitlePosition = bannerTitlePosition;
        this.bannerLinkText = bannerLinkText;
        this.bannerLinkUrl = bannerLinkUrl;
        this.bannerSubTitle = bannerSubTitle;
        this.bannerSubTitlePosition = bannerSubTitlePosition;
        this.bannerDescPosition = bannerDescPosition;
        this.bannerTitleFont = bannerTitleFont;
        this.bannerTitleFontSize = bannerTitleFontSize;
        this.bannerTitle = bannerTitle;
        this.mobileImage = mobileImage;
        this.tintOpacity = tintOpacity;
        this.bannerSubtitleFont = bannerSubtitleFont;
        this.bannerSubtitleFontSize = bannerSubtitleFontSize;
        this.bannerDescFont = bannerDescFont;
        this.bannerDescFontSize = bannerDescFontSize;
        this.bannerButtonLayout = bannerButtonLayout;
        this.bannerRichText = bannerRichText;
        this.context = context;
    }


	public String getBannerRichText() {
		return bannerRichText;
	}


	public void setBannerRichText(String bannerRichText) {
		this.bannerRichText = bannerRichText;
	}


}
