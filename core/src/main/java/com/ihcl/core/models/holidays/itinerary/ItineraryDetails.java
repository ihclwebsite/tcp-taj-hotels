package com.ihcl.core.models.holidays.itinerary;

import java.util.List;

import org.json.JSONObject;

public class ItineraryDetails {
	private String itineraryTitle;

	private String itineraryDescription;

	private List<String> itineraryTags;

	private List<String> itineraryTypes;

	private List<String> itineraryPlaces;

	private List<String> itineraryImages;

	private int sizeOfItineraryPlaces;

	private int sizeOfItineraryImages;

	private int sizeOfItineraryTypes;

	private int sizeOfItineraryTags;

	private List<String> firstThreeImages;

	private List<JSONObject> itineraryImageValues;

	public String getItineraryTitle() {
		return itineraryTitle;
	}

	public void setItineraryTitle(String itineraryTitle) {
		this.itineraryTitle = itineraryTitle;
	}

	public String getItineraryDescription() {
		return itineraryDescription;
	}

	public void setItineraryDescription(String itineraryDescription) {
		this.itineraryDescription = itineraryDescription;
	}

	public List<String> getItineraryTags() {
		return itineraryTags;
	}

	public void setItineraryTags(List<String> itineraryTags) {
		this.itineraryTags = itineraryTags;
	}

	public List<String> getItineraryTypes() {
		return itineraryTypes;
	}

	public void setItineraryTypes(List<String> itineraryTypes) {
		this.itineraryTypes = itineraryTypes;
	}

	public List<String> getItineraryPlaces() {
		return itineraryPlaces;
	}

	public void setItineraryPlaces(List<String> itineraryPlaces) {
		this.itineraryPlaces = itineraryPlaces;
	}

	public List<String> getItineraryImages() {
		return itineraryImages;
	}

	public void setItineraryImages(List<String> listOfImageList) {
		this.itineraryImages = listOfImageList;
	}

	public int getSizeOfItineraryPlaces() {
		return sizeOfItineraryPlaces;
	}

	public int getSizeOfItineraryImages() {
		return sizeOfItineraryImages;
	}

	public int getSizeOfItineraryTypes() {
		return sizeOfItineraryTypes;
	}

	public int getSizeOfItineraryTags() {
		return sizeOfItineraryTags;
	}

	public void setSizeOfItineraryPlaces(int sizeOfItineraryPlaces) {
		this.sizeOfItineraryPlaces = sizeOfItineraryPlaces;
	}

	public void setSizeOfItineraryImages(int sizeOfItineraryImages) {
		this.sizeOfItineraryImages = sizeOfItineraryImages;
	}

	public void setSizeOfItineraryTypes(int sizeOfItineraryTypes) {
		this.sizeOfItineraryTypes = sizeOfItineraryTypes;
	}

	public void setSizeOfItineraryTags(int sizeOfItineraryTags) {
		this.sizeOfItineraryTags = sizeOfItineraryTags;
	}

	public List<String> getFirstThreeImages() {
		return firstThreeImages;
	}

	public void setFirstThreeImages(List<String> firstThreeImages) {
		this.firstThreeImages = firstThreeImages;
	}

	public List<JSONObject> getItineraryImageValues() {
		return itineraryImageValues;
	}

	public void setItineraryImageValues(List<JSONObject> itineraryImageValues) {
		this.itineraryImageValues = itineraryImageValues;
	}

}
