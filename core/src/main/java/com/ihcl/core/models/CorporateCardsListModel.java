/**
 *
 */
package com.ihcl.core.models;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

import com.ihcl.core.models.ihcl.KeyFiguresBean;

/**
 * @author moonraft
 *
 */
public interface CorporateCardsListModel {

    /**
     * @return
     */
    ValueMap getValueMap();

    List<KeyFiguresBean> getKeyFiguresList();

}
