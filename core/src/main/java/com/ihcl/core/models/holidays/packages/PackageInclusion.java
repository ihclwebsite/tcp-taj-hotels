package com.ihcl.core.models.holidays.packages;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Required;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class)
public class PackageInclusion {
	
	protected static final Logger LOG = LoggerFactory.getLogger(PackageInclusion.class);
	
	@Inject
    @Required
    @Named("inclusionImageMap")
    public String inclusionImageMap;
	
	@Inject
    @Optional
    @Named("inclusionDescription")
    public String inclusionDescription;

	private String inclusionTitle;
	
	private String inclusionImagePath;
	
	@PostConstruct
    public void init() {
		String methodName ="init";
		LOG.trace("Method Entry : "+methodName);
		String delimiter = ":";
		String[] nameImageMap = inclusionImageMap.split(delimiter);
		if(nameImageMap.length == 2) {
			inclusionTitle=nameImageMap[0];
			inclusionImagePath=nameImageMap[1];
		}
		LOG.trace("Method Exit : "+methodName);
	}
	
	public String getInclusionDescription() {
		return inclusionDescription;
	}

	public String getInclusionTitle() {
		return inclusionTitle;
	}

	public String getInclusionImagePath() {
		return inclusionImagePath;
	}

}
