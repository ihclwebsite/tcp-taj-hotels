package com.ihcl.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class SearchSuggestion {
    @Inject
    @Optional
    private String searchCategory;

    @Inject
    @Optional
    private List<String> suggestions;

    public String getSearchCategory() {
        return searchCategory;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }
}
