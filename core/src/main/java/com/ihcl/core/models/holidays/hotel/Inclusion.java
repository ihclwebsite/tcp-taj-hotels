package com.ihcl.core.models.holidays.hotel;

public class Inclusion {
	
	private String inclusionTitle;
	
	private String inclusionImagePath;
	
	private String inclusionDescription;

	public String getInclusionTitle() {
		return inclusionTitle;
	}

	public void setInclusionTitle(String inclusionTitle) {
		this.inclusionTitle = inclusionTitle;
	}

	public String getInclusionImagePath() {
		return inclusionImagePath;
	}

	public void setInclusionImagePath(String inclusionImagePath) {
		this.inclusionImagePath = inclusionImagePath;
	}

	public String getInclusionDescription() {
		return inclusionDescription;
	}

	public void setInclusionDescription(String inclusionDescription) {
		this.inclusionDescription = inclusionDescription;
	}
	
}

