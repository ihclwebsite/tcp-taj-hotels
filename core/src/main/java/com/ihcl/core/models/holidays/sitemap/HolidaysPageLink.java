package com.ihcl.core.models.holidays.sitemap;

public class HolidaysPageLink implements Comparable<Object> {

	private String pageName;
	
	private String pageUrl;

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	@Override
	public int compareTo(Object o) {
		return this.getPageName().compareTo(((HolidaysPageLink) o).getPageName());
	}
}
