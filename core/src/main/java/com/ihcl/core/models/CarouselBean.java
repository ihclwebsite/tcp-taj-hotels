package com.ihcl.core.models;


public class CarouselBean {
    
    private String title;
    private String imagepath;
    private String description;
    private int index;
    
    
    public int getIndex() {
        return index;
    }

    
    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getImagepath() {
        return imagepath;
    }
    
    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    


}
