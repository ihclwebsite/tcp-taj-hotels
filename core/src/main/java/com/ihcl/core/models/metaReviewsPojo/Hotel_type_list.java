/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */

public class Hotel_type_list {

    private String sentiment;

    private String text;

    private String global_popularity;

    private Highlight_list[] highlight_list;

    private String category_name;

    private Sub_category_list[] sub_category_list;

    private String score;

    private String category_id;

    private String short_text;

    private String popularity;

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getGlobal_popularity() {
        return global_popularity;
    }

    public void setGlobal_popularity(String global_popularity) {
        this.global_popularity = global_popularity;
    }

    public Highlight_list[] getHighlight_list() {
        return highlight_list;
    }

    public void setHighlight_list(Highlight_list[] highlight_list) {
        this.highlight_list = highlight_list;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Sub_category_list[] getSub_category_list() {
        return sub_category_list;
    }

    public void setSub_category_list(Sub_category_list[] sub_category_list) {
        this.sub_category_list = sub_category_list;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getShort_text() {
        return short_text;
    }

    public void setShort_text(String short_text) {
        this.short_text = short_text;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return "ClassPojo [sentiment = " + sentiment + ", text = " + text + ", global_popularity = " + global_popularity
                + ", highlight_list = " + highlight_list + ", category_name = " + category_name
                + ", sub_category_list = " + sub_category_list + ", score = " + score + ", category_id = " + category_id
                + ", short_text = " + short_text + ", popularity = " + popularity + "]";
    }
}
