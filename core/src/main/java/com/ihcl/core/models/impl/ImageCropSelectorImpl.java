/**
 *
 */
package com.ihcl.core.models.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.Value;
import com.ihcl.core.models.ImageCropSelector;

/**
 * @author Srikanta.moonraft
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = ImageCropSelector.class)
public class ImageCropSelectorImpl implements ImageCropSelector {

    private static final Logger LOG = LoggerFactory.getLogger(ImageCropSelectorImpl.class);

    private static final String BANNER_ASPECT = "849_249";


    @Inject
    SlingHttpServletRequest request;


    @Inject
    @Optional
    private String hotelName;

    @Inject
    @Optional
    @Named("bannerImage")
    @Default(values = "No Image selected")
    private String bannerImage;

    @Inject
    ResourceResolver resourceResolver;

    @Inject
    @Optional
    private Resource resource;

    private ValueMap valueMap;

    private Map<String, Number> map;

    @Inject
    @Optional
    @Named("name")
    private String name;

    @Inject
    @Optional
    private String label;

    @Inject
    @Optional
    private String path;

    @Inject
    @Optional
    private Boolean isCropped;

    private String suffix;

    private String aspect = "1.0";


    @PostConstruct
    public void activate() {
        LOG.trace("Entry -> Method[ activate() ] : @PostConstruct");
        if (resource != null) {
            isCropped = false;
            LOG.trace(" Resource is :" + resource);
            ValueMap adaptTo = resource.adaptTo(ValueMap.class);
            String imagePathToCrop = adaptTo.get("name", String.class);
            aspect = adaptTo.get("aspect", String.class);
            name = imagePathToCrop;
            if (!name.contains("./")) {
                name = "./" + name;
            }
            label = adaptTo.get("label", String.class);
            LOG.trace("Image to crop is : " + imagePathToCrop);
            LOG.trace("name and label of the cropimageselector is : " + name + " : " + label);
            // initializing to default values
            Integer xAxis = 20, yAxis = 20, width = 800, height = 200;
            String cropperAspect = "840_249";
            String[] orientation;
            String resourcePath = (String) request.getAttribute(Value.CONTENTPATH_ATTRIBUTE);
            LOG.debug("Resource Path from Request : " + resourcePath);
            path = resourcePath;
            resource = resourceResolver.getResource(resourcePath);

            valueMap = resource.getValueMap();
            imagePathToCrop = imagePathToCrop.replace("./", "");
            suffix = imagePathToCrop + resourcePath.substring(resourcePath.lastIndexOf('/') + 1);
            LOG.trace("Value Map --: " + valueMap);
            if (valueMap.containsKey(imagePathToCrop)) {
                bannerImage = valueMap.get(imagePathToCrop, String.class);
                if (bannerImage.contains("renditions")) {
                    isCropped = true;
                    path = bannerImage;
                    for (int i = 1; i <= 3; i++) {
                        path = path.substring(0, path.lastIndexOf('/'));
                    }
                }
                LOG.debug("bannerImage from ValueMap: " + bannerImage);
                if (valueMap.containsKey("width") && !valueMap.get("xAxis", String.class).equals("NaN")) {
                    xAxis = valueMap.get("xAxis", Integer.class);
                    yAxis = valueMap.get("yAxis", Integer.class);
                    width = valueMap.get("width", Integer.class);
                    height = valueMap.get("height", Integer.class);
                    // cropperAspect = valueMap.get("cropperAspect", String.class);
                    // orientation = cropperAspect.split("_");
                    // width = Integer.parseInt(orientation[0]);
                    // height = Integer.parseInt(orientation[1]);
                    LOG.debug("x, y, width, height, Aspect : " + xAxis + ", " + yAxis + ", " + width + ", " + height
                            + ", " + cropperAspect);
                } else {
                    xAxis = 20;
                    yAxis = 20;
                    width = 820;
                    height = 220;
                    cropperAspect = BANNER_ASPECT;
                    LOG.debug(" Set to Default Value (Probably first time ): x, y, width, height, Aspect : " + xAxis
                            + ", " + yAxis + ", " + width + ", " + height + ", " + cropperAspect);
                }
            }

            LOG.debug(" BannerImage is not Available (Probably First Time");
            map = new HashMap<>();
            map.put("xAxis", xAxis);
            map.put("yAxis", yAxis);
            map.put("width", width);
            map.put("height", height);

            // map.put("cropperAspect", cropperAspect);
            LOG.trace("Exit -> Method[ activate() ] : @PostConstruct");
        } else if (resource == null) {
            LOG.debug("Resource is Null, Can't proceed further. ");

        }
    }


    @Override
    public String getBannerImage() {
        LOG.trace("Entry -> Method[ getBannerImage() ] : @Override");
        return bannerImage;
    }

    @Override
    public ValueMap getCropBoxData() {
        LOG.trace("Entry -> Method[getCropBoxData() ] : @Override");
        return valueMap;
    }

    @Override
    public Map<String, Number> getCropBoxDataMap() {
        LOG.trace("Entry -> Method[getCropBoxDataMap() ] : @Override");
        return map;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getName()
     */
    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return name;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getLabel()
     */
    @Override
    public String getLabel() {
        // TODO Auto-generated method stub
        return label;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getPath()
     */
    @Override
    public String getPath() {
        // TODO Auto-generated method stub
        return path;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getIsCropped()
     */
    @Override
    public Boolean getIsCropped() {
        // TODO Auto-generated method stub
        return isCropped;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getSuffix()
     */
    @Override
    public String getSuffix() {
        // TODO Auto-generated method stub
        return suffix;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.ImageCropSelector#getAspect()
     */
    @Override
    public String getAspect() {
        // TODO Auto-generated method stub
        return aspect;
    }


}
