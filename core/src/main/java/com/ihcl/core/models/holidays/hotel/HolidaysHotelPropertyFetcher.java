package com.ihcl.core.models.holidays.hotel;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface HolidaysHotelPropertyFetcher {
	
	HotelDetails getHotelDetails();

}
