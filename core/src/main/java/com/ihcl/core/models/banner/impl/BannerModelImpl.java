package com.ihcl.core.models.banner.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.banner.BannerModel;

import javax.inject.Inject;

@Model(adaptables = Resource.class, adapters = BannerModel.class)
public class BannerModelImpl implements BannerModel {

    private static final Logger LOG = LoggerFactory.getLogger(BannerModelImpl.class);

    @Inject
    @Optional
    private String bannerImage;

    @Inject
    @Optional
    private String tintOpacity;

    @Inject
    @Optional
    private String bannerTitle;

    @Inject
    @Optional
    private String bannerDescDesk;

    @Inject
    @Optional
    private String bannerLinkText;

    @Inject
    @Optional
    private String bannerLinkUrl;

    @Inject
    @Optional
    private String bannerSubTitle;

    @Inject
    @Optional
    private String bfooterTitle;

    @Inject
    @Optional
    private String bfooterDesc;

    @Inject
    @Optional
    private String mobileImage;
    
    @Inject
    @Optional
    private String bannerRichText;

    @Inject
    @Optional
    private List<String> context;

    @Override
    public String getDate() {
        String methodName = "getDate";
        LOG.trace("Method Entry: " + methodName);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM YYYY");
        Date date = new Date();
        String formattedDate = formatter.format(date);
        LOG.trace("Method Exit: " + methodName);
        return formattedDate;
    }

    @Override
    public String getBannerImage() {
        return bannerImage;
    }

    @Override
    public String getTintOpacity() {
        return tintOpacity;
    }

    @Override
    public String getBannerTitle() {
        return bannerTitle;
    }

    @Override
    public String getBannerDescDesk() {
        return bannerDescDesk;
    }

    @Override
    public String getBannerLinkText() {
        return bannerLinkText;
    }

    @Override
    public String getBannerLinkUrl() {
        return bannerLinkUrl;
    }

    @Override
    public String getBannerSubTitle() {
        return bannerSubTitle;
    }

    @Override
    public String getBfooterTitle() {
        return bfooterTitle;
    }

    @Override
    public String getBfooterDesc() {
        return bfooterDesc;
    }

    @Override
    public String getMobileImage() {
        return mobileImage;
    }

    @Override
    public List<String> getContext() {
        return context;
    }

	@Override
	public String getBannerRichText() {
		// TODO Auto-generated method stub
		return bannerRichText;
	}
}
