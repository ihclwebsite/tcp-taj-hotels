/**
 *
 */
package com.ihcl.core.models.jivaspa;

import java.util.List;

import com.ihcl.core.models.ParticipatingHotel;

/**
 * @author moonraft
 *
 */
public interface LocateJivaSpaService {

    List<ParticipatingHotel> locateJivaSpa();
}
