/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(
        adaptables = Resource.class,
        adapters = IhclCarouselModel.class)

public class IhclCarouselModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclCarouselModel.class);

    @Self
    private Resource resource;

    public List<IhclCarouselBean> carouselList = new ArrayList<IhclCarouselBean>();


    /**
     * Getter for the field carouselList
     *
     * @return the carouselList
     */
    public List<IhclCarouselBean> getCarouselList() {
        return carouselList;
    }

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getCardValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @return
     *
     */
    private List<IhclCarouselBean> getCardValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] list = valueMap.get("list", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != list) {
            for (int i = 0; i < list.length; i++) {
                JsonElement jsonElement = gson.fromJson(list[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclCarouselBean listBean = new IhclCarouselBean();
                LOG.trace("jsonObject :" + jsonObject);
                if (i == 0) {
                    listBean.setClassList("carousel-item active");
                } else {
                    listBean.setClassList("carousel-item");
                }
                if (null != jsonObject.get("logolmage")) {
                    listBean.setLogoImage(jsonObject.get("logolmage").getAsString());
                }
                if (null != jsonObject.get("description")) {
                    listBean.setDescription(jsonObject.get("description").getAsString());
                }
                if (null != jsonObject.get("title")) {
                    listBean.setTitle(jsonObject.get("title").getAsString());
                }

                carouselList.add(listBean);

            }

        }
        return carouselList;
    }


}
