package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.ihcl.core.hotels.BrandDetailsBean;
import com.ihcl.core.hotels.DestinationDetailsBean;
import com.ihcl.core.hotels.DestinationHotelBean;
import com.ihcl.core.hotels.DestinationHotelMapBean;
import com.ihcl.core.hotels.HotelBean;
import com.ihcl.core.hotels.HotelDetailsBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;
import com.ihcl.core.models.DestinationsList;
import com.ihcl.core.models.destination.OurHotelDestinationBean;
import com.ihcl.core.services.search.GlobalSearchService;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = DestinationsList.class)
public class DestinationsListImpl implements DestinationsList {

    private static final Logger LOG = LoggerFactory.getLogger(DestinationsListImpl.class);

    @Inject
    GlobalSearchService globalSearchService;

    @Inject
    private TajhotelsHotelsBeanService tajhotelsHotelsBeanService;

    @Inject
    @Default(values="/content/tajhotels/en-in")
    private String siteConfigContentPath;
    
    @Inject
    @Optional
    private String otherWebsitePaths;
    

    @Self
    SlingHttpServletRequest request;

    private Resource resource;

    private List<String> destinations;
    
    private List<String> destinationNames;

    private Map<String, String> hotelTypes;

    private Map<String, String> countryMap;
    
    private List<OurHotelDestinationBean> destinationHotelsBean;
    
    private List<OurHotelDestinationBean> ticTapHotelsList;
    
    private List<OurHotelDestinationBean> tajholidaysTicHotelsList;
    
    private List<OurHotelDestinationBean> ticTapMeHotelsList;
    
    private List<OurHotelDestinationBean> ticHotelsBean;
    
    private List<OurHotelDestinationBean> ticRoomRedemptionList;
    


	@PostConstruct
    protected void init() {
        try {
        	Gson gson = new Gson();
    		
            resource = request.getResource();
            Map<String, DestinationHotelMapBean> websitesBean = tajhotelsHotelsBeanService.getWebsiteSpecificHotelsBean().getWebsiteHotelsMap();
            DestinationHotelMapBean websiteDestinations = websitesBean.get(siteConfigContentPath);
            Map<String, DestinationHotelMapBean> websiteSpecificBean = new HashMap<>();
            websiteSpecificBean.put(siteConfigContentPath, websiteDestinations);
            String otherPaths[] = null;
            String otherPath = null;
        	if (null != otherWebsitePaths) {
        		if (otherWebsitePaths.contains(",")) {
        			otherPaths = otherWebsitePaths.split(",");
        			for (String path : otherPaths) {
        				 DestinationHotelMapBean otherDestinations = websitesBean.get(path);
        				 websiteSpecificBean.put(path, otherDestinations);
        			}
        		} else {
        			otherPath = otherWebsitePaths;
        			DestinationHotelMapBean otherDestinations = websitesBean.get(otherPath);
   				 	websiteSpecificBean.put(otherPath, otherDestinations);
        		}
        	}
            getAllDestinations();
            destinationHotelsBean = getAllDestinationHotelsList(websiteSpecificBean);
            ticTapHotelsList = getAllTicHotelsList("tap", websiteSpecificBean);
            LOG.trace("Tap bean :: {}", gson.toJson(ticTapHotelsList));
            ticHotelsBean = getAllTicHotelsList("tic", websiteSpecificBean);
            LOG.trace("Tic Hotels bean :: {}", gson.toJson(ticHotelsBean));
            tajholidaysTicHotelsList = getAllTicHotelsList("taj-holiday-redemption", websiteSpecificBean);
            LOG.trace("Holidays Hotels bean :: {}", gson.toJson(tajholidaysTicHotelsList));
            ticTapMeHotelsList = getAllTicHotelsList("tappme", websiteSpecificBean);
            LOG.trace("TapMe Hotels bean :: {}", gson.toJson(ticTapMeHotelsList));
            
            ticRoomRedemptionList = getAllTicHotelsList("ticRoomRedemption", websiteSpecificBean);
            LOG.trace("Tic Redemption Bean :: {}", gson.toJson(ticRoomRedemptionList));
            hotelTypes = new HashMap<>();
            hotelTypes = globalSearchService.fetchAllHotelTypes();
            countryMap = new HashMap<>();
            countryMap = globalSearchService.fetchAllCountries();
        } catch (Exception e) {
            LOG.error("Exception Found in PostConstruct method of DestinationListImpl :: {}", e.getMessage());
            LOG.debug("Exception Found in PostConstruct method of DestinationListImpl :: {}", e);

        }
    }

	@Override
    public void getAllDestinations() {
        destinations = new ArrayList<>();
        destinationNames = new ArrayList<>();
        Map<String, DestinationHotelMapBean> websitesBean = tajhotelsHotelsBeanService.getWebsiteSpecificHotelsBean().getWebsiteHotelsMap();
    	Map<String, DestinationHotelBean> destinationMap = websitesBean.get(siteConfigContentPath).getDestinationMap();
    	for (Entry<String, DestinationHotelBean> entry : destinationMap.entrySet()) {
    		destinations.add(entry.getValue().getDestinationDetails().getDestinationPath());
    		destinationNames.add(entry.getKey());
    	}
    }
    
    
    public List<OurHotelDestinationBean> getAllDestinationHotelsList(Map<String, DestinationHotelMapBean> websitesBean) {
    	destinationHotelsBean = new ArrayList<>();
    	
		for (String destination : destinationNames) {
			Map<String, HotelBean> hotelsMap = new TreeMap<>();
			DestinationDetailsBean destinationDetailsBean = null;
			List<HotelDetailsBean> websiteHotelsList = new ArrayList<>();
			List<HotelDetailsBean> othersHotelsList = new ArrayList<>();
			for (Entry<String, DestinationHotelMapBean> entry : websitesBean.entrySet()) {
				Map<String, DestinationHotelBean> destinationMap = entry.getValue().getDestinationMap();
				DestinationHotelBean destinationObject = destinationMap.get(destination);
				if (null != destinationObject) {
					if (destinationObject.getDestinationDetails().getDestinationPath()
							.contains(siteConfigContentPath)) {
						destinationDetailsBean = destinationObject.getDestinationDetails();
						for (Entry<String, BrandDetailsBean> brandHotels : destinationObject.getBrandHotels()
								.entrySet()) {
							HotelBean hotelBean = brandHotels.getValue().getHotelsBean();
							if (null != hotelBean) {
								websiteHotelsList.addAll(hotelBean.getHotelsList());
							}
						}
					} else {
						for (Entry<String, BrandDetailsBean> brandHotels : destinationObject.getBrandHotels()
								.entrySet()) {
							HotelBean hotelBean = brandHotels.getValue().getHotelsBean();
							if (null != hotelBean) {
								othersHotelsList.addAll(hotelBean.getHotelsList());
							}
						}
					}
				}
			}
			HotelBean websiteHotels = new HotelBean(websiteHotelsList);
			HotelBean otherHotels = new HotelBean(othersHotelsList);
			int websiteHotelCount = websiteHotelsList.size();
			int otherHotelsCount = othersHotelsList.size();
			hotelsMap.put("website", websiteHotels);
			hotelsMap.put("others", otherHotels);
			OurHotelDestinationBean destinationBean = new OurHotelDestinationBean(destinationDetailsBean, hotelsMap, websiteHotelCount, otherHotelsCount, websiteHotelCount+otherHotelsCount);
			destinationHotelsBean.add(destinationBean);
		}
		return destinationHotelsBean;
    }
    
    
    public List<OurHotelDestinationBean> getAllTicHotelsList(String membershipType, Map<String, DestinationHotelMapBean> websitesBean) {
    	List<OurHotelDestinationBean> hotelsSearchList = new ArrayList<>();
		for (String destination : destinationNames) {
			Map<String, HotelBean> hotelsMap = new TreeMap<>();
			DestinationDetailsBean destinationDetailsBean = null;
			List<HotelDetailsBean> websiteHotelsList = new ArrayList<>();
			List<HotelDetailsBean> othersHotelsList = new ArrayList<>();
			for (Entry<String, DestinationHotelMapBean> entry : websitesBean.entrySet()) {Map<String, DestinationHotelBean> destinationMap = entry.getValue().getDestinationMap();
			DestinationHotelBean destinationObject = destinationMap.get(destination);
			if (null != destinationObject) {
				if (destinationObject.getDestinationDetails().getDestinationPath()
						.contains(siteConfigContentPath)) {
					destinationDetailsBean = destinationObject.getDestinationDetails();
					for (Entry<String, BrandDetailsBean> brandHotels : destinationObject.getBrandHotels()
							.entrySet()) {
						HotelBean hotelBean = brandHotels.getValue().getHotelsBean();
						if (null != hotelBean) {
							List<HotelDetailsBean> hotelsList = hotelBean.getHotelsList();
							for (HotelDetailsBean hotel : hotelsList) {
								if (membershipType.equals("tic")) {
									if (!hotel.getAdditionaldetails().getNoTic()) {
										websiteHotelsList.add(hotel);
									} 
								}  else if (membershipType.equals("ticRoomRedemption")) {
									if (!hotel.getAdditionaldetails().isRoomRedemptionTIC()) {
										websiteHotelsList.add(hotel);
									}
								}else if (membershipType.equals("tap")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tap")) {
										websiteHotelsList.add(hotel);
									}
								} else if (membershipType.equals("tappme")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tappme")) {
										websiteHotelsList.add(hotel);
									}
								} else if (membershipType.equals("taj-holiday-redemption")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tic-room-redemption/taj-holidays-redemption")) {
										websiteHotelsList.add(hotel);
									}
								}  
							}
						}
					}
				} else {
					for (Entry<String, BrandDetailsBean> brandHotels : destinationObject.getBrandHotels()
							.entrySet()) {
						HotelBean hotelBean = brandHotels.getValue().getHotelsBean();
						if (null != hotelBean) {
							List<HotelDetailsBean> hotelsList = hotelBean.getHotelsList();
							for (HotelDetailsBean hotel : hotelsList) {
								if (membershipType.equals("tic")) {
									if (!hotel.getAdditionaldetails().getNoTic()) {
										othersHotelsList.add(hotel);
									} 
								}  else if (membershipType.equals("ticRoomRedemption")) {
									if (!hotel.getAdditionaldetails().isRoomRedemptionTIC()) {
										othersHotelsList.add(hotel);
									}
								}else if (membershipType.equals("tap")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tap")) {
										othersHotelsList.add(hotel);
									}
								} else if (membershipType.equals("tappme")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tappme")) {
										othersHotelsList.add(hotel);
									}
								} else if (membershipType.equals("taj-holiday-redemption")) {
									List<String> tags = hotel.getAdditionaldetails().getCqTags();
									if (tags.contains("taj:tic-room-redemption/taj-holidays-redemption")) {
										othersHotelsList.add(hotel);
									}
								}  
							}
						}
					}
				}
			}}
			HotelBean websiteHotels = new HotelBean(websiteHotelsList);
			HotelBean otherHotels = new HotelBean(othersHotelsList);
			int websiteHotelCount = websiteHotelsList.size();
			int otherHotelsCount = othersHotelsList.size();
			hotelsMap.put("website", websiteHotels);
			hotelsMap.put("others", otherHotels);
			OurHotelDestinationBean destinationBean = new OurHotelDestinationBean(destinationDetailsBean, hotelsMap, websiteHotelCount, otherHotelsCount, websiteHotelCount+otherHotelsCount);
			hotelsSearchList.add(destinationBean);
		}
		return hotelsSearchList;
    }

    @Override
    public List<OurHotelDestinationBean> getDestinationHotelsBean() {
		return destinationHotelsBean;
	}

    @Override
    public List<OurHotelDestinationBean> getTicHotelsBean() {
		return ticHotelsBean;
	}
    
	@Override
    public List<String> getDestinations() {
        return destinations;
    }

    @Override
    public Map<String, String> getHotelTypes() {
        return hotelTypes;
    }

    @Override
    public Map<String, String> getCountryMap() {
        return countryMap;
    }

    @Override
	public List<OurHotelDestinationBean> getTajholidaysTicHotelsList() {
		return tajholidaysTicHotelsList;
	}

    @Override
    public List<OurHotelDestinationBean> getTicTapMeHotelsList() {
		return ticTapMeHotelsList;
	}

    @Override
	public List<OurHotelDestinationBean> getTicTapHotelsList() {
		return ticTapHotelsList;
	}
    
    @Override
    public List<OurHotelDestinationBean> getTicRoomRedemptionList() {
		return ticRoomRedemptionList;
	}


}
