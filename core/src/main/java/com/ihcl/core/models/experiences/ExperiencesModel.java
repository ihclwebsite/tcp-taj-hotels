package com.ihcl.core.models.experiences;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = Resource.class,
        adapters = ExperiencesModel.class)
public class ExperiencesModel {

    private static final Logger LOG = LoggerFactory.getLogger(ExperienceListModel.class);

    private static final String EXPERIENCES_RESOURCE_TYPE = "tajhotels/components/content/experience-details";

    private static final String EXPERIENCES_LIST_RESOURCE_TYPE = "tajhotels/components/structure/experiences-list-page";

    @Self
    private Resource resource;

    private String experienceName;

    private String imagepath;

    private String description;

    private String experienceType;

    private String experienceLongDescription;

    private String experienceAddress;

    private String experienceId;

    private String hotelExperiencesPath;

    private String hotelName;

    private String hotelCity;

    private String draggedImagePath;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    /**
     * @return the experienceId
     */
    public String getExperienceId() {
        return experienceId;
    }

    @PostConstruct
    public void init() {
        Resource hotelExperiencesList = resourceFetcherService.getParentOfType(resource, EXPERIENCES_LIST_RESOURCE_TYPE)
                .getParent();
        hotelExperiencesPath = hotelExperiencesList.getPath();
        // if injected resource is of experiences resource type, set properties using resource
        // else loop child properties and find the experiences resource type
        if (resource.getResourceType().equals(EXPERIENCES_RESOURCE_TYPE)) {
            initProperties(resource);
        } else {
            Resource root = resource.getChild("root");
            for (Resource child : root.getChildren()) {
                if (child.getResourceType().equals(EXPERIENCES_RESOURCE_TYPE)) {
                    initProperties(child);
                }
            }
        }
    }

    public void initProperties(Resource resource) {
        ValueMap valueMap = resource.getValueMap();
        description = valueMap.get("experienceDescription", String.class);
        experienceLongDescription = valueMap.get("experienceLongDescription", String.class);
        experienceName = valueMap.get("experienceName", String.class);
        experienceType = valueMap.get("experienceType", String.class);
        imagepath = valueMap.get("experienceImage", String.class);
        draggedImagePath = valueMap.get("fileReference", String.class);
        experienceId = experienceName.replaceAll(" ", "").replaceAll(",", "");
        experienceAddress = valueMap.get("experienceAddress", String.class);
        getHotelProperties();
    }

    private void getHotelProperties() {
        Resource hotelDetails = resourceFetcherService.getParentOfType(resource,
                CrxConstants.HOTEL_LANDING_PAGE_RESOURCETYPE);
        ParticipatingHotel parentHotel = hotelDetails.adaptTo(ParticipatingHotel.class);
        hotelName = parentHotel.getHotelName();
        hotelCity = parentHotel.getHotelCity();
    }


    public String getImagepath() {
        return imagepath;
    }

    public String getDraggedImagePath() {
        return draggedImagePath;
    }

    public String getDescription() {
        return description;
    }

    public String getExperienceName() {
        return experienceName;
    }

    public String getExperienceType() {
        return experienceType;
    }

    public String getExperienceLongDescription() {
        return experienceLongDescription;
    }


    public String getExperienceAddress() {
        return experienceAddress;
    }

    public String getHotelExperiencesPath() {
        return hotelExperiencesPath;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCity() {
        return hotelCity;
    }
}
