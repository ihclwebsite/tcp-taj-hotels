package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;


@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = DAMImageWrapper.class)
public class DAMImageWrapper {

    private static final String ALT_TEXT_META_TAG = "dc:altText";

    private static final String DC_TITLE_META_TAG = "dc:title";

    private static final String EXTENSION_FORMAT_PNG = "png";

    private static final String EXTENSION_FORMAT_JPEG = "jpeg";

    private static final String EXTENSION_FORMAT_JPG = "jpg";

    private static final Logger LOG = LoggerFactory.getLogger(DAMImageWrapper.class);

    private static ScriptEngine engine = new ScriptEngineManager(null).getEngineByName("javascript");

    @Inject
    SlingHttpServletRequest request;

    @Inject
    @Optional
    String imageReference;

    @Inject
    @Default(values = "")
    String renditionPathSuffix;

    @Inject
    @Optional
    String mobileImageReference;

    @Inject
    @Optional
    String mobileRenditionPathSuffix;

    @Inject
    @Optional
    boolean encodeImagePath;

    String alternateText;

    String title;

    String mobileAlternateText;

    String mobileTitle;

    String renditionPath;

    String mobileRenditionPath;

    @PostConstruct
    private void processDamImageDetails() {
        try {
            Resource assetResource = request.getResourceResolver().getResource(imageReference);
            Resource mobileAssetResource = request.getResourceResolver().getResource(mobileImageReference);
            if (null != assetResource) {
                Asset asset = assetResource.adaptTo(Asset.class);
                if (null != asset) {
                    title = asset.getMetadataValue(DC_TITLE_META_TAG);
                    alternateText = asset.getMetadataValue(ALT_TEXT_META_TAG);
                    if (null != renditionPathSuffix && !renditionPathSuffix.equalsIgnoreCase("")) {
                        renditionPath = getRenditionPath(asset, renditionPathSuffix);
                    } else {
                        renditionPath = imageReference;
                    }
                    if (encodeImagePath) {
                        renditionPath = encodeImageUri(renditionPath);
                    }
                }
            }
            if (null != mobileAssetResource) {
                Asset mobileAsset = mobileAssetResource.adaptTo(Asset.class);
                if (null != mobileAsset) {
                    mobileTitle = mobileAsset.getMetadataValue(DC_TITLE_META_TAG);
                    mobileAlternateText = mobileAsset.getMetadataValue(ALT_TEXT_META_TAG);
                    mobileRenditionPath = getRenditionPath(mobileAsset,
                            null != mobileRenditionPathSuffix ? mobileRenditionPathSuffix : renditionPathSuffix);
                    if (encodeImagePath) {
                        mobileRenditionPath = encodeImageUri(mobileRenditionPath);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Resource not found :: {}", e.getMessage());
            LOG.debug("Resource not found :: {}", e);
        }
    }


    /**
     * @param value
     * @param string
     * @param i
     * @return splitValue
     */
    private String getSplittedValue(String value, String string, int index) {
        String[] arr = value.split(string);
        return arr[index];

    }


    public String getAlternateText() {
        return alternateText;
    }


    public String getTitle() {
        return title;
    }


    public String getMobileAlternateText() {
        return mobileAlternateText;
    }


    public String getMobileTitle() {
        return mobileTitle;
    }


    public String getRenditionPath() {
        return renditionPath;
    }


    public String getMobileRenditionPath() {
        return mobileRenditionPath;
    }


    /**
     * @param mobileAsset
     * @param suffix
     * @return renditionPath
     */
    private String getRenditionPath(Asset asset, String suffix) {
        String path = asset.getPath();
        String extension = getSplittedValue(path, "\\.", 1);
        String updatedSuffix = updateRenditionSuffix(extension, suffix);
        if (checkUpdatedRendition(asset, updatedSuffix)) {
            return path.concat(updatedSuffix);
        } else
            return path;
    }

    /**
     * @param asset
     * @param updatedSuffix
     * @return flag
     */
    private boolean checkUpdatedRendition(Asset asset, String updatedSuffix) {
        String renditionType = getSplittedValue(updatedSuffix, "renditions/", 1);
        Rendition rendition = asset.getRendition(renditionType);
        return null != rendition ? true : false;
    }


    /**
     * @param extension
     * @param suffix
     * @return updatedRenditionSuffix
     */
    private String updateRenditionSuffix(String extension, String suffix) {
        String updatedSuffix = null;
        switch (extension) {
            case EXTENSION_FORMAT_JPG:
                updatedSuffix = suffix.replace(EXTENSION_FORMAT_JPG, EXTENSION_FORMAT_JPEG);
                break;
            case EXTENSION_FORMAT_PNG:
                updatedSuffix = suffix.replace(EXTENSION_FORMAT_JPEG, EXTENSION_FORMAT_PNG);
                break;
            default:
                updatedSuffix = suffix;
                break;
        }
        return updatedSuffix;
    }

    /**
     * @param requestedRenditionPath
     * @return encodedRenditionPath
     */
    private String encodeImageUri(String requestedRenditionPath) {
        try {
            return engine.eval(String.format("escape(\"%s\")", requestedRenditionPath.replace("%20", " "))).toString()
                    .replace("%3A", ":").replace("%2F", "/").replace("%3B", ";").replace("%40", "@").replace("%3C", "<")
                    .replace("%3E", ">").replace("%3D", "=").replace("%26", "&").replace("%25", "%").replace("%24", "$")
                    .replace("%23", "#").replace("%2B", "+").replace("%2C", ",").replace("%3F", "?");
        } catch (ScriptException ex) {
            LOG.error("ScriptException :: {}", ex.getMessage());
            LOG.debug("ScriptException :: {}", ex);
            return null;
        } catch (Exception ex) {
            LOG.error("exception :: {}", ex.getMessage());
            LOG.debug("exception :: {}", ex);
            return null;
        }
    }

}

