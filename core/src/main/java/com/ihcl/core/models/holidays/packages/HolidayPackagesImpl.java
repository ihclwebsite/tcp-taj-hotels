/**
 *
 */
package com.ihcl.core.models.holidays.packages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.services.search.SearchProvider;

/**
 * @author moonraft
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = IHolidayPackages.class)
public class HolidayPackagesImpl implements IHolidayPackages {

    private static final Logger LOG = LoggerFactory.getLogger(HolidayPackagesImpl.class);

    private static final String PACKAGES_RESOURCE_TYPE = "tajhotels/components/structure/tajholidays-base-page";

    private static final String COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/holidays-packages-hotels";
    
    private static final String IMAGE_GALLERY_NODE_NAME = "galleryImages";

    @Inject
	Resource resource;
    
    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;
    
    @Inject
    private SearchProvider searchProvider;

    private List<ParticipatingHotelDetails> hotelDetails;

    private List<String> threeImagePaths;
    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: {}", methodName);
        Resource pageResource = resource;
		try {
			LOG.debug("initial resource path: {}", resource.getPath());
			ValueMap valueMap = pageResource.adaptTo(ValueMap.class);
			LOG.info("Trying to locate the page path where the component is added... ");

			while (!(valueMap.get("jcr:primaryType", String.class).equals("cq:Page"))) {
				LOG.debug("primary resource type not found as cq:Page. Moving to parent");
				pageResource = pageResource.getParent();
				valueMap = pageResource.adaptTo(ValueMap.class);
				LOG.debug("primarytype : {}", valueMap.get("jcr:primaryType", String.class));
			}
			LOG.debug("Page path : {}", pageResource.getPath());
            List<String> allResourcePath = fetchHotelsPaths( pageResource.getPath());

            hotelDetails = populateHotelsDetails(allResourcePath);
        } catch (Exception e) {
            LOG.error("Exception while building details for offers.:: {} ", e.getMessage());
        } 
		LOG.trace("HotelDetails added successfully");
        LOG.trace("Method Exit: {}", methodName);
    }

    private List<String> fetchHotelsPaths(String rootPath) {
        LOG.info("Inside method: fetchHotelsPaths()");

        List<String> resourcePaths = new ArrayList<>();
        try {
            HashMap<String, String> predicateMap = new HashMap<>();
            predicateMap.put("path", rootPath);
            predicateMap.put("property", "sling:resourceType");
            predicateMap.put("property.value", PACKAGES_RESOURCE_TYPE);
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            for (Hit hit : searchResult.getHits()) {
                LOG.debug("Found package resource at : {}", hit.getPath());
                resourcePaths.add(hit.getPath());
            }
        } catch (Exception e) {
            LOG.error("Exception occured searching for resourcePaths {}", e);
            e.printStackTrace();
        }
        LOG.info("Leaving method: fetchHotelsPaths()");
        return resourcePaths;
    }

    private List<ParticipatingHotelDetails> populateHotelsDetails(List<String> pathList) {

        List<ParticipatingHotelDetails> hotelsList = new LinkedList<>();
        LOG.info("Inside method: populateHotelsDetails()");

        try {
        	 ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        	 for (String resourcePath : pathList) {
                Resource resource = resourceResolver.getResource(resourcePath);
                Iterable<Resource> detailsNode = (resource.getChild("root")).getChildren();
                for (Resource child : detailsNode) {

                    LOG.debug("Iterating over child: {}", child);
                    LOG.debug("resource type : {}", child.getResourceType());
                    if (child.getResourceType().equals(COMPONENT_RESOURCE_TYPE)) {
                        LOG.debug("Found hotelDetails component at child: {}", child);
                        ParticipatingHotelDetails hotel = buildHotelDetails(child);
                        hotelsList.add(hotel);
                        if(!hotelsList.isEmpty()) {
                        	Collections.sort(hotelsList,(h1,h2)->h1.getHotelCity().toLowerCase().compareTo(h2.getHotelCity().toLowerCase()));
        				}
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error occured while mapping values to HotelDetails Object :: {}", e.getMessage());
        }
        LOG.info("Leaving method: populateHotelsDetails()");
        return hotelsList;
    }


    /**
     * @param child
     * @return
     * @throws IOException
     * @throws JSONException 
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    private ParticipatingHotelDetails buildHotelDetails(Resource child) throws IOException, JSONException {

        LOG.info("Inside method: buildHotelDetails()");

        ParticipatingHotelDetails buildHotel = new ParticipatingHotelDetails();
        ValueMap hotelValueMap = child.adaptTo(ValueMap.class);
        
        buildHotel.setHotelAreaName(getProperty(hotelValueMap,"hotelAreaName",String.class,""));
        buildHotel.setHotelCity(getProperty(hotelValueMap,"hotelCity",String.class,""));
        buildHotel.setHotelDescription(getProperty(hotelValueMap,"hotelDescription",String.class,""));
        buildHotel.setHotelEmail(getProperty(hotelValueMap,"hotelEmail",String.class,""));
        buildHotel.setHotelLatitude(getProperty(hotelValueMap,"hotelLatitude",String.class,""));
        buildHotel.setHotelLongitude(getProperty(hotelValueMap,"hotelLongitude",String.class,""));
        buildHotel.setHotelName(getProperty(hotelValueMap,"hotelName",String.class,""));
        buildHotel.setHotelPhone(getProperty(hotelValueMap,"hotelPhone",String.class,""));
        buildHotel.setStateName(getProperty(hotelValueMap,"stateName",String.class,""));
        buildHotel.setPhoneStdCode(getProperty(hotelValueMap,"phoneStdCode",String.class,""));
        buildHotel.setRoomTitle(getProperty(hotelValueMap,"roomTitle",String.class,""));
        if(getProperty(hotelValueMap,"isCityhotel",String.class,"") != null)
        	buildHotel.setIsCityHotel(getProperty(hotelValueMap,"isCityhotel",String.class,""));
        if(getProperty(hotelValueMap,"hotelId",String.class,"") != null)
        	buildHotel.setHotelId(getProperty(hotelValueMap,"hotelId",String.class,""));
        if(getProperty(hotelValueMap,"isRequired",String.class,"") != null)
        	buildHotel.setRequiredHotel(getProperty(hotelValueMap,"isRequired",String.class,""));
        if(getProperty(hotelValueMap,"roomCode",String.class,"") != null)
            buildHotel.setRoomCode(getProperty(hotelValueMap,"roomCode",String.class,""));
        if(getProperty(hotelValueMap,"category",String.class,"") != null)
            buildHotel.setCategory(getProperty(hotelValueMap,"category",String.class,""));
        buildHotel.setHotelImages(getImagesList(child));
        buildHotel.setFirstThreeImages(threeImagePaths);
        LOG.info("Leaving method: buildHotelDetails()");
        return buildHotel;
    }

    private List<JSONObject> getImagesList(Resource child) throws JSONException {
		
    	List<JSONObject> imagePathsasJSON=null;
    	threeImagePaths=new LinkedList<>();
    	
    	int count=0;
    	LOG.info("Inside method : getImagesList()");
    	LOG.debug("resource path : {}",child.getPath());
    	Resource galleryNode= child.getChild(IMAGE_GALLERY_NODE_NAME);
       if(galleryNode!=null){
            LOG.debug("resource name : {}", galleryNode.getName());
            imagePathsasJSON=galleryImageFetcherService.getGalleryImagePathsAt(galleryNode);
            for(JSONObject value: imagePathsasJSON){
            	if(count<3){
            		threeImagePaths.add(value.getString("imagePath"));
            		count++;
            	}
            }
       }else{
    	   LOG.error("gallery image component not found at this resource location.");
       }
    	LOG.info("Leaving method : getImagesList()");
		return imagePathsasJSON;
	}
    
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
		String methodName = "getProperty";
		LOG.trace("Method Entry: " + methodName);
		T value = defaultValue;
		if (valueMap.containsKey(key)) {
			value = valueMap.get(key, type);
		}
		LOG.trace("Value found for key: " + key + " : " + value);
		LOG.trace("Method Exit: " + methodName);
		return value;
	}

	@Override
    public List<ParticipatingHotelDetails> getHotelDetails() {
		return hotelDetails;
    }

}
