package com.ihcl.core.models;

import com.ihcl.core.jsonresponses.sealapi.Response;

public class SealApiResponse {

    private Meta meta;

    private Response response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }


    public Response getResponse() {
        return response;
    }


    public void setResponse(Response response) {
        this.response = response;
    }


}
