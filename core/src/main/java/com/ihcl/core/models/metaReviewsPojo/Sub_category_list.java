/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */

public class Sub_category_list {

    private String sentiment;

    private String text;

    private Highlight_list[] highlight_list;

    private String category_name;

    private String count;

    private String relevance;

    private String score;

    private String category_id;

    private String short_text;

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Highlight_list[] getHighlight_list() {
        return highlight_list;
    }

    public void setHighlight_list(Highlight_list[] highlight_list) {
        this.highlight_list = highlight_list;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getRelevance() {
        return relevance;
    }

    public void setRelevance(String relevance) {
        this.relevance = relevance;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getShort_text() {
        return short_text;
    }

    public void setShort_text(String short_text) {
        this.short_text = short_text;
    }

    @Override
    public String toString() {
        return "ClassPojo [sentiment = " + sentiment + ", text = " + text + ", highlight_list = " + highlight_list
                + ", category_name = " + category_name + ", count = " + count + ", relevance = " + relevance
                + ", score = " + score + ", category_id = " + category_id + ", short_text = " + short_text + "]";
    }
}
