/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclRankingBean {


    private String title;


    private String description;


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


}
