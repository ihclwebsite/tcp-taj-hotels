package com.ihcl.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.models.annotations.Optional;

/*Filter params are configured in the dialog for the filter component
 *Path - path to search / filter the pages 
 *Text - Full text to be matched against the results
 *ResourceType - Type of the resource to be searched & filtered*/
public class FilterParams {

	@Inject
	@Named("filterPath")
	@Optional
	private String filterPath;

	@Inject
	@Named("queryString")
	@Optional
	private String queryString;

	@Inject
	@Named("resourceType")
	@Optional
	private String resourceType;

	public String getFilterPath() {
		return filterPath;
	}

	public String getQueryString() {
		return queryString;
	}

	public String getResourceType() {
		return resourceType;
	}

}
