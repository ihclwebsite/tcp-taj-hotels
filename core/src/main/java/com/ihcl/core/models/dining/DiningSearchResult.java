package com.ihcl.core.models.dining;

import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class DiningSearchResult {

    private static final String DINING_NAME = "diningName";

    private String diningName;

    private String diningPath;

    private String hotelName;

    private String hotelCity;

    @Self
    private Resource resource;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    @PostConstruct
    public void init() {
        diningPath = resource.getParent().getPath();
        Resource diningDetails = resourceFetcherService.getChildrenOfType(resource, CrxConstants.DINING_DETAILS_COMPONENT_TYPE);
        ValueMap diningProperties = diningDetails.getValueMap();
        diningName = diningProperties.get(DINING_NAME, String.class);
        getHotelProperties();
    }

    private void getHotelProperties() {
        Resource hotelDetails = resourceFetcherService.getParentOfType(resource, CrxConstants.HOTEL_LANDING_PAGE_RESOURCETYPE);
        ParticipatingHotel parentHotel = hotelDetails.adaptTo(ParticipatingHotel.class);
        hotelName = parentHotel.getHotelName();
        hotelCity = parentHotel.getHotelCity();
    }

    public String getDiningName() {
        return diningName;
    }

    public String getDiningPath() {
        return diningPath;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCity() {
        return hotelCity;
    }
}
