package com.ihcl.core.models.rateandreview;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.ihcl.core.models.MetaReviewsApiResponse;
import com.ihcl.core.models.ReviewsApiResponse;
import com.ihcl.core.models.SealApiResponse;
import com.ihcl.core.models.metaReviewsPojo.FormattedCategoryListData;

@ConsumerType
public interface RateAndReview {
	
	SealApiResponse getSealApiResponse();
	
	ReviewsApiResponse getReviewsApiResponse();
	
	MetaReviewsApiResponse getMetaReviewsApiResponse();
	
	List<FormattedCategoryListData> getFormattedCategoryList();

}
