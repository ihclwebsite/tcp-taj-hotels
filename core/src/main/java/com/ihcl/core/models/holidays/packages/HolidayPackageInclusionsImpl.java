/**
 *
 */
package com.ihcl.core.models.holidays.packages;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.holidays.hotel.Inclusion;


/**
 * @author moonraft
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = IHolidayPackageInclusions.class)
public class HolidayPackageInclusionsImpl implements IHolidayPackageInclusions {

    private static final Logger LOG = LoggerFactory.getLogger(HolidayPackageInclusionsImpl.class);

    private static final String COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/holidays-package-inclusions";

    @Inject
    Resource resource;

    private List<Inclusion> allInclusions;

    @PostConstruct
    protected void init() throws IOException {

        LOG.info("Inside init() of class HolidayPackageInclusionsImpl");

        List<Inclusion> inclusionsList = new LinkedList<>();
        LOG.debug("resorce path : {}", resource.getPath());
        LOG.debug("resource type : {}", resource.getResourceType());
        if (resource.getResourceType().equals(COMPONENT_RESOURCE_TYPE)) {
            LOG.debug("Found inclusions component at child: {}", resource);
            ValueMap inclsionMap = resource.adaptTo(ValueMap.class);
            String[] inclusionsArray = inclsionMap.get("inclusions", String[].class);
            if (inclusionsArray != null && inclusionsArray.length > 0) {
                for (String jsonStr : inclusionsArray) {
                    ObjectMapper mapper = new ObjectMapper();
                    LOG.debug("JSON String : {}", jsonStr);
                    Inclusion inclusion = mapper.readValue(jsonStr, Inclusion.class);
                    inclusionsList.add(inclusion);
                }
            }
        }
        allInclusions = inclusionsList;
        LOG.info("list size : {}", allInclusions.size());
        LOG.info("Leaving init() mof class HolidayPackageInclusionsImpl");
    }


    @Override
    public List<Inclusion> getAllInclusions() {
        return allInclusions;
    }

}
