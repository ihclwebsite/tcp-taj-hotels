/**
 *
 */
package com.ihcl.core.models.campaigns;


public class CampaignSummerTabBean {


    private String tabName;

    private String tabtextlink;


    public String getTabName() {
        return tabName;
    }


    public void setTabName(String tabName) {
        this.tabName = tabName;
    }


    public String getTabtextlink() {
        return tabtextlink;
    }


    public void setTabtextlink(String tabtextlink) {
        this.tabtextlink = tabtextlink;
    }

}
