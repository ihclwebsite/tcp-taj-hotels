/**
 *
 */
package com.ihcl.core.models.meeting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.search.SearchProvider;

/**
 * <pre>
 * MeetingListModel.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :30-Aug-2019
 * @ClassName : MeetingListModel.java
 * @Description : com.ihcl.core.models.meeting -MeetingListModel.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  30-Aug-2019     Subharun Mukherjee            Create
 *
 *               </pre>
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = MeetingListModel.class)
public class MeetingListModel {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(MeetingListModel.class);

    /** The Constant MEETING_TEMPLATE. */
    private static final String MEETING_TEMPLATE = "/conf/tajhotels/settings/wcm/templates/taj-hotel-meeting-details-template";

    /** The current node. */
    @Inject
    @Optional
    private Node currentNode;

    /** The search provider. */
    @Inject
    private SearchProvider searchProvider;

    /** The resolver factory. */
    @Inject
    private ResourceResolverFactory resolverFactory;

    @Inject
    @Default(values = "/content/tajhotels/en-in/our-hotels")
    private String searchPath;

    @Inject
    @Optional
    private String isWeddingList;

    @Inject
    @Default(
            values = "/content/tajhotels/en-in/our-hotels,/content/seleqtions/en-in/our-hotels,/content/vivanta/en-in/our-hotels")
    private String allWebsitePaths;


    /**
     * Gets the meeting list.
     *
     * @return the meeting list
     */
    public List<String> getMeetingList() {

        List<String> meetingList = new ArrayList<>();
        ResourceResolver resolver = null;
        try {
            resolver = resolverFactory.getServiceResourceResolver(null);
            SearchResult results = null;
            if (StringUtils.isNotBlank(isWeddingList)) {
                if (allWebsitePaths.contains(",")) {
                    String paths[] = allWebsitePaths.split(",");
                    results = getSearchResult(null, paths);
                }

            } else {
                results = getSearchResult(searchPath, null);
            }
            if (null != results) {
                LOG.trace("Result size : {}", results.getHits().size());
                for (Hit hit : results.getHits()) {
                    try {
                        Resource resultResource = resolver.getResource(hit.getPath());
                        if (null != resultResource) {
                            meetingList.add(resultResource.getPath());
                        }
                    } catch (RepositoryException e) {
                        LOG.error("Error while returning meeting results : {}", e.getMessage());
                        LOG.debug("Error while returning meeting results : {}", e);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error getting resolver : {}", e.getMessage());
            LOG.debug("Error getting resolver : {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return meetingList;
    }

    /**
     * Gets the search result.
     *
     * @param searchPath
     *            the search path
     * @param allWebsites
     * @return the search result
     */
    private SearchResult getSearchResult(String searchPath, String[] paths) {
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("type", "cq:Page");
            if (null != searchPath) {
                predicateMap.put("path", searchPath);
                predicateMap.put("property", "jcr:content/cq:template");
                predicateMap.put("property.value", MEETING_TEMPLATE);
            } else {
                predicateMap.put("1_group.p.or", "true");
                for (int i = 0; i < paths.length; i++) {
                    predicateMap.put("1_group." + i + "_path", paths[i]);
                }
                predicateMap.put("2_property", "jcr:content/cq:template");
                predicateMap.put("2_property.value", MEETING_TEMPLATE);
            }

            predicateMap.put("p.limit", "-1");
            predicateMap.put("orderby", "@jcr:content/rank");
            predicateMap.put("orderby.sort", "desc");

        } catch (Exception e) {
            LOG.error("Error while fetching meeting results : {}", e.getMessage());
            LOG.debug("Error while fetching meeting results : {}", e);
        }

        return searchProvider.getQueryResult(predicateMap);
    }
}
