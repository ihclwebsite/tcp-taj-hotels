package com.ihcl.core.models.config;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface IhclcbApiConfigModel {

    String getIhclcbApiHostUrl();

    String getEntityDetailsFetchApiUrl();

    String getGuestDetailsFetchApiUrl();

    String getIhclcbBookingApiUrl();

}
