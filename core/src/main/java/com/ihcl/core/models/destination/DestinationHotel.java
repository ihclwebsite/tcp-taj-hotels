package com.ihcl.core.models.destination;

/**
 * @author Ravindar Dev
 *
 */
public class DestinationHotel {
	private String jcrPath;
	private String averageRate;
	private String discountedRate;
	
	public String getJcrPath() {
		return jcrPath;
	}
	public void setJcrPath(String jcrPath) {
		this.jcrPath = jcrPath;
	}
	public String getAverageRate() {
		return averageRate;
	}
	public void setAverageRate(String averageRate) {
		this.averageRate = averageRate;
	}
	public String getDiscountedRate() {
		return discountedRate;
	}
	public void setDiscountedRate(String discountedRate) {
		this.discountedRate = discountedRate;
	}
	
	@Override
	public String toString() {
		return "DestinationHotel [jcrPath=" + jcrPath + ", averageRate=" + averageRate + ", discountedRate="
				+ discountedRate + "]";
	}
	
	
	
}
