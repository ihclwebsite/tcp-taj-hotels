package com.ihcl.core.models.hotel;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

import com.ihcl.core.models.SignatureFeatureWithGroupIconModel;

@ConsumerType
public interface HotelPropertiesFetcher {

    String getName();

    String getHotelIdentifier();

    String getHotelLatitude();

    String getHotelLongitude();

    String getHotelArea();

    String getHotelCity();

    String getHotelPinCode();

    String getHotelStdCode();

    List<String> getHotelPhoneNumber();

    String getHotelEmail();

    String getHotelCountry();

    String getTrustYouId();

    String getHotelChainCode();

    String getHotelHeirarchy();

    String getHotelRoomsPath();

    String getGuestRoomPath();

    String getHotelState();

    String getJivaSpaEmail();

    String getRequestQuoteEmail();

    ValueMap getHotelDetailMap();

    String getHotelBrand();

    String getHotelLocale();

    String getJivaSpaBookingPath();

    ArrayList<SignatureFeatureWithGroupIconModel> getSignatureModel();

    String getSpaBookingPath();

    String getHotelLocationId();
    
    String getHotelsPath();
    
    String getNavTitle();
    
    String getRoomTypeCode();
    
    String getSameDayCheckout();
    
    String getisOnlyBungalowPage();
}
