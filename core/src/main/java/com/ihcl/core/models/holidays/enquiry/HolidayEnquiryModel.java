package com.ihcl.core.models.holidays.enquiry;

import java.util.List;

public class HolidayEnquiryModel {

	List<HolidayMultiFieldForm> listOfFields;

	List<String> genderOptions;

	public List<HolidayMultiFieldForm> getListOfFields() {
		return listOfFields;
	}

	public void setListOfFields(List<HolidayMultiFieldForm> listOfFields) {
		this.listOfFields = listOfFields;
	}

	public List<String> getGenderOptions() {
		return genderOptions;
	}

	public void setGenderOptions(List<String> genderOptions) {
		this.genderOptions = genderOptions;
	}
}
