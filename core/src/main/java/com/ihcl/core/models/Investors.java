package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.util.URLMapUtil;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Investors {
	
	private static final Logger LOG = LoggerFactory.getLogger(Investors.class);
	
	@Inject
	@Optional
	@Named("analystPath")
	private String analystReportDamPath;
	
	@Inject
	@Optional
	@Named("annualPath")
	private String annualReportDamPath;
	
	@Inject
	@Optional
	@Named("quarterPath")
	private String quarterReportDamPath;	
	
	String aboutUsInvestorsPath;
	
	@Self
	Resource resource;

	ResourceResolver resourceResolver;

	@PostConstruct
	protected void init() {
		resourceResolver=resource.getResourceResolver();
		LOG.trace("Method Entry >> init()");
		aboutUsInvestorsPath = getMappedPath(resource.getParent().getPath());
		LOG.trace("Investors path >> " + aboutUsInvestorsPath);
		LOG.trace("Method Exit >> init()");
	}
	
	private String getMappedPath(String url) {
		if (url != null) {
			String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
			return URLMapUtil.getPathFromURL(resolvedURL);
		}
		return url;
	}

	public String getAnalystPath() {
		return analystReportDamPath;
	}

	public String getAnnualPath() {
		return annualReportDamPath;
	}

	public String getQuarterPath() {
		return quarterReportDamPath;
	}

}
