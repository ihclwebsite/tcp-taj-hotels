package com.ihcl.core.models;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CorporateCompanyTab {

    protected static final Logger LOG = LoggerFactory.getLogger(CorporateCompanyTab.class);

    @Inject
    private List<String> subsidaryDomesticCompanyList;

    @Inject
    private List<String> internationalSubsidaryCompanyList;

    @Inject
    private List<String> associateDomesticCompanyList;

    @Inject
    private List<String> internationalAssociateCompanyList;

    @Inject
    private List<String> domesticJointVentureCompanyList;

    @Inject
    private List<String> internationalJointVentureCompanyList;

    @PostConstruct
    public void init() {
        String methodName = "init";
        LOG.info("Method Entry of CorporateCompanyTab :: " + methodName);
        LOG.info("Method Exit of CorporateCompanyTab :: " + methodName);
    }

    public List<String> getSubsidaryDomesticCompanyList() {
        return subsidaryDomesticCompanyList;
    }

    public List<String> getInternationalSubsidaryCompanyList() {
        return internationalSubsidaryCompanyList;
    }

    public List<String> getAssociateDomesticCompanyList() {
        return associateDomesticCompanyList;
    }

    public List<String> getInternationalAssociateCompanyList() {
        return internationalAssociateCompanyList;
    }

    public List<String> getDomesticJointVentureCompanyList() {
        return domesticJointVentureCompanyList;
    }

    public List<String> getInternationalJointVentureCompanyList() {
        return internationalJointVentureCompanyList;
    }

}
