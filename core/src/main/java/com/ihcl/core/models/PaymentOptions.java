package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;
   
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
   
import com.adobe.cq.sightly.WCMUsePojo;
 
import javax.jcr.Node;
import javax.jcr.NodeIterator; 
   
public class PaymentOptions extends WCMUsePojo {
   
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
 
	private List<PaymentOptionBean> multiFieldItems = new ArrayList<>();
	   
	@Override
	public void activate() throws Exception {
	 
	    Node currentNode = getResource().adaptTo(Node.class);
	    if(currentNode != null) {
	    	NodeIterator ni =  currentNode.getNodes() ;
		    while (ni.hasNext()) {
		        Node child = ni.nextNode();
		        if(child != null){
		        	NodeIterator ni2 =  child.getNodes() ;
			        setMultiFieldItems(ni2);
		        }
		        
		    }
	    }
	    
	}
   
	private List<PaymentOptionBean> setMultiFieldItems(NodeIterator ni2) {
	   
		try{	 
		    while (ni2.hasNext()) {
		        PaymentOptionBean menuItem = new PaymentOptionBean();
		        Node grandChild = ni2.nextNode();
		         
		        log.info("*** GRAND CHILD NODE PATH IS "+grandChild.getPath());
		        
		        if(grandChild.hasProperty("paymentType") && grandChild.hasProperty("paymentTypeItemField")) {
		        	menuItem.setPaymentOption(grandChild.getProperty("paymentType").getString());
		        	menuItem.setPaymentOptionItemField(grandChild.getProperty("paymentTypeItemField").getString());
		        	if(grandChild.hasProperty("paymentTypeDescription")) {
		        		menuItem.setPaymentOptionDescription(grandChild.getProperty("paymentTypeDescription").getString());
		        }
		        
		        	
		        }
		        
		        if((menuItem.getPaymentOption() != null) && (menuItem.getPaymentOptionItemField() != null)) {
		        	multiFieldItems.add(menuItem);
		        }
		    }
		}
		   
		catch(Exception e){
		    log.error("Exception while Multifield data {}", e.getMessage(), e);
		}
		
		return multiFieldItems;
	}
   
	public List<PaymentOptionBean> getMultiFieldItems() {
		
		return multiFieldItems;
	}
}