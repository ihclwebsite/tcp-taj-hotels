package com.ihcl.core.models.holidays.packages;

import java.util.Comparator;
import java.util.List;
import com.ihcl.core.models.holidays.hotel.Inclusion;

public class IncredibleEscapeCard {

	private String incredibleEscapeTitle;
	private String packageTitle;
	private String packageDesc;
	private String startDate;
	private String endDate;
	private String packageShortTitle;
	private String cities;
	private List<Inclusion> inclusions;
	private String packageLink;
	private String imagePath;
	private String packageRank;
	private String price;

	public String getPackageRank() {
		return packageRank;
	}

	public void setPackageRank(String packageRank) {
		this.packageRank = packageRank;
	}

	public String getIncredibleEscapeTitle() {
		return incredibleEscapeTitle;
	}

	public void setIncredibleEscapeTitle(String incredibleEscapeTitle) {
		this.incredibleEscapeTitle = incredibleEscapeTitle;
	}

	public String getPackageTitle() {
		return packageTitle;
	}

	public void setPackageTitle(String packageTitle) {
		this.packageTitle = packageTitle;
	}

	public String getPackageDesc() {
		return packageDesc;
	}

	public void setPackageDesc(String packageDesc) {
		this.packageDesc = packageDesc;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPackageShortTitle() {
		return packageShortTitle;
	}

	public void setPackageShortTitle(String packageShortTitle) {
		this.packageShortTitle = packageShortTitle;
	}

	public String getCities() {
		return cities;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}

	public List<Inclusion> getInclusions() {
		return inclusions;
	}

	public void setInclusions(List<Inclusion> inclusions) {
		this.inclusions = inclusions;
	}

	public String getPackageLink() {
		return packageLink;
	}

	public void setPackageLink(String packageLink) {
		this.packageLink = packageLink;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

}

class PackageRankComparator implements Comparator<Object> {

	@Override
	public int compare(Object arg0, Object arg1) {

		IncredibleEscapeCard obj1 = (IncredibleEscapeCard) arg0;
		IncredibleEscapeCard obj2 = (IncredibleEscapeCard) arg1;

		if (Integer.parseInt(obj1.getPackageRank()) == Integer.parseInt(obj2.getPackageRank())) {
			return 0;
		}

		else if (Integer.parseInt(obj1.getPackageRank()) > Integer.parseInt(obj2.getPackageRank())) {
			return 1;
		} else {
			return -1;
		}
	}
}
