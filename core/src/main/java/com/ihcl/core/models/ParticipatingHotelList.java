package com.ihcl.core.models;

import java.util.List;

public interface ParticipatingHotelList {

	List<String> getAllHotels();
}
