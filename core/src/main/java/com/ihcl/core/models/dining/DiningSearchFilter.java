package com.ihcl.core.models.dining;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.services.SpellCheck;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.core.services.search.SearchServiceConstants;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;

@Component(service = { Servlet.class },
property = { Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword search",
        "sling.servlet.paths=" + "/bin/dining-filter" })
public class DiningSearchFilter extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DiningSearchFilter.class);

    private static final String RESP_JSON_TYPE = "application/json";

    private static final String REQ_PARAMETER_SEARCH_KEY = "searchText";



    @Reference
    SpellCheck spellCheckService;

    @Reference
    private SearchProvider searchProvider;

    @Reference
    private QueryBuilder queryBuilder;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        try {
            String filter = request.getParameter("filter");
            String searchKeyInReq = request.getParameter(REQ_PARAMETER_SEARCH_KEY);
            String contentRootPath = request.getParameter(ReservationConstants.CONTENT_ROOT_PATH);
            if (StringUtils.isBlank(contentRootPath)) {
                contentRootPath = SearchServiceConstants.PATH.TAJ_ROOT;
            }
            String searchKey = searchKeyInReq + "*";
            ResourceResolver resourceResolver;

            resourceResolver = request.getResourceResolver();

            List<String> correctedWords = spellCheckService.performSpellCheck(searchKeyInReq);

            // Adding wildcard to search key
            if (correctedWords.size() > 0) {
                searchKey = correctedWords.get(0) + "*";
            }
            Map<String, ArrayList<Object>> categorizedResponse = new HashMap<>();
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayList<Object> restaurants = new ArrayList<>();
            HashMap<String, String> predicateMap = new HashMap<>();
            predicateMap.put("fulltext", searchKey);
            predicateMap.put("path", contentRootPath);
            predicateMap.put("property", "sling:resourceType");
            predicateMap.put("property.1_value", SearchServiceConstants.RESOURCETYPE.RESTAURANTS);
            predicateMap.put("orderby", "@jcr:score");
            predicateMap.put("orderby.sort", "desc");
            predicateMap.put("p.limit", "-1");
            com.day.cq.search.result.SearchResult searchResult = getQueryResult(predicateMap, resourceResolver);
            try {
                for (Hit hit : searchResult.getHits()) {
                    HashMap<String, String> resultObj = new HashMap<>();
                    Resource resultResource = hit.getResource();
                    if (filter.equalsIgnoreCase("all")) {
                        DiningSearchResult resultRest = resultResource.adaptTo(DiningSearchResult.class);
                        String restTitle = resultRest.getDiningName() + ", " + resultRest.getHotelName() + ", "
                                + resultRest.getHotelCity();
                        LOGGER.info("Result title and path :" + restTitle);
                        resultObj.put("title", restTitle);
                        resultObj.put("path", getMappedPath(resultRest.getDiningPath(), resourceResolver));
                        restaurants.add(resultObj);
                    } else if (getTagName(resultResource.getValueMap().get("servesCuisine", String[].class), filter, resourceResolver)) {
                        LOGGER.info("Cuisine match found :::::::::::");
                        DiningSearchResult resultRest = resultResource.adaptTo(DiningSearchResult.class);
                        String restTitle = resultRest.getDiningName() + ", " + resultRest.getHotelName() + ", "
                                + resultRest.getHotelCity();
                        LOGGER.info("Result title and path :" + restTitle);
                        resultObj.put("title", restTitle);
                        resultObj.put("path", getMappedPath(resultRest.getDiningPath(), resourceResolver));
                        restaurants.add(resultObj);
                    }
                }
                categorizedResponse.put("restaurants", restaurants);
                response.setContentType(RESP_JSON_TYPE);
                response.setCharacterEncoding(BookingConstants.CHARACTER_ENCOADING);
                response.getWriter().write(objectMapper.writeValueAsString(categorizedResponse));
            } catch (Exception e) {
                LOGGER.error("Error while searching for hotels with text= {}", e);
            }
        } catch (Exception e) {
            LOGGER.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}", e.getMessage());
        }
    }

    private boolean getTagName(String[] path, String filter, ResourceResolver resourceResolver) {

        boolean cuisineFilterCheck = false;
        Arrays.stream(path).forEach(x -> LOGGER.trace("The String Path: " + x));
        LOGGER.info("resourceResolver initialized");
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        LOGGER.info("Adapted to tag manager");
        for (int i = 0; i < path.length; i++) {
            LOGGER.info("I th path : " + path[i]);
            Tag tagRoot = tagManager.resolve(path[i]);
            LOGGER.info("Tag root has been set");
            if (tagRoot != null) {
                LOGGER.info("Cuisine TItle : " + tagRoot.getTitle());
                cuisineFilterCheck = tagRoot.getTitle().equalsIgnoreCase(filter);
            }
        }
        return cuisineFilterCheck;
    }

    private String getMappedPath(String url, ResourceResolver resourceResolver) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return URLMapUtil.getPathFromURL(resolvedURL);
        }
        return url;
    }

    public SearchResult getQueryResult(HashMap<String, String> predicates, ResourceResolver resourceResolver) {
        try {
            final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates),
                    resourceResolver.adaptTo(Session.class));
            return query.getResult();
        } catch (Exception e) {
            LOGGER.error(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}", e.getMessage());
            LOGGER.debug(
                    "Could not invoked search provider service. Please check service user configuration in the instance :: {}", e);
        }
        return null;
    }
}
