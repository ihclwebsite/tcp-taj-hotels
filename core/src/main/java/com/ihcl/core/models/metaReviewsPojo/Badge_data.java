/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */

public class Badge_data {

    private String global_popularity;

    private String popularity;

    public String getGlobal_popularity() {
        return global_popularity;
    }

    public void setGlobal_popularity(String global_popularity) {
        this.global_popularity = global_popularity;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return "ClassPojo [global_popularity = " + global_popularity + ", popularity = " + popularity + "]";
    }
}
