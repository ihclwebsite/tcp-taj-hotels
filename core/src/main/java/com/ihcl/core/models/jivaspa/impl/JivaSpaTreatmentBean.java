/**
 *
 */
package com.ihcl.core.models.jivaspa.impl;


/**
 * @author moonraft
 *
 */
public class JivaSpaTreatmentBean {

    private String treatmentAmount;

    private String treatmentName;

    private String treatmentDuration;

    private String treatmentCurrency;

    private String treatmentPath;

    public String getTreatmentAmount() {
        return treatmentAmount;
    }

    public void setTreatmentAmount(String treatmentAmount) {
        this.treatmentAmount = treatmentAmount;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentNane) {
        this.treatmentName = treatmentNane;
    }

    public String getTreatmentDuration() {
        return treatmentDuration;
    }

    public void setTreatmentDuration(String treatmentDuration) {
        this.treatmentDuration = treatmentDuration;
    }

    public String getTreatmentCurrency() {
        return treatmentCurrency;
    }

    public void setTreatmentCurrency(String treatmentCurrency) {
        this.treatmentCurrency = treatmentCurrency;
    }

    public String getTreatmentPath() {
        return treatmentPath;
    }

    public void setTreatmentPath(String treatmentPath) {
        this.treatmentPath = treatmentPath;
    }


}
