package com.ihcl.core.models.metaReviewsPojo;

public class Reviews_distribution {

    private String stars;

    private String reviews_count;

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(String reviews_count) {
        this.reviews_count = reviews_count;
    }

    @Override
    public String toString() {
        return "ClassPojo [stars = " + stars + ", reviews_count = " + reviews_count + "]";
    }
}
