/**
 *
 */
package com.ihcl.core.models.jivaspa.impl;


/**
 * @author moonraft
 *
 */
public class LocateJivaSpaBean {

    private String hotelName;

    private String hotelPath;

    private String treatmentDuration;

    private String hotelPhone;

    private String hotelAddress;

    private String hotelEmailId;

    private String hotelJivaSpaPath;

    private String hotelimagePath;

    public String getHotelName() {
        return hotelName;
    }


    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }


    public String getHotelPath() {
        return hotelPath;
    }


    public void setHotelPath(String hotelPath) {
        this.hotelPath = hotelPath;
    }


    public String getTreatmentDuration() {
        return treatmentDuration;
    }


    public void setTreatmentDuration(String treatmentDuration) {
        this.treatmentDuration = treatmentDuration;
    }


    public String getHotelPhone() {
        return hotelPhone;
    }


    public void setHotelPhone(String hotelPhone) {
        this.hotelPhone = hotelPhone;
    }


    public String getHotelAddress() {
        return hotelAddress;
    }


    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }


    public String getHotelEmailId() {
        return hotelEmailId;
    }

    public void setHotelEmailId(String hotelEmailId) {
        this.hotelEmailId = hotelEmailId;
    }


    public String getHotelJivaSpaPath() {
        return hotelJivaSpaPath;
    }

    public void setHotelJivaSpaPath(String hotelJivaSpaPath) {
        this.hotelJivaSpaPath = hotelJivaSpaPath;
    }

    public String getHotelimagePath() {
        return hotelimagePath;
    }

    public void setHotelimagePath(String hotelimagePath) {
        this.hotelimagePath = hotelimagePath;
    }
}
