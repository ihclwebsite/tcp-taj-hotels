/**
 *
 */
package com.ihcl.core.models.holidays.packages;

import java.util.List;

import org.json.JSONObject;

/**
 * @author moonraft
 *
 */
public class ParticipatingHotelDetails {

	private String hotelName;

	private String hotelCity;

	private String stateName;

	private String hotelAreaName;

	private String phoneStdCode;

	private String hotelDescription;

	private String hotelLatitude;

	private String hotelLongitude;

	private String roomTitle;

	private String hotelPhone;

	private String hotelEmail;
	
	private String isCityHotel;
	
	private String hotelId;
	
	private String requiredHotel;
	
	private String roomCode;
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	private String category;
	
	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getIsCityHotel() {
		return isCityHotel;
	}

	public void setIsCityHotel(String isCityHotel) {
		this.isCityHotel = isCityHotel;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getRequiredHotel() {
		return requiredHotel;
	}

	public void setRequiredHotel(String requiredHotel) {
		this.requiredHotel = requiredHotel;
	}

	private List<String> firstThreeImages;

	private List<JSONObject> hotelImages;

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelCity() {
		return hotelCity;
	}

	public void setHotelCity(String hotelCity) {
		this.hotelCity = hotelCity;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getHotelAreaName() {
		return hotelAreaName;
	}

	public void setHotelAreaName(String hotelAreaName) {
		this.hotelAreaName = hotelAreaName;
	}

	public String getPhoneStdCode() {
		return phoneStdCode;
	}

	public void setPhoneStdCode(String phoneStdCode) {
		this.phoneStdCode = phoneStdCode;
	}

	public String getHotelDescription() {
		return hotelDescription;
	}

	public void setHotelDescription(String hotelDescription) {
		this.hotelDescription = hotelDescription;
	}

	public String getHotelLatitude() {
		return hotelLatitude;
	}

	public void setHotelLatitude(String hotelLatitude) {
		this.hotelLatitude = hotelLatitude;
	}

	public String getHotelLongitude() {
		return hotelLongitude;
	}

	public void setHotelLongitude(String hotelLongitude) {
		this.hotelLongitude = hotelLongitude;
	}

	public String getRoomTitle() {
		return roomTitle;
	}

	public void setRoomTitle(String roomTitle) {
		this.roomTitle = roomTitle;
	}

	public String getHotelPhone() {
		return hotelPhone;
	}

	public void setHotelPhone(String hotelPhone) {
		this.hotelPhone = hotelPhone;
	}

	public String getHotelEmail() {
		return hotelEmail;
	}

	public void setHotelEmail(String hotelEmail) {
		this.hotelEmail = hotelEmail;
	}

	public List<JSONObject> getHotelImages() {
		return hotelImages;
	}

	public void setHotelImages(List<JSONObject> hotelImages) {
		this.hotelImages = hotelImages;
	}

	public List<String> getFirstThreeImages() {
		return firstThreeImages;
	}

	public void setFirstThreeImages(List<String> firstThreeImages) {
		this.firstThreeImages = firstThreeImages;
	}

}
