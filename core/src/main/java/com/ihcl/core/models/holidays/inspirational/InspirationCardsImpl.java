package com.ihcl.core.models.holidays.inspirational;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = IInspirationCards.class)
public class InspirationCardsImpl implements IInspirationCards {

	private static final Logger LOG = LoggerFactory.getLogger(InspirationCardsImpl.class);
	private static final String PROPERTY_KEY = "inspirationVideos";
	private static final String COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays/holidays-inspirational-journeys-list";

	@Inject
	Resource resource;

	List<InspirationCard> listOfInspirationCards;

	@PostConstruct
	protected void init() throws IOException {

		LOG.info("Inside init() of class InspirationCardsImpl");
		LOG.debug("resorce path : {}", resource.getPath());
		LOG.debug("resource type : {}", resource.getResourceType());
		
		listOfInspirationCards= new LinkedList<>();
		if (resource.getResourceType().equals(COMPONENT_RESOURCE_TYPE)) {
			LOG.debug("Found inspirational journey component at child: {}", resource.getPath());
			ValueMap inclsionMap = resource.adaptTo(ValueMap.class);
			String[] inspirationsArray = inclsionMap.get(PROPERTY_KEY, String[].class);
			if (inspirationsArray != null && inspirationsArray.length > 0) {
				for (String jsonStr : inspirationsArray) {
					ObjectMapper mapper = new ObjectMapper();
					LOG.debug("JSON String : {}", jsonStr);
					InspirationCard card = mapper.readValue(jsonStr, InspirationCard.class);
					LOG.info("card added");
					listOfInspirationCards.add(card);
				}
			}
		}
		LOG.info("Leaving init() of class InspirationCardsImpl");
	}

	@Override
	public List<InspirationCard> getListOfInspirationCards() {
		return listOfInspirationCards;
	}

}
