package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class GalleryCarousel extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<CarouselBean> imageObjList = new ArrayList<CarouselBean>();
    
    private int imgLsSize;

    public int getImgLsSize() {
        return imgLsSize;
    }

    
    public void setImgLsSize(int imgLsSize) {
        this.imgLsSize = imgLsSize;
    }

    @Override
    public void activate() throws Exception {

        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();

        while (ni.hasNext()) {

            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setImageObjList(ni2);
        }
    }

    private void setImageObjList(NodeIterator ni2) {

        try {

            while (ni2.hasNext()) {

                CarouselBean imgObj = new CarouselBean();
                Node grandChild = ni2.nextNode();

                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
      
                    imgObj.setTitle(getParameters(grandChild,"title"));
                    imgObj.setDescription(getParameters(grandChild,"description"));
                    imgObj.setImagepath(getParameters(grandChild,"imagepath"));
                    imgObj.setIndex(imageObjList.size());
                    imageObjList.add(imgObj);
            }
            setImgLsSize(imageObjList.size());  
        }

        catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }

    }
    
    private String getParameters ( Node grandChild, String key) throws RepositoryException {
    	String value=null;
    	try {
            value=grandChild.getProperty(key).getString();
        } catch (PathNotFoundException e) {
           value=" ";
        }
		return value;
    }

    public List<CarouselBean> getImageObjList() {
        return imageObjList;
    }
    


}
