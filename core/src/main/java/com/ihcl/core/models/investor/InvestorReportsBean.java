package com.ihcl.core.models.investor;

public class InvestorReportsBean {
	
	 private String title;

	 private String pdfPath;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
	 
	 

}
