package com.ihcl.core.models.tic.redeemCard;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

import java.util.Map;

@ConsumerType
public interface IRedeemCardListModel {

    ValueMap getProperties();

    Map<String, String> getCurrencies();
}
