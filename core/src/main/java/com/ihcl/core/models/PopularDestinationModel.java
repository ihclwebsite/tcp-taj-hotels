package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;


@Model(adaptables = Resource.class)
public class PopularDestinationModel {

	@Self
	Resource resource;
	private String name;
	List<Resource> children;
	
	private String destinationName;
	private String destinationImage;
    private String resourceURL;
    
    @PostConstruct
	protected void init() {
		name = "Static content \n";
		Resource content = resource.getChild("jcr:content");
		name += "Resource Name : " + content;
		resourceURL = resource.getPath().concat(".html");
		
		ValueMap valueMap = content.adaptTo(ValueMap.class);
		destinationName = valueMap.get("jcr:title", String.class);
		Resource root = content.getChild("root");
		Iterable<Resource> rootChildren = root.getChildren();
		for (Resource resource : rootChildren) {
			String resourceType = resource.getResourceType();
			if (resourceType.equals("tajhotels/components/content/hotel-banner")) {
				ValueMap rootValueMap = resource.adaptTo(ValueMap.class);
				destinationImage = rootValueMap.get("bannerImage", String.class);
			}
		}
		
    }
	public Resource getResource() {
		return resource;
	}
	public String getName() {
		return name;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public String getDestinationImage() {
		return destinationImage;
	}
	public String getResourceURL() {
		return resourceURL;
	}
	
		
		

}
