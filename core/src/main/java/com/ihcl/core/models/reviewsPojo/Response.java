/**
 *
 */
package com.ihcl.core.models.reviewsPojo;

/**
 * @author moonraft
 *
 */
public class Response {


    private Review_list[] review_list;

    private String[] trip_type_distribution;

    private String page_size;

    private String page;

    private String score;

    private String reviews_count;

    private Reviews_distribution[] reviews_distribution;

    public Review_list[] getReview_list() {
        return review_list;
    }

    public void setReview_list(Review_list[] review_list) {
        this.review_list = review_list;
    }

    public String[] getTrip_type_distribution() {
        return trip_type_distribution;
    }

    public void setTrip_type_distribution(String[] trip_type_distribution) {
        this.trip_type_distribution = trip_type_distribution;
    }

    public String getPage_size() {
        return page_size;
    }

    public void setPage_size(String page_size) {
        this.page_size = page_size;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(String reviews_count) {
        this.reviews_count = reviews_count;
    }

    public Reviews_distribution[] getReviews_distribution() {
        return reviews_distribution;
    }

    public void setReviews_distribution(Reviews_distribution[] reviews_distribution) {
        this.reviews_distribution = reviews_distribution;
    }

    @Override
    public String toString() {
        return "ClassPojo [review_list = " + review_list + ", trip_type_distribution = " + trip_type_distribution
                + ", page_size = " + page_size + ", page = " + page + ", score = " + score + ", reviews_count = "
                + reviews_count + ", reviews_distribution = " + reviews_distribution + "]";
    }

}
