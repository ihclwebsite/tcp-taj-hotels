package com.ihcl.core.models.destination;

import com.ihcl.core.models.offers.OfferImpl;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

/**
 * Destination Offer model extending basic Offer model
 * Use this for destination specific offer overrides
 *
 * @author Ravindar Dev
 */
@Model(adaptables = Resource.class)
public class DestinationOffer extends OfferImpl {}
