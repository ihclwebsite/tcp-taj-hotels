package com.ihcl.core.models;

public class DonationRequest {

    String firstName;

    String lastName;

    String email;
    
    String panCardNumber;
    
    String address1;
    
    String address2;

    String city;

    String pincode;

    String country;
    
    String donation;
    
    String isGDPRCompliance;
    
    String receiptNumber;
    
    String orderId;
    
    String orderIdMismatch;
    
    long timeStamp;

	public long getTimeStamp() {
		return timeStamp;
	}


	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}


	boolean mailStatus;


    public boolean isMailStatus() {
		return mailStatus;
	}


	public void setMailStatus(boolean mailStatus) {
		this.mailStatus = mailStatus;
	}


	public String getOrderIdMismatch() {
		return orderIdMismatch;
	}


	public void setOrderIdMismatch(String orderIdMismatch) {
		this.orderIdMismatch = orderIdMismatch;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getReceiptNumber() {
		return receiptNumber;
	}


	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}


	/**
     * Getter for the field firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Setter for the field firstName
     *
     * @param firstName
     *            the firstName to set
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    /**
     * Getter for the field lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Setter for the field lastName
     *
     * @param lastName
     *            the lastName to set
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Getter for the field email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }


    /**
     * Setter for the field email
     *
     * @param email
     *            the email to set
     */

    public void setEmail(String email) {
        this.email = email;
    }



    /**
     * Getter for the field address
     *
     * @return the address
     */

    /**
     * Getter for the field city
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }


    /**
     * Setter for the field city
     *
     * @param city
     *            the city to set
     */

    public void setCity(String city) {
        this.city = city;
    }


    /**
     * Getter for the field pincode
     *
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }


    /**
     * Setter for the field pincode
     *
     * @param pincode
     *            the pincode to set
     */

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }



    /**
     * Getter for the field country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }


    /**
     * Setter for the field country
     *
     * @param country
     *            the country to set
     */

    public void setCountry(String country) {
        this.country = country;
    }


    public String getPanCardNumber() {
		return panCardNumber;
	}


	public void setPanCardNumber(String panCardNumber) {
		this.panCardNumber = panCardNumber;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getIsGDPRCompliance() {
		return isGDPRCompliance;
	}


	public void setIsGDPRCompliance(String isGDPRCompliance) {
		this.isGDPRCompliance = isGDPRCompliance;
	}


	/**
     * Getter for the field pickup
     *
     * @return the pickup
     */
    public String getDonation() {
		return donation;
	}


	public void setDonation(String donation) {
		this.donation = donation;
	}

}
