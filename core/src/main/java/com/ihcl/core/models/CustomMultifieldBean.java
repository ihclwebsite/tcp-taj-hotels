package com.ihcl.core.models;


public class CustomMultifieldBean {
    
    public String imagepath;
    public int index;
    
    public String getImagepath() {
        return imagepath;
    }
    
    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
    
    public int getIndex() {
        return index;
    }
    
    public void setIndex(int index) {
        this.index = index;
    }
    
    

}
