package com.ihcl.core.models.tic;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class TicOurPartner {
	@Inject
	@Optional
	private String partnerName;

	@Inject
	@Optional
	private String title;

	@Inject
	@Optional
	private String image;

	@Inject
	@Optional
	private String description;

	@Inject
	@Optional
	private String linkBtnLabel;

	@Inject
	@Optional
	private String viewBtnLabel;

	@Inject
	@Optional
	private String viewBtnUrl;

	@Inject
	@Optional
	private String linkBtnUrl;

	@Inject
	@Optional
	private Boolean enablelink;

	@Inject
	@Optional
	private Boolean enableview;

	@Inject
	@Optional
	private String imageClass;

	@Inject
	@Optional
	private String bgClass;

	@Inject
	@Optional
	private String linkBtnElem;

	@Inject
	@Optional
	private String viewBtnElem;

	public String getPartnerName() {
		return partnerName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLinkBtnLabel() {
		return linkBtnLabel;
	}

	public String getViewBtnLabel() {
		return viewBtnLabel;
	}

	public String getViewBtnUrl() {
		return viewBtnUrl;
	}

	public String getLinkBtnUrl() {
		return linkBtnUrl;
	}

	public Boolean getEnablelink() {
		return enablelink;
	}

	public Boolean getEnableview() {
		return enableview;
	}

	public String getImageClass() {
		return imageClass;
	}

	public String getBgClass() {
		return bgClass;
	}

	public String getLinkBtnElem() {
		return linkBtnElem;
	}

	public String getViewBtnElem() {
		return viewBtnElem;
	}
}
