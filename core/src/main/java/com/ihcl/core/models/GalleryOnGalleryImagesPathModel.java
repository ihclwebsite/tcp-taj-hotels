package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = GalleryOnGalleryImagesPathModel.class)
public class GalleryOnGalleryImagesPathModel {

    private static final Logger LOG = LoggerFactory.getLogger(GalleryOnGalleryImagesPathModel.class);

    @Inject
    @Default(values = "")
    private String galleryPath;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver = null;


    private List<JSONObject> galleryImagePaths;

    @PostConstruct
    public void activate() {
        LOG.info("INSIDE ACTIVATE OF GalleryOnGalleryImagesPathModel");
        LOG.info("galleryPath", galleryPath);
        buildImagePaths(galleryPath);
    }

    private void buildImagePaths(String galleryPath) {
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource galleryPathResource = null;
            galleryPathResource = resourceResolver.getResource(galleryPath);
            if (galleryPathResource == null) {
                return;
            }
            galleryImagePaths = new ArrayList<>();
            for (Resource resource : galleryPathResource.getChildren()) {
                if (resource.isResourceType(DamConstants.NT_DAM_ASSET)) {
                    JSONObject imageObject = new JSONObject();
                    imageObject.put("imagePath", resource.getPath());

                    Asset imageAsset = resource.adaptTo(Asset.class);
                    Map<String, Object> metadata = imageAsset.getMetadata();
                    LOG.debug("Received metadata as: " + metadata);
                    if (metadata.containsKey("dc:title")) {
                        imageObject.put("imageTitle", metadata.get("dc:title"));
                    }
                    if (metadata.containsKey("dc:description")) {
                        imageObject.put("imageDescription", metadata.get("dc:description"));
                    }

                    galleryImagePaths.add(imageObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }

}
