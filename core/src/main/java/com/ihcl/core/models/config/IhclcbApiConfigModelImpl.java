package com.ihcl.core.models.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.IhclcbApiConfigurationService;


@Model(
        adaptables = { org.apache.sling.api.resource.Resource.class, SlingHttpServletRequest.class },
        adapters = IhclcbApiConfigModel.class)

public class IhclcbApiConfigModelImpl implements IhclcbApiConfigModel {

	   private static final Logger LOG = LoggerFactory.getLogger(IhclcbApiConfigModelImpl.class);
	   
	   @Inject
	    private IhclcbApiConfigurationService globalConfig;
	   
	   @PostConstruct
	    public void activate() {
	        LOG.trace("Method -->> Entry -->> activate()");

	        if (globalConfig == null) {
	            LOG.debug(" globalConfig at Model is  Null");
	        }
	        LOG.trace("Method -->> Exit -->> activate()");
	    }
	   
	    @Override
	    public String getGuestDetailsFetchApiUrl() {
	        // TODO Auto-generated method stub
	        return globalConfig.getGuestDetailsFetchApiUrl();
	    }
	    
	    
	    @Override
	    public String getEntityDetailsFetchApiUrl() {
	        // TODO Auto-generated method stub
	        return globalConfig.getEntityDetailsFetchApiUrl();
	    }
	    
	    @Override
	    public String getIhclcbBookingApiUrl() {
	        // TODO Auto-generated method stub
	        return globalConfig.getIhclcbBookingApiUrl();
	    }
	    
	    @Override
	    public String getIhclcbApiHostUrl() {
	        // TODO Auto-generated method stub
	        return globalConfig.getIhclcbApiHostUrl();
	    }

}
