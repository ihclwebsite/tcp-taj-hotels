package com.ihcl.core.models.holidays.packages;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.CurrencyFetcherService;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HolidayOfferPackage {

    protected static final Logger LOG = LoggerFactory.getLogger(HolidayOfferPackage.class);

    private static final String CRXconstantForHotelDetailsComponent = "tajhotels/components/content/holidays-destination/holidays-hotel-details";

    @Inject
    Resource resource;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    @Named("packageTitle")
    private String packageTitle;

    @Inject
    @Named("discountedRate")
    private String packagePrice;

    @Inject
    @Named("packageDescription")
    private String packageDescription;

    @Inject
    @Optional
    @Named("nights")
    private String nights;

    @Inject
    @Optional
    @Named("offerRateCode")
    private String offerRateCode;

    @Inject
    @Optional
    @Named("packageValidityStartDate")
    private String packageStartDate;

    @Inject
    @Optional
    @Named("packageValidityEndDate")
    private String packageEndDate;

    @Inject
    @Optional
    @Named("packageRedirectionType")
    private String packageRedirectionType;

    @Inject
    @Optional
    @Named("enquiryPath")
    private String enquiryPath;

    @Inject
    @Default(values = "")
    @Named("termsAndCondition")
    private String termsAndConditionLongString;

    @Inject
    @Optional
    private List<PackageInclusion> inclusions;

    @Inject
    @Optional
    private List<PackageBlackOutDates> blackOutDates;

    private String packageRedirectionPath = "";

    private List<String> termsAndConditionLs;

    private String currencySymbol;

    private String hotelName;

    private String hotelCity;

    private String hotelCountry;

    @PostConstruct
    public void init() {
        String methodName = "init";
        try {
            LOG.trace("Method Entry : " + methodName);
            LOG.debug("Received PackageDetail Component Resource as  : " + resource);
            LOG.debug("Received currencyFetcherService as  : " + currencyFetcherService);
            LOG.debug("Injected Package Price :" + packagePrice);

            if (null != resource) {
                processRoomsRedirectionPathFrom(resource.getParent().getParent().getParent().getParent());
            }
            LOG.debug("Obtained Rooms Redirection path as " + packageRedirectionPath);
            LOG.debug("injected termsAndConditionLongString is " + termsAndConditionLongString);
            termsAndConditionLs = formTermsAndConditionAsList(termsAndConditionLongString);
        } catch (Exception e) {
            LOG.error("Exception found in init method : {}", e.getMessage());
        }
        LOG.trace("Method Exit : " + methodName);
    }

    private List<String> formTermsAndConditionAsList(String termsAndConditionLongStr) {
        String methodName = "formTermsAndConditionAsList";
        LOG.trace("Method Entry : " + methodName);
        LOG.debug("Obtained termsAndConditionLongString  :: " + termsAndConditionLongStr);
        List<String> termsAndConditions = new ArrayList<String>();
        String delimiter = ";";
        if (!termsAndConditionLongStr.isEmpty()) {
            for (String str : termsAndConditionLongStr.split(delimiter)) {
                termsAndConditions.add(str);
            }
        }
        LOG.trace("Method Exit : " + methodName);
        return termsAndConditions;
    }

    private void processRoomsRedirectionPathFrom(Resource hotelPageResource) {
        String methodName = "processRoomsRedirectionPathFrom";
        LOG.trace("Method Entry : " + methodName);
        Resource hotelDetailsComponentResource = null;
        LOG.debug("Attempting to iterate over children of: " + hotelPageResource);
        Iterable<Resource> children = hotelPageResource.getChildren();
        for (Resource child : children) {
            LOG.debug("Iterating over child: " + child);
            if (child.getResourceType().equals(CRXconstantForHotelDetailsComponent)) {
                LOG.debug("Found hotelDetails component at child: " + child);
                hotelDetailsComponentResource = child;
                formRedirectionPathUsing(hotelDetailsComponentResource);
                setHotelNameAndCity(hotelDetailsComponentResource);
                setCurrencySymbol(hotelDetailsComponentResource);
            }
            if (hotelDetailsComponentResource == null) {
                processRoomsRedirectionPathFrom(child);
            }
        }
        LOG.trace("Method Exit : " + methodName);
    }

    private void formRedirectionPathUsing(Resource hotelDetailsComponentResource) {
        ValueMap valueMap = hotelDetailsComponentResource.adaptTo(ValueMap.class);
        packageRedirectionPath = getProperty(valueMap, "hotelPath", String.class, "");
        if (packageRedirectionPath.contains("/ama/")) {
            packageRedirectionPath = packageRedirectionPath + "/accommodations/";
        } else {
            packageRedirectionPath = packageRedirectionPath + "/rooms-and-suites";
        }
    }

    private void setCurrencySymbol(Resource hotelDetailsComponentResource) {
        ValueMap valueMap = hotelDetailsComponentResource.adaptTo(ValueMap.class);
        String currencyShortString = currencyFetcherService
                .getCurrencySymbolValue(getProperty(valueMap, "currencySymbol", String.class, ""));
        if (null != currencyShortString) {
            currencySymbol = currencyShortString;
            LOG.trace("Currency Symbol formed :: " + currencySymbol);
        }
    }

    private void setHotelNameAndCity(Resource hotelDetailsComponentResource) {

        ResourceResolver resourceResolver = null;
        ValueMap valueMap = hotelDetailsComponentResource.adaptTo(ValueMap.class);
        String hotelPath = getProperty(valueMap, "hotelPath", String.class, "");
        try {
            if (!hotelPath.isEmpty()) {
                resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
                Resource hotelResource = resourceResolver.getResource(hotelPath + "/jcr:content");
                valueMap = hotelResource.adaptTo(ValueMap.class);
                hotelName = getProperty(valueMap, "hotelName", String.class, "");
                hotelCity = getProperty(valueMap, "hotelCity", String.class, "");
                hotelCountry = getProperty(valueMap, "hotelCountry", String.class, "");
            }
        } catch (LoginException e) {
            LOG.error("Login Exception ");
        } finally {
            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }
    }

    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        String methodName = "getProperty";
        LOG.trace("Method Entry: " + methodName);
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        LOG.trace("Value found for key: " + key + " : " + value);
        LOG.trace("Method Exit: " + methodName);
        return value;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public String getPackagePrice() {
        return packagePrice;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public String getNights() {
        return nights;
    }

    public String getOfferRateCode() {
        return offerRateCode;
    }

    public String getPackageEndDate() {
        return packageEndDate;
    }

    public List<PackageInclusion> getInclusions() {
        return inclusions;
    }

    public List<PackageBlackOutDates> getBlackOutDates() {
        return blackOutDates;
    }

    public String getPackageRedirectionPath() {
        return packageRedirectionPath;
    }

    public List<String> getTermsAndConditionLs() {
        return termsAndConditionLs;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getPackageStartDate() {
        return packageStartDate;
    }

    public String getPackageRedirectionType() {
        return packageRedirectionType;
    }

    public String getEnquiryPath() {
        return enquiryPath;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public String getHotelCountry() {
        return hotelCountry;
    }

}
