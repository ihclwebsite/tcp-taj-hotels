package com.ihcl.core.models;

public class WarningPopup {
	
	private String warningHeading;
	private String warningDescription;
	private String warningPopupType;
	
	public String getWarningHeading() {
		return warningHeading;
	}
	public void setWarningHeading(String warningHeading) {
		this.warningHeading = warningHeading;
	}
	public String getWarningDescription() {
		return warningDescription;
	}
	public void setWarningDescription(String warningDescription) {
		this.warningDescription = warningDescription;
	}
	public String getWarningPopupType() {
		return warningPopupType;
	}
	public void setWarningPopupType(String warningPopupType) {
		this.warningPopupType = warningPopupType;
	}

}
