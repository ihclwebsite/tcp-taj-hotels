package com.ihcl.core.models.offers;

import java.util.List;
import java.util.Map;

import com.day.cq.wcm.api.Page;

public interface OfferList {

	List<Page> getOfferList();
	
	Map<String, String> getCountryMap();
	
	Map<String, String> getDestinationMap();

    Map<String, String> getHotelMap();
}

