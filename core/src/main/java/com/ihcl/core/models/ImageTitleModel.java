package com.ihcl.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

/**
 * A generic image and title model, can be used to adapt any multifield model
 */
@Model(adaptables = Resource.class)
public class ImageTitleModel {

	@Inject
	@Optional
	private String title;

	@Inject
	@Optional
	private String image;

	public String getTitle() {
		return title;
	}

	public String getImage() {
		return image;
	}
}
