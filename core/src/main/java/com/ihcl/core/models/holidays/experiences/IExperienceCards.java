package com.ihcl.core.models.holidays.experiences;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface IExperienceCards {

	List<ExperiencesCards> getListOfExperienceCards();
}
