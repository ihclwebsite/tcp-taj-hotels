package com.ihcl.core.models.serviceamenity;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface ServiceAmenityPropertiesFetcher {

    String getPath();

    String getName();

    String getImagePath();

    String getGroup();

    String getDescription();

}
