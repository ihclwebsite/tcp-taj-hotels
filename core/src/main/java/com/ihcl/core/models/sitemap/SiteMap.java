package com.ihcl.core.models.sitemap;

public class SiteMap {
	private String loc;
	private String lastmod;
	private String changefreq;
	private String priority;
	private boolean isPagePresent;
	
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getLastmod() {
		return lastmod;
	}
	public void setLastmod(String lastmod) {
		this.lastmod = lastmod;
	}
	public String getChangefreq() {
		return changefreq;
	}
	public void setChangefreq(String changefreq) {
		this.changefreq = changefreq;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}

	public boolean getIsPagePresent() {
		return isPagePresent;
	}
	public void setIsPagePresent(Boolean isPagePresent) {
		this.isPagePresent = isPagePresent;
	}
	
	@Override
	public String toString() {
		return "SiteMap [loc=" + loc + ", lastmod=" + lastmod + ", changefreq=" + changefreq + ", priority=" + priority
				+ ", isPagePresent=" + isPagePresent + "]";
	}
	
}
