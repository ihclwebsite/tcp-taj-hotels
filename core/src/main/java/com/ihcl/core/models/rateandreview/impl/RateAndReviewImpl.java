package com.ihcl.core.models.rateandreview.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.MetaReviewsApiResponse;
import com.ihcl.core.models.ReviewsApiResponse;
import com.ihcl.core.models.SealApiResponse;
import com.ihcl.core.models.metaReviewsPojo.Category_list;
import com.ihcl.core.models.metaReviewsPojo.FormattedCategoryListData;
import com.ihcl.core.models.rateandreview.RateAndReview;

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = RateAndReview.class)
public class RateAndReviewImpl implements RateAndReview {

private static final Logger log = LoggerFactory.getLogger(RateAndReviewImpl.class);

	private String headURL = "https://api.trustyou.com/hotels";
    
    private SealApiResponse sealApiResponse;

    private ReviewsApiResponse reviewsApiResponse;

    private MetaReviewsApiResponse metaReviewsApiResponse;
    
    private List<FormattedCategoryListData> formattedCategoryList;
    
    @Inject
    @Default(values = "")
    private Resource resource;
    
    @Inject
    @Default(values = "")
    private String trustyouid;
    
    
    @PostConstruct
    protected void init() {
    	String methodName = "init";
    	log.info("Method Entry : " + methodName);
    	
    	if (trustyouid != "") {
    		processRateAndReview(trustyouid);
    	}
    	
    	log.info("Method Exit : " + methodName);
    }
    
    private void processRateAndReview(String trustid) {
    	String methodName = "processRateAndReview";
    	log.info("Method Entry : " + methodName);
    	
    	ObjectMapper mapper = new ObjectMapper();
        try {
        	
            log.trace("Injected TrustYouId :--> " + trustid );
            
            final String sealJson = getApiResponse(formJSONUrl(headURL,trustid,"seal"));

            final String reviewsJson = getApiResponse(formJSONUrl(headURL,trustid,"reviews"));

            final String metaReviewJson = getApiResponse(formJSONUrl(headURL,trustid,"meta_review"));


            if (sealJson != null) {
                sealApiResponse = mapper.readValue(sealJson, SealApiResponse.class);
            } else {
                log.warn("sealApiResponse parsed data is null");
            }


            if (reviewsJson != null) {
                reviewsApiResponse = mapper.readValue(reviewsJson, ReviewsApiResponse.class);

            } else {
                log.warn("reviewsApiResponse parsed data is null");
            }


            if (metaReviewJson != null)
                metaReviewsApiResponse = mapper.readValue(metaReviewJson, MetaReviewsApiResponse.class);
            else {
                log.warn("metaReviewsApiResponse parsed data is null");
            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    	log.info("Method Exit : " + methodName);
    }
    
    private String formJSONUrl (String mainUrl,String trustYouId, String key) {
    	String methodName = "formJSONUrl";
    	log.info("Method Entry : " + methodName);
    	
    	StringBuilder finalURL = new StringBuilder();
    	String delimiter ="/";
    	String tailString=".json";
    	
    	finalURL.append(mainUrl).append(delimiter).append(trustYouId).append(delimiter).append(key).append(tailString);
    	
    	log.trace("URL formed :: " +finalURL.toString());
    	log.info("Method Exit : " + methodName);
    	return finalURL.toString();
    }


    public String getApiResponse(final String api) {
        return readJsonFromUrl(api);

    }


    private static String readAll(Reader reader) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            stringBuilder.append((char) cp);
        }
        return stringBuilder.toString();
    }

    private String readJsonFromUrl(String url) {
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        String jsonText = null;
        try {
            inputStream = new URL(url).openStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            jsonText = readAll(bufferedReader);
            bufferedReader.close();
            inputStream.close();
            log.info("JSON Content:\n" + jsonText);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return jsonText;
    }

	@Override
	public SealApiResponse getSealApiResponse() {
		return sealApiResponse;
	}

	@Override
	public ReviewsApiResponse getReviewsApiResponse() {
		return reviewsApiResponse;
	}

	@Override
	public MetaReviewsApiResponse getMetaReviewsApiResponse() {
		return metaReviewsApiResponse;
	}

	@Override
	public List<FormattedCategoryListData> getFormattedCategoryList() {
		if (metaReviewsApiResponse != null) {

            final Category_list[] category_lists = metaReviewsApiResponse.getResponse().getCategory_list();
            formattedCategoryList = new ArrayList<>();
            log.info("category_lists from response API size: " + category_lists.length);
            for (final Category_list category : category_lists) {
                final FormattedCategoryListData formattedCategoryListData = new FormattedCategoryListData();
                formattedCategoryListData.setCategory_name(category.getCategory_name());
                formattedCategoryListData.setScore(String.format("%.1f", category.getScore() / 20));
                formattedCategoryListData.setBarWidth(category.getScore());
                formattedCategoryList.add(formattedCategoryListData);
            }
        } else {
            log.warn("MetaReview API Response is null..");
        }
        log.info("Formatted List created. Size: " + formattedCategoryList.size());
        return formattedCategoryList;

	}

}
