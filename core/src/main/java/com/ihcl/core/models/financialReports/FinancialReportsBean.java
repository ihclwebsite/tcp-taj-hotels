package com.ihcl.core.models.financialReports;

public class FinancialReportsBean {
	

    private String title;

    private String pdfPath;

    private String reportType;

    private String financialYear;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	@Override
	public String toString() {
		return "FinancialReportsBean [title=" + title + ", pdfPath=" + pdfPath + ", reportType=" + reportType
				+ ", financialYear=" + financialYear + "]";
	}  
	
	

}
