/**
 *
 */
package com.ihcl.core.models.financialReports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;

/**
 * @author moonraft
 *
 */
@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = AmalgamationReportsFetcher.class)
public class AmalgamationReportsFetcher {

    private final Logger LOG = LoggerFactory.getLogger(AmalgamationReportsFetcher.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    @Inject
    Resource resource;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private QueryBuilder builder;

    private Session session;

    private String tifcoPath;

    private String landsEndPath;

    private String internationalHotelsPath;

    private String ShareHolderPatternPath;

    private String FinancialStatusPath;


    private ResourceResolver resolver;

    private List<FinancialReportsBean> tifco = new ArrayList<FinancialReportsBean>();

    private List<FinancialReportsBean> landsEnd = new ArrayList<FinancialReportsBean>();

    private List<FinancialReportsBean> internationalHotels = new ArrayList<FinancialReportsBean>();

    private List<FinancialReportsBean> ShareHolderPattern = new ArrayList<FinancialReportsBean>();

    private List<FinancialReportsBean> FinancialStatus = new ArrayList<FinancialReportsBean>();


    @PostConstruct
    protected void init() {
        String methodName = "init";
        LOG.trace("Method Entry: " + methodName);
        LOG.trace("Method Entred :init " + resource.getResourceType());
        pdfPath();
        LOG.trace("Method Exit: " + methodName);
    }


    private void pdfPath() {
        String methodName = "pdfPath";
        LOG.trace("Method Entry: " + methodName);
        LOG.trace("Method Entred :init " + resource.getPath());

        try {
            ValueMap valuemap = resource.adaptTo(ValueMap.class);
            if (valuemap != null) {
                if (valuemap.get("tifcoPath", String.class) != null) {
                    tifcoPath = valuemap.get("tifcoPath", String.class);
                    LOG.trace("tifcoPath path " + tifcoPath);
                    tifco = getpdffDetails(tifcoPath);
                }
                if (valuemap.get("landsEndPath", String.class) != null) {
                    landsEndPath = valuemap.get("landsEndPath", String.class);
                    LOG.trace("landsEndPath path " + landsEndPath);
                    landsEnd = getpdffDetails(landsEndPath);
                }
                if (valuemap.get("internationalHotelsPath", String.class) != null) {
                    internationalHotelsPath = valuemap.get("internationalHotelsPath", String.class);
                    LOG.trace("internationalHotelsPath path " + internationalHotelsPath);
                    internationalHotels = getpdffDetails(internationalHotelsPath);
                }
                if (valuemap.get("ShareHolderPatternPath", String.class) != null) {
                    ShareHolderPatternPath = valuemap.get("ShareHolderPatternPath", String.class);
                    LOG.trace("ShareHolderPatternPath path " + ShareHolderPatternPath);
                    ShareHolderPattern = getpdffDetails(ShareHolderPatternPath);
                }
                if (valuemap.get("FinancialStatusPath", String.class) != null) {
                    FinancialStatusPath = valuemap.get("FinancialStatusPath", String.class);
                    LOG.trace("FinancialStatusPath path " + FinancialStatusPath);
                    FinancialStatus = getpdffDetails(FinancialStatusPath);
                }
                /*
                 * LOG.trace("The  ReportPath " + tifcoPath); landsEndPath = valuemap.get("landsEndPath", String.class);
                 * List<String> landEndpagePath = getListofReports(landsEndPath); landsEnd =
                 * filterReports(landEndpagePath); LOG.trace("The  ReportPath " + landsEndPath);
                 *
                 * internationalHotelsPath = valuemap.get("internationalHotelsPath", String.class); List<String>
                 * internationalpagePath = getListofReports(internationalHotelsPath); internationalHotels =
                 * filterReports(internationalpagePath); LOG.trace("The MeetReportPath" + internationalHotelsPath);
                 */


            }

        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LOG.trace("Method Exit: " + methodName);
    }

    public List<String> getListofReports(String reportPath) throws RepositoryException {
        String methodName = "getListofReports";
        LOG.trace(" Method Entry :" + methodName);

        List<String> pagePath = new ArrayList<String>();
        Map<String, String> queryParam = new HashMap<String, String>();
        queryParam.put("type", "dam:Asset");
        queryParam.put("path", reportPath);
        queryParam.put("orderby","@jcr:content/jcr:lastModified");
        queryParam.put("orderby.sort", "desc");
        queryParam.put("p.limit", "-1");
        LOG.trace(" queryParam : " + queryParam);

        resolver = resource.getResourceResolver();
        session = resolver.adaptTo(Session.class);

        QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
        LOG.trace("Query builder : " + builder);
        PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
        LOG.trace("SearchPredicates : " + searchPredicates);
        Query query = builder.createQuery(searchPredicates, session);

        LOG.trace("Query to be executed is : " + query);
        query.setStart(0L);

        SearchResult result = query.getResult();
        LOG.trace("Query Executed");
        LOG.trace("Query result is : " + result.getHits().size());

        List<Hit> hits = result.getHits();
        Iterator<Hit> hitList = hits.iterator();
        Hit hit;
        while (hitList.hasNext()) {
            hit = hitList.next();
            LOG.trace("The result is : " + hit.getPath());
            pagePath.add(hit.getPath());
        }

        return pagePath;
    }


    public List<FinancialReportsBean> filterReports(List<String> pdfList) {
        String methodName = "filterReports";
        LOG.trace("Method Entry: " + methodName);
        List<FinancialReportsBean> amalgamationBean = new ArrayList<FinancialReportsBean>();
        if (pdfList != null) {
            for (String listItem : pdfList) {
                FinancialReportsBean resBean = getDetails(listItem);
                amalgamationBean.add(resBean);

            }
        }
        return amalgamationBean;
    }

    public FinancialReportsBean getDetails(String PDFPath) {
        String methodName = "getDetails";
        LOG.trace("Method Entry: " + methodName);
        FinancialReportsBean beanObj = new FinancialReportsBean();
        if (PDFPath != null) {
            beanObj.setPdfPath(PDFPath);
            LOG.trace("pdf path for title: " + PDFPath);
            Resource pdfPathResource = resolver.getResource(PDFPath);
            pdfPathResource = pdfPathResource.getChild("jcr:content");
            LOG.trace("jscr resource data : " + pdfPathResource);
            ValueMap vMap = pdfPathResource.adaptTo(ValueMap.class);
            beanObj.setTitle(vMap.get("jcr:title", String.class));
            LOG.trace("The PDF title Value : " + vMap.get("jcr:title", String.class));
        }
        return beanObj;
    }

    public List<FinancialReportsBean> getpdffDetails(String pdfPath) throws RepositoryException {
        List<FinancialReportsBean> reportBean = new ArrayList<FinancialReportsBean>();
        List<String> pagePathList = getListofReports(pdfPath);
        LOG.trace("The  tifcopagePath size" + pagePathList.size());
        reportBean = filterReports(pagePathList);
        LOG.trace("The  ReportPath " + tifcoPath);
        return reportBean;
    }


    public List<FinancialReportsBean> getTifco() {
        return tifco;
    }

    public List<FinancialReportsBean> getLandsEnd() {
        return landsEnd;
    }


    public List<FinancialReportsBean> getInternationalHotels() {
        return internationalHotels;
    }


    public List<FinancialReportsBean> getShareHolderPattern() {
        return ShareHolderPattern;
    }

    public List<FinancialReportsBean> getFinancialStatus() {
        return FinancialStatus;
    }


}
