package com.ihcl.core.models;

public class HotelEssentialBean {

	private String hotelEssentialDesc;
	private String pathToDownloadPdfFrom;
	
    public String getHotelEssentialDesc() {
		return hotelEssentialDesc;
	}
	public void setHotelEssentialDesc(String hotelEssentialDesc) {
		this.hotelEssentialDesc = hotelEssentialDesc;
	}
	public String getPathToDownloadPdfFrom() {
		return pathToDownloadPdfFrom;
	}
	public void setPathToDownloadPdfFrom(String pathToDownloadPdfFrom) {
		this.pathToDownloadPdfFrom = pathToDownloadPdfFrom;
	}
	
}
