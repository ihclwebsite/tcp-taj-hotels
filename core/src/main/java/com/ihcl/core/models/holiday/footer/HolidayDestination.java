package com.ihcl.core.models.holiday.footer;

public class HolidayDestination {

	private String destinationName;
	private String destinationPath;
	private String destinationCountry;
	private String resolvedDestinationPath;
	
	public String getResolvedDestinationPath() {
		return resolvedDestinationPath;
	}
	public void setResolvedDestinationPath(String resolvedDestinationPath) {
		this.resolvedDestinationPath = resolvedDestinationPath;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getDestinationPath() {
		return destinationPath;
	}
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
}
