package com.ihcl.core.models.holidays.experiences;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = IExperienceCards.class)
public class ExperienceCardsImpl implements IExperienceCards {

	private static final Logger LOG = LoggerFactory.getLogger(ExperienceCardsImpl.class);
	private static final String PROPERTY_KEY = "experiencesList";
	private static final String COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays/holidays-experiences-list";
	private static final String AMP_COMPONENT_RESOURCE_TYPE="tajhotels/components/content/amp/holidays/holidays-experiences-list";
	@Inject
	Resource resource;

	List<ExperiencesCards> listOfExperienceCards;

	@PostConstruct
	protected void init() throws IOException {

		LOG.info("Inside init() of class ExperienceCardsImpl");
		LOG.debug("resorce path : {}", resource.getPath());
		LOG.debug("resource type : {}", resource.getResourceType());
		
		listOfExperienceCards= new LinkedList<>();
		if ((resource.getResourceType().equals(COMPONENT_RESOURCE_TYPE)) ||(resource.getResourceType().equals(AMP_COMPONENT_RESOURCE_TYPE))) {
			LOG.debug("Found experiences list component at child: {}", resource.getPath());
			ValueMap inclsionMap = resource.adaptTo(ValueMap.class);
			String[] inspirationsArray = inclsionMap.get(PROPERTY_KEY, String[].class);
			if (inspirationsArray != null && inspirationsArray.length > 0) {
				ObjectMapper mapper = new ObjectMapper();
				for (String jsonStr : inspirationsArray) {
					
					LOG.debug("JSON String : {}", jsonStr);
					ExperiencesCards card = mapper.readValue(jsonStr, ExperiencesCards.class);
					LOG.info("card added");
					listOfExperienceCards.add(card);
				}
			}
		}
		LOG.info("Leaving init() of class ExperienceCardsImpl");
	}

	@Override
	public List<ExperiencesCards> getListOfExperienceCards() {
		return listOfExperienceCards;
	}

}
