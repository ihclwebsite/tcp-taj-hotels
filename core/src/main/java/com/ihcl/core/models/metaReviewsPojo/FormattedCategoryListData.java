/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 *
 */
public class FormattedCategoryListData {

    private String category_name;

    private String score;

    private float barWidth;

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public float getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(float barWidth) {
        this.barWidth = barWidth;
    }


}
