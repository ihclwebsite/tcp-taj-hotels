/**
 *
 */
package com.ihcl.core.models.holidays.offers;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author moonraft
 *
 */
@ConsumerType
public interface IHolidaysOffersDetailsFetcher {

    List<HolidaysOffers> getAllHolidaysOffers();
}
