/**
 *
 */
package com.ihcl.core.models;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author Vijay.pal
 *
 */
@ConsumerType
public interface CompetitiveStrengthsModel {

    /**
     * @return
     */
    ValueMap getValueMap();

}
