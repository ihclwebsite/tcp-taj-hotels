package com.ihcl.core.models;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;

@ConsumerType
public interface FetchHotelsServiceModel extends ComponentExporter {

    /**
     * @return
     */
    String getPagePath();

    /**
     * @return
     */
    ParticipatingHotel getHotel();

	ValueMap getHotelDetails();
}

