package com.ihcl.core.models.holidays.explore;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = Resource.class)
public class ExploreDestinationPropertyFetcher  {

	private static final Logger LOG = LoggerFactory.getLogger(ExploreDestinationPropertyFetcher.class);
			
	@Inject
	@Optional
	private String destinationRootPath;
	
	@Inject
	@Optional
	private String[] localList;
	
	@Inject
	private Resource resource;
	
	@Inject
	private ResourceResolverFactory resourceResolverFactory;
	
    @Inject
    private SearchProvider searchProvider;
    
    @Inject
    private TajApiKeyConfigurationService config;
    
    private List<LocaleList> localLs = new ArrayList<LocaleList>();
    
    private String iteneraryDataJsonStr;
    
    private List<SightSeeing> sightSeeing = new ArrayList<SightSeeing>();
	
	private List<ActivitiesToDo> activities = new ArrayList<ActivitiesToDo>();

	private List<Shopping> shopping = new ArrayList<Shopping>();
	
	private Set<Dining> dining = new HashSet<Dining>();
	
	private Set<Spa> spaList = new HashSet<Spa>();
	
	private String googleApiKey;
    
    @PostConstruct
	public void init() throws Exception {

		String methodName = "init";
		LOG.trace("Method Entry: " + methodName);
		long startTime = new Date().getTime();
		LOG.trace("Received destination Root path as :: " + destinationRootPath);
		LOG.trace("Revieved localList as ::: " + localList);
		LOG.trace("Google API key :: " + config.getGoogleMapApiKey());

		setGoogleApiKey(config.getGoogleMapApiKey());
		Stream.<Runnable>of(() -> buildLocaleList(), () -> buildIteneraryList(), () -> {
			try {
				if (null != destinationRootPath) {
					fetchAllDiningPaths();
				}
			} catch (RepositoryException e) {
				LOG.error("Error occured while fetching dinning paths :: " + e.getMessage());
			}
		}, () -> {
			try {
				if (null != destinationRootPath) {
					fetchAllSPApaths();
				}
			} catch (RepositoryException e) {
				LOG.error("Error occured while fetching SPA paths :: " + e.getMessage());
			}
		}).parallel().forEach(Runnable::run);

		long endTime = new Date().getTime();
		LOG.debug("Total Time elapsed : " + (endTime - startTime));
		LOG.trace("Method Exit: " + methodName);
	}
    
    
    private void buildLocaleList()  {
    	
	LOG.trace("Method Entry: buildLocaleList ");
	try {	
		if (localList != null && localList.length > 0) {
			for(String jsonStr : localList) {
				ObjectMapper mapper = new ObjectMapper();
				LocaleList localLsObj = mapper.readValue(jsonStr, LocaleList.class);
				LOG.debug("Attraction Type :: " +localLsObj.getType());
				String[] attrationTypeArr = localLsObj.getType();
				if (null != attrationTypeArr && attrationTypeArr.length > 0) {
					for (String attractionType : attrationTypeArr) {
						switch(attractionType)
						{
							case "taj:Attractions/SightSeeing": {
								SightSeeing obj = new SightSeeing();
								obj.setTitle(localLsObj.getTitle());
								obj.setDescription(localLsObj.getDescription());
								obj.setImage(localLsObj.getImage());
								obj.setOpeningTime(localLsObj.getOpeningTime());
								obj.setClosingTime(localLsObj.getClosingTime());
								obj.setPlaceid(localLsObj.getPlaceid());
								obj.setLatitude(localLsObj.getLatitude());
								obj.setLongitude(localLsObj.getLongitude());
								obj.setRecommended(localLsObj.getRecommended());
								obj.setUniqueName(removeWhiteSpaces(localLsObj.getTitle()));
								sightSeeing.add(obj);
								break;
							}
							case "taj:Attractions/ActivitiesToDo": {
								ActivitiesToDo obj = new ActivitiesToDo();
								obj.setTitle(localLsObj.getTitle());
								obj.setDescription(localLsObj.getDescription());
								obj.setImage(localLsObj.getImage());
								obj.setOpeningTime(localLsObj.getOpeningTime());
								obj.setClosingTime(localLsObj.getClosingTime());
								obj.setPlaceid(localLsObj.getPlaceid());
								obj.setLatitude(localLsObj.getLatitude());
								obj.setLongitude(localLsObj.getLongitude());
								obj.setRecommended(localLsObj.getRecommended());
								obj.setUniqueName(removeWhiteSpaces(localLsObj.getTitle()));
								activities.add(obj);
								break;
							}
							case "taj:Attractions/Shopping" :{
								Shopping obj = new Shopping();
								obj.setTitle(localLsObj.getTitle());
								obj.setDescription(localLsObj.getDescription());
								obj.setImage(localLsObj.getImage());
								obj.setOpeningTime(localLsObj.getOpeningTime());
								obj.setClosingTime(localLsObj.getClosingTime());
								obj.setPlaceid(localLsObj.getPlaceid());
								obj.setLatitude(localLsObj.getLatitude());
								obj.setLongitude(localLsObj.getLongitude());
								obj.setRecommended(localLsObj.getRecommended());
								obj.setUniqueName(removeWhiteSpaces(localLsObj.getTitle()));
								shopping.add(obj);
								break;
							}
					}
				}
			}
			else {
				LOG.trace("AttractionType has not been Set or its NULL");
			}
		}
	}
	} catch (JsonParseException e) {
		LOG.error("Error occured while extracting localList json String Array to corresponding JSON Object :: {}", e.getMessage());
	} catch (JsonMappingException e) {
		LOG.error("Error occured while mapping localList json String Array to corresponding POJO Object :: {}", e.getMessage());
	} catch (Exception e) {
		LOG.error("Error occured while building Locale List :: {}", e.getMessage());
	}
	LOG.trace("Method Exit: buildLocaleList");
    }
    
    private void fetchAllDiningPaths() throws RepositoryException {
    	
    	LOG.trace("Method Entry: fetchDiningPaths");
    	try {
    		HashMap<String, String> predicateMap = new HashMap<>();
    		LOG.debug("Searching for various restaurant paths under the path : " + destinationRootPath	);
            predicateMap.put("path",destinationRootPath);
            predicateMap.put("property","sling:resourceType");
            predicateMap.put("property.value","tajhotels/components/content/dining-details");
            predicateMap.put("p.limit", "-1");
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            LOG.debug("Total Restaurant Hits :: "+ searchResult.getHits().size());
            for(Hit hit: searchResult.getHits()) {
            	LOG.debug("Restaurant Path fetched at" + hit.getPath());
            	buildDiningProperties(hit.getPath());
            } 
    	}catch (Exception e) {
    		LOG.error("Error occured while fetching dinning paths :: {}" , e.getMessage());
    	} 
    	LOG.trace("Method Exit: fetchDiningPaths");
    }
    
    private void buildDiningProperties(String diningResourcePath) {
    	
    	String methodName = "buildDiningProperties";
    	ResourceResolver resourceResolver = null;
    	LOG.trace("Method Entry: " + methodName);
    	LOG.debug("Recieved Dining Resource path as:: " + diningResourcePath);
    try {
    	resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
    	Resource diningResource = resourceResolver.getResource(diningResourcePath);
    	LOG.debug("Recieved Dining Resource as:: " + diningResource);
    	Dining diningObj = new Dining();
    	ValueMap valueMap = diningResource.adaptTo(ValueMap.class);
    	diningObj.setDiningName(getProperty(valueMap, "diningName", String.class, ""));
    	diningObj.setUniqueName(removeWhiteSpaces(diningObj.getDiningName()));
    	diningObj.setDiningDescription(getProperty(valueMap, "diningLongDescription", String.class, ""));
    	diningObj.setDinningTime(getProperty(valueMap, "dinnerTime", String.class, ""));
    	String imagePath=getProperty(valueMap, "diningImage", String.class, "");
    	LOG.debug("Dining Image path obtained :: " + imagePath);
    	if(imagePath.isEmpty()) {
    		LOG.debug("Dining Image path obtained is empty hence obtaining from child gallery node");
    		Resource galleryResource = diningResource.getChild("gallery");
    		LOG.debug("Received gallery resource as " + galleryResource);
    		if(null!=galleryResource) {
    			valueMap = galleryResource.adaptTo(ValueMap.class);
    			String[] galleryPaths = getProperty(valueMap, "images", String[].class, new String[] {});
    			if (galleryPaths != null && galleryPaths.length > 0) {
    				diningObj.setDiningImagePath(galleryPaths[0]);
    			}
    		}
    	}
    	else {
    		diningObj.setDiningImagePath(imagePath);
    	}
    	Resource hotelResource =diningResource.getParent().getParent().getParent().getParent().getParent();
    	LOG.debug("Received hotel resource as " + hotelResource);
    	if(null!=hotelResource) {
    		Resource hotelJCRResource =hotelResource.getChild("jcr:content");
    		valueMap = hotelJCRResource.adaptTo(ValueMap.class);
    		diningObj.setDiningLatitude(getProperty(valueMap, "latitude", String.class, ""));
    		diningObj.setDiningLongitude(getProperty(valueMap, "longitude", String.class, ""));
    		String hotelAddress = formHotelAddress(hotelJCRResource);
    		LOG.debug("Hotel Address formed " + hotelAddress);
    		diningObj.setDiningAddress(hotelAddress);
    	} 
    	dining.add(diningObj);
    } catch (LoginException e) {
    	LOG.debug("Exception occured in "+ methodName + " while accessing resource resolver" + e.getMessage());
    }
    finally {
    	if(resourceResolver != null && resourceResolver.isLive()) {
    		resourceResolver.close();
    	}
    }
    	LOG.trace("Method Exit: " + methodName);
    }
    
    private String formHotelAddress(Resource hotelJCRResource) {
    	
    	ValueMap valueMap = hotelJCRResource.adaptTo(ValueMap.class);
    	String hotelArea=getProperty(valueMap, "hotelArea", String.class, "");
    	hotelArea = hotelArea.contains(",")?hotelArea.replaceAll(",",""):hotelArea;
    	String hotelCity=getProperty(valueMap, "hotelCity", String.class, "");
    	hotelCity = hotelCity.contains(",")?hotelCity.replaceAll(",",""):hotelCity;
    	String hotelPinCode=getProperty(valueMap, "hotelPinCode", String.class, "");
    	hotelPinCode = hotelPinCode.contains(",")?hotelPinCode.replaceAll(",",""):hotelPinCode;
    	String hotelCountry=getProperty(valueMap, "hotelCountry", String.class, "");
    	hotelCountry = hotelCountry.contains(",")?hotelCountry.replaceAll(",",""):hotelCountry;
    	
    	if(!hotelArea.isEmpty() && !hotelCity.isEmpty() && !hotelPinCode.isEmpty() && !hotelCountry.isEmpty())
    		return hotelArea+","+hotelCity+","+hotelPinCode+","+hotelCountry;
    	else if (hotelCountry.isEmpty())
    		return hotelArea+","+hotelCity+","+hotelPinCode;
    	else if (hotelPinCode.isEmpty())
    		return hotelArea+","+hotelCity;
    	else if (hotelCity.isEmpty())
    		return hotelArea+","+hotelPinCode+","+hotelCountry;
    	else if (hotelArea.isEmpty())
    		return hotelCity+","+hotelPinCode+","+hotelCountry;
    	else
    		return "";
    }
    
    private void fetchAllSPApaths() throws RepositoryException {
    	
    	LOG.trace("Method Entry: fetchAllSPApaths");
    		HashMap<String, String> predicateMap = new HashMap<>();
    		LOG.debug("Searching for various spa paths under the path : " + destinationRootPath	);
            predicateMap.put("path",destinationRootPath);
            predicateMap.put("property","jcr:title");
            predicateMap.put("property.value","Jiva Overview");
            predicateMap.put("p.limit", "-1");
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            LOG.debug("Total SPA Hits :: "+ searchResult.getHits().size());
            for(Hit hit: searchResult.getHits()) {
            	LOG.debug("SPA Path fetched at" + hit.getPath());
            	buildHotelWithSpaProperties(hit.getPath());
            }
    	LOG.trace("Method Exit: fetchAllSPApaths");
    }
    
    private void buildHotelWithSpaProperties(String sPath) {
    	
    	String methodName = "fetchAllSPApaths";
    	LOG.trace("Method Entry: " + methodName);
    	LOG.debug("Recieved Jiva SPA Resource path as:: " + sPath);
    	ResourceResolver resourceResolver = null;
    try {
    	resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
    	Spa spa = new Spa();
    	Resource spaResource = resourceResolver.getResource(sPath);
    	LOG.debug("Recieved SPA Resource as:: " + spaResource);
    	ValueMap valueMap = spaResource.adaptTo(ValueMap.class);
    	spa.setSpaTiming(getProperty(valueMap, "spaTiming", String.class, ""));
    	Resource spaRootResource = spaResource.getChild("root");
    	if(null!=spaRootResource) {
    		Resource spaTitleResource = spaRootResource.getChild("title_description");
        	LOG.debug("Recieved Child Title Resource as:: " + spaTitleResource);
        	if(null!=spaTitleResource) {
        		valueMap = spaTitleResource.adaptTo(ValueMap.class);
            	spa.setSpaDesciption(getProperty(valueMap, "headerDescription", String.class, ""));
        	}
    	}
    	Resource hotelResource = resourceResolver.getResource(sPath).getParent().getParent();
    	LOG.debug("Recieved Hotel Resource as:: " + hotelResource);
    	if(null!=hotelResource) {
    		Resource hotelJCRResource =hotelResource.getChild("jcr:content");
    		valueMap = hotelJCRResource.adaptTo(ValueMap.class);
    		spa.setHotelName(getProperty(valueMap, "hotelName", String.class, ""));
    		spa.setUniqueName(removeWhiteSpaces(spa.getHotelName()));
    		spa.setHotelLatitude(getProperty(valueMap, "hotelLatitude", String.class, ""));
    		spa.setHotelLongitude(getProperty(valueMap, "hotelLongitude", String.class, ""));
    		String hotelAddress = formHotelAddress(hotelJCRResource);
    		LOG.debug("Hotel Address formed " + hotelAddress);
    		spa.setHotelAddress(hotelAddress);
    		String imagePath =extractBannerImage(hotelJCRResource);
    		LOG.debug("Extracted banner image path :: " + imagePath);
    		spa.setHotelImage(imagePath);
    	}
    	spaList.add(spa);
    }catch (Exception e) {
    	LOG.debug("Exception occured in "+ methodName + " while accessing resource resolver" + e.getMessage());
    }finally {
    	if(resourceResolver != null && resourceResolver.isLive()) {
    		resourceResolver.close();
    	}
    }
    	LOG.trace("Method Exit: " + methodName);
    }
    
    private String extractBannerImage(Resource resource) {
    	
        Resource bannerParsys = resource.getChild("banner_parsys");
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild("hotel_banner");
            if (hotelBanner != null) {
                ValueMap valMap = hotelBanner.getValueMap();
                String imageURL = String.valueOf(valMap.get("bannerImage"));
                return imageURL;
            }
            LOG.trace("Returning null [condition failed], hotelBanner is null");
            return null;
        }
        LOG.trace("Returning null [condition failed], bannerParsys is null");
        return null;
    }
    
    private void buildIteneraryList()  {
        String methodName = "buildItenaryList";
    	LOG.trace("Method Entry: " + methodName);
    	try {	
    		if (localList != null && localList.length > 0) {
    			for(String jsonStr : localList) {
    				ObjectMapper mapper = new ObjectMapper();
    				LocaleList localLsObj = mapper.readValue(jsonStr, LocaleList.class);
    				localLs.add(localLsObj);	
    			}
    		  buildMapDataUsing(localLs);
    		}	
    	} catch (JsonParseException e) {
    		LOG.error("Error occured while extracting localList json String Array to corresponding JSON Object");
    	} catch (JsonMappingException e) {
    		LOG.error("Error occured while mapping localList json String Array to corresponding POJO Object");
    	} catch (Exception e) {
    		LOG.error("Error occured while building Locale List  " + e.getMessage());
    	}
    	LOG.trace("Method Exit: " + methodName);
        }
    
    private void buildMapDataUsing(List<LocaleList> localLs) {
    	String methodName = "buildMapDataUsing";
    	LOG.trace("Method Entry: " + methodName);
    	ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonNode = mapper.createObjectNode();
        jsonNode.put("data",buildArrayDataNode(localLs));
        iteneraryDataJsonStr =jsonNode.toString();
    	LOG.info("Itenary data JSON object formed :: " + iteneraryDataJsonStr);
    	LOG.trace("Method Exit: " + methodName);
    }
    
    private ArrayNode buildArrayDataNode(List<LocaleList> localLs) {
    	ObjectMapper mapper = new ObjectMapper();
    	ArrayNode node = mapper.createArrayNode();
        ObjectNode jsonNode = mapper.createObjectNode();
        if(null!=resource) {
        	LOG.info("Injected Resource :: " + resource);
        	Resource jcrContentResource = resource.getParent().getParent();
        	if (null!=jcrContentResource) {
        		LOG.info("jcr content resource path  :: " + jcrContentResource.getPath());
        		ObjectNode childJsonNode = mapper.createObjectNode();
        		ValueMap valueMap = jcrContentResource.adaptTo(ValueMap.class);
        		String city = getProperty(valueMap, "jcr:title", String.class,"");
        		String latitude = getProperty(valueMap, "latitude", String.class,"");
        		String longitude = getProperty(valueMap, "longitude", String.class,"");
        		LOG.info("Properties fetched from destination jcr content :: " + city + " && " + latitude + " && " + longitude);
        		jsonNode.put("city",city);
        		childJsonNode.put("lat",latitude);
        		childJsonNode.put("lng", longitude);
        		jsonNode.put("coordinates", childJsonNode);
        	}
        }
        jsonNode.put("localArea",buildLocalAreaJsonArrayNode(localLs));
        node.add(jsonNode);
        return node;
    }
    
    private ArrayNode buildLocalAreaJsonArrayNode(List<LocaleList> localLs) {
    	ObjectMapper mapper = new ObjectMapper();
    	ArrayNode node = mapper.createArrayNode();
        for (int i=0;i<localLs.size();i++) {
        	ObjectNode jsonNode = mapper.createObjectNode();
        	jsonNode.put("id","d"+i);
        	jsonNode.put("name", localLs.get(i).getTitle());
        	jsonNode.put("imagePath", localLs.get(i).getImage());
        	jsonNode.put("openingTime", localLs.get(i).getOpeningTime());
        	jsonNode.put("closingTime", localLs.get(i).getClosingTime());
        	jsonNode.put("description",localLs.get(i).getDescription());
        	ObjectNode childJsonNode = mapper.createObjectNode();
        	childJsonNode.put("lat",localLs.get(i).getLatitude());
    		childJsonNode.put("lng", localLs.get(i).getLongitude());
    		jsonNode.put("coordinates", childJsonNode);
    		node.add(jsonNode);
        }
        return node;
    }
    
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
		String methodName = "getProperty";
		LOG.trace("Method Entry: " + methodName);
		T value = defaultValue;
		if (valueMap.containsKey(key)) {
			value = valueMap.get(key, type);
		}
		LOG.trace("Value found for key: " + key + " : " + value);
		LOG.trace("Method Exit: " + methodName);
		return value;
	}
    
    private String removeWhiteSpaces(String propertyValue) {
    	return propertyValue.replaceAll("[^\\w]", "").trim();
    }

	public List<SightSeeing> getSightSeeing() {
		return sightSeeing;
	}


	public List<ActivitiesToDo> getActivities() {
		return activities;
	}

	public List<Shopping> getShopping() {
		return shopping;
	}

	public Set<Dining> getDining() {
		return dining;
	}


	public Set<Spa> getSpaList() {
		return spaList;
	}

	public List<LocaleList> getLocalLs() {
		return localLs;
	}

	public String getIteneraryDataJsonStr() {
		return iteneraryDataJsonStr;
	}


	public String getGoogleApiKey() {
		return googleApiKey;
	}


	public void setGoogleApiKey(String googleApiKey) {
		this.googleApiKey = googleApiKey;
	}

}
