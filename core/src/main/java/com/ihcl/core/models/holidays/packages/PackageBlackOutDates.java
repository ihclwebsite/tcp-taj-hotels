package com.ihcl.core.models.holidays.packages;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class PackageBlackOutDates {
	
	@Inject
    @Optional
    @Named("startDate")
    public String startDate;
	
	@Inject
    @Optional
    @Named("endDate")
    public String endDate;

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}
	
}
