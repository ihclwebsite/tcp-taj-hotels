/**
 *
 */
package com.ihcl.core.models;

import static com.ihcl.core.ama.Constants.activity.CQ_TAGS;
import static com.ihcl.core.ama.Constants.activity.DESCRIPTION;
import static com.ihcl.core.ama.Constants.activity.IMAGE;
import static com.ihcl.core.ama.Constants.activity.TITLE;
import static com.ihcl.core.ama.Constants.activity.VIEW_DETAIL;
import static com.ihcl.core.ama.Constants.activity.VIEW_URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.ihcl.core.ama.ActivityBean;
import com.ihcl.core.services.search.SearchProvider;


/**
 * <pre>
 * ActivityList.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :28-Nov-2019
 * @ClassName : ActivityList.java
 * @Description : com.ihcl.core.models -ActivityList.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author                 description
 *  ===========     ==================   =========================
 *  28-Nov-2019     Subharun Mukherjee            Create
 *
 *               </pre>
 */

@Model(adaptables = Resource.class,
        adapters = ActivityList.class)
public class ActivityList {

    private static final Logger LOG = LoggerFactory.getLogger(ActivityList.class);

    private static final String RESOURCE_TYPE = "tajhotels/components/content/ama/ama-activities-card";

    @Self
    private Resource resource;

    @Inject
    @Optional
    String parentPath;

    @Inject
    @Optional
    String[] destinationTag;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private SearchProvider searchProvider;


    public List<ActivityBean> getActivityList() {

        List<ActivityBean> activityList = new ArrayList<>();
        ResourceResolver resolver = null;
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            SearchResult results = null;
            LOG.trace("DATA : {} : {}", parentPath, destinationTag);
            results = getSearchResult(parentPath, destinationTag);
            if (null != results) {
                LOG.trace("Result size : {}", results.getHits().size());
                for (Hit hit : results.getHits()) {
                    try {
                        Resource resultResource = resolver.getResource(hit.getPath());
                        if (null != resultResource) {
                            ValueMap valueMap = resultResource.adaptTo(ValueMap.class);
                            if (null != valueMap) {
                                String title = null != valueMap.get(TITLE, String.class)
                                        ? valueMap.get(TITLE, String.class)
                                        : "";
                                String description = null != valueMap.get(DESCRIPTION, String.class)
                                        ? valueMap.get(DESCRIPTION, String.class)
                                        : "";
                                String image = null != valueMap.get(IMAGE, String.class)
                                        ? valueMap.get(IMAGE, String.class)
                                        : "";
                                String viewButton = null != valueMap.get(VIEW_DETAIL, String.class)
                                        ? valueMap.get(VIEW_DETAIL, String.class)
                                        : "";
                                String viewButtonUrl = null != valueMap.get(VIEW_URL, String.class)
                                        ? valueMap.get(VIEW_URL, String.class)
                                        : "";
                                String[] tags = null != valueMap.get(CQ_TAGS, String[].class)
                                        ? valueMap.get(CQ_TAGS, String[].class)
                                        : null;
                                List<String> cqTags = getDestinationTags(tags, resolver);
                                ActivityBean activityBean = new ActivityBean(image, title, description, viewButton,
                                        cqTags, viewButtonUrl);
                                activityList.add(activityBean);
                            }
                        }
                    } catch (RepositoryException e) {
                        LOG.error("Error while returning activity results : {}", e.getMessage());
                        LOG.debug("Error while returning activity results : {}", e);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error getting resolver : {}", e.getMessage());
            LOG.debug("Error getting resolver : {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return activityList;
    }

    public List<String> getAllDestinations() {
        ResourceResolver resolver = null;
        List<String> allDestinations = new ArrayList<>();
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            allDestinations = getDestinationTags(destinationTag, resolver);
        } catch (LoginException e) {
            LOG.error("Exception while getting the destinations :: {}", e.getMessage());
            LOG.debug("Exception while getting the destinations :: {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return allDestinations;
    }

    private List<String> getDestinationTags(String[] destinations, ResourceResolver resolver) {
        List<String> destinationList = new ArrayList<>();
        try {
            if (null != destinations && destinations.length > 0) {
                for (String destination : destinations) {
                    TagManager tagMgr = resolver.adaptTo(TagManager.class);
                    Tag tag = tagMgr.resolve(destination);
                    if (null != tag) {
                        destinationList.add(tag.getTitle());
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Exception while getting the destination tags :: {}", e.getMessage());
            LOG.debug("Exception while getting the destination tags :: {}", e);
        }
        return destinationList;
    }

    private SearchResult getSearchResult(String searchPath, String[] destinationTags) {
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", searchPath);
            predicateMap.put("1_property", "sling:resourceType");
            predicateMap.put("1_property.value", RESOURCE_TYPE);
            predicateMap.put("2_property", "cq:tags");
            for (int i = 0; i < destinationTags.length; i++) {
                predicateMap.put("2_property." + i + "_value", destinationTags[i]);
            }
            predicateMap.put("p.limit", "-1");
            predicateMap.put("orderby", "@rank");
            predicateMap.put("orderby.sort", "desc");
            Gson gson = new Gson();
            String obj = gson.toJson(predicateMap);
            LOG.trace("DATA : {}", obj);

        } catch (Exception e) {
            LOG.error("Error while fetching activity results : {}", e.getMessage());
            LOG.debug("Error while fetching activity results : {}", e);
        }

        return searchProvider.getQueryResult(predicateMap);
    }

}
