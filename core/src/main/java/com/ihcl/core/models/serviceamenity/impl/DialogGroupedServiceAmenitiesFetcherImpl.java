/**
 *
 */
package com.ihcl.core.models.serviceamenity.impl;

import com.adobe.granite.ui.components.Value;
import com.ihcl.core.models.serviceamenity.DialogGroupedServiceAmenitiesFetcher;
import com.ihcl.core.models.serviceamenity.GroupedServiceAmenitiesFetcher;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sampathkumar
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = DialogGroupedServiceAmenitiesFetcher.class)
public class DialogGroupedServiceAmenitiesFetcherImpl implements DialogGroupedServiceAmenitiesFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(DialogGroupedServiceAmenitiesFetcherImpl.class);

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private ResourceResolverFactory resolverFactory;

    private List<String> allAmenities;

    private String[] bedAndBathAmenities;

    private String[] entertainment;

    private String[] otherConveniences;

    private String[] wellnessAmenities;

    private String[] suiteFeatures;

    private String[] hotelFacilities;

    int sizeOfAllAmenities;

    int sizeOfBedAndBathAmenities;

    int sizeOfEntertainment;

    int sizeOfOtherConveniences;

    int sizeOfWellnessAmenities;

    int sizeOfSuiteFeatures;

    int sizeOfHotelFacilities;

    @Inject
    @Default(values = "")
    private String componentPath;

    @PostConstruct
    protected void init() {
        String methodName = "init";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received request as: " + request);
        String contentPath = request.getAttribute(Value.CONTENTPATH_ATTRIBUTE).toString();
        LOG.debug("Fetched content path as: " + request);

        fetchGroupedServiceAmenities(contentPath);
        LOG.trace("Method Exit: " + methodName);
    }

    private void fetchGroupedServiceAmenities(String contentPath) {
        String methodName = "fetchGroupedServiceAmenities";
        LOG.trace("Method Entry: " + methodName);
        try {
            ResourceResolver resolver = resolverFactory.getServiceResourceResolver(null);
            Resource contentResource = resolver.getResource(contentPath);
            LOG.debug("Fetched content resource at: " + contentPath + " as: " + contentResource);
            GroupedServiceAmenitiesFetcher serviceAmenitiesFetcher = contentResource.adaptTo(GroupedServiceAmenitiesFetcher.class);
            fetchServiceAmenities(contentResource);
        } catch (Throwable e) {
            LOG.error("An error occured while fetching grouped service amenities.", e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void fetchServiceAmenities(Resource resource) {
        String methodName = "fetchServiceAmenities";
        LOG.trace("Method Entry: " + methodName);
        GroupedServiceAmenitiesFetcher serviceAmenitiesFetcher = resource.adaptTo(GroupedServiceAmenitiesFetcher.class);
        try {
            if (serviceAmenitiesFetcher != null) {
                bedAndBathAmenities = serviceAmenitiesFetcher.getBedAndBathAmenities();
                entertainment = serviceAmenitiesFetcher.getEntertainment();
                otherConveniences = serviceAmenitiesFetcher.getOtherConveniences();
                wellnessAmenities = serviceAmenitiesFetcher.getWellnessAmenities();
                suiteFeatures = serviceAmenitiesFetcher.getSuiteFeatures();
                hotelFacilities = serviceAmenitiesFetcher.getHotelFacilities();

                sizeOfOtherConveniences = otherConveniences == null ? 0 : otherConveniences.length;
                sizeOfBedAndBathAmenities = bedAndBathAmenities == null ? 0 : bedAndBathAmenities.length;
                sizeOfEntertainment = entertainment == null ? 0 : entertainment.length;
                sizeOfHotelFacilities = hotelFacilities == null ? 0 : hotelFacilities.length;
                sizeOfSuiteFeatures = suiteFeatures == null ? 0 : suiteFeatures.length;
                sizeOfWellnessAmenities = wellnessAmenities == null ? 0 : wellnessAmenities.length;

                populateAllAmenities();
            }
        } catch (Throwable e) {
            LOG.error("Exception : ", e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void populateAllAmenities() {
        String methodName = "populateAllAmenities";
        LOG.trace("Method Entry: " + methodName);
        allAmenities = new ArrayList<>();

        addToAllAmenities(bedAndBathAmenities);
        addToAllAmenities(entertainment);
        addToAllAmenities(suiteFeatures);
        addToAllAmenities(hotelFacilities);
        addToAllAmenities(otherConveniences);
        addToAllAmenities(wellnessAmenities);

        LOG.debug("Populated all amenities as: " + allAmenities);
        LOG.trace("Method Exit: " + methodName);
    }

    private void addToAllAmenities(String[] amenities) {
        String methodName = "addToAllAmenities";
        LOG.trace("Method Entry: " + methodName);
        if (amenities != null) {
            int amenitiesLength = amenities.length;
            LOG.debug("Attempting to add the given amenities array with length: " + amenitiesLength
                    + " to all amenities");
            if (amenitiesLength > 0) {
                allAmenities.addAll(Arrays.asList(amenities));
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }


    @Override
    public List<String> getAllAmenities() {
        return allAmenities;
    }

    @Override
    public String[] getBedAndBathAmenities() {
        return bedAndBathAmenities;
    }

    @Override
    public String[] getEntertainment() {
        return entertainment;
    }

    @Override
    public String[] getOtherConveniences() {
        return otherConveniences;
    }

    @Override
    public String[] getWellnessAmenities() {
        return wellnessAmenities;
    }

    @Override
    public String[] getSuiteFeatures() {
        return suiteFeatures;
    }

    @Override
    public String[] getHotelFacilities() {
        return hotelFacilities;
    }

    public int getSizeOfAllAmenities() {
        return sizeOfAllAmenities;
    }

    public int getSizeOfBedAndBathAmenities() {
        return sizeOfBedAndBathAmenities;
    }

    public int getSizeOfEntertainment() {
        return sizeOfEntertainment;
    }

    public int getSizeOfOtherConveniences() {
        return sizeOfOtherConveniences;
    }

    public int getSizeOfWellnessAmenities() {
        return sizeOfWellnessAmenities;
    }

    public int getSizeOfSuiteFeatures() {
        return sizeOfSuiteFeatures;
    }

    public int getSizeOfHotelFacilities() {
        return sizeOfHotelFacilities;
    }

}
