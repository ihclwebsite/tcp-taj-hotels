package com.ihcl.core.models.gallery;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface to fetch dam format.
 * 
 */
@ConsumerType
public interface DamFormatFetcher {

	/**
	 * Method to fetch the dam format from the given dam element.
	 * 
	 * @param damElement
	 * @return damFormat
	 */
    String getDamFormat();
}
