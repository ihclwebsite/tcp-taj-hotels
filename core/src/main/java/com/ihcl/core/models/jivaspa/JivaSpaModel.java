/**
 *
 */
package com.ihcl.core.models.jivaspa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = JivaSpaModel.class)
public class JivaSpaModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    private Resource resource;

    @Inject
    SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    private String currentPagePath;


    @Reference
    private QueryBuilder builder;

    private Session session;

    private ResourceResolver resolver;

    @PostConstruct
    public void init() {
        getJivaSpaList();
    }

    public List<String> getJivaSpaList() {

        String methodName = "getJivaSpaList";
        LOG.trace(" Method Entry :" + methodName);
        List<String> pagePath = new ArrayList<String>();
        try {

            LOG.trace("Resource Path :" + resource.getPath());
            String spaType = null;
            if (resource.getResourceType().equals("tajhotels/components/content/jiva-spa-list")) {
                ValueMap valueMap = resource.adaptTo(ValueMap.class);
                spaType = valueMap.get("spaType", String.class);
            }

            LOG.trace("spatype :" + spaType);

            fetchCurrentPath();
            Map<String, String> queryParam = new HashMap<String, String>();
            queryParam.put("type", "cq:Page");
            queryParam.put("path", currentPagePath);
            queryParam.put("property", "jcr:content/cq:template");
            queryParam.put("property.value", "/conf/tajhotels/settings/wcm/templates/jiva-spa-details");
            queryParam.put("fulltext", spaType);
            queryParam.put("p.limit", "-1");
            LOG.trace(" queryParam : " + queryParam);

            resolver = resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);

            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            LOG.trace("Query builder : " + builder);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            LOG.trace("SearchPredicates : " + searchPredicates);
            Query query = builder.createQuery(searchPredicates, session);

            LOG.trace("Query to be executed is : " + query);
            query.setStart(0L);

            SearchResult result = query.getResult();
            LOG.trace("Query Executed");
            LOG.trace("Query result is : " + result.getHits().size());

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                pagePath.add(hit.getPath());
            }
            LOG.trace(" Page Path :" + pagePath);
            LOG.trace(" Page Path Size:" + pagePath.size());
            LOG.trace(" Method Exit :" + methodName);
        } catch (

        Exception e) {
            e.printStackTrace();
        }
        return pagePath;

    }

    private void fetchCurrentPath() {
        String methodName = "fetchCurrentPath";
        LOG.trace(" Method Entry :" + methodName);
        Resource currentPageResource = currentPage.getParent().getContentResource();
        LOG.trace("currentPageResource :" + currentPageResource);
        ValueMap valueMap = currentPageResource.adaptTo(ValueMap.class);
        String currentPageTemplate = valueMap.get("cq:template", String.class);
        if (currentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/hotel-landing-page")
                || currentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/destination-landing-page")
                || currentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/home-page")) {
            currentPagePath = currentPage.getParent().getPath();
            LOG.trace("pagePath : " + currentPagePath);
        }
        LOG.trace("Method Exit: " + methodName);

    }
}
