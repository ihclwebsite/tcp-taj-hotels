package com.ihcl.core.models.holidays.itinerary;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface DestinationItineraryPropertyFetcher {

	ItineraryDetails getItineraryDetails();

}
