package com.ihcl.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class UpcomingEvents {
    @Inject
    @Optional
    private String componentTitle;

    @Inject
    @Optional
    private List<UpcomingEvent> events;

    public String getComponentTitle() {
        return componentTitle;
    }

    public List<UpcomingEvent> getEvents() {
        return events;
    }
}
