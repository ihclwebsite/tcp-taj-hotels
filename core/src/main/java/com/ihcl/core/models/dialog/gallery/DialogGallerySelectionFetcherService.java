package com.ihcl.core.models.dialog.gallery;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface that defines methods using which a gallery-image-selector component's dialog values can be populated.
 */
@ConsumerType
public interface DialogGallerySelectionFetcherService {

    String getSourceSelection();

    List<String> getSelectedStaticImagePaths();

    String getParentPath();

    Integer getDepth();

}
