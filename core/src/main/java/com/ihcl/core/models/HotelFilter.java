package com.ihcl.core.models;

import java.util.Map;

public interface HotelFilter {

 Map<String,String> getHotelTypes();
	
}
