package com.ihcl.core.models;


public class AwardMultifieldBean {

	private String awardTitle;
	private String awardImagePath;
	private String awardDesc;
	public String getAwardTitle() {
		return awardTitle;
	}
	public void setAwardTitle(String awardTitle) {
		this.awardTitle = awardTitle;
	}
	public String getAwardImagePath() {
		return awardImagePath;
	}
	public void setAwardImagePath(String awardImagePath) {
		this.awardImagePath = awardImagePath;
	}
	public String getAwardDesc() {
		return awardDesc;
	}
	public void setAwardDesc(String awardDesc) {
		this.awardDesc = awardDesc;
	}
}
