package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.ihcl.core.models.Destination;
import com.ihcl.core.models.HotelPageList;
import com.ihcl.core.models.offers.OfferImpl;
import com.ihcl.core.services.search.DestinationSearchService;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.core.services.search.OffersSearchService;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = HotelPageList.class)
public class HotelPageListImpl implements HotelPageList {

    private static final Logger LOG = LoggerFactory.getLogger(HotelPageListImpl.class);

    @Inject
    private HotelsSearchService hotelsearchService;

    @Inject
    private DestinationSearchService destinationSearchService;

    @Inject
    OffersSearchService offersSearchService;

    @Self
    SlingHttpServletRequest request;

    private Resource resource;

    private Resource tagResource;

    private ValueMap valueMap;

    @Inject
    private ResourceResolverFactory resolverFactory;

    private ResourceResolver resourceResolver = null;

    @Reference
    SearchProvider searchProvider;

    private Session session;

    private List<String> allHotelPagePath = new ArrayList<>();

    private Map<String, Map<String, Map<String, String>>> cityNameHotelNamesAndLinks = null;

    private Map<String, List<Destination>> countryMapSitemap;

    private Map<String, String> destinationLink;

    private Map<String, String> destinationDiningLink;

    private Map<String, String> destinationMeetingLink;

    private Map<String, String> offerNameList = null;

    private List<String> pagePath = null;

    private Map<Page, String> offerAllOffersList = null;

    private Map<String, Map<String, Map<String, String>>> hotelMeetingDetails = null;

    private static final String COUNTRY_TYPE_ALL = "all";

    private static final String RESTAURANTS_IN = "/restaurants-in-";

    @Inject @Default(values= "/content/tajhotels")
    private String rootPath;

    private static final String CONTENT_ROOT_PATH = "/content/tajhotels";

    @PostConstruct
    protected void init() {
        try {
            LOG.trace("Inside 12 method init()");
            allHotelPagePath = hotelsearchService.getAllHotels(rootPath + "/en-in/our-hotels");
            try {
                resourceResolver = resolverFactory.getServiceResourceResolver(null);
            } catch (LoginException e) {
                LOG.error("LoginException occured in init method :: {}", e.getMessage());
            }
            destinationLink = new HashMap<>();
            destinationDiningLink = new HashMap<>();
            destinationMeetingLink = new HashMap<>();
            cityNameHotelNamesAndLinks = new HashMap<>();
            hotelMeetingDetails = new HashMap<>();

            countryMapSitemap = destinationSearchService.getDestinationModelMap(rootPath);
            // getDestinationWithLinks();
            getAllCitiesAndHotelsByCountry();

            getAllOffers();
            getAllHotelsMeetingsPath(rootPath);
            getAllMeetingDetails();
            LOG.trace("Method exit - init()");
        } catch (Exception e) {
            LOG.error("Exception occured in init method :: {}", e.getMessage());
        }
    }


    @Override
    public Map<String, Map<String, Map<String, String>>> getHotelListMapData() {
        return cityNameHotelNamesAndLinks;
    }


    private void getAllCitiesAndHotelsByCountry() {

        String hotelCountry = "";
        String hotelCity = "";
        for (String hotelPagePath : allHotelPagePath) {

            resource = resourceResolver.getResource(hotelPagePath + "/jcr:content");
            valueMap = resource.adaptTo(ValueMap.class);

            Map<String, Map<String, String>> hotelCityLinkMap = new HashMap<>();
            Map<String, Map<String, String>> hotelCityLinkMapData = new HashMap<>();
            Map<String, String> hotelNameLinkMapData = new HashMap<>();
            Map<String, String> hotelNameLinkMapDataNew = new HashMap<>();

            String hotelLocations = valueMap.get("locations", String.class);
            LOG.debug("Current hotelLocations value is: " + hotelLocations);

            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (tagManager != null) {
                Tag tagRoot = tagManager.resolve(hotelLocations);
                if (tagRoot != null) {
                    LOG.debug("tagRoot value is not null");
                    tagResource = tagRoot.adaptTo(Resource.class);
                    hotelCountry = tagResource.getParent().adaptTo(Tag.class).getTitle();
                    hotelCity = tagResource.adaptTo(Tag.class).getTitle();
                } else if (tagRoot == null) {
                    LOG.debug("tagRoot value is null");
                }
            } else if (tagManager == null) {
                LOG.debug("tagManager is null");
            }
            String hotelName = valueMap.get("hotelName", String.class);

            String[] hotelPagePathSplitted = hotelPagePath.substring(1).split("/");

            String hotelPageLink = "/en-in/" + hotelPagePathSplitted[5] + "/" + hotelPagePathSplitted[6];
            String destinationPageLink = "/" + hotelPagePathSplitted[0] + "/" + hotelPagePathSplitted[1] + "/"
                    + hotelPagePathSplitted[2] + "/" + hotelPagePathSplitted[3] + "/" + hotelPagePathSplitted[4];
            String destinationNameInPath = hotelPagePathSplitted[4]
                    .substring(hotelPagePathSplitted[4].lastIndexOf('-') + 1);
            destinationLink.put(hotelCity, destinationPageLink);
            destinationDiningLink.put(hotelCity, destinationPageLink + RESTAURANTS_IN + destinationNameInPath);
            LOG.debug("destination link : " + destinationPageLink);
            if (hotelCountry != "" && cityNameHotelNamesAndLinks.containsKey(hotelCountry)) {
                hotelCityLinkMapData = cityNameHotelNamesAndLinks.get(hotelCountry);
                if (hotelCityLinkMapData.containsKey(hotelCity)) {
                    hotelCityLinkMapData.get(hotelCity).put(hotelName, hotelPagePath);
                } else {
                    hotelNameLinkMapData.put(hotelName, hotelPagePath);
                    hotelCityLinkMapData.put(hotelCity, hotelNameLinkMapData);
                }
            } else {
                hotelNameLinkMapDataNew.put(hotelName, hotelPagePath);
                hotelCityLinkMap.put(hotelCity, hotelNameLinkMapDataNew);
                cityNameHotelNamesAndLinks.put(hotelCountry, hotelCityLinkMap);
            }

        }

    }

    private void getAllOffers() {
        LOG.trace("Inside method : getAllOffers()");
        offerAllOffersList = new HashMap<>();

        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

        List<String> offersPathList = offersSearchService.getAllOffers(rootPath + "/en-in");
        LOG.debug("offersPathList data : " + offersPathList.toString());
        for (String path : offersPathList) {
            offerAllOffersList.put(pageManager.getContainingPage(path), path);
        }
        LOG.debug("offerAllOffersList data : " + offerAllOffersList.toString());
    }

    @Override
    public Map<String, String> getOfferNameList() {
        LOG.trace("Inside method : getOfferNameList()");

        offerNameList = new HashMap<>();
        for (Page page : offerAllOffersList.keySet()) {
            OfferImpl offer = page.getContentResource().adaptTo(OfferImpl.class);
            LOG.debug("Value of offer description is : " + offer.getOfferTitle());
            // String[] offerPathWords = offerAllOffersList.get(page).substring(1).split("/");
            // int lastIndex = offerPathWords.length;
            String offerPath = offerAllOffersList.get(page);
            // offerNameList.put(offer.getOfferTitle(), "/en-in/offers/" + offerPathWords[lastIndex - 1] + ".html");
            offerNameList.put(offer.getOfferTitle(), offerPath);
        }
        LOG.debug("Data in offerNameList : " + offerNameList.toString());
        return offerNameList;
    }

    @Override
    public Map<String, Map<String, Map<String, String>>> getMeetingDetails() {
        return hotelMeetingDetails;
    }

    private void getAllHotelsMeetingsPath(String rootPath) {
        pagePath = new ArrayList<String>();
        try {
            Map<String, String> queryParam = new HashMap<String, String>();
            queryParam.put("path", rootPath + "/en-in");
            queryParam.put("type", "cq:Page");
            queryParam.put("property", "jcr:content/cq:template");
            queryParam.put("property.value",
                    "/conf/tajhotels/settings/wcm/templates/taj-hotel-meeting-details-template");
            queryParam.put("p.limit", "-1");

            session = resourceResolver.adaptTo(Session.class);

            QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            Query query = builder.createQuery(searchPredicates, session);

            query.setStart(0L);

            SearchResult result = query.getResult();

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                pagePath.add(hit.getPath());
            }

            LOG.trace("Page Path Size:" + pagePath.size());
        } catch (Exception e) {
            LOG.error("Error/Exception is thrown while fetching the meeting list" + e.toString(), e);
        }
    }

    private void getAllMeetingDetails() {
        LOG.trace("Inside method : getAllMeetingDetails()");
        String hotelName = "";
        String hotelCountry = "";
        String hotelCity = "";
        for (String meetingPagePath : pagePath) {

            String[] meetingPathWords = meetingPagePath.substring(1).split("/");
            StringBuilder hotelPath = new StringBuilder("/");

            for (int i = 0; i < 7; i++) {
                hotelPath.append("/" + meetingPathWords[i]);
            }

            LOG.debug("Value of hotelPath is :" + hotelPath);

            resource = resourceResolver.getResource(hotelPath.toString() + "/jcr:content");
            LOG.debug("value of resource : " + resource.getPath());
            valueMap = resource.adaptTo(ValueMap.class);
            LOG.debug("value of title in valueMap : " + valueMap.get("jcr:title", String.class));

            Map<String, Map<String, String>> hotelMeetingLinkMapData = new HashMap<>();
            Map<String, Map<String, String>> hotelNameLinkMapDataNew = new HashMap<>();
            Map<String, String> meetingMap = new HashMap<>();
            Map<String, String> meetingMapData = new HashMap<>();

            String hotelLocations = valueMap.get("locations", String.class);
            LOG.debug("Current location value: " + hotelLocations);

            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (tagManager != null) {
                LOG.debug("tagManager value is not null");
                Tag tagRoot = tagManager.resolve(hotelLocations);
                if (tagRoot != null) {
                    LOG.debug("tagRoot value is not null");
                    tagResource = tagRoot.adaptTo(Resource.class);
                    hotelCountry = tagResource.getParent().adaptTo(Tag.class).getTitle();
                    hotelCity = tagResource.adaptTo(Tag.class).getTitle();
                } else if (tagRoot == null) {
                    LOG.debug("tagRoot value is null");
                }
            } else if (tagManager == null) {
                LOG.debug("tagManager value is null");
            }
            hotelName = valueMap.get("hotelName", String.class);

            resource = resourceResolver.getResource(meetingPagePath + "/jcr:content");
            valueMap = resource.adaptTo(ValueMap.class);

            String meetingName = valueMap.get("jcr:title", String.class);
            LOG.debug("meetingName value : " + meetingName);

            String[] hotelPagePathSplitted = meetingPagePath.substring(1).split("/");

            String hotelPageLink = "/en-in/" + hotelPagePathSplitted[5] + "/" + hotelPagePathSplitted[6] + "/"
                    + hotelPagePathSplitted[7];


            if (hotelCountry != "" && hotelMeetingDetails.containsKey(hotelCountry)) {
                hotelMeetingLinkMapData = hotelMeetingDetails.get(hotelCountry);
                if (hotelMeetingLinkMapData.containsKey(hotelCity)) {
                    hotelMeetingLinkMapData.get(hotelCity).put(hotelName, hotelPageLink);
                } else {
                    meetingMap.put(hotelName, hotelPageLink);
                    hotelMeetingLinkMapData.put(hotelCity, meetingMap);
                }
            } else {
                meetingMapData.put(hotelName, hotelPageLink);
                hotelNameLinkMapDataNew.put(hotelCity, meetingMapData);
                hotelMeetingDetails.put(hotelCountry, hotelNameLinkMapDataNew);
            }

            LOG.debug("hotelMeetingDetails value : " + hotelMeetingDetails.toString());


        }

    }

    @Override
    public Map<String, String> getAllDestinationLinks() {
        return destinationLink;
    }


    @Override
    public Map<String, String> getAllDestinationDiningLinks() {
        return destinationDiningLink;
    }

}
