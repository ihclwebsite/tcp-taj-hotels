package com.ihcl.core.models.gallery.impl;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.ihcl.core.models.gallery.DamFormatFetcher;

/**
 * Class to get dam format from the given dam element.
 * 
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
adapters = DamFormatFetcher.class)
public class DamFormatFetcherImpl   implements DamFormatFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(GalleryImageSelectorImpl.class);

    @Inject
    private String damElement;
    
    @Inject
    private ResourceResolverFactory resolverFactory;
    
    private ResourceResolver resourceResolver;
    Asset imageAsset = null;
    
    private String damFormat;

    @PostConstruct
    public void activate()  {
    	
    	LOG.trace("DAM from HTML "+damElement);
    	try {
    		resourceResolver = resolverFactory.getServiceResourceResolver(null);
            if (resourceResolver != null) {
                LOG.debug("Resolver is not Null & User ID : " + resourceResolver.getUserID());
            } else if (resourceResolver == null) {
                LOG.debug("resourceResolver is null, Can't proceed further. ");
            }
            
            LOG.trace("Resource here : "+resourceResolver +" For the dam element :"+damElement);
            
			Resource imageResource = resourceResolver.getResource(damElement);
			
			 LOG.trace("Image Resource here : "+imageResource);
			
			imageAsset = imageResource.adaptTo(Asset.class);
			
			 LOG.trace("Image asset here : "+imageAsset);
			
		LOG.trace("Asset type is :"+imageAsset.getMetadataValue("dc:format"));
		damFormat=imageAsset.getMetadataValue("dc:format");
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
    }
    
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDamFormat() {
		 LOG.trace("Received DAM element as : "+damFormat);
		return damFormat;
	}

}
