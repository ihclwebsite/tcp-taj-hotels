/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclCardBean {


    private String imageCard;

    private String title;

    private String titleVariation;

    private String description;

    private String buttontext;

    private String pagePath;

    private String cardidTitle;
    
    private String popupdescription;

    public String getPagePath() {
        return pagePath;
    }


    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }


    /**
     * Getter for the field imageCard
     *
     * @return the imageCard
     */
    public String getImageCard() {
        return imageCard;
    }


    /**
     * Setter for the field imageCard
     *
     * @param imageCard
     *            the imageCard to set
     */

    public void setImageCard(String imageCard) {
        this.imageCard = imageCard;
    }


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for the field titleVariation
     *
     * @return the titleVariation
     */
    public String getTitleVariation() {
        return titleVariation;
    }


    /**
     * Setter for the field titleVariation
     *
     * @param titleVariation
     *            the titleVariation to set
     */

    public void setTitleVariation(String titleVariation) {
        this.titleVariation = titleVariation;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the field buttontext
     *
     * @return the buttontext
     */
    public String getButtontext() {
        return buttontext;
    }


    /**
     * Setter for the field buttontext
     *
     * @param buttontext
     *            the buttontext to set
     */

    public void setButtontext(String buttontext) {
        this.buttontext = buttontext;
    }

    public String getCardidTitle() {
        return cardidTitle;
    }


    public void setCardidTitle(String cardidTitle) {
        this.cardidTitle = cardidTitle;
    }


	public String getPopupdescription() {
		return popupdescription;
	}


	public void setPopupdescription(String popupdescription) {
		this.popupdescription = popupdescription;
	}
    
    

}
