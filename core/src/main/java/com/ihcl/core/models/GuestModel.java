package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class GuestModel extends WCMUsePojo {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	private List<BookingBean> guestList = new ArrayList<>();

	@Override
	public void activate() throws Exception {

		Node currentNode = getResource().adaptTo(Node.class);
		if(currentNode != null) {
			NodeIterator ni = currentNode.getNodes();
			while (ni.hasNext()) {
				Node child =  ni.nextNode();
				if(child != null) {
					NodeIterator ni2 = child.getNodes();
					setGuestList(ni2);
				}
			}
		}
	}

	private void setGuestList(NodeIterator ni2) {

		try {
			while (ni2.hasNext()) {
				BookingBean imgObj = new BookingBean();
				Node grandChild = ni2.nextNode();
				if(grandChild != null) {
					
					if (grandChild.hasProperty("guestFormItemLabel") && grandChild.hasProperty("guestFormItemField")) {
						log.info("*** GRAND CHILD NODE PATH IS" + grandChild.getPath());
						imgObj.setGuestFormItemLabel(grandChild.getProperty("guestFormItemLabel").getString());
						imgObj.setGuestFormItemField(grandChild.getProperty("guestFormItemField").getString());
					}

					if(imgObj.getGuestFormItemLabel() != null && imgObj.getGuestFormItemField() != null) {
						guestList.add(imgObj);
					}
				}
			}
		}
		catch (Exception e) {
			log.error("Exception in setGuestList", e.getMessage(), e);
		}
	}

	public List<BookingBean> getGuestList() {
		
		return guestList;
	}
}
