/**
 *
 */
package com.ihcl.core.models;


public class EventBookingRequest {

    String customerName;

    String customerEmail;

    String phoneNumber;

    String eventName;

    String eventVenue;

    String eventPath;

    String mapLocation;

    String availableSeats;

    String propertyCode;

    String eventDate;

    String productName;

    String tickets;

    String currency;

    String totalAmount;


    public String getCustomerName() {
        return customerName;
    }


    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public String getCustomerEmail() {
        return customerEmail;
    }


    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public String getEventName() {
        return eventName;
    }


    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public String getEventVenue() {
        return eventVenue;
    }


    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }


    public String getEventPath() {
        return eventPath;
    }


    public void setEventPath(String eventPath) {
        this.eventPath = eventPath;
    }


    public String getMapLocation() {
        return mapLocation;
    }


    public void setMapLocation(String mapLocation) {
        this.mapLocation = mapLocation;
    }


    public String getAvailableSeats() {
        return availableSeats;
    }


    public void setAvailableSeats(String availableSeats) {
        this.availableSeats = availableSeats;
    }


    public String getPropertyCode() {
        return propertyCode;
    }


    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }


    public String getEventDate() {
        return eventDate;
    }


    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }


    public String getProductName() {
        return productName;
    }


    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getTickets() {
        return tickets;
    }


    public void setTickets(String tickets) {
        this.tickets = tickets;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getTotalAmount() {
        return totalAmount;
    }


    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }


    // public EventBookingRequest(String customerName, String customerEmail, String phoneNumber, String eventName,
    // String eventVenue, String eventPath, String mapLocation, String availableSeats, String propertyCode,
    // String eventDate, String currency, String tickets, String totalAmount) {
    // super();
    // this.customerName = customerName;
    // this.customerEmail = customerEmail;
    // this.phoneNumber = phoneNumber;
    // this.eventName = eventName;
    // this.eventVenue = eventVenue;
    // this.eventPath = eventPath;
    // this.mapLocation = mapLocation;
    // this.availableSeats = availableSeats;
    // this.propertyCode = propertyCode;
    // this.eventDate = eventDate;
    // this.productName = currency;
    // this.tickets = tickets;
    // this.totalAmount = totalAmount;
    // }


}
