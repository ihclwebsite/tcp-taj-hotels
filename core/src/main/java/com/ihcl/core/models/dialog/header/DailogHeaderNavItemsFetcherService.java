package com.ihcl.core.models.dialog.header;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface that defines methods using which a gallery-image-selector component's dialog values can be populated.
 */
@ConsumerType
public interface DailogHeaderNavItemsFetcherService {


}
