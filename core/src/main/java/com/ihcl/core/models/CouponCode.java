package com.ihcl.core.models;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class CouponCode extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    private Map<String,String> couponCodeMap = new HashMap<String,String>();
    
    private String key="couponCodes";
    
    private String couponCodesString;

	public String getCouponCodesString() {
		return couponCodesString;
	}

	public void setCouponCodesString(String couponCodesString) {
		this.couponCodesString = couponCodesString;
	}

	@Override
	public void activate() throws Exception {
		
		String methodName ="activate";
		log.trace("Method Entry :: " + methodName);
		
		Node currentNode = getResource().adaptTo(Node.class);
		if(currentNode!=null) { // handling null check
	        NodeIterator ni = currentNode.getNodes();
	        while (ni.hasNext()) {
	
	            Node child = ni.nextNode();
	            NodeIterator ni2 = child.getNodes();
	            processNode(ni2);
	        }
		}
        setCouponCodesString(buildJSONObject(key,couponCodeMap).toString()); 
      log.trace("Method Exit :: " +methodName);
	}
	
	private void processNode(NodeIterator ni2) {
		
		String methodName ="processNode";
		log.trace("Method Entry :: " + methodName);
		
		 try {
	            while (ni2.hasNext()) {

	                Node grandChild = ni2.nextNode();	                
	                if(grandChild.getPath().contains(key)) {
	                	log.trace("Grand Child Node Path for CouponCodes :: > " + grandChild.getPath());
	                	if(grandChild.hasProperty("couponLabel") && grandChild.hasProperty("couponValue"))
	                		couponCodeMap.put(grandChild.getProperty("couponLabel").getString(), grandChild.getProperty("couponValue").getString());
	                }
	            }
	        } catch (Exception e) {
	            log.error("Exception while fetching Multifield data :: >", e.getMessage(), e);
	        }
		log.trace("Method Exit :: " +methodName);
	}
	
	private JSONObject buildJSONObject (String key, Map<String,String> inputMap) {
        
        JSONObject jsonObject = new JSONObject();
        try {
        	jsonObject.put(key, inputMap);
		} catch (JSONException e) {
			log.error("Exception occured while creating the JSON Object :: > " + e.getMessage());
		}
		return jsonObject;	
	}        
}
