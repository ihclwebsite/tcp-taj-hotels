/**
 *
 */
package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.LocalareaFilter;

@Model(adaptables = Resource.class, adapters = LocalareaFilter.class)
public class LocalareaFilterImpl implements LocalareaFilter {

	private List<String> attractions;

	@Self
	Resource resource;

	ResourceResolver resourceResolver;

	@Inject
	ResourceResolverFactory resourceResolverFactory;

	private static final Logger LOG = LoggerFactory.getLogger(LocalareaFilterImpl.class);

	@PostConstruct
	public void init() {
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			attractions = new ArrayList<>();
			attractions = getTagList("/etc/tags/taj/Attractions");
		} catch (LoginException e) {
			LOG.error("Failed to retreive resource resolver using service resolver factory");
		}
	}

	public List<String> getTagList(String path) {
		LOG.debug("Tags path data : " + path);
		List<String> tagList = new ArrayList<String>();
		if (resourceResolver != null) {
			Resource tagRoot = resourceResolver.getResource(path);
			if (tagRoot != null) {
				Iterable<Resource> childTags = tagRoot.getChildren();
				for (Resource resource : childTags) {

					tagList.add((String) resource.getValueMap().get("jcr:title"));
					// LOG.debug(">> Resource Name: " + resource.getPath());

				}
			}
		}
		return tagList;
	}

	@Override
	public List<String> getAttractions() {
		return attractions;
	}

	/*
	 * Depth Index is part of the tag if tagId is '/abc/def/taj/Attractions' pass 0
	 * to get 'abc', pas 1 to get 'def', pass 2 to get 'taj', pass 3 to get
	 * 'Attractions'
	 */
	/*
	 * private String splitTags(String tagId, int depthIndex) { String[] tagValues =
	 * tagId.split("/"); LOG.debug("TAG value array : " + tagValues[0] +
	 * " at depthIndex : " + depthIndex + " value at depthIndex : " +
	 * tagValues[depthIndex]); return tagValues[depthIndex]; }
	 */

}
