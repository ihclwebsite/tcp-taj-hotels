/**
 *
 */
package com.ihcl.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * @author Gayathri
 *
 */
@Model(
        adaptables = Resource.class)
public class MiscellaneousBean {

    @Inject
    @Optional
    @Named("miscellaneous")
    private String miscellaneousName;

    @Inject
    @Optional
    @Named("hotelName")
    private String hotelName;

    @Inject
    @Optional
    @Named("address")
    private String address;

    @Inject
    @Optional
    @Named("contactNumber")
    private String contactNumber;

    @Inject
    @Optional
    @Named("miscellaneousType")
    private String miscellaneousType;


    public String getMiscellaneousName() {
        return miscellaneousName;
    }

    public String getHotelName() {
        return hotelName;
    }


    public String getAddress() {
        return address;
    }


    public String getContactNumber() {
        return contactNumber;
    }


    public String getMiscellaneousType() {
        return miscellaneousType;
    }
}
