/**
 *
 */
package com.ihcl.core.models.localarea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.jackrabbit.oak.commons.json.JsonObject;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.JcrTagManagerFactory;
import com.ihcl.core.shared.services.ResourceFetcherService;

@Model(adaptables = Resource.class)
public class ShowMapModel {

    private String areaTitle;

    private String contactNumber;

    private String locationAddress;

    private String time;

    String test;

    private String currentPagePath;

    private ResourceResolver resolver;

    private Session session;

    private JSONObject jsonobj = new JSONObject();

    private JSONObject jsonobject = new JSONObject();

    private JSONArray jsonArray = new JSONArray();

    @Reference
    JcrTagManagerFactory tmf;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    List<ValueMap> addressList = new ArrayList<ValueMap>();

    int localAreaId = 101;

    @Self
    Resource resource;

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    private static final Logger LOG = LoggerFactory.getLogger(ShowMapModel.class);

    private String name;

    List<Resource> children;

    HashMap<String, String> addressMap;

    JsonObject obj = new JsonObject();

    private String resourceURL;

    private String pagePathUri;

    @PostConstruct
    protected void init() {
        try {
            LOG.trace("inside showmap model init() method");
            resolver = resourceResolverFactory.getServiceResourceResolver(null);// resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);
        } catch (LoginException e) {
            LOG.error("Failed to retreive resource resolver using service resolver factory");
        }
    }

    public String getCoordinatesList() throws JSONException {

        LOG.trace(" Method Entry :getCoordinatesList  ");
        List<String> pagePath = new ArrayList<String>();
        try {

            LOG.trace("\nShowMapModel -> Resource Path :" + resource.getPath());
            fetchCurrentPath();
            Map<String, String> queryParam = new HashMap<String, String>();
            LOG.debug("INSIDE THE getCoordinatesList()      " + currentPagePath);// /content/tajhotels/en-in/our-hotels/bangalore/Area-List
            LOG.debug("parent path for jcr: " + resource.getParent().getParent().getPath());
            LOG.debug("parent path for jcr: of cq:tag  "
                    + resource.getParent().getParent().getParent().getValueMap().get("cq:tags"));
            queryParam.put("type", "nt:unstructured");
            queryParam.put("path", currentPagePath);
            queryParam.put("property", "sling:resourceType");
            queryParam.put("property.value", "tajhotels/components/content/local-area-details");
            queryParam.put("p.limit", "-1");
            LOG.trace(" \nShowMapModel -> queryParam : " + queryParam);

            // resolver = resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);

            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            LOG.trace("\nShowMapModel -> Query builder : " + builder);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            LOG.trace("\nShowMapModel -> SearchPredicates : " + searchPredicates);
            Query query = builder.createQuery(searchPredicates, session);

            LOG.trace("Query to be executed is : " + query);
            query.setStart(0L);

            SearchResult result = query.getResult();
            LOG.trace("\nShowMapModel -> Query Executed");
            LOG.trace("\nShowMapModel -> Query result is : " + result.getHits().size());

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                pagePath.add(hit.getPath());
            }
            LOG.trace(" \nShowMapModel ->  Page Path :" + pagePath);
            LOG.trace("\nShowMapModel ->  Page Path Size:" + pagePath.size());
            LOG.trace(" \nShowMapModel -> Method Exit : getCoordinatesList");
            getValue(pagePath);
            getDestinationName();
        } catch (Exception e) {
            e.printStackTrace();
            LOG.debug("EXCEPTION - " + e);
        }
        jsonArray.put(jsonobj);
        jsonobject.put("data", jsonArray);
        LOG.debug("Json data - " + jsonobject.toString());
        return "" + jsonobject.toString();
    }

    private void getDestinationName() {
        try {
            Resource currentResourcePath = resolver.getResource(currentPagePath);
            currentResourcePath = currentResourcePath.getParent();
            LOG.debug("INSIDE THE ShowMapModel.getDestinationName() - currentResourcePath - "
                    + currentResourcePath.getPath());
            getPageTitle(currentResourcePath);
        } catch (Exception e) {
            LOG.debug("EXCEPTION IN getDestinationName()" + e);
        }
    }

    String tagValue;

    private void getValue(List<String> pagePath) throws JSONException {
        // TagManager tMgr = tmf.getTagManager(session);

        JSONArray jsonArrayCoordinates = new JSONArray();
        for (String item : pagePath) {
            pagePathUri = resolver.getResource(item).getParent().getParent().getParent().getPath();
            LOG.trace("ShowMApModel page Path URI: " + pagePathUri);
            LOG.trace("ITEM - -" + item);
            if (resolver != null) {
                Resource resourceItem = resolver.getResource(item);
                ValueMap valueMap = resourceItem.adaptTo(ValueMap.class);
                String latitude = valueMap.get("latitude", String.class);
                LOG.debug("latitude value - " + latitude);
                String longitude = valueMap.get("longitude", String.class);
                LOG.debug("longitude value - " + longitude);
                String openingTime = valueMap.get("openingTime", String.class);
                String closingTime = valueMap.get("closingTime", String.class);
                String placeName = valueMap.get("placeName", String.class);
                String address = valueMap.get("address", String.class);
                String locationImage = valueMap.get("locationImage", String.class);
                String phoneNo = valueMap.get("phoneNo", String.class);
                String description = valueMap.get("description", String.class);
                tagValue = getParent(item);
                String formattedTagValue = null;
                if (tagValue != null) {
                    // RangeIterator<Resource> tagsIt = tMgr.find(tagValue);
                    // LOG.trace("tag values - - " + tagsIt);
                    LOG.trace("tagValue - - -" + tagValue);
                    formattedTagValue = tagValue.substring(tagValue.lastIndexOf("/") + 1);
                    LOG.trace("formatted tag value: " + formattedTagValue);
                }
                JSONObject jsonObjLocalArea = new JSONObject();
                JSONObject jsonObjCoordinates = new JSONObject();
                jsonObjCoordinates.put("lat", latitude);
                jsonObjCoordinates.put("lng", longitude);
                jsonObjLocalArea.put("id", localAreaId++);
                jsonObjLocalArea.put("name", placeName);
                if (address == "undefined" || null == address) {
                    jsonObjLocalArea.put("address", "");
                } else {
                    jsonObjLocalArea.put("address", address);
                }
                jsonObjLocalArea.put("locationImage", locationImage);
                jsonObjLocalArea.put("openingTime", openingTime);
                jsonObjLocalArea.put("closingTime", closingTime);
                if (phoneNo == "undefined" || null == phoneNo) {
                    jsonObjLocalArea.put("phoneNo", "");
                } else {
                    jsonObjLocalArea.put("phoneNo", phoneNo);
                }
                jsonObjLocalArea.put("description", description);
                jsonObjLocalArea.put("coordinates", jsonObjCoordinates);
                jsonObjLocalArea.put("tagValue", formattedTagValue);
                jsonObjLocalArea.put("pagePathUri", pagePathUri);
                jsonArrayCoordinates.put(jsonObjLocalArea);
                jsonobj.put("localArea", jsonArrayCoordinates);
                LOG.debug("\\n\\nResource after converting from String - " + resourceItem.getPath());
            }
        }
    }

    private String getParent(String item) {
        Resource parentpath = resolver.getResource(item);
        parentpath = parentpath.getParent().getParent();
        LOG.debug("parentpath inside getParent()" + parentpath);
        ValueMap valueMap = parentpath.adaptTo(ValueMap.class);
        tagValue = valueMap.get("cq:tags", String.class);
        LOG.debug("tagValuetagValuetagValue - -" + tagValue);
        return tagValue;
    }

    private void fetchCurrentPath() {
        Resource currentPath = resourceFetcherService.getParentOfType(resource,
                "tajhotels/components/structure/local-area-list-page");
        LOG.debug("INSIDE THE ShowMapModel.fetchCurrentPath() - " + currentPath.getParent());
        currentPagePath = currentPath.getParent().getPath();

    }

    private void getPageTitle(Resource currentResourcePath) throws JSONException {
        Resource jcrContentRes = currentResourcePath.getChild(JcrConstants.JCR_CONTENT);
        ValueMap valueMap = jcrContentRes.adaptTo(ValueMap.class);
        String title = valueMap.get("jcr:title", String.class);
        float latitude = Float.parseFloat(valueMap.get("latitude", String.class));
        float longitude = Float.parseFloat(valueMap.get("longitude", String.class));
        LOG.debug("INSIDE THE ShowMapModel.getPageTitle() - TITLE OF THE PAGE - " + title);
        jsonobj.put("city", title);
        JSONObject jsonObjCoordinates = new JSONObject();
        jsonObjCoordinates.put("lat", latitude);
        jsonObjCoordinates.put("lng", longitude);
        jsonobj.put("coordinates", jsonObjCoordinates);
        jsonobj.put("cityId", title.replaceAll("\\s", "-"));
    }

    public String getFetchTitle() {
        Resource currentPath = resourceFetcherService.getParentOfType(resource,
                "tajhotels/components/structure/destination-landing-page");
        LOG.debug("INSIDE THE ShowMapModel.fetchTitle-- Path() - " + currentPath.getName());
        ValueMap valueMap = currentPath.adaptTo(ValueMap.class);
        String title = valueMap.get("jcr:title", String.class);
        LOG.debug("in fetchTitle() method- " + title);
        return title;
    }
}
