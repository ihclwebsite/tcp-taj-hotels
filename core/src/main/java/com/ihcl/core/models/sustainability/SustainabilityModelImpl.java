package com.ihcl.core.models.sustainability;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.Sustainability;
import com.ihcl.core.util.services.TajCommonsFetchResource;

@Model(
        adaptables = SlingHttpServletRequest.class,
        adapters = Sustainability.class)
public class SustainabilityModelImpl implements Sustainability {

    private static final Logger LOG = LoggerFactory.getLogger(Sustainability.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private Resource resource;

    ValueMap valueMap;

    @PostConstruct
    public void init() {
        LOG.trace(" Method Entry : init()");
        resource = request.getResource();
        LOG.debug("resource Path In sustainabilty Model : " + resource.getPath());
        if (tajCommonsFetchResource != null) {
            valueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "sustainability_detai",
                    "tajhotels/components/content/sustainability-details");
            LOG.debug("valuemap from individual sustainability :" + valueMap);
        } else {
            LOG.debug("tajCommonsFetchResource is null :");

        }
        LOG.trace(" Method Exit : init()");
    }

    @Override
    public ValueMap getValueMap() {
        LOG.trace(" Method Entry : getvalueMap()");
        return valueMap;
    }

}
