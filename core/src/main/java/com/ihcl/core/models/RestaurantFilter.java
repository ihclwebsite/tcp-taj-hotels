package com.ihcl.core.models;

import java.util.List;

public interface RestaurantFilter {

    List<String> getMealTypeList();

    List<String> getMenu();

    List<String> getCuisineList();

    List<String> getAmenitiesList();


}
