package com.ihcl.core.models;

import java.util.List;

import com.ihcl.core.models.impl.DropDownItem;

public interface MeetingFilter {

    List<DropDownItem> getMeetingSizeList();

    List<DropDownItem> getMeetingCapacityList();

    List<DropDownItem> getMeetingEventTypeList();
    
    List<DropDownItem> getWeddingEventTypeList();

}
