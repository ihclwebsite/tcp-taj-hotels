package com.ihcl.core.models.holidays.inspirational;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface IInspirationCards {

	List<InspirationCard> getListOfInspirationCards();
}
