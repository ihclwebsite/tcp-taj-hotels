package com.ihcl.core.models.config;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface PointRedemptionMessageConfigModel {

	String getMsgForRoomAndSuitesPointRedemption();
	
}
