/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ihcl.core.models.experiences;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;


@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = ExperienceListModel.class)
public class ExperienceListModel {

    private static final long serialVersionUID = 1L;

    private static final String EXPERIENCE_DETAILS_TEMPLATE_PATH ="/conf/tajhotels/settings/wcm/templates/experience-details";
    private static final String ABOUT_US_PAGE = "/conf/tajhotels/settings/wcm/templates/about-destination-template";
    private static final String DINING_PAGE = "/conf/tajhotels/settings/wcm/templates/dining-list-destination";
    private static final String EXPERIENCE_PAGE = "/conf/tajhotels/settings/wcm/templates/all-experience-destination";

    private static final Logger LOG = LoggerFactory.getLogger(ExperienceListModel.class);

    @Inject
    private Resource resource;

    @Inject
    SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    private String pagePath;

    private Session session;

    private ResourceResolver resolver;

    @PostConstruct
    public void init() {
        getExperienceList();
    }

    /**
     * @param experienceType
     */
    public List<String> getExperienceList() {
        List<String> customHotelPathsList =null;
        ValueMap vMap=null;
        List<String> pagePathList = new ArrayList<>();
        try {
            String methodName = "fetchExperienceList";
            LOG.trace("Method Entry: " + methodName);
            resolver = resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);

            fetchCurrentPath();
            Map<String, String> queryParam = new HashMap<>();
            LOG.trace("Building Query Map");
            
            vMap=resource.adaptTo(ValueMap.class);
            if(vMap!=null && vMap.containsKey("customHotelPaths")) {
                LOG.trace("ValueMap is not null and  has the customHotelPaths");
                customHotelPathsList= Arrays.asList(resource.adaptTo(ValueMap.class).get("customHotelPaths",String[].class));
                LOG.trace("The Custom Hotel Paths are not empty :"+customHotelPathsList);
            }else if(vMap!=null && !vMap.containsKey("customHotelPaths")){
                customHotelPathsList= new ArrayList<>();
                customHotelPathsList.add(pagePath);
            } else {
            	customHotelPathsList= new ArrayList<>();
            	customHotelPathsList.add(pagePath);
            }
            if(customHotelPathsList!=null && customHotelPathsList.size()>0){
                queryParam.put("group.p.or", "true");
                for(int count=0;count<customHotelPathsList.size();count++){
                    queryParam.put("group."+count+"_group.path", customHotelPathsList.get(count));
                    queryParam.put("group."+count+"_group.type", "cqPage");
                    queryParam.put("group."+count+"_group.property", "jcr:content/cq:template");
                    queryParam.put("group."+count+"_group.property.value", EXPERIENCE_DETAILS_TEMPLATE_PATH);
                    queryParam.put("group."+count+"_group.property", "jcr:content/cq:template");
                    queryParam.put("group."+count+"_group.property.value", EXPERIENCE_DETAILS_TEMPLATE_PATH);
                }
            }
            LOG.trace("The Final Hotel Paths as map : "+queryParam);
            queryParam.put("p.limit", "-1");
            LOG.trace(" queryParam : " + queryParam);


            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            LOG.trace("Query builder : " + builder);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            LOG.trace("SearchPredicates : " + searchPredicates);
            Query query = builder.createQuery(searchPredicates, session);

            LOG.trace("Query to be executed is : " + query);
            query.setStart(0L);

            SearchResult result = query.getResult();
            LOG.trace("Query Executed");
            LOG.trace("Query result is : " + result.getHits().size());

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                pagePathList.add(hit.getPath());
            }
            LOG.trace(" Page Path :" + pagePathList);
            LOG.trace(" Page Path Size:" + pagePathList.size());
            LOG.trace(" Method Exit :" + methodName);

        } catch (Exception e) {
            LOG.error("Exception while getting the Experience List :: {}", e.getMessage());
            LOG.debug("Exception while getting the Experience List :: {}", e);
        }
        return pagePathList;
    }


    /**
     *
     */
    private void fetchCurrentPath() {
        String methodName = "fetchCurrentPath";
        LOG.trace(" Method Entry :" + methodName);
        // Logic to handle Destination level listing of Experiences
        Resource currentPageResource = currentPage.getContentResource();
        ValueMap currentPageResourceValueMap = currentPageResource.adaptTo(ValueMap.class);
        String currentPageResourceTemplate = currentPageResourceValueMap.get("cq:template", String.class);
        if (currentPageResourceTemplate.equals("/conf/tajhotels/settings/wcm/templates/destination-landing-page") ) {
                pagePath = currentPage.getPath();
        } else if(currentPageResourceTemplate.equals(ABOUT_US_PAGE) || currentPageResource.equals(DINING_PAGE) || currentPageResource.equals(EXPERIENCE_PAGE)) {
        		pagePath = currentPage.getParent().getPath();
        }else {
        	//TO Segregate Signature/Dining Experiences 
        	String str = currentPage.getPath();
        	if(currentPage.getPath().contains("/dining-experiences")) {
        		pagePath = currentPage.getPath();
        	}else if(currentPage.getPath().contains("/signature-experiences")) {
        		pagePath = currentPage.getPath();
        	}else {
        		Resource currentParentPageResource = currentPage.getParent().getContentResource();
	            LOG.trace("currentParentPageResource :" + currentParentPageResource);
	            ValueMap valueMapvalueMap = currentParentPageResource.adaptTo(ValueMap.class);
	            String currentParentPageTemplate = valueMapvalueMap.get("cq:template", String.class);
	            if (currentParentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/hotel-landing-page")
	                    || currentParentPageTemplate
	                            .equals("/conf/tajhotels/settings/wcm/templates/destination-landing-page")) {
	                pagePath = currentPage.getParent().getPath();
	                LOG.trace("pagePath : " + pagePath);
	            }
        	}
        }
        LOG.trace("Current Page Path : {}", pagePath);
        LOG.trace("Method Exit: {}", methodName);

    }
}
