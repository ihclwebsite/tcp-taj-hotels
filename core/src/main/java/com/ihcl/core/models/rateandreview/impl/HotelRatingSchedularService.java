/**
 *
 */
package com.ihcl.core.models.rateandreview.impl;

import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.rateandreview.AllHotelsRatingService;

@Component(service = Runnable.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
public class HotelRatingSchedularService implements Runnable {


    private static final Logger log = LoggerFactory.getLogger(HotelRatingSchedularService.class);

    private BundleContext bundleContext;

    @Reference
    private Scheduler scheduler;

    @Reference
    AllHotelsRatingService service;

    @Override
    public void run() {
        log.info("Running...");
    }


    protected void activate(ComponentContext ctx) {
        log.info("inside activate");
        this.bundleContext = ctx.getBundleContext();
        String schedulingExpression = "0 0 12 ? * FRI *";
        final Runnable job = new Runnable() {

            @Override
            public void run() {
                log.trace("inside run method");
                service.getSealApiResponse();
            }
        };
        try {
            // Add the Job
            this.scheduler.addJob("myJob", job, null, schedulingExpression, true);
        } catch (Exception e) {
            job.run();
        }
    }

    protected void deactivate(ComponentContext ctx) {
        this.bundleContext = null;
    }
}
