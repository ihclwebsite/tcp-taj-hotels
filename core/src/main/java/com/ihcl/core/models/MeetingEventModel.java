package com.ihcl.core.models;

import static com.ihcl.tajhotels.constants.CrxConstants.MEETING_EVENT_DETAILS_COMPONENT_RESOURCETYPE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.shared.services.ResourceFetcherService;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })
public class MeetingEventModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    private Resource resource;

    @Inject
    private ResourceResolverFactory resolverFactory;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;

    private final static String CONSTANT_STRING_NA = "NA";

    private String name;

    private List<Resource> children;

    private String roomTitle;

    private String pagePath;

    private String roomCapacity;

    private String roomLongDescription;

    private String roomBuilt;

    private String roomRenovated;

    private String airportDistance;

    private String roomDimension;

    private String roomArea;

    private String roomHeight;

    private String guestEntry;

    private String seatingStyle;
    
    private String moreSeating;

    private String theatreCapacity;

    private String uCapacity;

    private String classroomCapacity;

    private String circularCapacity;

    private String boardroomCapacity;

    private String reception;

    private String roomTheme;

    private Value[] sizeValues;

    private List<String> tagValues;

    private String capacity;

    private String size;

    private String meetingImage;

    private List<JSONObject> galleryImagePaths;

    private String location;

    private String hotelName;

    private String[] venueEventTypes;

    private List<String> venueTypes;

    private String requestQuoteEmailId;

    private String urlParamHotelName;

    private ResourceResolver resourceResolver = null;


    @PostConstruct
    protected void init() {
        LOG.debug("Recevied resource form injection as: " + resource);
        LOG.debug("Recevied resource fetcher service form injection as: " + resourceFetcherService);
        LOG.debug("Attempting to read properties");
        try {
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            readProperties();
        } catch (Exception e) {
            LOG.error("Exception found while getting the resourceResolver :: {} ", e.getMessage());
            LOG.debug("Exception found while getting the resourceResolver :: {} ", e);
        }
    }

    private void readProperties() {
        LOG.trace(" Method Entry : readProperties");
        try {

            Resource child = resourceFetcherService.getChildrenOfType(resource,
                    MEETING_EVENT_DETAILS_COMPONENT_RESOURCETYPE);
            venueTypes = new ArrayList<>();
            if (null != child) {
                ValueMap valueMap = child.getValueMap();

                roomTitle = valueMap.get("roomTitle", String.class);

                pagePath = valueMap.get("requestQuotePagePath", String.class);

                roomLongDescription = valueMap.get("roomLongDescription", String.class);

                roomBuilt = valueMap.get("roomBuilt", String.class);

                roomRenovated = valueMap.get("roomRenovated", String.class);

                airportDistance = valueMap.get("airportDistance", String.class);

                roomDimension = valueMap.get("roomDimension", String.class);

                roomArea = valueMap.get("roomArea", String.class);

                roomHeight = valueMap.get("roomHeight", String.class);

                guestEntry = valueMap.get("guestEntry", String.class);

                seatingStyle = valueMap.get("seatingStyle", String.class);

                theatreCapacity = valueMap.get("theatreCapacity", String.class);
                theatreCapacity = returnNAWhenValueIsNotThere(theatreCapacity);

                uCapacity = valueMap.get("uCapacity", String.class);
                uCapacity = returnNAWhenValueIsNotThere(uCapacity);

                classroomCapacity = valueMap.get("classroomCapacity", String.class);
                classroomCapacity = returnNAWhenValueIsNotThere(classroomCapacity);

                circularCapacity = valueMap.get("circularCapacity", String.class);
                circularCapacity = returnNAWhenValueIsNotThere(circularCapacity);

                boardroomCapacity = valueMap.get("boardroomCapacity", String.class);
                boardroomCapacity = returnNAWhenValueIsNotThere(boardroomCapacity);

                reception = valueMap.get("reception", String.class);
                reception = returnNAWhenValueIsNotThere(reception);
                
                moreSeating = valueMap.get("text", String.class);
                moreSeating = returnNAWhenValueIsNotThere(moreSeating);

                capacity = findMaxCapacity(theatreCapacity, uCapacity, classroomCapacity, circularCapacity,
                        boardroomCapacity, reception, valueMap.get("roomCapacity", String.class));

                roomCapacity = capacity;

                roomTheme = valueMap.get("roomTheme", String.class);

                meetingImage = valueMap.get("meetingImage", String.class);

                Resource venue = child.getParent().getParent();
                if (venue != null) {
                    ValueMap venueValues = venue.getValueMap();
                    venueEventTypes = venueValues.get("venueEventTypes", String[].class);
                    if (venueEventTypes != null) {
                        for (String str : venueEventTypes) {
                            venueTypes.add(str);
                        }
                    }
                    LOG.trace("venueTypes of the venue " + venueTypes);
                }

                Resource hotel = venue.getParent().getParent().getParent().getChild("jcr:content");
                if (hotel != null) {
                    ValueMap HotelValues = hotel.getValueMap();
                    location = HotelValues.get("locations", String.class);
                    hotelName = HotelValues.get("hotelName", String.class);
                    location = location + "/" + hotelName + "/" + roomTitle;
                    requestQuoteEmailId = HotelValues.get("requestQuoteEmail", String.class);
                    if (null == requestQuoteEmailId || requestQuoteEmailId == "") {
                        LOG.trace("Request quote email id value is not there in properties.:");
                        requestQuoteEmailId = HotelValues.get("hotelEmail", String.class);
                    }
                    LOG.trace("RequestQuote Mail Id == > " + requestQuoteEmailId);
                    if (location != null) {
                        location = location.toLowerCase();
                        LOG.trace("hotel location" + location);
                    }
                    urlParamHotelName = hotelName.replaceAll("&", "and");
                }

                Resource galleryResource = child.getChild("gallery");
                if (galleryResource != null) {
                    galleryImagePaths = galleryImageFetcherService.getGalleryImagePathsAt(galleryResource);
                }


            }

        } catch (Exception e) {
            LOG.error("Error/Exception is thrown while fetching meeting details: " + e.toString(), e);
        }
        LOG.trace(" Method Exit : readProperties");
    }

    public String getMoreSeating() {
		return moreSeating;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	private String findMaxCapacity(String theatreCapacity2, String uCapacity2, String classroomCapacity2,
            String circularCapacity2, String boardroomCapacity2, String reception2, String roomCapacity2) {

        if (roomCapacity2 == null) {
            LOG.trace("Room capacity is null. Finding max capacity from room details");
            int theatre = (theatreCapacity2 != null) ? Integer.parseInt(theatreCapacity2) : 0;
            int u = (uCapacity2 != null) ? Integer.parseInt(uCapacity2) : 0;
            int classroom = (classroomCapacity2 != null) ? Integer.parseInt(classroomCapacity2) : 0;
            int circular = (circularCapacity2 != null) ? Integer.parseInt(circularCapacity2) : 0;
            int boardroom = (boardroomCapacity2 != null) ? Integer.parseInt(boardroomCapacity2) : 0;
            int receptionNum = (reception2 != null) ? Integer.parseInt(reception2) : 0;
            List<Integer> list = Arrays.asList(theatre, u, classroom, circular, boardroom, receptionNum);

            Optional<Integer> maxNumber = list.stream().max((i, j) -> i.compareTo(j));
            roomCapacity2 = maxNumber.get().toString();
        }
        LOG.trace("Max Room Capacity:{}", roomCapacity2);
        return roomCapacity2;
    }

    public String returnNAWhenValueIsNotThere(String value) {

        if (StringUtils.isNotBlank(value)) {
            LOG.trace("Values received in seat style are : {}", value);
            return value;
        } else {
            return CONSTANT_STRING_NA;
        }
    }


    public Resource getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }

    public List<Resource> getChildren() {
        return children;
    }


    public String getRoomTitle() {
        return roomTitle;
    }

    public String getPagePath() {
        return pagePath;
    }


    public String getRoomCapacity() {
        return roomCapacity;
    }


    public String getRoomLongDescription() {
        return roomLongDescription;
    }


    public String getRoomBuilt() {
        return roomBuilt;
    }


    public String getRoomRenovated() {
        return roomRenovated;
    }


    public String getAirportDistance() {
        return airportDistance;
    }


    public String getRoomDimension() {
        return roomDimension;
    }


    public String getRoomArea() {
        return roomArea;
    }


    public String getRoomHeight() {
        return roomHeight;
    }


    public String getGuestEntry() {
        return guestEntry;
    }


    public String getSeatingStyle() {
        return seatingStyle;
    }


    public String getTheatreCapacity() {
        return theatreCapacity;
    }


    public String getClassroomCapacity() {
        return classroomCapacity;
    }


    public String getCircularCapacity() {
        return circularCapacity;
    }


    public String getBoardroomCapacity() {
        return boardroomCapacity;
    }

    public String getReception() {
        return reception;
    }


    public String getRoomTheme() {
        return roomTheme;
    }


    public String getUCapacity() {
        return uCapacity;
    }


    public Value[] getSizeValues() {
        return sizeValues;
    }


    public String getCapacity() {
        return capacity;
    }


    public List<String> getTagValues() {
        return tagValues;
    }

    public String getSize() {
        return size;
    }


    public String getMeetingImage() {
        return meetingImage;
    }

    public String getLocation() {
        return location;
    }

    public List<String> getVenueTypes() {
        return venueTypes;
    }

    public String getHotelName() {
        return hotelName;
    }


    public String getRequestQuoteEmailId() {
        return requestQuoteEmailId;
    }


    public String getUrlParamHotelName() {
        return urlParamHotelName;
    }

}
