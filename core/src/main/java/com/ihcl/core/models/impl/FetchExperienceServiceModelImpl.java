/**
 *
 */
package com.ihcl.core.models.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ModifiableValueMapDecorator;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.FetchExperienceServiceModel;
import com.ihcl.core.util.services.TajCommonsFetchResource;


/**
 * @author Srikanta.moonraft
 *
 */
@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = FetchExperienceServiceModel.class)
public class FetchExperienceServiceModelImpl implements FetchExperienceServiceModel {

    private static final Logger LOG = LoggerFactory.getLogger(FetchExperienceServiceModelImpl.class);

    @Inject
    SlingHttpServletRequest request;

    // @Inject
    // @Via
    private Resource resource;

    @Inject
    private TajCommonsFetchResource commonsFetchResource;

    private final String PAGE_NAME_EXPERIENCE = "";

    private final String RESOURCE_NAME_EXPERIENCE = "";

    ValueMap valueMap;

    @PostConstruct
    public void init() {
        LOG.trace("Entry > [Method : init() ] :: [ Annotations : @PostConstruct");
        LOG.debug("commonsFetchResource [HashCode : " + commonsFetchResource.hashCode() + " ]");

        resource = request.getResource();
        LOG.debug("currentResource [HashCode : " + resource.hashCode() + " Path : " + resource.getPath() + "]");


        valueMap = commonsFetchResource.getChildAsValueMapUpto(resource, RESOURCE_NAME_EXPERIENCE);
        Map<String, Object> map = new HashMap<>();
        map.put("experiencePageImage",
                "/content/dam/tajhotels/in/en/our-hotels/bangalore/taj-bangalore/executive-suite/Executive Suite.jpg");
        map.put("experienceName", "Pelwan Malish");
        String experienceShortDescription = "Contact\r\n" + "Writing Prompts\r\n"
                + "Read and Submit to our List of Shareable, Short Story Ideas\r\n"
                + "All writers need some inspiration every once in a while. Prompts or story starters can be a great way to give yourself some ideas for writing if you’re having a bout of writer’s block.\r\n";
        map.put("experienceShortDescription", experienceShortDescription);
        map.put("experiencePageLink", "/content/tajhotels/en-in/our-hotels");
        valueMap = new ModifiableValueMapDecorator(map);

    }


    public ValueMap getExperience() {

        return valueMap;
    }
}
