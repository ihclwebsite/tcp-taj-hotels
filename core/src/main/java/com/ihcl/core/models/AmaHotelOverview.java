package com.ihcl.core.models;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.offers.OfferImpl;
 
@Model(adaptables=Resource.class)
public class AmaHotelOverview {
 
    @Inject
    private SlingSettingsService settings;
    
    @Inject
    private Resource hotelPath;
    
    @Inject
    private ResourceResolver resourceResolver;    
 
    @Inject @Named("sling:resourceType") @Default(values="No resourceType")
    protected String resourceType;
 
    private static final Logger LOG = LoggerFactory.getLogger(AmaHotelOverview.class);
    
    private String hotelName;

	private String address;

	private String email;
	
	private ArrayList<String> hotelPhoneNo = new ArrayList<String>();

	private String bannerImage;

	private String hotelDescription;

	private HashMap<String, String> roomAmenitiesList = new HashMap<String, String>();;

	private String experienceImagePath;

	private String experienceDescription;

	private String experienceHeading;

	private String amaHotelPoliciesDescription;

	private String amaHotelPoliciesTitle;

	private String serviceDescription;

	private String noOfRooms;

	private String noOfGuests;
        
    @PostConstruct
    public void init() throws PathNotFoundException, RepositoryException {
        getDataFromNode();
    }
    

	private void getDataFromNode() throws PathNotFoundException, RepositoryException {
    	Resource hotelContentPath = resourceResolver.getResource(hotelPath.getPath() + "/jcr:content");
    	
    	Node parent = hotelContentPath.adaptTo(Node.class);
    	LOG.trace("parent : {}", parent);
    	Node root = parent.getNode("root");
    	LOG.trace("root : {}", root);
    	Node banner_parsys = parent.getNode("banner_parsys");
    	LOG.trace("banner_parsys : {}", banner_parsys);
    	Node hotelNameNode = banner_parsys.getNode("ama_text");
    	hotelName = hotelNameNode.getProperty("title").getValue().getString();
    	Node amaInfoStrip = banner_parsys.getNode("ama_info_strip");
    	address = amaInfoStrip.getProperty("descriptionmap").getValue().getString();
    	email = amaInfoStrip.getProperty("descriptionmail").getValue().getString();
    	Node phone = amaInfoStrip.getNode("phone");
    	if(phone.hasNodes()) {
    		NodeIterator phoneNodeIterator = phone.getNodes();
    		while (phoneNodeIterator.hasNext()) {
    	    	String phoneNo = "";
    			Node phoneNumber = phoneNodeIterator.nextNode();
    			phoneNo = phoneNumber.getProperty("descriptionphone").getValue().getString();
    			hotelPhoneNo.add(phoneNo);
    			LOG.trace("hotelPhoneNo : {}", hotelPhoneNo);
    		}
    	}
    	
    	Node hotel_banner = banner_parsys.getNode("hotel_banner");
    	bannerImage = hotel_banner.getProperty("bannerImage").getValue().getString();
    	
    	Node hotelDescriptionNode = root.getNode("ama_text");
    	hotelDescription = hotelDescriptionNode.getProperty("description").getValue().getString();
    	
    	Node amaAmenitiesParentNode = root.getNode("ama_room_amenities");
    	noOfRooms = amaAmenitiesParentNode.getProperty("rooms").getValue().getString();
    	noOfGuests = amaAmenitiesParentNode.getProperty("guests").getValue().getString();
    	LOG.trace("amaAmenitiesParentNode : {}", amaAmenitiesParentNode);
    	Node amaAmenitiesNode = amaAmenitiesParentNode.getNode("amenities");
    	LOG.trace("amaAmenitiesNode : {}", amaAmenitiesNode);
    	if(amaAmenitiesNode.hasNodes()) {
    		NodeIterator roomAmenitiesNodeIterator = amaAmenitiesNode.getNodes();
    		while (roomAmenitiesNodeIterator.hasNext()) {
    	    	String icon = "";
    	    	String description = "";
    			Node roomAmenities = roomAmenitiesNodeIterator.nextNode();
    			if(roomAmenities.hasProperty("description")) {
    				icon = roomAmenities.getProperty("icon").getValue().getString();
    				description = roomAmenities.getProperty("description").getValue().getString();
    				LOG.trace("ama amenity icon : {}", icon);
    				LOG.trace("ama amenity description : {}", description);
    			}
    			roomAmenitiesList.put(icon, description);
    			LOG.trace("roomAmenitiesList : {}", roomAmenitiesList);
    		}
    	}
    	
    	Node amaExperiencesNode = root.getNode("ama_experiences");
    	Node experienceNode = amaExperiencesNode.getNode("experience");
    	if(experienceNode.hasNodes()) {
    		NodeIterator experienceNodeIterator = experienceNode.getNodes();
    		while (experienceNodeIterator.hasNext()) {
    			Node experienceItemNode = experienceNodeIterator.nextNode();
    			experienceImagePath = experienceItemNode.getProperty("imagePath").getValue().getString();
    			experienceDescription = experienceItemNode.getProperty("description").getValue().getString();
    			experienceHeading = experienceItemNode.getProperty("experienceHeading").getValue().getString();
    		}
    	}
    	
    	Node amaHotelPoliciesNode = root.getNode("ama_hotel_policies");
    	amaHotelPoliciesDescription = amaHotelPoliciesNode.getProperty("description").getValue().getString();
    	amaHotelPoliciesTitle = amaHotelPoliciesNode.getProperty("title").getValue().getString();
    	
    	if(root.hasNodes()) {
    		NodeIterator serviceNodeIterator = root.getNodes();
    		while (serviceNodeIterator.hasNext()) {
    			Node serviceNode = serviceNodeIterator.nextNode();
    			String resourceType = serviceNode.getProperty("sling:resourceType").getValue().getString();
    			if (resourceType.equals("tajhotels/components/content/ama/ama-text")) {
    				if(serviceNode.hasProperty("title")) {
	    				serviceDescription = serviceNode.getProperty("description").getValue().getString();
    				}
    			}
    		}
    	}
	}


	public Resource getHotelPath() {
		return hotelPath;
	}


	public static Logger getLog() {
		return LOG;
	}


	public String getHotelName() {
		return hotelName;
	}


	public String getAddress() {
		return address;
	}


	public String getEmail() {
		return email;
	}


	public ArrayList<String> getHotelPhoneNo() {
		return hotelPhoneNo;
	}


	public String getBannerImage() {
		return bannerImage;
	}


	public String getHotelDescription() {
		return hotelDescription;
	}


	public Map<String, String> getRoomAmenitiesList() {
		return roomAmenitiesList;
	}


	public String getExperienceImagePath() {
		return experienceImagePath;
	}


	public String getExperienceDescription() {
		return experienceDescription;
	}


	public String getExperienceHeading() {
		return experienceHeading;
	}


	public String getAmaHotelPoliciesDescription() {
		return amaHotelPoliciesDescription;
	}


	public String getAmaHotelPoliciesTitle() {
		return amaHotelPoliciesTitle;
	}


	public String getServiceDescription() {
		return serviceDescription;
	}
	
	public String getNoOfRooms() {
		return noOfRooms;
	}


	public String getNoOfGuests() {
		return noOfGuests;
	}
}