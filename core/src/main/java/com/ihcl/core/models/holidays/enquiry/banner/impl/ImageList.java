/**
 *
 */
package com.ihcl.core.models.holidays.enquiry.banner.impl;


/**
 * @author moonraft
 *
 */
public class ImageList {

    String bannerTitle;
    String bannerDescDesk;
    String bannerDescMob;
    String bannerLinkText;
    String bannerLinkUrl;
    String bannerImage;
    
	public String getBannerTitle() {
		return bannerTitle;
	}
	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}
	public String getBannerDescDesk() {
		return bannerDescDesk;
	}
	public void setBannerDescDesk(String bannerDescDesk) {
		this.bannerDescDesk = bannerDescDesk;
	}
	public String getBannerDescMob() {
		return bannerDescMob;
	}
	public void setBannerDescMob(String bannerDescMob) {
		this.bannerDescMob = bannerDescMob;
	}
	public String getBannerLinkText() {
		return bannerLinkText;
	}
	public void setBannerLinkText(String bannerLinkText) {
		this.bannerLinkText = bannerLinkText;
	}
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public String getBannerLinkUrl() {
		return bannerLinkUrl;
	}
	public void setBannerLinkUrl(String bannerLinkUrl) {
		this.bannerLinkUrl = bannerLinkUrl;
	}
    
}
