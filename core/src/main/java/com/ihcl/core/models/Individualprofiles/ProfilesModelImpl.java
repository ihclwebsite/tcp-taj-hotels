package com.ihcl.core.models.Individualprofiles;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.ProfilesModel;
import com.ihcl.core.util.services.TajCommonsFetchResource;

@Model(
        adaptables = SlingHttpServletRequest.class,
        adapters = ProfilesModel.class)
public class ProfilesModelImpl implements ProfilesModel {

    private static final Logger LOG = LoggerFactory.getLogger(ProfilesModel.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private Resource resource;

    ValueMap valueMap;

    private Map<String, Object> valueDataMap = new HashMap<>();

    @PostConstruct
    public void init() {
        LOG.trace(" Method Entry : init()");
        resource = request.getResource();

        LOG.debug("resource Path In Profile Model : " + resource.getPath());
        if (tajCommonsFetchResource != null) {

            // TODO You can directly optimize this by using adaptTo(ModifiableValueMap.class) instead of first fetching
            // value map and copying into another

            ValueMap valueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "individual_profile",
                    "tajhotels/components/content/individual-profile-details");
            LOG.debug("valuemap from individual profile2 :" + valueMap);

            valueMap.forEach((k, v) -> {
                valueDataMap.put(k, v);
                LOG.debug("kEY = " + k + "    value:=" + v);
            });

            if (resource != null) {

                if (resource.getParent() != null) {
                    if (!resource.getParent().getPath().contains("board-of-directors")) {
                        LOG.debug("ParentPath in profileModel : " + resource.getParent().getPath());
                        valueDataMap.put("corporatePath", resource.getParent().getPath());
                    }
                } else {
                    LOG.warn("getParent is null");
                }
            } else {
                LOG.warn("Resource is null profile model");
            }

        } else {
            LOG.debug("tajCommonsFetchResource is null :");

        }
        LOG.trace(" Method Exit : init()");
    }

    @Override
    public ValueMap getValueMap() {
        LOG.trace(" Method Entry : getvalueMap()");
        return valueMap;
    }


    public Map<String, Object> getValueDataMap() {
        LOG.trace(" Method Entry : getValueDataMap()");
        return valueDataMap;
    }


}
