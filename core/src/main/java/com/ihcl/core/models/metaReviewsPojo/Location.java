/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;

public class Location {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + "]";
    }
}
