package com.ihcl.core.models.holidays.packages;

import java.util.List;

public interface IIncredibleEscapesCard {

	List<IncredibleEscapeCard> getAllPackgeCards();
	boolean getRootPageFlag();
}
