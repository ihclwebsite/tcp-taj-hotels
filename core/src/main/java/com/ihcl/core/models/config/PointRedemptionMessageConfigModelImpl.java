package com.ihcl.core.models.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.PointRedemptionConfigurationService;

@Model(adaptables = org.apache.sling.api.resource.Resource.class, adapters = PointRedemptionMessageConfigModel.class)
public class PointRedemptionMessageConfigModelImpl implements PointRedemptionMessageConfigModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(PointRedemptionMessageConfigModelImpl.class);
	
	@Inject
	private PointRedemptionConfigurationService config;
	
	private String messageForPointRedemption;
	
	@PostConstruct
    public void activate() {
        LOG.trace("Method -->> Entry -->> activate() in PointRedemptionMessageConfigModelImpl");

        if (config == null) {
            LOG.debug(" config at Model is  Null");
        }
        getRoomAndSuitesPointRedemptionMsg();
        LOG.trace("Method -->> Exit -->> activate() from PointRedemptionMessageConfigModelImpl");
    }

	private void getRoomAndSuitesPointRedemptionMsg() {
        LOG.trace("inside method:: getRoomAndSuitesPointRedemptionMsg()");
        messageForPointRedemption = config.getmessageForRoomAndSuitesPointRedemption();
        LOG.debug("actualURL value is " + messageForPointRedemption);
    }
	
	@Override
	public String getMsgForRoomAndSuitesPointRedemption() {
		return messageForPointRedemption;
	}

}
