/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclOtherServicesBean {


    private String imageCard;

    private String title;

    private String description;

    private String buttontext;

    private String CardId;

    private String otherServicesLongDesc;

    private String longdescription;

    private String buttonurl;


    public String getButtonurl() {
        return buttonurl;
    }


    public void setButtonurl(String buttonurl) {
        this.buttonurl = buttonurl;
    }


    public String getCardId() {
        return CardId;
    }


    public void setCardId(String cardId) {
        CardId = cardId;
    }


    /**
     * Getter for the field imageCard
     *
     * @return the imageCard
     */
    public String getImageCard() {
        return imageCard;
    }


    /**
     * Setter for the field imageCard
     *
     * @param imageCard
     *            the imageCard to set
     */

    public void setImageCard(String imageCard) {
        this.imageCard = imageCard;
    }


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Setter for the field titleVariation
     *
     * @param titleVariation
     *            the titleVariation to set
     */

    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the field buttontext
     *
     * @return the buttontext
     */
    public String getButtontext() {
        return buttontext;
    }


    /**
     * Setter for the field buttontext
     *
     * @param buttontext
     *            the buttontext to set
     */

    public void setButtontext(String buttontext) {
        this.buttontext = buttontext;
    }


    public String getOtherServicesLongDesc() {
        return otherServicesLongDesc;
    }


    public void setOtherServicesLongDesc(String otherServicesLongDesc) {
        this.otherServicesLongDesc = otherServicesLongDesc;
    }


    public String getLongdescription() {
        return longdescription;
    }


    public void setLongdescription(String longdescription) {
        this.longdescription = longdescription;
    }


}
