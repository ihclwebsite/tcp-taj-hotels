/**
 *
 */
package com.ihcl.core.models.campaigns;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(adaptables = Resource.class,
        adapters = CampaignSummerTabModel.class)

public class CampaignSummerTabModel {

    private static final Logger LOG = LoggerFactory.getLogger(CampaignSummerTabModel.class);

    @Self
    private Resource resource;

    public List<CampaignSummerTabBean> list = new ArrayList<>();


    /**
     * Getter for the field list
     *
     * @return the list
     */
    public List<CampaignSummerTabBean> getList() {
        return list;
    }


    /**
     * Setter for the field list
     *
     * @param list
     *            the list to set
     */

    public void setList(List<CampaignSummerTabBean> list) {
        this.list = list;
    }


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getfiguresValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */

    public List<CampaignSummerTabBean> getfiguresValues() {
        LOG.trace("resource is:: {}", resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);

        String[] listBlocks = valueMap.get("list", String[].class);
        LOG.trace("listBlocks :: {}", listBlocks);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != listBlocks) {
            for (int i = 0; i < listBlocks.length; i++) {
                JsonElement jsonElement = gson.fromJson(listBlocks[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                CampaignSummerTabBean blockBean = new CampaignSummerTabBean();
                LOG.trace("jsonObject : {}", jsonObject);


                if (null != jsonObject.get("tabName")) {
                    blockBean.setTabName(jsonObject.get("tabName").getAsString());
                }

                if (null != jsonObject.get("tabtextlink")) {
                    blockBean.setTabtextlink(jsonObject.get("tabtextlink").getAsString());
                }

                LOG.trace("list :: {}", list);
                list.add(blockBean);

            }

        }

        return list;
    }
}
