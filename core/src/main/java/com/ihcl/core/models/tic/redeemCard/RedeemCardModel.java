/**
 *
 */
package com.ihcl.core.models.tic.redeemCard;

import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;
import org.apache.sling.api.resource.*;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Model(adaptables = {Resource.class}, adapters = IRedeemCardModel.class)
public class RedeemCardModel implements IRedeemCardModel {

    private final Logger LOG = LoggerFactory.getLogger(RedeemCardModel.class);

    @Self
    private Resource resource;

    Map<String, String> currencies;

    boolean isCurrencySet;

    @Inject
    private ResourceResolverFactory resolverFactory;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    @PostConstruct
    public void init() {
        String methodName = "init";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Received resource as: " + resource);
            ResourceResolver serviceResourceResolver = resolverFactory.getServiceResourceResolver(null);
            ValueMap valueMap = resource.adaptTo(ValueMap.class);
            isCurrencySet = Boolean.parseBoolean(valueMap.get("needCurrencySetting", String.class));
            getAllCurrencies();
        } catch (LoginException e) {
            LOG.error("An error occurred while fetching gift card details.", e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    @Override
    public ValueMap getProperties() {
        String methodName = "getProperties";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = null;
        try {
            ResourceResolver serviceResourceResolver = resolverFactory.getServiceResourceResolver(null);
            Resource giftCardDetails = resourceFetcherService.getChildrenOfType(resource,
                    CrxConstants.REDEEM_CARD_DETAILS_RESOURCE_TYPE);
            valueMap = giftCardDetails.adaptTo(ValueMap.class);
        } catch (LoginException e) {
            LOG.error("An error occurred while fetching gift card details.", e);
        }
        LOG.trace("Method Exit: " + methodName);
        return valueMap;
    }

    public void getAllCurrencies() {
        LOG.trace("Method Entry: getAllCurrencies");
        currencies = new HashMap<>();
        if (resource != null && isCurrencySet) {
            Resource currencyResource = resource.getChild("currencyOptions");
            if (currencyResource != null) {
                Iterable<Resource> options = currencyResource.getChildren();
                for (Resource childResrouce : options) {
                    ValueMap valueMap = childResrouce.adaptTo(ValueMap.class);
                    currencies.put(valueMap.get("shortSymbol", String.class), valueMap.get("name", String.class));
                }
            }
        } else {
            currencies = null;
        }
        LOG.trace("Method Exit: getAllCurrencies");
    }

    @Override
    public Map<String, String> getCurrencies() {
        return currencies;
    }

}
