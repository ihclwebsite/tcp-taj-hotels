package com.ihcl.core.models;

import static com.ihcl.tajhotels.constants.CrxConstants.KEY_HOTEL_BANNER_BANNER_IMAGE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.tajhotels.constants.BookingConstants;
import com.ihcl.tajhotels.constants.CrxConstants;
import com.ihcl.tajhotels.constants.ReservationConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = HotelDetailsFetcher.class)
public class HotelDetailsFetcher {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    SlingHttpServletRequest request;

    String hotelValue = null;

    @Inject
    private Resource resource;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private String hotelIDValue;

    @Inject
    private String roomValue;

    @Reference
    private QueryBuilder builder;

    private Session session;

    private ResourceResolver resolver;

    private String Longitude;

    private String Latitude;

    private String hotelPhoneNumber;

    private String hotelName;

    private String hotelEmail;

    private String roomDescription;

    private String hotelPath;

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    public void setHotelPhoneNumber(String hotelPhoneNumber) {
        this.hotelPhoneNumber = hotelPhoneNumber;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelEmail() {
        return hotelEmail;
    }

    public void setHotelEmail(String hotelEmail) {
        this.hotelEmail = hotelEmail;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public String getHotelPath() {
        return hotelPath;
    }

    public void setHotelPath(String hotelPath) {
        this.hotelPath = hotelPath;
    }

    @PostConstruct
    public void activate() throws RepositoryException {
        LOG.trace("The init method of HotelDetailsFetcher");
        LOG.trace("The value of hotelID :" + hotelIDValue);
        LOG.trace("The value of room :" + roomValue);
        getHotelDetails(hotelIDValue, roomValue);
    }

    public Resource getHotelDetails(String hotelID, String roomType) throws RepositoryException {
        String methodName = "getHotelDetails";
        LOG.trace(" Method Entry :" + methodName);
        LOG.trace("Current page " + resource.getPath());
        List<String> pagePath = new ArrayList<String>();
        Map<String, String> queryParam = new HashMap<String, String>();
        queryParam.put("type", "cq:Page");
        queryParam.put("path", "/content/tajhotels");
        queryParam.put("property", "jcr:content/cq:template");
        queryParam.put("property.value", "/conf/tajhotels/settings/wcm/templates/hotel-landing-page");
        queryParam.put("2_property", "jcr:content/hotelId");
        queryParam.put("2_property.value", hotelID);
        queryParam.put("p.limit", "-1");
        LOG.trace(" queryParam : " + queryParam);

        resolver = resource.getResourceResolver();
        session = resolver.adaptTo(Session.class);

        QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
        LOG.trace("Query builder : " + builder);
        PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
        LOG.trace("SearchPredicates : " + searchPredicates);
        Query query = builder.createQuery(searchPredicates, session);

        LOG.trace("Query to be executed is : " + query);
        query.setStart(0L);

        SearchResult result = query.getResult();
        LOG.trace("Query Executed");
        LOG.trace("Query result is : " + result.getHits().size());

        List<Hit> hits = result.getHits();
        Iterator<Hit> hitList = hits.iterator();
        Hit hit;
        while (hitList.hasNext()) {
            hit = hitList.next();
            pagePath.add(hit.getPath());
        }
        hotelPath = pagePath.get(0);
        LOG.trace(" The hotelPath Value :" + pagePath.get(0));
        Resource pagePathResource = resolver.getResource(pagePath.get(0));
        LOG.trace(" Method Exit :" + methodName);
        LOG.trace(" Method Exit :" + pagePathResource.getPath());

        Resource child = pagePathResource.getChild("jcr:content");
        LOG.trace(" The Child Path :" + child.getPath());
        ValueMap valueMap = child.adaptTo(ValueMap.class);
        String hotelname = valueMap.get("hotelName", String.class);
        LOG.trace(" The Hotel Name :" + hotelname);
        setHotelName(valueMap.get("hotelName", String.class));
        setHotelEmail(valueMap.get("hotelEmail", String.class));
        setHotelPhoneNumber(valueMap.get("hotelPhoneNumber", String.class));
        setLatitude(valueMap.get("latitude", String.class));
        setLongitude(valueMap.get("longitude", String.class));
        LOG.trace("HotelName: " + getHotelName());
        LOG.trace("Hotel Email: " + getHotelEmail());
        LOG.trace("Hotel Phone Number: " + getHotelPhoneNumber());
        LOG.trace("Hotel Latitude: " + getLatitude());
        LOG.trace("Hotel Longitude: " + getLongitude());
        Resource resultresource = search(roomType, pagePathResource);
        ValueMap values = resultresource.adaptTo(ValueMap.class);
        setRoomDescription(values.get("roomDescription", String.class));
        LOG.trace("THe rooom decription is: " + getRoomDescription());

        return pagePathResource;

    }

    public Resource search(String property, Resource resource) {
        LOG.trace("The room type response: " + property);
        LOG.trace("The resource path: " + resource.getPath());
        String methodName = "SearchQuery";
        String room_details = "room_details";
        List<String> resultPagePath = new ArrayList<String>();
        Map<String, String> queryParam = new HashMap<String, String>();
        queryParam.put("path", resource.getPath());
        queryParam.put("type", "nt:unstructured");
        queryParam.put("1_property", "roomTypeCode");
        queryParam.put("1_property.value", property);
        LOG.trace(" queryParam : " + queryParam);

        resolver = resource.getResourceResolver();
        session = resolver.adaptTo(Session.class);

        QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
        PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
        Query query = builder.createQuery(searchPredicates, session);

        LOG.trace("Query to be executed is : " + query);
        query.setStart(0L);

        SearchResult result = query.getResult();
        LOG.trace("Query Executed");
        LOG.trace("Query result is : " + result.getHits().size());

        List<Hit> hits = result.getHits();
        Iterator<Hit> hitList = hits.iterator();
        Hit hit;
        while (hitList.hasNext()) {
            hit = hitList.next();
            try {
                resultPagePath.add(hit.getPath());
            } catch (RepositoryException e) {
                LOG.error("Exception occured in HotelsDetailsFetcher", e.getMessage());
            }
        }
        LOG.trace(" Page Path :" + resultPagePath.get(0));
        Resource pagePathResource = resolver.getResource(resultPagePath.get(0));
        LOG.trace("The Roomtype Query Response: " + pagePathResource);
        return pagePathResource;

    }

    public Hotel getHotelDetails(SlingHttpServletRequest request, String hotelId, String rootPath,String bedType)
            throws RepositoryException {
        Hotel hotelDetailsBean = new Hotel();
        resolver = request.getResourceResolver();
        session = resolver.adaptTo(Session.class);
        String methodName = "getHotel with request";
        LOG.trace(" Method Entry :" + methodName);
        String hotelIdInReq = request.getParameter(BookingConstants.FORM_HOTEL_ID);
        LOG.trace("hotelIdInReq:-> " + hotelIdInReq);
        if (hotelIdInReq == null || hotelIdInReq.isEmpty() || hotelIdInReq.equalsIgnoreCase("undefined")) {
            LOG.trace("hotelId:-> " + hotelId);
            hotelIdInReq = hotelId;
        }
        List<String> pagePath = new ArrayList<>();
        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("type", "cq:Page");
        queryParam.put("path", rootPath);
        queryParam.put("property", "jcr:content/cq:template");
        queryParam.put("property.value", "/conf/tajhotels/settings/wcm/templates/hotel-landing-page");
        queryParam.put("2_property", "jcr:content/hotelId");
        queryParam.put("2_property.value", hotelIdInReq);
        
        queryParam.put("p.limit", "-1");
        LOG.trace(" queryParam : " + queryParam);

        QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
        PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
        Query query = builder.createQuery(searchPredicates, session);
        query.setStart(0L);
        SearchResult result = query.getResult();
        LOG.trace("Query result is : " + result.getHits().size());

        List<Hit> hits = result.getHits();
        if (null != hits) {
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                if(StringUtils.isNotBlank(bedType)) {
                	LOG.debug("bedType: {}",bedType);
                	resolver = request.getResourceResolver();
                    session = resolver.adaptTo(Session.class);
                    Map<String, String> queryParam1 = new HashMap<>();
                    queryParam1.put("path", hit.getPath());
                    queryParam1.put("type", ReservationConstants.NT_UNSTRUCTURED);
                    queryParam1.put("1_property", ReservationConstants.ROOM_CODE);
                    queryParam1.put("1_property.value", bedType);
                    queryParam1.put("p.limit", "-1");
                    QueryBuilder builder1 = resolver.adaptTo(QueryBuilder.class);
                    PredicateGroup searchPredicates1 = PredicateGroup.create(queryParam1);
                    Query query1 = builder1.createQuery(searchPredicates1, session);
                    query1.setStart(0L);
                    SearchResult result1 = query1.getResult();
                    List<Hit> hits1 = result1.getHits();
                    if (null != hits1) {
                    	Iterator<Hit> hitList1 = hits1.iterator();
                    	Hit hit1;
                    	while (hitList1.hasNext()) {
                            hit1 = hitList1.next();
                            pagePath.add(hit.getPath());
                            LOG.debug("the  hotel path: {}",hit.getPath());
                    	}
                    }

                }else {
                    pagePath.add(hit.getPath());
                }
            }
            if (!pagePath.isEmpty()) {
                hotelPath = pagePath.get(0);
                LOG.trace(" The hotelPath Value : {}", pagePath.get(0));
                Resource pagePathResource = resolver.getResource(pagePath.get(0));
                Resource child = pagePathResource.getChild("jcr:content");
                LOG.trace(" The Child Path :" + child.getPath());
                ValueMap valueMap = child.adaptTo(ValueMap.class);
                String hotelname = valueMap.get("hotelName", String.class);
                LOG.trace(" The Hotel Name :" + hotelname);
                hotelDetailsBean.setHotelName(valueMap.get("hotelName", String.class));
                LOG.trace(" The Hotel Email :" + valueMap.get("hotelEmail", String.class));
                hotelDetailsBean.setHotelEmail(valueMap.get("hotelEmail", String.class));
                String stdCode = valueMap.get("hotelStdCode", String.class);
                if (stdCode != null && stdCode != "") {
                    LOG.debug("Hotel STD Code :" + stdCode);
                    String phoneNumber = stdCode + "-" + valueMap.get("hotelPhoneNumber", String.class);
                    hotelDetailsBean.setHotelPhoneNumber(phoneNumber);
                } else {
                    hotelDetailsBean.setHotelPhoneNumber(valueMap.get("hotelPhoneNumber", String.class));
                }
                LOG.trace(" The Hotel phone :" + hotelDetailsBean.getHotelPhoneNumber());
                String giftHamperAdminMailId = valueMap.get("giftHamperAdminMailId", String.class);
                LOG.trace(" The GiftCard Hamper SPOC Mail Id  :" + giftHamperAdminMailId);
                if (null != giftHamperAdminMailId && giftHamperAdminMailId != "") {
                    hotelDetailsBean.setGiftHamperAdminMailId(giftHamperAdminMailId);
                } else {
                    hotelDetailsBean.setGiftHamperAdminMailId(valueMap.get("hotelEmail", String.class));
                    LOG.trace(" The GiftCard Hamper SPOC Mail Id is null or empty so setting  hotelMailId :"
                            + valueMap.get("hotelEmail", String.class));
                }
                if (null != valueMap.get("pmspropertyCode")) {
                    hotelDetailsBean.setPmspropertyCode(valueMap.get("pmspropertyCode", String.class));
                }
                if (null != valueMap.get("propertyCode")) {
                    hotelDetailsBean.setPropertyCode(valueMap.get("propertyCode", String.class));
                }
                hotelDetailsBean.setHotelLatitude(valueMap.get("latitude", String.class));
                hotelDetailsBean.setHotelLongitude(valueMap.get("longitude", String.class));
                hotelDetailsBean.setHotelResourcePath(pagePathResource.getPath());
                updateHotelBanner(hotelDetailsBean, child);
            }
        }
        LOG.trace(" Method Exit :" + methodName);
        return hotelDetailsBean;

    }

    private void updateHotelBanner(Hotel hotel, Resource jcrContentResource) {
        String methodName = "updateHotelBanner";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Received jcr content resource as: " + jcrContentResource);
            if (jcrContentResource != null) {
                Iterable<Resource> children = jcrContentResource.getChildren();
                Resource bannerComponentResource = null;
                for (Resource child : children) {
                    String resourceType = child.getResourceType();
                    LOG.trace("Resource type of current child is: " + resourceType);
                    bannerComponentResource = getBannerComponentResourceFrom(child);
                    if (bannerComponentResource != null) {
                        LOG.debug("Found banner resource as: " + bannerComponentResource);
                        ValueMap bannerValueMap = bannerComponentResource.adaptTo(ValueMap.class);
                        String bannerImagePath = bannerValueMap.get(KEY_HOTEL_BANNER_BANNER_IMAGE, String.class);
                        hotel.setHotelBannerImagePath(bannerImagePath);
                        LOG.debug("Set banner image path as: " + bannerImagePath);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            LOG.warn("An error occurred while updating hotel banner.", e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private Resource getBannerComponentResourceFrom(Resource resource) {
        String methodName = "getBannerComponentResourceFrom";
        LOG.trace("Method Entry: " + methodName);
        Resource bannerResource = null;
        String resourceType = resource.getResourceType();
        if (resourceType.equals(CrxConstants.HOTEL_BANNER_COMPONENT_RESOURCETYPE)) {
            bannerResource = resource;
        } else {
            Iterable<Resource> children = resource.getChildren();
            for (Resource child : children) {
                bannerResource = getBannerComponentResourceFrom(child);
                if (bannerResource != null) {
                    break;
                }
            }
        }
        LOG.trace("Method Exit: " + methodName);
        return bannerResource;

    }

    public Iterator<Node> searchRoomType(Map<String, String> queryParam, Session session,
            ResourceResolver resResolver) {

        Iterator<Node> itrSearchNode = null;
        try {

            QueryBuilder builder = resResolver.adaptTo(QueryBuilder.class);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            Query query = builder.createQuery(searchPredicates, session);

            query.setHitsPerPage(9999);
            SearchResult searchResult = query.getResult();
            itrSearchNode = searchResult.getNodes();
            return itrSearchNode;

        } catch (Exception e) {
            LOG.error("Exception found while trying to get the bedType for roomCode:: {}", e.getMessage());
            LOG.debug("Exception found while trying to get the bedType for roomCode:: {}", e);
        }
        return null;
    }

}
