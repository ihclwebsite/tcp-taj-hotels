/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclCarouselBean {


    private String logoImage;

    private String description;

    private String title;

    private String classList;


    /**
     * Getter for the field logoImage
     *
     * @return the logoImage
     */
    public String getLogoImage() {
        return logoImage;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Setter for the field logoImage
     *
     * @param logoImage
     *            the logoImage to set
     */

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the field classList
     *
     * @return the classList
     */
    public String getClassList() {
        return classList;
    }


    /**
     * Setter for the field classList
     *
     * @param classList
     *            the classList to set
     */

    public void setClassList(String classList) {
        this.classList = classList;
    }


}
