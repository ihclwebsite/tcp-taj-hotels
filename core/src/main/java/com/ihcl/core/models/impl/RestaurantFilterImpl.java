package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.ihcl.core.models.RestaurantFilter;

@Model(adaptables = Resource.class, adapters = RestaurantFilter.class)
public class RestaurantFilterImpl implements RestaurantFilter {

	private List<String> mealTypeList;

	private List<String> menu;

	private List<String> cuisineList;

	private List<String> amenitiesList;

	private static final Logger LOG = LoggerFactory.getLogger(RestaurantFilterImpl.class);

	@Self
	Resource resource;

	ResourceResolver resourceResolver;

	@Inject
	ResourceResolverFactory resourceResolverFactory;

	@PostConstruct
	public void init() {
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			mealTypeList = getTagList("/etc/tags/taj/restaurants/meal-type");
			menu = getTagList("/etc/tags/taj/restaurants/menu");
			cuisineList = getTagList("/etc/tags/taj/restaurants/cuisine");
			amenitiesList = getTagList("/etc/tags/taj/restaurants/amenities");
		} catch (LoginException e) {
			LOG.error("Failed to retreive resource resolver using service resolver factory");
		}
	}

	public List<String> getTagList(String path) {
		List<String> tagList = new ArrayList<String>();
		if (resourceResolver != null) {
			Resource tagRoot = resourceResolver.getResource(path);
			if (tagRoot != null) {
				Iterable<Resource> childTags = tagRoot.getChildren();
				for (Resource resource : childTags) {
					Tag tag = resource.adaptTo(Tag.class);
					tagList.add(tag.getTitle());
				}
				Collections.sort(tagList);
			}
		}
		return tagList;
	}

	@Override
	public List<String> getMealTypeList() {
		return mealTypeList;
	}

	@Override
	public List<String> getMenu() {
		return menu;
	}

	@Override
	public List<String> getCuisineList() {
		return cuisineList;
	}

	@Override
	public List<String> getAmenitiesList() {
		return amenitiesList;
	}

}
