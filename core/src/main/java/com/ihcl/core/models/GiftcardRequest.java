/**
 *
 */
package com.ihcl.core.models;


/**
 * @author admin
 *
 */
public class GiftcardRequest {

    String title;

    String firstName;

    String lastName;

    String email;

    String phoneNumber;

    String currenyType;

    String amount;

    String payableAmount;

    String itineraryNumber;


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter for the field firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for the field firstName
     *
     * @param firstName
     *            the firstName to set
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for the field lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for the field lastName
     *
     * @param lastName
     *            the lastName to set
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for the field email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter for the field email
     *
     * @param email
     *            the email to set
     */

    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Getter for the field phoneNumber
     *
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Setter for the field phoneNumber
     *
     * @param phoneNumber
     *            the phoneNumber to set
     */

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Getter for the field currenyType
     *
     * @return the currenyType
     */
    public String getCurrenyType() {
        return currenyType;
    }


    /**
     * Setter for the field currenyType
     *
     * @param currenyType
     *            the currenyType to set
     */

    public void setCurrenyType(String currenyType) {
        this.currenyType = currenyType;
    }


    /**
     * Getter for the field amount
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Setter for the field amount
     *
     * @param amount
     *            the amount to set
     */

    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Getter for the field payableAmount
     *
     * @return the payableAmount
     */
    public String getPayableAmount() {
        return payableAmount;
    }

    /**
     * Setter for the field payableAmount
     *
     * @param payableAmount
     *            the payableAmount to set
     */

    public void setPayableAmount(String payableAmount) {
        this.payableAmount = payableAmount;
    }


    /**
     * Getter for the field itineraryNumber
     *
     * @return the itineraryNumber
     */
    public String getItineraryNumber() {
        return itineraryNumber;
    }


    /**
     * Setter for the field itineraryNumber
     *
     * @param itineraryNumber
     *            the itineraryNumber to set
     */

    public void setItineraryNumber(String itineraryNumber) {
        this.itineraryNumber = itineraryNumber;
    }

}
