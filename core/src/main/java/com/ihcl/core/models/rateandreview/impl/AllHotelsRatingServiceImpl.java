/**
 *
 */
package com.ihcl.core.models.rateandreview.impl;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.ihcl.core.models.SealApiResponse;
import com.ihcl.core.models.rateandreview.AllHotelsRatingService;
import com.ihcl.core.models.rateandreview.SealResponse;
import com.ihcl.core.services.search.SearchProvider;

@Component(service = AllHotelsRatingService.class,
        immediate = true)
public class AllHotelsRatingServiceImpl implements AllHotelsRatingService {


    private static final Logger log = LoggerFactory.getLogger(AllHotelsRatingServiceImpl.class);

    private static final String PATH = "/content/tajhotels/en-in/our-hotels";

    private String headURL = "http://api.trustyou.com/bulk";

    private final String apiKey = "dad97582-016d-432d-ab9b-11a766bfca9a";

    private SealResponse sealResponse;

    private SealResponse sealResponseOfSecondBatch;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    QueryBuilder builder;

    ResourceResolver resourceResolver;

    @Reference
    HotelRatingsReplicator replicator;

    @Reference
    SearchProvider searchProvider;


    private void processRateAndReview() {
        String methodName = "processRateAndReview";
        log.info("Method Entry : " + methodName);

        ObjectMapper mapper = new ObjectMapper();
        try {
            log.trace("Entered processRateAndReview :--> ");

            Boolean areRatingsUpdated = false;
            String response;
            Stack<String> ids = gettyIds();

            List<String> requestList = constructRequestList(ids);
            for (String request : requestList) {
                log.trace("reuest list =" + request);
                response = readJsonFromUrl(formJSONUrl(headURL), request);
                if (response != null) {
                    sealResponse = mapper.readValue(response, SealResponse.class);
                    areRatingsUpdated = true;
                } else {
                    log.warn("sealApiResponse parsed data is null");
                }
                updateHotelPages(sealResponse);
            }
            if (areRatingsUpdated) {
                replicator.activatePage(resourceResolver.adaptTo(Session.class));
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Method Exit : " + methodName);
    }

    @Override
    public SealResponse getSealApiResponse() {

        processRateAndReview();
        return sealResponse;
    }

    private String formJSONUrl(String mainUrl) {
        String methodName = "formJSONUrl";
        log.info("Method Entry : " + methodName);

        StringBuilder finalURL = new StringBuilder();
        String delimiter = "?";
        String key = "key=";

        finalURL.append(mainUrl).append(delimiter).append(key).append(apiKey);

        log.trace("URL formed :: " + finalURL.toString());
        log.info("Method Exit : " + methodName);
        return finalURL.toString();
    }


    private String readJsonFromUrl(String url, String input) {
        String jsonText = null;
        StringBuilder stringBuilder = new StringBuilder();
        log.info("Entered readJsonFromUrl method");
        try {
            URL myURL = new URL(url);
            HttpURLConnection httpConnection = (HttpURLConnection) myURL.openConnection();
            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConnection.setUseCaches(false);
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            OutputStream outputStream = httpConnection.getOutputStream();
            outputStream.write(input.getBytes());
            outputStream.flush();
            if (httpConnection.getResponseCode() != 200) {
                log.error("Failed : HTTP error code : " + httpConnection.getResponseMessage());
                throw new RuntimeException("Failed : HTTP error code : " + httpConnection.getResponseCode());

            }
            BufferedReader responseBuffer = new BufferedReader(
                    new InputStreamReader((httpConnection.getInputStream())));
            log.trace("Output from Server:\n");
            while ((jsonText = responseBuffer.readLine()) != null) {
                stringBuilder.append(jsonText);
            }
            httpConnection.disconnect();
            log.trace("JSON Content:\n" + stringBuilder);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("Exiting readJsonFromUrl method");
        return stringBuilder.toString();
    }

    private Stack<String> gettyIds() {
        log.info("Entered  gettyIds method");
        String id = null;
        Stack<String> ids = new Stack<String>();
        HashMap<String, String> queryMap = new HashMap<>();
        try {

            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            queryMap.put("path", PATH);
            queryMap.put("1_property", "sling:resourceType");
            queryMap.put("1_property.value", "tajhotels/components/structure/hotels-landing-page");
            queryMap.put("2_property", "cq:lastReplicationAction");
            queryMap.put("2_property.value", "Activate");
            queryMap.put("p.hits", "all");
            queryMap.put("p.limit", "-1");
            Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
            for (Hit hit : query.getResult().getHits()) {
                ValueMap pageProperties = hit.getProperties();
                id = String.valueOf(pageProperties.get("trustyouid"));
                if (id != null && !"null".equalsIgnoreCase(id) && !id.isEmpty()) {
                    ids.push(id);
                    log.trace("ty_id " + id);
                }
            }
            log.info("Exiting  gettyIds method");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ids;
    }

    private void updateHotelPages(SealResponse response) {
        log.info("Entered  updateHotelPages method");
        if (response != null) {
            List<SealApiResponse> list = response.getResponse().getResponse_list();
            Map<String, String> tyIdMapping = new HashMap<String, String>();
            for (SealApiResponse resp : list) {
                tyIdMapping.put(resp.getResponse().getTy_id(),
                        resp.getResponse().getScore() * 20 + ":" + resp.getResponse().getReviews_count());
                log.trace("name " + resp.getResponse().getTy_id());
            }
            String id = null;
            Map<String, String> queryMap = new TreeMap<>();
            try {
                resourceResolver = resolverFactory.getServiceResourceResolver(null);
                queryMap.put("path", PATH);
                queryMap.put("1_property", "sling:resourceType");
                queryMap.put("1_property.value", "tajhotels/components/structure/hotels-landing-page");
                queryMap.put("2_property", "cq:lastReplicationAction");
                queryMap.put("2_property.value", "Activate");
                queryMap.put("p.hits", "all");
                queryMap.put("p.limit", "-1");
                Query query = builder.createQuery(PredicateGroup.create(queryMap),
                        resourceResolver.adaptTo(Session.class));
                for (Hit hit : query.getResult().getHits()) {
                    ValueMap pageProperties = hit.getProperties();
                    id = String.valueOf(pageProperties.get("trustyouid"));
                    if (id != null && !"null".equalsIgnoreCase(id) && !id.isEmpty() && tyIdMapping.containsKey(id)) {
                        String[] review = tyIdMapping.get(id).split(":");
                        String score = review[0];
                        String reviews_count = review[1];
                        log.info(
                                "Name of hotel " + pageProperties.get("pageTitle") + " score " + score);
                        hit.getNode().setProperty("reviewScore", score).getSession().save();
                        hit.getNode().setProperty("totalReview", reviews_count).getSession().save();
                    }
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        log.info("Exiting  updateHotelPages method");
    }


    private List<String> constructRequestList(Stack<String> ids) {
        List<String> requestList = new ArrayList<String>();
        int length = ids.size();
        if (length > 100)
            length = 100;
        String request_list = "request_list=[";
        for (int i = 0; i < length; i++) {
            request_list = request_list + "\"/hotels/" + ids.pop() + "/seal.json?scale=5\"" + ",";
        }
        request_list = request_list.substring(0, request_list.lastIndexOf(","));
        request_list = request_list + "]";
        requestList.add(request_list);
        log.info("firstBatchResponse =" + request_list);

        if (!ids.isEmpty()) {
            request_list = "request_list=[";
            while (!ids.isEmpty()) {
                request_list = request_list + "\"/hotels/" + ids.pop() + "/seal.json?scale=5\"" + ",";
            }
            request_list = request_list.substring(0, request_list.lastIndexOf(","));
            request_list = request_list + "]";
            requestList.add(request_list);
            log.info("secondBatchResponse " + request_list);
        }
        return requestList;
    }
}
