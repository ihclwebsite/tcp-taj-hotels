package com.ihcl.core.models.metaReviewsPojo;

public class Summary {

    private String text;

    private String global_popularity;

    private Highlight_list[] highlight_list;

    private String location_nearby;

    private Location location;

    private String score;

    private Summary_sentence_list[] summary_sentence_list;

    private String popular_with;

    private Reviews_distribution[] reviews_distribution;

    private String score_description;

    private String popularity;

    private Hotel_type hotel_type;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getGlobal_popularity() {
        return global_popularity;
    }

    public void setGlobal_popularity(String global_popularity) {
        this.global_popularity = global_popularity;
    }

    public Highlight_list[] getHighlight_list() {
        return highlight_list;
    }

    public void setHighlight_list(Highlight_list[] highlight_list) {
        this.highlight_list = highlight_list;
    }

    public String getLocation_nearby() {
        return location_nearby;
    }

    public void setLocation_nearby(String location_nearby) {
        this.location_nearby = location_nearby;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Summary_sentence_list[] getSummary_sentence_list() {
        return summary_sentence_list;
    }

    public void setSummary_sentence_list(Summary_sentence_list[] summary_sentence_list) {
        this.summary_sentence_list = summary_sentence_list;
    }

    public String getPopular_with() {
        return popular_with;
    }

    public void setPopular_with(String popular_with) {
        this.popular_with = popular_with;
    }

    public Reviews_distribution[] getReviews_distribution() {
        return reviews_distribution;
    }

    public void setReviews_distribution(Reviews_distribution[] reviews_distribution) {
        this.reviews_distribution = reviews_distribution;
    }

    public String getScore_description() {
        return score_description;
    }

    public void setScore_description(String score_description) {
        this.score_description = score_description;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public Hotel_type getHotel_type() {
        return hotel_type;
    }

    public void setHotel_type(Hotel_type hotel_type) {
        this.hotel_type = hotel_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", global_popularity = " + global_popularity + ", highlight_list = "
                + highlight_list + ", location_nearby = " + location_nearby + ", location = " + location + ", score = "
                + score + ", summary_sentence_list = " + summary_sentence_list + ", popular_with = " + popular_with
                + ", reviews_distribution = " + reviews_distribution + ", score_description = " + score_description
                + ", popularity = " + popularity + ", hotel_type = " + hotel_type + "]";
    }
}
