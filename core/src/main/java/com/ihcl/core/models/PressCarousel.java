/**
 *
 */
package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class PressCarousel extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    private List<String> imagePathLs = new ArrayList<String>();


    public List<String> getImagePathLs() {
        return imagePathLs;
    }

    @Override
    public void activate() throws Exception {
        String methodName = "activate";
        log.info("Method Entry :: > " + methodName);
        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();

        while (ni.hasNext()) {

            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setImagePathLs(ni2);
        }
        log.info("Method Exit ::> " + methodName);
    }

    public void setImagePathLs(NodeIterator ni2) {
        String methodName = "setImagePathLs";
        log.info("Method Entry :: > " + methodName);
        try {

            while (ni2.hasNext()) {
                Node grandChild = ni2.nextNode();
                log.info("GRAND CHILD NODE PATH IS " + grandChild.getPath());
                if (grandChild.hasProperty("imagepath")) {
                    imagePathLs.add(grandChild.getProperty("imagepath").getString());
                }
            }
            log.info("Method Exit ::> " + methodName);
        } catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }
    }
}
