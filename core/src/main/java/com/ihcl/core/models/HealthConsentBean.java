/**
 * 
 */
package com.ihcl.core.models;

/**
 * @author TCS
 *
 */
public class HealthConsentBean {

    String title;

    String firstName;

    String lastName;

    String email;

    String phoneNumber;

    String address;

    String city;

    String pincode;

    String state;

    String country;
    
    String passportNo;
    
    String organization;
    
    String relativesName;

	String relativePhoneNumber;
    
    String arrivingFrom;
    
    String departingTo;
    
    String departureDate;
    
    String arrivalDateInHotel;
    
    String arrivalDateInIndia;
    
    String listedSymptoms;

    String contactWithPatient;
    
    String quarantined;
    
    String confirmedPatient;
    
    String beenToRedZone;
    
    String hotelEmail;
    
    String signature;
    
    String hotelName;
    
    String mailToSelf;
    
    String hotelCity;
    
    String hotelArea;
    
    String hotelPin;
    
    String hotelStd;
    
    String hotelFax;
    
    String hotelPhone;
    
    String brandSite;
   

	public String getHotelCity() {
		return hotelCity;
	}


	public void setHotelCity(String hotelCity) {
		this.hotelCity = hotelCity;
	}


	public String getHotelArea() {
		return hotelArea;
	}


	public void setHotelArea(String hotelArea) {
		this.hotelArea = hotelArea;
	}


	public String getHotelPin() {
		return hotelPin;
	}


	public void setHotelPin(String hotelPin) {
		this.hotelPin = hotelPin;
	}


	public String getHotelStd() {
		return hotelStd;
	}


	public void setHotelStd(String hotelStd) {
		this.hotelStd = hotelStd;
	}


	public String getHotelFax() {
		return hotelFax;
	}


	public void setHotelFax(String hotelFax) {
		this.hotelFax = hotelFax;
	}


	public String getHotelPhone() {
		return hotelPhone;
	}


	public void setHotelPhone(String hotelPhone) {
		this.hotelPhone = hotelPhone;
	}


	public String getBrandSite() {
		return brandSite;
	}


	public void setBrandSite(String brandSite) {
		this.brandSite = brandSite;
	}


	public String getMailToSelf() {
		return mailToSelf;
	}


	public void setMailToSelf(String mailToSelf) {
		this.mailToSelf = mailToSelf;
	}


	public String getHotelEmail() {
		return hotelEmail;
	}


	public void setHotelEmail(String hotelEmail) {
		this.hotelEmail = hotelEmail;
	}


	public String getHotelName() {
		return hotelName;
	}


	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}


	public String getPassportNo() {
		return passportNo;
	}


	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}


	public String getOrganization() {
		return organization;
	}


	public void setOrganization(String organization) {
		this.organization = organization;
	}


	public String getRelativePhoneNumber() {
		return relativePhoneNumber;
	}


	public void setRelativePhoneNumber(String relativePhoneNumber) {
		this.relativePhoneNumber = relativePhoneNumber;
	}


	public String getArrivingFrom() {
		return arrivingFrom;
	}


	public void setArrivingFrom(String arrivingFrom) {
		this.arrivingFrom = arrivingFrom;
	}


	public String getDepartingTo() {
		return departingTo;
	}


	public void setDepartingTo(String departingTo) {
		this.departingTo = departingTo;
	}


	public String getArrivalDateInHotel() {
		return arrivalDateInHotel;
	}


	public void setArrivalDateInHotel(String arrivalDateInHotel) {
		this.arrivalDateInHotel = arrivalDateInHotel;
	}


	public String getArrivalDateInIndia() {
		return arrivalDateInIndia;
	}


	public void setArrivalDateInIndia(String arrivalDateInIndia) {
		this.arrivalDateInIndia = arrivalDateInIndia;
	}


	public String getListedSymptoms() {
		return listedSymptoms;
	}


	public void setListedSymptoms(String listedSymptoms) {
		this.listedSymptoms = listedSymptoms;
	}


	public String getContactWithPatient() {
		return contactWithPatient;
	}


	public void setContactWithPatient(String contactWithPatient) {
		this.contactWithPatient = contactWithPatient;
	}


	public String getQuarantine() {
		return quarantined;
	}


	public void setQuarantine(String quarantined) {
		this.quarantined = quarantined;
	}


	public String getConfirmedPatient() {
		return confirmedPatient;
	}


	public void setConfirmedPatient(String confirmedPatient) {
		this.confirmedPatient = confirmedPatient;
	}


	public String getBeenToRedZone() {
		return beenToRedZone;
	}


	public void setBeenToRedZone(String beenToRedZone) {
		this.beenToRedZone = beenToRedZone;
	}
    

    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for the field firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Setter for the field firstName
     *
     * @param firstName
     *            the firstName to set
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    /**
     * Getter for the field lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Setter for the field lastName
     *
     * @param lastName
     *            the lastName to set
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Getter for the field email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }


    /**
     * Setter for the field email
     *
     * @param email
     *            the email to set
     */

    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Getter for the field phoneNumber
     *
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Setter for the field phoneNumber
     *
     * @param phoneNumber
     *            the phoneNumber to set
     */

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Getter for the field address
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }


    /**
     * Setter for the field address
     *
     * @param address
     *            the address to set
     */

    public void setAddress(String address) {
        this.address = address;
    }


    /**
     * Getter for the field city
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }


    /**
     * Setter for the field city
     *
     * @param city
     *            the city to set
     */

    public void setCity(String city) {
        this.city = city;
    }


    /**
     * Getter for the field pincode
     *
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }


    /**
     * Setter for the field pincode
     *
     * @param pincode
     *            the pincode to set
     */

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }


    /**
     * Getter for the field state
     *
     * @return the state
     */
    public String getState() {
        return state;
    }


    /**
     * Setter for the field state
     *
     * @param state
     *            the state to set
     */

    public void setState(String state) {
        this.state = state;
    }


    /**
     * Getter for the field country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }


    /**
     * Setter for the field country
     *
     * @param country
     *            the country to set
     */

    public void setCountry(String country) {
        this.country = country;
    }


	public String getRelativesName() {
		return relativesName;
	}


	public void setRelativesName(String relativesName) {
		this.relativesName = relativesName;
	}


	public String getDepartureDate() {
		return departureDate;
	}


	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}


	public String getQuarantined() {
		return quarantined;
	}


	public void setQuarantined(String quarantined) {
		this.quarantined = quarantined;
	}


	public String getSignature() {
		return signature;
	}


	public void setSignature(String signature) {
		this.signature = signature;
	}

	
}
