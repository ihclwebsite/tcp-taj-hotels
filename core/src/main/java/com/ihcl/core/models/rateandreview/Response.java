/**
 *
 */
package com.ihcl.core.models.rateandreview;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.ihcl.core.models.SealApiResponse;

public class Response {

    @JsonProperty("response_list")
    private List<SealApiResponse> response_list;


    public List<SealApiResponse> getResponse_list() {
        return response_list;
    }


    public void setResponse_list(List<SealApiResponse> response_list) {
        this.response_list = response_list;
    }


}
