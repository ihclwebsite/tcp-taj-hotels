/**
 *
 */
package com.ihcl.core.models;

import org.apache.sling.api.resource.ValueMap;

/**
 * @author moonraft
 *
 */
public interface TajAirCardsListModel {

    /**
     * @return
     */
    ValueMap getValueMap();

}
