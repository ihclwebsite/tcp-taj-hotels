/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(
        adaptables = Resource.class,
        adapters = IhclGuestSpeakModel.class)

public class IhclGuestSpeakModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclGuestSpeakModel.class);

    @Self
    private Resource resource;

    public List<IhclGuestSpeakBean> list = new ArrayList<IhclGuestSpeakBean>();


    /**
     * Getter for the field list
     *
     * @return the list
     */
    public List<IhclGuestSpeakBean> getList() {
        return list;
    }


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getRankingValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @return
     *
     */
    private List<IhclGuestSpeakBean> getRankingValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] ranks = valueMap.get("list", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != ranks) {
            for (int i = 0; i < ranks.length; i++) {
                JsonElement jsonElement = gson.fromJson(ranks[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclGuestSpeakBean listBean = new IhclGuestSpeakBean();
                LOG.trace("jsonObject :" + jsonObject);


                if (null != jsonObject.get("name")) {
                    listBean.setName(jsonObject.get("name").getAsString());
                }
                if (null != jsonObject.get("title")) {
                    listBean.setDesignation(jsonObject.get("title").getAsString());
                }

                if (null != jsonObject.get("imagecard")) {
                    listBean.setImage(jsonObject.get("imagecard").getAsString());
                    LOG.debug("Image Card :::" + jsonObject.get("imagecard").getAsString());
                }

                if (null != jsonObject.get("description")) {
                    listBean.setDescription(jsonObject.get("description").getAsString());
                }


                list.add(listBean);

            }

        }
        return list;
    }


}
