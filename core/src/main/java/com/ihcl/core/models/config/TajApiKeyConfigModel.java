/**
 *
 */
package com.ihcl.core.models.config;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author Srikanta.moonraft
 *
 */
@ConsumerType
public interface TajApiKeyConfigModel {

    /**
     * @return
     */
    String getOpenWeatherMapApiKey();

    /**
     * @return
     */
    String getGoogleMapApiKey();

    String getTajAdminEmailId();

    String getHotelChainCode();
    
    String getIhclOwnerUsername();
    
    String getIhclOwnerPassword();
    
    String getChatBotId();
}
