package com.ihcl.core.models.offers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.day.cq.wcm.api.Page;

@Model(
        adaptables = SlingHttpServletRequest.class,
        adapters = PaginatedList.class)
public class PaginatedListImpl implements PaginatedList {

    private static final int PAGINATION_VAL = 3;

    private static final String OFFSET_PARAM = "offset";

    private int offset;

    @Self
    SlingHttpServletRequest request;

    @Inject
    List<Page> inputList;

    List<Page> paginatedList;

    Boolean more;

    @PostConstruct
    private void init() {
        if (inputList != null) {
            offset = request.getParameter(OFFSET_PARAM) == null ? 0
                    : Integer.parseInt(request.getParameter(OFFSET_PARAM));
            if (offset < inputList.size()) {
                more = offset + PAGINATION_VAL < inputList.size();
                paginatedList = inputList.subList(offset, more ? offset + PAGINATION_VAL : inputList.size());
            }
        }
    }

    @Override
    public Boolean getMore() {
        return more;
    }

    protected void setMore(Boolean more) {
        this.more = more;
    }

    @Override
    public List<Page> getPaginatedList() {
        return paginatedList;
    }

    @Override
    public int getPaginationVal() {
        return PAGINATION_VAL;
    }

    @Override
    public String getExportedType() {
        // TODO Auto-generated method stub
        return null;
    }

}
