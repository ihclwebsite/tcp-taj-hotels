/**
 *
 */
package com.ihcl.core.models.tic;

import java.util.List;

import javax.inject.Inject;

import com.ihcl.core.models.ImageTitleModel;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class)
public class TicEarnRedeemList {

	@Inject
	private List<ImageTitleModel> list;

	public List<ImageTitleModel> getList() {
		return list;
	}
}
