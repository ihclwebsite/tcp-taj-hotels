package com.ihcl.core.models.holidays.itinerary.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.models.holidays.itinerary.DestinationItineraryPropertyFetcher;
import com.ihcl.core.models.holidays.itinerary.ImageObject;
import com.ihcl.core.models.holidays.itinerary.ItineraryDetails;
import com.ihcl.core.models.holidays.itinerary.PlaceObject;

@Model(adaptables = { Resource.class,
		SlingHttpServletRequest.class }, adapters = DestinationItineraryPropertyFetcher.class)
public class DestinationItineraryPropertyFetcherImpl implements DestinationItineraryPropertyFetcher {

	private static final Logger LOG = LoggerFactory.getLogger(DestinationItineraryPropertyFetcherImpl.class);

	private static final String CRXconstantForItineraryDetailsComponent = "tajhotels/components/content/holidays-destination/itinerary-detail";

	private static final String IMAGE_GALLERY_NODE_NAME = "gallery";

	private ItineraryDetails itineraryDetails = new ItineraryDetails();

	private List<String> threeImagePaths;

	private List<String> itineraryTags;

	@Inject
	@Default(values = "")
	private String path;

	@Inject
	private ResourceResolverFactory resourceResolverFactory;

	@Inject
	private GalleryImageFetcherService galleryImageFetcherService;

	@PostConstruct
	public void activate() throws LoginException {
		String methodName = "activate";
		long startTime = new Date().getTime();
		LOG.trace("Method Entry: " + methodName);
		LOG.debug("Received path as: " + path);
		LOG.debug("Received resource resolver factory as: " + resourceResolverFactory);
		buildProperties();

		/*
		 * JSONObject jsonObj = new JSONObject(itineraryDetails);
		 * LOG.trace("ItineraryDetails obj :: " + jsonObj.toString());
		 */

		long endTime = new Date().getTime();
		LOG.debug("Total Time elapsed : " + (endTime - startTime));
		LOG.trace("Method Exit: " + methodName);
	}

	private void buildProperties() {
		String methodName = "buildProperties";
		try {
			LOG.trace("Method Entry: " + methodName);
			ResourceResolver resolver = resourceResolverFactory.getServiceResourceResolver(null);
			LOG.debug("Received resource resolver as: " + resolver);
			Resource itineraryPageResource = resolver.getResource(path);
			LOG.debug("Received itinerary page resource as: " + itineraryPageResource);
			fetchItineraryPropertiesFrom(itineraryPageResource);
		} catch (LoginException e) {
			LOG.error("An error occured while building properties for hotel at the sepcified path", e);
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private void fetchItineraryPropertiesFrom(Resource itineraryPageResource) {
		String methodName = "fetchItineraryPropertiesFrom";
		LOG.trace("Method Entry: " + methodName);
		Resource itineraryDetailsComponentResource = null;
		LOG.debug("Attempting to iterate over children of: " + itineraryPageResource);
		Iterable<Resource> children = itineraryPageResource.getChildren();
		for (Resource child : children) {
			LOG.debug("Iterating over child: " + child);
			if (child.getResourceType().equals(CRXconstantForItineraryDetailsComponent)) {
				LOG.debug("Found itineraryDetails component at child: " + child);
				itineraryDetailsComponentResource = child;
				buildItineraryProperties(itineraryDetailsComponentResource);
				break;
			}
			if (itineraryDetailsComponentResource == null) {
				fetchItineraryPropertiesFrom(child);
			}
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private void buildItineraryProperties(Resource itineraryDetailsComponentResource) {
		String methodName = "buildItineraryProperties";
		LOG.trace("Method Entry: " + methodName);
		try {
			itineraryDetails = new ItineraryDetails();
			ValueMap valueMap = itineraryDetailsComponentResource.adaptTo(ValueMap.class);

			String itineraryTitle = getProperty(valueMap, "itineraryTitle", String.class, "");
			LOG.trace("buildItineraryProperties :: Received itineraryTitle from JCR properties as " + itineraryTitle);

			String itineraryDescription = getProperty(valueMap, "itineraryDescription", String.class, "");
			LOG.trace("buildItineraryProperties :: Received itineraryDescription from JCR properties as "
					+ itineraryDescription);

			String[] imageList = getProperty(valueMap, "imageList", String[].class, new String[] {});
			LOG.trace("buildItineraryProperties :: Received imageList from JCR properties as " + imageList);

			String[] placeList = getProperty(valueMap, "placeList", String[].class, new String[] {});
			LOG.trace("buildItineraryProperties :: Received placeList from JCR properties as " + placeList);

			String[] type = getProperty(valueMap, "type", String[].class, new String[] {});
			LOG.trace("buildItineraryProperties :: Received type from JCR properties as " + type);

			String[] itineraryTag = getProperty(valueMap, "itineraryTag", String[].class, new String[] {});
			LOG.trace("buildItineraryProperties :: Received itineraryTag from JCR properties as " + itineraryTag);

			List<String> listOfImageList = new ArrayList<String>();
			if (imageList.length > 0) {
				listOfImageList = new ArrayList<>(Arrays.asList(imageList));
			}

			List<String> listOfType = new ArrayList<String>();
			if (type.length > 0) {
				listOfType = new ArrayList<>(Arrays.asList(type));
			}

			List<String> listOfItineraryTag = new ArrayList<String>();
			if (itineraryTag.length > 0) {
				listOfItineraryTag = new ArrayList<>(Arrays.asList(itineraryTag));
			}

			List<String> listOfPlaceList = new ArrayList<String>();
			if (placeList.length > 0) {
				listOfPlaceList = new ArrayList<>(Arrays.asList(placeList));
			}
			List<String> placeStringList = new ArrayList<String>();
			ObjectMapper mapper = new ObjectMapper();
			for (String str : listOfPlaceList) {
				PlaceObject placeObject = mapper.readValue(str, PlaceObject.class);
				LOG.info("Place Title : " + placeObject.title);
				placeStringList.add(placeObject.title);
				itineraryDetails.setSizeOfItineraryImages(placeStringList.size());
			}

			List<String> imageStringList = new ArrayList<String>();
			ObjectMapper mapper1 = new ObjectMapper();
			for (String str : listOfImageList) {
				ImageObject imageObject = mapper1.readValue(str, ImageObject.class);
				LOG.info("Images : " + imageObject.images);
				imageStringList.add(imageObject.images);
				itineraryDetails.setSizeOfItineraryImages(imageStringList.size());
			}
			/*
			 * LOG.info("Place Title 1 started");
			 * 
			 * PlaceObject placeObject = mapper.readValue(listOfPlaceList,
			 * mapper.getTypeFactory().constructCollectionType(List.class,
			 * PlaceObject.class)); //List<PlaceObject> placeObjectList =
			 * mapper.readValue((JsonParser) listOfPlaceList, new
			 * TypeReference<List<PlaceObject>>(){});
			 * LOG.info("Place Title 1 : "+placeObjectList.get(0).title);
			 */

			itineraryDetails.setItineraryTitle(itineraryTitle);
			itineraryDetails.setItineraryDescription(itineraryDescription);
			itineraryDetails.setItineraryImages(imageStringList);
			itineraryDetails.setItineraryPlaces(placeStringList);
			itineraryDetails.setItineraryTypes(listOfType);
			itineraryDetails.setItineraryTags(fetchItineraryTags(itineraryDetailsComponentResource));
			itineraryDetails.setItineraryImageValues(getImagesList(itineraryDetailsComponentResource));
			itineraryDetails.setFirstThreeImages(threeImagePaths);
			LOG.info("Leaving method: buildItineraryDetails()");

		} catch (Exception g) {
			LOG.error("Exception occured while building ItineraryProperties " + g.getMessage());
		}
		LOG.trace("Method Exit: " + methodName);
	}

	private List<JSONObject> getImagesList(Resource child) throws JSONException {

		List<JSONObject> imagePathsasJSON = null;
		threeImagePaths = new LinkedList<>();

		int count = 0;
		LOG.info("Inside method : getImagesList()");
		LOG.debug("resource path : {}", child.getPath());
		Resource galleryNode = child.getChild(IMAGE_GALLERY_NODE_NAME);
		if (galleryNode != null) {
			LOG.debug("resource name : {}", galleryNode.getName());
			imagePathsasJSON = galleryImageFetcherService.getGalleryImagePathsAt(galleryNode);
			for (JSONObject value : imagePathsasJSON) {
				if (count < 3) {
					threeImagePaths.add(value.getString("imagePath"));
					count++;
				}
			}
		} else {
			LOG.error("gallery image component not found at this resource location.");
		}
		LOG.info("Leaving method : getImagesList()");
		return imagePathsasJSON;
	}

	private List<String> fetchItineraryTags(Resource itineraryDetailsComponentResource) throws LoginException {

		List<String> itineraryTagsList = new ArrayList<String>();
		itineraryTags = new LinkedList<>();
		LOG.info("Inside method : getImagesList()");
		LOG.debug("resource path : {}", itineraryDetailsComponentResource.getPath());
		ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
		ValueMap valueMap = itineraryDetailsComponentResource.adaptTo(ValueMap.class);
		LOG.debug("valueMap ::" + valueMap);
		String[] tags = valueMap.get("itineraryTag", String[].class);
		LOG.debug("itineraryTag ::" + tags);
		Tag tagRoot = null;
		if (resourceResolver != null) {
			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			LOG.trace("tags array " + tags);
			if (tags != null) {
				for (int i = 0; i < tags.length; i++) {
					if (tags[i].startsWith("taj:Attractions")) {
						tagRoot = tagManager.resolve(tags[i]);
						if (tagRoot != null) {
							LOG.trace("Tag Root Path : " + tagRoot.getTitle());
							if (tagRoot.getPath().startsWith("/etc/tags/taj/Attractions")) {
								itineraryTags.add(tagRoot.getTitle());
							}
						}
					}
				}
			}
		}
		LOG.info("Leaving method : getImagesList()");
		itineraryTagsList.addAll(itineraryTags);
		return itineraryTagsList;

	}

	private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
		String methodName = "getProperty";
		LOG.trace("Method Entry: " + methodName);
		T value = defaultValue;
		if (valueMap.containsKey(key)) {
			value = valueMap.get(key, type);
		}
		LOG.trace("Value found for key: " + key + " : " + value);
		LOG.trace("Method Exit: " + methodName);
		return value;
	}

	@Override
	public ItineraryDetails getItineraryDetails() {
		return itineraryDetails;
	}
}
