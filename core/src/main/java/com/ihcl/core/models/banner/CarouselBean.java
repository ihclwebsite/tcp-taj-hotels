package com.ihcl.core.models.banner;

public class CarouselBean {
	
	private String bannerImage;
	private String bannerTitle;
	private String bannerLinkText;
	private String bannerLinkUrl;
	private String bannerdescription;
	private String bannerRichText;
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public String getBannerTitle() {
		return bannerTitle;
	}
	public void setBannerTitle(String bannerTitle) {
		this.bannerTitle = bannerTitle;
	}
	public String getBannerLinkText() {
		return bannerLinkText;
	}
	public void setBannerLinkText(String bannerLinkText) {
		this.bannerLinkText = bannerLinkText;
	}
	public String getBannerLinkUrl() {
		return bannerLinkUrl;
	}
	public void setBannerLinkUrl(String bannerLinkUrl) {
		this.bannerLinkUrl = bannerLinkUrl;
	}
	public String getBannerdescription() {
		return bannerdescription;
	}
	public void setBannerdescription(String bannerdescription) {
		this.bannerdescription = bannerdescription;
	}
	public CarouselBean(String bannerImage, String bannerTitle, String bannerLinkText, String bannerLinkUrl,
			String bannerdescription, String bannerRichText) {
		super();
		this.bannerImage = bannerImage;
		this.bannerTitle = bannerTitle;
		this.bannerLinkText = bannerLinkText;
		this.bannerLinkUrl = bannerLinkUrl;
		this.bannerdescription = bannerdescription;
		this.bannerRichText = bannerRichText;
	}
	public String getBannerRichText() {
		return bannerRichText;
	}
	public void setBannerRichText(String bannerRichText) {
		this.bannerRichText = bannerRichText;
	}
	
	

}
