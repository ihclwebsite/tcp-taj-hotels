package com.ihcl.core.models.jivaspa;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;


@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = JivaSpaListModel.class)

public class JivaSpaListModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    @Inject
    Resource resource;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @Inject
    private GalleryImageFetcherService galleryImageFetcher;

    @Inject
    @Default(values = "")
    private String componentPath;

    /*
     * @Inject
     *
     * @Default( values = "") private JivaSpaTreatmentBean jivaSpaBean;
     */

    private String spaName;

    List<Resource> children;

    private String spaShortDescription;

    private String spaLongDescription;

    private String spaDuration;

    private String spaAmount;

    private String resourceURL;

    private String treatmentTag;

    private String hotelLocation;

    private String spaRecommendedFor;

    private String bookingLink;

    private String spaId;

    private String spaCurrency;

    private List<JSONObject> galleryImagePaths;

    private Integer numberOfImages;

    private String isNonJivaSpa;

    private String draggedImagePath;

    private String treatmentName;

    @PostConstruct
    protected void init() {
        LOG.trace("Method Entry: init");
        LOG.trace("Method Entred :init " + resource.getResourceType());
        try {
            LOG.trace("componentPath :::::" + componentPath);
            if (StringUtils.isNotBlank(componentPath)) {
                LOG.debug("componentPath is not blank : " + componentPath);
                Resource jivaSpaResource = request.getResourceResolver().getResource(componentPath);
                fetchSpaResource(jivaSpaResource);
            }
            if (!resource.getResourceType().equals("tajhotels/components/content/jiva-spa-details")) {
                fetchSpaResource(resource);
            } else {
                initializeProperties(resource);
            }
        } catch (Exception e) {
            LOG.error("An error occured while fetching a resource jiva spa details: {}", e.getMessage());
            LOG.debug("An error occured while fetching a resource jiva spa details: {}", e);
        } finally {
            LOG.trace("Method Exit: init");
        }

    }


    /**
     *
     */
    private void fetchSpaResource(Resource spaResource) {
        LOG.trace("Method Entry: fetchSpaResource");
        Resource root = spaResource.getChild("jcr:content/root");
        if (null != root && !"".equals(root)) {
            LOG.trace("Inside root ");
            Iterable<Resource> rootChildren = root.getChildren();
            for (Resource rootResource : rootChildren) {
                LOG.trace("Inside resource :: " + rootResource);
                String resourceType = rootResource.getResourceType();
                if (resourceType.equals("tajhotels/components/content/jiva-spa-details")) {
                    initializeProperties(rootResource);
                }
            }

        }
        LOG.trace("Method Exit: fetchSpaResource");


    }


    private void initializeProperties(Resource currentResource) {
        LOG.trace("Method Entry: initializeProperties");
        ValueMap valueMap = currentResource.adaptTo(ValueMap.class);
        LOG.trace("valueMap :::::" + valueMap);
        spaShortDescription = valueMap.get("treatmentShortDescription", String.class);
        if (spaShortDescription == null || spaShortDescription.isEmpty()) {
            spaShortDescription = valueMap.get("spaShortDescription", String.class);
        }
        spaLongDescription = valueMap.get("spaLongDescription", String.class);
        spaDuration = valueMap.get("spaDuration", String.class);
        /*
         * if (null != jivaSpaBean && !"".equals(jivaSpaBean)) { LOG.trace("Spa Amount " +
         * jivaSpaBean.getTreatmentAmount()); spaAmount = jivaSpaBean.getTreatmentAmount(); } else { spaAmount =
         * valueMap.get("spaAmount", String.class); }
         */
        spaAmount = valueMap.get("spaAmount", String.class);
        hotelLocation = valueMap.get("hotelLocation", String.class);
        spaRecommendedFor = valueMap.get("spaRecommendedFor", String.class);
        spaName = valueMap.get("spaName", String.class);
        treatmentTag = valueMap.get("treatmentTag", String.class);
        resourceURL = resource.getPath().concat(".html");
        bookingLink = valueMap.get("bookingLink", String.class);
        isNonJivaSpa = valueMap.get("isNonJivaSpa", String.class);
        draggedImagePath = valueMap.get("fileReference", String.class);
        LOG.debug("draggedImagePath value now is : " + draggedImagePath);
        LOG.trace("Is jiva spa " + isNonJivaSpa);
        String currencyValue = valueMap.get("currencySymbol", String.class);
        if (null != currencyValue) {
            spaCurrency = currencyFetcherService.getCurrencySymbolValue(currencyValue);
        }
        if (bookingLink != null && !"".equals(bookingLink)) {
            bookingLink.concat(".html");
        }
        if (null != spaName && null != treatmentTag) {
            spaId = spaName.replaceAll(" ", "");
            treatmentName = splitTreatmentTag(treatmentTag);
            spaId = spaId.concat(treatmentName);
        }
        Resource gallery = currentResource.getChild("gallery");
        if (null != gallery) {
            galleryImagePaths = galleryImageFetcher.getGalleryImagePathsAt(gallery);
            numberOfImages = galleryImagePaths.size();
        }
        LOG.trace("Method Exit: initializeProperties");
    }


    /**
     * @return the request
     */
    public SlingHttpServletRequest getRequest() {
        return request;
    }

    /**
     * @return the currentPage
     */
    public Page getCurrentPage() {
        return currentPage;
    }

    /**
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @return the spaName
     */
    public String getSpaName() {
        return spaName;
    }

    /**
     * @return the children
     */
    public List<Resource> getChildren() {
        return children;
    }

    /**
     * @return the spaShortDescription
     */
    public String getSpaShortDescription() {
        return spaShortDescription;
    }

    /**
     * @return the spaLongDescription
     */
    public String getSpaLongDescription() {
        return spaLongDescription;
    }

    /**
     * @return the spaDuration
     */
    public String getSpaDuration() {
        return spaDuration;
    }

    /**
     * @return the spaAmount
     */
    public String getSpaAmount() {
        return spaAmount;
    }

    /**
     * @return the resourceURL
     */
    public String getResourceURL() {
        return resourceURL;
    }

    /**
     * @return the treatmentTag
     */
    public String getTreatmentTag() {
        return treatmentTag;
    }

    /**
     * @return the hotelLocation
     */
    public String getHotelLocation() {
        return hotelLocation;
    }

    /**
     * @return the spaRecommendedFor
     */
    public String getSpaRecommendedFor() {
        return spaRecommendedFor;
    }

    /**
     * @return the bookingLink
     */
    public String getBookingLink() {
        return bookingLink;
    }

    /**
     * @return the spaId
     */
    public String getSpaId() {
        return spaId;
    }

    /**
     * @return the galleryImagePaths
     */
    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }

    /**
     * @return the galleryImageFetcher
     */
    public GalleryImageFetcherService getGalleryImageFetcher() {
        return galleryImageFetcher;
    }

    /**
     * @return the numberOfImages
     */
    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    public String getSpaCurrency() {
        return spaCurrency;
    }

    public String getIsNonJivaSpa() {
        return isNonJivaSpa;
    }

    public String getDraggedImagePath() {
        return draggedImagePath;
    }

    public String getJivaSpaBookingLink() {
        LOG.trace("Value for booking link in Jiva spa: " + CrxConstants.JIVA_SPA_BOOKING_URL);
        return CrxConstants.JIVA_SPA_BOOKING_URL;
    }

    public String getSpaBookingLink() {
        LOG.trace("Value for booking link in spa: " + CrxConstants.SPA_BOOKING_URL);
        return CrxConstants.SPA_BOOKING_URL;
    }

    private String splitTreatmentTag(String treatMentTag) {
        String[] arrOfStr = treatMentTag.split("/", -1);
        LOG.trace("Returning treatment name as :" + arrOfStr[1]);
        return arrOfStr[1];
    }
}
