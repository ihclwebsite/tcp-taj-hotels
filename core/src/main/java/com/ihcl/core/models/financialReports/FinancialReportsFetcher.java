package com.ihcl.core.models.financialReports;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = FinancialReportsFetcher.class)
public class FinancialReportsFetcher {

	  private final Logger LOG = LoggerFactory.getLogger(getClass());

	    @Inject
	    SlingHttpServletRequest request;

	    @Inject
	    private Page currentPage;

	    @Inject
	    Resource resource;
	    
	    @Inject
	    private ResourceResolverFactory resourceResolverFactory;

	    @Reference
	    private QueryBuilder builder;

	    private Session session;
	    
	    private String quarterlyReportPath;
	    
	    private String annualReportPath;
	    
	    private String analystMeetReportPath;
	    
	    public List<FinancialReportsBean> getQuarterihcl() {
	    	LOG.debug("quarterihcl: "+quarterihcl);
			return quarterihcl;
		}

		public List<FinancialReportsBean> getQuartershare() {
			LOG.debug("quartershare: "+quartershare);
			return quartershare;
		}


		public List<FinancialReportsBean> getAnnualihcl() {
			LOG.debug("annualihcl: "+annualihcl);
			return annualihcl;
		}

		public List<FinancialReportsBean> getAnnualshare() {
			LOG.debug("annualshare: "+annualshare);
			return annualshare;
		}

		public List<FinancialReportsBean> getAnalystihcl() {
			LOG.debug("analystihcl: "+analystihcl);
			return analystihcl;
		}


		public List<FinancialReportsBean> getAnalystshare() {
			LOG.debug("analystshare: "+analystshare);
			return analystshare;
		}


		private ResourceResolver resolver;
	    
	    private List<FinancialReportsBean> quarterihcl = new ArrayList<FinancialReportsBean>();
	    
	    private List<FinancialReportsBean> quartershare = new ArrayList<FinancialReportsBean>();
	    
	    private List<FinancialReportsBean> annualihcl = new ArrayList<FinancialReportsBean>();
	    
	    private List<FinancialReportsBean> annualshare = new ArrayList<FinancialReportsBean>();
	    
	    private List<FinancialReportsBean> analystihcl = new ArrayList<FinancialReportsBean>();
	    
	    private List<FinancialReportsBean> analystshare = new ArrayList<FinancialReportsBean>();
	    

	    @PostConstruct
	    protected void init() {
	        String methodName = "init";
	        LOG.trace("Method Entry: " + methodName);
	        LOG.trace("Method Entred :init " + resource.getResourceType());
	        pdfPath();
	        LOG.trace("Method Exit: " + methodName);
	    }

	    
	    private void pdfPath()
	    {
	    	 String methodName = "pdfPath";
		        LOG.trace("Method Entry: " + methodName);
		        LOG.trace("Method Entred :init " + resource.getPath());
              	
		        try {
		        	ValueMap valuemap=resource.adaptTo(ValueMap.class);
		        	if(valuemap!=null) {
		        	quarterlyReportPath=valuemap.get("quarterPath", String.class);
		        	getListofReports(quarterlyReportPath);
		        	LOG.trace("The Quarterly ReportPath " + quarterlyReportPath);
		        	annualReportPath=valuemap.get("annualPath", String.class);
		        	getListofReports(annualReportPath);
		        	LOG.trace("The Annual ReportPath " + annualReportPath);
		        	analystMeetReportPath=valuemap.get("analystPath", String.class);
		        	getListofReports(analystMeetReportPath);
		        	LOG.trace("The Analyst MeetReportPath" + analystMeetReportPath);
		        	}
					
					} catch (RepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();	        			        
					}
		        LOG.trace("Method Exit: " + methodName);
	    }
	       public void getListofReports(String reportPath) throws RepositoryException {
	        String methodName = "getListofReports";
	        LOG.trace(" Method Entry :" + methodName);
	        
	        List<String> pagePath = new ArrayList<String>();
	        Map<String, String> queryParam = new HashMap<String, String>();
	        queryParam.put("type", "dam:Asset");
	        queryParam.put("path",reportPath);
	        queryParam.put("p.limit", "-1");
	        LOG.trace(" queryParam : " + queryParam);

	        resolver = resource.getResourceResolver();
	        session = resolver.adaptTo(Session.class);

	        QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
	        LOG.trace("Query builder : " + builder);
	        PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
	        LOG.trace("SearchPredicates : " + searchPredicates);
	        Query query = builder.createQuery(searchPredicates, session);

	        LOG.trace("Query to be executed is : " + query);
	        query.setStart(0L);

	        SearchResult result = query.getResult();
	        LOG.trace("Query Executed");
	        LOG.trace("Query result is : " + result.getHits().size());

	        List<Hit> hits = result.getHits();
	        Iterator<Hit> hitList = hits.iterator();
	        Hit hit;
	        while (hitList.hasNext()) {
	            hit = hitList.next();
	            LOG.trace("The result is : " + hit.getPath());
	            pagePath.add(hit.getPath());
	        }
	        filterReports(pagePath);
	       }
	    
	       
	       public void filterReports(List<String> pdfList)
	       {
	    	   
	    	   if(pdfList!=null)
	    	   {
	    		   for (String listItem :pdfList) {
	    		   if (listItem.contains("quarterly-reports") && listItem.contains("The Indian Hotels Company Limited (IHCL) Results"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   quarterihcl.add(resBean);
	    		   }
	    		   else if((listItem.contains("quarterly-reports")) && listItem.contains("Distribution of Shareholding"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   quartershare.add(resBean);
	    			
	    		   }
	    		   else if (listItem.contains("annual-reports") && listItem.contains("The Indian Hotels Company Limited (IHCL) Results"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   annualihcl.add(resBean);
	    		   }
	    		   else if((listItem.contains("annual-reports")) && listItem.contains("Distribution of Shareholding"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   annualshare.add(resBean);
	    			
	    		   }
	    		   else if (listItem.contains("analyst-meet") && listItem.contains("The Indian Hotels Company Limited (IHCL) Results"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   analystihcl.add(resBean);
	    		   }
	    		   else if((listItem.contains("analyst-meet")) && listItem.contains("Distribution of Shareholding"))
	    		   {
	    			   FinancialReportsBean resBean= getDetails(listItem);
	    			   analystshare.add(resBean);
	    			
	    		   }
	    		   }
	    		   

	    		   
	    	   }
	       }
	       
	       public FinancialReportsBean getDetails(String PDFPath)
	       {
	    	   String methodName = "getDetails";
		        LOG.trace("Method Entry: " + methodName);
	    	   FinancialReportsBean beanObj=new FinancialReportsBean();
	    	   if(PDFPath!=null) {
	    		   beanObj.setPdfPath(PDFPath);
	    		   Resource pdfPathResource = resolver.getResource(PDFPath);
	    		   pdfPathResource=pdfPathResource.getChild("jcr:content/metadata");
	    		   ValueMap vMap=pdfPathResource.adaptTo(ValueMap.class);
	    		   beanObj.setTitle(vMap.get("jcr:title",String.class));	    		   
	    		   LOG.trace("The PDF title Value : " + vMap.get("jcr:title",String.class));
	    	   }
	    	   return beanObj;
	    	   	       }
}
