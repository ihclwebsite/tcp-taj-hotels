/**
 *
 */
package com.ihcl.core.models.ihcl;


/**
 * @author moonraft
 *
 */
public class KeyFiguresBean {


    private String numberKeys;

    private String currency;

    private String entity;


    /**
     * Getter for the field numberKeys
     *
     * @return the numberKeys
     */
    public String getNumberKeys() {
        return numberKeys;
    }


    /**
     * Setter for the field numberKeys
     *
     * @param numberKeys
     *            the numberKeys to set
     */

    public void setNumberKeys(String numberKeys) {
        this.numberKeys = numberKeys;
    }


    /**
     * Getter for the field currency
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }


    /**
     * Setter for the field currency
     *
     * @param currency
     *            the currency to set
     */

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    /**
     * Getter for the field entity
     *
     * @return the entity
     */
    public String getEntity() {
        return entity;
    }


    /**
     * Setter for the field entity
     *
     * @param entity
     *            the entity to set
     */

    public void setEntity(String entity) {
        this.entity = entity;
    }


}
