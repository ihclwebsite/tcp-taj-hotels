/**
 * 
 */
package com.ihcl.core.models.destination;

import java.util.List;

/**
 * <pre>
 * DestinationMap Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :31-May-2019
 * @ClassName : DestinationMap.java
 * @Description : com.ihcl.core.models.impl -DestinationMap.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  31-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class DestinationMap {
	
	private List<DestinationHotelsBean> destinationList;

	/**
	 * @return the destinationList
	 */
	public List<DestinationHotelsBean> getDestinationList() {
		return destinationList;
	}

	/**
	 * @param destinationList the destinationList to set
	 */
	public void setDestinationList(List<DestinationHotelsBean> destinationList) {
		this.destinationList = destinationList;
	}

	/**
	 * @param destinationList
	 */
	public DestinationMap(List<DestinationHotelsBean> destinationList) {
		super();
		this.destinationList = destinationList;
	}
	

}
