
package com.ihcl.core.models;

import java.util.Map;

public interface HotelPageList {

    Map<String, Map<String, Map<String, String>>> getHotelListMapData();

    Map<String, String> getOfferNameList();

    Map<String, Map<String, Map<String, String>>> getMeetingDetails();

    Map<String, String> getAllDestinationLinks();

    Map<String, String> getAllDestinationDiningLinks();

}
