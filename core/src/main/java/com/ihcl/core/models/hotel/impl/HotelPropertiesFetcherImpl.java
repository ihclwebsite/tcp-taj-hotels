package com.ihcl.core.models.hotel.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.SignatureFeatureWithGroupIconModel;
import com.ihcl.core.models.hotel.HotelPropertiesFetcher;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = HotelPropertiesFetcher.class)
public class HotelPropertiesFetcherImpl implements HotelPropertiesFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(HotelPropertiesFetcherImpl.class);

    private static final String DINING_DETAILS_RESOURCE_NAME = "jcr:content";

    private static final String HOTEL_DETAILS_RESOURCE_TYPE = "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-palace/jcr:content";

    private static final String DINING_DETAILS_RESOURCE_TYPE = "tajhotels/components/content/dining-details";

    @Inject
    private Resource resource;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    @Inject
    @Optional
    private String diningDetailsPath;

    private String name;
    
    private String navTitle;

    private String hotelLongitude;

    private String hotelLatitude;

    private List<String> hotelPhoneNumber;

    private String hotelStdCode;

    private String hotelPinCode;

    private String hotelCity;

    private String hotelArea;

    private String hotelEmail;

    private String hotelIdentifier;

    private String hotelCountry;

    private String trustYouId;

    private String hotelChainCode;

    private String hotelHeirarchy;

    private String hotelPath;

    private String hotelRoomsPath;

    private String guestRoomPath;

    private String hotelState;

    private String jivaSpaEmail;

    private String requestQuoteEmail;

    private ResourceResolver resolver;

    private ValueMap hotelDetailsMap;

    private String hotelBrand;

    private String hotelLocale;

    private String jivaSpaBookingPath;

    private String spaBookingPath;

    private String hotelLocationId;

    private String hotelsPath;

    private String resolvedHotelsPath;
    
    private String roomTypeCode;
    
    private String SameDayCheckOut;
    
    private String isOnlyBungalowPage;

    private ArrayList<SignatureFeatureWithGroupIconModel> signatureModel = new ArrayList<SignatureFeatureWithGroupIconModel>();


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Received resource as: " + resource);
            LOG.debug("Received resource fetcher service as: " + resourceFetcherService);
            resolver = resource.getResourceResolver();
            if (resource.isResourceType("tajhotels/components/content/dining-list")) {
                LOG.debug("Resource type is true : " + resourceFetcherService);
                buildHotelPropertiesFromChild();
            }
            buildHotelProperties();
            getHotelLocaleValue();
        } catch (Exception e) {
            LOG.debug("Exception found in PostConstruct :: {}", e);
            LOG.error("Exception found in PostConstruct :: {}", e.getMessage());
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void buildHotelPropertiesFromChild() {
        try {
            LOG.trace("Method etry : buildHotelPropertiesFromChild()");
            LOG.trace("diningDetailsPath3 : " + diningDetailsPath);

            if (StringUtils.isBlank(diningDetailsPath)) {
                LOG.debug("diningDetailsPath is Null, Can't proceed further");
                LOG.warn("diningDetailsPath is Null, Can't proceed further");
                // LOG.error("diningDetailsPath is Null, Can't proceed further");
            }
            if (StringUtils.isNotBlank(diningDetailsPath)) {
                LOG.debug("diningDetailsPath : " + diningDetailsPath);
                hotelDetailsMap = resource.getResourceResolver().getResource(diningDetailsPath).getParent().getParent()
                        .getChild("jcr:content").getValueMap();
                hotelCity = hotelDetailsMap.get("hotelCity", String.class);
                hotelCountry = hotelDetailsMap.get("hotelCountry", String.class);
                hotelArea = hotelDetailsMap.get("hotelArea", String.class);
                hotelState = hotelDetailsMap.get("addressRegion", String.class);
                name = hotelDetailsMap.get("hotelName", String.class);
                navTitle = hotelDetailsMap.get("navTitle", String.class);
                // Setting jiva spa email id from properties. if the values is not there, set hotel email id.
                jivaSpaEmail = hotelDetailsMap.get("jivaSpaEmail", String.class);
                if (null == jivaSpaEmail || jivaSpaEmail == "") {
                    LOG.trace("Jiva spa email id value is not there in properties.:");
                    jivaSpaEmail = hotelDetailsMap.get("hotelEmail", String.class);
                }
                LOG.trace("Jiva spa email id is :" + jivaSpaEmail);

                // Setting request quote email id from properties. if the values is not there, set hotel email id.
                requestQuoteEmail = hotelDetailsMap.get("requestQuoteEmail", String.class);
                if (null == requestQuoteEmail || requestQuoteEmail == "") {
                    LOG.trace("Request quote email id value is not there in properties.:");
                    requestQuoteEmail = hotelDetailsMap.get("hotelEmail", String.class);
                }
                LOG.trace("Request Quote email id is :" + requestQuoteEmail);

                LOG.debug("Hotel JCR content : " + hotelDetailsMap);
            }

        } catch (Exception e) {
            LOG.error("Exception found in BuildHotelPropertiesFromChild :: {}", e.getMessage());
            LOG.debug("Exception found in BuildHotelPropertiesFromChild :: {}", e);
        }
        LOG.trace("Method exit : buildHotelPropertiesFromChild()");
    }

    /**
     *
     */
    private void buildHotelProperties() {
        String methodName = "buildHotelProperties";
        try {

            LOG.trace("Method Entry: " + methodName);
            Resource hotelResource = resourceFetcherService.getParentOfType(resource,
                    CrxConstants.HOTEL_LANDING_PAGE_RESOURCETYPE);
            LOG.debug("Received hotel page in hierarchy as: " + hotelResource);
            if (hotelResource != null) {
                fetchPropertiesFrom(hotelResource);
            }
        } catch (Exception e) {
            LOG.error("An error occured while building properties for hotel using the given resource: " + resource, e);
        }
        LOG.trace("Method Exit: " + methodName);

    }

    /**
     * @param resource
     */
    private void fetchPropertiesFrom(Resource resource) {
        LOG.trace("Method Entry: fetchPropertiesFrom");
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        hotelPhoneNumber = new ArrayList<>();
        if (StringUtils.isBlank(name)) {
            name = getProperty(valueMap, "hotelName", String.class, "");
        }
        navTitle = getProperty(valueMap, "navTitle", String.class, "");
        hotelLatitude = getProperty(valueMap, "hotelLatitude", String.class, "");
        hotelLongitude = getProperty(valueMap, "hotelLongitude", String.class, "");
        hotelArea = getProperty(valueMap, "hotelArea", String.class, "");
        hotelState = getProperty(valueMap, "addressRegion", String.class, "");
        hotelCity = getProperty(valueMap, "hotelCity", String.class, "");
        hotelPinCode = getProperty(valueMap, "hotelPinCode", String.class, "");
        hotelStdCode = getProperty(valueMap, "hotelStdCode", String.class, "");
        hotelPhoneNumber = getNumbersList(getProperty(valueMap,"hotelPhoneNumber", String.class, ""));
        hotelEmail = getProperty(valueMap, "hotelEmail", String.class, "");
        hotelCountry = getProperty(valueMap, "hotelCountry", String.class, "");
        hotelIdentifier = getProperty(valueMap, "hotelId", String.class, "");
        trustYouId = getProperty(valueMap, "trustyouid", String.class, "");
        hotelChainCode = getProperty(valueMap, "hotelchaincode", String.class, "");
        hotelPath = resource.getPath();
        hotelRoomsPath = getMappedPath(getProperty(valueMap, "roomsPath", String.class, ""));
        hotelBrand = getProperty(valueMap, "brand", String.class, "");
        hotelLocationId = getProperty(valueMap, "locationId", String.class, "");
        hotelsPath = resource.getPath().replaceAll("/jcr:content", "");
        resolvedHotelsPath = resolver.map(hotelsPath);
        roomTypeCode=getProperty(valueMap, "roomTypeCode", String.class, "");
        
        // Same Day Checkin checkout 
        SameDayCheckOut = getProperty(valueMap, "SameDayCheckOut", String.class, "");
        LOG.trace("SameDayCheckOut:::::::: ", SameDayCheckOut);
        LOG.debug("SameDayCheckOut:::::::: ", SameDayCheckOut);
        LOG.info("SameDayCheckOut:::::::: ", SameDayCheckOut);
        LOG.error("SameDayCheckOut:::::::: ", SameDayCheckOut);
        // Setting jiva spa email id from properties. if the values is not there, set hotel email id.
        jivaSpaEmail = getProperty(valueMap, "jivaSpaEmail", String.class, "");
        if (null == jivaSpaEmail || jivaSpaEmail == "") {
            LOG.trace("Jiva spa email id value is not there in properties.:");
            jivaSpaEmail = getProperty(valueMap, "hotelEmail", String.class, "");
        }
        LOG.trace("Jiva spa email id is :" + jivaSpaEmail);

        // Setting request quote email id from properties. if the values is not there, set hotel email id.
        requestQuoteEmail = getProperty(valueMap, "requestQuoteEmail", String.class, "");
        if (null == requestQuoteEmail || requestQuoteEmail == "") {
            LOG.trace("Request quote email id value is not there in properties.:");
            requestQuoteEmail = getProperty(valueMap, "hotelEmail", String.class, "");
        }
        LOG.trace("Request Quote email id is :" + requestQuoteEmail);
        try {
        Resource hotelFeatures = resource.getChild("root").getChild("hotel_overview")
                .getChild("hotelSignatureFeatures");
        LOG.trace("Values to fetch signature features : " + hotelFeatures);
        if (hotelFeatures != null) {
            fetchHotelSignatureFeatures(hotelFeatures);
        }
        }catch(NullPointerException e) {
        	LOG.error("Exception caugt for hotelSignature Feature not present");
        }
        
        jivaSpaBookingPath = getBookingPathForJivaSpa(hotelPath);
        spaBookingPath = "/content/tajhotels/en-in/spa/spa-booking.html";

        LOG.trace("Spa booking path is : " + spaBookingPath);

        LOG.trace("Hotel path is : " + hotelPath);
        LOG.trace("Method Exit: fetchPropertiesFrom");
    }

    public List<String> getNumbersList(String contactNumbers){
         
        LOG.trace("Method entry contactNumbers(), received number string as {}",contactNumbers);
        List<String> hotelNumbers=new ArrayList<>();
        if(contactNumbers != null){
            String[] phoneNumbersArray = contactNumbers.split(",");
            hotelNumbers=Arrays.asList(phoneNumbersArray); 
            }
        LOG.trace("Method exit contactNumbers()");
        return hotelNumbers;
    }
    
    private String getMappedPath(String localRoomsPath) {
        if (localRoomsPath != null) {
            String resolvedRoomURL = resolver.map(URLMapUtil.appendHTMLExtension(localRoomsPath));
            return resolvedRoomURL;
        }
        return localRoomsPath;
    }

    public void fetchHotelSignatureFeatures(Resource roomCodesList) {
        String method = "fetchHotelSignatureFeatures";
        LOG.trace("Method entry :" + method);

        Iterable<Resource> roomCodeIter = roomCodesList.getChildren();
        for (Resource items : roomCodeIter) {
            SignatureFeatureWithGroupIconModel siteMapEntry = new SignatureFeatureWithGroupIconModel();
            LOG.trace("Item value is :" + items);
            ValueMap valueMap = items.adaptTo(ValueMap.class);
            siteMapEntry.setGroupIconPath(getProperty(valueMap, "type", String.class, ""));
            siteMapEntry.setSignatureFeatureValue(getProperty(valueMap, "signatureFeatureValue", String.class, ""));
            LOG.trace("Signature Model values :" + siteMapEntry.getGroupIconPath() + " "
                    + siteMapEntry.getSignatureFeatureValue());
            signatureModel.add(siteMapEntry);
            LOG.trace("Signature Model values :" + siteMapEntry.getGroupIconPath() + " "
                    + siteMapEntry.getSignatureFeatureValue());
        }
        LOG.trace("Method exit :" + method);
    }


    private String getBookingPathForJivaSpa(String jivaSpaHotelPath) {
        String path = jivaSpaHotelPath.replace("jcr:content", "");
        String jivaSpa = path.concat("jiva-spa/jiva-spa-booking.html");
        LOG.trace("Path to booking jiva spa appointment :" + jivaSpa);
        return jivaSpa;
    }

    /**
     * @param valueMap
     * @param key
     * @param type
     * @param defaultValue
     */
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        String methodName = "getProperty";
        LOG.trace("Method Entry: " + methodName);
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        LOG.trace("Value found for key: " + key + " : " + value);
        LOG.trace("Method Exit: " + methodName);
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getHotelLatitude() {
        return hotelLatitude;
    }

    @Override
    public String getHotelLongitude() {
        return hotelLongitude;
    }

    @Override
    public String getHotelArea() {
        return hotelArea;
    }

    @Override
    public String getHotelCity() {
        return hotelCity;
    }

    @Override
    public String getHotelPinCode() {
        return hotelPinCode;
    }

    @Override
    public String getHotelStdCode() {
        return hotelStdCode;
    }

    @Override
    public List<String> getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    @Override
    public String getHotelEmail() {
        return hotelEmail;
    }

    @Override
    public String getHotelCountry() {
        return hotelCountry;
    }

    @Override
    public String getHotelIdentifier() {
        return hotelIdentifier;
    }

    @Override
    public String getTrustYouId() {
        return trustYouId;
    }

    @Override
    public String getHotelChainCode() {
        return hotelChainCode;
    }

    @Override
    public String getGuestRoomPath() {
        return guestRoomPath;
    }

    @Override
    public String getHotelState() {
        return hotelState;
    }

    @Override
    public String getJivaSpaEmail() {
        return jivaSpaEmail;
    }

    @Override
    public String getRequestQuoteEmail() {
        return requestQuoteEmail;
    }

    @Override
    public String getHotelHeirarchy() {
        return extractHotelHierarchy(hotelPath);
    }

    @Override
    public ValueMap getHotelDetailMap() {
        return hotelDetailsMap;
    }

    private String extractHotelHierarchy(String hotelPath) {
        if (hotelPath != null) {
            String[] pathArray = hotelPath.split(CrxConstants.PATH_SEPARATOR);
            StringBuilder sb = new StringBuilder();
            // eg:
            // ,content,tajhotels,in,en,our-hotels,hotels-in-mumbai,taj,taj-mahal-palace,jcr:content
            try {
                if (pathArray.length > 0) {
                    sb.append(pathArray[2]);
                    sb.append(",");
                    sb.append(pathArray[3]);
                    sb.append(",");
                    sb.append(pathArray[pathArray.length - 3]);
                    sb.append(",");
                    sb.append(pathArray[pathArray.length - 2]);
                    return sb.toString();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                LOG.error(
                        "Could not fetch path for the hotel" + hotelPath + ": hence assigning hotel hierarchy to null");

            }
        }
        return null;
    }

    private void getHotelLocaleValue() {
        LOG.trace("Inside getHotelLocaleValue()");
        hotelLocale = extractHotelLocale(hotelPath);

    }

    private String extractHotelLocale(String hotelPathLocale) {
        LOG.trace("Inside getHotelLocaleValue()");
        LOG.debug("Current path value " + hotelPathLocale);
        if (hotelPathLocale != null) {
            String[] pathArray = hotelPathLocale.split(CrxConstants.PATH_SEPARATOR);

            try {
                if (pathArray.length > 0) {
                    LOG.debug("Locale value is " + pathArray[3]);
                    return pathArray[3];
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.getMessage();
            }
        }
        return null;
    }

    @Override
    public String getHotelRoomsPath() {
        return hotelRoomsPath;
    }

    @Override
    public String getHotelBrand() {
        LOG.trace("Inside getHotelBrand()");
        String hotelBrandArrayvalue = null;
        String[] hotelBrandData = null;
        LOG.debug("Value of hotelBrand is " + hotelBrand);
        if (hotelBrand != null && !hotelBrand.isEmpty()) {
            hotelBrandData = hotelBrand.split("/");
            if (hotelBrandData.length >= 2) {
                hotelBrandArrayvalue = hotelBrandData[2];
                LOG.debug("Value of brand is " + hotelBrandArrayvalue);
            } else {
                LOG.debug("Value of brand is not available, Array Length is less than expected ");
            }
        } else {
            LOG.debug("hotelBrand is probably null or empty, And returned null in this case.");
        }
        return hotelBrandArrayvalue;
    }

    @Override
    public String getHotelLocale() {

        return hotelLocale;
    }

    @Override
    public String getJivaSpaBookingPath() {
        return jivaSpaBookingPath;
    }

    @Override
    public ArrayList<SignatureFeatureWithGroupIconModel> getSignatureModel() {
        LOG.trace("Returning model :" + signatureModel);
        return signatureModel;
    }

    @Override
    public String getSpaBookingPath() {
        return spaBookingPath;
    }

    @Override
    public String getHotelLocationId() {
        return hotelLocationId;
    }

    @Override
    public String getHotelsPath() {
        return resolvedHotelsPath;
    }

	@Override
	public String getNavTitle() {
		return navTitle;
	}

	@Override
	public String getRoomTypeCode() {
		
		return roomTypeCode;
	}

	@Override
	public String getSameDayCheckout() {
		// TODO Auto-generated method stub
		return SameDayCheckOut;
	}

	public String getIsOnlyBungalowPage() {
		return isOnlyBungalowPage;
	}

	@Override
	public String getisOnlyBungalowPage() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
}
