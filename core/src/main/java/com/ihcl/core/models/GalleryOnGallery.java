package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class)
public class GalleryOnGallery {

    private static final Logger LOG = LoggerFactory.getLogger(GalleryOnGallery.class);

    @Inject
    @Optional
    private List<Gallery> galleryPaths;

    @PostConstruct
    public void activate() {
        LOG.info("INSIDE ACTIVATE OF GalleryOnGallery");

    }

    public List<Gallery> getGalleryPaths() {
        return galleryPaths;
    }


}
