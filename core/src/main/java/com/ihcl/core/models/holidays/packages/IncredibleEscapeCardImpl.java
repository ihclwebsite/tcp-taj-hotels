package com.ihcl.core.models.holidays.packages;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.holidays.hotel.Inclusion;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = IIncredibleEscapesCard.class)
public class IncredibleEscapeCardImpl implements IIncredibleEscapesCard {

	private static final Logger LOG = LoggerFactory.getLogger(IncredibleEscapeCardImpl.class);
	private static final String PACKAGES_PAGE_RESOURCE_TYPE = "tajhotels/components/structure/tajholidays-incredible-escapes-landing-page";
	private static final String PACKAGES_HOTELS_RESOURCE_TYPE = "tajhotels/components/structure/tajholidays-base-page";
	private static final String PACKAGE_DESC_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/package-description";
	private static final String PACKAGE_SUMMARY_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/package-summary";
	private static final String PACKAGE_INCLUSIONS_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/holidays-package-inclusions";
	private static final String HOTELS_DETAILS_RESOURCE_TYPE = "tajhotels/components/content/holidays-packages/holidays-packages-hotels";
	private static final String BANNER_CAROUSEL_RESOURCE_TYPE = "tajhotels/components/content/banner-carousel";
	private static final String ROOT_PAGE_PATH = "/content/tajhotels/en-in/taj-holidays/incredible-escapes";
	private static final String CATEGORY_COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays/holidays-category-selection";
	private static final String DESTINATION_RESOURCE_TYPE = "tajhotels/components/structure/tajholidays-destination-landing-page";
	@Inject
	Resource resource;

	@Inject
	ResourceResolverFactory resourceResolverFactory;

	@Inject
	SearchProvider searchProvider;

	List<IncredibleEscapeCard> allPackageCards;

	boolean rootPageFlag;

	@PostConstruct
	protected void init() throws IOException {

		LOG.info("\n\nInside init() of class IncredibleEscapeCardImpl");
		Resource pageResource = resource;
		List<String> allResourcePath = new LinkedList<>();
		ResourceResolver resourceResolver = null;
		try {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			LOG.debug("initial resource path: {}", resource.getPath());
			ValueMap valueMap = pageResource.adaptTo(ValueMap.class);
			LOG.info("Trying to locate the page path where the component is added... ");

			while (!(valueMap.get("jcr:primaryType", String.class).equals("cq:Page"))) {
				LOG.debug("primary resource type not found as cq:Page. Moving to parent");
				pageResource = pageResource.getParent();
				valueMap = pageResource.adaptTo(ValueMap.class);

				LOG.debug("primarytype : {}", valueMap.get("jcr:primaryType", String.class));
			}
			Resource pageJCRResource = getResourceFromPath(pageResource.getPath() + "/jcr:content");
			ValueMap jcrValueMap = pageJCRResource.adaptTo(ValueMap.class);
			LOG.debug("Page path : {}, sling resource type: {}", pageResource.getPath(),
					jcrValueMap.get("sling:resourceType", String.class));

			if ((jcrValueMap.get("sling:resourceType", String.class)).equals(DESTINATION_RESOURCE_TYPE)) {
				/*
				 * If component is added to specific destination show custom packages
				 */
				LOG.info("Component added to a holiday destination detected");
				rootPageFlag = false;
				allResourcePath = fetchPackagePaths(ROOT_PAGE_PATH, PACKAGES_PAGE_RESOURCE_TYPE);
				/*
				 * Remove the pages where incredible escape category is not similar to the
				 * invoking package page category
				 */
				List<String> otherPages = getSimilarPackages(allResourcePath, pageJCRResource);
				allResourcePath.clear();
				allResourcePath.addAll(otherPages);
			} else if (!(pageResource.getPath().equals(ROOT_PAGE_PATH))) {
				/*
				 * If component is added to one of the child package pages, removing the
				 * resource path of the invoking package page
				 */
				LOG.info("Component added to package page detected");
				rootPageFlag = false;
				Resource currentpage = pageResource.getChild("jcr:content");

				Resource parentPage = pageResource.getParent();
				LOG.debug("Parent page path : {}", parentPage.getPath());

				allResourcePath = fetchPackagePaths(parentPage.getPath(), PACKAGES_PAGE_RESOURCE_TYPE);

				List<String> otherPages = new LinkedList<>();
				for (String path : allResourcePath) {
					if (!path.equals(currentpage.getPath())) {
						otherPages.add(path);
					}
				}

				allResourcePath.clear();
				/*
				 * Remove the pages where incredible escape category is not similar to the
				 * invoking package page category
				 */
				allResourcePath.addAll(getSimilarPackages(otherPages, currentpage));
			} else {
				rootPageFlag = true;
				allResourcePath = fetchPackagePaths(pageResource.getPath(), PACKAGES_PAGE_RESOURCE_TYPE);
			}

			allPackageCards = populateCardsDetails(allResourcePath);
		} catch (Exception e) {
			LOG.error("Exception while building details for Incredible Escpe Cards. {} ", e.getMessage());
		}
		LOG.info("Leaving init() of class IncredibleEscapeCardImpl\n\n");
		sortIncredibleEscapeCard(allPackageCards);
	}

	private Resource getResourceFromPath(String path) throws LoginException {
		ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
		Resource sendResource = resourceResolver.getResource(path);
		return sendResource;
	}

	private LinkedList<String> getSimilarPackages(List<String> otherPages, Resource currentpage) throws LoginException {

		LOG.debug("Inside method: getSimilarPackages()");
		LinkedList<String> similarPackagePaths = new LinkedList<>();
		String packagePageCategory = null;
		String otherPageCategory = null;
		boolean categoryExists = false;
		Iterable<Resource> currentPageRoot = (currentpage.getChild("root")).getChildren();
		LOG.debug("fetching the curent page category:");
		for (Resource child : currentPageRoot) {
			if (child.getResourceType().equals(CATEGORY_COMPONENT_RESOURCE_TYPE)) {
				categoryExists = true;
				ValueMap childValueMap = child.adaptTo(ValueMap.class);
				packagePageCategory = childValueMap.get("incredibleEscape", String.class);
			}
		}

		if (categoryExists) {
			LOG.debug("category found : {}, {}", categoryExists, packagePageCategory);
			for (String path : otherPages) {
				otherPageCategory = getPageCategory(path);
				if ((packagePageCategory != null) && (packagePageCategory.compareTo(otherPageCategory) == 0)) {
					LOG.debug("path selected: {}", path);
					similarPackagePaths.add(path);
				}

			}
		} else {
			LOG.debug("category not found : {}, {}, adding all packages to the page", categoryExists,
					packagePageCategory);
			similarPackagePaths.addAll(otherPages);
		}
		LOG.debug("Leaving method: getSimilarPackages()");
		return similarPackagePaths;
	}

	private String getPageCategory(String pagePath) throws LoginException {

		String pageCategory = null;
		String newPagePath = pagePath + "/root";
		LOG.debug("resource path {}", newPagePath);
		LOG.debug("getting resource");
		ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
		Resource resource = resourceResolver.getResource(newPagePath);
		Iterable<Resource> pageChildren = resource.getChildren();
		for (Resource child : pageChildren) {
			if (child.getResourceType().equals(CATEGORY_COMPONENT_RESOURCE_TYPE)) {
				ValueMap childValueMap = child.adaptTo(ValueMap.class);
				pageCategory = childValueMap.get("incredibleEscape", String.class);
			}
		}
		return pageCategory;
	}

	private List<IncredibleEscapeCard> populateCardsDetails(List<String> allPagePaths)
			throws LoginException, IOException {
		LOG.info("\n\nInside method populateCardsDetails()");

		List<IncredibleEscapeCard> cardsList = new LinkedList<>();
		List<String> packagePaths = null;
		ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
		try {
		for (String pagePath : allPagePaths) {

			IncredibleEscapeCard card = new IncredibleEscapeCard();
			Resource basePageResource = resourceResolver.getResource(pagePath);
			Iterable<Resource> detailsNode = (basePageResource.getChild("root")).getChildren();
			for (Resource child : detailsNode) {

				LOG.debug("Iterating over child: {} with resource type: {}", child, child.getResourceType());

				if (child.getResourceType().equals(PACKAGE_SUMMARY_RESOURCE_TYPE)) {
					LOG.debug("Found package summary component at child");

					// add package summary details to card object
					ValueMap valueMap = child.adaptTo(ValueMap.class);
					card.setIncredibleEscapeTitle(valueMap.get("pageTitle", String.class));
					card.setPackageTitle(card.getIncredibleEscapeTitle());
					card.setPackageDesc(valueMap.get("pageDescription", String.class));
					card.setStartDate(valueMap.get("packageStartDate", String.class));
					card.setEndDate(valueMap.get("packageEndDate", String.class));
					card.setPackageRank(valueMap.get("packageRank", String.class));
				} else if (child.getResourceType().equals(PACKAGE_DESC_RESOURCE_TYPE)) {
					LOG.debug("Found package description component at child");

					// add package desc details to card object
					ValueMap valueMap = child.adaptTo(ValueMap.class);
					card.setPackageShortTitle(valueMap.get("packageDescription", String.class));
					card.setPrice(valueMap.get("price", String.class));
				} else if (child.getResourceType().equals(PACKAGE_INCLUSIONS_RESOURCE_TYPE)) {
					LOG.debug("Found package inclusions component at child");

					// add inclusions details to card object
					card.setInclusions(getInclusionsList(child));
				}
			}

			LOG.info("\n\nGetting the paths of all hotels in the package\n\n");
			packagePaths = fetchPackagePaths(basePageResource.getParent().getPath(), PACKAGES_HOTELS_RESOURCE_TYPE);

			card.setCities(getPackageCities(packagePaths, resourceResolver));
			card.setPackageLink(basePageResource.getParent().getPath());
			card.setImagePath(getImagePath(basePageResource));
			cardsList.add(card);

		}
		} catch (Exception e) {
			LOG.error("Exception found in PopulateCardDetails :: {}", e.getMessage());
		}
		
		return cardsList;
	}

	private String getImagePath(Resource basePageResource) {
		LOG.info("\n\nInside method getImagePath()");

		Iterable<Resource> imageChildren = (basePageResource.getChild("banner_parsys")).getChildren();
		String path = null;
		for (Resource imageChild : imageChildren) {
			LOG.debug("comparing the child resource with banner resource type: {}",
					imageChild.getResourceType().equals(BANNER_CAROUSEL_RESOURCE_TYPE));
			if (imageChild.getResourceType().equals(BANNER_CAROUSEL_RESOURCE_TYPE)) {
				LOG.debug("Found banner carousel component at child");
				if (imageChild.getChild("banners").hasChildren()) {
					ValueMap valueMap = ((imageChild.getChild("banners")).getChild("item0")).adaptTo(ValueMap.class);
					path = valueMap.get("bannerImage", String.class);
				} else {
					LOG.debug("No images found in the banner.");
					path = "#";
				}

			}
		}
		LOG.info("Leaving method getImagePath()\n\n");
		return path;
	}

	private List<Inclusion> getInclusionsList(Resource child) throws IOException {
		LOG.info("Inside method getInclusionsList()");

		List<Inclusion> inclusions = new LinkedList<>();
		ValueMap inclsionMap = child.adaptTo(ValueMap.class);
		String[] inclusionsArray = inclsionMap.get("inclusions", String[].class);
		if (inclusionsArray != null && inclusionsArray.length > 0) {
			for (String jsonStr : inclusionsArray) {
				ObjectMapper mapper = new ObjectMapper();
				LOG.debug("JSON String : {}", jsonStr);
				Inclusion inclusion = mapper.readValue(jsonStr, Inclusion.class);
				inclusions.add(inclusion);
			}
		}
		LOG.info("Leaving method getInclusionsList()");
		return inclusions;
	}

	private String getPackageCities(List<String> packagePaths, ResourceResolver resourceResolver) {
		LOG.info("\n\nInside method getPackageCities()");

		String cities = "";
		for (String paths : packagePaths) {
			Resource packageResource = resourceResolver.getResource(paths);
			Iterable<Resource> detailsNodeforPackage = (packageResource.getChild("root")).getChildren();
			for (Resource child : detailsNodeforPackage) {

				LOG.debug("Iterating over child: {}", child);
				LOG.debug("resource type : {}", child.getResourceType());
				if (child.getResourceType().equals(HOTELS_DETAILS_RESOURCE_TYPE)) {
					LOG.debug("Found hotelDetails component at child: {}", child);
					ValueMap valueMap = child.adaptTo(ValueMap.class);
					cities = cities + valueMap.get("hotelCity", String.class) + " | ";
					break;
				}
			}
		}
		cities = cities.substring(0, cities.length() - 3);
		LOG.debug("Cities String: {}", cities);
		LOG.info("Leaving method getPackageCities()\n\n");
		return cities;
	}

	private List<String> fetchPackagePaths(String pageResourcePath, String resourceType) {

		LOG.info("\n\nInside method: fetchPackagePaths()");
		LOG.info("Root Path: {}", pageResourcePath);
		LOG.info("sling resource type: {}", resourceType);

		List<String> resourcePaths = new LinkedList<>();
		HashMap<String, String> predicateMap = new HashMap<>();
		try {
			predicateMap.put("path", pageResourcePath);
			predicateMap.put("property", "sling:resourceType");
			predicateMap.put("property.value", resourceType);
			predicateMap.put("p.limit", "-1");
			SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
			for (Hit hit : searchResult.getHits()) {
				LOG.debug("Found package resource at : {}", hit.getPath());
				resourcePaths.add(hit.getPath());
			}
		} catch (Exception e) {
			LOG.error("Exception occured searching for resourcePaths {}", e.getMessage());
		}
		LOG.info("Leaving method: fetchPackagePaths()\n\n");
		return resourcePaths;
	}

	@Override
	public List<IncredibleEscapeCard> getAllPackgeCards() {
		return allPackageCards;
	}

	@Override
	public boolean getRootPageFlag() {
		return rootPageFlag;
	}

	private List<IncredibleEscapeCard> sortIncredibleEscapeCard(List<IncredibleEscapeCard> incredibleEscapeCardList) {
		String methodName = "sortIncredibleEscapeCard";
		LOG.trace("Method Entry: " + methodName);
		Collections.sort(incredibleEscapeCardList, new PackageRankComparator());

		return incredibleEscapeCardList;
	}

}
