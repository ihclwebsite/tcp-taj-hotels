package com.ihcl.core.models.serviceamenity;

import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface GroupedServiceAmenitiesFetcher {

	/**
	 * @return
	 */
	List<String> getAllAmenities();

	/**
	 * @return
	 */
	String[] getBedAndBathAmenities();

	/**
	 * @return
	 */
	String[] getEntertainment();

	/**
	 * @return
	 */
	String[] getOtherConveniences();

	/**
	 * @return
	 */
	String[] getWellnessAmenities();

	/**
	 * @return
	 */
	String[] getSuiteFeatures();

	/**
	 * @return
	 */
	String[] getHotelFacilities();

}
