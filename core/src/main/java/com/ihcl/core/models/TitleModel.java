package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class TitleModel extends WCMUsePojo {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	private List<TitleBean> titleList = new ArrayList<>();

	@Override
	public void activate() throws Exception {

		if(getResource().adaptTo(Node.class) != null) {
			Node currentNode = getResource().adaptTo(Node.class);
			if(currentNode != null) {
				NodeIterator ni = currentNode.getNodes();
				while (ni.hasNext()) {
					Node child = ni.nextNode();
					if(child != null) {
						NodeIterator ni2 = child.getNodes();
						setTitleList(ni2);
					}
				}
			}
		}
	}

	private void setTitleList(NodeIterator ni2) {

		try {
			while (ni2.hasNext()) {
				TitleBean tilObj = new TitleBean();
				Node grandChild = ni2.nextNode();
				
				if (grandChild.hasProperty("titleFormItemLabel")) {
					log.info("*** GRAND CHILD NODE PATH IS 1111 " + grandChild.getPath());
					tilObj.setTitleFormItemLabel(grandChild.getProperty("titleFormItemLabel").getString());
				}

				if(tilObj.getTitleFormItemLabel() != null) {
					titleList.add(tilObj);
				}
			}
		}
		catch (Exception e) {
			log.error("Exception while Multifield data {}", e.getMessage(), e);
		}
	}

	public List<TitleBean> getTitleList() {
		
		return titleList;
	}

}
