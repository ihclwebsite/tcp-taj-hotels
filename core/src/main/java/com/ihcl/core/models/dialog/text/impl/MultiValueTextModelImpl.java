package com.ihcl.core.models.dialog.text.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.Value;
import com.ihcl.core.models.dialog.text.MultiValueTextModel;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = MultiValueTextModel.class)
public class MultiValueTextModelImpl implements MultiValueTextModel {

    private static final Logger LOG = LoggerFactory.getLogger(MultiValueTextModelImpl.class);

    @Inject
    private Resource resource;

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private ResourceResolverFactory resolverFactory;

    private String[] values;

    private String[] name;

    String contentPath;

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.trace("Received resource as: " + resource);
            LOG.trace("Received sling http servlet request as: " + request);
            LOG.trace("Received resource resolver factory as: " + resolverFactory);
            ValueMap valueMapName = resource.adaptTo(ValueMap.class);

            String name = valueMapName.get("name", String.class);
            LOG.trace("Received name as: " + name);

            contentPath = request.getAttribute(Value.CONTENTPATH_ATTRIBUTE).toString();
            contentPath.concat("/serviceAmenityPriority");
            // contentPath =
            // "/content/tajhotels/en-in/our-hotels/hotels-in-colombo/taj/taj-samudra-colombo/jcr:content/root/hotel_overview/serviceAmenityPriority";
            LOG.debug("Received content path from request as: " + contentPath);

            ResourceResolver resolver = resolverFactory.getServiceResourceResolver(null);
            Resource contentResource = resolver.getResource(contentPath);
            LOG.debug("Received content resource as: " + contentResource);


            ValueMap contentValueMap = contentResource.adaptTo(ValueMap.class);
            LOG.trace("contentValueMap is :" + contentValueMap);
            values = contentValueMap.get(name, String[].class);
            LOG.debug("Received values as: " + values);


        } catch (LoginException e) {
            LOG.error("An unexpected error occured in " + MultiValueTextModelImpl.class, e);
        }
        LOG.trace("Method Exit: " + methodName);

    }


    @Override
    public String[] getValues() {
        return values;
    }

    @Override
    public String[] getNames() {
        return fetchServiceAmenityFromPath(values);
    }

    /**
     * @return
     */
    private String[] fetchServiceAmenityFromPath(String[] path) {
        String methodName = "fetchServiceAmenityFromPath";
        LOG.trace("Method Entry: " + methodName);

        try {
            ResourceResolver resourceResolver;
            LOG.debug("Attempting to fetch service resolver to fetch name from path");
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resourceResolver as: " + resourceResolver);
            LOG.trace("Content path is : " + contentPath);
            Resource resource = resourceResolver.getResource(contentPath);
            LOG.debug("Received root node under service amenity page path as: " + resource);
            if (resource != null && resource.hasChildren()) {
                LOG.debug("resource is not null: proceeding further");
                String resourceType;
                ValueMap valueMap;
                int i = 0;
                for (Resource rootNodeResource : resource.getChildren()) {
                    LOG.trace("Root node resource is : " + rootNodeResource);
                    resourceType = rootNodeResource.getResourceType();
                    LOG.trace("Resource type is " + resourceType);
                    if (resourceType.equals("tajhotels/components/content/service-amenity")) {
                        LOG.trace("Resource type is matching");
                        valueMap = resource.getValueMap();
                        LOG.trace("Value map is :" + valueMap);
                        if (valueMap.containsKey("name") && path[i].equals(resource.getPath())) {
                            LOG.trace("value map key is name and path is matching");
                            name[i] = valueMap.get("name", String.class);
                            LOG.trace("Name of the amenity and path is : " + name[i] + " path : " + path[i]);
                        }
                    }
                }
            }
        } catch (LoginException e) {
            LOG.error("An error occured while logging in.", e);
        }
        LOG.trace("Method Exit: " + methodName);
        return name;
    }
}
