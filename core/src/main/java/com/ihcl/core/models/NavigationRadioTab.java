package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

/**
 * Class to get multiple items from radio navigation tab.
 * 
 * @author user
 *
 */
public class NavigationRadioTab extends WCMUsePojo {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	List<NavigationRadioTabBean> multiTabList = new ArrayList<NavigationRadioTabBean>();

	@Override
	public void activate() throws Exception {

		Node currentNode = getResource().adaptTo(Node.class);
		NodeIterator ni = currentNode.getNodes();

		while (ni.hasNext()) {

			Node child = ni.nextNode();
			if (child.getPath().contains("galleryOption")) {
				NodeIterator ni2 = child.getNodes();
				log.info("*** CHILD PATH NODES " + ni2);
				setMultiFieldItems(ni2);
			}
		}
	}

	private void setMultiFieldItems(NodeIterator ni2) {

		try {

			String radioButtonText;
			String format;

			while (ni2.hasNext()) {

				Node grandChild = ni2.nextNode();
				
				log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
				if (null != grandChild.getProperty("radioButtonText")) {
					NavigationRadioTabBean tabObj = new NavigationRadioTabBean();
					log.info("*** RADIO BUTTON TEXT AFTER NULL CHECK " + grandChild.getProperty("radioButtonText"));
					radioButtonText = grandChild.getProperty("radioButtonText").getString();
					format = grandChild.getProperty("format").getString();
					tabObj.setRadioButtonText(radioButtonText);
					tabObj.setFormat(format);
					multiTabList.add(tabObj);
				}
			}
		}

		catch (Exception e) {
			log.error("Exception while Multifield data {}", e.getMessage(), e);
		}

	}

	public List<NavigationRadioTabBean> getMultiFieldTabList() {
		return multiTabList;
	}

}
