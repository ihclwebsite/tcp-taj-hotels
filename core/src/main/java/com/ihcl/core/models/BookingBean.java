package com.ihcl.core.models;

public class BookingBean {

	private String billerFormItemLabel;
	private String billerFormItemField;
	private String guestFormItemLabel;
	private String guestFormItemField;
	public String getBillerFormItemLabel() {
		return billerFormItemLabel;
	}
	public void setBillerFormItemLabel(String billerFormItemLabel) {
		this.billerFormItemLabel = billerFormItemLabel;
	}
	public String getBillerFormItemField() {
		return billerFormItemField;
	}
	public void setBillerFormItemField(String billerFormItemField) {
		this.billerFormItemField = billerFormItemField;
	}
	public String getGuestFormItemLabel() {
		return guestFormItemLabel;
	}
	public void setGuestFormItemLabel(String guestFormItemLabel) {
		this.guestFormItemLabel = guestFormItemLabel;
	}
	public String getGuestFormItemField() {
		return guestFormItemField;
	}
	public void setGuestFormItemField(String guestFormItemField) {
		this.guestFormItemField = guestFormItemField;
	}
	
	

}
