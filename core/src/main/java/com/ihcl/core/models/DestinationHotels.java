/**
 *
 */
package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.ihcl.core.hotels.BrandDetailsBean;
import com.ihcl.core.hotels.DestinationHotelBean;
import com.ihcl.core.hotels.DestinationHotelMapBean;
import com.ihcl.core.hotels.HotelDetailsBean;
import com.ihcl.core.hotels.RestaurantDetailsBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;

/**
 * <pre>
 * DestinationHotels Class
 * </pre>
 *
 * .
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :03-Jun-2019
 * @ClassName : DestinationHotels.java
 * @Description : com.ihcl.core.models -DestinationHotels.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since              author               description
 *  ===========     ===================   =========================
 *  03-Jun-2019     Neha Priyanka                 Create
 *  02-Dec-2019     Subharun Mukherjee      Modified (Restaurants)
 *
 *               </pre>
 */
@Model(adaptables = SlingHttpServletRequest.class,
        adapters = DestinationHotels.class)
public class DestinationHotels {

    private static final Logger LOG = LoggerFactory.getLogger(DestinationHotels.class);

    private static final String WEBSITE = "website";

    private static final String OTHERS = "others";


    @Inject
    @Optional
    private String currentPagePath;

    @Inject
    @Optional
    private String currentPageName;

    @Inject
    @Optional
    private String siteConfigHomePath;

    @Inject
    @Optional
    private String otherWebsitePaths;

    @Inject
    @Optional
    private Page currentPage;

    @Inject
    private TajhotelsHotelsBeanService hotelsBeanService;

    @Inject
    @Optional
    private Node currentNode;

    private Map<String, List<HotelDetailsBean>> hotelsMap;

    private Map<String, List<RestaurantDetailsBean>> restaurantMap;

    @Inject
    @Via("resource")
    @Optional
    private String isAuthorable;

    @Inject
    @Via("resource")
    @Optional
    private String isGlobalDining;

    @Inject
    @Via("resource")
    @Optional
    private String isDestinationDining;


    /**
     * Gets the hotels map.
     *
     * @return the hotels map
     */
    public Map<String, List<HotelDetailsBean>> getHotelsMap() {
        hotelsMap = new HashMap<>();
        try {
            Map<String, DestinationHotelMapBean> websitesHotelsMap = hotelsBeanService.getWebsiteSpecificHotelsBean()
                    .getWebsiteHotelsMap();
            if (Boolean.parseBoolean(isAuthorable)) {
                List<String> websitePathList = new ArrayList<>();
                List<String> othersPathList = new ArrayList<>();
                if (currentNode.hasNode(WEBSITE)) {
                    websitePathList = getHotelsList(WEBSITE);
                }
                if (currentNode.hasNode(OTHERS)) {
                    othersPathList = getHotelsList(OTHERS);
                }
                hotelsMap = getAuthorableHotelsDetails(websitesHotelsMap, websitePathList, othersPathList);
            } else {
                DestinationHotelMapBean websiteDestinations = websitesHotelsMap.get(siteConfigHomePath);
                Map<String, DestinationHotelMapBean> websiteSpecificBean = new HashMap<>();
                websiteSpecificBean.put(siteConfigHomePath, websiteDestinations);
                String otherPaths[] = null;
                String otherPath = null;
                if (null != otherWebsitePaths) {
                    if (otherWebsitePaths.contains(",")) {
                        otherPaths = otherWebsitePaths.split(",");
                        for (String path : otherPaths) {
                            DestinationHotelMapBean otherDestinations = websitesHotelsMap.get(path);
                            websiteSpecificBean.put(path, otherDestinations);
                        }
                    } else {
                        otherPath = otherWebsitePaths;
                        DestinationHotelMapBean otherDestinations = websitesHotelsMap.get(otherPath);
                        websiteSpecificBean.put(otherPath, otherDestinations);
                    }
                }
                hotelsMap = getDynamicHotelsListByDestination(websiteSpecificBean);
            }
        } catch (Exception e) {
            LOG.error("Exception while getting the hotels List :: {}", e.getMessage());
            LOG.debug("Exception while getting the hotels List :: {}", e);
        }
        return hotelsMap;
    }

    public Map<String, List<RestaurantDetailsBean>> getRestaurantMap() {
        restaurantMap = new HashMap<>();
        List<RestaurantDetailsBean> websiteRestaurantList = new ArrayList<>();
        List<RestaurantDetailsBean> othersRestaurantList = new ArrayList<>();
        LOG.debug("inside restaurant map :: ");
        try {
            Map<String, DestinationHotelMapBean> websitesHotelsMap = hotelsBeanService.getWebsiteSpecificHotelsBean()
                    .getWebsiteHotelsMap();
            Iterator<Entry<String, DestinationHotelMapBean>> websiteHotels = websitesHotelsMap.entrySet().iterator();
            while (websiteHotels.hasNext()) {
                Map.Entry<String, DestinationHotelMapBean> entry = websiteHotels.next();
                String websitePath = entry.getKey();
                if (Boolean.parseBoolean(isGlobalDining)) {
                    Iterator<Entry<String, DestinationHotelBean>> destinationMap = entry.getValue().getDestinationMap()
                            .entrySet().iterator();
                    while (destinationMap.hasNext()) {
                        Map.Entry<String, DestinationHotelBean> destEntry = destinationMap.next();
                        DestinationHotelBean destinationBean = destEntry.getValue();
                        if (null != destinationBean) {
                            Map<String, BrandDetailsBean> hotelsInDestination = destinationBean.getBrandHotels();
                            if (null != hotelsInDestination) {
                                createRestaurantMap(hotelsInDestination, websitePath, websiteRestaurantList,
                                        othersRestaurantList);
                            }
                        }
                    }
                } else if (Boolean.parseBoolean(isDestinationDining)) {
                    DestinationHotelBean destinationBean = entry.getValue().getDestinationMap()
                            .get(currentPage.getParent().getName());
                    if (null != destinationBean) {
                        Map<String, BrandDetailsBean> hotelsInDestination = destinationBean.getBrandHotels();
                        if (null != hotelsInDestination) {
                            createRestaurantMap(hotelsInDestination, websitePath, websiteRestaurantList,
                                    othersRestaurantList);
                        }
                    }
                } else {
                    DestinationHotelBean destinationBean = entry.getValue().getDestinationMap()
                            .get(currentPage.getParent(3).getName());
                    if (null != destinationBean) {
                        Map<String, BrandDetailsBean> hotelsInDestination = destinationBean.getBrandHotels();
                        if (null != hotelsInDestination) {
                            for (Map.Entry<String, BrandDetailsBean> brands : hotelsInDestination.entrySet()) {
                                List<HotelDetailsBean> hotelsBeanList = brands.getValue().getHotelsBean()
                                        .getHotelsList();
                                for (HotelDetailsBean bean : hotelsBeanList) {
                                    if (bean.getHotelPath().contains(currentPage.getParent().getName())) {
                                        List<RestaurantDetailsBean> restaurantList = bean.getRestaurantDetails();
                                        websiteRestaurantList.addAll(restaurantList);
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            restaurantMap.put(WEBSITE, websiteRestaurantList);
            restaurantMap.put(OTHERS, othersRestaurantList);
        } catch (Exception e) {
            LOG.error("Exception while getting the restaurants List :: {}", e.getMessage());
            LOG.debug("Exception while getting the restaurants List :: {}", e);
        }
        Gson gson = new Gson();
        String obj = gson.toJson(restaurantMap);
        LOG.debug("restaurant Map :  {}", obj);
        return restaurantMap;
    }


    /**
     * @param othersRestaurantList
     * @param websiteRestaurantList
     * @param websitePath
     * @param hotelsInDestination
     *
     */
    private void createRestaurantMap(Map<String, BrandDetailsBean> hotelsInDestination, String websitePath,
            List<RestaurantDetailsBean> websiteRestaurantList, List<RestaurantDetailsBean> othersRestaurantList) {
        for (Map.Entry<String, BrandDetailsBean> brands : hotelsInDestination.entrySet()) {
            List<HotelDetailsBean> hotelsBeanList = brands.getValue().getHotelsBean().getHotelsList();
            for (HotelDetailsBean bean : hotelsBeanList) {
                List<RestaurantDetailsBean> restaurantList = bean.getRestaurantDetails();
                if (currentPagePath.contains(websitePath)) {
                    websiteRestaurantList.addAll(restaurantList);
                } else {
                    othersRestaurantList.addAll(restaurantList);
                }
            }
        }

    }

    /**
     * Gets the hotels list.
     *
     * @param nodeName
     *            the node name
     * @return the hotels list
     */
    private List<String> getHotelsList(String nodeName) {
        List<String> hotelList = new ArrayList<>();
        try {
            Node node = currentNode.getNode(nodeName);
            NodeIterator nodeItr = node.getNodes();
            while (nodeItr.hasNext()) {
                Node hotelNode = nodeItr.nextNode();
                String hotelPath = hotelNode.getProperty("hotelsPath").getValue().getString();
                hotelList.add(hotelPath);
            }
        } catch (RepositoryException e) {
            LOG.error("Exception in getAuthourableHotelsMap :: {}", e.getMessage());
            LOG.debug("Exception in getAuthourableHotelsMap :: {}", e);
        }
        return hotelList;
    }

    /**
     * Gets the dynamic hotels list by destination.
     *
     * @param websitesHotelsMap
     *            the websites hotels map
     * @return the dynamic hotels list by destination
     */
    private Map<String, List<HotelDetailsBean>> getDynamicHotelsListByDestination(
            Map<String, DestinationHotelMapBean> websitesHotelsMap) {
        List<HotelDetailsBean> hotelsList = new ArrayList<>();
        List<HotelDetailsBean> othersHotelsList = new ArrayList<>();
        try {
            Iterator<Entry<String, DestinationHotelMapBean>> websiteHotels = websitesHotelsMap.entrySet().iterator();
            while (websiteHotels.hasNext()) {
                Map.Entry<String, DestinationHotelMapBean> entry = websiteHotels.next();
                String websitePath = entry.getKey();

                if (null != entry.getValue()) {
                    DestinationHotelBean destinationBean = entry.getValue().getDestinationMap().get(currentPageName);
                    if (null != destinationBean) {
                        Map<String, BrandDetailsBean> hotelsInDestination = destinationBean.getBrandHotels();
                        if (null != hotelsInDestination) {
                            for (Map.Entry<String, BrandDetailsBean> brands : hotelsInDestination.entrySet()) {
                                List<HotelDetailsBean> hotelsBeanList = brands.getValue().getHotelsBean()
                                        .getHotelsList();
                                if (currentPagePath.contains(websitePath)) {
                                    hotelsList.addAll(hotelsBeanList);
                                } else {
                                    othersHotelsList.addAll(hotelsBeanList);
                                }
                            }
                        }
                    }

                }
            }
            hotelsMap.put(WEBSITE, hotelsList);
            hotelsMap.put(OTHERS, othersHotelsList);
            return hotelsMap;
        } catch (Exception e) {
            LOG.error("Exception while getting the dynamic hotels List :: {}", e.getMessage());
            LOG.debug("Exception while getting the dynamic hotels List :: {}", e);
        }
        return null;
    }


    /**
     * Gets the authorable hotels details.
     *
     * @param websitesHotelsMap
     *            the websites hotels map
     * @param hotels
     *            the hotels
     * @param otherHotels
     *            the other hotels
     * @return the authorable hotels details
     */
    private Map<String, List<HotelDetailsBean>> getAuthorableHotelsDetails(
            Map<String, DestinationHotelMapBean> websitesHotelsMap, List<String> hotels, List<String> otherHotels) {
        if (null != hotels) {
            Map<String, DestinationHotelBean> destinations = websitesHotelsMap.get(siteConfigHomePath)
                    .getDestinationMap();
            List<HotelDetailsBean> hotelsList = getHotelsList(destinations, hotels);
            hotelsMap.put(WEBSITE, hotelsList);
        }
        if (null != otherHotels) {
            List<HotelDetailsBean> hotelsList = new ArrayList<>();
            for (String hotelPath : otherHotels) {
                String splittedPaths[] = hotelPath.split("/our-hotels/");
                Map<String, DestinationHotelBean> destinations = websitesHotelsMap.get(splittedPaths[0])
                        .getDestinationMap();
                hotelsList.add(getListByPath(destinations, splittedPaths[1], hotelPath));
            }
            hotelsMap.put(OTHERS, hotelsList);
        }
        return hotelsMap;
    }

    /**
     * Gets the hotels list.
     *
     * @param destinations
     *            the destinations
     * @param hotels
     *            the hotels
     * @return the hotels list
     */
    private List<HotelDetailsBean> getHotelsList(Map<String, DestinationHotelBean> destinations, List<String> hotels) {
        List<HotelDetailsBean> hotelsList = new ArrayList<>();
        for (String hotelPath : hotels) {
            String splittedPagePath = hotelPath.split("/our-hotels/")[1];
            hotelsList.add(getListByPath(destinations, splittedPagePath, hotelPath));
        }
        return hotelsList;

    }

    /**
     * Gets the list by path.
     *
     * @param destinations
     *            the destinations
     * @param splittedPagePath
     *            the splitted page path
     * @param hotelPath
     *            the hotel path
     * @return the list by path
     */
    private HotelDetailsBean getListByPath(Map<String, DestinationHotelBean> destinations, String splittedPagePath,
            String hotelPath) {
        String destinationPaths[] = splittedPagePath.split("/");
        DestinationHotelBean destinationsMap = destinations.get(destinationPaths[0]);
        if (null != destinationsMap && StringUtils.isNotBlank(destinationPaths[1])) {
            BrandDetailsBean brandHotels = destinationsMap.getBrandHotels().get(destinationPaths[1]);
            List<HotelDetailsBean> brandHotelsList = brandHotels.getHotelsBean().getHotelsList();
            for (HotelDetailsBean hotelDetail : brandHotelsList) {
                if (hotelPath.equals(hotelDetail.getHotelPath())) {
                    return hotelDetail;
                }
            }

        }
        return null;
    }


}
