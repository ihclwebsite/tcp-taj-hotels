package com.ihcl.core.models;
 
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.settings.SlingSettingsService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.offers.OfferImpl;
 
@Model(adaptables=Resource.class)
public class HotelOverview {
 
    @Inject
    private SlingSettingsService settings;
    
    @Inject
    private Resource hotelPath;
    
    @Inject
    private ResourceResolver resourceResolver;    
 
    @Inject @Named("sling:resourceType") @Default(values="No resourceType")
    protected String resourceType;
 
    private static final Logger LOG = LoggerFactory.getLogger(OfferImpl.class);
    
    private String hotelName;
    
    private String overviewPath;

	private String pageTitle;

	private String hotelStdCode;

	private String hotelPhoneNumber;

	private String countryName;

	private String hotelCity;

	private String hotelLocation;

	private String hotelLongDesc;
	
	private ArrayList<String> hotelPolicy = new ArrayList<String>();
	
	private HashMap<String, String> hotelEssentials = new HashMap<String, String>();
	
	private HashMap<String, String> hotelSignatureFeatures = new HashMap<String, String>();

	private String galleryImgPath;

	private String hotelEmail;

	private String hotelLatitude;

	private String hotelLongitude;

//	private Value[] hotelFacilities;
	
	private ArrayList<String> hotelFacilities = new ArrayList<String>();

	private ArrayList<String> otherConveniences = new ArrayList<String>();

	private ArrayList<String> wellnessAmenities = new ArrayList<String>();
	
	private ArrayList<org.json.simple.JSONObject> hotelAwards = new ArrayList<org.json.simple.JSONObject>();

	private String amenitiesName;

	private String subtitle;
        
    @PostConstruct
    public void init() throws PathNotFoundException, RepositoryException, JSONException {
        getDataFromNode();
    }
    

	private void getDataFromNode() throws PathNotFoundException, RepositoryException, JSONException {
//    	overviewPath = hotelPath.getPath();
    	Resource hotelContentPath = resourceResolver.getResource(hotelPath.getPath() + "/jcr:content");
    	ValueMap hotelProperties = hotelContentPath.getValueMap();
    	hotelName = hotelProperties.get("jcr:title", String.class);
    	pageTitle = hotelProperties.get("pageTitle", String.class);
    	hotelStdCode = hotelProperties.get("hotelStdCode", String.class);
    	hotelPhoneNumber = hotelProperties.get("hotelPhoneNumber", String.class);
    	countryName = hotelProperties.get("countryName", String.class);
    	hotelEmail = hotelProperties.get("hotelEmail", String.class);
    	hotelLatitude = hotelProperties.get("hotelLatitude", String.class);
    	hotelLongitude = hotelProperties.get("hotelLongitude", String.class);
    	if(hotelProperties.get("subtitle") != null) {
    		subtitle = hotelProperties.get("subtitle", String.class);
		}
    	
    	Node parent = hotelContentPath.adaptTo(Node.class);
    	LOG.trace("parent : {}", parent);
    	Node root = parent.getNode("root");
    	LOG.trace("root : {}", root);
    	Node overview = root.getNode("hotel_overview");
    	LOG.trace("overview : {}", overview);
    	if(overview.hasProperty("hotelCity"))
    	hotelCity = overview.getProperty("hotelCity").getValue().getString();
    	if(overview.hasProperty("hotelLocation"))
    	hotelLocation = overview.getProperty("hotelLocation").getValue().getString();
    	LOG.trace("hotelLongDesc : {}", overview.getProperty("hotelLongDesc"));
    	LOG.trace("hotelLongDesc : {}", overview.getProperty("hotelLongDesc").getValue().getString());
    	if(overview.hasProperty("hotelLongDesc"))
    	hotelLongDesc = overview.getProperty("hotelLongDesc").getValue().getString();
    	LOG.trace("Title of Page : {}", hotelName);
    	String policy = "";
    	Node hotelPolicyNode = overview.getNode("hotelPolicies");
    	LOG.trace("hotelPolicyNode : {}", hotelPolicyNode);
    	if(hotelPolicyNode.hasNodes()) {
			NodeIterator hotelPolicyNodeIterator = hotelPolicyNode.getNodes();
			LOG.trace("hotelPolicyNodeIterator : {}", hotelPolicyNodeIterator);
			while (hotelPolicyNodeIterator.hasNext()) {
        		Node policyNode = hotelPolicyNodeIterator.nextNode();
        		LOG.trace("policyNode : {}", policyNode);
        		LOG.trace("policy : {}", policy);
        		if(policyNode.hasProperty("hotelPoliciesDescription"))
	        		{
		        		policy = policyNode.getProperty("hotelPoliciesDescription").getValue().getString();
		        		LOG.trace("policy : {}", policy);
		        		LOG.trace("hotelPolicy : {}", hotelPolicy);
		        		hotelPolicy.add(policy);
		        		LOG.trace("hotelPolicy : {}", hotelPolicy);
	        		}	
			}
    	}
    	if(overview.hasNode("galleryImages")) {
	    	Node galleryImgNode = overview.getNode("galleryImages");
	    	if(galleryImgNode.hasProperty("parentPath")) {
		    	LOG.trace("galleryImgNode.getProperty : {}", galleryImgNode.getProperty("parentPath"));
		    	galleryImgPath = galleryImgNode.getProperty("parentPath").getValue().getString();
		    	LOG.trace("galleryImgPath : {}", galleryImgPath);
	    	}
    	}
    	Node hotelEssentialsNode = overview.getNode("hotelEssentials");
    	LOG.trace("hotelEssentialsNode : {}", hotelEssentialsNode);
    	if(hotelEssentialsNode.hasNodes()) {
    		NodeIterator hotelEssentialsNodeIterator = hotelEssentialsNode.getNodes();
    		while (hotelEssentialsNodeIterator.hasNext()) {
    	    	String key = "";
    	    	String value = "";
    			Node hotelEssential = hotelEssentialsNodeIterator.nextNode();
    			if(hotelEssential.hasProperty("hotelEssentialDesc")) {
	    			key = hotelEssential.getProperty("hotelEssentialDesc").getValue().getString();
	    			if(hotelEssential.hasProperty("pathToDownloadPdfFrom")) {
		    			if(hotelEssential.getProperty("pathToDownloadPdfFrom") != null) {
		    				value = hotelEssential.getProperty("pathToDownloadPdfFrom").getValue().getString();
		    			}
	    			}
    			}
    			hotelEssentials.put(key, value);
    			LOG.trace("hotelEssentials : {}", hotelEssentials);
    		}
    	}
    	Node hotelFeaturesNode = overview.getNode("hotelSignatureFeatures");
    	if(hotelFeaturesNode.hasNodes()) {
    		NodeIterator hotelFeaturesNodeIterator = hotelFeaturesNode.getNodes();
    		while (hotelFeaturesNodeIterator.hasNext()) {
    	    	String key = "";
    	    	String value = "";
    			Node hotelFeatures = hotelFeaturesNodeIterator.nextNode();
    			key = hotelFeatures.getProperty("type").getValue().getString();
    			value = hotelFeatures.getProperty("signatureFeatureValue").getValue().getString();
    			hotelSignatureFeatures.put(key, value);
    			LOG.trace("hotelSignatureFeatures : {}", hotelSignatureFeatures);
    		}
    	}
    	if(overview.hasNode("serviceAmenities")) {
	    	Node hotelAmenitiesNode = overview.getNode("serviceAmenities");
	    	if(hotelAmenitiesNode.hasProperty("hotelFacilities")) {
	    		Property hotelFacilitiesProperty = hotelAmenitiesNode.getProperty("hotelFacilities");
		    	if (hotelFacilitiesProperty.isMultiple()) {
		            for (Value propertyValue : hotelFacilitiesProperty.getValues()) {
		            	Resource amenitiesResource = resourceResolver.getResource(propertyValue.getString());
		            	ValueMap amenitiesProperties = amenitiesResource.getValueMap();
		            	amenitiesName = amenitiesProperties.get("name", String.class);
		            	hotelFacilities.add(amenitiesName);
		            }
		        }
	    	}
		    if(hotelAmenitiesNode.hasProperty("otherConveniences")) {
		    	Property otherConveniencesProperty = hotelAmenitiesNode.getProperty("otherConveniences");
		    	if (otherConveniencesProperty.isMultiple()) {
		            for (Value propertyValue : otherConveniencesProperty.getValues()) {
		            	Resource amenitiesResource = resourceResolver.getResource(propertyValue.getString());
		            	ValueMap amenitiesProperties = amenitiesResource.getValueMap();
		            	amenitiesName = amenitiesProperties.get("name", String.class);
		            	otherConveniences.add(amenitiesName);
		            }
		        }
		    }
		    if(hotelAmenitiesNode.hasProperty("wellnessAmenities")) {
		    	Property wellnessAmenitiesProperty = hotelAmenitiesNode.getProperty("wellnessAmenities");
		    	if (wellnessAmenitiesProperty.isMultiple()) {
		            for (Value propertyValue : wellnessAmenitiesProperty.getValues()) {
		            	Resource amenitiesResource = resourceResolver.getResource(propertyValue.getString());
		            	ValueMap amenitiesProperties = amenitiesResource.getValueMap();
		            	amenitiesName = amenitiesProperties.get("name", String.class);
		            	wellnessAmenities.add(amenitiesName);
		            }
		        }
		    }
	    	LOG.trace("hotelFacilities : {}", hotelFacilities);
	    	LOG.trace("otherConveniences : {}", otherConveniences);
	    	LOG.trace("wellnessAmenities : {}", wellnessAmenities);
    	}
    	if(root.hasNode("hotelawards")) {
	    	Node hotelAwardsNode = root.getNode("hotelawards");
	    	Node awardsNode = hotelAwardsNode.getNode("awards");
	    	if(awardsNode.hasNodes()) {
	    		NodeIterator awardsNodeIterator = awardsNode.getNodes();
	    		while (awardsNodeIterator.hasNext()) {
	    	    	org.json.simple.JSONObject obj=new org.json.simple.JSONObject();
	    	    	String awardDesc = "";
	    	    	String awardImagePath = "";
	    	    	String awardTitle = "";
	    			Node award = awardsNodeIterator.nextNode();
	    			if(award.hasProperty("awardDesc")) {
	    				awardDesc = award.getProperty("awardDesc").getValue().getString();
	    			}if(award.hasProperty("awardImagePath")) {
	    				awardImagePath = award.getProperty("awardImagePath").getValue().getString();
	    			}
	    			if(award.hasProperty("awardTitle")) {
	    				awardTitle = award.getProperty("awardTitle").getValue().getString();
	    			}
	    			obj.put("awardTitle",awardTitle);
	    			obj.put("awardDesc",awardDesc);
	    			obj.put("awardImagePath",awardImagePath);
	    			hotelAwards.add(obj);
	    			LOG.trace("hotelAwards : {}", hotelAwards);
	    		}
	    	}
    	}
    }
    
    public ArrayList<org.json.simple.JSONObject> getHotelAwards() {
		return hotelAwards;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public HashMap<String, String> getHotelSignatureFeatures() {
		return hotelSignatureFeatures;
	}

	public String getHotelEmail() {
		return hotelEmail;
	}

	public String getHotelLatitude() {
		return hotelLatitude;
	}

	public String getHotelLongitude() {
		return hotelLongitude;
	}

	public HashMap<String, String> getHotelEssentials() {
		return hotelEssentials;
	}

	public String getGalleryImgPath() {
		return galleryImgPath;
	}

	public String getHotelCity() {
		return hotelCity;
	}

	public ArrayList<String> getHotelFacilities() {
		return hotelFacilities;
	}

	public ArrayList<String> getOtherConveniences() {
		return otherConveniences;
	}

	public ArrayList<String> getWellnessAmenities() {
		return wellnessAmenities;
	}
	
	public String getHotelLocation() {
		return hotelLocation;
	}

	public String getHotelLongDesc() {
		return hotelLongDesc;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public String getHotelStdCode() {
		return hotelStdCode;
	}

	public String getHotelPhoneNumber() {
		return hotelPhoneNumber;
	}

	public String getCountryName() {
		return countryName;
	}

	public String getHotelName() {
		return hotelName;
	}
	
    public ArrayList<String> getHotelPolicy() {
		return hotelPolicy;
	}
}