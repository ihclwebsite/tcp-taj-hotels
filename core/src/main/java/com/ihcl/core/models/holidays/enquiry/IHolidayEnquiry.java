package com.ihcl.core.models.holidays.enquiry;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface IHolidayEnquiry {

	HolidayEnquiryModel getFormFields();
}
