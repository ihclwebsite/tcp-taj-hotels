package com.ihcl.core.models.holidays.hotel;

public class UserSellingPoints {
	
	private String signatureFeaturePoint;
	
	private String signatureFeatureIcon;

	public String getSignatureFeaturePoint() {
		return signatureFeaturePoint;
	}

	public String getSignatureFeatureIcon() {
		return signatureFeatureIcon;
	}

	public void setSignatureFeaturePoint(String signatureFeaturePoint) {
		this.signatureFeaturePoint = signatureFeaturePoint;
	}

	public void setSignatureFeatureIcon(String signatureFeatureIcon) {
		this.signatureFeatureIcon = signatureFeatureIcon;
	}
	
	

}
