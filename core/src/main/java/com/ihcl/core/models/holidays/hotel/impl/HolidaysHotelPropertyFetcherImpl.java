package com.ihcl.core.models.holidays.hotel.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.models.holidays.hotel.HolidayPackage;
import com.ihcl.core.models.holidays.hotel.HolidaysHotelPropertyFetcher;
import com.ihcl.core.models.holidays.hotel.HotelDetails;
import com.ihcl.core.models.holidays.hotel.Inclusion;
import com.ihcl.core.models.holidays.hotel.UserSellingPoints;
import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = HolidaysHotelPropertyFetcher.class)
public class HolidaysHotelPropertyFetcherImpl implements HolidaysHotelPropertyFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(HolidaysHotelPropertyFetcherImpl.class);

    private static final String CRXconstantForHotelDetailsComponent = "tajhotels/components/content/holidays-destination/holidays-hotel-details";

    private static final String CRXconstantForPackageDetailsComponent = "tajhotels/components/content/holidays-destination/holidays-hotel-package-details";

    private static String RESOURCE_NAME_HOTEL_BANNER = "bannerImage";

    private HotelDetails hotelDetails = new HotelDetails();

    private List<HolidayPackage> packageLs = new ArrayList<HolidayPackage>();

    private Map<String, Set<String>> destinationTags = new HashMap<String, Set<String>>();

    private Set<String> experiencesTags = new HashSet<String>();

    private Set<String> themesTags = new HashSet<String>();

    @Inject
    @Default(values = "")
    private String path;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private SearchProvider searchProvider;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @PostConstruct
    public void activate() throws LoginException {
        String methodName = "activate";
        long startTime = new Date().getTime();
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received path as: " + path);
        LOG.debug("Received resource resolver factory as: " + resourceResolverFactory);

        buildProperties();

        /*
         * JSONObject jsonObj = new JSONObject(hotelDetails); LOG.trace("HotelDetails obj :: " + jsonObj.toString());
         */

        long endTime = new Date().getTime();
        LOG.debug("Total Time elapsed : " + (endTime - startTime));
        LOG.trace("Method Exit: " + methodName);
    }

    private void buildProperties() {
        String methodName = "buildProperties";
        ResourceResolver resolver = null;
        try {
            LOG.trace("Method Entry: " + methodName);
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver as: " + resolver);
            Resource hotelPageResource = resolver.getResource(path);
            LOG.debug("Received hotel page resource as: " + hotelPageResource);
            String pathForCountry = (hotelPageResource.getParent()).getParent().getPath() + "/jcr:content";
            LOG.debug("custom path for country : {}", pathForCountry);
            Resource parentResource = resolver.getResource(pathForCountry);
            String hotelCountry = getCountryFromTags(parentResource);
            getOffersTagsList(parentResource, hotelPageResource);

            fetchHotelPropertiesFrom(hotelPageResource);
            hotelDetails.setCountryName(hotelCountry);
            hotelDetails.setDestinationTags(destinationTags);
            hotelDetails.setExperiencesTags(experiencesTags);
            hotelDetails.setThemesTags(themesTags);

        } catch (LoginException e) {
            LOG.error("An error occured while building properties for hotel at the sepcified path", e);
        } finally {
            if (resolver != null && resolver.isLive()) {
                resolver.close();
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void fetchHotelPropertiesFrom(Resource hotelPageResource) {
        String methodName = "fetchHotelPropertiesFrom";
        LOG.trace("Method Entry: " + methodName);
        Resource hotelDetailsComponentResource = null;
        LOG.debug("Attempting to iterate over children of: " + hotelPageResource);
        Iterable<Resource> children = hotelPageResource.getChildren();
        for (Resource child : children) {
            LOG.debug("Iterating over child: " + child);
            if (child.getResourceType().equals(CRXconstantForHotelDetailsComponent)) {
                LOG.debug("Found hotelDetails component at child: " + child);
                hotelDetailsComponentResource = child;
                buildHotelProperties(hotelDetailsComponentResource);
                fetchHoildayPackageProperties(path);
                if (packageLs.isEmpty()) {
                    hotelDetails.setPackages(packageLs);
                } else {
                    hotelDetails.setPackages(sortHolidayPackages(packageLs));
                }

                break;
            }
            if (hotelDetailsComponentResource == null) {
                fetchHotelPropertiesFrom(child);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void buildHotelProperties(Resource hotelDetailsComponentResource) {
        String methodName = "buildHotelProperties";
        LOG.trace("Method Entry: " + methodName);
        ResourceResolver resourceResolver = null;
        try {
            hotelDetails = new HotelDetails();
            ValueMap valueMap = hotelDetailsComponentResource.adaptTo(ValueMap.class);

            String hotelPath = getProperty(valueMap, "hotelPath", String.class, "");
            LOG.trace("buildHotelProperties :: Received hotelPath from JCR properties as " + hotelPath);
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver as: " + resourceResolver);

            Resource hotelJCRResource = resourceResolver.getResource(hotelPath + "/jcr:content");
            Resource hotelPageResource = resourceResolver.getResource(hotelPath + "/jcr:content");
            LOG.debug("Hotel resource from the path -" + hotelPath + " is " + hotelJCRResource);
            ParticipatingHotel hotelModel;
            hotelModel = hotelJCRResource.adaptTo(ParticipatingHotel.class);

            hotelDetails.setHotelPath(hotelPath);
            hotelDetails.setHotelName(hotelModel.getHotelName());
            if (hotelPath.contains("/ama/")) {
                hotelDetails.setHotelRoomsPath(hotelPath + "/accommodations/");
            } else {
                hotelDetails.setHotelRoomsPath(hotelPath + "/rooms-and-suites");
            }

            Resource hotelOverviewComponentResrouce = hotelPageResource.getResourceResolver()
                    .getResource(hotelPath + "/jcr:content/root/hotel_overview");
            if (null != hotelOverviewComponentResrouce) {
                ValueMap hotelOverviewValueMap = hotelOverviewComponentResrouce.adaptTo(ValueMap.class);
                hotelDetails
                        .setHotelDescription(getProperty(hotelOverviewValueMap, "hotelShortDesc", String.class, ""));
            } else {
                Resource amaTextResource = hotelPageResource.getResourceResolver()
                        .getResource(hotelPath + "/jcr:content/root/ama_text");
                if (null != amaTextResource) {
                    ValueMap amaTextValueMap = amaTextResource.adaptTo(ValueMap.class);
                    hotelDetails.setHotelDescription(getProperty(amaTextValueMap, "description", String.class, ""));
                }
            }

            String imageUrl = extractBannerImage(hotelJCRResource);
            if (imageUrl != null) {
                hotelDetails.setHotelImagePath(imageUrl);
            }

            hotelDetails.setHotelLatitude(hotelModel.getLat());
            hotelDetails.setHoteLongitude(hotelModel.getLng());
            hotelDetails.setHotelArea(hotelModel.getHotelArea());
            hotelDetails.setHotelCity(hotelModel.getHotelCity());
            hotelDetails.setHotelLocationStat(hotelModel.getLocationStats());

            String hotelAddress = formHotelAddress(hotelPath);
            LOG.trace("Address formed using JCR properties :: " + hotelAddress);

            hotelDetails.setHotelAddress(hotelAddress);
            hotelDetails.setHotelDiscountedRate(getProperty(valueMap, "hotelDiscountedRate", String.class, ""));
            hotelDetails.setHotelActualRate(getProperty(valueMap, "hotelActualRate", String.class, ""));
            hotelDetails.setRatePerNightlabel(getProperty(valueMap, "ratePerNightlabel", String.class, ""));
            hotelDetails.setTicPoints(getProperty(valueMap, "ticPoints", String.class, ""));
            hotelDetails.setEpicurePoints(getProperty(valueMap, "epicurePoints", String.class, ""));

            String currencySymbol = currencyFetcherService
                    .getCurrencySymbolValue(getProperty(valueMap, "currencySymbol", String.class, ""));
            if (null != currencySymbol) {
                hotelDetails.setCurrencySymbol(currencySymbol);
            }


            List<UserSellingPoints> uspLs = getUserSellingPointsLs(hotelPageResource, hotelPath);

            hotelDetails.setUspLs(uspLs);
        } catch (Exception g) {
            LOG.error("Exception occured while building HotelProperties " + g.getMessage());
        } finally {
            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private List<UserSellingPoints> getUserSellingPointsLs(Resource hotelResource, String hotelPath) {

        Resource hotelSignatureFeaturesResource = hotelResource.getResourceResolver()
                .getResource(hotelPath + "/jcr:content/root/hotel_overview/hotelSignatureFeatures");
        LOG.debug("Recieved hotel_overview/hotelSignatureFeatures resource as " + hotelSignatureFeaturesResource);
        List<UserSellingPoints> uspLs = new ArrayList<UserSellingPoints>();
        if (null != hotelSignatureFeaturesResource) {
            Iterable<Resource> children = hotelSignatureFeaturesResource.getChildren();
            for (Resource childResrouce : children) {
                UserSellingPoints usp = new UserSellingPoints();
                ValueMap valueMap = childResrouce.adaptTo(ValueMap.class);
                usp.setSignatureFeaturePoint(getProperty(valueMap, "signatureFeatureValue", String.class, ""));
                usp.setSignatureFeatureIcon(getProperty(valueMap, "type", String.class, ""));
                uspLs.add(usp);
            }
        }
        return uspLs;
    }

    private String extractBannerImage(Resource resource) {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                ValueMap valMap = hotelBanner.getValueMap();
                String imageURL = String.valueOf(valMap.get(RESOURCE_NAME_HOTEL_BANNER));
                return imageURL;
            }
            LOG.trace("Returning null [condition failed], hotelBanner is null");
            return null;
        }
        LOG.trace("Returning null [condition failed], bannerParsys is null");
        return null;
    }

    private String formHotelAddress(String hotelPath) {

        ResourceResolver resolver = null;
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver as: " + resolver);
            Resource hotelResource = resolver.getResource(hotelPath);
            LOG.debug("Received hotel resource as: " + hotelResource);
            Resource hotelJCRResource = hotelResource.getChild("jcr:content");

            ValueMap valueMap = hotelJCRResource.adaptTo(ValueMap.class);
            String hotelArea = getProperty(valueMap, "hotelArea", String.class, "");
            hotelArea = hotelArea.contains(",") ? hotelArea.replaceAll(",", "") : hotelArea;
            String hotelCity = getProperty(valueMap, "hotelCity", String.class, "");
            hotelCity = hotelCity.contains(",") ? hotelCity.replaceAll(",", "") : hotelCity;
            String hotelPinCode = getProperty(valueMap, "hotelPinCode", String.class, "");
            hotelPinCode = hotelPinCode.contains(",") ? hotelPinCode.replaceAll(",", "") : hotelPinCode;
            String hotelCountry = getProperty(valueMap, "hotelCountry", String.class, "");
            hotelCountry = hotelCountry.contains(",") ? hotelCountry.replaceAll(",", "") : hotelCountry;

            if (!hotelArea.isEmpty() && !hotelCity.isEmpty() && !hotelPinCode.isEmpty() && !hotelCountry.isEmpty())
                return hotelArea + "," + hotelCity + "," + hotelPinCode + "," + hotelCountry;
            else if (hotelCountry.isEmpty())
                return hotelArea + "," + hotelCity + "," + hotelPinCode;
            else if (hotelPinCode.isEmpty())
                return hotelArea + "," + hotelCity;
            else if (hotelCity.isEmpty())
                return hotelArea + "," + hotelPinCode + "," + hotelCountry;
            else if (hotelArea.isEmpty())
                return hotelCity + "," + hotelPinCode + "," + hotelCountry;

        } catch (LoginException e) {
            LOG.error("An error occured while building properties for hotel at the sepcified path", e);
        } finally {
            if (resolver != null && resolver.isLive()) {
                resolver.close();
            }
        }
        return "";
    }

    private void fetchHoildayPackageProperties(String hotelPath) {
        String methodName = "fetchHoildayPackageProperties";
        LOG.trace("Method Entry: " + methodName);
        try {
            HashMap<String, String> predicateMap = new HashMap<>();
            LOG.debug("Searching for componentPath under the hotelPath : " + hotelPath);

            predicateMap.put("path", hotelPath);
            predicateMap.put("property", "sling:resourceType");
            predicateMap.put("property.value", CRXconstantForPackageDetailsComponent);
            predicateMap.put("p.limit", "-1");
            ResourceResolver resolver = null;
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            for (Hit hit : searchResult.getHits()) {
                LOG.debug("Found package component at : " + hit.getPath());
                String packageComponentPath = hit.getPath();
                Resource hotelJcrResource = resolver.getResource(packageComponentPath);
                String season = getSeasonFromPath(hotelJcrResource.getParent().getParent());
                buildPackageProperties(packageComponentPath, season);
            }
        } catch (Exception e) {
            LOG.error("Exception occured searching for packageComponent :: {}", e.getMessage());
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private String getSeasonFromPath(Resource packageJcrResource) {

        LOG.debug("Method entry getSeasonFromPath :{}", packageJcrResource.getPath());
        ValueMap hotelValueMap = packageJcrResource.adaptTo(ValueMap.class);
        String season = getProperty(hotelValueMap, "season", String.class, "");
        LOG.debug("Method exit getSeasonFromPath, Value of season found as {}", season);
        return season;
    }

    private void buildPackageProperties(String packageComponentPath, String seasonUnderHotel) {
        String methodName = "buildPackageProperties";
        ResourceResolver resolver = null;
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("getting resolver");

            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            Resource packageComponentResource = resolver.getResource(packageComponentPath);
            HolidayPackage holidayPackage = new HolidayPackage();
            ValueMap valueMap = packageComponentResource.adaptTo(ValueMap.class);
            LOG.debug("Fetchig the offer theme tag");
            Resource offerPackage = packageComponentResource.getParent().getParent();
            ValueMap packageMap = offerPackage.adaptTo(ValueMap.class);
            List<String> categoryTag = Arrays.asList(packageMap.get("cq:tags", String[].class));
            String tagTitle = getTagTitle(categoryTag);
            LOG.debug("Offer Tag: {}", tagTitle);
            holidayPackage.setPackageTitle(getProperty(valueMap, "packageTitle", String.class, ""));
            holidayPackage.setOfferCode(getProperty(valueMap, "offerRateCode", String.class, ""));
            holidayPackage.setRateDescription(getProperty(valueMap, "rateDescription", String.class, ""));
            holidayPackage.setCancellationPolicy(getProperty(valueMap, "cancellationPolicy", String.class, ""));
            holidayPackage.setDiscountedRate(
                    getProperty(valueMap, "discountedRate", String.class, "").replaceAll("[^0-9.]", ""));
            holidayPackage.setActualRate(getProperty(valueMap, "actualRate", String.class, ""));
            holidayPackage
                    .setLastFewRoomsAvailableFlag(getProperty(valueMap, "lastFewRoomsAvailableFlag", String.class, ""));
            holidayPackage.setLastFewRoomsAvailableLabel(
                    getProperty(valueMap, "lastFewRoomsAvailableLabel", String.class, ""));
            holidayPackage.setRatePerNightLabel(getProperty(valueMap, "ratePerNightLabel", String.class, ""));
            holidayPackage.setBookNowLabel(getProperty(valueMap, "bookNowLabel", String.class, ""));
            holidayPackage.setNoOfnights(getProperty(valueMap, "nights", String.class, ""));
            holidayPackage.setSeasonCategory(seasonUnderHotel);
            holidayPackage.setTheme(tagTitle);

            holidayPackage.setPackageDescription(getProperty(valueMap, "packageDescription", String.class, ""));
            holidayPackage.setTermsAndConditions(
                    formTermsAndConditionAsList(getProperty(valueMap, "termsAndCondition", String.class, "")));
            holidayPackage.setInclusionLs(fetchPackageInclusionFrom(packageComponentResource));

            holidayPackage.setOfferStartDate(getProperty(valueMap, "packageValidityStartDate", String.class, ""));
            holidayPackage.setOfferEndDate(getProperty(valueMap, "packageValidityEndDate", String.class, ""));

            String offerPagePath = packageComponentResource.getParent().getParent().getParent().getPath();
            LOG.trace("HolidayOfferPagePath is " + offerPagePath);
            holidayPackage.setHolidayOfferPagePath(offerPagePath);

            String packageRedirectionType = getProperty(valueMap, "packageRedirectionType", String.class, "");
            if (packageRedirectionType.equalsIgnoreCase("enquiry")
                    || packageRedirectionType.equalsIgnoreCase("internationalEnquiry")) {
                String enquiryRedirectionPath = getProperty(valueMap, "enquiryPath", String.class, "");
                holidayPackage.setPackageRedirectionType(packageRedirectionType);
                holidayPackage.setEnquiryPath(enquiryRedirectionPath);
            } else {
                holidayPackage.setPackageRedirectionType("bookdirect");
            }

            packageLs.add(holidayPackage);

        } catch (LoginException e) {
            LOG.error("An error occured while building package properties for hotel at the sepcified path", e);
        } finally {
            if (resolver != null && resolver.isLive()) {
                resolver.close();
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private List<String> formTermsAndConditionAsList(String termsAndConditionLongStr) {

        String methodName = "formTermsAndConditionAsList";
        LOG.trace("Method Entry : " + methodName);
        LOG.debug("Obtained termsAndConditionLongString  :: " + termsAndConditionLongStr);
        List<String> termsAndConditions = new ArrayList<String>();
        String delimiter = ";";
        if (!termsAndConditionLongStr.isEmpty()) {
            for (String str : termsAndConditionLongStr.split(delimiter)) {
                termsAndConditions.add(str);
            }
        }
        LOG.trace("Method Exit : " + methodName);
        return termsAndConditions;
    }

    private List<Inclusion> fetchPackageInclusionFrom(Resource packageResource) {

        String methodName = "fetchPackageInclusionFrom";
        LOG.trace("Method Entry : " + methodName);
        LOG.debug("Obtained packageComponent Resource as " + packageResource);
        List<Inclusion> inclusionLs = new ArrayList<Inclusion>();
        if (null != packageResource) {
            Resource inclusionNodeResource = packageResource.getChild("inclusions");
            Iterable<Resource> children = inclusionNodeResource.getChildren();
            for (Resource itemRes : children) {
                LOG.debug("Obtrained itemResource as : " + itemRes);
                ValueMap valueMap = itemRes.adaptTo(ValueMap.class);
                String delimiter = ":";
                Inclusion obj = new Inclusion();
                String[] nameImageMap = getProperty(valueMap, "inclusionImageMap", String.class, "").split(delimiter);
                if (nameImageMap.length == 2) {
                    obj.setInclusionTitle(nameImageMap[0]);
                    obj.setInclusionImagePath(nameImageMap[1]);
                }
                obj.setInclusionDescription(getProperty(valueMap, "inclusionDescription", String.class, ""));
                inclusionLs.add(obj);
            }
        }
        LOG.trace("Method Exit : " + methodName);
        return inclusionLs;
    }

    private String getTagTitle(List<String> categoryTag) throws LoginException {
        LOG.trace("Method Entry: getTagTitle");
        String tagTitle = "";
        String parentTagTitle = "";
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        LOG.debug("destination resourceResolver : {}", resourceResolver);
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            LOG.trace("city array before if:" + categoryTag.get(0));
            if (categoryTag != null) {

                for (String tagchild : categoryTag) {
                    if (tagchild.startsWith("taj:holidays")) {
                        tagRoot = tagManager.resolve(tagchild);
                        if (tagRoot != null) {
                            LOG.trace("Tag Root Path : {}, title: {}", tagRoot.getPath(), tagRoot.getTitle());
                            if (tagRoot.getPath().startsWith("/etc/tags/taj/holidays/themes")) {
                                tagTitle = tagRoot.getTitle();
                                parentTagTitle = tagRoot.getParent().getTitle();
                            }
                        }
                    }
                }
            }
        }
        if (parentTagTitle.equals("Palace Retreats")) {
            tagTitle = parentTagTitle;
        }
        LOG.trace("Method Exit -> returning  : Tag Title : " + tagTitle);
        return tagTitle;
    }

    private String getCountryFromTags(Resource hotelPageResource) throws LoginException {
        LOG.trace("Method Entry: getCountry()");
        LOG.debug("resource for country : {}", hotelPageResource);
        String countryName = "";
        ValueMap map = hotelPageResource.adaptTo(ValueMap.class);
        List<String> city = new ArrayList<String>();
        if (map.containsKey("city")) {
            city = Arrays.asList(map.get("city", String[].class));
        }
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        LOG.debug("destination resourceResolver : {}", resourceResolver);
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            // LOG.trace("city array before if:" + city.get(0));
            if (null != city && !city.isEmpty()) {
                LOG.trace("city array :" + city.get(0));
                LOG.trace("TagManager resolve :" + tagManager.resolve(city.get(0)));
                tagRoot = tagManager.resolve(city.get(0));
                if (tagRoot != null) {
                    LOG.trace("Tag Root Path : " + tagRoot.getPath());
                    countryName = tagRoot.getParent().getTitle();
                }
            }
        }
        LOG.trace("Method Exit -> returning  : Country name : " + countryName);
        return countryName;
    }

    private void getOffersTagsList(Resource hotelResource, Resource hotelPageResource) throws LoginException {

        String methodName = "getholidayTags()";
        LOG.trace("Method Entry:" + methodName);
        LOG.debug("resource for tags: {}", hotelResource.getPath());

        getChildPath(hotelPageResource, "cq:Page");
        LOG.trace("Method Exit -> {}s", methodName);
    }

    private Resource getChildPath(Resource packageResource, String resourceType) throws LoginException {
        String methodName = "getChildPath()";
        LOG.trace("Method Entry:" + methodName);
        fetchOfferTags(packageResource);
        Iterable<Resource> holidayPackages = packageResource.getChildren();
        LOG.debug("holidayPackages in iterator :" + holidayPackages);
        Resource resourcePackage = null;
        for (Resource packages : holidayPackages) {
            LOG.debug("packages.getResourceType() :" + packages.getResourceType());
            if (packages.getResourceType().equals(resourceType)) {
                LOG.debug("packages under hotel :" + packages.getPath());
                fetchOfferTags(packages);
                getChildPath(packages, "cq:Page");

            }
        }
        return resourcePackage;
    }

    private Map<String, Set<String>> fetchOfferTags(Resource hotelResource) throws LoginException {

        String methodName = "fetchOfferTags()";
        LOG.trace("Method Entry:" + methodName);
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        Resource jcrResoruce = hotelResource.getChild("jcr:content");
        LOG.debug("packages path after if :" + jcrResoruce.getPath());
        ValueMap valueMap = jcrResoruce.adaptTo(ValueMap.class);
        LOG.debug("valueMap ::" + valueMap);
        String[] tags = valueMap.get("cq:tags", String[].class);
        LOG.debug("tags ::" + tags);

        Tag tagRoot = null;
        if (null != resourceResolver) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            LOG.trace("tags array " + tags);
            if (tags != null) {
                for (int i = 0; i < tags.length; i++) {
                    if (tags[i].startsWith("taj:holidays")) {
                        tagRoot = tagManager.resolve(tags[i]);
                        if (tagRoot != null) {
                            LOG.trace("Tag Root Path : " + tagRoot.getTitle());
                            if (tagRoot.getPath().startsWith("/etc/tags/taj/holidays/experiences")) {
                                experiencesTags.add(tagRoot.getTitle());
                            }
                            if (tagRoot.getPath().startsWith("/etc/tags/taj/holidays/themes")) {
                                themesTags.add(tagRoot.getTitle());
                            }
                        }
                    }
                }
            }
        }
        LOG.debug("destinationTags ::::" + destinationTags.size());
        LOG.debug("destinationTags ::::" + destinationTags);
        destinationTags.put("experience", experiencesTags);
        destinationTags.put("themes", themesTags);
        return destinationTags;

    }

    private List<HolidayPackage> sortHolidayPackages(List<HolidayPackage> packageList) {
        String methodName = "sortHolidayPackages";
        LOG.trace("Method Entry: " + methodName);
        try {

            String[] packageSortOrder = new String[] { "familygetaways", "romanticrendezvous", "spasojourn",
                    "royalretreat", "royalhoneymoon", "royalspa" };

            Collections.sort(packageList, new Comparator<HolidayPackage>() {

                @Override
                public int compare(HolidayPackage p1, HolidayPackage p2) {

                    int rate1 = getFormatedValue(p1.getDiscountedRate());
                    int rate2 = getFormatedValue(p2.getDiscountedRate());
                    String packageName1 = p1.getPackageTitle();
                    String packageName2 = p2.getPackageTitle();

                    return rate1 > rate2 ? 1
                            : rate1 < rate2 ? -1
                                    : Integer.compare(stringContainsItemFromList(packageName1, packageSortOrder),
                                            stringContainsItemFromList(packageName2, packageSortOrder));
                }

                private int getFormatedValue(String packageRate) {
                    String formattedString = packageRate.replaceAll("[^0-9.]", "");
                    return Math.round(Float.parseFloat(formattedString));
                }

                private int stringContainsItemFromList(String inputStr, String[] items) {
                    inputStr = inputStr.replaceAll(" ", "").toLowerCase();
                    for (int i = 0; i < items.length; i++) {
                        if (inputStr.contains(items[i].toLowerCase())) {
                            return i;
                        }
                    }
                    return -1;
                }
            });

            /*
             * Setting the least Package Rate for the specifc Hotel post Sorting to avoid content Authoring mismatches
             */
            hotelDetails.setHotelDiscountedRate(packageList.get(0).getDiscountedRate());

        } catch (NumberFormatException e) {
            LOG.error(
                    "Exception Occured : One of the Holiday Package Price has not been authored properly , Sorting failed :: {}",
                    e.getMessage());
            LOG.trace("Method Exit: " + methodName);
            return packageList;
        }
        LOG.trace("Method Exit: " + methodName);
        return packageList;
    }


    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        String methodName = "getProperty";
        LOG.trace("Method Entry: " + methodName);
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        LOG.trace("Value found for key: " + key + " : " + value);
        LOG.trace("Method Exit: " + methodName);
        return value;
    }

    @Override
    public HotelDetails getHotelDetails() {
        return hotelDetails;
    }
}
