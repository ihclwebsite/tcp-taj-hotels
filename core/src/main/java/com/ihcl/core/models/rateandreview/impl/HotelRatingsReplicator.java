/**
 *
 */
package com.ihcl.core.models.rateandreview.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;


@Component(service = HotelRatingsReplicator.class)
public class HotelRatingsReplicator {

    private static final String PATH = "/content/tajhotels/en-in/our-hotels";

    private static final Logger log = LoggerFactory.getLogger(HotelRatingsReplicator.class);

    @Reference
    private Replicator replicator;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    QueryBuilder builder;

    ResourceResolver resourceResolver;

    public void activatePage(Session session) {
        log.info("inside activatePage page method");
        ReplicationOptions opts = new ReplicationOptions();
        opts.setFilter(new AgentFilter() {

            @Override
            public boolean isIncluded(Agent arg0) {
                log.trace("Agent Name =" + arg0.getConfiguration().getName());
                return "publish1apsoutheast1Agent".equals(arg0.getConfiguration().getName())
                        || "publish1apsoutheast1Agent".equals(arg0.getId());

            }
        });

        ReplicationOptions opts1 = new ReplicationOptions();
        opts1.setFilter(new AgentFilter() {

            @Override
            public boolean isIncluded(Agent arg0) {
                log.trace("Agent Name =" + arg0.getConfiguration().getName());
                return "publish2apsoutheast1Agent".equals(arg0.getConfiguration().getName())
                        || "publish2apsoutheast1Agent".equals(arg0.getId());
            }
        });

        String[] paths = getPathsToBeReplicated();
        for (String path : paths) {
            log.trace("path of jcr content/ node " + path);
        }
        try {
            replicator.replicate(session, ReplicationActionType.ACTIVATE, paths, opts);
            replicator.replicate(session, ReplicationActionType.ACTIVATE, paths, opts1);
        } catch (ReplicationException e) {
            log.error(e.getMessage(), e);
        }
    }


    private String[] getPathsToBeReplicated() {
        log.info("Entered  getPathsToBeReplicated method");
        List<String> paths = new ArrayList<String>();
        Map<String, String> queryMap = new TreeMap<>();
        try {
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            queryMap.put("path", PATH);
            queryMap.put("property", "sling:resourceType");
            queryMap.put("property.value", "tajhotels/components/structure/hotels-landing-page");
            queryMap.put("p.hits", "all");
            queryMap.put("p.limit", "-1");
            Query query = builder.createQuery(PredicateGroup.create(queryMap), resourceResolver.adaptTo(Session.class));
            for (Hit hit : query.getResult().getHits()) {
                paths.add(hit.getPath());
            }
            log.info("Exiting  gettyIds method");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return paths.toArray(new String[0]);
    }

}
