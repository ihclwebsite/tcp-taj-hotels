package com.ihcl.core.models.gallery.impl;

import com.day.cq.dam.api.DamConstants;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import org.apache.sling.api.resource.*;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.jcr.NamespaceException;

import java.util.ArrayList;
import java.util.List;


@Component(service = GalleryImageFetcherService.class)
public class GalleryImageFetcherServiceImpl implements GalleryImageFetcherService {

    private static final Logger LOG = LoggerFactory.getLogger(GalleryImageFetcherServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received resource resolver factory as: " + resolverFactory);
        LOG.trace("Method Exit: " + methodName);
    }

    @Override
    public List<JSONObject> getGalleryImagePathsAt(Resource galleryResource) {
        String methodName = "getGalleryImagePathsAt";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = galleryResource.adaptTo(ValueMap.class);
        List<JSONObject> imagesJsonList = buildImagePaths(valueMap);
        LOG.debug("Returning list of image json as: " + imagesJsonList);
        LOG.trace("Method Exit: " + methodName);
        return imagesJsonList;
    }

    @Override
    public String getParentPath(Resource galleryResource) {
        ValueMap valueMap = galleryResource.adaptTo(ValueMap.class);
        String parentPath = valueMap.get("parentPath", String.class);
        return parentPath;
    }

    @Override
    public Integer getDepth(Resource galleryResource) {
        ValueMap valueMap = galleryResource.adaptTo(ValueMap.class);
        Integer depth = valueMap.get("depth", Integer.class);
        return depth;
    }

    /**
     * @param valueMap
     * @return
     */
    private List<JSONObject> buildImagePaths(ValueMap valueMap) {
        String methodName = "buildImagePaths";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Attempting to fetch images json array from value map: " + valueMap);
        List<JSONObject> imagePaths = new ArrayList<>();
        if (valueMap.containsKey("galleryImageSource") && (valueMap.containsKey("images") || valueMap.containsKey("parentPath"))) {
            String imageSource = valueMap.get("galleryImageSource", String.class);
            if (imageSource.equals("children")) {
                imagePaths = buildChildImagePaths(valueMap);
            } else {
                imagePaths = buildStaticImagePaths(valueMap);
            }
        }
        LOG.trace("Method Exit: " + methodName);
        return imagePaths;
    }


    /**
     * @param valueMap
     * @return
     */
    private List<JSONObject> buildStaticImagePaths(ValueMap valueMap) {
        String methodName = "buildStaticImagePaths";
        LOG.trace("Method Entry: " + methodName);

        List<JSONObject> imagesJsonArray = new ArrayList<>();
        try {
            ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver from factory as: " + resourceResolver);
            String[] imagePathsArray = valueMap.get("images", String[].class);
            LOG.debug("Received property from value map as: " + imagePathsArray);
            for (int i = 0; i < imagePathsArray.length; i++) {
                String imagePath = imagePathsArray[i];
                LOG.debug("Processing image number: " + i + " having image path as: " + imagePath);
                Resource resource = resourceResolver.getResource(imagePath);
                LOG.debug("Received resource for image path: " + imagePath + " as: " + resource);
                addJsonIfDamAsset(imagesJsonArray, resource);
            }
        } catch (LoginException e) {
            LOG.error("An error occured while fetching resource resolver. Please check if system user has appropriate permissions.", e);
        }

        LOG.trace("Method Exit: " + methodName);
        return imagesJsonArray;

    }

    /**
     * @param valueMap
     * @return
     */
    private List<JSONObject> buildChildImagePaths(ValueMap valueMap) {
        String methodName = "buildChildImagePaths";
        LOG.trace("Method Entry: " + methodName);
        List<JSONObject> imagePaths = new ArrayList<JSONObject>();
        try {
            ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);

            String parentPath = valueMap.get("parentPath", String.class);

            Resource resource = resourceResolver.getResource(parentPath);

            Integer depth = valueMap.get("depth", Integer.class);

            recurseResource(resource, depth, 0, imagePaths);

        } catch (LoginException e) {
            LOG.error("An error occured while fetching a resource during image paths creation for: " + valueMap, e);

        }
        LOG.trace("Method Exit: " + methodName);
        return imagePaths;
    }

    private void recurseResource(Resource resource, Integer depth, int currentDepth, List<JSONObject> imagePaths) {
        if (currentDepth < depth) {
            if (null == resource) {
                return;
            }

            Iterable<Resource> children = resource.getChildren();
            for (Resource child : children) {
                addJsonIfDamAsset(imagePaths, child);
                recurseResource(child, depth, currentDepth + 1, imagePaths);
            }
        }
    }

    private void addJsonIfDamAsset(List<JSONObject> imagePaths, Resource resource) {
        String methodName = "addJsonIfDamAsset";
        LOG.trace("Method Entry: " + methodName);
        if (resource != null && resource.isResourceType(DamConstants.NT_DAM_ASSET)) {
            addImageDetailsJson(imagePaths, resource);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void addImageDetailsJson(List<JSONObject> imagePaths, Resource child) {
        String methodName = "addImageDetailsJson";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Attempting to add image details json for the resource: " + child);
            if (child != null) {
                JSONObject imageObject = new JSONObject();

                String imagePath = child.getPath();
                imageObject.put("imagePath", imagePath);
                Resource assetMetadata=child.getChild("jcr:content/metadata");
                if(assetMetadata != null) {
                	 ValueMap assetMap=assetMetadata.adaptTo(ValueMap.class);
                	 LOG.debug("Received image metadata as: " + assetMap);
                	 if(assetMap.containsKey("dc:title")){
                		 imageObject.put("imageTitle", assetMap.get("dc:title"));
                	 }
                	 if(assetMap.containsKey("dc:description")){
                		 imageObject.put("imageDescription", assetMap.get("dc:description"));
                	 }
                }else {
                	LOG.info("Metadata not found for the image at path: {}",imagePath);
                }
                imagePaths.add(imageObject);
            }
        } catch (Exception e) {
        	if (e.getClass().isInstance(NamespaceException.class)) {
                LOG.trace("Name space exception encountered. Ckeck the metadata properties of the image ",
                        e.getMessage(), e);
            }else {
            LOG.error("An error occured while trying to add image json object to array.", e);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }
}
