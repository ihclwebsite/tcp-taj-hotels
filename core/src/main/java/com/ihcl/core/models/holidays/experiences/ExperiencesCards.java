package com.ihcl.core.models.holidays.experiences;

public class ExperiencesCards {

	private String experienceImagePath;
	private String experienceTitle;
	private String experiencePagePath;
	private String description;

	public String getExperienceImagePath() {
		return experienceImagePath;
	}

	public void setExperienceImagePath(String experienceImagePath) {
		this.experienceImagePath = experienceImagePath;
	}

	public String getExperienceTitle() {
		return experienceTitle;
	}

	public void setExperienceTitle(String experienceTitle) {
		this.experienceTitle = experienceTitle;
	}

	public String getExperiencePagePath() {
		return experiencePagePath;
	}

	public void setExperiencePagePath(String experiencePagePath) {
		this.experiencePagePath = experiencePagePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
