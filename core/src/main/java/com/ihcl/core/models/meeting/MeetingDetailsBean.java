/**
 *
 */
package com.ihcl.core.models.meeting;

import java.util.List;

import org.json.JSONObject;

/**
 * <pre>
 * MeetingDetailsBean.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :30-Aug-2019
 * @ClassName : MeetingDetailsBean.java
 * @Description : com.ihcl.core.models.meeting -MeetingDetailsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  30-Aug-2019     Subharun Mukherjee            Create
 *
 *               </pre>
 */
public class MeetingDetailsBean {

    /** The room title. */
    private String roomTitle;

    /** The request quote page path. */
    private String requestQuotePagePath;

    /** The room capacity. */
    private String roomCapacity;

    /** The room long description. */
    private String roomLongDescription;

    /** The room built. */
    private String roomBuilt;

    /** The room renovated. */
    private String roomRenovated;

    /** The airport distance. */
    private String airportDistance;

    /** The room dimension. */
    private String roomDimension;

    /** The room area. */
    private String roomArea;

    /** The room height. */
    private String roomHeight;

    /** The guest entry. */
    private String guestEntry;

    /** The seating style. */
    private String seatingStyle;

    /** The theatre capacity. */
    private String theatreCapacity;

    /** The u capacity. */
    private String uCapacity;

    /** The classroom capacity. */
    private String classroomCapacity;

    /** The circular capacity. */
    private String circularCapacity;

    /** The boardroom capacity. */
    private String boardroomCapacity;

    /** The reception. */
    private String reception;

    /** The room theme. */
    private String roomTheme;

    /** The capacity. */
    private String capacity;

    /** The meeting image. */
    private String meetingImage;

    /** The gallery image paths. */
    private List<JSONObject> galleryImagePaths;

    /** The location. */
    private String location;

    /** The hotel name. */
    private String hotelName;

    /** The venue event types. */
    private List<String> venueEventTypes;

    /** The request quote email id. */
    private String requestQuoteEmailId;


    /**
     * Gets the room title.
     *
     * @return the room title
     */
    public String getRoomTitle() {
        return roomTitle;
    }


    /**
     * Sets the room title.
     *
     * @param roomTitle
     *            the new room title
     */
    public void setRoomTitle(String roomTitle) {
        this.roomTitle = roomTitle;
    }


    /**
     * Gets the request quote page path.
     *
     * @return the request quote page path
     */
    public String getRequestQuotePagePath() {
        return requestQuotePagePath;
    }


    /**
     * Sets the request quote page path.
     *
     * @param requestQuotePagePath
     *            the new request quote page path
     */
    public void setRequestQuotePagePath(String requestQuotePagePath) {
        this.requestQuotePagePath = requestQuotePagePath;
    }


    /**
     * Gets the room capacity.
     *
     * @return the room capacity
     */
    public String getRoomCapacity() {
        return roomCapacity;
    }


    /**
     * Sets the room capacity.
     *
     * @param roomCapacity
     *            the new room capacity
     */
    public void setRoomCapacity(String roomCapacity) {
        this.roomCapacity = roomCapacity;
    }


    /**
     * Gets the room long description.
     *
     * @return the room long description
     */
    public String getRoomLongDescription() {
        return roomLongDescription;
    }


    /**
     * Sets the room long description.
     *
     * @param roomLongDescription
     *            the new room long description
     */
    public void setRoomLongDescription(String roomLongDescription) {
        this.roomLongDescription = roomLongDescription;
    }


    /**
     * Gets the room built.
     *
     * @return the room built
     */
    public String getRoomBuilt() {
        return roomBuilt;
    }


    /**
     * Sets the room built.
     *
     * @param roomBuilt
     *            the new room built
     */
    public void setRoomBuilt(String roomBuilt) {
        this.roomBuilt = roomBuilt;
    }


    /**
     * Gets the room renovated.
     *
     * @return the room renovated
     */
    public String getRoomRenovated() {
        return roomRenovated;
    }


    /**
     * Sets the room renovated.
     *
     * @param roomRenovated
     *            the new room renovated
     */
    public void setRoomRenovated(String roomRenovated) {
        this.roomRenovated = roomRenovated;
    }


    /**
     * Gets the airport distance.
     *
     * @return the airport distance
     */
    public String getAirportDistance() {
        return airportDistance;
    }


    /**
     * Sets the airport distance.
     *
     * @param airportDistance
     *            the new airport distance
     */
    public void setAirportDistance(String airportDistance) {
        this.airportDistance = airportDistance;
    }


    /**
     * Gets the room dimension.
     *
     * @return the room dimension
     */
    public String getRoomDimension() {
        return roomDimension;
    }


    /**
     * Sets the room dimension.
     *
     * @param roomDimension
     *            the new room dimension
     */
    public void setRoomDimension(String roomDimension) {
        this.roomDimension = roomDimension;
    }


    /**
     * Gets the room area.
     *
     * @return the room area
     */
    public String getRoomArea() {
        return roomArea;
    }


    /**
     * Sets the room area.
     *
     * @param roomArea
     *            the new room area
     */
    public void setRoomArea(String roomArea) {
        this.roomArea = roomArea;
    }


    /**
     * Gets the room height.
     *
     * @return the room height
     */
    public String getRoomHeight() {
        return roomHeight;
    }


    /**
     * Sets the room height.
     *
     * @param roomHeight
     *            the new room height
     */
    public void setRoomHeight(String roomHeight) {
        this.roomHeight = roomHeight;
    }


    /**
     * Gets the guest entry.
     *
     * @return the guest entry
     */
    public String getGuestEntry() {
        return guestEntry;
    }


    /**
     * Sets the guest entry.
     *
     * @param guestEntry
     *            the new guest entry
     */
    public void setGuestEntry(String guestEntry) {
        this.guestEntry = guestEntry;
    }


    /**
     * Gets the seating style.
     *
     * @return the seating style
     */
    public String getSeatingStyle() {
        return seatingStyle;
    }


    /**
     * Sets the seating style.
     *
     * @param seatingStyle
     *            the new seating style
     */
    public void setSeatingStyle(String seatingStyle) {
        this.seatingStyle = seatingStyle;
    }


    /**
     * Gets the theatre capacity.
     *
     * @return the theatre capacity
     */
    public String getTheatreCapacity() {
        return theatreCapacity;
    }


    /**
     * Sets the theatre capacity.
     *
     * @param theatreCapacity
     *            the new theatre capacity
     */
    public void setTheatreCapacity(String theatreCapacity) {
        this.theatreCapacity = theatreCapacity;
    }


    /**
     * Gets the u capacity.
     *
     * @return the u capacity
     */
    public String getuCapacity() {
        return uCapacity;
    }


    /**
     * Sets the u capacity.
     *
     * @param uCapacity
     *            the new u capacity
     */
    public void setuCapacity(String uCapacity) {
        this.uCapacity = uCapacity;
    }


    /**
     * Gets the classroom capacity.
     *
     * @return the classroom capacity
     */
    public String getClassroomCapacity() {
        return classroomCapacity;
    }


    /**
     * Sets the classroom capacity.
     *
     * @param classroomCapacity
     *            the new classroom capacity
     */
    public void setClassroomCapacity(String classroomCapacity) {
        this.classroomCapacity = classroomCapacity;
    }


    /**
     * Gets the circular capacity.
     *
     * @return the circular capacity
     */
    public String getCircularCapacity() {
        return circularCapacity;
    }


    /**
     * Sets the circular capacity.
     *
     * @param circularCapacity
     *            the new circular capacity
     */
    public void setCircularCapacity(String circularCapacity) {
        this.circularCapacity = circularCapacity;
    }


    /**
     * Gets the boardroom capacity.
     *
     * @return the boardroom capacity
     */
    public String getBoardroomCapacity() {
        return boardroomCapacity;
    }


    /**
     * Sets the boardroom capacity.
     *
     * @param boardroomCapacity
     *            the new boardroom capacity
     */
    public void setBoardroomCapacity(String boardroomCapacity) {
        this.boardroomCapacity = boardroomCapacity;
    }


    /**
     * Gets the reception.
     *
     * @return the reception
     */
    public String getReception() {
        return reception;
    }


    /**
     * Sets the reception.
     *
     * @param reception
     *            the new reception
     */
    public void setReception(String reception) {
        this.reception = reception;
    }


    /**
     * Gets the room theme.
     *
     * @return the room theme
     */
    public String getRoomTheme() {
        return roomTheme;
    }


    /**
     * Sets the room theme.
     *
     * @param roomTheme
     *            the new room theme
     */
    public void setRoomTheme(String roomTheme) {
        this.roomTheme = roomTheme;
    }


    /**
     * Gets the capacity.
     *
     * @return the capacity
     */
    public String getCapacity() {
        return capacity;
    }


    /**
     * Sets the capacity.
     *
     * @param capacity
     *            the new capacity
     */
    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }


    /**
     * Gets the meeting image.
     *
     * @return the meeting image
     */
    public String getMeetingImage() {
        return meetingImage;
    }


    /**
     * Sets the meeting image.
     *
     * @param meetingImage
     *            the new meeting image
     */
    public void setMeetingImage(String meetingImage) {
        this.meetingImage = meetingImage;
    }


    /**
     * Gets the gallery image paths.
     *
     * @return the gallery image paths
     */
    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }


    /**
     * Sets the gallery image paths.
     *
     * @param galleryImagePaths
     *            the new gallery image paths
     */
    public void setGalleryImagePaths(List<JSONObject> galleryImagePaths) {
        this.galleryImagePaths = galleryImagePaths;
    }


    /**
     * Gets the location.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }


    /**
     * Sets the location.
     *
     * @param location
     *            the new location
     */
    public void setLocation(String location) {
        this.location = location;
    }


    /**
     * Gets the hotel name.
     *
     * @return the hotel name
     */
    public String getHotelName() {
        return hotelName;
    }


    /**
     * Sets the hotel name.
     *
     * @param hotelName
     *            the new hotel name
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }


    /**
     * Gets the venue event types.
     *
     * @return the venue event types
     */
    public List<String> getVenueEventTypes() {
        return venueEventTypes;
    }


    /**
     * Sets the venue event types.
     *
     * @param venueEventTypes
     *            the new venue event types
     */
    public void setVenueEventTypes(List<String> venueEventTypes) {
        this.venueEventTypes = venueEventTypes;
    }


    /**
     * Gets the request quote email id.
     *
     * @return the request quote email id
     */
    public String getRequestQuoteEmailId() {
        return requestQuoteEmailId;
    }


    /**
     * Sets the request quote email id.
     *
     * @param requestQuoteEmailId
     *            the new request quote email id
     */
    public void setRequestQuoteEmailId(String requestQuoteEmailId) {
        this.requestQuoteEmailId = requestQuoteEmailId;
    }


    /**
     * Instantiates a new meeting details bean.
     *
     * @param roomTitle
     *            the room title
     * @param requestQuotePagePath
     *            the request quote page path
     * @param roomCapacity
     *            the room capacity
     * @param roomLongDescription
     *            the room long description
     * @param roomBuilt
     *            the room built
     * @param roomRenovated
     *            the room renovated
     * @param airportDistance
     *            the airport distance
     * @param roomDimension
     *            the room dimension
     * @param roomArea
     *            the room area
     * @param roomHeight
     *            the room height
     * @param guestEntry
     *            the guest entry
     * @param seatingStyle
     *            the seating style
     * @param theatreCapacity
     *            the theatre capacity
     * @param uCapacity
     *            the u capacity
     * @param classroomCapacity
     *            the classroom capacity
     * @param circularCapacity
     *            the circular capacity
     * @param boardroomCapacity
     *            the boardroom capacity
     * @param reception
     *            the reception
     * @param roomTheme
     *            the room theme
     * @param capacity
     *            the capacity
     * @param meetingImage
     *            the meeting image
     * @param galleryImagePaths
     *            the gallery image paths
     * @param location
     *            the location
     * @param hotelName
     *            the hotel name
     * @param venueEventTypes
     *            the venue event types
     * @param requestQuoteEmailId
     *            the request quote email id
     */
    public MeetingDetailsBean(String roomTitle, String requestQuotePagePath, String roomCapacity,
            String roomLongDescription, String roomBuilt, String roomRenovated, String airportDistance,
            String roomDimension, String roomArea, String roomHeight, String guestEntry, String seatingStyle,
            String theatreCapacity, String uCapacity, String classroomCapacity, String circularCapacity,
            String boardroomCapacity, String reception, String roomTheme, String capacity, String meetingImage,
            List<JSONObject> galleryImagePaths, String location, String hotelName, List<String> venueEventTypes,
            String requestQuoteEmailId) {
        super();
        this.roomTitle = roomTitle;
        this.requestQuotePagePath = requestQuotePagePath;
        this.roomCapacity = roomCapacity;
        this.roomLongDescription = roomLongDescription;
        this.roomBuilt = roomBuilt;
        this.roomRenovated = roomRenovated;
        this.airportDistance = airportDistance;
        this.roomDimension = roomDimension;
        this.roomArea = roomArea;
        this.roomHeight = roomHeight;
        this.guestEntry = guestEntry;
        this.seatingStyle = seatingStyle;
        this.theatreCapacity = theatreCapacity;
        this.uCapacity = uCapacity;
        this.classroomCapacity = classroomCapacity;
        this.circularCapacity = circularCapacity;
        this.boardroomCapacity = boardroomCapacity;
        this.reception = reception;
        this.roomTheme = roomTheme;
        this.capacity = capacity;
        this.meetingImage = meetingImage;
        this.galleryImagePaths = galleryImagePaths;
        this.location = location;
        this.hotelName = hotelName;
        this.venueEventTypes = venueEventTypes;
        this.requestQuoteEmailId = requestQuoteEmailId;
    }

}
