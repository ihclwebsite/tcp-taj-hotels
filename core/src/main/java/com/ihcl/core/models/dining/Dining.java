package com.ihcl.core.models.dining;

import java.util.List;

import org.json.JSONObject;


public interface Dining {

    void init();

    String getDiningName();

    String getDiningShortDesc();

    String getDiningAddress();

    String getDiningDescription();

    String getDiningLongDescription();

    String getDiningImage();

    String getDiningPhoneNumber();

    List<String> getDiningCuisine();

    String getDressCode();

    String getReviewRating();

    String getTicPoints();

    String getEpicurePoints();

    String getAveragePrice();

    String getTaxDisclaimer();

    String getDiningID();

    String getResourceURL();

    String getOfferSpecifics();

    String getDiscountValue();

    String getCurrencySymbol();

    List<JSONObject> getMenuImagePaths();

    String getDiningPath();

    String getPathToDownloadFrom();

    String getTimingsLabel1();

    String getTimingsLabel2();

    String getTimingsValue1();

    String getTimingsValue2();

    String getDiningLongitude();

    String getDiningLatitude();

    String getHotelArea();

    String getHotelCity();

    String getHotelPinCode();

    String getHotelCountry();

    String getRestaurantAddress();
    
    String getDiningBrand();

}
