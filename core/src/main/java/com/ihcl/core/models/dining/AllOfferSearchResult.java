package com.ihcl.core.models.dining;

import java.util.Date;

public class AllOfferSearchResult {

    private String bannerImage;

    private String pagePath;
    
    private String pageTitle;
    
    private String offerCategory;
    
    private String offerRateCode;
    
    private String offerShortDesc;
    
    private String offerTitle;
    
    private String offerType;
    
    private Boolean roundTheYearOffer;
    
    private String seoDescription;
    
    private String seoKeywords;
    
    private String seoMetaTag;

    private String jcrCanonical;
    
    private String priority;
    
    private String listImage;
    
    private String validity;
    
    private String validityMsg;
    
    private String validityText;
    
    private String offerDescription;
    
    private String validityStartDate;
    
    private String offerSpecifics;
    
    private String spa;
    
    private String stay;
    
    private String stayInclusions;
    
    private String sightseeing;
    
    private String dining;
    
    private String conveniences;
    
    private String convenience;
    
    private String termsDescription;
    
    private String discountValue;
    
    private String offerEndsOn;
    
    private String offerStartsFrom;
    
	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getOfferCategory() {
		return offerCategory;
	}

	public void setOfferCategory(String offerCategory) {
		this.offerCategory = offerCategory;
	}

	public String getOfferRateCode() {
		return offerRateCode;
	}

	public void setOfferRateCode(String offerRateCode) {
		this.offerRateCode = offerRateCode;
	}

	public String getOfferShortDesc() {
		return offerShortDesc;
	}

	public void setOfferShortDesc(String offerShortDesc) {
		this.offerShortDesc = offerShortDesc;
	}

	public String getOfferTitle() {
		return offerTitle;
	}

	public void setOfferTitle(String offerTitle) {
		this.offerTitle = offerTitle;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public Boolean getRoundTheYearOffer() {
		return roundTheYearOffer;
	}

	public void setRoundTheYearOffer(Boolean roundTheYearOffer) {
		this.roundTheYearOffer = roundTheYearOffer;
	}

	public String getSeoDescription() {
		return seoDescription;
	}

	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoMetaTag() {
		return seoMetaTag;
	}

	public void setSeoMetaTag(String seoMetaTag) {
		this.seoMetaTag = seoMetaTag;
	}

	public String getJcrCanonical() {
		return jcrCanonical;
	}

	public void setJcrCanonical(String jcrCanonical) {
		this.jcrCanonical = jcrCanonical;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getListImage() {
		return listImage;
	}

	public void setListImage(String listImage) {
		this.listImage = listImage;
	}

	public String getValidity() {
		return validity;
	}

	public void setValidity(String validity) {
		this.validity = validity;
	}

	public String getValidityMsg() {
		return validityMsg;
	}

	public void setValidityMsg(String validityMsg) {
		this.validityMsg = validityMsg;
	}

	public String getValidityText() {
		return validityText;
	}

	public void setValidityText(String validityText) {
		this.validityText = validityText;
	}

	public String getOfferDescription() {
		return offerDescription;
	}

	public void setOfferDescription(String offerDescription) {
		this.offerDescription = offerDescription;
	}

	public String getValidityStartDate() {
		return validityStartDate;
	}

	public void setValidityStartDate(String validityStartDate) {
		this.validityStartDate = validityStartDate;
	}

	public String getOfferSpecifics() {
		return offerSpecifics;
	}

	public void setOfferSpecifics(String offerSpecifics) {
		this.offerSpecifics = offerSpecifics;
	}

	public String getSpa() {
		return spa;
	}

	public void setSpa(String spa) {
		this.spa = spa;
	}

	public String getStay() {
		return stay;
	}

	public void setStay(String stay) {
		this.stay = stay;
	}

	public String getStayInclusions() {
		return stayInclusions;
	}

	public void setStayInclusions(String stayInclusions) {
		this.stayInclusions = stayInclusions;
	}

	public String getSightseeing() {
		return sightseeing;
	}

	public void setSightseeing(String sightseeing) {
		this.sightseeing = sightseeing;
	}

	public String getDining() {
		return dining;
	}

	public void setDining(String dining) {
		this.dining = dining;
	}

	public String getConveniences() {
		return conveniences;
	}

	public void setConveniences(String conveniences) {
		this.conveniences = conveniences;
	}

	public String getConvenience() {
		return convenience;
	}

	public void setConvenience(String convenience) {
		this.convenience = convenience;
	}

	public String getTermsDescription() {
		return termsDescription;
	}

	public void setTermsDescription(String termsDescription) {
		this.termsDescription = termsDescription;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

	public String getOfferEndsOn() {
		return offerEndsOn;
	}

	public void setOfferEndsOn(String offerEndsOn) {
		this.offerEndsOn = offerEndsOn;
	}

	public String getOfferStartsFrom() {
		return offerStartsFrom;
	}

	public void setOfferStartsFrom(String offerStartsFrom) {
		this.offerStartsFrom = offerStartsFrom;
	}


}
