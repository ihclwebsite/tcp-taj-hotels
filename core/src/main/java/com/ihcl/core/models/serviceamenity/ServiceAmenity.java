package com.ihcl.core.models.serviceamenity;


import java.util.Objects;

/**
 *
 */
public class ServiceAmenity {

    private String name;

    private String image;

    private String description;

    private String path;

    private String group;

    /**
     * @param serviceName
     * @param imagePath
     * @param description
     * @param group
     * @param path
     */
    public ServiceAmenity(String serviceName, String imagePath, String description, String group, String path) {
        this.name = serviceName;
        this.image = imagePath;
        this.description = description;
        this.group = group;
        this.path = path;
    }


    /**
     * Getter for the field name
     *
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * Setter for the field name
     *
     * @param name
     *            the name to set
     */

    public void setName(String name) {
        this.name = name;
    }


    /**
     * Getter for the field image
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }


    /**
     * Setter for the field image
     *
     * @param image
     *            the image to set
     */

    public void setImage(String image) {
        this.image = image;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the field path
     *
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     */
    public void setPath(String path) {
        this.path = path;

    }


    /**
     * Getter for the field group
     *
     * @return the group
     */
    public String getGroup() {
        return group;
    }


    /**
     * Setter for the field group
     *
     * @param group
     *            the group to set
     */

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "ServiceAmenity{" +
                "name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", path='" + path + '\'' +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        boolean equality = false;
        if (o == null || getClass() != o.getClass()) {
            equality = false;
        } else {
            ServiceAmenity that = (ServiceAmenity) o;
            if (that.path != null) {
                equality = path.equals(that.path);
            }
        }
        return equality;
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
