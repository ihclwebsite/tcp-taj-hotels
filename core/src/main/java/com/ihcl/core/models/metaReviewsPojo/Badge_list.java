/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */

public class Badge_list {

    private String subtext;

    private String text;

    private Highlight_list[] highlight_list;

    private Badge_data badge_data;

    private String badge_type;

    public String getSubtext() {
        return subtext;
    }

    public void setSubtext(String subtext) {
        this.subtext = subtext;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Highlight_list[] getHighlight_list() {
        return highlight_list;
    }

    public void setHighlight_list(Highlight_list[] highlight_list) {
        this.highlight_list = highlight_list;
    }

    public Badge_data getBadge_data() {
        return badge_data;
    }

    public void setBadge_data(Badge_data badge_data) {
        this.badge_data = badge_data;
    }

    public String getBadge_type() {
        return badge_type;
    }

    public void setBadge_type(String badge_type) {
        this.badge_type = badge_type;
    }

    @Override
    public String toString() {
        return "ClassPojo [subtext = " + subtext + ", text = " + text + ", highlight_list = " + highlight_list
                + ", badge_data = " + badge_data + ", badge_type = " + badge_type + "]";
    }
}
