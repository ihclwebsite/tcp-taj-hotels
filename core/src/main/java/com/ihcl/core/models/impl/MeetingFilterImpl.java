package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.MeetingFilter;

@Model(adaptables = Resource.class,
        adapters = MeetingFilter.class)
public class MeetingFilterImpl implements MeetingFilter {

    private List<DropDownItem> meetingSizeList;

    private List<DropDownItem> meetingCapacityList;

    private List<DropDownItem> meetingEventTypeList;

    private List<DropDownItem> weddingEventTypeList;

    private static final Logger LOG = LoggerFactory.getLogger(MeetingFilter.class);

    private String title;

    private String value;

    @Self
    Resource resource;


    @Inject
    ResourceResolverFactory resourceResolverFactory;

    @PostConstruct
    public void init() {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            meetingSizeList = getTajhotelsTagDetails("/etc/tags/taj/venues/size", resourceResolver);
            meetingCapacityList = getTajhotelsTagDetails("/etc/tags/taj/venues/capacity", resourceResolver);
            meetingEventTypeList = getTajhotelsTagDetails("/etc/tags/taj/venues/events", resourceResolver);
            weddingEventTypeList = getTajhotelsTagDetails("/etc/tags/taj/weddings", resourceResolver);
        } catch (LoginException e) {
            LOG.error("Failed to retreive resource resolver using service resolver factory {} ", e.getMessage());
            LOG.debug("Failed to retreive resource resolver using service resolver factory {} ", e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }
    }


    /**
     * Gets the tajhotels tag details.
     *
     * @param tagPath
     *            the tag path
     * @param resourceResolver
     * @return the tajhotels tag details
     */
    private List<DropDownItem> getTajhotelsTagDetails(String tagPath, ResourceResolver resourceResolver) {
        List<DropDownItem> tagList = new ArrayList<>();
        Resource tagRoot = resourceResolver.getResource(tagPath);
        if (null != tagRoot) {
            Iterable<Resource> childTags = tagRoot.getChildren();
            for (Resource tagResource : childTags) {
                ValueMap eventType = tagResource.getValueMap();
                title = eventType.get("jcr:title", String.class);
                value = eventType.get("value", String.class);
                DropDownItem newItem = new DropDownItem();
                newItem.setTitle(title);
                newItem.setValue(value);
                tagList.add(newItem);
            }
        }
        return tagList;
    }

    @Override
    public List<DropDownItem> getMeetingSizeList() {
        return meetingSizeList;
    }

    @Override
    public List<DropDownItem> getMeetingCapacityList() {
        return meetingCapacityList;
    }

    @Override
    public List<DropDownItem> getMeetingEventTypeList() {
        return meetingEventTypeList;
    }

    @Override
    public List<DropDownItem> getWeddingEventTypeList() {
        return weddingEventTypeList;
    }

}
