package com.ihcl.core.models.banner;

import org.osgi.annotation.versioning.ConsumerType;

import java.util.List;

@ConsumerType
public interface BannerModel {

    String getDate();

    String getBannerImage();

    String getTintOpacity();

    String getBannerTitle();

    String getBannerDescDesk();

    String getBannerLinkText();

    String getBannerLinkUrl();

    String getBannerSubTitle();

    String getBfooterTitle();

    String getBfooterDesc();

    String getMobileImage();

    List<String> getContext();
    
    String getBannerRichText();
}
