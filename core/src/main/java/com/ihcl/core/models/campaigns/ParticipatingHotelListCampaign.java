/**
 *
 */
package com.ihcl.core.models.campaigns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * @author Nutan
 *
 */
@Model(
        adaptables = { Resource.class })
public class ParticipatingHotelListCampaign {

    private static final Logger LOG = LoggerFactory.getLogger(ParticipatingHotelListCampaign.class);


    @Self
    private Resource resource;

    private Resource tagResource;

    private ValueMap valueMap;

    @Inject
    private ResourceResolverFactory resolverFactory;

    private ResourceResolver resourceResolver = null;

    private Map<String, List<String>> cityNameHotels = new HashMap<String, List<String>>();

    public Map<String, List<String>> getHotelListMapData() {
        return cityNameHotels;
    }

    @Inject
    @Optional
    private String[] pages;

    @PostConstruct
    protected void init() {
        LOG.debug("Inside method init() of participating hotel campaign");
        try {
            resourceResolver = resolverFactory.getServiceResourceResolver(null);
            getCityHotels();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        LOG.trace("Method exit - init()");
    }

    /**
     * This method is to create a map of city name and corresponding hotels.
     *
     */
    private void getCityHotels() {


        for (String hotelPagePath : pages) {
            LOG.debug("Current hotelpage path value is: " + hotelPagePath);
            resource = resourceResolver.getResource(hotelPagePath + "/jcr:content");
            valueMap = resource.adaptTo(ValueMap.class);


            String hotelLocations = valueMap.get("locations", String.class);
            LOG.debug("Current hotelLocations value is: " + hotelLocations);

            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (tagManager == null)
                LOG.debug("tagManager value is null");
            Tag tagRoot = tagManager.resolve(hotelLocations);
            if (tagRoot != null) {
                LOG.debug("tagRoot value is not null");
                tagResource = tagRoot.adaptTo(Resource.class);

                String hotelCity = tagResource.adaptTo(Tag.class).getTitle();
                String hotelName = valueMap.get("hotelName", String.class);


                LOG.debug("show hotel citty and hotel name::" + hotelCity + "::" + hotelName);
                List<String> hotelNameList = new ArrayList<String>();
                if (cityNameHotels.containsKey(hotelCity)) {
                    LOG.debug("inside the if condition");
                    hotelNameList = cityNameHotels.get(hotelCity);
                    hotelNameList.add(hotelPagePath);
                    cityNameHotels.put(hotelCity, hotelNameList);
                } else

                {
                    LOG.debug("inside the else condition");
                    hotelNameList = new ArrayList<String>();
                    hotelNameList.add(hotelPagePath);
                    cityNameHotels.put(hotelCity, hotelNameList);
                }

            } else if (tagRoot == null) {
                LOG.debug("tagRoot value is null");
            }

        }


    }
}
