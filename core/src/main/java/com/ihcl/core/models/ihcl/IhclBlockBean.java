/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclBlockBean {


    private String title;

    private String titleAddOn;


    private String description;
    
    private String longdescription;


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for the field titleAddOn
     *
     * @return the titleAddOn
     */
    public String getTitleAddOn() {
        return titleAddOn;
    }


    /**
     * Setter for the field titleAddOn
     *
     * @param titleAddOn
     *            the titleAddOn to set
     */

    public void setTitleAddOn(String titleAddOn) {
        this.titleAddOn = titleAddOn;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


	public String getLongdescription() {
		return longdescription;
	}


	public void setLongdescription(String longdescription) {
		this.longdescription = longdescription;
	}


}
