package com.ihcl.core.models.room.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.RoomTypeBedType;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.models.room.RoomPropertiesFetcher;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = RoomPropertiesFetcher.class)
public class RoomPropertiesFetcherImpl implements RoomPropertiesFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(RoomPropertiesFetcherImpl.class);

    @Inject
    @Default(values = "")
    private String path;

    @Inject
    private Resource resource;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;

    private List<String> allAmenities;

    private String[] bedAndBathAmenities;

    private String[] entertainment;

    private String[] otherConveniences;

    private String[] wellnessAmenities;

    private String[] suiteFeatures;

    private String[] hotelFacilities;

    int sizeOfAllAmenities;

    int sizeOfBedAndBathAmenities;

    int sizeOfEntertainment;

    int sizeOfOtherConveniences;

    int sizeOfWellnessAmenities;

    int sizeOfSuiteFeatures;

    int sizeOfHotelFacilities;

    private String description;

    private String maxoccupancyDetails;

    private String size;

    private String signatureFeature;

    private String roomTypeName;

    private List<JSONObject> galleryImagePaths;

    private Integer numberOfImages;

    private String epicurePoints;

    private String roomType;

    private String offerSpecifics;

    private String ticPoints;

    private String discountValue;

    private Map<String, String> roomCodeBedTypeMap;

    private String area;

    private String wifi;

    private String maximumNoOfGuests;

    private String numberOfBeds;

    private String maxOccupancy;

    private String bedType;
    
    private String areaImage;
    private String wifiImage;
    private String maxOccupancyImage;
    private String bedTypeImage;
    private String otherAmenity;
    private String otherAmenityImage;

    private String signatureFeatureOfRoom;

    private String[] signatureFeaturesList;

    private List<String> signatureList = new ArrayList<String>();

    private int minimumDaysSelectionForBooking;

    private boolean bookableOnline;

    private String onlineUnavailableMessage;

    private String draggedImagePath;

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received path as: " + path);
        LOG.debug("Received room page resource from injection as: " + resource);
        LOG.debug("Received resource resolver factory as: " + resourceResolverFactory);
        LOG.debug("Received gallery image fetcher service as: " + galleryImageFetcherService);
        buildProperties();
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     *
     */
    private void buildProperties() {
        String methodName = "buildProperties";
        try {
            LOG.trace("Method Entry: " + methodName);
            ResourceResolver resolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver as: " + resolver);
            if ((path != null && !path.isEmpty()) || resource == null) {
                LOG.debug("Attempting to fetch resource from path at: " + path);
                resource = resolver.getResource(path);
            }
            LOG.debug("Received room page resource as: " + resource);
            Resource roomDetailsComponent = getRoomDetailsComponentUnder(resource);
            LOG.debug("Received room details component as: " + roomDetailsComponent);
            fetchPropertiesFrom(roomDetailsComponent);
        } catch (LoginException e) {
            LOG.error("An error occured while building properties for room at the sepcified path", e);
        }
        LOG.trace("Method Exit: " + methodName);

    }

    /**
     * @param roomDetailsComponent
     */
    private void fetchPropertiesFrom(Resource roomDetailsComponent) {
        String methodName = "fetchPropertiesFrom";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = roomDetailsComponent.adaptTo(ValueMap.class);
        LOG.trace("Received value map from room component as: " + valueMap);
        description = getProperty(valueMap, "roomDescription", String.class, "");
        size = getProperty(valueMap, "size", String.class, "");
        signatureFeature = getProperty(valueMap, "signatureFeature", String.class, "");
        maxoccupancyDetails = getProperty(valueMap, "maxoccupancyDetails", String.class, "");
        roomTypeName = getProperty(valueMap, "roomTitle", String.class, "");
        ticPoints = getProperty(valueMap, "ticPoints", String.class, "");
        epicurePoints = getProperty(valueMap, "epicurePoints", String.class, "");
        discountValue = getProperty(valueMap, "discountValue", String.class, "");
        offerSpecifics = getProperty(valueMap, "offerSpecifics", String.class, "");
        maximumNoOfGuests = getProperty(valueMap, "maximumNoOfGuests", String.class, "");
        numberOfBeds = getProperty(valueMap, "numberOfBeds", String.class, "");
        roomType = getProperty(valueMap, "roomType", String.class, "");
        areaImage = getProperty(valueMap, "areaImage", String.class, "");
        wifiImage = getProperty(valueMap, "wifiImage", String.class, "");
        maxOccupancyImage = getProperty(valueMap, "maxOccupancyImage", String.class, "");
        bedTypeImage = getProperty(valueMap, "bedTypeImage", String.class, "");
        otherAmenity = getProperty(valueMap, "otherAmenity", String.class, "");
        otherAmenityImage = getProperty(valueMap, "otherAmenityImage", String.class, "");
        if (roomDetailsComponent.getChild("roomCodes") != null) {
            roomCodeBedTypeMap = fetchRoomCodeBedTypeMap(roomDetailsComponent.getChild("roomCodes"));
        }

        // Other room details
        area = getProperty(valueMap, "area", String.class, "");
        wifi = getProperty(valueMap, "wifi", String.class, "");
        maxOccupancy = getProperty(valueMap, "maxoccupancy", String.class, "");
        bedType = getProperty(valueMap, "bedtype", String.class, "");
        signatureFeatureOfRoom = getProperty(valueMap, "signaturefeatureofroom", String.class, "");
        bookableOnline = !getProperty(valueMap, "onlineBookingUnavailable", Boolean.class, false);
        onlineUnavailableMessage = getProperty(valueMap, "onlineUnavailableMessage", String.class, "");
        minimumDaysSelectionForBooking = getProperty(valueMap, "minDaystoBook", Integer.class, 1);
        draggedImagePath = getProperty(valueMap, "fileReference", String.class, "");

        LOG.trace("Other room details are : Area = " + area + " wifi = " + wifi + " Maximum Ocupancy = " + maxOccupancy
                + " Bed Type = " + bedType + " Signature feature of room = " + signatureFeatureOfRoom);

        String[] signatureFeaturesListArr = getProperty(valueMap, "featureValue", String[].class, new String[] {});
        LOG.trace("Signature features array of the rooms are : " + signatureFeaturesListArr.toString());
        if (null != signatureFeaturesListArr && signatureFeaturesListArr.length > 0) {
            signatureList.addAll(Arrays.asList(signatureFeaturesListArr));
        }

        fetchGroupedServiceAmenities(roomDetailsComponent);
        fetchGalleryImagePaths(roomDetailsComponent);
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param resource
     */
    private void fetchGalleryImagePaths(Resource resource) {
        String methodName = "fetchPropertiesFrom";
        LOG.trace("Method Entry: " + methodName);
        Resource gallery = resource.getChild("gallery");
        if (gallery != null) {
            LOG.debug("Gallery Resource is not null, And it's valueMap is  : " + gallery.getValueMap());
            buildImagePaths(gallery);
        } else if (gallery == null) {
            LOG.debug("Gallery Resource is null, And the method is about to exit : ");
        }

        LOG.trace("Method Exiy: " + methodName);
    }

    /**
     * @param gallery
     */
    private void buildImagePaths(Resource gallery) {
        String methodName = "buildImagePaths";
        LOG.trace("Method Entry: " + methodName);
        galleryImagePaths = galleryImageFetcherService.getGalleryImagePathsAt(gallery);
        if (galleryImagePaths.size() == 0) {
            addDefaultImageJson(galleryImagePaths);
        }
        numberOfImages = galleryImagePaths.size();
        LOG.trace("Method Exit: " + methodName);
    }

    private void addDefaultImageJson(List<JSONObject> galleryImagePaths) {
        String methodName = "buildImagePaths";
        LOG.trace("Method Entry: " + methodName);
        try {
            JSONObject imageObject = new JSONObject();
            imageObject.put("imagePath",
                    "/content/dam/tajhotels/in/en/guest-room-default-image/Guest-rroms-default-img.jpg");
            galleryImagePaths.add(imageObject);
        } catch (JSONException e) {
            LOG.error("An error occured while adding default image to json array.", e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param roomDetailsComponent
     */
    private void fetchGroupedServiceAmenities(Resource roomDetailsComponent) {
        String methodName = "fetchGroupedServiceAmenities";
        LOG.trace("Method Entry: " + methodName);
        Resource serviceAmenitiesNode = roomDetailsComponent.getChild("serviceAmenities");
        if (serviceAmenitiesNode != null) {
            LOG.debug("Found service amenities node at: " + serviceAmenitiesNode);
            ValueMap valueMap = serviceAmenitiesNode.adaptTo(ValueMap.class);
            bedAndBathAmenities = getProperty(valueMap, "bedAndBathExperience", String[].class, new String[] {});
            entertainment = getProperty(valueMap, "entertainment", String[].class, new String[] {});
            otherConveniences = getProperty(valueMap, "otherConveniences", String[].class, new String[] {});
            wellnessAmenities = getProperty(valueMap, "wellnessAmenities", String[].class, new String[] {});
            suiteFeatures = getProperty(valueMap, "suiteFeatures", String[].class, new String[] {});
            hotelFacilities = getProperty(valueMap, "hotelFacilities", String[].class, new String[] {});

            sizeOfOtherConveniences = otherConveniences == null ? 0 : otherConveniences.length;
            sizeOfBedAndBathAmenities = bedAndBathAmenities == null ? 0 : bedAndBathAmenities.length;
            sizeOfEntertainment = entertainment == null ? 0 : entertainment.length;
            sizeOfHotelFacilities = hotelFacilities == null ? 0 : hotelFacilities.length;
            sizeOfSuiteFeatures = suiteFeatures == null ? 0 : suiteFeatures.length;
            sizeOfWellnessAmenities = wellnessAmenities == null ? 0 : wellnessAmenities.length;

            populateAllAmenities();

        }
        LOG.trace("Method Exit: " + methodName);
    }

    public Map<String, String> fetchRoomCodeBedTypeMap(Resource roomCodesList) {
        Map<String, String> map = new HashMap<>();
        Iterable<Resource> roomCodeIter = roomCodesList.getChildren();
        for (Resource roomBedMap : roomCodeIter) {
            RoomTypeBedType roomTypeBedType = roomBedMap.adaptTo(RoomTypeBedType.class);
            map.put(roomTypeBedType.getBedType(), roomTypeBedType.getRoomCode());
        }
        return map;
    }

    private void populateAllAmenities() {
        String methodName = "populateAllAmenities";
        LOG.trace("Method Entry: " + methodName);
        allAmenities = new ArrayList<>();

        addToAllAmenities(bedAndBathAmenities);
        addToAllAmenities(entertainment);
        addToAllAmenities(otherConveniences);
        addToAllAmenities(hotelFacilities);
        addToAllAmenities(suiteFeatures);
        addToAllAmenities(wellnessAmenities);

        LOG.debug("Populated all amenities as: " + allAmenities);
        LOG.trace("Method Exit: " + methodName);
    }

    private void addToAllAmenities(String[] amenities) {
        String methodName = "addToAllAmenities";
        LOG.trace("Method Entry: " + methodName);
        int amenitiesLength = amenities.length;
        LOG.debug("Attempting to add the given amenities array with length: " + amenitiesLength + " to all amenities");
        if (amenities != null && amenitiesLength > 0) {
            allAmenities.addAll(Arrays.asList(amenities));
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param valueMap
     * @param key
     * @param type
     * @param defaultValue
     */
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        String methodName = "getProperty";
        LOG.trace("Method Entry: " + methodName);
        T value;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
            LOG.trace("Value found in value map for key: " + key + " : " + value);
        } else {
            value = defaultValue;
        }
        LOG.trace("Returning value for: " + key + " : " + value);
        LOG.trace("Method Exit: " + methodName);
        return value;
    }

    /**
     * @param root
     * @return
     */
    private Resource getRoomDetailsComponentUnder(Resource root) {
        String methodName = "getRoomDetailsComponentUnder";
        LOG.trace("Method Entry: " + methodName);
        Resource roomComponentResource = null;
        if (root.getResourceType().equals(CrxConstants.ROOM_COMPONENT_RESOURCETYPE)) {
            roomComponentResource = root;
        } else {
            LOG.debug("Attempting to iterate over children of: " + root);
            Iterable<Resource> children = root.getChildren();
            for (Resource child : children) {
                LOG.debug("Iterating over child: " + child);
                if (child.getResourceType().equals(CrxConstants.ROOM_COMPONENT_RESOURCETYPE)) {
                    LOG.debug("Found room component at child: " + child);
                    roomComponentResource = child;
                    break;
                }
                if (roomComponentResource == null) {
                    roomComponentResource = getRoomDetailsComponentUnder(child);
                }
            }
        }
        LOG.trace("Method Exit: " + methodName);
        return roomComponentResource;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getDescription() {
        LOG.trace("Returning room description as: " + description);
        return description;
    }

    @Override
    public String getSize() {
        LOG.trace("Returning room size as: " + size);
        return size;
    }

    @Override
    public String getSignatureFeature() {
        LOG.trace("Returning room signature feature as: " + signatureFeature);
        return signatureFeature;
    }

    @Override
    public String getMaxOccupancyDetails() {
        LOG.trace("Returning room signature feature as: " + maxoccupancyDetails);
        return maxoccupancyDetails;
    }

    @Override
    public String getRoomTypeName() {
        return roomTypeName;
    }

    @Override
    public String[] getServiceAmenities() {
        return otherConveniences;
    }

    @Override
    public String[] getBedAndBathAemenities() {
        return bedAndBathAmenities;
    }

    @Override
    public String[] getEntertainment() {
        return entertainment;
    }

    @Override
    public String[] getEntertainmentAemenities() {
        return hotelFacilities;
    }

    @Override
    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }

    @Override
    public String getHighightImagePath() {
        String methodName = "getHighightImagePath";
        LOG.trace("Method Entry: " + methodName);
        String highlightImagePath = "";
        if (galleryImagePaths != null && galleryImagePaths.size() > 0) {
            try {
                JSONObject highlightImageJson = galleryImagePaths.get(0);
                highlightImagePath = highlightImageJson.getString("imagePath");
            } catch (JSONException e) {
                LOG.error("An errr occured while fetching default highlight image path.", e);
            }
        }
        LOG.trace("Method Exit: " + methodName);
        return highlightImagePath;
    }

    @Override
    public String getDraggedImagePath() {
        return draggedImagePath;
    }

    /**
     * Getter for the field numberOfImages
     *
     * @return the numberOfImages
     */
    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    /**
     * Getter for the field Epicure
     *
     * @return the Epicure Points
     */
    public String getEpicurePoints() {
        return epicurePoints;
    }

    /**
     * Getter for the field OfferSpecifics
     *
     * @return the OfferSpecifics Points
     */
    public String getOfferSpecifics() {
        return offerSpecifics;
    }


    /**
     * Getter for the field maximumNoOfGuests
     *
     * @return the maximumNoOfGuests
     */
    public String getMaximumNoOfGuests() {
        return maximumNoOfGuests;
    }


    public String getRoomType() {
        return roomType;
    }

    /**
     * Getter for the field numberOfBeds
     *
     * @return the numberOfBeds
     */
    public String getNumberOfBeds() {
        return numberOfBeds;
    }

    public String getTicPoints() {
        return ticPoints;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    @Override
    public List<String> getAllAmenities() {
        return allAmenities;
    }

    @Override
    public String[] getBedAndBathAmenities() {
        return bedAndBathAmenities;
    }

    @Override
    public String[] getOtherConveniences() {
        return otherConveniences;
    }

    @Override
    public String[] getWellnessAmenities() {
        return wellnessAmenities;
    }

    @Override
    public String[] getSuiteFeatures() {
        return suiteFeatures;
    }

    @Override
    public String[] getHotelFacilities() {
        return hotelFacilities;
    }

    public int getSizeOfAllAmenities() {
        return sizeOfAllAmenities;
    }

    public int getSizeOfBedAndBathAmenities() {
        return sizeOfBedAndBathAmenities;
    }

    public int getSizeOfEntertainment() {
        return sizeOfEntertainment;
    }

    public int getSizeOfOtherConveniences() {
        return sizeOfOtherConveniences;
    }

    public int getSizeOfWellnessAmenities() {
        return sizeOfWellnessAmenities;
    }

    public int getSizeOfSuiteFeatures() {
        return sizeOfSuiteFeatures;
    }

    public int getSizeOfHotelFacilities() {
        return sizeOfHotelFacilities;
    }

    public Map<String, String> getRoomCodeBedTypeMap() {
        return roomCodeBedTypeMap;
    }

    @Override
    public String getRoomCodeBedTypeMapString() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(roomCodeBedTypeMap);
    }

    @Override
    public String getArea() {
        return area;
    }

    @Override
    public String getWifi() {
        return wifi;
    }

    @Override
    public String getMaxOccupancy() {
        return maxOccupancy;
    }

    @Override
    public String getBedType() {
        return bedType;
    }

    @Override
    public String getSignatureFeatureOfRoom() {
        return signatureFeatureOfRoom;
    }

    @Override
    public String[] getSignatureFeaturesList() {
        return signatureFeaturesList;
    }

    @Override
    public List<String> getSignatureList() {
        return signatureList;
    }

    @Override
    public boolean getBookableOnline() {
        return bookableOnline;
    }

    @Override
    public String getOnlineUnavailableMessage() {
        return onlineUnavailableMessage;
    }

    @Override
    public int getMinimumDaysSelectionForBooking() {
        return minimumDaysSelectionForBooking;
    }

	@Override
	public String getAreaImage() {
		return areaImage;
	}

	@Override
	public String getWifiImage() {
		return wifiImage;
	}

	@Override
	public String getMaxOccupancyImage() {
		return maxOccupancyImage;
	}

	@Override
	public String getBedTypeImage() {
		return bedTypeImage;
	}

	@Override
	public String getOtherAmenity() {
		return otherAmenity;
	}

	@Override
	public String getOtherAmenityImage() {
		return otherAmenityImage;
	}
}
