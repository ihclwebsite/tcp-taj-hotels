/**
 *
 */
package com.ihcl.core.models.jivaspa.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.models.jivaspa.LocateJivaSpaService;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = LocateJivaSpaService.class)
public class LocateJivaSpaServiceImpl implements LocateJivaSpaService {

    private static final Logger LOG = LoggerFactory.getLogger(LocateJivaSpaServiceImpl.class);

    @Inject
    private Resource resource;

    @Inject
    HotelsSearchService hotelSearchService;

    @Reference
    private ResourceResolver resourceResolver;

    protected static final String RESOURCE_JIVA_SPA_LINK = "/jiva-spa.html";

    protected static final String RESOURCE_NAME_HOTEL_BANNER = "bannerImage";

    protected static final String JIVA_SPA_TAG = "taj:treatment-and-therapies";

    protected static final String TAJ_ROOT_PATH = "/content/tajhotels/en-in";

    List<ParticipatingHotel> hotelList = new ArrayList<>();


    @PostConstruct
    public void init() {
        locateJivaSpa();
    }

    @Override
    public List<ParticipatingHotel> locateJivaSpa() {
        ValueMap vMap = resource.adaptTo(ValueMap.class);
        String homePagePath = null != vMap.get("homePagePath", String.class) ? vMap.get("homePagePath", String.class)
                : TAJ_ROOT_PATH;
        try {
            SearchResult searchResult = hotelSearchService.getHotelsByTeatments(homePagePath);
            String hotelPath;
            String imageUrl;
            LOG.trace("Search reslt size: {}", searchResult.getHits().size());
            for (Hit hit : searchResult.getHits()) {
                LocateJivaSpaBean resultObj = new LocateJivaSpaBean();
                resourceResolver = resource.getResourceResolver();
                Resource resultResource = resourceResolver.getResource(hit.getPath());
                if (null != resultResource) {
                    ParticipatingHotel resultHotel = resultResource.adaptTo(ParticipatingHotel.class);
                    if (null != resultHotel) {
                        imageUrl = extractBannerImage(resultResource);
                        if (null != imageUrl) {
                            resultHotel.setImageUrl(imageUrl);
                        }
                        hotelPath = resultHotel.getHotelPath().replaceAll(".html", "").concat(RESOURCE_JIVA_SPA_LINK);
                        if (hotelPath.contains("//jiva-spa.html")) {
                            hotelPath = hotelPath.replace("//jiva-spa.html", RESOURCE_JIVA_SPA_LINK);
                        }
                        resultHotel.setJivaSpaPath(hotelPath);
                        if (null != resultHotel.getJivaSpaEmail()) {
                            resultObj.setHotelEmailId(resultHotel.getJivaSpaEmail());
                        } else {
                            resultObj.setHotelEmailId(resultHotel.getHotelEmail());
                        }
                        hotelList.add(resultHotel);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error("Error while searching for hotels", e);
        }
        return hotelList;
    }


    private String extractBannerImage(Resource resource) {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (null != bannerParsys) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (null != hotelBanner) {
                ValueMap valMap = hotelBanner.getValueMap();
                return String.valueOf(valMap.get(RESOURCE_NAME_HOTEL_BANNER));
            }
            LOG.debug("Returning null [condition failed], hotelBanner is null");
            return null;
        }
        LOG.debug("Returning null [condition failed], bannerParsys is null");
        return null;
    }


    public List<ParticipatingHotel> getHotelList() {
        return hotelList;
    }

}
