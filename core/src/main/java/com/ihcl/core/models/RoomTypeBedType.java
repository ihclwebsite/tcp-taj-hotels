package com.ihcl.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class RoomTypeBedType {

    @Inject
    @Optional
    private String bedType;

    @Inject
    private String roomCode;

    public String getBedType() {
        return bedType;
    }

    public String getRoomCode() {
        return roomCode;
    }
}
