/**
 *
 */
package com.ihcl.core.models.ihcl;


public class IhclprofileBean {


    private String name;

    private String image;

    private String designation;

    private String description;

    private String id;


    /**
     * Getter for the field name
     *
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * Setter for the field name
     *
     * @param name
     *            the name to set
     */

    public void setName(String name) {
        this.name = name;
    }


    /**
     * Getter for the field image
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }


    /**
     * Setter for the field image
     *
     * @param image
     *            the image to set
     */

    public void setImage(String image) {
        this.image = image;
    }


    /**
     * Getter for the field description
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * Setter for the field description
     *
     * @param description
     *            the description to set
     */

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the field id
     *
     * @return the id
     */
    public String getId() {
        return id;
    }


    /**
     * Setter for the field id
     *
     * @param id
     *            the id to set
     */

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Getter for the field designation
     *
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }


    /**
     * Setter for the field designation
     *
     * @param designation
     *            the designation to set
     */

    public void setDesignation(String designation) {
        this.designation = designation;
    }


}
