package com.ihcl.core.models.holidays.hotel;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class HotelDetails {

	private String hotelName;

	private String hotelRoomsPath;

	private String hotelPath;

	private String hotelDescription;

	private String hotelImagePath;

	private String hotelLatitude;

	private String hoteLongitude;

	private String hotelArea;

	private String hotelCity;

	private String hotelLocationStat;

	private String hotelAddress;

	private String hotelDiscountedRate;

	private String hotelActualRate;
	
	private String currencySymbol;

	private String ratePerNightlabel;

	private String ticPoints;

	private String epicurePoints;

	private List<UserSellingPoints> uspLs;

	private List<HolidayPackage> packages;

	private String countryName;

	private Map<String, Set<String>> destinationTags;

	private Set<String> experiencesTags;

	private Set<String> themesTags;

	private List<String> holidayOfferTag;
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelRoomsPath() {
		return hotelRoomsPath;
	}

	public void setHotelRoomsPath(String hotelRoomsPath) {
		String roomsPath = hotelRoomsPath + ".html";
		this.hotelRoomsPath = roomsPath;
	}

	public String getHotelDescription() {
		return hotelDescription;
	}

	public void setHotelDescription(String hotelDescription) {
		this.hotelDescription = hotelDescription;
	}

	public String getHotelImagePath() {
		return hotelImagePath;
	}

	public void setHotelImagePath(String hotelImagePath) {
		this.hotelImagePath = hotelImagePath;
	}

	public String getHotelLatitude() {
		return hotelLatitude;
	}

	public void setHotelLatitude(String hotelLatitude) {
		this.hotelLatitude = hotelLatitude;
	}

	public String getHotelAddress() {
		return hotelAddress;
	}

	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}

	public String getHoteLongitude() {
		return hoteLongitude;
	}

	public void setHoteLongitude(String hoteLongitude) {
		this.hoteLongitude = hoteLongitude;
	}

	public String getHotelDiscountedRate() {
		return hotelDiscountedRate;
	}

	public void setHotelDiscountedRate(String hotelDiscountedRate) {
		this.hotelDiscountedRate = hotelDiscountedRate;
	}

	public String getHotelActualRate() {
		return hotelActualRate;
	}

	public void setHotelActualRate(String hotelActualRate) {
		this.hotelActualRate = hotelActualRate;
	}

	public String getRatePerNightlabel() {
		return ratePerNightlabel;
	}

	public void setRatePerNightlabel(String ratePerNightlabel) {
		this.ratePerNightlabel = ratePerNightlabel;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public String getTicPoints() {
		return ticPoints;
	}

	public void setTicPoints(String ticPoints) {
		this.ticPoints = ticPoints;
	}

	public String getEpicurePoints() {
		return epicurePoints;
	}

	public void setEpicurePoints(String epicurePoints) {
		this.epicurePoints = epicurePoints;
	}

	public List<HolidayPackage> getPackages() {
		return packages;
	}

	public void setPackages(List<HolidayPackage> packages) {
		this.packages = packages;
	}

	public String getHotelPath() {
		return hotelPath;
	}

	public void setHotelPath(String hotelPath) {
		this.hotelPath = hotelPath;
	}

	public List<UserSellingPoints> getUspLs() {
		return uspLs;
	}

	public void setUspLs(List<UserSellingPoints> uspLs) {
		this.uspLs = uspLs;
	}

	public String getHotelArea() {
		return hotelArea;
	}

	public String getHotelCity() {
		return hotelCity;
	}

	public void setHotelArea(String hotelArea) {
		this.hotelArea = hotelArea;
	}

	public void setHotelCity(String hotelCity) {
		this.hotelCity = hotelCity;
	}

	public String getHotelLocationStat() {
		return hotelLocationStat;
	}

	public void setHotelLocationStat(String hotelLocationStat) {
		this.hotelLocationStat = hotelLocationStat;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Map<String, Set<String>> getDestinationTags() {
		return destinationTags;
	}

	public void setDestinationTags(Map<String, Set<String>> destinationTags) {
		this.destinationTags = destinationTags;
	}

	public Set<String> getExperiencesTags() {
		return experiencesTags;
	}

	public void setExperiencesTags(Set<String> experiencesTags) {
		this.experiencesTags = experiencesTags;
	}

	public Set<String> getThemesTags() {
		return themesTags;
	}

	public void setThemesTags(Set<String> themesTags) {
		this.themesTags = themesTags;
	}

	public List<String> getHolidayOfferTag() {
		return holidayOfferTag;
	}

	public void setHolidayOfferTag(List<String> holidayOfferTag) {
		this.holidayOfferTag = holidayOfferTag;
	}

}
