/**
 * 
 */
package com.ihcl.core.models;

/**
 * @author TCS
 *
 */
public class RestaurantDataBean {
	//parent page properties
	private String hotelName;
	private String hotelId;
	private String hotelLatitude;
	private String hotelLongitude;
	
	// page properties
	private String pageTitle;
	private String areaCode;
	private String jcr_description;
	private String jcr_title;
	private String navTitle;
	private String ogSitename;
	private String rateReview;
	private String restaurantId;
	private String searchBoost;
	private String seoDescription;
	private String seoKeywords;
	private String servesCuisine;
	private String jcr_canonical;
	private String restaurantPath;
	
	// from component
	private String currencySymbol;
	private String averagePrice;
	private String diningDescription;
	private String diningID;
	private String diningImage;
	private String diningLongDescription;
	private String diningName;
	private String diningPhoneNumber;
	private String dinnerTime;
	private String dressCode;
	private String lunchTime;
	private String pathToDownloadFrom;
	private String timingslabel1;
	private String timingsvalue1;
	private String diningCuisine;
	
	
	
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getDiningLongDescription() {
		return diningLongDescription;
	}
	public void setDiningLongDescription(String diningLongDescription) {
		this.diningLongDescription = diningLongDescription;
	}
	public String getDiningCuisine() {
		return diningCuisine;
	}
	public void setDiningCuisine(String diningCuisine) {
		this.diningCuisine = diningCuisine;
	}
	public String getDiningDescription() {
		return diningDescription;
	}
	public void setDiningDescription(String diningDescription) {
		this.diningDescription = diningDescription;
	}
	public String getDiningPhoneNumber() {
		return diningPhoneNumber;
	}
	public void setDiningPhoneNumber(String diningPhoneNumber) {
		this.diningPhoneNumber = diningPhoneNumber;
	}
	public String getDiningName() {
		return diningName;
	}
	public void setDiningName(String diningName) {
		this.diningName = diningName;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getJcr_description() {
		return jcr_description;
	}
	public void setJcr_description(String jcr_description) {
		this.jcr_description = jcr_description;
	}
	public String getOgSitename() {
		return ogSitename;
	}
	public void setOgSitename(String ogSitename) {
		this.ogSitename = ogSitename;
	}
	public String getRateReview() {
		return rateReview;
	}
	public void setRateReview(String rateReview) {
		this.rateReview = rateReview;
	}
	public String getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getSearchBoost() {
		return searchBoost;
	}
	public void setSearchBoost(String searchBoost) {
		this.searchBoost = searchBoost;
	}
	public String getSeoDescription() {
		return seoDescription;
	}
	public void setSeoDescription(String seoDescription) {
		this.seoDescription = seoDescription;
	}
	public String getSeoKeywords() {
		return seoKeywords;
	}
	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}
	public String getServesCuisine() {
		return servesCuisine;
	}
	public void setServesCuisine(String servesCuisine) {
		this.servesCuisine = servesCuisine;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	public String getAveragePrice() {
		return averagePrice;
	}
	public void setAveragePrice(String averagePrice) {
		this.averagePrice = averagePrice;
	}
	public String getDiningID() {
		return diningID;
	}
	public void setDiningID(String diningID) {
		this.diningID = diningID;
	}
	public String getDiningImage() {
		return diningImage;
	}
	public void setDiningImage(String diningImage) {
		this.diningImage = diningImage;
	}
	public String getDinnerTime() {
		return dinnerTime;
	}
	public void setDinnerTime(String dinnerTime) {
		this.dinnerTime = dinnerTime;
	}
	public String getDressCode() {
		return dressCode;
	}
	public void setDressCode(String dressCode) {
		this.dressCode = dressCode;
	}
	public String getLunchTime() {
		return lunchTime;
	}
	public void setLunchTime(String lunchTime) {
		this.lunchTime = lunchTime;
	}
	public String getPathToDownloadFrom() {
		return pathToDownloadFrom;
	}
	public void setPathToDownloadFrom(String pathToDownloadFrom) {
		this.pathToDownloadFrom = pathToDownloadFrom;
	}
	public String getTimingslabel1() {
		return timingslabel1;
	}
	public void setTimingslabel1(String timingslabel1) {
		this.timingslabel1 = timingslabel1;
	}
	public String getTimingsvalue1() {
		return timingsvalue1;
	}
	public void setTimingsvalue1(String timingsvalue1) {
		this.timingsvalue1 = timingsvalue1;
	}
	public String getJcr_title() {
		return jcr_title;
	}
	public void setJcr_title(String jcr_title) {
		this.jcr_title = jcr_title;
	}
	public String getNavTitle() {
		return navTitle;
	}
	public void setNavTitle(String navTitle) {
		this.navTitle = navTitle;
	}
	public String getJcr_canonical() {
		return jcr_canonical;
	}
	public void setJcr_canonical(String jcr_canonical) {
		this.jcr_canonical = jcr_canonical;
	}
	public String getHotelLatitude() {
		return hotelLatitude;
	}
	public void setHotelLatitude(String hotelLatitude) {
		this.hotelLatitude = hotelLatitude;
	}
	public String getHotelLongitude() {
		return hotelLongitude;
	}
	public void setHotelLongitude(String hotelLongitude) {
		this.hotelLongitude = hotelLongitude;
	}
	public String getRestaurantPath() {
		return restaurantPath;
	}
	public void setRestaurantPath(String restaurantPath) {
		this.restaurantPath = restaurantPath;
	}
}
