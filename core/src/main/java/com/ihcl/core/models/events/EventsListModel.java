package com.ihcl.core.models.events;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.search.SearchProvider;

/**
 * <pre>
 * EventsListModel.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :03-Dec-2019
 * @ClassName : EventsListModel.java
 * @Description : com.ihcl.core.events -EventsListModel.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  03-Dec-2019     Subharun Mukherjee            Create
 *
 *               </pre>
 */

@Model(adaptables = Resource.class,
        adapters = EventsListModel.class)
public class EventsListModel {

    private static final Logger LOG = LoggerFactory.getLogger(EventsListModel.class);

    private static final String RESOURCE_TYPE = "tajhotels/components/content/taj-events";

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    @Optional
    private String parentPage;

    @Inject
    private SearchProvider searchProvider;

    public List<EventsDetailsBean> getEventList() {

        List<EventsDetailsBean> eventList = new ArrayList<>();
        ResourceResolver resolver = null;
        try {
            resolver = resourceResolverFactory.getServiceResourceResolver(null);
            SearchResult results = null;
            results = getSearchResult(parentPage);
            if (null != results) {
                List<Hit> hits = results.getHits();
                int size = hits.size();
                LOG.trace("Result size : {}", size);
                for (int i = 0; i < size; i++) {
                    Hit hit = hits.get(i);
                    EventsDetailsBean eventBean = getEventBean(resolver, hit);
                    eventList.add(eventBean);
                }
            }
        } catch (Exception e) {
            LOG.error("Error getting resolver : {}", e.getMessage());
            LOG.debug("Error getting resolver : {}", e);
        } finally {
            if (null != resolver) {
                resolver.close();
            }
        }
        return eventList;

    }

    /**
     * @param resolver
     * @param hit
     * @param eventBean
     * @return
     */
    private EventsDetailsBean getEventBean(ResourceResolver resolver, Hit hit) {
        EventsDetailsBean eventBean = null;
        try {
            Resource resultResource = resolver.getResource(hit.getPath());
            if (null != resultResource) {
                ValueMap valueMap = resultResource.adaptTo(ValueMap.class);
                if (null != valueMap) {

                    String title = getValueFrom(valueMap, "eventTitle", String.class, "");

                    String description = getValueFrom(valueMap, "description", String.class, "");

                    String location = getValueFrom(valueMap, "eventDetailedLocation", String.class, "");

                    String productName = getValueFrom(valueMap, "productName", String.class, "");

                    String startDate = getDateString(getValueFrom(valueMap, "eventstartDate", Date.class, null),
                            "E, dd MMM yyyy | hh:mm aa", "");

                    String endDate = getDateString(getValueFrom(valueMap, "eventendDate", Date.class, null), "hh:mm aa",
                            "Onwards");

                    String viewDetailButton = getValueFrom(valueMap, "viewDetailButton", String.class, "");

                    String imagePath = getValueFrom(valueMap, "eventsImage", String.class, "");

                    String city = getValueFrom(valueMap, "eventLocation", String.class, "");

                    String eventUrl = getValueFrom(valueMap, "eventUrl", String.class, "");

                    String ticketsAvailable = getValueFrom(valueMap, "ticketsAvail", String.class, "");

                    String eventPrice = getValueFrom(valueMap, "eventPrice", String.class, "");

                    String propertyCode = getValueFrom(valueMap, "propertyCode", String.class, "");

                    eventBean = new EventsDetailsBean(title, description, productName, startDate, viewDetailButton,
                            imagePath, city, eventUrl, ticketsAvailable, endDate, eventPrice, location, propertyCode);
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Error while getting event results : {}", e.getMessage());
            LOG.debug("Error while getting event results : {}", e);
        }
        return eventBean;
    }

    /**
     * @param valueMap
     * @param string
     * @param type
     * @param blank
     * @return
     */
    private <T> T getValueFrom(ValueMap valueMap, String string, Class<T> type, T blank) {
        T value = valueMap.get(string, type);
        return null != value ? value : blank;
    }

    /**
     * @param date
     * @param format
     * @param string
     * @return
     */
    private String getDateString(Date date, String format, String string) {
        if (null != date) {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            return formatter.format(date);
        } else
            return string;
    }

    private SearchResult getSearchResult(String searchPath) {
        HashMap<String, String> predicateMap = new HashMap<>();
        try {
            predicateMap.put("path", searchPath);
            predicateMap.put("1_property", "sling:resourceType");
            predicateMap.put("1_property.value", RESOURCE_TYPE);
            predicateMap.put("p.limit", "-1");
            predicateMap.put("orderby", "@eventstartDate");
            predicateMap.put("orderby.sort", "asc");
        } catch (Exception e) {
            LOG.error("Error while fetching activity results : {}", e.getMessage());
            LOG.debug("Error while fetching activity results : {}", e);
        }
        return searchProvider.getQueryResult(predicateMap);
    }
}
