package com.ihcl.core.models.holidays.explore;

public class Spa {
	
	private String hotelName;
	
	private String hotelLatitude;
	
	private String hotelLongitude;
	
	private String hotelAddress;
	
	private String spaDesciption;
	
	private String spaTiming;
	
	private String hotelImage;
	
	private String uniqueName;

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelLatitude() {
		return hotelLatitude;
	}

	public void setHotelLatitude(String hotelLatitude) {
		this.hotelLatitude = hotelLatitude;
	}

	public String getHotelLongitude() {
		return hotelLongitude;
	}

	public void setHotelLongitude(String hotelLongitude) {
		this.hotelLongitude = hotelLongitude;
	}

	public String getSpaDesciption() {
		return spaDesciption;
	}

	public void setSpaDesciption(String spaDesciption) {
		this.spaDesciption = spaDesciption;
	}

	public String getSpaTiming() {
		return spaTiming;
	}

	public void setSpaTiming(String spaTiming) {
		this.spaTiming = spaTiming;
	}

	public String getHotelImage() {
		return hotelImage;
	}

	public void setHotelImage(String hotelImage) {
		this.hotelImage = hotelImage;
	}

	public String getUniqueName() {
		return uniqueName;
	}

	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

	public String getHotelAddress() {
		return hotelAddress;
	}

	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	
	

}
