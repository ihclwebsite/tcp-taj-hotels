package com.ihcl.core.models.room;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.ihcl.core.models.room.impl.RateFilterBean;

@ConsumerType
public interface RateFiltersModel {


    /**
     * @return
     */
    List<RateFilterBean> getRateFilters();

}
