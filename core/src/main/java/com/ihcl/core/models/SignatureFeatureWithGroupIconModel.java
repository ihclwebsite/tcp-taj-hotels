package com.ihcl.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class)
public class SignatureFeatureWithGroupIconModel {

    @Inject
    private String signatureFeatureValue;


    public void setSignatureFeatureValue(String signatureFeatureValue) {
        this.signatureFeatureValue = signatureFeatureValue;
    }


    public void setGroupIconPath(String groupIconPath) {
        this.groupIconPath = groupIconPath;
    }

    @Inject
    private String groupIconPath;

    public String getSignatureFeatureValue() {
        return signatureFeatureValue;
    }

    public String getGroupIconPath() {
        return groupIconPath;
    }

}
