package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class)
public class Gallery {

    private static final Logger LOG = LoggerFactory.getLogger(Gallery.class);

    @Inject
    @Optional
    private String galleryTitle;

    @Inject
    @Optional
    private String galleryPath;


    @PostConstruct
    public void activate() {
        LOG.info("INSIDE ACTIVATE OF Gallery");

    }

    public String getGalleryPath() {
        return galleryPath;
    }

    public String getGalleryTitle() {
        return galleryTitle;
    }


}
