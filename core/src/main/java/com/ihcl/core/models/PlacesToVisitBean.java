package com.ihcl.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(
        adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PlacesToVisitBean {

    private String conciereUniqueName;

    @Inject
    private String placeName;

    @Inject
    private String latitude;

    @Inject
    private String longitude;

    @Inject
    private String address;

    @Inject
    private String phoneNo;

    @Inject
    private String mailId;

    @Inject
    private String openingTime;

    @Inject
    private String closingTime;

    @Inject
    private String description;

    @Inject
    private String name;

    @Inject
    private String contactNo;

    @Inject
    private String contactMailId;

    @Inject
    private String contactType;

    @Inject
    private String locationImage;
    
    @Inject
    private String fileReference;


    public String getPlaceName() {
        return placeName;
    }


    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }


    public String getAddress() {
        return address;
    }


    public String getLatitude() {
        return latitude;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getMailId() {
        return mailId;
    }


    public String getClosingTime() {
        return closingTime;
    }

    public String getDescription() {
        return description;
    }

    public String getContactName() {
        return name;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getContactMailId() {
        return contactMailId;
    }


    public String getOpeningTime() {
        return openingTime;
    }

    public String getContactType() {
        return contactType;
    }

    public String getLocationImage() {
        return locationImage;
    }
    
    public String getDraggedImagePath() {
        return fileReference;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getConciereUniqueName() {
        conciereUniqueName = placeName.replaceAll("\\s", "");
        return conciereUniqueName;
    }

}
