package com.ihcl.core.models.holiday.footer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.models.FooterLink;
import com.ihcl.core.services.search.DestinationSearchService;
import com.ihcl.core.util.URLMapUtil;

@Model(adaptables = Resource.class)
public class HolidayFooter {

    protected static final Logger LOG = LoggerFactory.getLogger(HolidayFooter.class);

    @Inject
    @Optional
    private List<FooterLink> footerLinks;

    @Inject
    @Optional
    private List<FooterLink> policyLinks;

    @Inject
    @Optional
    private List<FooterLink> brandLinks;

    @Inject
    @Optional
    private List<FooterLink> ourBrandLinks;

    @Inject
    @Optional
    private List<FooterLink> companyLinks;

    @Inject
    @Optional
    private List<FooterLink> developmentLinks;

    @Inject
    @Optional
    private List<FooterLink> responsibilityLinks;

    @Inject
    @Optional
    private List<FooterLink> careerLinks;

    @Inject
    @Optional
    private List<FooterLink> newsRoomLinks;

    @Inject
    @Optional
    private List<FooterLink> homeLinks;

    @Inject
    @Optional
    private List<FooterLink> contactLinks;

    @Inject
    @Optional
    private List<FooterLink> investorsLinks;

    private List<String> destinations;

    @Inject
    private DestinationSearchService destinationSearchService;

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    private ResourceResolver resourceResolver = null;

    Map<String, List<HolidayDestination>> countryMap;

    private static final String COUNTRY_TYPE_ALL = "all";

    private static final String COUNTRY_TYPE_INDIA = "India";

    private static final String COUNTRY_TYPE_EXCEPT_INDIA = "except_india";

    private static final String PAGE_RESOURCE_TYPE = "cq:Page";

    public Map<String, List<HolidayDestination>> getCountryMap() {
        return countryMap;
    }

    @PostConstruct
    public void init() throws LoginException {
        LOG.trace("Method entry -> init()");
        destinations = new ArrayList<>();
        HolidayFooter footerObject = new HolidayFooter();
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        } catch (LoginException e) {
            LOG.error("LoginException occured in init method :: {}", e.getMessage());
        }
        LOG.trace("resource resolver  {}", resourceResolver);
        destinations = destinationSearchService.getAllHolidayDestinations();
        countryMap = footerObject.getHolidayDestinationModelMap(destinations, resourceResolver);

        LOG.trace("destinationModel Map size : " + countryMap.size());
        LOG.trace("Method exit -> init()");
    }

    public List<FooterLink> getFooterLinks() {
        return footerLinks;
    }

    public List<FooterLink> getPolicyLinks() {
        return policyLinks;
    }

    public List<FooterLink> getBrandLinks() {
        return brandLinks;
    }

    public List<String> getDestinations() {
        return destinations;
    }

    public static Logger getLog() {
        return LOG;
    }

    public List<FooterLink> getOurBrandLinks() {
        return ourBrandLinks;
    }

    public List<FooterLink> getCompanyLinks() {
        return companyLinks;
    }

    public List<FooterLink> getDevelopmentLinks() {
        return developmentLinks;
    }

    public List<FooterLink> getResponsibilityLinks() {
        return responsibilityLinks;
    }

    public List<FooterLink> getCareerLinks() {
        return careerLinks;
    }

    public List<FooterLink> getNewsRoomLinks() {
        return newsRoomLinks;
    }

    public List<FooterLink> getHomeLinks() {
        return homeLinks;
    }

    public List<FooterLink> getContactLinks() {
        return contactLinks;
    }

    public List<FooterLink> getInvestorsLinks() {
        return investorsLinks;
    }

    public String getCountryFromTags(Resource hotelPageResource, ResourceResolver resourceResolver) {
        LOG.trace("Method Entry: getCountry()");
        LOG.debug("resource for country : {}", hotelPageResource);
        String countryName = "";
        ValueMap map = hotelPageResource.adaptTo(ValueMap.class);
        List<String> city = Arrays.asList(map.get("city", String[].class));
        LOG.debug("tags found for country :{}", city);

        LOG.debug("destination resourceResolver : {}", resourceResolver);
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            LOG.trace("city array before if:" + city.get(0));
            if (city != null) {
                LOG.trace("city array :" + city.get(0));
                LOG.trace("TagManager resolve :" + tagManager.resolve(city.get(0)));
                tagRoot = tagManager.resolve(city.get(0));
                if (tagRoot != null) {
                    LOG.trace("Tag Root Path : " + tagRoot.getPath());
                    countryName = tagRoot.getParent().getTitle();
                }
            }
        }
        LOG.trace("Method Exit -> returning  : Country name : " + countryName);
        return countryName;
    }

    public Map<String, List<HolidayDestination>> getHolidayDestinationModelMap(List<String> destinationsPaths,
            ResourceResolver resourceResolver) {
        Map<String, List<HolidayDestination>> countryMap = new HashMap<>();
        List<HolidayDestination> allDestinationsList = new ArrayList<>();
        List<HolidayDestination> indianDestinationsList = new ArrayList<>();
        List<HolidayDestination> nonIndianDestinationsList = new ArrayList<>();

        allDestinationsList = getAllHolidayDestinationModels(destinationsPaths, resourceResolver);
        countryMap.put(COUNTRY_TYPE_ALL, allDestinationsList);
        LOG.debug("Found all the destinations models");
        try {
            for (HolidayDestination child : allDestinationsList) {
                LOG.debug("destination checking for country: {}, {}", child.getDestinationCountry(),
                        child.getDestinationName());
                if (child.getDestinationCountry().equalsIgnoreCase(COUNTRY_TYPE_INDIA)) {
                    LOG.debug("the destination name here:{}", child.getDestinationName());
                    indianDestinationsList.add(child);
                } else {
                    nonIndianDestinationsList.add(child);
                }
            }
            countryMap.put(COUNTRY_TYPE_INDIA, indianDestinationsList);
            countryMap.put(COUNTRY_TYPE_EXCEPT_INDIA, nonIndianDestinationsList);
            LOG.trace("built the country wise destination Map and returning it");
        } catch (Exception e) {
            LOG.error("Error while fetching all destinationModels", e);
        }
        return countryMap;

    }

    public List<HolidayDestination> getAllHolidayDestinationModels(List<String> destinationsPathList,
            ResourceResolver resourceResolver) {
        Resource destResource = null;

        List<HolidayDestination> destinationsList = new ArrayList<>();
        try {

            for (String rPath : destinationsPathList) {
                destResource = resourceResolver.getResource(rPath);
                LOG.trace("destination Res ValueMap :" + destResource.getChild("jcr:content").getValueMap());
                ValueMap destinationMap = destResource.getChild("jcr:content").adaptTo(ValueMap.class);
                HolidayDestination destinationModel = new HolidayDestination();

                destinationModel.setDestinationName(destinationMap.get("destinationName", String.class));
                String packagepath = getPackagePagePath(destResource);
                destinationModel.setDestinationPath(packagepath);

                String resolvedDestinationPath = getMappedPath(resourceResolver, destinationModel.getDestinationPath());
                destinationModel.setResolvedDestinationPath(resolvedDestinationPath);

                HolidayFooter footerObject = new HolidayFooter();
                Resource resource = resourceResolver.getResource(rPath + "/jcr:content");
                String country = footerObject.getCountryFromTags(resource, resourceResolver);
                LOG.debug("Fething country from tag Method : {}", country);
                destinationModel.setDestinationCountry(country);
                destinationsList.add(destinationModel);
            }
        } catch (Exception e) {
            LOG.error("Error while fetching all destinationModels", e);
        }
        return destinationsList;
    }

    private String getPackagePagePath(Resource destResource) {

        LOG.debug("Inside method: getPackagePagePath()");
        LOG.debug("Resource path: {}", destResource.getPath());
        String pathForPackage = "";
        Iterable<Resource> children = destResource.getChildren();
        for (Resource child : children) {

            if (child.getResourceType().equals(PAGE_RESOURCE_TYPE) && child.getPath().contains("packages")) {
                LOG.debug("resource type: {}", child.getResourceType());
                pathForPackage = child.getPath();
            }
        }
        LOG.debug("Leaving method: getPackagePagePath():: returning path: {}", pathForPackage);
        return pathForPackage;
    }

    private String getMappedPath(ResourceResolver resourceResolver, String url) {
        String methodName = "getMappedPath";
        LOG.debug("Method Entry :: " + methodName);
        if (null != url) {
            LOG.debug("Recieved resolvedResolver : " + resourceResolver);
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            LOG.trace("Resolved URL " + resolvedURL);
            LOG.debug("Method Exit :: " + methodName);
            return URLMapUtil.getPathFromURL(resolvedURL);
        }
        LOG.trace("URL unresolved , hence returning  :-" + url);
        LOG.debug("Method Exit :: " + methodName);
        return url;
    }
}
