package com.ihcl.core.models.offers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.ihcl.core.services.search.DestinationSearchService;
import com.ihcl.core.services.search.GlobalSearchService;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.core.services.search.OffersSearchService;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = OfferList.class)
public class OfferListImpl implements OfferList {

	private static final Logger LOG = LoggerFactory.getLogger(OfferListImpl.class);
	private final String FILTER_TYPE_PARAM = "filterType";
	private final String FILTER_VALUE_PARAM = "filterValue";

	Resource resource;

	@Inject
	GlobalSearchService globalSearchService;

	@Inject
	DestinationSearchService destinationSearchService;

	@Inject
	OffersSearchService offersSearchService;

	@Inject
	HotelsSearchService hotelSearchService;

	@Self
	SlingHttpServletRequest request;

	List<Page> offerList;

	Map<String, String> countryMap;

	HashMap<String, String> destinationMap;

	Map<String, String> hotelMap;

	HashMap<String, String> offerTypes;

	@Inject
	SearchProvider searchProvider;

	@Inject
	@Optional
	String destinationPage;

	@Inject
	@Optional
	String hotelPath;

	@Inject
	@Default(values="/content/tajhotels/en-in")
	String homePagePath;
	ResourceResolver resourceResolver;

	ValueMap vMap= null;

	@PostConstruct
	public void initModel() {
		
		String country = request.getParameter("country");
		String hotel = request.getParameter("hotel");
		resource = request.getResource();
		resourceResolver = resource.getResourceResolver();
		LOG.trace("Resource Path : {}", resource.getPath());
		vMap = resource.getValueMap();
		offerList = new ArrayList<>();
		countryMap = new HashMap<>();
		destinationMap = new HashMap<>();
		hotelMap = new HashMap<>();

		if (StringUtils.isEmpty(destinationPage)) {
			destinationPage = request.getParameter("destination");
		}
		LOG.trace("Destination Page : {}", destinationPage);
		if (!StringUtils.isEmpty(hotel)) {
			getCorporateOfferForHotel(hotel);
		} else if (!StringUtils.isEmpty(hotelPath)) {
			getOfferForHotel(hotelPath);
		} else if (!StringUtils.isEmpty(destinationPage) || vMap.containsKey("customHotelPaths")) {
			populateHotelDropdown(destinationPage);
			getOffersForDestination(destinationPage);
		} else if (!StringUtils.isEmpty(country)) {
			populateDestinationDropDown(country);
			getOffersForCountry(country);
		} else {
			populateCountryDropDown();
			getAllOffers();
			getOfferCategories();
		}
		LOG.trace("Method Exit : initModel()");

	}

	private void populateHotelDropdown(String destination) {
		LOG.trace("Method Entry : populateHotelDropdown(String destination)");
		hotelMap = hotelSearchService.getHotelByDestination(destination);
		LOG.trace("hotelMap from searchService : "+hotelMap);
		LOG.trace("Method Exit : populateHotelDropdown(String destination)");
	}

	private void populateDestinationDropDown(String countryTagPath) {
		LOG.trace("Method Entry : populateDestinationDropDown(String countryTagPath)");
		destinationMap = destinationSearchService.getDestinationByCountry(countryTagPath);
		LOG.trace("Method Exit : populateDestinationDropDown(String countryTagPath)");
	}

	private void populateCountryDropDown() {
		LOG.trace("Method Entry : populateCountryDropDown()");
		countryMap = globalSearchService.fetchAllCountries();
		LOG.trace("Method Exit : populateCountryDropDown()");
	}

	protected void getAllOffers() {
		LOG.trace("Method Entry : getAllOffers()");
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> offersPathList = offersSearchService.getAllOffers(homePagePath);
		for (String path : offersPathList) {
			offerList.add(pageManager.getContainingPage(path));
		}
		LOG.trace("Method Exit : getAllOffers()");

	}

	private void getOffersForCountry(String country) {
		LOG.trace("Method Entry : getOffersForCountry(String country)");
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> allHotelsForCountry = hotelSearchService.getHotelsByCountry(country);
		List<String> offerPathList = offersSearchService.getAllOffersForHotel(allHotelsForCountry);
		for (String path : offerPathList) {
			offerList.add(pageManager.getContainingPage(path));
		}
		LOG.trace("Method Exit : getOffersForCountry(String country)");

	}

	private void getOffersForDestination(String destination) {
		List<String> offerPathList= null;
		List<String> customHotelPathsList=null;

		LOG.trace("Method Entry : getOffersForDestination(String destination)");
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> allHotelsInDestination = hotelSearchService.getHotelPathsByDestination(destination);

		if(vMap.containsKey("customHotelPaths")){
			customHotelPathsList= Arrays.asList(resource.adaptTo(ValueMap.class).get("customHotelPaths",String[].class));
			LOG.trace("Contains Custom Destination Paths");
			offerPathList=offersSearchService.getSelectedOffersByRank(customHotelPathsList);
		}else {
			LOG.trace("No Custom Destination Paths found, taking default path");
			offerPathList = offersSearchService.getAllOffersForHotel(allHotelsInDestination);
		}




		for (String path : offerPathList) {
			offerList.add(pageManager.getContainingPage(path));
		}
		LOG.trace("Method Exit : getOffersForDestination(String destination)");
	}

	private void getOfferForHotel(String hotelPath) {
		LOG.trace("Method Entry : getOfferForHotel(String hotelPath)");
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> offerPathList = offersSearchService.getAllOffersForHotelPath(hotelPath);
		for (String path : offerPathList) {
			offerList.add(pageManager.getContainingPage(path));
		}
		LOG.trace("Method Exit : getOfferForHotel(String hotelPath)");
	}

	private void getCorporateOfferForHotel(String hotelPath) {
		LOG.trace("Method Entry : getCorporateOfferForHotel(String hotelPath)");
		PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
		List<String> offerPathList = offersSearchService.getCorporateOffersForHotelPath(hotelPath);
		for (String path : offerPathList) {
			offerList.add(pageManager.getContainingPage(path));
		}
		LOG.trace("Method Exit : getCorporateOfferForHotel(String hotelPath)");
	}

	private void getOfferCategories() {
		offerTypes = offersSearchService.getOfferCategories();
	}

	@Override
	public List<Page> getOfferList() {
		return offerList;
	}

	@Override
	public Map<String, String> getCountryMap() {
		return countryMap;
	}

	@Override
	public Map<String, String> getDestinationMap() {
		return destinationMap;
	}

	@Override
	public Map<String, String> getHotelMap() {
		return hotelMap;
	}

	public HashMap<String, String> getOfferTypes() {
		return offerTypes;
	}
}