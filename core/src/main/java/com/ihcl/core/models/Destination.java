package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.services.search.HotelsSearchService;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = Resource.class)
public class Destination {

    private static final Logger LOG = LoggerFactory.getLogger(Destination.class);


    @Inject
    HotelsSearchService hotelsSearchService;

    @Inject
    @Named("jcr:uuid")
    private String destinationId;

    @Inject
    @Named("jcr:title")
    private String destinationTitle;

    @Inject
    @Optional
    private List<String> city;

    @Inject
    @Optional
    private String destinationShortDesc;

    @Inject
    @Optional
    private String destinationLongDesc;

    @Inject
    @Named("imagePath")
    @Optional
    private String hotelImage;

    @Optional
    private List<String> hotels;

    @Optional
    private int hotelsCount;

    @Optional
    private List<String> ticHotels;
    
    @Optional
    private List<String> roomRedemptionTicHotels;

    @Optional
    private List<String> tapHotels;

    @Optional
    private List<String> tapmeHotels;

    @Optional
    private int ticHotelsCount;
    
    @Optional
    private int roomRedemptionTicHotelsCount;

    @Optional
    private int tapHotelsCount;

    @Optional
    private int tapmeHotelsCount;

    @Optional
    private int tajHolidaysRedemptionHotelsCount;

    @Optional
    private List<String> tajHolidaysRedemptionHotels;

    private String destinationPath;

    @Self
    Resource resource;

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        hotelImage = getBannerImage();
        destinationPath = resource.getParent().getPath();
        getDestinationHotels(destinationPath);
        getDestinationTicHotels(destinationPath);
        getRoomRedemptionDestinationTicHotels(destinationPath);
        getDestinationsTapHotels(destinationPath);
        getDestinationsTapMeHotels(destinationPath);
        getDestinationsTajHolidayRedemptionHotels(destinationPath);
    }

    private String getBannerImage() {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                return String.valueOf(hotelBanner.getValueMap().get("bannerImage"));
            }
        }

        return null;
    }

    public List<String> getHotels() {
        return hotels;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public String getDestinationTitle() {
        return destinationTitle;
    }

    public String getDestinationShortDesc() {
        return destinationShortDesc;
    }

    public String getDestinationLongDesc() {
        return destinationLongDesc;
    }

    public int getHotelsCount() {
        return hotelsCount;
    }

    public List<String> getCity() {
        return city;
    }

    public String getCountry() {
        LOG.trace("Method Entry : getCountry()");
        String countryName = "";
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (city != null) {
                LOG.trace("city array :" + city);
                if (tagRoot != null) {
                    tagRoot = tagManager.resolve(city.get(0));
                    LOG.trace("Tag Root Path : " + tagRoot.getPath());
                    countryName = tagRoot.getParent().getTitle();
                }
            }
        }
        LOG.trace("Method Exit -> returning  : Country name : " + countryName);
        return countryName;
    }

    public String getHotelImage() {
        return hotelImage;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public void getDestinationHotels(String destinationPath) {
        hotels = new ArrayList<>();
        hotels = hotelsSearchService.getHotelPathsByDestinationPath(destinationPath);
        hotelsCount = hotels.size();
    }

    public void getDestinationTicHotels(String destinationPath) {
        ticHotels = new ArrayList<>();
        ticHotels = hotelsSearchService.getTicHotelPathsByDestinationPath(destinationPath);
        ticHotelsCount = ticHotels.size();
    }
    
    public void getRoomRedemptionDestinationTicHotels(String destinationPath) {
        roomRedemptionTicHotels = new ArrayList<>();
        roomRedemptionTicHotels = hotelsSearchService.getRoomRedemptionTicHotelPathsByDestinationPath(destinationPath);
        roomRedemptionTicHotelsCount = roomRedemptionTicHotels.size();
    }

    private void getDestinationsTapHotels(String destinationPath) {
        tapHotels = new ArrayList<>();
        tapHotels = hotelsSearchService.getTapHotelPathsByDestinationPath(destinationPath);
        tapHotelsCount = tapHotels.size();

    }

    private void getDestinationsTapMeHotels(String destinationPath) {
        tapmeHotels = new ArrayList<>();
        tapmeHotels = hotelsSearchService.getTappmeHotelPathsByDestinationPath(destinationPath);
        tapmeHotelsCount = tapmeHotels.size();

    }

    public List<String> getTicHotels() {
        return ticHotels;
    }
    
    public List<String> getRoomRedemptionTicHotels() {
        return roomRedemptionTicHotels;
    }

    public int getTicHotelsCount() {
        return ticHotelsCount;
    }
    
    public int getRoomRedemptionHotelsCount() {
        return roomRedemptionTicHotelsCount;
    }

    public List<String> getTapHotels() {
        return tapHotels;
    }

    public int getTapHotelsCount() {
        return tapHotelsCount;
    }

    public List<String> getTapmeHotels() {
        return tapmeHotels;
    }

    public int getTapmeHotelsCount() {
        return tapmeHotelsCount;
    }

    private void getDestinationsTajHolidayRedemptionHotels(String destinationPath) {
        tajHolidaysRedemptionHotels = new ArrayList<>();
        tajHolidaysRedemptionHotels = hotelsSearchService
                .getTajHolidaysRedemptionHotelsPathsByDestinationPath(destinationPath);
        tajHolidaysRedemptionHotelsCount = tajHolidaysRedemptionHotels.size();

    }

    public int getTajHolidaysRedemptionHotelsCount() {
        return tajHolidaysRedemptionHotelsCount;
    }

    public List<String> getTajHolidaysRedemptionHotels() {
        return tajHolidaysRedemptionHotels;
    }


}
