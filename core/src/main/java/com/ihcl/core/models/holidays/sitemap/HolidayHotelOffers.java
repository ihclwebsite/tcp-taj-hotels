package com.ihcl.core.models.holidays.sitemap;

import java.util.List;

public class HolidayHotelOffers implements Comparable<Object> {

	private String hotelName;
	
	private List<HolidaysPageLink> HotelOffers;

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public List<HolidaysPageLink> getHotelOffers() {
		return HotelOffers;
	}

	public void setHotelOffers(List<HolidaysPageLink> hotelOffers) {
		HotelOffers = hotelOffers;
	}

	@Override
	public int compareTo(Object o) {
		return this.getHotelName().compareTo(((HolidayHotelOffers) o).getHotelName());
	}
	
}
