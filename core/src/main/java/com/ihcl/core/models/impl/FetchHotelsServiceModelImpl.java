package com.ihcl.core.models.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Style;
import com.ihcl.core.models.FetchHotelsServiceModel;
import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.models.serviceamenity.GroupedServiceAmenitiesFetcher;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.core.util.services.TajCommonsFetchResource;
import com.ihcl.tajhotels.constants.CrxConstants;

/**
 * @author Srikanta.moonraft
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = FetchHotelsServiceModel.class)
public class FetchHotelsServiceModelImpl implements FetchHotelsServiceModel {

    private static final Logger LOG = LoggerFactory.getLogger(FetchHotelsServiceModelImpl.class);

    protected final String RESOURCE_NAME_HOTEL_BANNER = "bannerImage";

    protected final String TEMPLATE_TYPE_NAME = "hotel-landing-page";

    @Inject
    SlingHttpServletRequest request;

    @Inject
    @Source("script-bindings")
    private Style currentStyle;

    @Inject
    @Optional
    protected String resourcePath;

    @Inject
    ResourceResolver resourceresolver;

    @Inject
    TajCommonsFetchResource commonsFetchResource;

    ParticipatingHotel hotel;

    Resource resource;

    Page hotelLandingPage;

    Resource child;

    String pagePath;

    String linkPath;

    String templateTypeName;

    List<String> hotelTypes;

    ValueMap hotelData;

    @Inject
    private ResourceFetcherService resourceFetcherService;


    @PostConstruct
    protected void init() {
        LOG.trace("Method Entry: Init");
        try {
            resource = request.getResource();
            LOG.trace("Received resource path by injection: {} Resource :: {}", resourcePath, resource);
            templateTypeName = resource.getParent().adaptTo(Page.class).getTemplate().getName();
            if (templateTypeName.equals(TEMPLATE_TYPE_NAME)) {
                hotel = resource.adaptTo(ParticipatingHotel.class);
                LOG.trace("The resource Value after hotel: {} " , resource);
                hotel.setServiceAmenities(fetchServiceAmenitiesFrom(resource));
                String imageUrl = extractBannerImage(resource);
                if (StringUtils.isNotBlank(imageUrl)) {
                    hotel.setImageUrl(imageUrl);
                }

                linkPath = resource.getPath().replaceAll("/jcr:content", "");
                hotelData = resource.getResourceResolver().getResource(linkPath + "/jcr:content/root/hotel_overview").getValueMap();
                LOG.trace("hotelData :: {} linkPath :: {} ", hotelData, linkPath);
            }

        } catch (Exception e) {
            LOG.error(e.getMessage());
            LOG.debug("Exception in Init :: {}",e);
        }
        LOG.trace("Method Exit: Init");
    }


    private List<String> fetchServiceAmenitiesFrom(Resource resource) {
        LOG.trace("Method Entry: fetchServiceAmenitiesFrom");
        List<String> serviceAmenities = new ArrayList<>();
        Resource hotelOverviewResource = resourceFetcherService.getChildrenOfType(resource, CrxConstants.HOTEL_OVERVIEW_COMPONENT_RESOURCETYPE);
        LOG.debug("Fetched hotel overview resource as: {}" ,hotelOverviewResource);
        if (hotelOverviewResource != null) {
            LOG.debug("Attempting to adapt hotel overview resource to {}", GroupedServiceAmenitiesFetcher.class);
            GroupedServiceAmenitiesFetcher groupedServiceAmenitiesFetcher = hotelOverviewResource.adaptTo(GroupedServiceAmenitiesFetcher.class);
            serviceAmenities = groupedServiceAmenitiesFetcher.getAllAmenities();
            LOG.debug("Fetched all amenities as: " + serviceAmenities);
        }
        LOG.trace("Method Exit: fetchServiceAmenitiesFrom");
        return serviceAmenities;
    }


    private String extractBannerImage(Resource resource) {
    	Node node = resource.adaptTo(Node.class);
		try {
			if (node.hasProperty("hotelImagePath")) {
				return node.getProperty("hotelImagePath").getValue().getString();
			}
			Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
			if (bannerParsys != null) {
				Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
				if (hotelBanner != null) {
					ValueMap valMap = hotelBanner.getValueMap();
					String imageURL = String.valueOf(valMap.get(RESOURCE_NAME_HOTEL_BANNER));
					return imageURL;
				}
				LOG.trace("Returning null [condition failed], hotelBanner is null");
				return null;
			}
		} catch (IllegalStateException | RepositoryException e) {
			LOG.error("Exception found while getting the image path :: {}", e.getMessage());
			LOG.debug("Exception found while getting the image path :: {}", e);
		} 
        LOG.trace("Returning null [condition failed], bannerParsys is null");
        return null;
    }

    public String getLinkPath() {
        return linkPath;
    }

    @Override
    public String getPagePath() {
        LOG.trace("Entry > [Method : getPagePath() ] :: @Override {}" , pagePath);
        return pagePath;
    }

    @Override
    public ParticipatingHotel getHotel() {
        LOG.trace("Entry > [Method : getHotel() ] :: @Override");
        return hotel;
    }

    @Override
    public ValueMap getHotelDetails() {
        LOG.trace("Entry > [Method : getHotelDetails() ] :: @Override");
        return hotelData;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.adobe.cq.export.json.ComponentExporter#getExportedType()
     */
    @Override
    public String getExportedType() {
        // TODO Auto-generated method stub
        LOG.trace("Returning null [forcefully overridding a method ]");
        return null;
    }

}// End of class--ParticipatingHotelsModel
