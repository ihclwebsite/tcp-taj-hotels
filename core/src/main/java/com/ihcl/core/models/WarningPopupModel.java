package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class WarningPopupModel extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
 
	private List<WarningPopup> multiFieldItems = new ArrayList<>();
	   
	@Override
	public void activate() throws Exception {
	 
	    Node currentNode = getResource().adaptTo(Node.class);
	    if(currentNode != null) {
	    	NodeIterator ni =  currentNode.getNodes() ;
		    while (ni.hasNext()) {
		        Node child = ni.nextNode();
		        if(child != null){
		        	NodeIterator ni2 =  child.getNodes() ;
			        setMultiFieldItems(ni2);
		        }
		        
		    }
	    }
	    
	}
   
	private List<WarningPopup> setMultiFieldItems(NodeIterator ni2) {
	   
		try{	 
		    while (ni2.hasNext()) {
		    	WarningPopup menuItem = new WarningPopup();
		        Node grandChild = ni2.nextNode();
		         
		        log.info("*** GRAND CHILD NODE PATH IS "+grandChild.getPath());
		        
		        if(grandChild.hasProperty("warningHeading") && grandChild.hasProperty("warningPopupType")) {
		        	menuItem.setWarningHeading(grandChild.getProperty("warningHeading").getString());
		        	menuItem.setWarningPopupType(grandChild.getProperty("warningPopupType").getString());
		        }
		        if(grandChild.hasProperty("warningDescription")) {
		        	menuItem.setWarningDescription(grandChild.getProperty("warningDescription").getString());
		        }
		        
		        if((menuItem.getWarningHeading() != null) && (menuItem.getWarningPopupType() != null)) {
		        	multiFieldItems.add(menuItem);
		        }
		    }
		}
		   
		catch(Exception e){
		    log.error("Exception while Multifield data {}", e.getMessage(), e);
		}
		
		return multiFieldItems;
	}
   
	public List<WarningPopup> getMultiFieldItems() {
		
		return multiFieldItems;
	}

}
