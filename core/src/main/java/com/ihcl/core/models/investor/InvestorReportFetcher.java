package com.ihcl.core.models.investor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = Resource.class)
public class InvestorReportFetcher {
		
	private static final Logger LOG = LoggerFactory.getLogger(InvestorReportFetcher.class);
	
	@Inject
	private String pdfDamPath;
	
    @Inject
    private SearchProvider searchProvider;
    
    
    List<InvestorReportsBean> pdfList = new ArrayList<InvestorReportsBean>();
      
    @PostConstruct
    protected void init() {
        buildReportProperties();
    }
    
    private void buildReportProperties() {
    	try {
    		HashMap<String, String> predicateMap = new HashMap<>();
    		LOG.debug("Searching for various PDFpaths under the path : " + pdfDamPath);
            predicateMap.put("path",pdfDamPath);
            predicateMap.put("property","jcr:primaryType");
            predicateMap.put("property.value","dam:Asset");
            predicateMap.put("p.limit", "-1");
            SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
            for(Hit hit: searchResult.getHits()) {
            	String pdfPath = hit.getPath();
            	LOG.debug("Found pdf  at : " + pdfPath);
            	Resource pdfMetaResource =hit.getResource().getChild("jcr:content/metadata");
            	if(null!=pdfMetaResource)
            		fetchReportPropertiesFrom(pdfMetaResource,pdfPath);
            	}
    		} catch(Exception e) {
    			LOG.error("Exception occured searching for packageComponent :: {}" , e.getMessage());
    		} 
    		
    }
    
    private void fetchReportPropertiesFrom(Resource pdfMetaResource, String pdfPath) {
    	InvestorReportsBean pdfObj = new InvestorReportsBean();
    	ValueMap valueMap = pdfMetaResource.adaptTo(ValueMap.class);
    	pdfObj.setTitle(getProperty(valueMap,"jcr:title", String.class, ""));
    	pdfObj.setPdfPath(pdfPath);
    	pdfList.add(pdfObj);
    }
    
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
		String methodName = "getProperty";
		LOG.trace("Method Entry: " + methodName);
		T value = defaultValue;
		if (valueMap.containsKey(key)) {
			value = valueMap.get(key, type);
		}
		LOG.trace("Value found for key: " + key + " : " + value);
		LOG.trace("Method Exit: " + methodName);
		return value;
	}
 
	public List<InvestorReportsBean> getPdfList() {
		return pdfList;
	}

    
}
