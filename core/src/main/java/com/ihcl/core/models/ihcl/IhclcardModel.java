/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(
        adaptables = Resource.class,
        adapters = IhclcardModel.class)

public class IhclcardModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclcardModel.class);

    @Self
    private Resource resource;

    private String CardId;


    public List<IhclCardBean> cardList = new ArrayList<IhclCardBean>();


    /**
     * Getter for the field cardList
     *
     * @return the cardList
     */
    public List<IhclCardBean> getCardList() {
        return cardList;
    }

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getCardValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @return
     *
     */
    private List<IhclCardBean> getCardValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is ihcl cards::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] cards = valueMap.get("cardlist", String[].class);
        JsonObject jsonObject;
        LOG.trace("card is::" + cards);
        Gson gson = new Gson();
        if (null != cards) {
            for (int i = 0; i < cards.length; i++) {
                JsonElement jsonElement = gson.fromJson(cards[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclCardBean cardListBean = new IhclCardBean();
                LOG.trace("jsonObject :" + jsonObject);

                if (null != jsonObject.get("imagecard")) {
                    cardListBean.setImageCard(jsonObject.get("imagecard").getAsString());
                }

                if (null != jsonObject.get("title")) {
                    cardListBean.setTitle(jsonObject.get("title").getAsString());
                }

                if (null != jsonObject.get("titlevariation")) {
                    cardListBean.setTitleVariation(jsonObject.get("titlevariation").getAsString());
                }
                if (null != jsonObject.get("description")) {
                    cardListBean.setDescription(jsonObject.get("description").getAsString());
                }
                if (null != jsonObject.get("popupdescription")) {
                    cardListBean.setPopupdescription(jsonObject.get("popupdescription").getAsString());
                    
                }

                if (null != jsonObject.get("buttontext")) {
                    cardListBean.setButtontext(jsonObject.get("buttontext").getAsString());
                }
                if (null != jsonObject.get("pagePath")) {
                    cardListBean.setPagePath(jsonObject.get("pagePath").getAsString());
                }
                if (null != jsonObject.get("title")) {
                    String title = jsonObject.get("title").getAsString();
                    LOG.trace("title list data ::: " + title);
                    CardId = title.replaceAll(" ", "");
                    LOG.trace("card data id ::: " + CardId);
                    cardListBean.setCardidTitle(CardId);
                }


                cardList.add(cardListBean);

            }

        }
        LOG.trace("cards list data ::: " + cardList);
        return cardList;
    }

    /**
     * @return the card id
     */
    public String getCardId() {
        return CardId;
    }
}

