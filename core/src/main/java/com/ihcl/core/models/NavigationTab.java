package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class NavigationTab extends WCMUsePojo {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	List<NavigationTabBean> multiTabList = new ArrayList<NavigationTabBean>();

	@Override
	public void activate() throws Exception {

		Node currentNode = getResource().adaptTo(Node.class);
		NodeIterator ni = currentNode.getNodes();

		while (ni.hasNext()) {

			Node child = ni.nextNode();
			if (child.getPath().contains("navigationOption") || child.getPath().contains("navigationTabs")) {
				NodeIterator ni2 = child.getNodes();
				log.info("*** CHILD PATH NODES " + ni2);
				setMultiFieldItems(ni2);
			}
		}
	}

	private void setMultiFieldItems(NodeIterator ni2) {

        try {
            while (ni2.hasNext()) {

                NavigationTabBean tabObj = new NavigationTabBean();
                Node grandChild = ni2.nextNode();
                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
                if (grandChild.hasProperty("tabtextlink") && grandChild.hasProperty("tabtext")) {
                    tabObj.setTabtext(grandChild.getProperty("tabtext").getString());
                    tabObj.setTabtextlink(grandChild.getProperty("tabtextlink").getString());
                    multiTabList.add(tabObj);
                }
            }
        }

        catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }

    }

	public List<NavigationTabBean> getMultiFieldTabList() {
		return multiTabList;
	}

}
