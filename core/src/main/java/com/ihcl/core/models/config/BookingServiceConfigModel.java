package com.ihcl.core.models.config;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface BookingServiceConfigModel {

    String getBookingConfirmationURL();

    String getBookingFailedURL();
}
