package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class BillerModel extends WCMUsePojo {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	private List<BookingBean> billerList = new ArrayList<>();

	@Override
	public void activate() throws Exception {

		if(getResource().adaptTo(Node.class) != null) {
			Node currentNode = getResource().adaptTo(Node.class);
			if(currentNode != null) {
				NodeIterator ni = currentNode.getNodes();
				while (ni.hasNext()) {
					Node child = ni.nextNode();
					if(child != null) {
						NodeIterator ni2 = child.getNodes();
						setBillerList(ni2);
					}
				}
			}
		}
	}

	private void setBillerList(NodeIterator ni2) {

		try {
			while (ni2.hasNext()) {
				BookingBean imgObj = new BookingBean();
				Node grandChild = ni2.nextNode();

				log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());

				if (grandChild.hasProperty("billerFormItemLabel") && grandChild.hasProperty("billerFormItemField")) {
					imgObj.setBillerFormItemLabel(grandChild.getProperty("billerFormItemLabel").getString());
					imgObj.setBillerFormItemField(grandChild.getProperty("billerFormItemField").getString());
				}

				if((imgObj.getBillerFormItemLabel() != null) && (imgObj.getBillerFormItemField() != null)){
					billerList.add(imgObj);
				}
			}
		}
		catch (Exception e) {
			log.error("Exception in setBillerList", e.getMessage(), e);
		}
	}

	public List<BookingBean> getBillerList() {
		
		return billerList;
	}

}
