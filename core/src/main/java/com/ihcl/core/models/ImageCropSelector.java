/**
 *
 */
package com.ihcl.core.models;

import java.util.Map;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author Srikanta.moonraft
 *
 */
@ConsumerType
public interface ImageCropSelector {

    /**
     * @return
     */
    String getBannerImage();

    /**
     * @return
     */
    ValueMap getCropBoxData();

    /**
     * @return
     */
    Map<String, Number> getCropBoxDataMap();

    /**
     * @return
     */
    String getName();

    /**
     * @return
     */
    String getLabel();

    /**
     * @return
     */
    String getPath();

    /**
     * @return
     */
    Boolean getIsCropped();

    /**
     * @return
     */
    String getSuffix();

    /**
     * @return
     */
    String getAspect();


}
