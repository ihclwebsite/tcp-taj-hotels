package com.ihcl.core.models;

public interface FilteredList {
	void executeFilter(FilterParams params);
}
