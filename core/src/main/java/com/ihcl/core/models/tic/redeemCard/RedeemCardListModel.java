package com.ihcl.core.models.tic.redeemCard;

import com.ihcl.core.shared.services.ResourceFetcherService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.*;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = IRedeemCardListModel.class)
public class RedeemCardListModel implements IRedeemCardListModel {

    private final Logger LOG = LoggerFactory.getLogger(RedeemCardListModel.class);

    @Inject
    @Named("path")
    private String path;

    @Inject
    private ResourceResolverFactory resolverFactory;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    private IRedeemCardModel redeemCardModel;

    @PostConstruct
    protected void init() {
        String methodName = "init";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Received path as: " + path);
            ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(null);
            Resource resource = resourceResolver.getResource(path);
            if (resource != null) {
                redeemCardModel = resource.adaptTo(IRedeemCardModel.class);
            }
        } catch (LoginException e) {
            LOG.error("An error occurred while getting resource resolver.", e);
        }

        LOG.trace("Method Exit: " + methodName);
    }

    @Override
    public ValueMap getProperties() {
        if (redeemCardModel != null) {
            return redeemCardModel.getProperties();
        }
        return null;
    }

    @Override
    public Map<String, String> getCurrencies() {
        if (redeemCardModel != null) {
            return redeemCardModel.getCurrencies();
        }
        return null;
    }

}
