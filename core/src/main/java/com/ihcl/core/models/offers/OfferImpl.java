package com.ihcl.core.models.offers;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = Resource.class,
        adapters = Offer.class,
        resourceType = "tajhotels/components/structure/offer-details-page")
public class OfferImpl implements Offer {

    private static final Logger LOG = LoggerFactory.getLogger(OfferImpl.class);

    @Self
    private Resource resource;

    @Inject
    private ResourceResolver resourceResolver;

    private static final String OFFER_DESCRIPTION = "offerDescription";

    private static final String BANNER_IMAGE_PATH = "bannerImage";

    private static final String VALIDITY = "validity";

    private static final String NOOFNIGHTS = "nights";

    private static final String DISCOUNT_VALUE = "discountValue";

    private static final String OFFER_SPECIFIC = "offerSpecifics";

    private static final String OFFERS_DETAILS_RESOURCETYPE = "tajhotels/components/content/offerdetails";

    private static final String PACKAGE_OFFER_REFERENCE_RESOURCETYPE = "foundation/components/reference";

    private static final String PATH_REFERENCE_HOLIDAYS = "path";

    private static final String TAJ_HOLIDAYS_DESTINATIONS = "/content/tajhotels/en-in/taj-holidays/destinations/";

    private static final String PACKAGE_DESCRIPTION = "packageDescription";

    private static final String PACKAGE_VALIDITY_END_DATE = "packageValidityEndDate";

    private static final String OFFER_END_DATE = "offerEndsOn";

    private static final String DISCOUNTED_RATE = "discountedRate";

    private static final String PACKAGE_TITLE = "packageTitle";

    private static final String PACKAGE_NOOFNIGHTS = "nights";

    private static final String PACKAGE_OFFER_RATE_CODE = "offerRateCode";

    private static final String PACKAGE_COMPARABLE_OFFER_RATE_CODE = "comparableOfferRateCode";

    private static final String PACKAGE_OFFER_STARTS_FROM = "offerStartsFrom";

    private static final String ROUND_THE_YEAR_OFFER = "roundTheYearOffer";

    private static final String IMAGE_FILE_REFERENCE = "fileReference";
    
    private static final String MEMBER_ONLY_OFFER = "memberOnlyOffer";
    
    private static final String OFFER_CATEGORY = "offerCategory";

    protected final String RENDITION_VAL = "cq5dam.web.319.319.jpeg";

    private String name;

    List<Resource> children;

    private String validityDate;

    @Inject
    @Optional
    private String offerShortDesc;

    @Inject
    @Optional
    private String offerRateCode;

    @Inject
    @Optional
    private String comparableOfferRateCode;

    @Inject
    @Optional
    private String offerStartsFrom;

    @Inject
    @Optional
    private String offerEndsOn;

    private Date offerStartsDate;

    private Date offerEndsDate;

    private String offerDescription;

    private String roundTheYearOffer;

    private String referenceHolidaysPath;

    @Inject
    @Optional
    private String offerTitle;

    private String offerSpecific;

    private String discountValue;
    
    private String offerType;

    @Named("cq:tags")
    @Inject
    @Optional
    private List<String> offerTypes;


    private String offerImage;

    private String hotelSpecificOfferImageUrl;

    private String offerPageResourceURL;

    private String draggedImagePath;

    @Inject
    @Optional
    private String offerCategory;

    @Inject
    @Named("nights")
    @Optional
    private String noOfNights;
    
    @Inject
    @Optional
    private String memberOnlyOffer;
    

    @Override
    @PostConstruct
    public void init() {
        readProperties();
    }

    private void readProperties() {
        try {
            offerPageResourceURL = resource.getParent().getPath();
            resourceResolver = resource.getResourceResolver();
            String imageUrl = extractBannerImage(resource);
            if (StringUtils.isNotBlank(imageUrl)) {
                String renditionURL = getImageURLForRendition(imageUrl);
                if (null != renditionURL) {
                    offerImage = renditionURL;
                } else {
                    offerImage = imageUrl;
                }
            } else {
                offerImage = extractHotelBanner(offerPageResourceURL);
            }
            hotelSpecificOfferImageUrl = extractOfferBanner(offerPageResourceURL);
            LOG.debug("hotelSpecificOfferImageUrl value is " + hotelSpecificOfferImageUrl);

            if (StringUtils.isNotBlank(hotelSpecificOfferImageUrl)) {
                String specificHotelOfferRenditionURL = getImageURLForRendition(hotelSpecificOfferImageUrl);
                if (specificHotelOfferRenditionURL != null) {
                    hotelSpecificOfferImageUrl = specificHotelOfferRenditionURL;
                }
            } else {
                hotelSpecificOfferImageUrl = extractHotelBanner(offerPageResourceURL);
            }
            LOG.trace("No of nights : {}", noOfNights);
            Resource root = resource.getChild("root");
            ValueMap resourceValueMap = resource.getValueMap();
            for (Resource child : root.getChildren()) {
                if (child.getResourceType().equals(OFFERS_DETAILS_RESOURCETYPE)) {
                    ValueMap properties = child.getValueMap();
                    offerDescription = properties.get(OFFER_DESCRIPTION, String.class);
                    validityDate = properties.get(VALIDITY, String.class);
                    roundTheYearOffer = resourceValueMap.get(ROUND_THE_YEAR_OFFER, String.class);
                    offerEndsDate = resourceValueMap.get(OFFER_END_DATE, Date.class);
                    if(offerEndsDate == null && properties.get(OFFER_END_DATE) != null) {
                    	offerEndsDate = properties.get(OFFER_END_DATE, Date.class);
                    }
                    offerStartsDate = resourceValueMap.get(PACKAGE_OFFER_STARTS_FROM, Date.class);
                    if(offerStartsDate == null && properties.get(PACKAGE_OFFER_STARTS_FROM) != null) {
                    	offerStartsDate = properties.get(PACKAGE_OFFER_STARTS_FROM, Date.class);
                    }
                    discountValue = properties.get(DISCOUNT_VALUE, String.class);
                    if(getProperty(properties, "offerType", String.class, "") != null)
                    	offerType = getProperty(properties, "offerType", String.class, "");
                    offerSpecific = properties.get(OFFER_SPECIFIC, String.class);
                    draggedImagePath = properties.get(IMAGE_FILE_REFERENCE, String.class);
                    memberOnlyOffer = resourceValueMap.get(MEMBER_ONLY_OFFER, String.class);
                    if(memberOnlyOffer == null && properties.get(MEMBER_ONLY_OFFER) != null) {
                    	memberOnlyOffer = properties.get(MEMBER_ONLY_OFFER, String.class);
                    }
                    offerCategory = resourceValueMap.get(OFFER_CATEGORY, String.class);
                    if(offerCategory == null && properties.get(OFFER_CATEGORY) != null) {
                    	offerCategory = properties.get(OFFER_CATEGORY, String.class);
                    }
                    LOG.debug("memberOnlyOffer>>>> "+ memberOnlyOffer);
                } else if (child.getResourceType().equals(PACKAGE_OFFER_REFERENCE_RESOURCETYPE)) {
                    ValueMap holidaysProperties = child.getValueMap();
                    referenceHolidaysPath = holidaysProperties.get(PATH_REFERENCE_HOLIDAYS, String.class);

                    if (referenceHolidaysPath.contains(TAJ_HOLIDAYS_DESTINATIONS)) {
                        Resource tajHolidaysDestination = resourceResolver.getResource(referenceHolidaysPath);
                        if (tajHolidaysDestination != null) {
                            ValueMap holidaysPropertiesValueMap = tajHolidaysDestination.adaptTo(ValueMap.class);
                            offerShortDesc = getProperty(holidaysPropertiesValueMap, PACKAGE_DESCRIPTION, String.class,
                                    "");
                            validityDate = getProperty(holidaysPropertiesValueMap, PACKAGE_VALIDITY_END_DATE,
                                    String.class, "");
                            roundTheYearOffer = getProperty(holidaysPropertiesValueMap, ROUND_THE_YEAR_OFFER,
                                    String.class, "");
                            offerEndsDate = holidaysPropertiesValueMap.containsKey(OFFER_END_DATE)
                                    ? holidaysPropertiesValueMap.get(OFFER_END_DATE, Date.class)
                                    : null;
                            discountValue = getProperty(holidaysPropertiesValueMap, DISCOUNTED_RATE, String.class, "");
                            if(getProperty(holidaysPropertiesValueMap, "offerType", String.class, "") != null)
                            	offerType = getProperty(holidaysPropertiesValueMap, "offerType", String.class, "");
                            offerTitle = getProperty(holidaysPropertiesValueMap, PACKAGE_TITLE, String.class, "");
                            noOfNights = getProperty(holidaysPropertiesValueMap, PACKAGE_NOOFNIGHTS, String.class, "");
                            offerRateCode = getProperty(holidaysPropertiesValueMap, PACKAGE_OFFER_RATE_CODE,
                                    String.class, "");
                            comparableOfferRateCode = getProperty(holidaysPropertiesValueMap,
                                    PACKAGE_COMPARABLE_OFFER_RATE_CODE, String.class, "");
                            offerStartsFrom = getProperty(holidaysPropertiesValueMap, PACKAGE_OFFER_STARTS_FROM,
                                    String.class, "");
                            offerEndsOn = getProperty(holidaysPropertiesValueMap, OFFER_END_DATE, String.class, "");
                            offerStartsDate = getProperty(holidaysPropertiesValueMap, PACKAGE_OFFER_STARTS_FROM,
                                    Date.class, null);
                            if(holidaysPropertiesValueMap.containsKey(MEMBER_ONLY_OFFER)) {
                            memberOnlyOffer = getProperty(holidaysPropertiesValueMap, MEMBER_ONLY_OFFER,
                                    String.class, null);
                            	LOG.trace("memberOnlyOffer::::: "+ memberOnlyOffer);
                            }
                            if(holidaysPropertiesValueMap.containsKey(MEMBER_ONLY_OFFER)) {
                            offerCategory = getProperty(holidaysPropertiesValueMap, OFFER_CATEGORY,
                                    String.class, null);
                            }    
                        }
                    }
                }

            }
        } catch (Exception e) {
            LOG.error("Exception found while trying to get the offerpage data :: {}", e.getMessage());
            LOG.debug("Exception found while trying to get the offerpage data :: {}", e);
        }

    }


    public String getOfferType() {
		return offerType;
	}

	private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value = defaultValue;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        LOG.trace("Value found for key: {} : Value : {}", key, value);
        return value;
    }


    private String getImageURLForRendition(String imagePath) {
        Asset asset = getAssetForImagePath(imagePath);
        if (asset != null) {
            return extractAssetRendtion(asset);
        }
        return null;
    }

    private Asset getAssetForImagePath(String imagePath) {
        Resource assetResource = resourceResolver.getResource(imagePath);
        if (null != assetResource) {
            return assetResource.adaptTo(Asset.class);
        }
        return null;
    }

    private String extractAssetRendtion(Asset asset) {

        if (asset != null) {
            Rendition renditionForCard = asset.getRendition(RENDITION_VAL);
            if (renditionForCard != null) {
                return renditionForCard.getPath();
            } else {
                return asset.getPath();
            }
        }
        return null;
    }

    private String extractBannerImage(Resource resource) {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource offerBanner = bannerParsys.getChild(CrxConstants.OFFER_BANNER_COMPONENT_NAME);
            if (offerBanner != null) {
                ValueMap valMap = offerBanner.getValueMap();
                String imageURL = String.valueOf(valMap.get(BANNER_IMAGE_PATH));
                return imageURL;
            }
            return null;
        }
        return null;
    }

    private String extractHotelBanner(String offerPath) {
        LOG.debug("Inside extractHotelBanner with current offer path is :  {}", offerPath);
        if (isHotelSpecificOffer(offerPath)) {
            LOG.debug("current offer path is hotel specific offer : {}", offerPath);
            Resource parentOfferContainer = resource.getParent().getParent();
            if (null != parentOfferContainer) {
                Resource hotelPath = parentOfferContainer.getParent();
                if (null != hotelPath) {
                    LOG.debug("current hotelPath is not null : {}", hotelPath);
                    Resource hotelContentPath = resourceResolver.getResource(hotelPath.getPath() + "/jcr:content");
                    Resource contentResource = findBannerComponent(hotelContentPath);
                    return getBannerImage(contentResource);
                }
            }
        }
        return null;
    }

    private String extractOfferBanner(String offerPath) {
        LOG.debug("Inside extractOfferBanner with OfferPath as : {}", offerPath);
        if (isHotelSpecificOffer(offerPath)) {
            Resource offerContent = resource.getParent();
            if (null != offerContent) {
                LOG.debug("Parent Offer path in extractOfferBanner is {} for {} ", offerContent.getPath(), offerPath);
                Resource offerContentPath = resourceResolver.getResource(offerContent.getPath() + "/jcr:content");
                if (offerContentPath != null) {
                    ValueMap bannerValueMap = offerContentPath.getValueMap();
                    String bannerImagePath = String.valueOf(bannerValueMap.get("bannerImage"));
                    String packageImagePath = String.valueOf(bannerValueMap.get("packageImagePath"));
                    LOG.debug("banner :: {} : packageImage :: {}", bannerImagePath, packageImagePath);
                    if (bannerImagePath != null) {
                        LOG.debug("BannerImagePath :: - {}", bannerImagePath);
                        return bannerImagePath;
                    } else {
                        LOG.debug("Package ImagePath :: {}", packageImagePath);
                        return packageImagePath;
                    }
                }
            }
        }
        return StringUtils.EMPTY;
    }

    private boolean isHotelSpecificOffer(String offerPath) {
        return offerPath.contains("our-hotels");

    }

    private Resource findBannerComponent(Resource resource) {
        if (null != resource && resource.hasChildren()) {
            Iterator<Resource> children = resource.getChildren().iterator();
            while (children.hasNext()) {
                Resource child = children.next();
                if (child.getName().equals(CrxConstants.BANNER_PARSYS_COMPONENT_NAME)) {
                    return child;
                }
            }
        }
        return null;
    }

    private String getBannerImage(Resource resource) {
        String imagePath = "";
        if (null != resource && resource.hasChildren()) {
            Iterator<Resource> bannerChildren = resource.getChildren().iterator();
            while (bannerChildren.hasNext()) {
                Resource bannerChild = bannerChildren.next();
                ValueMap bannerValueMap = bannerChild.getValueMap();
                imagePath = String.valueOf(bannerValueMap.get("bannerImage"));
            }
            return imagePath;
        }
        return null;
    }

    public Resource getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public Date getOfferEndsDate() {
        return offerEndsDate;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public String getOfferShortDesc() {
        return offerShortDesc;
    }

    @Override
    public String getOfferRateCode() {
        return offerRateCode;
    }

    @Override
    public String getComparableOfferRateCode() {
        return comparableOfferRateCode;
    }

    @Override
    public String getOfferStartsFrom() {
        return offerStartsFrom;
    }

    @Override
    public String getOfferEndsOn() {
        return offerEndsOn;
    }

    public String getNoOfNights() {
        return noOfNights;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public String getOfferSpecific() {
        return offerSpecific;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public List<String> getOfferTypes() {
        return offerTypes;
    }

    public String getOfferImage() {
        LOG.debug("OfferImage1902: {}", offerImage);
        return offerImage;
    }

    @Override
    public String getDraggedImagePath() {
        LOG.debug("DraggedImagePath1190: {}", draggedImagePath);
        return draggedImagePath;
    }

    public String getSpecificOfferImage() {
        LOG.debug("SpecificOfferImage11902: {}", hotelSpecificOfferImageUrl);
        return hotelSpecificOfferImageUrl;
    }

    public String getOfferPageResourceURL() {
        return getMappedPath(offerPageResourceURL);
    }

    private String getMappedPath(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return URLMapUtil.getPathFromURL(resolvedURL);
        }
        return url;
    }

    public String getOfferCategory() {
        return offerCategory;
    }

    public void setOfferCategory(String offerCategory) {
        this.offerCategory = offerCategory;
    }

    public String getRoundTheYearOffer() {
        return roundTheYearOffer;
    }

    public Date getOfferStartsDate() {
        return offerStartsDate;
    }
    
    public String getMemberOnlyOffer() {
        return memberOnlyOffer;
    }
    
    public void setMemberOnlyOffer(String memberOnlyOffer) {
        this.memberOnlyOffer = memberOnlyOffer;
    }
    
}