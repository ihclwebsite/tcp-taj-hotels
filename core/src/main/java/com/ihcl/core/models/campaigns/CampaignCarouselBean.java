/**
 *
 */
package com.ihcl.core.models.campaigns;


public class CampaignCarouselBean {


    private String imagePath;

    private String imageTitle;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    /**
     * Getter for the field imageTitle
     *
     * @return the imageTitle
     */
    public String getImageTitle() {
        return imageTitle;
    }


    /**
     * Setter for the field imageTitle
     *
     * @param imageTitle
     *            the imageTitle to set
     */

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    /**
     * @param imagePath
     * @param imageTitle
     */
    public CampaignCarouselBean(String imagePath, String imageTitle) {
        super();
        this.imagePath = imagePath;
        this.imageTitle = imageTitle;
    }


}
