/**
 *
 */
package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(
        adaptables = Resource.class,
        adapters = PartnerPromotionModel.class)

public class PartnerPromotionModel {

    private static final Logger LOG = LoggerFactory.getLogger(PartnerPromotionModel.class);

    @Self
    private Resource resource;

    public List<PartnerPromotionBean> partnerPromotionList = new ArrayList<PartnerPromotionBean>();


    public List<PartnerPromotionBean> getPartnerPromotionList() {
        return partnerPromotionList;
    }

    public void setPartnerPromotionList(List<PartnerPromotionBean> partnerPromotionList) {
        this.partnerPromotionList = partnerPromotionList;
    }

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getfiguresValues();
        LOG.trace("Method Exit: " + methodName);
    }

    public List<PartnerPromotionBean> getfiguresValues() {
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] listBlocks = valueMap.get("list", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != listBlocks) {
            for (int i = 0; i < listBlocks.length; i++) {
                JsonElement jsonElement = gson.fromJson(listBlocks[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                PartnerPromotionBean partnerPromotionBean = new PartnerPromotionBean();
                LOG.trace("jsonObject :" + jsonObject);


                if (null != jsonObject.get("Name")) {
                    partnerPromotionBean.setTitle(jsonObject.get("Name").getAsString());
                }

                if (null != jsonObject.get("partnerImg")) {
                    partnerPromotionBean.setImagePath(jsonObject.get("partnerImg").getAsString());
                }

                if (null != jsonObject.get("partnelogorImg")) {
                    partnerPromotionBean.setLogoImagePath(jsonObject.get("partnelogorImg").getAsString());
                }

                if (null != jsonObject.get("partnerResourceUrl")) {
                    partnerPromotionBean.setPartnerResourceUrl(jsonObject.get("partnerResourceUrl").getAsString());
                }

                if (null != jsonObject.get("partnerDetails")) {
                    partnerPromotionBean.setPartnerDetails(jsonObject.get("partnerDetails").getAsString());
                }
                partnerPromotionList.add(partnerPromotionBean);

            }

        }
        return partnerPromotionList;
    }
}
