/**
 * 
 */
package com.ihcl.core.models.destination;

import java.util.List;

import com.ihcl.core.models.impl.WebsiteHotelsBean;

/**
 * <pre>
 * DestinationHotelsBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :03-Jun-2019
 * @ClassName : DestinationHotelsBean.java
 * @Description : com.ihcl.core.models.impl -DestinationHotelsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  03-Jun-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class DestinationHotelsBean {
	
	private List<WebsiteHotelsBean> websitesList;
	private String destinationPath;
	private String destinationName;
	private String destinationId;
	private String destinationImage;
	private String destinationTitle;
	private String destinationCityTag;
	/**
	 * @return the websitesList
	 */
	public List<WebsiteHotelsBean> getWebsitesList() {
		return websitesList;
	}
	/**
	 * @param websitesList the websitesList to set
	 */
	public void setWebsitesList(List<WebsiteHotelsBean> websitesList) {
		this.websitesList = websitesList;
	}
	/**
	 * @return the destinationPath
	 */
	public String getDestinationPath() {
		return destinationPath;
	}
	/**
	 * @param destinationPath the destinationPath to set
	 */
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	/**
	 * @return the destinationName
	 */
	public String getDestinationName() {
		return destinationName;
	}
	/**
	 * @param destinationName the destinationName to set
	 */
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	/**
	 * @return the destinationId
	 */
	public String getDestinationId() {
		return destinationId;
	}
	/**
	 * @param destinationId the destinationId to set
	 */
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	/**
	 * @return the destinationImage
	 */
	public String getDestinationImage() {
		return destinationImage;
	}
	/**
	 * @param destinationImage the destinationImage to set
	 */
	public void setDestinationImage(String destinationImage) {
		this.destinationImage = destinationImage;
	}
	/**
	 * @return the destinationTitle
	 */
	public String getDestinationTitle() {
		return destinationTitle;
	}
	/**
	 * @param destinationTitle the destinationTitle to set
	 */
	public void setDestinationTitle(String destinationTitle) {
		this.destinationTitle = destinationTitle;
	}
	/**
	 * @return the destinationCityTag
	 */
	public String getDestinationCityTag() {
		return destinationCityTag;
	}
	/**
	 * @param destinationCityTag the destinationCityTag to set
	 */
	public void setDestinationCityTag(String destinationCityTag) {
		this.destinationCityTag = destinationCityTag;
	}
	/**
	 * @param websitesList
	 * @param destinationPath
	 * @param destinationName
	 * @param destinationId
	 * @param destinationImage
	 * @param destinationTitle
	 * @param destinationCityTag
	 */
	public DestinationHotelsBean(List<WebsiteHotelsBean> websitesList, String destinationPath, String destinationName,
			String destinationId, String destinationImage, String destinationTitle, String destinationCityTag) {
		super();
		this.websitesList = websitesList;
		this.destinationPath = destinationPath;
		this.destinationName = destinationName;
		this.destinationId = destinationId;
		this.destinationImage = destinationImage;
		this.destinationTitle = destinationTitle;
		this.destinationCityTag = destinationCityTag;
	}
	
	
	

}
