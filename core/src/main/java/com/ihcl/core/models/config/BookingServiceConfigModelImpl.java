package com.ihcl.core.models.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.booking.ConfigurationService;

@Model(adaptables = org.apache.sling.api.resource.Resource.class,
        adapters = BookingServiceConfigModel.class)
public class BookingServiceConfigModelImpl implements BookingServiceConfigModel {

    private static final Logger LOG = LoggerFactory.getLogger(BookingServiceConfigModelImpl.class);

    @Inject
    private ConfigurationService config;

    private String mappedBookingConfirmationURL;

    private String mappedBookingFailedURL;

    @PostConstruct
    public void activate() {
        LOG.trace("Method -->> Entry -->> activate() in BookingServiceConfigModelImpl");

        if (config == null) {
            LOG.debug(" config at Model is  Null");
        }
        getMappedBookingConfirmationURL();
        getMappedBookingCartURL();
        LOG.trace("Method -->> Exit -->> activate() from BookingServiceConfigModelImpl");
    }

    private void getMappedBookingConfirmationURL() {
        LOG.trace("inside method:: getMappedBookingConfirmationURL()");
        mappedBookingConfirmationURL = config.getBookingConfirmationURL();
        LOG.debug("Config BookingConfirmationURL: {}", mappedBookingConfirmationURL);
    }

    private void getMappedBookingCartURL() {
        LOG.trace("inside method:: mappedBookingFailedURL()");
        mappedBookingFailedURL = config.getBookingFailedURL();
        LOG.debug("Config BookingFailedURL: {}", mappedBookingFailedURL);
    }

    @Override
    public String getBookingConfirmationURL() {
        return mappedBookingConfirmationURL;
    }

    @Override
    public String getBookingFailedURL() {
        return mappedBookingFailedURL;
    }

}
