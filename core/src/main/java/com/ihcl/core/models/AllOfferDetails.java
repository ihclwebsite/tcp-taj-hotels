/**
 * 
 */
package com.ihcl.core.models;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;


import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.dining.AllOfferSearchResult;


/**
 * @author TCS
 *
 */
@Component(service = { Servlet.class },
property = { Constants.SERVICE_DESCRIPTION + "=Servlet to perform a keyword search",
        "sling.servlet.paths=" + "/bin/all-offer" })
public class AllOfferDetails extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AllOfferDetails.class);
    
    private static final String BANNER_IMAGE = "bannerImage";
    
    private static final String OFFER_CATEGORY = "offerCategory";
 
	private static final String OFFER_RATE_CODE = "offerRateCode";
    
	private static final String OFFER_SHORT_DESCRIPTION = "offerShortDesc";
    
	private static final String OFFER_TITLE = "offerTitle";
	
	private static final String OFFER_TYPE = "offerType";
	
	private static final String ROUND_THE_YEAR_OFFER = "roundTheYearOffer";
	
	private static final String SEO_DESCRIPTION = "seoDescription";
	
	private static final String SEO_KEYWORDS = "seoKeywords";
	
	private static final String SEO_META_TAG = "seoMetaTag";
	
	private static final String PRIORITY = "priority";
	
	private static final String LIST_IMAGE = "listImage";
	
	private static final String JCR_CANONICAL_TAG = "jcr:canonical";
	
	private static final String OFFER_ENDS_ON = "offerEndsOn";
	
	private static final String OFFER_STARTS_FROM = "offerStartsFrom";
	
	
    private Session session;
    
    //Inject a Sling ResourceResolverFactory
    @Reference
    private ResourceResolverFactory resolverFactory;

    
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        try {
            response.getWriter().write(getOfferDetails(request, response));
        }
        catch(Exception e) {
        	LOGGER.error("error in getting data: "+ e.getMessage());
        	LOGGER.error("" + e);
        }
        
    }

    private String getOfferDetails(SlingHttpServletRequest request, SlingHttpServletResponse response)
    		throws LoginException, RepositoryException, JsonGenerationException, JsonMappingException, IOException, ParseException{
    	String contentRootPath = request.getParameter("contentRootPath");
    	String inDetailed = request.getParameter("inDetailed");
    	ObjectMapper objectMapper = new ObjectMapper();
    	AllOfferSearchResult offer = null;
    	Map<String, ArrayList<Object>> categorizedResponse = new HashMap<>();
        ArrayList<Object> offerList = new ArrayList<>();
        ResourceResolver resolver = null;
        
        //Invoke the adaptTo method to create a Session used to create a QueryManager
        resolver = resolverFactory.getServiceResourceResolver(null);
        session = resolver.adaptTo(Session.class);
         
        LOGGER.info("Created session");
           
        //Obtain the query manager for the session ...
        javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();
             
        //Setup the query based on user input     
        String sqlStatement="";
     
        sqlStatement = "SELECT * FROM [cq:PageContent] AS nodes WHERE ISDESCENDANTNODE(["+ contentRootPath +"]) "
        		+ "AND nodes.[sling:resourceType] = \"tajhotels/components/structure/hotel-specific-offer-details-page\" "
        		+ "AND nodes.[cq:lastReplicationAction] = \"Activate\"";
        
        LOGGER.info("sqlStatement " + sqlStatement);
        javax.jcr.query.Query query = queryManager.createQuery(sqlStatement,"JCR-SQL2");
        
        String childSqlStatement = "SELECT * FROM [nt:unstructured] AS nodes WHERE ISDESCENDANTNODE(["+ contentRootPath +"]) AND "
			        		+ "(nodes.[sling:resourceType] = \"tajhotels/components/content/offerdetails\" OR "
			        		+ "nodes.[sling:resourceType] = \"tajhotels/components/content/offerinclusion\" OR "
			        		+ "nodes.[sling:resourceType] = \"tajhotels/components/content/offer-terms-conditions\")";
        
        
        LOGGER.info("childSqlStatement " + childSqlStatement);
        javax.jcr.query.Query childQuery = queryManager.createQuery(childSqlStatement,"JCR-SQL2");       
        
        //Execute the query and get the results ...
        javax.jcr.query.QueryResult result = query.execute();
            
        //Iterate over the nodes in the results ...
        javax.jcr.NodeIterator nodeIter = result.getNodes();
        
        //Execute the childQuery and get the results ...
        javax.jcr.query.QueryResult childResult = childQuery.execute();
        	

		DateFormat outputFormat = new SimpleDateFormat("dd/MM/YYYY");
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        
        while ( nodeIter.hasNext() ) {
            
        	//For each node-- create an offer instance
        	offer = new AllOfferSearchResult();
                      
        	javax.jcr.Node node = nodeIter.nextNode();
            
	        //Iterate over the childNodes in the results ...
	        javax.jcr.NodeIterator childnodeIter = childResult.getNodes();
	        
	        offer.setPagePath(node.getParent().getPath().toString());
        	
	        if(inDetailed != null && inDetailed.equalsIgnoreCase("yes")) {
	        	while ( childnodeIter.hasNext() ) {
	        		javax.jcr.Node childNode = childnodeIter.nextNode();
	        		
	        		if(childNode.getParent().getPath().toString().contains(node.getParent().getPath().toString())) {
	        			if(childNode.getPath().toString().contains("offerdetails")) {
	    	            	if(childNode.hasProperty("validity")) {
	    	            		offer.setValidity(childNode.getProperty("validity").getString());
	    	            	}
	    	            	if(childNode.hasProperty("validityMsg")) {
	    	            		offer.setValidityMsg(childNode.getProperty("validityMsg").getString());
	    	            	}
	    	            	if(childNode.hasProperty("validityText")) {
	    	            		offer.setValidityText(childNode.getProperty("validityText").getString());
	    	            	}
	    	            	if(childNode.hasProperty("offerDescription")) {
	    	            		offer.setOfferDescription(childNode.getProperty("offerDescription").getString());
	    	            	}
	    	            	if(childNode.hasProperty("validityStartDate")) {
	    	            		offer.setValidityStartDate(childNode.getProperty("validityStartDate").getString());
	    	            	}
	    	            	if(childNode.hasProperty("discountValue")) {
	    	            		offer.setDiscountValue(childNode.getProperty("discountValue").getString());
	    	            	}
	        			}
	        			else if(childNode.getPath().toString().contains("offerinclusion")) {
	    	            	if(childNode.hasProperty("offerSpecifics")) {
	    	            		offer.setOfferSpecifics(childNode.getProperty("offerSpecifics").getString());
	    	            	}
	    	            	if(childNode.hasProperty("spa")) {
	    	            		offer.setSpa(childNode.getProperty("spa").getString());
	    	            	}
	    	            	if(childNode.hasProperty("stay")) {
	    	            		offer.setStay(childNode.getProperty("stay").getString());
	    	            	}
	    	            	if(childNode.hasProperty("stayInclusions")) {
	    	            		offer.setStayInclusions(childNode.getProperty("stayInclusions").getString());
	    	            	}
	    	            	if(childNode.hasProperty("sightseeing")) {
	    	            		offer.setSightseeing(childNode.getProperty("sightseeing").getString());
	    	            	}
	    	            	if(childNode.hasProperty("dining")) {
	    	            		offer.setDining(childNode.getProperty("dining").getString());
	    	            	}
	    	            	if(node.hasProperty("conveniences")) {
	    	            		offer.setConveniences(childNode.getProperty("conveniences").getString());
	    	            	}
	    	            	if(childNode.hasProperty("convenience")) {
	    	            		offer.setConvenience(childNode.getProperty("convenience").getString());
	    	            	}
	        			}
	        			else if(childNode.getPath().toString().contains("offer_terms_conditions")) {
	        				if(childNode.hasProperty("termsDescription")) {
	        					offer.setTermsDescription(childNode.getProperty("termsDescription").getString());
	        				}
	        			}
	        		}
	        	}
	        }

	            	
        	//Set all offer object fields
        	if(node.hasProperty(BANNER_IMAGE)) {
        		offer.setBannerImage(node.getProperty(BANNER_IMAGE).getString());
        	}
        	if(node.hasProperty(OFFER_CATEGORY)) {
        		offer.setOfferCategory(node.getProperty(OFFER_CATEGORY).getString());
        	}
        	if(node.hasProperty(OFFER_RATE_CODE)) {
        		offer.setOfferRateCode(node.getProperty(OFFER_RATE_CODE).getString());
        	}
        	if(node.hasProperty(OFFER_SHORT_DESCRIPTION)) {
        		offer.setOfferShortDesc(node.getProperty(OFFER_SHORT_DESCRIPTION).getString());
        	}
        	if(node.hasProperty(OFFER_TITLE)) {
        		offer.setOfferTitle(node.getProperty(OFFER_TITLE).getString());
        	}
        	if(node.hasProperty(OFFER_TYPE)) {
        		offer.setOfferType(node.getProperty(OFFER_TYPE).getString());
        	}
        	if(node.hasProperty(ROUND_THE_YEAR_OFFER)) {
        		offer.setRoundTheYearOffer(node.getProperty(ROUND_THE_YEAR_OFFER).getBoolean());
        	}
        	if(node.hasProperty(SEO_DESCRIPTION)) {
        		offer.setSeoDescription(node.getProperty(SEO_DESCRIPTION).getString());
        	}
        	if(node.hasProperty(SEO_KEYWORDS)) {
        		offer.setSeoKeywords(node.getProperty(SEO_KEYWORDS).getString());
        	}
        	if(node.hasProperty(SEO_META_TAG)) {
        		offer.setSeoMetaTag(node.getProperty(SEO_META_TAG).getString());
        	}
        	if(node.hasProperty(PRIORITY)) {
        		offer.setPriority(node.getProperty(PRIORITY).getString());
        	}
        	if(node.hasProperty(LIST_IMAGE)) {
        		offer.setListImage(node.getProperty(LIST_IMAGE).getString());
        	}
        	if(node.hasProperty(JCR_CANONICAL_TAG)) {
        		offer.setJcrCanonical(node.getProperty(JCR_CANONICAL_TAG).getString());
        	}
        	//
        	if(node.hasProperty(OFFER_ENDS_ON)) {
        		Date date = inputFormat.parse(node.getProperty(OFFER_ENDS_ON).getString());
        		String outputText = outputFormat.format(date);
        		offer.setOfferEndsOn(outputText);
        	}
        	if(node.hasProperty(OFFER_STARTS_FROM)) {
        		Date date = inputFormat.parse(node.getProperty(OFFER_STARTS_FROM).getString());
        		String outputText = outputFormat.format(date);
        		offer.setOfferStartsFrom(outputText);
        	}
        	
        	
                        
            //Push the offer Object to the list
        	offerList.add(offer);
        }
		 // Log out  
        session.logout();
        categorizedResponse.put("offers", offerList);
        response.setContentType("application/json");
        return objectMapper.writeValueAsString(categorizedResponse);
    }


}
