/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */
public class Hotel_type {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + "]";
    }
}
