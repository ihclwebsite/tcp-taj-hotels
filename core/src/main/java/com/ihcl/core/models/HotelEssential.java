package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class HotelEssential extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<HotelEssentialBean> essentialObj = new ArrayList<HotelEssentialBean>();
    

    @Override
    public void activate() throws Exception {

    	log.info("*** Entering ");
        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();

        while (ni.hasNext()) {

            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setHotelEssentials(ni2);
        }
    }

    private void setHotelEssentials(NodeIterator ni2) {

        try {

            while (ni2.hasNext()) {

            	HotelEssentialBean imgObj = new HotelEssentialBean();
                Node grandChild = ni2.nextNode();

                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
      
                String desc = getParameters(grandChild, "hotelEssentialDesc");
                String pdfDownload = getParameters(grandChild, "pathToDownloadPdfFrom");
                if ((desc.trim() != null && !desc.trim().isEmpty())
                        || (pdfDownload.trim() != null && !pdfDownload.trim().isEmpty())) {
                    log.debug("desc value is : " + desc);
                    imgObj.setHotelEssentialDesc(desc);
                    imgObj.setPathToDownloadPdfFrom(pdfDownload);
                    essentialObj.add(imgObj);
                }    
            }
        }

        catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }

    }
    
    private String getParameters ( Node grandChild, String key) throws RepositoryException {
    	String value=null;
    	try {
            value=grandChild.getProperty(key).getString();
        } catch (PathNotFoundException e) {
           value=" ";
        }
		return value;
    }

    public List<HotelEssentialBean> getHotelEssentialBean() {
        return essentialObj;
    }

}
