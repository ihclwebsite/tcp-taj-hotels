package com.ihcl.core.models.banner;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <pre>
 * BannerCarousel.java
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :03-Jan-2020
 * @ClassName : BannerCarousel.java
 * @Description : com.ihcl.core.models.banner-BannerCarousel.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author                 description
 *  ===========     ==================   =========================
 *  03-Jan-2020     Subharun Mukherjee            Create
 *
 *               </pre>
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = BannerCarousel.class)
public class BannerCarousel {

    private static final Logger LOG = LoggerFactory.getLogger(BannerCarousel.class);

    private static final String BLANK_VALUE = "";

    @Inject
    @Optional
    private Node currentNode;

    /**
     * @return bannerList
     */
    public List<BannerBean> getBanners() {

        List<BannerBean> banners = new ArrayList<>();
        try {
            Node bannerNode = currentNode.getNode("banners");
            if (null != bannerNode) {
                banners = getBannerResults(bannerNode);
            }
        } catch (RepositoryException e) {
            LOG.error("Error while returning banners : {}", e.getMessage());
            LOG.debug("Error while returning banners : {}", e);
        }
        return banners;
    }

    /**
     * @return tierBannerList
     */
    public List<BannerBean> getTierBanners() {

        List<BannerBean> tierBanners = new ArrayList<>();
        try {
            Node bannerNode = currentNode.getNode("tierBanners");
            if (null != bannerNode) {
                tierBanners = getBannerResults(bannerNode);
            }
        } catch (RepositoryException e) {
            LOG.error("Error while returning tier banners : {}", e.getMessage());
            LOG.debug("Error while returning tier banners : {}", e);
        }
        return tierBanners;

    }

    /**
     * @param bannerNode
     * @return resultList
     */
    private List<BannerBean> getBannerResults(Node bannerNode) {
        List<BannerBean> resultList = new ArrayList<>();
        NodeIterator itemNodes = null;
        try {
            itemNodes = bannerNode.getNodes();
            while (itemNodes.hasNext()) {
                Node bannerItem = itemNodes.nextNode();
                BannerBean bannerBean = getBannerBean(bannerItem);
                resultList.add(bannerBean);
            }
        } catch (RepositoryException e) {
            LOG.error("Error while returning banner results : {}", e.getMessage());
            LOG.debug("Error while returning banner results : {}", e);
        }

        return resultList;
    }

    /**
     * @param bannerItem
     * @return bannerBean
     */

    private BannerBean getBannerBean(Node bannerItem) {
        BannerBean bannerBean = null;
        try {
            String bannerDescDesk = getValueFrom(bannerItem, "bannerDescDesk");
            String bannerDescMob = getValueFrom(bannerItem, "bannerDescMob");
            String bannerImage = getValueFrom(bannerItem, "bannerImage");
            String bannerTitlePosition = getValueFrom(bannerItem, "bannerTitlePosition");
            String bannerButtonLayout = getValueFrom(bannerItem, "bannerButtonLayout");
            String bannerLinkText = getValueFrom(bannerItem, "bannerLinkText");
            String bannerLinkUrl = getValueFrom(bannerItem, "bannerLinkUrl");
            String bannerSubTitle = getValueFrom(bannerItem, "bannerSubTitle");
            String bannerSubTitlePosition = getValueFrom(bannerItem, "bannerSubTitlePosition");
            String bannerDescPosition = getValueFrom(bannerItem, "bannerDescPosition");
            String bannerTitleFont = getValueFrom(bannerItem, "bannerTitleFont");
            String bannerTitleFontSize = getValueFrom(bannerItem, "bannerTitleFontSize");
            String bannerSubtitleFont = getValueFrom(bannerItem, "bannerSubtitleFont");
            String bannerSubtitleFontSize = getValueFrom(bannerItem, "bannerSubtitleFontSize");
            String bannerDescFont = getValueFrom(bannerItem, "bannerDescFont");
            String bannerDescFontSize = getValueFrom(bannerItem, "bannerDescFontSize");
            String bannerTitle = getValueFrom(bannerItem, "bannerTitle");
            String mobileImage = getValueFrom(bannerItem, "mobileImage");
            String tintOpacity = getValueFrom(bannerItem, "tintOpacity");
            String bannerRichText = "";
            if(getValueFrom(bannerItem, "bannerRichText") != null) {
            	bannerRichText = getValueFrom(bannerItem, "bannerRichText");
            }
            List<String> context = getMultiValueFrom(bannerItem, "context");

            bannerBean = new BannerBean(bannerDescDesk, bannerDescMob, bannerImage, bannerTitlePosition, bannerLinkText,
                    bannerLinkUrl, bannerSubTitle, bannerSubTitlePosition, bannerDescPosition, bannerTitleFont,
                    bannerTitleFontSize, bannerTitle, mobileImage, tintOpacity, bannerSubtitleFont,
                    bannerSubtitleFontSize, bannerDescFont, bannerDescFontSize, bannerButtonLayout, bannerRichText, context);


        } catch (Exception e) {
            LOG.error("Error while getting banner bean : {}", e.getMessage());
            LOG.debug("Error while getting banner bean : {}", e);
        }
        return bannerBean;
    }

    /**
     * @param bannerItem
     * @param string
     * @return valueList
     */
    private List<String> getMultiValueFrom(Node bannerItem, String property) {
        List<String> valueList = new ArrayList<>();
        Property properties = null;
        try {
            properties = bannerItem.getProperty(property);
            if (properties.isMultiple()) {
                Value[] values = properties.getValues();
                for (Value value : values) {
                    valueList.add(value.getString());
                }
            } else {
                Value value = properties.getValue();
                valueList.add(value.getString());
            }
        } catch (RepositoryException e) {
            LOG.error("Error while returning banners multi property value : {}", e.getMessage());
            LOG.debug("Error while returning banners multi property value : {}", e);
        }
        return valueList;
    }

    /**
     * @param bannerItem
     * @param property
     * @return propertyValue
     */
    private String getValueFrom(Node bannerItem, String property) {
        String value = null;
        try {
            value = bannerItem.getProperty(property).getValue().getString();
        } catch (IllegalStateException | RepositoryException e) {
            LOG.error("Error while returning banners property value : {}", e.getMessage());
            LOG.debug("Error while returning banners property value : {}", e);
        }
        return null != value ? value : BLANK_VALUE;
    }


}
