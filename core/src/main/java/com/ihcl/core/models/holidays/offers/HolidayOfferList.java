package com.ihcl.core.models.holidays.offers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.core.util.URLMapUtil;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HolidayOfferList {

    protected static final Logger LOG = LoggerFactory.getLogger(HolidayOfferList.class);

    private static String RESOURCE_NAME_HOTEL_BANNER = "bannerImage";

    @Inject
    Resource resource;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @Inject
    @Optional
    @Named("offerList")
    private String[] offerListString;

    @Inject
    @Optional
    @Named("currencySymbol")
    @Default(values = "INR")
    private String currencyString;

    private List<HotelOffer> hotelOfferList = new ArrayList<HotelOffer>();

    private String currencySymbol;

    ResourceResolver resourceResolver = null;

    @PostConstruct
    public void init() {
        String methodEntry = "init";
        LOG.trace("Method Entry => " + methodEntry);
        try {
            LOG.trace("Obtained Offer List JSON string as  =>" + offerListString);
            LOG.trace("Obtained currency string as " + currencyString);
            String symbol = currencyFetcherService.getCurrencySymbolValue(currencyString);
            if (null != symbol) {
                setCurrencySymbol(symbol);
            } else {
                setCurrencySymbol("");
            }
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            if (offerListString != null && offerListString.length > 0) {
                for (String jsonStr : offerListString) {
                    LOG.trace("JSON string " + jsonStr);
                    ObjectMapper mapper = new ObjectMapper();
                    HotelOffer hotelOfferObj = new HotelOffer();
                    hotelOfferObj = mapper.readValue(jsonStr, HotelOffer.class);
                    LOG.trace("value" + hotelOfferObj.getHotelName());
                    String hotelPath = hotelOfferObj.getHotelPath();
                    hotelOfferObj
                            .setHotelPath(hotelPath.contains("/ama/") ? getMappedPath(hotelPath + "/accommodations")
                                    : getMappedPath(hotelPath + "/rooms-and-suites"));
                    String viewDetailsPath = hotelOfferObj.getViewDetails();
                    if (null != viewDetailsPath && !viewDetailsPath.isEmpty()) {
                        hotelOfferObj.setViewDetails(getMappedPath(viewDetailsPath));
                    }
                    Resource hotelJCRResource = resourceResolver.getResource(hotelPath + "/jcr:content");
                    LOG.trace("Obtained hotelJCRResource as " + hotelJCRResource);
                    if (null != hotelJCRResource) {
                        String imageUrl = extractBannerImage(hotelJCRResource);
                        LOG.trace("Obtained bannerImage as " + imageUrl);
                        if (imageUrl != null) {
                            hotelOfferObj.setHotelImagePath(imageUrl);
                        }
                    }
                    hotelOfferList.add(hotelOfferObj);
                }
            }
        } catch (JsonParseException e) {
            LOG.error("Error occured while extracting offerListString json String Array to corresponding JSON Object");
        } catch (JsonMappingException e) {
            LOG.error("Error occured while mapping offerListString json String Array to corresponding POJO Object");
        } catch (Exception e) {
            LOG.error("Error occured while building offerList   " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (resourceResolver != null && resourceResolver.isLive()) {
                LOG.trace("Closing the unclosed resource resolver");
                resourceResolver.close();
            }
        }
        LOG.trace("Method Exit => " + methodEntry);
    }

    private String getMappedPath(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }

    private String extractBannerImage(Resource resource) {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                ValueMap valMap = hotelBanner.getValueMap();
                String imageURL = String.valueOf(valMap.get(RESOURCE_NAME_HOTEL_BANNER));
                return imageURL;
            }
            LOG.trace("Returning null [condition failed], hotelBanner is null");
            return null;
        }
        LOG.trace("Returning null [condition failed], bannerParsys is null");
        return null;
    }

    public List<HotelOffer> getHotelOfferList() {
        return hotelOfferList;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
