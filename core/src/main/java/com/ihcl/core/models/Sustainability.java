/**
 *
 */
package com.ihcl.core.models;

import org.apache.sling.api.resource.ValueMap;

/**
 * @author moonraft
 *
 */
public interface Sustainability {

    /**
     * @return
     */
    ValueMap getValueMap();

}
