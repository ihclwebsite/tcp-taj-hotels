package com.ihcl.core.models;

public class Review {
	
	public String name;
	public String commentTitle;
	public String comment;
	public String rating;
	public String commentdate;
	public String ratingleveltext;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCommentTitle() {
		return commentTitle;
	}
	public void setCommentTitle(String commentTitle) {
		this.commentTitle = commentTitle;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getCommentdate() {
		return commentdate;
	}
	public void setCommentdate(String commentdate) {
		this.commentdate = commentdate;
	}
	
	public String getRatingleveltext() {
		return ratingleveltext;
	}
	public void setRatingleveltext(String ratingleveltext) {
		this.ratingleveltext = ratingleveltext;
	}
	

}
