package com.ihcl.core.models.reviewsPojo;

public class Reviews_distribution {

    private String count;

    private String stars;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    @Override
    public String toString() {
        return "ClassPojo [count = " + count + ", stars = " + stars + "]";
    }
}
