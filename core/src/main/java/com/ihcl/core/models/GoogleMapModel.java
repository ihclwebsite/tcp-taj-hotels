package com.ihcl.core.models;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.GoogleMapService;

public class GoogleMapModel {
	private final Logger  LOG = LoggerFactory.getLogger(getClass());

    @Self
    Resource resource;
	@Inject
	GoogleMapService googlemapService;
	private String lat;
	private String lng;
    private String address;
    private String[] latlng;	

	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}


	@PostConstruct
	protected void init() {
	resource.getPath();
	ValueMap valueMap = resource.adaptTo(ValueMap.class);
	address = valueMap.get("source", String.class);
	LOG.debug("The value of source address:"+address);
	latlng=googlemapService.getLatLong(address);

	}


}
