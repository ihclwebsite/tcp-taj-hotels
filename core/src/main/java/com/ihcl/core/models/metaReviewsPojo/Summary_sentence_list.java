package com.ihcl.core.models.metaReviewsPojo;

public class Summary_sentence_list {

    private String sentiment;

    private String text;

    private String[] category_id_list;

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getCategory_id_list() {
        return category_id_list;
    }

    public void setCategory_id_list(String[] category_id_list) {
        this.category_id_list = category_id_list;
    }

    @Override
    public String toString() {
        return "ClassPojo [sentiment = " + sentiment + ", text = " + text + ", category_id_list = " + category_id_list
                + "]";
    }
}
