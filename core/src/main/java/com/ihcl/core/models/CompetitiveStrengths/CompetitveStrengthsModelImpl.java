package com.ihcl.core.models.CompetitiveStrengths;

/**
 * @author Vijay.pal
 *
 */
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.CompetitiveStrengthsModel;
import com.ihcl.core.util.services.TajCommonsFetchResource;

@Model(
        adaptables = SlingHttpServletRequest.class,
        adapters = CompetitiveStrengthsModel.class)
public class CompetitveStrengthsModelImpl implements CompetitiveStrengthsModel {

    private static final Logger LOG = LoggerFactory.getLogger(CompetitiveStrengthsModel.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private Resource resource;

    ValueMap valueMap;

    @PostConstruct
    public void init() {
        LOG.trace(" Method Entry : init()");
        resource = request.getResource();
        LOG.debug("resource Path In competitive strengths  Model : " + resource.getPath());
        if (tajCommonsFetchResource != null) {
            valueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "competitive_stength_",
                    "tajhotels/components/content/competitive-stength-details");
            LOG.debug("valuemap from individual competitive strength :" + valueMap);
        } else {
            LOG.debug("tajCommonsFetchResource is null :");

        }
        LOG.trace(" Method Exit : init()");
    }

    @Override
    public ValueMap getValueMap() {
        LOG.trace(" Method Entry : getvalueMap()");
        return valueMap;
    }

}
