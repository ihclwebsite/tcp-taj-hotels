package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class CustomMultifieldImageComponent extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<CustomMultifieldBean> ls = new ArrayList<CustomMultifieldBean>();

    @Override
    public void activate() throws Exception {

        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();

        while (ni.hasNext()) {

            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setMultiFieldItems(ni2);
        }
    }

    private void setMultiFieldItems(NodeIterator ni2) {

        try {

            while (ni2.hasNext()) {

                String imagepath;
                CustomMultifieldBean obj = new CustomMultifieldBean();
                Node grandChild = ni2.nextNode();
                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
                imagepath = grandChild.getProperty("imagepath").getString();
                obj.setImagepath(imagepath);
                ls.add(obj);

            }
        } catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }
    }

    public List<CustomMultifieldBean> getMultiFieldItems() {

        for (int i = 0; i < ls.size(); i++)
            ls.get(i).setIndex(i);

        return ls;
    }
}

