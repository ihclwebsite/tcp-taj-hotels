package com.ihcl.core.models.metaReviewsPojo;

public class Category_list {

    private String sentiment;

    private String text;

    private Highlight_list[] highlight_list;

    private String category_name;

    private String count;

    private Sub_category_list[] sub_category_list;

    private String relevance;

    private float score;

    private Summary_sentence_list[] summary_sentence_list;

    private String category_id;

    private String short_text;

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Highlight_list[] getHighlight_list() {
        return highlight_list;
    }

    public void setHighlight_list(Highlight_list[] highlight_list) {
        this.highlight_list = highlight_list;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public Sub_category_list[] getSub_category_list() {
        return sub_category_list;
    }

    public void setSub_category_list(Sub_category_list[] sub_category_list) {
        this.sub_category_list = sub_category_list;
    }

    public String getRelevance() {
        return relevance;
    }

    public void setRelevance(String relevance) {
        this.relevance = relevance;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Summary_sentence_list[] getSummary_sentence_list() {
        return summary_sentence_list;
    }

    public void setSummary_sentence_list(Summary_sentence_list[] summary_sentence_list) {
        this.summary_sentence_list = summary_sentence_list;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getShort_text() {
        return short_text;
    }

    public void setShort_text(String short_text) {
        this.short_text = short_text;
    }

    @Override
    public String toString() {
        return "ClassPojo [sentiment = " + sentiment + ", text = " + text + ", highlight_list = " + highlight_list
                + ", category_name = " + category_name + ", count = " + count + ", sub_category_list = "
                + sub_category_list + ", relevance = " + relevance + ", score = " + score + ", summary_sentence_list = "
                + summary_sentence_list + ", category_id = " + category_id + ", short_text = " + short_text + "]";
    }
}
