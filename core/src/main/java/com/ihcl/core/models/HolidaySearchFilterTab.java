package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HolidaySearchFilterTab {
	
	protected static final Logger LOG = LoggerFactory.getLogger(HolidaySearchFilterTab.class);
	
	@Inject  @Optional
    private String countryLabel;
	
	@Inject  @Optional
    private String experienceLabel;

	@Inject  @Optional
    private String priceLabel;
	
	@Inject  @Optional
    private String seasonLabel;
	
	@Inject  @Optional
    private String themeLabel;
	 
	@Inject  @Optional
    private List<String> countriesDropdownList;
	
	@Inject  @Optional
    private List<String> experiencesDropdownList;
	
	@Inject  @Optional
    private List<String> pricesDropdownList;
	
	@Inject  @Optional
    private List<String> seasonDropdownList;
	
	@Inject  @Optional
    private List<String> themesDropdownList;
	
	@Inject  @Optional
	private String nightsLabel;
	
	@Inject  @Optional
	private List<String> nightsDropdownList;
	
	@Inject @Optional
	private List<String> nightsDropdownListValues;
	
	@Inject @Optional
	private List<String> weddingvenues;
	
	@Inject @Optional
	private String weddingvenuelabel;
	
	@Inject  @Optional
	private String capacityLabel;
	
	@Inject @Optional
	private List<String> capacityDropDownList;
	
	@Inject @Optional
	ResourceResolverFactory resolverFactory;
	
	private String jsonString;
		
	@PostConstruct
	public void init() {
		LOG.trace("Method Entry of HolidaySearchFilterTab :: init");
		ResourceResolver resolver = null;
		try {
			resolver = resolverFactory.getServiceResourceResolver(null);
			setJsonString(processAndFormJSONbody(resolver));
			LOG.info("JSON Object formed :::::: " + getJsonString());
		} catch (JSONException |LoginException e) {
			LOG.error("Exception found in init method while trying to get the resolver :: {}", e.getMessage());
			LOG.debug("Exception found in init method while trying to get the resolver :: {}", e);
		} finally {
			if (null != resolver) {
				resolver.close();
			}
			LOG.trace("Method Exit of HolidaySearchFilterTab :: init ");
		}
	}
	
	private String processAndFormJSONbody(ResourceResolver resolver) throws JSONException  {
		
		JSONArray arrayObject = new JSONArray();
		try {
		formChildObject(experienceLabel,experiencesDropdownList, "experiences", arrayObject);
		formChildObject(countryLabel,countriesDropdownList, "country", arrayObject);
		formChildObject(themeLabel,themesDropdownList, "theme", arrayObject);
		formChildObject(seasonLabel,seasonDropdownList , "season", arrayObject);
		formChildObject(priceLabel,pricesDropdownList, "price", arrayObject);
		formChildObject(capacityLabel, capacityDropDownList, "capacity", arrayObject);
		formChildObject(nightsLabel, nightsDropdownList, "stay", arrayObject);
			if (null != weddingvenues) {
				JSONObject weddingTheme = getWeddingTagsObject(resolver);
				arrayObject.put(weddingTheme);
			}
		} catch (JSONException e) {
			LOG.error("Exception found while creating the JSON Body :: {}", e.getMessage());
			LOG.debug("Exception found while creating the JSON Body :: {}", e);
		}
		return arrayObject.toString();
		
	}
	
	private JSONObject getWeddingTagsObject(ResourceResolver resolver) {
		try {
			TagManager tagMgr = resolver.adaptTo(TagManager.class);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("selector", weddingvenuelabel);
			jsonObj.put("selectorCode", "wedding");
			String selectedOption = null;
			JSONArray option = new JSONArray();
			for (String venue : weddingvenues) {
				Tag venueTag = tagMgr.resolve(venue);
				if (null != venueTag) {
					String tagTitle = venueTag.getTitle();
					String tagName = venueTag.getTagID();
					JSONObject tempObj = new JSONObject();
					tempObj.put("label", tagTitle);
					tempObj.put("value", tagName);
					option.put(tempObj);
				}
			}
			jsonObj.put("option", option);
			jsonObj.put("selected", selectedOption);
			return jsonObj;
		} catch (JSONException e) {
			LOG.error("Exception found while getting the wedding venues :: {} ", e.getMessage());
		}
		return null;
	}

	private void formChildObject(String label,List<String> dropdownList, String selectorCode, JSONArray arrayObject) throws JSONException {
		JSONObject jsonObj = new JSONObject();
		if (null != label) {
			jsonObj.put("selector", label);
			jsonObj.put("selectorCode", selectorCode);
			String selectedOption = null;
			JSONArray option = new JSONArray();
			if (selectorCode.equals("stay")) {
				for (int i = 0, j = 0; i < nightsDropdownList.size() && j < nightsDropdownListValues.size(); i++, j++) {
					JSONObject tempObj = new JSONObject();
					tempObj.put("label", nightsDropdownList.get(i));
					tempObj.put("value", nightsDropdownListValues.get(j));
					option.put(tempObj);
				}
			} else {
				for (String dropdown : dropdownList) {
					JSONObject tempObj = new JSONObject();
					tempObj.put("label", dropdown);
					tempObj.put("value", dropdown);
					option.put(tempObj);
				}
			}
			jsonObj.put("option", option);
			jsonObj.put("selected", selectedOption);
			arrayObject.put(jsonObj);
		}
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

}
