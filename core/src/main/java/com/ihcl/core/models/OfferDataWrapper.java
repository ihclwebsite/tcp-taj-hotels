package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.offers.Offer;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })

public class OfferDataWrapper {
	
	private static final Logger LOG = LoggerFactory.getLogger(OfferDataWrapper.class);

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	@Optional
	private String resourcePath;

	private Offer offer;
	
	private String draggedImagePath;

	@PostConstruct
	public void init() {
		resourcePath = resourcePath + "/jcr:content";
		Resource resource = resourceResolver.resolve(resourcePath);
		offer = resource.adaptTo(Offer.class);
		Resource imageResource = resourceResolver.resolve(resourcePath+"/root/offerdetails");
		if(null != imageResource) {
			ValueMap valueMap = imageResource.adaptTo(ValueMap.class);
			if(null != valueMap) {
				draggedImagePath = getProperty(valueMap, "fileReference", String.class, "");
			}
		}
	}

	public Offer getOffer() {
		return offer;
	}
	
	public String getDraggedImagePath() {
		return draggedImagePath;
	}
	
	/**
     * @param valueMap
     * @param key
     * @param type
     * @param defaultValue
     */
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type, T defaultValue) {
        T value;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        } else {
            value = defaultValue;
        }
        LOG.trace("Returning value for: " + key + " : " + value);
        return value;
    }

}
