package com.ihcl.core.models.tic.redeemCard;

import java.util.Map;

import org.apache.sling.api.resource.ValueMap;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface IRedeemCardModel {

    ValueMap getProperties();

    Map<String, String> getCurrencies();
}
