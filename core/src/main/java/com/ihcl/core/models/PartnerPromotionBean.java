/**
 *
 */
package com.ihcl.core.models;


public class PartnerPromotionBean {


    private String title;

    private String imagePath;

    private String logoImagePath;

    private String partnerResourceUrl;

    private String partnerDetails;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getImagePath() {
        return imagePath;
    }


    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    public String getLogoImagePath() {
        return logoImagePath;
    }


    public void setLogoImagePath(String logoImagePath) {
        this.logoImagePath = logoImagePath;
    }


    public String getPartnerResourceUrl() {
        return partnerResourceUrl;
    }


    public void setPartnerResourceUrl(String partnerResourceUrl) {
        this.partnerResourceUrl = partnerResourceUrl;
    }


    public String getPartnerDetails() {
        return partnerDetails;
    }


    public void setPartnerDetails(String partnerDetails) {
        this.partnerDetails = partnerDetails;
    }


}
