package com.ihcl.core.models.holidays.inspirational;

public class InspirationCard {

	private String inspirationVideoPath;
	private String inspirationPagePath;
	private String inspirationTitle;
	private String inspirationDescription;
	private String inspirationShortText;

	public String getInspirationVideoPath() {
		return inspirationVideoPath;
	}

	public void setInspirationVideoPath(String inspirationVideoPath) {
		this.inspirationVideoPath = inspirationVideoPath;
	}

	public String getInspirationPagePath() {
		return inspirationPagePath;
	}

	public void setInspirationPagePath(String inspirationPagePath) {
		this.inspirationPagePath = inspirationPagePath;
	}

	public String getInspirationTitle() {
		return inspirationTitle;
	}

	public void setInspirationTitle(String inspirationTitle) {
		this.inspirationTitle = inspirationTitle;
	}

	public String getInspirationDescription() {
		return inspirationDescription;
	}

	public void setInspirationDescription(String inspirationDescription) {
		this.inspirationDescription = inspirationDescription;
	}

	public String getInspirationShortText() {
		return inspirationShortText;
	}

	public void setInspirationShortText(String inspirationShortText) {
		this.inspirationShortText = inspirationShortText;
	}

}
