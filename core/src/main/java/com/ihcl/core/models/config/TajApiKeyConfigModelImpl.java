/**
 *
 */
package com.ihcl.core.models.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.config.TajApiKeyConfigurationService;

/**
 * @author Srikanta.moonraft
 *
 */
@Model(
        adaptables = { org.apache.sling.api.resource.Resource.class, SlingHttpServletRequest.class },
        adapters = TajApiKeyConfigModel.class)
public class TajApiKeyConfigModelImpl implements TajApiKeyConfigModel {

    private static final Logger LOG = LoggerFactory.getLogger(TajApiKeyConfigModelImpl.class);

    @Inject
    private TajApiKeyConfigurationService globalConfig;

    @PostConstruct
    public void activate() {
        LOG.trace("Method -->> Entry -->> activate()");

        if (globalConfig == null) {
            LOG.debug(" globalConfig at Model is  Null");
        }
        LOG.trace("Method -->> Exit -->> activate()");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.GlobalConfigurationService#getOpenWeatherMapApiKey()
     */
    @Override
    public String getOpenWeatherMapApiKey() {
        // TODO Auto-generated method stub
        return globalConfig.getOpenWeatherMapApiKey();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.services.config.GlobalConfigurationService#getGoogleMapApiKey()
     */
    @Override
    public String getGoogleMapApiKey() {
        // TODO Auto-generated method stub
        return globalConfig.getGoogleMapApiKey();
    }

    @Override
    public String getHotelChainCode() {
        // TODO Auto-generated method stub
        return globalConfig.getHotelChainCode();
    }

    @Override
    public String getTajAdminEmailId() {

        return globalConfig.getTajAdminEmailId();
    }

	@Override
	public String getIhclOwnerUsername() {
		// TODO Auto-generated method stub
		return globalConfig.getIhclOwnerUsername();
	}

	@Override
	public String getIhclOwnerPassword() {
		// TODO Auto-generated method stub
		return globalConfig.getIhclOwnerPassword();
	}

	@Override
	public String getChatBotId() {
		// TODO Auto-generated method stub
		return globalConfig.getChatBotId();
	}


}
