/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(adaptables = Resource.class,
        adapters = IhclBlockModel.class)

public class IhclBlockModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclBlockModel.class);

    @Self
    private Resource resource;

    public List<IhclBlockBean> list = new ArrayList<IhclBlockBean>();


    /**
     * Getter for the field list
     *
     * @return the list
     */
    public List<IhclBlockBean> getList() {
        return list;
    }


    /**
     * Setter for the field list
     *
     * @param list
     *            the list to set
     */

    public void setList(List<IhclBlockBean> list) {
        this.list = list;
    }


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getfiguresValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */

    public List<IhclBlockBean> getfiguresValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] listBlocks = valueMap.get("list", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != listBlocks) {
            for (int i = 0; i < listBlocks.length; i++) {
                JsonElement jsonElement = gson.fromJson(listBlocks[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclBlockBean blockBean = new IhclBlockBean();
                LOG.trace("jsonObject :" + jsonObject);


                if (null != jsonObject.get("title")) {
                    blockBean.setTitle(jsonObject.get("title").getAsString());
                }

                if (null != jsonObject.get("titleaddon")) {
                    blockBean.setTitleAddOn(jsonObject.get("titleaddon").getAsString());
                }

                if (null != jsonObject.get("description")) {
                    blockBean.setDescription(jsonObject.get("description").getAsString());
                }
                if (null != jsonObject.get("longdescription")) {
                    blockBean.setLongdescription(jsonObject.get("longdescription").getAsString());
                }

                list.add(blockBean);

            }

        }
        return list;
    }
}
