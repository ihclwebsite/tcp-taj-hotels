package com.ihcl.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class UpcomingEvent {

    @Inject
    @Optional
    private String eventTitle;

    @Inject
    @Optional
    private String eventLocation;

    @Inject
    @Optional
    private String eventOpeningDateDesk;

    @Inject
    @Optional
    private String eventOpeningDateMob;

    @Inject
    @Optional
    private String eventLinkText;

    @Inject
    @Optional
    private String eventLinkUrl;

    @Inject
    @Optional
    private String eventImage;

    public String getEventTitle() {
        return eventTitle;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public String getEventOpeningDateDesk() {
        return eventOpeningDateDesk;
    }

    public String getEventOpeningDateMob() {
        return eventOpeningDateMob;
    }

    public String getEventLinkText() {
        return eventLinkText;
    }

    public String getEventLinkUrl() {
        return eventLinkUrl;
    }

    public String getEventImage() {
        return eventImage;
    }
}
