/**
 *
 */
package com.ihcl.core.models.rateandreview;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Servlet.class,
        property = { Constants.SERVICE_DESCRIPTION + "=Servlet to create a hotels ratings",
                "sling.servlet.paths=" + "/bin/generate/hotels/ratings"

        })
public class AllHotelsRatingServlet extends SlingSafeMethodsServlet {


    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(AllHotelsRatingServlet.class);

    @Reference
    AllHotelsRatingService service;

    @Reference
    private ResourceResolverFactory resolverFactory;

    ResourceResolver resourceResolver;

    @Override
    protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
            throws ServletException, IOException {
        log.info("Entered AllHotelsRatingServlet");

        try {
            PrintWriter pw = resp.getWriter();
            service.getSealApiResponse();
            pw.write("successfully retrieved hotle ratings");
        } catch (Exception e) {
            log.error("Error/Exception is thrown " + e.toString(), e);
        }
    }
}
