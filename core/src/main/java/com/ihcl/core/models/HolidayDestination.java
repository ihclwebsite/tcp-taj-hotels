package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.services.search.HotelsSearchService;

@Model(
        adaptables = Resource.class)
public class HolidayDestination {

    private static final Logger LOG = LoggerFactory.getLogger(HolidayDestination.class);

    @Inject
    HotelsSearchService hotelsSearchService;

    @Inject
    @Optional
    @Named("destinationName")
    private String destinationTitle;

    @Inject
    @Optional
    private List<String> city;

    private String destinationPath;

    @Self
    Resource resource;

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        destinationPath = resource.getParent().getPath();
    }

    public List<String> getCity() {
        return city;
    }

    public String getCountry() {
        LOG.trace("Method Entry : getCountry()");
        String countryName = "";
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (city != null) {
                LOG.trace("city array :" + city);
                if (tagRoot != null) {
                    tagRoot = tagManager.resolve(city.get(0));
                    LOG.trace("Tag Root Path : " + tagRoot.getPath());
                    countryName = tagRoot.getParent().getTitle();
                }
            }
        }
        LOG.trace("Method Exit -> returning  : Country name : " + countryName);
        return countryName;
    }

    public String getDestinationTitle() {
        return destinationTitle;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

}
