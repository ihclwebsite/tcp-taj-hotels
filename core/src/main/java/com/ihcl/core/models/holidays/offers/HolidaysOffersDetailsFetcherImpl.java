/**
 *
 */
package com.ihcl.core.models.holidays.offers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.models.ParticipatingHotel;
import com.ihcl.core.models.holidays.hotel.UserSellingPoints;
import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.core.services.search.SearchProvider;
import com.ihcl.tajhotels.constants.CrxConstants;

/**
 * @author moonraft
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = IHolidaysOffersDetailsFetcher.class)
public class HolidaysOffersDetailsFetcherImpl implements IHolidaysOffersDetailsFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(HolidaysOffersDetailsFetcherImpl.class);

    private static final String HOTEL_DETAILS_COMPONENT_RESOURCE_TYPE = "tajhotels/components/content/holidays-destination/holidays-hotel-details";

    private static final String PACKAGES_RESOURCE_TYPE = "tajhotels/components/content/holidays-destination/holidays-hotel-package-details";

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    @Inject
    SearchProvider searchProvider;

    @Inject
    Resource resource;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    private List<HolidaysOffers> allHolidaysOffers;

    private List<String> allResourcePath;

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("invoking resource : {}", resource);
        try {
            allResourcePath = fetchHolidayOffers();
            allHolidaysOffers = populateTrendingOffers(allResourcePath);
        } catch (Exception e) {
            LOG.error("Exception while building details for offers. {} ", e.getMessage());
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private List<HolidaysOffers> populateTrendingOffers(List<String> listPackagePaths) throws LoginException {

        LOG.info("Method entry : populateTrendingOffers");
        List<HolidaysOffers> holidayOffers = new LinkedList<HolidaysOffers>();
       
		try {
			ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			for (String resourcePath : listPackagePaths) {
				HolidaysOffers holidayPackage = new HolidaysOffers();
				Resource hotelResourceFromPath = resourceResolver.getResource(resourcePath);
				ValueMap valueMap = hotelResourceFromPath.adaptTo(ValueMap.class);
				holidayPackage.setOfferCode(valueMap.get("offerRateCode", String.class));
				holidayPackage.setOfferDescription(valueMap.get("packageDescription", String.class));
				holidayPackage.setOfferPrice(valueMap.get("discountedRate", String.class));
				holidayPackage.setOfferTitle(valueMap.get("packageTitle", String.class));
				holidayPackage.setNoOfNights(valueMap.get("nights", String.class));
				Resource hotelResource = getHotelResourcePath(hotelResourceFromPath);
				ParticipatingHotel hotelModel;
				hotelModel = hotelResource.adaptTo(ParticipatingHotel.class);
				holidayPackage.setHotelCity(hotelModel.getHotelCity());
				holidayPackage.setHotelGuestRoomLink(hotelResource.getParent().getPath() + "/rooms-and-suites");
				holidayPackage.setHotelLink(hotelResource.getParent().getPath());
				holidayPackage.setHotelName(hotelModel.getHotelName());
				holidayPackage.setLatitude(hotelModel.getLat());
				holidayPackage.setLongitude(hotelModel.getLng());
				holidayPackage.setOfferpageLink(hotelResourceFromPath.getParent().getParent().getParent().getPath());
				String imageUrl = extractBannerImage(hotelResource);
				if (imageUrl != null) {
					holidayPackage.setOfferImage(imageUrl);
				}
				String currencySymbol = getCurrencySymbol(hotelResourceFromPath);
				if (null != currencySymbol) {
					holidayPackage.setCurrencySymbol(currencySymbol);
				} else {
					holidayPackage.setCurrencySymbol("");
				}
				List<UserSellingPoints> uspList = getUSPList(hotelResource);
				holidayPackage.setListOfUserSellingPoints(uspList);

				LOG.info("Adding package to package list");
				holidayOffers.add(holidayPackage);
			}
		} catch (Exception e) {
            LOG.error("Error occured while mapping values to HolidayOffer Object :: {}", e.getMessage());
        } 
        LOG.info("Method exit : populateTrendingOffers");
        return holidayOffers;
    }

    private List<UserSellingPoints> getUSPList(Resource hotelResource) {

        LOG.debug("Method entry: getUSPList");
        String hotelPath = hotelResource.getPath();
        Resource hotelSignatureFeaturesResource = hotelResource.getResourceResolver()
                .getResource(hotelPath + "/root/hotel_overview/hotelSignatureFeatures");
        LOG.debug("Recieved hotel_overview/hotelSignatureFeatures resource as " + hotelSignatureFeaturesResource);
        List<UserSellingPoints> uspLs = new ArrayList<UserSellingPoints>();
        if (null != hotelSignatureFeaturesResource) {
            Iterable<Resource> children = hotelSignatureFeaturesResource.getChildren();
            for (Resource childResrouce : children) {
                UserSellingPoints usp = new UserSellingPoints();
                ValueMap valueMap = childResrouce.adaptTo(ValueMap.class);
                usp.setSignatureFeaturePoint(valueMap.get("signatureFeatureValue", String.class));
                usp.setSignatureFeatureIcon(valueMap.get("type", String.class));
                uspLs.add(usp);
            }
        }
        LOG.debug("Method exit: getUSPList");
        return uspLs;
    }

    private String extractBannerImage(Resource resource) {

        LOG.info("inside method extractBannerImage()");
        LOG.debug("resource address: {}", resource.getPath());
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                ValueMap valMap = hotelBanner.getValueMap();
                String imageURL = valMap.get("bannerImage", String.class);
                LOG.debug("found Image: {}", imageURL);
                return imageURL;
            }
            LOG.trace("Returning null [condition failed], hotelBanner is null");
            return null;
        }
        LOG.trace("Returning null [condition failed], bannerParsys is null");
        LOG.info("inside method extractBannerImage()");
        return null;
    }

    private Resource getHotelResourcePath(Resource resource) throws LoginException {

        Resource hotelResource = ((((((resource.getParent()).getParent()).getParent()).getParent())
                .getChild("jcr:content")).getChild("root"));
        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
        Resource hotel = null;
        Iterable<Resource> hotelRootChildren = hotelResource.getChildren();
        for (Resource hotelChild : hotelRootChildren) {
            if (hotelChild.getResourceType().equals(HOTEL_DETAILS_COMPONENT_RESOURCE_TYPE)) {
                LOG.debug("path for hotel details : {}", hotelChild.getPath());
                Resource hotelDetailsComponent = resourceResolver.getResource(hotelChild.getPath());
                ValueMap hotelDetailsValueMap = hotelDetailsComponent.adaptTo(ValueMap.class);
                String hotelUrl = hotelDetailsValueMap.get("hotelPath", String.class);
                hotel = resourceResolver.getResource(hotelUrl + "/jcr:content");
            }
        }
        LOG.debug("hotelResourcePath: {}", hotelResource.getPath());
        return hotel;
    }

    private List<String> fetchHolidayOffers() throws RepositoryException {

        LOG.info("Inside method: fetchHolidayOffers()");

        List<String> allResourcePaths = new ArrayList<>();
        HashMap<String, String> predicateMap = new HashMap<>();


        ValueMap offersListValueMap = resource.adaptTo(ValueMap.class);
        String[] fixedResourceList = offersListValueMap.get("pages", String[].class);
        if (fixedResourceList != null && fixedResourceList.length > 0) {
            for (String jsonStr : fixedResourceList) {
                predicateMap.put("path", jsonStr);
                predicateMap.put("property", "sling:resourceType");
                predicateMap.put("property.value", PACKAGES_RESOURCE_TYPE);
                SearchResult searchResult = searchProvider.getQueryResult(predicateMap);
                for (Hit hit : searchResult.getHits()) {
                    LOG.debug("Found package component at : {}", hit.getPath());
                    allResourcePaths.add(hit.getPath());
                }
            }
        }
        LOG.info("Leaving method: fetchHolidayOffers()");
        return allResourcePaths;
    }

    public List<String> getAllResourcePath() {
        return allResourcePath;
    }

    @Override
    public List<HolidaysOffers> getAllHolidaysOffers() {

        return allHolidaysOffers;
    }

	private String getCurrencySymbol(Resource hotelResourceFromPath) {
		String currencySymbol = null;
		ResourceResolver resourceResolver = null;
		try {

			resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			Resource children = hotelResourceFromPath.getParent().getParent().getParent().getParent()
					.getChild("jcr:content").getChild("root");
			Iterable<Resource> hoteldetailsChildren = children.getChildren();
			for (Resource child : hoteldetailsChildren) {
				if (child.getResourceType().equals(HOTEL_DETAILS_COMPONENT_RESOURCE_TYPE)) {
					LOG.debug("path for hotel details : {}", child.getPath());
					Resource hotelDetailsComponent = resourceResolver.getResource(child.getPath());
					ValueMap hotelDetailsValueMap = hotelDetailsComponent.adaptTo(ValueMap.class);
					currencySymbol = currencyFetcherService
							.getCurrencySymbolValue(hotelDetailsValueMap.get("currencySymbol", String.class));
				}
			}
		} catch (LoginException e) {
			LOG.debug("Exception occured while accessing resource resolver :: {}", e.getMessage());
		} finally {
			if (resourceResolver != null && resourceResolver.isLive()) {
				resourceResolver.close();
			}
		}
		return currencySymbol;
	}
}
