/**
 *
 */
package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(
        adaptables = Resource.class)
public class RelatedImageList {

    private static final Logger LOG = LoggerFactory.getLogger(RelatedImageList.class);

    private String profileName;

    private String description;

    private String profileImage;

    private String resourceURL;

    @Inject
    private Resource resource;

    private static final String RESOURCE_TYPE = "tajhotels/components/content/individual-profile-details";

    private static final String FILE_REFERENCE = "fileReference";

    private static final String DESIGNATION = "designation";

    private static final String PROFILE_NAME = "personName";

    @PostConstruct
    protected void init() {
        String methodName = "init";
        LOG.trace("Method Entry :" + methodName);
        LOG.trace("Received Resource Path as :" + resource.getPath());
        for (Resource child : resource.getChildren()) {
            if (child.getResourceType().equals(RESOURCE_TYPE)) {
                ValueMap properties = child.getValueMap();
                profileImage = properties.get(FILE_REFERENCE, String.class);
                profileName = properties.get(PROFILE_NAME, String.class);
                LOG.trace("Method Exit :" + methodName);
            }
        }
    }

    public Resource getResource() {
        return resource;
    }

    public String getResourceURL() {
        return resourceURL;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getDescription() {
        return description;
    }

    public String getProfileImage() {
        return profileImage;
    }


}
