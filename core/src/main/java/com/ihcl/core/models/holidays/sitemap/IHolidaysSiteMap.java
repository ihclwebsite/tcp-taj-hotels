package com.ihcl.core.models.holidays.sitemap;

import java.util.List;
import java.util.Map;

public interface IHolidaysSiteMap {

	List<HolidaysPageLink> getPageLinks();

	List<HolidaysPageLink> getDiscoverMoreLinks();

	Map<String, List<HolidaysPageLink>> getCategoriesandChildren();

	List<HolidayHotelOffers> getHolidayHotelOffers();

}
