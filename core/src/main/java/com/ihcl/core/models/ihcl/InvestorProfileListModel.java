package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Model(adaptables = Resource.class,
        adapters = InvestorProfileListModel.class)

public class InvestorProfileListModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Self
    Resource resource;


    public List<IhclprofileBean> profileList = new ArrayList<IhclprofileBean>();


    @PostConstruct
    protected void init() {
        String methodName = "init";
        LOG.trace("Method Entry: " + methodName);
        LOG.trace("Method Entred :init " + resource.getResourceType());
        try {

            ValueMap map = resource.getValueMap();
            String parentPage = map.get("parentPage", String.class);
            // Page pagePath = resource.adaptTo(Page.class);

            if (null != parentPage && !"".equals(parentPage)) {
                Resource resource1 = resource.getResourceResolver().getResource(parentPage);
                Iterable<Resource> children = resource1.getChildren();
                for (Resource res : children) {
                    if (res.getName().equals("jcr:content"))
                        continue;
                    LOG.trace("Name of childern" + res.getPath());
                    ValueMap valueMap = getChildTypeAsValueMapUpto(res,
                            "tajhotels/components/content/individual-profile-details");
                    initializeProperties(valueMap);

                }
            }

        } catch (Exception e) {
            LOG.error("An error occured while fetching a resource jiva spa details: " + e);
        }
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @param rootResource
     */
    private List<IhclprofileBean> initializeProperties(ValueMap valueMap) {
        // TODO Auto-generated method stub
        String methodName = "initializeProperties";
        LOG.trace("Method Entry: " + methodName);
        if (null != valueMap) {
            IhclprofileBean profileBean = new IhclprofileBean();
            LOG.trace("valueMap :::::" + valueMap);
            String designation = valueMap.get("designation", String.class);
            String name = valueMap.get("personName", String.class);
            String id = name.replaceAll(" ", "");
            profileBean.setName(name);
            profileBean.setId(id);
            profileBean.setDesignation(designation);
            String image = valueMap.get("fileReference", String.class);
            String description = valueMap.get("description", String.class);
            profileBean.setDescription(description);
            profileBean.setImage(image);
            LOG.trace("Method Exit: " + methodName);
            profileList.add(profileBean);
        }
        return profileList;

    }


    /**
     *
     */
    public ValueMap getChildTypeAsValueMapUpto(Resource currentResource, String childResourceType) {
        LOG.trace(
                "Entry > [Method : getChildAsValueMapUpto(currentResource, childResourceName) ] :: [ Annotations : @Override");
        if (currentResource != null) {
            if (currentResource.isResourceType(childResourceType)) {
                LOG.debug("Returning > [child > path :" + currentResource.getPath() + " ]");
                return currentResource.adaptTo(ValueMap.class);
            }
            if (currentResource.hasChildren()) {
                for (Resource child : currentResource.getChildren()) {
                    return getChildTypeAsValueMapUpto(child, childResourceType);
                }
            }
            LOG.warn("Returning > [child > as null : Can not find the resource in the Hirarchy ]");
            return null;
        }
        LOG.warn("Returning > [child > as null : condition failed(currentResource is null ]");
        LOG.trace("Exit > [Method : getChildUpto(currentResource, childResourceName) ] :: [ Annotations : @Override ]");
        return null;
    }


    /**
     * Getter for the field profileList
     *
     * @return the profileList
     */
    public List<IhclprofileBean> getProfileList() {
        return profileList;
    }


    /**
     * Setter for the field profileList
     *
     * @param profileList
     *            the profileList to set
     */

    public void setProfileList(List<IhclprofileBean> profileList) {
        this.profileList = profileList;
    }


}
