package com.ihcl.core.models.holidays.hotel;

import java.util.List;

public class HolidayPackage {
	
	private String packageTitle;
	
	private String packageDescription;
	
	private List<String> termsAndConditions;
	
	private String offerCode;
	
	private String rateDescription;
	
	private String cancellationPolicy;
	
	private String discountedRate;
	
	private String actualRate;
	
	private String lastFewRoomsAvailableFlag;
	
	private String lastFewRoomsAvailableLabel;
	
	private String offerStartDate;
	
	private String offerEndDate;
	
	private String holidayOfferPagePath;
	
	private String bookNowLabel;
	
	private String noOfnights;
	
	private String seasonCategory;
	
	private String theme;
	
	private String packageRedirectionType;
	
	private String enquiryPath;
	
	private List<Inclusion> inclusionLs; 
	
	private String ratePerNightLabel;
	
	public String getRatePerNightLabel() {
		return ratePerNightLabel;
	}

	public void setRatePerNightLabel(String ratePerNightLabel) {
		this.ratePerNightLabel = ratePerNightLabel;
	}

	public String getPackageTitle() {
		return packageTitle;
	}

	public void setPackageTitle(String packageTitle) {
		this.packageTitle = packageTitle;
	}

	public String getOfferCode() {
		return offerCode;
	}

	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	public String getRateDescription() {
		return rateDescription;
	}

	public void setRateDescription(String rateDescription) {
		this.rateDescription = rateDescription;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getDiscountedRate() {
		return discountedRate;
	}

	public void setDiscountedRate(String discountedRate) {
		this.discountedRate = discountedRate;
	}

	public String getActualRate() {
		return actualRate;
	}

	public void setActualRate(String actualRate) {
		this.actualRate = actualRate;
	}

	public String getLastFewRoomsAvailableFlag() {
		return lastFewRoomsAvailableFlag;
	}

	public void setLastFewRoomsAvailableFlag(String lastFewRoomsAvailableFlag) {
		this.lastFewRoomsAvailableFlag = lastFewRoomsAvailableFlag;
	}

	public String getLastFewRoomsAvailableLabel() {
		return lastFewRoomsAvailableLabel;
	}

	public void setLastFewRoomsAvailableLabel(String lastFewRoomsAvailableLabel) {
		this.lastFewRoomsAvailableLabel = lastFewRoomsAvailableLabel;
	}

	public String getBookNowLabel() {
		return bookNowLabel;
	}

	public void setBookNowLabel(String bookNowLabel) {
		this.bookNowLabel = bookNowLabel;
	}

	public String getNoOfnights() {
		return noOfnights;
	}

	public void setNoOfnights(String noOfnights) {
		this.noOfnights = noOfnights;
	}

	public String getSeasonCategory() {
		return seasonCategory;
	}

	public void setSeasonCategory(String seasonCategory) {
		this.seasonCategory = seasonCategory;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getPackageRedirectionType() {
		return packageRedirectionType;
	}

	public void setPackageRedirectionType(String packageRedirectionType) {
		this.packageRedirectionType = packageRedirectionType;
	}

	public String getEnquiryPath() {
		return enquiryPath;
	}

	public void setEnquiryPath(String enquiryPath) {
		this.enquiryPath = enquiryPath;
	}

	public String getPackageDescription() {
		return packageDescription;
	}

	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public List<String> getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(List<String> termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	public List<Inclusion> getInclusionLs() {
		return inclusionLs;
	}

	public void setInclusionLs(List<Inclusion> inclusionLs) {
		this.inclusionLs = inclusionLs;
	}

	public String getHolidayOfferPagePath() {
		return holidayOfferPagePath;
	}

	public void setHolidayOfferPagePath(String holidayOfferPagePath) {
		this.holidayOfferPagePath = holidayOfferPagePath;
	}

	public String getOfferStartDate() {
		return offerStartDate;
	}

	public void setOfferStartDate(String offerStartDate) {
		this.offerStartDate = offerStartDate;
	}

	public String getOfferEndDate() {
		return offerEndDate;
	}

	public void setOfferEndDate(String offerEndDate) {
		this.offerEndDate = offerEndDate;
	}

}
