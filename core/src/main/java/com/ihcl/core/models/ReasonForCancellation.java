package com.ihcl.core.models;


import com.ihcl.tajhotels.constants.ReservationConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class)
public class ReasonForCancellation {

    private static final Logger LOG = LoggerFactory.getLogger(ReasonForCancellation.class);
    private static final String EXCEPTION_MESSAGE = "Exception found while trying to get the reasons :: {}";
    @Inject
    Node currentNode;

    public List<String> getReasonsForCancellation() {

        List<String> reasons = new ArrayList<>();
        try {
            Node reasonNode = currentNode.getNode(ReservationConstants.REASONS);
            NodeIterator nodes = reasonNode.getNodes();
            while (nodes.hasNext()) {
                Node presentNode = nodes.nextNode();
                String reasonForCancel = presentNode.getProperty(ReservationConstants.REASON).getValue().getString();
                reasons.add(reasonForCancel);
            }
        } catch (RepositoryException e) {
            LOG.error(EXCEPTION_MESSAGE, e.getMessage());
            LOG.debug(EXCEPTION_MESSAGE, e);
        } catch (Exception e) {
            LOG.error(EXCEPTION_MESSAGE, e.getMessage());
            LOG.debug(EXCEPTION_MESSAGE, e);
        }
        return reasons;
    }


}
