package com.ihcl.core.models.tic;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = Resource.class)
public class MembershipTiers {

	private static final Logger LOG = LoggerFactory.getLogger(MembershipTiers.class);

	@Inject
	private List<TierBenefits> tierBenefits;

	public List<TierBenefits> getTierBenefits() {
		return tierBenefits;
	}
}
