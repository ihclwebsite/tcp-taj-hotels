package com.ihcl.core.models.holidays.explore;

public class Dining {
	
	private String diningName;
	
	private String diningDescription;
	
	private String dinningTime;
	
	private String diningImagePath;
	
	private String diningLatitude;
	
	private String diningLongitude;
	
	private String diningAddress;
	
	private String uniqueName;

	public String getUniqueName() {
		return uniqueName;
	}

	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}

	public String getDiningName() {
		return diningName;
	}

	public void setDiningName(String diningName) {
		this.diningName = diningName;
	}

	public String getDiningDescription() {
		return diningDescription;
	}

	public void setDiningDescription(String diningDescription) {
		this.diningDescription = diningDescription;
	}

	public String getDinningTime() {
		return dinningTime;
	}

	public void setDinningTime(String dinningTime) {
		this.dinningTime = dinningTime;
	}

	public String getDiningImagePath() {
		return diningImagePath;
	}

	public void setDiningImagePath(String diningImagePath) {
		this.diningImagePath = diningImagePath;
	}

	public String getDiningLatitude() {
		return diningLatitude;
	}

	public void setDiningLatitude(String diningLatitude) {
		this.diningLatitude = diningLatitude;
	}

	public String getDiningLongitude() {
		return diningLongitude;
	}

	public void setDiningLongitude(String diningLongitude) {
		this.diningLongitude = diningLongitude;
	}

	public String getDiningAddress() {
		return diningAddress;
	}

	public void setDiningAddress(String diningAddress) {
		this.diningAddress = diningAddress;
	}
	
	
}
