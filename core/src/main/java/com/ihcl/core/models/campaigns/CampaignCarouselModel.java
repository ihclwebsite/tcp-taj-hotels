/**
 *
 */
package com.ihcl.core.models.campaigns;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 * The Class CampaignCarouselModel.
 *
 * @author moonraft
 */
@Model(adaptables = Resource.class,
        adapters = CampaignCarouselModel.class)

public class CampaignCarouselModel {

    private static final Logger LOG = LoggerFactory.getLogger(CampaignCarouselModel.class);

    @Self
    private Resource resource;

    List<CampaignCarouselBean> list = new ArrayList<>();


    @PostConstruct
    public void activate() {
        getfiguresValues();
    }


    /**
     * Gets the figures values.
     *
     * @return the figures values
     */
    public List<CampaignCarouselBean> getfiguresValues() {
        if (null != resource) {
            Node node = resource.adaptTo(Node.class);
            try {
                Property imageProperties = node.getProperty("list");
                Value[] values = null;
                if (imageProperties.isMultiple()) {
                    values = imageProperties.getValues();
                } else {
                    values = new Value[1];
                    values[0] = imageProperties.getValue();
                }
                for (Value listImages : values) {
                    CampaignCarouselBean campaignBean = convertJsonToBean(listImages.getString(),
                            CampaignCarouselBean.class);
                    list.add(campaignBean);
                }
            } catch (RepositoryException e) {
                LOG.error("Exception in getFigureValues :: {}", e.getMessage());
                LOG.debug("Exception in getFigureValues :: {}", e);
            }
        }
        return list;
    }


    /**
     * Convert json to bean.
     *
     * @param <T>
     *            the generic type
     * @param text
     *            the text
     * @param beanClass
     *            the bean class
     * @return the t
     */
    public static <T> T convertJsonToBean(String text, Class<T> beanClass) {
        T beanRef;
        Gson gson = new Gson();
        beanRef = gson.fromJson(text, beanClass);
        return beanRef;
    }

    /**
     * Getter for the field list.
     *
     * @return the list
     */
    public List<CampaignCarouselBean> getList() {
        return list;
    }


    /**
     * Setter for the field list.
     *
     * @param list
     *            the list to set
     */

    public void setList(List<CampaignCarouselBean> list) {
        this.list = list;
    }
}
