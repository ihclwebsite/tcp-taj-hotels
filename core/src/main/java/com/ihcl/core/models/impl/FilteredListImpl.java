package com.ihcl.core.models.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.ihcl.core.models.FilterParams;
import com.ihcl.core.models.FilteredList;
import com.ihcl.core.models.search.SearchResult;
import com.ihcl.core.services.search.SearchProvider;

@Model(adaptables = SlingHttpServletRequest.class, adapters = FilteredList.class)
public class FilteredListImpl implements FilteredList {

	@Self
	SlingHttpServletRequest request;
	@Inject
	List<Page> inputList;
	List<Page> filteredList;
	FilterParams params;

	@Inject
	SearchProvider searchProvider;
	
	
	private static final Logger LOG = LoggerFactory.getLogger(FilteredListImpl.class);

	HashMap<String, String> predicates = new HashMap<String, String>();
	{
		predicates.put("1_orderby", "@jcr:score");
		predicates.put("p.guessTotal", "true");
		predicates.put("p.limit", "-1");
	}

	@PostConstruct
	void initModel() {
		params = request.adaptTo(FilterParams.class);
		executeFilter(params);

	}

	@Override
	public void executeFilter(FilterParams params) {
		try {
		predicates.put("path", params.getFilterPath());
		predicates.put("property", "sling:resourceType");
		predicates.put("property.value", params.getResourceType());
		predicates.put("fulltext", params.getQueryString());
		List<SearchResult> searchResults = searchProvider.executeQuery(predicates);
		} catch (Exception e) {
			LOG.error("Exception found in executeFilter :: {}", e.getMessage());
		} 
		// return filteredList;
	}

}
