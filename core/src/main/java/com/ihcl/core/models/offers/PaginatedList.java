package com.ihcl.core.models.offers;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;

@ConsumerType
public interface PaginatedList extends ComponentExporter {

	Boolean getMore();

	List<Page> getPaginatedList();

	int getPaginationVal();
}
