package com.ihcl.core.models.metaReviewsPojo;

public class Highlight_list {

    private String text;

    private String[] category_id_list;

    private String confidence;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String[] getCategory_id_list() {
        return category_id_list;
    }

    public void setCategory_id_list(String[] category_id_list) {
        this.category_id_list = category_id_list;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", category_id_list = " + category_id_list + ", confidence = " + confidence
                + "]";
    }
}
