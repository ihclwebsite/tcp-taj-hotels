/**
 *
 */
package com.ihcl.core.models;


/**
 * @author admin
 *
 */
public class GiftHamperRequest {

    String title;

    String firstName;

    String lastName;

    String email;

    String phoneNumber;

    String address;

    String city;

    String pincode;

    String state;

    String country;

    String code;

    String currency;

    String shippingAmount;

    String subTotalAmount;

    String totalAmount;

    boolean pickup;

    boolean delivery;


    /**
     * Getter for the field title
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }


    /**
     * Setter for the field title
     *
     * @param title
     *            the title to set
     */

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Getter for the field firstName
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }


    /**
     * Setter for the field firstName
     *
     * @param firstName
     *            the firstName to set
     */

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    /**
     * Getter for the field lastName
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * Setter for the field lastName
     *
     * @param lastName
     *            the lastName to set
     */

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Getter for the field email
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }


    /**
     * Setter for the field email
     *
     * @param email
     *            the email to set
     */

    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Getter for the field phoneNumber
     *
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Setter for the field phoneNumber
     *
     * @param phoneNumber
     *            the phoneNumber to set
     */

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Getter for the field address
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }


    /**
     * Setter for the field address
     *
     * @param address
     *            the address to set
     */

    public void setAddress(String address) {
        this.address = address;
    }


    /**
     * Getter for the field city
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }


    /**
     * Setter for the field city
     *
     * @param city
     *            the city to set
     */

    public void setCity(String city) {
        this.city = city;
    }


    /**
     * Getter for the field pincode
     *
     * @return the pincode
     */
    public String getPincode() {
        return pincode;
    }


    /**
     * Setter for the field pincode
     *
     * @param pincode
     *            the pincode to set
     */

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }


    /**
     * Getter for the field state
     *
     * @return the state
     */
    public String getState() {
        return state;
    }


    /**
     * Setter for the field state
     *
     * @param state
     *            the state to set
     */

    public void setState(String state) {
        this.state = state;
    }


    /**
     * Getter for the field country
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }


    /**
     * Setter for the field country
     *
     * @param country
     *            the country to set
     */

    public void setCountry(String country) {
        this.country = country;
    }


    /**
     * Getter for the field code
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }


    /**
     * Setter for the field code
     *
     * @param code
     *            the code to set
     */

    public void setCode(String code) {
        this.code = code;
    }


    /**
     * Getter for the field currency
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }


    /**
     * Setter for the field currency
     *
     * @param currency
     *            the currency to set
     */

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    /**
     * Getter for the field shippingAmount
     *
     * @return the shippingAmount
     */
    public String getShippingAmount() {
        return shippingAmount;
    }


    /**
     * Setter for the field shippingAmount
     *
     * @param shippingAmount
     *            the shippingAmount to set
     */

    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }


    /**
     * Getter for the field subTotalAmount
     *
     * @return the subTotalAmount
     */
    public String getSubTotalAmount() {
        return subTotalAmount;
    }


    /**
     * Setter for the field subTotalAmount
     *
     * @param subTotalAmount
     *            the subTotalAmount to set
     */

    public void setSubTotalAmount(String subTotalAmount) {
        this.subTotalAmount = subTotalAmount;
    }


    /**
     * Getter for the field totalAmount
     *
     * @return the totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }


    /**
     * Setter for the field totalAmount
     *
     * @param totalAmount
     *            the totalAmount to set
     */

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }


    /**
     * Getter for the field pickup
     *
     * @return the pickup
     */
    public boolean isPickup() {
        return pickup;
    }


    /**
     * Setter for the field pickup
     *
     * @param pickup
     *            the pickup to set
     */

    public void setPickup(boolean pickup) {
        this.pickup = pickup;
    }


    /**
     * Getter for the field delivery
     *
     * @return the delivery
     */
    public boolean isDelivery() {
        return delivery;
    }


    /**
     * Setter for the field delivery
     *
     * @param delivery
     *            the delivery to set
     */

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

}
