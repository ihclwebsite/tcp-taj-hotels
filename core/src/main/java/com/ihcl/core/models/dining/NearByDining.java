/**
 *
 */
package com.ihcl.core.models.dining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;

/**
 * @author moonraft
 *
 */

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = NearByDining.class)
public class NearByDining {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    private Resource resource;

    @Inject
    SlingHttpServletRequest request;

    @Inject
    private Page currentPage;


    @Reference
    private QueryBuilder builder;

    private Session session;

    private ResourceResolver resolver;

    private String destinationDiningPath;

    @PostConstruct
    public void init() {
        getDiningList();
    }

    public List<String> getDiningList() {
        String methodName = "getDiningList";
        LOG.trace(" Method Entry :" + methodName);
        List<String> pagePath = new ArrayList<String>();
        String currentParentPath = null;
        try {
            Node node = currentPage.adaptTo(Node.class);
            LOG.trace("Current Page Path :" + currentPage.getContentResource());
            ValueMap currentPageValueMap = currentPage.getContentResource().adaptTo(ValueMap.class);
            String currentPageTemplate = currentPageValueMap.get("cq:template", String.class);
            if (currentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/dining-list")) {
                Node parentNode = node.getParent().getParent();
                destinationDiningPath = fetchDestintionDiningPath(parentNode.getNodes());
                LOG.trace("destinatonPathNode :" + destinationDiningPath);
                currentParentPath = parentNode.getPath();
            } else if (currentPageTemplate.equals("/conf/tajhotels/settings/wcm/templates/dining-details")) {
                Node parentNode = node.getParent().getParent(); //.getParent();
                currentParentPath = parentNode.getPath();
                destinationDiningPath = fetchDestintionDiningPath(parentNode.getNodes());
                LOG.trace("destinatonPathNode :" + destinationDiningPath);

            }

            String currentPath = currentPage.getPath();
            LOG.trace("currentPagePath :" + currentParentPath);
            Map<String, Object> queryParam = new HashMap<String, Object>();
            queryParam.put("type", "cq:Page");
            queryParam.put("path", currentParentPath);
            queryParam.put("property", "jcr:content/cq:template");
            queryParam.put("property.value", "/conf/tajhotels/settings/wcm/templates/dining-details");
            queryParam.put("p.limit", "-1");
            LOG.trace(" queryParam : " + queryParam);

            resolver = resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);

            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            LOG.trace("Query builder : " + builder);
            PredicateGroup searchPredicates = PredicateGroup.create(queryParam);
            LOG.trace("SearchPredicates : " + searchPredicates);
            Query query = builder.createQuery(searchPredicates, session);

            LOG.trace("Query to be executed is : " + query);
            query.setStart(0L);

            SearchResult result = query.getResult();
            LOG.trace("Query Executed");
            LOG.trace("Query result is : " + result.getHits().size());

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            String path = null;
            while (hitList.hasNext()) {
                hit = hitList.next();
                path = hit.getPath();
                if (!path.contains(currentPath))
                    pagePath.add(path);
            }
            LOG.trace(" Page Path :" + pagePath);
            LOG.trace(" Page Path Size:" + pagePath.size());
            LOG.trace(" Method Exit :" + methodName);
        } catch (

                Exception e) {
            e.printStackTrace();
        }
        return pagePath;
    }

    public String fetchDestintionDiningPath(NodeIterator destinationNode)
            throws RepositoryException {
        String diningPath = null;
        while (destinationNode.hasNext()) {
            Node destinatonPathNode = destinationNode.nextNode();
            LOG.trace("destinatonPathNode :::" + destinatonPathNode.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString());
            if (destinatonPathNode.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString().equals("cq:Page")) {
                LOG.trace("Inside cq:page");
                Node jcrNode = destinatonPathNode.getNode(JcrConstants.JCR_CONTENT);
                LOG.trace("resource type :" + jcrNode.getProperty("sling:resourceType"));
                if (jcrNode.getProperty("sling:resourceType").getString()
                        .equals("tajhotels/components/structure/dining-list-page")) {
                    diningPath = destinatonPathNode.getPath();
                    break;
                }
            }
        }
        return diningPath;
    }


    public String getDestinationDiningPath() {
        return destinationDiningPath;
    }


}
