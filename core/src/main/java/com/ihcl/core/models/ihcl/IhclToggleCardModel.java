/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(adaptables = Resource.class,
        adapters = IhclToggleCardModel.class)

public class IhclToggleCardModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclToggleCardModel.class);

    @Self
    private Resource resource;

    public List<IhclRankingBean> list = new ArrayList<IhclRankingBean>();


    /**
     * Getter for the field list
     *
     * @return the list
     */
    public List<IhclRankingBean> getList() {
        return list;
    }


    /**
     * Setter for the field list
     *
     * @param list
     *            the list to set
     */

    public void setList(List<IhclRankingBean> list) {
        this.list = list;
    }


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getRankingValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @return
     *
     */
    private List<IhclRankingBean> getRankingValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] ranks = valueMap.get("cardlist", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != ranks) {
            for (int i = 0; i < ranks.length; i++) {
                JsonElement jsonElement = gson.fromJson(ranks[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclRankingBean rankingListBean = new IhclRankingBean();
                LOG.trace("jsonObject :" + jsonObject);


                if (null != jsonObject.get("title")) {
                    rankingListBean.setTitle(jsonObject.get("title").getAsString());
                }


                if (null != jsonObject.get("description")) {
                    rankingListBean.setDescription(jsonObject.get("description").getAsString());
                }


                list.add(rankingListBean);

            }

        }
        return list;
    }
    
    


}
