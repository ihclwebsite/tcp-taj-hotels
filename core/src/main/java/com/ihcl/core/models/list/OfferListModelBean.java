package com.ihcl.core.models.list;

public class OfferListModelBean {

    private String offerImagePath;

    private String offerTitle;

    private String offerDescription;

    private String offerValidity;

    private String offerPageURL;

    private String offerRateCode;

    private String noOfNights;

    private String offerStartsFrom;

    private String comparableOfferRateCode;

    public String getOfferImagePath() {
        return offerImagePath;
    }

    public void setOfferImagePath(String offerImagePath) {
        this.offerImagePath = offerImagePath;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferValidity() {
        return offerValidity;
    }

    public void setOfferValidity(String offerValidity) {
        this.offerValidity = offerValidity;
    }


    public String getOfferPageURL() {
        return offerPageURL;
    }

    public void setOfferPageURL(String offerPageURL) {
        this.offerPageURL = offerPageURL;
    }

    public String getOfferRateCode() {
        return offerRateCode;
    }

    public void setOfferRateCode(String offerRateCode) {
        this.offerRateCode = offerRateCode;
    }

    public String getNoOfNights() {
        return noOfNights;
    }

    public void setNoOfNights(String noOfNights) {
        this.noOfNights = noOfNights;
    }

    public String getOfferStartsFrom() {
        return offerStartsFrom;
    }

    public void setOfferStartsFrom(String offerStartsFrom) {
        this.offerStartsFrom = offerStartsFrom;
    }

    public String getComparableOfferRateCode() {
        return comparableOfferRateCode;
    }

    public void setComparableOfferRateCode(String comparableOfferRateCode) {
        this.comparableOfferRateCode = comparableOfferRateCode;
    }
}
