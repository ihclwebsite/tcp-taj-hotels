package com.ihcl.core.models.tic;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class TierBenefits {

	@Inject
	private String title;

	@Inject
	private String image;

	@Inject
	private String benefit1;

	@Inject
	private String benefit2;

	@Inject
	@Optional
	private String addnBenefits;

	@Inject
	@Optional
	private String tierCss;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getBenefit1() {
		return benefit1;
	}

	public String getBenefit2() {
		return benefit2;
	}

	public String getTierCss() {
		return tierCss;
	}

	public String getAddnBenefits() {
		return addnBenefits;
	}
}
