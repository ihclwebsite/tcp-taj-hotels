package com.ihcl.core.models;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

@Model(
        adaptables = Resource.class)
public class EventModel {

    @Self
    Resource resource;

    private String name;

    List<Resource> children;

    private Date eventDate;

    private String convertedDate;

    private String eventLoc;

    private String eventName;

    private String ticketsAvailable;

    private String eventDescription;

    private String eventsImage;

    private String resourceURL;

    SimpleDateFormat formatter;

    @PostConstruct
    protected void init() {
        Resource root = resource.getChild("jcr:content/root");
        if (null != root && !"".equals(root)) {
            resourceURL = resource.getPath().concat(".html");

            Iterable<Resource> rootChildren = root.getChildren();
            for (Resource resource : rootChildren) {
                String resourceType = resource.getResourceType();
                if (resourceType.equals("tajhotels/components/content/event-details")) {
                    ValueMap valueMap = resource.adaptTo(ValueMap.class);
                    eventName = valueMap.get("eventName", String.class);
                    eventDate = valueMap.get("eventDate", Date.class);
                    formatter = new SimpleDateFormat("EEEE, dd'th' MMMM yyyy '-' K a ");
                    convertedDate = formatter.format(eventDate);

                    eventLoc = valueMap.get("eventLoc", String.class);
                    ticketsAvailable = valueMap.get("ticketsAvail", String.class);
                    eventsImage = valueMap.get("eventsImage", String.class);
                    eventDescription = valueMap.get("eventDescription", String.class);
                }
            }
        }
    }

    public Resource getResource() {
        return resource;
    }

    public List<Resource> getChildren() {
        return children;
    }

    public String getName() {
        return name;
    }

    public String getConvertedDate() {
        return convertedDate;
    }

    public String getEventLoc() {
        return eventLoc;
    }

    public String getEventName() {
        return eventName;
    }

    public String getTicketsAvailable() {
        return ticketsAvailable;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public String getEventsImage() {
        return eventsImage;
    }

    public String getResourceURL() {
        return resourceURL;
    }
}
