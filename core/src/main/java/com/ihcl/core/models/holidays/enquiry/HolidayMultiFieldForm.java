package com.ihcl.core.models.holidays.enquiry;

public class HolidayMultiFieldForm {

	private String userFormItemField;
	private String userFormItemLabel;

	public String getUserFormItemField() {
		return userFormItemField;
	}

	public void setUserFormItemField(String userFormItemField) {
		this.userFormItemField = userFormItemField;
	}

	public String getUserFormItemLabel() {
		return userFormItemLabel;
	}

	public void setUserFormItemLabel(String userFormItemLabel) {
		this.userFormItemLabel = userFormItemLabel;
	}
}
