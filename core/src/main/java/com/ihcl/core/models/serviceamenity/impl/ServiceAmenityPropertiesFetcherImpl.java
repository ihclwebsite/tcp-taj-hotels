package com.ihcl.core.models.serviceamenity.impl;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.serviceamenity.ServiceAmenityPropertiesFetcher;
import com.ihcl.core.services.serviceamenity.AmenityGroupIconMapper;
import com.ihcl.tajhotels.constants.CrxConstants;


@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = ServiceAmenityPropertiesFetcher.class)
public class ServiceAmenityPropertiesFetcherImpl implements ServiceAmenityPropertiesFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceAmenityPropertiesFetcherImpl.class);

    @Inject
    @Default(values = "")
    private String path;

    @Inject
    private ResourceResolverFactory resourceResolverFactory;
    
    @Inject 
    private AmenityGroupIconMapper groupIcon;

    private String group;

    private String imagePath;

    private String name;

    private String description;


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received path as: " + path);
        LOG.debug("Received resource resolver factory as: " + resourceResolverFactory);
        buildProperties();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */
    private void buildProperties() {
        String methodName = "buildProperties";
        try {
            LOG.trace("Method Entry: " + methodName);
            ResourceResolver resolver = resourceResolverFactory.getServiceResourceResolver(null);
            LOG.debug("Received resource resolver as: " + resolver);
            Resource serviceAmenityResource = resolver.getResource(path);
            LOG.debug("Received room page resource as: " + serviceAmenityResource);
            Resource serviceAmenityComponent;
            if(serviceAmenityResource!=null) {
            	 if (isServiceAmenityType(serviceAmenityResource)) {
                     serviceAmenityComponent = serviceAmenityResource;
                 } else {
                     serviceAmenityComponent = getServiceAmenityComponentUnder(serviceAmenityResource);
                 }
            	 fetchPropertiesFrom(serviceAmenityComponent);
            }         
           
        } catch (LoginException e) {
            LOG.error("An error occured while building properties for room at the sepcified path", e);
        }
        LOG.trace("Method Exit: " + methodName);

    }


    /**
     * @param roomDetailsComponent
     */
    private void fetchPropertiesFrom(Resource roomDetailsComponent) {
        String methodName = "fetchPropertiesFrom";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = roomDetailsComponent.adaptTo(ValueMap.class);
        name = getProperty(valueMap, "name", String.class);
        description = getProperty(valueMap, "description", String.class);
        group = getProperty(valueMap, "group", String.class);
        imagePath = getProperty(valueMap, "image", String.class);
        if(imagePath == null || imagePath.trim().isEmpty()){
            imagePath = getAmenityGroupImage(group);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private String getAmenityGroupImage( String group) {
    	return groupIcon.getIconPath(group);
    }


    /**
     * @param valueMap
     * @param key
     * @param type
     */
    private <T> T getProperty(ValueMap valueMap, String key, Class<T> type) {
        T value = null;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }


    /**
     * @param root
     * @return
     */
    private Resource getServiceAmenityComponentUnder(Resource root) {
        String methodName = "getServiceAmenityComponentUnder";
        LOG.trace("Method Entry: " + methodName);
        Resource roomComponentResource = null;
        LOG.debug("Attempting to iterate over children of: " + root);
        Iterable<Resource> children = root.getChildren();
        for (Resource child : children) {
            LOG.debug("Iterating over child: " + child);
            if (isServiceAmenityType(child)) {
                LOG.debug("Found service amenity component at child: " + child);
                roomComponentResource = child;
                break;
            }
            roomComponentResource = getServiceAmenityComponentUnder(child);
        }
        LOG.trace("Method Exit: " + methodName);
        return roomComponentResource;
    }


    /**
     * @param child
     * @return
     */
    private boolean isServiceAmenityType(Resource child) {
        return child.getResourceType().equals(CrxConstants.SERVICE_AMENITY_COMPONENT_RESOURCETYPE);
    }


    @Override
    public String getPath() {
        return path;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.serviceamenity.ServiceAmenityPropertiesFetcher#getTitle()
     */
    @Override
    public String getName() {
        return name;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.serviceamenity.ServiceAmenityPropertiesFetcher#getImagePath()
     */
    @Override
    public String getImagePath() {
        return imagePath;
    }


    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.serviceamenity.ServiceAmenityPropertiesFetcher#getGroup()
     */
    @Override
    public String getGroup() {
        return group;
    }

    /**
     * Getter for the field description
     *
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }

}
