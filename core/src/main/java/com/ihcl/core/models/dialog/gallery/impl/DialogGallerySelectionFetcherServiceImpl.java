package com.ihcl.core.models.dialog.gallery.impl;

import com.adobe.granite.ui.components.Value;
import com.ihcl.core.models.dialog.gallery.DialogGallerySelectionFetcherService;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.*;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = {Resource.class, SlingHttpServletRequest.class}, adapters = DialogGallerySelectionFetcherService.class)
public class DialogGallerySelectionFetcherServiceImpl implements DialogGallerySelectionFetcherService {

    private static final Logger LOG = LoggerFactory.getLogger(DialogGallerySelectionFetcherServiceImpl.class);

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    @Default(values = "gallery")
    private String nodeName;

    @Inject
    private ResourceResolverFactory resolverFactory;

    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;

    private String sourceSelection;

    private List<String> selectedStaticImagePaths;

    private String parentPath;

    private Integer depth;


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        try {
            LOG.debug("Received request as: " + request);
            String contentPath = request.getAttribute(Value.CONTENTPATH_ATTRIBUTE).toString();
            LOG.debug("Received content path from request as: " + contentPath);
            populateGallerySelectionDetails(contentPath);
        } catch (Exception e) {
            LOG.error("An error occured while trying to get gallery details for the request: " + request, e);
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param contentPath
     * @throws LoginException
     */
    private void populateGallerySelectionDetails(String contentPath) throws LoginException {
        String methodName = "populateGallerySelectionDetails";
        LOG.trace("Method Entry: " + methodName);
        try {
            String galleryPath = contentPath + "/" + nodeName;
            ResourceResolver resolver;
            resolver = resolverFactory.getServiceResourceResolver(null);
            Resource galleryResource = resolver.getResource(galleryPath);
            LOG.debug("Fetched gallery resource at: " + galleryPath + " as: " + galleryResource);
            if (galleryResource != null) {
                ValueMap valueMap = galleryResource.adaptTo(ValueMap.class);
                sourceSelection = valueMap.get("galleryImageSource", String.class);
                LOG.debug("Fetched source selection as: " + sourceSelection);

                if (sourceSelection.equals("static")) {
                    selectedStaticImagePaths = new ArrayList<>();
                    List<JSONObject> galleryImagesJsonArray = galleryImageFetcherService.getGalleryImagePathsAt(galleryResource);
                    for (int i = 0; i < galleryImagesJsonArray.size(); i++) {
                        JSONObject imageJson = galleryImagesJsonArray.get(i);
                        selectedStaticImagePaths.add(imageJson.getString("imagePath"));
                    }
                } else {
                    parentPath = galleryImageFetcherService.getParentPath(galleryResource);
                    depth = galleryImageFetcherService.getDepth(galleryResource);
                }

            }
        } catch (JSONException e) {
            LOG.error("An error occured while fetching a JSON value.", e);
        }

        LOG.trace("Method Exit: " + methodName);
    }

    @Override
    public String getSourceSelection() {
        return sourceSelection;
    }

    @Override
    public List<String> getSelectedStaticImagePaths() {
        return selectedStaticImagePaths;
    }

    @Override
    public String getParentPath() {
        return parentPath;
    }

    @Override
    public Integer getDepth() {
        return depth;
    }


}
