package com.ihcl.core.models.offers;

public interface Offer {

    void init();

    String getOfferRateCode();

    /**
     * @return
     */
    String getOfferStartsFrom();

    String getOfferEndsOn();

    String getDraggedImagePath();

    /**
     * @return
     */
    String getComparableOfferRateCode();

}
