package com.ihcl.core.models.destination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(
        adaptables = Resource.class)
public class HolidayDestinationModel {

    private static final Logger LOG = LoggerFactory.getLogger(HolidayDestinationModel.class);

    @Inject
    @Named("destinationName")
    @Optional
    private String destinationTitle;

    @Inject
    @Named("city")
    @Optional
    private List<String> city;

    @Inject
    @Named("holidayOfferTag")
    @Optional
    private List<String> holidayOfferTag;

    @Inject
    @Optional
    private String destinationShortDesc;

    @Inject
    @Optional
    private String destinationLongDesc;

    @Inject
    @Named("imagePath")
    @Optional
    private String hotelImage;

    @Optional
    private List<String> hotels;

    @Optional
    private int hotelsCount;

    private String destinationPath;

    private String countryName;

    private Map<String, Set<String>> destinationTags;

    private Set<String> experiencesTags;

    private Set<String> themesTags;

    private Set<String> holidaySeasons;

    private Set<String> stayDuration;

    private Set<String> packagePrice;

    @Inject
    Resource resource;

    @Inject
    ResourceResolverFactory resourceResolverFactory;

    ResourceResolver resourceResolver;

    @Inject
    ResourceFetcherService resourceFetcherService;

    @PostConstruct
	protected void init() {
		try {
			String methodName = "init()";
			LOG.trace("Method entry : {}" , methodName);
			try {
				resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
			} catch (LoginException e) {
				LOG.error("Exception in init method of HolidayDestinationModel :: {}", e.getMessage());
				LOG.debug("Exception in init method of HolidayDestinationModel :: {}", e);
			}
			LOG.trace("");
			hotelImage = getBannerImage();
			Resource parentResource = resource.getParent();
			destinationPath = getPackagePath(parentResource);

			countryName = getCountry();
			getholidayTags();
		} catch (Exception e) {
			LOG.error("Exception in init method of HolidayDestinationModel :: {}", e.getMessage());
			LOG.debug("Exception in init method of HolidayDestinationModel :: {}", e);
		}
	}

    /**
     * @return
     */
    public String getPackagePath(Resource parentResource) {
        String methodName = "getPackagePath()";
        LOG.trace("Method Entry:" + methodName);
        LOG.debug("Package Resource :" + parentResource);

        Iterable<Resource> holidayPackages = parentResource.getChildren();
        for (Resource packages : holidayPackages) {
            LOG.debug("packages.getResourceType() :" + packages.getResourceType());
            if (packages.getResourceType().equals(CrxConstants.TAJHOTELS_PAGE_RESOURCE_TYPE)) {
                LOG.debug("Destination Package Path :" + packages.getPath());
                return packages.getPath();
            }
        }
        return null;
    }

    private String getBannerImage() {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource hotelBanner = bannerParsys.getChild(CrxConstants.HOTEL_BANNER_COMPONENT_NAME);
            if (hotelBanner != null) {
                return String.valueOf(hotelBanner.getValueMap().get("bannerImage"));
            }
        }
        return null;
    }

    public List<String> getHotels() {
        return hotels;
    }

    public String getDestinationTitle() {
        return destinationTitle;
    }

    public String getDestinationShortDesc() {
        return destinationShortDesc;
    }

    public String getDestinationLongDesc() {
        return destinationLongDesc;
    }

    public int getHotelsCount() {
        return hotelsCount;
    }

    public List<String> getCity() {
        return city;
    }

    public String getCountry() {
        LOG.trace("Method Entry: getCountry()");
        Tag tagRoot = null;
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (city != null) {
                LOG.trace("city array :" + city.get(0));
                LOG.trace("TagManager resolve :" + tagManager.resolve(city.get(0)));
                tagRoot = tagManager.resolve(city.get(0));
                if (tagRoot != null) {
                    LOG.trace("Tag Root Path : " + tagRoot.getPath());
                    countryName = tagRoot.getParent().getTitle();
                }
            }
        }
        LOG.trace("Method Exit -> returning  : Country name : " + countryName);
        return countryName;
    }


    private void getholidayTags() {
        String methodName = "getholidayTags()";
        LOG.trace("Method Entry:" + methodName);
        destinationTags = new HashMap<String, Set<String>>();
        experiencesTags = new HashSet<String>();
        themesTags = new HashSet<String>();
        holidaySeasons = new HashSet<String>();
        stayDuration = new HashSet<String>();
        packagePrice = new HashSet<String>();
        LOG.debug("Received Resource path as -> " + resource.getParent().getPath());

        getChildPath(resource.getParent(), CrxConstants.TAJHOTELS_PAGE_RESOURCE_TYPE);
        LOG.debug("destinationTags List Size :" + destinationTags.size());
        LOG.debug("destinationTags List:" + destinationTags);
        LOG.debug("holiday Season List:" + holidaySeasons);
        LOG.debug("Stay Duration List:" + stayDuration);
        LOG.debug("Package Price List:" + packagePrice);
        LOG.trace("Method Exit -> returning  : " + methodName);
    }


    private Resource getChildPath(Resource packageResource, String resourceType) {
        String methodName = "getChildPath()";
        LOG.trace("Method Entry:" + methodName);
        Iterable<Resource> holidayPackages = packageResource.getChildren();
        Resource resourcePackage = null;
        for (Resource packages : holidayPackages) {
            LOG.debug("Received Resource type as :" + packages.getResourceType());
            if (packages.getResourceType().equals(resourceType)) {
                LOG.debug("Received Package Path as :" + packages.getPath());
                fetchOfferTags(packages);
                getChildPath(packages, CrxConstants.TAJHOTELS_PAGE_RESOURCE_TYPE);

            }
        }
        return resourcePackage;
    }

    private Map<String, Set<String>> fetchOfferTags(Resource hotelResource) {
        String methodName = "fetchOfferTags()";
        LOG.trace("Method Entry:" + methodName);
        Resource jcrResoruce = hotelResource.getChild(CrxConstants.TAJHOTELS_PAGE_JCR_CONTENT);
		if (null != jcrResoruce) {
			ValueMap valueMap = jcrResoruce.adaptTo(ValueMap.class);
			LOG.debug("Received ValueMap as :" + valueMap);
			String season = valueMap.get("season", String.class);
			if (null != season && !("").equals(season)) {
				holidaySeasons.add(valueMap.get("season", String.class));
			}
			String[] tags = valueMap.get(CrxConstants.TAJHOTELS_CQ_TAGS, String[].class);
			LOG.debug("Received Tags :" + tags);

			Tag tagRoot = null;
			if (resourceResolver != null) {
				TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
				LOG.trace("Received tags array :" + tags);
				if (tags != null) {
					for (int i = 0; i < tags.length; i++) {
						if (tags[i].startsWith(CrxConstants.TAJHOTELS_TAJ_HOLIDAY)) {
							tagRoot = tagManager.resolve(tags[i]);
							if (tagRoot != null) {
								LOG.trace("Tag Root Path : " + tagRoot.getTitle());
								if (tagRoot.getPath().startsWith(CrxConstants.TAJHOTELS_EXPERIENCES_TAG_PATH)) {
									experiencesTags.add(tagRoot.getTitle());
								}
								if (tagRoot.getPath().startsWith(CrxConstants.TAJHOTELS_THEMES_TAG_PATH)) {
									themesTags.add(tagRoot.getTitle());
								}
							}
						}
					}
				}
			}
			destinationTags.put("experience", experiencesTags);
			destinationTags.put("themes", themesTags);

			fetchPriceAndStayDuration(jcrResoruce);
		}
        return destinationTags;

    }

    private void fetchPriceAndStayDuration(Resource jcrResoruce) {
        Resource packageResoure = resourceFetcherService.getChildrenOfType(jcrResoruce,
                "tajhotels/components/content/holidays-destination/holidays-hotel-package-details");
        if (null != packageResoure) {
            String duration = packageResoure.getValueMap().get("nights", String.class);
            if (null != duration && !"".equals(duration)) {
                stayDuration.add(duration);
            }
            String price = packageResoure.getValueMap().get("discountedRate", String.class).replaceAll(",", "");
            if (null != price && !"".equals(price)) {
                packagePrice.add(price);
            }
        }
    }

    public String getHotelImage() {
        return hotelImage;
    }

    public String getDestinationPath() {
        return destinationPath;
    }

    public List<String> getHolidayOfferTag() {
        return holidayOfferTag;
    }

    public String getCountryName() {
        return countryName;
    }


    public Map<String, Set<String>> getDestinationTags() {
        return destinationTags;
    }

    public Set<String> getExperiencesTags() {
        return experiencesTags;
    }


    public Set<String> getThemesTags() {
        return themesTags;
    }


    public Set<String> getHolidaySeasons() {
        return holidaySeasons;
    }


    public Set<String> getStayDuration() {
        return stayDuration;
    }


    public Set<String> getPackagePrice() {
        return packagePrice;
    }

    public void getDestinationHotels(String destinationPath) {
        hotels = new ArrayList<>();
        // hotels = hotelsSearchService.getHotelPathsByDestinationPath(destinationPath);
        hotelsCount = hotels.size();
    }


}
