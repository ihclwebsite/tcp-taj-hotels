package com.ihcl.core.models.reviewsPojo;

public class Response_list {

    private String text;

    private String created;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", created = " + created + "]";
    }
}
