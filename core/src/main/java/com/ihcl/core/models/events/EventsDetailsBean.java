package com.ihcl.core.models.events;


public class EventsDetailsBean {

    private String title;

    private String description;

    private String productName;

    private String startDate;

    private String viewDetailButton;

    private String imagePath;

    private String city;

    private String eventUrl;

    private String ticketsAvailable;

    private String endDate;

    private String eventPrice;

    private String location;

    private String propertyCode;

    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public String getProductName() {
        return productName;
    }


    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getStartDate() {
        return startDate;
    }


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }


    public String getViewDetailButton() {
        return viewDetailButton;
    }


    public void setViewDetailButton(String viewDetailButton) {
        this.viewDetailButton = viewDetailButton;
    }


    public String getImagePath() {
        return imagePath;
    }


    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public String getEventUrl() {
        return eventUrl;
    }


    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }


    public String getTicketsAvailable() {
        return ticketsAvailable;
    }


    public void setTicketsAvailable(String ticketsAvailable) {
        this.ticketsAvailable = ticketsAvailable;
    }


    public String getEndDate() {
        return endDate;
    }


    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


    public String getRupees() {
        return eventPrice;
    }


    public void setRupees(String rupees) {
        this.eventPrice = rupees;
    }


    public String getLocation() {
        return location;
    }


    public void setLocation(String location) {
        this.location = location;
    }

    public String getPropertyCode() {
        return propertyCode;
    }


    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }


    public EventsDetailsBean(String title, String description, String productName, String startDate,
            String viewDetailButton, String imagePath, String city, String eventUrl, String ticketsAvailable,
            String endDate, String eventPrice, String location, String propertyCode) {
        super();
        this.title = title;
        this.description = description;
        this.productName = productName;
        this.startDate = startDate;
        this.viewDetailButton = viewDetailButton;
        this.imagePath = imagePath;
        this.city = city;
        this.eventUrl = eventUrl;
        this.ticketsAvailable = ticketsAvailable;
        this.endDate = endDate;
        this.eventPrice = eventPrice;
        this.location = location;
        this.propertyCode = propertyCode;
    }


}
