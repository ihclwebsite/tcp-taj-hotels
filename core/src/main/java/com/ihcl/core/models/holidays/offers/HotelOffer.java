package com.ihcl.core.models.holidays.offers;

public class HotelOffer {

    private String hotelName;

    private String hotelPath;

    private String hotelImagePath;

    private String offerRateCodeOne;

    private String nightsOne;

    private String offerRateCodeTwo;

    private String nightsTwo;

    private String startingRateOne;

    private String startingRateTwo;

    private String stay;

    private String stayDescription;

    private String restaurant;

    private String restaurantDescription;

    private String sightSeeing;

    private String sightSeeingDescription;

    private String spa;

    private String spaDescription;

    private String convenience;

    private String convenienceDescription;

    private String blackOutDateShortString;

    private String userSellingText;

    private String viewDetails;

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelPath() {
        return hotelPath;
    }

    public void setHotelPath(String hotelPath) {
        this.hotelPath = hotelPath;
    }

    public String getOfferRateCodeOne() {
        return offerRateCodeOne;
    }

    public void setOfferRateCodeOne(String offerRateCodeOne) {
        this.offerRateCodeOne = offerRateCodeOne;
    }

    public String getNightsOne() {
        return nightsOne;
    }

    public void setNightsOne(String nightsOne) {
        this.nightsOne = nightsOne;
    }

    public String getOfferRateCodeTwo() {
        return offerRateCodeTwo;
    }

    public void setOfferRateCodeTwo(String offerRateCodeTwo) {
        this.offerRateCodeTwo = offerRateCodeTwo;
    }

    public String getNightsTwo() {
        return nightsTwo;
    }

    public void setNightsTwo(String nightsTwo) {
        this.nightsTwo = nightsTwo;
    }

    public String getStartingRateOne() {
        return startingRateOne;
    }

    public void setStartingRateOne(String startingRateOne) {
        this.startingRateOne = startingRateOne;
    }

    public String getStartingRateTwo() {
        return startingRateTwo;
    }

    public void setStartingRateTwo(String startingRateTwo) {
        this.startingRateTwo = startingRateTwo;
    }

    public String getStay() {
        return stay;
    }

    public void setStay(String stay) {
        this.stay = stay;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getSightSeeing() {
        return sightSeeing;
    }

    public void setSightSeeing(String sightSeeing) {
        this.sightSeeing = sightSeeing;
    }

    public String getSpa() {
        return spa;
    }

    public void setSpa(String spa) {
        this.spa = spa;
    }

    public String getConvenience() {
        return convenience;
    }

    public void setConvenience(String convenience) {
        this.convenience = convenience;
    }

    public String getHotelImagePath() {
        return hotelImagePath;
    }

    public void setHotelImagePath(String hotelImagePath) {
        this.hotelImagePath = hotelImagePath;
    }

    public String getStayDescription() {
        return stayDescription;
    }

    public void setStayDescription(String stayDescription) {
        this.stayDescription = stayDescription;
    }

    public String getRestaurantDescription() {
        return restaurantDescription;
    }

    public void setRestaurantDescription(String restaurantDescription) {
        this.restaurantDescription = restaurantDescription;
    }

    public String getSightSeeingDescription() {
        return sightSeeingDescription;
    }

    public void setSightSeeingDescription(String sightSeeingDescription) {
        this.sightSeeingDescription = sightSeeingDescription;
    }

    public String getSpaDescription() {
        return spaDescription;
    }

    public void setSpaDescription(String spaDescription) {
        this.spaDescription = spaDescription;
    }

    public String getConvenienceDescription() {
        return convenienceDescription;
    }

    public void setConvenienceDescription(String convenienceDescription) {
        this.convenienceDescription = convenienceDescription;
    }

    public String getBlackOutDateShortString() {
        return blackOutDateShortString;
    }

    public void setBlackOutDateShortString(String blackOutDateShortString) {
        this.blackOutDateShortString = blackOutDateShortString;
    }

    public String getUserSellingText() {
        return userSellingText;
    }

    public void setUserSellingText(String userSellingText) {
        this.userSellingText = userSellingText;
    }

    public String getViewDetails() {
        return viewDetails;
    }

    public void setViewDetails(String viewDetails) {
        this.viewDetails = viewDetails;
    }
}
