package com.ihcl.core.models;

import java.text.DecimalFormat;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.util.URLMapUtil;

@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ParticipatingHotel {

    private static final Logger LOG = LoggerFactory.getLogger(ParticipatingHotel.class);

    private static final String HOTEL_BRAND_TAG_INTIAL = "taj:hotels/brands/";

    @Inject
    @Required
    @Named("hotelName")
    private String hotelName;

    @Inject
    @Named("hotelArea")
    private String hotelArea;

    @Inject
    @Required
    @Named("hotelId")
    private String hotelId;

    @Inject
    @Named("locationStats")
    private String locationStats;

    @Inject
    @Named("hotelCity")
    private String hotelCity;

    @Inject
    @Named("addressRegion")
    private String state;

    @Inject
    @Named("hotelAddress")
    private String hotelAddress;

    @Inject
    @Named("countryName")
    private String hotelCountry;

    @Inject
    @Named("hotelPinCode")
    private String hotelPinCode;

    @Inject
    private String reviewScore;

    @Inject
    private String totalReview;

    @Inject
    @Named("imagePath")
    private String imageUrl;

    @Inject
    @Named("lowestPrice")
    private String lowestPrice;

    @Inject
    @Named("discountedPrice")
    private String discountedPrice;

    @Inject
    @Named("hotelThemeInterest")
    private List<String> hotelTypes;

    @Inject
    private String locations;

    @Inject
    @Named("brand")
    private String hotelBrand;

    @Inject
    @Named("longitude")
    private String lng;

    @Inject
    @Named("latitude")
    private String lat;

    @Inject
    @Named("hotelType")
    private String hotelType;

    @Inject
    @Default(values = "")
    @Named("amountAfterTax")
    private String amountAfterTax;

    @Inject
    @Default(values = "")
    @Named("amountBeforeTax")
    private String amountBeforeTax;

    @Inject
    private String streetAddress;

    @Inject
    @Optional
    private String hotelEmail;

    @Inject
    @Optional
    private String jivaSpaEmail;

    @Inject
    @Optional
    private String spaName;

    @Inject
    @Optional
    private String requestQuoteEmail;

    @Inject
    @Optional
    private String hotelPhoneNumber;

    @Inject
    @Optional
    private String hotelStdCode;

    private String hotelPath;

    private String jivaSpaPath;

    @Self
    Resource resource;

    ResourceResolver resourceResolver;

    private List<String> serviceAmenities;

    @PostConstruct
    protected void init() {
        resourceResolver = resource.getResourceResolver();
        LOG.trace("Method Entry >> init()");
        hotelPath = getMappedPath(resource.getParent().getPath());
        LOG.trace("Participating Hotel path >> " + hotelPath);
        LOG.trace("Method Exit >> init()");
    }


    public String getState() {
        return state;
    }

    public List<String> getHotelTypes() {
        return hotelTypes;
    }

    public String getLowestPrice() {
        return lowestPrice;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCountry() {
        return hotelCountry;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public String getHotelPinCode() {
        return hotelPinCode;
    }

    public String getReviewScore() {
        return reviewScore;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getHotelArea() {
        return hotelArea;
    }

    public String getHotelId() {
        return hotelId;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public String getTotalReview() {
        return totalReview;
    }

    public String getLocationStats() {
        return locationStats;
    }

    public String getLocations() {
        return locations;
    }

    public String getHotelBrand() {
        if (hotelBrand != null && hotelBrand.contains(HOTEL_BRAND_TAG_INTIAL)) {
            hotelBrand = hotelBrand.replace(HOTEL_BRAND_TAG_INTIAL, "");
        }
        return hotelBrand;
    }

    public String getLng() {
        return lng;
    }

    public String getLat() {
        return lat;
    }

    public String getHotelType() {
        return hotelType;
    }

    public String getAmountAfterTax() {
        if (StringUtils.isNotEmpty(amountAfterTax) && amountAfterTax != null) {
            return priceFormatter(amountAfterTax);
        } else {
            return amountAfterTax;
        }
    }

    public String getAmountBeforeTax() {
        if (StringUtils.isNotEmpty(amountBeforeTax) && amountBeforeTax != null) {
            return priceFormatter(amountBeforeTax);
        } else {
            return amountBeforeTax;
        }
    }

    private String priceFormatter(String number) {
        double amount = Double.parseDouble(number);
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(amount);
    }

    public String getHotelPath() {
        return hotelPath;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getJivaSpaEmail() {
        return jivaSpaEmail;
    }

    public String getRequestQuoteEmail() {
        return requestQuoteEmail;
    }

    public String getHotelEmail() {
        return hotelEmail;
    }

    public String getSpaName() {
        return spaName;
    }

    private String getMappedPath(String url) {
        if (url != null) {
            String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
            return resolvedURL;
        }
        return url;
    }

    public void setServiceAmenities(List<String> serviceAmenities) {
        this.serviceAmenities = serviceAmenities;
    }

    public List<String> getServiceAmenities() {
        return serviceAmenities;
    }


    public String getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    public String getHotelStdCode() {
        return hotelStdCode;
    }

    public String getJivaSpaPath() {
        return jivaSpaPath;
    }

    public void setJivaSpaPath(String jivaSpaPath) {
        this.jivaSpaPath = jivaSpaPath;
    }

}
