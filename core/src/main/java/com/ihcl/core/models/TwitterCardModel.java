package com.ihcl.core.models;

import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class TwitterCardModel {

	private static final String bannerImage = "bannerImage";
	private static final String reference_path = "path";
	private static final String rendition_val = "cq5dam.web.1280.1280.jpeg";
	private static final String reference = "foundation/components/reference";
	@Self
	SlingHttpServletRequest request;

	Resource resource;

	ResourceResolver resourceResolver;

	private String imageURL;

	private String resourcePath;

	private String requestURL;

	Logger logger;

	@PostConstruct
	protected void init() {
		logger = LoggerFactory.getLogger(TwitterCardModel.class);
		resource = request.getResource();
		resourceResolver = resource.getResourceResolver();
		requestURL = request.getRequestURL().toString();
		if (resource != null) {
			logger.debug("Extracting banner image for twitter card from resource" + resource.getPath());
			Resource bannerComponent = findBannerComponent(resource);
			if (bannerComponent != null) {
				imageURL = extractBannerImage(bannerComponent);
			}

		}

	}

	private Resource findBannerComponent(Resource resource) {
		if (resource.getChildren() != null) {
			Iterator<Resource> children = resource.getChildren().iterator();
			while (children.hasNext()) {
				Resource child = children.next();
				if (child.getName().equals(CrxConstants.BANNER_PARSYS_COMPONENT_NAME)) {
					return child;
				}
			}
		}
		return null;
	}

	private String extractBannerImage(Resource resource) {
		String imagePath = "";
		if (resource.getChildren() != null) {
			Iterator<Resource> bannerChildren = resource.getChildren().iterator();
			while (bannerChildren.hasNext()) {
				Resource bannerChild = bannerChildren.next();
				if (bannerChild.getResourceType().equals(reference)) {
					imagePath = getImagePathFromReference(bannerChild);
				} else {
					ValueMap bannerValueMap = bannerChild.getValueMap();
					imagePath = String.valueOf(bannerValueMap.get(bannerImage));
				}
			}
			Asset asset = getAssetForImagePath(imagePath);
			return generateImageURL(asset);
		}
		return null;
	}

	private Asset getAssetForImagePath(String imagePath) {
		Resource assetResource = resourceResolver.getResource(imagePath);
		if (assetResource != null) {
			Asset asset = assetResource.adaptTo(Asset.class);
			return asset;
		}
		return null;
	}

	private String getImagePathFromReference(Resource reference) {
		ValueMap referenceValMap = reference.getValueMap();
		String originalBannerPath = String.valueOf(referenceValMap.get(reference_path));
		Resource originalBanner = resourceResolver.getResource(originalBannerPath);
		if (originalBanner != null) {
			ValueMap bannerValueMap = originalBanner.getValueMap();
			String imagePath = String.valueOf(bannerValueMap.get(bannerImage));
			return imagePath;
		}
		return null;
	}

	private String generateImageURL(Asset asset) {
		if (asset != null) {
			Rendition renditionForCard = asset.getRendition(rendition_val);
			if (renditionForCard != null) {
				return getHostString() + renditionForCard.getPath();
			} else {
				return getHostString() + asset.getPath();
			}

		}
		return null;
	}

	private String getHostString() {
		String hostString = request.getScheme() + "://" + request.getServerName();
		if (request.getServerPort() != 0) {
			hostString += ":" + request.getServerPort();
		}
		return hostString;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public String getImageURL() {
		return imageURL;
	}

	public String getRequestURL() {

		if (requestURL.endsWith(".html")) {
			return requestURL.substring(0,requestURL.indexOf(".html")) + "/";
		}
		return requestURL;

	}

}
