package com.ihcl.core.models.destination;

import java.util.Map;

import com.ihcl.core.hotels.DestinationDetailsBean;
import com.ihcl.core.hotels.HotelBean;

public class OurHotelDestinationBean {
	
	private DestinationDetailsBean destinationDetails;
	private Map<String, HotelBean> hotelsMap;
	private int websiteHotelsCount;
	private int otherHotelsCount;
	private int totalHotelsCount;
	
	public DestinationDetailsBean getDestinationDetails() {
		return destinationDetails;
	}
	public void setDestinationDetails(DestinationDetailsBean destinationDetails) {
		this.destinationDetails = destinationDetails;
	}
	public Map<String, HotelBean> getHotelsMap() {
		return hotelsMap;
	}
	public void setHotelsMap(Map<String, HotelBean> hotelsMap) {
		this.hotelsMap = hotelsMap;
	}
	public int getWebsiteHotelsCount() {
		return websiteHotelsCount;
	}
	public void setWebsiteHotelsCount(int websiteHotelsCount) {
		this.websiteHotelsCount = websiteHotelsCount;
	}
	public int getOtherHotelsCount() {
		return otherHotelsCount;
	}
	public void setOtherHotelsCount(int otherHotelsCount) {
		this.otherHotelsCount = otherHotelsCount;
	}
	public int getTotalHotelsCount() {
		return totalHotelsCount;
	}
	public void setTotalHotelsCount(int totalHotelsCount) {
		this.totalHotelsCount = totalHotelsCount;
	}
	public OurHotelDestinationBean(DestinationDetailsBean destinationDetails, Map<String, HotelBean> hotelsMap,
			int websiteHotelsCount, int otherHotelsCount, int totalHotelsCount) {
		super();
		this.destinationDetails = destinationDetails;
		this.hotelsMap = hotelsMap;
		this.websiteHotelsCount = websiteHotelsCount;
		this.otherHotelsCount = otherHotelsCount;
		this.totalHotelsCount = totalHotelsCount;
	}
	
	
	
}
