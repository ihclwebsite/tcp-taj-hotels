package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.ihcl.tajhotels.constants.CrxConstants;

@Model(
        adaptables = Resource.class,
        adapters = PartnerPromotionOfferModel.class)
public class PartnerPromotionOfferModel {

    private static final Logger LOG = LoggerFactory.getLogger(PartnerPromotionOfferModel.class);

    @Self
    private Resource resource;

    @Inject
    private ResourceResolver resourceResolver;

    private static final String OFFER_DESCRIPTION = "offerDescription";

    private static final String BANNER_IMAGE_PATH = "bannerImage";

    private static final String VALIDITY = "validity";

    private static final String DISCOUNT_VALUE = "discountValue";

    private static final String OFFER_SPECIFIC = "offerSpecifics";

    private static final String GIFT_CARD_LINK = "giftCardLink";

    private static final String OFFERS_DETAILS_RESOURCETYPE = "tajhotels/components/content/partner-promotion-offer-details";

    protected final String RENDITION_VAL = "cq5dam.web.319.319.jpeg";

    private String name;

    List<Resource> children;

    private String validityDate;

    @Inject
    @Optional
    private String offerShortDesc;

    @Inject
    @Optional
    private String offerRateCode;

    private String offerDescription;

    @Inject
    @Optional
    private String offerTitle;

    private String offerSpecific;

    private String discountValue;

    @Named("cq:tags")
    @Inject
    @Optional
    private List<String> offerTypes;

    private String offerImage;

    private String resourceURL;

    @PostConstruct
    public void init() {
        readProperties();
    }

    private void readProperties() {
        String methodName = "readProperties";
        LOG.trace("Method Entry :" + methodName);
        resourceResolver = resource.getResourceResolver();
        String imageUrl = extractBannerImage(resource);
        if (imageUrl != null) {
            String renditionURL = getImageURLForRendition(imageUrl);
            if (renditionURL != null) {
                offerImage = renditionURL;
            } else {
                offerImage = imageUrl;
            }
        }

        Resource root = resource.getChild("root");
        for (Resource child : root.getChildren()) {
            if (child.getResourceType().equals(OFFERS_DETAILS_RESOURCETYPE)) {
                ValueMap properties = child.getValueMap();
                offerDescription = properties.get(OFFER_DESCRIPTION, String.class);
                validityDate = properties.get(VALIDITY, String.class);
                discountValue = properties.get(DISCOUNT_VALUE, String.class);
                offerSpecific = properties.get(OFFER_SPECIFIC, String.class);
                resourceURL = properties.get(GIFT_CARD_LINK, String.class);
                LOG.trace("Received Resource URL as :" + resourceURL);

            }
        }

    }

    private String getImageURLForRendition(String imagePath) {
        Asset asset = getAssetForImagePath(imagePath);
        if (asset != null) {
            return extractAssetRendtion(asset);
        }
        return null;
    }

    private Asset getAssetForImagePath(String imagePath) {
        Resource assetResource = resourceResolver.getResource(imagePath);
        if (assetResource != null) {
            Asset asset = assetResource.adaptTo(Asset.class);
            return asset;
        }
        return null;
    }

    private String extractAssetRendtion(Asset asset) {

        if (asset != null) {
            Rendition renditionForCard = asset.getRendition(RENDITION_VAL);
            if (renditionForCard != null) {
                return renditionForCard.getPath();
            } else {
                return asset.getPath();
            }
        }
        return null;
    }

    private String extractBannerImage(Resource resource) {
        Resource bannerParsys = resource.getChild(CrxConstants.BANNER_PARSYS_COMPONENT_NAME);
        if (bannerParsys != null) {
            Resource offerBanner = bannerParsys.getChild(CrxConstants.OFFER_BANNER_COMPONENT_NAME);
            if (offerBanner != null) {
                ValueMap valMap = offerBanner.getValueMap();
                String imageURL = String.valueOf(valMap.get(BANNER_IMAGE_PATH));
                return imageURL;
            }
            return null;
        }
        return null;
    }

    public Resource getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public String getValidityDate() {
        return validityDate;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public String getOfferShortDesc() {
        return offerShortDesc;
    }

    public String getOfferRateCode() {
        return offerRateCode;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public String getOfferSpecific() {
        return offerSpecific;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public List<String> getOfferTypes() {
        return offerTypes;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public String getResourceURL() {
        return resourceURL;
    }

}
