package com.ihcl.core.models;

import com.ihcl.core.models.impl.DropDownItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class EpicureEnroll {
    @Inject
    private List<DropDownItem> genderOptions;

    public List<DropDownItem> getGenderOptions() {
        return genderOptions;
    }
}
