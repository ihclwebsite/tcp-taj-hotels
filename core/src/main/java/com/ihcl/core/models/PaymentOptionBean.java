package com.ihcl.core.models;

public class PaymentOptionBean {

	private String paymentOption;
	private String paymentOptionItemField;
	private String paymentOptionDescription;
	
	public String getPaymentOptionItemField() {
		return paymentOptionItemField;
	}

	public void setPaymentOptionItemField(String paymentOptionItemField) {
		this.paymentOptionItemField = paymentOptionItemField;
	}
	
	public String getPaymentOptionDescription() {
		return paymentOptionDescription;
	}

	public void setPaymentOptionDescription(String paymentOptionDescription) {
		this.paymentOptionDescription = paymentOptionDescription;
	}

	public String getPaymentOption() {
        return paymentOption;
    }
    
    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }
}
