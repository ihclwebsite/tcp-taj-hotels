package com.ihcl.core.models.dialog.text;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface MultiValueTextModel {

    String[] getValues();

    String[] getNames();

}
