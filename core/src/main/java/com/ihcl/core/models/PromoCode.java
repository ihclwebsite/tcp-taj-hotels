package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class PromoCode extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    private Map<String,String> promoCodeMap = new HashMap<String,String>();
    
    private String key="promoCodes";

    private static final String PROMO_CODES_LIST_JSON_KEY = "validPromos";
    
    private String promoCodesString;

    private List<String> promoCodesList;

	public String getPromoCodesString() {
		return promoCodesString;
	}

	public void setPromoCodesString(String promoCodesString) {
		this.promoCodesString = promoCodesString;
	}

	public String getPromoCodesListJson() {
	    JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(PROMO_CODES_LIST_JSON_KEY, promoCodesList);
        } catch (JSONException e) {
            log.error("Exception occured while creating the JSON Object :: > " + e.getMessage());
        }
	    return jsonObject.toString();
    }

    public List<String> getPromoCodesList() {
        return promoCodesList;
    }

    @Override
	public void activate() throws Exception {
		
		String methodName ="activate";
		log.trace("Method Entry :: " + methodName);
		promoCodesList = new ArrayList<>();
		Node currentNode = getResource().adaptTo(Node.class);
		if(currentNode!=null) {
	        NodeIterator ni = currentNode.getNodes();
	
	        while (ni.hasNext()) {
	
	            Node child = ni.nextNode();
	            NodeIterator ni2 = child.getNodes();
	            processNode(ni2);
	        }
	        setPromoCodesString(buildJSONObject(key,promoCodeMap).toString());
		}
      log.trace("Method Exit :: " +methodName);
	}
	
	private void processNode(NodeIterator ni2) {
		
		String methodName ="processNode";
		log.trace("Method Entry :: " + methodName);
		
		 try {
	            while (ni2.hasNext()) {

	                Node grandChild = ni2.nextNode();
	                if(grandChild.getPath().contains(key)) {
	                	log.trace("Grand Child Node Path for PromoCodes  :: > " + grandChild.getPath());
	                	if(grandChild.hasProperty("label") && grandChild.hasProperty("value")) {
                            promoCodeMap.put(grandChild.getProperty("label").getString(), grandChild.getProperty("value").getString());
                            promoCodesList.add(grandChild.getProperty("value").getString());
                        }
	                }
	            }
	        } catch (Exception e) {
	            log.error("Exception while fetching Multifield data :: >", e.getMessage(), e);
	        }
		log.trace("Method Exit :: " +methodName);
	}
	
	private JSONObject buildJSONObject (String key, Map<String,String> inputMap) {
        
        JSONObject jsonObject = new JSONObject();
        try {
        	jsonObject.put(key, inputMap);
		} catch (JSONException e) {
			log.error("Exception occured while creating the JSON Object :: > " + e.getMessage());
		}
		return jsonObject;	
	}
        
}
