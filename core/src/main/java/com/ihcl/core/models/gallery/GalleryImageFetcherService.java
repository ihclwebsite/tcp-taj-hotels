package com.ihcl.core.models.gallery;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.json.JSONObject;

public interface GalleryImageFetcherService {

    /**
     * @param galleryResource
     * @return
     */
    List<JSONObject> getGalleryImagePathsAt(Resource galleryResource);

    String getParentPath(Resource galleryResource);

    Integer getDepth(Resource galleryResource);
}
