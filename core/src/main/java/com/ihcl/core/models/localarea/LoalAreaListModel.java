package com.ihcl.core.models.localarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.PlacesToVisitBean;

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = LoalAreaListModel.class)
public class LoalAreaListModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    SlingHttpServletRequest request;

    @Inject
    Resource resource;


    String message;

    private String time;

    private Resource currentRes;

    private String localAreaName;

    private String conciereUniqueName;


    private String tagValue;

    List<Resource> children;

    ValueMap vm = null;

    @PostConstruct
    protected void init() {
        if (!resource.getResourceType().equals("tajhotels/components/content/local-area-details")) {
            Resource tags = resource.getChild("jcr:content");
            tagValue = tags.adaptTo(ValueMap.class).get("cq:tags", String.class);
            LOG.trace("Local area tags :" + tagValue);
            Resource root = resource.getChild("jcr:content/root");
            if (null != root && !"".equals(root)) {
                // LOG.trace("Inside root " + root);
                Iterable<Resource> rootChildren = root.getChildren();
                for (Resource rootResource : rootChildren) {
                    // LOG.trace("\n\n\n\nInside resource :: " + rootResource.getPath());
                    // LOG.trace("\n\n\n\nInside resource getName:: " + rootResource.getName());
                    String resourceType = rootResource.getResourceType();
                    if (resourceType.equals("tajhotels/components/content/local-area-details")) {
                        initializeProperties(rootResource);
                    }
                }
            }
        } else {
            // LOG.trace("\n\nin localarealistmodel.java - in else block");
            initializeProperties(resource);
        }
    }

    private void initializeProperties(Resource currentResource) {

        // LOG.trace(" in initializepro mthod() \n\n- " + currentResource.getPath());

        vm = currentResource.getValueMap();
        // LOG.trace("using ValueMappp - - - - " + vm.get("time"));

        placeToVisit = resource.adaptTo(PlacesToVisitBean.class);
        currentRes = currentResource;
        // LOG.info("Time from PlacesToVisitBean " + placeToVisit.getTime());
        // LOG.info("VALUEMAPP - " + vm);
    }

    public ValueMap getValueMap() {
        // LOG.trace("using ValueMappp2 - - - - " + vm.get("time"));
        return vm;
    }

    private PlacesToVisitBean placeToVisit;

    public PlacesToVisitBean getPlaceToVisitBean() {
        LOG.info("INIDE THE PLACETOVISITBEAN -- --");
        return placeToVisit;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getConciereUniqueName() {
        LOG.trace("vm.get(placeName) -" + vm.get("placeName"));
        conciereUniqueName = ((String) vm.get("placeName")).replaceAll("\\s", "");
        LOG.trace("conciereUniqueName - " + conciereUniqueName);
        return conciereUniqueName;
    }


    public String getTagValue() {
        LOG.trace("Inside Getter method --> >Local area tags :" + tagValue);
        return tagValue;
    }


}
