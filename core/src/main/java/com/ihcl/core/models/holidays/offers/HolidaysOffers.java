package com.ihcl.core.models.holidays.offers;

import java.util.List;

import com.ihcl.core.models.holidays.hotel.UserSellingPoints;

/**
 * @author moonraft
 *
 */
public class HolidaysOffers {

    private List<UserSellingPoints> listOfUserSellingPoints;

    private String offerImage;

    private String offerTitle;

    private String offerDescription;

    private String offerPrice;

    private String offerCode;

    private String hotelName;

    private String hotelCity;

    private String latitude;

    private String longitude;

    private String hotelGuestRoomLink;

    private String hotelLink;

    private String noOfNights;

    private String offerpageLink;

    private String currencySymbol;

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public void setHotelCity(String hotelCity) {
        this.hotelCity = hotelCity;
    }

    public void setHotelGuestRoomLink(String hotelGuestRoomLink) {
        this.hotelGuestRoomLink = hotelGuestRoomLink;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public String getOfferCode() {
        return offerCode;
    }

    public String getHotelName() {
        return hotelName;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getHotelGuestRoomLink() {
        return hotelGuestRoomLink;
    }

    public List<UserSellingPoints> getListOfUserSellingPoints() {
        return listOfUserSellingPoints;
    }

    public void setListOfUserSellingPoints(List<UserSellingPoints> listOfUserSellingPoints) {
        this.listOfUserSellingPoints = listOfUserSellingPoints;
    }

    public String getHotelLink() {
        return hotelLink;
    }

    public void setHotelLink(String hotelLink) {
        this.hotelLink = hotelLink;
    }

    public String getNoOfNights() {
        return noOfNights;
    }

    public void setNoOfNights(String noOfNights) {
        this.noOfNights = noOfNights;
    }

    public String getOfferpageLink() {
        return offerpageLink;
    }

    public void setOfferpageLink(String offerpageLink) {
        this.offerpageLink = offerpageLink;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }


}
