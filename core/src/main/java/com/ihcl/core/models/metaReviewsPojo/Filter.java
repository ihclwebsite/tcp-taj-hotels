/**
 *
 */
package com.ihcl.core.models.metaReviewsPojo;


/**
 * @author moonraft
 *
 */

public class Filter {

    private String trip_type;

    private String language;

    public String getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(String trip_type) {
        this.trip_type = trip_type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "ClassPojo [trip_type = " + trip_type + ", language = " + language + "]";
    }
}
