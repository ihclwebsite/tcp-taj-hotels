package com.ihcl.core.models.holidays.sitemap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.DestinationSearchService;

/**
 * 
 * @author moonraft This class implements IHolidaysSiteMap Interface to provide
 *         all the page links for Holidays Site Map
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class }, adapters = IHolidaysSiteMap.class)
public class HolidaysSiteMapImpl implements IHolidaysSiteMap {

	private static final Logger LOG = LoggerFactory.getLogger(HolidaysSiteMapImpl.class);

	private static final String HOLIDAYS_HOME_PAGE_PATH = "/content/tajhotels/en-in/taj-holidays";
	private static final String DESTINATION_PAGE_RESOURCE_TYPE = "tajhotels/components/structure/tajholidays-destination-listing-page";
	private static final String DISCOVER_MORE_TEMPLATE = "/conf/tajhotels/settings/wcm/templates/tajholidays-discovermore-landing-page-template";
	private static final String PAGE_RESOURCE_TYPE = "cq:Page";
	private static final String PAGE_JCR_CONTENT = "/jcr:content";
	private static final String PAGE_JCR_TITLE = "jcr:title";
	private static final String OFFERS = "offers";
	@Inject
	private DestinationSearchService destinationSearchService;

	@Inject
	ResourceResolverFactory resourceResolverFactory;

	private List<HolidaysPageLink> pages;
	private List<HolidaysPageLink> discoverMoreLinks;
	private Map<String, List<HolidaysPageLink>> categoriesAndChildren;
	private List<HolidayHotelOffers> holidayHotelOffers;

	@PostConstruct
	public void activate() {
		String methodName = "activate";
		LOG.trace("Method Entry: {}", methodName);
		try {
			pages = fetchHomePageLink();
			discoverMoreLinks = fetchDiscoverMorePages();
			categoriesAndChildren = fetchAllHolidayCategoriesAndChildPages();
			holidayHotelOffers = fetchAllHolidayHotelOffers();
		} catch (Exception e) {
			LOG.error("Exception while building links for Holiday pages");
			e.printStackTrace();
		}
		LOG.trace("Method Exit: {}", methodName);
	}

	/**
	 * Fetch all holiday Hotels Offers
	 * 
	 * @return List of hotels ad their offers
	 * @throws LoginException 
	 */
	private List<HolidayHotelOffers> fetchAllHolidayHotelOffers() throws LoginException {

		LOG.debug("Method entry : fetchAllHolidayHotelOffers()");

		List<HolidayHotelOffers> hotelsoffers = new LinkedList<>();
		List<String> hotelsPath = destinationSearchService.getAllHolidayHotels();

		for (String path : hotelsPath) {
			HolidayHotelOffers hotel= new HolidayHotelOffers();
			List<String> hotelOffersLinks = destinationSearchService.getAllHolidayHotelOffers(path);
			if(!(hotelOffersLinks.isEmpty()) || !(hotelOffersLinks.size()<1)){
				hotel.setHotelName(getHotelName(path));
				hotel.setHotelOffers(getOffers(hotelOffersLinks));
				hotelsoffers.add(hotel);
			}
		}
		Collections.sort(hotelsoffers);
		LOG.debug("Method exit : fetchAllHolidayHotelOffers()");
		return hotelsoffers;
	}
	
	/**
	 * Fetch Holiday Hotel name from page JCR
	 * @return String hotel name
	 * @throws LoginException 
	 */
	private String getHotelName(String path) throws LoginException {
		
		LOG.debug("Method Entry getHotelName(), received hotelPath as: {}",path);
		
		Resource hotelResource=getResourceFromResolver(path+PAGE_JCR_CONTENT);
		ValueMap hotelValueMap= hotelResource.adaptTo(ValueMap.class);
		String hotelName=toCamelCase(hotelValueMap.get("jcr:title",String.class));
		
		LOG.debug("Method Exit getHotelName(), returning hotelname as: {}",hotelName); 
		return hotelName;
	}

	/**
	 * Generates links for all holiday offer pages under a given offer paths
	 * 
	 * @param allHolidayOffers
	 * @return list of offer page links if available null otherwise
	 * @throws LoginException 
	 */
	private List<HolidaysPageLink> getOffers(List<String> allHolidayOffers) throws LoginException {
		
		LOG.debug("Method entry getOffers()");
		
		List<HolidaysPageLink> offers=new LinkedList<>();
		for(String path: allHolidayOffers){
			HolidaysPageLink offer= new HolidaysPageLink();
			Resource offerResource=getResourceFromResolver(path+PAGE_JCR_CONTENT);
			ValueMap offerValueMap= offerResource.adaptTo(ValueMap.class);
			String hotelName=offerValueMap.get("jcr:title",String.class);
			offer.setPageName(toCamelCase(hotelName));
			offer.setPageUrl(path);
			offers.add(offer);
			
		}
		Collections.sort(offers);
		LOG.debug("Method exit getOffers()");
		return offers;
	}

	/**
	 * Generate a list of discover more links
	 * 
	 * @return list of links
	 * @throws LoginException
	 */
	private List<HolidaysPageLink> fetchDiscoverMorePages() throws LoginException {

		LOG.debug("Method Entry: fetchDiscoverMorePages()");

		List<HolidaysPageLink> discoverMore = new LinkedList<>();
		Resource resource = getResourceFromResolver(HOLIDAYS_HOME_PAGE_PATH);
		Iterable<Resource> children = resource.getChildren();
		for (Resource child : children) {
			if (child.getResourceType().equals(PAGE_RESOURCE_TYPE)) {
				LOG.debug("Page type child found:{}", child.getPath());
				Resource childPage = getResourceFromResolver(child.getPath() + PAGE_JCR_CONTENT);
				LOG.debug("resource value: {}", childPage);
				ValueMap pageMap = childPage.adaptTo(ValueMap.class);
				String templateType = pageMap.get("cq:template", String.class);
				String pageName = toCamelCase(pageMap.get(PAGE_JCR_TITLE, String.class));
				if (templateType.equals(DISCOVER_MORE_TEMPLATE)) {
					LOG.debug("Discover more page template type found. Building page link..: {}", childPage.getPath());
					HolidaysPageLink pagelink = new HolidaysPageLink();
					pagelink.setPageUrl(child.getPath());
					pagelink.setPageName(pageName);
					discoverMore.add(pagelink);
				}
			} // end of if block
		}
		LOG.debug("Method Exit: fetchDiscoverMorePages()");
		return discoverMore;
	}

	/**
	 * Generates a map of links and title for all child pages for taj holidays
	 * tree
	 * 
	 * @return map of categories and list of its child pages
	 * @throws LoginException
	 */
	private Map<String, List<HolidaysPageLink>> fetchAllHolidayCategoriesAndChildPages() throws LoginException {

		LOG.debug("Method Entry: fetchAllHolidayCategoriesAndChildPages()");

		Map<String, List<HolidaysPageLink>> categoriesMap = new TreeMap<>();
		Resource resource = getResourceFromResolver(HOLIDAYS_HOME_PAGE_PATH);
		Iterable<Resource> homepageChildren = resource.getChildren();

		for (Resource child : homepageChildren) {
			if (child.getResourceType().equals(PAGE_RESOURCE_TYPE)) {
				LOG.debug("Page type child found:{}", child.getPath());
				Resource childPage = getResourceFromResolver(child.getPath() + PAGE_JCR_CONTENT);
				LOG.debug("resource value: {}", childPage);
				List<HolidaysPageLink> childPageChildren = getAllChildrenLinks(child.getPath());
				if (!(null == childPageChildren)) {
					LOG.debug("This page has Child pages: {}", child.getPath());
					ValueMap childValueMap = childPage.adaptTo(ValueMap.class);
					String pageTitle = toCamelCase(childValueMap.get(PAGE_JCR_TITLE, String.class));
					LOG.debug("Adding list and title to map: {}", pageTitle);
					categoriesMap.put(pageTitle, childPageChildren);
				} else {
					LOG.debug("This page does not have Child pages: {}", childPage.getPath());
				}

			} // end of if block
		} // end of for block
		LOG.debug("Method Exit: fetchAllHolidayCategoriesAndChildPages()");
		return categoriesMap;
	}

	/**
	 * Generates links for all the child pages for specified parent path
	 * 
	 * @param childPagePath
	 * @return List of links and page names for each parent path
	 * @throws LoginException
	 */
	private List<HolidaysPageLink> getAllChildrenLinks(String childPagePath) throws LoginException {

		LOG.debug("Method Entry: getAllChildrenLinks()");

		List<HolidaysPageLink> allPages = new LinkedList<>();
		Resource page = getResourceFromResolver(childPagePath + PAGE_JCR_CONTENT);
		if (page.getValueMap().get("sling:resourceType", String.class).equals(DESTINATION_PAGE_RESOURCE_TYPE)) {
			LOG.debug("destinations page found. Attempting to get all destinations");
			allPages = getAllDestinationsPages(destinationSearchService.getAllHolidayDestinations());
		} else {
			allPages = getAllPages(childPagePath);
		}

		LOG.debug("Method Exit: getAllChildrenLinks()");
		return allPages;
	}

	/**
	 * Generates links for all holiday destination pages
	 * 
	 * @param allDestinationPages
	 * @return list of destinations page links
	 * @throws LoginException
	 */
	private List<HolidaysPageLink> getAllDestinationsPages(List<String> allDestinationPages) throws LoginException {

		LOG.debug("Method Entry: getAllDestinationsPages()");

		List<HolidaysPageLink> destinationsLinks = new LinkedList<>();
		for (String path : allDestinationPages) {
			LOG.debug("building details for path : {}", path);
			HolidaysPageLink pageLink = new HolidaysPageLink();
			Resource page = getResourceFromResolver(path + PAGE_JCR_CONTENT);
			ValueMap pageMap = page.adaptTo(ValueMap.class);
			pageLink.setPageName(pageMap.get("destinationName", String.class));
			pageLink.setPageUrl(getPackagePagePath(path));
			destinationsLinks.add(pageLink);
		}
		LOG.debug("Method Exit: getAllDestinationsPages()");
		return destinationsLinks;
	}

	/**
	 * Generates path of the package page for a given destination page path
	 * 
	 * @param destPath
	 * @return destination package page path
	 * @throws LoginException
	 */
	private String getPackagePagePath(String destPath) throws LoginException {

		LOG.debug("Inside method: getPackagePagePath()");
		LOG.debug("Resource path: {}", destPath);
		Resource page = getResourceFromResolver(destPath);
		String pathForPackage = "";
		Iterable<Resource> children = page.getChildren();
		for (Resource child : children) {

			if (child.getResourceType().equals(PAGE_RESOURCE_TYPE) && child.getPath().contains("packages")) {
				LOG.debug("resource type: {}", child.getResourceType());
				pathForPackage = child.getPath();
			}
		}
		LOG.debug("Leaving method: getPackagePagePath():: returning path: {}", pathForPackage);
		return pathForPackage;
	}

	/**
	 * Generates list of links for all the child pages under the given root path
	 * 
	 * @param rootPath
	 * @return list of paths with page name
	 * @throws LoginException
	 */
	private List<HolidaysPageLink> getAllPages(String rootPath) throws LoginException {

		LOG.debug("Method Entry: getAllPages()");
		List<HolidaysPageLink> pagesList = new ArrayList<>();
		boolean hasChildPages = false;
		Iterable<Resource> currentPageRoot = getResourceFromResolver(rootPath).getChildren();
		for (Resource child : currentPageRoot) {
			if (child.getResourceType().equals(PAGE_RESOURCE_TYPE)) {
				hasChildPages = true;
				HolidaysPageLink linkmodel = new HolidaysPageLink();
				Resource pageJcr = getResourceFromResolver(child.getPath() + PAGE_JCR_CONTENT);
				ValueMap pageValueMap = pageJcr.adaptTo(ValueMap.class);
				linkmodel.setPageUrl(child.getPath());
				linkmodel.setPageName(toCamelCase(pageValueMap.get(PAGE_JCR_TITLE, String.class)));
				LOG.debug("Adding page :{}- {}", linkmodel.getPageName(), linkmodel.getPageUrl());
				pagesList.add(linkmodel);
				Collections.sort(pagesList);
			}
		}
		if (!hasChildPages) {
			LOG.debug("no page type children found");
			pagesList = null;
		}
		LOG.debug("Method Exit: getAllPages()");
		return pagesList;
	}

	/**
	 * Fetches the home page link with page title
	 * 
	 * @return List of page links with title
	 * @throws LoginException
	 */
	private List<HolidaysPageLink> fetchHomePageLink() throws LoginException {

		LOG.debug("Method Entry: fetchHomePageLink()");

		List<HolidaysPageLink> pageLinks = new LinkedList<>();
		HolidaysPageLink links = new HolidaysPageLink();
		Resource resource = getResourceFromResolver(HOLIDAYS_HOME_PAGE_PATH + PAGE_JCR_CONTENT);
		ValueMap pageValueMap = resource.adaptTo(ValueMap.class);
		links.setPageName(toCamelCase(pageValueMap.get(PAGE_JCR_TITLE, String.class)));
		links.setPageUrl(HOLIDAYS_HOME_PAGE_PATH);
		pageLinks.add(links);
		LOG.debug("Method Exit: fetchHomePageLink()");
		return pageLinks;
	}

	/**
	 * Utility to get resource form a string path
	 * 
	 * @param resourcePath
	 *            path of required resource
	 * @return resource form the input path String
	 * @throws LoginException
	 */
	private Resource getResourceFromResolver(String resourcePath) throws LoginException {

		LOG.debug("Method Entry: getResourceFromResolver()");
		LOG.debug("Resolving resource for path :{}", resourcePath);
		ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
		Resource resource = resourceResolver.getResource(resourcePath);
		LOG.debug("Method Exit: getResourceFromResolver()");
		return resource;
	}

	/**
	 * Utility to convert any string expression to Camel case String
	 * 
	 * @param inputString
	 *            unformatted string
	 * @return formatted string in camel case
	 */
	private String toCamelCase(String inputString) {

		String input = inputString.trim().toLowerCase();
		final StringBuilder formattedString = new StringBuilder(input.length());
		for (final String word : input.split(" ")) {
			if (!word.isEmpty()) {
				formattedString.append(word.substring(0, 1).toUpperCase());
				formattedString.append(word.substring(1).toLowerCase());
			}
			if (formattedString.length() != input.length()) {
				formattedString.append(" ");
			}
		}

		return formattedString.toString();
	}

	@Override
	public List<HolidaysPageLink> getPageLinks() {
		return pages;
	}

	@Override
	public List<HolidaysPageLink> getDiscoverMoreLinks() {
		return discoverMoreLinks;
	}

	@Override
	public Map<String, List<HolidaysPageLink>> getCategoriesandChildren() {
		return categoriesAndChildren;
	}

	@Override
	public List<HolidayHotelOffers> getHolidayHotelOffers() {
		return holidayHotelOffers;
	}

}
