/**
 *
 */
package com.ihcl.core.models.jivaspa;

import java.util.List;

/**
 * @author moonraft
 *
 */
public interface JivaSpaTreatmentFetcherService {

    List<String> getJivaSpaTreatmentPath();
}
