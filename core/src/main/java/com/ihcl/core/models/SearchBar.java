package com.ihcl.core.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class SearchBar {

    @Inject
    @Optional
    private String helloText;

    @Inject
    @Optional
    private String searchPlaceholder;

    @Inject
    @Optional
    private String searchButtonText;

    @Inject
    @Optional
    private String exploreTajTitle;

    @Inject
    @Optional
    private String destinationResultsTitle;

    @Inject
    @Optional
    private String holidayDestinationTitle;

    @Inject
    @Optional
    private String holidayHotelTitle;

    @Inject
    @Optional
    private String websiteHotelResultsTitle;

    @Inject
    @Optional
    private String otherHotelResultsTitle;

    @Inject
    @Optional
    private String websiteRestaurantResultsTitle;

    @Inject
    @Optional
    private String otherRestaurantResultsTitle;

    @Inject
    @Optional
    private String websiteExperienceResultsTitle;

    @Inject
    @Optional
    private String otherExperienceResultsTitle;

    @Inject
    @Optional
    private List<SearchSuggestion> searchSuggestions;

    @Inject
    @Optional
    private List<Link> exploreTajLinks;

    @Inject
    @Optional
    private List<Link> trendingSearches;


    public String getHelloText() {
        return helloText;
    }


    public String getSearchPlaceholder() {
        return searchPlaceholder;
    }


    public String getSearchButtonText() {
        return searchButtonText;
    }


    public String getExploreTajTitle() {
        return exploreTajTitle;
    }


    public String getDestinationResultsTitle() {
        return destinationResultsTitle;
    }


    public String getHolidayDestinationTitle() {
        return holidayDestinationTitle;
    }


    public String getHolidayHotelTitle() {
        return holidayHotelTitle;
    }


    public String getWebsiteHotelResultsTitle() {
        return websiteHotelResultsTitle;
    }


    public String getOtherHotelResultsTitle() {
        return otherHotelResultsTitle;
    }


    public String getWebsiteRestaurantResultsTitle() {
        return websiteRestaurantResultsTitle;
    }


    public String getOtherRestaurantResultsTitle() {
        return otherRestaurantResultsTitle;
    }


    public String getWebsiteExperienceResultsTitle() {
        return websiteExperienceResultsTitle;
    }


    public String getOtherExperienceResultsTitle() {
        return otherExperienceResultsTitle;
    }


    public List<SearchSuggestion> getSearchSuggestions() {
        return searchSuggestions;
    }


    public List<Link> getExploreTajLinks() {
        return exploreTajLinks;
    }


    public List<Link> getTrendingSearches() {
        return trendingSearches;
    }


}
