/**
 *
 */
package com.ihcl.core.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author moonraft
 *
 */
@Model(adaptables = Resource.class)
public class IncredibleEscapesModel {

    private static final Logger LOG = LoggerFactory.getLogger(IncredibleEscapesModel.class);

    @Self
    Resource resource;

    private String destinationName;

    private String destinationDescription;

    private String destinationImage;

    @PostConstruct
    protected void init() {

        LOG.info("Inside init() of class IncredibleEscapesModel");
        Resource content = resource.getChild("jcr:content");
        LOG.debug("checking if content has values : {}", content);
        if (content != null) {

            ValueMap valueMap = content.adaptTo(ValueMap.class);
            destinationImage = valueMap.get("sampleImage", String.class);
            destinationName = valueMap.get("categoryName", String.class);
            destinationDescription = valueMap.get("description", String.class);

            LOG.debug("destinationImage : {}", destinationImage);
            LOG.debug("destinationName : {}", destinationName);
            LOG.debug("destinationDescription : {}", destinationDescription);
        } else {
            LOG.error("content found as null.");
        }
    }

    public Resource getResource() {
        return resource;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public String getDestinationDescription() {
        return destinationDescription;
    }

    public String getDestinationImage() {
        return destinationImage;
    }
}
