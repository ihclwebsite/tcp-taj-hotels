/**
 *
 */
package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.destination.HolidayDestinationModel;
import com.ihcl.core.util.URLMapUtil;


/**
 * @author moonraft
 *
 */
@Model(
        adaptables = Resource.class)
public class HolidaysPopularDestinationsModel {

    private static final Logger LOG = LoggerFactory.getLogger(HolidaysPopularDestinationsModel.class);

    @Self
    Resource resource;
    
    @Inject
	private ResourceResolverFactory resourceResolverFactory;

    private String destinationCity;

    private String destinationState;

    private String destinationImage;

    private String destinationPath;
    
    private String mappedDestinationPath;

    @PostConstruct
    protected void init() {

        LOG.info("Inside init() of class HolidaysPopularDestinationsModel");
        Resource content = resource.getChild("jcr:content");
        LOG.debug("checking if content has values : {}", content);

        if (content != null) {
            ValueMap valueMap = content.adaptTo(ValueMap.class);
            LOG.info("getting values");
            LOG.debug("destinationCity : {}", valueMap.get("destinationName", String.class));
            LOG.debug("destinationState : {}", valueMap.get("stateName", String.class));
            LOG.debug("ImagePath : {}", valueMap.get("imagePath", String.class));
            destinationImage = valueMap.get("imagePath", String.class);
            destinationCity = valueMap.get("destinationName", String.class);
            destinationState = valueMap.get("stateName", String.class);
            HolidayDestinationModel objHolodayDestinationModel = new HolidayDestinationModel();
            destinationPath = objHolodayDestinationModel.getPackagePath(resource);
            mappedDestinationPath = getMappedPath(destinationPath);
        } else {
            LOG.error("content found as null.");
        }
    }

    public Resource getResource() {
        return resource;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public String getDestinationState() {
        return destinationState;
    }

    public String getDestinationImage() {
        return destinationImage;
    }


    public String getDestinationPath() {
        return destinationPath;
    }
    
	public String getMappedDestinationPath() {
		return mappedDestinationPath;
	}

	private String getMappedPath(String url) {
		String methodName = "getMappedPath";
		LOG.debug("Method Entry :: " + methodName);
		if (url != null) {
			ResourceResolver resourceResolver = null;
			try {
				resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
				LOG.debug("Recieved resolvedResolver : " + resourceResolver);
				String resolvedURL = resourceResolver.map(URLMapUtil.appendHTMLExtension(url));
				LOG.trace("Resolved URL " + resolvedURL);
				LOG.debug("Method Exit :: " + methodName);
				return URLMapUtil.getPathFromURL(resolvedURL);
			} catch (LoginException e) {
				LOG.error("Exxception occured while fetching the resourceResolver :: " + e.getMessage());
			}
		}
		LOG.trace("URL unresolved returning  :-" + url);
		LOG.debug("Method Exit :: " + methodName);
		return url;
	}

}
