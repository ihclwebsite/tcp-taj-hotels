package com.ihcl.core.models.gallery;

import java.util.List;

import org.json.JSONObject;
import org.osgi.annotation.versioning.ConsumerType;

/**
 * Interface to fetch gallery images.
 *
 */
@ConsumerType
public interface GalleryImageSelector {

    /**
     * Method to get list of images from resource.
     *
     * @return List<String> - list of image paths.
     */
    List<JSONObject> getImagePaths();

    /**
     * Method to get the number of images in gallery.
     *
     * @return Integer - numberOfImages.
     */
    Integer getTotalNumberOfImages();
}
