package com.ihcl.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.ihcl.core.services.search.OfferSearchImpl;
import com.ihcl.core.services.search.OffersSearchService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class)
public class TrendingOffers {

    private List<Page> offerList;

    private ResourceResolver resourceResolver;
    private static final Logger LOG = LoggerFactory.getLogger(TrendingOffers.class);

    @Self
    private Resource resource;
    
    @Inject
    @Default(values="/content/tajhotels/en-in/offers")
    private String viewAllLinkUrl;

    @Inject
    OffersSearchService offersSearchService;

    @PostConstruct
    protected void init() {
        resourceResolver = resource.getResourceResolver();
        offerList = new ArrayList<>();
        getAllOffersByRank();
    }

    private void getAllOffersByRank() {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        List<String> offersPathList = offersSearchService.getAllOffersByRank(viewAllLinkUrl + "/");
        try {
            for (String path : offersPathList) {
                offerList.add(pageManager.getContainingPage(path));
            }
        } catch (Exception e) {
            LOG.error("Exception found while getting the offers :: {} for the list :: {}", e.getMessage(), offersPathList);
            LOG.debug("Exception found while getting the offers :: {} for the list :: {}", e, offersPathList);
        }
    }

    public List<Page> getOfferList() {
        return offerList;
    }
}
