package com.ihcl.core.models;

import com.ihcl.core.services.search.DestinationSearchService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class PopularDestinations {
    @Inject
    private DestinationSearchService destinationSearchService;

    private List<String> destinations;

    public List<String> getDestinations() {
        return destinations;
    }

    @PostConstruct
    protected void init() {
        destinations = destinationSearchService.getDestinationsByRank();
    }
}
