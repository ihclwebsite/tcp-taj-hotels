/**
 *
 */
package com.ihcl.core.models.holidays.packages;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

import com.ihcl.core.models.holidays.hotel.Inclusion;


/**
 * @author moonraft
 *
 */
@ConsumerType
public interface IHolidayPackageInclusions {

    List<Inclusion> getAllInclusions();
}
