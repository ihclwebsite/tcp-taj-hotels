/**
 *
 */
package com.ihcl.core.models;

import java.util.List;

public interface LocalareaFilter {

    List<String> getAttractions();
}
