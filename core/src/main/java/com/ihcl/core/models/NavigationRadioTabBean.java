package com.ihcl.core.models;

/**
 * Model class for radio button in experience navigation bar component
 * 
 * @author user
 *
 */
public class NavigationRadioTabBean {

	public String radioButtonText;
	public String format;

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getRadioButtonText() {
		return radioButtonText;
	}

	public void setRadioButtonText(String radioButtonText) {
		this.radioButtonText = radioButtonText;
	}
}
