/**
 *
 */
package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.util.services.TajCommonsFetchResource;


@Model(adaptables = Resource.class,
        adapters = PressReleaseCard.class)
public class PressReleaseCard {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Self
    private Resource resource;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private static final String DEFAULT_IMAGE_PATH = "/content/dam/thrp/homepage/3x2/pressRelease_3x2.jpg";

    private String title;

    private String date;

    private String imagePath;

    private String urlPath;

    private String brand;

    private String theme;

    private String year;

    private String text;

    ValueMap valueMap;

    @PostConstruct
    public void init() {
        log.trace(" Method Entry : init()");
        log.trace("resource :" + resource.getPath());
        urlPath = resource.getPath().concat(".html");
        try {
            Resource pressResource = resource.getChild("jcr:content/root/press_release_main/par_1");
            ValueMap valueMap;
            for (Resource child : pressResource.getChildren()) {
                if (child.getResourceType().equals("tajhotels/components/content/ihcl/title")
                        || child.getResourceType().equals("tajhotels/components/content/title")) {
                    valueMap = child.getValueMap();
                    title = valueMap.get("jcr:title", String.class);
                } else if (child.getResourceType().equals("tajhotels/components/content/press-release")) {
                    valueMap = child.getValueMap();
                    date = valueMap.get("date", String.class);

                    text = valueMap.get("text", String.class);

                    String pressBrand = valueMap.get("brands", String.class);
                    if (pressBrand != null) {
                        brand = pressBrand.substring(pressBrand.lastIndexOf("/") + 1);
                    }

                    String pressTheme = valueMap.get("cq:tags", String.class);
                    if (pressTheme != null) {
                        theme = pressTheme.substring(pressTheme.lastIndexOf("/") + 1);
                    }

                    String pressYear = valueMap.get("year", String.class);
                    if (pressYear != null) {
                        year = pressYear.substring(pressYear.lastIndexOf("/") + 1);
                    }

                    Resource imageResource = child.getChild("pressCarousel/item0");
                    ValueMap imageValueMAp = imageResource.getValueMap();
                    String pressImage = imageValueMAp.get("imagepath", String.class);
                    if (pressImage != null && !("".equals(pressImage))) {
                        imagePath = pressImage;
                    } else {
                        imagePath = DEFAULT_IMAGE_PATH;
                    }
                }
            }
            log.trace("imagePath :" + imagePath);
            log.trace(" Method Exit : init()");
        } catch (Exception e) {
            log.error("An error occured while fetching Presee releases", e);
        }
    }

    public ValueMap getValueMap() {
        log.trace(" Method Entry : getvalueMap()");
        return valueMap;
    }


    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }


    public String getDate() {
        return date;
    }


    public String getImagePath() {
        return imagePath;
    }

    public String getUrlPath() {
        return urlPath;
    }


    public String getBrand() {
        return brand;
    }


    public String getTheme() {
        return theme;
    }


    public String getYear() {
        return year;
    }


}
