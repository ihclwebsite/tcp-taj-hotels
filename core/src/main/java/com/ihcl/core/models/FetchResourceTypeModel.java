/**
 *
 */
package com.ihcl.core.models;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.search.QueryBuilder;

/**
 * @author moonraft
 *
 */

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class })

public class FetchResourceTypeModel {

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    Resource resource;

    @Inject
    SlingHttpServletRequest request;

    @Reference
    private QueryBuilder builder;

    private Session session;

    private ResourceResolver resolver;

    @PostConstruct
    protected void init() throws Exception {
        String methodName = "init";
        LOG.trace("Method Entry:" + methodName);
        resolver = resource.getResourceResolver();
        session = resolver.adaptTo(Session.class);
        LOG.trace("session :" + session);
        QueryResult result = fetchQueryResult();
        LOG.trace("Query Result Size :" + result.getNodes().getSize());
        final Map<String, String> resourceType = fetchResourceType(result);

        DataSource dataSource = createDataSource(resourceType);
        LOG.trace("Method Exit:" + methodName);
        request.setAttribute(DataSource.class.getName(), dataSource);
    }

    private QueryResult fetchQueryResult() throws RepositoryException {
        String methodName = "fetchQueryResult";
        LOG.trace("Method Entry:" + methodName);
        QueryManager queryManager = session.getWorkspace().getQueryManager();

        Query query = queryManager.createQuery(
                "/jcr:root/apps/tajhotels/components/content//element(*,cq:Component)[jcr:contains(@componentGroup,'tajhotels')]",
                Query.XPATH);

        QueryResult result = query.execute();
        LOG.trace("Method Exit:" + methodName);
        return result;
    }

    private Map<String, String> fetchResourceType(QueryResult result) throws Exception {
        String methodName = "fetchResourceType";
        LOG.trace("Method Entry:" + methodName);
        Map<String, String> resourceType = new HashMap<String, String>();
        NodeIterator nodes = result.getNodes();
        Node node;
        while (nodes.hasNext()) {
            node = nodes.nextNode();
            LOG.trace("node ::::" + node);
            if (null != node.getProperty("jcr:title").getValue()
                    && !"".equals(node.getProperty("jcr:title").getValue())) {
                resourceType.put(node.getPath(), node.getProperty("jcr:title").getValue().getString());
            }
        }
        LOG.trace("Method Exit:" + methodName);
        return resourceType;
    }

    @SuppressWarnings("unchecked")
    private DataSource createDataSource(Map<String, String> resourceType) {
        String methodName = "createDataSource";
        LOG.trace("Method Entry:" + methodName);
        // Creating the Datasource Object for populating the drop-down control.
        DataSource dataSource = new SimpleDataSource(
                new TransformIterator(resourceType.keySet().iterator(), new Transformer() {

                    @Override
                    // Transforms the input object into output object
                    public Object transform(Object inputResourePath) {
                        String methodName = "transform";
                        LOG.trace("Method Entry:" + methodName);
                        String resourcePath = (String) inputResourePath;
                        // Allocating memory to Map
                        ValueMap valueMap = new ValueMapDecorator(new HashMap<String, Object>());
                        // Populate the Map
                        valueMap.put("value", resourcePath);
                        valueMap.put("text", resourceType.get(resourcePath));
                        LOG.trace("Method Exit:" + methodName);
                        return new ValueMapResource(resolver, new ResourceMetadata(), "nt:unstructured", valueMap);
                    }
                }));
        LOG.trace("Method Exit:" + methodName);
        return dataSource;
    }

}
