/**
 * 
 */
package com.ihcl.core.models.impl;

import java.util.Map;

import com.ihcl.core.hotels.BrandDetailsBean;

/**
 * <pre>
 * WebsiteHotelsBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :03-Jun-2019
 * @ClassName : WebsiteHotelsBean.java
 * @Description : com.ihcl.core.models.impl -WebsiteHotelsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  03-Jun-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class WebsiteHotelsBean {

	private String websitePath;
	private Map<String, BrandDetailsBean> hotels;
	/**
	 * @return the websitePath
	 */
	public String getWebsitePath() {
		return websitePath;
	}
	/**
	 * @param websitePath the websitePath to set
	 */
	public void setWebsitePath(String websitePath) {
		this.websitePath = websitePath;
	}
	/**
	 * @return the hotels
	 */
	public Map<String, BrandDetailsBean> getHotels() {
		return hotels;
	}
	/**
	 * @param hotels the hotels to set
	 */
	public void setHotels(Map<String, BrandDetailsBean> hotels) {
		this.hotels = hotels;
	}
	/**
	 * @param websitePath
	 * @param hotels
	 */
	public WebsiteHotelsBean(String websitePath, Map<String, BrandDetailsBean> hotels) {
		super();
		this.websitePath = websitePath;
		this.hotels = hotels;
	}
	 
}
