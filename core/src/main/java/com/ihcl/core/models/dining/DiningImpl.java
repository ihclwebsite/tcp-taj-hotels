package com.ihcl.core.models.dining;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.core.shared.services.ResourceFetcherService;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = Dining.class)
public class DiningImpl implements Dining {

    private static final Logger LOG = LoggerFactory.getLogger(DiningImpl.class);

    @Inject
    private SlingHttpServletRequest request;

    private Resource resource;

    @Inject
    @Default(values = "")
    private String resourcePath;

    @Inject
    @Optional
    private Page currentPage;

    @Inject
    private ResourceFetcherService resourceFetcherService;

    @Inject
    private GalleryImageFetcherService galleryImageFetcher;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @Inject
    ResourceResolver resourceResolver;

    @Inject
    private ResourceResolverFactory resolverFactory;

    private static final String DINING_NAME = "diningName";

    private static final String DINING_SHORT_DESC = "diningShortDesc";

    private static final String DINING_ADDRESS = "diningAddress";

    private static final String DINING_DESCRIPTION = "diningDescription";

    private static final String DINING_LONG_DESCRIPTION = "diningLongDescription";

    private static final String IMAGE_PATH = "diningImage";

    private static final String DINING_PHONE_NUMBER = "diningPhoneNumber";

    private static final String DINING_CUISINE = "diningCuisine";

    private static final String DRESS_CODE = "dressCode";

    private static final String REVIEW_RATING = "reviewRating";

    private static final String TIC_POINTS = "ticPoints";

    private static final String EPICURE_POINTS = "epicurePoints";

    private static final String DINING_COST = "averagePrice";

    private static final String TAX_DISCLAIMER = "taxDisclaimer";

    private static final String DINING_ID = "diningID";

    private static final String OFFER_SPECIFICS = "offerSpecifics";

    private static final String DISCOUNT_VALUE = "discountValue";

    private static final String CURRENCY_SYMBOL = "currencySymbol";

    private static final String DINING_DETAILS_RESOURCETYPE = "tajhotels/components/content/dining-details";

    private static final String ROOT_GRID_RESOURCETYPE = "wcm/foundation/components/responsivegrid";

    private static final String DINING_BANNER_RESOURCETYPE = "tajhotels/components/content/diningbanner";

    private static final String DINING_LIST_RESOURCE_TYPE = "tajhotels/components/content/dining-list";

    private static final String PATH_TO_DOWNLOAD_FROM = "pathToDownloadFrom";

    private static final String TIMINGS_LABEL_1 = "timingslabel1";

    private static final String TIMINGS_LABEL_2 = "timingslabel2";

    private static final String TIMINGS_LABEL_3 = "timingslabel3";

    private static final String TIMINGS_VALUE_1 = "timingsvalue1";

    private static final String TIMINGS_VALUE_2 = "timingsvalue2";

    private static final String TIMINGS_VALUE_3 = "timingsvalue3";

    private static final String RESTAURANT_ADDRESS = "restaurantAddress";

    private String name;

    List<Resource> children;

    private String diningName;

    private String diningShortDesc;

    private String diningAddress;

    private String diningDescription;

    private String diningLongDescription;

    private String diningImage;

    private String diningPhoneNumber;

    private List<String> diningCuisine;

    private String dressCode;

    private String reviewRating;

    private String ticPoints;

    private String epicurePoints;

    private String averagePrice;

    private String taxDisclaimer;

    private String diningID;

    private String resourceURL;

    private String offerSpecifics;

    private String discountValue;

    private List<JSONObject> galleryImagePaths;

    private Integer numberOfImages;

    private String currencySymbol;

    private List<JSONObject> menuImagePaths;

    private String rateReview;

    private String diningPath;

    private String pathToDownloadFrom;

    private String timingslabel1;

    private String timingslabel2;

    private String timingslabel3;

    private String timingsvalue1;

    private String timingsvalue2;

    private String timingsvalue3;

    private String diningLongitude;

    private String diningLatitude;

    private String hotelArea;

    private String hotelCity;

    private String hotelPinCode;

    private String hotelCountry;

    private String restaurantAddress;

    private String diningBrand;


    @Override
    @PostConstruct
    public void init() {
        String methodName = " fetchProperties";
        LOG.trace("Method Entry: " + methodName);
        resource = request.getResource();
        LOG.debug("Received injected resource as:" + resource);
        LOG.debug("Received injected resource path as:" + resourcePath);

        if (resource.isResourceType(DINING_LIST_RESOURCE_TYPE) && resourcePath != "") {
            LOG.trace("Dining Card resources : " + resourceResolver.getResource(resourcePath).getPath());
            diningPath = resource.getParent().getPath();
            fetchProperties();
        } else {
            LOG.debug("Injected resource path is null ");

            diningPath = resource.getParent().getPath();
            fetchProperties();
        }
        LOG.trace("Method Exit: " + methodName);
    }

    private void fetchProperties() {
        String methodName = " fetchProperties";
        LOG.trace("Method Entry: " + methodName);
        try {
            Resource root = selectRootResource();
            if (null != root) {
                resourceURL = root.getPath().concat(".html");
                LOG.trace("The RESOURCE URL VALUE: " + resourceURL);
                if (root.getResourceType().equals(DINING_DETAILS_RESOURCETYPE)) {
                    initializeProperties(root);
                } else {
                    Resource child = resourceFetcherService.getChildrenOfType(root, DINING_DETAILS_RESOURCETYPE);
                    initializeProperties(child);
                }
            }
            LOG.trace("Method Exit: " + methodName);
        } catch (Exception e) {
            LOG.error("Error/Exception is thrown while fetching the dining model" + e.toString(), e);
        }
    }

    private Resource selectRootResource() {
        String methodName = " selectRootResource";
        LOG.trace("Method Entry: " + methodName);
        Resource selectedResource = null;
        try {
            if (resourcePath != null && !resourcePath.trim().isEmpty()) {
                ResourceResolver resolver = resolverFactory.getServiceResourceResolver(null);
                selectedResource = resolver.getResource(resourcePath);
            } else {
                selectedResource = resource;
            }
        } catch (LoginException e) {
            LOG.error("An error occurred while trying to login using service resolver to fetch resource.", e);
        }
        LOG.debug("Returning selected resource as: " + selectedResource);
        LOG.trace("Method Exit: " + methodName);
        return selectedResource;
    }

    private void initializeProperties(Resource resource) {
        String methodName = "initializeProperties";
        LOG.trace("Method Entry: " + methodName);
        try {

            Resource gallery = resource.getChild("gallery");
            if (gallery != null) {
                galleryImagePaths = galleryImageFetcher.getGalleryImagePathsAt(gallery);
                numberOfImages = galleryImagePaths.size();
            }
            LOG.debug("Fetched gallery image paths: " + galleryImagePaths);

            Resource viewMenuImages = resource.getChild("viewMenu");
            if (viewMenuImages != null) {
                menuImagePaths = galleryImageFetcher.getGalleryImagePathsAt(viewMenuImages);
            }
            LOG.debug("Fetched menu image paths: " + menuImagePaths);

            ValueMap properties = resource.getValueMap();

            diningName = properties.get(DINING_NAME, String.class);

            getRestaurantLocation(resource);

            diningShortDesc = properties.get(DINING_SHORT_DESC, String.class);

            diningDescription = properties.get(DINING_DESCRIPTION, String.class);

            diningLongDescription = properties.get(DINING_LONG_DESCRIPTION, String.class);

            diningImage = properties.get(IMAGE_PATH, String.class);

            diningPhoneNumber = properties.get(DINING_PHONE_NUMBER, String.class);

            String[] Cuisine = properties.get(DINING_CUISINE, String[].class);

            diningCuisine = getTagName(Cuisine);

            diningBrand = getBrandName(resource);

            timingslabel1 = properties.get(TIMINGS_LABEL_1, String.class);

            timingslabel2 = properties.get(TIMINGS_LABEL_2, String.class);

            timingslabel3 = properties.get(TIMINGS_LABEL_3, String.class);

            timingsvalue1 = properties.get(TIMINGS_VALUE_1, String.class);

            timingsvalue2 = properties.get(TIMINGS_VALUE_2, String.class);

            timingsvalue3 = properties.get(TIMINGS_VALUE_3, String.class);

            LOG.trace("First Values for timings : Label=" + timingslabel1 + " timings value = " + timingsvalue1);
            LOG.trace("Second Values for timings : Label=" + timingslabel2 + " timings value = " + timingsvalue2);

            dressCode = properties.get(DRESS_CODE, String.class);

            reviewRating = ratereviewFetcher();

            ticPoints = properties.get(TIC_POINTS, String.class);

            epicurePoints = properties.get(EPICURE_POINTS, String.class);

            String currencyValue = properties.get(CURRENCY_SYMBOL, String.class);

            currencySymbol = currencyFetcherService.getCurrencySymbolValue(currencyValue);

            LOG.trace("The Currency Symbol Value :::" + currencySymbol);

            averagePrice = properties.get(DINING_COST, String.class);

            LOG.trace("averagePrice :::" + averagePrice);

            taxDisclaimer = properties.get(TAX_DISCLAIMER, String.class);

            diningID = properties.get(DINING_ID, String.class);

            diningImage = properties.get(IMAGE_PATH, String.class);

            offerSpecifics = properties.get(OFFER_SPECIFICS, String.class);

            discountValue = properties.get(DISCOUNT_VALUE, String.class);

            pathToDownloadFrom = properties.get(PATH_TO_DOWNLOAD_FROM, String.class);

            restaurantAddress = properties.get(RESTAURANT_ADDRESS, String.class);

            LOG.trace("Authored restaurant address is : " + restaurantAddress);

            LOG.trace("Method Exit: " + methodName);
        } catch (Exception e) {
            LOG.error("Error/Exception is thrown while fetching dining list" + e.toString(), e);
        }
    }


    /**
     * @param resource2
     * @return
     */
    private String getBrandName(Resource restaurantResource) {
        String brand = "";
        try {
            Resource resourceHotel = restaurantResource.getParent().getParent().getParent().getParent().getParent();
            if (null != resourceHotel) {
                ValueMap parentHotelMap = resourceHotel.getChild("jcr:content").getValueMap();
                String brandTag = parentHotelMap.get("brand", String.class);
                String[] arr = brandTag.split("taj:hotels/brands/");
                brand = arr[1];
            }
        } catch (Exception e) {
            LOG.error("Exception found in getting Restaurant Brand", e.getMessage());
        }
        return brand;
    }

    /**
     * @param resource2
     *
     */
    private void getRestaurantLocation(Resource restaurantResource) {
        try {
            Resource resourceHotel = restaurantResource.getParent().getParent().getParent().getParent().getParent();
            if (null != resourceHotel) {
                ValueMap parentHotelMap = resourceHotel.getChild("jcr:content").getValueMap();
                diningLatitude = parentHotelMap.get("hotelLatitude", String.class);
                diningLongitude = parentHotelMap.get("hotelLongitude", String.class);
                hotelArea = parentHotelMap.get("hotelArea", String.class);
                hotelCity = parentHotelMap.get("hotelCity", String.class);
                hotelCountry = parentHotelMap.get("hotelCountry", String.class);
                hotelPinCode = parentHotelMap.get("hotelPinCode", String.class);
            }


        } catch (Exception e) {
            LOG.error("Exception found in getting RestaurantLocation", e.getMessage());
        }
    }

    public List<String> getTagName(String[] path) {
        LOG.trace("The String Path: " + path);
        List<String> pathValue = new ArrayList<>();
        if (path != null) {
            for (int i = 0; i < path.length; i++) {
                String tagPath = path[i].replace(":", "/");
                tagPath = "/etc/tags/" + tagPath;
                ResourceResolver resolver;
                try {
                    resolver = resolverFactory.getServiceResourceResolver(null);
                    Resource tagRoot = resolver.getResource(tagPath);
                    if (null != tagRoot) {
                        LOG.trace("The Tag Path Value: " + tagRoot.getPath());
                        ValueMap valueMap = tagRoot.adaptTo(ValueMap.class);
                        pathValue.add(valueMap.get("jcr:title", String.class));
                        LOG.trace("The Tag Value: " + pathValue);
                    }
                } catch (LoginException e) {
                    LOG.error("An error occurred while trying to login using service resolver to fetch resource.", e); // TODO
                }
            }
        }
        return pathValue;
    }

    private String ratereviewFetcher() {
        String methodName = " ratereviewFetcher";
        LOG.trace("Method Entry: " + methodName);
        String pagePathVal = null;
        try {
            if (resourceURL.contains("jcr:content")) {
                String[] pagePathArray = resourceURL.split("/jcr:content");
                pagePathVal = pagePathArray[0];
            } else {
                String[] pagePathArray = resourceURL.split(".html");
                pagePathVal = pagePathArray[0];
            }
            LOG.trace("The Page Path Value: " + pagePathVal);
            ResourceResolver resolver = resolverFactory.getServiceResourceResolver(null);
            Resource pagePathRes = resolver.getResource(pagePathVal);
            pagePathRes = pagePathRes.getChild("jcr:content");
            ValueMap valueMap = pagePathRes.adaptTo(ValueMap.class);
            rateReview = valueMap.get("rateReview", String.class);
            LOG.trace("The Rate Review Value : " + rateReview);
            LOG.trace("Method Exit: " + methodName);
        } catch (LoginException e) {
            LOG.error("An error occurred while trying to login using service resolver to fetch resource.", e);
        }
        return rateReview;
    }

    public Resource getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    public List<Resource> getChildren() {
        return children;
    }

    @Override
    public String getDiningName() {
        return diningName;
    }

    @Override
    public String getDiningShortDesc() {
        return diningShortDesc;
    }

    @Override
    public String getDiningAddress() {
        return diningAddress;
    }

    @Override
    public String getDiningDescription() {
        return diningDescription;
    }

    @Override
    public String getDiningLongDescription() {
        LOG.trace("-----------" + diningLongDescription);
        return diningLongDescription;
    }

    @Override
    public String getDiningImage() {
        return diningImage;
    }

    @Override
    public String getDiningPhoneNumber() {
        return diningPhoneNumber;
    }

    @Override
    public List<String> getDiningCuisine() {
        return diningCuisine;
    }

    @Override
    public String getDressCode() {
        return dressCode;
    }

    @Override
    public String getReviewRating() {
        return reviewRating;
    }

    @Override
    public String getTicPoints() {
        return ticPoints;
    }

    @Override
    public String getEpicurePoints() {
        return epicurePoints;
    }

    @Override
    public String getAveragePrice() {
        return averagePrice;
    }

    @Override
    public String getTaxDisclaimer() {
        return taxDisclaimer;
    }

    @Override
    public String getDiningID() {
        return diningID;
    }

    @Override
    public String getResourceURL() {
        return resourceURL;
    }

    @Override
    public String getOfferSpecifics() {
        return offerSpecifics;
    }

    @Override
    public String getDiscountValue() {
        return discountValue;
    }

    @Override
    public List<JSONObject> getMenuImagePaths() {
        return menuImagePaths;
    }

    /**
     * Getter for the field galleryImagePaths
     *
     * @return the galleryImagePaths
     */
    public List<JSONObject> getGalleryImagePaths() {
        return galleryImagePaths;
    }

    /**
     * Getter for the field numberOfImages
     *
     * @return the numberOfImages
     */
    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    /**
     * Getter for the field diningPath
     *
     * @return the path of the dining/restaurant
     */
    @Override
    public String getDiningPath() {
        return diningPath;
    }

    @Override
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    @Override
    public String getTimingsLabel1() {
        LOG.trace("Timings label 1: " + timingslabel1);
        return timingslabel1;
    }

    @Override
    public String getTimingsLabel2() {
        return timingslabel2;
    }

    @Override
    public String getTimingsValue1() {
        return timingsvalue1;
    }

    @Override
    public String getTimingsValue2() {
        return timingsvalue2;
    }

    @Override
    public String getPathToDownloadFrom() {
        return pathToDownloadFrom;
    }

    @Override
    public String getDiningLongitude() {
        return diningLongitude;
    }

    @Override
    public String getDiningLatitude() {
        return diningLatitude;
    }


    public String getTimingslabel3() {
        return timingslabel3;
    }

    public String getTimingsvalue3() {
        return timingsvalue3;
    }

    /**
     * Getter for the field hotelArea
     *
     * @return the hotelArea
     */
    @Override
    public String getHotelArea() {
        return hotelArea;
    }


    /**
     * Getter for the field hotelCity
     *
     * @return the hotelCity
     */
    @Override
    public String getHotelCity() {
        return hotelCity;
    }


    /**
     * Getter for the field hotelPinCode
     *
     * @return the hotelPinCode
     */
    @Override
    public String getHotelPinCode() {
        return hotelPinCode;
    }


    /**
     * Getter for the field hotelCountry
     *
     * @return the hotelCountry
     */
    @Override
    public String getHotelCountry() {
        return hotelCountry;
    }

    /**
     * Getter for the field restaurantAddress
     *
     * @return the restaurantAddress
     */
    @Override
    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    /**
     * Getter for the field diningBrand
     *
     * @return the diningBrand
     */
    @Override
    public String getDiningBrand() {
        return diningBrand;
    }

}
