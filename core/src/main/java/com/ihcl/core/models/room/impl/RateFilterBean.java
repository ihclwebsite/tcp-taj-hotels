/**
 *
 */
package com.ihcl.core.models.room.impl;


/**
 * @author sampathkumar
 *
 */
public class RateFilterBean {

    private String rateFilterDisplayName;

    private String rateFilterCode;


    /**
     * Getter for the field rateFilterDisplayName
     *
     * @return the rateFilterDisplayName
     */
    public String getRateFilterDisplayName() {
        return rateFilterDisplayName;
    }

    /**
     * Getter for the field rateFilterCode
     *
     * @return the rateFilterCode
     */
    public String getRateFilterCode() {
        return rateFilterCode;
    }

    /**
     * Setter for the field rateFilterCode
     *
     * @param rateFilterCode
     *            the rateFilterCode to set
     */

    public void setRateFilterCode(String rateFilterCode) {
        this.rateFilterCode = rateFilterCode;
    }

    /**
     * Setter for the field rateFilterDisplayName
     *
     * @param rateFilterDisplayName
     *            the rateFilterDisplayName to set
     */

    public void setRateFilterDisplayName(String rateFilterDisplayName) {
        this.rateFilterDisplayName = rateFilterDisplayName;
    }

}
