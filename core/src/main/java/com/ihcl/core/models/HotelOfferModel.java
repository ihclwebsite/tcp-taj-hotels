package com.ihcl.core.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class })
public class HotelOfferModel {

    /**
     *
     */
    private static final String TAJHOTELS_COMPONENTS_CONTENT_HOTEL_OFFER_LIST = "tajhotels/components/content/hotel-offer-list";

    private static final String TAJHOTELS_COMPONENTS_CONTENT_OFFER_DETAILS = "tajhotels/components/content/offer-details";

    private static final String TAJHOTELS_COMPONENTS_CONTENT_HOTEL_PARTICPATING_HOTEL = "tajhotels/components/content/participating-hotels";

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Inject
    SlingHttpServletRequest request;

    @Inject
    Resource resource;

    @Inject
    private Page currentPage;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    List<Resource> children;

    private String hotelPagePath;

    private String validityDate;

    private String offerDescription;

    private String offerName;

    private String specialOffer;

    private String offerType;

    private String offerImage;

    private String resourceURL;

    private int childDepth;

    @PostConstruct
    protected void init() {
        try {
            initializePagePathProperty();
            Resource root = resource.getChild("jcr:content/root");
            resourceURL = resource.getPath().concat(".html");
            LOG.debug("Constructed offer page resource URL as: " + resourceURL);
            Resource participatingHotelsResource = getParticipatingHotelsUnder(root);
            String listType = getListTypeFrom(participatingHotelsResource);
            if (listType.equalsIgnoreCase("static")) {
                processStaticListOption(root, participatingHotelsResource);
            } else if (listType.equalsIgnoreCase("children")) {
                processChildPagesOption(participatingHotelsResource);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }


    /**
     * @param participatingHotelsResource
     */
    private void processChildPagesOption(Resource participatingHotelsResource) {
        String methodName = "processChildPagesOption";
        LOG.trace("Method Entry: " + methodName);
        ValueMap valueMap = participatingHotelsResource.adaptTo(ValueMap.class);
        String parentPageProp = fetchPropertyFromValueMap("parentPage", valueMap, String.class);
        childDepth = fetchPropertyFromValueMap("childDepth" + "", valueMap, Integer.class);
        Resource parentParticipatingHotelResource = getResourceAtPath(parentPageProp);
        int currentChildLevel = 0;
        fetchPropertiesUnder(parentParticipatingHotelResource, currentChildLevel);
        LOG.trace("Method Exit: " + methodName);

    }


    /**
     * @param root
     * @param participatingHotelsResource
     */
    private void processStaticListOption(Resource root, Resource participatingHotelsResource) {
        String methodName = "processStaticListOption";
        LOG.trace("Method Entry: " + methodName);
        String[] particpatingHotelPages = getParticipatingHotelsFrom(participatingHotelsResource);
        for (String participatingHotelPage : particpatingHotelPages) {
            if (participatingHotelPage.equals(hotelPagePath)) {
                initializePropertiesFromRootOf(resource);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */
    private void initializePagePathProperty() {
        LOG.debug("currentPage resource:" + currentPage.getContentResource());
        Resource currentPageResource = currentPage.getContentResource();
        Resource currentPageRoot = currentPageResource.getChild("root");
        LOG.debug("currentPageRoot :::" + currentPageRoot);
        fetchPagePathProperty(currentPageRoot);
    }

    /**
     * @param currentResource
     * @param currentChildLevel
     */
    private void fetchPropertiesUnder(Resource currentResource, int currentChildLevel) {
        String methodName = "fetchPropertiesUnder";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Received current child level as: " + currentChildLevel);
        Iterable<Resource> children = currentResource.getChildren();
        LOG.debug("Received iterable of resource as: " + children);
        for (Resource childResource : children) {
            if (currentChildLevel < childDepth) {
                LOG.debug("Processing resource: " + childResource);
                String resourcePath = childResource.getPath();
                LOG.debug("Comparing: " + resourcePath + " with hotel page path: " + hotelPagePath);
                if (resourcePath.equals(hotelPagePath)) {
                    LOG.debug("Processing resource: " + childResource);
                    initializePropertiesFromRootOf(resource);
                }
                fetchPropertiesUnder(childResource, currentChildLevel + 1);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param resource
     */
    private void initializePropertiesFromRootOf(Resource resource) {
        String methodName = "initializePropertiesFromRootOf";
        LOG.trace("Method Entry: " + methodName);
        Resource rootResource = resource.getChild(JcrConstants.JCR_CONTENT + "/" + "root");
        LOG.debug("Attempting to iterate over children of: " + rootResource);
        Iterable<Resource> children = rootResource.getChildren();
        for (Resource childResource : children) {
            String resourceType = childResource.getResourceType();
            LOG.debug(
                    "Comparing resource type: " + resourceType + " to: " + TAJHOTELS_COMPONENTS_CONTENT_OFFER_DETAILS);
            if (resourceType.equals(TAJHOTELS_COMPONENTS_CONTENT_OFFER_DETAILS)) {
                ValueMap offerDetailValueMap = childResource.adaptTo(ValueMap.class);
                initializePropertiesFromOfferDetails(offerDetailValueMap);
            }
        }
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @param offerDetailValueMap
     */
    private void initializePropertiesFromOfferDetails(ValueMap offerDetailValueMap) {
        String methodName = "initializePropertiesFromOfferDetails";
        LOG.trace("Method Entry: " + methodName);
        offerType = fetchPropertyFromValueMap("offerType", offerDetailValueMap, String.class);
        offerName = fetchPropertyFromValueMap("offerName", offerDetailValueMap, String.class);
        validityDate = fetchPropertyFromValueMap("validityDate", offerDetailValueMap, String.class);
        specialOffer = fetchPropertyFromValueMap("specialOffer", offerDetailValueMap, String.class);
        offerImage = fetchPropertyFromValueMap("offerImage", offerDetailValueMap, String.class);
        offerDescription = fetchPropertyFromValueMap("offerDescription", offerDetailValueMap, String.class);
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * @param parentPageProp
     * @return
     */
    private Resource getResourceAtPath(String parentPageProp) {
        LOG.debug("Attempting to fetch request resolver");
        ResourceResolver resourceResolver = request.getResourceResolver();
        LOG.debug("Received request resolver as: " + resourceResolver);
        LOG.debug("Attempting to fetch parent page resource");
        Resource parentPageResource = resourceResolver.getResource(parentPageProp);
        LOG.debug("Received parent page resource as: " + parentPageResource);
        return parentPageResource;
    }


    /**
     * @param resource
     * @return
     */
    private String[] getParticipatingHotelsFrom(Resource resource) {
        String[] participatingHotels;
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        participatingHotels = fetchPropertyFromValueMap("pages", valueMap, String[].class);
        return participatingHotels;
    }

    /**
     * @param resource
     * @return
     */
    private String getListTypeFrom(Resource resource) {
        String listType = "";
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        listType = fetchPropertyFromValueMap("listFrom", valueMap, String.class);
        return listType;
    }

    /**
     * @param listType
     * @param valueMap
     * @param key
     * @return
     */
    private <T> T fetchPropertyFromValueMap(String key, ValueMap valueMap, Class<T> type) {
        T value = null;
        if (valueMap.containsKey(key)) {
            value = valueMap.get(key, type);
        }
        return value;
    }

    /**
     * @param root
     * @return
     */
    private Resource getParticipatingHotelsUnder(Resource root) {
        Resource participatingHotelResource = null;
        Iterable<Resource> children = root.getChildren();
        for (Resource child : children) {
            if (child.getResourceType().equals(TAJHOTELS_COMPONENTS_CONTENT_HOTEL_PARTICPATING_HOTEL)) {
                participatingHotelResource = child;
            }
        }
        return participatingHotelResource;
    }

    /**
     * @param currentPageRoot
     */
    private void fetchPagePathProperty(Resource currentPageRoot) {
        Iterable<Resource> currentRootChildren = currentPageRoot.getChildren();
        String resourceType;
        ValueMap valueMap;
        for (Resource resource : currentRootChildren) {
            resourceType = resource.getResourceType();
            if (resourceType.equals(TAJHOTELS_COMPONENTS_CONTENT_HOTEL_OFFER_LIST)) {
                valueMap = resource.adaptTo(ValueMap.class);
                hotelPagePath = valueMap.get("pagePath", String.class);
                LOG.debug("Received hotelPagePath as: " + hotelPagePath);
            }
        }
    }


    public String getValidityDate() {
        return validityDate;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getSpecialOffer() {
        return specialOffer;
    }

    public String getOfferType() {
        return offerType;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public String getResourceURL() {
        return resourceURL;
    }

    public String getHotelPagePath() {
        return hotelPagePath;
    }
}
