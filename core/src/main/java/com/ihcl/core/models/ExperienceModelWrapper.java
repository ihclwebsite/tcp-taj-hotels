package com.ihcl.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.ihcl.core.models.experiences.ExperiencesModel;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })

public class ExperienceModelWrapper {

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	@Optional
	private String resourcePath;

	private ExperiencesModel experiencesModel;

	@PostConstruct
	public void init() {
		resourcePath = resourcePath + "/jcr:content";
		Resource resource = resourceResolver.resolve(resourcePath);
		experiencesModel = resource.adaptTo(ExperiencesModel.class);
	}

	public ExperiencesModel getExperiencesModel() {
		return experiencesModel;
	}
}
