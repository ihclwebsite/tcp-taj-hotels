package com.ihcl.core.models.list;


import com.day.cq.wcm.api.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class)
public class OfferListModel  {

    private static final Logger LOG = LoggerFactory.getLogger(OfferListModel.class);

    @Inject
    private Page currentPage;

    @Inject
    ResourceResolver resourceResolver;

    private String holidayPropertyFlag = "searchKey";

    private String holidayPropertyFlagValue = "holidayPackages";

    private List<OfferListModelBean> offerModelDetail = new ArrayList<OfferListModelBean>();


    public List<OfferListModelBean> getOfferCardDetails() {
        Page hotelOfferListPage = currentPage;
        if (null != hotelOfferListPage) {
            Iterator<Page> childPageList = hotelOfferListPage.listChildren();
            while (childPageList.hasNext()) {
                Page childPage = childPageList.next();
                OfferListModelBean offerDetails = new OfferListModelBean();
                if(isHolidayOffer(childPage)) {
                    getHolidayOfferDetails(childPage, offerDetails);
                } else {
                    getHotelOfferDetails(childPage, offerDetails);
                }
            }
        }
        return offerModelDetail;
    }

    public void getHolidayOfferDetails(Page childPage, OfferListModelBean offerDetails) {
        try {
            Node childPageNode = childPage.adaptTo(Node.class);
            Node referenceNode = childPageNode.getNode("jcr:content/root/reference_holidays");
            String referencePath = referenceNode.getProperty("path").getValue().toString();
            offerDetails.setOfferPageURL(childPage.getPath());
            offerDetails.setNoOfNights(getHolidayOfferPropertyValue(referencePath, "nights"));
            offerDetails.setOfferRateCode(getHolidayOfferPropertyValue(referencePath, "offerRateCode"));
            offerDetails.setOfferStartsFrom(getHolidayOfferPropertyValue(referencePath, "packageValidityStartDate"));
            offerDetails.setComparableOfferRateCode(getHolidayOfferPropertyValue(referencePath,"comparableOfferRateCode"));
            offerDetails.setOfferDescription(getHolidayOfferPropertyValue(referencePath, "packageDescription"));
            offerDetails.setOfferImagePath(getImagePath(childPage));
            offerDetails.setOfferTitle(getHolidayOfferPropertyValue(referencePath, "packageTitle"));
            offerDetails.setOfferValidity(getHolidayOfferPropertyValue(referencePath, "packageValidityEndDate"));
            offerModelDetail.add(offerDetails);
        } catch (RepositoryException e) {
            LOG.error("Repository exception in getHolidayOfferDetails : {}", e);
            LOG.debug("Repository exception in getHolidayOfferDetails : {}", e.getMessage());
        }
    }

    public void getHotelOfferDetails(Page childPage, OfferListModelBean offerDetails) {
        try {
            offerDetails.setOfferPageURL(childPage.getPath());
            offerDetails.setOfferDescription(getHotelOfferPropertyValue(childPage, "offerShortDesc"));
            offerDetails.setOfferImagePath(getImagePath(childPage));
            offerDetails.setOfferTitle(getHotelOfferPropertyValue(childPage, "offerTitle"));
            offerDetails.setOfferValidity(getOfferValidity(childPage, "validity", "jcr:content/root/offerdetails"));
            offerModelDetail.add(offerDetails);
        } catch (Exception e) {
            LOG.error("Exception in getHotelOfferDetails : {}", e);
            LOG.debug("Exception in getHotelOfferDetails : {}", e.getMessage());
        }
    }

    public String getImagePath(Page childPage){
        String imagePath = "";
        String bannerImagePath;
        String packageImagePath;
        try {
            Node childPageNode = childPage.adaptTo(Node.class);
            if(childPageNode.hasProperty("jcr:content/bannerImage")) {
                bannerImagePath = childPageNode.getProperty("jcr:content/bannerImage").getValue().toString();
                imagePath = bannerImagePath;
            }
            if(childPageNode.hasProperty("jcr:content/packageImagePath")) {
                packageImagePath = childPageNode.getProperty("jcr:content/packageImagePath").getValue().toString();
                imagePath = packageImagePath;
            }
        } catch (RepositoryException e) {
            LOG.error("Repository exception in getImagePath : {}", e);
            LOG.debug("Repository exception in getImagePath : {}", e.getMessage());
        }
        return imagePath;
    }

    public String getHotelOfferPropertyValue(Page childPage, String propertyName){
        String propertyValue = "";
        try {
            Node childPageNode = childPage.adaptTo(Node.class);
            if(childPageNode.hasProperty("jcr:content/" + propertyName)){
                Property hotelProperty = childPageNode.getProperty("jcr:content/" + propertyName);
                String property = hotelProperty.getValue().toString();
                if (null != property) {
                    propertyValue = property;
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Repository exception in getHotelOfferPropertyValue : {}", e);
            LOG.debug("Repository exception in getHotelOfferPropertyValue : {}", e.getMessage());
        } catch (Exception e) {
            if(e.getClass().isInstance(PathNotFoundException.class) || e.getClass().isInstance(com.day.cq.replication.PathNotFoundException.class))
            LOG.error("PathNotFoundException in getHotelOfferPropertyValue : {}", e);
            LOG.debug("PathNotFoundException in getHotelOfferPropertyValue : {}", e.getMessage());
        }
        return propertyValue;
    }

    public String getHolidayOfferPropertyValue(String referencePath, String propertyName) {
        String propertyValue = "";
        try {
            if (null != resourceResolver) {
                Resource holidayComponentResource = resourceResolver.getResource(referencePath);
                if (null != holidayComponentResource) {
                    Node referenceNode = holidayComponentResource.adaptTo(Node.class);
                    if (referenceNode.hasProperty(propertyName)) {
                        String property = referenceNode.getProperty(propertyName).getValue().toString();
                        if (null != property) {
                            propertyValue = property;
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Repository exception in getHolidayOfferPropertyValue : {}", e);
            LOG.debug("Repository exception in getHolidayOfferPropertyValue : {}", e.getMessage());
        }
        return propertyValue;
    }

    public boolean isHolidayOffer(Page childPage) {
        boolean isHoliday = false;
        try {
            String holidayCheckKey = getHotelOfferPropertyValue(childPage, holidayPropertyFlag);
            if (null != holidayCheckKey) {
                if (StringUtils.equalsIgnoreCase(holidayCheckKey, holidayPropertyFlagValue)) {
                    isHoliday = true;
                }
            }
        } catch (Exception e) {
            LOG.error("Exception in isHolidayOffer : {}", e);
            LOG.debug("Exception in isHolidayOffer : {}", e.getMessage());
        }
        return isHoliday;
    }

    public String getOfferValidity(Page childPage, String propertyName, String nodePath) {
        String offerValidity = "";
        try {
            Node childPageNode = childPage.adaptTo(Node.class);
            if (childPageNode.getNode(nodePath).hasProperty(propertyName)) {
                String offerDetailsNode = childPageNode.getNode(nodePath).getProperty(propertyName).getValue().toString();
                if (null != offerDetailsNode) {
                    offerValidity = offerDetailsNode;
                }
            }
        } catch (RepositoryException e) {
            LOG.error("Repository exception in getOfferValidity : {}", e);
            LOG.debug("Repository exception in getOfferValidity : {} :: Path : {} ", e.getMessage(), childPage.getPath());
        }
        return offerValidity;
    }
}
