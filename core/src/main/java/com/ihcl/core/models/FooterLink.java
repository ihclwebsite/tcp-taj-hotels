package com.ihcl.core.models;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class FooterLink {

    @Inject
    @Optional
    @Named("linkLabel")
    public String label;

    @Inject
    @Optional
    @Named("linkUrl")
    public String path;

    @Inject
    @Optional
    @Named("cssClassName")
    public String cssClassName;

    @Inject
    @Optional
    @Named("linkNofollow")
    public String nofollow;

    @Inject
    @Optional
    @Named("policylinkNofollow")
    public String policynofollow;

    @Inject
    @Optional
    @Named("brandLinkNofollow")
    public String brandnofollow;

    @Inject
    @Optional
    @Named("homeLinkNofollow")
    public String ihclHomeLinkNofollow;

    @Inject
    @Optional
    @Named("ourBrandLinkNofollow")
    public String ihclOurBrandLinkNofollow;

    @Inject
    @Optional
    @Named("companyLinkNofollow")
    public String ihclCompanyLinkNofollow;

    @Inject
    @Optional
    @Named("investorsLinkNofollow")
    public String ihclInvestorsLinkNofollow;

    @Inject
    @Optional
    @Named("developmentLinkNofollow")
    public String ihclDevelopmentLinkNofollow;

    @Inject
    @Optional
    @Named("responsibilityLinkNofollow")
    public String ihclResponsibilityLinkNofollow;

    @Inject
    @Optional
    @Named("careerLinkNofollow")
    public String ihclCareerLinkNofollow;

    @Inject
    @Optional
    @Named("newsRoomLinkNofollow")
    public String ihclNewsRoomLinkNofollow;

    @Inject
    @Optional
    @Named("contactLinkNofollow")
    public String ihclContactLinkNofollow;

    @Inject
    @Optional
    @Named("footerLinkNofollow")
    public String ihclFooterLinkNofollow;

    @Inject
    @Optional
    @Named("ihclPolicyLinkNofollow")
    public String ihclPolicyLinkNofollow;

    @Inject
    @Optional
    @Named("ihclBrandLinkNofollow")
    public String ihclBrandLinkNofollow;


    @Inject
    @Optional
    @Named("externalUrl")
    public String externalUrl;


    public String getExternalUrl() {
        return externalUrl;
    }

    public String getLabel() {
        return label;
    }

    public String getPath() {
        return path;
    }

    public String getCssClassName() {
        return cssClassName;
    }

    public String getNofollow() {
        return nofollow;
    }

    public String getPolicyNofollow() {
        return policynofollow;
    }

    public String getBrandLinkNofollow() {
        return brandnofollow;
    }

    public String getHomeLinkNofollow() {
        return ihclHomeLinkNofollow;
    }

    public String getOurBrandLinkNofollow() {
        return ihclOurBrandLinkNofollow;
    }

    public String getCompanyLinkNofollow() {
        return ihclCompanyLinkNofollow;
    }

    public String getInvestorsLinkNofollow() {
        return ihclInvestorsLinkNofollow;
    }

    public String getDevelopmentLinkNofollow() {
        return ihclDevelopmentLinkNofollow;
    }

    public String getResponsibilityLinkNofollow() {
        return ihclResponsibilityLinkNofollow;
    }

    public String getCareerLinkNofollow() {
        return ihclCareerLinkNofollow;
    }

    public String getNewsRoomLinkNofollow() {
        return ihclNewsRoomLinkNofollow;
    }

    public String getContactLinkNofollow() {
        return ihclContactLinkNofollow;
    }

    public String getFooterLinkNofollow() {
        return ihclFooterLinkNofollow;
    }

    public String getIhclPolicyLinkNofollow() {
        return ihclPolicyLinkNofollow;
    }

    public String getIhclBrandLinkNofollow() {
        return ihclBrandLinkNofollow;
    }
}
