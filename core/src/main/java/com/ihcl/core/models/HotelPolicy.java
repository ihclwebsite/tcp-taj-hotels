package com.ihcl.core.models;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;

public class HotelPolicy extends WCMUsePojo {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public List<String> policyList = new ArrayList<String>();


    public List<String> getPolicyList() {
        return policyList;
    }

    @Override
    public void activate() throws Exception {
        Node currentNode = getResource().adaptTo(Node.class);
        NodeIterator ni = currentNode.getNodes();
        while (ni.hasNext()) {
            Node child = ni.nextNode();
            NodeIterator ni2 = child.getNodes();
            setPolicyList(ni2);
        }
    }

    public void setPolicyList(NodeIterator ni2) {
        try {
            while (ni2.hasNext()) {
                Node grandChild = ni2.nextNode();
                log.info("*** GRAND CHILD NODE PATH IS " + grandChild.getPath());
                if (grandChild.hasProperty("hotelPoliciesDescription"))
                    policyList.add(grandChild.getProperty("hotelPoliciesDescription").getString());
            }
        } catch (Exception e) {
            log.error("Exception while Multifield data {}", e.getMessage(), e);
        }
    }
}
