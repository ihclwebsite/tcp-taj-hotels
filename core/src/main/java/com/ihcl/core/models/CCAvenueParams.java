/**
 *
 */
package com.ihcl.core.models;


/**
 * @author Vijay Chikkani
 *
 */
public class CCAvenueParams {

    String merchantId;

    String orderId;

    String trackingId;

    String currency;

    String amount;

    String ccAvenueUrl;

    String redirectUrl;

    String cancelUrl;

    String ccAvenueWorkingKey;

    String ccAvenueAccessKey;

    String billingName;

    String billingAddress;

    String billingCity;

    String billingState;

    String billingZip;

    String billingCountry;

    String billingTel;

    String billingEmail;

    String deliveryName;

    String deliveryAddress;

    String deliveryCity;

    String deliveryState;

    String deliveryZip;

    String deliveryCountry;

    String deliveryTel;

    String merchantParam1;

    String merchantParam2;

    String merchantParam3;

    String merchantParam4;

    String merchantParam5;

    String subAccountId;

    boolean paymentstatus;


    public String getMerchantId() {
        return merchantId;
    }


    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }


    public String getOrderId() {
        return orderId;
    }


    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    /**
     * Getter for the field trackingId
     *
     * @return the trackingId
     */
    public String getTrackingId() {
        return trackingId;
    }


    /**
     * Setter for the field trackingId
     *
     * @param trackingId
     *            the trackingId to set
     */

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getAmount() {
        return amount;
    }


    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getCcAvenueUrl() {
        return ccAvenueUrl;
    }


    public void setCcAvenueUrl(String ccAvenueUrl) {
        this.ccAvenueUrl = ccAvenueUrl;
    }


    public String getRedirectUrl() {
        return redirectUrl;
    }


    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }


    public String getCancelUrl() {
        return cancelUrl;
    }


    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }


    public String getCcAvenueWorkingKey() {
        return ccAvenueWorkingKey;
    }


    public void setCcAvenueWorkingKey(String ccAvenueWorkingKey) {
        this.ccAvenueWorkingKey = ccAvenueWorkingKey;
    }


    public String getCcAvenueAccessKey() {
        return ccAvenueAccessKey;
    }


    /**
     * @param ccAvenueAccessKey
     */
    public void setCcAvenueAccessKey(String ccAvenueAccessKey) {
        this.ccAvenueAccessKey = ccAvenueAccessKey;
    }


    /**
     * Getter for the field billingName
     *
     * @return the billingName
     */
    public String getBillingName() {
        return billingName;
    }


    /**
     * Setter for the field billingName
     *
     * @param billingName
     *            the billingName to set
     */

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }


    /**
     * Getter for the field billingAddress
     *
     * @return the billingAddress
     */
    public String getBillingAddress() {
        return billingAddress;
    }


    /**
     * Setter for the field billingAddress
     *
     * @param billingAddress
     *            the billingAddress to set
     */

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }


    /**
     * Getter for the field billingCity
     *
     * @return the billingCity
     */
    public String getBillingCity() {
        return billingCity;
    }


    /**
     * Setter for the field billingCity
     *
     * @param billingCity
     *            the billingCity to set
     */

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }


    /**
     * Getter for the field billingState
     *
     * @return the billingState
     */
    public String getBillingState() {
        return billingState;
    }


    /**
     * Setter for the field billingState
     *
     * @param billingState
     *            the billingState to set
     */

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }


    /**
     * Getter for the field billingZip
     *
     * @return the billingZip
     */
    public String getBillingZip() {
        return billingZip;
    }


    /**
     * Setter for the field billingZip
     *
     * @param billingZip
     *            the billingZip to set
     */

    public void setBillingZip(String billingZip) {
        this.billingZip = billingZip;
    }


    /**
     * Getter for the field billingCountry
     *
     * @return the billingCountry
     */
    public String getBillingCountry() {
        return billingCountry;
    }


    /**
     * Setter for the field billingCountry
     *
     * @param billingCountry
     *            the billingCountry to set
     */

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }


    /**
     * Getter for the field billingTel
     *
     * @return the billingTel
     */
    public String getBillingTel() {
        return billingTel;
    }


    /**
     * Setter for the field billingTel
     *
     * @param billingTel
     *            the billingTel to set
     */

    public void setBillingTel(String billingTel) {
        this.billingTel = billingTel;
    }


    /**
     * Getter for the field billingEmail
     *
     * @return the billingEmail
     */
    public String getBillingEmail() {
        return billingEmail;
    }


    /**
     * Setter for the field billingEmail
     *
     * @param billingEmail
     *            the billingEmail to set
     */

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }


    /**
     * Getter for the field deliveryName
     *
     * @return the deliveryName
     */
    public String getDeliveryName() {
        return deliveryName;
    }


    /**
     * Setter for the field deliveryName
     *
     * @param deliveryName
     *            the deliveryName to set
     */

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }


    /**
     * Getter for the field deliveryAddress
     *
     * @return the deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }


    /**
     * Setter for the field deliveryAddress
     *
     * @param deliveryAddress
     *            the deliveryAddress to set
     */

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }


    /**
     * Getter for the field deliveryCity
     *
     * @return the deliveryCity
     */
    public String getDeliveryCity() {
        return deliveryCity;
    }


    /**
     * Setter for the field deliveryCity
     *
     * @param deliveryCity
     *            the deliveryCity to set
     */

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }


    /**
     * Getter for the field deliveryState
     *
     * @return the deliveryState
     */
    public String getDeliveryState() {
        return deliveryState;
    }


    /**
     * Setter for the field deliveryState
     *
     * @param deliveryState
     *            the deliveryState to set
     */

    public void setDeliveryState(String deliveryState) {
        this.deliveryState = deliveryState;
    }


    /**
     * Getter for the field deliveryZip
     *
     * @return the deliveryZip
     */
    public String getDeliveryZip() {
        return deliveryZip;
    }


    /**
     * Setter for the field deliveryZip
     *
     * @param deliveryZip
     *            the deliveryZip to set
     */

    public void setDeliveryZip(String deliveryZip) {
        this.deliveryZip = deliveryZip;
    }


    /**
     * Getter for the field deliveryCountry
     *
     * @return the deliveryCountry
     */
    public String getDeliveryCountry() {
        return deliveryCountry;
    }


    /**
     * Setter for the field deliveryCountry
     *
     * @param deliveryCountry
     *            the deliveryCountry to set
     */

    public void setDeliveryCountry(String deliveryCountry) {
        this.deliveryCountry = deliveryCountry;
    }


    /**
     * Getter for the field deliveryTel
     *
     * @return the deliveryTel
     */
    public String getDeliveryTel() {
        return deliveryTel;
    }


    /**
     * Setter for the field deliveryTel
     *
     * @param deliveryTel
     *            the deliveryTel to set
     */

    public void setDeliveryTel(String deliveryTel) {
        this.deliveryTel = deliveryTel;
    }


    public String getMerchantParam1() {
        return merchantParam1;
    }


    public void setMerchantParam1(String merchantParam1) {
        this.merchantParam1 = merchantParam1;
    }


    public String getMerchantParam2() {
        return merchantParam2;
    }


    public void setMerchantParam2(String merchantParam2) {
        this.merchantParam2 = merchantParam2;
    }


    public String getMerchantParam3() {
        return merchantParam3;
    }


    public void setMerchantParam3(String merchantParam3) {
        this.merchantParam3 = merchantParam3;
    }


    public String getMerchantParam4() {
        return merchantParam4;
    }


    public void setMerchantParam4(String merchantParam4) {
        this.merchantParam4 = merchantParam4;
    }


    public String getMerchantParam5() {
        return merchantParam5;
    }


    public void setMerchantParam5(String merchantParam5) {
        this.merchantParam5 = merchantParam5;
    }


    public String getSubAccountId() {
        return subAccountId;
    }


    public void setSubAccountId(String subAccountId) {
        this.subAccountId = subAccountId;
    }


    public boolean getPaymentstatus() {
        return paymentstatus;
    }


    public void setPaymentstatus(boolean paymentstatus) {
        this.paymentstatus = paymentstatus;
    }

}
