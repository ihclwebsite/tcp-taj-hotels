package com.ihcl.core.models;

import java.util.List;
import java.util.Map;

import com.ihcl.core.models.destination.OurHotelDestinationBean;

public interface DestinationsList {

    void getAllDestinations();

    List<String> getDestinations();

    Map<String, String> getHotelTypes();

    Map<String, String> getCountryMap();
    
    List<OurHotelDestinationBean> getDestinationHotelsBean();
	
    List<OurHotelDestinationBean> getTicHotelsBean();

	List<OurHotelDestinationBean> getTajholidaysTicHotelsList();

	List<OurHotelDestinationBean> getTicTapMeHotelsList();

	List<OurHotelDestinationBean> getTicTapHotelsList();

	List<OurHotelDestinationBean> getTicRoomRedemptionList();
    

}
