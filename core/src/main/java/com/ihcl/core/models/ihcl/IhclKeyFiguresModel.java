/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(adaptables = Resource.class,
        adapters = IhclKeyFiguresModel.class)

public class IhclKeyFiguresModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclKeyFiguresModel.class);

    @Self
    private Resource resource;

    public List<KeyFiguresBean> keyFiguresList = new ArrayList<KeyFiguresBean>();

    /**
     * Getter for the field keyFiguresList
     *
     * @return the keyFiguresList
     */
    public List<KeyFiguresBean> getKeyFiguresList() {
        return keyFiguresList;
    }


    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getfiguresValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     *
     */

    public List<KeyFiguresBean> getfiguresValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] figures = valueMap.get("tabs", String[].class);
        JsonObject jsonObject;
        Gson gson = new Gson();
        if (null != figures) {
            for (int i = 0; i < figures.length; i++) {
                JsonElement jsonElement = gson.fromJson(figures[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                KeyFiguresBean keyFiguresBean = new KeyFiguresBean();
                LOG.trace("jsonObject :" + jsonObject);

                if (null != jsonObject.get("numberkeys")) {
                    keyFiguresBean.setNumberKeys(jsonObject.get("numberkeys").getAsString());
                }

                if (null != jsonObject.get("currency")) {
                    keyFiguresBean.setCurrency(jsonObject.get("currency").getAsString());
                }

                if (null != jsonObject.get("entity")) {
                    keyFiguresBean.setEntity(jsonObject.get("entity").getAsString());
                }
                LOG.trace("test the bean::" + keyFiguresBean.getNumberKeys() + "::" + keyFiguresBean.getCurrency()
                        + "::" + keyFiguresBean.getEntity());
                keyFiguresList.add(keyFiguresBean);
                LOG.trace("keyFiguresList :" + keyFiguresList.get(0).getNumberKeys()
                        + keyFiguresList.get(0).getCurrency() + keyFiguresList.get(0).getEntity());
            }

        }
        return keyFiguresList;
    }
}
