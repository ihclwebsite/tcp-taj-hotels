package com.ihcl.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class Faq {
    @Inject
    private List<Issue> questions;

    public List<Issue> getQuestions() {
        return questions;
    }
}
