/**
 *
 */
package com.ihcl.core.models.ihcl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Model(
        adaptables = Resource.class,
        adapters = IhclOfficeCardModel.class)

public class IhclOfficeCardModel {

    private static final Logger LOG = LoggerFactory.getLogger(IhclOfficeCardModel.class);

    @Self
    private Resource resource;

    private String CardId;


    public List<IhclOfficeCardBean> cardList = new ArrayList<IhclOfficeCardBean>();


    /**
     * Getter for the field cardList
     *
     * @return the cardList
     */
    public List<IhclOfficeCardBean> getCardList() {
        return cardList;
    }

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        getCardValues();
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * @return
     *
     */
    private List<IhclOfficeCardBean> getCardValues() {
        // TODO Auto-generated method stub
        LOG.trace("resource is ihcl cards::" + resource);
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String[] cards = valueMap.get("cardlist", String[].class);
        JsonObject jsonObject;
        LOG.trace("card is::" + cards);
        Gson gson = new Gson();
        if (null != cards) {
            for (int i = 0; i < cards.length; i++) {
                JsonElement jsonElement = gson.fromJson(cards[i], JsonElement.class);
                jsonObject = jsonElement.getAsJsonObject();
                IhclOfficeCardBean cardListBean = new IhclOfficeCardBean();
                LOG.trace("jsonObject :" + jsonObject);
                if (null != jsonObject.get("title")) {
                    cardListBean.setTitle(jsonObject.get("title").getAsString());
                }
                if (null != jsonObject.get("description")) {
                    cardListBean.setDescription(jsonObject.get("description").getAsString());
                }
                if (null != jsonObject.get("phoneNumber")) {
                    cardListBean.setPhoneNumber(jsonObject.get("phoneNumber").getAsString());
                }
                if (null != jsonObject.get("emailId")) {
                    cardListBean.setEmailId(jsonObject.get("emailId").getAsString());
                }
                if (null != jsonObject.get("fax")) {
                    cardListBean.setFax(jsonObject.get("fax").getAsString());
                }
                cardList.add(cardListBean);
            }

        }
        LOG.trace("cards list data ::: " + cardList);
        return cardList;
    }

    /**
     * @return the card id
     */
    public String getCardId() {
        return CardId;
    }
}

