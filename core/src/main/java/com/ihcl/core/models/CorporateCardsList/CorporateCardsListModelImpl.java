package com.ihcl.core.models.CorporateCardsList;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ihcl.core.models.CorporateCardsListModel;
import com.ihcl.core.models.ihcl.KeyFiguresBean;
import com.ihcl.core.util.services.TajCommonsFetchResource;

@Model(adaptables = SlingHttpServletRequest.class,
        adapters = CorporateCardsListModel.class)
public class CorporateCardsListModelImpl implements CorporateCardsListModel {

    private static final Logger LOG = LoggerFactory.getLogger(CorporateCardsListModelImpl.class);

    @Inject
    SlingHttpServletRequest request;

    @Inject
    TajCommonsFetchResource tajCommonsFetchResource;

    private Resource resource;

    ValueMap valueMap;

    List<KeyFiguresBean> keyFiguresList = new ArrayList<KeyFiguresBean>();

    @PostConstruct
    public void init() {
      
        LOG.trace(" Method Entry : init()");
        resource = request.getResource();
        LOG.trace("resource Path In corporate list cards  Model : " + resource.getPath());
        if (tajCommonsFetchResource != null) {
            valueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "corporate_card_detai",
                    "tajhotels/components/content/corporate-card-details");
            LOG.trace("valuemap from individual Corporate cards list  :" + valueMap);
            ValueMap keyFigureValueMap = tajCommonsFetchResource.getChildTypeAsValueMapUpto(resource, "ihclkeyfigures",
                    "tajhotels/components/content/ihcl/ihclkeyfigures");
            if(null !=keyFigureValueMap){
            String[] listKeyfigures = keyFigureValueMap.get("tabs", String[].class);
            JsonObject jsonObject;
            Gson gson = new Gson();
            if (null != listKeyfigures) {
                for (int i = 0; i < listKeyfigures.length; i++) {
                    JsonElement jsonElement = gson.fromJson(listKeyfigures[i], JsonElement.class);
                    jsonObject = jsonElement.getAsJsonObject();
                    KeyFiguresBean keyFiguresBean = new KeyFiguresBean();
                    LOG.trace("jsonObject :" + jsonObject);


                    if (null != jsonObject.get("numberkeys")) {
                        keyFiguresBean.setNumberKeys(jsonObject.get("numberkeys").getAsString());
                    }

                    if (null != jsonObject.get("currency")) {
                        keyFiguresBean.setCurrency(jsonObject.get("currency").getAsString());
                    }

                    if (null != jsonObject.get("entity")) {
                        keyFiguresBean.setEntity(jsonObject.get("entity").getAsString());
                    }

                    keyFiguresList.add(keyFiguresBean);

                }

            }
            }
            LOG.trace("keyFigureValueList from individual Corporate cards list  :" + keyFiguresList);
        } else {
            LOG.trace("tajCommonsFetchResource is null :");

        }
        LOG.trace(" Method Exit : init()");
    }

    @Override
    public ValueMap getValueMap() {
        LOG.trace(" Method Entry : getvalueMap()");
        return valueMap;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.CorporateCardsListModel#getKeyFiguresList()
     */
    @Override
    public List<KeyFiguresBean> getKeyFiguresList() {
        LOG.trace(" Method Entry : getlistkeyfigures()");
        return keyFiguresList;

    }

    /*
     * (non-Javadoc)
     *
     * @see com.ihcl.core.models.CorporateCardsListModel#keyFigureValueMap()
     */


}
