package com.ihcl.core.models;

import com.ihcl.core.models.metaReviewsPojo.Badge_list;
import com.ihcl.core.models.metaReviewsPojo.Category_list;
import com.ihcl.core.models.metaReviewsPojo.Good_to_know_list;
import com.ihcl.core.models.metaReviewsPojo.Hotel_type_list;
import com.ihcl.core.models.metaReviewsPojo.Language_meta_review_list;
import com.ihcl.core.models.metaReviewsPojo.Summary;
import com.ihcl.core.models.metaReviewsPojo.Trip_type_meta_review_list;

public class MetaReviewsResponse {

    private String ty_id;

    private Summary summary;

    private Good_to_know_list[] good_to_know_list;

    private Badge_list[] badge_list;

    private String lang;

    private String gender_talks_about;

    private String version;

    private String response;

    private Category_list[] category_list;

    private String reviews_count;

    private Hotel_type_list[] hotel_type_list;

    private Language_meta_review_list[] language_meta_review_list;

    private String[] old_ty_ids;

    private Trip_type_meta_review_list[] trip_type_meta_review_list;

    public String getTy_id() {
        return ty_id;
    }

    public void setTy_id(String ty_id) {
        this.ty_id = ty_id;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public Good_to_know_list[] getGood_to_know_list() {
        return good_to_know_list;
    }

    public void setGood_to_know_list(Good_to_know_list[] good_to_know_list) {
        this.good_to_know_list = good_to_know_list;
    }

    public Badge_list[] getBadge_list() {
        return badge_list;
    }

    public void setBadge_list(Badge_list[] badge_list) {
        this.badge_list = badge_list;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getGender_talks_about() {
        return gender_talks_about;
    }

    public void setGender_talks_about(String gender_talks_about) {
        this.gender_talks_about = gender_talks_about;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Category_list[] getCategory_list() {
        return category_list;
    }

    public void setCategory_list(Category_list[] category_list) {
        this.category_list = category_list;
    }

    public String getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(String reviews_count) {
        this.reviews_count = reviews_count;
    }

    public Hotel_type_list[] getHotel_type_list() {
        return hotel_type_list;
    }

    public void setHotel_type_list(Hotel_type_list[] hotel_type_list) {
        this.hotel_type_list = hotel_type_list;
    }

    public Language_meta_review_list[] getLanguage_meta_review_list() {
        return language_meta_review_list;
    }

    public void setLanguage_meta_review_list(Language_meta_review_list[] language_meta_review_list) {
        this.language_meta_review_list = language_meta_review_list;
    }

    public String[] getOld_ty_ids() {
        return old_ty_ids;
    }

    public void setOld_ty_ids(String[] old_ty_ids) {
        this.old_ty_ids = old_ty_ids;
    }

    public Trip_type_meta_review_list[] getTrip_type_meta_review_list() {
        return trip_type_meta_review_list;
    }

    public void setTrip_type_meta_review_list(Trip_type_meta_review_list[] trip_type_meta_review_list) {
        this.trip_type_meta_review_list = trip_type_meta_review_list;
    }

    @Override
    public String toString() {
        return "ClassPojo [ty_id = " + ty_id + ", summary = " + summary + ", good_to_know_list = " + good_to_know_list
                + ", badge_list = " + badge_list + ", lang = " + lang + ", gender_talks_about = " + gender_talks_about
                + ", version = " + version + ", response = " + response + ", category_list = " + category_list
                + ", reviews_count = " + reviews_count + ", hotel_type_list = " + hotel_type_list
                + ", language_meta_review_list = " + language_meta_review_list + ", old_ty_ids = " + old_ty_ids
                + ", trip_type_meta_review_list = " + trip_type_meta_review_list + "]";
    }
}
