package com.ihcl.core.models.gallery.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.gallery.GalleryImageFetcherService;
import com.ihcl.core.models.gallery.GalleryImageSelector;


/**
 * Gallery image selector class.
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = GalleryImageSelector.class)
public class GalleryImageSelectorImpl implements GalleryImageSelector {

    private static final Logger LOG = LoggerFactory.getLogger(GalleryImageSelectorImpl.class);

    private static final Integer CONSTANT_INTEGER_I0 = 0;

    @Inject
    private Resource resource;

    @Inject
    @Default(values = "gallery")
    private String galleryNodeName;


    @Inject
    private GalleryImageFetcherService galleryImageFetcherService;

    private List<JSONObject> imagePaths;

    private Integer numberOfImages;

    @PostConstruct
    public void activate() {
        String methodName = "activate";
        LOG.trace("Method Entry: " + methodName);
        LOG.debug("Resource received by injection: " + resource);
        LOG.debug("Received gallery image fetcher service as: " + galleryImageFetcherService);
        LOG.debug("Received gallery node name as: " + galleryNodeName);
        Resource gallery = resource.getChild(galleryNodeName);
        LOG.debug("Fetched gallery resource as: " + gallery);
        if (gallery != null) {
            buildImagePaths(gallery);
            numberOfImages = getTotalNumberOfImages();
        }
        LOG.trace("Method Exit: " + methodName);
    }


    /**
     * Method to build images paths from resource.
     *
     * @param gallery
     *
     */
    private void buildImagePaths(Resource gallery) {
        String methodName = "buildImagePaths";
        LOG.trace("Method Entry: " + methodName);
        imagePaths = galleryImageFetcherService.getGalleryImagePathsAt(gallery);
        LOG.trace("Method Exit: " + methodName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getTotalNumberOfImages() {
        int imagesSize = 0;
        if (imagePaths != null) {
            imagesSize = imagePaths.size();
        }
        return imagesSize;
    }

    /**
     * Getter for the field numberOfImages
     *
     * @return the numberOfImages
     */
    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<JSONObject> getImagePaths() {
        return imagePaths;
    }
}
