/**
 *
 */
package com.ihcl.core.models.holidays.packages;


/**
 * @author moonraft
 *
 */
public class ImageList {

    String imagePath;

    String imageTitle;

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
