/**
 *
 */
package com.ihcl.core.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vijay Pal
 *
 */
@Model(
        adaptables = Resource.class)
public class OtherServicesIhclBrands {

    private static final Logger LOG = LoggerFactory.getLogger(OtherServicesIhclBrands.class);

    @Self
    Resource resource;

    private String serviceTitle;

    private String servicesDescription;

    private String servicesImage;

    @PostConstruct
    protected void init() {

        LOG.info("Inside init() of class OtherServicesBrandsModel");
        Resource content = resource.getChild("jcr:content");
        LOG.debug("checking if content has values : {}", content);
        if (content != null) {

            ValueMap valueMap = content.adaptTo(ValueMap.class);

            servicesImage = valueMap.get("imagePath", String.class);
            serviceTitle = valueMap.get("serviceName", String.class);
            servicesDescription = valueMap.get("description", String.class);

            LOG.debug("servicesImage : {}", servicesImage);
            LOG.debug("serviceTitle : {}", serviceTitle);
            LOG.debug("servicesDescription : {}", servicesDescription);
        } else {
            LOG.error("content found as null.");
        }
    }

    public Resource getResource() {
        return resource;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }


    public String getServicesDescription() {
        return servicesDescription;
    }


    public String getServicesImage() {
        return servicesImage;
    }

}
