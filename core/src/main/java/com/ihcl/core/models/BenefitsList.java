package com.ihcl.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class BenefitsList {
    @Inject
    @Optional
    private List<Benefit> benefits;

    public List<Benefit> getBenefits() {
        return benefits;
    }
}
