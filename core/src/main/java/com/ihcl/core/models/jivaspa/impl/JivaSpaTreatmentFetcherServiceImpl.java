/**
 *
 */
package com.ihcl.core.models.jivaspa.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ihcl.core.models.jivaspa.JivaSpaTreatmentFetcherService;
import com.ihcl.core.services.CurrencyFetcherService;
import com.ihcl.core.services.search.SearchServiceConstants.PATH;

@Model(adaptables = { Resource.class, SlingHttpServletRequest.class },
        adapters = JivaSpaTreatmentFetcherService.class)
public class JivaSpaTreatmentFetcherServiceImpl implements JivaSpaTreatmentFetcherService {

    private static final Logger LOG = LoggerFactory.getLogger(JivaSpaTreatmentFetcherServiceImpl.class);

    @Inject
    private Resource resource;

    @Inject
    private CurrencyFetcherService currencyFetcherService;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private QueryBuilder builder;

    @Reference
    private Session session = null;


    @Reference
    private ResourceResolver resolver;

    @Inject
    @Default(values = "/content/tajhotels")
    private String siteRootPath;

    private List<JivaSpaTreatmentBean> signatureExperienceSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> indianTherapySpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> bodyScrubSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> ayurvedaTherapySpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> meditationSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> beautySpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> aromaTherapySpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> spaIndulgenceSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> traditionalBathSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    private List<JivaSpaTreatmentBean> kidsRetreatSpaBean = new ArrayList<JivaSpaTreatmentBean>();

    @PostConstruct
    public void activate() {

        try {
            resolver = resource.getResourceResolver();
            session = resolver.adaptTo(Session.class);
            LOG.trace("Method Entry: Activate ");
            getJivaSpaTreatmentPath();
        } catch (Exception e) {
            LOG.error("Exception found in Activate method :: {}", e.getMessage());
        } finally {
            LOG.trace("Method Exit: Activate");
        }

    }

    @Override
    public List<String> getJivaSpaTreatmentPath() {
        ValueMap valueMap = null;
        try {
            LOG.trace("jivaResource :" + resource);
            valueMap = resource.adaptTo(ValueMap.class);
            String[] treatments = valueMap.get("treatments", String[].class);
            JsonObject jsonObject;
            Gson gson = new Gson();

            if (null != treatments) {
                for (int i = 0; i < treatments.length; i++) {
                    JsonElement jsonElement = gson.fromJson(treatments[i], JsonElement.class);
                    jsonObject = jsonElement.getAsJsonObject();
                    JivaSpaTreatmentBean jivaSpaTreatmentBeanObj = new JivaSpaTreatmentBean();
                    LOG.trace("jsonObject :" + jsonObject);
                    String tagName = "";
                    if (jsonObject.getAsJsonArray("treatmentName").isJsonArray()) {
                        tagName = jsonObject.getAsJsonArray("treatmentName").get(0).toString();
                    } else {
                        tagName = jsonObject.get("treatmentName").toString();
                    }

                    if (null != tagName) {

                        LOG.trace("Tag Name :" + tagName);
                        if (jsonObject.get("treatmentPrice") != null) {
                            jivaSpaTreatmentBeanObj.setTreatmentAmount(
                                    jsonObject.get("treatmentPrice").toString().replaceAll("\"", ""));
                        }
                        String jivaSpaPath = siteRootPath + PATH.JIVA_SPA_PATH;
                        String treatmentPath = fetchTreatmentPath((tagName).trim(), jivaSpaPath);
                        jivaSpaTreatmentBeanObj.setTreatmentPath(treatmentPath);
                        LOG.trace("Treatment Path :" + treatmentPath);
                        if (jsonObject.get("treatmentCurrency") != null) {
                            String treatmentCurrency = jsonObject.get("treatmentCurrency").toString().replaceAll("\"",
                                    "");
                            jivaSpaTreatmentBeanObj.setTreatmentCurrency(
                                    currencyFetcherService.getCurrencySymbolValue(treatmentCurrency));
                        }
                        if (jsonObject.get("treatmentDuration") != null) {
                            jivaSpaTreatmentBeanObj.setTreatmentDuration(
                                    jsonObject.get("treatmentDuration").toString().replaceAll("\"", ""));
                        }

                        if (tagName != null && tagName.contains("signature-experience")) {
                            LOG.trace("Inside Signature Experience");
                            signatureExperienceSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("spa-indulgences")) {
                            spaIndulgenceSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("body-scrups-and-wraps")) {
                            bodyScrubSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("ayurveda-therapies")) {
                            ayurvedaTherapySpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("yoga-and-meditation")) {
                            meditationSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("beauty")) {
                            beautySpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("indian-therapies")) {
                            indianTherapySpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("traditional-bhutanese-bath")) {
                            traditionalBathSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("indian-aroma-therapy")) {
                            aromaTherapySpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                        if (tagName != null && tagName.contains("kids-retreat")) {
                            kidsRetreatSpaBean.add(jivaSpaTreatmentBeanObj);
                        }
                    }
                }
            }

        } catch (Exception e) {
            LOG.error("Exception occured in getJivaSpaTreatmentPath :: {}", e.getMessage());
            LOG.debug("Exception occured in getJivaSpaTreatmentPath :: {}", e);
        }
        return null;

    }

    /**
     * @param valueMap
     * @return
     */
    private String fetchTreatmentPath(String treatments, String searchPath) {
        String jivaSpa = null;

        try {
            HashMap<String, String> predicates = new HashMap<String, String>();
            predicates.put("type", "cq:Page");
            predicates.put("path", searchPath);
            predicates.put("fulltext", treatments);
            predicates.put("p.limit=", "-1");
            LOG.trace("predicates :" + predicates);


            QueryBuilder builder = resolver.adaptTo(QueryBuilder.class);
            LOG.trace("Query builder : " + builder);
            PredicateGroup searchPredicates = PredicateGroup.create(predicates);
            LOG.trace("SearchPredicates : " + searchPredicates);
            Query query = builder.createQuery(searchPredicates, session);
            LOG.trace("Query to be executed is : " + query);
            query.setStart(0L);

            SearchResult result = query.getResult();
            LOG.trace("Query Executed");
            LOG.trace("Query result is : " + result.getHits().size());

            List<Hit> hits = result.getHits();
            Iterator<Hit> hitList = hits.iterator();
            Hit hit;
            while (hitList.hasNext()) {
                hit = hitList.next();
                jivaSpa = hit.getPath();
            }
        } catch (Exception e) {
            LOG.error("Exception occured in fetchTreatmentPath :: {}", e.getMessage());
            LOG.debug("Exception occured in fetchTreatmentPath :: {}", e);
        }
        LOG.trace("jivaSpa :" + jivaSpa);
        return jivaSpa;
    }

    public List<JivaSpaTreatmentBean> getSignatureExperienceSpaBean() {
        return signatureExperienceSpaBean;
    }

    public List<JivaSpaTreatmentBean> getIndianTherapySpaBean() {
        return indianTherapySpaBean;
    }

    public List<JivaSpaTreatmentBean> getBodyScrubSpaBean() {
        return bodyScrubSpaBean;
    }

    public List<JivaSpaTreatmentBean> getAyurvedaTherapySpaBean() {
        return ayurvedaTherapySpaBean;
    }

    public List<JivaSpaTreatmentBean> getMeditationSpaBean() {
        return meditationSpaBean;
    }

    public List<JivaSpaTreatmentBean> getBeautySpaBean() {
        return beautySpaBean;
    }

    public List<JivaSpaTreatmentBean> getAromaTherapySpaBean() {
        return aromaTherapySpaBean;
    }

    public List<JivaSpaTreatmentBean> getSpaIndulgenceSpaBean() {
        return spaIndulgenceSpaBean;
    }

    public List<JivaSpaTreatmentBean> getTraditionalBathSpaBean() {
        return traditionalBathSpaBean;
    }

    public List<JivaSpaTreatmentBean> getKidsRetreatSpaBean() {
        return kidsRetreatSpaBean;
    }

}
