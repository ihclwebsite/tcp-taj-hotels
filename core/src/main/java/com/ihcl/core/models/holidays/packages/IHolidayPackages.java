/**
 *
 */
package com.ihcl.core.models.holidays.packages;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * @author moonraft
 *
 */
@ConsumerType
public interface IHolidayPackages {

    List<ParticipatingHotelDetails> getHotelDetails();

}
