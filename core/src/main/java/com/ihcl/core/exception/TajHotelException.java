/**
 *
 */
package com.ihcl.core.exception;


/**
 * @author Srikanta.moonraft
 *
 */
public class TajHotelException extends Exception {

    /**
     * Generated serialVersionUID.
     */
    private static final long serialVersionUID = 4563269593739557905L;

    public TajHotelException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public TajHotelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public TajHotelException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public TajHotelException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public TajHotelException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }


}
