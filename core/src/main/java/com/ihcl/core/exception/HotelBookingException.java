/**
 *
 */
package com.ihcl.core.exception;


/**
 * @author moonraft
 *
 */
public class HotelBookingException extends Exception {

    /**
     * This is used to throw Hotel Bookings related exceptions with the cause of exception as message
     */
    private static final long serialVersionUID = 1L;

    public HotelBookingException(String message) {
        super(message);
    }

}
