/**
 *
 */
package com.ihcl.core.exception;


/**
 * @author moonraft
 *
 */
public class PaymentException extends Exception {

    /**
     * This exception is used to throw payment related business exceptions with the cause of exception as message
     */
    private static final long serialVersionUID = 1L;

    public PaymentException(String message) {
        super(message);
    }
}
