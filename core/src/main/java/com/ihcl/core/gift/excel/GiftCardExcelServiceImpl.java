/**
 *
 */
package com.ihcl.core.gift.excel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.email.api.IEmailService;


/**
 * @author Nutan
 *
 */
@Component(immediate = true,
        service = IGiftExcelService.class)
public class GiftCardExcelServiceImpl implements IGiftExcelService {

    public static final String PREFIX = "giftCardRedemption";

    public static final String SUFFIX = ".csv";

    private static final String CSV_SEPARATOR = ",";

    private static final String PROGRAM_NAME = "Taj InnerCircle";

    private final Logger LOG = LoggerFactory.getLogger(GiftCardExcelServiceImpl.class);

    @Reference
    private ResourceFetcherService resourceFetcherService;

    @Reference
    private IEmailService emailService;

    @Override
    public void generateExcelFromGiftNode(Resource giftExcelResource, String emailAddressTo, String emailAddressCc,
            ResourceResolver resolver) throws IOException {

        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        LOG.debug("file name before valling geneate func::" + tempFile.getAbsolutePath());
        generateCsv(giftExcelResource, tempFile, resolver);
        LOG.debug("file size after genrate func::" + tempFile.length());
        emailService.sendGiftExcel(tempFile, emailAddressTo, emailAddressCc);
        tempFile.deleteOnExit();


    }


    private void generateCsv(Resource giftExcelResource, File tempFile, ResourceResolver resolver) throws IOException {

        try {

            FileWriter fileWriter = new FileWriter(tempFile);
            // adding header to csv
            String[] header = { "Redemption Id", "Value Egift Certiciate", "Product Name", "Member Name",
                    "Member Number", "Created Date", "Recipients Email Id", "Points Redeem Type", "Member Email Id",
                    "Recipients Name", "Points Redeemed", "Currency", "Denomination" };

            // add data to csv
            List<GiftExcelPojo> data = new ArrayList<GiftExcelPojo>();
            data = generateDataList(giftExcelResource, resolver);


            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.append(Arrays.toString(header));
            bw.newLine();
            if (null != data) {
                for (GiftExcelPojo giftExcelPojo : data) {
                    StringBuffer sbf = new StringBuffer();
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getRdr_no()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getPoints_pay_amount()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getProduct_name()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(
                            giftExcelPojo.getFirstname() + " " + giftExcelPojo.getLastname()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getMemno()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getRedemption_date()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getRecipientEmailId()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getPoint_type()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getPreferredEmail()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getRecipientName()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getPoints()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getCurrency()));
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(giftExcelPojo.getDenomination()));
                    bw.write(sbf.toString());
                    bw.newLine();

                }
            }

            bw.flush();
            bw.close();

        } catch (Exception e) {
            LOG.error("exception caught::" + e.getMessage());
            e.printStackTrace();
        }


    }

    private String surroundQuotesWithNullCheck(String string) {
        String methodName = "surroundQuotesWithNullCheck";
        String surroundedString = string;
        LOG.trace("Method Entry: " + methodName);
        if (null != string) {


            if (string.contains(",")) {
                if (string.contains("\"")) {
                    string.replaceAll("\"", "\"\"");
                }
                surroundedString = "\"".concat(string).concat("\"");
            }
        } else {
            surroundedString = "";
        }
        LOG.trace("Method Exit: " + methodName);
        return surroundedString;
    }


    /**
     * @return
     * @throws ParseException
     * @throws RepositoryException
     * @throws ConstraintViolationException
     * @throws LockException
     * @throws VersionException
     * @throws ValueFormatException
     */
    private List<GiftExcelPojo> generateDataList(Resource giftExcelResource, ResourceResolver resolver)
            throws ParseException, ValueFormatException, VersionException, LockException, ConstraintViolationException,
            RepositoryException {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yy");
            Iterable<Resource> children = giftExcelResource.getChildren();
            List<GiftExcelPojo> listgiftPojo = new ArrayList<>();
            for (Resource child : children) {

                GiftExcelPojo giftExcelPojo = new GiftExcelPojo();
                LOG.trace("the path is ::" + child.getPath());
                ValueMap giftValueMap = child.adaptTo(ValueMap.class);

                String redemptionDate = giftValueMap.get("createdDate", String.class);
                String redemDateStr = sdf1.format(sdf.parse(redemptionDate));
                giftExcelPojo.setRedemption_date(redemDateStr);
                giftExcelPojo.setRdr_no(child.getName());
                giftExcelPojo.setPurchase_order_date(redemDateStr);
                String order_no = giftValueMap.get("orderId", String.class);
                giftExcelPojo.setOrder_no(order_no);
                String memno = giftValueMap.get("memberNumber", String.class);
                giftExcelPojo.setMemno(memno);
                giftExcelPojo.setProgram_name(PROGRAM_NAME);
                String tier_name = giftValueMap.get("tier", String.class);
                giftExcelPojo.setTier_name(tier_name);
                String pointType = giftValueMap.get("pointType", String.class);
                giftExcelPojo.setPoint_type(pointType);
                String productType = giftValueMap.get("productType", String.class);
                giftExcelPojo.setProduct_type(productType);
                String productName = giftValueMap.get("productName", String.class);
                giftExcelPojo.setProduct_name(productName);
                String itemCode = giftValueMap.get("itemcode", String.class);
                giftExcelPojo.setShort_name(itemCode);
                String points = giftValueMap.get("pointsRedeemed", String.class);
                giftExcelPojo.setPoints(points);
                String quantity = giftValueMap.get("quantity", String.class);
                giftExcelPojo.setQuantity(quantity);
                giftExcelPojo.setActual_points_redeem(points);

                String amount_paid = giftValueMap.get("valueEgiftCertificated", String.class);
                giftExcelPojo.setPoints_pay_amount(amount_paid);
                String firstName = giftValueMap.get("firstName", String.class);
                giftExcelPojo.setFirstname(firstName);
                String lastName = giftValueMap.get("lastName", String.class);
                giftExcelPojo.setLastname(lastName);
                String salutation = giftValueMap.get("sal", String.class);
                giftExcelPojo.setSal(salutation);
                giftExcelPojo.setAddress_as("Dear " + salutation + " " + lastName);
                String mobile = giftValueMap.get("mobile", String.class);
                giftExcelPojo.setPreferredMobile(mobile);
                String memberEmailId = giftValueMap.get("memberEmailId", String.class);
                giftExcelPojo.setPreferredEmail(memberEmailId);
                giftExcelPojo.setOrder_type(productType);

                String recipientEmailId = giftValueMap.get("reciepientEmailId", String.class);
                giftExcelPojo.setRecipientEmailId(recipientEmailId);

                String recipientName = giftValueMap.get("reciepientsName", String.class);
                giftExcelPojo.setRecipientName(recipientName);

                String currency = giftValueMap.get("currency", String.class);
                giftExcelPojo.setCurrency(currency);

                String denomination = giftValueMap.get("denomination", String.class);
                giftExcelPojo.setDenomination(denomination);
                listgiftPojo.add(giftExcelPojo);


            }
            return listgiftPojo;

        } catch (Exception e) {
            LOG.error("exception caught::" + e.getMessage());
        }
        return null;
    }

}

