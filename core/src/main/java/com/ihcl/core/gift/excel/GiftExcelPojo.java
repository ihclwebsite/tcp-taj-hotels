/**
 *
 */
package com.ihcl.core.gift.excel;

import java.io.Serializable;

/**
 * @author Nutan
 *
 */
public class GiftExcelPojo implements Serializable {


    private static final long serialVersionUID = 1447598970301543423L;

    String redemption_date = "";

    String rdr_no = "";

    String purchase_order_date = "";

    String order_no = "";

    String memno = "";

    String program_name = "";

    String tier_name = "";

    String point_type = "";

    String reward_category = "";

    String product_type = "";

    String product_name = "";

    String short_name = "";

    String item_code_model_no = "";

    String value = "";

    String retail_price_to_taj = "";

    String type_of_redemption = "";

    String points = "";

    String pay = "";

    String quantity = "";

    String total_amount = "";

    String actual_points_redeem = "";

    String points_pay_amount = "";

    String vendor_name = "";

    String vendor_company = "";

    String vendoradd1 = "";

    String vendoradd2 = "";

    String vendorcity = "";

    String vendorstate = "";

    String vendorpin = "";

    String vendorcountry = "";

    String vendor_add_type = "";

    String vendor_email = "";

    String vendor_mobile = "";

    String vendor_tel = "";

    String address_as = "";

    String sal = "";

    String firstname = "";

    String lastname = "";

    String company = "";

    String job_title = "";

    String job_desc = "";

    String addr_name = "";

    String preferredadd1 = "";

    String preferredadd2 = "";

    String preferredadd3 = "";

    String preferredCity = "";

    String preferredPin = "";

    String preferredState = "";

    String preferredCountry = "";

    String otherPreferredCity = "";

    String otherPreferredState = "";

    String otherPreferredCountry = "";

    String preferredAddtype = "";

    String preferredEmail = "";

    String preferredMobile = "";

    String preferredTel = "";

    String altAdd1 = "";

    String altAdd2 = "";

    String altAdd3 = "";

    String altCity = "";

    String altPin = "";

    String altState = "";

    String altCountry = "";

    String othAltCity = "";

    String othAltState = "";

    String othAltCountry = "";

    String alt_mobile = "";


    String ship_to_address = "";

    String cards_dispatch_on_date = "";

    String pod_no = "";

    String courier_name = "";

    String courier_file_return_date = "";

    String card_delivered_on_date = "";

    String card_delivered_on_time = "";

    String accepted_by_name = "";

    String undelivered_kit_date = "";

    String undelivered_kit_reason = "";

    String person_uid = "";

    String ship_no = "";

    String ship_status = "";

    String awm_num = "";

    String order_type = "";

    String comments = "";

    String source_of_txn = "";

    String card_No = "";

    String Amount = "";

    String Date_Sent = "";

    String currency = "";

    String qc_id = "";

    private String recipientEmailId;

    private String recipientName = "";

    private String denomination = "";


    public String getRedemption_date() {
        return redemption_date;
    }


    public void setRedemption_date(String redemption_date) {
        this.redemption_date = redemption_date;
    }


    public String getRdr_no() {
        return rdr_no;
    }


    public void setRdr_no(String rdr_no) {
        this.rdr_no = rdr_no;
    }


    public String getPurchase_order_date() {
        return purchase_order_date;
    }


    public void setPurchase_order_date(String purchase_order_date) {
        this.purchase_order_date = purchase_order_date;
    }


    public String getOrder_no() {
        return order_no;
    }


    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }


    public String getMemno() {
        return memno;
    }


    public void setMemno(String memno) {
        this.memno = memno;
    }


    public String getProgram_name() {
        return program_name;
    }


    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }


    public String getTier_name() {
        return tier_name;
    }


    public void setTier_name(String tier_name) {
        this.tier_name = tier_name;
    }


    public String getPoint_type() {
        return point_type;
    }


    public void setPoint_type(String point_type) {
        this.point_type = point_type;
    }


    public String getReward_category() {
        return reward_category;
    }


    public void setReward_category(String reward_category) {
        this.reward_category = reward_category;
    }


    public String getProduct_type() {
        return product_type;
    }


    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }


    public String getProduct_name() {
        return product_name;
    }


    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }


    public String getShort_name() {
        return short_name;
    }


    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }


    public String getItem_code_model_no() {
        return item_code_model_no;
    }


    public void setItem_code_model_no(String item_code_model_no) {
        this.item_code_model_no = item_code_model_no;
    }


    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }


    public String getRetail_price_to_taj() {
        return retail_price_to_taj;
    }


    public void setRetail_price_to_taj(String retail_price_to_taj) {
        this.retail_price_to_taj = retail_price_to_taj;
    }


    public String getType_of_redemption() {
        return type_of_redemption;
    }


    public void setType_of_redemption(String type_of_redemption) {
        this.type_of_redemption = type_of_redemption;
    }


    public String getPoints() {
        return points;
    }


    public void setPoints(String points) {
        this.points = points;
    }


    public String getPay() {
        return pay;
    }


    public void setPay(String pay) {
        this.pay = pay;
    }


    public String getQuantity() {
        return quantity;
    }


    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


    public String getTotal_amount() {
        return total_amount;
    }


    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }


    public String getActual_points_redeem() {
        return actual_points_redeem;
    }


    public void setActual_points_redeem(String actual_points_redeem) {
        this.actual_points_redeem = actual_points_redeem;
    }


    public String getPoints_pay_amount() {
        return points_pay_amount;
    }


    public void setPoints_pay_amount(String points_pay_amount) {
        this.points_pay_amount = points_pay_amount;
    }


    public String getVendor_name() {
        return vendor_name;
    }


    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }


    public String getVendor_company() {
        return vendor_company;
    }


    public void setVendor_company(String vendor_company) {
        this.vendor_company = vendor_company;
    }


    public String getVendoradd1() {
        return vendoradd1;
    }


    public void setVendoradd1(String vendoradd1) {
        this.vendoradd1 = vendoradd1;
    }


    public String getVendoradd2() {
        return vendoradd2;
    }


    public void setVendoradd2(String vendoradd2) {
        this.vendoradd2 = vendoradd2;
    }


    public String getVendorcity() {
        return vendorcity;
    }


    public void setVendorcity(String vendorcity) {
        this.vendorcity = vendorcity;
    }


    public String getVendorstate() {
        return vendorstate;
    }


    public void setVendorstate(String vendorstate) {
        this.vendorstate = vendorstate;
    }


    public String getVendorpin() {
        return vendorpin;
    }


    public void setVendorpin(String vendorpin) {
        this.vendorpin = vendorpin;
    }


    public String getVendorcountry() {
        return vendorcountry;
    }


    public void setVendorcountry(String vendorcountry) {
        this.vendorcountry = vendorcountry;
    }


    public String getVendor_email() {
        return vendor_email;
    }


    public void setVendor_email(String vendor_email) {
        this.vendor_email = vendor_email;
    }


    public String getVendor_mobile() {
        return vendor_mobile;
    }


    public void setVendor_mobile(String vendor_mobile) {
        this.vendor_mobile = vendor_mobile;
    }


    public String getVendor_tel() {
        return vendor_tel;
    }


    public void setVendor_tel(String vendor_tel) {
        this.vendor_tel = vendor_tel;
    }


    public String getAddress_as() {
        return address_as;
    }


    public void setAddress_as(String address_as) {
        this.address_as = address_as;
    }


    public String getSal() {
        return sal;
    }


    public void setSal(String sal) {
        this.sal = sal;
    }


    public String getFirstname() {
        return firstname;
    }


    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getLastname() {
        return lastname;
    }


    public void setLastname(String lastname) {
        this.lastname = lastname;
    }


    public String getCompany() {
        return company;
    }


    public void setCompany(String company) {
        this.company = company;
    }


    public String getJob_title() {
        return job_title;
    }


    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }


    public String getJob_desc() {
        return job_desc;
    }


    public void setJob_desc(String job_desc) {
        this.job_desc = job_desc;
    }


    public String getAddr_name() {
        return addr_name;
    }


    public void setAddr_name(String addr_name) {
        this.addr_name = addr_name;
    }


    public String getPreferredadd1() {
        return preferredadd1;
    }


    public void setPreferredadd1(String preferredadd1) {
        this.preferredadd1 = preferredadd1;
    }


    public String getPreferredadd2() {
        return preferredadd2;
    }


    public void setPreferredadd2(String preferredadd2) {
        this.preferredadd2 = preferredadd2;
    }


    public String getPreferredadd3() {
        return preferredadd3;
    }


    public void setPreferredadd3(String preferredadd3) {
        this.preferredadd3 = preferredadd3;
    }


    public String getPreferredCity() {
        return preferredCity;
    }


    public void setPreferredCity(String preferredCity) {
        this.preferredCity = preferredCity;
    }


    public String getPreferredPin() {
        return preferredPin;
    }


    public void setPreferredPin(String preferredPin) {
        this.preferredPin = preferredPin;
    }


    public String getPreferredState() {
        return preferredState;
    }


    public void setPreferredState(String preferredState) {
        this.preferredState = preferredState;
    }


    public String getPreferredCountry() {
        return preferredCountry;
    }


    public void setPreferredCountry(String preferredCountry) {
        this.preferredCountry = preferredCountry;
    }


    public String getOtherPreferredCity() {
        return otherPreferredCity;
    }


    public void setOtherPreferredCity(String otherPreferredCity) {
        this.otherPreferredCity = otherPreferredCity;
    }


    public String getOtherPreferredState() {
        return otherPreferredState;
    }


    public void setOtherPreferredState(String otherPreferredState) {
        this.otherPreferredState = otherPreferredState;
    }


    public String getOtherPreferredCountry() {
        return otherPreferredCountry;
    }


    public void setOtherPreferredCountry(String otherPreferredCountry) {
        this.otherPreferredCountry = otherPreferredCountry;
    }


    public String getPreferredAddtype() {
        return preferredAddtype;
    }


    public void setPreferredAddtype(String preferredAddtype) {
        this.preferredAddtype = preferredAddtype;
    }


    public String getPreferredEmail() {
        return preferredEmail;
    }


    public void setPreferredEmail(String preferredEmail) {
        this.preferredEmail = preferredEmail;
    }


    public String getPreferredMobile() {
        return preferredMobile;
    }


    public void setPreferredMobile(String preferredMobile) {
        this.preferredMobile = preferredMobile;
    }


    public String getPreferredTel() {
        return preferredTel;
    }


    public void setPreferredTel(String preferredTel) {
        this.preferredTel = preferredTel;
    }


    public String getAltAdd1() {
        return altAdd1;
    }


    public void setAltAdd1(String altAdd1) {
        this.altAdd1 = altAdd1;
    }


    public String getAltAdd2() {
        return altAdd2;
    }


    public void setAltAdd2(String altAdd2) {
        this.altAdd2 = altAdd2;
    }


    public String getAltAdd3() {
        return altAdd3;
    }


    public void setAltAdd3(String altAdd3) {
        this.altAdd3 = altAdd3;
    }


    public String getAltCity() {
        return altCity;
    }


    public void setAltCity(String altCity) {
        this.altCity = altCity;
    }


    public String getAltPin() {
        return altPin;
    }


    public void setAltPin(String altPin) {
        this.altPin = altPin;
    }


    public String getAltState() {
        return altState;
    }


    public void setAltState(String altState) {
        this.altState = altState;
    }


    public String getAltCountry() {
        return altCountry;
    }


    public void setAltCountry(String altCountry) {
        this.altCountry = altCountry;
    }


    public String getOthAltCity() {
        return othAltCity;
    }


    public void setOthAltCity(String othAltCity) {
        this.othAltCity = othAltCity;
    }


    public String getOthAltState() {
        return othAltState;
    }


    public void setOthAltState(String othAltState) {
        this.othAltState = othAltState;
    }


    public String getOthAltCountry() {
        return othAltCountry;
    }


    public void setOthAltCountry(String othAltCountry) {
        this.othAltCountry = othAltCountry;
    }


    public String getShip_to_address() {
        return ship_to_address;
    }


    public void setShip_to_address(String ship_to_address) {
        this.ship_to_address = ship_to_address;
    }


    public String getCards_dispatch_on_date() {
        return cards_dispatch_on_date;
    }


    public void setCards_dispatch_on_date(String cards_dispatch_on_date) {
        this.cards_dispatch_on_date = cards_dispatch_on_date;
    }


    public String getPod_no() {
        return pod_no;
    }


    public void setPod_no(String pod_no) {
        this.pod_no = pod_no;
    }


    public String getCourier_name() {
        return courier_name;
    }


    public void setCourier_name(String courier_name) {
        this.courier_name = courier_name;
    }


    public String getCourier_file_return_date() {
        return courier_file_return_date;
    }


    public void setCourier_file_return_date(String courier_file_return_date) {
        this.courier_file_return_date = courier_file_return_date;
    }


    public String getCard_delivered_on_date() {
        return card_delivered_on_date;
    }


    public void setCard_delivered_on_date(String card_delivered_on_date) {
        this.card_delivered_on_date = card_delivered_on_date;
    }


    public String getCard_delivered_on_time() {
        return card_delivered_on_time;
    }


    public void setCard_delivered_on_time(String card_delivered_on_time) {
        this.card_delivered_on_time = card_delivered_on_time;
    }


    public String getAccepted_by_name() {
        return accepted_by_name;
    }


    public void setAccepted_by_name(String accepted_by_name) {
        this.accepted_by_name = accepted_by_name;
    }


    public String getUndelivered_kit_date() {
        return undelivered_kit_date;
    }


    public void setUndelivered_kit_date(String undelivered_kit_date) {
        this.undelivered_kit_date = undelivered_kit_date;
    }


    public String getUndelivered_kit_reason() {
        return undelivered_kit_reason;
    }


    public void setUndelivered_kit_reason(String undelivered_kit_reason) {
        this.undelivered_kit_reason = undelivered_kit_reason;
    }


    public String getPerson_uid() {
        return person_uid;
    }


    public void setPerson_uid(String person_uid) {
        this.person_uid = person_uid;
    }


    public String getShip_no() {
        return ship_no;
    }


    public void setShip_no(String ship_no) {
        this.ship_no = ship_no;
    }


    public String getShip_status() {
        return ship_status;
    }


    public void setShip_status(String ship_status) {
        this.ship_status = ship_status;
    }


    public String getAwm_num() {
        return awm_num;
    }


    public void setAwm_num(String awm_num) {
        this.awm_num = awm_num;
    }


    public String getOrder_type() {
        return order_type;
    }


    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }


    public String getComments() {
        return comments;
    }


    public void setComments(String comments) {
        this.comments = comments;
    }


    public String getSource_of_txn() {
        return source_of_txn;
    }


    public void setSource_of_txn(String source_of_txn) {
        this.source_of_txn = source_of_txn;
    }


    public String getCard_No() {
        return card_No;
    }


    public void setCard_No(String card_No) {
        this.card_No = card_No;
    }


    public String getAmount() {
        return Amount;
    }


    public void setAmount(String amount) {
        Amount = amount;
    }


    public String getDate_Sent() {
        return Date_Sent;
    }


    public void setDate_Sent(String date_Sent) {
        Date_Sent = date_Sent;
    }


    public String getQc_id() {
        return qc_id;
    }


    public void setQc_id(String qc_id) {
        this.qc_id = qc_id;
    }


    public String getVendor_add_type() {
        return vendor_add_type;
    }


    public void setVendor_add_type(String vendor_add_type) {
        this.vendor_add_type = vendor_add_type;
    }

    public String getAlt_mobile() {
        return alt_mobile;
    }


    public void setAlt_mobile(String alt_mobile) {
        this.alt_mobile = alt_mobile;
    }

    public void setRecipientEmailId(String recipientEmailId) {
        this.recipientEmailId = recipientEmailId;
    }

    public String getRecipientEmailId() {
        return recipientEmailId;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientName() {
        return recipientName;
    }


    public String getCurrency() {
        return currency;
    }


    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getDenomination() {
        return denomination;
    }


    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }


}
