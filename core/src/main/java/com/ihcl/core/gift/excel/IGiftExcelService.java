package com.ihcl.core.gift.excel;

import java.io.IOException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

public interface IGiftExcelService {


    void generateExcelFromGiftNode(Resource giftExcelResource, String emailAddressTo, String emailAdddressCc,
            ResourceResolver resourceResolver) throws IOException;


}
