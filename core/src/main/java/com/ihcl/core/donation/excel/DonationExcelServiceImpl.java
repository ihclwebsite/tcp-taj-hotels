/**
 * 
 */
package com.ihcl.core.donation.excel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.donation.excel.DonationExcelPojo;
import com.ihcl.core.shared.services.ResourceFetcherService;
import com.ihcl.tajhotels.email.api.IEmailService;

/**
 * @author TCS
 *
 */
@Component(	immediate = true,
			service = IDonationExcelService.class)
public class DonationExcelServiceImpl implements IDonationExcelService {

    public static final String PREFIX = "donationUsersData";

    public static final String SUFFIX = ".csv";

    private static final String CSV_SEPARATOR = ",";

    private static final String PROGRAM_NAME = "Taj InnerCircle";

    private final Logger LOG = LoggerFactory.getLogger(DonationExcelServiceImpl.class);

    @Reference
    private ResourceFetcherService resourceFetcherService;

    @Reference
    private IEmailService emailService;
    
	@Override
	public void generateExcelFromDonationNode(Resource donationExcelResource, String emailAddressTo, String emailAdddressCc,
			ResourceResolver resourceResolver) throws IOException {
        
    	final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        LOG.debug("file name before valling generate func::" + tempFile.getAbsolutePath());
        generateCsv(donationExcelResource, tempFile, resourceResolver);
        LOG.debug("file size after genrate func::" + tempFile.length());
        emailService.sendDonationExcel(tempFile, emailAddressTo, emailAdddressCc);
        tempFile.deleteOnExit();
	}
	
    private void generateCsv(Resource donationExcelResource, File tempFile, ResourceResolver resolver) throws IOException {

        try {

            FileWriter fileWriter = new FileWriter(tempFile);
            // adding header to csv
            String[] header = { "First Name", "Last Name","Email Id", "Pan Card Number",
                    "Donation Amount", "City", "Country", "Pincode", "Address Line One",
                    "Address Line two"};
         // add data to csv
            LOG.trace("donationExcelResource " + donationExcelResource);
            List<DonationExcelPojo> data = new ArrayList<DonationExcelPojo>();
            
            data = generateDataList(donationExcelResource, resolver);
            
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.append(Arrays.toString(header));
            bw.newLine();
            if (null != data) {
                for (DonationExcelPojo donationExcelPojo : data) {
                    StringBuffer sbf = new StringBuffer();
                    
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getFirstName()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getLastName()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getEmail()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getPancard()));

                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getDonation()));
                    
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getCity()));
                    
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getCountry()));
                    
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getPincode()));
                    
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getAddressLineOne()));
                    
                    sbf.append(CSV_SEPARATOR);
                    sbf.append(surroundQuotesWithNullCheck(donationExcelPojo.getAddressLineTwo()));

                    bw.write(sbf.toString());
                    bw.newLine();

                }
            }

            bw.flush();
            bw.close();

        } catch (Exception e) {
            LOG.error("exception caught::" + e.getMessage());
            e.printStackTrace();
        }


    }

    private String surroundQuotesWithNullCheck(String string) {
        String methodName = "surroundQuotesWithNullCheck";
        String surroundedString = string;
        LOG.trace("Method Entry: " + methodName);
        if (null != string) {


            if (string.contains(",")) {
                if (string.contains("\"")) {
                    string.replaceAll("\"", "\"\"");
                }
                surroundedString = "\"".concat(string).concat("\"");
            }
        } else {
            surroundedString = "";
        }
        LOG.trace("Method Exit: " + methodName);
        return surroundedString;
    }


    /**
     * @return
     * @throws ParseException
     * @throws RepositoryException
     * @throws ConstraintViolationException
     * @throws LockException
     * @throws VersionException
     * @throws ValueFormatException
     */
    private List<DonationExcelPojo> generateDataList(Resource donationExcelResource, ResourceResolver resolver)
            throws ParseException, ValueFormatException, VersionException, LockException, ConstraintViolationException,
            RepositoryException {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yy");
            Iterable<Resource> children = donationExcelResource.getChildren();
            List<DonationExcelPojo> listDonationPojo = new ArrayList<>();
            
            Node rootNode = donationExcelResource.adaptTo(Node.class);
            NodeIterator nodes = rootNode.getNodes();
            while (nodes.hasNext()) {
                Node yearNode = nodes.nextNode(); 	
                LOG.trace("Name of yearNode : " + yearNode.getName());
                if(yearNode.hasNodes()) {
                	NodeIterator monthNodeIterator = yearNode.getNodes();
                	while (monthNodeIterator.hasNext()) {
                		Node monthNode = monthNodeIterator.nextNode();
                		LOG.trace("Name of monthNode : " + monthNode.getName());
                		if(monthNode.hasNodes()) {
                			NodeIterator dayNodeIterator = monthNode.getNodes();
                			while (dayNodeIterator.hasNext()) {
                        		Node dayNode = dayNodeIterator.nextNode();
                        		LOG.trace("Name of dayNode : " + dayNode.getName());
                        		if(dayNode.hasNodes()) {
                        			NodeIterator timeNodeIterator = dayNode.getNodes();
                        			while (timeNodeIterator.hasNext()) {
                                		Node timeNode = timeNodeIterator.nextNode();
                                		LOG.trace("Name of timeNode : " + timeNode.getName());
                                		if(timeNode.hasNodes()) {
                                			NodeIterator userDataIterator = timeNode.getNodes();
                                			while (userDataIterator.hasNext()) {
                                        		Node userNode = userDataIterator.nextNode();
                                        		LOG.trace("Name of userNode : " + userNode.getName());
                                        		
                                        		DonationExcelPojo donationExcelPojo = new DonationExcelPojo();
                                        		donationExcelPojo.setFirstName(userNode.getProperty("FirstName").getValue().getString());
                                                donationExcelPojo.setLastName(userNode.getProperty("LastName").getValue().getString());
                                                donationExcelPojo.setEmail(userNode.getProperty("Email").getValue().getString());
                                                donationExcelPojo.setPancard(userNode.getProperty("PAN").getValue().getString());
                                                donationExcelPojo.setDonation(userNode.getProperty("Donation amount").getValue().getString());
                                                donationExcelPojo.setCity(userNode.getProperty("City").getValue().getString());
                                                donationExcelPojo.setCountry(userNode.getProperty("Country").getValue().getString());
                                                donationExcelPojo.setPincode(userNode.getProperty("Pincode").getValue().getString());
                                                donationExcelPojo.setAddressLineOne(userNode.getProperty("AddressLineOne").getValue().getString());
                                                if(userNode.getProperty("State").getValue().getString() != null) {
                                                	donationExcelPojo.setAddressLineTwo(userNode.getProperty("State").getValue().getString());
                                                }
                                        		listDonationPojo.add(donationExcelPojo);
                                			}
                                		}
                        			}
                        		}
                			}
                		}
                	}
                }
              }
            
//            for (Resource child : children) {
//
//            	DonationExcelPojo donationExcelPojo = new DonationExcelPojo();
//                LOG.trace("the path is generateDataList ::" + child.getPath());
//                
//                ValueMap donationValueMap = child.adaptTo(ValueMap.class);
//
//                String redemptionDate = donationValueMap.get("createdDate", String.class);
//                String redemDateStr = sdf1.format(sdf.parse(redemptionDate));
//                donationExcelPojo.setRedemption_date(redemDateStr);
//                donationExcelPojo.setPurchase_order_date(redemDateStr);
//                donationExcelPojo.setRdr_no(child.getName()); 
                
//                donationExcelPojo.setFirstName(donationValueMap.get("Name", String.class));
//                donationExcelPojo.setLastName(donationValueMap.get("lastName", String.class));
//                donationExcelPojo.setProgram_name(PROGRAM_NAME);
//                donationExcelPojo.setEmail(donationValueMap.get("email", String.class));
//                donationExcelPojo.setPancard(donationValueMap.get("pancard", String.class));
//                donationExcelPojo.setDonation(donationValueMap.get("donation", String.class));
//                donationExcelPojo.setCity(donationValueMap.get("city", String.class));
//                donationExcelPojo.setCountry(donationValueMap.get("country", String.class));
//                donationExcelPojo.setPincode(donationValueMap.get("pincode", String.class));
//                donationExcelPojo.setAddressLineOne(donationValueMap.get("addressLineOne", String.class));
//                donationExcelPojo.setAddressLineTwo(donationValueMap.get("addressLineTwo", String.class));
//                
//                listDonationPojo.add(donationExcelPojo);

//            }
            return listDonationPojo;

        } catch (Exception e) {
            LOG.error("exception caught::" + e.getMessage());
        }
        return null;
    }


}
