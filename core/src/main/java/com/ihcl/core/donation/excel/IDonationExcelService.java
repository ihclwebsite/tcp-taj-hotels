/**
 * 
 */
package com.ihcl.core.donation.excel;

import java.io.IOException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * @author TCS
 *
 */
public interface IDonationExcelService {

    void generateExcelFromDonationNode(Resource giftExcelResource, String emailAddressTo, String emailAdddressCc,
            ResourceResolver resourceResolver) throws IOException;

}
