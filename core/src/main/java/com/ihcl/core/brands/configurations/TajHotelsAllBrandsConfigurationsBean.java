/**
 *
 */
package com.ihcl.core.brands.configurations;

import java.util.Map;

/**
 * <pre>
 * TajHotelsBrandsConfigurationsBean Class
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  Feb 14, 2019     Neha Priyanka            Create
 *
 * </pre>
 */
public class TajHotelsAllBrandsConfigurationsBean {

    private Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean;


    /**
     * Getter for the field etcConfigBean
     *
     * @return the etcConfigBean
     */
    public Map<String, TajhotelsBrandAllEtcConfigBean> getEtcConfigBean() {
        return etcConfigBean;
    }


    /**
     * Setter for the field etcConfigBean
     *
     * @param etcConfigBean
     *            the etcConfigBean to set
     */

    public void setEtcConfigBean(Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean) {
        this.etcConfigBean = etcConfigBean;
    }

    @Override
    public String toString() {
        return "TajHotelsAllBrandsConfigurationsBean{" + "etcConfigBean=" + etcConfigBean + '}';
    }
}
