/**
 *
 */
package com.ihcl.core.brands.configurations;

import java.util.HashMap;
import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.tajhotels.constants.ReservationConstants;

/**
 * <pre>
* TajHotelsBrandsETCServiceImpl Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :Feb 14, 2019
 * @ClassName : TajHotelsBrandsETCServiceImpl.java
 * @Description : com.ihcl.core.brands.configurations
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  Feb 14, 2019     Neha Priyanka            Create
 *
 *               </pre>
 */


@Designate(ocd = TajHotelsBrandsETCServiceImpl.Configurations.class)
@Component(immediate = true,
        service = TajHotelsBrandsETCService.class)
public class TajHotelsBrandsETCServiceImpl implements TajHotelsBrandsETCService {


    private TajHotelsAllBrandsConfigurationsBean configBean;

    private static final Logger LOG = LoggerFactory.getLogger(TajHotelsBrandsETCServiceImpl.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Activate
    public void activate(Configurations config) {
        String configPath = config.getEtcConfigPath();
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(null);
            this.configBean = getConfigBeanFromETCConfigPath(configPath, resourceResolver);
        } catch (LoginException e) {
            LOG.error("LoginError in TajHotelsBrandsETCServiceImpl: {}", e.getMessage());
            LOG.debug("LoginError in TajHotelsBrandsETCServiceImpl: {}", e);
        } catch (Exception e) {
            LOG.error("Error in TajHotelsBrandsETCServiceImpl: {}", e.getMessage());
            LOG.debug("Error in TajHotelsBrandsETCServiceImpl: {}", e);
        } finally {
            if (null != resourceResolver) {
                resourceResolver.close();
            }
        }

    }

    /**
     * @param configPath
     * @param resourceResolver
     * @return
     */
    private TajHotelsAllBrandsConfigurationsBean getConfigBeanFromETCConfigPath(String configPath,
            ResourceResolver resourceResolver) {
        Resource etcResource = resourceResolver.getResource(configPath);
        if (null != etcResource) {
            Iterator<Resource> allBrandsConfigResource = etcResource.listChildren();
            return getTajHotelsAllBrandsConfigBean(allBrandsConfigResource);
        }
        return null;
    }

    /**
     * @param allBrandsConfigResource
     * @return
     */
    private TajHotelsAllBrandsConfigurationsBean getTajHotelsAllBrandsConfigBean(
            Iterator<Resource> allBrandsConfigResource) {
        HashMap<String, TajhotelsBrandAllEtcConfigBean> configMap = new HashMap<>();
        TajHotelsAllBrandsConfigurationsBean allBrandsConfigBean = new TajHotelsAllBrandsConfigurationsBean();
        while (allBrandsConfigResource.hasNext()) {
            try {
                Resource brandConfigResource = allBrandsConfigResource.next();
                Node brandConfigNode = brandConfigResource.adaptTo(Node.class);
                String brandId = brandConfigNode.hasProperty(ReservationConstants.BRAND_ID)
                        ? brandConfigNode.getProperty(ReservationConstants.BRAND_ID).getValue().getString()
                        : "";
                boolean amaBooking = brandConfigNode.hasProperty(ReservationConstants.IS_AMA_BOOKNG)
                        ? brandConfigNode.getProperty(ReservationConstants.IS_AMA_BOOKNG).getValue().getBoolean()
                        : false;
                if (StringUtils.isNotBlank(brandId)) {
                    HashMap<String, TajHotelBrandsETCConfigurations> functionConfigMap = getConfigurationsForInidvidualBrandPages(
                            brandConfigNode);
                    TajhotelsBrandAllEtcConfigBean individualBrandConfigBean = new TajhotelsBrandAllEtcConfigBean(
                            functionConfigMap, amaBooking);
                    configMap.put(brandId, individualBrandConfigBean);
                    allBrandsConfigBean.setEtcConfigBean(configMap);
                }
            } catch (Exception e) {
                LOG.error("Error in getConfigBeanFromETCConfigPath: {}", e.getMessage());
                LOG.debug("Error in getConfigBeanFromETCConfigPath: {}", e);
            }

        }
        return allBrandsConfigBean;
    }

    /**
     * @param brandConfigNode
     * @param brandId
     * @return
     * @throws RepositoryException
     * @throws PathNotFoundException
     */
    private HashMap<String, TajHotelBrandsETCConfigurations> getConfigurationsForInidvidualBrandPages(
            Node brandConfigNode) throws RepositoryException {
        Node childNode = brandConfigNode.getNode(ReservationConstants.CONFIG_NODE);
        Iterator<Node> configNodes = childNode.getNodes();
        HashMap<String, TajHotelBrandsETCConfigurations> configMap = new HashMap<>();
        while (configNodes.hasNext()) {
            Node configNode = configNodes.next();
            String functionId = configNode.hasProperty(ReservationConstants.FUNCTION_ID)
                    ? configNode.getProperty(ReservationConstants.FUNCTION_ID).getValue().getString()
                    : "";
            String ccAvenueAccessKey = configNode.hasProperty(ReservationConstants.CC_AVENUE_ACCESS_KEY)
                    ? configNode.getProperty(ReservationConstants.CC_AVENUE_ACCESS_KEY).getValue().getString()
                    : "";
            String ccAvenueCancelUrl = configNode.hasProperty(ReservationConstants.CC_AVENUE_CANCEL_URL)
                    ? configNode.getProperty(ReservationConstants.CC_AVENUE_CANCEL_URL).getValue().getString()
                    : "";
            String ccAvenueRedirectUrl = configNode.hasProperty(ReservationConstants.CC_AVENUE_REDIRECT_URL)
                    ? configNode.getProperty(ReservationConstants.CC_AVENUE_REDIRECT_URL).getValue().getString()
                    : "";
            String ccAvenueUrl = configNode.hasProperty(ReservationConstants.CC_AVENUE_URL)
                    ? configNode.getProperty(ReservationConstants.CC_AVENUE_URL).getValue().getString()
                    : "";
            String ccAvenueWorkingKey = configNode.hasProperty(ReservationConstants.CC_AVENUE_WORKING_KEY)
                    ? configNode.getProperty(ReservationConstants.CC_AVENUE_WORKING_KEY).getValue().getString()
                    : "";
            String paymentConfirmUrl = configNode.hasProperty(ReservationConstants.PAYMENT_CONFIRM_URL)
                    ? configNode.getProperty(ReservationConstants.PAYMENT_CONFIRM_URL).getValue().getString()
                    : "";
            String paymentMerchantID = configNode.hasProperty(ReservationConstants.PAYMENT_MERCHANT_ID)
                    ? configNode.getProperty(ReservationConstants.PAYMENT_MERCHANT_ID).getValue().getString()
                    : "";
            String contentRootPath = configNode.hasProperty(ReservationConstants.CONTENT_ROOT_PATH_ID)
                    ? configNode.getProperty(ReservationConstants.CONTENT_ROOT_PATH_ID).getValue().getString()
                    : "";
            TajHotelBrandsETCConfigurations etcConfigBean = new TajHotelBrandsETCConfigurations(functionId,
                    ccAvenueAccessKey, ccAvenueCancelUrl, ccAvenueRedirectUrl, ccAvenueUrl, ccAvenueWorkingKey,
                    paymentConfirmUrl, paymentMerchantID, contentRootPath);
            configMap.put(functionId, etcConfigBean);
        }
        return configMap;
    }

    @Override
    public TajHotelsAllBrandsConfigurationsBean getEtcConfigBean() {
        return this.configBean;
    }


    @ObjectClassDefinition(name = "Tajhotels Brands ETC Configurations",
            description = "Provides the path for the ETC Configurations for Tajhotels Brands")
    public @interface Configurations {

        @AttributeDefinition(name = "Tajhotels ETC Config Path",
                description = "Path for the Brands configurations ")
        String getEtcConfigPath() default "/etc/cloudservices/tajhotels";

    }

    /**
     * Setter for the field configBean
     *
     * @param configBean
     *            the configBean to set
     */

    public void setConfigBean(TajHotelsAllBrandsConfigurationsBean configBean) {
        this.configBean = configBean;
    }

}
