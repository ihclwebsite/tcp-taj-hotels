/**
 *
 */
package com.ihcl.core.brands.configurations;

import java.util.Map;

/**
 * <pre>
 * TajhotelsBrandAllEtcConfigBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :06-May-2019
 * @ClassName : TajhotelsBrandAllEtcConfigBean.java
 * @Description : com.ihcl.core.brands.configurations -TajhotelsBrandAllEtcConfigBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  06-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class TajhotelsBrandAllEtcConfigBean {

    private Map<String, TajHotelBrandsETCConfigurations> etcConfigBean;

    private boolean amaBooking;

    /**
     * @return the etcConfigBean
     */
    public Map<String, TajHotelBrandsETCConfigurations> getEtcConfigBean() {
        return etcConfigBean;
    }

    /**
     * @param etcConfigBean
     *            the etcConfigBean to set
     */
    public void setEtcConfigBean(Map<String, TajHotelBrandsETCConfigurations> etcConfigBean) {
        this.etcConfigBean = etcConfigBean;
    }

    /**
     * @return amaBooking
     */
    public boolean isAmaBooking() {
        return amaBooking;
    }

    /**
     * @param amaBooking
     */
    public void setAmaBooking(boolean amaBooking) {
        this.amaBooking = amaBooking;
    }

    /**
     * @param etcConfigBean
     */
    public TajhotelsBrandAllEtcConfigBean(Map<String, TajHotelBrandsETCConfigurations> etcConfigBean,
            boolean amaBooking) {
        super();
        this.etcConfigBean = etcConfigBean;
        this.amaBooking = amaBooking;
    }

    @Override
    public String toString() {
        return "TajhotelsBrandAllEtcConfigBean{" + "etcConfigBean=" + etcConfigBean + ", amaBooking=" + amaBooking +
                '}';
    }
}
