/**
 * 
 */
package com.ihcl.core.brands.configurations;

/**
 * <pre>
 * BookingETCConfigBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :06-May-2019
 * @ClassName : BookingETCConfigBean.java
 * @Description : com.ihcl.core.brands.configurations -BookingETCConfigBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  06-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class CCAvenueETCConfigBean {
	
	private String id;

    private String ccAvenueAccessKey;

    private String ccAvenueCancelUrl;

    private String ccAvenueRedirectUrl;

    private String ccAvenueUrl;

    private String ccAvenueWorkingKey;

    private String paymentConfirmUrl;

    private String paymentMerchantId;


    /**
     * Getter for the field id
     *
     * @return the id
     */
    public String getId() {
        return id;
    }


    /**
     * Setter for the field id
     *
     * @param id
     *            the id to set
     */

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Getter for the field ccAvenueAccessKey
     *
     * @return the ccAvenueAccessKey
     */
    public String getCcAvenueAccessKey() {
        return ccAvenueAccessKey;
    }


    /**
     * Setter for the field ccAvenueAccessKey
     *
     * @param ccAvenueAccessKey
     *            the ccAvenueAccessKey to set
     */

    public void setCcAvenueAccessKey(String ccAvenueAccessKey) {
        this.ccAvenueAccessKey = ccAvenueAccessKey;
    }


    /**
     * Getter for the field ccAvenueCancelUrl
     *
     * @return the ccAvenueCancelUrl
     */
    public String getCcAvenueCancelUrl() {
        return ccAvenueCancelUrl;
    }


    /**
     * Setter for the field ccAvenueCancelUrl
     *
     * @param ccAvenueCancelUrl
     *            the ccAvenueCancelUrl to set
     */

    public void setCcAvenueCancelUrl(String ccAvenueCancelUrl) {
        this.ccAvenueCancelUrl = ccAvenueCancelUrl;
    }


    /**
     * Getter for the field ccAvenueRedirectUrl
     *
     * @return the ccAvenueRedirectUrl
     */
    public String getCcAvenueRedirectUrl() {
        return ccAvenueRedirectUrl;
    }


    /**
     * Setter for the field ccAvenueRedirectUrl
     *
     * @param ccAvenueRedirectUrl
     *            the ccAvenueRedirectUrl to set
     */

    public void setCcAvenueRedirectUrl(String ccAvenueRedirectUrl) {
        this.ccAvenueRedirectUrl = ccAvenueRedirectUrl;
    }


    /**
     * Getter for the field ccAvenueUrl
     *
     * @return the ccAvenueUrl
     */
    public String getCcAvenueUrl() {
        return ccAvenueUrl;
    }


    /**
     * Setter for the field ccAvenueUrl
     *
     * @param ccAvenueUrl
     *            the ccAvenueUrl to set
     */

    public void setCcAvenueUrl(String ccAvenueUrl) {
        this.ccAvenueUrl = ccAvenueUrl;
    }


    /**
     * Getter for the field ccAvenueWorkingKey
     *
     * @return the ccAvenueWorkingKey
     */
    public String getCcAvenueWorkingKey() {
        return ccAvenueWorkingKey;
    }


    /**
     * Setter for the field ccAvenueWorkingKey
     *
     * @param ccAvenueWorkingKey
     *            the ccAvenueWorkingKey to set
     */

    public void setCcAvenueWorkingKey(String ccAvenueWorkingKey) {
        this.ccAvenueWorkingKey = ccAvenueWorkingKey;
    }


    /**
     * Getter for the field paymentConfirmUrl
     *
     * @return the paymentConfirmUrl
     */
    public String getPaymentConfirmUrl() {
        return paymentConfirmUrl;
    }


    /**
     * Setter for the field paymentConfirmUrl
     *
     * @param paymentConfirmUrl
     *            the paymentConfirmUrl to set
     */

    public void setPaymentConfirmUrl(String paymentConfirmUrl) {
        this.paymentConfirmUrl = paymentConfirmUrl;
    }


    /**
     * Getter for the field paymentMerchantId
     *
     * @return the paymentMerchantId
     */
    public String getPaymentMerchantId() {
        return paymentMerchantId;
    }


    /**
     * Setter for the field paymentMerchantId
     *
     * @param paymentMerchantId
     *            the paymentMerchantId to set
     */

    public void setPaymentMerchantId(String paymentMerchantId) {
        this.paymentMerchantId = paymentMerchantId;
    }


    /**
     * @param id
     * @param ccAvenueAccessKey
     * @param ccAvenueCancelUrl
     * @param ccAvenueRedirectUrl
     * @param ccAvenueUrl
     * @param ccAvenueWorkingKey
     * @param paymentConfirmUrl
     * @param paymentMerchantId
     */
    public CCAvenueETCConfigBean(String id, String ccAvenueAccessKey, String ccAvenueCancelUrl,
            String ccAvenueRedirectUrl, String ccAvenueUrl, String ccAvenueWorkingKey, String paymentConfirmUrl,
            String paymentMerchantId) {
        super();
        this.id = id;
        this.ccAvenueAccessKey = ccAvenueAccessKey;
        this.ccAvenueCancelUrl = ccAvenueCancelUrl;
        this.ccAvenueRedirectUrl = ccAvenueRedirectUrl;
        this.ccAvenueUrl = ccAvenueUrl;
        this.ccAvenueWorkingKey = ccAvenueWorkingKey;
        this.paymentConfirmUrl = paymentConfirmUrl;
        this.paymentMerchantId = paymentMerchantId;
    }


}
