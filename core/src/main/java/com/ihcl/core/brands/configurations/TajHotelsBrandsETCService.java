/**
 *
 */
package com.ihcl.core.brands.configurations;

/**
 * <pre>
 * TajHotelsBrandsETCService Class
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  Feb 14, 2019     Neha Priyanka            Create
 *
 * </pre>
 */
public interface TajHotelsBrandsETCService {

    TajHotelsAllBrandsConfigurationsBean getEtcConfigBean();


}
