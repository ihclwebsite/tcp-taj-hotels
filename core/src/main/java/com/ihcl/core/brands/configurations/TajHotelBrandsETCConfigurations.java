/**
 *
 */
package com.ihcl.core.brands.configurations;


/**
 * <pre>
 * TajHotelBrandsETCConfigurations Class
 *     since            author               description
 *  ===========     ==============   =========================
 *  Feb 14, 2019     Neha Priyanka            Create
 *
 * </pre>
 */
public class TajHotelBrandsETCConfigurations {


    private String id;

    private String ccAvenueAccessKey;

    private String ccAvenueCancelUrl;

    private String ccAvenueRedirectUrl;

    private String ccAvenueUrl;

    private String ccAvenueWorkingKey;

    private String paymentConfirmUrl;

    private String paymentMerchantId;

    private String contentRootPath;


    /**
     * Getter for the field id
     *
     * @return the id
     */
    public String getId() {
        return id;
    }


    /**
     * Setter for the field id
     *
     * @param id
     *            the id to set
     */

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Getter for the field ccAvenueAccessKey
     *
     * @return the ccAvenueAccessKey
     */
    public String getCcAvenueAccessKey() {
        return ccAvenueAccessKey;
    }


    /**
     * Setter for the field ccAvenueAccessKey
     *
     * @param ccAvenueAccessKey
     *            the ccAvenueAccessKey to set
     */

    public void setCcAvenueAccessKey(String ccAvenueAccessKey) {
        this.ccAvenueAccessKey = ccAvenueAccessKey;
    }


    /**
     * Getter for the field ccAvenueCancelUrl
     *
     * @return the ccAvenueCancelUrl
     */
    public String getCcAvenueCancelUrl() {
        return ccAvenueCancelUrl;
    }


    /**
     * Setter for the field ccAvenueCancelUrl
     *
     * @param ccAvenueCancelUrl
     *            the ccAvenueCancelUrl to set
     */

    public void setCcAvenueCancelUrl(String ccAvenueCancelUrl) {
        this.ccAvenueCancelUrl = ccAvenueCancelUrl;
    }


    /**
     * Getter for the field ccAvenueRedirectUrl
     *
     * @return the ccAvenueRedirectUrl
     */
    public String getCcAvenueRedirectUrl() {
        return ccAvenueRedirectUrl;
    }


    /**
     * Setter for the field ccAvenueRedirectUrl
     *
     * @param ccAvenueRedirectUrl
     *            the ccAvenueRedirectUrl to set
     */

    public void setCcAvenueRedirectUrl(String ccAvenueRedirectUrl) {
        this.ccAvenueRedirectUrl = ccAvenueRedirectUrl;
    }


    /**
     * Getter for the field ccAvenueUrl
     *
     * @return the ccAvenueUrl
     */
    public String getCcAvenueUrl() {
        return ccAvenueUrl;
    }


    /**
     * Setter for the field ccAvenueUrl
     *
     * @param ccAvenueUrl
     *            the ccAvenueUrl to set
     */

    public void setCcAvenueUrl(String ccAvenueUrl) {
        this.ccAvenueUrl = ccAvenueUrl;
    }


    /**
     * Getter for the field ccAvenueWorkingKey
     *
     * @return the ccAvenueWorkingKey
     */
    public String getCcAvenueWorkingKey() {
        return ccAvenueWorkingKey;
    }


    /**
     * Setter for the field ccAvenueWorkingKey
     *
     * @param ccAvenueWorkingKey
     *            the ccAvenueWorkingKey to set
     */

    public void setCcAvenueWorkingKey(String ccAvenueWorkingKey) {
        this.ccAvenueWorkingKey = ccAvenueWorkingKey;
    }


    /**
     * Getter for the field paymentConfirmUrl
     *
     * @return the paymentConfirmUrl
     */
    public String getPaymentConfirmUrl() {
        return paymentConfirmUrl;
    }


    /**
     * Setter for the field paymentConfirmUrl
     *
     * @param paymentConfirmUrl
     *            the paymentConfirmUrl to set
     */

    public void setPaymentConfirmUrl(String paymentConfirmUrl) {
        this.paymentConfirmUrl = paymentConfirmUrl;
    }


    /**
     * Getter for the field paymentMerchantId
     *
     * @return the paymentMerchantId
     */
    public String getPaymentMerchantId() {
        return paymentMerchantId;
    }


    /**
     * Setter for the field paymentMerchantId
     *
     * @param paymentMerchantId
     *            the paymentMerchantId to set
     */

    public void setPaymentMerchantId(String paymentMerchantId) {
        this.paymentMerchantId = paymentMerchantId;
    }


    /**
     * Getter for the field contentRootPath
     *
     * @return the contentRootPath
     */
    public String getContentRootPath() {
        return contentRootPath;
    }


    /**
     * Setter for the field contentRootPath
     *
     * @param contentRootPath
     *            the contentRootPath to set
     */

    public void setContentRootPath(String contentRootPath) {
        this.contentRootPath = contentRootPath;
    }


    /**
     * @param id
     * @param ccAvenueAccessKey
     * @param ccAvenueCancelUrl
     * @param ccAvenueRedirectUrl
     * @param ccAvenueUrl
     * @param ccAvenueWorkingKey
     * @param paymentConfirmUrl
     * @param paymentMerchantId
     */
    public TajHotelBrandsETCConfigurations(String id, String ccAvenueAccessKey, String ccAvenueCancelUrl,
            String ccAvenueRedirectUrl, String ccAvenueUrl, String ccAvenueWorkingKey, String paymentConfirmUrl,
            String paymentMerchantId, String contentRootPath) {
        super();
        this.id = id;
        this.ccAvenueAccessKey = ccAvenueAccessKey;
        this.ccAvenueCancelUrl = ccAvenueCancelUrl;
        this.ccAvenueRedirectUrl = ccAvenueRedirectUrl;
        this.ccAvenueUrl = ccAvenueUrl;
        this.ccAvenueWorkingKey = ccAvenueWorkingKey;
        this.paymentConfirmUrl = paymentConfirmUrl;
        this.paymentMerchantId = paymentMerchantId;
        this.contentRootPath = contentRootPath;
    }

    @Override
    public String toString() {
        return "TajHotelBrandsETCConfigurations{" + "id='" + id + '\'' + ", ccAvenueAccessKey='" + ccAvenueAccessKey +
                '\'' + ", ccAvenueCancelUrl='" + ccAvenueCancelUrl + '\'' + ", ccAvenueRedirectUrl='" +
                ccAvenueRedirectUrl + '\'' + ", ccAvenueUrl='" + ccAvenueUrl + '\'' + ", ccAvenueWorkingKey='" +
                ccAvenueWorkingKey + '\'' + ", paymentConfirmUrl='" + paymentConfirmUrl + '\'' +
                ", paymentMerchantId='" + paymentMerchantId + '\'' + ", contentRootPath='" + contentRootPath + '\'' +
                '}';
    }
}
