/**
 *
 */
package com.ihcl.core.currency;

import com.ihcl.loyalty.dto.CurrencyResponsePojo;

/**
 * @author Nutan
 *
 */
public interface ICurrencyFetcher {

    /**
     * @param currency
     */
    CurrencyResponsePojo convertCurrency(String currency);


}
