/**
 *
 */
package com.ihcl.core.currency;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.net.HttpHeaders;
import com.ihcl.integration.rest.service.AuthorizationInfo;
import com.ihcl.integration.rest.service.Header;
import com.ihcl.integration.rest.service.api.IRestService;
import com.ihcl.loyalty.config.ISibelConfiguration;
import com.ihcl.loyalty.dto.CurrencyResponsePojo;
import com.ihcl.loyalty.services.ISiebelObjectsBuilderService;
import com.ihcl.loyalty.services.impl.TransferPointsServiceImpl;

/**
 * @author shwetha
 *
 */
@Component(immediate = true,
        service = ICurrencyFetcher.class)
public class CurrencyFetcherImpl implements ICurrencyFetcher {

    private final Logger LOG = LoggerFactory.getLogger(TransferPointsServiceImpl.class);

    @Reference
    private IRestService restService;

    @Reference
    private ISiebelObjectsBuilderService siebelObjectBuilderService;

    @Reference
    private ISibelConfiguration siebelConfig;


    @Override
    public CurrencyResponsePojo convertCurrency(String currency) {

        LOG.debug("inside the TransferPoints method");

        String requestJson = null;
        CurrencyResponsePojo currencyResponsePojo = new CurrencyResponsePojo();
        try {
            requestJson = siebelObjectBuilderService.buildCurrencyConvertorRequest(currency);
            LOG.debug("print the request" + requestJson);

            String username = siebelConfig.getUsername();
            String password = siebelConfig.getPassword();
            AuthorizationInfo authInfo = new AuthorizationInfo();
            authInfo.setPassword(password);
            authInfo.setUsername(username);

            Header contentType = new Header(HttpHeaders.CONTENT_TYPE, "application/json");
            List<Header> headers = new ArrayList<>();
            headers.add(contentType);
            String response = restService.invokePost(siebelConfig.getHostUrl(),
                    siebelConfig.getCurrencyConvertorEndPoint(), authInfo, headers, requestJson);

            LOG.debug("Response received from service as: " + response);
            currencyResponsePojo = siebelObjectBuilderService.buildCurrencyConvertorResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return currencyResponsePojo;

    }

}
