package com.ihcl.core.workflow;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.granite.asset.api.AssetManager;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;

/**
 * @author Ravindar Dev
 * This Process step will copy sitemap.xml to the configured ROOT_PATH
 */
@Component(service = WorkflowProcess.class,immediate = true,
property = {
		"process.label=Workflow to copy sitemap.xml to root folder"
})
public class CopySiteMap implements WorkflowProcess {
	    
	Logger logger = LoggerFactory.getLogger(CopySiteMap.class);
	
	private static final String ROOT_PATH = "/content/tajhotels/en-in/sitemap.xml";
	
	@Reference 
	ResourceResolverFactory resourceResolverFactory;
	
	@Override
	public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap) throws WorkflowException {
		String payload = (String)workItem.getWorkflowData().getPayload();
		logger.info("Payload >>>>"+ payload);
		try {
			ResourceResolver resourceResolver= resourceResolverFactory.getServiceResourceResolver(null);
			AssetManager assetManager = resourceResolver.adaptTo(AssetManager.class);
			if(assetManager.assetExists(ROOT_PATH)) {
				assetManager.removeAsset(ROOT_PATH);
				resourceResolver.commit();
			}
			assetManager.copyAsset(payload,ROOT_PATH);
			resourceResolver.commit();
		} catch (Exception e) {
			logger.error("Error/Exception is thrown executing sitemap copy workflow" + e.toString(),e);
		}

	}
	
}
