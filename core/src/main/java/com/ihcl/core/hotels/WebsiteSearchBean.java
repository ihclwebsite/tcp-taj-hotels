package com.ihcl.core.hotels;

import java.util.HashMap;
import java.util.List;

public class WebsiteSearchBean {

    private List<SearchBean> holidays;

    private List<SearchBean> holidayHotels;

    private List<SearchBean> destinations;

    private List<SearchBean> suggestions;

    private HashMap<String, List<SearchBean>> hotels;

    private HashMap<String, List<SearchBean>> restaurants;

    private HashMap<String, List<SearchBean>> experiences;

    public HashMap<String, List<SearchBean>> getHotels() {
        return hotels;
    }

    public void setHotels(HashMap<String, List<SearchBean>> hotels) {
        this.hotels = hotels;
    }

    public HashMap<String, List<SearchBean>> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(HashMap<String, List<SearchBean>> restaurants) {
        this.restaurants = restaurants;
    }

    public HashMap<String, List<SearchBean>> getExperiences() {
        return experiences;
    }

    public void setExperiences(HashMap<String, List<SearchBean>> experiences) {
        this.experiences = experiences;
    }

    public List<SearchBean> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<SearchBean> holidays) {
        this.holidays = holidays;
    }

    public List<SearchBean> getHolidayHotels() {
        return holidayHotels;
    }

    public void setHolidayHotels(List<SearchBean> holidayHotels) {
        this.holidayHotels = holidayHotels;
    }

    public List<SearchBean> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<SearchBean> destinations) {
        this.destinations = destinations;
    }

    public List<SearchBean> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<SearchBean> suggestions) {
        this.suggestions = suggestions;
    }

    public WebsiteSearchBean(List<SearchBean> holidays, List<SearchBean> holidayHotels, List<SearchBean> destinations,
            List<SearchBean> suggestions, HashMap<String, List<SearchBean>> hotels,
            HashMap<String, List<SearchBean>> restaurants, HashMap<String, List<SearchBean>> experiences) {
        super();
        this.holidays = holidays;
        this.holidayHotels = holidayHotels;
        this.destinations = destinations;
        this.suggestions = suggestions;
        this.hotels = hotels;
        this.restaurants = restaurants;
        this.experiences = experiences;
    }


}
