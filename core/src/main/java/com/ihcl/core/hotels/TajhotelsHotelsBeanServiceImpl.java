/**
 * 
 */
package com.ihcl.core.hotels;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;


/**
 * <pre>
 * TajhotelsHotelsBeanServiceImpl Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :24-May-2019
 * @ClassName : TajhotelsHotelsBeanServiceImpl.java
 * @Description : com.ihcl.core.hotels -TajhotelsHotelsBeanServiceImpl.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  24-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */

@Component(immediate = true,
service = TajhotelsHotelsBeanService.class)
public class TajhotelsHotelsBeanServiceImpl implements TajhotelsHotelsBeanService {
	
	private WebsiteSpecificHotelsBean websiteSpecificHotelsBean;
	/**
	 * @return the websiteSpecificHotelsBean
	 */
	@Override
	public WebsiteSpecificHotelsBean getWebsiteSpecificHotelsBean() {
		return websiteSpecificHotelsBean;
	}

	/**
	 * @param websiteSpecificHotelsBean the websiteSpecificHotelsBean to set
	 */
	@Override
	public void setWebsiteSpecificHotelsBean(WebsiteSpecificHotelsBean websiteSpecificHotelsBean) {
		this.websiteSpecificHotelsBean = websiteSpecificHotelsBean;
	}
	
	@Activate
	public void activate() {
		GetAllTajHotelsScheduler scheduleObj = new GetAllTajHotelsScheduler();
		scheduleObj.run();
	}


}
