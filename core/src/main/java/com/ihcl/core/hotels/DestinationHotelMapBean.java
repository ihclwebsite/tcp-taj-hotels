/**
 * 
 */
package com.ihcl.core.hotels;

import java.util.Map;

/**
 * <pre>
 * DestinationHotelMapBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :23-May-2019
 * @ClassName : DestinationHotelMapBean.java
 * @Description : com.ihcl.core.hotels -DestinationHotelMapBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  23-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class DestinationHotelMapBean {
	
	Map<String, DestinationHotelBean> destinationMap;

	/**
	 * @return the destinationMap
	 */
	public Map<String, DestinationHotelBean> getDestinationMap() {
		return destinationMap;
	}

	/**
	 * @param destinationMap the destinationMap to set
	 */
	public void setDestinationMap(Map<String, DestinationHotelBean> destinationMap) {
		this.destinationMap = destinationMap;
	}

	/**
	 * @param destinationMap
	 */
	public DestinationHotelMapBean(Map<String, DestinationHotelBean> destinationMap) {
		super();
		this.destinationMap = destinationMap;
	}
	
}
