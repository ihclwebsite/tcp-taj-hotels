/**
 * 
 */
package com.ihcl.core.hotels;

/**
 * <pre>
 * BrandDetailsBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :23-May-2019
 * @ClassName : BrandDetailsBean.java
 * @Description : com.ihcl.core.hotels -BrandDetailsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  23-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class BrandDetailsBean {
	
	private String brandName;
	private String brandPath;
	private HotelBean hotelsBean;
	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * @return the brandPath
	 */
	public String getBrandPath() {
		return brandPath;
	}
	/**
	 * @param brandPath the brandPath to set
	 */
	public void setBrandPath(String brandPath) {
		this.brandPath = brandPath;
	}
	/**
	 * @return the hotelsBean
	 */
	public HotelBean getHotelsBean() {
		return hotelsBean;
	}
	/**
	 * @param hotelsBean the hotelsBean to set
	 */
	public void setHotelsBean(HotelBean hotelsBean) {
		this.hotelsBean = hotelsBean;
	}
	/**
	 * @param brandName
	 * @param brandPath
	 * @param hotelsBean
	 */
	public BrandDetailsBean(String brandName, String brandPath, HotelBean hotelsBean) {
		super();
		this.brandName = brandName;
		this.brandPath = brandPath;
		this.hotelsBean = hotelsBean;
	}
	
	

}
