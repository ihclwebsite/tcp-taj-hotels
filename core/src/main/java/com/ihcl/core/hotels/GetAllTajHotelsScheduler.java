/**
 * 
 */
package com.ihcl.core.hotels;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.services.search.SearchProvider;

/**
 * <pre>
 * GetAllTajHotelsScheduler Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : GetAllTajHotelsScheduler.java
 * @Description : com.ihcl.core.hotels -GetAllTajHotelsScheduler.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */

@Designate(ocd=GetAllTajHotelsScheduler.Config.class)
@Component(service=Runnable.class)
public class GetAllTajHotelsScheduler implements Runnable{

	private static final Logger EXCEPTION_LOGGER = LoggerFactory.getLogger(GetAllTajHotelsScheduler.class);
	private boolean tajhotelsSchedulerEnabled;
    private String[] searchPaths;
	
	@Reference
    SearchProvider searchProvider;
	
	@Reference
	ResourceResolverFactory resolverFactory;
	
	@Reference
	TajhotelsHotelsBeanService hotelBeanService;
	
	
	@Override
	public void run() {
		if (tajhotelsSchedulerEnabled) {
			EXCEPTION_LOGGER.info("tajhotels scheduler started");
			ResourceResolver resolver = null;
			try {
				resolver = resolverFactory.getServiceResourceResolver(null);
				GetAllTajhotelsHotelsMapHelper helper = new GetAllTajhotelsHotelsMapHelper();
				WebsiteSpecificHotelsBean websiteBean = helper.createTajhotelsAllHotelsMap(searchPaths, resolver);
				hotelBeanService.setWebsiteSpecificHotelsBean(websiteBean);
			} catch (Exception e) {
				EXCEPTION_LOGGER.error("Problem running GetAllTajHotelsScheduler {} ",e.getMessage());
				EXCEPTION_LOGGER.debug("Problem running GetAllTajHotelsScheduler {} ",e);
			} finally {
				if (null != resolver) {
					resolver.close();
				}
				EXCEPTION_LOGGER.info("GetAllTajHotelsScheduler Ended");
			}
		}
	}
	 
	
	@Activate
	@Modified
	public void activate(Config config) {
		tajhotelsSchedulerEnabled = config.enableScheduler();
		searchPaths = config.schedulerPath();
		run();
		}
	
	@ObjectClassDefinition(name="Tajhotels - Get All TajHotels Hotels Bean Schedulers",
            description = "This job will get the list of all the hotels")
	public @interface Config {
		
		@AttributeDefinition(name = "Cron expression defining when this Scheduled Service will run", 
			description = "[every minute = 0 * * * * ?], [01:00am daily = 0 0 1 * * ?]")
		String scheduler_expression() default "0 0 0/1 * * ?";
		
		 @AttributeDefinition(name = "Concurrent task",
             description = "Whether or not to schedule this task concurrently")
		 boolean scheduler_concurrent() default false;
		
		@AttributeDefinition(name = "Enable Get All TajHotels Scheduler", description = "ex: true or false")
		boolean enableScheduler();
		
		@AttributeDefinition(name = "Path where the scheduler should run", description = "ex: /content/tajhotels/en-in")
		String[] schedulerPath();
		
		
	}

}
