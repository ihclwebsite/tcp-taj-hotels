/**
 *
 */
package com.ihcl.core.hotels;


/**
 * <pre>
 * Amenities Class
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :16-Jul-2019
 * @ClassName : Amenities.java
 * @Description : com.ihcl.core.hotels -Amenities.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since               author               description
 *  ===========     ======================   =========================
 *   16-Jul-2019    	Subharun Mukherjee           Create
 *
 *               </pre>
 */
public class Amenities {

    private String type;

    private String signatureFeature;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSignatureFeature() {
        return signatureFeature;
    }

    public void setSignatureFeature(String signatureFeature) {
        this.signatureFeature = signatureFeature;
    }

    public Amenities(String type, String signatureFeature) {
        super();
        this.type = type;
        this.signatureFeature = signatureFeature;
    }


}
