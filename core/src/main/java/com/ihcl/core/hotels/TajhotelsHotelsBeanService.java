/**
 *
 */
package com.ihcl.core.hotels;

/**
 * <pre>
 * TajHotelsBrandsETCService Class
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  Feb 14, 2019     Neha Priyanka            Create
 *
 * </pre>
 */
public interface TajhotelsHotelsBeanService {

    WebsiteSpecificHotelsBean getWebsiteSpecificHotelsBean();
    void setWebsiteSpecificHotelsBean(WebsiteSpecificHotelsBean mapBean);

}
