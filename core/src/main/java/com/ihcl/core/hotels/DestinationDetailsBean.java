package com.ihcl.core.hotels;

public class DestinationDetailsBean {
	
	private String destinationName;
	private String destinationPath;
	private String destinationTitle;
	private String destinationImage;
	private String destinationCityTag;
	private String destinationId;
	private String websiteId;
	private String navTitle;
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getDestinationPath() {
		return destinationPath;
	}
	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}
	public String getDestinationTitle() {
		return destinationTitle;
	}
	public void setDestinationTitle(String destinationTitle) {
		this.destinationTitle = destinationTitle;
	}
	public String getDestinationImage() {
		return destinationImage;
	}
	public void setDestinationImage(String destinationImage) {
		this.destinationImage = destinationImage;
	}
	public String getDestinationCityTag() {
		return destinationCityTag;
	}
	public void setDestinationCityTag(String destinationCityTag) {
		this.destinationCityTag = destinationCityTag;
	}
	public String getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	public String getNavTitle() {
		return navTitle;
	}
	public void setNavTitle(String navTitle) {
		this.navTitle = navTitle;
	}
	public String getWebsiteId() {
		return websiteId;
	}
	public void setWebsiteId(String websiteId) {
		this.websiteId = websiteId;
	}
	public DestinationDetailsBean(String destinationName, String destinationPath, String destinationTitle,
			String destinationImage, String destinationCityTag, String destinationId, String websiteId, String navTitle) {
		super();
		this.destinationName = destinationName;
		this.destinationPath = destinationPath;
		this.destinationTitle = destinationTitle;
		this.destinationImage = destinationImage;
		this.destinationCityTag = destinationCityTag;
		this.destinationId = destinationId;
		this.websiteId = websiteId;
		this.navTitle = navTitle;
	}
	
	

}
