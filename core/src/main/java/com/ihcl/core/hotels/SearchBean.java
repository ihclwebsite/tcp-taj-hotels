package com.ihcl.core.hotels;

public class SearchBean {

    private String title;

    private String path;

    private String id;

    private String ihclOurHotelsBrand;

    private String maxGuests;

    private String maxBeds;
    
    private String sameDayCheckout;
    
    public String getIsOnlyBungalowPage() {
		return isOnlyBungalowPage;
	}

	public void setIsOnlyBungalowPage(String isOnlyBungalowPage) {
		this.isOnlyBungalowPage = isOnlyBungalowPage;
	}

	private String isOnlyBungalowPage;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaxGuests() {
        return maxGuests;
    }


    public void setMaxGuests(String maxGuests) {
        this.maxGuests = maxGuests;
    }

    public String getMaxBeds() {
        return maxBeds;
    }

    public void setMaxBeds(String maxBeds) {
        this.maxBeds = maxBeds;
    }

    public String getIhclOurHotelsBrand() {
        return ihclOurHotelsBrand;
    }

    public void setIhclOurHotelsBrand(String ihclOurHotelsBrand) {
        this.ihclOurHotelsBrand = ihclOurHotelsBrand;
    }

    public String getSameDayCheckout() {
		return sameDayCheckout;
	}

	public void setSameDayCheckout(String sameDayCheckout) {
		this.sameDayCheckout = sameDayCheckout;
	}

	public SearchBean(String title, String path, String id, String maxGuests, String maxBeds, String sameDayCheckout, String isOnlyBungalowPage) {
        super();
        this.title = title;
        this.path = path;
        this.id = id;
        this.maxGuests = maxGuests;
        this.maxBeds = maxBeds;
        this.sameDayCheckout = sameDayCheckout;
        this.isOnlyBungalowPage= isOnlyBungalowPage;
    }
    
    
    public SearchBean(String title, String path, String id, String maxGuests, String maxBeds) {
        super();
        this.title = title;
        this.path = path;
        this.id = id;
        this.maxGuests = maxGuests;
        this.maxBeds = maxBeds;
    }

    public SearchBean(String title, String path, String id, String ihclOurHotelsBrand) {
        super();
        this.title = title;
        this.path = path;
        this.id = id;
        this.ihclOurHotelsBrand = ihclOurHotelsBrand;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SearchBean) {
            return ((SearchBean) obj).title == title;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.title.hashCode();
    }


}
