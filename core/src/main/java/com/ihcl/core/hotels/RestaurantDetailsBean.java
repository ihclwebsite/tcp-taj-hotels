/**
 *
 */
package com.ihcl.core.hotels;

import java.util.List;

import org.json.JSONObject;


/**
 * <pre>
 * RestaurantDetailsBean Class
 * </pre>
 *
 *
 *
 * @author : Subharun Mukherjee
 * @version : 1.0
 * @see
 * @since :05-Jul-2019
 * @ClassName : RestaurantDetailsBean.java
 * @Description : com.ihcl.core.hotels -RestaurantDetailsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since               author               description
 *  ===========     ======================   =========================
 *   05-Jul-2019        Subharun Mukherjee           Create
 *
 *               </pre>
 */
public class RestaurantDetailsBean {

    private String diningName;

    private String diningBrand;

    private String diningShortDesc;

    private String diningDescription;

    private String diningLongDescription;

    private String diningImage;

    private String diningPhoneNumber;

    private List<String> diningCuisine;

    private String dressCode;

    private String ticPoints;

    private String epicurePoints;

    private String averagePrice;

    private String taxDisclaimer;

    private String resourceURL;

    private String diningID;

    private String offerSpecifics;

    private String discountValue;

    private List<Amenities> galleryImagePaths;

    private List<JSONObject> galleryCarousel;

    private Integer numberOfImages;

    private String currencySymbol;

    private String rateReview;

    private String pathToDownloadFrom;

    private String timingslabel1;

    private String timingslabel2;

    private String timingslabel3;

    private String timingsvalue1;

    private String timingsvalue2;

    private String timingsvalue3;

    private String diningLongitude;

    private String diningLatitude;

    private String hotelArea;

    private String hotelCity;

    private String hotelPinCode;

    private String hotelCountry;

    private String restaurantAddress;

    private String hotelName;


    public String getHotelName() {
        return hotelName;
    }


    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }


    public String getDiningName() {
        return diningName;
    }


    public void setDiningName(String diningName) {
        this.diningName = diningName;
    }

    public String getDiningBrand() {
        return diningBrand;
    }


    public void setDiningBrand(String diningBrand) {
        this.diningBrand = diningBrand;
    }


    public String getDiningShortDesc() {
        return diningShortDesc;
    }


    public void setDiningShortDesc(String diningShortDesc) {
        this.diningShortDesc = diningShortDesc;
    }


    public String getDiningDescription() {
        return diningDescription;
    }


    public void setDiningDescription(String diningDescription) {
        this.diningDescription = diningDescription;
    }


    public String getDiningLongDescription() {
        return diningLongDescription;
    }


    public void setDiningLongDescription(String diningLongDescription) {
        this.diningLongDescription = diningLongDescription;
    }


    public String getDiningImage() {
        return diningImage;
    }


    public void setDiningImage(String diningImage) {
        this.diningImage = diningImage;
    }


    public String getDiningPhoneNumber() {
        return diningPhoneNumber;
    }


    public void setDiningPhoneNumber(String diningPhoneNumber) {
        this.diningPhoneNumber = diningPhoneNumber;
    }


    public List<String> getDiningCuisine() {
        return diningCuisine;
    }


    public void setDiningCuisine(List<String> diningCuisine) {
        this.diningCuisine = diningCuisine;
    }


    public String getDressCode() {
        return dressCode;
    }


    public void setDressCode(String dressCode) {
        this.dressCode = dressCode;
    }

    public String getTicPoints() {
        return ticPoints;
    }


    public void setTicPoints(String ticPoints) {
        this.ticPoints = ticPoints;
    }


    public String getEpicurePoints() {
        return epicurePoints;
    }


    public void setEpicurePoints(String epicurePoints) {
        this.epicurePoints = epicurePoints;
    }


    public String getAveragePrice() {
        return averagePrice;
    }


    public void setAveragePrice(String averagePrice) {
        this.averagePrice = averagePrice;
    }


    public String getTaxDisclaimer() {
        return taxDisclaimer;
    }


    public void setTaxDisclaimer(String taxDisclaimer) {
        this.taxDisclaimer = taxDisclaimer;
    }


    public String getResourceURL() {
        return resourceURL;
    }


    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }


    public String getDiningID() {
        return diningID;
    }


    public void setDiningID(String diningID) {
        this.diningID = diningID;
    }


    public String getOfferSpecifics() {
        return offerSpecifics;
    }


    public void setOfferSpecifics(String offerSpecifics) {
        this.offerSpecifics = offerSpecifics;
    }


    public String getDiscountValue() {
        return discountValue;
    }


    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }


    public List<Amenities> getGalleryImagePaths() {
        return galleryImagePaths;
    }


    public void setGalleryImagePaths(List<Amenities> galleryImagePaths) {
        this.galleryImagePaths = galleryImagePaths;
    }


    public Integer getNumberOfImages() {
        return numberOfImages;
    }


    public void setNumberOfImages(Integer numberOfImages) {
        this.numberOfImages = numberOfImages;
    }


    public String getCurrencySymbol() {
        return currencySymbol;
    }


    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }


    public String getRateReview() {
        return rateReview;
    }


    public void setRateReview(String rateReview) {
        this.rateReview = rateReview;
    }


    public String getPathToDownloadFrom() {
        return pathToDownloadFrom;
    }


    public void setPathToDownloadFrom(String pathToDownloadFrom) {
        this.pathToDownloadFrom = pathToDownloadFrom;
    }


    public String getTimingslabel1() {
        return timingslabel1;
    }


    public void setTimingslabel1(String timingslabel1) {
        this.timingslabel1 = timingslabel1;
    }


    public String getTimingslabel2() {
        return timingslabel2;
    }


    public void setTimingslabel2(String timingslabel2) {
        this.timingslabel2 = timingslabel2;
    }


    public String getTimingslabel3() {
        return timingslabel3;
    }


    public void setTimingslabel3(String timingslabel3) {
        this.timingslabel3 = timingslabel3;
    }


    public String getTimingsvalue1() {
        return timingsvalue1;
    }


    public void setTimingsvalue1(String timingsvalue1) {
        this.timingsvalue1 = timingsvalue1;
    }


    public String getTimingsvalue2() {
        return timingsvalue2;
    }


    public void setTimingsvalue2(String timingsvalue2) {
        this.timingsvalue2 = timingsvalue2;
    }


    public String getTimingsvalue3() {
        return timingsvalue3;
    }


    public void setTimingsvalue3(String timingsvalue3) {
        this.timingsvalue3 = timingsvalue3;
    }


    public String getDiningLongitude() {
        return diningLongitude;
    }


    public void setDiningLongitude(String diningLongitude) {
        this.diningLongitude = diningLongitude;
    }


    public String getDiningLatitude() {
        return diningLatitude;
    }


    public void setDiningLatitude(String diningLatitude) {
        this.diningLatitude = diningLatitude;
    }


    public String getHotelArea() {
        return hotelArea;
    }


    public void setHotelArea(String hotelArea) {
        this.hotelArea = hotelArea;
    }


    public String getHotelCity() {
        return hotelCity;
    }


    public void setHotelCity(String hotelCity) {
        this.hotelCity = hotelCity;
    }


    public String getHotelPinCode() {
        return hotelPinCode;
    }


    public void setHotelPinCode(String hotelPinCode) {
        this.hotelPinCode = hotelPinCode;
    }


    public String getHotelCountry() {
        return hotelCountry;
    }


    public void setHotelCountry(String hotelCountry) {
        this.hotelCountry = hotelCountry;
    }


    public String getRestaurantAddress() {
        return restaurantAddress;
    }


    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }


    /**
     * Getter for the field galleryCarousel
     *
     * @return the galleryCarousel
     */
    public List<JSONObject> getGalleryCarousel() {
        return galleryCarousel;
    }


    /**
     * Setter for the field galleryCarousel
     *
     * @param galleryCarousel
     *            the galleryCarousel to set
     */

    public void setGalleryCarousel(List<JSONObject> galleryCarousel) {
        this.galleryCarousel = galleryCarousel;
    }


    public RestaurantDetailsBean(String diningName, String diningBrand, String diningShortDesc,
            String diningDescription, String diningLongDescription, String diningImage, String diningPhoneNumber,
            List<String> diningCuisine, String dressCode, String ticPoints, String epicurePoints, String averagePrice,
            String taxDisclaimer, String resourceURL, String diningID, String offerSpecifics, String discountValue,
            List<Amenities> galleryImagePaths, List<JSONObject> galleryCarousel, Integer numberOfImages,
            String currencySymbol, String rateReview, String pathToDownloadFrom, String timingslabel1,
            String timingslabel2, String timingslabel3, String timingsvalue1, String timingsvalue2,
            String timingsvalue3, String diningLongitude, String diningLatitude, String hotelArea, String hotelCity,
            String hotelPinCode, String hotelCountry, String restaurantAddress, String hotelName) {
        super();
        this.diningName = diningName;
        this.diningBrand = diningBrand;
        this.diningShortDesc = diningShortDesc;
        this.diningDescription = diningDescription;
        this.diningLongDescription = diningLongDescription;
        this.diningImage = diningImage;
        this.diningPhoneNumber = diningPhoneNumber;
        this.diningCuisine = diningCuisine;
        this.dressCode = dressCode;
        this.ticPoints = ticPoints;
        this.epicurePoints = epicurePoints;
        this.averagePrice = averagePrice;
        this.taxDisclaimer = taxDisclaimer;
        this.resourceURL = resourceURL;
        this.diningID = diningID;
        this.offerSpecifics = offerSpecifics;
        this.discountValue = discountValue;
        this.galleryImagePaths = galleryImagePaths;
        this.galleryCarousel = galleryCarousel;
        this.numberOfImages = numberOfImages;
        this.currencySymbol = currencySymbol;
        this.rateReview = rateReview;
        this.pathToDownloadFrom = pathToDownloadFrom;
        this.timingslabel1 = timingslabel1;
        this.timingslabel2 = timingslabel2;
        this.timingslabel3 = timingslabel3;
        this.timingsvalue1 = timingsvalue1;
        this.timingsvalue2 = timingsvalue2;
        this.timingsvalue3 = timingsvalue3;
        this.diningLongitude = diningLongitude;
        this.diningLatitude = diningLatitude;
        this.hotelArea = hotelArea;
        this.hotelCity = hotelCity;
        this.hotelPinCode = hotelPinCode;
        this.hotelCountry = hotelCountry;
        this.restaurantAddress = restaurantAddress;
        this.hotelName = hotelName;
    }


}
