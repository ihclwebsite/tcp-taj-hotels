/**
 *
 */
package com.ihcl.core.hotels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.ihcl.core.services.search.SearchServiceConstants.RESOURCETYPE;

/**
 * <pre>
 * GetAllTajhotelsHotelsMapHelper Class
 * </pre>
 *
 * .
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : GetAllTajhotelsHotelsMapHelper.java
 * @Description : com.ihcl.core.hotels -GetAllTajhotelsHotelsMapHelper.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class GetAllTajhotelsHotelsMapHelper {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAllTajhotelsHotelsMapHelper.class);

    private static final String BLANK_VALUE = "";

    /**
     * Creates the tajhotels all hotels map.
     *
     * @param searchPaths
     *            the search paths
     * @param resolver
     *            the resolver
     * @param galleryImageFetcherService
     * @return the website specific hotels bean
     */
    public WebsiteSpecificHotelsBean createTajhotelsAllHotelsMap(String[] searchPaths, ResourceResolver resolver) {
        Map<String, DestinationHotelMapBean> websiteMap = new HashMap<>();
        try {
            for (String searchPath : searchPaths) {
                HashMap<String, String> predicates = new HashMap<>();
                if (searchPath.contains("/ihclcb/")) {
                    predicates.put("path", searchPath);
                } else {
                    predicates.put("path", searchPath + "/our-hotels");
                }
                predicates.put("1_property", "sling:resourceType");
                predicates.put("1_property.value", RESOURCETYPE.DESTINATION);
                predicates.put("p.limit", "-1");
                SearchResult searchResults = getQueryResult(predicates, resolver);
                Iterator<Node> nodeIterator = searchResults.getNodes();
                Map<String, DestinationHotelBean> destinationHotelMap = new TreeMap<>();
                while (nodeIterator.hasNext()) {
                	String navTitle = "";
                    Node destinationNode = nodeIterator.next().getParent();
                    String destinationName = destinationNode.getName();
                    String destinationPath = destinationNode.getPath();
                    String destinationTitle = destinationNode.getProperty("jcr:content/jcr:title").getValue()
                            .getString();
                    if(destinationNode.hasProperty("jcr:content/anchorTitle")) {
                    navTitle = destinationNode.getProperty("jcr:content/anchorTitle").getValue()
                            .getString();
                    }else {
//                    	LOGGER.trace("Property not Found", destinationNode);
                    }
                    String imagePath = getNodeProperty(destinationNode,
                            "jcr:content/banner_parsys/hotel_banner/bannerImage");
                    String destinationId = destinationNode.getProperty("jcr:content/jcr:uuid").getValue().getString();
                    Value[] destinationLocValues = destinationNode.getProperty("jcr:content/city").getValues();
                    String destinationLocTag = destinationLocValues.length > 0 ? destinationLocValues[0].getString()
                            : null;
                    Map<String, BrandDetailsBean> brandDetailsMap = getBrandsHotelsDetails(destinationNode, resolver);
                    DestinationDetailsBean detailBean = new DestinationDetailsBean(destinationName, destinationPath,
                            destinationTitle, imagePath, destinationLocTag, destinationId, searchPath,navTitle);
                    DestinationHotelBean destinationBean = new DestinationHotelBean(brandDetailsMap, detailBean, null);
                    destinationHotelMap.put(destinationName, destinationBean);
                }
                DestinationHotelMapBean destinationMapBean = new DestinationHotelMapBean(destinationHotelMap);
                websiteMap.put(searchPath, destinationMapBean);
            }
            Gson gson = new Gson();
            String obj = gson.toJson(websiteMap);
            LOGGER.trace("HotelObject Created :: {} ", obj);
            return new WebsiteSpecificHotelsBean(websiteMap);
        } catch (RepositoryException e) {
            LOGGER.debug("Exception while getting the destination nodes :: {}", e);
            LOGGER.error("Exception while getting the destination nodes :: {}", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception occured in createTajhotelsAllHotelsMap :: {}", e);
            LOGGER.error("Exception occured in createTajhotelsAllHotelsMap :: {}", e.getMessage());
        }
        return null;
    }


    /**
     * Gets the brands hotels details.
     *
     * @param destinationNode
     *            the destination node
     * @param resolver
     * @param galleryImageFetcherService
     * @return the brands hotels details
     */
    private Map<String, BrandDetailsBean> getBrandsHotelsDetails(Node destinationNode, ResourceResolver resolver) {
        try {
            NodeIterator childNodes = destinationNode.getNodes();
            Map<String, BrandDetailsBean> brandMap = new HashMap<>();
            while (childNodes.hasNext()) {
                Node brandNode = childNodes.nextNode();
                if (brandNode.isNodeType("cq:Page")) {
                    if (!brandNode.hasProperty("jcr:content/cq:template")
                            || brandNode.getPath().contains("/content/ama/")) {
                        String brandName = brandNode.getName();
                        String brandPath = brandNode.getPath();
                        HotelBean hotelsBean = getHotelsMapping(brandNode, brandName, resolver);
                        BrandDetailsBean brandBean = new BrandDetailsBean(brandName, brandPath, hotelsBean);
                        brandMap.put(brandName, brandBean);
                    }
                }
            }
            return brandMap;
        } catch (RepositoryException e) {
            LOGGER.debug("Exception while getting the brand nodes :: {}", e);
            LOGGER.error("Exception while getting the brand nodes :: {}", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception while getting the brand nodes :: {}", e);
            LOGGER.error("Exception while getting the brand nodes :: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Gets the hotels mapping.
     *
     * @param brandNode
     *            the brand node
     * @param brandName
     *            the brand name
     * @param resolver
     * @param galleryImageFetcherService
     * @return the hotels mapping
     */
    private HotelBean getHotelsMapping(Node brandNode, String brandName, ResourceResolver resolver) {
        try {
            NodeIterator hotelNodes = brandNode.getNodes();
            List<HotelDetailsBean> hotelsList = new ArrayList<>();
            while (hotelNodes.hasNext()) {
                Node hotelNode = hotelNodes.nextNode();

                if (hotelNode.isNodeType("cq:Page") && !hotelNode.hasProperty("jcr:content/noShow")) {
                    String hotelName = getNodeProperty(hotelNode, "jcr:content/hotelName");
                    String hotelId = getNodeProperty(hotelNode, "jcr:content/hotelId");
                    String hotelEmail = getNodeProperty(hotelNode, "jcr:content/hotelEmail");
                    String hotelCountry = getNodeProperty(hotelNode, "jcr:content/hotelCountry");
                    String hotelBrand = getNodeProperty(hotelNode, "jcr:content/brand");
                    String hotelPhone = null;
                    if (hotelNode.hasProperty("jcr:content/hotelPhoneNumber")
                            && hotelNode.getProperty("jcr:content/hotelPhoneNumber").isMultiple()) {
                        Value[] phoneNumbers = hotelNode.getProperty("jcr:content/hotelPhoneNumber").getValues();
                        if (phoneNumbers.length > 0) {
                            hotelPhone = phoneNumbers[0].getString();
                            for (int i = 1; i < phoneNumbers.length; i++) {
                                hotelPhone = hotelPhone + " , " + phoneNumbers[i].getString();
                            }
                        }
                    } else {
                        hotelPhone = getNodeProperty(hotelNode, "jcr:content/hotelPhoneNumber");
                    }
                    String hotelPath = hotelNode.getPath();
                    String hotelCity = getNodeProperty(hotelNode, "jcr:content/hotelCity");
                    String numberOfBeds = getNodeProperty(hotelNode, "jcr:content/numberOfBeds");
                    String maximumNoOfGuests = getNodeProperty(hotelNode, "jcr:content/maximumNoOfGuests");
                    String hotelBannerImage = getNodeProperty(hotelNode,
                            "jcr:content/banner_parsys/hotel_banner/bannerImage");
                    String amaShortDescription = getNodeProperty(hotelNode, "jcr:content/root/ama_text/description");
                    String state = getNodeProperty(hotelNode, "jcr:content/addressRegion");
                    String hotelRoomCode = getNodeProperty(hotelNode, "jcr:content/roomTypeCode");
                    String staticRate = getNodeProperty(hotelNode, "jcr:content/staticRate");
                    // for sameDayCheckout
                    String SameDayCheckout = getNodeProperty(hotelNode, "jcr:content/SameDayCheckOut");
                    //LOGGER.error("SameDayCheckout : {}",SameDayCheckout);
                    String isOnlyBungalowPage = getNodeProperty(hotelNode, "jcr:content/isOnlyBungalowPage");
                    AdditionalHotelDetailsBean addtionalDetails = getAdditionalHotelDetailsFromNode(hotelNode);
                    List<RestaurantDetailsBean> restaurantDetails = getRestaurantMapping(hotelNode, hotelCity,
                            hotelName, hotelBrand, hotelCountry, addtionalDetails, resolver);
                    HotelDetailsBean hotelDetailsBean = new HotelDetailsBean(hotelName, hotelId, hotelEmail,
                            hotelCountry, hotelBrand, hotelPhone, hotelPath, hotelCity, hotelBannerImage, state,
                            brandName, hotelRoomCode, numberOfBeds, maximumNoOfGuests, amaShortDescription, staticRate,
                            addtionalDetails, restaurantDetails, SameDayCheckout, isOnlyBungalowPage);
                    hotelsList.add(hotelDetailsBean);
                }
            }
            return new HotelBean(hotelsList);
        } catch (RepositoryException e) {
            LOGGER.debug("Exception while getting the hotel nodes :: {} for {}", e, brandNode);
            LOGGER.error("Exception while getting the hotel nodes :: {} for {}", e.getMessage(), brandNode);
        } catch (Exception e) {
            LOGGER.debug("Exception while getting the hotel nodes :: {} for {}", e, brandNode);
            LOGGER.error("Exception while getting the hotel nodes :: {} for {}", e.getMessage(), brandNode);
        }
        return null;
    }


    /**
     * @param hotelNode
     * @param addtionalDetails
     * @param hotelCountry
     * @param hotelCity
     * @param resolver
     * @param galleryImageFetcherService
     * @return
     */
    private List<RestaurantDetailsBean> getRestaurantMapping(Node hotelNode, String hotelCity, String hotelName,
            String hotelBrand, String hotelCountry, AdditionalHotelDetailsBean addtionalDetails,
            ResourceResolver resolver) {
        try {
            List<RestaurantDetailsBean> restaurantList = new ArrayList<>();
            if (hotelNode.hasNode("restaurants")) {
                Node node = hotelNode.getNode("restaurants");
                NodeIterator restaurantNodes = node.getNodes();
                while (restaurantNodes.hasNext()) {
                    Node restaurantNode = restaurantNodes.nextNode();
                    if (restaurantNode.hasProperty("jcr:content/cq:template")
                            && restaurantNode.getProperty("jcr:content/cq:template").getValue().getString()
                                    .equals("/conf/tajhotels/settings/wcm/templates/dining-details")) {
                        RestaurantDetailsBean restaurantDetailsBean = getRestaurantPropertiesDetails(restaurantNode,
                                hotelCountry, hotelCity, hotelName, hotelBrand, addtionalDetails, resolver);
                        restaurantList.add(restaurantDetailsBean);
                    }
                }
            }
            return restaurantList;
        } catch (RepositoryException e) {
            LOGGER.debug("Exception while getting the restaurant details :: {}", e);
            LOGGER.error("Exception while getting the restaurant details :: {}", e.getMessage());
        } catch (Exception e) {
            LOGGER.debug("Exception while getting the restaurant details :: {}", e);
            LOGGER.error("Exception while getting the restaurant details :: {}", e.getMessage());
        }
        return null;
    }


    private RestaurantDetailsBean getRestaurantPropertiesDetails(Node restaurantNode, String country, String city,
            String hName, String hBrand, AdditionalHotelDetailsBean addtionalDetails, ResourceResolver resolver) {
        try {

            if (restaurantNode.hasNode("jcr:content/root/dining_details")) {
                Node diningNode = restaurantNode.getNode("jcr:content/root/dining_details");
                String diningName = getNodeProperty(diningNode, "diningName");
                String diningBrand = hBrand;
                String diningShortDesc = getNodeProperty(diningNode, "diningShortDesc");
                String diningDescription = getNodeProperty(diningNode, "diningDescription");
                String diningLongDescription = getNodeProperty(diningNode, "diningLongDescription");
                String diningImage = getNodeProperty(diningNode, "diningImage");
                String diningPhoneNumber = getNodeProperty(diningNode, "diningPhoneNumber");
                List<String> diningCuisine = getDiningCuisine(diningNode, resolver);
                String resourceURL = restaurantNode.getPath().concat(".html");
                String dressCode = getNodeProperty(diningNode, "dressCode");
                String rateReview = getNodeProperty(diningNode, "jcr:content/rateReview");
                String ticPoints = getNodeProperty(diningNode, "ticPoints");
                String epicurePoints = getNodeProperty(diningNode, "epicurePoints");
                String averagePrice = getNodeProperty(diningNode, "averagePrice");
                String taxDisclaimer = getNodeProperty(diningNode, "taxDisclaimer");
                String diningID = getNodeProperty(diningNode, "diningID");
                String offerSpecifics = getNodeProperty(diningNode, "offerSpecifics");
                String discountValue = getNodeProperty(diningNode, "discountValue");
                String currencySymbol = getNodeProperty(diningNode, "currencySymbol");
                String timingslabel1 = getNodeProperty(diningNode, "timingslabel1");
                String timingslabel2 = getNodeProperty(diningNode, "timingslabel2");
                String timingslabel3 = getNodeProperty(diningNode, "timingslabel3");
                String timingsvalue1 = getNodeProperty(diningNode, "timingsvalue1");
                String timingsvalue2 = getNodeProperty(diningNode, "timingsvalue2");
                String timingsvalue3 = getNodeProperty(diningNode, "timingsvalue3");
                String pathToDownloadFrom = getNodeProperty(diningNode, "pathToDownloadFrom");
                String hotelCountry = country;
                String hotelCity = city;
                String hotelName = hName;
                String hotelArea = addtionalDetails.getHotelArea();
                String hotelPinCode = addtionalDetails.getPinCode();
                String diningLatitude = addtionalDetails.getLatitude();
                String diningLongitude = addtionalDetails.getLongitude();
                String restaurantAddress = getNodeProperty(diningNode, "restaurantAddress");
                List<Amenities> galleryImagePaths = getGalleryImagePaths(diningNode, resolver);
                List<JSONObject> galleryCarousel = getGalleryCarousel(diningNode, resolver);
                Integer numberOfImages = galleryImagePaths.size();
                return new RestaurantDetailsBean(diningName, diningBrand, diningShortDesc, diningDescription,
                        diningLongDescription, diningImage, diningPhoneNumber, diningCuisine, dressCode, ticPoints,
                        epicurePoints, averagePrice, taxDisclaimer, resourceURL, diningID, offerSpecifics,
                        discountValue, galleryImagePaths, galleryCarousel, numberOfImages, currencySymbol, rateReview,
                        pathToDownloadFrom, timingslabel1, timingslabel2, timingslabel3, timingsvalue1, timingsvalue2,
                        timingsvalue3, diningLongitude, diningLatitude, hotelArea, hotelCity, hotelPinCode,
                        hotelCountry, restaurantAddress, hotelName);
            }
        } catch (RepositoryException e) {
            LOGGER.error("Exception while getting the restaurant details :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the restaurant details :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception while getting the restaurant details :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the restaurant details :: {}", e);
        }
        return null;
    }


    /**
     * @param diningNode
     * @param resolver
     * @return
     */
    private List<JSONObject> getGalleryCarousel(Node diningNode, ResourceResolver resolver) {
        List<JSONObject> galleryImagePaths = new ArrayList<>();
        try {
            if (diningNode.hasNode("gallery")) {
                Node galleryNode = diningNode.getNode("gallery");
                if (galleryNode.hasProperty("images") && galleryNode.getProperty("images").isMultiple()) {
                    Value[] images = galleryNode.getProperty("images").getValues();
                    if (images.length > 0) {
                        for (Value image : images) {
                            String filePath = image.getString();
                            Asset galleryAsset = resolver.resolve(filePath).adaptTo(Asset.class);
                            if (null != galleryAsset) {
                                JSONObject jsonObj = new JSONObject();
                                String fileName = galleryAsset.getMetadataValue("dc:title");
                                if (StringUtils.isNotBlank(fileName)) {
                                    jsonObj.put("imagePath", filePath);
                                    jsonObj.put("imageTitle", fileName);
                                } else {
                                    jsonObj.put("imagePath", filePath);
                                    jsonObj.put("imageTitle", BLANK_VALUE);
                                }
                                galleryImagePaths.add(jsonObj);
                            }
                        }
                    }
                } else {
                    String imageProperty = getNodeProperty(galleryNode, "images");
                    Asset galleryAsset = resolver.resolve(imageProperty).adaptTo(Asset.class);
                    if (null != galleryAsset) {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("imagePath", imageProperty);
                        jsonObj.put("imageTitle", galleryAsset.getName());
                        galleryImagePaths.add(jsonObj);
                    }
                }
            }
        } catch (RepositoryException | JSONException e) {
            LOGGER.error("Exception while getting the gallery image :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the gallery image :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception while getting the gallery image :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the gallery image :: {}", e);
        }
        return galleryImagePaths;
    }


    /**
     * @param diningNode
     * @param resolver
     * @return
     */
    private List<String> getDiningCuisine(Node diningNode, ResourceResolver resolver) {
        List<String> diningCuisine = new ArrayList<>();
        try {
            if (diningNode.hasProperty("diningCuisine")) {
                Property cuisineProp = diningNode.getProperty("diningCuisine");
                if (cuisineProp.isMultiple()) {
                    Value[] cuisines = cuisineProp.getValues();
                    if (cuisines.length > 0) {
                        for (Value cuisine : cuisines) {
                            TagManager tagMgr = resolver.adaptTo(TagManager.class);
                            Tag tajTag = tagMgr.resolve(cuisine.getString());
                            if (null != tajTag) {
                                diningCuisine.add(tajTag.getTitle());
                            }
                        }
                    }
                } else {
                    Value cuisine = cuisineProp.getValue();
                    TagManager tagMgr = resolver.adaptTo(TagManager.class);
                    Tag tajTag = tagMgr.resolve(cuisine.getString());
                    if (null != tajTag) {
                        diningCuisine.add(tajTag.getTitle());
                    }
                }
            }
        } catch (RepositoryException e) {
            LOGGER.error("Exception while getting the dining cuisines :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the dining cuisines :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception while getting the dining cuisines :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the dining cuisines :: {}", e);
        }
        return diningCuisine;
    }


    /**
     * @param resolver
     * @param diningNode
     * @return
     */
    private List<Amenities> getGalleryImagePaths(Node diningNode, ResourceResolver resolver) {
        List<Amenities> galleryImagePaths = new ArrayList<>();
        try {
            if (diningNode.hasNode("gallery")) {
                Node galleryNode = diningNode.getNode("gallery");
                if (galleryNode.hasProperty("images") && galleryNode.getProperty("images").isMultiple()) {
                    Value[] images = galleryNode.getProperty("images").getValues();
                    if (images.length > 0) {
                        for (Value image : images) {
                            String filePath = image.getString();
                            Asset galleryAsset = resolver.resolve(filePath).adaptTo(Asset.class);
                            if (null != galleryAsset) {
                                Amenities gallery = null;
                                String fileName = galleryAsset.getMetadataValue("dc:title");
                                if (StringUtils.isNotBlank(fileName)) {
                                    gallery = new Amenities(filePath, fileName);
                                } else {
                                    gallery = new Amenities(filePath, galleryAsset.getName());
                                }
                                galleryImagePaths.add(gallery);
                            }
                        }
                    }
                } else {
                    String imageProperty = getNodeProperty(galleryNode, "images");
                    Asset galleryAsset = resolver.resolve(imageProperty).adaptTo(Asset.class);
                    if (null != galleryAsset) {
                        Amenities gallery = new Amenities(imageProperty, galleryAsset.getName());
                        galleryImagePaths.add(gallery);
                    }
                }
            }
        } catch (RepositoryException e) {
            LOGGER.error("Exception while getting the gallery image :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the gallery image :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception while getting the gallery image :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the gallery image :: {}", e);
        }
        return galleryImagePaths;
    }


    private String getNodeProperty(Node diningNode, String string) {
        try {
            return diningNode.hasProperty(string) ? diningNode.getProperty(string).getValue().getString() : null;
        } catch (IllegalStateException | RepositoryException e) {
            LOGGER.error("Exception while getting the property :: {}", e.getMessage());
            LOGGER.debug("Exception while getting the property :: {}", e);
        }
        return null;
    }


    /**
     * Gets the additional hotel details from node.
     *
     * @param hotelNode
     *            the hotel node
     * @return the additional hotel details from node
     */
    private AdditionalHotelDetailsBean getAdditionalHotelDetailsFromNode(Node hotelNode) {

        try {
            String jivaSpaEmail = getNodeProperty(hotelNode, "jcr:content/jivaSpaEmail");
            String locationStats = getNodeProperty(hotelNode, "jcr:content/locationStats");
            String hotelLocation = null;
            if (hotelNode.hasProperty("jcr:content/locations")) {
                hotelLocation = hotelNode.getProperty("jcr:content/locations").isMultiple()
                        ? hotelNode.getProperty("jcr:content/locations").getValues()[0].getString()
                        : hotelNode.getProperty("jcr:content/locations").getValue().getString();
            }
            String hotelLongitude = getNodeProperty(hotelNode, "jcr:content/hotelLongitude");
            String hotelLatitude = getNodeProperty(hotelNode, "jcr:content/hotelLatitude");
            String totalReviews = getNodeProperty(hotelNode, "jcr:content/totalReview");
            String reviewScore = getNodeProperty(hotelNode, "jcr:content/reviewScore");
            String hotelShortDesc = getNodeProperty(hotelNode, "jcr:content/root/hotel_overview/hotelShortDesc");
            String hotelArea = getNodeProperty(hotelNode, "jcr:content/hotelArea");
            String pinCode = getNodeProperty(hotelNode, "jcr:content/hotelPinCode");
            List<Amenities> hotelAmenitiesgetHotelAmenities = getHotelAmenities(hotelNode);
            List<String> cqTags = new ArrayList<>();
            if (hotelNode.hasProperty("jcr:content/cq:tags")
                    && hotelNode.getProperty("jcr:content/cq:tags").isMultiple()) {
                Value[] tagValues = hotelNode.getProperty("jcr:content/cq:tags").getValues();
                if (tagValues.length > 0) {
                    for (Value tag : tagValues) {
                        cqTags.add(tag.getString());
                    }
                }
            }
            String hotelType = null;
            if (hotelNode.hasProperty("jcr:content/hotelThemeInterest")
                    && hotelNode.getProperty("jcr:content/hotelThemeInterest").isMultiple()) {
                Value[] hotelTypes = hotelNode.getProperty("jcr:content/hotelThemeInterest").getValues();
                if (hotelTypes.length > 0) {
                    hotelType = hotelTypes[0].getString();
                    for (int i = 1; i < hotelTypes.length; i++) {
                        hotelType = hotelType + " , " + hotelTypes[i].getString();
                    }
                }
            } else {
                hotelType = getNodeProperty(hotelNode, "jcr:content/hotelThemeInterest");

            }
            boolean noTic = hotelNode.hasProperty("jcr:content/noTic")
                    ? hotelNode.getProperty("jcr:content/noTic").getBoolean()
                    : false;
            boolean isRoomRedemptionTIC = hotelNode.hasProperty("jcr:content/roomRedemptionTIC")
                    ? hotelNode.getProperty("jcr:content/roomRedemptionTIC").getBoolean()
                    : false;
            return new AdditionalHotelDetailsBean(jivaSpaEmail, locationStats, hotelLocation, hotelLongitude,
                    hotelLatitude, totalReviews, reviewScore, hotelShortDesc, hotelArea, pinCode,
                    hotelAmenitiesgetHotelAmenities, hotelType, cqTags, noTic, isRoomRedemptionTIC);
        } catch (RepositoryException e) {
            LOGGER.error("Exception occured while getting hotel additional details  :: {}", e.getMessage());
            LOGGER.debug("Exception occured while getting hotel additional details  :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception occured while getting hotel additional details  :: {}", e.getMessage());
            LOGGER.debug("Exception occured while getting hotel additional details  :: {}", e);
        }
        return null;
    }


    /**
     * Gets the hotel amenities.
     *
     * @param hotelNode
     *            the hotel node
     * @return the hotel amenities
     */
    private List<Amenities> getHotelAmenities(Node hotelNode) {
        try {
            Node amenitiesNode = hotelNode.hasNode("jcr:content/root/hotel_overview/hotelSignatureFeatures")
                    ? hotelNode.getNode("jcr:content/root/hotel_overview/hotelSignatureFeatures")
                    : null;
            if (null != amenitiesNode) {
                NodeIterator amenitiesNodes = amenitiesNode.getNodes();
                List<Amenities> amenitiesList = new ArrayList<>();
                while (amenitiesNodes.hasNext()) {
                    Node amenity = amenitiesNodes.nextNode();
                    String iconType = getNodeProperty(amenity, "type");
                    String signatureFeature = getNodeProperty(amenity, "signatureFeatureValue");
                    Amenities amenitiesObj = new Amenities(iconType, signatureFeature);
                    amenitiesList.add(amenitiesObj);
                }
                return amenitiesList;
            }
        } catch (RepositoryException e) {
            LOGGER.error("Exception occured while getting hotel amenities details  :: {}", e.getMessage());
            LOGGER.debug("Exception occured while getting hotel amenities details  :: {}", e);
        } catch (Exception e) {
            LOGGER.error("Exception occured while getting hotel amenities details  :: {}", e.getMessage());
            LOGGER.debug("Exception occured while getting hotel amenities details  :: {}", e);
        }
        return null;
    }


    /**
     * Gets the query result.
     *
     * @param predicates
     *            the predicates
     * @param resourceResolver
     *            the resource resolver
     * @return the query result
     */
    public SearchResult getQueryResult(Map<String, String> predicates, ResourceResolver resourceResolver) {
        Session session = resourceResolver.adaptTo(Session.class);
        QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
        final Query query = queryBuilder.createQuery(PredicateGroup.create(predicates), session);
        query.setHitsPerPage(99999);
        return query.getResult();
    }
}
