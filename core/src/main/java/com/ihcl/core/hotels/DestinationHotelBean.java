/**
 * 
 */
package com.ihcl.core.hotels;

import java.util.Map;

/**
 * <pre>
 * DestinationHotelBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : DestinationHotelBean.java
 * @Description : com.ihcl.core.hotels -DestinationHotelBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class DestinationHotelBean {
	
	private Map<String, BrandDetailsBean> brandHotels;

	private DestinationDetailsBean destinationDetails;
	
	private Map<String, HotelBean> hotelsPerBrandSplit;

	public Map<String, BrandDetailsBean> getBrandHotels() {
		return brandHotels;
	}

	public void setBrandHotels(Map<String, BrandDetailsBean> brandHotels) {
		this.brandHotels = brandHotels;
	}

	public DestinationDetailsBean getDestinationDetails() {
		return destinationDetails;
	}

	public void setDestinationDetails(DestinationDetailsBean destinationDetails) {
		this.destinationDetails = destinationDetails;
	}

	public Map<String, HotelBean> getHotelsPerBrandSplit() {
		return hotelsPerBrandSplit;
	}

	public void setHotelsPerBrandSplit(Map<String, HotelBean> hotelsPerBrandSplit) {
		this.hotelsPerBrandSplit = hotelsPerBrandSplit;
	}

	public DestinationHotelBean(Map<String, BrandDetailsBean> brandHotels, DestinationDetailsBean destinationDetails,
			Map<String, HotelBean> hotelsPerBrandSplit) {
		super();
		this.brandHotels = brandHotels;
		this.destinationDetails = destinationDetails;
		this.hotelsPerBrandSplit = hotelsPerBrandSplit;
	}


	
}
