/**
 * 
 */
package com.ihcl.core.hotels;

import java.util.List;

/**
 * <pre>
 * HotelBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : HotelBean.java
 * @Description : com.ihcl.core.hotels -HotelBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class HotelBean {
	
	private List<HotelDetailsBean> hotelsList;

	/**
	 * @return the hotelsList
	 */
	public List<HotelDetailsBean> getHotelsList() {
		return hotelsList;
	}

	/**
	 * @param hotelsList the hotelsList to set
	 */
	public void setHotelsList(List<HotelDetailsBean> hotelsList) {
		this.hotelsList = hotelsList;
	}

	/**
	 * @param hotelsList
	 */
	public HotelBean(List<HotelDetailsBean> hotelsList) {
		super();
		this.hotelsList = hotelsList;
	}

	
	

}
