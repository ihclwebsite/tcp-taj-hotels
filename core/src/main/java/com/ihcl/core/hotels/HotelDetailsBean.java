/**
 *
 */
package com.ihcl.core.hotels;

import java.util.List;

/**
 * <pre>
 * HotelDetailsBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : HotelDetailsBean.java
 * @Description : com.ihcl.core.hotels -HotelDetailsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class HotelDetailsBean {

    private String hotelName;

    private String hotelID;

    private String hotelEmail;

    private String hotelCountry;

    private String hotelBrand;

    private String hotelPhoneNumber;

    private String hotelPath;

    private String hotelCity;

    private String hotelBannerImage;

    private String state;

    private String hotelBrandName;

    private String hotelRoomCode;

    private String numberOfBeds;

    private String maximumNoOfGuests;

    private String amaShortDescription;

    private String staticRate;

    private AdditionalHotelDetailsBean additionaldetails;

    private List<RestaurantDetailsBean> restaurantDetails;
    
    private String SameDayCheckout;
    
    private String isOnlyBungalowPage;


    public String getIsOnlyBungalowPage() {
		return isOnlyBungalowPage;
	}


	public void setIsOnlyBungalowPage(String isOnlyBungalowPage) {
		this.isOnlyBungalowPage = isOnlyBungalowPage;
	}


	public String getAmaShortDescription() {
        return amaShortDescription;
    }


    public void setAmaShortDescription(String amaShortDescription) {
        this.amaShortDescription = amaShortDescription;
    }


    public String getHotelName() {
        return hotelName;
    }


    public String getMaximumNoOfGuests() {
        return maximumNoOfGuests;
    }


    /**
     * Setter for the field maximumNoOfGuests
     *
     * @param maximumNoOfGuests
     *            the maximumNoOfGuests to set
     */

    public void setMaximumNoOfGuests(String maximumNoOfGuests) {
        this.maximumNoOfGuests = maximumNoOfGuests;
    }


    /**
     * Getter for the field numberOfBeds
     *
     * @return the numberOfBeds
     */
    public String getNumberOfBeds() {
        return numberOfBeds;
    }


    /**
     * Setter for the field numberOfBeds
     *
     * @param numberOfBeds
     *            the numberOfBeds to set
     */

    public void setNumberOfBeds(String numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelID() {
        return hotelID;
    }

    public void setHotelID(String hotelID) {
        this.hotelID = hotelID;
    }

    public String getHotelEmail() {
        return hotelEmail;
    }

    public void setHotelEmail(String hotelEmail) {
        this.hotelEmail = hotelEmail;
    }

    public String getHotelCountry() {
        return hotelCountry;
    }

    public void setHotelCountry(String hotelCountry) {
        this.hotelCountry = hotelCountry;
    }

    public String getHotelBrand() {
        return hotelBrand;
    }

    public void setHotelBrand(String hotelBrand) {
        this.hotelBrand = hotelBrand;
    }

    public String getHotelPhoneNumber() {
        return hotelPhoneNumber;
    }

    public void setHotelPhoneNumber(String hotelPhoneNumber) {
        this.hotelPhoneNumber = hotelPhoneNumber;
    }

    public String getHotelPath() {
        return hotelPath;
    }

    public void setHotelPath(String hotelPath) {
        this.hotelPath = hotelPath;
    }

    public String getHotelCity() {
        return hotelCity;
    }

    public void setHotelCity(String hotelCity) {
        this.hotelCity = hotelCity;
    }

    public String getHotelBannerImage() {
        return hotelBannerImage;
    }

    public void setHotelBannerImage(String hotelBannerImage) {
        this.hotelBannerImage = hotelBannerImage;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHotelBrandName() {
        return hotelBrandName;
    }

    public void setHotelBrandName(String hotelBrandName) {
        this.hotelBrandName = hotelBrandName;
    }

    public AdditionalHotelDetailsBean getAdditionaldetails() {
        return additionaldetails;
    }

    public void setAdditionaldetails(AdditionalHotelDetailsBean additionaldetails) {
        this.additionaldetails = additionaldetails;
    }

    public List<RestaurantDetailsBean> getRestaurantDetails() {
        return restaurantDetails;
    }

    public void setRestaurantDetails(List<RestaurantDetailsBean> restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }


    public String getHotelRoomCode() {
        return hotelRoomCode;
    }

    public void setHotelRoomCode(String hotelRoomCode) {
        this.hotelRoomCode = hotelRoomCode;
    }

    public String getStaticRate() {
        return staticRate;
    }

    public void setStaticRate(String staticRate) {
        this.staticRate = staticRate;
    }


    public String getSameDayCheckout() {
		return SameDayCheckout;
	}


	public void setSameDayCheckout(String sameDayCheckout) {
		SameDayCheckout = sameDayCheckout;
	}


	/**
     * @param hotelName
     * @param hotelID
     * @param hotelEmail
     * @param hotelCountry
     * @param hotelBrand
     * @param hotelPhoneNumber
     * @param hotelPath
     * @param hotelCity
     * @param hotelBannerImage
     * @param state
     * @param hotelBrandName
     * @param hotelRoomCode
     * @param numberOfBeds
     * @param maximumNoOfGuests
     * @param amaShortDescription
     * @param staticRate
     * @param additionaldetails
     * @param restaurantDetails
	 * @param isOnlyBungalowPage 
     */
    public HotelDetailsBean(String hotelName, String hotelID, String hotelEmail, String hotelCountry, String hotelBrand,
            String hotelPhoneNumber, String hotelPath, String hotelCity, String hotelBannerImage, String state,
            String hotelBrandName, String hotelRoomCode, String numberOfBeds, String maximumNoOfGuests,
            String amaShortDescription, String staticRate, AdditionalHotelDetailsBean additionaldetails,
            List<RestaurantDetailsBean> restaurantDetails, String SameDayCheckout, String isOnlyBungalowPage) {
        super();
        this.hotelName = hotelName;
        this.hotelID = hotelID;
        this.hotelEmail = hotelEmail;
        this.hotelCountry = hotelCountry;
        this.hotelBrand = hotelBrand;
        this.hotelPhoneNumber = hotelPhoneNumber;
        this.hotelPath = hotelPath;
        this.hotelCity = hotelCity;
        this.hotelBannerImage = hotelBannerImage;
        this.state = state;
        this.hotelBrandName = hotelBrandName;
        this.hotelRoomCode = hotelRoomCode;
        this.numberOfBeds = numberOfBeds;
        this.maximumNoOfGuests = maximumNoOfGuests;
        this.amaShortDescription = amaShortDescription;
        this.staticRate = staticRate;
        this.additionaldetails = additionaldetails;
        this.restaurantDetails = restaurantDetails;
        this.SameDayCheckout = SameDayCheckout;
        this.isOnlyBungalowPage = isOnlyBungalowPage;
    }


    public HotelDetailsBean(String hotelName, String hotelID, String hotelEmail, String hotelCountry, String hotelBrand,
            String hotelPhoneNumber, String hotelPath, String hotelCity, String hotelBannerImage, String state,
            String hotelBrandName, String hotelRoomCode, String numberOfBeds, String maximumNoOfGuests,
            String amaShortDescription, String staticRate, AdditionalHotelDetailsBean additionaldetails,
            List<RestaurantDetailsBean> restaurantDetails) {
        super();
        this.hotelName = hotelName;
        this.hotelID = hotelID;
        this.hotelEmail = hotelEmail;
        this.hotelCountry = hotelCountry;
        this.hotelBrand = hotelBrand;
        this.hotelPhoneNumber = hotelPhoneNumber;
        this.hotelPath = hotelPath;
        this.hotelCity = hotelCity;
        this.hotelBannerImage = hotelBannerImage;
        this.state = state;
        this.hotelBrandName = hotelBrandName;
        this.hotelRoomCode = hotelRoomCode;
        this.numberOfBeds = numberOfBeds;
        this.maximumNoOfGuests = maximumNoOfGuests;
        this.amaShortDescription = amaShortDescription;
        this.staticRate = staticRate;
        this.additionaldetails = additionaldetails;
        this.restaurantDetails = restaurantDetails;
    }
}
