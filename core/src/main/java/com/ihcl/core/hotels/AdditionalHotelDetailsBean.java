package com.ihcl.core.hotels;

import java.util.List;

public class AdditionalHotelDetailsBean {

    private String jivaSpaMail;

    private String locationStats;

    private String hotelLocation;

    private String longitude;

    private String latitude;

    private String totalReviews;

    private String reviewScore;

    private String shortDesc;

    private String hotelArea;

    private String pinCode;

    private List<Amenities> hotelAmenities;

    private String hotelTypes;
    
    private List<String> cqTags;
    
    private boolean noTic;
    
    private boolean isRoomRedemptionTIC;


    public String getJivaSpaMail() {
        return jivaSpaMail;
    }

    public void setJivaSpaMail(String jivaSpaMail) {
        this.jivaSpaMail = jivaSpaMail;
    }

    public String getLocationStats() {
        return locationStats;
    }

    public void setLocationStats(String locationStats) {
        this.locationStats = locationStats;
    }

    public String getHotelLocation() {
        return hotelLocation;
    }

    public void setHotelLocation(String hotelLocation) {
        this.hotelLocation = hotelLocation;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    public String getReviewScore() {
        return reviewScore;
    }

    public void setReviewScore(String reviewScore) {
        this.reviewScore = reviewScore;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getHotelArea() {
        return hotelArea;
    }

    public void setHotelArea(String hotelArea) {
        this.hotelArea = hotelArea;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public List<Amenities> getHotelAmenities() {
        return hotelAmenities;
    }

    public void setHotelAmenities(List<Amenities> hotelAmenities) {
        this.hotelAmenities = hotelAmenities;
    }

    public String getHotelTypes() {
        return hotelTypes;
    }

    public void setHotelTypes(String hotelTypes) {
        this.hotelTypes = hotelTypes;
    }

	public List<String> getCqTags() {
		return cqTags;
	}

	public void setCqTags(List<String> cqTags) {
		this.cqTags = cqTags;
	}

	public boolean getNoTic() {
		return noTic;
	}

	public void setNoTic(boolean noTic) {
		this.noTic = noTic;
	}

	public boolean isRoomRedemptionTIC() {
		return isRoomRedemptionTIC;
	}

	public void setRoomRedemptionTIC(boolean isRoomRedemptionTIC) {
		this.isRoomRedemptionTIC = isRoomRedemptionTIC;
	}

	public AdditionalHotelDetailsBean(String jivaSpaMail, String locationStats, String hotelLocation, String longitude,
			String latitude, String totalReviews, String reviewScore, String shortDesc, String hotelArea,
			String pinCode, List<Amenities> hotelAmenities, String hotelTypes, List<String> cqTags, boolean noTic,
			boolean isRoomRedemptionTIC) {
		super();
		this.jivaSpaMail = jivaSpaMail;
		this.locationStats = locationStats;
		this.hotelLocation = hotelLocation;
		this.longitude = longitude;
		this.latitude = latitude;
		this.totalReviews = totalReviews;
		this.reviewScore = reviewScore;
		this.shortDesc = shortDesc;
		this.hotelArea = hotelArea;
		this.pinCode = pinCode;
		this.hotelAmenities = hotelAmenities;
		this.hotelTypes = hotelTypes;
		this.cqTags = cqTags;
		this.noTic = noTic;
		this.isRoomRedemptionTIC = isRoomRedemptionTIC;
	}


}
