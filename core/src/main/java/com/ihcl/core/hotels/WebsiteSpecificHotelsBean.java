/**
 * 
 */
package com.ihcl.core.hotels;

import java.util.Map;

/**
 * <pre>
 * WebsiteSpecificHotelsBean Class
 * </pre>
 *
 *
 *
 * @author : Neha Priyanka
 * @version : 1.0
 * @see
 * @since :22-May-2019
 * @ClassName : WebsiteSpecificHotelsBean.java
 * @Description : com.ihcl.core.hotels -WebsiteSpecificHotelsBean.java
 * @Modification Information
 *
 *               <pre>
 *
 *     since            author               description
 *  ===========     ==============   =========================
 *  22-May-2019     Neha Priyanka            Create
 *
 *               </pre>
 */
public class WebsiteSpecificHotelsBean {
	
	private Map<String, DestinationHotelMapBean> websiteHotelsMap;

	/**
	 * @return the websiteHotelsMap
	 */
	public Map<String, DestinationHotelMapBean> getWebsiteHotelsMap() {
		return websiteHotelsMap;
	}

	/**
	 * @param websiteHotelsMap the websiteHotelsMap to set
	 */
	public void setWebsiteHotelsMap(Map<String, DestinationHotelMapBean> websiteHotelsMap) {
		this.websiteHotelsMap = websiteHotelsMap;
	}

	/**
	 * @param websiteMap
	 */
	public WebsiteSpecificHotelsBean(Map<String, DestinationHotelMapBean> websiteMap) {
		super();
		this.websiteHotelsMap = websiteMap;
	}
	

}
