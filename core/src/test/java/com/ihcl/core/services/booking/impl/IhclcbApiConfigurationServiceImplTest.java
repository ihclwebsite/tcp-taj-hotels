/**
 *
 */
package com.ihcl.core.services.booking.impl;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.testing.mock.sling.junit.SlingContext;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.ihcl.core.models.config.IhclcbApiConfigModelImpl;

/**
 * @author Admin
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class IhclcbApiConfigurationServiceImplTest {

    @Mock
    private IhclcbApiConfigModelImpl underTest;

    private String entityDetailsFetchApiUrl = "/integration/flowapi/rest/CORP_BOOKING_TOOL/v01/contact-partyid";

    private String guestDetailsFetchApiUrl = "/integration/flowapi/rest/GUESTDETAILSONMEM/v01/GuestDetails";

    private String ihclcbBookingApiUrl = "/integration/flowapi/rest/MDMFETCHRESERVATION/v01/FetchReservation?q=ContactName_Id_c=100000036005307";

    private String ihclcbApiHostUrl = "https://tajics-a455764.integration.us2.oraclecloud.com";


    @Rule
    public SlingContext context = new SlingContext();

    Map<String, Object> parameters = new HashMap<>();


    @Before
    public void setup() throws NoSuchFieldException {

        Mockito.when(underTest.getIhclcbApiHostUrl()).thenReturn(ihclcbApiHostUrl);
        Mockito.when(underTest.getEntityDetailsFetchApiUrl()).thenReturn(entityDetailsFetchApiUrl);
        Mockito.when(underTest.getGuestDetailsFetchApiUrl()).thenReturn(guestDetailsFetchApiUrl);
        Mockito.when(underTest.getIhclcbBookingApiUrl()).thenReturn(ihclcbBookingApiUrl);


        /*
         * PrivateAccessor.setField(underTest, "ihclcbApiHostUrl", ihclcbApiHostUrl);
         * PrivateAccessor.setField(underTest, "entityDetailsFetchApiUrl", entityDetailsFetchApiUrl);
         * PrivateAccessor.setField(underTest, "guestDetailsFetchApiUrl", guestDetailsFetchApiUrl);
         * PrivateAccessor.setField(underTest, "corporateLoginApiUrl", corporateLoginApiUrl);
         * PrivateAccessor.setField(underTest, "ihclcbForgotApiUrl", ihclcbForgotApiUrl);
         * PrivateAccessor.setField(underTest, "ihclcbSendTokenApiUrl", ihclcbSendTokenApiUrl);
         * PrivateAccessor.setField(underTest, "ihclcbValidateTokenApiUrl", ihclcbValidateTokenApiUrl);
         * PrivateAccessor.setField(underTest, "ihclcbBookingApiUrl", ihclcbBookingApiUrl);
         */


    }

    @Test
    public void checkIhclcbApiHostUrl() {
        assertEquals("https://tajics-a455764.integration.us2.oraclecloud.com", underTest.getIhclcbApiHostUrl());
    }

    @Test
    public void checkentityDetailsFetchApiUrl() {
        assertEquals("/integration/flowapi/rest/CORP_BOOKING_TOOL/v01/contact-partyid",
                underTest.getEntityDetailsFetchApiUrl());
    }

    @Test
    public void checkguestDetailsFetchApiUrl() {
        assertEquals("/integration/flowapi/rest/GUESTDETAILSONMEM/v01/GuestDetails",
                underTest.getGuestDetailsFetchApiUrl());
    }

    @Test
    public void checkbookingDetailsFetchApiUrl() {
        assertEquals(
                "/integration/flowapi/rest/MDMFETCHRESERVATION/v01/FetchReservation?q=ContactName_Id_c=100000036005307",
                underTest.getIhclcbBookingApiUrl());
    }
}
