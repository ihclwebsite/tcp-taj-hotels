/**
 *
 */
package com.ihcl.core.services.booking.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author Admin
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class GuaranteeCodesConfigurationServiceImpTest {

    @Mock
    GuaranteeCodesConfigurationServiceImpl underTest;


    String[] guarentrrCodesOne = new String[] { "NG", };

    String[] guarenteeCodesTwo = new String[] { "GT" };

    @Before
    public void setup() {
        Mockito.when(underTest.getGuaranteeCodes()).thenReturn(guarentrrCodesOne);
    }

    @Test
    public void checkGuarenteeCode() {
        Assert.assertEquals(guarentrrCodesOne[0], underTest.getGuaranteeCodes()[0]);
    }

    @Test
    public void checkGuarenteeCodeNot() {
        Assert.assertNotEquals(guarenteeCodesTwo[0], underTest.getGuaranteeCodes()[0]);
    }


}
