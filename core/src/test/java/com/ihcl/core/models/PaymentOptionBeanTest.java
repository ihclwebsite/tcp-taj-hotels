/**
 *
 */
package com.ihcl.core.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * @author Admin
 *
 */
public class PaymentOptionBeanTest {

    @Mock
    private PaymentOptionBean underTest;

    @Before
    public void setup() {
        underTest = new PaymentOptionBean();
        // List<PaymentOptionBean> childList = new ArrayList<>();
        // PaymentOptionBean bean = new PaymentOptionBean();
        // bean.setPaymentOptionDescription("this_is_bill_to_company");
        // childList.add(bean);
        underTest.setPaymentOptionDescription("this_is_bill_to_company");
    }

    @Test
    public void testIfPaymentOptionDescription() {
        assertEquals("this_is_bill_to_company", underTest.getPaymentOptionDescription());
    }


}

