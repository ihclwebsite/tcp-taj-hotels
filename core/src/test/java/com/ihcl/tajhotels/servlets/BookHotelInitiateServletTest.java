/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.core.services.booking.PaymentCardDetailsConfigurationService;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.tajhotels.constants.BookingConstants;

import io.wcm.testing.mock.aem.junit.AemContext;
import junitx.util.PrivateAccessor;

/**
 * @author moonraft
 *
 */
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class BookHotelInitiateServletTest {

    @Mock
    SlingHttpServletRequest request;

    @Mock
    SlingHttpServletResponse response;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Rule
    public final AemContext context = new AemContext(ResourceResolverType.JCR_MOCK);

    @Mock
    private SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    BookHotelInitiateServlet bookHotelInitiateServlet = new BookHotelInitiateServlet();

    @Mock
    private IBookingRequestHandler bookingHandler;

    @Mock
    private TajHotelsBrandsETCService tajHotelsBrandsETCService;

    @Mock
    PaymentCardDetailsConfigurationService paymentCardDetailsService;

    public final StringWriter stringWriter = new StringWriter();

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        PrivateAccessor.setField(bookHotelInitiateServlet, "synixsDowntimeConfigurationService",
                synixsDowntimeConfigurationService);
        PrivateAccessor.setField(bookHotelInitiateServlet, "bookingHandler", bookingHandler);

        PrivateAccessor.setField(bookHotelInitiateServlet, "resolverFactory", resolverFactory);

        PrivateAccessor.setField(bookHotelInitiateServlet, "tajHotelsBrandsETCService", tajHotelsBrandsETCService);

        PrivateAccessor.setField(bookHotelInitiateServlet, "paymentCardDetailsService", paymentCardDetailsService);

        // Mocking RequestURL in ServletRequest
        when(request.getRequestURL())
                .thenReturn(new StringBuffer("http://localhost:4502/bin/bookHotelInitiateServlet"));

        // Mocking ServerName in ServletRequest
        when(request.getServerName()).thenReturn("localhost");

        // Mocking CCAvenue Credentials for Etc Configurations
        mockingEtcCCAvenueConfigurations();

        // Mocking PrintWriter for ServletResponse Writer
        Mockito.when(response.getWriter()).thenReturn(new PrintWriter(stringWriter));
    }

    @Test
    public void checkSynixsDown() throws ServletException, IOException {

        // Mocking Synixs DowntimeCheck as true and DowntimeMessage in SynixsDowntimeConfigurationService
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(true);
        Mockito.when(synixsDowntimeConfigurationService.getDowntimeMessage()).thenReturn("Backend is down!");

        bookHotelInitiateServlet.doPost(request, response);

        String responseString = stringWriter.getBuffer().toString();
        assertEquals("Backend is down!", responseString);
    }

    @Test
    public void testPayOnline() throws Exception {

        // Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
        // CHECK_IN_DATE, CHECKOUT_DATE.
        mockingRequestParameters();

        // Mocking SynixsDowntimeCheck as false in SynixsDowntimeConfigurationService
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(false);

        // Mocking PAYMENT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_PAYMENT_DETAILS)).thenReturn(
                "{\"payAtHotel\":false,\"payOnlineNow\":true,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"creditCardRequired\":true,\"cardType\":\"\",\"nameOnCard\":\"\",\"cardNumber\":\"\",\"expiryMonth\":\"\",\"ticpoints\":{},\"currencyCode\":\"INR\"}");

        // Mocking Credit_Card_Details for PaymentCardDetailsService
        mockingPaymentCardDetailsService();

        // Mocking Booking Object for BookingBackendResponse
        mockingBookingBackendResponse(
                "{\"itineraryNumber\":\"21305B3515428\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"testing\",\"firstName\":\"test\",\"email\":\"vijay.kumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"917207592095\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":null,\"hotel\":null,\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC016873\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View King Bed\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"T05\",\"ratePlanName\":\"Bed Breakfast and More\",\"rateDescription\":\"Includes buffet breakfast and free WiFi for four devices. This offer comes with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"13750.00\",\"roomCostAfterTax\":\"16225.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2475.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"16225.00\",\"price\":\"13750.00\",\"tax\":\"2475.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":true}],\"totalAmountAfterTax\":\"16225.0\",\"totalAmountBeforeTax\":\"13750.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}");

        // Mocking HotelDetails for Querying for Hotel Details using File Resources
        mockingHotelDetails("/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai");

        bookHotelInitiateServlet.doPost(request, response);

        String responseString = stringWriter.getBuffer().toString();

        String expectingResponse = "<form action=\"https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction\" method=\"post\"><input type=\"hidden\" name=\"encRequest\" value=\"cf825bf3c9e022fbdc7c97ffac7e249201bd4ad411bd8d95ed96fff59a3a4e46bf6b6ef5c02d8991078c59f6c93383df1338ec71543d0c108080aab96930f8657a59fd785ac30e572981b798cf0856f2f9408c1fb88cde23d0dbd800a3bd11e2404cae29308cca12323889e79d9bcd83366f9c285f6b35419efdef773a2414bb106c13d64bc70f95630173c059ee86fa20d31d015caa40e572b5272499d035cc07b4ec5b679ca498f4462d5302850e104c53e59c6fed3102bc623c1976eb9413c26c30c3215278931a7c4f08919d3c256fa09d1b4b3caed8679eb01af687c8dbf6552a2cd01e0e5ed5a876562c559d278f753e9a24d9931795f3a78537c4bca52a73169990eeae6df913b4050e4282bd17b8ee9ce8457a20e515e362258071937e97eca2de0e7d324387ef4220e1d9072745dcadf644d3d5b12bc7e9d336db51da385dfde73f88ca187aa61ca5021f02e6792293f6093594fb4c76ca7705b50b5aeeb76ea8081dc511d09f7ca63bf2010d8d1e6bac017260a1bffaf50d3628cb1b69e57c456105dc5907eeaaeca4cff416e2fa23fbe8b31eea457f83ca3e2a8be920640decdbcce26887327f5da6889f1dde2ca72962d257d0e0688d69cff87e6db58d7491e4c3d27b41489af1816623a63553420f17e3d4b137da41638de68c955dec8fb274ba7e5146bfc82a840d6960552b37bae386b97f49e7829dc131051f9e9cfc39da56e58a691901adb8b659e4772fb4faf65f8073724de4fe5b08e2107b1bfc7dcadb9babaaf3b3e1760ca4d6d126475eb3de35972228a7b8da308abe017d6ab86337ac4cf0f481960924c8589490b1af4a43b5b4c41def4cff746c950b6fa2c99811029bb417393e65e4758cd046a633ce571daa9abf582d4e8c5545b938f2b8e43e9b046afa2f5dce3b60d8f058f081103b7ec2b0f24ac199cbe07e3d1437d42554c404fd483dcc11eec342b61d372369c840c08d2abacc6c4ab0d60b065add7b3c8f5430c34a943f74c8\" /><input type=\"hidden\" name=\"access_code\" value=\"AVQF01FG85AS47FQSA\" /></form>";
        assertEquals(expectingResponse, responseString);
    }

    @Test
    public void testPayAtHotel() throws Exception {

        // Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
        // CHECK_IN_DATE, CHECKOUT_DATE.
        mockingRequestParameters();

        // Mocking SynixsDowntimeCheck as false in SynixsDowntimeConfigurationService
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(false);

        // Mocking PAYMENT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_PAYMENT_DETAILS)).thenReturn(
                "{\"payAtHotel\":true,\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"creditCardRequired\":true,\"cardType\":\"VI\",\"nameOnCard\":\"test\",\"cardNumber\":\"4111111111111111\",\"expiryMonth\":\"12/20\",\"ticpoints\":{},\"currencyCode\":\"INR\"}");

        // Mocking Booking Object for BookingBackendResponse
        mockingBookingBackendResponse(
                "{\"itineraryNumber\":\"21305B3515428\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"testing\",\"firstName\":\"test\",\"email\":\"vijay.kumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"917207592095\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":null,\"hotel\":null,\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC016873\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View King Bed\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"T05\",\"ratePlanName\":\"Bed Breakfast and More\",\"rateDescription\":\"Includes buffet breakfast and free WiFi for four devices. This offer comes with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"13750.00\",\"roomCostAfterTax\":\"16225.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2475.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"16225.00\",\"price\":\"13750.00\",\"tax\":\"2475.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":true}],\"totalAmountAfterTax\":\"16225.0\",\"totalAmountBeforeTax\":\"13750.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}");

        // Mocking HotelDetails for Querying for Hotel Details using File Resources
        mockingHotelDetails("/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai");

        bookHotelInitiateServlet.doPost(request, response);

        String responseString = stringWriter.getBuffer().toString();

        String expectingResponse = "{\"itineraryNumber\":\"21305B3515428\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"testing\",\"firstName\":\"test\",\"email\":\"vijay.kumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"917207592095\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":{\"payAtHotel\":true,\"creditCardRequired\":true,\"nameOnCard\":\"test\",\"cardNumber\":\"************1111\",\"expiryMonth\":\"12/20\",\"currencyCode\":\"INR\",\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"ticpoints\":{\"redeemTicOrEpicurePoints\":false,\"ticPlusCreditCard\":null,\"epicurePlusCreditCard\":null,\"pointsType\":null,\"noTicPoints\":0,\"noEpicurePoints\":0,\"pointsPlusCash\":false,\"currencyConversionRate\":null},\"cardType\":\"VI\"},\"hotel\":{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":\"HLTBOMTM\",\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"},\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC016873\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"T05\",\"ratePlanName\":\"Bed Breakfast and More\",\"rateDescription\":\"Includes buffet breakfast and free WiFi for four devices. This offer comes with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":\"\",\"bookingStatus\":true,\"roomCostBeforeTax\":\"13750.00\",\"roomCostAfterTax\":\"16225.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2475.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"16225.00\",\"price\":\"13750.00\",\"tax\":\"2475.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":true}],\"totalAmountAfterTax\":\"16225.0\",\"totalAmountBeforeTax\":\"13750.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}";
        assertEquals(expectingResponse, responseString);
    }

    /**
     * @throws Exception
     */
    @Test
    public void testPayByTICPoints() throws Exception {

        // Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
        // CHECK_IN_DATE, CHECKOUT_DATE.
        mockingTICRequestParameters();

        // Mocking PAYMENT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_PAYMENT_DETAILS)).thenReturn(
                "{\"payAtHotel\":true,\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"creditCardRequired\":true,\"cardType\":\"\",\"nameOnCard\":\"\",\"cardNumber\":\"\",\"expiryMonth\":\"\",\"ticpoints\":{\"redeemTicOrEpicurePoints\":true,\"ticPlusCreditCard\":\"35031\",\"epicurePlusCreditCard\":\"\",\"pointsPlusCash\":false,\"currencyConversionRate\":1,\"pointsType\":\"TIC\",\"noTicPoints\":13275},\"currencyCode\":\"INR\"}");

        // Mocking Credit_Card_Details for PaymentCardDetailsService
        mockingPaymentCardDetailsService();

        // Mocking Booking Object for BookingBackendResponse
        mockingBookingBackendResponse(
                "{\"itineraryNumber\":\"21305B3524021\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"R\",\"firstName\":\"Sampathkumar\",\"email\":\"sampathkumar54@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\" 917207592095\",\"gstNumber\":null,\"membershipId\":\"101015496604\",\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":null,\"hotel\":null,\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC017204\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View King Bed\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"H00N\",\"ratePlanName\":\"TIC Room Redemption Value - Silver\",\"rateDescription\":\"This offer is applicable only to Taj InnerCircle members for booking against redemption of TIC points, on room only basis. Breakfast is not included in the offer unless otherwise specified. All extras to be borne by the member and paid directly at the hotel. Appropriate point adjustments will be made on your Taj InnerCircle account within 7 days in case of modification or cancellation. Confirmation subject to availability of points. Points redeemed will not be reinstated in case reservation is cancelled after the applicable cancellation period.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"11250.00\",\"roomCostAfterTax\":\"13275.00\",\"cancellationPolicy\":\"The reservation cannot be modified. For any assistance, please email innercircle@tajhotels.com\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2025.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"13275.00\",\"price\":\"11250.00\",\"tax\":\"2025.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":false}],\"totalAmountAfterTax\":\"13275.0\",\"totalAmountBeforeTax\":\"11250.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"sampathkumar54@moonraft.com\",\"currencyCode\":\"INR\",\"pinNumber\":\"300000832363082\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}");

        // Mocking HotelDetails for Querying for Hotel Details using File Resources
        mockingHotelDetails("/content/taj-inner-circle/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai");

        bookHotelInitiateServlet.doPost(request, response);

        String responseString = stringWriter.getBuffer().toString();

        String expectingResponse = "{\"itineraryNumber\":\"21305B3524021\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"R\",\"firstName\":\"Sampathkumar\",\"email\":\"sampathkumar54@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\" 917207592095\",\"gstNumber\":null,\"membershipId\":\"101015496604\",\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":{\"payAtHotel\":true,\"creditCardRequired\":true,\"nameOnCard\":\"test\",\"cardNumber\":\"************1111\",\"expiryMonth\":\"12/25\",\"currencyCode\":\"INR\",\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"ticpoints\":{\"redeemTicOrEpicurePoints\":true,\"ticPlusCreditCard\":\"35031\",\"epicurePlusCreditCard\":\"\",\"pointsType\":\"TIC\",\"noTicPoints\":13275,\"noEpicurePoints\":0,\"pointsPlusCash\":false,\"currencyConversionRate\":\"1\"},\"cardType\":\"VI\"},\"hotel\":{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/taj-inner-circle/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":null,\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"},\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC017204\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"H00N\",\"ratePlanName\":\"TIC Room Redemption Value - Silver\",\"rateDescription\":\"This offer is applicable only to Taj InnerCircle members for booking against redemption of TIC points, on room only basis. Breakfast is not included in the offer unless otherwise specified. All extras to be borne by the member and paid directly at the hotel. Appropriate point adjustments will be made on your Taj InnerCircle account within 7 days in case of modification or cancellation. Confirmation subject to availability of points. Points redeemed will not be reinstated in case reservation is cancelled after the applicable cancellation period.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":\"H00N\",\"bookingStatus\":true,\"roomCostBeforeTax\":\"11250.00\",\"roomCostAfterTax\":\"13275.00\",\"cancellationPolicy\":\"The reservation cannot be modified. For any assistance, please email innercircle@tajhotels.com\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2025.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"13275.00\",\"price\":\"11250.00\",\"tax\":\"2025.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":false}],\"totalAmountAfterTax\":\"13275.0\",\"totalAmountBeforeTax\":\"11250.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"sampathkumar54@moonraft.com\",\"currencyCode\":\"INR\",\"pinNumber\":\"300000832363082\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}";
        assertEquals(expectingResponse, responseString);
    }

    @Test
    public void testPayByTICPointsPlusCash() throws Exception {

        // Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
        // CHECK_IN_DATE, CHECKOUT_DATE.
        mockingTICRequestParameters();

        // Mocking PAYMENT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_PAYMENT_DETAILS)).thenReturn(
                "{\"payAtHotel\":false,\"payOnlineNow\":true,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"creditCardRequired\":true,\"cardType\":\"\",\"nameOnCard\":\"\",\"cardNumber\":\"\",\"expiryMonth\":\"\",\"ticpoints\":{\"redeemTicOrEpicurePoints\":true,\"ticPlusCreditCard\":\"35031\",\"epicurePlusCreditCard\":\"\",\"pointsPlusCash\":true,\"currencyConversionRate\":1,\"pointsType\":\"TIC\",\"noTicPoints\":\"6638\"},\"currencyCode\":\"INR\"}");

        // Mocking Credit_Card_Details for PaymentCardDetailsService
        mockingPaymentCardDetailsService();

        // Mocking Booking Object for BookingBackendResponse
        mockingBookingBackendResponse(
                "{\"itineraryNumber\":\"21305B3524021\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"R\",\"firstName\":\"Sampathkumar\",\"email\":\"sampathkumar54@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\" 917207592095\",\"gstNumber\":null,\"membershipId\":\"101015496604\",\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":null,\"hotel\":null,\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC017204\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View King Bed\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"H00N\",\"ratePlanName\":\"TIC Room Redemption Value - Silver\",\"rateDescription\":\"This offer is applicable only to Taj InnerCircle members for booking against redemption of TIC points, on room only basis. Breakfast is not included in the offer unless otherwise specified. All extras to be borne by the member and paid directly at the hotel. Appropriate point adjustments will be made on your Taj InnerCircle account within 7 days in case of modification or cancellation. Confirmation subject to availability of points. Points redeemed will not be reinstated in case reservation is cancelled after the applicable cancellation period.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"11250.00\",\"roomCostAfterTax\":\"13275.00\",\"cancellationPolicy\":\"The reservation cannot be modified. For any assistance, please email innercircle@tajhotels.com\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2025.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-17\",\"expireDate\":\"2020-03-17\"}],\"nightlyRates\":[{\"date\":\"3/17/2020\",\"priceWithFeeAndTax\":\"13275.00\",\"price\":\"11250.00\",\"tax\":\"2025.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":false}],\"totalAmountAfterTax\":\"13275.0\",\"totalAmountBeforeTax\":\"11250.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-17\",\"checkOutDate\":\"2020-03-18\",\"userId\":\"sampathkumar54@moonraft.com\",\"currencyCode\":\"INR\",\"pinNumber\":\"300000832363082\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}");

        // Mocking HotelDetails for Querying for Hotel Details using File Resources
        mockingHotelDetails("/content/taj-inner-circle/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai");

        // Mocking CurrencyConversionDetails to fetch conversion rates using File Resources
        mockingCurrencyConversionContent("/content/shared-content/global-shared-content/currency-conversion");

        bookHotelInitiateServlet.doPost(request, response);

        String responseString = stringWriter.getBuffer().toString();

        String expectingResponse = "<form action=\"https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction\" method=\"post\"><input type=\"hidden\" name=\"encRequest\" value=\"cf825bf3c9e022fbdc7c97ffac7e2492d3fc0194651907fdde5fef07551fc9ebfc34fe3a370aac0890e6afe85b7f7405c722d7e2586834e68cda84bdae397b0e1e52f0e37ab114bcf320715542a7101ffa3128e14b8f80d20063b5d3ed77974a4d4f1c2d4c6a45e07d7d3c22e80c0d7892e98b2e679492b0a405de8b0f8eb517c495e5ca5b16f48a4a44367e209e519b2dfa9e33572cbfd47aa81fe4c4b4060c215af6e4d6c59f1fcb458b92b8eb20b38902a69909a782085fd8e02b802fbad7161a40655f374ef54eda85f76dcf73cb2e585b6dc2ab7ff804e7d762cb827b7e645d776d882fd6f00f5af51d68c657a2bbf422eb942cfa130a2a94878fc0233527469a92c9b3d95e5c6387b4f1827784cbbb5d2007534f77e90e912e11a4a88e3b9b7e3689f658efd1ac4c661241c21a18d57a1474b8b59329ca79cb03f7c39656852598ea2896468b3392a83efb61683ffba64a6a4c350f601f2fad2008f89959eea712acb57907c75a009bac62c2ee3b93125cc6e11ca17b5417245a79c1b4bdd4d619d62a1c617735d56e87eb2255ab4adc6a46a68ebc503a75eb896896fa8270dc578313ce6dd4e47afee9014d3c31ae1c8cf710936a1d83e62c56c14deeea9895ad6689b749b4b480b5a047d138eb88f95757e76b012d91f7001c52c4142d4afd5d73ae47bf80aeb8c59e167a198f97c22d1c7e2a831ad3a6eec90c66f009007cc2a43a3eda5b464792cc0d5932b5c3e987fb3459600771e667df89c055cae2aa3d98160aa87a6e673c2c2c1eed9d1612244acb3071fb129ff220e41c151f780ada4246a14b29932058140dcd99e91b14fa655a47a688df6e9b3ff3c24b5f1e873ef2365a67ade4461eaad6d7b49b315dc6db1d56ba952cec52e881346f0634c102d19b1aaaeaac60713e6b43b351377d067044b9b080268cfca93b4e1d00b083b2a9c034816cd5d51d699cce80e84fe79a7c18088cced07423929f724c027767b41748fdc3e1aeb298b6447597edf98cc13effd9b412f486a10f3a8aa9\" /><input type=\"hidden\" name=\"access_code\" value=\"AVQF01FG85AS47FQSA\" /></form>";
        assertEquals(expectingResponse, responseString);
    }

    /**
     * @param currencyConversionContentPath
     * @throws LoginException
     * @throws FileNotFoundException
     */
    protected void mockingCurrencyConversionContent(String currencyConversionContentPath)
            throws LoginException, FileNotFoundException {
        ResourceResolver mockResourceResolver = mock(ResourceResolver.class);
        when(resolverFactory.getServiceResourceResolver(null)).thenReturn(mockResourceResolver);

        ClassLoader classLoader = getClass().getClassLoader();
        File requestFile = new File(classLoader.getResource("currency-conversion.json").getFile());
        context.load().json(new FileInputStream(requestFile), currencyConversionContentPath);

        Resource resource = context.resourceResolver().getResource(currencyConversionContentPath);
        when(mockResourceResolver.getResource(currencyConversionContentPath)).thenReturn(resource);
    }


    /**
     * Mocking Credit_Card_Details for PaymentCardDetailsService
     */
    protected void mockingPaymentCardDetailsService() {
        Mockito.when(paymentCardDetailsService.getCardName()).thenReturn("test");
        Mockito.when(paymentCardDetailsService.getExpiryDate()).thenReturn("12/25");
        Mockito.when(paymentCardDetailsService.getCardNumber()).thenReturn("4111111111111111");
        Mockito.when(paymentCardDetailsService.getCardType()).thenReturn("VI");
    }

    /**
     * @param bookingBackendResponseString
     * @throws IOException
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws Exception
     */
    protected void mockingBookingBackendResponse(String bookingBackendResponseString)
            throws IOException, JsonParseException, JsonMappingException, Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        BookingObject bookingObjectResponse = objectMapper.readValue(bookingBackendResponseString,
                new TypeReference<BookingObject>() {
                });
        Mockito.when(bookingHandler.createRoomReservation(Mockito.any())).thenReturn(bookingObjectResponse);
    }

    /**
     * @param hotelPath
     * @throws RepositoryException
     * @throws FileNotFoundException
     */
    protected void mockingHotelDetails(String hotelPath) throws RepositoryException, FileNotFoundException {
        ResourceResolver mockResourceResolver = mock(ResourceResolver.class);
        when(request.getResourceResolver()).thenReturn(mockResourceResolver);

        QueryBuilder mockQueryBuilder = mock(QueryBuilder.class);
        when(mockResourceResolver.adaptTo(QueryBuilder.class)).thenReturn(mockQueryBuilder);

        Query mockQuery = mock(Query.class);
        when(mockQueryBuilder.createQuery(any(), any())).thenReturn(mockQuery);

        SearchResult mockResult = mock(SearchResult.class);
        when(mockQuery.getResult()).thenReturn(mockResult);

        Hit mockHit = mock(Hit.class);
        when(mockHit.getPath()).thenReturn(hotelPath);

        List<Hit> hits = new ArrayList<>();
        hits.add(mockHit);
        when(mockResult.getHits()).thenReturn(hits);

        ClassLoader classLoader = getClass().getClassLoader();
        File requestFile = new File(classLoader.getResource("taj-tower-hotel-details.json").getFile());
        context.load().json(new FileInputStream(requestFile), hotelPath);

        Resource resource = context.resourceResolver().getResource(hotelPath);
        when(mockResourceResolver.getResource(hotelPath)).thenReturn(resource);
    }

    /**
     * Mocking EtcCCAvenueConfigurations of bookingCCAvenue and ticBookingCCAvenue for localhost Server
     */
    protected void mockingEtcCCAvenueConfigurations() {
        TajHotelBrandsETCConfigurations tajHotelBrandsETCConfigurations = new TajHotelBrandsETCConfigurations(
                "bookingCCAvenue", "AVQF01FG85AS47FQSA",
                "http://localhost:4502/content/tajhotels/en-in/booking-cart.html",
                "http://localhost:4502/bin/postPaymentServlet",
                "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction",
                "E0AF518195DD811AE2526EFF2571F380",
                "http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html", "4", "/content/tajhotels");

        TajHotelBrandsETCConfigurations ticTajHotelBrandsETCConfigurations = new TajHotelBrandsETCConfigurations(
                "ticBookingCCAvenue", "AVQF01FG85AS47FQSA",
                "http://localhost:4502/content/taj-inner-circle/en-in/booking-cart.html",
                "http://localhost:4502/bin/ticPostPaymentServlet",
                "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction",
                "E0AF518195DD811AE2526EFF2571F380",
                "http://localhost:4502/content/taj-inner-circle/en-in/booking-confirmation.html", "4",
                "/content/taj-inner-circle");

        Map<String, TajHotelBrandsETCConfigurations> tajHotelBrandsETCConfigurationsBean = new HashMap<>();
        tajHotelBrandsETCConfigurationsBean.put("bookingCCAvenue", tajHotelBrandsETCConfigurations);
        tajHotelBrandsETCConfigurationsBean.put("ticBookingCCAvenue", ticTajHotelBrandsETCConfigurations);

        TajhotelsBrandAllEtcConfigBean TajhotelsBrandAllEtcConfigBean = new TajhotelsBrandAllEtcConfigBean(
                tajHotelBrandsETCConfigurationsBean, false);

        Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean = new HashMap<>();
        etcConfigBean.put("localhost", TajhotelsBrandAllEtcConfigBean);

        TajHotelsAllBrandsConfigurationsBean tajHotelsAllBrandsConfigurationsBean = new TajHotelsAllBrandsConfigurationsBean();
        tajHotelsAllBrandsConfigurationsBean.setEtcConfigBean(etcConfigBean);
        Mockito.when(tajHotelsBrandsETCService.getEtcConfigBean()).thenReturn(tajHotelsAllBrandsConfigurationsBean);
    }

    /**
     * Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
     * CHECK_IN_DATE, CHECKOUT_DATE.
     */
    protected void mockingRequestParameters() {
        // Mocking Booking GUEST_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_GUEST_DETAILS)).thenReturn(
                "{\"title\":\"Mr.\",\"firstName\":\"test\",\"lastName\":\"testing\",\"email\":\"vijay.kumar@moonraft.com\",\"phoneNumber\":\" 917207592095\",\"gstNumber\":\"\",\"country\":\"India\",\"membershipId\":\"\",\"specialRequests\":\"Please cancel this\"}");

        // Mocking Booking ROOM_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_ROOM_DETAILS)).thenReturn(
                "[{\"hotelId\":75768,\"noOfAdults\":\"1\",\"noOfChilds\":\"0\",\"roomTypeCode\":\"CKC\",\"roomCostAfterTax\":13750,\"bedType\":\"king\",\"roomTypeName\":\"Superior Room City View\",\"ratePlanCode\":\"T05\",\"promoCode\":\"\"}]");

        // Mocking Booking FLIGHT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_FLIGHT_DETAILS))
                .thenReturn("{\"arrivalFlightNo\":\"\",\"flightArrivalTime\":\"\",\"arrivalAirportName\":\"\"}");

        // Mocking Booking HOTEL_CHAIN_CODE for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_HOTEL_CHAIN_CODE)).thenReturn("21305");

        // Mocking Booking HOTEL_ID for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_HOTEL_ID)).thenReturn("75768");

        // Mocking Booking LOCATION_ID for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.HOTEL_LOCATION_ID)).thenReturn("HLTBOMTM");

        // Mocking Booking CHECK_IN for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_CHECK_IN_DATE)).thenReturn("2020-03-17");

        // Mocking Booking CHECKOUT_DATE for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_CHECKOUT_DATE)).thenReturn("2020-03-18");
    }

    /**
     * Mocking Booking GUEST_DETAILS, ROOM_DETAILS, FLIGHT_DETAILS, CHAIN_CODE, HOTEL_ID, HOTEL_LOCATION_ID,
     * CHECK_IN_DATE, CHECKOUT_DATE.
     */
    protected void mockingTICRequestParameters() {
        // Mocking SynixsDowntimeCheck as false in SynixsDowntimeConfigurationService
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(false);

        // Mocking Booking GUEST_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_GUEST_DETAILS)).thenReturn(
                "{\"title\":\"Mr.\",\"firstName\":\"Sampathkumar\",\"lastName\":\"R\",\"email\":\"sampathkumar54@moonraft.com\",\"phoneNumber\":\" 91200902221916\",\"gstNumber\":\"\",\"country\":\"India\",\"membershipId\":\"101015496604\",\"specialRequests\":\"Please cancel this\"}");

        // Mocking Booking ROOM_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_ROOM_DETAILS)).thenReturn(
                "[{\"hotelId\":75768,\"noOfAdults\":\"1\",\"noOfChilds\":\"0\",\"roomTypeCode\":\"CKC\",\"roomCostAfterTax\":11250,\"bedType\":\"king\",\"roomTypeName\":\"Superior Room City View\",\"ratePlanCode\":\"H00N\",\"promoCode\":\"H00N\"}]");

        // Mocking Booking FLIGHT_DETAILS for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_FLIGHT_DETAILS))
                .thenReturn("{\"arrivalFlightNo\":\"\",\"flightArrivalTime\":\"\",\"arrivalAirportName\":\"\"}");

        // Mocking Booking HOTEL_CHAIN_CODE for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_HOTEL_CHAIN_CODE)).thenReturn("21305");

        // Mocking Booking HOTEL_ID for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_HOTEL_ID)).thenReturn("75768");

        // Mocking Booking LOCATION_ID for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.HOTEL_LOCATION_ID)).thenReturn("");

        // Mocking Booking CHECK_IN for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_CHECK_IN_DATE)).thenReturn("2020-03-17");

        // Mocking Booking CHECKOUT_DATE for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.FORM_CHECKOUT_DATE)).thenReturn("2020-03-18");

        // Mocking Booking CHECKOUT_DATE for Request Parameter
        Mockito.when(request.getParameter(BookingConstants.LOGGED_IN_USER_DETAILS)).thenReturn(
                "{\"firstName\":\"Sampathkumar\",\"lastName\":\"R\",\"email\":\"sampathkumar54@moonraft.com\",\"cdmReferenceId\":\"300000832363082\"}");
    }
}
