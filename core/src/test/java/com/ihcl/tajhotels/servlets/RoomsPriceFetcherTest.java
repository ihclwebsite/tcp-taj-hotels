/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ihcl.core.hotels.BrandDetailsBean;
import com.ihcl.core.hotels.DestinationDetailsBean;
import com.ihcl.core.hotels.DestinationHotelBean;
import com.ihcl.core.hotels.DestinationHotelMapBean;
import com.ihcl.core.hotels.HotelBean;
import com.ihcl.core.hotels.HotelDetailsBean;
import com.ihcl.core.hotels.TajhotelsHotelsBeanService;
import com.ihcl.core.hotels.WebsiteSpecificHotelsBean;
import com.ihcl.core.services.booking.SynixsDowntimeConfigurationService;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.availability.DestinationPagePriceResponse;
import com.ihcl.integration.dto.availability.HotelDto;

import junitx.util.PrivateAccessor;

/**
 * @author apple
 *
 */
public class RoomsPriceFetcherTest {

    @Mock
    RequestPathInfo requestPathInfo;

    @Mock
    SlingHttpServletRequest request;

    @Mock
    SlingHttpServletResponse response;

    @Mock
    WebsiteSpecificHotelsBean websiteSpecificHotelsBean;

    @Mock
    IRoomAvailabilityService roomAvailabilityService;

    @Mock
    TajhotelsHotelsBeanService tajhotelsHotelsBeanService;

    @Mock
    SynixsDowntimeConfigurationService synixsDowntimeConfigurationService;

    ObjectMapper objectMapper = new ObjectMapper();

    RoomsPriceFetcher roomsPriceFetcher = new RoomsPriceFetcher();

    // "http://localhost:4502/bin/hotel-rates/destination-hotel-rates.rates/tajhotels/taj:destination+india+mumbai/2019-11-25/2019-11-26/1,1/STD//destinationRatesCache.json";

    @Before
    public void setup() throws NoSuchFieldException, IOException {

        MockitoAnnotations.initMocks(this);
        PrivateAccessor.setField(roomsPriceFetcher, "roomAvailabilityService", roomAvailabilityService);
        PrivateAccessor.setField(roomsPriceFetcher, "tajhotelsHotelsBeanService", tajhotelsHotelsBeanService);
        PrivateAccessor.setField(roomsPriceFetcher, "synixsDowntimeConfigurationService",
                synixsDowntimeConfigurationService);

        Mockito.when(synixsDowntimeConfigurationService.getDowntimeMessage())
                .thenReturn("Synixs under downtime. Please try after some time.");
    }


    @Test
    public void testSynxisDownTime() throws ServletException, IOException, JSONException {
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(true);
        Mockito.when(synixsDowntimeConfigurationService.getServletStatusCode()).thenReturn("413");

        String suffixString = "/tajhotels/taj:destination+india+mumbai/2019-11-25/2019-11-26/1,1/STD//destinationRatesCache.json";
        Mockito.when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        Mockito.when(requestPathInfo.getSuffix()).thenReturn(suffixString);

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        Mockito.when(response.getWriter()).thenReturn(printWriter);

        roomsPriceFetcher.doGet(request, response);

        String result = stringWriter.getBuffer().toString().trim();
        assertNotNull(result);

        System.out.println("Result: " + result);
        JSONObject resultJSONObject = new JSONObject(result);
        assertEquals("FAIL", resultJSONObject.get("responseCode"));
    }

    @Test
    public void testSTDRatesForDestinations() throws Exception {
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(false);
        String suffixString = "/tajhotels/taj:destination+india+mumbai/2019-11-25/2019-11-26/1,1/STD//destinationRatesCache.json";
        String resourcePath = "/bin/hotel-rates/destination-hotel-rates";

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        Mockito.when(response.getWriter()).thenReturn(printWriter);

        Mockito.when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        Mockito.when(requestPathInfo.getSuffix()).thenReturn(suffixString);
        Mockito.when(requestPathInfo.getResourcePath()).thenReturn(resourcePath);

        Mockito.when(tajhotelsHotelsBeanService.getWebsiteSpecificHotelsBean()).thenReturn(websiteSpecificHotelsBean);
        Mockito.when(websiteSpecificHotelsBean.getWebsiteHotelsMap()).thenReturn(getWebsiteHotelsMap());

        Mockito.when(roomAvailabilityService.getAveragePriceForDestinationPage(Mockito.any()))
                .thenReturn(getRoomAvailabilityResponse());

        roomsPriceFetcher.doGet(request, response);

        String result = stringWriter.getBuffer().toString().trim();
        assertNotNull(result);

        System.out.println("Result: " + result);
        JSONObject resultJSONObject = new JSONObject(result);

        assertNotNull(resultJSONObject);
        assertEquals("SUCCESS", resultJSONObject.get("responseCode"));
        assertNotNull(resultJSONObject.get("hotelDetails"));

        System.out.println(resultJSONObject.get("hotelDetails"));
        JSONArray hotelDetailsJSONArray = new JSONArray(resultJSONObject.get("hotelDetails").toString());

        for (int i = 0; i < hotelDetailsJSONArray.length(); i++) {
            JSONObject hotelDetailsJSONObject = hotelDetailsJSONArray.getJSONObject(i);
            assertNotNull(hotelDetailsJSONObject);
            JSONObject currencyCodeJSONObject = hotelDetailsJSONObject.getJSONObject("currencyCode");
            assertNotNull(currencyCodeJSONObject);
            assertNotNull(currencyCodeJSONObject.get("currencyString"));
        }
    }

    @Test
    public void testCorporateRatesForDestinations() throws Exception {
        Mockito.when(synixsDowntimeConfigurationService.isForcingToSkipCalls()).thenReturn(false);
        String suffixString = "/tajhotels/taj:destination+india+mumbai/2019-11-25/2019-11-26/1,1//90185976/destinationRatesCache.json";
        String resourcePath = "/bin/hotel-rates/destination-hotel-rates";

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        Mockito.when(response.getWriter()).thenReturn(printWriter);

        Mockito.when(request.getRequestPathInfo()).thenReturn(requestPathInfo);
        Mockito.when(requestPathInfo.getSuffix()).thenReturn(suffixString);
        Mockito.when(requestPathInfo.getResourcePath()).thenReturn(resourcePath);

        Mockito.when(tajhotelsHotelsBeanService.getWebsiteSpecificHotelsBean()).thenReturn(websiteSpecificHotelsBean);
        Mockito.when(websiteSpecificHotelsBean.getWebsiteHotelsMap()).thenReturn(getWebsiteHotelsMap());

        Mockito.when(roomAvailabilityService.getAveragePriceForDestinationPage(Mockito.any()))
                .thenReturn(getRoomAvailabilityResponse());

        roomsPriceFetcher.doGet(request, response);

        String result = stringWriter.getBuffer().toString().trim();
        assertNotNull(result);

        System.out.println("Result: " + result);
        JSONObject resultJSONObject = new JSONObject(result);

        assertNotNull(resultJSONObject);
        assertEquals("SUCCESS", resultJSONObject.get("responseCode"));
        assertNotNull(resultJSONObject.get("hotelDetails"));

        System.out.println(resultJSONObject.get("hotelDetails"));
        JSONArray hotelDetailsJSONArray = new JSONArray(resultJSONObject.get("hotelDetails").toString());

        for (int i = 0; i < hotelDetailsJSONArray.length(); i++) {
            JSONObject hotelDetailsJSONObject = hotelDetailsJSONArray.getJSONObject(i);
            assertNotNull(hotelDetailsJSONObject);
            JSONObject currencyCodeJSONObject = hotelDetailsJSONObject.getJSONObject("currencyCode");
            assertNotNull(currencyCodeJSONObject);
            assertNotNull(currencyCodeJSONObject.get("currencyString"));
        }
    }


    /**
     * @return
     * @throws IOException
     * @throws JsonParseException
     * @throws JsonMappingException
     */
    protected DestinationPagePriceResponse getRoomAvailabilityResponse()
            throws IOException, JsonParseException, JsonMappingException {
        String hotelPriceString = "[{\"hotelCode\":\"75691\",\"availableRooms\":null,\"lowestPrice\":9000,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":9000,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}},{\"hotelCode\":\"75764\",\"availableRooms\":null,\"lowestPrice\":26000,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":26000,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}},{\"hotelCode\":\"75768\",\"availableRooms\":null,\"lowestPrice\":13000,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":13000,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}},{\"hotelCode\":\"75688\",\"availableRooms\":null,\"lowestPrice\":16000,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":16000,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}},{\"hotelCode\":\"75765\",\"availableRooms\":null,\"lowestPrice\":16500,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":16500,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}},{\"hotelCode\":\"75692\",\"availableRooms\":null,\"lowestPrice\":0,\"lowestPriceTax\":0,\"lowestDiscountedPrice\":0,\"lowestTotalPrice\":0,\"warnings\":null,\"errors\":null,\"currencyCode\":{\"currencyString\":\"INR\",\"shortCurrencyString\":null}}]";

        List<HotelDto> hotelDtoList = objectMapper.readValue(hotelPriceString,
                objectMapper.getTypeFactory().constructCollectionType(List.class, HotelDto.class));

        DestinationPagePriceResponse roomAvailabilityResponse = new DestinationPagePriceResponse();
        roomAvailabilityResponse.setHotelPricesList(hotelDtoList);
        roomAvailabilityResponse.setSuccess(true);
        return roomAvailabilityResponse;
    }

    /**
     * @return
     */
    protected Map<String, DestinationHotelMapBean> getWebsiteHotelsMap() {

        HotelBean HotelBean = new HotelBean(getHotelsList());
        BrandDetailsBean BrandDetailsBean = new BrandDetailsBean("taj",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj", HotelBean);

        Map<String, BrandDetailsBean> brandHotels = new HashMap<>();
        brandHotels.put("taj", BrandDetailsBean);

        DestinationDetailsBean destinationDetailsBean = new DestinationDetailsBean("hotels-in-mumbai",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai", "Mumbai",
                "/content/dam/luxury/hotels/Taj_Mahal_Mumbai/images/3x2/ViewoftheGatewayofIndia-3x2.jpg",
                "taj:destination/india/mumbai", "7caaebcb-0a6a-4e69-9a66-a0d41218d2ed", "/content/tajhotels/en-in", "test");

        DestinationHotelBean DestinationHotelBean = new DestinationHotelBean(brandHotels, destinationDetailsBean, null);

        Map<String, DestinationHotelBean> destinationMap = new HashMap<>();
        destinationMap.put("hotels-in-mumbai", DestinationHotelBean);
        DestinationHotelMapBean DestinationHotelMapBean = new DestinationHotelMapBean(destinationMap);
        Map<String, DestinationHotelMapBean> websiteHotelsMap = new HashMap<>();
        websiteHotelsMap.put("/content/tajhotels/en-in", DestinationHotelMapBean);
        return websiteHotelsMap;
    }

    /**
     * @return
     */
    protected List<HotelDetailsBean> getHotelsList() {
        HotelDetailsBean landsEndHotelDetailsBean = new HotelDetailsBean("Taj Lands End", "75688",
                "landsend.mumbai@tajhotels.com", "India", "taj:hotels/brands/taj", "6668-1234",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-lands-end-mumbai", "Mumbai",
                "/content/dam/luxury/tle/16x7/Exterior-16x7.jpg", "Maharashtra", "taj", null, null, null, null, null,
                null, null);

        HotelDetailsBean santacruzHotelDetailsBean = new HotelDetailsBean("Taj Santacruz", "75765",
                "resvs.santacruz@tajhotels.com", "India", "taj:hotels/brands/taj", "6211 5211",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-santacruz-mumbai", "Mumbai",
                "/content/dam/luxury/hotels/Taj_Santacruz/images/16x7/Taj Santacruz exterior.jpg", "Maharashtra", "taj",
                null, null, null, null, null, null, null);

        HotelDetailsBean wellingtonHotelDetailsBean = new HotelDetailsBean("Taj Wellington Mews", "75692",
                "twmresv.mumbai@tajhotels.com", "India", "taj:hotels/brands/taj", "66569494",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-wellington-mews-luxury-residences-mumbai",
                "Mumbai", "/content/dam/luxury/hotels/old-wellington-mews/images/at-a-glance/Exterior-7-16x7.jpg",
                "Maharashtra", "taj", null, null, null, null, null, null, null);

        HotelDetailsBean tajMahalTowerHotelDetailsBean = new HotelDetailsBean("Taj Mahal Tower", "75768",
                "Tmhresv.bom@tajhotels.com", "India", "taj:hotels/brands/taj", "6665 3000",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai", "Mumbai",
                "/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg", "Maharashtra",
                "taj", null, null, null, null, null, null, null);

        HotelDetailsBean tajMahalPalaceHotelDetailsBean = new HotelDetailsBean("Taj Mahal Palace", "75764",
                "tmhbc.bom@tajhotels.com", "India", "taj:hotels/brands/taj", "6665 3333",
                "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-palace-mumbai", "Mumbai",
                "/content/dam/luxury/hotels/Taj_Mahal_Mumbai/images/16x7/AAG_27642205-H1-015Pooside-16x7.jpg",
                "Maharashtra", "taj", null, null, null, null, null, null, null);

        List<HotelDetailsBean> hotelsList = new ArrayList<>();
        hotelsList.add(landsEndHotelDetailsBean);
        hotelsList.add(santacruzHotelDetailsBean);
        hotelsList.add(wellingtonHotelDetailsBean);
        hotelsList.add(tajMahalTowerHotelDetailsBean);
        hotelsList.add(tajMahalPalaceHotelDetailsBean);
        return hotelsList;
    }
}
