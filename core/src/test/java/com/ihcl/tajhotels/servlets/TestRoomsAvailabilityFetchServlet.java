package com.ihcl.tajhotels.servlets;

import org.junit.Ignore;

@Ignore
public class TestRoomsAvailabilityFetchServlet {

//    @Rule
//    public AemContext context = new AemContext();
//
//    private static final Logger LOG = LoggerFactory.getLogger(TestRoomsAvailabilityFetchServlet.class);
//
//    private RoomsAvailabilityFetchServlet availabilityFetch;
//
//    private IRoomAvailabilityService mockRoomAvailabilityService;
//
//    @Before
//    public void setup() {
//        availabilityFetch = new RoomsAvailabilityFetchServlet();
//        mockRoomAvailabilityService = mock(IRoomAvailabilityService.class);
//        context.registerService(IRoomAvailabilityService.class, mockRoomAvailabilityService);
//        context.registerInjectActivateService(availabilityFetch);
//    }
//
//    @After
//    public void teardown() {
//
//    }
//
//    /**
//     * @param responseString
//     * @param code
//     * @param message
//     * @throws IOException
//     * @throws JsonProcessingException
//     */
//    private void assertResponse(String responseString, String code, String message)
//            throws IOException, JsonProcessingException {
//        assertNotNull(responseString);
//        JsonNode jsonNode = getJsonNode(responseString);
//        assertNotNull(jsonNode);
//        assertResponseMessage(jsonNode, message);
//        assertResponseCode(jsonNode, code);
//    }
//
//    /**
//     * @param responseString
//     * @return
//     * @throws IOException
//     * @throws JsonProcessingException
//     */
//    private JsonNode getJsonNode(String responseString) throws IOException, JsonProcessingException {
//        ObjectMapper mapper = new ObjectMapper();
//        JsonNode jsonNode = mapper.readTree(responseString);
//        return jsonNode;
//    }
//
//    /**
//     * @param jsonNode
//     * @param code
//     */
//    private void assertResponseCode(JsonNode jsonNode, String code) {
//        assertTrue("Unable to find response code.", jsonNode.has(RESPONSE_CODE_KEY));
//        assertEquals(code, jsonNode.get(RESPONSE_CODE_KEY).asText());
//    }
//
//    /**
//     * @param jsonNode
//     * @param message
//     */
//    private void assertResponseMessage(JsonNode jsonNode, String message) {
//        assertTrue(jsonNode.has(MESSAGE_KEY));
//        assertEquals(message, jsonNode.get(MESSAGE_KEY).asText());
//    }
//
//    @Test
//    public void testServletFailureEmptyResponse() throws Exception {
//        LOG.debug("Object under test: " + availabilityFetch);
//        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);
//        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
//
//        when(mockRoomAvailabilityService.getAvailability(Mockito.any())).thenThrow(Exception.class);
//
//        StringWriter stringWriter = new StringWriter();
//        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
//
//        availabilityFetch.doGet(mockRequest, mockResponse);
//
//        verify(mockResponse).getWriter();
//        String responseString = stringWriter.toString();
//
//        assertResponse(responseString, RESPONSE_CODE_FAILURE, ROOMS_AVAILABILITY_FAILURE);
//    }
//
//    @Test
//    public void testSendReceivedResponseAsJson() throws Exception {
//        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);
//        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
//
//        String hotelId = "Random Hotel-Id";
//        String rateFilterCode = "STANDARD";
//        String[] hotelIdentifiers = { hotelId };
//        when(mockRequest.getParameterValues(HttpRequestParams.HOTEL_IDS)).thenReturn(hotelIdentifiers);
//        when(mockRequest.getParameter(HttpRequestParams.RATE_FILTER_CODE)).thenReturn(rateFilterCode);
//
//        IAvailabilityResponse mockAvailabilityResponse = mock(IAvailabilityResponse.class, "Created in test case.");
//
//        Map<String, IHotels> hotelsMap = new HashMap<>();
//
//        IHotels mockHotel = mock(IHotels.class);
//        hotelsMap.put(hotelId, mockHotel);
//
//        when(mockAvailabilityResponse.getRoomAvailabilityFor(Mockito.any())).thenReturn(hotelsMap);
//
//
//        Map<String, IRoomAvailability> roomsMap = new HashMap<>();
//
//        IRoomAvailability mockRoomAvailability = mock(IRoomAvailability.class);
//        roomsMap.put("roomTypeCode1", mockRoomAvailability);
//
//        when(mockHotel.getAvailableRooms()).thenReturn(roomsMap);
//
//        when(mockRoomAvailabilityService.getAvailability(Mockito.any())).thenReturn(mockAvailabilityResponse);
//
//        StringWriter stringWriter = new StringWriter();
//        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
//
//        availabilityFetch.doGet(mockRequest, mockResponse);
//
//        verify(mockResponse).getWriter();
//
//        ArgumentCaptor<IAvailabilityRequest> argument = ArgumentCaptor.forClass(IAvailabilityRequest.class);
//        verify(mockRoomAvailabilityService).getAvailability(argument.capture());
//        IAvailabilityRequest capturedAvailabilityRequest = argument.getValue();
//        assertNotNull("Was expecting to invoke availability service with a non null request.",
//                capturedAvailabilityRequest);
//        List<String> capturedHotelIdentifiers = capturedAvailabilityRequest.getHotelIdentifiers();
//        assertNotNull("Expected to pass a list of hotel ids in the request.", capturedHotelIdentifiers);
//        assertTrue("Expected the list of hotel ids to contain the hotel id passed in http request.",
//                capturedHotelIdentifiers.contains(hotelId));
//
//
//        String responseString = stringWriter.toString();
//        JsonNode responseJsonNode = getJsonNode(responseString);
//        assertResponseMessage(responseJsonNode, ROOMS_AVAILABILITY_SUCCESS);
//        assertResponseCode(responseJsonNode, RESPONSE_CODE_SUCCESS);
//
//        assertTrue(responseJsonNode.has(hotelId));
//    }
//
//
//    @Test
//    public void testResponseJsonContainsRooms() throws Exception {
//        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);
//        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
//
//        String hotelId = "Random Hotel-Id";
//        String rateFilterCode = "STANDARD";
//        String[] hotelIdentifiers = { hotelId };
//        when(mockRequest.getParameterValues(HttpRequestParams.HOTEL_IDS)).thenReturn(hotelIdentifiers);
//        when(mockRequest.getParameter(HttpRequestParams.RATE_FILTER_CODE)).thenReturn(rateFilterCode);
//
//        IAvailabilityResponse mockAvailabilityResponse = mock(IAvailabilityResponse.class, "Created in test case.");
//
//        Map<String, IHotels> hotelsMap = new HashMap<>();
//
//        IHotels mockHotel = mock(IHotels.class);
//        hotelsMap.put(hotelId, mockHotel);
//
//        when(mockAvailabilityResponse.getRoomAvailabilityFor(Mockito.any())).thenReturn(hotelsMap);
//
//
//        Map<String, IRoomAvailability> roomsMap = new HashMap<>();
//
//        String roomTypeCode = "roomTypeCode1";
//        IRoomAvailability mockRoomAvailability = mock(IRoomAvailability.class);
//        roomsMap.put(roomTypeCode, mockRoomAvailability);
//
//        when(mockHotel.getAvailableRooms()).thenReturn(roomsMap);
//
//        when(mockRoomAvailabilityService.getAvailability(Mockito.any())).thenReturn(mockAvailabilityResponse);
//
//        StringWriter stringWriter = new StringWriter();
//        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
//
//        availabilityFetch.doGet(mockRequest, mockResponse);
//
//        verify(mockResponse).getWriter();
//
//        String responseString = stringWriter.toString();
//        JsonNode responseJsonNode = getJsonNode(responseString);
//        assertTrue(responseJsonNode.has(hotelId));
//        JsonNode hotelNode = responseJsonNode.get(hotelId);
//        assertFalse("Expected hotel id to contain a value.", hotelNode instanceof NullNode);
//
//        JsonNode roomsAvailabilityNode = hotelNode.get(HttpResponseParams.HOTEL_ROOMS_AVAILABILITY);
//        assertFalse("Expected hotel to contain roomsAvailability.", roomsAvailabilityNode instanceof NullNode);
//
//        JsonNode roomAvailabilityNode = roomsAvailabilityNode.get(roomTypeCode);
//        assertFalse("Expected rooms availability to contain room of code: " + roomTypeCode,
//                roomAvailabilityNode instanceof NullNode);
//
//
//    }
//
//
//    @Test
//    public void testServletCallServiceWithHotelIdFromRequest() throws Exception {
//        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);
//        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
//
//        String hotelId = "RandomHotelId";
//        String[] hotelIds = { hotelId };
//        when(mockRequest.getParameterValues(HttpRequestParams.HOTEL_IDS)).thenReturn(hotelIds);
//
//        IAvailabilityResponse mockAvailabilityResponse = mock(IAvailabilityResponse.class);
//        when(mockRoomAvailabilityService.getAvailability(Mockito.any())).thenReturn(mockAvailabilityResponse);
//
//        StringWriter stringWriter = new StringWriter();
//        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
//
//        availabilityFetch.doGet(mockRequest, mockResponse);
//
//        ArgumentCaptor<IAvailabilityRequest> argument = ArgumentCaptor.forClass(IAvailabilityRequest.class);
//        verify(mockRoomAvailabilityService).getAvailability(argument.capture());
//        IAvailabilityRequest capturedAvailabilityRequest = argument.getValue();
//        List<String> hotelIdentifiers = capturedAvailabilityRequest.getHotelIdentifiers();
//        assertTrue(hotelIdentifiers.contains(hotelId));
//
//    }

}
