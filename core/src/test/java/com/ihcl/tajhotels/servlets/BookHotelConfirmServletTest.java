/**
 *
 */
package com.ihcl.tajhotels.servlets;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.ihcl.core.brands.configurations.TajHotelBrandsETCConfigurations;
import com.ihcl.core.brands.configurations.TajHotelsAllBrandsConfigurationsBean;
import com.ihcl.core.brands.configurations.TajHotelsBrandsETCService;
import com.ihcl.core.brands.configurations.TajhotelsBrandAllEtcConfigBean;
import com.ihcl.core.servicehandler.createBooking.IBookingRequestHandler;
import com.ihcl.integration.api.IRoomAvailabilityService;
import com.ihcl.integration.dto.details.BookingObject;
import com.ihcl.integration.dto.details.Hotel;
import com.ihcl.loyalty.config.ISibelConfiguration;
import com.ihcl.loyalty.services.IRedemptionTransactionsService;
import com.ihcl.tajhotels.constants.BookingConstants;

import io.wcm.testing.mock.aem.junit.AemContext;


/**
 * @author apple
 *
 */
@Ignore
public class BookHotelConfirmServletTest {

    @Rule
    public final AemContext context = new AemContext(ResourceResolverType.JCR_MOCK);

    private BookHotelConfirmServlet classUnderTest = new BookHotelConfirmServlet();

    @Before
    public void setup() throws NoSuchFieldException, IOException {
    }

    @Test
    public void testPayAtHotel() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        IBookingRequestHandler mockBookingRequestHandler = mock(IBookingRequestHandler.class,
                "Mock IBookingRequestHandler created in testPayAtHotel");
        context.registerService(IBookingRequestHandler.class, mockBookingRequestHandler);

        IRedemptionTransactionsService mockRedemptionTransactionsService = mock(IRedemptionTransactionsService.class);
        context.registerService(IRedemptionTransactionsService.class, mockRedemptionTransactionsService);

        TajHotelsBrandsETCService mockTajHotelsBrandsETCService = mock(TajHotelsBrandsETCService.class);
        context.registerService(TajHotelsBrandsETCService.class, mockTajHotelsBrandsETCService);

        IRoomAvailabilityService mockRoomAvailabilityService = mock(IRoomAvailabilityService.class);
        context.registerService(IRoomAvailabilityService.class, mockRoomAvailabilityService);

        ISibelConfiguration mockSibelConfiguration = mock(ISibelConfiguration.class);
        context.registerService(ISibelConfiguration.class, mockSibelConfiguration);

        classUnderTest = context.registerInjectActivateService(new BookHotelConfirmServlet());

        String resourcePathString = "/bin/postPaymentServlet";
        String bookingObjectReqeustString = "{\"itineraryNumber\":\"21305B3138884\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"testing\",\"firstName\":\"test\",\"email\":\"vijay.kumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"917207592095\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":{\"payAtHotel\":true,\"creditCardRequired\":true,\"nameOnCard\":\"test\",\"cardNumber\":\"************1111\",\"expiryMonth\":\"12/20\",\"currencyCode\":\"INR\",\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"ticpoints\":{\"redeemTicOrEpicurePoints\":false,\"ticPlusCreditCard\":null,\"epicurePlusCreditCard\":null,\"pointsType\":null,\"noTicPoints\":0,\"noEpicurePoints\":0,\"pointsPlusCash\":false,\"currencyConversionRate\":null},\"cardType\":\"VI\"},\"hotel\":{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":\"HLTBOMTM\",\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"},\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC000778\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"R04\",\"ratePlanName\":\"Best Flexible Rate With WiFi\",\"rateDescription\":\"Includes accommodation and free WiFi for four devices with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":\"\",\"bookingStatus\":true,\"roomCostBeforeTax\":\"30000.00\",\"roomCostAfterTax\":\"35400.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"5400.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-02-25\",\"expireDate\":\"2020-02-26\"}],\"nightlyRates\":[{\"date\":\"2/25/2020\",\"priceWithFeeAndTax\":\"17700.00\",\"price\":\"15000.00\",\"tax\":\"2700.00\",\"fee\":\"0.00\"},{\"date\":\"2/26/2020\",\"priceWithFeeAndTax\":\"17700.00\",\"price\":\"15000.00\",\"tax\":\"2700.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":true},{\"reservationNumber\":\"75768SC000779\",\"reservationSubString\":null,\"roomTypeCode\":\"BKC\",\"roomTypeName\":\"Deluxe Room City View\",\"roomTypeDesc\":\"This room has a contemporary atmosphere with its minimalistic decor. Work seamlessly, using the work desk, voicemail, and internet facility. Enjoy your favorite program on the high-definition television and sip a drink from our well-stocked minibar. Take a rejuvenating shower in the bathroom that comes with a bathrobe, and hairdryer.\",\"ratePlanCode\":\"N57M\",\"ratePlanName\":\"Taj Inner Circle Member Exclusive Rate\",\"rateDescription\":\"Taj Innercircle member only rate. Rate includes buffet breakfast, 20 percent off spa, salon, food and soft beverage (except at Wasabi) and WiFi for four devices. Flexible cancellation. Credit card guarantee is required. Taxes extra. Valid membership number required at check in to avail this offer.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":1,\"bedType\":null,\"promoCode\":\"N57M\",\"bookingStatus\":true,\"roomCostBeforeTax\":\"29700.00\",\"roomCostAfterTax\":\"35046.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"5346.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-02-25\",\"expireDate\":\"2020-02-26\"}],\"nightlyRates\":[{\"date\":\"2/25/2020\",\"priceWithFeeAndTax\":\"17523.00\",\"price\":\"14850.00\",\"tax\":\"2673.00\",\"fee\":\"0.00\"},{\"date\":\"2/26/2020\",\"priceWithFeeAndTax\":\"17523.00\",\"price\":\"14850.00\",\"tax\":\"2673.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Pending\",\"cancellable\":true}],\"totalAmountAfterTax\":\"70446.0\",\"totalAmountBeforeTax\":\"59700.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-02-25\",\"checkOutDate\":\"2020-02-27\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}";

        String bookingResponseString = "{\"itineraryNumber\":\"21305B3182074\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"Booking\",\"firstName\":\"Test\",\"email\":\"sampathkumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"91098098098\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Test booking please ignore\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":{\"payAtHotel\":true,\"creditCardRequired\":false,\"nameOnCard\":null,\"cardNumber\":null,\"expiryMonth\":null,\"currencyCode\":\"INR\",\"payOnlineNow\":false,\"payUsingGiftCard\":false,\"payUsingCerditCard\":false,\"ticpoints\":{\"redeemTicOrEpicurePoints\":false,\"ticPlusCreditCard\":null,\"epicurePlusCreditCard\":null,\"pointsType\":null,\"noTicPoints\":0,\"noEpicurePoints\":0,\"pointsPlusCash\":false,\"currencyConversionRate\":null},\"cardType\":null},\"hotel\":{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":null,\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"},\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75748\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75748SC000522\",\"reservationSubString\":null,\"roomTypeCode\":\"BKX\",\"roomTypeName\":\"Superior Charm City View King Bed\",\"roomTypeDesc\":\"This room comes equipped with a tea/coffee maker, minibar, ironing board and an electronic safe. The separate working table makes it easy to take care of business in the room. A high-definition television with plug-ins for MP3 and iPod & iPad ensures optimal entertainment.\",\"ratePlanCode\":\"T05\",\"ratePlanName\":\"Bed Breakfast and More\",\"rateDescription\":\"Rate inclusive of buffet breakfast at all day dining. Flexible cancellation policy. No minimum length of stay required.\\nAdditional taxes applicable are extra.\",\"hotelId\":\"75748\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":\"king\",\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"2000.00\",\"roomCostAfterTax\":\"2240.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"240.0\",\"taxName\":\"GOODS AND SERVICE TAX 12PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 12PCT\",\"effectiveDate\":\"2020-03-11\",\"expireDate\":\"2020-03-11\"}],\"nightlyRates\":[{\"date\":\"3/11/2020\",\"priceWithFeeAndTax\":\"2240.00\",\"price\":\"2000.00\",\"tax\":\"240.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Committed\",\"cancellable\":true}],\"totalAmountAfterTax\":\"2240.0\",\"totalAmountBeforeTax\":\"2000.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-11\",\"checkOutDate\":\"2020-03-12\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}";


        BookingObject bookingObjectResponse = objectMapper.readValue(bookingResponseString,
                new TypeReference<BookingObject>() {
                });
        String hoteldetailsString = "{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":\"HLTBOMTM\",\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"}";
        Hotel hotel = objectMapper.readValue(hoteldetailsString, new TypeReference<Hotel>() {
        });

        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);

        RequestPathInfo mockRequestPathInfo = mock(RequestPathInfo.class);

        when(mockRequest.getRequestPathInfo()).thenReturn(mockRequestPathInfo);
        when(mockRequestPathInfo.getResourcePath()).thenReturn(resourcePathString);
        when(mockRequest.getRequestURL()).thenReturn(new StringBuffer("http://localhost:4502/bin/postPaymentServlet"));
        when(mockRequest.getServerName()).thenReturn("localhost");

        TajHotelBrandsETCConfigurations tajHotelBrandsETCConfigurations = new TajHotelBrandsETCConfigurations(
                "bookingCCAvenue", "AVQF01FG85AS47FQSA",
                "http://localhost:4502/content/tajhotels/en-in/booking-cart.html",
                "http://localhost:4502/bin/postPaymentServlet",
                "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction",
                "E0AF518195DD811AE2526EFF2571F380",
                "http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html", "4", "/content/tajhotels");

        Map<String, TajHotelBrandsETCConfigurations> tajHotelBrandsETCConfigurationsBean = new HashMap<>();
        tajHotelBrandsETCConfigurationsBean.put("bookingCCAvenue", tajHotelBrandsETCConfigurations);

        TajhotelsBrandAllEtcConfigBean TajhotelsBrandAllEtcConfigBean = new TajhotelsBrandAllEtcConfigBean(
                tajHotelBrandsETCConfigurationsBean, false);
        // when(tajHotelsBrandsETCService.getEtcConfigBean()).thenReturn(tajHotelBrandsETCConfigurations);

        TajHotelsAllBrandsConfigurationsBean tajHotelsAllBrandsConfigurationsBean = new TajHotelsAllBrandsConfigurationsBean();
        Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean = new HashMap<>();

        etcConfigBean.put("localhost", TajhotelsBrandAllEtcConfigBean);

        tajHotelsAllBrandsConfigurationsBean.setEtcConfigBean(etcConfigBean);
        when(mockTajHotelsBrandsETCService.getEtcConfigBean()).thenReturn(tajHotelsAllBrandsConfigurationsBean);

        when(mockRequest.getParameter(BookingConstants.BOOKING_OBJECT_REQUEST)).thenReturn(bookingObjectReqeustString);
        ResourceResolver mockResourceResolver = mock(ResourceResolver.class);
        when(mockRequest.getResourceResolver()).thenReturn(mockResourceResolver);

        QueryBuilder mockQueryBuilder = mock(QueryBuilder.class);
        when(mockResourceResolver.adaptTo(QueryBuilder.class)).thenReturn(mockQueryBuilder);

        Query mockQuery = mock(Query.class);
        when(mockQueryBuilder.createQuery(any(), any())).thenReturn(mockQuery);

        SearchResult mockResult = mock(SearchResult.class);
        when(mockQuery.getResult()).thenReturn(mockResult);

        List<Hit> hits = new ArrayList<>();

        Hit mockHit = mock(Hit.class);
        String hotelPath = "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai";

        when(mockHit.getPath()).thenReturn(hotelPath);

        hits.add(mockHit);
        when(mockResult.getHits()).thenReturn(hits);

        ClassLoader classLoader = getClass().getClassLoader();
        File requestFile = new File(classLoader.getResource("taj-tower-hotel-details.json").getFile());

        context.load().json(new FileInputStream(requestFile), hotelPath);

        Resource resource = context.resourceResolver().getResource(hotelPath);

        when(mockResourceResolver.getResource(hotelPath)).thenReturn(resource);


        when(mockBookingRequestHandler.createRoomReservation(any())).thenReturn(bookingObjectResponse);

        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
        StringWriter stringWriter = new StringWriter();
        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
        classUnderTest.doPost(mockRequest, mockResponse);

        String responseString = stringWriter.getBuffer().toString();

        assertEquals(bookingResponseString, responseString);
    }

    @Test
    public void testPayOnline() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        IBookingRequestHandler mockBookingRequestHandler = mock(IBookingRequestHandler.class,
                "Mock IBookingRequestHandler created in testPayAtHotel");
        context.registerService(IBookingRequestHandler.class, mockBookingRequestHandler);

        IRedemptionTransactionsService mockRedemptionTransactionsService = mock(IRedemptionTransactionsService.class);
        context.registerService(IRedemptionTransactionsService.class, mockRedemptionTransactionsService);

        TajHotelsBrandsETCService mockTajHotelsBrandsETCService = mock(TajHotelsBrandsETCService.class);
        context.registerService(TajHotelsBrandsETCService.class, mockTajHotelsBrandsETCService);

        ISibelConfiguration mockSibelConfiguration = mock(ISibelConfiguration.class);
        context.registerService(ISibelConfiguration.class, mockSibelConfiguration);

        IRoomAvailabilityService mockRoomAvailabilityService = mock(IRoomAvailabilityService.class);
        context.registerService(IRoomAvailabilityService.class, mockRoomAvailabilityService);

        classUnderTest = context.registerInjectActivateService(new BookHotelConfirmServlet());

        String resourcePathString = "/bin/postPaymentServlet";
        String encRcsp = "444eefbf9445c039d92745195a6483c781e2fb685f6c9a58aeb452887ca07a2dc71cc957b7ee896b57f703ea08757be534ce5a40a48f3ed030b4f2944edb167441c9b223cc49a1035ef5b8e28b954bd93cdf7b32c3ca75c98644bb49245d4257f1dc8abb4eb4b2764b8babd4badce703e1f6bcbf4def8930260763481290d0932780136f70a96a86874392168d61a08d1db8eeb9febd572d7502fbcbf1e75b8e77d8f8afe5e739a1f4ae9c58450c76c8bca8b287809a0a6efc2a967fcd903b5630e6b77965d8572962415b8a7e5872be339292accd5c6faf5b3adcf2dee80729c154f0bf0d6e88c8d3610a60af1ccc8de75b14157d8c841dcb5a5fdff36c02b1326b5a71bbabf2a796f2b1d5d943cc738206ba8499dce7218604bcd98f3a03b3fafe6fc170c314c83ed7184fe399359cc57b6ff1ba39ec2e37ae417d7163bf953ca549c1501b288bef983b20dc7e6e65ada7c54475f70f1909007b2542ee79d7045e15ec8c5edcbd5c08968fbdaefb136b665cbf8d0938c6af4d977e100022f316f3596f883732550621f7b7c68d0f44f64aeb4e05582b7ed04f3a398fd2e378ed08877853aba13ede6ddbcf3bc9900ba880e6ad7b568e9590c3329a1ef617faba65cc09a520938846da92dd49c09db6035d48014250bb6753b89ef84a44098b64c04d405c84ff9d48d20677ff4d87fd6a4ef4881f8066cbd5f086c53a2ed12f861724316605982b7341926bdbdd3f6c26180918f108d15c57142bf80b9ee791172d39ae00ae287981c39fa434813062c0620dc4b15f1cfa77019ca5ac680922df6c17dcc802829b652046c1462e28e1810f0cbb304d4363553ed606415c84485b2f7b1845c4ffcbc54e599938dffdf10f93839c1f4aab8f5e911db99284c1f9f4478e28cda8b67fd172da1d1fc083bfae8373d75821b07a6cfc77186d0653325864e030345363e47a9b6c8e9379e985f9811a8633491795a7cc66fc73afe4d2e5940d61659d284093cdc61f92ff43770611fc6fb2dc0342dac049efac71f2405b8497b090478fdcc790d8fc3b4f498f5fad8e4ad7cfb09fb59acfc589363f87c1de7d2f6b77b54458a42d2ee8c0070b094c82efc565cca6ad80fd3219418cb43c6e66831ab3026f018442a3e10c44b5";

        String bookingResponseString = "{\"itineraryNumber\":\"21305B3216333\",\"guest\":{\"title\":\"Mr.\",\"lastName\":\"testing\",\"firstName\":\"test\",\"email\":\"vijay.kumar@moonraft.com\",\"country\":\"IN\",\"phoneNumber\":\"911234567890\",\"gstNumber\":null,\"membershipId\":null,\"specialRequests\":\"Please cancel this\",\"autoEnrollTic\":false,\"gdpr\":false,\"airportPickup\":false,\"cdmReferenceId\":null},\"loggedInUser\":null,\"flight\":null,\"payments\":null,\"hotel\":null,\"coupon\":null,\"promotionCode\":null,\"hotelId\":\"75768\",\"resStatus\":null,\"roomList\":[{\"reservationNumber\":\"75768SC004070\",\"reservationSubString\":null,\"roomTypeCode\":\"CKC\",\"roomTypeName\":\"Superior Room City View King Bed\",\"roomTypeDesc\":\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\",\"ratePlanCode\":\"R04\",\"ratePlanName\":\"Best Flexible Rate With WiFi\",\"rateDescription\":\"Includes accommodation and free WiFi for four devices with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\",\"hotelId\":\"75768\",\"noOfAdults\":1,\"noOfChilds\":0,\"bedType\":null,\"promoCode\":null,\"bookingStatus\":true,\"roomCostBeforeTax\":\"13000.00\",\"roomCostAfterTax\":\"15340.00\",\"cancellationPolicy\":\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\",\"petPolicy\":null,\"taxes\":[{\"taxAmount\":\"2340.0\",\"taxName\":\"GOODS AND SERVICE TAX 18PCT\",\"taxCode\":\"19\",\"taxDescription\":\"GOODS AND SERVICE TAX 18PCT\",\"effectiveDate\":\"2020-03-11\",\"expireDate\":\"2020-03-11\"}],\"nightlyRates\":[{\"date\":\"3/11/2020\",\"priceWithFeeAndTax\":\"15340.00\",\"price\":\"13000.00\",\"tax\":\"2340.00\",\"fee\":\"0.00\"}],\"discountedNightlyRates\":null,\"resStatus\":\"Committed\",\"cancellable\":true}],\"totalAmountAfterTax\":\"15340.0\",\"totalAmountBeforeTax\":\"13000.0\",\"chainCode\":\"21305\",\"warnings\":null,\"errorMessage\":null,\"checkInDate\":\"2020-03-11\",\"checkOutDate\":\"2020-03-12\",\"userId\":\"\",\"currencyCode\":\"INR\",\"pinNumber\":\"\",\"success\":true,\"partialBooking\":false,\"bookingDate\":null,\"paymentStatus\":false,\"corporateBooking\":false,\"cancelObject\":null,\"redemptionType\":null,\"noOfTicPoints\":null}";
        BookingObject bookingObjectResponse = objectMapper.readValue(bookingResponseString,
                new TypeReference<BookingObject>() {
                });

        String hoteldetailsString = "{\"hotelName\":\"Taj Mahal Tower\",\"hotelCode\":null,\"hotelEmail\":\"Tmhresv.bom@tajhotels.com\",\"hotelPhoneNumber\":\"+91 22-6665 3000\",\"hotelLatitude\":\"18.922437\",\"hotelLongitude\":\"72.833181\",\"giftHamperAdminMailId\":\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\",\"hotelResourcePath\":\"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai\",\"hotelBannerImagePath\":\"/content/dam/luxury/hotels/taj-mahal-tower-mumbai/images/new-images/taj_tower.jpg\",\"hotelLocationId\":\"HLTBOMTM\",\"pmspropertyCode\":\"BOMTM\",\"propertyCode\":\"00\"}";
        Hotel hotel = objectMapper.readValue(hoteldetailsString, new TypeReference<Hotel>() {
        });

        SlingHttpServletRequest mockRequest = mock(SlingHttpServletRequest.class);

        RequestPathInfo mockRequestPathInfo = mock(RequestPathInfo.class);

        when(mockRequest.getRequestPathInfo()).thenReturn(mockRequestPathInfo);
        when(mockRequestPathInfo.getResourcePath()).thenReturn(resourcePathString);
        when(mockRequest.getRequestURL()).thenReturn(new StringBuffer("http://localhost:4502/bin/postPaymentServlet"));
        when(mockRequest.getServerName()).thenReturn("localhost");

        TajHotelBrandsETCConfigurations tajHotelBrandsETCConfigurations = new TajHotelBrandsETCConfigurations(
                "bookingCCAvenue", "AVQF01FG85AS47FQSA",
                "http://localhost:4502/content/tajhotels/en-in/booking-cart.html",
                "http://localhost:4502/bin/postPaymentServlet",
                "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction",
                "E0AF518195DD811AE2526EFF2571F380",
                "http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html", "4", "/content/tajhotels");

        Map<String, TajHotelBrandsETCConfigurations> tajHotelBrandsETCConfigurationsBean = new HashMap<>();
        tajHotelBrandsETCConfigurationsBean.put("bookingCCAvenue", tajHotelBrandsETCConfigurations);

        TajhotelsBrandAllEtcConfigBean TajhotelsBrandAllEtcConfigBean = new TajhotelsBrandAllEtcConfigBean(
                tajHotelBrandsETCConfigurationsBean, false);
        // when(tajHotelsBrandsETCService.getEtcConfigBean()).thenReturn(tajHotelBrandsETCConfigurations);

        TajHotelsAllBrandsConfigurationsBean tajHotelsAllBrandsConfigurationsBean = new TajHotelsAllBrandsConfigurationsBean();
        Map<String, TajhotelsBrandAllEtcConfigBean> etcConfigBean = new HashMap<>();

        etcConfigBean.put("localhost", TajhotelsBrandAllEtcConfigBean);

        tajHotelsAllBrandsConfigurationsBean.setEtcConfigBean(etcConfigBean);
        when(mockTajHotelsBrandsETCService.getEtcConfigBean()).thenReturn(tajHotelsAllBrandsConfigurationsBean);

        ResourceResolver mockResourceResolver = mock(ResourceResolver.class);
        when(mockRequest.getResourceResolver()).thenReturn(mockResourceResolver);

        QueryBuilder mockQueryBuilder = mock(QueryBuilder.class);
        when(mockResourceResolver.adaptTo(QueryBuilder.class)).thenReturn(mockQueryBuilder);

        Query mockQuery = mock(Query.class);
        when(mockQueryBuilder.createQuery(any(), any())).thenReturn(mockQuery);

        SearchResult mockResult = mock(SearchResult.class);
        when(mockQuery.getResult()).thenReturn(mockResult);

        List<Hit> hits = new ArrayList<>();

        Hit mockHit = mock(Hit.class);
        String hotelPath = "/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai";

        when(mockHit.getPath()).thenReturn(hotelPath);

        hits.add(mockHit);
        when(mockResult.getHits()).thenReturn(hits);

        ClassLoader classLoader = getClass().getClassLoader();
        File requestFile = new File(classLoader.getResource("taj-tower-hotel-details.json").getFile());

        context.load().json(new FileInputStream(requestFile), hotelPath);

        Resource resource = context.resourceResolver().getResource(hotelPath);

        when(mockResourceResolver.getResource(hotelPath)).thenReturn(resource);

        when(mockBookingRequestHandler.createRoomReservation(any())).thenReturn(bookingObjectResponse);

        when(mockRequest.getParameter(BookingConstants.CCAVENUE_RESPONSE_PARAM_NAME)).thenReturn(encRcsp);
        SlingHttpServletResponse mockResponse = mock(SlingHttpServletResponse.class);
        StringWriter stringWriter = new StringWriter();
        when(mockResponse.getWriter()).thenReturn(new PrintWriter(stringWriter));
        classUnderTest.doPost(mockRequest, mockResponse);
        String responseString = stringWriter.getBuffer().toString();
        System.out.println("responseString: " + responseString);
        String responseForm = "<form id='confirmbooking' action='http://localhost:4502/content/tajhotels/en-in/booking-confirmation.html.html' method='get'><input type='hidden' name='status' value='allbooked'></form><script type=\"text/javascript\">window.onload = function(){sessionStorage.setItem('bookingDetailsRequest', '{\\\"itineraryNumber\\\":\\\"21305B3216333\\\",\\\"guest\\\":{\\\"title\\\":\\\"Mr.\\\",\\\"lastName\\\":\\\"testing\\\",\\\"firstName\\\":\\\"test\\\",\\\"email\\\":\\\"vijay.kumar@moonraft.com\\\",\\\"country\\\":\\\"IN\\\",\\\"phoneNumber\\\":\\\"911234567890\\\",\\\"gstNumber\\\":null,\\\"membershipId\\\":null,\\\"specialRequests\\\":\\\"Please cancel this\\\",\\\"autoEnrollTic\\\":false,\\\"gdpr\\\":false,\\\"airportPickup\\\":false,\\\"cdmReferenceId\\\":null},\\\"loggedInUser\\\":null,\\\"flight\\\":null,\\\"payments\\\":{\\\"payAtHotel\\\":false,\\\"creditCardRequired\\\":false,\\\"nameOnCard\\\":null,\\\"cardNumber\\\":null,\\\"expiryMonth\\\":null,\\\"currencyCode\\\":null,\\\"payOnlineNow\\\":true,\\\"payUsingGiftCard\\\":false,\\\"payUsingCerditCard\\\":false,\\\"ticpoints\\\":null,\\\"cardType\\\":null},\\\"hotel\\\":{\\\"hotelName\\\":\\\"Taj Mahal Tower\\\",\\\"hotelCode\\\":null,\\\"hotelEmail\\\":\\\"Tmhresv.bom@tajhotels.com\\\",\\\"hotelPhoneNumber\\\":\\\"+91 22-6665 3000\\\",\\\"hotelLatitude\\\":\\\"18.922437\\\",\\\"hotelLongitude\\\":\\\"72.833181\\\",\\\"giftHamperAdminMailId\\\":\\\"mala.vaidya@tajhotels.com, Tony.Sebastian@ihcltata.com\\\",\\\"hotelResourcePath\\\":\\\"\\/content\\/tajhotels\\/en-in\\/our-hotels\\/hotels-in-mumbai\\/taj\\/taj-mahal-tower-mumbai\\\",\\\"hotelBannerImagePath\\\":\\\"\\/content\\/dam\\/luxury\\/hotels\\/taj-mahal-tower-mumbai\\/images\\/new-images\\/taj_tower.jpg\\\",\\\"hotelLocationId\\\":null,\\\"pmspropertyCode\\\":\\\"BOMTM\\\",\\\"propertyCode\\\":\\\"00\\\"},\\\"coupon\\\":null,\\\"promotionCode\\\":null,\\\"hotelId\\\":\\\"75768\\\",\\\"resStatus\\\":null,\\\"roomList\\\":[{\\\"reservationNumber\\\":\\\"75768SC004070\\\",\\\"reservationSubString\\\":null,\\\"roomTypeCode\\\":\\\"CKC\\\",\\\"roomTypeName\\\":\\\"Superior Room City View King Bed\\\",\\\"roomTypeDesc\\\":\\\"This room has a distinct blend of Indian aesthetics and European flair. Equipped with digital connectivity, the room has a work desk and voicemail facilities. You can torch calories at the gym, dig into a relaxing session at the Spa. The bathroom is fully prepared for a relaxing experience.\\\",\\\"ratePlanCode\\\":\\\"R04\\\",\\\"ratePlanName\\\":\\\"Best Flexible Rate With WiFi\\\",\\\"rateDescription\\\":\\\"Includes accommodation and free WiFi for four devices with a flexible cancellation policy and an option to pay at the hotel. No minimum length of stay required. Taxes extra.\\\",\\\"hotelId\\\":\\\"75768\\\",\\\"noOfAdults\\\":1,\\\"noOfChilds\\\":0,\\\"bedType\\\":null,\\\"promoCode\\\":null,\\\"bookingStatus\\\":true,\\\"roomCostBeforeTax\\\":\\\"13000.00\\\",\\\"roomCostAfterTax\\\":\\\"15340.00\\\",\\\"cancellationPolicy\\\":\\\"Free cancellation by 2PM-2 days prior to arrival to avoid a penalty of 1 night charge plus any applicable taxes & fees.\\\",\\\"petPolicy\\\":null,\\\"taxes\\\":[{\\\"taxAmount\\\":\\\"2340.0\\\",\\\"taxName\\\":\\\"GOODS AND SERVICE TAX 18PCT\\\",\\\"taxCode\\\":\\\"19\\\",\\\"taxDescription\\\":\\\"GOODS AND SERVICE TAX 18PCT\\\",\\\"effectiveDate\\\":\\\"2020-03-11\\\",\\\"expireDate\\\":\\\"2020-03-11\\\"}],\\\"nightlyRates\\\":[{\\\"date\\\":\\\"3\\/11\\/2020\\\",\\\"priceWithFeeAndTax\\\":\\\"15340.00\\\",\\\"price\\\":\\\"13000.00\\\",\\\"tax\\\":\\\"2340.00\\\",\\\"fee\\\":\\\"0.00\\\"}],\\\"discountedNightlyRates\\\":null,\\\"resStatus\\\":\\\"Committed\\\",\\\"cancellable\\\":true}],\\\"totalAmountAfterTax\\\":\\\"15340.0\\\",\\\"totalAmountBeforeTax\\\":\\\"13000.0\\\",\\\"chainCode\\\":\\\"21305\\\",\\\"warnings\\\":null,\\\"errorMessage\\\":null,\\\"checkInDate\\\":\\\"2020-03-11\\\",\\\"checkOutDate\\\":\\\"2020-03-12\\\",\\\"userId\\\":\\\"\\\",\\\"currencyCode\\\":\\\"INR\\\",\\\"pinNumber\\\":\\\"\\\",\\\"success\\\":true,\\\"partialBooking\\\":false,\\\"bookingDate\\\":null,\\\"paymentStatus\\\":false,\\\"corporateBooking\\\":false,\\\"cancelObject\\\":null,\\\"redemptionType\\\":null,\\\"noOfTicPoints\\\":null}');sessionStorage.setItem('status', 'allbooked');sessionStorage.setItem('paymentType', 'payOnline');document.getElementById("
                + "\"confirmbooking\"" + ").submit()}</script>" + "\n";
        assertEquals(responseForm, responseString);
    }
}
