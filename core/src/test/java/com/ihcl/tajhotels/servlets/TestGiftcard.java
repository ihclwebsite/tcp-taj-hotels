/**
 *
 */
package com.ihcl.tajhotels.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import com.ccavenue.security.AesCryptUtil;

/**
 * @author admin
 *
 */
@SuppressWarnings("deprecation")
public class TestGiftcard {

    public static void main(String[] args) throws Exception {
        AesCryptUtil aesUtil = new AesCryptUtil("E0AF518195DD811AE2526EFF2571F380");
        int transactionId = (int) (new Date().getTime() / 1000);
        for (int i = 0; i < 50; i++) {
            String order_id = "GCO" + (transactionId + i + i);
            String tracking_id = String.valueOf(transactionId + i);
            String billingName = "test testing " + i;
            String data = "order_id=" + order_id + "&tracking_id=" + tracking_id
                    + "&bank_ref_no=1566208373053&order_status=Success&failure_message=&payment_mode=Net Banking"
                    + "&card_name=AvenuesTest&status_code=null&status_message=Y&currency=INR&amount=2294.00"
                    + "&billing_name=" + billingName
                    + "&billing_address=&billing_city=&billing_state=&billing_zip=&billing_country="
                    + "&billing_tel=917207592095&billing_email=vijay.kumar@moonraft.com&delivery_name="
                    + "&delivery_address=&delivery_city=&delivery_state=&delivery_zip=&delivery_country=&delivery_tel="
                    + "&merchant_param1=&merchant_param2=&merchant_param3=&merchant_param4="
                    + "&merchant_param5=GCPA-2294_GCA-2548"
                    + "&vault=N&offer_type=null&offer_code=null&discount_value=0.0&mer_amount=2294.00"
                    + "&eci_value=null&retry=N&response_code=0&billing_notes="
                    + "&trans_date=19/08/2019 15:23:24&bin_country=";
            System.out.println("data: " + data);
            String encryptData = aesUtil.encrypt(data);
            System.out.println("encryptData: " + encryptData);
            sendGet(encryptData);
        }
    }

    // HTTP GET request
    private static void sendGet(String encryptData) throws Exception {
        String username = "admin";
        String password = "admin";
        String url = "http://localhost:4502/bin/giftcardPostPaymentServlet";

        // Equivalent command conversion for Java execution
        String[] command = { "curl", "-u", username + ":" + password, "-X", "POST", "-F", "cmd=unlockPage", "-F",
                "encResp=" + encryptData, "-F", "_charset_=utf-8", url };

        ProcessBuilder process = new ProcessBuilder(command);
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            String result = builder.toString();
            System.out.print(result);

        } catch (IOException e) {
            System.out.print("error");
            e.printStackTrace();
        }
    }
}
