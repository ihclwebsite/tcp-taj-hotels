/**
 *
 */
package tajhotels.core;

import static org.mockito.Mockito.mock;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihcl.core.models.impl.FetchHotelsServiceModelImpl;


/**
 * @author Srikanta.moonraft
 *
 */

// @RunWith(MockitoJUnitRunner.class)
public class FetchHotelsServiceModelImplTest {

    private static final Logger LOG = LoggerFactory.getLogger(FetchHotelsServiceModelImplTest.class);

    private SlingHttpServletRequest request;

    Resource testResource;

    @Before
    public void prepare() {
        request = mock(SlingHttpServletRequest.class);
        testResource = mock(Resource.class);

    }

    // @Test
    public void getParentUptoTest() {

        final FetchHotelsServiceModelImpl fetchHotelsServiceModelImpl = new FetchHotelsServiceModelImpl();
        // Resource cuurentResource = request.getResource();
        Mockito.when(request.getResource()).thenReturn(testResource);
        // Mockito.when(cuurentResource.getParent()).thenReturn(testResource);
        final String parentResourceName = "taj";
        // final Resource someResource = fetchHotelsServiceModelImpl.getParentUpto(testResource, parentResourceName);
        // assertEquals("taj", someResource.getName());
    }

}

