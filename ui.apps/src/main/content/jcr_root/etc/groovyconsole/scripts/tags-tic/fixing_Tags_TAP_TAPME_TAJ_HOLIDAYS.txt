import javax.jcr.Node
    
def tapArray = ["/content/tajhotels/en-in/our-hotels/hotels-in-pench-national-park/taj/baghvan-pench-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-katra/vivanta/country-resort-katra",
"/content/tajhotels/en-in/our-hotels/hotels-in-jim-corbett/taj/taj-corbett-uttarakhand",
"/content/tajhotels/en-in/our-hotels/hotels-in-coorg/taj/taj-madikeri-coorg",
"/content/tajhotels/en-in/our-hotels/hotels-in-coimbatore/vivanta/surya-coimbatore",
"/content/tajhotels/en-in/our-hotels/hotels-in-lucknow/taj/taj-mahal-lucknow",
"/content/tajhotels/en-in/our-hotels/hotels-in-srinagar/vivanta/dal-view-srinagar",
"/content/tajhotels/en-in/our-hotels/hotels-in-vadodara/gateway/akota-gardens-vadodara",
"/content/tajhotels/en-in/our-hotels/hotels-in-surat/gateway/athwalines-surat",
"/content/tajhotels/en-in/our-hotels/hotels-in-coonoor/gateway/church-road-coonoor",
"/content/tajhotels/en-in/our-hotels/hotels-in-amritsar/taj/taj-amritsar",
"/content/tajhotels/en-in/our-hotels/hotels-in-hubli/gateway/lakeside-hubli",
"/content/tajhotels/en-in/our-hotels/hotels-in-chitwan-national-park/taj/meghauli-serai-chitwan-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-gurgaon/gateway/damdama-lake-gurgaon",
"/content/tajhotels/en-in/our-hotels/hotels-in-gurgaon/taj/taj-city-centre-gurugram",
"/content/tajhotels/en-in/our-hotels/hotels-in-raipur/gateway/ge-road-raipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-andaman/taj/taj-exotica-andamans",
"/content/tajhotels/en-in/our-hotels/hotels-in-ernakulam/gateway/marine-drive-ernakulam",
"/content/tajhotels/en-in/our-hotels/hotels-in-bekal/taj/taj-bekal-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-gondia/gateway/balaghat-road-gondia",
"/content/tajhotels/en-in/our-hotels/hotels-in-kovalam/taj/taj-green-cove",
"/content/tajhotels/en-in/our-hotels/hotels-in-guwahati/vivanta/guwahati-assam",
"/content/tajhotels/en-in/our-hotels/hotels-in-varanasi/taj/taj-ganges",
"/content/tajhotels/en-in/our-hotels/hotels-in-varanasi/taj/taj-nadesar-palace-varanasi",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-fort-aguada-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-exotica-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-holiday-village-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/vivanta/panaji-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/seleqtions/cidade-de-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-kochi/taj/taj-malabar-cochin",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/gateway/kuteeram",
"/content/tajhotels/en-in/our-hotels/hotels-in-mangalore/gateway/old-port-road-mangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-kozhikode/gateway/beach-road-calicut",
"/content/tajhotels/en-in/our-hotels/hotels-in-kathmandu/vivanta/vivanta-kathmandu",
"/content/tajhotels/en-in/our-hotels/hotels-in-bandhavgarh-national-park/taj/mahua-kothi-bandhavgarh-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-jodhpur/taj/umaid-bhawan-palace-jodhpur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jodhpur/taj/taj-hari-mahal-jodhpur",
"/content/tajhotels/en-in/our-hotels/hotels-in-ranthambore/vivanta/sawai-madhopur-ranthambore",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/gateway/it-expressway-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-connemara-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-agra/seleqtions/taj-view-agra",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-west-end-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-mgroad-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-yeshwantpur-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/gateway/residency-road-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/vivanta/whitefield-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-ajmer/gateway/the-gateway-hotel-ajmer",
"/content/tajhotels/en-in/our-hotels/hotels-in-kanha-national-park/taj/banjaar-tola-kanha-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-kumarakom/taj/taj-kumarakom-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/seleqtions/president-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-thimphu/taj/taj-tashi",
"/content/tajhotels/en-in/our-hotels/hotels-in-aurangabad/vivanta/aurangabad-maharashtra",
"/content/tajhotels/en-in/our-hotels/hotels-in-visakhapatnam/gateway/beach-road-visakhapatnam",
"/content/tajhotels/en-in/our-hotels/hotels-in-varkala/gateway/janardhanapuram-varkala",
"/content/tajhotels/en-in/our-hotels/hotels-in-rishikesh/taj/taj-rishikesh",
"/content/tajhotels/en-in/our-hotels/hotels-in-nashik/taj/ambad-nashik",
"/content/tajhotels/en-in/our-hotels/hotels-in-khajuraho/gateway/hotel-chandela",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-santacruz-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-palace-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-lands-end-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-krishna-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-falaknuma-palace-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/deccan",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-banjara",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/vivanta/begumpet-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-ahmedabad/gateway/ummed-ahmedabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/gateway/sms",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/gateway/ramgarh-lodge-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/taj/rambagh-palace-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/taj/jai-mahal-palace-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-kolkata/gateway/em-bypass-kolkata",
"/content/tajhotels/en-in/our-hotels/hotels-in-kolkata/taj/taj-bengal-kolkata",
"/content/tajhotels/en-in/our-hotels/hotels-in-pune/gateway/hinjawadi-pune",
"/content/tajhotels/en-in/our-hotels/hotels-in-pune/seleqtions/blue-diamond-pune",
"/content/tajhotels/en-in/our-hotels/hotels-in-ooty/seleqtions/savoy",
"/content/tajhotels/en-in/our-hotels/hotels-in-gir/gateway/gir-forest",
"/content/tajhotels/en-in/our-hotels/hotels-in-udaipur/taj/taj-aravali-resort-and-spa-udaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-udaipur/taj/taj-lake-palace-udaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-trivandrum/vivanta/trivandrum-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-gwalior/taj/taj-usha-kiran-palace-gwalior",
"/content/tajhotels/en-in/our-hotels/hotels-in-vijaywada/gateway/mg-road-vijayawada",
"/content/tajhotels/en-in/our-hotels/hotels-in-chikmagalur/gateway/km-road-chikmagalur",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/taj/taj-palace-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/taj/the-taj-mahal-hotel-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/vivanta/surajkund-ncr",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/vivanta/dwarka-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/seleqtions/connaught-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/seleqtions/ambassador-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/club-house-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-fishermans-cove-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-coromandel-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-madurai/gateway/pasumalai-madurai",
"/content/tajhotels/en-in/our-hotels/hotels-in-panna-national-park/taj/pashan-garh-panna-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-shimla/taj/taj-theog",
"/content/tajhotels/en-in/our-hotels/hotels-in-chandigarh/taj/chandigarh-punjab",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaisalmer/gateway/rawalkot-jaisalmer"];

    
def tajHolidayArray = ["/content/tajhotels/en-in/our-hotels/hotels-in-coorg/taj/taj-madikeri-coorg",
"/content/tajhotels/en-in/our-hotels/hotels-in-srinagar/vivanta/dal-view-srinagar",
"/content/tajhotels/en-in/our-hotels/hotels-in-bekal/taj/taj-bekal-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-fort-aguada-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-exotica-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-holiday-village-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/vivanta/panaji-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-kochi/taj/taj-malabar-cochin",
"/content/tajhotels/en-in/our-hotels/hotels-in-jodhpur/taj/umaid-bhawan-palace-jodhpur",
"/content/tajhotels/en-in/our-hotels/hotels-in-kumarakom/taj/taj-kumarakom-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-falaknuma-palace-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/taj/jai-mahal-palace-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-udaipur/taj/taj-lake-palace-udaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-fishermans-cove-chennai"];



def tapMEArray = ["/content/tajhotels/en-in/our-hotels/hotels-in-pench-national-park/taj/baghvan-pench-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-katra/vivanta/country-resort-katra",
"/content/tajhotels/en-in/our-hotels/hotels-in-jim-corbett/taj/taj-corbett-uttarakhand",
"/content/tajhotels/en-in/our-hotels/hotels-in-coorg/taj/taj-madikeri-coorg",
"/content/tajhotels/en-in/our-hotels/hotels-in-coimbatore/vivanta/surya-coimbatore",
"/content/tajhotels/en-in/our-hotels/hotels-in-lucknow/taj/taj-mahal-lucknow",
"/content/tajhotels/en-in/our-hotels/hotels-in-srinagar/vivanta/dal-view-srinagar",
"/content/tajhotels/en-in/our-hotels/hotels-in-vadodara/gateway/akota-gardens-vadodara",
"/content/tajhotels/en-in/our-hotels/hotels-in-surat/gateway/athwalines-surat",
"/content/tajhotels/en-in/our-hotels/hotels-in-coonoor/gateway/church-road-coonoor",
"/content/tajhotels/en-in/our-hotels/hotels-in-amritsar/taj/taj-amritsar",
"/content/tajhotels/en-in/our-hotels/hotels-in-hubli/gateway/lakeside-hubli",
"/content/tajhotels/en-in/our-hotels/hotels-in-chitwan-national-park/taj/meghauli-serai-chitwan-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-gurgaon/gateway/damdama-lake-gurgaon",
"/content/tajhotels/en-in/our-hotels/hotels-in-gurgaon/taj/taj-city-centre-gurugram",
"/content/tajhotels/en-in/our-hotels/hotels-in-raipur/gateway/ge-road-raipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-andaman/taj/taj-exotica-andamans",
"/content/tajhotels/en-in/our-hotels/hotels-in-ernakulam/gateway/marine-drive-ernakulam",
"/content/tajhotels/en-in/our-hotels/hotels-in-bekal/taj/taj-bekal-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-gondia/gateway/balaghat-road-gondia",
"/content/tajhotels/en-in/our-hotels/hotels-in-kovalam/taj/taj-green-cove",
"/content/tajhotels/en-in/our-hotels/hotels-in-guwahati/vivanta/guwahati-assam",
"/content/tajhotels/en-in/our-hotels/hotels-in-varanasi/taj/taj-ganges",
"/content/tajhotels/en-in/our-hotels/hotels-in-varanasi/taj/taj-nadesar-palace-varanasi",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-fort-aguada-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-exotica-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/taj/taj-holiday-village-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/vivanta/panaji-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-goa/seleqtions/cidade-de-goa",
"/content/tajhotels/en-in/our-hotels/hotels-in-kochi/taj/taj-malabar-cochin",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/gateway/kuteeram",
"/content/tajhotels/en-in/our-hotels/hotels-in-mangalore/gateway/old-port-road-mangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-kozhikode/gateway/beach-road-calicut",
"/content/tajhotels/en-in/our-hotels/hotels-in-kathmandu/vivanta/vivanta-kathmandu",
"/content/tajhotels/en-in/our-hotels/hotels-in-bandhavgarh-national-park/taj/mahua-kothi-bandhavgarh-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-jodhpur/taj/umaid-bhawan-palace-jodhpur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jodhpur/taj/taj-hari-mahal-jodhpur",
"/content/tajhotels/en-in/our-hotels/hotels-in-ranthambore/vivanta/sawai-madhopur-ranthambore",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/gateway/it-expressway-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-connemara-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-agra/seleqtions/taj-view-agra",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-west-end-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-mgroad-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/taj/taj-yeshwantpur-bengaluru",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/gateway/residency-road-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-bangalore/vivanta/whitefield-bangalore",
"/content/tajhotels/en-in/our-hotels/hotels-in-ajmer/gateway/the-gateway-hotel-ajmer",
"/content/tajhotels/en-in/our-hotels/hotels-in-kanha-national-park/taj/banjaar-tola-kanha-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-kumarakom/taj/taj-kumarakom-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-tower-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/seleqtions/president-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-thimphu/taj/taj-tashi",
"/content/tajhotels/en-in/our-hotels/hotels-in-aurangabad/vivanta/aurangabad-maharashtra",
"/content/tajhotels/en-in/our-hotels/hotels-in-visakhapatnam/gateway/beach-road-visakhapatnam",
"/content/tajhotels/en-in/our-hotels/hotels-in-varkala/gateway/janardhanapuram-varkala",
"/content/tajhotels/en-in/our-hotels/hotels-in-rishikesh/taj/taj-rishikesh",
"/content/tajhotels/en-in/our-hotels/hotels-in-nashik/taj/ambad-nashik",
"/content/tajhotels/en-in/our-hotels/hotels-in-khajuraho/gateway/hotel-chandela",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-santacruz-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-mahal-palace-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-mumbai/taj/taj-lands-end-mumbai",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-krishna-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-falaknuma-palace-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/deccan",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/taj-banjara",
"/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/vivanta/begumpet-hyderabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-ahmedabad/gateway/ummed-ahmedabad",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/gateway/sms",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/gateway/ramgarh-lodge-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/taj/rambagh-palace-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/taj/jai-mahal-palace-jaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-kolkata/gateway/em-bypass-kolkata",
"/content/tajhotels/en-in/our-hotels/hotels-in-kolkata/taj/taj-bengal-kolkata",
"/content/tajhotels/en-in/our-hotels/hotels-in-pune/gateway/hinjawadi-pune",
"/content/tajhotels/en-in/our-hotels/hotels-in-pune/seleqtions/blue-diamond-pune",
"/content/tajhotels/en-in/our-hotels/hotels-in-ooty/seleqtions/savoy",
"/content/tajhotels/en-in/our-hotels/hotels-in-gir/gateway/gir-forest",
"/content/tajhotels/en-in/our-hotels/hotels-in-udaipur/taj/taj-aravali-resort-and-spa-udaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-udaipur/taj/taj-lake-palace-udaipur",
"/content/tajhotels/en-in/our-hotels/hotels-in-trivandrum/vivanta/trivandrum-kerala",
"/content/tajhotels/en-in/our-hotels/hotels-in-gwalior/taj/taj-usha-kiran-palace-gwalior",
"/content/tajhotels/en-in/our-hotels/hotels-in-vijaywada/gateway/mg-road-vijayawada",
"/content/tajhotels/en-in/our-hotels/hotels-in-chikmagalur/gateway/km-road-chikmagalur",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/taj/taj-palace-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/taj/the-taj-mahal-hotel-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/vivanta/surajkund-ncr",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/vivanta/dwarka-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/seleqtions/connaught-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-new-delhi/seleqtions/ambassador-new-delhi",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/club-house-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-fishermans-cove-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-coromandel-chennai",
"/content/tajhotels/en-in/our-hotels/hotels-in-madurai/gateway/pasumalai-madurai",
"/content/tajhotels/en-in/our-hotels/hotels-in-panna-national-park/taj/pashan-garh-panna-national-park",
"/content/tajhotels/en-in/our-hotels/hotels-in-shimla/taj/taj-theog",
"/content/tajhotels/en-in/our-hotels/hotels-in-chandigarh/taj/chandigarh-punjab",
"/content/tajhotels/en-in/our-hotels/hotels-in-jaisalmer/gateway/rawalkot-jaisalmer"
]

def fixTags(arr,tag){
    for(int i = 0;i<arr.size();i++) {
         Resource hotelResource = resourceResolver.getResource(arr[i]+"/jcr:content")
         if(hotelResource == null){
             println("NULL FOUND"+arr[i]+"/jcr:content");
             continue;
         }
         Node hotelNode = hotelResource.adaptTo(Node)
         ModifiableValueMap hotelValueMap = hotelResource.adaptTo(ModifiableValueMap.class);
         if(hotelValueMap.containsKey("cq:tags")){
             def tags = hotelValueMap.get("cq:tags", String[].class)
             
             println("OLD"+ tags)
             def newTag = [tag]
             newTag = [tags, newTag].flatten() as String[]
             //newTag.add("taj:tic-room-redemption/taj-holidays-redemption");
             println("NEW"+ newTag)
             hotelNode.setProperty("cq:tags",newTag);
             resourceResolver.commit();
             
         }
    }
}


def removeParticularTag(arr,tag){
    for(int i = 0;i<arr.size();i++) {
         Resource hotelResource = resourceResolver.getResource(arr[i]+"/jcr:content")
         if(hotelResource == null){
             println("NULL FOUND"+arr[i]+"/jcr:content");
             continue;
         }
         Node hotelNode = hotelResource.adaptTo(Node)
         ModifiableValueMap hotelValueMap = hotelResource.adaptTo(ModifiableValueMap.class);
         if(hotelValueMap.containsKey("cq:tags")){
             def tags = hotelValueMap.get("cq:tags", String[].class)
             def tagsList = tags.toList()
             
             tagsList.removeAll(tag);
             //tags as ArrayList
             println("OLD"+ tagsList)
             def newTag = [tag]
             newTag = [tagsList].flatten() as String[]
             
             println("NEW"+ newTag)
             hotelNode.setProperty("cq:tags",newTag);
             resourceResolver.commit();
             
         }
    }
}
//fixTags(tajHolidayArray,"taj:tic-room-redemption/taj-holidays-redemption"); // taj holidays

//fixTags(tapArray,"taj:tap"); // taj tap

fixTags(tapMEArray,"taj:tappme"); // taj tapME

//removeParticularTag(tapArray,"taj:tic-room-redemption/taj-holidays-redemption");



