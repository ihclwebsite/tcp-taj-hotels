import com.day.cq.dam.api.Asset
import com.day.cq.replication.Replicator
import com.day.cq.replication.ReplicationActionType
import com.day.cq.replication.ReplicationStatus
import javax.jcr.Session
import java.util.GregorianCalendar


executeDamContentRisk()

def executeDamContentRisk(){
    def predicates = [
        "path":"/content/dam/tajhotels/others/canonical-seo",
        "type":"nt:Asset",
        "1_property":"jcr:primaryType",
        "1_property.value":"dam:Asset"
    ]
    
    def query = createQuery(predicates)
    def result = query.result
    // "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    result.hits.each { hit ->
        return getDamAssetFromNode(hit.node)
    }
}


//Execute after getting DAM file node 
def getDamAssetFromNode(node){
    //println "node path in getDamAssetFromNode "+node.path
    def assetNodePath = node.path
    Resource res = resourceResolver.getResource(node.path)
    Asset asset= res.adaptTo(Asset.class)
    r=asset.getOriginal()
    text = r.getStream().text
    //println ""+text
    def lines = text.readLines()
    lineIndex= 0
    
    for (String line in lines) {
        lineIndex++
        //println ""+lineIndex + " -> "+line
        executeOneLineForDestination(line)
    }
}


def executeOneLineForDestination(line){
    //println 'running executeOneLineForDestination'
   lineArr = line.split("%")

    def ticPath = lineArr[0]
   def canTag = lineArr[1].split('"')[3]
    
   def contentPath = lineArr[0]
   
   String contentPathNew = contentPath.replace('https://www.tajinnercircle.com','/content/tajhotels')
   contentPathDetails = contentPathNew.split("/")

    if(ticPath.contains('hotels-in-')) {
      finalContentPath = contentPathNew.replace('destination','our-hotels')
   }
   
   if(ticPath.contains('about-')) {
      hotelsInDestinationRest =  contentPathDetails[5].replace('about','hotels-in')
       finalContentPath = contentPathNew.replace('destination','our-hotels/'+hotelsInDestinationRest)
   }
   
   if(ticPath.contains('experiences-in-')) {
      hotelsInDestinationRest =  contentPathDetails[5].replace('experiences-in','hotels-in')
       finalContentPath = contentPathNew.replace('destination','our-hotels/'+hotelsInDestinationRest)
   }
   
   if(ticPath.contains('restaurants-in-')) {
      hotelsInDestinationRest =  contentPathDetails[5].replace('restaurants-in','hotels-in')
       finalContentPath = contentPathNew.replace('destination','our-hotels/'+hotelsInDestinationRest)
   }
   
   
   
   
  // println 'ticPath : ' +  ticPath
   //println 'finalContentPath : ' + finalContentPath +'jcr:content'
   //println 'canTag : ' + canTag
   
   try {
       def nodeAtPath = getNode(finalContentPath +'jcr:content')
       nodeAtPath.setProperty("jcr:canonical",canTag)
       save()
   } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + finalContentPath)
   }
   
   //println 'content updated successfully at path ' + finalContentPath
}
