package etc.groovyconsole.scripts.tajhotels

executeContentRisk()


/*
    Please provide "2_property.value" as "Deactivate" to run the same logic for the deactivated page.
    To activate simulteneously the changes pages line No- 59 and Line No- 99 ['activate()']
*/

//Node content access
def executeContentRisk(){

    def predicates = [
            "path":"/content/tajhotels/en-in",
            "type":"nt:Asset",
            "nodename":"*signature-experiences*",
            "1_property":"jcr:primaryType",
            "1_property.value":"cq:Page",
            "2_property":"jcr:content/cq:lastReplicationAction",
            "2_property.value":"Activate"
    ]

    def query = createQuery(predicates)
    query.hitsPerPage = 200
    def result = query.result

    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    executeResult1(result)
}

def executeResult1(result){
    // def singleCheckPath="/content/tajhotels/en-in/our-hotels/hotels-in-jaipur/signature-experiences-in-jaipur"

    result.hits.each { hit ->
        if(hit.node.path.contains("/signature-experiences")){
            println "hit path = ${hit.node.path}"
            def actualPath= hit.node.path
            //def modifiedPath=actualPath.replace("/signature-experiences", "/experiences")
            def pathArr=actualPath.split("/")
            def modifiedPath=pathArr[pathArr.length-1].replace("signature-experiences", "experiences")
            println "Changed : "+ modifiedPath
            // if(actualPath==singleCheckPath){
            println "Running for the path : "+actualPath
            renameNode(hit.node, modifiedPath)
            //  }
        }
    }
}

def renameNode(Node node, String newName) throws RepositoryException{
    def tempPath=node.getPath()
    node.getSession().move(node.getPath(), node.getParent().getPath() + "/" + newName)
    // Don't forget - not necessarily here at this place:
    //Uncomment the below line to affect the structural
    node.getSession().save()
    println "Updated as :"+node.path    //The path already changed
    executeContentRisk2(tempPath)
    activate(node.getParent().getPath())

}

//Node content access
def executeContentRisk2(pathToModify){

    def predicates = [
            "path":"/content/tajhotels/en-in/our-hotels",
            "type":"nt:Asset",
            "nodename":"item*",
            "1_property":"jcr:primaryType",
            "1_property.value":"nt:unstructured",
            "2_property":"tabtext",
            "2_property.value":"Experiences",
            "3_property":"tabtextlink",
            "3_property.value":pathToModify
    ]

    def query = createQuery(predicates)
    query.hitsPerPage = 200
    def result = query.result

    println "${result.totalMatches} hits in Content2, execution time = ${result.executionTime}s\n--"

    executeResult2(result)

}

def executeResult2(result){
    result.hits.each { hit ->
        println "fetched Path :"+hit.node.path
        def fetchedLink=hit.node.get("tabtextlink")
        def parentLink=hit.node.getParent().getPath()
        println "Parent Path : "+parentLink
        def modifiedLink = fetchedLink.replace("/signature-experiences", "/experiences")
        println "modifiedLink : "+modifiedLink
        hit.node.set("tabtextlink", modifiedLink)
        //Uncomment the below line to affect the structural
        save()
        activate(modifiedLink)

        println "Updating . . ."
        if(hit.node.get("tabtextlink")==modifiedLink){
            println "Update Success."
        }else{
            println "Update Failed."
        }
    }
}





