package etc.groovyconsole.scripts.tajhotels

import com.day.cq.dam.api.Asset

// To update the properties uncomment the line 72 "updateSeoProperty()"
// Please Upload the CSV file in following location before running the script
filename = "/content/dam/tajhotels/others/CSV_RestaurantsInDestination_SEO_Updated-v2.csv"
text = executeDamContentRisk()

//Execute after getting DAM file node
def getDamAssetFromNode(node){
    Resource res = resourceResolver.getResource(node.path)
    Asset asset= res.adaptTo(Asset.class)
    r=asset.getOriginal()
    text = r.getStream().text
    def lines = text.readLines()
    lineIndex= 1
    for (line in lines) {
        //println "DAM2 : "+line
        executeOneLine(line)
        lineIndex++
    }
    return text
}

//for each line of the text until \n is reached
def executeOneLine(line){
    // println "line  : "+line
    //lineindex is 3 for start at restaurants-in-dest [total lineindex 71]
    if(checkLineIsEmpty(line) && lineIndex>2 && lineIndex <71){
        count=1
        lineArr=line.split(",")
        firstUrl= lineArr[1]
        line2= line
        // println "line index : "+lineIndex
        //println "line 2 : "+line2
        line2=line2.replace(","+firstUrl+",", "\"")
        if(line2.contains(",\"")){
            line2=line2.replace(",\"", "\",\"")
        }
        if(line2.contains("\",")){
            line2=line2.replace("\",", "\",\"")
        }
        if(line2.charAt(0)=="\"" && line2.charAt(1)=="\""){
            line2=line2.substring(1)
        }
        //println "line 02 : "+line2
        lineArr2=line2.split("\",\"")
        //println "lARR2"+lineArr2
        firstUrl=modifyLoc(firstUrl)

        // println "URL : "+firstUrl
        def secondPageTitle,thirdPageTitle,forthPageTitle
        if(lineArr2.length>=2){
            secondPageTitle=lineArr2[0].replace("\"", "").replace("Ã¢â¬â¢","'")
            thirdPageTitle=lineArr2[1].replace("\"", "").replace("Ã¢â¬â¢","'")
            forthPageTitle=lineArr2[2]
            for(i1=3; i1<lineArr2.length; i1++){
                // forthPageTitle=lineArr2[2].replace("\"", "").replace("Ã¢â¬â¢","'")
                forthPageTitle=forthPageTitle+","+lineArr2[i1]
            }
            forthPageTitle=forthPageTitle.replace("\"", "").replace("Ã¢â¬â¢","'")
        }
        // println "Page Title : "+secondPageTitle
        // println "Meta Desc: "+thirdPageTitle
        // println "Meta Key: "+forthPageTitle

        updateSeoProperty(firstUrl, secondPageTitle, thirdPageTitle, forthPageTitle)
    }
}

def updateSeoProperty(locPath, pageTitle, metaDesc, metaKeywords){

    restaurantsInDestTemplate="/conf/tajhotels/settings/wcm/templates/dining-list-destination"
    restaurantsInDestPageType="tajhotels/components/structure/dining-list-destination-page"

    def tempArr = [restaurantsInDestTemplate]
    def pageTypeArr=[restaurantsInDestPageType]

    //Please change the arrIndexC according to the above array min=0 for Home page max=7 fir aboutDestTemplate
    arrIndexC=0
    templateNameValue= tempArr[arrIndexC]
    pageSuperTypeValue= pageTypeArr[arrIndexC]

    def predicates = [
            "path" : locPath,
            "jcr:primaryType" : "cq:PageContent",
            "1_property": "cq:template",
            "1_property.value" : templateNameValue,
            "2_property" : "sling:resourceType",
            "2_property.value" : pageSuperTypeValue
    ]

    def query = createQuery(predicates)
    //query.hitsPerPage = 10
    def result = query.result

    println "${result.totalMatches} hits, execution time = ${result.executionTime}s"

    result.hits.each { hit ->
        println "hit path = ${hit.node.path}"
        // println "hit pageTitle = ${hit.node.pageTitle} :: "+pageTitle
        // println "hit seoDescription = ${hit.node.seoDescription} :: "+metaDesc
        // println "hit seoKeywords = ${hit.node.seoKeywords} :: "+metaKeywords
        // println "line index 2 : "+lineIndex
        if(((hit.node.get("cq:template")).equals(templateNameValue)) && (lineIndex>2 && lineIndex <71)){
            hit.node.set('pageTitle',pageTitle)
            hit.node.set('seoDescription',metaDesc)
            hit.node.set('seoKeywords',metaKeywords)
            println "Updating ... : "+hit.node.path

            println "Updated pageTitle : "+hit.node.get('pageTitle')
            println "Updated seoDescription : "+hit.node.get('seoDescription')
            println "Updated seoKeywords : "+hit.node.get('seoKeywords')
            def activateStatus=hit.node.get('cq:lastReplicationAction')
            println "is Activate : "+hit.node.get('cq:lastReplicationAction')

            if(hit.node.pageTitle == pageTitle && hit.node.seoDescription==metaDesc && hit.node.seoKeywords==metaKeywords){
                println "Successfuly completed : "+hit.node.path
            }else{
                println "Not able to update "
            }
            save()
            if(activateStatus.trim()=="Activate"){
                println "activating Path : "+hit.node.getParent().getPath()
                activate(hit.node.getParent().getPath())
            }
        }
    }
    println "--------------------------------"
}

def modifyLoc(firstUrl){
    if(firstUrl.contains("/en-in/")){
        firstUrl=firstUrl.replace("https://www.tajhotels.com", "/content/tajhotels")
        if(firstUrl=="/content/tajhotels/en-in/events/"){
            firstUrl=firstUrl.replace("/content/tajhotels/en-in/events", "/content/tajhotels/en-in/meetings")
        }else if(firstUrl.contains("/content/tajhotels/en-in/restaurants-in-") || firstUrl.contains("/content/tajhotels/en-in/our-hotels/hotels-in-")){
            destName= firstUrl.split("/")
            destName2=destName[destName.length-1]
            if(firstUrl.contains("/content/tajhotels/en-in/restaurants-in-")){
                destName2=destName2.replace("restaurants-in-", "")
                firstUrl=firstUrl.replace("/content/tajhotels/en-in/restaurants-in-","/content/tajhotels/en-in/our-hotels/hotels-in-"+destName2+"/restaurants-in-")
            }
        }

    }else{
        firstUrl=firstUrl.replace("https://www.tajhotels.com", "/content/tajhotels/en-in")
    }
    return firstUrl
}

//If line is empty returns false else true
boolean checkLineIsEmpty(textValue){
    if(textValue=="")
        return false
    if(textValue==null)
        return false
    else
        return true
}

//DAM content access
def executeDamContentRisk(){

    def predicates = [
            "path":"/content/dam/tajhotels/others",
            "type":"nt:Asset",
            "1_property":"jcr:primaryType",
            "1_property.value":"dam:Asset"
    ]

    def query = createQuery(predicates)
    //query.hitsPerPage = 10
    def result = query.result
    //println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"

    result.hits.each { hit ->
        println "hit path 2 = ${hit.node.path}"
        if(hit.node.path==filename)
            return getDamAssetFromNode(hit.node)
    }
}


