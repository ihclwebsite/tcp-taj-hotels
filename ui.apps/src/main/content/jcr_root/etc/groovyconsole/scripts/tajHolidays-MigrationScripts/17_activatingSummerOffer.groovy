import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
j=0
init()

def init(){
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"searchKey",
       "property.value":"holidayPackages",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(hit.path.contains("summer")){
            String summerPath =hit.path.replaceAll("/jcr:content","")
            println summerPath
            activate(summerPath)
        }
   }
}