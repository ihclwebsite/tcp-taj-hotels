 def buildQuery(page, term) {
       def queryManager = session.workspace.queryManager
      def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
       queryManager.createQuery(statement, 'sql')
 }

  
  page = getPage('/content/tajhotels/en-in/our-hotels/')
  destAboutDiningArr = ["tajhotels/components/structure/about-destination-page", "tajhotels/components/structure/dining-list-destination-page"]
  
  for(value in destAboutDiningArr) {
      
      final def query = buildQuery(page, value)
      final def result = query.execute()
    
      result.nodes.each { node ->
          //println 'nodePath -' + node.path
          node.setProperty("navTitle", node.get('jcr:title'))
          session.save()
      }
  }