/*groov to remove the inheritence of destination page - list/map view part*/


import groovy.json.JsonSlurper
import javax.jcr.Node
import javax.jcr.Workspace;
import org.apache.jackrabbit.commons.JcrUtils;

changeContentRefs()

def changeContentRefs(){
    
    removeInheritence("/content/ihclcb/en-in/our-hotels-vivanta");
    removeInheritence("/content/ihclcb/en-in/our-hotel-ihclcb-selections");
    removeInheritence("/content/ihclcb/en-in/ihclcbtajourhotels");
}

def removeInheritence(brandrootPath){
    
    
    def predicate = [
        "path": brandrootPath,
        "property":"jcr:content/sling:resourceType",
        "property.value":"tajhotels/components/structure/destination-landing-page",
        "p.limit":"-1"
        ]
        
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        println hit.path
        def destinationNode = hit.getResource().getChild("jcr:content/root/hotels_in_destinatio")
        if(null != destinationNode){
        Node node = destinationNode.adaptTo(Node.class);
        node.setProperty("cq:isCancelledForChildren",true);
        node.setProperty("isListMapViewLabelHidden",true);
        save();
    }
        
    }
}
