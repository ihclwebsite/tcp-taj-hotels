
    getPage("/content/tajhotels/en-in/our-hotels").recurse { page ->
       def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/hotels-landing-page")
        {
        
           // println "JCR Title:::"+content.get("jcr:title")+" Nav Title:::"+content.get("navTitle")
           if(content.get("navTitle")=="Overview")
           {
            content.set("navTitle",content.get("jcr:title"))
            // println content.get("jcr:title")    
           }
           save()
           activate(content.path)
        }
    }

    def t_captalize(jcrTitle)
    {
        jcrTitle.split(" ").collect{it.toLowerCase().capitalize()}.join(" ")
    }


