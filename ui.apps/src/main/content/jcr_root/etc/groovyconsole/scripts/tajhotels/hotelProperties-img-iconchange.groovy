def predicates = [
    "path":"/content/tajhotels/en-in/our-hotels",
    "nodename":"hotelSignatureFeatures",
     "p.limit":"-1"
   ]
   def query = createQuery(predicates)
   def result = query.result
  // path=""
   result.hits.each { hit -> 
     signatureNode= hit.node
     if (isActivePage(signatureNode.path))
     {
     signatureNode.recurse(){ item->
        type= item.get("type")
        switch (type)
        {
            case "/content/dam/tajhotels/icons/hotel-properties/location.svg":
                item.setProperty("type","icon-location")
                
                break
            case  "/content/dam/tajhotels/icons/hotel-properties/direction.svg":
                item.setProperty("type","icon-direction")
                
                break
            case  "/content/dam/tajhotels/icons/hotel-properties/yoga.svg":
                 item.setProperty("type","icon-yoga")
                 
                break
            case  "/content/dam/tajhotels/icons/hotel-properties/garden.svg":
                  item.setProperty("type","icon-garden")
                 
                  break
            case  "/content/dam/tajhotels/icons/hotel-properties/distanceFromAirport.svg":
                  item.setProperty("type","icon-distanceFromAirport")
                  
                  break
             case  "/content/dam/tajhotels/icons/hotel-properties/fitness.svg":
                  item.setProperty("type","icon-fitness")
                  
                  break
             case  "/content/dam/tajhotels/icons/hotel-properties/pet%20friendly.svg":
                  item.setProperty("type","icon-petFriendly")
                   
                  break
            case  "/content/dam/tajhotels/icons/hotel-properties/pet%20friendly.svg":
                  item.setProperty("type","icon-petFriendly")
                   
                  break
            case "/content/dam/tajhotels/icons/hotel-properties/food.svg":
                    item.setProperty("type","icon-food")
                    break
            case "/content/dam/tajhotels/icons/hotel-properties/seaView.svg":
                    item.setProperty("type","icon-seaView")
                    break
        }
        save()
        activate(item.path)
     }
     }
   }


def isActivePage(path)
{
    String [] hotelContent=path.split("/root")
    hotelContentNode=getNode(hotelContent[0])
    if(hotelContentNode && hotelContentNode.get("cq:lastReplicationAction")=="Activate")
    {
        return true
    }
    else
    {
        return false
    }
}