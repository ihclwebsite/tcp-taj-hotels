
executeTajHolidaysTitleType()

def executeTajHolidaysTitleType() {
    def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "nodename":"inspiration-gallery",
        "p.limit":"-1"
        ]

 def query = createQuery(predicate)
 def result = query.result
 
 result.hits.each { hit ->
    galleryNodePath = hit.getResource().path
     println "Gallery Path : " + galleryNodePath

     deactivate(galleryNodePath)
 }
}