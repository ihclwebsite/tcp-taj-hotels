removeIhclcbPagesExceptRoomsPage("/content/ihclcb/en-in/ihclcbtajourhotels");

def removeIhclcbPagesExceptRoomsPage(ihclcbpath){
    println "in method"
    //def deletedComponentType = "wcm/msm/components/ghost";
    def predicate = [
        "path": ihclcbpath,
        "type" : "cq:Page",
          "property": "jcr:content/sling:resourceType",
          "property.1_value": "tajhotels/components/structure/fitness-page",
          "property.2_value": "tajhotels/components/structure/inspiration-gallery-page",
          "property.3_value": "tajhotels/components/structure/jiva-spa-list-page",
          "property.4_value": "tajhotels/components/structure/local-area-list-page",
          "property.5_value": "tajhotels/components/structure/tajhotels-meetings-page",
          "property.6_value": "tajhotels/components/structure/all-offers-page",
          "property.7_value": "tajhotels/components/structure/dining-list-page",
          "property.8_value": "tajhotels/components/structure/experiences-list-page",
          "p.limit":"-1"
        ]
        
    def query = createQuery(predicate)
    def result = query.result
    //println result.hits
    result.hits.each{
    hit -> 
        def refPath = hit.path        
            println refPath
      try {
    
            getNode(refPath).remove()
            //save()
          }
          catch(Exception e) {
            println("44444 Exception occured : Not able to get node  ");
        }
   
    }
}
