
import javax.jcr.nodetype.PropertyDefinition

getAllGlobalHotelOffersPages()

def getAllGlobalHotelOffersPages(){
   def predicates = [
       "path":"/content/tajhotels/en-in/offers/",
       "p.limit":"-1",
       "type":"cq:PageContent",
       "1_property":"sling:resourceType",
       "1_property.value":"tajhotels/components/structure/offer-details-page"
   ]
   
   def query = createQuery(predicates)
   query.hitsPerPage = 100000
   def result = query.result
   
   result.hits.each { hit ->
       println "hit path12 = " + hit.node.path
       nodePath = hit.node.path

       currentGlobalOfferName = hit.node.getParent().getName()
       globalOfferParticipatingHotelNodePath = hit.node.path+'/root/participating_hotels'
       hotelPagesArr = getAllParticipatingHotels(globalOfferParticipatingHotelNodePath, currentGlobalOfferName)
   }
}


def getAllParticipatingHotels(pHotelnodePath, gOfferName) {
    
    try {
       participatingHotelNode = getNode(pHotelnodePath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + pHotelnodePath)
    }
    
    pages = participatingHotelNode.get("pages")

    for(hotelPath in pages) {
        /*offerPathInHotelPath = hotelPath + '/offers-and-promotions';
        try {
          offerNodeInHotelPath = getNode(offerPathInHotelPath)
          println 'hotelPathNode = ' + offerNodeInHotelPath.path;
        } catch(Exception e) {
           println("Exception occured : Not able to get node at path " + hotelPath);
        }*/
        
        jcrContentPathOfDestOffer = hotelPath + '/offers-and-promotions/' + gOfferName + '/jcr:content'
        //println 'jcrContentPathOfDestOffer - '+jcrContentPathOfDestOffer
        
        addCurrentOfferAtPath(hotelPath, gOfferName, jcrContentPathOfDestOffer)

    }
}


def addCurrentOfferAtPath(hotelPathForOffer, offerGlobalName, jcrPath) {
    //println 'offerGlobalName - ' + offerGlobalName
    offerPathOfCurrentHotel = hotelPathForOffer + '/offers-and-promotions'
    //println 'Hotel Offer path : ' + offerPathOfCurrentHotel;
    
    def originPath = '/content/example-page/hotels-in-mumbai/staycations'
    def destPath = offerPathOfCurrentHotel + '/' + offerGlobalName
    println 'this path : ' + destPath
    
    try {
      offerNodeInHotelPath = getNode(destPath)
      println 'this offer is already there at path = ' + offerNodeInHotelPath.path
        //offerNodeInHotelPath.remove();
      //save();
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer" + destPath)
    }
    
    
    
    
    Workspace workspace = session.getWorkspace()
    workspace.copy(originPath, destPath)
    println("offer node copied successfully at path : " + destPath)

    updateJcrContent(nodePath, jcrPath)
    updateOfferDetails(nodePath+'/root/offerdetails', jcrPath+'/root')
    updateOfferInclusion(nodePath+'/root/offerinclusion', jcrPath+'/root')
    updateOfferTermsCondition(nodePath+'/root/offer_terms_conditions', jcrPath+'/root')
    updateAllHotelPathsInReferences(jcrPath, hotelPathForOffer)
    //activateNewOfferNode(destPath);
    
    try {
       destOfferJcrNode = getNode(destPath+'/jcr:content')
        currentNodeActivationStatus = destOfferJcrNode.get("cq:lastReplicationAction")


        if(currentNodeActivationStatus=="Activate") {
           //println 'currentNodeActivationStatus - '+ offerGlobalName +' -> '+currentNodeActivationStatus
           activate(destPath)
        }
       
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + destPath)
    }
    
}


def updateJcrContent(cNodePath, pathJcr) {
    
    try {
       destJcrNode = getNode(pathJcr)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + cNodePath)
    }
    
    println 'cNodePath - ' + cNodePath
    println 'pathJcr - ' + pathJcr
    
    
    
    Resource res = resourceResolver.getResource(cNodePath)
    ValueMap properties = res.adaptTo(ValueMap.class)
    Node currNode=res.adaptTo(Node.class)

    jcrKeySet = properties.keySet()

    for(jcrKey in jcrKeySet) {
       //println 'key - ' + jcrKey;
       //println 'value - ' + properties.get(jcrKey);
       //println 'destJcrNode - ' + destJcrNode.path
       if(!currNode.getProperty(jcrKey).getDefinition().isProtected()){
        //   println "Key : "+jcrKey+ "- Proteccted, - "+currNode.getProperty(jcrKey).getDefinition().isProtected();
        
       destJcrNode.setProperty(jcrKey, properties.get(jcrKey))

       }
    }
    
    globalOfferBannerPath = cNodePath + '/banner_parsys/offerbanner'

    try {
      globalOfferBannerNode = getNode(globalOfferBannerPath)
      println 'hotelPathNode = ' + globalOfferBannerNode.path
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + globalOfferBannerPath)
    }
    
    imagePathInGlobalOfferBannerNode = globalOfferBannerNode.get("bannerImage")
    println 'imagePathInGlobalOfferBannerNode - ' + imagePathInGlobalOfferBannerNode


    pathForCurrentGlobalOffer = cNodePath.replace("/jcr:content", "")

    destJcrNode.setProperty("cq:template", "/conf/tajhotels/settings/wcm/templates/hotel-specific-offer-details")
    destJcrNode.setProperty("sling:resourceType", "tajhotels/components/structure/hotel-specific-offer-details-page")
    destJcrNode.setProperty("globalOfferPath", pathForCurrentGlobalOffer)
    destJcrNode.setProperty("bannerImage", imagePathInGlobalOfferBannerNode)

    save()

}

def updateOfferDetails(originOfferDetailsPath, destOfferDetailsPath) {

    removeNodeIfExists(destOfferDetailsPath+'/offerdetails')

    Workspace workspace = session.getWorkspace()
    workspace.copy(originOfferDetailsPath, destOfferDetailsPath+'/offerdetails')
    println("offer node copied successfully at path : " + destOfferDetailsPath)

    try {
       destOfferDetailsNode = getNode(destOfferDetailsPath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + destOfferDetailsPath)
    }
    
    destOfferDetailsNode.orderBefore('offerdetails','book_now_button')
    save()


}


def updateOfferInclusion(originOfferDetailsPath, destOfferDetailsPath) {

    removeNodeIfExists(destOfferDetailsPath+'/offerinclusion')

    Workspace workspace = session.getWorkspace()
    workspace.copy(originOfferDetailsPath, destOfferDetailsPath+'/offerinclusion')
    println("offer node copied successfully at path : " + destOfferDetailsPath)

    try {
       destOfferDetailsNode = getNode(destOfferDetailsPath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + destOfferDetailsPath)
    }
    
    destOfferDetailsNode.orderBefore('offerinclusion','book_now_button')
    save()


}

def updateOfferTermsCondition(originOfferDetailsPath, destOfferDetailsPath) {

    removeNodeIfExists(destOfferDetailsPath+'/offer_terms_conditions')

    Workspace workspace = session.getWorkspace()
    workspace.copy(originOfferDetailsPath, destOfferDetailsPath+'/offer_terms_conditions')
    println("offer node copied successfully at path : " + destOfferDetailsPath)

    try {
       destOfferDetailsNode = getNode(destOfferDetailsPath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + destOfferDetailsPath)
    }
    
    destOfferDetailsNode.orderBefore('offer_terms_conditions','book_now_button')
    save()


}

def removeNodeIfExists(removeNodePath) {
    
    try {
       removeNode = getNode(removeNodePath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + removeNodePath)
    }
    
    removeNode.remove()
    save()

}

def updateAllHotelPathsInReferences(localJcrPath, currentHotelPath) {
    
    rootReferencePath = localJcrPath + '/root/reference'
    bookNowButtonPath = localJcrPath + '/root/book_now_button'
    bannerParsysReferencePath = localJcrPath + '/banner_parsys/reference'

    try {
       rootReferencePathNode = getNode(rootReferencePath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + rootReferencePath)
    }
    
    rootReferencePathNode.setProperty('path', currentHotelPath+'/jcr:content/root/hotel_navigation')

    try {
       bookNowButtonPathNode = getNode(bookNowButtonPath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + bookNowButtonPath)
    }
    
    bookNowButtonPathNode.setProperty("buttonLink", currentHotelPath)

    try {
       bannerParsysReferencePathNode = getNode(bannerParsysReferencePath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + bannerParsysReferencePath)
    }
    
    bannerParsysReferencePathNode.setProperty("path", currentHotelPath+'/jcr:content/banner_parsys/hotel_banner')
    save()

}


