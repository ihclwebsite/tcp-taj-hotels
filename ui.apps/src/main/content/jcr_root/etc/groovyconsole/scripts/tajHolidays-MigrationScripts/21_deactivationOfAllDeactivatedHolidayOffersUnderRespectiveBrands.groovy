import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils


init()

def init() {
    def brandList = ["tajhotels", "seleqtions", "vivanta"]
    for (String brand : brandList) {
        searchForAllOffersUnder(brand)
    }
}

def searchForAllOffersUnder(String brand) {
   
   def deacativatedPagePaths = getDeactvatedHolidayOffers()
   
   def predicate = [
       "path": "/content/"+brand+"/en-in/our-hotels",
       "property":"sling:resourceType",
       "property.value":"foundation/components/reference",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(hit.path.contains("reference_holiday")) {
           String path= hit.getResource().adaptTo(ModifiableValueMap.class).get("path")
           for(String deactivatedPath : deacativatedPagePaths) {
               if(path.equals(deactivatedPath)) {
                   String hotelOfferPath = hit.getResource().getParent().getParent().path
                   println "Deactivating " + hotelOfferPath
                   deactivate(hotelOfferPath)
               }
           }
        }
   }
}


def getDeactvatedHolidayOffers(){
   def deactivatedHolidayPaths = new ArrayList<String>()
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"searchKey",
       "property.value":"holidayPackages",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(hit.getResource().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")=="Deactivate") {
            String path = hit.path+"/root/holidays_hotel_package"
            deactivatedHolidayPaths.add(path)
        }
   }
    return deactivatedHolidayPaths
}

