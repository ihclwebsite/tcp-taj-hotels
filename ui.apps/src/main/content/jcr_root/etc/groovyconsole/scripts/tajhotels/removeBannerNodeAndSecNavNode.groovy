import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
 
count=0
fetchAllJIVAspaBookingPaths()



def fetchAllJIVAspaBookingPaths(){
    def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/jiva-spa-list-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        def sess=hit.node.getSession()
        if(hit.node.getParent().getName()=="jiva-spa-booking") {
            deleteChildNodesFrom(hit.node.path)
        } 
    }
    println "Successfully deleted "+count+" Nodes"
}

def deleteChildNodesFrom(path) {
    count++
    Resource res = resourceResolver.getResource(path)
    Resource secondaryNavRes = res.getChild("root").getChild("reference")
    if(null!=secondaryNavRes) {
        Node secondaryNavNode = secondaryNavRes.adaptTo(Node.class)
        println "Deleting Node from the path - " +secondaryNavNode.path
        def sess=secondaryNavNode.getSession()
        secondaryNavNode.remove()
        sess.save()
    }
    else{
        println "SecondNavNode not present or already deleted"
    }
    Resource bannerRes =res.getChild("banner_parsys")
    if(null!=bannerRes) { 
        Node bannerNode = bannerRes.adaptTo(Node.class)
        println "Deleting Node from the path - " +bannerNode.path
        def session=bannerNode.getSession()
        bannerNode.remove()
        session.save()
    }
    else {
        println "BannerNode not present or already deleted"
    }
    println "=============================================================="
}
