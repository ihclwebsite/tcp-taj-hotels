
import javax.jcr.Node

/*This method is used to Query the JCR and find results as per the Query.*/
 def buildQuery(page, term) {
       def queryManager = session.workspace.queryManager
       def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
/*Here term is the sling:resourceType property value*/
       queryManager.createQuery(statement, 'sql')
   }

   /*Defined Content Hierarchy */
  final def page = getPage('/content/tajhotels/en-in/our-hotels')
  /*Template which is searched in the content hierarchy */
  final def query = buildQuery(page, 'tajhotels/components/content/room-list')
  final def result = query.execute()
  def rateFilter 
 

  println 'No Of pages found = ' + result.nodes.size()
 result.nodes.each { node ->
       def nodePath=node.path
       println 'nodePath::'+nodePath
    
       rateFilter = node.getNode('rateFilters')
        
       rateFilter.getNodes().each { item ->
       def itemValue = item.getProperty('rateFilterCode')
       def val = itemValue.getValue().getString()
      
       if(val.equals('PKG')){
            item.remove()
            session.save()
       }
    }
 }
 
 
 