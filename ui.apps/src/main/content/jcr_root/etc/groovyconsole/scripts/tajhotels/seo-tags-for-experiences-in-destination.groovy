
import com.day.cq.dam.api.Asset
import com.day.cq.replication.Replicator
import com.day.cq.replication.ReplicationActionType
import com.day.cq.replication.ReplicationStatus
import javax.jcr.Session
import java.util.GregorianCalendar


//filename = "/content/dam/tajhotels/others/Taj Hotels - Beta Website - SEO Optimization Recommendations.csv"
hotelDestinaitonMap = [:]
getHotelDestinationMap()

def getHotelDestinationMap(){
   
   def count=0
    def predicates = [
       "path":"/content/tajhotels/en-in/our-hotels/",
       "p.limit":"-1",
       "type":"cq:PageContent",
       "1_property":"sling:resourceType",
       "1_property.value":"tajhotels/components/structure/hotels-landing-page"
   ]
   
   def query = createQuery(predicates)
   
   query.hitsPerPage = 10000
   
   def result = query.result
   
   result.hits.each { hit ->
       //println "hit path = " + hit.node.path
       
       String hotelPath = hit.node.path
       hotelPathArr = hotelPath.split("/")
       hotelDestinaitonMap.put(hotelPathArr[7], hotelPathArr[5])
   }
}

executeDamContentRisk()

//DAM content access
def executeDamContentRisk(){
    def predicates = [
        "path":"/content/dam/tajhotels/others",
        "type":"nt:Asset",
        "1_property":"jcr:primaryType",
        "1_property.value":"dam:Asset"
    ]
    
    def query = createQuery(predicates)
    def result = query.result
    // "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    result.hits.each { hit ->
        println "hit path for asset = ${hit.node.path}"
        return getDamAssetFromNode(hit.node)
    }
}


//Execute after getting DAM file node 
def getDamAssetFromNode(node){
    //println "node path in getDamAssetFromNode "+node.path
    def assetNodePath = node.path
    Resource res = resourceResolver.getResource(node.path)
    Asset asset= res.adaptTo(Asset.class)
    r=asset.getOriginal()
    text = r.getStream().text
    //println ""+text
    def lines = text.readLines()
    lineIndex= 0
    
    for (String line in lines) {
        lineIndex++
        println ""+lineIndex + " -> "+line
        executeOneLineForRestaurants(line)
    }
    
}


def executeOneLineForRestaurants(line){
   println 'running executeOneLineForRestaurants'
   lineArr = line.split("%")

    def contentPath = lineArr[0]
   def title = modifyDetails(lineArr[1])
   def metaDesc = modifyDetails(lineArr[2])
   def metaKeyword = modifyDetails(lineArr[3])
   
   String contentPathNew = contentPath.replace('https://beta.tajhotels.com','/content/tajhotels')
   contentPathDetails = contentPathNew.split("/")
    hotelsInDestinationRest =  contentPathDetails[5].replace('experiences','hotels')
    finalContentPath = contentPathNew.replace('destination','our-hotels/'+hotelsInDestinationRest) + '/jcr:content'
    //println 'finalContentPath - ' + finalContentPath
   //println 'Title - ' + title
   //println 'metaDesc - ' + metaDesc
   //println 'metaKeyword - ' + metaKeyword
   try {
       def nodeAtPath = getNode(finalContentPath)
       //println nodeAtPath.get("seoDescription")
       nodeAtPath.setProperty("pageTitle",title)
       nodeAtPath.setProperty("seoDescription",metaDesc)
       nodeAtPath.setProperty("seoKeywords",metaKeyword)
       save()
       
       currentNodeActivationStatus = nodeAtPath.get("cq:lastReplicationAction")

       if(currentNodeActivationStatus=="Activate") {
           activate(finalContentPath)
       }
       
   } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + finalContentPath)
   }
   
   println 'content updated successfully at path ' + finalContentPath
   
}

def modifyDetails(seoDetails) {
    return seoDetails.replace('"','')
}

