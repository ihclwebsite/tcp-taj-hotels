import javax.jcr.Node
import javax.jcr.Workspace;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.commons.lang3.text.WordUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

init();

def init() {
    def predicate = [
       "path": "/content/tajhotels/ihcl/press-room",
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/content/ihcl/title",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(null!=hit.getResource().adaptTo(ModifiableValueMap.class).get("title")){
         String title = hit.getResource().adaptTo(ModifiableValueMap.class).get("title").replace("\n","");
         hit.getResource().adaptTo(ModifiableValueMap.class).put("title",title);
         hit.getResource().getResourceResolver().commit();
         activate(hit.path);
        }
   }
}
