import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
j=0
init()

def init(){
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"searchKey",
       "property.value":"aboutDestination",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
   
       String[] city = hit.getResource().adaptTo(ModifiableValueMap.class).get("city")
       if(city[0].split("/")[1]!="india") {
           searchPackages(hit.getResource().getParent().path)
       }
   }
}


def searchPackages(destinationPath){
   def predicate = [
       "path": destinationPath,
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-package-details",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
       ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
       modifiableValueMap.put("packageRedirectionType","internationalEnquiry")
       modifiableValueMap.put("enquiryPath","/content/tajhotels/en-in/taj-holidays/holidays-enquiry")
       hit.getResource().getResourceResolver().commit()
   }
}