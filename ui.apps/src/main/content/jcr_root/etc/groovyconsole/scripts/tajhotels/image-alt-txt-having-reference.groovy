package tajhotels

import javax.jcr.query.Query
import javax.jcr.query.QueryManager
import com.day.cq.wcm.commons.ReferenceSearch
import org.apache.sling.api.resource.ResourceResolver
import com.day.cq.dam.api.AssetManager
import java.nio.charset.StandardCharsets

def buildQueryPrdicate(queryPath){
    def predicate = [
            "path": queryPath,
            "nodename":"metadata",
            "1_property":"dc:altText",
            "1_property.operation":"not",
            "orderby":"@jcr:content/publishDate",
            "orderby.sort":"desc",
            "p.guessTotal":"true",
            "p.limit":"-1",
            "p.offset":"10"
    ]
    def query = createQuery(predicate)
    def result = query.result
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    def rowCount=0
    def headersArray= Arrays.asList("Sl No", "Image Path")
    StringBuffer csvRows=new StringBuffer("")
    StringBuffer headersRow=new StringBuffer("")
    StringBuffer bodyRow=new StringBuffer("")
    for(def eachHeader:headersArray){
        headersRow.append(eachHeader).append(",")
    }
    headersRow.append("\n ")
    rowCount++
    println "Header Row : "+headersRow
    def bodyIndex=1
    result.hits.each { hit ->
        println "Image Asset Path : " + hit.node.parent.parent.path
        def isExists=queryXpathForAssetReference2(hit.node.parent.parent.path)
        if(isExists){
            bodyRow.append(bodyIndex).append(", ").append(hit.node.parent.parent.path).append(", \n ")
            bodyIndex++
        }
    }
    // println "Body Rows : "+bodyRow;
    csvRows.append(headersRow).append(bodyRow)
    println "CSV Rows : "+csvRows
    InputStream stream =org.apache.commons.io.IOUtils.toInputStream(csvRows, "UTF-8")
    pathTobeCreatedAt="/content/dam/tajhotels/others/altTextImagesList.csv"
    def csvMimeType="text/csv"
    createAssetInDam(pathTobeCreatedAt, stream, "text/csv", true)
}

boolean queryXpathForAssetReference2(imagePath){
    String damPath = imagePath
    def refCount=0
    // println "Reference List starts ------------"
    def refSearchCollection=new ReferenceSearch().search(resourceResolver, damPath).values()
    if(refSearchCollection.size()>1){
        return true
    }else{
        return false
    }
    /*
    for (ReferenceSearch.Info info: refSearchCollection) {
        println "ReferenceCount_1 : "+refSearchCollection.size();
         println "ReferenceCount_2  : "+info.getProperties().size();
       for (String p: info.getProperties()) {
           println "Path is "+info.getPage().getPath();
           refCount++;
       }
    }
       println "Reference List ends ------------";
       */
}

def createAssetInDam(pathTobeCreatedAt, inputStream, fileType, isToBeSaved){
    isToBeSaved=true
    println "inside writeToDam"
    AssetManager assetMgr = resourceResolver.adaptTo(AssetManager.class)
    if (assetMgr != null) {
        println "inisde asset mgr path of node to be removed is" + pathTobeCreatedAt
        resource = resourceResolver.getResource(pathTobeCreatedAt)
        if (resource != null) {
            javax.jcr.Node node = resource.adaptTo(javax.jcr.Node.class)
            node.remove()
            node.getSession().save()
            println "node has removed successfully"
        }
        assetMgr.createAsset(pathTobeCreatedAt, inputStream, fileType, isToBeSaved)
    }
}

// queryXpathForAssetReference2("/content/dam/thrp/destinations/Ajmer/16x7intro/ESY-006542187-3.jpg")
def arr=Arrays.asList("/content/dam/gateway/homepage","/content/dam/gateway","/content/dam/thrp","/content/dam/luxury","/content/dam/vivanta", "/content/dam/taj", "/content/dam/jiva-spa", "/content/dam/tajhotels")
int i=1
// buildQueryPrdicate(arr.get(i));


getChildPathsOf("/content/dam/")


"/content/dam/"
def getChildPathsOf(parentPath){
    Node topNode=getNode(parentPath)
    println "Path 2 : "+topNode.path
    topNode.getNodes().each{ n ->
        println "Path : "+n.getPath()
    }
}

