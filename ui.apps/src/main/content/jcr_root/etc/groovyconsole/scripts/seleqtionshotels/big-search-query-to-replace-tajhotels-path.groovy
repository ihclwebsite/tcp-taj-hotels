package etc.groovyconsole.scripts.seleqtionshotels


SELEQTIONS_REPLACING_TEXT_1="/seleqtions/"

CHECK_INCLUDE_PATH_1="/content/seleqtions/en-in/jcr:content"
CHECK_INCLUDE_PATH_2="/serviceAmenities"

CHECK_INCLUDE_PATH=CHECK_INCLUDE_PATH_2


/*Generic method takes a predicatesMap and resultSizePerPage then returns result*/
def processQueryByPredicates(predicates, queryHitsPerPage){
    def query = createQuery(predicates)
    query.hitsPerPage = queryHitsPerPage
    def result = query.result
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    return result
}

def getAllContentTajhotelsPath(pathLimitedTo, fulltext, startIndex, maxIndex, queryHitsPerPage, isToPermanentlySaved){
    def predicates= queryForSearchText(pathLimitedTo, fulltext)
    def result =processQueryByPredicates(predicates, queryHitsPerPage)
    if(result.totalMatches!=0){
        if(maxIndex==-1)
            maxIndex=result.totalMatches
        processQueryResult(result, startIndex, maxIndex, isToPermanentlySaved)
    }
    return result
}
def processQueryResult(result, startIndex, maxIndex, isToPermanentlySaved){
    result.hits.eachWithIndex { hit , index ->
        if(index>startIndex && index<maxIndex){
            def nodePath=hit.node.path
            def propertyKeyToChange=checkForReferencesToUpdate(nodePath)
            // println "Node path is :" +nodePath+ ", isExecuting :"+checkForSkippingPath(nodePath);
            if(nodePath.contains(CHECK_INCLUDE_PATH_1)){        //for en-in/jcr:content/*
                println "Hotel node path executing CHECK_INCLUDE_PATH01 :" +nodePath
                processNodeToReplacePerPropertyKeySingleAndMulti(nodePath, propertyKeyToChange, isToPermanentlySaved)
            }
            if(checkForSkippingPath(nodePath) && (propertyKeyToChange!=false)){
                // println "Node path executing :" +nodePath + "\n PropertyKeyToLookup :" +propertyKeyToChange;
                if(nodePath.contains(CHECK_INCLUDE_PATH_2)){
                    println "Hotel node path executing CHECK_INCLUDE_PATH02 :" +nodePath
                    processNodeToReplacePerPropertyKeySingleAndMulti(nodePath, propertyKeyToChange, isToPermanentlySaved)
                }else{
                    // processNodeToReplacePerPropertyKeySingleAndMulti(nodePath, propertyKeyToChange, isToPermanentlySaved)
                }
                // println "PropertyValue after changing :"+getNode(nodePath).get(propertyKeyToChange)
            }
        }
    }
}

def processNodeToReplacePerPropertyKeySingleAndMulti(nodePath, propertyKey, isToPermanentlySaved){
    if(isSingleStringValued(propertyKey)){
        //  println "Property Key as String :"+propertyKey
        processNodeToReplaceTajhotelsContent(nodePath, propertyKey, isToPermanentlySaved)
    }else if(isMultiValued(propertyKey)){
        //  println "Property Key as List :"+propertyKey
        propertyKey.each{ singlePropertyKey ->
            processNodeToReplaceTajhotelsContent(nodePath, singlePropertyKey, isToPermanentlySaved)
        }
    }
}
def processNodeToReplaceTajhotelsContent(nodePath, propertyKey, isToPermanentlySaved){
    // println "Node path executing_2 :"+nodePath + "\n Checking for the propertyKey_2 :" +propertyKey
    def node1=getNode(nodePath)
    if(node1.hasProperty(propertyKey)){
        def propertyValue=node1.get(propertyKey)
        // println "Property Value :"+propertyValue+ "\n Property Value classType :"+propertyValue.getClass()
        if(isSingleStringValued(propertyValue)){
            //  println "Property Value as String :"+propertyValue
            propertyValue=processNodeToReplcePerPropertyKey(node1, propertyKey, propertyValue, isToPermanentlySaved)
            node1.set(propertyKey, propertyValue)
        }else if(isMultiValued(propertyValue)){
            //  println "Property Value as List :"+propertyValue
            //  processNodeToReplcePerPropertyKeyList(node1, propertyKey, propertyValue, isToPermanentlySaved)
            def updatingValue= new ArrayList()
            propertyValue.each{ singleValue ->
                singleValue=processNodeToReplcePerPropertyKey(node1, propertyKey, singleValue, isToPermanentlySaved)
                updatingValue.add(singleValue)
            }
            if(updatingValue.size()>0){
                node1.set(propertyKey, updatingValue)
            }
        }
        if(isToPermanentlySaved){
            save()
        }
        println "Property key: $propertyKey and Changed value :"+node1.get(propertyKey)
    }
}

def isSingleStringValued(propertyValue){
    return (propertyValue instanceof String)
}
def isMultiValued(propertyValue){
    return (propertyValue instanceof java.util.List)
}

def processNodeToReplcePerPropertyKey(node1, propertyKey, propertyValue, isToPermanentlySaved){
    if(checkStringContainsMapKeys(propertyValue, replacingMap)){
        propertyValue=replaceStringContainsMapKeys(propertyValue, replacingMap)
    }
    return propertyValue
}

def checkForSkippingPath(pathToCheck){
    def isExists= false
    def arr2=pathToCheck.split("/")
    if(arr2.size()>6){
        // println "arr2 : "+arr2[6]
        if(arr2[6].contains("seleqtions")){
            if(seleqtionsHotelPaths.contains(arr2[7])){
                // println "seleqtionsHotelPaths : "+arr2[7]
                isExists= true
            }
        }
    }
    return isExists
}

def checkForReferencesToUpdate(pathToCheck){
    def arr1=pathToCheck.split("/")
    def lastElement=arr1[arr1.size()-1]
    return checkStringContainsMapKeys(lastElement, componentsMap)
}

def checkStringContainsMapKeys(stringValue, someMap){
    def isExists= false
    // println "someMap :"+someMap
    for(elePath in someMap.entrySet()){
        // println "entryKey :"+elePath.key
        if(stringValue.trim().contains(elePath.key.trim())){
            isExists= elePath.value
            // println "Path contains node name : "+   stringValue+"\n isExists : "+   elePath.value;
            return isExists
        }
        else{
            isExists= false
            // println "Path Not contains : "+   stringValue
        }
        // println "nodeName to change : "+   stringValue
    }
    return isExists
}

def replaceStringContainsMapKeys(stringValue, someMap){
    // println "someMap :"+someMap
    for(elePath in someMap.entrySet()){
        // println "entryKey :"+elePath.key
        if(stringValue.trim().contains(elePath.key.trim())){
            // println "Path contains node name : "+   stringValue;
            stringValue=stringValue.replace(elePath.key, elePath.value)
        }
        else{
            // println "There is no String to replace : \n"+ "Original String : "+ stringValue +"\n String should contains : "+ elePath.key+"\n and should replaced by : "+elePath.value
        }
    }
    return stringValue
}

def processNodeToReplace(nodePath, propertyKey, propertyValue){
    def node1=getNode(nodePath)
    if(node1.hasProperty(propertyKey)){
        node1.setProperty(propertyKey, propertyValue)
    }
}

def deleteDuplicateSeleqtionsHotelsUnderOtherBrand(pathLimitedTo, queryHitsPerPage, isToPermanentlySaved){
    def slingResourceType="tajhotels/components/structure/hotels-landing-page"
    def predicates = queryForListOfPagesByResourceType(pathLimitedTo, slingResourceType)
    def result = processQueryByPredicates(predicates, queryHitsPerPage)
    if(result.totalMatches!=0){
        result.hits.eachWithIndex { hit , index ->
            def pathToCheck=hit.node.path
            // println "seleqtionsHotelPaths23 : "+pathToCheck
            def arr2=pathToCheck.split("/")
            if(arr2.size()>6){
                // println "arr2[6] : "+arr2[6]
                if(!arr2[6].contains("seleqtions")){
                    if(seleqtionsHotelPaths.contains(arr2[7])){
                        // println "seleqtionsHotelPaths : "+arr2[7]
                        getNode(pathToCheck).remove()
                        println "seleqtionsHotelPaths22 : "+pathToCheck
                        if(isToPermanentlySaved){
                            save()
                        }
                    }
                }
            }
        }
    }
}
def queryForListOfPagesByResourceType(pathLimitedTo, slingResourceType){
    def predicates = [
            "path":pathLimitedTo,
            "type":"cq:Page",
            "1_property":"jcr:content/sling:resourceType",
            "1_property.value":"tajhotels/components/structure/hotels-landing-page"
            //   "p.limit":100
    ]
    return predicates
}
def prepareData(){
    //Below hotels are list of hotels under seleqtionsBrand
    seleqtionsHotelPaths= new HashSet()
    seleqtionsHotelPaths.add("president-mumbai")
    seleqtionsHotelPaths.add("blue-diamond-pune")
    seleqtionsHotelPaths.add("ambassador-new-delhi")
    seleqtionsHotelPaths.add("fatehabad-road-agra")
    seleqtionsHotelPaths.add("savoy")

    componentsMap= new HashMap()
    componentsMap.put("reference", "path")
    def itemKeyList=Arrays.asList("tabtextlink", "link")
    componentsMap.put("item", itemKeyList)
    def serviceAminitiesKeyList=Arrays.asList("otherConveniences","bedAndBathExperience","hotelFacilities","wellnessAmenities","suiteFeatures","king-size-bed","airport-pick-up","sea-view","thermo-dynamic-showers")
    componentsMap.put("serviceAmenities", serviceAminitiesKeyList)

    // The below map has key:value as oldValue:newValue or oldExistingValue:toBeUpdatedTheNewValue
    replacingMap= new HashMap()
    replacingMap.put("/content/tajhotels/en-in/shared-content", "/content/shared-content/global-shared-content/shared-content")
    replacingMap.put("content/tajhotels", "content/seleqtions")
    replacingMap.put("/taj/",SELEQTIONS_REPLACING_TEXT_1)
    replacingMap.put("/vivanta/",SELEQTIONS_REPLACING_TEXT_1)
    replacingMap.put("/gateway/",SELEQTIONS_REPLACING_TEXT_1)

}

def queryForSearchText(pathLimitedTo, fulltext){
    def predicates = [
            "path":pathLimitedTo,
            "fulltext":fulltext,
            "orderby":"@path",
            "orderby.sort":"desc"
            //   "p.limit":100          //query.hitsPerPage has more priority in this query
    ]
    return predicates
}
prepareData()
// def pathLimitedTo="/content/seleqtions/en-in"
def pathLimitedTo="/content/seleqtions/en-in/our-hotels"
def fulltext="content/tajhotels"
def startIndex=0
def maxIndex=23650  //-1 for all
def queryHitsPerPage=23650      // it is used for query and different from maxIndex
def isToPermanentlySaved=false
// def isToPermanentlySaved=true
getAllContentTajhotelsPath(pathLimitedTo, fulltext, startIndex, maxIndex, queryHitsPerPage,isToPermanentlySaved)
//  deleteDuplicateSeleqtionsHotelsUnderOtherBrand(pathLimitedTo, queryHitsPerPage, isToPermanentlySaved)      //for deleting duplicate Seleqtion Hotels present in other brand

// def nodePath2="/content/seleqtions/en-in/our-hotels/hotels-in-maldives/taj/coral-reef-maldives/fitness-centre/jcr:content/root/fitness/serviceAmenities"
// def propertyKey2="otherConveniences"
// def isToPermanentlySaved2=false
// processNodeToReplaceTajhotelsContent(nodePath2, propertyKey2, isToPermanentlySaved2)



