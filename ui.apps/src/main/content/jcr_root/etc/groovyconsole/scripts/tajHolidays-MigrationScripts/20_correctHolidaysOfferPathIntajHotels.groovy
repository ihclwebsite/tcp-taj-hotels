import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

init()

def init() {
    def brandList = ["tajhotels", "seleqtions", "vivanta"]
    for (String brand : brandList) {
        correctEntryUnder(brand)
    }
}

def correctEntryUnder(String brand){

   def predicate = [
       "path": "/content/"+brand+"/en-in/our-hotels",
       "property":"sling:resourceType",
       "property.value":"foundation/components/reference",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(hit.path.contains("reference_holiday")) {
            String path= hit.getResource().adaptTo(ModifiableValueMap.class).get("path")
            if (!path.contains("/content/tajhotels")){
                String updatedPath = path.replaceAll("/content/"+brand,"/content/tajhotels")
                println path
                println updatedPath
                println hit.getResource().getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")
                hit.getResource().adaptTo(ModifiableValueMap.class).put("path",updatedPath)
                hit.getResource().getResourceResolver().commit()
                if(hit.getResource().getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")=="Activate") {
                    activate(hit.path)
                }
            }
        }
   }
}