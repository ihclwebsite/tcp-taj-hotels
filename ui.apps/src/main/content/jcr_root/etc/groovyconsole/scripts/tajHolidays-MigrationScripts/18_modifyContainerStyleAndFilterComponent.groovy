import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
j=0
init()

def init(){
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(hit.path.contains("packages")){
            String newContainerStyle = "specific-hotels-page holiday-theme holidays-packages-layout"
            hit.getResource().adaptTo(ModifiableValueMap.class).put("containerStyles",newContainerStyle)
            hit.getResource().getResourceResolver().commit()
            
                
            String rootPath = hit.path +"/root"
            //println "RootPath of PackagePath : " + rootPath;
            Resource rootPathResource = resourceResolver.getResource(rootPath)
            println "Resource : " + rootPathResource
            Node rootNode = rootPathResource.adaptTo(Node.class)
            def sess=rootNode.getSession()
            rootNode.addNode("holiday_search_filt","nt:unstructured")
            sess.save()
            Resource searchFilterResource = rootPathResource.getChild("holiday_search_filt")
            ModifiableValueMap modifiableValueMap = searchFilterResource.adaptTo(ModifiableValueMap.class)
            modifiableValueMap.put("sling:resourceType","tajhotels/components/content/holidays/holidays-search-filter")
            try{
            rootNode.orderBefore('holiday_search_filt','holidays_hotel_list')
            }catch(Exception e) {
                println "Exception occured ::> " + hit.path
            }
            hit.getResource().getResourceResolver().commit()
            
        }
   }
}