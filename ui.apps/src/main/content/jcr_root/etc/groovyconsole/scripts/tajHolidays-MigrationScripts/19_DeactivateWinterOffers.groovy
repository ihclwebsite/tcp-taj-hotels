import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

init()
def init(){
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"searchKey",
       "property.value":"holidayPackages",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(!hit.path.contains("summer")){
            String winterPath =hit.path.replaceAll("/jcr:content","")
            Resource compRes = resourceResolver.getResource(winterPath+"/jcr:content/root/holidays_hotel_package")
            String winterPackageEndDate = compRes.adaptTo(ModifiableValueMap.class).get("packageValidityEndDate")
            //println winterPackageEndDate +" :  "+ winterPath
            if(null!=winterPackageEndDate){
                if(winterPackageEndDate.equals("31 Mar 2019")) {
                   println winterPackageEndDate +" :  "+  resourceResolver.map(winterPath+".html")
                   deactivate(winterPath)
                }
            }
            
        }
   }
}