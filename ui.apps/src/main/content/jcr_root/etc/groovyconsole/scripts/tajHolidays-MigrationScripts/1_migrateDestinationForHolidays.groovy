import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #1 - backboneScript(need to executed first before executing the subsequent scripts. The following scripts creates each holiday destination in 
accordance with destinations present in thrp/Holidays .Create a reference path consisting of single destination with new components with no data 
and when query is run,reference page is copied and set with data from old content */

getAllHolidayDestination()

def getAllHolidayDestination(){
    def predicate = [
        "path": "/content/thrp/holidays/en-in/destinations",
        "property":"sling:resourceType",
        "property.value":"taj-core/pagecomponents/destinationPage",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        def sess=hit.node.getSession()
        
        createNewNJCRNodeUnderDestination(hit.node.getParent())

    }
}

def createNewNJCRNodeUnderDestination(sourceNode) {
    Resource res = resourceResolver.getResource("/content/tajhotels/en-in/holidays-landing-page/holidays-destination")
    if(res!=null){
        Node node = res.adaptTo(Node.class)
        def sess=node.getSession()
        def workspace = sess.getWorkspace()
        def destination_page_name = sourceNode.getName().replaceAll("about-","holidays-in-")
        node.addNode(destination_page_name, "cq:Page")
        sess.save()
        Resource childResource = res.getChild(destination_page_name)
        Node childNode = childResource.adaptTo(Node.class)
        def newJCRpath=childNode.path+"/jcr:content"
        println newJCRpath
        workspace.copy("/content/tajhotels/en-in/reference-holidays-destination/holidays-in-goa/jcr:content",newJCRpath)
        sess.save()
        Resource jcrResource = childResource.getChild("jcr:content")
        Node jcrNode = jcrResource.adaptTo(Node.class)
        buildJCRContent(sourceNode,jcrNode)
    }
}

def buildJCRContent(sourceNode,jcrNode){
    
    Resource sourceNodeResource = resourceResolver.getResource(sourceNode.path)
    Resource sourceJCRNodeResource = sourceNodeResource.getChild("jcr:content")
    Node sourceJCRnode = sourceJCRNodeResource.adaptTo(Node.class)
    
    migrateJcrContentProperties(sourceJCRnode,jcrNode)
    migrateBannerProperties(sourceJCRnode,jcrNode)
    migrateAboutDestinationComponent(sourceJCRnode,jcrNode)
    migratePlanYourItineraryComponent(sourceJCRnode,jcrNode)
}

def migrateJcrContentProperties(sourceNode,destinationNode) {
    
    Resource sourceNodeResource = resourceResolver.getResource(sourceNode.path)
    
    Resource destinationNodeResource = resourceResolver.getResource(destinationNode.path)
    ModifiableValueMap modifiableValueMap = destinationNodeResource.adaptTo(ModifiableValueMap.class)
    
    def destinationName = sourceNode.get("jcr:title").replaceAll("Destination-","")
    def changeFreq = sourceNode.get("changefreq")
    String[] city =sourceNode.get("city")
    def pageTitle=sourceNode.get("pageTitle")
    def priority=sourceNode.get("priority")
    def seoDescription=sourceNode.get("seoDescription")
    def seoKeywords=sourceNode.get("seoKeywords")
    def seoMetaTag=sourceNode.get("seoMetaTag")
    def taggedTo=sourceNode.get("taggedTo")
    
    if(null!=destinationName) {
        modifiableValueMap.put('destinationName',destinationName)
    }
    if(null!=changeFreq) {
        modifiableValueMap.put('changefreq',changeFreq)
    }
    if(null!=city) {
        modifiableValueMap.put('city',city)
    }
    if(null!=pageTitle) {
        modifiableValueMap.put('pageTitle',pageTitle)
    }
    if(null!=priority) {
        modifiableValueMap.put('priority',priority)
    }
    if(null!=seoDescription) {
        modifiableValueMap.put('seoDescription',seoDescription)
    }
    if(null!=seoKeywords) {
        modifiableValueMap.put('seoKeywords',seoKeywords)
    }
    if(null!=seoMetaTag) {
        modifiableValueMap.put('seoMetaTag',seoMetaTag)
    }
    if(null!=taggedTo) {
       modifiableValueMap.put('taggedTo',taggedTo)
    }
    
    modifiableValueMap.put('destinationRootPath',destinationNode.getParent().path)

    Resource imageRes =sourceNodeResource.getChild("image")
    if(null!=imageRes){
        Node imageNode = imageRes.adaptTo(Node.class)
        if(null!=imageNode) {
            def imagePath=imageNode.get("fileReference")
            if(null!=imagePath){
                modifiableValueMap.put('imagePath',imagePath)
            }
        }
    }
    
    Resource offerFacetRes =sourceNodeResource.getChild("offerFacetRes")
    if(null!=offerFacetRes){
        Node offerFacetNode = offerFacetRes.adaptTo(Node.class)
        if(null!=offerFacetNode) {
            def holidayOfferTag=imageNode.get("holidayOfferTag")
            if(null!=holidayOfferTag){
                modifiableValueMap.put('holidayOfferTag',holidayOfferTag)
            }
        }
    }
    
    resourceResolver.commit()
}

def migrateBannerProperties(sourceNode,destinationJCRNode) {
    
    Resource sourceNodeResource = resourceResolver.getResource(sourceNode.path)
    Resource destinationJCRNodeResource = resourceResolver.getResource(destinationJCRNode.path)
    
    Resource heroCarouselComponentRes =sourceNodeResource.getChild("heroCarouselComponent")
    if(null!=heroCarouselComponentRes){
        Node heroCarouselComponentNode = heroCarouselComponentRes.adaptTo(Node.class)
        if(null!=heroCarouselComponentNode) {
            def bannerImage=heroCarouselComponentNode.get("imagePath16_7")
            def hotelName=heroCarouselComponentNode.get("title")
            if(null!=bannerImage || null!=hotelName){
                Resource destinationBannerRes = destinationJCRNodeResource.getChild("banner_parsys").getChild("hotel_banner")
                ModifiableValueMap modifiableValueMap = destinationBannerRes.adaptTo(ModifiableValueMap.class)
                modifiableValueMap.put('bannerImage',bannerImage)
                modifiableValueMap.put('hotelName',WordUtils.capitalizeFully(hotelName))
                resourceResolver.commit()
            }
        }
    }
}

def migrateAboutDestinationComponent(sourceNode,destinationJCRNode) {
    
    String[] city =sourceNode.get("city")
    def targetPath=destinationJCRNode.path +"/root/about_destination"
    if(null!=city) {
        
        def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "1_property":"city",
        "1_property.value":city,
        "2_property":"sling:resourceType",
        "2_property.value":"tajhotels/components/structure/destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        def parentPath= hit.node.getParent().path
        String aboutDestinationPath=getAboutDestinationComponent(parentPath)
        println aboutDestinationPath
        def sess=hit.node.getSession()
        def workspace = sess.getWorkspace()
        workspace.copy(aboutDestinationPath,targetPath)
        sess.save()
        }
    }
}

def getAboutDestinationComponent(path){
    
    String resultPath
    def predicate = [
        "path": path,
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/content/about-destination",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        String hitPath = hit.node.path
        int depthOfHitPath = hitPath.split("\\/", -1).length - 1
        if(depthOfHitPath == 9) {
            resultPath=hitPath
        }
    }
    return resultPath
}

def migratePlanYourItineraryComponent(sourceJCRnode,destinationJCRNode) {
    
    Resource sourceJCRresource = resourceResolver.getResource(sourceJCRnode.path)
    Resource destinationJCRNodeRootResource = resourceResolver.getResource(destinationJCRNode.path+"/root")
    Node targetRootNode = destinationJCRNodeRootResource.adaptTo(Node.class)
    
    Resource childResource = sourceJCRresource.getChild("destinationHighlightsComp")
    if(null!=childResource) {
        Node destinationHighlightsCompNode = childResource.adaptTo(Node.class)
        String[] localeList = destinationHighlightsCompNode.get("destinations")
        def sess=targetRootNode.getSession()
        targetRootNode.addNode("dest_plan_itinerary","nt:unstructured")
        sess.save()
        Resource planYourItineraryRes = destinationJCRNodeRootResource.getChild("dest_plan_itinerary")
        ModifiableValueMap modifiableValueMap = planYourItineraryRes.adaptTo(ModifiableValueMap.class)
        if(null != localeList) {
            modifiableValueMap.put("localList",localeList)
        }
        modifiableValueMap.put("sling:resourceType","tajhotels/components/content/holidays-destination/destination-plan-your-itenerary")
        modifiableValueMap.put("planYourIteneraryLabel","Plan your Itinerary")
        resourceResolver.commit()
    }
}

