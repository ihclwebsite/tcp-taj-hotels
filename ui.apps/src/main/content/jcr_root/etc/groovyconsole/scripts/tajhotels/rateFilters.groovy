import javax.jcr.Node
 


/*This method is used to Iterate all the pages under a hierarchy
 *and find pages with a specific template
 */

getPage("/content/tajhotels/en-in/our-hotels").recurse { page ->
    def content = page.node
    if (content && content.get("sling:resourceType")=="tajhotels/components/structure/tajhotels-rooms-listing-page"){
         def path =page.path
         //   println path
            findRateFilters(path)
         
    }
}



def findRateFilters(path)
 {
    // println path
    def predicates = [
    "path":path ,
    "property": "rateFilterCode",
    "property.1_value":"STD",
    "property.2_value":"TIC",
    "property.3_value":"PKG"]
    def query = createQuery(predicates)
    def result = query.result
    
    result.hits.each { hit ->
    def path1=hit.node.path
    def filterCode = hit.node.get("rateFilterCode")
    if(filterCode=="STD"){
       // println 'Inside STD='+hit.node.get("rateFilterName")
        hit.node.setProperty('rateFilterName','STANDARD RATES')
    }
     if(filterCode=="TIC"){
      //  println 'Inside TIC='+hit.node.get("rateFilterName")
        hit.node.setProperty('rateFilterName','MEMBER RATES')
    }
     if(filterCode=="PKG"){
      //  println 'Inside PKG='+hit.node.get("rateFilterName")
        hit.node.setProperty('rateFilterName','PACKAGES')
    }
  
   
   session.save()
   
    }
 }