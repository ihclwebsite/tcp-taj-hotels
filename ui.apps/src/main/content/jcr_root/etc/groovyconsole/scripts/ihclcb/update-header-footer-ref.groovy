/*groov to change header reference path and footer path for a particular path , Please altert the paths/lines as per your needs before running the script*/


import groovy.json.JsonSlurper
import javax.jcr.Node
import javax.jcr.Workspace;
import org.apache.jackrabbit.commons.JcrUtils;

changeContentRefs()

def changeContentRefs(){
    
    changeRef("/content/ihclcb/en-in/our-hotels-vivanta");
    changeRef("/content/ihclcb/en-in/our-hotel-ihclcb-selections");
    changeRef("/content/ihclcb/en-in/ihclcbtajourhotels");
}

def changeRef(brandrootPath){
    
    //destination section
    def predicate = [
        "path": brandrootPath,
        "property":"jcr:content/sling:resourceType",
        "property.value":"tajhotels/components/structure/destination-landing-page",
        "p.limit":"-1"
        ]
        
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        println hit.path
        def destinationHeaderNode = hit.getResource().getChild("jcr:content/header_parsys/reference")
        if(null != destinationHeaderNode){
            Node node = destinationHeaderNode.adaptTo(Node.class);
            node.setProperty("path","/content/ihclcb/en-in/ihclcb-header-footer/jcr:content/header_parsys/hotelheader");
            save();
         }
        
        def destinationFooterNode = hit.getResource().getChild("jcr:content/footer_parsys/reference")
         
        if(null != destinationFooterNode){
            Node node = destinationFooterNode.adaptTo(Node.class);
            node.setProperty("path","/content/ihclcb/en-in/ihclcb-header-footer/jcr:content/footer_parsys");
            save();
        }
        
    }
    
    //rooms and suits section
    
    predicate = [
        "path": brandrootPath,
        "property":"jcr:content/sling:resourceType",
        "property.value":"tajhotels/components/structure/tajhotels-rooms-listing-page",
        "p.limit":"-1"
        ]
        
    query = createQuery(predicate)
    result = query.result
    result.hits.each{
    hit -> 
        println hit.path
        destinationHeaderNode = hit.getResource().getChild("jcr:content/header_parsys/reference")
        if(null != destinationHeaderNode){
            Node node = destinationHeaderNode.adaptTo(Node.class);
            node.setProperty("path","/content/ihclcb/en-in/ihclcb-header-footer/jcr:content/header_parsys/hotelheader");
            save();
         }
        
        destinationFooterNode = hit.getResource().getChild("jcr:content/footer_parsys/reference")
         
        if(null != destinationFooterNode){
            Node node = destinationFooterNode.adaptTo(Node.class);
            node.setProperty("path","/content/ihclcb/en-in/ihclcb-header-footer/jcr:content/footer_parsys");
            save();
        }
        
    }
    
    
}
