import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #2 - Latitude&Longitutude migration - The old content didnt have the destination level lattitude and longitude , this script will 
migrate the latitude and the longitude from the destinations present in new TajContent 
Note  : Manual Intervention is required for couple of destination as there are mismatch in the destination names present in new TajContent
and the existing TajHolidayDestination . ( example  :- bengaluru ,kerala ,rajasthan */

getAllHolidayDestination()

def getAllHolidayDestination(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        String[] city= hit.node.get("city")
        Node destinationNodeFromTajHotels =getHotelDestinationJCRPropertiesUsing(city)
        if(null!=destinationNodeFromTajHotels) {
            println destinationNodeFromTajHotels.path
            def latitude = destinationNodeFromTajHotels.get("latitude")
            def longitude = destinationNodeFromTajHotels.get("longitude")
            def sess=hit.node.getSession()
            Resource hitRes = hit.getResource()
            ModifiableValueMap modifiableValueMap = hitRes.adaptTo(ModifiableValueMap.class)
            modifiableValueMap.put("latitude",latitude)
            sess.save()
            modifiableValueMap.put("longitude",longitude)
            sess.save()
        }
        else {
            println "mismatch of city parameter under " + hit.node.getParent().getName() 
        }
    }
}


def getHotelDestinationJCRPropertiesUsing(city){
    Node destinationNodeFromTajHotels
    def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "1_property":"city",
        "1_property.value":city,
        "2_property":"cq:template",
        "2_property.value":"/conf/tajhotels/settings/wcm/templates/destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        destinationNodeFromTajHotels= hit.node
    }
    return destinationNodeFromTajHotels
}

