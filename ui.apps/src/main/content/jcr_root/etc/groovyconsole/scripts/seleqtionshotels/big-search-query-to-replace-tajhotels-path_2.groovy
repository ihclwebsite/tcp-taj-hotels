package etc.groovyconsole.scripts.seleqtionshotels


SELEQTIONS_REPLACING_TEXT_1="/seleqtions/"

CHECK_INCLUDE_PATH_1="/content/seleqtions/en-in/jcr:content"
CHECK_INCLUDE_PATH_2="/serviceAmenities"

CHECK_INCLUDE_PATH=CHECK_INCLUDE_PATH_2


/*Generic method takes a predicatesMap and resultSizePerPage then returns result*/
def processQueryByPredicates(predicates, queryHitsPerPage){
    def query = createQuery(predicates)
    query.hitsPerPage = queryHitsPerPage
    def result = query.result
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    return result
}

def getAllContentTajhotelsPath(pathLimitedTo, fulltext, startIndex, maxIndex, queryHitsPerPage, isToPermanentlySaved){
    def predicates= queryForSearchText(pathLimitedTo, fulltext)
    def result =processQueryByPredicates(predicates, queryHitsPerPage)
    if(result.totalMatches!=0){
        if(maxIndex==-1)
            maxIndex=result.totalMatches
        processQueryResult(result, startIndex, maxIndex, isToPermanentlySaved)
    }
    return result
}
def processQueryResult(result, startIndex, maxIndex, isToPermanentlySaved){
    result.hits.eachWithIndex { hit , index ->
        if(index>startIndex && index<maxIndex){
            def nodePath=hit.node.path
            def propertyKeyToChange=checkForReferencesToUpdate(nodePath)
             println "Node path is :" +nodePath

            //Add your logic here

        }
    }
}

def prepareData(){
    //Below hotels are list of hotels under seleqtionsBrand
    seleqtionsHotelPaths= new HashSet()
    seleqtionsHotelPaths.add("president-mumbai")
    seleqtionsHotelPaths.add("blue-diamond-pune")
    seleqtionsHotelPaths.add("ambassador-new-delhi")
    seleqtionsHotelPaths.add("fatehabad-road-agra")
    seleqtionsHotelPaths.add("savoy")

    componentsMap= new HashMap()
    componentsMap.put("reference", "path")
    def itemKeyList=Arrays.asList("tabtextlink", "link")
    componentsMap.put("item", itemKeyList)
    def serviceAminitiesKeyList=Arrays.asList("otherConveniences","bedAndBathExperience","hotelFacilities","wellnessAmenities","suiteFeatures","king-size-bed","airport-pick-up","sea-view","thermo-dynamic-showers")
    componentsMap.put("serviceAmenities", serviceAminitiesKeyList)

    // The below map has key:value as oldValue:newValue or oldExistingValue:toBeUpdatedTheNewValue
    replacingMap= new HashMap()
    replacingMap.put("/content/tajhotels/en-in/shared-content", "/content/shared-content/global-shared-content/shared-content")
    replacingMap.put("content/tajhotels", "content/seleqtions")
    replacingMap.put("/taj/",SELEQTIONS_REPLACING_TEXT_1)
    replacingMap.put("/vivanta/",SELEQTIONS_REPLACING_TEXT_1)
    replacingMap.put("/gateway/",SELEQTIONS_REPLACING_TEXT_1)

}

def queryForSearchText(pathLimitedTo, fulltext){
    def predicates = [
            "path":pathLimitedTo,
            "fulltext":fulltext,
            "orderby":"@path",
            "orderby.sort":"desc"
            //   "p.limit":100          //query.hitsPerPage has more priority in this query
    ]
    return predicates
}
prepareData()
// def pathLimitedTo="/content/seleqtions/en-in"
def pathLimitedTo="/content/seleqtions/en-in/our-hotels"
def fulltext="content/tajhotels"
def startIndex=0
def maxIndex=23650  //-1 for all
def queryHitsPerPage=23650      // it is used for query and different from maxIndex
def isToPermanentlySaved=false
// def isToPermanentlySaved=true
getAllContentTajhotelsPath(pathLimitedTo, fulltext, startIndex, maxIndex, queryHitsPerPage,isToPermanentlySaved)



