import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
 
init()
def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/taj-holidays",
        "property":"seoMetaTag",
        "property.operation":"exists",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
    println hit.path
    ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
    if(!modifiableValueMap.get("seoMetaTag").isEmpty()){
        String metaTag =modifiableValueMap.get("seoMetaTag")
        println "updated seoMetaTag == > " +metaTag
        modifiableValueMap.put("seoMetaTag",metaTag)
        hit.getResource().getResourceResolver().commit()
        
        def nodeAtPath = hit.node
        currentNodeActivationStatus = nodeAtPath.get("cq:lastReplicationAction")
        println currentNodeActivationStatus
        if(currentNodeActivationStatus=="Activate") {
           activate(hit.path)
            }
        }
    }
}


/*

post updatation for verifacation


import javax.jcr.Node
import javax.jcr.Workspace;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
 
init()
def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/taj-holidays",
        "property":"seoMetaTag",
        "property.operation":"exists",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
    println hit.path
    ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class);
    if(!modifiableValueMap.get("seoMetaTag").isEmpty()){
        String metaTag =modifiableValueMap.get("seoMetaTag");
        println "updated seoMetaTag == > " +metaTag 
        }
    }
}



*/