package tajhotels

getPathForAllHotels()



def getPathForAllHotels() {
    
    hotelNameAndPathMap = new HashMap<>()
    holidayHotelAndNewHotel = new HashMap<>()
    
    holidayHotelAndNewHotel.put('meghauli-serai--a-taj-safari-lodge','meghauli-serai-chitwan-national-park')
    holidayHotelAndNewHotel.put('hari-mahal-jodhpur','taj-hari-mahal-jodhpur')
    holidayHotelAndNewHotel.put('umaid-bhawan-palace','umaid-bhawan-palace-jodhpur')
    holidayHotelAndNewHotel.put('ganges-varanasi','taj-ganges')
    holidayHotelAndNewHotel.put('taj-usha-kiran-palace','taj-usha-kiran-palace-gwalior')
    holidayHotelAndNewHotel.put('gomti-nagar-lucknow','taj-mahal-lucknow')
    holidayHotelAndNewHotel.put('the-gateway-resort-corbett','taj-corbett-uttarakhand')
    
    
    def predicates = [
      "path": "/content/tajhotels/en-in/our-hotels/",
      "property": "sling:resourceType",
      "property.value": "tajhotels/components/structure/hotels-landing-page",
      "p.limit":"-1"
     ]
     def query = createQuery(predicates)
     def result = query.result
    
     result.hits.each { hit ->
      //println "hit path = " + hit.node.path
      currentNodePath3 = hit.node.path
      
      currentPathArr = currentNodePath3.split("/")
      //println 'currentPathArr = ' + currentPathArr[7];
      hotelNameAndPathMap.put(currentPathArr[7], currentNodePath3.replace('/jcr:content', ''))
      
      
      
     }
     
     //println 'hotelNameAndPathMap - ' + hotelNameAndPathMap;
     
     getAllHolidaysOffer(hotelNameAndPathMap)
}







def getAllHolidaysOffer(hotelNameAndPathMap1) {
    
    //println 'new value - ' + hotelNameAndPathMap1.get('taj-mahal-palace-mumbai');
    count = 0
    def predicates = [
      "path": "/content/tajhotels/en-in/taj-holidays/destinations/",
      "property": "sling:resourceType",
      "property.value": "tajhotels/components/structure/tajholidays-destination-landing-page",
      "p.limit":"-1"
     ]
     def query = createQuery(predicates)
     def result = query.result
    
     result.hits.each { hit ->
      //println "hit path = " + hit.node.path
      currentNodePath = hit.node.path
      
      getAllOffersInDestination(currentNodePath, hotelNameAndPathMap1)
      
     }
}

def getAllOffersInDestination(currentNodePathLocal, hotelNameAndPathMap2) {
    
    
    
    if(currentNodePathLocal.contains('-packages')) {
        currentNodePathLocal = currentNodePathLocal.replace('/jcr:content','')
        
        //println 'currentNodePathLocal - ' + currentNodePathLocal;
        
        getOffersInPackage(currentNodePathLocal, hotelNameAndPathMap2)
        
    }
    
}


def getOffersInPackage(packagePath, hotelNameAndPathMap3) {
    
    def predicates = [
      "path": packagePath,
      "property": "sling:resourceType",
      "property.value": "tajhotels/components/structure/tajholidays-base-page",
      "p.limit":"-1"
     ]
     def query1 = createQuery(predicates)
     def result1 = query1.result
    
     result1.hits.each { hit1 ->
     count++
      //println "hit path = " + hit1.node.path
      currentNodePath1 = hit1.node.path
      
      getOfferAndHotelNameInPath(currentNodePath1, hotelNameAndPathMap3)
      
     }
    
    
}

def getOfferAndHotelNameInPath(pathForOfferAndHotel, hotelNameAndPathMap4) {
    
    pathArr = pathForOfferAndHotel.split("/")
    
    //println 'hotel name - ' + pathArr[8];
    //println 'offer name - ' + pathArr[9];
    
    hotelNamFromArr = hotelNameAndPathMap4.get(pathArr[8])
    
    if(hotelNamFromArr == null || hotelNamFromArr == "") {
        hotelNamFromArr = hotelNameAndPathMap4.get(holidayHotelAndNewHotel.get(pathArr[8]))
    }
    
    pathToCreateNewOffer = hotelNamFromArr + '/offers-and-promotions/' + pathArr[9]
    
    //println 'pathToTakeOfferFrom - ' + pathForOfferAndHotel
    //println 'pathToCreateNewOffer - ' + pathToCreateNewOffer;
    
    createHolidayOfferNodeInHotel(pathForOfferAndHotel, pathToCreateNewOffer)
    
}

def createHolidayOfferNodeInHotel(pathForOfferAndHotel1, pathToCreateNewOffer1) {
    
    
    pathForOfferAndHotelNew = pathForOfferAndHotel1.replace('/jcr:content','')
    
    
    
    removeIfExists(pathToCreateNewOffer1)
    
    Workspace workspace = session.getWorkspace()
    workspace.copy(pathForOfferAndHotelNew, pathToCreateNewOffer1)
    
    println("offer node copied successfully at path : " + pathToCreateNewOffer1)
    
    addAndUpdateNodes(pathForOfferAndHotel1, pathToCreateNewOffer1)
    
    try {
      jcrContentNodeForDestOffer = getNode(pathToCreateNewOffer1 + '/jcr:content')
      currentActiveStauts = jcrContentNodeForDestOffer.get('cq:lastReplicationAction')
	  if(currentActiveStauts == "Activate") {
	  	activate(pathToCreateNewOffer1)
	  }
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + nodePathForCheck)
    }
}

def removeIfExists(nodePathForCheck) {
    try {
      offerNodeInHotel = getNode(nodePathForCheck)
      println 'this offer is available at path = ' + offerNodeInHotel.path
      deactivate(nodePathForCheck)
      offerNodeInHotel.remove()
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + nodePathForCheck)
    }
}

def addAndUpdateNodes(pathForOfferAndHotel2, pathToCreateNewOffer2) {
    
    hotelPathFromOfferArr = pathToCreateNewOffer2.split('/')
    
    hotelPathFromOffer = hotelPathFromOfferArr[0] + "/" + hotelPathFromOfferArr[1] + "/" + hotelPathFromOfferArr[2] + "/" + hotelPathFromOfferArr[3] + "/" +
                     hotelPathFromOfferArr[4] + "/" + hotelPathFromOfferArr[5] + "/" + hotelPathFromOfferArr[6] + "/" + hotelPathFromOfferArr[7]
    
    try {
      jcrContentNode = getNode(pathToCreateNewOffer2 + '/jcr:content')
      println 'this offer is available at path = ' + jcrContentNode.path
      jcrContentNode.setProperty("cq:template","/conf/tajhotels/settings/wcm/templates/hotel-specific-offer-details")
      jcrContentNode.setProperty("sling:resourceType","tajhotels/components/structure/hotel-specific-offer-details-page")
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content')
    }
    
    holidaysHotelPackagePath = pathForOfferAndHotel2 + '/root/holidays_hotel_package'
    
    
    
    try {
      packageNode1 = getNode(pathToCreateNewOffer2 + '/jcr:content/root/holidays_hotel_package')
      println 'this offer is available at path = ' + packageNode1.path
      packageNode1.remove()
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/root/holidays_hotel_package')
    }
    
    try {
      bannerParsys = getNode(pathToCreateNewOffer2 + '/jcr:content/banner_parsys')
      println 'this offer is available at path = ' + bannerParsys.path
      bannerParsys.remove()
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/banner_parsys')
    }
    
    try {
      headerParsys = getNode(pathToCreateNewOffer2 + '/jcr:content/header_parsys')
      println 'this offer is available at path = ' + headerParsys.path
      headerParsys.remove()
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/header_parsys')
    }
    
    try {
      footerParsys = getNode(pathToCreateNewOffer2 + '/jcr:content/footer_parsys')
      println 'this offer is available at path = ' + footerParsys.path
      footerParsys.remove()
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/footer_parsys')
    }
    
    Workspace workspace = session.getWorkspace()
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/root/breadcrumb', pathToCreateNewOffer2 + '/jcr:content/root/breadcrumb')
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/root/reference', pathToCreateNewOffer2 + '/jcr:content/root/reference')
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/root/reference', pathToCreateNewOffer2 + '/jcr:content/root/reference_holidays')
    
    //println("offer node copied successfully at path : " + pathToCreateNewOffer1);
    
    try {
      referenceNode = getNode(pathToCreateNewOffer2 + '/jcr:content/root/reference')
      println 'this offer is available at path = ' + referenceNode.path
      referenceNode.setProperty("path",hotelPathFromOffer + '/jcr:content/root/hotel_navigation')
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/root/reference')
    }
    
    try {
      referenceHolidaysNode = getNode(pathToCreateNewOffer2 + '/jcr:content/root/reference_holidays')
      println 'this offer is available at path = ' + referenceHolidaysNode.path
      referenceHolidaysNode.setProperty("path",holidaysHotelPackagePath)
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/root/reference_holidays')
    }
    
    
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/header_parsys', pathToCreateNewOffer2 + '/jcr:content/header_parsys')
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/footer_parsys', pathToCreateNewOffer2 + '/jcr:content/footer_parsys')
    workspace.copy('/content/tajhotels/example/breakfast-inclusive-rate-for-clone/jcr:content/banner_parsys', pathToCreateNewOffer2 + '/jcr:content/banner_parsys')
    
    
    try {
      bannerParsysReferenceNode = getNode(pathToCreateNewOffer2 + '/jcr:content/banner_parsys/reference')
      println 'this offer is available at path = ' + bannerParsysReferenceNode.path
      bannerParsysReferenceNode.setProperty("path", hotelPathFromOffer + '/jcr:content/banner_parsys/hotel_banner')
      save()
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path for offer - " + pathToCreateNewOffer2 + '/jcr:content/root/reference_holidays')
    }
    
    
}






//println 'count - ' + count;