/*This method is used to Query the JCR and find results as per the Query.*/
 def buildQuery(page, term) {
 def queryManager = session.workspace.queryManager
 def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
 queryManager.createQuery(statement, 'sql')
 }
 
 /*Defined Content Hierarchy */
 final def page = getPage('/content/seleqtions/en-in/our-hotels/')
 /*Component ResourceType which is searched in the content hierarchy */
 final def query = buildQuery(page, 'tajhotels/components/content/specific-hotel-offer-carousel')
 final def result = query.execute()
 count = 0
 result.nodes.each { node ->
 String nodePath = node.path
 /*Put the check for the path while deleting the code*/
 if(nodePath.contains('/restaurants/') ){
    count ++
    println 'deleting--'+nodePath
    node.remove()
 /* Save this session if you are sure the correct nodes are being deleted. Once the session is saved the nodes couldn't be retrieved back.*/
    session.save()
    }
 }
 println 'No Of component found :' + result.nodes.size()
 println 'Number of Component Deleted: ' + count