package tajhotels
import java.util.*

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import java.io.*

class GroovyExcelParser {
    //http://poi.apache.org/spreadsheet/quick-guide.html#Iterator

    def parse(path, sheetNo) {
        InputStream inp = new FileInputStream(path)
        Workbook wb = WorkbookFactory.create(inp)
        Sheet sheet = wb.getSheetAt(sheetNo)

        Iterator<Row> rowIt = sheet.rowIterator()
        Row row = rowIt.next()
        def headers = getRowData(row)

        def rows = []
        while(rowIt.hasNext()) {
            row = rowIt.next()
            rows << getRowData(row)
        }
        [headers, rows]
    }

    def getRowData(Row row) {
        def data = []
        for (Cell cell : row) {
            getValue(row, cell, data)
        }
        data
    }

    def getRowReference(Row row, Cell cell) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        CellReference ref = new CellReference(rowIndex, colIndex)
        ref.getRichStringCellValue().getString()
    }

    def getValue(Row row, Cell cell, List data) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        def value = ""
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString()
                break
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    value = cell.getDateCellValue()
                } else {
                    value = cell.getNumericCellValue()
                }
                break
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue()
                break
            case Cell.CELL_TYPE_FORMULA:
                value = cell.getCellFormula()
                break
            default:
                value = ""
        }
        data[colIndex] = value
        data
    }
}
set=new HashSet()
map3=new HashMap()
map= new HashMap()
map2= new HashMap()
setWrongHotelNames= new HashSet()
def toProcess(header, row, rowsIndex) {
    if(rowsIndex<520){
        def pathOurHotels="/content/tajhotels/en-in/our-hotels"
        def limit3=100
        // def resourceValueOfferPage="tajhotels/components/structure/all-offers-page"
        def resourceValueOfferPage="tajhotels/components/structure/hotels-landing-page"
        def resourceValueOfferDetailsPage="tajhotels/components/structure/hotel-specific-offer-details-page"
        def codeKeyInJcr1="offerRateCode"
        def codeKeyInJcr2="accessCode"
        def codeKeyInJcr3="offer"
        def hotelOffersPath=""
        def resultpath=""
        def rateCodeFromJcr=""

        def hotelName=""
        // println "Row row size : "+rowsIndex
        // println "Row row : "+row
        def hotelNameFromExcel=row[0]+""
        def offerNameFromExcel=row[1]+""
        def newOfferNameFromExcel=row[2]+""
        def wrongOfferCodeFromExcel=row[3]+""
        def correctOfferCodeFromExcel=row[4]+""
        if(hotelNameFromExcel!=null){
            if(hotelNameFromExcel.contains(",")){
                hotelName=hotelNameFromExcel.trim().split(",")[0]
            }else{
                hotelName=hotelNameFromExcel.trim()
            }
            // println "hotelName at start :"+hotelName
            if(map.containsKey(hotelName)){
                hotelOffersPath=map.get(hotelName)
                // println "Hotel Promotion Path contains 01: "+hotelOffersPath;
            }else if(map2.containsKey(hotelNameFromExcel.trim())){
                hotelOffersPath=map2.get(hotelNameFromExcel.trim())
                // println "Hotel Promotion Path contains in map 02: "+hotelOffersPath;
            }else{
                // println "Hotel Promotion Path not contains in map 03: "+hotelName;
                hotelOffersPath=getAllOfferDetailsPages2(pathOurHotels, limit3, hotelName, resourceValueOfferPage, false)
                // println "Hotel Promotion Path from Query 04: "+hotelOffersPath;
                if(hotelOffersPath!=""){
                    map.put(hotelName, hotelOffersPath)
                }else{
                    hotelName=hotelNameFromExcel.trim()
                    if(!setWrongHotelNames.contains(hotelName)){
                        // println "Hotel Promotion Path cant find from 1st Query 05: "+hotelName;
                        hotelOffersPath=getAllOfferDetailsPages2(pathOurHotels, limit3, hotelName, resourceValueOfferPage, false)
                        // println "Hotel Promotion Path from Query 06: "+hotelOffersPath;
                        if(hotelOffersPath!=""){
                            map2.put(hotelName, hotelOffersPath)
                        }else{
                            // println "Wrong hotel name :"+hotelName
                            setWrongHotelNames.add(hotelName)
                        }
                    }else{
                        // println "Never come to this block : "+hotelName
                    }
                }
            }

            // println "hotelName >> path after query :"+hotelName+" >> "+hotelOffersPath;
            // println "Offer Name : "+offerNameFromExcel;
            if(hotelOffersPath!=null && hotelOffersPath!=""){
                def hotelPathWithoutJcr=hotelOffersPath.replace("/jcr:content","")
                // println "hotelPathWithoutJcr :"+ hotelPathWithoutJcr
                resultpath=getAllOfferDetailsPages2(hotelPathWithoutJcr, limit3, offerNameFromExcel, resourceValueOfferDetailsPage, true)
                // println "Specific Offer Path : "+resultpath;
                if(resultpath!=""&& !set.contains(resultpath)){
                    def matchingStatus=""
                    set.add(resultpath)
                    rateCodeFromJcr=getNode(resultpath).get(codeKeyInJcr1)
                    if(rateCodeFromJcr==correctOfferCodeFromExcel){
                        matchingStatus= "Correct"
                    }else if(correctOfferCodeFromExcel=="NA"){
                        matchingStatus= "NA in sheet"
                    }else{
                        matchingStatus= "Incorrect"
                    }
                    println "\t $matchingStatus >>>> $resultpath \t >>>> \t $rateCodeFromJcr >>>>  \t \t $correctOfferCodeFromExcel "
                }else{
                    // println "$hotelOffersPath >>>> $offerNameFromExcel" // offer details page not found
                }
            }
        }
        // println "Row values : "+row[0]+", "+row[1]+", "+row[3]+ ", "+row[4]

    }else if(rowsIndex==520){
        println "Wrong Hotel list : "+setWrongHotelNames
    }else{
        //skipping empty rows
    }
}


def startReadingExcel(excelfilename, sheetNumber){
    def sheetNoToRead=sheetNumber-1
    // println "File name : "+excelfilename+"\n---";
    GroovyExcelParser parser = new GroovyExcelParser()
    def (headers, rows) = parser.parse(excelfilename, sheetNoToRead)
    headers.each { header ->
    }
    // println "row size : "+rows.size()
    rows.eachWithIndex { row, rowsIndex ->
        toProcess(headers, row, rowsIndex)
    }
}
def toExecuteEach(headerName, rowElementForHeaderName) {
    println "$headerName : $rowElementForHeaderName"
}

def excelfilename = 'C:\\Users\\Srikanta\\Downloads\\Prod offfer.xlsx'
startReadingExcel(excelfilename, 1)

def getAllOfferDetailsPages2(path, queryResultLimitedTo, titlePropertyValue, resourceTypePropertyValue, isForOfferDetailsPage){
    def resultPath=""
    def predicates = [
            "path":path,
            "1_property":"jcr:title",
            "1_property.value":titlePropertyValue,
            "2_property":"sling:resourceType",
            "2_property.value":resourceTypePropertyValue,
            //   "1_property.operation":"like",
            "p.limit":-1
    ]
    def query = createQuery(predicates)
    query.hitsPerPage = queryResultLimitedTo
    def result = query.result
//   println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--";
    resultPath= processQueryResult(result, isForOfferDetailsPage)
    return resultPath
}

def processQueryResult(result, isForOfferDetailsPage){
    def resultPath=""
    def prefixString=""
    def statusString=""
    if(isForOfferDetailsPage){
        prefixString="For Specific Offer -->>"
    }else{
        prefixString="For Hotel  -->>"
    }
    if(result.totalMatches==1){
        statusString= "$prefixString Single result found"
    }else if(result.totalMatches==0){
        statusString= "$prefixString No result found"
    }else{
        statusString= "$prefixString Multiple result found"
    }
    if(isForOfferDetailsPage){
        // println "$statusString"  //log only for OfferDetails pages
    }else{
        // println "$statusString"  //log only for Hotel pages
    }
    // println "$statusString"  //log for both
    result.hits.each { hit ->
        resultPath = hit.node.path
        //   println "Hit path : "+isForOfferDetailsPage+"-"+resultPath ;
        if(isForOfferDetailsPage){
            //   println "Offer code : "+hit.node.get("offerRateCode") ;
        }
    }
    // println "Hits Returning  Path :"+isForOfferDetailsPage+"-"+ resultPath
    return resultPath
}

