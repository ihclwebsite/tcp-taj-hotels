package etc.groovyconsole.scripts.tajhotels


executeDamContentRisk()


def executeDamContentRisk(){
    
    def exclusiveText="* Rates Exclusive of Taxes"
    def inclusiveText="* Rates Inclusive of Taxes"
    
    def predicates = [
        "path":"/content/tajhotels/en-in",
        "1_property":"sling:resourceType",
        "1_property.value":"tajhotels/components/content/dining-details"
    ]
    
    def query = createQuery(predicates)
    
    query.hitsPerPage = 450
    
    def result = query.result
    
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    def taxDisclaimer=''
    
    result.hits.each { hit ->
        println "hit path = ${hit.node.path}"
        if(hit.node.path.contains("london")){
            taxDisclaimer=inclusiveText
        }else{
            taxDisclaimer=exclusiveText
        }
        hit.node.set("taxDisclaimer", taxDisclaimer)
        save()

        println "Updated Node as : "+hit.node.get("taxDisclaimer")

    }
}
