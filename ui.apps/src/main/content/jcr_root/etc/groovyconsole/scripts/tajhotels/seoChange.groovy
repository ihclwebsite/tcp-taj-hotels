package etc.groovyconsole.scripts.tajhotels

import com.day.cq.dam.api.Asset



filename = "/content/dam/tajhotels/others/CSV_Taj Hotels - New Website Manual Optimization Recommendations-Approved.csv"
text = executeDamContentRisk()



//Execute after getting DAM file node 
def getDamAssetFromNode(node){
    Resource res = resourceResolver.getResource(node.path)
    Asset asset= res.adaptTo(Asset.class)
    r=asset.getOriginal()
    text = r.getStream().text
    def lines = text.readLines()
    lineIndex= 1
    for (line in lines) {
        //println "DAM2"+line
        executeOneLine(line)
        lineIndex++
    }
    return text
}
 
//for each line of the text until \n is reached
def executeOneLine(line){
    //println "line  : "+line
    //lineindex is 3 for start at global and lineindex is 9 for start at about-dest [total lineindex 146    hotels starts at 78]
    if(checkLineIsEmpty(line) && lineIndex>3 && lineIndex <146){
    count=1
        lineArr=line.split(",")
        firstUrl= lineArr[1]
        line2= line
        println "line index : "+lineIndex
        //println "line 2 : "+line2
        line2=line2.replace(","+firstUrl+",", "\"")
        if(line2.contains(",\"")){
            line2=line2.replace(",\"", "\",\"")
        }
        if(line2.contains("\",")){
            line2=line2.replace("\",", "\",\"")
        }
        if(line2.charAt(0)=="\"" && line2.charAt(1)=="\""){
            line2=line2.substring(1)
        }
        //println "line 22 : "+line2
        lineArr2=line2.split("\",\"")
        //println "lARR2"+lineArr2
        firstUrl=modifyLoc(firstUrl)
        
        println "URL : "+firstUrl
         
        secondPageTitle=lineArr2[0].replace("\"", "").replace("Ã¢â¬â¢","'")
        thirdPageTitle=lineArr2[1].replace("\"", "").replace("Ã¢â¬â¢","'")
        forthPageTitle=lineArr2[2].replace("\"", "").replace("Ã¢â¬â¢","'")
       
        println "Page Title : "+secondPageTitle
        println "Meta Desc: "+thirdPageTitle
        println "Meta Key: "+forthPageTitle
         
        //executeStringArray(lineArr2, firstUrl)
        
        //updateSeoProperty(firstUrl, secondPageTitle, thirdPageTitle, forthPageTitle)
    }
}
        //updateSeoProperty(locPath)



def updateSeoProperty(locPath, pageTitle, metaDesc, metaKeywords){
    
    homeTemplate="/conf/tajhotels/settings/wcm/templates/home-page"
    homePageType= "tajhotels/components/structure/home-page"
    
    meetingTemplate="/conf/tajhotels/settings/wcm/templates/taj-hotel-meetings-global-page-template"
    meetingPageType="tajhotels/components/structure/meetings-events-list-global-page"
    
    ourHotelsTemplate="/conf/tajhotels/settings/wcm/templates/listing-page"
    ourHotelsPageType="tajhotels/components/structure/hotels-listing-page"
    
    restaturantsTemplate="/conf/tajhotels/settings/wcm/templates/dining-list-global"
    restaurantsPageType="tajhotels/components/structure/dining-list-global-page"
    
    jivaSpaTemplate="/conf/tajhotels/settings/wcm/templates/jiva-spa-list-global"
    jivaSpaPageType="tajhotels/components/structure/jiva-spa-list-global-page"
    
    offersTemplate="/conf/tajhotels/settings/wcm/templates/all-offers-global"
    OffersPageType="tajhotels/components/structure/all-offers-global-page"
    
    hotelsInTemplate="/conf/tajhotels/settings/wcm/templates/destination-landing-page"
    hotelsInPageType="tajhotels/components/structure/destination-landing-page"
    
    aboutDestTemplate="/conf/tajhotels/settings/wcm/templates/about-destination-template"
    aboutPageType="tajhotels/components/structure/about-destination-page"
    
    def tempArr = [homeTemplate, meetingTemplate, ourHotelsTemplate, restaturantsTemplate, jivaSpaTemplate, offersTemplate, hotelsInTemplate, aboutDestTemplate]
    def pageTypeArr=[homePageType, meetingPageType, ourHotelsPageType, restaurantsPageType, jivaSpaPageType, OffersPageType, hotelsInPageType, aboutPageType]
    
    //Please change the arrIndexC according to the above array min=0 for Home page max=7 fir aboutDestTemplate
    arrIndexC=9
    templateNameValue= tempArr[arrIndexC]
    pageSuperTypeValue= pageTypeArr[arrIndexC]
    
    
   def predicates = [
        "path" : locPath,
        "jcr:primaryType" : "cq:PageContent",
        "1_property": "cq:template",
        "1_property.value" : templateNameValue,
        "2_property" : "sling:resourceType",
        "2_property.value" : pageSuperTypeValue
    ]
    
    def query = createQuery(predicates)
    
    //query.hitsPerPage = 10
    
    def result = query.result
    
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s"
    
    result.hits.each { hit ->
        println "hit path = ${hit.node.path}"
        println "hit pageTitle = ${hit.node.pageTitle} :: "+pageTitle
        println "hit seoDescription = ${hit.node.seoDescription} :: "+metaDesc
        println "hit seoKeywords = ${hit.node.seoKeywords} :: "+metaKeywords
        if(hit.node.pageTitle == pageTitle && hit.node.seoDescription==metaDesc && hit.node.seoKeywords==metaKeywords){
            println "Successfuly completed "
        }
        println "line index 2 : "+lineIndex
       if(((hit.node.get("cq:template")).equals(templateNameValue)) && hit.node.get("cq:template").contains("about-") && (lineIndex>9 && lineIndex <78)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated About"
         }
         else if(((hit.node.get("cq:template")).equals(templateNameValue)) && !(hit.node.get("cq:template").contains("about-")) && (lineIndex>78 && lineIndex <146)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Hotels"
         }else if(((hit.node.path).endsWith("/en-in/jcr:content")) && (lineIndex>3 && lineIndex <10)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global Home pages"
         }else if(((hit.node.path).endsWith("/en-in/our-hotels/jcr:content")) && (lineIndex==5)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global Our Hotels pages"
         }else if(((hit.node.path).endsWith("/en-in/restaurants/jcr:content")) && (lineIndex==6)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global restaurants pages"
         }else if(((hit.node.path).endsWith("/en-in/meetings/jcr:content")) && (lineIndex==7)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global meetings pages"
         }else if(((hit.node.path).endsWith("/en-in/jiva-spa/jcr:content")) && (lineIndex==8)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global jiva-spa pages"
         }else if(((hit.node.path).endsWith("/en-in/offers/jcr:content")) && (lineIndex==9)){
            hit.node.set('pageTitle',pageTitle)
           hit.node.set('seoDescription',metaDesc)
           hit.node.set('seoKeywords',metaKeywords)
           println "Just Updated Global offers pages"
         }
    }
    println "--------------------------------"
    save()
    
}


def modifyLoc(firstUrl){
    
    if(firstUrl.contains("/en-in/")){
         firstUrl=firstUrl.replace("https://www.tajhotels.com", "/content/tajhotels")
         if(firstUrl=="/content/tajhotels/en-in/events/"){
             firstUrl=firstUrl.replace("/content/tajhotels/en-in/events", "/content/tajhotels/en-in/meetings")
         }else if(firstUrl.contains("/content/tajhotels/en-in/about-") || firstUrl.contains("/content/tajhotels/en-in/our-hotels/hotels-in-")){
             destName= firstUrl.split("/")
             destName2=destName[destName.length-1]
             if(firstUrl.contains("/content/tajhotels/en-in/about-")){
                 destName2=destName2.replace("about-", "")
                firstUrl=firstUrl.replace("/content/tajhotels/en-in/about-","/content/tajhotels/en-in/our-hotels/hotels-in-"+destName2+"/about-")
             }else if(firstUrl.contains("/content/tajhotels/en-in/our-hotels/hotels-in-")){
                 destName2=destName2.replace("hotels-in-", "")
                 firstUrl=firstUrl.replace("/content/tajhotels/en-in/our-hotels/hotels-in-","/content/tajhotels/en-in/our-hotels/hotels-in-")
             }
         }
         
        }else{
            firstUrl=firstUrl.replace("https://www.tajhotels.com", "/content/tajhotels/en-in")
        }
        return firstUrl
}

def executeStringArray(lineArr2, firstUrl){
    lineArr2.eachWithIndex { val, idx ->
   println "${idx}. ${val}"
   //findDataByIndex(idx, firstUrl)
   
    }   
}


def findDataByIndex(idx, firstUrl){
    //println "URL : "+firstUrl //prints double time
    if(idx==1){
   secondPageTitle=lineArr2[1]
        println "PageTitle : "+secondPageTitle
   }else if(idx==3){
       thirdPageTitle=lineArr2[3]
       println "Meta Desc: "+thirdPageTitle
   }else if(idx==5){
       forthPageTitle=lineArr2[5]
       println "Meta Key: "+forthPageTitle
   }
   
}

//If line is empty returns false else true
boolean checkLineIsEmpty(textValue){
    if(textValue=="")
        return false
    if(textValue==null)
        return false
    else 
        return true
}

//Query Hits Logic
def executeContentRisk(){
    
    def predicates = [
        "path": "/content/we-retail",
        "type": "cq:Page",
        "fulltext": "Biking"
    ]
    
    def query = createQuery(predicates)
    
    //query.hitsPerPage = 10
    
    def result = query.result
    
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    
    result.hits.each { hit ->
        println "hit path = ${hit.node.path}"
    }
}


//DAM content access
def executeDamContentRisk(){
    
    def predicates = [
        "path":"/content/dam/tajhotels/others",
        "type":"nt:Asset",
        "1_property":"jcr:primaryType",
        "1_property.value":"dam:Asset"
    ]
    
    def query = createQuery(predicates)
    
    //query.hitsPerPage = 10
    
    def result = query.result
    
    //println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    
    result.hits.each { hit ->
        //println "hit path = ${hit.node.path}"
        return getDamAssetFromNode(hit.node)
    }
}


