def path = "/content/thrp/taj/en-in"
offerPage()

def offerPage() {
    def predicates = [
            "path"          : "/content/thrp/taj/en-in",
            "property"      : "sling:resourceType",
            "property.value": "taj-core/components/offerInclusionList",
            "p.limit"       : "-1"]
    def query = createQuery(predicates)
    def result = query.result

    result.hits.each {
        x ->
        // println x.path
            def nodetemp = x.node
            numLinks = nodetemp.get("numLinks")
            title = nodetemp.get("title")
            description = nodetemp.get("description")
            offerPath = nodetemp.path
            pathArray = offerPath.split("/")
            hotelname = pathArray[5]
            // println "${offerPath}"
            if (hotelname != "offers") {
                offername = pathArray[7]
                // println "${hotelname}" + "," + "${offername}" + "," + "${title}" + "," + "${description}" + "," + "${numLinks}"
                findHotelInNewContent(hotelname, offername, title, description, numLinks)
            }else{
                offername = pathArray[6]
                // findGLobalOffers(offername,title,description,numLinks)
            }
    }
}

def findGLobalOffers(offername,title,description,numLinks){
    def predicates = [
            "path"          : "/content/tajhotels/en-in/offers/"+"${offername}",
            "property"      : "sling:resourceType",
            "property.value": "tajhotels/components/content/offerinclusion",
            "p.limit"       : "-1"
    ]
    def query = createQuery(predicates)
    def result = query.result
    result.hits.each { hit ->
        def nodetemp = hit.node
        // println nodetemp.path
        migrateOfferInclusionData(nodetemp, title, description, numLinks)
    }
}


def findHotelInNewContent(hotelname, offername, title, description, numLinks) {
    def predicates = [
            "path"    : "/content/tajhotels/en-in/our-hotels",
            "nodename": hotelname]
    def query = createQuery(predicates)
    def result = query.result

    result.hits.each { hit ->
        def path = hit.node.path
        // println path
        findOfferinPath(path, offername, title, description, numLinks)
    }
}

def findOfferinPath(path, offername, title, description, numLinks) {
    def predicates = [
            "path"          : path,
            "property"      : "sling:resourceType",
            "property.value": "tajhotels/components/content/offerinclusion",
            "p.limit"       : "-1"]
    def query = createQuery(predicates)
    def result = query.result


    result.hits.each { hit ->
        def nodetemp = hit.node
        // println nodetemp.path
        def name, value
    if (numLinks != "1" && numLinks != "0") {

        def count = title.size
        for (i = 0; i < count; i++) {
            name = title[i].toLowerCase().trim()
            value = description[i]
            // println "${name}" + " , " + "${value}"
            // Uncomment to set the values
            if(name && value){
                if(value.contains("<p>&nbsp;</p>")){
                value = value.replaceAll("<p>&nbsp;</p>","")
                println nodetemp.path
                println value
                }
            nodetemp.set(name, value)
            nodetemp.getSession().save()
            }
        }
    } else {
        if (title) {
            // Uncomment to set the values
            title = title.toLowerCase().trim()
            if(description.contains("<p>&nbsp;</p>")){
                description = description.replaceAll("<p>&nbsp;</p>","")
                println nodetemp.path
                println description
                
                }
            nodetemp.set(title, description)
            nodetemp.getSession().save()
            // println "${title}" + " , " + " ${description}"
        }
    }
       
    }
}