import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #6 - SectionNavigation-pathSetting script - This script essentially sets the correct path for navigation among the explore , package and about page */

 
i=0
init()
def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/content/destination/section-navigation",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
            i++
            println  i + " : " + hit.node.path
            setNavigationPathsFor(hit.getResource(),hit.node)
    }
}

def setNavigationPathsFor(resource,node) {
    
    String cityName = node.getParent().getParent().getParent().path.replaceAll("/content/tajhotels/en-in/holidays-landing-page/holidays-destination/holidays-in-","")
    String aboutDestinationPath = node.getParent().getParent().getParent().path
    String packageDestinationPath = aboutDestinationPath + "/packages-in-" + cityName
    String exploreDestinationPath = aboutDestinationPath + "/explore-" + cityName
    
    println "City                   :" + cityName
    println "aboutDestinationPath   :" + aboutDestinationPath
    println "packageDestinationPath :" + packageDestinationPath
    println "exploreDestinationPath :" + exploreDestinationPath
    println "==================================================================================================================="
    
    Resource navigationTabsRes = resource.getChild("navigationTabs")
    Iterable<Resource> navTabsChildren = navigationTabsRes.getChildren()
    for (Resource childRes : navTabsChildren) {
        Node childNode = childRes.adaptTo(Node.class)
        ModifiableValueMap modifiableValueMap = childRes.adaptTo(ModifiableValueMap.class)
        String key =childNode.get("tabtext")
        switch (key) {
        case "About":
            modifiableValueMap.put("tabtextlink",aboutDestinationPath)
            childRes.getResourceResolver().commit()
            break
        case "Packages":
            modifiableValueMap.put("tabtextlink",packageDestinationPath)
            childRes.getResourceResolver().commit()
            break
        case "Explore":
            modifiableValueMap.put("tabtextlink",exploreDestinationPath)
            childRes.getResourceResolver().commit()
            break
        }
    }
}


