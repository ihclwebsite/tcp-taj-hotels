package etc.groovyconsole.scripts.tajhotels

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import java.io.*
import com.day.cq.dam.api.Asset


hotelsUrlMap= new HashMap()
hotelsUrlMapNonMatched= new HashMap()

class GroovyExcelParser {
    //http://poi.apache.org/spreadsheet/quick-guide.html#Iterator

    def parse(path, sheetNo) {
        InputStream inp = new FileInputStream(path)
        parseFromStream(inp, sheetNo)
    }

    def parseFromStream(steramToUse, sheetNo) {
        Workbook wb = WorkbookFactory.create(steramToUse)
        Sheet sheet = wb.getSheetAt(sheetNo)

        Iterator<Row> rowIt = sheet.rowIterator()
        Row row = rowIt.next()
        def headers = getRowData(row)

        def rows = []
        while(rowIt.hasNext()) {
            row = rowIt.next()
            rows << getRowData(row)
        }
        [headers, rows]
    }

    def getRowData(Row row) {
        def data = []
        for (Cell cell : row) {
            getValue(row, cell, data)
        }
        data
    }

    def getRowReference(Row row, Cell cell) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        CellReference ref = new CellReference(rowIndex, colIndex)
        ref.getRichStringCellValue().getString()
    }

    def getValue(Row row, Cell cell, List data) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        def value = ""
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString()
                break
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    value = cell.getDateCellValue()
                } else {
                    value = cell.getNumericCellValue()
                }
                break
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue()
                break
            case Cell.CELL_TYPE_FORMULA:
                value = cell.getCellFormula()
                break
            default:
                value = ""
        }
        data[colIndex] = value
        data
    }

    def toProcessLocally(header, row, rowsIndex) {
        println "Row Index : "+rowsIndex
        def obj=""
        row.eachWithIndex { datum, i ->
            def headerName =header[i]
            // obj += "<$headerName>$datum</$headerName>\n"
            toExecuteEach("$headerName", "$datum")
        }
        obj += ""
    }
}// End of Class

def toProcess(header, row, rowsIndex) {
    def minRange=70
    def maxRange=140       //here max is 140
    if(rowsIndex>=minRange && rowsIndex<=maxRange ){

        // println "Row Index: "+rowsIndex;
        def excelPagePath=row[0]
        def excelPageTitle=row[1]
        def excelSeoDescription=row[2]
        def excelSeoKeywords=row[3]

        // println "$headerName : $rowElementForHeaderName";
        // def urlLinkWithoutMap=excelPagePath.replace("https://beta.tajhotels.com", "http://localhost:4502")
        // println "urlLinkWithoutMap : $urlLinkWithoutMap"
        // def mappedUrlInsideToOutside=resourceResolver.map("/content/tajhotels/en-in.html"); //returns "http://localhost:4502/en-in"
        // def anUri=new URI("http://localhost:4502/en-in").getPath();
        // def mappedUrlOutsideToInside=resourceResolver.resolve("https://beta.tajhotels.com/en-in/gateway/fatehabad-road-agra");   // this doesn't work
        // println "Mapped URL :"+mappedUrlOutsideToInside;

        fillHotelsUrlMapNonMatched()
        def resultFromQuery=kindlyResolveUrlToNodeLevel(excelPagePath)
        // println "resultFromQuery : "+resultFromQuery
        if(resultFromQuery!="NA"){
            if(resultFromQuery.endsWith("/")){
                resultFromQuery=resultFromQuery+"jcr:content"
            }
            else if(resultFromQuery.endsWith("/jcr:content")){

            }else {
                resultFromQuery=resultFromQuery+"/jcr:content"
            }
            def resultNode
            try{
                resultNode=getNode(resultFromQuery)
                if(resultNode!=null){
                    println "pageTitle : $excelPageTitle, seoDescription : $excelSeoDescription, seoKeywords : $excelSeoKeywords"
                    //     resultNode.set("pageTitle", excelPageTitle);
                    //     resultNode.set("seoDescription", excelSeoDescription);
                    //     resultNode.set("seoKeywords", excelSeoKeywords);
                    // resultNode.getSession().save()
                    // activate(resultFromQuery.replace("/jcr:content",""))

                    def pageTitle= resultNode.get("pageTitle")
                    def seoDescription=resultNode.get("seoDescription")
                    def seoKeywords=resultNode.get("seoKeywords")
                    def seoMetaTag=resultNode.get("seoMetaTag")


                    println "pageTitle : $pageTitle, seoDescription : $seoDescription, seoKeywords : $seoKeywords , seoMetaTag : $seoMetaTag"
                    if(pageTitle==excelPageTitle){
                        println "Same Title"
                    }else{
                        println "Not Same Title"
                    }
                    if(seoDescription==excelSeoDescription){
                        println "Same Seo Description"
                    }else{
                        println "Not Same Description"
                    }
                    if(seoKeywords==excelSeoKeywords){
                        println "Same seoKeywords"
                    }else {
                        println "Not Same seoKeywords "
                    }
                }
            }catch(e){
                println "Node not found 3: $resultFromQuery --"+e
            }

        }else{
            println "resultFromQuery in else : $resultFromQuery"
        }
    }
}

def toExecuteEach(headerName, rowElementForHeaderName) {
    // println "$headerName : $rowElementForHeaderName";


}
def fillHotelsUrlMapNonMatched(){
    hotelsUrlMapNonMatched.put("fishermans-cove-chennai", "/content/tajhotels/en-in/our-hotels/hotels-in-chennai/taj/taj-fishermans-cove-chennai")
    hotelsUrlMapNonMatched.put("pamodzi-lusaka", "/content/tajhotels/en-in/our-hotels/hotels-in-lusaka/taj/taj-pamodzi-lusaka")
    hotelsUrlMapNonMatched.put("deccan", "/content/tajhotels/en-in/our-hotels/hotels-in-hyderabad/taj/deccan")


}
def kindlyResolveUrlToNodeLevel(rowElementForHeaderName){
    println 'rowElementForHeaderName -> ' + rowElementForHeaderName
    def resultPathToReturn="NA"
    // println "Incoming Path : $rowElementForHeaderName";
    def pathWithoutHostName=rowElementForHeaderName.replace("https:/beta.tajhotels.com","").replace("https://beta.tajhotels.com", "")
    // println "pathWithoutHostName : "+pathWithoutHostName
    def pathArr1=pathWithoutHostName.split("/")
    def pathArr2
    if(pathArr1.size()>3){
        def enIn=pathArr1[1]
        def brandName=pathArr1[2]
        def hotelName=pathArr1[3]
        pathArr2=pathWithoutHostName.split(hotelName)
        // println "pathArr1 : "+pathArr1[3]
        if(brandName=="taj"||brandName=="vivanta"||brandName=="gateway" ){
            // println "pathArr1 : "+pathArr1[2]
            resultPathToReturn= findHotelNodePathFromExternalPath(hotelName)
            resultPathToReturn=resultPathToReturn+pathArr2[1]
            println "resultPathToReturn : $resultPathToReturn"
        }else if( brandName=="about-us" || brandName=="offers"){
            resultPathToReturn="/content/tajhotels"+pathWithoutHostName
            // println "pathArr skipping : "+resultPathToReturn
            try{
                getNode(resultPathToReturn+"jcr:content")
                // resultPathToReturn=resultPathToReturn+pathArr2[1]
            }catch(Exception e){
                println "Node not found 3: $resultPathToReturn --"+e
                resultPathToReturn="NA"
            }
        }else if(brandName=="destination"){
            def destinationPath="/content/tajhotels"+pathWithoutHostName
            def destinationName=""
            if(hotelName.contains("about")){
                destinationName=hotelName.replace("about-", "")
                destinationPath=destinationPath.replace("destination","our-hotels/hotels-in-"+destinationName)
            }
            resultPathToReturn=destinationPath
            resultPathToReturn=resultPathToReturn+pathArr2[1]
            resultPathToReturn=resultPathToReturn.replace("//", "/")
            // println "resultPathToReturn : $resultPathToReturn"
            try{
                getNode(resultPathToReturn)
            }catch(Exception e){
                println "Node not found 2: $resultPathToReturn --"+e
                resultPathToReturn="NA"
            }
        }else{
            println "Never come to this block 3 : $pathWithoutHostName"
        }

    }else{
        // println "skipping : "+ pathArr1
    }
    // println "changedPath : $pathWithoutHostName"
    return resultPathToReturn
}

def findHotelNodePathFromExternalPath(hotelName){
    def resultPath=""
    if(hotelsUrlMap.containsKey(hotelName)){
        resultPath= hotelsUrlMap.get(hotelName)
    }else {
        def ourHotelPath="/content/tajhotels/en-in/our-hotels"
        def predicates = [
                "path" : ourHotelPath,
                "nodename":hotelName,
                "cq:lastReplicationAction":"Activate"
                // "jcr:primaryType" : "cq:PageContent",
                // "1_property": "cq:template",
                // "1_property.value" : templateNameValue,
                // "2_property" : "sling:resourceType",
                // "2_property.value" : pageSuperTypeValue
        ]

        def query = createQuery(predicates)
        //query.hitsPerPage = 10
        def result = query.result
        // println "${result.totalMatches} hits, execution time = ${result.executionTime}s"
        if(result.totalMatches==0){
            println "No result from query :"+hotelName
            if(hotelsUrlMapNonMatched.containsKey(hotelName)){
                return hotelsUrlMapNonMatched.get(hotelName)
            }else{
                println "Not in Non-matching map "
            }

        }else if(result.totalMatches==1){
            println "Single result from query :"+hotelName
        }else if(result.totalMatches>1){
            println "Multiple result from query :"+hotelName
            if(hotelsUrlMapNonMatched.containsKey(hotelName)){
                return hotelsUrlMapNonMatched.get(hotelName)
            }else{
                println "Not in Non-matching map 2 "
            }
        }else{
            println "Never come to this block 001 :"+hotelName
        }
        result.hits.each { hit ->
            println "hit path = ${hit.node.path}"
            hotelsUrlMap.put(hotelName, hit.node.path)
            resultPath= hit.node.path
        }
    }
    return resultPath
}

def startReadingExcel(excelfilename, sheetNumber){
    def isLocal=false
    if(!excelfilename.startsWith("/content/dam/")){
        isLocal=true
    }
    def sheetNoToRead=sheetNumber-1
    if(sheetNoToRead>=0){
        println "File name : "+excelfilename+"\n---"
        GroovyExcelParser parser = new GroovyExcelParser()
        if(!isLocal){
            getDamExcelAssetFromPath(excelfilename, sheetNumber, parser)
        }else if(isLocal){
            def (headers, rows) = parser.parse(excelfilename, sheetNoToRead)
            executeHeadersAndRows(headers, rows)
        }else{
            println "Never come to this Block"
        }
    }else{
        println "Please select a sheet number in positive number(not zero)"
    }
}
def executeHeadersAndRows(headers, rows){
    println 'Headers----'
    headers.each { header ->
        println header
    }
    println "\n  Rows ------------------"
    rows.eachWithIndex { row, rowsIndex ->
        toProcess(headers, row, rowsIndex)
    }
}

//Execute after getting DAM file node
def getDamExcelAssetFromPath(excelNodePath, sheetNumber, parser){
    def sheetNoToRead=sheetNumber-1
    if(sheetNoToRead>=0){
        Resource res = resourceResolver.getResource(excelNodePath)
        Asset asset= res.adaptTo(Asset.class)
        r=asset.getOriginal()
        def stream = r.getStream()
        def (headers, rows) = parser.parseFromStream(stream, sheetNoToRead)
        executeHeadersAndRows(headers, rows)
    }
}

// Please Upload the Excel file in following location before running the script
def filenameInAemServer ="/content/dam/tajhotels/others/Taj Hotels - New Website Pending SEO Tags (1).xlsx"
def excelfilenameInLocalSys = 'C:\\Users\\Srikanta\\Downloads\\Taj Hotels - New Website Pending SEO Tags (1).xlsx'
def sheetNoToRead=1    //sheetNo starts from 1
startReadingExcel(filenameInAemServer, sheetNoToRead)



