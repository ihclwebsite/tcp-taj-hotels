/*groov to change header reference path and footer path for a particular path , Please altert the paths/lines as per your needs before running the script*/


import groovy.json.JsonSlurper
import javax.jcr.Node
import javax.jcr.Workspace;
import org.apache.jackrabbit.commons.JcrUtils;

changeContentRefs()

def changeContentRefs(){
    
    changeHeaderFooterRefsForBrand("/content/ihclcb/en-in/ourhotels-ihclcb/hotels-in-chennai");
    //changeHeaderFooterRefsForBrand("/content/taj-inner-circle/en-in/vivanta-our-hotels/hotels-in-agra");
    //changeHeaderFooterRefsForBrand("/content/taj-inner-circle/en-in/seleqtions-our-hotels/hotels-in-agra");
    println "start here"
    //removeNavigationandOffersForBrand("/content/taj-inner-circle/en-in/taj-our-hotels/hotels-in-agra");
    //removeNavigationandOffersForBrand("/content/taj-inner-circle/en-in/vivanta-our-hotels/hotels-in-agra");
    //removeNavigationandOffersForBrand("/content/taj-inner-circle/en-in/seleqtions-our-hotels/hotels-in-agra");
    
    //changeEtcMappingforNode("/etc/map/http/www.tajinnercircle.com/en-in/taj");
    println "update completed"
}

def changeHeaderFooterRefsForBrand(brandrootPath){
    
    def ihclcbHeaderRef = "/content/ihclcb/en-in/jcr:content/header_parsys";
    //def ticFooterRef = "/content/ihclcb/en-in/jcr:content/footer_parsys";
    
    def predicate = [
        "path": brandrootPath,
        "property":"jcr:primaryType",
        "property.value":"cq:PageContent",
        "p.limit":"-1"
        ]
        
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        println hit.path
        def headerpath=hit.getResource().getChild('header_parsys');
        //def footerpath=hit.getResource().getChild('footer_parsys');
        def headerRefResource = null;
        //def footerRefResource = null;
        if(headerpath != null){
           headerRefResource = headerpath.getChild('reference');
           Node node = headerRefResource.adaptTo(Node.class);
           node.setProperty("path",ihclcbHeaderRef);
           node.setProperty("cq:isCancelledForChildren",true);
           save();
           println headerRefResource.path +" changed"
        }
       /* if(footerpath != null){
            footerRefResource = footerpath.getChild('reference'); 
            Node node = footerRefResource.adaptTo(Node.class);
            node.setProperty("path",ticFooterRef);
            node.setProperty("cq:isCancelledForChildren",true);
            save();
            println footerRefResource.path +" changed"
        }
       */ 
    }
}

//def changeEtcMappingforNode(nodePath){
    
//}
//getNode('/content/taj-inner-circle/en-in/taj-our-hotels/hotels-in-mumbai/restaurants-in-mumbai/jcr:content/header_parsys/reference').getProperty('path').remove()
//save()
