import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"searchKey",
        "property.value":"aboutDestination",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        println hit.path
        String aboutDestinationPath = hit.getResource().getParent().path
        String latitude = hit.getResource().adaptTo(Node.class).get("latitude")
        String longitude = hit.getResource().adaptTo(Node.class).get("longitude")
        println latitude
        println longitude
        inquirePackageAndExplore(aboutDestinationPath,latitude,longitude)
    }
}

def inquirePackageAndExplore( path,latitude,longitude) {
    def predicate = [
        "path": path,
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        String checkStr = hit.path
        if (checkStr !=  path+"/jcr:content") {
            ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
            modifiableValueMap.put("latitude",latitude)
            modifiableValueMap.put("longitude",longitude)
            hit.getResource().getResourceResolver().commit()
            println hit.path
            println latitude
            println longitude
        }
    }
}

