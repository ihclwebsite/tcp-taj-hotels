import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #5 - Explore-destination-rootPathMigration script - This script essentially migrates or sets the destinationRootPath which is the 
AboutHotel page present in the new TajHotelContent, so that Dining and Spa related properties are fetched and built in this ExploreDestination page 
Note  : Manual Intervention is required for couple of destination as there are mismatch in the destination names present in new TajContent
and the existing TajHolidayDestination . ( example  :- bengaluru ,kerala ,rajasthan */

getAllHolidayDestination()

def getAllHolidayDestination(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        if(!hit.node.getParent().path.contains("packages-in") && !hit.node.getParent().path.contains("explore-")) {
            String[] city= hit.node.get("city")
            Node destinationNodeFromTajHotels =getHotelDestinationJCRPropertiesUsing(city)
            if(null!=destinationNodeFromTajHotels) {
                def destinationRootPath=destinationNodeFromTajHotels.getParent().path
                println "destinationRootPath : " + destinationRootPath
                def destinationName= hit.getResource().getParent().path.replaceAll("/content/tajhotels/en-in/holidays-landing-page/holidays-destination/holidays-in-","")
                String exploreDest="explore-"+destinationName
                Resource res= hit.getResource().getParent().getChild(exploreDest)
                if(null!=res){
                    Resource exploreRes = res.getChild("jcr:content").getChild("root").getChild("destination_explore")
                    ModifiableValueMap modifiableValueMap = exploreRes.adaptTo(ModifiableValueMap.class)
                    modifiableValueMap.put("destinationRootPath",destinationRootPath)
                    exploreRes.getResourceResolver().commit()
                }
            }
            else {
                println "mismatch of city parameter under " + hit.node.getParent().getName() 
            }
        }
    }
}


def getHotelDestinationJCRPropertiesUsing(city){
    Node destinationNodeFromTajHotels
    def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "1_property":"city",
        "1_property.value":city,
        "2_property":"cq:template",
        "2_property.value":"/conf/tajhotels/settings/wcm/templates/destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
        destinationNodeFromTajHotels= hit.node
    }
    return destinationNodeFromTajHotels
}

