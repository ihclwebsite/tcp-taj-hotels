package etc.groovyconsole.scripts.tajhotels


/**  Declaration starts here ***/
def queryForSearchText(pathLimitedTo){
    def predicates = [
            "path":pathLimitedTo,
            "type":"cq:ClientLibraryFolder",
            "p.limit":"-1"         //query.hitsPerPage has more priority in this query
    ]
    return predicates
}


/*Generic method takes a predicatesMap and resultSizePerPage then returns result*/
def processQueryByPredicates(predicates){
    def query = createQuery(predicates)
    // query.hitsPerPage = queryHitsPerPage
    def result = query.result
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    return result
}

def processQueryResult(pathLimitedTo, isToPermanentlySaved){
    def predicates= queryForSearchText(pathLimitedTo)
    def result =processQueryByPredicates(predicates)
    if(result.totalMatches!=0){
    }
    result.hits.eachWithIndex { hit , index ->
        def nodePath=hit.node.path
        // println "Node path is :" +nodePath;
        processNodePath(nodePath, isToPermanentlySaved)

    }
}

def processNodePath(resultNodePath, isToPermanentlySaved){
    def clientLibNode=getNode(resultNodePath);
    def catagories=clientLibNode.get('categories')
    def singleValuedCategories="";
    def emptyArrayList;
    // println "Catagory : "+catagories;

    if(isMultiValued(catagories)){
        //Multivalued Logic
        emptyArrayList= catagories;
        emptyArrayList.add(categoryNameForAllComponent);
        emptyArrayList.add(categoryNameForAppLevel);
        // println "Single Category Empty List : "+emptyArrayList;

    }
    else if(isSingleStringValued(catagories)){
        //single valued Logic
        singleValuedCategories=catagories;
        emptyArrayList=Arrays.asList(singleValuedCategories, categoryNameForAllComponent, categoryNameForAppLevel);
        // println "Multi Category Empty List : "+emptyArrayList;
        clientLibNode.set("categories", null);
    }
    clientLibNode.set('categories', emptyArrayList);
    if(isToPermanentlySaved){
        save();
    }
    catagories=clientLibNode.get('categories')
    println "Catagory 2 : "+catagories;
}

def isSingleStringValued(propertyValue){
    return (propertyValue instanceof String)
}
def isMultiValued(propertyValue){
    return (propertyValue instanceof java.util.List)
}

/**  Declaration ends here ***/


/**  Execution starts here ***/
path="/apps/tajhotels/components";
crxdeWriteAccess=false;
categoryNameForAllComponent= "apps.tajhotels.components.all";
categoryNameForAppLevel= "apps.tajhotels.all";
processQueryResult(path, crxdeWriteAccess);




/**  Execution ends here ***/