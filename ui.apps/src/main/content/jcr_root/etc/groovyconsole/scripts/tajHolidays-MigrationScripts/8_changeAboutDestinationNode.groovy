import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/content/about-destination",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        String imagePath = hit.getResource().adaptTo(Node.class).get("destinationimage")
        if(null!=imagePath) {
            ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
            modifiableValueMap.put("destinationImages",imagePath)
            hit.getResource().getResourceResolver().commit()
        }
        else {
            println "Couldnt find an imagePath for " + hit.node.path
        }
    }
}

