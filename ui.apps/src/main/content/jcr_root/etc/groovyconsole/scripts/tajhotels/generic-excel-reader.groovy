package etc.groovyconsole.scripts.tajhotels

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import java.io.*
import com.day.cq.dam.api.Asset

class GroovyExcelParser {
    //http://poi.apache.org/spreadsheet/quick-guide.html#Iterator

    def parse(path, sheetNo) {
        InputStream inp = new FileInputStream(path)
        parseFromStream(inp, sheetNo)
    }

    def parseFromStream(steramToUse, sheetNo) {
        Workbook wb = WorkbookFactory.create(steramToUse)
        Sheet sheet = wb.getSheetAt(sheetNo)

        Iterator<Row> rowIt = sheet.rowIterator()
        Row row = rowIt.next()
        def headers = getRowData(row)

        def rows = []
        while(rowIt.hasNext()) {
            row = rowIt.next()
            rows << getRowData(row)
        }
        [headers, rows]
    }

    def getRowData(Row row) {
        def data = []
        for (Cell cell : row) {
            getValue(row, cell, data)
        }
        data
    }

    def getRowReference(Row row, Cell cell) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        CellReference ref = new CellReference(rowIndex, colIndex)
        ref.getRichStringCellValue().getString()
    }

    def getValue(Row row, Cell cell, List data) {
        def rowIndex = row.getRowNum()
        def colIndex = cell.getColumnIndex()
        def value = ""
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString()
                break
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    value = cell.getDateCellValue()
                } else {
                    value = cell.getNumericCellValue()
                }
                break
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue()
                break
            case Cell.CELL_TYPE_FORMULA:
                value = cell.getCellFormula()
                break
            default:
                value = ""
        }
        data[colIndex] = value
        data
    }

    def toProcessLocally(header, row, rowsIndex) {
        println "rows size : "+rowsIndex
        def obj=""
        row.eachWithIndex { datum, i ->
            def headerName =header[i]
            // obj += "<$headerName>$datum</$headerName>\n"
            toExecuteEach("$headerName", "$datum")
        }
        obj += ""
    }
}// End of Class

def toProcessByRow(header, row, rowsIndex, maxRowIndex) {
    println "Row Index 001: "+rowsIndex
    def minRange=0             //min default is 0
    def maxRange=maxRowIndex          //max default is 1000
    if(rowsIndex>=minRange && rowsIndex<=maxRange ){
        println "Row Array : "+row
    }
}

def toProcessByRowElement(header, row, rowsIndex) {
    println "Row Index 002 : "+rowsIndex
    def obj=""
    row.eachWithIndex { datum, i ->
        def headerName =header[i]
        // obj += "<$headerName>$datum</$headerName>\n"
        toExecuteEach("$headerName", "$datum")
    }
    obj += ""
}

def toExecuteEach(headerName, rowElementForHeaderName) {
    println "$headerName : $rowElementForHeaderName"
}

def executeHeadersAndRows(headers, rows, maxRowIndex){
    println 'Headers----'
    headers.each { header ->
        println header
    }
    println "\n  Rows ------------------"
    rows.eachWithIndex { row, rowsIndex ->
        toProcessByRow(headers, row, rowsIndex, maxRowIndex)
    }
}

//Execute after getting DAM file node
def getDamExcelAssetFromPath(parser, excelNodePath, sheetNumber, maxRowIndex ){
    def sheetNoToRead=sheetNumber-1
    if(sheetNoToRead>=0){
        Resource res = resourceResolver.getResource(excelNodePath)
        Asset asset= res.adaptTo(Asset.class)
        r=asset.getOriginal()
        def stream = r.getStream()
        def (headers, rows) = parser.parseFromStream(stream, sheetNoToRead)
        executeHeadersAndRows(headers, rows, maxRowIndex)
    }
}

def startReadingExcel(excelfilename, sheetNumber, maxRowIndex){
    def isLocal=false
    if(!excelfilename.startsWith("/content/dam/")){
        isLocal=true
    }
    def sheetNoToRead=sheetNumber-1
    if(sheetNoToRead>=0){
        println "File name : "+excelfilename+"\n---"
        GroovyExcelParser parser = new GroovyExcelParser()
        if(!isLocal){
            getDamExcelAssetFromPath(parser, excelfilename, sheetNumber, maxRowIndex )
        }else if(isLocal){
            def (headers, rows) = parser.parse(excelfilename, sheetNoToRead)
            executeHeadersAndRows(headers, rows, maxRowIndex)
        }else{
            println "Never come to this Block"
        }
    }else{
        println "Please select a sheet number in positive number(not zero)"
    }
}

// Please Upload the Excel file in following location before running the script
def filenameInAemServer ="/content/dam/tajhotels/others/Prod offfer (3).xlsx"
def excelfilenameInLocalSys = 'C:\\Users\\Srikanta\\Downloads\\Prod offfer (3).xlsx'
def sheetNoToRead=1    //sheetNo starts from 1
def maxRowIndex=1000    //default max rowCount is 1000
startReadingExcel(filenameInAemServer, sheetNoToRead, maxRowIndex)
