def predicates = [    
    "path":"/content/seleqtions",
	"type":"cq:Page",
	"1_property":"jcr:content/cq:lastReplicationAction",
	"1_property.value":"Activate",
	"2_property":"jcr:content/containerStyles",
	"2_property.value":"%-theme",
	"2_property.operation":"like",
	"p.limit":"-1"
]

def query = createQuery(predicates)


def result = query.result

println "${result.totalMatches} hits"
def index = 1
result.hits.each { hit ->
    println "hit path = ${hit.node.path}"
    try{
        println index++
        def containerStyles = hit.node.get("jcr:content/containerStyles")
        println containerStyles
        def updatedContainerStyles = ""
        def containerStylesArray = containerStyles.split(" ")
        println containerStylesArray
        containerStylesArray.each{ item ->
            if(!item.contains("theme")){
                updatedContainerStyles += item +" "
            }
        }
        println updatedContainerStyles
        if(updatedContainerStyles==""){
            hit.node.getProperty("jcr:content/containerStyles").remove()
            println "removed"
        }else{
            hit.node.getProperty("jcr:content/containerStyles").setValue(updatedContainerStyles)   
            println hit.node.get("jcr:content/containerStyles")
        }
        save()
        activate("${hit.node.path}")
    }catch(e){
       println "Unable to execute : ${hit.node.path}"+e 
    }
}