
import javax.jcr.Node

/*This method is used to Query the JCR and find results as per the Query.*/
 def buildQuery(page, term) {
       def queryManager = session.workspace.queryManager
       def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
/*Here term is the sling:resourceType property value*/
       queryManager.createQuery(statement, 'sql')
   }

   /*Defined Content Hierarchy */
  final def page = getPage('/content/tajhotels/en-in')
  /*Template which is searched in the content hierarchy */
  final def query = buildQuery(page, 'wcm/foundation/components/parsys')
  final def result = query.execute()

    println 'No Of pages found = ' + result.nodes.size()
    result.nodes.each { node ->
       def nodePath=node.path
       if(nodePath.contains('/footer_parsys')){
           if(node.hasNode('reference')){
            def refNode=node.getNode('reference')
            refNode.setProperty('path','/content/tajhotels/en-in/jcr:content/footer_parsys')
            session.save()
           }
       }
 }