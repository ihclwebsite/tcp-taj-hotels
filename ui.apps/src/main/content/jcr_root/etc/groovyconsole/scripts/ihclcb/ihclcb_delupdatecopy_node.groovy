def pagePathToExcute = '/content/ihclcb/en-in/ourhotels-ihclcb/hotels-in-kolkata'
def pageTemplate = 'tajhotels/components/structure/destination-landing-page'


def buildQuery(pagePathToExcute, pageTemplate) {
 final def page = getPage(pagePathToExcute)
 def queryManager = session.workspace.queryManager;
 def statement = 'select * from nt:base where jcr:path like \'' + page.path + '/%\' and sling:resourceType = \'' + pageTemplate + '\'';
 def query=queryManager.createQuery(statement, 'sql');
 final def result = query.execute()
 return result;
}

def deleteGivenNode(pagePathToExcute, pageTemplate) {
 final def result = buildQuery(pagePathToExcute, pageTemplate);
 //println result;
 result.nodes.each {
  node ->
   String nodePath = node.path;
  //println nodePath;

  if (node.hasNode('header_parsys/reference')) {
   def refNode = node.getNode('header_parsys/reference');
   println("removing the node. " + refNode.remove());
   session.save()
  } else {

   println("no  node");
  }
 }
}
// copying a node and put into a defined path 

def copyGivenNode(pagePathToExcute, pageTemplate) {
 final def result = buildQuery(pagePathToExcute, pageTemplate);
 //println result;
 result.nodes.each {
  node ->
   String nodePath = node.path;
  println nodePath;
  try {
   println("insidetry");
   if (node.hasNode('header_parsys')) {
    def refNode = node.getNode('header_parsys');
    println refNode.getPath();
    Workspace workspace = session.getWorkspace();
    def nodePathToCheck = refNode.getPath() + "/reference";
    workspace.copy('/content/ihclcb/temp-live-copy/jcr:content/header_parsys/reference', nodePathToCheck);
    println("Reference node copied successfully at path" + refNode.getPath());

   }
  } catch (Exception e) {
   if (node.getNode('header_parsys').hasNode("reference")) {
    println("Node already exists");
   } else {

    println("Exception occured : Not able to get node at path for offer " + e);
   }
  }
 }
}
//Update a propertyValue for a propertyName ("path")

def updateGivenNodeProperty(pagePathToExcute, pageTemplate) {
 final def result = buildQuery(pagePathToExcute, pageTemplate);
 //println result;
 result.nodes.each {
  node ->
   String nodePath = node.path;
  //println nodePath;
  if (node.hasNode('header_parsys/reference')) {
   println(node);
   def refNode = node.getNode('header_parsys/reference');
   //println refNode;
   String pathProp = refNode.get('path');
   //println pathProp ;
   refNode.set('path', '/content/ihclcb/en-in/jcr:content/header_parsys');
   session.save()
  }
 }
}

//deleteGivenNode(pagePathToExcute, pageTemplate);
//copyGivenNode(pagePathToExcute, pageTemplate);
//updateGivenNodeProperty(pagePathToExcute, pageTemplate);

