import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
 
i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/taj-holidays",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-package-details",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
    ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
    String rateDescrioption =  modifiableValueMap.get("rateDescription")
    if(rateDescrioption.contains("<br />") || rateDescrioption.contains("<br>")) {
        String updatedString = rateDescrioption.replaceAll("<br />","")
        println "Old String :: "+ rateDescrioption
        println "\n"
        println "New String :: "+updatedString
        println "=========================================================="
        modifiableValueMap.put("rateDescription",updatedString)
        hit.getResource().getResourceResolver().commit()
        i++
        }
        println i + " rateDescription has been corrected"
    }
}

