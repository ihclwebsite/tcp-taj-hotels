package tajhotels

import java.util.*

def destination=""
def list
getPage("/content/tajhotels/en-in/our-hotels").recurse { page ->
    def content = page.node

    if (content && content.get("hotelId")) {
        def pagePath=page.path
        def pagePathArr=pagePath.split("/")
        if(pagePathArr.size()>5){
            def currentDestination=pagePathArr[5]
            if(destination!=currentDestination){
                // println list;
                list= new ArrayList()
                println " $currentDestination"
                destination=currentDestination
            }else{
                // println ""
            }

        }
        // println page.path+" : : "+content.get("hotelId");
        list.add('"'+content.get("hotelId")+'"')
        // println content.get("hotelId");
        // println destination
    }
}