import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

// def pathValue="/content/seleqtions/en-in/our-hotels"
def pathValue="/content/tajhotels/en-in/our-hotels"

// def nodename="meeting-request-quote"
def nodename="jiva-spa-booking"

executeTajHotelsList(pathValue, nodename)

def executeTajHotelsList(pathValue, nodename) {
	def queryManager = session.workspace.queryManager
    def count = 0
    def predicate = [
        "path": pathValue,
        "property":"jcr:primaryType",
        "property.value":"cq:Page",
        "nodename":nodename,
        "p.limit":"-1"
        ]

 def query = createQuery(predicate)
 def result = query.result
 
 result.nodes.each {
  node ->
    def meetingPath = node.path
      count ++
      println count
      println "Path : $meetingPath"
    deactivate(meetingPath)
      println "Node Deactivated Successfully"
    node.remove()
      session.save()
      println "Node Deleted Successfully"
 }
}