

hotels_in_loc_changes='[{"name":"title","type":"h1"},{"name":"title_description","type":"h3"},{"name":"destination_offer_ca/popular-destinations-title","type":"h4"},{"name":"destination_offer_ca","type":"h5"},{"name":"title_1959773","type":"h6"},{"name":"destination_experien","type":"h6"}]'
about_loc_changes='[{"name":"destination_offer_ca/popular-destinations-title","type":"h2"},{"name":"destination_offer_ca","type":"h3"},{"name":"destination_experien/popular-destinations-title","type":"h4"},{"name":"destination_experien","type":"h5"}]'
se_loc_changes='[{"name":"title","type":"h1"},{"name":"destination_offer_ca/popular-destinations-title","type":"h3"},{"name":"destination_offer_ca","type":"h4"}]'
restaurants_loc_changes='[{"name":"title_description","type":"h1"},{"name":"dining_list","type":"h2"},{"name":"title","type":"h3"},{"name":"destination_offer_ca","type":"h4"}]'

hotel_overview='[{"name":"rateandreview/popular-destinations-title","type":"h4"}]'
hotel_guestrooms='[{"name":"room_list/popular-destinations-title","type":"h1"},{"name":"specific_hotel_offer","type":"h3"},{"name":"specific_hotel_offer/popular-destinations-title","type":"h2"}]'
hotel_dining='[{"name":"title_description","type":"h1"},{"name":"dining_list","type":"h2"},{"name":"specific_hotel_offer","type":"h4"},{"name":"specific_hotel_offer/popular-destinations-title","type":"h3"},{"name":"title_399647699","type":"h5"},{"name":"nearby_dining_list","type":"h6"}]'
hotel_offer='[{"name":"title_description","type":"h1"}]'
hotel_jiva='[{"name":"title_description","type":"h1"}]'
hotel_experiences='[{"name":"title_description","type":"h1"},{"name":"all_experiences","type":"h2"}]'
hotel_events='[{"name":"title_description","type":"h1"}]'
//path='/content/tajhotels/en-in/our-hotels/hotels-in-colombo/jcr:content/root'
//findAllHotelsInLocation()
//findAllAboutLocation()
//findAllExperienceInLocation()
//findAllRestaurantsInLocation()
//findAllHotelsOverview()
findAllHotelsGuestRooms()
//findAllHotelsDiningPage()
//findAllHotelsOffersPage()
//findAllHotelsJivaSpa()
//findAllHotelsExp()
//findAllHotelsEvents()


def findAllHotelsEvents()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/tajhotels-meetings-page")
        {
            makeChanges(content.path+"/root",hotel_events)
        }
}
}

def findAllHotelsExp()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/experiences-list-page")
        {
            makeChanges(content.path+"/root",hotel_experiences)
        }
}
}


def findAllHotelsJivaSpa()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/jiva-spa-list-page")
        {
            makeChanges(content.path+"/root",hotel_jiva)
        }
}
}


def findAllHotelsOffersPage()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/all-offers-page")
        {
            makeChanges(content.path+"/root",hotel_offer)
        }
}
}

def findAllHotelsDiningPage()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/dining-list-page")
        {
            makeChanges(content.path+"/root",hotel_dining)
        }
}
}


def findAllHotelsGuestRooms()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/tajhotels-rooms-listing-page")
        {
            makeChanges(content.path+"/root",hotel_guestrooms)
        }
}
}


def findAllHotelsOverview()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/hotels-landing-page")
        {
            makeChanges(content.path+"/root",hotel_overview)
        }
}
}

def findAllHotelsInLocation()
{
getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/destination-landing-page")
        {
            makeChanges(content.path+"/root",hotels_in_loc_changes)
        }
}
}

def findAllAboutLocation()
{
    getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/about-destination-page")
        {
            makeChanges(content.path+"/root",about_loc_changes)
        }
}
}


def findAllExperienceInLocation()
{
    getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/all-experience-destination-page")
        {
            makeChanges(content.path+"/root",se_loc_changes)
        }
}
}

def findAllRestaurantsInLocation()
{
    getPage("/content/tajhotels/en-in/our-hotels").recurse{ page ->
 
        def content = page.node
        if (content && content.get("sling:resourceType")=="tajhotels/components/structure/dining-list-destination-page")
        {
            makeChanges(content.path+"/root",restaurants_loc_changes)
        }
}
}


def makeChanges(path,changes)
{
    changesArr= new JsonSlurper().parseText(changes)
   
    for(nodeJson in changesArr)
    {
        nodeName=nodeJson.get("name")
        try
        {
        node=getNode(path+"/"+nodeName)
        if(node)
         {
            println "Setting value in "+node.path
            node.set("type",nodeJson.get("type"))
            save()
            activate(node.path)
         }
         
        }catch(Exception e)
        {
            println e
        }
    }
}