 def buildQuery(page, term) {
       def queryManager = session.workspace.queryManager
      def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
       queryManager.createQuery(statement, 'sql')
 }

  
  page = getPage('/content/tajhotels/en-in/our-hotels/')
  final def query = buildQuery(page, 'tajhotels/components/structure/hotels-landing-page')
  final def result = query.execute()
  result.nodes.each{node ->
  try{
      
    //node.getNode('banner_parsys/hotel_banner').setProperty('hideBookingBar','true');
    println node.getProperty("brand").getString()
    if(node.getProperty("brand").getString()=='taj:hotels/brands/taj'){
        node.setProperty('listingRanker',100)
    }
    if(node.getProperty("brand").getString()=='taj:hotels/brands/vivanta'){
        node.setProperty('listingRanker',200)
    }
    if(node.getProperty("brand").getString()=='taj:hotels/brands/gateway'){
        node.setProperty('listingRanker',300)
    }
    // if(node.hasProperty('ogSitename')){
    //     if(node.getProperty("ogSitename").getString().toLowerCase()=='taj'){
    //         node.setProperty('listingRanker',1);
    //     } 
    //     if(node.getProperty("ogSitename").getString().toLowerCase()=='vivanta'){
    //         node.setProperty('listingRanker',2);
    //     }else{
    //         node.setProperty('listingRanker',3);
    //     }
    // }
    session.save()
    if(node.hasProperty('cq:lastReplicationAction')){
        if(node.getProperty('cq:lastReplicationAction').getString().toLowerCase()=='activate'){
         activate(node.getPath())
        }else{
        	println 'Ignoring '+node.getPath()
        }
    }else{
    	println 'Ignoring not ever replicated '+node.getPath()
    }
    
   
    
   
  }catch(exception){
      println exception
  }
  }
  
  
  //destAboutDiningArr = ["tajhotels/components/structure/about-destination-page", "tajhotels/components/structure/dining-list-destination-page"];
  
//   for(value in destAboutDiningArr) {
      
//       final def query = buildQuery(page, value);
//       final def result = query.execute()
    
//       result.nodes.each { node ->
//           //println 'nodePath -' + node.path
//           node.setProperty("navTitle", node.get('jcr:title'));
//           session.save()
//       }
//   }