import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        if(hit.path.contains("packages")) {
            println hit.getResource().getParent().path
            ModifiableValueMap modifiableValueMap = hit.getResource().adaptTo(ModifiableValueMap.class)
            modifiableValueMap.put("destinationRootPath",hit.getResource().getParent().path)
            modifiableValueMap.remove("packagesRootPath")
            hit.getResource().getResourceResolver().commit()
        }
    }
}

