import javax.jcr.Node
 
 /*This method is used to Query the JCR and find results as per the Query.*/
  def buildQuery(page, term) {
        def queryManager = session.workspace.queryManager
        def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\'' + 'and servesCuisine IS NOT NULL'
/*Here term is the sling:resourceType property value*/
        queryManager.createQuery(statement, 'sql')
    }
 
    /*Defined Content Hierarchy */
   final def page = getPage('/content/tajhotels/')
   /*Template which is searched in the content hierarchy */
   final def query = buildQuery(page, 'tajhotels/components/structure/hotels-landing-page')
   final def result = query.execute()
 
   println 'No Of pages found = ' + result.nodes.size()
 
   result.nodes.each { node ->
        println 'nodePath::'+node.path
        def cuisines = node.getProperty('servesCuisine').getValues()
        node.setProperty('restaurantCuisine', cuisines)
        node.getProperty('servesCuisine').remove()
        session.save()
   }
 