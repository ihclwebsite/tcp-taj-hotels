import com.day.cq.replication.Replicator
import com.day.cq.replication.ReplicationActionType

currentHotePath = ""

getAllHotelOffersPages()

def getAllHotelOffersPages(){
    
   def newTemplateBreadcrumbNodePath = "/content/example-page/hotels-in-mumbai/staycations/jcr:content/root/breadcrumb"
    def newTemplateReferenceNodePath =  "/content/example-page/hotels-in-mumbai/staycations/jcr:content/root/reference"
    def newTemplateBookNowNodePath = "/content/example-page/hotels-in-mumbai/staycations/jcr:content/root/book_now_button"
    def count=0
    def predicates = [
       "path":"/content/tajhotels/en-in/our-hotels/",
       "p.limit":"-1",
       "type":"cq:PageContent",
       "1_property":"sling:resourceType",
       "1_property.value":"tajhotels/components/structure/offer-details-page"
   ]
   
   def query = createQuery(predicates)
   
   query.hitsPerPage = 100000
   
   def result = query.result
   
   result.hits.each { hit ->
       println "hit path12 = " + hit.node.path+'/root'
       nodePath = hit.node.path
       nodeRootPath = hit.node.path+'/root'
       nodeParticipatingHotelPath = hit.node.path+'/root/participating_hotels'
       
       currentHotePathArr = nodePath.split("/")

       currentHotePath = currentHotePathArr[0] + "/" + currentHotePathArr[1] + "/" + currentHotePathArr[2] + "/" + currentHotePathArr[3] + "/" +
                         currentHotePathArr[4] + "/" + currentHotePathArr[5] + "/" + currentHotePathArr[6] + "/" + currentHotePathArr[7]
       println 'hit path13 = '+ currentHotePath

       updateCurrentOfferContent(hit.node)
       updateCurrentRootBreadcrumbNode(newTemplateBreadcrumbNodePath, nodePath+'/root/breadcrumb', nodeRootPath)
       updateCurrentRootReferenceNode(newTemplateReferenceNodePath, nodePath+'/root/reference', nodeRootPath)
       removeParticipatingHotelNode(nodeParticipatingHotelPath)
       updateBookNowButtonNode(newTemplateBookNowNodePath, nodePath+'/root/book_now_button', currentHotePath)
       updateBannerReference(nodePath)
       activateNewOfferNode(nodePath)
   }
}


def updateCurrentOfferContent(contentNode) {
    //println ''+contentNode.path
    contentNode.setProperty("cq:template","/conf/tajhotels/settings/wcm/templates/hotel-specific-offer-details")
    contentNode.setProperty("sling:resourceType","tajhotels/components/structure/hotel-specific-offer-details-page")
    contentNode.setProperty("navTitle",contentNode.get("jcr:title"))
    save()
    //println 'Current path cq:template updated :' + contentNode.get("cq:template");
    //println 'Current path resourceType updated :' + contentNode.get("sling:resourceType");
}

def updateCurrentRootBreadcrumbNode(path1, path2, path3) {
    //println 'path 1 : ' + path1
    //println 'path 1 : ' + path2
    //println 'path 1 : ' + path3
    
    Workspace workspace = session.getWorkspace()
    workspace.copy(path1, path2)
    println("Breadcrumb node copied successfully at path : " + path2)

    try {
       newBreadcrumbNode = getNode(path3)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + path3)
    }
    
    newBreadcrumbNode.orderBefore('breadcrumb','offerdetails')
    save()

    println("Breadcrumb node saved at first index successfully at path : " + path3)

}

def updateCurrentRootReferenceNode(path1, path2, path3) {
    //println 'path 1 : ' + path1
    //println 'path 1 : ' + path2
    //println 'path 1 : ' + path3
    
    Workspace workspace = session.getWorkspace()
    workspace.copy(path1, path2)
    println("reference node copied successfully at path : " + path2)

    try {
       referenceNode = getNode(path2)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + path2)
    }
    
    hotelNavigationPath = currentHotePath +'/jcr:content/root/hotel_navigation'
    referenceNode.setProperty('path', hotelNavigationPath)

    try {
       nodeAtRootPath = getNode(path3)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + path3)
    }
   
    nodeAtRootPath.orderBefore('reference','offerdetails')
    save()

    println("reference node saved at second index successfully at path : " + path3)
}

def removeParticipatingHotelNode(participatingHotelPath) {
    //println ''+participatingHotelPath
    
    try {
       participatingHotelNode = getNode(participatingHotelPath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + participatingHotelPath)
    }
    
    participatingHotelNode.remove()
    save()

    println("participating_hotel node removed successfully at path : " + participatingHotelPath)
}

def updateBookNowButtonNode(srcBookButtonNodePath, destBookButtonNodePath, hotelPagePath) {

    Workspace workspace = session.getWorkspace()
    workspace.copy(srcBookButtonNodePath, destBookButtonNodePath)
    println("book_now_button node copied successfully at path : " + destBookButtonNodePath)

    try {
       bookButtonNode = getNode(destBookButtonNodePath)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + destBookButtonNodePath)
    }
    
    bookButtonNode.setProperty("buttonLink", hotelPagePath)
    save()
}


def updateBannerReference(destPathForBanner) {
    
    hotelOfferAndPromotionsNodePath = currentHotePath + '/offers-and-promotions/jcr:content/banner_parsys/reference'
    newOfferAndPromotionsNodePath = destPathForBanner + '/banner_parsys/reference'
    deleteNodePathForBanner = destPathForBanner + '/banner_parsys/offerbanner'

    Workspace workspace = session.getWorkspace()
    workspace.copy(hotelOfferAndPromotionsNodePath, newOfferAndPromotionsNodePath)

    println("banner reference node copied successfully at path : " + newOfferAndPromotionsNodePath)

    try {
       bannerDeleteNodePath = getNode(deleteNodePathForBanner)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + deleteNodePathForBanner)
    }
    
    bannerImagePath = bannerDeleteNodePath.get("bannerImage")

    try {
       offerJcrContentNode = getNode(destPathForBanner)
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path : " + destPathForBanner)
    }
    offerJcrContentNode.setProperty("bannerImage", bannerImagePath)
    bannerDeleteNodePath.remove()
    save()

    println("banner node removed successfully at path : " + deleteNodePathForBanner)

}

def activateNewOfferNode(offerNodePath) {
    
    try {
        destRootActivationNode = getNode(offerNodePath)
        currentActiveStauts = destRootActivationNode.get('cq:lastReplicationAction')
        if(currentActiveStauts == "Activate") {
        
            activate(offerNodePath)
        }
    } catch(Exception e) {
       println("Exception occured : Not able to get node at path " + offerNodePath)
    }
    
}