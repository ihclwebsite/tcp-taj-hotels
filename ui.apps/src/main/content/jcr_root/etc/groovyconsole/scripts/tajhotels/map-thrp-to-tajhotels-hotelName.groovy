package tajhotels

import java.util.Map
import java.util.HashMap

hotelsUrlMap= new HashMap<>()
def getAllThrpPages(path, queryResultLimitedTo){
    def ignoreGLobalOfferResourceTypePath="/content/thrp/"+path.split("/")[3]+"/en-in/offers"
    println "ignoreGLobalOfferResourceTypePath : "+ignoreGLobalOfferResourceTypePath
    def predicates = [
            "path":path,
            "type":"cq:PageContent",
            "1_property":"sling:resourceType",
            "1_property.value":"taj-core/pagecomponents/offerDetailsPage",
            "p.limit":-1
    ]
    def query = createQuery(predicates)
    query.hitsPerPage = queryResultLimitedTo
    def result = query.result
    println "${result.totalMatches} hits, execution time = ${result.executionTime}s\n--"
    processQueryResult(result, ignoreGLobalOfferResourceTypePath)
    return result
}
def processQueryResult(result, ignoreGLobalOfferResourceTypePath){
    result.hits.each { hit ->
        nodePath = hit.node.path
        //   println "hit path01 = " + nodePath;
        if(!nodePath.contains(ignoreGLobalOfferResourceTypePath)){
            def nodePathArr=[]
            if(nodePath.contains("/offers/")){
                nodePathArr=nodePath.split("/offers/")
            }else if(nodePath.contains("/promotions/")){
                nodePathArr=nodePath.split("/promotions/")
            }
            def nodePathIn6_3=""
            def nodePathIn6_1=""
            def offerNodePathIn6_1=""
            if(nodePathArr.size()>1){
                nodePathIn6_1=nodePathArr[0]
                offerNodePathIn6_1=nodePathArr[1]
                if(hotelsUrlMap.containsKey(nodePathIn6_1)){
                    //   println "Map contains hotelPath";
                    nodePathIn6_3=hotelsUrlMap.get(nodePathIn6_1)
                }else{
                    //   println "Map does not contain hotelPath";
                    nodePathIn6_3=getHotelPathOf6_3By6_1(nodePathIn6_1)
                }
                //   println "nodePathIn6_3 : "+nodePathIn6_3
            }
            if(nodePathIn6_1!="" && nodePathIn6_3!=""){
                if(nodePath.contains("/offers/")){
                    updateNode(nodePathIn6_1+"/offers/"+offerNodePathIn6_1, nodePathIn6_3+"/offers-and-promotions/"+offerNodePathIn6_1)
                }else if(nodePath.contains("/promotions/")){
                    updateNode(nodePathIn6_1+"/promotions/"+offerNodePathIn6_1, nodePathIn6_3+"/offers-and-promotions/"+offerNodePathIn6_1)
                }
            }
        }
        else if(nodePath.contains(ignoreGLobalOfferResourceTypePath)){
            //   println "Ignoring path : "+nodePath;
        }

    }
}

def getHotelPathOf6_3By6_1(pathOf6_1){
    def node1= ""
    if(!pathOf6_1.contains("/jcr:content")){
        node1= getNode(pathOf6_1+"/jcr:content")
    }else{
        node1= getNode(pathOf6_1)
    }
    println "6.1 Node Path :"+node1.path
    def hotelBookingEngineId6_1Val=node1.get("hotelBookingEngineId")
    println "hotelBookingEngineId : "+ hotelBookingEngineId6_1Val

    def predicates = [
            "path":"/content/tajhotels/en-in/our-hotels/",
            "type":"cq:PageContent",
            "1_property":"sling:resourceType",
            "1_property.value":"tajhotels/components/structure/hotels-landing-page",
            "2_property":"hotelBookingEngineId",
            "2_property.value":hotelBookingEngineId6_1Val,
            "3_property":"cq:lastReplicationAction",
            "3_property.value":"Activate",
            "p.limit":-1
    ]
    def query = createQuery(predicates)
    def result2 = query.result
//   println "${result2.totalMatches} hits, execution time = ${result2.executionTime}s\n--";
    if(result2.totalMatches==1){
        println "sending first one for single result."
    }else{
        println "sending last one for multiple nodes."
    }
    result2.hits.each { hit ->
        nodePath = hit.node.path
        //   println "hit path02 = " + nodePath;
        if(nodePath.contains("/jcr:content")){
            nodePath=nodePath.replace("/jcr:content", "")
        }
    }
    hotelsUrlMap.put(pathOf6_1.replace("/jcr:content", ""), nodePath.replace("/jcr:content", ""))
    return nodePath
}

def executeMainFlow(){
    // Activate debugging line 25, 35, 38, 41, 59, 77, 79, 81
    def pathTHRP="/content/thrp"
    def pathTHRPGateway="/content/thrp/gateway"
    def pathTHRPTaj="/content/thrp/taj"
    def pathTHRPVivanta="/content/thrp/vivanta"

    // def ignoreGLobalOfferResourceTypePath="/content/thrp/"+tajText+"/en-in/offers";
    def queryResultLimitedTo=10000
    getAllThrpPages(pathTHRPGateway, queryResultLimitedTo)
}

executeMainFlow()


def updateNode(nodePathIn6_1, nodePathIn6_3){
    println "Updating Node : "+nodePathIn6_1+"------"+nodePathIn6_3
}

