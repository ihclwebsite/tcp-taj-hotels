import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/taj-holidays/destinations",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-package-details",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        String holidayOfferPackagePath = hit.getResource().getParent().getParent().path
        Resource offerPageResource = resourceResolver.getResource(holidayOfferPackagePath)
        ModifiableValueMap modifiableValueMap = offerPageResource.adaptTo(ModifiableValueMap.class)
        modifiableValueMap.put("cq:template","/conf/tajhotels/settings/wcm/templates/tajholidays-offer-package-page-template")
        modifiableValueMap.put("sling:resourceType","tajhotels/components/structure/tajholidays-offer-package-page")
        offerPageResource.getResourceResolver().commit()
        i++
        currentNodeActivationStatus = offerPageResource.node.get("cq:lastReplicationAction")
        if(currentNodeActivationStatus=="Activate") {
           activate(holidayOfferPackagePath)
            }
        }
    println i + " HolidayOfferPage's sling:resourceType and cq:Template has been modified"
    
}

