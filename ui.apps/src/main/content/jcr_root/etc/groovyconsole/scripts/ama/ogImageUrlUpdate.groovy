import javax.jcr.Node
 

path='/content/ama/en-in/'
String bannerPath=setBannerPath()
findAllPagesUnderama(bannerPath)
 
def setBannerPath(){
    def bannerImageRes=resourceResolver.getResource('/content/ama/en-in/jcr:content/banner_parsys/banner_carousel/banner/item0')
    String bannerImage=bannerImageRes.adaptTo(Node.class).getProperty("bannerImage").getString();
    println 'bannerImage '+bannerImage
    return bannerImage
}
 
def findAllPagesUnderama(bannerImage){
 getPage(path).recurse
 { page ->
      def contentNode = page.node
     println 'contentNode content:: '+contentNode.getPath()
      def session=contentNode.getSession()
      contentNode.setProperty('ogImageURL',bannerImage)
      session.save()
      
      
 }
}
