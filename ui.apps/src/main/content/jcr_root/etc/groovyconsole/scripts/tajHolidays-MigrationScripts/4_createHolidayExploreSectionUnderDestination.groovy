import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #3 - Explore-destination-Creation script - This script essentially creates Explore-<insert"HolidayDestinationName"> pages along 
with its child nodes , & its child nodes and the relevant component */

i=0
init()

def init(){
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit -> 
         if(!hit.node.getParent().path.contains("packages-in")) {
            i++
            println i + " : "+ hit.node.getParent().path
            createPackagesInDestinationPage(hit.node.getParent().path)
        }
    }
}

def createPackagesInDestinationPage(path) {
    
    def packagePageName =path.replaceAll("/content/tajhotels/en-in/holidays-landing-page/holidays-destination/holidays-in-","explore-")
    def jcrTitle =path.replaceAll("/content/tajhotels/en-in/holidays-landing-page/holidays-destination/holidays-in-","explore-")
    Resource parentPageRes = resourceResolver.getResource(path)
    Node parentNode = parentPageRes.adaptTo(Node.class)
    
    def sess=parentNode.getSession()
    parentNode.addNode(packagePageName,"cq:Page")
    sess.save()
    Resource childResource = parentPageRes.getChild(packagePageName)
    Node childNode = childResource.adaptTo(Node.class)
    
    sess=childNode.getSession()
    childNode.addNode("jcr:content","cq:PageContent")
    sess.save()
    Resource subChildResource = childResource.getChild("jcr:content")
    ModifiableValueMap modifiableValueMap = subChildResource.adaptTo(ModifiableValueMap.class)
    
    modifiableValueMap.put("containerStyles","our-hotels-layout")
    modifiableValueMap.put("cq:template","/conf/tajhotels/settings/wcm/templates/tajholidays-destination-landing-page")
    modifiableValueMap.put("jcr:title",jcrTitle)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/structure/tajholidays-destination-landing-page")
    resourceResolver.commit()
    
   
    Node subChildNode = subChildResource.adaptTo(Node.class)
    createChildrenForJcrContent(subChildNode)
}

def createChildrenForJcrContent(jcrNode){
    
    Resource jcrResource = resourceResolver.getResource(jcrNode.path)
    def sess=jcrNode.getSession()
    jcrNode.addNode("root","nt:unstructured")
    sess.save()
    
    Resource rootResource = jcrResource.getChild("root")
    modifiableValueMap = rootResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/responsivegrid")
    resourceResolver.commit()
    Node rootNode = rootResource.adaptTo(Node.class)
    createChildrenForRootNode(rootNode)
    
    jcrNode.addNode("header_parsys","nt:unstructured")
    sess.save()
    Resource headerResource = jcrResource.getChild("header_parsys")
    modifiableValueMap = headerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node headerNode = headerResource.adaptTo(Node.class)
    
    headerNode.addNode("reference","nt:unstructured")
    sess.save()
    Resource headerRefResource = headerResource.getChild("reference")
    modifiableValueMap = headerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def header_parsys_path = headerNode.getParent().getParent().getParent().getParent().getParent().path + "/jcr:content/header_parsys"
    modifiableValueMap.put("path",header_parsys_path)
    resourceResolver.commit()
    
    jcrNode.addNode("banner_parsys","nt:unstructured")
    sess.save()
    Resource bannerResource = jcrResource.getChild("banner_parsys")
    modifiableValueMap = bannerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node bannerNode = bannerResource.adaptTo(Node.class)
    
    bannerNode.addNode("reference","nt:unstructured")
    sess.save()
    Resource bannerRefResource = bannerResource.getChild("reference")
    modifiableValueMap = bannerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def banner_parsys_path = bannerNode.getParent().getParent().getParent().path + "/jcr:content/banner_parsys"
    modifiableValueMap.put("path",banner_parsys_path)
    resourceResolver.commit()
    
    jcrNode.addNode("footer_parsys","nt:unstructured")
    sess.save()
    Resource footerResource = jcrResource.getChild("footer_parsys")
    modifiableValueMap = footerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node footerNode = footerResource.adaptTo(Node.class)
    
    footerNode.addNode("reference","nt:unstructured")
    sess.save()
    Resource footerRefResource = footerResource.getChild("reference")
    modifiableValueMap = footerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def footer_parsys_path = footerNode.getParent().getParent().getParent().getParent().getParent().path + "/jcr:content/footer_parsys"
    modifiableValueMap.put("path",footer_parsys_path)
    resourceResolver.commit()
    
}

def createChildrenForRootNode(rootNode) {
    
    Resource rootResource = resourceResolver.getResource(rootNode.path)
    def sess=rootNode.getSession()
    rootNode.addNode("reference","nt:unstructured")
    sess.save()
    
    Resource refResource = rootResource.getChild("reference")
    modifiableValueMap = refResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def  nav_parsys_path = rootNode.getParent().getParent().getParent().path + "/jcr:content/root/section_navigation"
    modifiableValueMap.put("path",nav_parsys_path)
    resourceResolver.commit()
    
    rootNode.addNode("title","nt:unstructured")
    sess.save()
    Resource titleResource = rootResource.getChild("title")
    modifiableValueMap = titleResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/content/title")
    Node jcrnod = rootNode.getParent()
    def title = jcrnod.get("jcr:title").replaceAll("Holiday Packages-","")
    if(null!=title) {
        modifiableValueMap.put("jcr:title",WordUtils.capitalizeFully(title))
    }
    resourceResolver.commit()
    
    rootNode.addNode("text","nt:unstructured")
    sess.save()
    Resource textResource = rootResource.getChild("text")
    modifiableValueMap = textResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/text")
    modifiableValueMap.put("text","")
    modifiableValueMap.put("textIsRich","true")
    resourceResolver.commit()
    
    rootNode.addNode("destination_explore","nt:unstructured")
    sess.save()
    Resource hotelListResource = rootResource.getChild("destination_explore")
    modifiableValueMap = hotelListResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/content/holidays-destination/destination-explore-list")
    modifiableValueMap.put("activitiesLabel","Activities")
    modifiableValueMap.put("diningLabel","Dining")
    modifiableValueMap.put("shoppingLabel","Shopping")
    modifiableValueMap.put("sightSeeingLabel","Sight Seeing")
    modifiableValueMap.put("spaLabel","Spa's")
    modifiableValueMap.put("destinationRootPath","")
    
    def pathForLocalList = rootNode.getParent().getParent().getParent().path +"/jcr:content/root/dest_plan_itinerary"
    Resource planItinRes = resourceResolver.getResource(pathForLocalList)
    if(null!=planItinRes){
        Node planItinNode = planItinRes.adaptTo(Node.class)
            if(null!=planItinNode) {
                String[] localList = planItinNode.get("localList")
                if(null!=localList) {
                    modifiableValueMap.put("localList",localList)
                }
                else {
                    println "Migration Unsuccessfull as localList wasnt moved for path " + rootNode.path
                }
            }
             else {
                println "Migration Unsuccessfull as ItinCompoNode wasnt found for path " + rootNode.path
            }
    }
    else{
        println "Migration Unsuccessfull as planItinRes wasnt found for path " + rootNode.path
    }
    resourceResolver.commit()
}




