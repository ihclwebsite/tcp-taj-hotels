
 
/*This method is used to Query the JCR and find results as per the Query.*/
  def buildQuery(page, term) {
        def queryManager = session.workspace.queryManager
       def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
        queryManager.createQuery(statement, 'sql')
    }
 
    /*Defined Content Hierarchy */
   final def page = getPage('/content/tajhotels/en-in/')
   /*Component ResourceType which is searched in the content hierarchy */
   final def query = buildQuery(page, 'tajhotels/components/content/hotel-overview')
   final def result = query.execute()
 
 
   result.nodes.each { node ->
   String nodePath = node.path
   
   node.setProperty("hotelPolicies","Hotel Policies")
  // println 'Title--'+node.get('hotelTitle') ;
   //println 'HotelPolice Name--'+node.get('hotelPolicies');
   //println nodePath
   session.save()
 }
   println 'No Of Pages found :' + result.nodes.size()