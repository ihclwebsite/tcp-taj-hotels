import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

i=0
j=0
init()

def init(){
   def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-details",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        println hit.path
        println hit.getResource().adaptTo(ModifiableValueMap.class).get("ratePerNightlabel")
        println hit.getResource().getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")
        hit.getResource().adaptTo(ModifiableValueMap.class).put("ratePerNightlabel","Starting Rate/Night")
        hit.getResource().getResourceResolver().commit()
        if(hit.getResource().getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")=="Activate") {
            activate(hit.path)
        }
   }
}


