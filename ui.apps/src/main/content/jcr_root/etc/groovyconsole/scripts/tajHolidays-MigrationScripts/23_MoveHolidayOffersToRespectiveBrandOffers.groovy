import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.text.ParseException

init()

def init() {
    def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-package-details",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        try{
        if(validateIfDeactivatedOffer(hit.resource)) {
            String packageValidityEndDate=hit.getResource().adaptTo(ModifiableValueMap.class).get("packageValidityEndDate")
            String packageName = hit.getResource().adaptTo(ModifiableValueMap.class).get("packageTitle")
            if(null!=packageValidityEndDate && validateDateFormat(packageValidityEndDate)) {
                String hotelPath = getResource(hit.getResource().getParent().getParent().getParent().getParent().path+"/jcr:content/root/holidays_hotel_detail").adaptTo(ModifiableValueMap.class).get("hotelPath")
                Resource packageComponentResource = hit.getResource()
                String[] str = packageComponentResource.getParent().getParent().getParent().path.split("/")
                String packageNodeName = str[str.length-1]
                //println hotelPath
                //println packageComponentResource
                //println packageComponentResource.path
                //println packageComponentResource.getParent().getParent().getParent().path
                //println packageNodeName
                if (!checkOfferExistUnder(hotelPath,packageNodeName)) {
                    println packageName + "  Offer does not exist under path ->" + hotelPath+"/offers-and-promotions"
                    createOffer(hotelPath,packageComponentResource,packageNodeName,packageName,packageValidityEndDate)
                    println packageName + " offer created successfully"
                } 
            }
        }
        } catch (Exception e){
            println "EXCEPTION -> NULL resource > Holiday offer improperly placed for the path " + hit.path
        }
   }
}

def validateIfDeactivatedOffer(resource) {
    if(resource.getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")=="Deactivate") {
        return false
    }
    return true
}

def validateDateFormat(dateString){
    
    SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy")
    try {
        Date d= format.parse(dateString)
        return true
    } catch (ParseException e) {
        return false
    }
}

def toCalendar(dateString) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
    Date date = sdf. parse(dateString)
    Calendar calObj = Calendar. getInstance()
    calObj. setTime(date)
    calObj.set(Calendar.MILLISECOND, 0)
    calObj.set(Calendar.SECOND, 59)
    calObj.set(Calendar.MINUTE, 59)
    calObj.set(Calendar.HOUR, 23)
    return calObj
}

def checkOfferExistUnder(hotelPath,packageNodeName) {
    try {
    Resource offerPageResource = getResource(hotelPath+"/offers-and-promotions")
    for(Resource childRes : offerPageResource.getChildren()) {
        String[] str =  childRes.path.split("/")
        String childNodeName = str[str.length-1]
        if(childNodeName ==packageNodeName) {
            return true
        }
    }
    return false
    } catch (Exception e) {
        println "EXCEPTION -> Global offer page doesnot exist for the path " +  hotelPath
        return true
    }
}

def createOffer(hotelPath,holidayOfferDetailsResource,holidayOfferNodeName,holidayOfferName,offerEndDate){
    
    Resource globalOfferPageResource = getResource(hotelPath+"/offers-and-promotions")
    String bannerImage = "/content/dam/holidays/themes/romantic-rendzevous/4x3/4x3_Romantic.jpg"
    Resource holidayBannerResource = getResource(holidayOfferDetailsResource.getParent().getParent().path+"/banner_parsys/campaign_banner_caro/banners/item0")
    if(null!=holidayBannerResource) {
        bannerImage=holidayBannerResource.adaptTo(ModifiableValueMap.class).get("bannerImage")
    }
    Node globalOfferPageNode = globalOfferPageResource.adaptTo(Node.class)
    def session=globalOfferPageNode.getSession()
    globalOfferPageNode.addNode(holidayOfferNodeName,"cq:Page")
    session.save()
    Resource offerPageResource = globalOfferPageResource.getChild(holidayOfferNodeName)
    Node offerPageNode = offerPageResource.adaptTo(Node.class)
    
    session=offerPageNode.getSession()
    offerPageNode.addNode("jcr:content","cq:PageContent")
    session.save()
    Resource jcrNodeResource = offerPageResource.getChild("jcr:content")
    ModifiableValueMap modifiableValueMap = jcrNodeResource.adaptTo(ModifiableValueMap.class)
    
    modifiableValueMap.put("containerStyles","specific-hotels-page holiday-theme")
    modifiableValueMap.put("cq:template","/conf/tajhotels/settings/wcm/templates/hotel-specific-offer-details")
    modifiableValueMap.put("jcr:title",holidayOfferName)
    modifiableValueMap.put("navTitle",holidayOfferName)
    modifiableValueMap.put("offTime",toCalendar(offerEndDate))
    modifiableValueMap.put("bannerImage",bannerImage)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/structure/hotel-specific-offer-details-page")
    resourceResolver.commit()
    
    Node jcrNode = jcrNodeResource.adaptTo(Node.class)
    session=jcrNode.getSession()
    
    jcrNode.addNode("root","nt:unstructured")
    session.save()
    Resource rootResource = jcrNodeResource.getChild("root")
    modifiableValueMap = rootResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/responsivegrid")
    resourceResolver.commit()
    Node rootNode = rootResource.adaptTo(Node.class)
    session=rootNode.getSession()
    
    rootNode.addNode("breadcrumb","nt:unstructured")
    session.save()
    Resource breadcrumbResource = rootResource.getChild("breadcrumb")
    modifiableValueMap = breadcrumbResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/content/breadcrumb")
    modifiableValueMap.put("showHidden","false")
    modifiableValueMap.put("hideCurrent","false")
    modifiableValueMap.put("startLevel","5")
    resourceResolver.commit()
    
    rootNode.addNode("reference","nt:unstructured")
    session.save()
    Resource refResource = rootResource.getChild("reference")
    modifiableValueMap = refResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def  nav_parsys_path =  hotelPath + "/jcr:content/root/hotel_navigation"
    modifiableValueMap.put("path",nav_parsys_path)
    resourceResolver.commit()
    
    rootNode.addNode("reference_holidays","nt:unstructured")
    session.save()
    Resource referenceHolidaysResource = rootResource.getChild("reference_holidays")
    modifiableValueMap = referenceHolidaysResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    modifiableValueMap.put("path",holidayOfferDetailsResource.path)
    resourceResolver.commit()

    jcrNode.addNode("header_parsys","nt:unstructured")
    session.save()
    Resource headerResource = jcrNodeResource.getChild("header_parsys")
    modifiableValueMap = headerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node headerNode = headerResource.adaptTo(Node.class)
    
    headerNode.addNode("reference","nt:unstructured")
    session.save()
    Resource headerRefResource = headerResource.getChild("reference")
    modifiableValueMap = headerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def header_parsys_path = globalOfferPageResource.getParent().getParent().getParent().getParent().getParent().path + "/jcr:content/header_parsys"
    modifiableValueMap.put("path",header_parsys_path)
    resourceResolver.commit()
    
    jcrNode.addNode("banner_parsys","nt:unstructured")
    session.save()
    Resource bannerResource = jcrNodeResource.getChild("banner_parsys")
    modifiableValueMap = bannerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node bannerNode = bannerResource.adaptTo(Node.class)
    
    bannerNode.addNode("reference","nt:unstructured")
    session.save()
    Resource bannerRefResource = bannerResource.getChild("reference")
    modifiableValueMap = bannerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def banner_parsys_path = hotelPath+ "/jcr:content/banner_parsys/hotel_banner"
    modifiableValueMap.put("path",banner_parsys_path)
    resourceResolver.commit()
    
    jcrNode.addNode("footer_parsys","nt:unstructured")
    session.save()
    Resource footerResource = jcrNodeResource.getChild("footer_parsys")
    modifiableValueMap = footerResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/parsys")
    resourceResolver.commit()
    Node footerNode = footerResource.adaptTo(Node.class)
    
    footerNode.addNode("reference","nt:unstructured")
    session.save()
    Resource footerRefResource = footerResource.getChild("reference")
    modifiableValueMap = footerRefResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","foundation/components/reference")
    def footer_parsys_path = globalOfferPageResource.getParent().getParent().getParent().getParent().getParent().path + "/jcr:content/footer_parsys"
    modifiableValueMap.put("path",footer_parsys_path)
    resourceResolver.commit()
}
