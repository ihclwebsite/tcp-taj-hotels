 def buildQuery(page, term) {
       def queryManager = session.workspace.queryManager
      def statement = 'select * from nt:base where jcr:path like \''+page.path+'/%\' and sling:resourceType = \'' + term + '\''
       queryManager.createQuery(statement, 'sql')
 }

  
  page = getPage('/content/tajhotels/en-in/our-hotels/')
  final def query = buildQuery(page, 'tajhotels/components/structure/dining-details-page')
  final def result = query.execute()
  result.nodes.each{node ->
  try{
      
    if(node.hasProperty('cq:lastReplicationAction')){
        if(node.getProperty('cq:lastReplicationAction').getString().toLowerCase()=='activate'){
         deactivate(node.getPath()+'/root/specific_hotel_offer')
        }else{
        	println 'Ignoring '+node.getPath()
        }
    }else{
    	println 'Ignoring not ever replicated '+node.getPath()
    }
   
  }catch(exception){
      println exception
  }
  }
  
  
 