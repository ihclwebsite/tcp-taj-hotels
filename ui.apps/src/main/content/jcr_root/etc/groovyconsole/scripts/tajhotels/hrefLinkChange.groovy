package etc.groovyconsole.scripts.tajhotels

getPage("/content/tajhotels").recurse { page ->
    def content = page.node

    if(content && content.get("seoMetaTag")){
        def metaTag=content.get("seoMetaTag")
        //println page.path+" : \n "+metaTag

        def metaTagTrimmed= metaTag.trim()
        if(metaTagTrimmed.startsWith("<link rel=\"alternate\" hreflang=\"en\" href=")){
            def metaTagArr= metaTagTrimmed.split("<link")
            def metaTag1stEnTag= "<link"+metaTagArr[1]
            def metaEnTagRemoved=metaTagTrimmed.replace(metaTag1stEnTag, "")
            def hrefLink=metaTag1stEnTag.replace("<link rel=\"alternate\" hreflang=\"en\" href=\"", "").replace("\"/>", "")
            def hrefWwwArr=  hrefLink.split(".tajhotels.com/")
            def linkPart1=""
            def linkPart2=""
            linkPart1= hrefWwwArr[0]
            if(hrefWwwArr.length>1){
                linkPart2= hrefWwwArr[1]
            }
            def linkPart1SplitArr= linkPart1.split("//")
            def optionalSubDomainLinkName=""

            def modifiedHttpInitial=""
            optionalSubDomainLinkName=hrefWwwArr[0]
            if(linkPart1SplitArr.length>1 && !linkPart1.contains("www")){
                optionalSubDomainLinkName=optionalSubDomainLinkName

            }
            modifiedHttpInitial=optionalSubDomainLinkName
            if(!modifiedHttpInitial.contains(".tajhotels.com")){
                modifiedHttpInitial=optionalSubDomainLinkName+".tajhotels.com/"

            }
            if(modifiedHttpInitial.contains("//taj.tajhotels") || modifiedHttpInitial.contains("//vivanta.tajhotels") || modifiedHttpInitial.contains("//gateway.tajhotels")){
                modifiedHttpInitial=modifiedHttpInitial.replace("taj.", "www.")
                modifiedHttpInitial=modifiedHttpInitial.replace("vivanta.", "www.")
                modifiedHttpInitial=modifiedHttpInitial.replace("gateway.", "www.")
            }
            def pagePath= page.path
            def noContentPagePath= pagePath.replace("/content/tajhotels/", "")

            //for global pages
            def replacingValue=noContentPagePath
            def completeUrl= modifiedHttpInitial+replacingValue

            def replacingValueArr=replacingValue.split("/")

            if((replacingValueArr[replacingValueArr.length-1])=="en-in"){
                completeUrl=modifiedHttpInitial
            }

            if((replacingValueArr[replacingValueArr.length-1]).startsWith("jiva-spa")){
                if(modifiedHttpInitial.contains("https://jivaspa.tajhotels.com"))
                    modifiedHttpInitial=modifiedHttpInitial.replace("jivaspa.", "www.")
                completeUrl=modifiedHttpInitial+"/en-in/"+replacingValueArr[replacingValueArr.length-1]
            }

            if((replacingValueArr[replacingValueArr.length-1]).startsWith("hotels-in") || (replacingValueArr[replacingValueArr.length-1]).startsWith("restaurants-in-") || (replacingValueArr[replacingValueArr.length-1]).startsWith("about-")){
                completeUrl=modifiedHttpInitial+"en-in/destination/"+replacingValueArr[replacingValueArr.length-1]
            }else if(replacingValueArr.length>3 && ((replacingValueArr[3]=="taj") || (replacingValueArr[3]=="vivanta") || (replacingValueArr[3]=="gateway"))){
                completeUrl=modifiedHttpInitial+"en-in/"+replacingValueArr[replacingValueArr.length-1]
                if(replacingValueArr.length>4){
                    completeUrl=modifiedHttpInitial+"en-in/"+replacingValueArr[replacingValueArr.length-2]+"/"+replacingValueArr[replacingValueArr.length-1]
                }
                if(replacingValueArr.length>5){
                    completeUrl=modifiedHttpInitial+"en-in/"+replacingValueArr[replacingValueArr.length-3]+"/"+replacingValueArr[replacingValueArr.length-2]+"/"+replacingValueArr[replacingValueArr.length-1]
                    println replacingValueArr[4]+"899"
                }
            }
            completeUrl=completeUrl.replace(" ", "")
            if(completeUrl.endsWith("/")){
                completeUrl=completeUrl.substring(0, ((completeUrl.length()-1)))
            }
            //println "completeUrl2: "+completeUrl;
            completeUrl= "<link rel=\"alternate\" hreflang=\"en\" href=\""+completeUrl+"\"/>"
            //println metaTag1stEnTag;
            println page.path+" : \n read URL : "+metaTag
            //println "EN TAG: "+metaTag1stEnTag;
            //println "hrefValue: "+hrefLink;
            //println "hrefWwwArr: "+hrefWwwArr;
            //println "modifiedHttpInitial: "+modifiedHttpInitial;

            //println "replacingValue: "+replacingValue;
            println "completeUrl: "+completeUrl
            def finalUrl= completeUrl+metaEnTagRemoved
            println "final URL : "+finalUrl


            //if(page.path=="/content/tajhotels/en-in"){
            content.set("seoMetaTag", finalUrl)
            // }
            if(content.get("seoMetaTag")==finalUrl){
                println "Update SUCCESS"
            }
        }



        println "---------------------------"
    }else if (content && !content.get("seoMetaTag")) {
        //If seometaTag is not there do not process the node
        //println page.path
    }
    //println "----End of each recursion"
}

println "++++End of Program"