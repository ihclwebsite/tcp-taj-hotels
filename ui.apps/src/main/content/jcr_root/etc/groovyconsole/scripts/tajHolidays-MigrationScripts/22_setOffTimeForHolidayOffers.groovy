import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.text.ParseException

init()

def init() {
    def predicate = [
       "path": "/content/tajhotels/en-in/taj-holidays/destinations",
       "property":"sling:resourceType",
       "property.value":"tajhotels/components/content/holidays-destination/holidays-hotel-package-details",
       "p.limit":"-1"
       ]
   def query = createQuery(predicate)
   def result = query.result
   result.hits.each{
   hit ->
        if(validateIfDeactivatedOffer(hit.resource)) {
            String packageValidityEndDate=hit.getResource().adaptTo(ModifiableValueMap.class).get("packageValidityEndDate")
            if(null!=packageValidityEndDate && validateDateFormat(packageValidityEndDate)) {
                println packageValidityEndDate + " | " + hit.getResource().getParent().getParent().getParent().path
                setOffTime(hit.resource,toCalendar(packageValidityEndDate))
            }
        }
   }
}

def validateIfDeactivatedOffer(resource) {
    if(resource.getParent().getParent().adaptTo(ModifiableValueMap.class).get("cq:lastReplicationAction")=="Deactivate") {
        return false
    }
    return true
}

def validateDateFormat(dateString){
    
    SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy")
    try {
        Date d= format.parse(dateString)
        return true
    } catch (ParseException e) {
        return false
    }
}

def toCalendar(dateString) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
    Date date = sdf. parse(dateString)
    Calendar calObj = Calendar. getInstance()
    calObj. setTime(date)
    calObj.set(Calendar.MILLISECOND, 0)
    calObj.set(Calendar.SECOND, 0)
    calObj.set(Calendar.MINUTE, 0)
    calObj.set(Calendar.HOUR, 0)
    return calObj
}

def setOffTime(resource,calendarObj){
    ModifiableValueMap modMap = resource.getParent().getParent().adaptTo(ModifiableValueMap.class)
    modMap.put("offTime", calendarObj)
    resource.getResourceResolver().commit()
}