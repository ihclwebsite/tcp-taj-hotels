import javax.jcr.Node
import javax.jcr.Workspace
import org.apache.jackrabbit.commons.JcrUtils
import org.apache.sling.api.resource.Resource
import org.apache.sling.api.resource.ResourceResolver
import org.apache.sling.api.resource.ModifiableValueMap
import org.apache.commons.lang3.text.WordUtils

/* Script #7 - HotelMigration and its HolidayPackage Migration script - This script will create and then 
migrate all hotels and its packages under specific thrp/Destination path . The script can be modified to run all the Hotels in one go , 
but the risk of hanging is high (as this behaviour was observed in stage server , hence specify each thrp/destination path individually and then execute
*/

i=0
init()

def init(){
    def predicate = [
        "path": "/content/thrp/holidays/en-in/destinations/about-andaman",
        "property":"sling:resourceType",
        "property.value":"/apps/taj-core/pagecomponents/holidayHotelPage",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
    
        println "==============================================================="
        println  "Hotel From old content : " +  hit.node.path
        Node thrpHotelNode = hit.node
        Node tajHotelNode = getHitFromTajHotelSection(hit.node.getParent().getName())
       if(null!=tajHotelNode) {
            println  "Path From TajHotel : " + tajHotelNode.path
            setHotelInHolidayDestinations(tajHotelNode,thrpHotelNode)
        }
        else {
            println "MIGRATION UNSUCCESSFULL  " + hit.node.path + " as no Corresponding Hotel Fetched From Taj Section" 
            println "==============================================================="
        }
        
    }
}

def getHitFromTajHotelSection(searchKey){
    Node resultNode
    def predicate = [
        "path": "/content/tajhotels/en-in/our-hotels",
        "property":"sling:resourceType",
        "property.value":"tajhotels/components/structure/hotels-landing-page",
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
        if(searchKey.contains(hit.node.getParent().getName())) {
           resultNode=hit.node
        }
    }
    return resultNode
}

def setHotelInHolidayDestinations(tajHotelNode,thrpHotelJCRNode) {
    
    def destinationPath = tajHotelNode.getParent().getParent().getParent().path
    Resource nodeRes = resourceResolver.getResource(destinationPath+"/jcr:content")
    Node jcrNodeFromTaj = nodeRes.adaptTo(Node.class)
    
    String[] city = jcrNodeFromTaj.get("city")
    
    def predicate = [
        "path": "/content/tajhotels/en-in/holidays-landing-page/holidays-destination",
        "1_property":"sling:resourceType",
        "1_property.value":"tajhotels/components/structure/tajholidays-destination-landing-page",
        "2_property":"city",
        "2_property.value":city,
        "p.limit":"-1"
        ]
    def query = createQuery(predicate)
    def result = query.result
    result.hits.each{
    hit ->
            createHotelNodeUnder(hit.node.getParent().path,tajHotelNode.getParent().path,thrpHotelJCRNode.getParent())
        }
}

def createHotelNodeUnder(holidayDestinationPath,tajHotelPath,thrpHotelNode) {
    
    println "holidayDestinationPath " +holidayDestinationPath
    println "tajHotelPath " +tajHotelPath
    
    def destinationName = holidayDestinationPath.replaceAll("/content/tajhotels/en-in/holidays-landing-page/holidays-destination/holidays-in-","")
    def targetDestinationPath = holidayDestinationPath +"/packages-in-"+destinationName

    Resource targetDestinationResource = resourceResolver.getResource(targetDestinationPath)
    Node targetDestinationNode = targetDestinationResource.adaptTo(Node.class)
    
    def hotelPageName = thrpHotelNode.getName()
    
    String thrpHotelJCRpath = thrpHotelNode.path+"/jcr:content"
    Resource thrpHotelJCRResource =resourceResolver.getResource(thrpHotelJCRpath)
    Node thrpHotelJCRnode = thrpHotelJCRResource.adaptTo(Node.class)
    def thrpHotelJCRtitle = thrpHotelJCRnode.get("jcr:title")
    String[] thrpHotelJCRtags = thrpHotelJCRnode.get("cq:tags")
    
    def sess = targetDestinationNode.getSession()
    targetDestinationNode.addNode(hotelPageName,"cq:Page")
    sess.save()
    Resource hotelResource = targetDestinationResource.getChild(hotelPageName)
    Node hotelNode = hotelResource.adaptTo(Node.class)
    
    hotelNode.addNode("jcr:content","cq:PageContent")
    sess.save()
    Resource hotelJCRresource = hotelResource.getChild("jcr:content")
    ModifiableValueMap modifiableValueMap = hotelJCRresource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/structure/tajholidays-destination-hotel-page")
    modifiableValueMap.put("containerStyles","our-hotels-layout")
    modifiableValueMap.put("cq:template","/conf/tajhotels/settings/wcm/templates/tajholidays-destination-landing-page")
    modifiableValueMap.put("searchKey","hoidayHotel")
    if(null!=thrpHotelJCRtitle){
        modifiableValueMap.put("jcr:title",thrpHotelJCRtitle)
    }
    if(null!=thrpHotelJCRtags){
        modifiableValueMap.put("cq:tags",thrpHotelJCRtags)
    }
    resourceResolver.commit()
    
    Node hotelJCRnode = hotelJCRresource.adaptTo(Node.class)
    hotelJCRnode.addNode("root","nt:unstructured")
    sess.save()
    Resource rootResource = hotelJCRresource.getChild("root")
    modifiableValueMap = rootResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/responsivegrid")
    resourceResolver.commit()
    
    Node rootNode = rootResource.adaptTo(Node.class)
    rootNode.addNode("holidays_hotel_detail","nt:unstructured")
    sess.save()
    Resource hotelDetailResource = rootResource.getChild("holidays_hotel_detail")
    modifiableValueMap = hotelDetailResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/content/holidays-destination/holidays-hotel-details")
    modifiableValueMap.put("hotelPath",tajHotelPath)
    modifiableValueMap.put("hotelDiscountedRate","")
    modifiableValueMap.put("hotelActualRate","")
    modifiableValueMap.put("ratePerNightlabel","Starting Rate/Night")
    modifiableValueMap.put("ticPoints","")
    modifiableValueMap.put("epicurePoints","")
    resourceResolver.commit()
    
    def packageCreationStatusFlag=createHolidayPackagesUnder(hotelNode,thrpHotelNode)
    if(packageCreationStatusFlag) {
        println "Packages Successfully migrated under the Hotel Path " + hotelNode.path
    }
    else {
        println "No packages Found under the Hotel Path " + hotelNode.path
    }
}

def createHolidayPackagesUnder(hotelNode,thrpHotelNode) {
    
    def packageCreationStatus = false
    Resource thrpHotelNodeRes = resourceResolver.getResource(thrpHotelNode.path)
    Iterable<Resource> allPackageResources = thrpHotelNodeRes.getChildren()
    for (Resource packageResource : allPackageResources) {
        Node packageNode = packageResource.adaptTo(Node.class)
        if("jcr:content"!=packageNode.getName()) {
            packageCreationStatus =true
            buildHolidayPackageUsing(hotelNode,packageNode)
        }
    }
    return packageCreationStatus
}

def buildHolidayPackageUsing(hotelNode,thrpPackageNode) {
    
    Resource hotelResource = resourceResolver.getResource(hotelNode.path)
    String thrpPackageName = thrpPackageNode.getName()
    String thrpPackageJCRpath = thrpPackageNode.path+"/jcr:content"
    Resource thrpPackageJCRres = resourceResolver.getResource(thrpPackageJCRpath)
    Node thrpPackageJCRnode =thrpPackageJCRres.adaptTo(Node.class)
    
    Resource thrpPackageJCRImageRes =thrpPackageJCRres.getChild("image")
    Node thrpPackageImageNode = thrpPackageJCRImageRes.adaptTo(Node.class)
    
    def thrpJCRtitle =  thrpPackageJCRnode.get("jcr:title")
    String[] thrpcqTags = thrpPackageJCRnode.get("cq:tags")
    def thrpDescription = thrpPackageJCRnode.get("description")
    def thrpAccessCode = thrpPackageJCRnode.get("accessCode")
    def thrpOffer = thrpPackageJCRnode.get("offer")
    def thrpOfferType = thrpPackageJCRnode.get("offerType")
    def thrpPackageImagePath = thrpPackageImageNode.get("image3_2")
    
    def sess = hotelNode.getSession()
    hotelNode.addNode(thrpPackageName,"cq:Page")
    sess.save()
    
    Resource packageResource = hotelResource.getChild(thrpPackageName)
    Node packageNode = packageResource.adaptTo(Node.class)
    packageNode.addNode("jcr:content","cq:PageContent")
    sess.save()
    
    Resource packageJCRresource = packageResource.getChild("jcr:content")
    ModifiableValueMap modifiableValueMap = packageJCRresource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/structure/tajholidays-base-page")
    modifiableValueMap.put("containerStyles","our-hotels-layout")
    modifiableValueMap.put("cq:template","/conf/tajhotels/settings/wcm/templates/taj-holidays-base-page-template")
    modifiableValueMap.put("searchKey","hoidayPackages")
    if(null!=thrpJCRtitle){
        modifiableValueMap.put("jcr:title",thrpJCRtitle)
    }
    if(null!=thrpcqTags){
        modifiableValueMap.put("cq:tags",thrpcqTags)
    }
    if(null!=thrpAccessCode){
        modifiableValueMap.put("accessCode",thrpAccessCode)
    }
    if(null!=thrpOffer){
        modifiableValueMap.put("offer",thrpAccessCode)
    }
    if(null!=thrpOfferType){
        modifiableValueMap.put("offerType",thrpOfferType)
    }
    if(null!=thrpDescription){
        modifiableValueMap.put("jcr:description",thrpDescription)
    }
    if(null!=thrpPackageImagePath){
        modifiableValueMap.put("packageImagePath",thrpPackageImagePath)
    }
    resourceResolver.commit()
    
    Node packageJCRnode = packageJCRresource.adaptTo(Node.class)
    packageJCRnode.addNode("root","nt:unstructured")
    sess.save()
    Resource rootResource = packageJCRresource.getChild("root")
    modifiableValueMap = rootResource.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","wcm/foundation/components/responsivegrid")
    resourceResolver.commit()
    
    Node rootNode = rootResource.adaptTo(Node.class)
    rootNode.addNode("holidays_hotel_package","nt:unstructured")
    sess.save()
    Resource newPackageRes = rootResource.getChild("holidays_hotel_package")
    modifiableValueMap = newPackageRes.adaptTo(ModifiableValueMap.class)
    modifiableValueMap.put("sling:resourceType","tajhotels/components/content/holidays-destination/holidays-hotel-package-details")
    modifiableValueMap.put("actualRate","")
    modifiableValueMap.put("bookNowLabel","Book Now")
    modifiableValueMap.put("cancellationPolicy","")
    modifiableValueMap.put("discountedRate","")
    modifiableValueMap.put("lastFewRoomsAvailableLabel","Last few rooms available")
    if(null!=thrpAccessCode) {
         modifiableValueMap.put("offerRateCode",thrpAccessCode)
    }
    if(null!=thrpJCRtitle) {
         modifiableValueMap.put("packageTitle",thrpJCRtitle)
    }
    if(null!=thrpDescription){
        modifiableValueMap.put("rateDescription",thrpDescription)
    }
    resourceResolver.commit()
}
