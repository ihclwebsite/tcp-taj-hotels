$( document ).ready( function() {
    initConfirmationPage();
    
    // alert("inside merchantparam if block");
    if (dataCache.session.getData('bookingDetailsRequest') === null
            && sessionStorage.getItem('bookingDetailsRequest') === null) {
        console.log('booking details not found');
    } else {

        // open cancel reservation popup
        openCancelReservationPopup();

        doCommit();
    }

});     


function openCancelReservationPopup() {
	var cancelReservationBtn = $(".open-cancel-reservation.inline-block");
	if(cancelReservationBtn && cancelReservationBtn.length > 0) {
		$(".open-cancel-reservation.inline-block").each(function(){
			$(this).on("click", function(){
				$('#confirmReservationCancel').removeClass('d-none');
			});
		});
	}
}


function doCommit(){
        // payment page
        var jsonObject;
        var hoteladdress;
        if (sessionStorage.getItem('bookingDetailsRequest')){
            var sessionRequest = sessionStorage.getItem('bookingDetailsRequest');
			if(sessionRequest) {
				jsonObject = JSON.parse(sessionRequest);
			}
            dataCache.session.setData('bookingDetailsRequest',sessionRequest);
            sessionStorage.removeItem('bookingDetailsRequest');
        }
        var jsonStringObject = dataCache.session.getData('bookingDetailsRequest')
        if(!jsonStringObject){
            var popupParams = {
                    title: 'Booking!',
                    description: 'Failed to find Booking Details ',
                    
               }
               warningBox( popupParams );
			return;
        }
        jsonObject = JSON.parse(jsonStringObject);
        if(jsonObject && jsonObject.totalAmountAfterTax){
            var finalAmount = jsonObject.totalAmountAfterTax;
            var ecardMinValue = $('.data-ecard-details').attr("data-ecard-min-value") || 1000;
            var ecardMaxValue = $('.data-ecard-details').attr("data-ecard-max-value") || 1000000;
            var purchaseValuePct = $('.data-ecard-details').attr("data-ecard-purchaseValuePct") || 0;
            var defaultValueIncPct = $('.data-ecard-details').attr("data-ecard-defaultValueIncPct") || 100;
            finalAmount = parseInt(parseInt(finalAmount) * ( defaultValueIncPct / 100 ));
            if(finalAmount >= ecardMinValue){
                if(ecardMaxValue < finalAmount){
                    finalAmount = ecardMaxValue;
                }
                $(".gift-card-btn-wrp #total-amount").text(finalAmount);
                $(".gift-card-btn-wrp #amount-to-be-paid").text(Math.ceil(finalAmount * ((100 - purchaseValuePct)/100)));
            }else{
                $(".gift-card-btn-wrp").remove();
                $("#conformEnrollCon").hide();
            }
            
        }else{
            $(".gift-card-btn-wrp").remove();
            $("#conformEnrollCon").hide();
        }

        $(".gift-card-btn").click(function(){
            popUpGiftCard();
        });
        
        var giftcardResponse;
        if (sessionStorage.getItem('giftcardResponseString') && sessionStorage.getItem('paymentStatus')){
            var sessionGiftcardResponseString = sessionStorage.getItem('giftcardResponseString');
            if(sessionGiftcardResponseString) {
				giftcardResponse = JSON.parse(sessionGiftcardResponseString);
			}
            sessionStorage.removeItem('giftcardResponseString');
            sessionStorage.removeItem('paymentStatus');
            if(giftcardResponse && giftcardResponse.paymentStatus){
                giftcardCreationStatusPopup(giftcardResponse);
            }else{
                var gcPopupParams = {
                    title: 'Payment Transaction Failed!',
                    description: 'It appears the transaction has failed at the payment gateway. In case the amount has been debited it will be refunded back to your payment method automatically within 3 working days. For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .',
               }
               warningBox( gcPopupParams );
            }
        }
        
        var symbolCur = jsonObject.currencyCode;
        
        if(symbolCur != 'INR'){
            $(".gift-card-btn-wrp").remove();
            $("#conformEnrollCon").hide();
        }
            
        // [TIC-FLOW]
        ticBookHotelConfirmationFlow();
            
        // [IHCLCB START]
        showEntityInformation();
        // [IHCLCB END]
        dataCache.session.setData('selectedCurrencyString', symbolCur);
        if(jsonObject.currencyCode){
            var symbolInHeader = $($($.find("[data-currency-id='"+jsonObject.currencyCode+"']")[0]).find('.header-dd-option-currency')[0]).text().trim();
            if( symbolInHeader ){
                symbolCur = symbolInHeader;
                 setActiveCurrencyInDom(symbolCur, jsonObject.currencyCode, jsonObject.currencyCode);
                setCurrencyCache(symbolCur, jsonObject.currencyCode, jsonObject.currencyCode);
            }else{
                var newCurrency = jsonObject.currencyCode;
                var newCurrencyHtml = '<li class="cm-each-header-dd-item" data-currency-id="'+newCurrency+'"><a class="inline-block"><div class="inline-block header-dd-option-currency">'+newCurrency+'</div><div class="inline-block header-dd-option-text">'+newCurrency+'</div></a></li>';
                $('.header-currency-options .cm-header-dd-list-con').append(newCurrencyHtml);
                setActiveCurrencyInDom(jsonObject.currencyCode, jsonObject.currencyCode, jsonObject.currencyCode);
                setCurrencyCache(jsonObject.currencyCode, jsonObject.currencyCode, jsonObject.currencyCode);
            }


       }
        dataCache.session.setData('selectedCurrency', symbolCur);
       
        var currencyData;
        
        if(jsonObject != null && jsonObject.itineraryNumber){
            $('#itineraryNumber').text(jsonObject.itineraryNumber);
        }
        var bODataLayer=""
        if(dataCache.session.getData("bookingOptions")){
        if(dataCache.session.getData("bookingOptions").dataLayerData){
         bODataLayer = dataCache.session.getData("bookingOptions").dataLayerData;
        }
        }
        if (jsonObject.payments.payAtHotel) {
            processPayatHotel(jsonObject);
    
        } else if (jsonObject.payments.payOnlineNow) {
            processPayOnline(jsonObject);
        }else{
            console.warn('method not defined');
        }
        
        
        // [TIC-FLOW]
        printTicRoomRedemptionSummary(jsonObject);
    
        currencyData = dataCache.session.getData('selectedCurrency');
        if(currencyData != undefined){
         // [TIC-FLOW]
            var userDetails = getUserData(); 
            var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            var redeemPoints = 0;
            if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession &&  ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                if(ticRoomRedemptionObjectSession.ticpoints
                        && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                    $(".cart-currency-symbol").text('TIC');
                }else if(ticRoomRedemptionObjectSession.ticpoints 
                        && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                    $(".cart-currency-symbol").text('EPICURE');
                }else if(ticRoomRedemptionObjectSession.ticpoints 
                        && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                    $(".cart-currency-symbol").text('TAP');
                }else if(ticRoomRedemptionObjectSession.ticpoints 
                        && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                    $(".cart-currency-symbol").text('TAPP Me');
                }else{
                    $(".cart-currency-symbol").text(currencyData.trim());
                }
                $(".gift-card-btn-wrp").remove();
            }else{
                $(".cart-currency-symbol").text(currencyData.trim());
            }
        }
        if($('.summary-charges-room-total-price').not('.cancelled-room-style').length==5){
          $('.edit-bkng-btn').remove();
        }
         if(getQueryParameter('fromFindReservation')=='true'){
                $('.hide-in-find-reservation').hide();
            $('.show-in-find-reservation').show();
         }
         // modify booking script
         $('.modify-booking-link').on('click',function(){
            // bookedOptions is initiated in book-a-stay.js
            // clearModifyBookingCache();
			if(dataCache.session.getData('bookingDetailsRequest')) {
				bookedOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
			}
            var modifyBookingState = $(this).data('modify');
            dataCache.session.setData('modifyBookingState',modifyBookingState);
            if(modifyBookingState=='modifyRoomType' || modifyBookingState=='modifyRoomOccupancy'){
              var $modifyRoomCardContainer = $(this).closest('.confirmation-selected-rooms-card');
              var modifyRoomReservationNumber = $modifyRoomCardContainer.find('.room-reservation-number').text();
              $(bookedOptions.roomList).each(function(index){
                if(this.reservationNumber==modifyRoomReservationNumber){
                  bookedOptions.roomList[index].modifyRoom = modifyBookingState;
                  return false;
                }
              });
            }
            if(modifyBookingState=='modifyRoomType'){     
              var roomTypeName = $(this).closest('#roomTypeCode').text();                         
              bookedOptions.roomTypeName = roomTypeName;            
            }
            if(modifyBookingState=='modifyDate' || modifyBookingState=='modifyGuest'){
              $(bookedOptions.roomList).each(function(index){
                if(this.resStatus!='Cancelled'){
                  bookedOptions.roomList[index].modifyRoom = modifyBookingState;              
                }else{
                  bookedOptions.roomList[index].modifyRoom = false;
                }
              });
            }
            dataCache.session.setData('bookingDetailsRequest',JSON.stringify(bookedOptions));
            createModifyBookingDetailsJson(modifyBookingState);
            if(modifyBookingState !='modifyGuest'){
              window.location.href = jsonObject.hotel ? jsonObject.hotel.hotelResourcePath+'/rooms-and-suites.html?modifyBooking=true' : "/html";
            }        
         });
         // end of modify booking script
        $('.summary-charges-room-total-price').click(function () {     
            $(this).find('.nightly-rates').slideToggle();
            var $dropDownArrow = $(this).find('.nightly-rates-dropdown-image');
            // $('.nightly-rates-dropdown-image').not($dropDownArrow
            // ).removeClass('cm-rotate-neg-180deg');;
            $dropDownArrow.toggleClass('cm-rotate-neg-180deg');
        });
        
        $('#summary-taxes-charges').on( "click", function() {
            $('.taxes-dropdown-image').toggleClass("cm-rotate-icon-180");
            $('.all-type-of-taxes').slideToggle();
        });     
        
        // [TIC-FLOW]
        var userDetails = getUserData(); 
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(userDetails && userDetails.card && userDetails.card.tier){
            if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                $("#summary-taxes-charges").addClass('d-none');
            }
        }
        $(".modify-booking-link").hide();
        
        /*
         * This function pushes the analytics data to dataLayer
         * 
         */
        prepareConfirmationData(jsonObject,bODataLayer);
        
    
// [IHCLCB start]
        if($(window).width() < 768) {
            $('.ihcl-theme .mob-drpdwn-cont-ihclcb .add-drpdwn-arrow-ihclcb').click(function() {
                var $cont = $(this).closest('.mob-drpdwn-cont-ihclcb');
                if($cont.hasClass('open-con-drpdwn')) {
                    $cont.removeClass('open-con-drpdwn');
                } else {
                    $cont.addClass('open-con-drpdwn');
                }
            });
        }
// [IHCLCB End]
    }
    
    function validateInputFields() {
        var flag = true;
        $('.gift-card-details-wrp input.sub-form-mandatory,.gift-card-details-wrp select.sub-form-mandatory').each(function() {
            if ($(this).val() == "" || $(this).hasClass('invalid-input')) {
                $(this).addClass('invalid-input');
                flag = false;
                invalidWarningMessage($(this));
            }
        });
         $(".gift-card-details-wrp [type='checkbox']").each(function(){
            if(!$(this).is(":checked")){
                $(this).parent().find(".sub-form-input-warning").show();
                flag =false;
            }else{
                $(this).parent().find(".sub-form-input-warning").hide();
            }
        })
        return flag ;
    }
    function popUpGiftCard() {
        
        var presentScroll = $(window).scrollTop();
        var thisLightBox = $(".mr-gift-card-popup").css('display', 'block')
               .addClass('active-popUp');
        $(".cm-page-container").addClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling','unset');
        giftCardAutoPoputate();

        $(document).keydown(function(e) {
           if (($('.active-popUp').length) && (e.which === 27)) {
               $('.showMap-close').trigger("click");
           }
        });
        
        $('.showMap-close').click(function() {
           thisLightBox.hide().removeClass('active-popUp');
           $(".cm-page-container").removeClass('prevent-page-scroll');
           $('.the-page-carousel').css('-webkit-overflow-scrolling','touch');
           $(window).scrollTop(presentScroll);
        });
    }
    function giftCardAutoPoputate(){
        var bookingDetail = JSON.parse(dataCache.session.getData("bookingDetailsRequest"));
        var ecardMinValue = Number($('.data-ecard-details').attr("data-ecard-min-value") || 1000);
        var ecardMaxValue = Number($('.data-ecard-details').attr("data-ecard-max-value") || 1000000);
        var purchaseValuePct = Number($('.data-ecard-details').attr("data-ecard-purchaseValuePct") || 0);
        var defaultValueIncPct = Number($('.data-ecard-details').attr("data-ecard-defaultValueIncPct") || 100);
        if(bookingDetail){
            var giftcardamount = parseInt(bookingDetail.totalAmountAfterTax);
            giftcardamount = parseInt(giftcardamount * (defaultValueIncPct / 100));
            if(ecardMaxValue < giftcardamount){
                giftcardamount = ecardMaxValue;
            }
            var payableValue = parseInt(Math.ceil( giftcardamount * ((100 - purchaseValuePct)/100)));
            $('#giftcardCustomerTitle').val(bookingDetail.guest.title);
            $('#giftcardCustomerTitle').data("selectBox-selectBoxIt").refresh();
            $(".gift-card-details-wrp [name='customerFirstName']").val(bookingDetail.guest.firstName);
            $(".gift-card-details-wrp [name='customerLastName']").val(bookingDetail.guest.lastName);
            $(".gift-card-details-wrp [name='customerEmail']").val(bookingDetail.guest.email);
            $(".gift-card-details-wrp [name='customerPhoneNumber']").val(bookingDetail.guest.phoneNumber);
            $(".gift-card-details-wrp [name='customerAmount']").val(giftcardamount);
            $(".gift-card-details-wrp [name='customerPayableAmount']").val(payableValue);
            $("select[data-form-id='title']").val(bookingDetail.guest.title);
        }
        $(".gift-card-details-wrp [name='customerAmount']").change(function(){
            
            var pecntAmount = Number($(this).val());
            var payAmount = parseInt(pecntAmount * ((100 - purchaseValuePct)/100));
            $(".gift-card-details-wrp [name='customerPayableAmount']").val(payAmount);
            
            if((ecardMaxValue >= pecntAmount ) && ( pecntAmount >= ecardMinValue)){
                $(".sub-form-input-warning-min-amount").text("").hide();
                $(".gift-card-submit-btn").removeAttr("disabled","disabled").css({"opacity":"1","cursor": "pointer"});
            }else{
                var warningMessage = "";
                if(pecntAmount < ecardMinValue){
                    warningMessage = "Minimum card value should be more than Rs." + ecardMinValue;
                }else if(ecardMaxValue < pecntAmount){
                    warningMessage = "Maximum card value should be less than Rs." + ecardMaxValue;
                }
                
                $(".sub-form-input-warning-min-amount").text(warningMessage).show().css({"color":"red","font-size": "10px"});;
                $(".gift-card-submit-btn").attr("disabled","disabled").css({"opacity":"0.7","cursor": "not-allowed"});
            }
            
        })
        $(".gift-card-submit-btn").click(function(){
            if(validateInputFields()){
                callGiftcardInitializationService();
            }else{
                $('.invalid-input').eq(0).focus();
            }
        });
        $(".gift-card-details-wrp [type='checkbox']").change(function(){
            if(!$(this).is(":checked")){
                $(this).parent().find(".sub-form-input-warning").show();
            }else{
                $(this).parent().find(".sub-form-input-warning").hide();
            }
        })

    }
    
    function getGiftcardCustomerDetails(){
         var giftcardRequest = {};
         giftcardRequest.title = $('#giftcardCustomerTitle option:selected').text();
         giftcardRequest.firstName = $('input[name=customerFirstName]').val() ;
         giftcardRequest.lastName = $('input[name=customerLastName]').val() ;
         giftcardRequest.email = $('input[name=customerEmail]').val() ;
         giftcardRequest.phoneNumber = $('input[name=customerPhoneNumber]').val() ;
         giftcardRequest.currenyType = dataCache.session.getData('selectedCurrencyString');
         giftcardRequest.amount = $('input[name=customerAmount]').val() ;
         giftcardRequest.payableAmount = $('input[name=customerPayableAmount]').val() ;
         giftcardRequest.itineraryNumber = $('#itineraryNumber').text() ;
         
         return giftcardRequest;
     }
     
    function callGiftcardInitializationService(){
        console.log("Ajax call Started")
        var giftcardCustomerDetails = getGiftcardCustomerDetails();
        var popupParams = {};
        popupParams.title = 'E-Card Failed!';
        $('.mr-gift-card-popup').hide()
        $('body').showLoader(true);
        $.ajax({
            type: 'post',
            url : '/bin/giftcardInitializeServlet', 
            data: 'giftcardCustomerDetails=' + JSON.stringify(giftcardCustomerDetails),
            success: function(successResponse){
                console.log("successResponse: " + successResponse);
                successResponse = JSON.parse(successResponse);
                if(successResponse == undefined || successResponse == "" || successResponse.length == 0){
                    $('body').hideLoader(true);
                    popupParams.title = 'E-Card Failed!';
                    popupParams.description = 'Something went Wrong. Refreshing Page';
                    warningBox( popupParams );
                    location.reload();
                }else{
                    if(successResponse.status){
                        if(successResponse.apiWebProperties){
                            dataCache.session.setData("properties",JSON.parse(successResponse.apiWebProperties));
                        }
                        if(successResponse.giftcardCustomerDetails){
                            dataCache.session.setData("giftcardCustomerDetails",JSON.parse(successResponse.giftcardCustomerDetails));
                        }
                        $('body').hideLoader(true);
                        var $form=$(successResponse.CCAvenueForm);
                        $('body').append($form);
                        $form.submit();
                    }else {
                        $('body').hideLoader(true);
                        popupParams.description = successResponse.message;
                        warningBox( popupParams );
                    }   
                }
            },fail: function(failedResponse){
                $('body').hideLoader(true);
                console.log("failedResponse: "+failedResponse);
                popupParams.description = 'The Taj Experiences eCard initialization failed. Please try again later.';
                warningBox( popupParams );
                
            },error: function(errorResponse){
                $('body').hideLoader(true);
                console.log("errorResponse: "+errorResponse);
                popupParams.description = 'The Taj Experiences eCard initialization interrupted. Please try again later.';
                warningBox( popupParams );
            }
    
        });
    }
     
    function giftcardCreationStatusPopup(giftcardResponse){
        var popupParams = {};
        if(giftcardResponse.status){
            popupParams.title = 'Order placed successfully';
            popupParams.description = "The Taj Experiences e-card is on its way to your inbox and should be delivered shortly. Your payment tracking ID is " + giftcardResponse.ccAvenueTrackingId + ". For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .";
        }else {
            popupParams.title = 'E-card order failed!';
            popupParams.description =  "Online activation of e-gift card was not successful. Your order number " + giftcardResponse.ccAvenueTrackingId + " will be processed offline and you will receive the e gift card within 2 business days. You can get in touch with us on reservations@tajhotels.com / 1800 111 825 for any further support.";
        }
        warningBox( popupParams );
    }
     function giftcardCreation(giftcardDetails,ccAvenueParams){
         
         var giftcardCustomerDetails = dataCache.session.getData("giftcardCustomerDetails");
         dataCache.session.removeData("giftcardCustomerDetails")
         
         var properties = dataCache.session.getData("properties");
         dataCache.session.removeData("properties");
         
         var ccAvenueParamsData = dataCache.session.getData("ccAvenueParams");
         dataCache.session.removeData("ccAvenueParams")
         
         console.log("Ajax call Started");
         var popupParams = {};
         popupParams.title = 'E-card order failed!';
         $('body').showLoader(true);
         $.ajax({
             type: 'post',
             url : '/bin/giftcardPostPaymentServlet', 
             data: 'giftcardCustomerDetails=' + JSON.stringify(giftcardCustomerDetails)
                     + '&properties=' + JSON.stringify(properties)
                     +'&ccAvenueParamsData=' + JSON.stringify(ccAvenueParamsData),
             success: function(successResponse){
                 console.log("successResponse: "+successResponse);
                 successResponse = JSON.parse(successResponse);
                 $('body').hideLoader(true);
                 if(successResponse.status){
                     popupParams.title = 'Order placed successfully';
                     popupParams.description = "The Taj Experiences e-card is on its way to your inbox and should be delivered shortly. Your payment tracking ID is " + successResponse.ccAvenueTrackingId + ". For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .";
                 }else {
                     popupParams.description =  "It appears the e-card transaction has failed. The e-card will be sent to your inbox within 3 working days. Your payment tracking ID is " + successResponse.ccAvenueTrackingId + ". In case the amount you would prefer a refund, contact us to have the refund processed to your payment method within 3 working days. For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .";
                 }
                 warningBox( popupParams );
             },fail: function(failedResponse){
                 $('body').hideLoader(true);
                 popupParams.description = 'Taj Experiences eCard creation failed. Please try again later.';
                 console.log("failedResponse: "+failedResponse);
                 warningBox( popupParams );
             },error: function(errorResponse){
                 $('body').hideLoader(true);
                 popupParams.description = 'Taj Experiences eCard creation got interrupted. Please try again later.';
                 console.log("errorResponse: "+errorResponse);
                 warningBox( popupParams );
             }
         });
         
     }
    
    function setActiveCurrencyInDom(currencySymbol,currency,currencyId) {
    
            $($.find("[data-inject-key='currencySymbol']")[0]).text(currencySymbol);
            $($.find("[data-inject-key='currency']")[0]).text(currency);
            $($.find("[data-selected-currency]")[0]).attr("data-selected-currency",currencyId);
        }
    
    function clearSessionStorage(){
    
        // Analytic data call
            if(globalBookingOption && globalBookingOption.selection){
              var selectionArr1= globalBookingOption.selection;
              selectionArr1.forEach(function(item, index){
                  selectionAr=selectionArr1[index] || item;
                  prepareOnRoomSelect(ViewName, event);
              })
            }
      // dataCache.session.removeData('bookingOptions');
    }
    
    function processPayatHotel(jsonResponse){
        // dataCache.session.removeData('commitStatus');
      // trigger_bookingConfirm(jsonResponse) //No longer required, commenting
        // for
        // stability
      if(jsonResponse == undefined || jsonResponse == "" || jsonResponse.length == 0){
         var popupParams = {
             title: 'Something went Wrong!',
             description: 'Could not find confirmation details.',
             }
         warningBox( popupParams );
    
         $('#tickstatus').hide();
         $('.identification-container').hide();
         $('.special-request-section').hide();
         $('.flight-details-section').hide();
         $('.explore-more-btn').hide();
         $('.payment-section').hide();
         $('.chk-in-out-con').hide();
         $('.hotel-details-con').hide();
         $('.guest-details-con').hide();
         $('.confirmation-details-con').hide();
         $('.payment-details-section').hide();
         $('.confirmation-heading').hide();
         $('.download-options-print').hide();
         $('#thank-you').hide();
         $('.confirmation-text').append("Could not find confirmation details.");
     }else if(jsonResponse.success){
         if(jsonResponse.warnings){
           var res = jsonResponse.warnings;
           $('#Warning').html(res);
        }
        $('#bookingStatus').html("Booking Confirmed!");
        injectForConformationPage(jsonResponse);
        injectTicPoints(jsonResponse);
     }else if(!jsonResponse.success){
            $('#tickstatus').hide();
            $('.confirmation-heading').hide();
            // [TIC-FLOW]
            var userDetails = getUserData();
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            if(userDetails && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow && jsonResponse.errorMessage && jsonResponse.errorMessage.length>0){
                $('.confirmation-text').html("Booking Failed."+ jsonResponse.errorMessage +".Please check with support team.....");
                $(".tic-room-redemption-info").hide();
            }else{
                $('.confirmation-text').html("Booking Failed. Please check with support team.....");
            }
            showResidsFailed(jsonResponse);
     }else if(jsonResponse.partialBooking){
        var i;
        var roomdata="";
        var roomStatustoJson=JSON.parse(commitStats);
        $('.confirmation-text').html("Could not confirm all rooms. Please see below the list of confirmed rooms and contact the support team.");
        showResidsPartial(roomStatustoJson);
        injectTicPoints(roomStatustoJson);
     }
       clearSessionStorage();
    }
    
    function processPayOnline(jsonResponse){
       
        // trigger_bookingConfirm(jsonResponse); //no longer required,
        // commenting
        // for stability
        var confirmationstatus=sessionStorage.getItem('status'); 
        sessionStorage.removeItem('status');
    
         if(jsonResponse == undefined || jsonResponse == "" || jsonResponse.length == 0){
                var popupParams = {
                               title: 'Something went Wrong!',
                               description: 'Could not find confirmation details.',
                           }
                           warningBox( popupParams );
    
    
                 $('#tickstatus').hide();
                 $('.identification-container').hide();
                 $('.special-request-section').hide();
                 $('.flight-details-section').hide();
                 $('.explore-more-btn').hide();
                 $('.payment-section').hide();
                 $('.chk-in-out-con').hide();
                 $('.hotel-details-con').hide();
                 $('.guest-details-con').hide();
                 $('.confirmation-details-con').hide();
                 $('.payment-details-section').hide();
                 $('.confirmation-heading').hide();
                 $('.download-options-print').hide();
                 $('#thank-you').hide();
                 $('.confirmation-text').append("Could not find confirmation details.");
            }else{
                if(jsonResponse.success){ 
                    if(jsonResponse.warnings){
                        $('#Warning').html(jsonResponse.warnings);
                     }
                    $('#bookingStatus').html("Booking Confirmed!");
                    injectForConformationPage(jsonResponse);
                    injectTicPoints(jsonResponse);
                }else if(!jsonResponse.success){
                    $('#tickstatus').hide();
                    // [TIC-FLOW]
                    var userDetails = getUserData();
                    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                    if(userDetails && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow && jsonResponse.errorMessage && jsonResponse.errorMessage.length>0){
                        $('.confirmation-text').html("Booking Failed."+ jsonResponse.errorMessage +".Please check with support team for confirmation.");
                        $(".tic-room-redemption-info").hide();
                    }else{
                        $('.confirmation-text').html("Booking Failed. Please check with support team for confirmation.");
                    }
                    
                    showResidsFailed(jsonResponse);
                }else if(jsonResponse.resStatus != null && resStatus.resStatus == "Ignore"){
                    $('#tickstatus').hide();
                    $('.confirmation-text').html("Reservation Expired. Please check with support team.....");
                    showResidsIgnored(jsonResponse);
                }else if(jsonResponse.partialBooking){
                    $('.confirmation-text').html("Could not confirm all rooms. Please see below the list of rooms and contact the support team.");
                    showResidsPartial(jsonResponse);
                    injectTicPoints(jsonResponse);
                }else{
                    $('#tickstatus').hide();
                    $('.confirmation-text').html("Booking failed. Please check with support team.");
                    showResidsFailed(jsonResponse);
                }
             }
         clearSessionStorage();
    }
    
    
    /* ============================================== */
    
     function injectForConformationPage(requestJson) {
    
       /* this jsonObject can be used to fill data in the page */
        var jsonObject = requestJson;
        var checkinDate=jsonObject.checkInDate;
        var checkoutDate=jsonObject.checkOutDate;
        if(jsonObject.hotel ){
          if(jsonObject.hotel.hotelLatitude && jsonObject.hotel.hotelLongitude){
            setRedirectionURLforMAPinDOM(jsonObject.hotel);    
            processAddressUsingLongLat(jsonObject.hotel);
          }      
          setRedirectionURLforHotelResourcePath(jsonObject.hotel);
        }
    
       if(jsonObject.guest){
            var guestFullName = "";
            if(jsonObject.guest.firstName){
                guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.firstName+" "+ jsonObject.guest.lastName;
            }else{
                guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.lastName;
            }
            $("#name").html(guestFullName);
        }
        var checkInDateFormatted = moment(checkinDate,"YYYY-MM-DD").format("DD MMM YYYY");
        var checkOutDateFormatted = moment(checkoutDate,"YYYY-MM-DD").format("DD MMM YYYY");
        $("#check-in, #sm-checkin-date").html(checkInDateFormatted);
        $("#check-out, #sm-checkout-date").html(checkOutDateFormatted);
        var guestFullName = "";
        if(jsonObject.guest.firstName){
            guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.firstName+" "+ jsonObject.guest.lastName;
        }else{
            guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.lastName;
        }
        $("#guestname, #sm-guestname").html(guestFullName);
        $("#email").html(jsonObject.guest.email.split(',')[0]);
        $("#mobile, #sm-guestphone").html(jsonObject.guest.phoneNumber);
        // [TIC-FLOW]
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow && !ticRoomRedemptionObjectSession.isTicBookForSomeoneElse){
            $("#membershipNumberSection").removeClass('d-none'); 
        }
        $("#guestMembershipNumber").text(jsonObject.guest.membershipId);
        $("#nameOnCard").html(jsonObject.payments.nameOnCard);
        $("#cardType").html(creditCodeCardType(jsonObject.payments.cardType));
        $("#cardNumber").html(jsonObject.payments.cardNumber);
        $("#expiryMonth").html(jsonObject.payments.expiryMonth);
        // to update the tic points after hotel room redemption
        if(jsonObject.guest.membershipId){
            fetchMemberPoints(jsonObject.guest.membershipId);
        }
        
        if(jsonObject && jsonObject.voucher){
            $(".redeemed-voucher-number").html("Used EPICURE Voucher : "+jsonObject.voucher.ihclissuedVoucherNumber);
        }
        
        if(jsonObject.guest.specialRequests){
            $(".special-request").html(jsonObject.guest.specialRequests.split(',profileId')[0]);
        }else{
            $(".special-request-section").hide();
        }
    
        if(jsonObject != undefined){
            $("#payableAmount").html(jsonObject.totalAmountAfterTax);
            if(jsonObject.hotel != undefined){
                $("#hotelName").html(jsonObject.hotel.hotelName);
                $("#sm-hotelName").html(jsonObject.hotel.hotelName);
                $("#hotelPhoneNo, #sm-hotelphone").html(jsonObject.hotel.hotelPhoneNumber);
                $('#hotelPhoneHRef').attr('href','tel:'+jsonObject.hotel.hotelPhoneNumber);
                $("#hotelEmail").html(jsonObject.hotel.hotelEmail);
                $('#hotelEmailHRef').attr('href','mailto:'+jsonObject.hotel.hotelEmail);
            }
        }
        
       
        
        
        
        
     var mainobj =  jsonObject.roomList;
     /*
         * console.log("mainobj---->>" + JSON.stringify(jsonObject.roomList[0].hotelId));
         */
      var availroomcount=0;
      var allRoomsCancelled = true;
       for(var i =0; i<jsonObject.roomList.length;i++){
           var bedType=jsonObject.roomList[i].bedType;
           var bedTypeImage='';
           var petPolicyDiv='';
           var bedTypeDesc;
           if(bedType === "king") {
                bedTypeDesc = 'King';
               bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
           }else if(bedType === "double") {
                bedTypeDesc = 'Double';
               bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
           } else if (bedType === "queen") {
                bedTypeDesc = 'Queen';
                bedTypeImage = '<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
           } else if(bedType === "twin") {
               bedTypeDesc = 'Twin Beds';
               bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" /><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
           }
           if(jsonObject.roomList[i].petPolicy===null){
               petPolicyDiv='<div class="pet-policy" style="display:none;"><span class="bold-title">Pet policy: </span><span> NA </span></div>';
           }else{
               petPolicyDiv='<div class="pet-policy"><span class="bold-title">Pet policy: </span><span> '+ jsonObject.roomList[i].petPolicy +' </span></div>';
           }
           availroomcount=availroomcount+1;
          var s = '';
          var isRoomCancelled = (jsonObject.roomList[i].resStatus=='Cancelled') || false;
          allRoomsCancelled = allRoomsCancelled && isRoomCancelled;
          var cancelledIndicator = (isRoomCancelled?'<div class="cancel-indication">Cancelled</div>':'');
          var cancelledClass = (isRoomCancelled?'cancelled-room-style':'');
          var editIconTemplate = (isRoomCancelled?'':'<i class="icon-edit"></i> Edit');
          var bestDealDayClass = (isRoomCancelled?"cm-hide":'');
          
          // [TIC-FLOW]
          var userDetails = getUserData();
          var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
          var roomCostAfterTax =  0;
          if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
    
              if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                  roomCostAfterTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i])));
              }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                  roomCostAfterTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i]) / 2)); 
              }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                  roomCostAfterTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i]))); 
              }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                  roomCostAfterTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i]))); 
              }else{
                  roomCostAfterTax  = roundPrice(jsonObject.roomList[i].roomCostAfterTax);
              }
          }else{
              roomCostAfterTax  = roundPrice(jsonObject.roomList[i].roomCostAfterTax); 
          }
          
          if (jsonObject.roomList[i].bookingStatus == true) {
              $('#itineraryNumber').text(jsonObject.itineraryNumber);
              if(isIHCLCBSite()) {
                  s+= '<div class = "room-details-ihclcb"><div class = "reservation-number-ihclcb">Reservation Number : \
                          <span>' + jsonObject.roomList[i].reservationNumber + '</span>'+cancelledIndicator+'\
                      </div>\
                      <div class="cart-selected-rooms-card-container">\
                    <div class="cart-selected-rooms-card">\
                        <div class="cart-selected-rooms-header">\
                            <div class="cart-room-details">\
                                <div class="cart-room-number">Room <span>'+availroomcount+'</span>\
                                <span class="modify-booking-link modify-redirect-to-hotel-room" data-modify="modifyRoomOccupancy">'+editIconTemplate+'</span></div>\
                                <div class="cart-room-info-wrp">\
                                    <div class="cart-adult-count-wrp">\
                                        <div class="cart-adult-label">Adults</div>\
                                        <div class="cart-count-wrapper">\
                                            <div class="cart-count-value">'+ jsonObject.roomList[i].noOfAdults+'</div>\
                                        </div>\
                                    </div>\
                                    <div class="cart-children-count-wrp">\
                                        <div class="cart-children-label">\
                                            Children\
                                            <div class="children-age-limit">Age: 0-12 years</div>\
                                        </div>\
                                        <div class="cart-count-wrapper">\
                                            <div class="cart-count-value">'+ jsonObject.roomList[i].noOfChilds+'</div>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="cart-room-bed-type">\
                                    <div class="cart-bed-label">Bed</div>\
                                    <div class="cart-room-bed selected-bed-type">\
                                        '+bedTypeImage+'\
                                        <div class="cart-room-bed-label">'+bedTypeDesc+'</div></div>\
                                </div>\
                            </div>\
                            <div class="cart-room-cost-wrp">\
                                <div class = "best-deal-cont-ihclcb"><div class="best-deal-desc">Congratulations! You have bagged the best deal for the day</div>\
                                        <div class="total-price-deal-section-ihclcb '+bestDealDayClass+'">\
                                        <div class="best-deal-banner">\
                                        <span class="icon-left-right-clipped"><span class="path1"></span><span class="path2"></span></span>\
                                    <span>BEST DEAL</span></div>\
                                </div>\
                                 </div>\
                                <div class="cart-room-cost">\
                                    <span>Price: </span>\
                                    <span class="cart-currency-symbol rupee-symbol"></span>\
                                    <span class="room-amount">'+ roomCostAfterTax +'</span >\
                                </div>\
                            </div>\
                        </div>\
                        <div class="cart-selected-rooms-content">\
                            <div class="cart-selected-rooms-amenities">\
                                <h3 class="cart-amenities-title">'+ jsonObject.roomList[i].roomTypeName +'</h3>\
                                <div class="cart-amenities-description">'+jsonObject.roomList[i].ratePlanName+'</div>\
                                <span class="modify-booking-link modify-redirect-to-hotel-room" data-modify="modifyRoomOccupancy" style="display: none;"><i class="icon-edit"></i> Edit</span>\
                            </div>\
                            <div class="cart-selected-info-wrp">\
                                <div class="description-cancel-wrp">\
                                    <div class="rate-description">\
                                        <span class="bold-title">Description: </span>\
                                        <span class="conf-rate-description-details">'+ jsonObject.roomList[i].rateDescription +'</span>\
                                    </div>\
                                    <div class="cancel-policy">\
                                        <span class="bold-title">Cancellation policy: </span>\
                                        <span class = "confirmation-canc-details">'+ jsonObject.roomList[i].cancellationPolicy +'</span>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div></div>';
              } else {
           s += '<div class="confirmation-selected-rooms-card">'
           + '<div class="cart-selected-rooms-header clearfix">'
           + '<div class="confirmation-room-details">'
           + '<div class="heading">Reservation Number : <span class="room-reservation-number">' + jsonObject.roomList[i].reservationNumber + '</span>'+cancelledIndicator+'</div>'
           + '<div class="room-details"> <span class="room-details-colored">Room '+availroomcount+' - </span><span class="room-details-margin"><span id="noOfadults">'+ jsonObject.roomList[i].noOfAdults+'</span> <span>Adults</span>, <span id="noOfchild"></span>'+ jsonObject.roomList[i].noOfChilds+' <span>Child</span> </span><span class="bed-spec">'+bedTypeDesc+'</span>'+bedTypeImage
           + '</div>'
           +'<span class="modify-booking-link modify-redirect-to-hotel-room" data-modify="modifyRoomOccupancy">'+editIconTemplate+'</span>'
           + '</div>'
           + '<div class="confirmation-room-price">'
           + '<div class="confirmation-banner">'
           + '<div class="total-price-deal-section '+bestDealDayClass+'">'
           + '<div class="best-deal-banner"> <img src="/content/dam/tajhotels/icons/style-icons/left-right-clipped.svg" alt="background" /><span>BEST DEAL</span></div>'
           + '<div class="best-deal-desc">Congratulations! You have bagged the best deal for the day</div>'
           + '</div>'
           + '</div>'
           + '<div class="confirmation-price-con"><span>Price:</span><span class="rupee-text"><span class="cart-currency-symbol rupee-symbol"></span></span><span id="roomcost" class="cost-with-commas">'+ roomCostAfterTax +'</span></div>'
           + '</div>'
           + '</div>'
           + '<div class="cart-selected-rooms-content">'
           + '<div class="confirmation-selected-rooms-amenities">'
           + '<div class="cart-amenities-title"><span id="roomTypeCode">'+ jsonObject.roomList[i].roomTypeName +' <span class="modify-booking-link modify-redirect-to-hotel-room" data-modify="modifyRoomType">'+editIconTemplate+'</span></span><i class="icon-drop-down-arrow inline-block"></i></div>'
       + '<div class="cart-amenities-description">'+jsonObject.roomList[i].ratePlanName+'</div>'
           + '</div>'
           + '<div class="cart-selected-info-wrp">'
           + '<div class="description-cancel-wrp">'
           + '<div class="rate-description"><span class="bold-title">Rate description: </span><span class="conf-rate-description-details"> '+ jsonObject.roomList[i].rateDescription +' </span></div>'
           + '<div>'
           + '<div class="cancel-policy"><span class="bold-title">Cancellation policy: </span><span class="confirmation-canc-details"> '+ jsonObject.roomList[i].cancellationPolicy +' </span></div>'
           + petPolicyDiv
           + '</div>'
           + '</div>'
           + '</div>'
           + '</div>'
              }
           $(".selected-room-details").append(s);
           }
           }    
         $('.cost-with-commas').digits();
         if(allRoomsCancelled){
             $('.modify-booking-link').remove();
           $('a[data-redirect="cancelTopBtn"]').remove();       
         }
    // if (jsonObject.payments.payAtHotel == true) {
    // $('.payment-details-section').removeClass('payment-details-section-on-online-paytment');
    // }
       if (jsonObject && jsonObject.guest && jsonObject.guest.airportPickup) {
                if (jsonObject.guest.airportPickup == true) {
                    $('.flight-details-section').addClass('visible');
                    $('.flight-details-section').append('<div class="special-request">Flight Number - <span>' + jsonObject.flightDetails.arrivalFlightNo + '</span></div><div class="special-request">Flight Time - <span>' + jsonObject.flightDetails.flightArrivalTime + '</span></div><div class="special-request">Airport - <span>' + jsonObject.flightDetails.arrivalAirportName + '</span></div><div class="pickup-details">Airport pickup required</div>')
                }
            }
       var container = $('.summary-section');
       var noOfRooms = jsonObject.roomList.length;
       var totalTax = Number(jsonObject.totalAmountAfterTax) - Number(jsonObject.totalAmountBeforeTax);
       var totalNoOfDays = 0;
       var couponamount=0;
       var couponname='';
    
       var exploreMoreBtnUrl = jsonObject ? jsonObject.hotel ? jsonObject.hotel.hotelResourcePath+"/rooms-and-suites.html" : '.html' : ".html";
        var cancelPath =
         document.getElementById("cancelBtnPath").value+".html";
        var editBtnUrl =
            document.getElementById("editBtnPath").value+".html";
        
        // var editBtnUrl
        // ='https://be.synxis.com/signIn?_conv_s=si:12*sh:1540904948952-0.42962095170461034*pv:2&_conv_v=vi:1*sc:12*cs:1540904949*fs:1539347382*pv:37*ps:1540903380*exp:{100210453.{v.1002720977-g.{10025904.1-10025905.1-10026078.1-10026102.1-10026105.1-10026167.1-10026168.1-10027113.1}}}&adult=1&arrive=2018-10-31&chain=21305&child=0&depart=2018-11-01&level=chain&locale=en-US&rooms=1&sbe_ri=0&sbe_sid=MrTOFDyJzcopdtkS-_y_GkM9';
        $($.find("[data-redirect='editFooterBtn']")).attr('href',editBtnUrl);
        $($.find("[data-redirect='editTopBtn']")).attr('href',editBtnUrl);
        $($.find(".edit-bkng-btn")).attr('href',editBtnUrl)
        $($.find("[data-redirect='cancelFooterBtn']")).attr('href',editBtnUrl);
        $($.find("[data-redirect='cancelTopBtn']")).attr('href',editBtnUrl);
        // ($.find("[data-redirect='exploremore']")).attr('href',exploreMoreBtnUrl);
        
        var enrollUrl = 'https://tajinnercircle.tajhotels.com/reward-programmes/tajinnercircle/quick-registration.html';
        $($.find("[data-redirect='enrollButton']")).attr('href',enrollUrl);
       /*
         * if(jsonObject.appliedCoupon){ couponamount=Number(jsonObject.appliedCoupon.couponAmountBeforeTax);
         * couponname=jsonObject.appliedCoupon.appliedCoupon; }
         */
       var discountAmount = couponamount * noOfRooms;
       var totalAmount = 0;
    // [TIC-FLOW]
       if(userDetails && userDetails.card && userDetails.card.tier){
           var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
           if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
               if(ticRoomRedemptionObjectSession.ticpoints
                       && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                   totalAmount =  Math.ceil(getTotalTicRoomPrice()); 
               }else if(ticRoomRedemptionObjectSession.ticpoints 
                       && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                   totalAmount =  Math.ceil(getTotalTicRoomPrice()/2); 
               }else if(ticRoomRedemptionObjectSession.ticpoints 
                       && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                   totalAmount =  Math.ceil(getTotalTicRoomPrice()); 
               }else if(ticRoomRedemptionObjectSession.ticpoints 
                       && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                   totalAmount =  Math.ceil(getTotalTicRoomPrice()); 
               }else{
                   totalAmount = parseInt(jsonObject.totalAmountAfterTax) - discountAmount;
               }
           }
       }else{
           totalAmount = Number(jsonObject.totalAmountAfterTax) - discountAmount;
       }  
       
       availableRoomnumber=0;var roomNumber = 1;var taxHtml;var taxCheck = false;
       
       // [TIC-FLOW]
       var userDetails = getUserData();
       var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
       var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
       
       for (i = 0; i < noOfRooms; i++) {
           if (jsonObject.roomList[i].bookingStatus == true) {
               availableRoomnumber=availableRoomnumber+1;
               var noOfAdults = jsonObject.roomList[i].noOfAdults;
               var noOfChilds = jsonObject.roomList[i].noOfChilds;
               var fromDate = jsonObject.checkInDate;
               var toDate = jsonObject.checkOutDate;
               var noOfDays = ((moment(toDate) - moment(fromDate)) / 1000 / 60 / 60 / 24);
               totalNoOfDays = totalNoOfDays + noOfDays;
           /* console.log(totalNoOfDays) */
               
               var currencySymbolForRate = dataCache.session.getData('selectedCurrency');
               var isRoomCancelled = (jsonObject.roomList[i].resStatus=='Cancelled') || false;
               var cancelledIndicator = (isRoomCancelled?'<div class="cancel-indication">Cancelled</div>':'');
               var cancelledClass = (isRoomCancelled?'cancelled-room-style':'');
               var roomData = '(1 Room x ' + noOfDays + ' Nights - ' + noOfAdults + ' Adults & ' + noOfChilds + ' Children) ';
               
               // [TIC-FLOW]
               var roomCostBeforeTax =  0;
               if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                   if(ticRoomRedemptionObjectSession.ticpoints
                           && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                       roomCostBeforeTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i]))); 
                       
                   }else if(ticRoomRedemptionObjectSession.ticpoints
                            && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                       roomCostBeforeTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i])/2))
                   }else if(ticRoomRedemptionObjectSession.ticpoints
                            && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                       roomCostBeforeTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i])))
                   }else if(ticRoomRedemptionObjectSession.ticpoints
                            && ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                       roomCostBeforeTax =  roundPrice(Math.ceil(checkRatesBeforeTICRateFlip(jsonObject.roomList[i])))
                   }else{
                       roomCostBeforeTax  = roundPrice(jsonObject.roomList[i].roomCostBeforeTax);
                   }
               }else{
                   roomCostBeforeTax  = roundPrice(jsonObject.roomList[i].roomCostBeforeTax); 
               }
               
               var roomDataExpand = '<div class="summary-charges-room-total-price summary-charges-item-con '+cancelledClass+'"><div class="summary-charges-item-name">Room ' + availableRoomnumber + '<span>' + roomData + '</span><i class="nightly-rates-dropdown-image icon-drop-down-arrow inline-block"></i>'+cancelledIndicator+'</div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roomCostBeforeTax+ '</span></div><div class="nightly-rates">';
               for(var d=0; d<jsonObject.roomList[i].nightlyRates.length; d++){
                    
                    
                    var indNightRateData = jsonObject.roomList[i].nightlyRates[d];
    
                    var rateAfter = Number(indNightRateData.price).toFixed(2);
                    // [TIC-FLOW]
                    
                    if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                        if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                            rateAfter  = roundPrice(Math.ceil(getTicRoomNightlyRates(jsonObject.roomList[i],d)));
                            currencySymbolForRate = 'TIC';
                        }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                            rateAfter  = roundPrice(Math.ceil(getTicRoomNightlyRates(jsonObject.roomList[i],d)/2));
                            currencySymbolForRate = 'EPICURE';
                        }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                            rateAfter  = roundPrice(Math.ceil(getTicRoomNightlyRates(jsonObject.roomList[i],d)));
                            currencySymbolForRate = 'TAP';
                        }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                            rateAfter  = roundPrice(Math.ceil(getTicRoomNightlyRates(jsonObject.roomList[i],d)));
                            currencySymbolForRate = 'TAPP Me';
                        }
                    }  
                        
                    var indNightRateHtml = '<div class="ind-nightly-rate">'+
                    '<span class="ind-night-date">'+moment(indNightRateData.date,"MM/DD/YYYY").format("DD MMM YYYY")+'</span>'+
                    '<span class="ind-night-rate">'+currencySymbolForRate+' '+rateAfter+'</span>'+
                    '</div>';
                    
                    roomDataExpand = roomDataExpand + indNightRateHtml;
                }
               roomDataExpand = roomDataExpand + '</div></div>';
               container.append(roomDataExpand);
               
               if(jsonObject.roomList[i].taxes){
                   var roomTaxHtml = '<div class="room-level-tax"> <span class="room-info"> Room ' + roomNumber +'</span>';
    
                    for(var ii=0; ii<jsonObject.roomList[i].taxes.length; ii++){
            
                        var indTaxData = jsonObject.roomList[i].taxes[ii];
                        
                        var price = Number(indTaxData.taxAmount).toFixed(2);
                        
                        var indTaxHtml = '<div class="ind-tax-Details">'+
                        '<span class="ind-tax-name">'+indTaxData.taxName+'</span>'+
                        '<span class="ind-tax-amount">'+currencySymbolForRate+' '+price+'</span>'+
                        '</div>';
                        
                        roomTaxHtml = roomTaxHtml + indTaxHtml;
                    }
                  roomTaxHtml = roomTaxHtml + '</div>';
    
                  if(taxHtml == null){
                      taxHtml = roomTaxHtml;
                  }else{
                      taxHtml = taxHtml + roomTaxHtml;
                  }
                  taxCheck = true;
               }
               roomNumber = roomNumber+1;
               
           }
       }
    
       var totalRoomData = '(' + availableRoomnumber + ' Room x ' + totalNoOfDays + ' Nights)'
       
       var summaryHtml = '<div id="summary-taxes-charges" class="summary-charges-item-con"><div class="summary-charges-item-name">Taxes and fees <i class="taxes-dropdown-image inline-block icon-drop-down-arrow"></i></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalTax) + '</span></div><div class="all-type-of-taxes">'+taxHtml+'</div></div><div class="summary-charges-item-con" id="appliedCouponDiv"><div class="summary-charges-item-name">Applied Coupon<div class="input-disabled summary-coupon-applied">'+couponname+'</div></div><div class="summary-charges-discount-value"><span>- </span><span><span class="cart-currency-symbol rupee-symbol"></span></span><span class="cost-with-commas">' + roundPrice(discountAmount) + '</span></div></div><div class="summary-charges-item-con position"><div class="summary-charges-item-name" id="summaryTotalCharges">Total <span class="room-count">' + totalRoomData + '</span></div><div class="summary-charges-item-value" id="summaryTotalCharges"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalAmount) + '</span><span class="total-charge-asterik">*</span></div></div>';
       
       if(jsonObject.payments && jsonObject.payments.payGuaranteeAmount && jsonObject.totalPaidAmount){
           var paidOnlineLabel = $('#paidGuaranteeAmountLabel').val() || 'Paid online ';
           var toBePaidAtHotelLabel = $('#toBePaidAtHotelAmountLabel').val() || 'To be paid at hotel ';
           summaryHtml = summaryHtml+ '<div class="summary-charges-item-con position" id="totalPaidAmount"><div class="summary-charges-item-name">' + paidOnlineLabel + '</div><div class="summary-charges-item-value">- <span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(Number(jsonObject.totalPaidAmount)) + '</span><span class="total-charge-asterik">*</span></div></div>';

           summaryHtml = summaryHtml+ '<div class="summary-charges-item-con position" id="totalTobePaidAtHotelAmount"><div class="summary-charges-item-name">' + toBePaidAtHotelLabel + '</div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalAmount - Number(jsonObject.totalPaidAmount)) + '</span><span class="total-charge-asterik">*</span></div></div>';
       }
       summaryHtml = summaryHtml + '<div class="disclaimer ">* inclusive of all taxes.</div>';
       container.append(summaryHtml);
       
       $('.cost-with-commas').digits();
       
       if(discountAmount <= 0){
           $('#appliedCouponDiv').hide();
       }
       
       if(!taxCheck){
           $('#summary-taxes-charges').hide();
       }
       
       confirmationSelectedRooms();
     };
     
     function processAddressUsingLongLat (jsonHotelObject) {
      // if(jsonHotelObject && jsonHotelObject.hotelLatitude && jsonHotelObject.hotelLongitude)
        // getReverseGeocodingData(jsonHotelObject.hotelLatitude,jsonHotelObject.hotelLongitude);
     }
     
     function getReverseGeocodingData(lat, lng) {
          var latlng = new google.maps.LatLng(lat, lng);
          var address ="";
          // This is making the Geocode request
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({ 'latLng': latlng }, function (results, status) {
              if (status !== google.maps.GeocoderStatus.OK) {
              }
              // This is checking to see if the Geoeode Status is OK before
          // proceeding
              if (status == google.maps.GeocoderStatus.OK) {
                  address = (results[0].formatted_address);
                  hoteladdress=address;
                  dataCache.session.setData('hotelAddress', hoteladdress);
                  setAddressinDOM(address);
              }
          })
      }
     
     function setAddressinDOM(address) {
       return $($.find('.ho-location-address')[0]).text(address);
     }
     
     function setRedirectionURLforMAPinDOM(jsonHotelObject) {
       var url = formMapRedirectURL(jsonHotelObject.hotelLatitude,jsonHotelObject.hotelLongitude);
       return $($.find('.ho-hotel-location-link')[0]).attr('href',url);
     }
     
     function formMapRedirectURL(lattitude,longitude) {
       var url = "https://www.google.com/maps/search/?api=1&query="+lattitude+","+longitude ;
       return url;
     }
     
     var shareURL = "";
     function setRedirectionURLforHotelResourcePath(jsonHotelObject) {
		if(jsonHotelObject) {
		   var url = formResourceRedirectURL(jsonHotelObject.hotelResourcePath);
		   shareURL = url;
		   var doms = $.find("[data-redirect='exploremore']");
		   if (doms != undefined) {
			 for (i in doms) {
			   $($.find("[data-redirect='exploremore']")[i]).attr('href',url);
			 }
		   }
		}
     }
     
     function formResourceRedirectURL(hotelResourcePath) {
         var finalUrl = "";
         var res = hotelResourcePath.split("/");
         if (!hotelResourcePath.includes("/content/tajhotels")) {
             if(hotelResourcePath.includes("/content/ihclcb")) {
                 finalUrl ='/' + res[3] + '/corporate-booking/' + res[6] + '/' + res[7] + '/rooms-and-suites';
             } else {
                 finalUrl ='/' + res[3] + '/' + res[7];
             }
         } else {
            finalUrl ='/' + res[3] + '/' + res[6] + '/' + res[7];
         }
       return finalUrl;
     }
    
    function showResidsPartial(jsonObject){
        
        // [TIC-FLOW]
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        
         $('#tickstatus').hide();
         $('.identification-container').hide();
         $('.special-request-section').hide();
         $('.flight-details-section').hide();
         $('.explore-more-btn').hide();
    
         var checkinDate=jsonObject.checkInDate;
         var checkoutDate=jsonObject.checkOutDate;
         setRedirectionURLforMAPinDOM(jsonObject.hotel);
         processAddressUsingLongLat(jsonObject.hotel);
         setRedirectionURLforHotelResourcePath(jsonObject.hotel);
    
         if(jsonObject.guest){
             var guestFullName = "";
             if(jsonObject.guest.firstName){
                 guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.firstName+" "+ jsonObject.guest.lastName;
             }else{
                 guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.lastName;
             }
             $("#name").html(guestFullName);
         }
        var checkInDateFormatted = moment(checkinDate,"YYYY-MM-DD").format("DD MMM YYYY");
        var checkOutDateFormatted = moment(checkoutDate,"YYYY-MM-DD").format("DD MMM YYYY");
        $("#check-in").html(checkInDateFormatted);
        $("#check-out").html(checkOutDateFormatted);
        var guestFullName = "";
        if(jsonObject.guest.firstName){
            guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.firstName+" "+ jsonObject.guest.lastName;
        }else{
            guestFullName = jsonObject.guest.title +" "+ jsonObject.guest.lastName;
        }
        $("#guestname, .sm-guestname").html(guestFullName);
        $("#email, .sm-guestemail").html(jsonObject.guest.email);
        $("#mobile").html(jsonObject.guest.phoneNumber);
        
        // [TIC-FLOW]
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow && !ticRoomRedemptionObjectSession.isTicBookForSomeoneElse){
            $("#membershipNumberSection").removeClass('d-none'); 
        }
        $("#guestMembershipNumber").text(jsonObject.guest.membershipId);
        $("#nameOnCard").html(jsonObject.payments.nameOnCard);
        $("#cardType").html(creditCodeCardType(jsonObject.payments.cardType));
        $("#cardNumber").html(jsonObject.payments.cardNumber);
        $("#expiryMonth").html(jsonObject.payments.expiryMonth);
        $(".special-request").html(jsonObject.guest.specialRequests);
        $("#payableAmount").html(jsonObject.totalAmountAfterTax);
        $("#hotelName").html(jsonObject.hotel.hotelName);
        $("#sm-hotelName").html(jsonObject.hotel.hotelName);
        $("#hotelPhoneNo, #sm-hotelphone").html(jsonObject.hotel.hotelPhoneNumber);
        $('#hotelPhoneHRef').attr('href','tel:'+jsonObject.hotel.hotelPhoneNumber);
        $("#hotelEmail").html(jsonObject.hotel.hotelEmail);
        var couponamount=0;
        var couponname='';
        /*
         * if(jsonObject.appliedCoupon){ couponamount=Number(jsonObject.appliedCoupon.couponAmountBeforeTax);
         * couponname=jsonObject.appliedCoupon.appliedCoupon; }
         */
    
        for(var i =0; i<jsonObject.roomList.length;i++){
            var s = '';
            var petPolicyDiv='';
            if(jsonObject.roomList[i].petPolicy===null){
                petPolicyDiv='<div class="pet-policy" style="display:none;"><span class="bold-title">Pet policy: </span><span> NA </span></div>';
            }else{
                petPolicyDiv='<div class="pet-policy"><span class="bold-title">Pet policy: </span><span> '+ jsonObject.roomList[i].petPolicy +' </span></div>';
            }
            var bedType=jsonObject.roomList[i].bedType;
            var bedTypeImage='';
             if(bedType === "king") {
                 bedTypeDesc = 'King';
                bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
            }else if(bedType === "double") {
                 bedTypeDesc = 'Double';
                bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
            } else if (bedType === "queen") {
                 bedTypeDesc = 'Queen';
                 bedTypeImage = '<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
            } else if(bedType === "twin") {
                bedTypeDesc = 'Twin Beds';
                bedTypeImage='<img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" /><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" />';
            }
             
            // [TIC-FLOW]
            var userDetails = getUserData();
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            var roomCostAfterTax =  0;
            
            if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                    roomCostAfterTax =  roundPrice(Math.ceil(parseFloat(jsonObject.roomList[i].roomCostBeforeTax))); 
                }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                    roomCostAfterTax =  roundPrice(Math.ceil(parseFloat(jsonObject.roomList[i].roomCostBeforeTax)/2)); 
                }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                    roomCostAfterTax =  roundPrice(Math.ceil(parseFloat(jsonObject.roomList[i].roomCostBeforeTax))); 
                }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                    roomCostAfterTax =  roundPrice(Math.ceil(parseFloat(jsonObject.roomList[i].roomCostBeforeTax))); 
                }else{
                    roomCostAfterTax  = roundPrice(jsonObject.roomList[i].roomCostAfterTax); 
                }
            }else{
                roomCostAfterTax  = roundPrice(jsonObject.roomList[i].roomCostAfterTax); 
            }
            
            if (jsonObject.roomList[i].bookingStatus == true) {
                var noOfAdults=jsonObject.roomList[i].noOfAdults;
                var noOfChildren=jsonObject.roomList[i].noOfChilds;
                s += '<div class="confirmation-selected-rooms-card">'
                    + '<div class="cart-selected-rooms-header clearfix">'
                    + '<div class="confirmation-room-details">'
                    + '<div class="heading">Reservation Number : <span class="room-reservation-number">' + jsonObject.roomList[i].reservationNumber + '</span></div>'
                    + '<div class="room-details"> <span class="room-details-colored">Room '+(i+1)+' - </span><span class="room-details-margin"><span id="noOfadults">'+ noOfAdults +'</span> <span>'+(noOfAdults>1?'Adults':'Adult')+'</span>, <span id="noOfchild"></span>'+ noOfChildren+' <span>'+(noOfChildren>1?'Children':'Child')+'</span> </span><span class="bed-spec">'+bedTypeDesc+'</span>'+bedTypeImage
                    + '</div>'
                    + '</div>'
                    + '<div class="confirmation-room-price">'
                    + '<div class="confirmation-banner">'
                    + '<div class="total-price-deal-section">'
                    + '<div class="best-deal-banner"> <img src="/content/dam/tajhotels/icons/style-icons/left-right-clipped.svg" alt="background" /><span>BEST DEAL</span></div>'
                    + '<div class="best-deal-desc">Congratulations! You have bagged the best deal for the day</div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="confirmation-price-con"><span>Price:</span><span class="rupee-text"><span class="cart-currency-symbol rupee-symbol"></span></span><span id="roomcost">'+ roomCostAfterTax +'</span></div>'
                    + '</div>'
                    + '</div>'
                    + '<div class="cart-selected-rooms-content">'
                    + '<div class="confirmation-selected-rooms-amenities">'
                    + '<div class="cart-amenities-title"><span id="roomTypeCode">'+ jsonObject.roomList[i].roomTypeName +'</span><i class="icon-drop-down-arrow inline-block"></i></div>'
                + '<div class="cart-amenities-description">'+jsonObject.roomList[i].ratePlanName+'</div>'
                    + '</div>'
                    + '<div class="cart-selected-info-wrp">'
                    + '<div class="description-cancel-wrp">'
                    + '<div class="rate-description"><span class="bold-title">Rate description: </span><span class="conf-rate-description-details"> '+ jsonObject.roomList[i].rateDescription +' </span></div>'
                    + '<div>'
                    + '<div class="cancel-policy"><span class="bold-title">Cancellation policy: </span><span class="confirmation-canc-details"> '+ jsonObject.roomList[i].cancellationPolicy +' </span></div>'
                    + petPolicyDiv
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    
    
         }else{
             s +='<div class="confirmation-selected-rooms-card">'
                 + '<div class="cart-selected-rooms-header clearfix">'
                 + '<div class="confirmation-room-details">'
                 + '<div class="heading">Reservation Number : <span class="room-reservation-number">' + jsonObject.roomList[i].reservationNumber + '</span></div>'
                 + '<div class="room-details"> <span class="room-details-colored">Room '+(i+1)+' - </span><span class="room-details-margin"><span id="noOfadults">'+ jsonObject.roomList[i].noOfAdults+'</span> <span>Adults</span>, <span id="noOfchild"></span>'+ jsonObject.roomList[i].noOfChilds+' <span>Child</span> </span><span class="bed-spec">'+bedTypeDesc+'</span>'+bedTypeImage
                 + '</div>'
                 + '</div>'
                 + '<div class="confirmation-room-price">'
                 + '<div class="confirmation-banner">'
                 + '<div class="total-price-deal-section">'
                 + '<div class="confirm-fail-banner"><span>Confirmation failed</span></div>'
                 + '</div>'
                 + '</div>'
        }
        $("#reservationIdsList").append(s);
        /* alert(jsonObject.roomList[i].reservationNumber); */
        confirmationSelectedRooms();
    }
    
    // if (jsonObject.payments.payAtHotel == true) {
    // $('.payment-details-section').removeClass('payment-details-section-on-online-paytment');
    // }
       
        if (jsonObject && jsonObject.guest && jsonObject.guest.airportPickup) {
            if (jsonObject.guest.airportPickup == true) {
                $('.flight-details-section').addClass('visible');
                $('.flight-details-section').append('<div class="special-request">Flight Number - <span>' + jsonObject.flightDetails.arrivalFlightNo + '</span></div><div class="special-request">Flight Time - <span>' + jsonObject.flightDetails.flightArrivalTime + '</span></div><div class="special-request">Airport - <span>' + jsonObject.flightDetails.arrivalAirportName + '</span></div><div class="pickup-details">Airport pickup required</div>')
            }
        }
       var container = $('.summary-section');
       var noOfRooms = jsonObject.roomList.length;
       var totalTax = Number(jsonObject.totalAmountAfterTax) - Number(jsonObject.totalAmountBeforeTax);
       var totalNoOfDays = 0;
       var discountAmount = couponamount * noOfRooms;
       var totalAmount = Number(jsonObject.totalAmountAfterTax) - discountAmount;
    
    var checkForTax = false;var taxCheck = false;
       var roomNumber = 1;var taxHtml;var count = 0;
       for (i = 0; i < noOfRooms; i++) {
           if(jsonObject.roomList[i].bookingStatus == true){count++;
               var noOfAdults = jsonObject.roomList[i].noOfAdults;
               var noOfChilds = jsonObject.roomList[i].noOfChilds;
               var fromDate = jsonObject.checkInDate;
               var toDate = jsonObject.checkOutDate;
               var noOfDays = ((moment(toDate) - moment(fromDate)) / 1000 / 60 / 60 / 24);
    
               totalNoOfDays = totalNoOfDays + noOfDays;
    
               var currencySymbolForRate = dataCache.session.getData('selectedCurrency');           
               var isRoomCancelled = (jsonObject.roomList[i].resStatus=='Cancelled') || false;           
               var roomData = '(1 Room x ' + noOfDays + ' Nights - ' + noOfAdults + ' Adults & ' + noOfChilds + ' Children) ';
               var roomDataExpand = '<div class="summary-charges-room-total-price summary-charges-item-con"><div class="summary-charges-item-name">Room ' + (i + 1) + '<span>' + roomData + '</span><i class="nightly-rates-dropdown-image icon-drop-down-arrow inline-block"></i></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(jsonObject.roomList[i].roomCostBeforeTax) + '</span></div><div class="nightly-rates">';
               for(var d=0; d<jsonObject.roomList[i].nightlyRates.length; d++){
                    
                    
                    var indNightRateData = jsonObject.roomList[i].nightlyRates[d];
                    
                    var rateAfter = 0;
                        /*
                         * rateAfter = Math.round( rateAfter * 100 ) / 100; rateAfter = roundPrice( rateAfter );
                         */
                 // [TIC-FLOW]
                    if(userDetails && userDetails.card && userDetails.card.tier){
                        if(bookingOptionsSessionData && bookingOptionsSessionData.redeemedTic && userDetails.card && userDetails.card.type.includes("TIC")){
                            currencySymbolForRate = 'TIC';
                            rateAfter =  roundPrice(Math.ceil(indNightRateData.price)); 
                        }else  if(bookingOptionsSessionData && bookingOptionsSessionData.redeemedEpicure && userDetails.card && userDetails.card.type.includes("TIC")){
                            currencySymbolForRate = 'EPICURE';
                            rateAfter =  roundPrice(Math.ceil(indNightRateData.price/2)); 
                        }else  if(bookingOptionsSessionData && bookingOptionsSessionData.redeemedTic && userDetails.card && userDetails.card.type.includes("TAP")){
                            currencySymbolForRate = 'TAP';
                            rateAfter =  roundPrice(Math.ceil(indNightRateData.price)); 
                        }else  if(bookingOptionsSessionData && bookingOptionsSessionData.redeemedTic && userDetails.card && userDetails.card.type.includes("TAPPMe")){
                            currencySymbolForRate = 'TAPPMe';
                            rateAfter =  roundPrice(Math.ceil(indNightRateData.price)); 
                        }else{
                            rateAfter = Number(indNightRateData.price).toFixed(2);
                        }
                    }else{
                        rateAfter = Number(indNightRateData.price).toFixed(2);
                    }   
                    var indNightRateHtml = '<div class="ind-nightly-rate">'+
                    '<span class="ind-night-date">'+moment(indNightRateData.date,"MM/DD/YYYY").format("DD MMM YYYY")+'</span>'+
                    '<span class="ind-night-rate">'+currencySymbolForRate+' '+rateAfter+'</span>'+
                    '</div>';
                    
                    roomDataExpand = roomDataExpand + indNightRateHtml;
                }
               roomDataExpand = roomDataExpand + '</div></div>';
               container.append(roomDataExpand);
               
               if(jsonObject.roomList[i].taxes){
                   var roomTaxHtml = '<div class="room-level-tax"> <span class="room-info"> Room ' + roomNumber +'</span>';
    
                    for(var ii=0; ii<jsonObject.roomList[i].taxes.length; ii++){
            
                        var indTaxData = jsonObject.roomList[i].taxes[ii];
                        
                        var price = Number(indTaxData.taxAmount).toFixed(2);
                        
                        var indTaxHtml = '<div class="ind-tax-Details">'+
                        '<span class="ind-tax-name">'+indTaxData.taxName+'</span>'+
                        '<span class="ind-tax-amount">'+currencySymbolForRate+' '+price+'</span>'+
                        '</div>';
                        
                        roomTaxHtml = roomTaxHtml + indTaxHtml;
                    }
                  roomTaxHtml = roomTaxHtml + '</div>';
    
                  if(taxHtml == null){
                      taxHtml = roomTaxHtml;
                  }else{
                      taxHtml = taxHtml + roomTaxHtml;
                  }taxCheck = true;
               }checkForTax = true;
           }roomNumber = roomNumber+1;
       }
       if(checkForTax){
           var totalRoomData = '(' + count + ' Room x ' + totalNoOfDays + ' Nights)'
           
           var summaryHtml = '<div id="summary-taxes-charges" class="summary-charges-item-con"><div class="summary-charges-item-name">Taxes and fees <i class="taxes-dropdown-image inline-block icon-drop-down-arrow"></i></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalTax) + '</span></div><div class="all-type-of-taxes">'+taxHtml+'</div></div><div class="summary-charges-item-con" id="appliedCouponDiv"><div class="summary-charges-item-name">Applied Coupon<div class="input-disabled summary-coupon-applied">'+couponname+'</div></div><div class="summary-charges-discount-value"><span>- </span><span><span class="cart-currency-symbol rupee-symbol"></span></span><span class="cost-with-commas">' + roundPrice(discountAmount) + '</span></div></div><div class="summary-charges-item-con position"><div class="summary-charges-item-name" id="summaryTotalCharges">Total <span class="room-count">' + totalRoomData + '</span></div><div class="summary-charges-item-value" id="summaryTotalCharges"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalAmount) + '</span><span class="total-charge-asterik">*</span></div></div>';
           
           if(jsonObject.payments && jsonObject.payments.payGuaranteeAmount && jsonObject.totalPaidAmount){
               var paidOnlineLabel = $('#paidGuaranteeAmountLabel').val() || 'Paid online ';
               var toBePaidAtHotelLabel = $('#toBePaidAtHotelAmountLabel').val() || 'To be paid at hotel ';
               summaryHtml = summaryHtml+ '<div class="summary-charges-item-con position" id="totalPaidAmount"><div class="summary-charges-item-name">' + paidOnlineLabel + '</div><div class="summary-charges-item-value">- <span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(Number(jsonObject.totalPaidAmount)) + '</span><span class="total-charge-asterik">*</span></div></div>';

               summaryHtml = summaryHtml+ '<div class="summary-charges-item-con position" id="totalTobePaidAtHotelAmount"><div class="summary-charges-item-name">' + toBePaidAtHotelLabel + '</div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalAmount - Number(jsonObject.totalPaidAmount)) + '</span><span class="total-charge-asterik">*</span></div></div>';
           }
           summaryHtml = summaryHtml + '<div class="disclaimer ">* inclusive of all taxes.</div>';
           container.append(summaryHtml);
       }else{
           $('.payment-details-section').hide();
           $('#conformEnrollCon').hide();
       }
       
       if(!taxCheck){
           $('#summary-taxes-charges').hide();
       }
       
        $('.cost-with-commas').digits();
        
        if(discountAmount <= 0){
           $('#appliedCouponDiv').hide();
        }
    }
    
    function showResidsIgnored(jsonObject){
    
    
         $('#tickstatus').hide();
         $('.identification-container').hide();
         $('.special-request-section').hide();
         $('.flight-details-section').hide();
       // $('.explore-more-btn').hide();
         $('.payment-section').hide();
         $('.chk-in-out-con').hide();
         $('.hotel-details-con').hide();
         $('.guest-details-con').hide();
         $('.confirmation-details-con').hide();
         $('.payment-details-section').hide();
    
    
         for(var i =0; i<jsonObject.roomList.length;i++){
             var s = '';
    
             s +='<div class="confirmation-selected-rooms-card">'
                 + '<div class="cart-selected-rooms-header clearfix">'
                 + '<div class="confirmation-room-details">'
                 + '<div class="heading">Reservation Number : <span class="room-reservation-number">' + jsonObject.roomList[i].reservationNumber + '</span></div>'
                 + '<div class="room-details"> <span class="room-details-colored">Room '+(i+1)+' - </span><span class="room-details-margin"><span id="noOfadults">'+ jsonObject.roomList[i].noOfAdults+'</span> <span>Adults</span>, <span id="noOfchild"></span>'+ jsonObject.roomList[i].noOfChilds+' <span>Child</span> </span><span class="bed-spec">Twin Beds</span><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" /><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg"'
                 + 'alt="icon" /></div>'
                 + '</div>'
                 + '<div class="confirmation-room-price">'
                 + '<div class="confirmation-banner">'
                 + '<div class="total-price-deal-section">'
                 + '<div class="confirm-fail-banner"><span>Confirmation Ignored</span></div>'
                 + '</div>'
                 + '</div>'
                 $("#reservationIdsList").append(s);
        }
    
        /* alert(jsonObject.roomList[i].reservationNumber); */
    }
    
    function injectTicPoints( objectForTicPoints ){
        var curString = dataCache.session.getData("selectedCurrencyString");
        if((curString == "INR") && objectForTicPoints && objectForTicPoints.roomList){
            var availableRTotalBeforeTax = 0;
            for(var t=0;t<objectForTicPoints.roomList.length;t++){
                if(objectForTicPoints.roomList[t].bookingStatus == true){
                    availableRTotalBeforeTax = parseInt(objectForTicPoints.roomList[t].roomCostBeforeTax) + availableRTotalBeforeTax;
                }
            }
            var ticInfo = ticAndEpicureCal(availableRTotalBeforeTax);
            $("#ticPoints").html(ticInfo.ticPoints);
            $("#worthAount").html(ticInfo.worthAmount);
            /*
             * console.log("objectForTicPoints: "+JSON.stringify(objectForTicPoints)); console.log("ticPoints:
             * "+ticInfo.ticPoints+"ticInfo.worthAmount: "+ticInfo.worthAmount);
             */
        }else{
            $(".gift-card-btn-wrp").remove();
            $("#conformEnrollCon").hide();
        }
    }
    
    function ticAndEpicureCal(totalPriceWithoutTax){
        var pasingAmount = parseFloat(totalPriceWithoutTax);
        var amntPer = 4;
        var worthAmount = percentageCal( pasingAmount, amntPer );
        var eachTICPointWorth = 5;
        var ticPoints = ticPointsCal( worthAmount, eachTICPointWorth)
        var ticData = {};
        ticData.worthAmount = worthAmount;
        ticData.ticPoints = ticPoints;
        ticData.epicurePoints = '';
        return ticData;
    }
    
    function ticPointsCal( amount, eachPointWorth){
        var ticPoints = Math.floor(amount/eachPointWorth);
        return ticPoints;
    }
    
    function percentageCal( amount, percentage ){
        
        var finalAmount = ( amount / 100 ) * percentage;
        return finalAmount;
    }
    
    function showResidsFailed(jsonObject){
    
    
        $('#tickstatus').hide();
        $('.identification-container').hide();
        $('.special-request-section').hide();
        $('.flight-details-section').hide();
        // $('.explore-more-btn').hide();
        $('.payment-section').hide();
        $('.chk-in-out-con').hide();
        $('.hotel-details-con').hide();
        $('.guest-details-con').hide();
        $('.confirmation-details-con').hide();
        $('.payment-details-section').hide();
    
    
        for(var i =0; jsonObject.roomList && i<jsonObject.roomList.length;i++){
            var s = '';
    
            s +='<div class="confirmation-selected-rooms-card">'
                + '<div class="cart-selected-rooms-header clearfix">'
                + '<div class="confirmation-room-details">'
                + '<div class="heading">Reservation Number : <span class="room-reservation-number">' + jsonObject.roomList[i].reservationNumber + '</span></div>'
                + '<div class="room-details"> <span class="room-details-colored">Room '+(i+1)+' - </span><span class="room-details-margin"><span id="noOfadults">'+ jsonObject.roomList[i].noOfAdults+'</span> <span>Adults</span>, <span id="noOfchild"></span>'+ jsonObject.roomList[i].noOfChilds+' <span>Child</span> </span><span class="bed-spec">Twin Beds</span><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon" /><img src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg"'
                + 'alt="icon" /></div>'
                + '</div>'
                + '<div class="confirmation-room-price">'
                + '<div class="confirmation-banner">'
                + '<div class="total-price-deal-section">'
                + '<div class="confirm-fail-banner"><span>Confirmation Failed</span></div>'
                + '</div>'
                + '</div>'
                $("#reservationIdsList").append(s);
           }
        $(".gift-card-btn-wrp").remove();
        $('#conformEnrollCon').hide();
        /* alert(jsonObject.roomList[i].reservationNumber); */
     
    }
    
    $(document).ready(function() {

        // apply Theme for confirmation page
        setThemeValueToPageContainer();
        // end of theme script
        var urlSocial;
        var currentHost= document.location.host;
        if(shareURL != undefined && shareURL != "" && shareURL != null) {
            urlSocial = shareURL;
        }
        else {
            urlSocial = window.location.href;
        }
        if((urlSocial.startsWith("/") && (!urlSocial.includes(currentHost)))){
            urlSocial=currentHost+urlSocial;
        }
        var text = $('.share-btn-wrapper').attr("data-share-text");

        $("#socials").jsSocials({
            url : urlSocial,
            text : text,
            showCount : false,
            showLabel : false,
            shares : [ {
                share : "facebook",
                logo : "/content/dam/tajhotels/icons/social-icons/ic-facebook@2x.png"
            }, {
                share : "twitter",
                logo : "/content/dam/tajhotels/icons/style-icons/twitter.svg"
            }, {
                share : "googleplus",
                logo : "/content/dam/tajhotels/icons/social-icons/google+.png"
            }]

        })


    });

    // [TIC-FLOW]
    function checkRatesBeforeTICRateFlip(roomListJsonObject){
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.selection){
            for (var i = 0; i < ticRoomRedemptionObjectSession.selection.length; i++) {
                var roomData = ticRoomRedemptionObjectSession.selection[i];
                if((roomData.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES' || roomData.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES' 
                    || roomData.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES' ||roomData.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES' ) && (roomListJsonObject.roomTypeCode === roomData.roomTypeCode)){
                    
                    if (ticRoomRedemptionObjectSession.currencySelected != 'INR' && ticRoomRedemptionObjectSession.currencySelected != '₹') {
                        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                            var conversionRate = parseFloat(currencyRateConversionString[ticRoomRedemptionObjectSession.currencySelected + '_INR']);
                            return Math.round((roomData.roomBaseRate + roomData.roomTaxRate) * conversionRate );
                        }

                    }else{
                        return Math.ceil(roomData.roomBaseRate + roomData.roomTaxRate);
                    }
                }
            }
        }

        return 0;

    }

    function getTicRoomNightlyRates(roomListJsonObject,index){
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.selection){
            for (var i = 0; i < ticRoomRedemptionObjectSession.selection.length; i++) {
                var roomData = ticRoomRedemptionObjectSession.selection[i];
                if((roomData.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES' || roomData.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES' 
                    || roomData.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES' || roomData.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES' ) && (roomListJsonObject.roomTypeCode === roomData.roomTypeCode)){
                    
                    if (ticRoomRedemptionObjectSession.currencySelected != 'INR' && ticRoomRedemptionObjectSession.currencySelected != '₹') {
                        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                            var conversionRate = parseFloat(currencyRateConversionString[ticRoomRedemptionObjectSession.currencySelected + '_INR']);
                            return Math.round(roomData.nightlyRates[index].priceWithFeeAndTax * conversionRate);
                        }

                    }else{
                        return Math.ceil(roomData.nightlyRates[index].price);
                    }
                }
            }
        }

        return 0;

    }

    // [TIC-FLOW]
    function getTotalTicRoomPrice(){
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        var totalCardPrice = 0;
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.selection){  
            for (var i = 0; i < ticRoomRedemptionObjectSession.selection.length; i++) {
                var roomData = ticRoomRedemptionObjectSession.selection[i];
                totalCardPrice = totalCardPrice + roomData.roomBaseRate + roomData.roomTaxRate;
            }
        }
        
        
        if (ticRoomRedemptionObjectSession.currencySelected != 'INR' && ticRoomRedemptionObjectSession.currencySelected != '₹') {
            if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                var conversionRate = parseFloat(currencyRateConversionString[ticRoomRedemptionObjectSession.currencySelected + '_INR']);
                totalCardPrice = Math.round(totalCardPrice * conversionRate);
            }

        }
        
        
        return Math.ceil(totalCardPrice);
    }
    
    
    function printTicRoomRedemptionSummary(bookingDetailsRequest){
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
            $(".tic-room-redemption-info").removeClass("d-none");
            var totalTicPointsCart = getTotalTicRoomPrice();
            var totalTicMoneyCharged = 0;
            if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TIC'){
                if(ticRoomRedemptionObjectSession.ticpoints.pointsPlusCash){
                    totalTicMoneyCharged =  totalTicPointsCart - ticRoomRedemptionObjectSession.ticpoints.noTicPoints;
                    totalTicMoneyCharged = totalTicMoneyCharged * 5;
                    $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noTicPoints+" TIC Points redeemed + Rs "+totalTicMoneyCharged+" is charged.");
                }else{
                    $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noTicPoints+" TIC Points redeemed");
                }
            }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'EPICURE'){
                if(ticRoomRedemptionObjectSession.ticpoints.pointsPlusCash){
                    totalTicMoneyCharged =  totalTicPointsCart - (ticRoomRedemptionObjectSession.ticpoints.noEpicurePoints*2);
                    totalTicMoneyCharged = totalTicMoneyCharged * 5;
                    $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noEpicurePoints+" Epicure Points redeemed + Rs "+totalTicMoneyCharged+" is charged.");
                }else{
                    $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noEpicurePoints+" Epicure Points redeemed");
                }

            }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAP'){
                    $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noTicPoints+" TAP redeemed");
            }else if(ticRoomRedemptionObjectSession.ticpoints.pointsType === 'TAPPMe'){
                $(".tic-room-redemption-info").html(ticRoomRedemptionObjectSession.ticpoints.noTicPoints+" TAPP Me redeemed");
            }
        }
    }
    // [TIC-FLOWs]
    function ticBookHotelConfirmationFlow(){
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
            $(".cm-page-container").addClass('tic-room-redemption-fix');
            $('body').trigger('taj:fetch-profile-details');
        }
    }
    // [IHCLCB Flow]
    function showEntityInformation(){
        var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
        if(ihclCbBookingObject && ihclCbBookingObject.isIHCLCBFlow){
            var userDetails = dataCache.local.getData("userDetails");
            if(userDetails && userDetails.selectedEntity){
                $(".ihclcb-entity-info").removeClass("d-none");
                $(".entity-name").html(userDetails.selectedEntity.partyName); 
                $(".city-name").html(userDetails.selectedEntity.city);
                $(".gstin-no").html(userDetails.selectedEntity.gSTNTaxID_c);
            }
        }
    }