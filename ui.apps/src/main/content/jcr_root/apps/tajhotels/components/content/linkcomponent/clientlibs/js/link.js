var headerDDInitCon = $( '.cm-header-dd-options-con' );
var headerDropdowns = $( '.header-warpper .cm-header-dropdowns' );

headerDropdowns.each( function() {
    $( this ).on( 'click', function( e ) {
        e.stopPropagation();
        var target = $( this ).closest( '.nav-item' ).find( '.cm-header-dd-options-con' );
        if ( target.hasClass( 'active' ) ) {
            target.removeClass( 'active' );
            return;
        }
        headerDDInitCon.removeClass( 'active' );
        target.addClass( 'active' );
    } );
} );

$( 'body' ).on( 'click', function() {
    headerDDInitCon.removeClass( 'active' );
} );

$( '.header-currency-options .cm-each-header-dd-item' ).on( 'click', function() {

    var elDDCurrencySymbol = $( this ).find( '.header-dd-option-currency' );
    var elDDCurrency = $( this ).find( '.header-dd-option-text' );

    var elActiveCurrSymbol = $( this ).closest( '.nav-item' ).find( '.selected-currency' );
    var elActiveCurrency = $( this ).closest( '.nav-item' ).find( '.selected-txt' );


    var currencySymbol = elDDCurrencySymbol.text();
    var currency = elDDCurrency.text();

    var activeCurrSymbol = elActiveCurrSymbol.text();
    var activeCurrency = elActiveCurrency.text();

    elDDCurrencySymbol.text( activeCurrSymbol );
    elDDCurrency.text( activeCurrency );

    elActiveCurrSymbol.text( currencySymbol );
    elActiveCurrency.text( currency );
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9oZWFkZXItY29tcC9oZWFkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGhlYWRlckRESW5pdENvbiA9ICQoICcuY20taGVhZGVyLWRkLW9wdGlvbnMtY29uJyApO1xyXG52YXIgaGVhZGVyRHJvcGRvd25zID0gJCggJy5oZWFkZXItd2FycHBlciAuY20taGVhZGVyLWRyb3Bkb3ducycgKTtcclxuXHJcbmhlYWRlckRyb3Bkb3ducy5lYWNoKCBmdW5jdGlvbigpIHtcclxuICAgICQoIHRoaXMgKS5vbiggJ2NsaWNrJywgZnVuY3Rpb24oIGUgKSB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB2YXIgdGFyZ2V0ID0gJCggdGhpcyApLmNsb3Nlc3QoICcubmF2LWl0ZW0nICkuZmluZCggJy5jbS1oZWFkZXItZGQtb3B0aW9ucy1jb24nICk7XHJcbiAgICAgICAgaWYgKCB0YXJnZXQuaGFzQ2xhc3MoICdhY3RpdmUnICkgKSB7XHJcbiAgICAgICAgICAgIHRhcmdldC5yZW1vdmVDbGFzcyggJ2FjdGl2ZScgKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBoZWFkZXJEREluaXRDb24ucmVtb3ZlQ2xhc3MoICdhY3RpdmUnICk7XHJcbiAgICAgICAgdGFyZ2V0LmFkZENsYXNzKCAnYWN0aXZlJyApO1xyXG4gICAgfSApO1xyXG59ICk7XHJcblxyXG4kKCAnYm9keScgKS5vbiggJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICBoZWFkZXJEREluaXRDb24ucmVtb3ZlQ2xhc3MoICdhY3RpdmUnICk7XHJcbn0gKTtcclxuXHJcbiQoICcuaGVhZGVyLWN1cnJlbmN5LW9wdGlvbnMgLmNtLWVhY2gtaGVhZGVyLWRkLWl0ZW0nICkub24oICdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIHZhciBlbEREQ3VycmVuY3lTeW1ib2wgPSAkKCB0aGlzICkuZmluZCggJy5oZWFkZXItZGQtb3B0aW9uLWN1cnJlbmN5JyApO1xyXG4gICAgdmFyIGVsRERDdXJyZW5jeSA9ICQoIHRoaXMgKS5maW5kKCAnLmhlYWRlci1kZC1vcHRpb24tdGV4dCcgKTtcclxuXHJcbiAgICB2YXIgZWxBY3RpdmVDdXJyU3ltYm9sID0gJCggdGhpcyApLmNsb3Nlc3QoICcubmF2LWl0ZW0nICkuZmluZCggJy5zZWxlY3RlZC1jdXJyZW5jeScgKTtcclxuICAgIHZhciBlbEFjdGl2ZUN1cnJlbmN5ID0gJCggdGhpcyApLmNsb3Nlc3QoICcubmF2LWl0ZW0nICkuZmluZCggJy5zZWxlY3RlZC10eHQnICk7XHJcblxyXG5cclxuICAgIHZhciBjdXJyZW5jeVN5bWJvbCA9IGVsRERDdXJyZW5jeVN5bWJvbC50ZXh0KCk7XHJcbiAgICB2YXIgY3VycmVuY3kgPSBlbEREQ3VycmVuY3kudGV4dCgpO1xyXG5cclxuICAgIHZhciBhY3RpdmVDdXJyU3ltYm9sID0gZWxBY3RpdmVDdXJyU3ltYm9sLnRleHQoKTtcclxuICAgIHZhciBhY3RpdmVDdXJyZW5jeSA9IGVsQWN0aXZlQ3VycmVuY3kudGV4dCgpO1xyXG5cclxuICAgIGVsRERDdXJyZW5jeVN5bWJvbC50ZXh0KCBhY3RpdmVDdXJyU3ltYm9sICk7XHJcbiAgICBlbEREQ3VycmVuY3kudGV4dCggYWN0aXZlQ3VycmVuY3kgKTtcclxuXHJcbiAgICBlbEFjdGl2ZUN1cnJTeW1ib2wudGV4dCggY3VycmVuY3lTeW1ib2wgKTtcclxuICAgIGVsQWN0aXZlQ3VycmVuY3kudGV4dCggY3VycmVuY3kgKTtcclxufSApOyJdLCJmaWxlIjoibWFya3VwL2NvbXBvbmVudHMvaGVhZGVyLWNvbXAvaGVhZGVyLmpzIn0=
