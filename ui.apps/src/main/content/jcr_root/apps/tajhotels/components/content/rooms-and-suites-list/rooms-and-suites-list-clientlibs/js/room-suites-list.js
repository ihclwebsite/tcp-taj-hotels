$(document)
        .ready(
                function() {
                    trimDescriptionInRoomSuitesList();
                    hidePriceDisplayWrapper();
                    invokeRoomFetchAjaxCall();
                    $('.overview-page-rooms-carousel').customSlide(3);
                    $(document)
                            .on(
                                    'currency:changed',
                                    function(e, currency) {
                                        var popupParams = {
                                            title : 'Booking Alert!',
                                            description : 'Showing currency is Hotel Default currency. Now You will not be  allowed to change Currency. ',
                                        }
                                        warningBox(popupParams);
                                    });
                });

function hidePriceDisplayWrapper() {
    $('.room-cost-wrapper').hide();
}

function trimDescriptionInRoomSuitesList() {
    $.each($('.rooms-and-suites-content .room-description'), function(i, value) {
        $(value).cmTrimText({
            charLimit : 300,
        });
    });
}

function successHandler(response, isError) {
    var successString = "success";
    if (response.responseCode.toLowerCase() == successString) {
        injectResponseToSelector(response);
    } else {
        handleFailureToFetchRates(response);
    }
}

function invokeRoomFetchAjaxCall() {

    var requestData = {
    // hotelIds : getHotelCode(),
    // checkInDate : getCheckInDateInDisplay(),
    // checkOutDate : getCheckOutDateInDisplay(),
    // roomOccupantOptions : JSON.stringify(getRoomOccupantOptions()),
    // shortCurrencyString : getShortCurrencyStringInDisplay(),
    // currencyString : getCurrencyStringInDisplay(),
    // rateFilters : JSON.stringify(getAllRateFilters()),
    // rateCodes : JSON.stringify(getAllRatePlanCodes()),
    // promoCode : getPromoCode()
    };

    var rateFilters = getRateFilter();
    // var promoCode = getPromoCode();
    // var rateCodes = getAllRatePlanCodes();
    var rateCodes = [];
    //
    // rateFilters ? requestData.rateFilters = JSON.stringify([ rateFilters ]) : '';
    // rateCodes ? requestData.rateCodes = JSON.stringify(rateCodes) : '';
    // promoCode ? requestData.promoCode = JSON.stringify(promoCode) : '';
    if ($('#synxis-downtime-check').val() == "true") {
        $('.room-cost-card-wait-spinner').hide();
        $('.room-cost-wrapper').hide('visible');
    } else {
        $.ajax({
            type : 'GET',
            url : '/bin/room-rates/rooms-availability.rates/' + getHotelCode() + '/' + getCheckInDateInDisplay() + '/'
                    + getCheckOutDateInDisplay() + '/' + getShortCurrencyStringInDisplay() + '/'
                    + getRoomOccupantOptions() + '/' + JSON.stringify([ rateFilters ]) + '/'
                    + JSON.stringify(rateCodes) + '/' + 'ratesCache',
            dataType : 'json',
            data : requestData,
            error : function(response) {
                if (response.status == 412) {
                    synxisDowntimecond(response)
                } else {
                    successHandler(response.responseJSON, true)
                }
            },
            success : successHandler
        });
    }
}

function synxisDowntimecond(response) {
    console.error("Ajax call was successful, but the server returned a failure for room rates.");
    $('.room-cost-card-wait-spinner').hide();
    $('.room-cost-wrapper').hide('visible');
    // $('.room-rate-unavailable').addClass('visible');
    if (response && response.responseText) {
        setWarningInDom(response.responseText)
    } else {
        setWarningInDom("Oops! Something went wrong. Please Try Again");
    }

}

function handleFailureToFetchRates(response) {
    console.error("Ajax call was successful, but the server returned a failure for room rates.");
    $('.room-cost-card-wait-spinner').hide();
    $('.room-cost-wrapper').hide('visible');
    $('.room-rate-unavailable').addClass('visible');
    if (response && response.serviceError) {
        setWarningInDom(response.serviceError);
    } else {
        setWarningInDom("Oops! Something went wrong. Please Try Again");
    }
}

function setWarningInDom(warning) {
    tajToaster(warning);
}

function injectResponseToSelector(response) {
    var firstRoomResponse = response[1];
    if (firstRoomResponse) {
        showPricesIfHidden();
        processRateFilters(firstRoomResponse.rateFilters);
    } else {
        handleFailureToFetchRates();
    }
}

function showPricesIfHidden() {
    $('.room-cost-card-wait-spinner').hide();
    $('.room-cost-wrapper').show();
}
var romSutCurCheck;
function processRateFilters(rateFilters) {
    var rateFilterInDisplay = getRateFilter();
    var rateFilter = rateFilters[rateFilterInDisplay];

    var hotelCode = getHotelCode();
    var hotelRoot = rateFilter[hotelCode];

    romSutCurCheck = setActiveCurrencyWithResponseValue(hotelRoot.currencyString);
    var roomsAvailability = hotelRoot["roomsAvailability"];
    injectValuesForRoomCards(roomsAvailability);
}

function injectValuesForRoomCards(roomsAvailabilityData) {
    var allRoomContainersInDom = getAllRoomContainersFromDom();
    $(allRoomContainersInDom).each(function(key, roomContainerInDom) {
        var roomBedTypeMap = $(this).data("bed-room-map");
        if (!roomBedTypeMap) {
            $(roomContainerInDom).find('.room-rate-wrap').removeClass('visible');
            $(roomContainerInDom).find('.room-rate-unavailable').addClass('visible');
            return;
        }

        var kingBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.king];
        var queenBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.queen];
        var twinBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.twin];
        if (kingBedRoomAvail || queenBedRoomAvail || twinBedRoomAvail) {
            injectValuesForRoomContainers(this, kingBedRoomAvail || queenBedRoomAvail || twinBedRoomAvail);
        } else {
            $(this).find('.room-cost-wrapper').hide();
            $(this).find('.room-rate-unavailable').removeClass('cm-hide');
            $(this).data('sort-order', 9999999999999999999999999999 + key);
        }
    }).sort(function(a, b) {
        if ($(a).data('sort-order') === $(b).data('sort-order')) {
            return 0;
        }
        return $(a).data('sort-order') > $(b).data('sort-order') ? 1 : -1;
    }).detach().appendTo('[data-component-id="rooms-suites-list"] div.clearfix');
    $('.overview-page-rooms-carousel').customSlide(3);
}

function getAllRoomContainersFromDom() {
    return $.find('[data-component-id="room-suites-card"]');
}

function injectValuesForRoomContainers(roomContainer, data) {
    $(roomContainer).data('sort-order', data.lowestPrice);
    $(roomContainer).find('.room-prices').text(getCommaFormattedNumber(data.lowestPrice));
    var pricesymboll;
    if (romSutCurCheck) {
        pricesymboll = dataCache.session.getData("currencyCache").currencySymbol.trim();
    } else {
        pricesymboll = data.currencyString;
    }
    $(roomContainer).find('.room-price-symbol').text(pricesymboll);
}

function getCommaFormattedNumber(number) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number;
    } else {
        number = Math.round(number);
        formattedNumber = number.toLocaleString('en-IN')
    }
    return formattedNumber;
}

function getHotelCode() {

    var hotelIdDomArray = $.find("[data-hotel-id]");
    var hotelIdContainer = "";
    if (hotelIdDomArray != undefined) {
        hotelIdContainer = hotelIdDomArray[0];
    }
    return $(hotelIdContainer).data().hotelId;
}

function getCheckInDateInDisplay() {
    var sessionData = getBookingOptionsSessionData();

    var fromDate = moment(sessionData.fromDate, 'MMM Do YY').format('YYYY-MM-DD');
    return fromDate;

}

function getCheckOutDateInDisplay() {
    var sessionData = getBookingOptionsSessionData();

    var fromDate = moment(sessionData.toDate, 'MMM Do YY').format('YYYY-MM-DD');
    return fromDate;

}

function getRoomOccupantOptions() {
    var sessionData = getBookingOptionsSessionData();
    var roomOptions = [];

    sessionData.roomOptions.forEach(function(value) {
        roomOptions.push(value.adults);
        roomOptions.push(value.children)
    });
    // return sessionData.roomOptions;
    return roomOptions;
}

function getShortCurrencyStringInDisplay() {
    var sessionData = getBookingOptionsSessionData();
    return sessionData.currencySelected;
}

function getCurrencyStringInDisplay() {
    var sessionData = getBookingOptionsSessionData();
    return sessionData.currencySelected;
}

function getBookingOptionsSessionData() {
    return dataCache.session.getData("bookingOptions");
}

function getRateFilter() {
    var rateFilter = $('[data-component-id="rooms-suites-list"]').data("rate-filter");
    // Added to handle THU-1657 as default rate filter to be sent
    return rateFilter ? rateFilter : "STD";
}
