$(document).ready(function() {
    var expLength = $(".gallery-nav-option-list .gallery-nav-option").find("#expericenceType").prevObject.length;
    var data = $(".gallery-nav-option-list .gallery-nav-option").find("#expericenceType");
    for (var i = 0; i < expLength; i++) {

        if ((data.prevObject[i].innerHTML.toLowerCase() == 'all')) {
            $(data.prevObject[i]).addClass('gallery-nav-option-selected');
        }
    }

    // Gallery Navigation handler
    $('.gallery-nav-option').click(function() {
        var value = $(this).find("#expericenceType").context.innerHTML.toLowerCase();

        $(this).addClass('gallery-nav-option-selected');
        $('.gallery-nav-option').not(this).removeClass('gallery-nav-option-selected');
        if (value == 'all') {
            $(".row .experiences-container").show();

        } else {
            $(".row .experiences-container").filter(function() {

                $(this).toggle($(this).find(".experienceType").text().toLowerCase().indexOf(value) > -1)
            });
            filteredLength = $(".row .recommended-experiences-wrap:visible").length;
            if (filteredLength == 0) {
                console.info("No results to display")
            }
            if (value == '') {
                // init_pagination();
            }
        }

    });
    // Gallery radio button filter
    $('.gallery-nav-radio-input').click(function() {
        if (this.value == "gallery-filter-videos") {
            $('.gallery-images-wrapper').hide();
            $('.gallery-videos-wrapper').show();
        } else {
            $('.gallery-images-wrapper').show();
            $('.gallery-videos-wrapper').hide();
        }
    });

    $('.generic-show-more').click(function() {
        var textReference = $(this).find('span');
        var iconReference = $(this).find('img');
        if (textReference.text() == "SHOW MORE") {
            textReference.text("SHOW LESS");
        } else {
            textReference.text("SHOW MORE");
        }
        iconReference.toggleClass('rotate-show-more-icon');
        $(this).siblings('.row.gallery-show-more').toggleClass('gallery-show-more-active');
    });

    $('.gallery-filter-icon').click(function() {
        $('.gallery-nav-option-list').show();
    });
    $('.gallery-filter-back, .gallery-filter-done-btn').click(function() {
        $('.gallery-nav-option-list').hide();
    });

    // Carousel overlay handler
    $('.gallery-image-element, .gallery-video-element').on('click', function() {
        $('.mr-menu-carousel-overlay').removeClass('mr-overlay-initial-none ');
    });

    // On page load only images to be displayed by default and image radio button to be selected
    displayOnlyImagesOnPageLoad();

    // remove radiobutton filters if either of images or videos are not present
    hideRadioButtons();
});

/**
 * Method to display only images on gallery page load.
 * 
 */
function displayOnlyImagesOnPageLoad() {
    var selector = "[data-dam-format='image/jpeg']";
    var imagesToBeInNavigatoinTab = $.find(selector);
    var selectedElements = $(imagesToBeInNavigatoinTab).filter(selector);
    hideAll();
    $(selectedElements).find("video").addClass("hidden");
    $(selectedElements).removeClass("hidden");
    getColValuesBasedOnIndex(selectedElements);
}

/**
 * Method to find the input from navigation bar on selection.
 * 
 * @param obj -
 *            Navigation bar option selected.
 */
function navigationTabChanged(obj) {
    var path = $(obj).data("nav-link");
    hideAll();
    var selector = "[data-dam-path*='" + path + "']";
    var imagesToBeInNavigatoinTab = $.find(selector);

    var format = getSelectedRadioFormat();

    var selectedElements;
    if (format == undefined || format == "") {
        selectedElements = imagesToBeInNavigatoinTab;
    } else {
        var formatSelector = "[data-dam-format*='" + format + "']";
        selectedElements = $(imagesToBeInNavigatoinTab).filter(formatSelector);
    }

    $(selectedElements).removeClass("hidden");
    getColValuesBasedOnIndex(selectedElements);
}

/**
 * Method to hide all the gallery types before filtering.
 * 
 */
function hideAll() {
    var galleryTypeList = $.find(".gallery-image-element-wrapper");
    $(galleryTypeList).addClass("hidden");
}

/**
 * Method to filter the gallery type when radio button is selected.
 * 
 * @param obj -
 *            Object representing radio button selected.
 */
function radioButtonChanged(obj) {
    $(".radio-inner-fill").removeClass('visible');
    var format = $(obj).data("format-selector");
    var radioFill = $(obj).find(".radio-inner-fill");

    $(radioFill).addClass("visible");

    var path = getSelectedNavigationTabPath();

    var damPathSelector = "[data-dam-path*='" + path + "']";
    var imagesToBeInNavigatoinTab = $.find(damPathSelector);

    var selector = "[data-dam-format='" + format + "']";
    var selectedElements = $(imagesToBeInNavigatoinTab).filter(selector);
    hideAll();
    $(selectedElements).removeClass("hidden");
    getColValuesBasedOnIndex(selectedElements);
}

/**
 * Method to get the selected navigation tab option in experience nav bar.
 * 
 * @returns path of the selected option.
 */
function getSelectedNavigationTabPath() {
    var selectedNavigationTab = $.find(".gallery-nav-option-selected");
    var path = $(selectedNavigationTab).data("nav-link");
    return path;
}

/**
 * Method to find which radio button format got selected.
 * 
 * @returns - format selected in radio buton.
 */
function getSelectedRadioFormat() {
    var selectedRadioButton = $.find(".radio-inner-fill.visible");
    var formatSelected = $(selectedRadioButton).parent().data("format-selector");
    if (formatSelected == undefined) {
        formatSelected = "";
    }
    return formatSelected;
}

function hideRadioButtons() {
    if ($("[data-dam-format='image/jpeg']").length == 0 || $("[data-dam-format='video/mp4']").length == 0) {
        $(".gallery-nav-radio-btns-container").hide();
    } else {
        $(".gallery-nav-radio-btns-container").show();
    }
}
