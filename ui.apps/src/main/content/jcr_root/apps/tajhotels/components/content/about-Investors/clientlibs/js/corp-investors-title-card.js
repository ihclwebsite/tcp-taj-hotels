
    $(document).ready(function() {
        investorDOMReady();

    });


function investorDOMReady() {
    var options = [{
        selector: "Select Financial Year",
        selectorValue: "Year",
        option: [
            { label: '2017-2018', value: '2017-2018' },
            { label: '2016-2017', value: '2016-2017' },
            { label: '2015-2016', value: '2015-2016' },
            { label: '2014-2015', value: '2014-2015' },
            { label: '2013-2014', value: '2013-2014' },
            { label: '2012-2013', value: '2012-2013' },
            { label: '2011-2012', value: '2011-2012' },
            { label: '2010-2011', value: '2010-2011' }
        ],
        selectedOptionList: null,
        onChange: function(list) {
            //do
        }
    }];
    var selectedData = {};
    var filterOptionsWrap = $('.filter-wrap-catagory');
    var filterOnSearchBar = $('.pressRoom-filter .search-filters-container');

    createFilterOptions(filterOptionsWrap, options);
    //amalgamtionCards();
    switchingPage();
    report_content(options);
    eventsOnFilter(filterOnSearchBar, filterOptionsWrap, options);

};


function eventsOnFilter(filterOnSearchBar, filterOptionsWrap, options) {
    $('.cm-search-with-filter .filter-landing-cont-image .only-icon').on('click', function() {
        $('.events-filter-subsection').css('display', 'block');
    });

    $('.press-room-filter-wrapper .filter-go-con,.press-room-filter-wrapper .cm-press-room-filter').on('click', function() {
        var filterResultWrapper = $('.mr-investors-report-documents-wrapper.content-active .mr-audit-statement-template');

    })

    $('.pressRoom-filter .searchbar-input').on("click", function() {
        filterOnSearchBar.addClass("cm-hide");
    })

    $('.trending-search .individual-trends').click(function() {
        if (filterOnSearchBar.length > 0) {
            filterOnSearchBar.toggleClass('cm-hide', false);
        }
    })
};


function switchingPage() {
    var switching = document.getElementById("mr-reports-wrapper-id");
    if (switching != null) {
        var division = switching.getElementsByClassName("rate-tab");
        for (var i = 0; i < division.length; i++) {
            division[i].addEventListener("click", function() {
                var currentdiv = document.getElementsByClassName("tab-selected");
                currentdiv[0].className = currentdiv[0].className.replace("tab-selected", "");
                this.className += " tab-selected";

            });
        }
    }
};

function filterReset(options) {
    $(document).trigger('multiselect:reset');
    $('.filter-wrap-catagory').find('.cm-multi-dd').each(function(i) {
        $(this).multiselectDDreset();
    });
    $('.mr-investors-report-documents-wrapper.content-active .mr-audit-statement-template').each(function() {
        $(this).toggleClass('cm-hide', false);
    })

    for (var i = 0; i < options.length; i++) {
        options[i].selectedOptionList = null;
    }

};


function report_content(options) {
    $('.rate-tab').click(function() {
        var current = $(this);
        var contentElement = current.parent().children(),
            position = contentElement.index(current);

        var targetWrapper = $('.mr-investors-report-documents-wrapper').eq(position);

        if (!targetWrapper.hasClass('content-active')) {
            $('.mr-investors-report-documents-wrapper.content-active').toggleClass('content-active', false);
            targetWrapper.toggleClass('content-active', true);
            filterReset(options);
        }

    });
};
$(document).ready(function(){

    $( '.card-show-more-button' ).click( function( e ) {
       var wrapper = $( this ).siblings( '.show-more-card-wrapper' );
       var cards = $( wrapper ).find( '.show-more-card-element' );
       $( cards ).toggleClass( 'show-more-card-element-active' );
       var textReference = $( this ).find( 'span' );
       var iconReference = $( this ).find( 'img' );
       if ( textReference.text() == "SHOW MORE" ) {
           textReference.text( "SHOW LESS" );
       } else {
           textReference.text( "SHOW MORE" );
           this.scrollIntoView();
       }
       iconReference.toggleClass( 'cm-rotate-show-more-icon' );
   } );
});
