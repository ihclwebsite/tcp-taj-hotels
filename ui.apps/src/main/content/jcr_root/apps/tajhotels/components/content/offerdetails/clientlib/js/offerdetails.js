$('document').ready(function() {
    if($('.offers-rooms_desc>p').length>0){
        $('.offers-rooms_desc>p').cmToggleText({
            charLimit: 250
               });
    }
    
    ticOfferDetailsForAnalytics();
 
});

function ticOfferDetailsForAnalytics() {
	var offerValidityForAnalytics = $('.validity-content').text().replace(/ {1,}/g," ");
	
	offerDataDetails = {
			offerValidity: offerValidityForAnalytics,
	}
	return offerDataDetails;
}