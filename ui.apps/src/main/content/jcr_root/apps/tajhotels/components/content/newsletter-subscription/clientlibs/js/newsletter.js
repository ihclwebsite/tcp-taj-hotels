document
        .addEventListener(
                'DOMContentLoaded',
                function() {

                    function isEmail(email) {
                        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        return regex.test(email);
                    }
                    var email;
                    var flag;
                    $('.email-input').blur(function() {
                        email = $(this).val();
                        if (!isEmail(email)) {
                             var $footerEmailMsg =$(".result-message-for-newsletter");
                            // $( this ).css( 'border', 'solid 1px #d29751' );
                            flag = false;

                          $(this).val().trim()==''? $footerEmailMsg.text("Please Enter email id").addClass("subscription-failure"):$footerEmailMsg.text("Invalid Email-ID").addClass("subscription-failure");
                        } else {
                            flag = true;

                        }
                    });
                    $('#newsletter').click(function() {

					});
					// The below function call is declared at dining-filter js
					try {
						// This function is available only on dining filter
						// component in dining list page
						populateFilterFromHtml();

					} catch (e) {
						// Dining filter is not available in the page
						// console.log("The function[populateFilterFromHtml()]
						// can't be called. Dining filter is not available in
						// the page ")
					}
					// toggleFooterPadding();
					setupNewLetterSubscribe();
				});

function setupNewLetterSubscribe() {
	var $popupWindow=$('.cm-warning-box-con');

	var countries=getCountryList();

	$.each(countries,function(index,country){
		$('#customer-country').append($('<option value="'+country.name+'" '+(country.name=='India'?'selected':'')+'>'+country.name+'</option>'));
	});
	if( $('#customer-country').length !=0 ){
		$('#customer-country').selectBoxIt();
		$('#customer-country').data("selectBox-selectBoxIt").refresh();
	}

	var $content=$('.cm-content-blocks');
	var $btn = $('#news-letter-submit-btn');
	var $loader = $('.newsletter-loader');
    var $success=$('.success');
    var $successSubscribe = $('.success').find('.subscribe');
    var $successUnsubscribe = $('.success').find('.unsubscribe');
	var $error=$('.error');
    var $errorSubscribe = $('.error').find('.subscribe');
    var $errorUnsubscribe = $('.error').find('.unsubscribe');
    var $unsubscribe = $('.unsubscribe-link');
    var $privacyCheck = $('#privacyPolicyCheck');
    var $termsCondCheck = $('#termsCondCheck');
    var $popupClose = $('.warning-box-close-newsletter');

    $success.hide();
    $error.hide();
	$popupClose.on('click',function(){
		$content.show();
		$success.hide();
		$error.hide();
	});
    $( 'body' ).on( 'click', '.warning-box-close-icon.icon-close', function( e ) {
         $popupClose.trigger('click');
    } );

    $unsubscribe.on('click', function (){
		var unsubscribeValid = unsubscribeInputValidation();
        var data = {};
		data.firstName = $('#customer-first-name').val();
		data.lastName = $('#customer-last-name').val();
		data.title = $('#customer-title').val();
		data.gender = $('#customer-gender').val();
		data.city = $('#customer-city').val();
		data.emailId = $('#customer-email').val();
		data.country=$('#customer-country').val();
		data.sourceBrand = 'taj';
		data.currentDate = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString();
        data.isSubscribe =false;
        if(unsubscribeValid){
			$unsubscribe.removeClass("newsletter-disable-btn");
			$loader.show();
			$.ajax({
				url : '/bin/newsletterSubscribe',
				data : data
			}).done(function(response) {
				if (response.responseCode === 'SUCCESS') {
                    $success.css('display', 'flex');
                    $successSubscribe.hide();
                    $successUnsubscribe.show();
                    $loader.hide();
				} else {
                    $error.css('display', 'flex');
                    $errorSubscribe.hide();
                    $errorUnsubscribe.show();
                    $loader.hide();
				}
			}).fail(function(response) {
				$error.css('display', 'flex');
                $errorSubscribe.hide();
                $errorUnsubscribe.show();
                    $loader.hide();

			})
        }
    })
    $privacyCheck.on('click', function(){
        var privacyError = $('#privacyCheckbox');
        CheckboxChange($(this), privacyError);
    })
    $termsCondCheck.on('click', function(){
        var termsError = $('#termsConditionsCheckbox');
        CheckboxChange($(this), termsError);
    })
    function CheckboxChange(t, e) {
        if (t.is(':checked')) {
          e.removeClass('invalid-checkbox');
        } else {
          e.addClass('invalid-checkbox');
        }
    }
	$btn.on('click', function() {
		// check validation
		var data = {};
		data.firstName = $('#customer-first-name').val();
		data.lastName = $('#customer-last-name').val();
		data.title = $('#customer-title').val();
		data.gender = $('#customer-gender').val();
		data.city = $('#customer-city').val();
		data.emailId = $('#customer-email').val();
		data.country=$('#customer-country').val();
		data.sourceBrand = 'taj';
		data.currentDate = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString();
        data.isSubscribe =true;
        console.log("data is : " , data);
		var newsletterInputValid = newsletterInputValidation();
		if (newsletterInputValid) {
			$btn.removeClass("newsletter-disable-btn");
			$loader.show();
			$.ajax({
				url : '/bin/newsletterSubscribe',
				data : data
			}).done(function(response) {
				if (response.responseCode === 'SUCCESS') {
                    $success.css('display', 'flex');
                    $successSubscribe.show();
                    $successUnsubscribe.hide();
                    $loader.hide();
				} else {
                    $error.css('display', 'flex');
                    $errorSubscribe.show();
                    $errorUnsubscribe.hide();
                    $loader.hide();
				}
			}).fail(function(response) {
				$error.css('display', 'flex');
                $errorSubscribe.show();
                $errorUnsubscribe.hide();
                    $loader.hide();

			})
		}

	});
}

function boxDisable(e, t) {
    if (t.is(':checked')) {
      $(e).find('input').attr('disabled', true);
    } else {
      $(e).find('input').removeAttr('disabled');
    }
}

function newsletterInputValidation(){
    var validationResult = true;
    var $newsletterInputElems = $( 'form.newsletter-form' ).find( 'input.sub-form-mandatory', 'select.sub-form-mandatory' );
	
    $newsletterInputElems.each(function(){
        if($(this).val() == ""){
            $( this ).addClass( 'invalid-input' );
	        validationResult = false;
            invalidWarningMessage( $( this ) ); 
        }
    });
	if($newsletterInputElems.hasClass('invalid-input')){
		var $firstInvalidElem = $( 'form.newsletter-form' ).find('.invalid-input').first();
		($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();
		validationResult = false;
	}

    if($('#privacyPolicyCheck:checkbox:checked').length < 1){
        validationResult = false;
        $('#privacyCheckbox').addClass('invalid-checkbox');
    }
    if($('#termsCondCheck:checkbox:checked').length < 1){
        validationResult = false;
        $('#termsConditionsCheckbox').addClass('invalid-checkbox');
    }
	return validationResult;
}
function unsubscribeInputValidation(){
	var validationResult = true;
    var emailValidation = $('#customer-email');
    if($('#customer-email').val() == ""){
		 $('#customer-email').addClass( 'invalid-input' );
	        validationResult = false;
	        invalidWarningMessage( $('#customer-email') );
    }
    if($('#privacyPolicyCheck:checkbox:checked').length < 1){
        validationResult = false;
        $('#privacyCheckbox').addClass('invalid-checkbox');        
    }
    if($('#termsCondCheck:checkbox:checked').length < 1){
        validationResult = false;
        $('#termsConditionsCheckbox').addClass('invalid-checkbox');        
    }
	if($('#customer-email').hasClass('invalid-input')){
        var $firstInvalidElem = $('#customer-email');
		($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();
		validationResult = false;
	}

	return validationResult;
}