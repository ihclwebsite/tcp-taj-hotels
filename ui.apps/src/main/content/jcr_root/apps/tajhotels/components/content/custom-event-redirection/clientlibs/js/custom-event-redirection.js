$(document).ready(function() {
    var event = $('.custome-event-handler').data('custom-redirect-event');
    var redirectPath = $('.custome-event-handler').data('custom-redirect-path');
    registerCustomEventHandler(event, redirectPath);
});

function registerCustomEventHandler(event, redirectPath) {
    $('body').on(event, function() {
        window.location.href = redirectPath;
    });
}
