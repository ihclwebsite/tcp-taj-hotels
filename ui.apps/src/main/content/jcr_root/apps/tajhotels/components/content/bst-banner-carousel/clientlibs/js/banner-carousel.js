var contextualBanners = [];

$(document).ready(
        function() {
//            ihclImageCarousel();
            hideControlsMobile();
            hideControlsSingleBanner();
            $("#bannerCarousel").on("slid.bs.carousel", "", hideControlsMobile);

            $("#bannerCarousel").on("touchstart", function(event) {
                var xClick = event.originalEvent.touches[0].pageX;
                $(this).one("touchmove", function(event) {
                    var xMove = event.originalEvent.touches[0].pageX;
                    if (Math.floor(xClick - xMove) > 5) {
                        $(this).carousel('next');
                    } else if (Math.floor(xClick - xMove) < -5) {
                        $(this).carousel('prev');
                    }
                });
                $("#bannerCarousel").on("touchend", function() {
                    $(this).off("touchmove");
                });
            });

            // hide left, right control on mobile viewport
            function hideControlsMobile() {
                var $this = $("#bannerCarousel");
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $this.children(".carousel-control-prev").hide();
                    $this.children(".carousel-control-next").hide();
                }
            }
            ;

            // hide controls if there is a single banner
            function hideControlsSingleBanner() {
                var $this = $("#bannerCarousel");
                var banners = $this.find(".carousel-item");
                var indicators = $this.find(".carousel-indicators");
                if (banners.length === 1) {
                    indicators.hide();
                    $this.children(".carousel-control-prev").hide();
                    $this.children(".carousel-control-next").hide();
                }
            }

            // Login related
            if ($("#bannerCarousel").data("login-support")) {
                registerLoginListener(updateUserCarousel);
                if (userCacheExists()) {
                    updateUserCarousel();
                }
            }

            // Update the first banner slide with user name
            function updateUserCarousel() {
                var banner = $("#bannerCarousel");
                var userData = getUserData();
                var firstSlide = banner.find(".carousel-item").get(0);
                $(firstSlide).find(".tic-banner-main-heading").text("Welcome " + userData.name);
            }

            if (contextualBanners.length > 0) {
                profileFetchListener(updateContextualBanner)
                if (userCacheExists()) {
                    updateContextualBanner();
                }
            }

            function updateContextualBanner() {
                var userData = getUserData();
                contextualBanners.forEach(function(banner) {
                    if (userData.tier && banner.context.toLowerCase().includes(userData.tier.toLowerCase())) {
                        $("#bannerCarousel .carousel-inner").prepend(banner.dom);
                        var ind = $('#bannerCarousel .carousel-indicators li');
                        $('#bannerCarousel .carousel-indicators').append(
                                '<li data-target="#bannerCarousel" data-slide-to="' + (ind.length) + '"></li>');
                    }
                });
                $('#bannerCarousel').carousel(0);
            }
        });

function registerContextualBanner(contextBannerKey, context) {
    var banner = document.getElementById(contextBannerKey);
    var obj = {
        context : context,
        dom : banner
    };
    contextualBanners.push(obj);
}
