$("document").ready(function() {
    try {
        var bungalows = getUrlParameter('onlyBungalows');
        var checkAvail = getUrlParameter('checkAvail');
        var themeAma = $('.cm-page-container').hasClass('ama-theme');
        if (themeAma) {
            if (!bungalows && bungalows == "" && checkAvail) {
                $("#onlyRoom").trigger("click");
                $(".ama-theme .bas-about-room-container").css("display", "block");
            } else {
                $("#onlyBungalow").trigger("click");
                $(".ama-theme .bas-about-room-container").css("display", "none");
            }

            $('.ama-theme .bas-date-container-main-wrap input[type="radio"]').click(function() {

                var adultCountReset = $(".ama-theme  .bas-quantity").parent().parent().hasClass("bas-no-of-adults");
                var childCountReset = $(".ama-theme  .bas-quantity").parent().parent().hasClass("bas-no-of-child");
                if (adultCountReset) {
                    $(".ama-theme  .bas-quantity").closest(".bas-no-of-adults .bas-quantity").val(1);
                }
                if (childCountReset) {
                    $(".ama-theme  .bas-quantity").closest(".bas-no-of-child .bas-quantity").val(0);
                }
                if ($(this).hasClass("activeRadioButton")) {
                    $(".ama-theme .bas-about-room-container").css("display", "block");
                } else if (!$(this).hasClass("activeRadioButton")) {
                    $(".ama-theme .bas-about-room-container").css("display", "none");
                }

            });

            $(".ama-theme .bas-close.icon-close").click(function() {

                // if (!bungalows && bungalows == "" && checkAvail) {
                // $("#onlyRoom").trigger("click");
                // } else {
                // $("#onlyBungalow").trigger("click");
                // }

                amaBookingObject = fetchRoomOptionsSelected(amaBookingObject);
                amaBookingObject.fromDate = moment(new Date($("#input-box1").val())).format('MMM D YY');
                amaBookingObject.toDate = moment(new Date($("#input-box2").val())).format('MMM D YY');
                amaBookingObject.rooms = $('.bas-room-no').length;
                amaBookingObject.BungalowType = fetchRadioButtonSelectedAma();
                autoPopulateBannerBookAStay();
            });
        }

        $(".ama-theme .bas-date-container-main-wrap #onlyBungalow").click(function() {
            $(".bas-room-delete-close").trigger("click");
        });

        setTimeout(updateBasPlaceholder, 1000);

        function updateBasPlaceholder() {
            var isDestinationPage = $(".cm-page-container").hasClass("destination-layout");
            var isHotelPage = $(".cm-page-container").hasClass("specific-hotels-page");
            if (themeAma & !isDestinationPage & !isHotelPage) {
                $('#book-a-stay .dropdown-input').text("Select Destinations/Bungalows");
            } else if (themeAma & (isDestinationPage || isHotelPage)) {
                var destinationName = $('.specific-hotels-breadcrumb .breadcrumb-item:last-child').text().trim();
                $('#book-a-stay .dropdown-input').text(destinationName);
                $('#global-re-direct').attr('hrefvalue', window.location.href);
            }
        }
    } catch (error) {
        console.error('Error occurred in ama-bookastay', error);
    }


    /* is only bunglow check in home page*/
	isOnlyBunglowInHome();
});



function isOnlyBunglowInHome() {
	$('#select-results').on('click', function(ev){
        if($(ev.target).parent('li').hasClass('hotel-item')){
			console.log($(ev.target).attr('data-isonlybungalow'));
            var btnElem = $("#onlyRoomBtn");
            if($(ev.target).attr('data-isonlybungalow')==="true")
            {
                $(btnElem).prop("disabled", true);
				$(btnElem).parent('.radio-container').css('opacity', '0.5');
            	$("#onlyBungalowBtn").prop("checked", true);

            }
            else
            {
                $(btnElem).prop("disabled", false);
				$(btnElem).parent('.radio-container').css('opacity', '1');
            }

        }
	});
}
