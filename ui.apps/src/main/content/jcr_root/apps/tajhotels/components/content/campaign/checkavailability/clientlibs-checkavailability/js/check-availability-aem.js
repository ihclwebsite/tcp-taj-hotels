var searchComponent = function(searchInput, searchResutlsContainer, searchResutlsDestinations, searchResutlsHotels,
        noResultsCallBack) {
    var $searchInput = $(searchInput);
    var $searchPath = $('#searchPath').val();
    var $otherWebsitePath = $('#checkAvailOtherPath').val();
    var $searchResutlsContainer = $(searchResutlsContainer);
    var $searchResutlsHotels = $(searchResutlsHotels);
    var SEARCH_INPUT_DEBOUNCE_RATE = 1000;

    var alternateSearch = $('#alternateSearch').val();
	var alternateSearchPath = $('#alternateSearchPath').val();
	var $searchCompContainer = $('.search-comp-container');

    var performDestinationHotelSearch = function(key) {
        var url = "/bin/checkHotelsForOffers.data/" + $searchPath.replace(/\/content\//g, ":") + "/"
                            + $otherWebsitePath.replace(/\/content\//g, ":").replace(/,/g, "_") + "/"
                            + $searchInput.val() + "/offersSearchCache.json";

        if(alternateSearch && alternateSearch == 'true'){
			url = alternateSearchPath;
        }
        $.ajax(
                {
                    method : "GET",
                    url : url
                }).success(function(response) {
            succcesCallBack(response);
        }).fail(function(error) {
            console.error("search failed : ", error);
        });
    }
    var succcesCallBack = function(response) {
        clearSearchResults();
        $searchResutlsContainer.show();

        updateResults(response, $searchResutlsHotels, true);
        if (Object.keys(response.website).length == 0 && Object.keys(response.others).length == 0) {
            $('.search-comp-no-results').show();
            noResultsCallBack();
        } else {
            $('.search-comp-no-results').hide();
        }
    }
    var clearSearchResults = function() {
        $searchResutlsHotels.empty();

    }
    $searchInput.on("keyup", debounce(function() {
        disableCheckAvailabilityButton();
        var searchText = $(this).val();
        if (searchText.length > 2) {
            performDestinationHotelSearch(searchText);
        } else {
            $searchResutlsContainer.hide();
        }
    }, SEARCH_INPUT_DEBOUNCE_RATE));

    if(alternateSearch && alternateSearch == 'true') {
        // handle alternate search path
        $searchCompContainer.on('click', function(){
            performDestinationHotelSearch("");
        });
    }

    $searchResutlsContainer.on("click", '.search-result-item', function() {
        $searchInput.val($(this).text());
    })
    $searchResutlsContainer.click(function(e) {
        e.stopPropagation();
    })
    $(document).click(function(e) {
        $searchResutlsContainer.hide();
    });
    var updateResults = function(results, $resultsList, isHotels) {
        console.log("results from search", results);
        console.log("isHotels ", isHotels);
        if (isHotels) {
            var websiteHotels = results.websites;
            var otherHotels = results.others;
            var websiteResults = $('.search-comp-results-list.website');
            var otherResults = $('.search-comp-results-list.others');
            var websiteCount = websiteHotels ? Object.keys(websiteHotels).length : 0;
            var websiteBrand = $('.search-comp-results-sub-container.website');
            var otherCount = otherHotels ? Object.keys(otherHotels).length : 0;
            if(otherCount == 0){
                $('.others').hide();
            }
            var otherBrand = $('.search-comp-results-sub-container.others');
            if (Object.keys(websiteHotels).length || Object.keys(otherHotels).length) {
                if ((Object.keys(websiteHotels).length)) {
                    websiteHotels
                            .forEach(function(websiteHotel) {
                                var reDirectToRoomPath = websiteHotel.path;
                                var hotelId = websiteHotel.id;
                                if (isHotels) {
                                    var ROOMS_PATH = "rooms-and-suites/";

                                    reDirectToRoomPath = websiteHotel.path.replace(".html", "") + ROOMS_PATH;
                                }
                                reDirectToRoomPath = reDirectToRoomPath.replace('//', '/');

                                websiteResults.append('<li class="search-result-item" data-hotel-id="' + hotelId
                                        + '"  data-redirect-path="' + reDirectToRoomPath + '">' + websiteHotel.title
                                        + '</li>');
                            });
                }
                if ((Object.keys(otherHotels).length)) {
                    otherHotels.forEach(function(otherHotel) {
                        var reDirectToRoomPath = otherHotel.path;
                        var hotelId = otherHotel.id;
                        if (isHotels) {
                            var ROOMS_PATH = "rooms-and-suites/";
                            if (otherHotel.path.includes('amastaysandtrails') || otherHotel.path.includes('/ama/')) {
                                ROOMS_PATH = "accommodations/";
                            }
                            reDirectToRoomPath = otherHotel.path.replace(".html", "") + ROOMS_PATH;
                        }
                        if (!reDirectToRoomPath.includes('https')) {
                            reDirectToRoomPath = reDirectToRoomPath.replace('//', '/');
                        }
                        otherResults.append('<li class="search-result-item" data-hotel-id="' + hotelId
                                + '"  data-redirect-path="' + reDirectToRoomPath + '">' + otherHotel.title + '</li>');
                    });
                }
                if (websiteCount == 0 && otherCount == 0) {
                    websiteBrand.hide();
                    otherBrand.hide();
                } else if (websiteCount != 0 && otherCount == 0) {
                    websiteBrand.show();
                    otherBrand.hide();
                } else if (websiteCount == 0 && otherCount != 0) {
                    websiteBrand.hide();
                    otherBrand.show();
                } else if (websiteCount != 0 && otherCount != 0) {
                    websiteBrand.show();
                    otherBrand.show();
                }
            } else {
                $resultsList.closest('.search-comp-results-sub-container').hide();
            }
        } else {
            if ((results.length > 0) && $resultsList) {
                (results).forEach(function(result) {
                    var reDirectToRoomPath = result.path;
                    var hotelId = null ? "" : result.id;
                    reDirectToRoomPath = reDirectToRoomPath.replace('//', '/');
                    $resultsList.append('<li class="search-result-item" data-hotel-id="' + hotelId
                            + '"  data-redirect-path="' + reDirectToRoomPath + '">' + result.title + '</li>');
                });
                $resultsList.closest('.search-comp-results-sub-container').show();
            } else {
                $resultsList.closest('.search-comp-results-sub-container').hide();
            }
        }
    }
};
