document.addEventListener( 'DOMContentLoaded', function() {
	if(dataCache.session.getData("holidayTheme")){		
		if((window.location.href).indexOf('rooms-and-suites')!=-1)
			$('.offers-deals-container').hide();
	}
	var locator = location.href;
	var midStr = 'meeting/meeting-request-quote.html?';
	var laststr = locator.lastIndexOf("?");
	var dynamicStr = locator.substring(laststr+1);
	var finalStr = midStr + dynamicStr ;
	var formatedURL = locator.toString().replace(finalStr, 'meeting.html');
	injectHotelNameForMobileView();
	var entierUrl=locator.split("?");
    locator=entierUrl[0]; 
	
	$( '.tab-child-container' ).each( function() {
	    
        $( this ).removeClass( 'selected' );
        var id = $( this ).text();
        if ( (document.getElementById(id).href === locator) ||(document.getElementById(id).href === formatedURL)) {
            $( this ).addClass( 'selected' );
        } 
    });

    
    
    if($(window).width() < 992) {
        var tabCount = $('.cm-nav-tab-con .tab-child-container').length;
        if(tabCount < 3) {
            $('.more-container-mobile').hide();
        }
        $('.more-tab-child-container > a').each( function() {
            var id = $( this ).text();
            if (document.getElementById(id).href === locator) {
                $('.more-container-mobile').addClass('selected');
                $('.more-container-mobile').html('<span>'+$(this).text()+'</span><img src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow-white.svg" alt  = "drop-down-arrow-white-icon"/>' );
                $(this).css('display', 'none');
                if ($(window).width() > 767) {
                    var selTab = $('.tab-child-container:eq(2)').text();
                   if (id == selTab) {
                       $('.more-container-mobile').html('<span>More</span><img src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt = "drop-down-arrow-icon"/>');
                       $('.more-container-mobile').removeClass('selected');
                   }
               }
             }           
        })
    }
    
    $( '.more-container-mobile' ).click( function() {
        $( '.more-content-wrap' ).css( 'display', 'block' );
    } );

    $( '.more-content-heading .icon-prev-arrow' ).click( function() {
        $( '.more-content-wrap' ).css( 'display', 'none' );
    } );
	
	function injectHotelNameForMobileView() {
		
		var hotelName = getHotelNameFromDom();
		var mobileViewDomArray = $.find("[data-injector-key='hotel-name']");
		var mobileViewConatiner ="";
		if (mobileViewDomArray != undefined) {
			mobileViewConatiner = mobileViewDomArray[0];
		}
		if( hotelName != undefined) {
			$(mobileViewConatiner).text(hotelName);
		}
	}
	
	function getHotelNameFromDom() {
		var hotelNameDomArray = $.find("[data-hotel-name]");
		var hotelNameContainer ="";
		if(hotelNameDomArray.length > 0){
//		if(hotelNameDomArray != undefined) {
			hotelNameContainer=hotelNameDomArray[0];
			if($(hotelNameContainer).data()) {
			    return $(hotelNameContainer).data().hotelName; 
			}
		}
		
	}
    if(window.location.href.includes("drivecation")){
		console.log("Js working");
		var elem = document.querySelector(".tab-child-container.selected");
        elem.style.setProperty("--primaryColorLight", "#0c5d90");
		elem.style.setProperty("--primaryColor", "#002b49");
        elem.style.setProperty("--primaryColorDark", "#002b49");
    }
});
