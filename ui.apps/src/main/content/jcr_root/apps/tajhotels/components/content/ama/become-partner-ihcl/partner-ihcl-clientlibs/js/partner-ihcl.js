window.addEventListener('load', function() {
	console.log("inside js file");
	var successMsg=$('.becomePartnerWrap .successMsg');

	$('.jiva-spa-date-con').click(function(e) {
		e.stopPropagation();
		$(this).siblings('.jiva-spa-date').addClass('visible');
	});

	$('body, .generic-signin-content-holder').click(function(e) {
		$('.jiva-spa-date').removeClass('visible');
	});

	$('#apply-btn').on('click', function(e) {
		e.stopPropagation();
		console.log("inside click fn");
		if (validatePartnerFormInputFields() && captchaValidation) {
			console.log("data proper");
			var partnerFormDetails = getPartnerFormDetails();
			console.log("partner form details: ", partnerFormDetails);

			$('body').showLoader(true);

			$.ajax({
				cache : false,
            	contentType : false,
            	processData : false,
				type : 'POST',
				url : '/bin/partnerwithama',
				data : getPartnerFormDetails(),
				success : function(response) {
					$('body').hideLoader(true);
					preventPageScroll();
					console.log('inside the success of ajax');
					var data = JSON.parse(response);
                    if(data.successMsg){
                    	warningBox({
    						title : successMsg.text()
    					});
                        $(".cm-warning-box").addClass("hide-warn-symbol");

                    }else{
                    	warningBox({
    						title : 'Application to amã Stays & Trails property is not successful'
    					});
                    }
                    callwarningclose();
				},
				error : function(response) {
					$('body').hideLoader(true);
					preventPageScroll();
					warningBox({
						title : 'Application to amã Stays & Trails property is not successful'
					});
                    callwarningclose();
				},

			});
		}
 
	});
	function getPartnerFormDetails() {
		var name = $('input[data-form-id="partner-name"]').val();
		var loc = $('input[data-form-id="partner-loc"]').val();
		var region = $('select[data-form-id="partner-region"]').val();
		var email = $('input[data-form-id="partner-email"]').val();
		var mobile = $('input[data-form-id="partner-mobile"]').val();
		var area = $('input[data-form-id="partner-area"]').val();
		var areaUnit = $('input[data-form-id="partner-area-unit"]').val();
		var noOfBedrooms = $('input[data-form-id="no-of-bedrooms"]').val();
		var msg = $('textarea[data-form-id="msg"]').val();
		
		
		var formData = new FormData();
        formData.append("name", name);
        formData.append("loc", loc);
        formData.append("region", region);
        formData.append("email", email);
        formData.append("mobile", mobile);
        formData.append("area", area);
        formData.append("areaUnit", areaUnit);
        formData.append("noOfBedrooms", noOfBedrooms);
        formData.append("msg", msg);
        
        return formData;
	}

	function validatePartnerFormInputFields() {
		var flag = true;
		var $partnerFormInputElems = $('.partner-form input.sub-form-mandatory,.partner-form select.sub-form-mandatory');
		$partnerFormInputElems.each(function() {
			console.log("value is : " , $(this).val() == "");
			if ($(this).val() == "" || $(this).val() == null || $(this).hasClass('invalid-input')) {
				$(this).addClass('invalid-input');
				flag = false;
				invalidWarningMessage($(this));
			}
		});
		if ($partnerFormInputElems.hasClass('invalid-input')) {
			var $firstInvalidElem = $('form.partner-form').find('.invalid-input').first();
			($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();                        
		}
		console.log("flag value: " + flag);
		return flag;
	}

});
