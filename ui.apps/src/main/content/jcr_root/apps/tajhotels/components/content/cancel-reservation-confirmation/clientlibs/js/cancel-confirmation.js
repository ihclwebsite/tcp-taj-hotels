$(document).ready(function() {
    
    if (dataCache.session.getData('bookingDetailsRequest') === null
            && sessionStorage.getItem('bookingDetailsRequest') === null) {
        console.info('reservation details not found');
    } else {
        buildCancelConfirmPage();
    }

        $('.guest-details-expansion').click(function() {
            if ($(this).hasClass('guest-details-expansion-inverted')) {
                $('.guest-detail-value-mob').css('display', 'none');
                $(this).removeClass('guest-details-expansion-inverted')

            } else {
                $('.guest-detail-value-mob').css('display', 'inline-block');
                $(this).addClass('guest-details-expansion-inverted');
            }
        })

        $('.expan-img').click(function(e) {
            e.stopPropagation();
            $('.cancel-download-options').toggleClass('visible');
        })

        $('body').click(function() {

            $('.cancel-download-options').removeClass('visible');
            $('.confirmation-selected-rooms-amenities').removeClass('visible');
        })

        $('.cancel-ind-arrow').click(function(e) {
            e.stopPropagation();
            $(this).closest('.confirmation-selected-rooms-card').find('.confirmation-selected-rooms-amenities').toggleClass('visible');
            $(this).toggleClass('cancel-ind-reverted');
        })

    });


function buildCancelConfirmPage() {

   var jsonObject;
     if (dataCache.session.getData('bookingDetailsRequest')) {
         jsonObject = dataCache.session.getData('bookingDetailsRequest');
        // dataCache.session.removeData('bookingDetailsRequest');

    } else if (sessionStorage.getItem('bookingDetailsRequest')) {
        jsonObject = sessionStorage.getItem('bookingDetailsRequest');
        // sessionStorage.removeItem('bookingDetailsRequest');
    }
    var cancelledResponse = dataCache.session.getData('cancelledResponse');
    if(cancelledResponse){
        var cancellationCharges = 0;
        $(cancelledResponse).each(function(){
            cancellationCharges += parseFloat(this.cancellationAmount);
        });
        $('#total-cancellation-charges').text(cancellationCharges);
    }
   var roomsBooked = jsonObject.roomList;
   var totalCostWithoutTax, totalCostWithTax;
   var redirect=document.getElementById('redirectUrl').value + ".html"; 
   
   var htmlCode = '';
	 $.each(roomsBooked, function(index, data){
		
		var isKingBed = false;
		var bed = data.bedtype;
		var roomStatus = data.resStatus;
		if (roomStatus == 'Cancelled') {
			roomStatus = "CANCELLED";
			totalCostWithTax = data.roomCostAfterTax;
			totalCostWithoutTax = data.roomCostBeforeTax;
		} else {
			roomStatus = "NOT CANCELLED";
		}
		
		if (typeof bed == 'undefined' || bed == 'king') {
            bed = 'King';
			isKingBed = true;
		}
		
		
		
		htmlCode +=`<div class="cancel-selected-room-checkbox"><input type="checkbox" /><label></label></div>
        <div class="confirmation-selected-rooms-card">
            <div class="cart-selected-rooms-header clearfix">
                <div class="confirmation-room-details">
                    <div class="heading-con">
                        <div class="heading">Reservation Number: ${data.reservationNumber}</div>
                        
                        <div class="cancel-indication">${roomStatus}</div><img class="cancel-ind-arrow" src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt="dropdown" /></div>
                    <div class="room-details"> <span class="room-details-colored">Room ${index+1} - </span><span class="room-details-margin">${data.noOfAdults} Adults, ${data.noOfChilds} Child</span><span class="bed-spec">${bed}</span>
					<span class="bedtype-rooms"><img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon">
					</div>
                </div>
                <div class="confirmation-room-price">
                    <div class="confirmation-banner">
                        <div class="total-price-deal-section">
                            <div class="best-deal-banner"> <img src="/content/dam/tajhotels/icons/style-icons/left-right-clipped.svg" alt="background" /><span>BEST DEAL</span></div>
                            <div class="best-deal-desc">Congratulations! You've bagged the best deal for the day</div>
                        </div>
                    </div>
                    <div class="confirmation-price-con"><span>Price:</span><span class="rupee-text">₹</span><span class="best-deal-price">${data.roomCostAfterTax}</span></div>
                </div>
            </div>
            <div class="cart-selected-rooms-content">
                <div class="confirmation-selected-rooms-amenities">
                    <div class="cart-amenities-title">Executive suite<img class="cart-amenities-img" src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt="dropdown" /></div>
                    <div class="cart-amenities-description">${data.roomTypeDesc}</div>
                    <div class="cart-available-wrp">
                        
                        
                    </div>
                </div>
                <div class="cart-selected-info-wrp">
                    <div class="description-cancel-wrp">
                        <div class="rate-description"><span class="bold-title">Rate description: </span><span class="conf-rate-description-details">${data.rateDescription}</span></div>
                        <div
                            class="cancel-policy"><span class="bold-title">Cancellation policy: </span><span class="confirmation-canc-details">${data.cancellationPolicy}</span></div>
                    <div class="pet-policy"><span class="bold-title">Pet policy: </span><span>Allowed</span></div>
                </div>
            </div><a class="confirmation-cancel" href="/content/tajhotels/en-in/cancel-reservation.html"><button class="cm-btn-primary confirmation-cancel-btn">Cancel</button></a></div>
    </div>`; 
	var roomTypeIcon = `<img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon"><img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon">`;
		if (!isKingBed) {
			$(".bedtype-rooms").html(roomTypeIcon);
		}
	 });
	 
	 $(".jsonData-rooms").html(htmlCode);
    
    var hotelId = jsonObject.hotelId;
	var totalTaxes = totalCostWithTax - totalCostWithoutTax;
	
    if(jsonObject.guest){
        if(jsonObject.guest.name != ""){
    $("#name").html(jsonObject.guest.name);
        }else{
    $("#name").html(jsonObject.guest.title +" "+ jsonObject.guest.name);
        }
    }

    $("#mail, #sp-guest-mail").html(jsonObject.guest.email);
    $("#phoneNo").html(jsonObject.guest.phoneNumber);
    $("#membershipNumber").html(jsonObject.guest.membershipNumber);


    $("#hotelHeading, #sp-hotel-name").html(jsonObject.hotel.hotelName);
    $("#hotelPhoneNumber, #sp-hotel-phone, #hotel-phone").html(jsonObject.hotel.hotelPhoneNumber);
    $("#hotelMailId, #hotel-mail-id").html(jsonObject.hotel.hotelEmail);
	
	$("#total-charges").html(totalCostWithTax);
	$("#total-taxes").html(totalTaxes);

     var checkinDate=jsonObject.checkInDate;
    var checkoutDate=jsonObject.checkOutDate;
	
    var checkInDateFormatted = moment(checkinDate,"YYYY-MM-DD").format("DD MMM YYYY");
    var checkOutDateFormatted = moment(checkoutDate,"YYYY-MM-DD").format("DD MMM YYYY");
	 $("#check-in-date, #sm-checkin-date").html(checkInDateFormatted);
    $("#check-out-date, #sm-checkout-date").html(checkOutDateFormatted);
	

}



