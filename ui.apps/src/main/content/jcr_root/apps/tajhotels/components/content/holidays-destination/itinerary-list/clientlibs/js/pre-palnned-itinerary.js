document.addEventListener( 'DOMContentLoaded', function() {

            $('.pre-planned-itinerary-details-desc').each(function() {
                $(this).cmToggleText({
                    charLimit : 100
                });
            })

            $('.pre-planned-itinerary-our-itinerary-show').click(
                    function() {
                        $(this).closest('.pre-planned-itinerary-our-itinerary-sec').toggleClass(
                                'pre-planned-itinerary-our-itinerary-sec-exp');
                    });

            var entirePrePlannedData = $('.pre-planned-itinerary-sub-section .pre-planned-itinerary-outer-con').clone(
                    true);

            var itineraryTypeSelected = "full-day";

            var itineraryCategorySelected = [];

            $('.itinerary-category-checkbox label').each(function() {
                itineraryCategorySelected.push($(this).text());
            });

            function sortFilter(type, category) {
                $('.pre-planned-itinerary-sub-section').empty();

                for (i = 0; i < entirePrePlannedData.length; i++) {
                    itineraryType = $(entirePrePlannedData[i]).attr('itinerary-type');
                    itineraryCategory = $(entirePrePlannedData[i]).attr('itinerary-category')
                    if ((itineraryType == type)
                            && ((itineraryCategory.indexOf(category[0]) != -1)
                                    || (itineraryCategory.indexOf(category[1]) != -1) || (itineraryCategory
                                    .indexOf(category[2]) != -1))) {
                        $('.pre-planned-itinerary-sub-section').append(entirePrePlannedData[i])
                    } else {
                        // do nothing
                    }
                }
                $('.jiva-spa-show-more').remove();
                $('.pre-planned-itinerary-section').showMore();

                if ($('.pre-planned-itinerary-wrap .jiva-spa-show-more').length) {
                    $('.pre-planned-itinerary-book-now').css('bottom', '3.75rem');
                }

                else {
                    $('.pre-planned-itinerary-book-now').css('bottom', '1.75rem');
                }
            }

            // sortFilter(itineraryTypeSelected,itineraryCategorySelected);

            $('.itinerary-plan-radio-btn input').click(function() {
                itineraryTypeSelected = $(this).val();
                // sortFilter(itineraryTypeSelected,itineraryCategorySelected);
                $('.pre-planned-itinerary-full-day').toggle(itineraryTypeSelected == 'full-day');
                $('.pre-planned-itinerary-half-day').toggle(itineraryTypeSelected == 'half-day');
            });
        });

$(".pre-planned-itinerary-view-all").click(function() {
    // action goes here!!
});
