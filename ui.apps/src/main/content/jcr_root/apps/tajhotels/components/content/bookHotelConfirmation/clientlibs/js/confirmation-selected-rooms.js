function confirmationSelectedRooms(){
    $( 'document' ).ready( function() {
        $( '.cart-amenities-title i.icon-drop-down-arrow' ).click( function(e) {            
            e.stopPropagation();
            $( this ).parent().siblings( '.cart-available-wrp' ).toggleClass( 'visible' );
            $( this ).closest('.cart-selected-rooms-content').find( '.cart-selected-info-wrp' ).toggleClass( 'visible' );
			$(this).toggleClass('cm-rotate-show-more-icon');            
        } )
    
        $( '.cart-available-wrp, .cart-selected-info-wrp' ).click(function (e) {
            e.stopPropagation();
        })
    
         
        $('.conf-rate-description-details').cmToggleText({
            charLimit:280,
            showVal:'Show More',
            hideVal:'Show less'
        });
        $('.confirmation-canc-details').cmToggleText({
            charLimit:280,
            showVal:'Show More', 
            hideVal:'Show less'
        }); 
    } )
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb25maXJtYXRpb24tc2VsZWN0ZWQtcm9vbXMvY29uZmlybWF0aW9uLXNlbGVjdGVkLXJvb21zLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoICdkb2N1bWVudCcgKS5yZWFkeSggZnVuY3Rpb24oKSB7XHJcbiAgICAkKCAnLmNhcnQtYW1lbml0aWVzLXRpdGxlIGltZycgKS5jbGljayggZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgJCggdGhpcyApLnBhcmVudCgpLnNpYmxpbmdzKCAnLmNhcnQtYXZhaWxhYmxlLXdycCcgKS50b2dnbGVDbGFzcyggJ3Zpc2libGUnICk7XHJcbiAgICAgICAgJCggdGhpcyApLnBhcmVudHMoKS5lcSggMSApLnNpYmxpbmdzKCAnLmNhcnQtc2VsZWN0ZWQtaW5mby13cnAnICkudG9nZ2xlQ2xhc3MoICd2aXNpYmxlJyApO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCQoIHRoaXMgKS5wYXJlbnQoKS5lcSggMSApLmh0bWwoKSk7XHJcbiAgICB9IClcclxuXHJcbiAgICAkKCAnLmNhcnQtYXZhaWxhYmxlLXdycCwgLmNhcnQtc2VsZWN0ZWQtaW5mby13cnAnICkuY2xpY2soZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgfSlcclxuXHJcbiAgICAkKCcuY29uZi1yYXRlLWRlc2NyaXB0aW9uLWRldGFpbHMnKS5jbVRvZ2dsZVRleHQoe1xyXG4gICAgXHRjaGFyTGltaXQ6MjUwLFxyXG4gICAgXHRzaG93VmFsOidTaG93IE1vcmUnLFxyXG4gICAgXHRoaWRlVmFsOidTaG93IGxlc3MnXHJcbiAgICB9KTtcclxuICAgICQoJy5jb25maXJtYXRpb24tY2FuYy1kZXRhaWxzJykuY21Ub2dnbGVUZXh0KHtcclxuICAgIFx0Y2hhckxpbWl0OjEwMCxcclxuICAgIFx0c2hvd1ZhbDonU2hvdyBNb3JlJyxcclxuICAgIFx0aGlkZVZhbDonU2hvdyBsZXNzJ1xyXG4gICAgfSk7XHJcbn0gKSJdLCJmaWxlIjoibWFya3VwL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLXNlbGVjdGVkLXJvb21zL2NvbmZpcm1hdGlvbi1zZWxlY3RlZC1yb29tcy5qcyJ9
