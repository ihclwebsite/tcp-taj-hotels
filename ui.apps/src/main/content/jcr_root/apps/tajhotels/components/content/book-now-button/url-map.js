use(function() {
    if (this.reqUrl) {
        response = request.resourceResolver.map(this.reqUrl + ".html")
    } else {
        response = this.reqUrl
    }

    if (!this.isTajhotelsHost && response.startsWith('https://www.tajhotels.com')) {
        response = response.substring(25)
    } else if (!this.isTajhotelsHost && response.startsWith('https://www.vivantahotels.com')) {
        response = response.substring(29)
    } else if (!this.isTajhotelsHost && response.startsWith('https://www.seleqtionshotels.com')) {
        response = response.substring(32)
    }

    return {
        mappedUrl : response
    };
});