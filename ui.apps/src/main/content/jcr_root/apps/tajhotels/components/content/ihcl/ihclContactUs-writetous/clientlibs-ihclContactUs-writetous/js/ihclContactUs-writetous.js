$(document)
        .ready(
                function() {
                    var $contactFormCheckIn = $('#contact-form-check-in');
                    var $contactFormCheckOut = $('#contact-form-check-out');
                    $contactFormCheckIn.datepicker({
                        container : $('#contact-form-check-in-container')
                    }).on('change', function() {
                        var checkInDate = $contactFormCheckIn.datepicker('getDate');
                        var checkOutDate = $contactFormCheckOut.datepicker('getDate');
                        if (checkInDate > checkOutDate)
                            $contactFormCheckOut.datepicker('setDate', checkInDate);
                    });

                    $('#contact-form-check-out').datepicker({
                        container : $('#contact-form-check-out-container')
                    }).on('change', function() {
                        var checkInDate = $contactFormCheckIn.datepicker('getDate');
                        var checkOutDate = $contactFormCheckOut.datepicker('getDate');
                        if (checkOutDate < checkInDate)
                            $contactFormCheckIn.datepicker('setDate', checkOutDate);
                    });

                    var validateRequestQuoteElements = function() {
                        $('.sub-form-mandatory').each(function() {
                            if ($(this).val() == "") {
                                $(this).addClass('invalid-input');
                                invalidWarningMessage($(this));
                            }
                        });
                    };

                    // Request quote submit handler
                    $('.request-quote-submit-btn').click(function() {
                        validateRequestQuoteElements();
                        if ($('.contact-us-form-cont .sub-form-input-element').hasClass('invalid-input')) {
                            $('.invalid-input').first().focus();
                        } else {
                            $('.spinner-con').show();
                            initiateServletCall();
                        }
                    });

                    $(
                            '.hotels-event-confirmation-overlay .event-confirmation-close-icon, .event-confirmation-close-btn')
                            .click(function() {
                                $('.hotels-event-confirmation-popup').hide();
                                $(".cm-page-container").removeClass('prevent-page-scroll');
                            });
                    $('.hotels-event-confirmation-inner-wrp').click(function(e) {
                        e.stopPropagation();
                    });

                    $('.hotels-event-error-overlay .event-error-close-icon, .event-confirmation-close-btn').click(
                            function() {
                                $('.hotels-event-error-popup').hide();
                                $(".cm-page-container").removeClass('prevent-page-scroll');
                            });
                    $('.hotels-event-error-inner-wrp').click(function(e) {
                        e.stopPropagation();
                    });

                    $(document)
                            .keydown(
                                    function(e) {
                                        if ((($('.hotels-event-error-overlay').is(':visible')) || ($('.hotels-event-confirmation-overlay')
                                                .is(':visible')))
                                                && (e.which === 27)) {
                                            $('.event-confirmation-close-btn').trigger("click");
                                        }

                                    });
                });

function initiateServletCall() {
    var requestData = {
        fullName : $('#form-name').val(),
        mobile : $('#form-phone').val(),
        emailId : $('#form-email').val(),
        feedback : $('#form-feedback').val(),
        hotelname : getHotelName(),
        city : getHotelCity(),
        checkInDate : typeof ($('#contact-form-check-in').val()) === 'undefined' ? "" : $('#contact-form-check-in')
                .val(),
        checkOutdate : typeof ($('#contact-form-check-out').val()) === 'undefined' ? "" : $('#contact-form-check-out')
                .val()
    };

    $.ajax({
        type : "GET",
        url : '/bin/contactus',
        data : requestData,
        dataType : "json",
        success : function(data) {
            $('.spinner-con').hide();
            $('.hotels-event-confirmation-popup').show();
            $(".cm-page-container").addClass('prevent-page-scroll');
        },
        error : function(textStatus, errorThrown) {
            $('.spinner-con').hide();
            $('.hotels-event-error-popup').show();
            $(".cm-page-container").addClass('prevent-page-scroll');
        }
    });
}

function getHotelName() {
    var hotelName = $('#form-hotelname').val();
    if (typeof (hotelName) === 'undefined') {
        hotelName = "";
    }
    return hotelName;
}

function getHotelCity() {
    var hotelCity = $('#form-city').val();
    if (typeof (hotelCity) === 'undefined') {
        hotelCity = "";
    }
    return hotelCity;
}
