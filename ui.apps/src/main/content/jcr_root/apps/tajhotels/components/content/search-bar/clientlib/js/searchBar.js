window.addEventListener('load', function() {
    var searchSelector = "#home-search";
    var searchWidget = $(searchSelector);
    var searchPath = $('#searchPath').val();
    var otherWebsitePath = $('#searchOtherWebsitePath').val();
    var searchInput = $(searchSelector).find(".searchbar-input");
    var searchBarWrap = searchInput.closest(".searchBar-wrap");
    var wholeWrapper = searchBarWrap.closest('.search-and-suggestions-wrapper');
    var suggestionsContainer = searchBarWrap.siblings('.suggestions-wrap');
    var suggestionsWrapper = suggestionsContainer.find('.suggestions-container');
    var searchSuggestionsContainer = suggestionsWrapper.children('.search-suggestions-container');
    var trendingSuggestionsContainer = suggestionsWrapper.children('.trending-suggestions-container');
    var closeIcon = searchInput.siblings('.close-icon');
    var resultSuggestionContainer = searchWidget.find("#suggestion-cont");
    var hotelResultCont = searchWidget.find('#hotel-result-cont');
    var websiteHotelResults = hotelResultCont.find('#website-hotel-result');
    var otherHotelResults = hotelResultCont.find('#others-hotel-result');
    var destResultCont = searchWidget.find('#dest-result-cont');
    var destResults = destResultCont.find('#dest-result');
    var restaurantResultCont = searchWidget.find('#restrnt-result-cont');
    var websiteRestaurantResults = restaurantResultCont.find('#website-restrnt-result');
    var otherRestaurantResults = restaurantResultCont.find('#others-restrnt-result');
    var experienceResultsCont = searchWidget.find('#exp-result-cont');
    var websiteExperienceResults = experienceResultsCont.find('#website-exp-result');
    var otherExperienceResults = experienceResultsCont.find('#others-exp-result');

    var tabCont = searchSuggestionsContainer.find(".destination-page-nav-menu");
    var hotelTab = searchSuggestionsContainer.find("#Hotels");
    var restaurantsTab = searchSuggestionsContainer.find("#Dining");
    var experiencesTab = searchSuggestionsContainer.find("#Experiences");

    var noResultsText = searchWidget.find('#NoResults');

    var holidayResultsCont = searchWidget.find('#holiday-result-cont');
    var holidayResults = holidayResultsCont.find('#holiday-result');
    var holidayTab = searchSuggestionsContainer.find("#Holiday");
    var ifHolidayPage = false;
    var pageScopeData = $('#page-scope').attr('data-pagescope');

    var holidayHotelResultsCont = searchWidget.find('#holiday-hotel-result-cont');
    var holidayHotelResults = holidayHotelResultsCont.find('#holiday-hotel-result');

    var keys = {
        37 : 1,
        38 : 1,
        39 : 1,
        40 : 1,
        32 : 1,
        33 : 1,
        34 : 1,
        35 : 1,
        36 : 1
    };
    var SEARCH_INPUT_DEBOUNCE_RATE = 1000;

    var preventDefault = function(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    var preventDefaultForScrollKeys = function(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    searchWidget.on('click', '.individual-trends', function() {
        var bookingOptions = dataCache.session.getData("bookingOptions");
        bookingOptions.targetEntity = '';
        dataCache.session.setData("bookingOptions");
    });

    function hideSuggestionsContainer() {
        if (!$(suggestionsContainer).hasClass('display-none')) {
            $(suggestionsContainer).addClass('display-none');
        }
        // $('body').css('overflow', 'auto');

        if (deviceDetector.checkDevice() == "small") {
            $('.cm-page-container').removeClass("prevent-page-scroll");
        } else {
            $('body').css('overflow', 'auto');
        }

        $(document).off("keyup", searchSelector);
        $(document).off("click", searchSelector);
    }

    function showSuggestionsContainer() {
        $(suggestionsContainer).removeClass('display-none');
        // $('body').css('overflow', 'hidden');
        if (deviceDetector.checkDevice() == "small") {
            $('.cm-page-container').addClass("prevent-page-scroll");
        } else {
            $('body').css('overflow', 'hidden');
        }

        $(document).on("keyup", searchSelector, function(e) {
            // Esc pressed
            if (e.keyCode === 27) {
                $(searchInput).blur();
                hideSuggestionsContainer();
            }
        });

        $(document).on("click", searchSelector, function(e) {
            e.stopPropagation();
            hideSuggestionsContainer();
            if (!$(searchSuggestionsContainer).hasClass('display-none')) {
                $(searchSuggestionsContainer).addClass('display-none');
            }
            if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
                $(trendingSuggestionsContainer).addClass('display-none');
            }
            $(wholeWrapper).removeClass('input-scroll-top');
        });
    }

    $(searchInput).on('click', function(e) {
        e.stopPropagation();
        if (searchInput.val()) {
            if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
                $(trendingSuggestionsContainer).addClass('display-none');
            }
            if ($(searchSuggestionsContainer).hasClass('display-none')) {
                $(searchSuggestionsContainer).removeClass('display-none');
            }
        } else {
            if (!$(searchSuggestionsContainer).hasClass('display-none')) {
                $(searchSuggestionsContainer).addClass('display-none');
            }
            if ($(trendingSuggestionsContainer).hasClass('display-none')) {
                $(trendingSuggestionsContainer).removeClass('display-none');
            }
        }
        showSuggestionsContainer();

        if (window.matchMedia('(max-width: 767px)').matches) {
            $(closeIcon).removeClass('display-none');
            $(closeIcon).css("display", "inline-block");
            searchBarWrap.detach().prependTo(suggestionsWrapper);
            searchInput.focus();
        } else {
            var parentOffset = $('.cm-page-container').offset();
            var targetOffset = $(wholeWrapper).offset();
            $('html,body').animate({
                scrollTop : (parentOffset.top * -1) + targetOffset.top
            }, 300);
        }
    });

    $(suggestionsWrapper).on("click", function(event) {
        event.stopPropagation();
    });

    $(closeIcon).on("click", function(e) {
        e.stopPropagation();
        $(wholeWrapper).removeClass('input-scroll-top');
        $(closeIcon).addClass('display-none');
        $(closeIcon).css("display", "none");
        if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
            $(trendingSuggestionsContainer).addClass('display-none');
        }
        if (!$(searchSuggestionsContainer).hasClass('display-none')) {
            $(searchSuggestionsContainer).addClass('display-none');
        }
        searchBarWrap.detach().prependTo(wholeWrapper);
        hideSuggestionsContainer();
    });

    $(searchInput).on("keyup", debounce(function(e) {
        e.stopPropagation();
        if (e.keyCode !== 27 && e.keyCode !== 40 && e.keyCode !== 38 && e.keyCode !== 13) {
            if (searchInput.val().length > 2) {
                performSearch(searchInput.val(), searchPath).done(function() {
                    $(suggestionsContainer).removeClass('display-none');
                    if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
                        $(trendingSuggestionsContainer).addClass('display-none');
                    }
                    $(searchSuggestionsContainer).removeClass('display-none');
                });
            } else {
                noResultsText.hide();
                $(suggestionsContainer).removeClass('display-none');
                if (!$(searchSuggestionsContainer).hasClass('display-none')) {
                    $(searchSuggestionsContainer).addClass('display-none');
                }
                $(trendingSuggestionsContainer).removeClass('display-none');
            }
        } else {
            chooseDownUpEnterList(e);
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            $(closeIcon).removeClass('display-none');
            $(closeIcon).css('display', 'inline-block');
        }
    }, SEARCH_INPUT_DEBOUNCE_RATE));

    // Seach List to key up, down to show
    function chooseDownUpEnterList(e) {
        var $listItems = $('.individual-trends:visible');
        var $selected = $listItems.filter('.active');
        var $current = $selected;
        var currentIndex = 0;
        var listLength = $listItems.length;
        if (e.keyCode == 40) {
            $listItems.removeClass('active');
            if ($selected.length == 0) {
                $current = $listItems.first();
            } else {
                currentIndex = $listItems.index($current);
                currentIndex = (currentIndex + 1) % listLength;
                $current = $listItems.eq(currentIndex);
            }
            $current.addClass('active');
        }
        if (e.keyCode == 38) {
            $listItems.removeClass('active');
            if ($selected.length == 0) {
                $current = $listItems.last();
            } else {
                currentIndex = $listItems.index($current);
                currentIndex = (currentIndex - 1) % listLength;
                $current = $listItems.eq(currentIndex);
            }
            $current.addClass('active');
        }
        if (e.keyCode == 13) {
            if ($current.hasClass("active")) {
                $($current).focus();
                // $($current).trigger('click');
                location.href = $($current).attr('href');
            }
        }
    }

    // // left: 37, up: 38, right: 39, down: 40,
    // // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home:
    // 36

    function performSearch(key) {
        return $.ajax(
                {
                    method : "GET",
                    url : "/bin/search.data/" + searchPath.replace(/\/content\//g, ":") + "/"
                            + otherWebsitePath.replace(/\/content\//g, ":").replace(",", "_") + "/" + key
                            + "/result/searchCache.json"

                }).done(function(res) {
            clearSearchResults();
            addSuggestions(res.suggestions);
            addHotelSearchResults(res.hotels);
            addDestSearchResults(res.destinations);
            addRestrntSearchResults(res.restaurants);
            addExperiencesSearchResults(res.experiences);
            addHolidaySearchResults(res.holidays);
            addHolidayHotelSearchResults(res.holidayHotels);
            initiateTabs(res);
            holidayFunction(res);

        }).fail(function() {
            clearSearchResults();
        });
    }

    function isHolidayResultAvailable() {
        if (holidayResults.children().length > 0) {
            return true;
        } else {
            return false;
        }
    }
    function isHolidayHotelResultAvailable() {
        if (holidayHotelResults.children().length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function holidayFunction(res) {
        if (pageScopeData == "Taj Holidays") {
            // hide all tab and restaurant,experience contains in holiday page
            ifHolidayPage = true;
            restaurantsTab.addClass('display-none');
            experiencesTab.addClass('display-none');
            hotelTab.addClass('display-none');
            holidayTab.addClass('display-none');

            restaurantResultCont.hide();
            experienceResultsCont.hide();

            if (isHolidayResultAvailable() || isHolidayHotelResultAvailable()) {
                holidayResultsCont.show();
                holidayHotelResultsCont.show();

                hotelResultCont.hide();
                destResultCont.hide();

                if (!isHolidayResultAvailable())
                    holidayResultsCont.hide();

                else if (!isHolidayHotelResultAvailable())

                    holidayHotelResultsCont.hide();

            }
            showNoResultsHoliday(res);

        } else {
            showNoResults(res);
        }
    }
    function showNoResults(res) {
        if ((Object.keys(res.hotels.website).length == 0) && (Object.keys(res.hotels.others).length == 0)
                && (Object.keys(res.destinations).length == 0) && (Object.keys(res.restaurants.website).length == 0)
                && (Object.keys(res.restaurants.others).length == 0)
                && (Object.keys(res.experiences.website).length == 0)
                && (Object.keys(res.experiences.others).length == 0))
            noResultsText.show();
        else
            noResultsText.hide();
    }

    function showNoResultsHoliday(res) {
        if ((Object.keys(res.hotels.website).length == 0) && (Object.keys(res.hotels.others).length == 0)
                && (Object.keys(res.destinations).length == 0) && (Object.keys(res.holidays).length == 0)
                && (Object.keys(res.holidayHotels).length == 0))
            noResultsText.show();
        else
            noResultsText.hide();
    }

    function addSuggestions(suggestions) {
        if (Object.keys(suggestions).length) {
            suggestions.forEach(function(suggestion) {
                var resultHtml = '<a class="individual-common-suggesion">' + suggestion + '</a>';
                resultSuggestionContainer.append(resultHtml);
            });
            resultSuggestionContainer.show();
        }
    }

    function clearSearchResults() {
        resultSuggestionContainer.empty().hide();
        hotelResultCont.hide();
        websiteHotelResults.empty();
        otherHotelResults.empty();
        destResultCont.hide();
        destResults.empty();
        restaurantResultCont.hide();
        websiteRestaurantResults.empty();
        otherRestaurantResults.empty();
        experienceResultsCont.hide();
        websiteExperienceResults.empty();
        otherExperienceResults.empty();
        holidayResultsCont.hide();
        holidayResults.empty();
        holidayHotelResultsCont.hide();
        holidayHotelResults.empty();
        removeAllTabs();
    }

    function addHotelSearchResults(hotels) {
        if (Object.keys(hotels).length) {
            var websiteHotels = hotels.website;
            var otherHotels = hotels.others;
            if (Object.keys(websiteHotels).length) {
                websiteHotels.forEach(function(hotel) {
                    if (pageScopeData == "Taj Holidays") {
                        var hotelPath = hotel.path.replace(".html", "");
                        hotelPath = hotelPath + "rooms-and-suites.html";
                    } else {
                        var hotelPath = hotel.path;
                    }
                    if (hotelPath != "" && hotelPath != null && hotelPath != undefined) {
                        hotelPath = hotelPath.replace("//", "/");
                    }
                    var resultHtml = createSearchResult(hotel.title, hotelPath);
                    websiteHotelResults.append(resultHtml);
                    hotelResultCont.find('.website-result').show();
                });
            }

            if (otherHotels && Object.keys(otherHotels).length) {
                otherHotels.forEach(function(hotel) {
                    if (pageScopeData == "Taj Holidays") {
                        var hotelPath = hotel.path.replace(".html", "");
                        hotelPath = hotelPath + "rooms-and-suites.html";
                    } else {
                        var hotelPath = hotel.path;
                    }
                    if (hotelPath != "" && hotelPath != null && hotelPath != undefined) {
                        if (!hotelPath.includes('https')) {
                            hotelPath = hotelPath.replace("//", "/");
                        }
                    }
                    var resultHtml = createSearchResult(hotel.title, hotelPath);
                    otherHotelResults.append(resultHtml);
                    hotelResultCont.find('.others-result').show();
                });
            }
            if (websiteHotels && Object.keys(websiteHotels).length == 0) {
                hotelResultCont.find('.website-result').hide();

            }
            if (otherHotels && Object.keys(otherHotels).length == 0) {
                hotelResultCont.find('.others-result').hide();
            }
        }
    }

    function addDestSearchResults(dests) {
        if (Object.keys(dests).length) {
            dests.forEach(function(dest) {
                var resultHtml = createSearchResult(dest.title, dest.path);
                destResults.append(resultHtml);
            });
        }
    }

    function addRestrntSearchResults(restaurants) {
        if (Object.keys(restaurants).length) {
            var websiteRestaurants = restaurants.website;
            var otherRestaurants = restaurants.others;
            if (Object.keys(websiteRestaurants).length) {
                websiteRestaurants.forEach(function(restaurant) {
                    var resultHtml = createSearchResult(restaurant.title, restaurant.path);
                    websiteRestaurantResults.append(resultHtml);
                    restaurantResultCont.find('.website-result').show();
                });
            }

            if (Object.keys(otherRestaurants).length) {
                otherRestaurants.forEach(function(restaurant) {
                    var resultHtml = createSearchResult(restaurant.title, restaurant.path);
                    otherRestaurantResults.append(resultHtml);
                    restaurantResultCont.find('.others-result').show();
                });
            }
            if (Object.keys(websiteRestaurants).length == 0) {
                restaurantResultCont.find('.website-result').hide();
            }
            if (Object.keys(otherRestaurants).length == 0) {
                restaurantResultCont.find('.others-result').hide();
            }
        }
    }

    function addExperiencesSearchResults(experiences) {
        if (Object.keys(experiences).length) {
            var websiteExperiences = experiences.website;
            var otherExperiences = experiences.others;
            if (Object.keys(websiteExperiences).length) {
                websiteExperiences.forEach(function(experience) {
                    var resultHtml = createSearchResult(experience.title, experience.path);
                    websiteExperienceResults.append(resultHtml);
                    experienceResultsCont.find('.website-result').show();
                });
            }

            if (Object.keys(otherExperiences).length) {
                otherExperiences.forEach(function(experience) {
                    var resultHtml = createSearchResult(experience.title, experience.path);
                    otherExperienceResults.append(resultHtml);
                    experienceResultsCont.find('.others-result').show();
                });
            }
            if (Object.keys(websiteExperiences).length == 0) {
                experienceResultsCont.find('.website-result').hide();
            }
            if (Object.keys(otherExperiences).length == 0) {
                experienceResultsCont.find('.others-result').hide();
            }
        }
    }

    function addHolidaySearchResults(holidays) {
        if (Object.keys(holidays).length) {
            holidays.forEach(function(holidays) {
                if (holidays.title != null) {
                    var resultHtml = createSearchResult(holidays.title, holidays.path);
                    holidayResults.append(resultHtml);
                }
            });
        }
    }

    function addHolidayHotelSearchResults(holidayHotel) {
        if (Object.keys(holidayHotel).length) {
            holidayHotel.forEach(function(holidayHotel) {
                var resultHtml = createSearchResult(holidayHotel.title, holidayHotel.path);
                holidayHotelResults.append(resultHtml);
            });
        }
    }

    function isDestinationsResultAvailable() {
        if (destResults.children().length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function createSearchResult(title, path) {
        return '<a class="individual-trends" href="' + path + '">' + title + '</a>';
    }
    if ($('.home-page-layout, .holidays-homepage-layout').length > 0) {
        var stickyOffset = $('#home-search.search-container .search-and-suggestions-wrapper').offset() ? $(
                '#home-search.search-container .search-and-suggestions-wrapper').offset().top : null;

        $(window).scroll(
                function() {
                    var sticky = $('#home-search.search-container .search-and-suggestions-wrapper'), scroll = $(window)
                            .scrollTop();

                    if (scroll >= stickyOffset)
                        sticky.addClass('mr-stickyScroll');
                    else
                        sticky.removeClass('mr-stickyScroll');
                });
    }

    hotelTab.on('click', function() {
        hotelResultCont.show()
        if (isDestinationsResultAvailable()) {
            destResultCont.show();
        }
        if (ifHolidayPage == true) {
            hotelTabOnHoliday();
        }
        restaurantResultCont.hide()
        experienceResultsCont.hide()
        activateTab($(this))
    });

    restaurantsTab.on('click', function() {
        hotelResultCont.hide()
        destResultCont.hide()
        experienceResultsCont.hide()
        restaurantResultCont.show()
        activateTab($(this))
    });

    experiencesTab.on('click', function() {
        hotelResultCont.hide()
        destResultCont.hide()
        restaurantResultCont.hide()
        experienceResultsCont.show()
        activateTab($(this))
    });

    // for holiday
    holidayTab.on('click', function() {
        hotelResultCont.hide();
        destResultCont.hide();
        restaurantResultCont.hide();
        holidayResultsCont.show();
        activateTab($(this));

    });

    // on holiday page -on click on hotel tab
    function hotelTabOnHoliday() {
        holidayHotelResultsCont.hide();
        holidayResultsCont.hide();

    }

    /*
     * Search results will show a tab incase there are results across hotels , restaurants and experiences The logic
     * below dynamically creates the tabs based on the result. incase all three results are available all the tabs are
     * shown and the hotel tab is active by default Incase only two results are available only two tabs are shown . The
     * order of activation being hotel and then restaurants.If only one result set is available no tabs are displayed
     */
    function initiateTabs(res) {
        removeAllTabs();
        if (Object.keys(res.hotels.website).length || Object.keys(res.hotels.others).length) {
            if (Object.keys(res.restaurants.website).length || Object.keys(res.restaurants.others).length) {
                hotelTab.removeClass("display-none");
                restaurantsTab.removeClass("display-none");

            }
            if (Object.keys(res.experiences.website).length || Object.keys(res.experiences.others).length) {
                hotelTab.removeClass("display-none");
                experiencesTab.removeClass("display-none");

            }
            // show destination only if exists along with hotel
            if (Object.keys(res.destinations).length) {
                destResultCont.show();
            }
            hotelTab.click()
        } else if (Object.keys(res.restaurants.website).length || Object.keys(res.restaurants.others).length) {
            if (Object.keys(res.experiences.website).length || Object.keys(res.experiences.others).length) {
                restaurantsTab.removeClass("display-none");
                experiencesTab.removeClass("display-none");
            }
            restaurantsTab.click()
        } else if (Object.keys(res.experiences.website).length || Object.keys(res.experiences.others).length) {
            experiencesTab.click()
        }
    }

    function removeAllTabs() {
        tabCont.find("a").addClass("display-none");
    }

    function activateTab(tab) {
        tabCont.find("a").removeClass("selected");
        tab.addClass("selected");
    }

});
$('.searchbar-input').val('');
var initiateNavPreloginSearch = function() {
    var navPreloginSearch = $('.header-nav-prelogin-search');
    $('.gb-search-con').click(function() {
        navPreloginSearch.show().promise().then(function() {
            navPreloginSearch.find('.searchbar-input').click();
        });
    });
    $('.nav-prelogin-close, .closeIconImg ,.cm-overlay').click(function() {
        navPreloginSearch.hide();
    });
}

initiateNavPreloginSearch();
