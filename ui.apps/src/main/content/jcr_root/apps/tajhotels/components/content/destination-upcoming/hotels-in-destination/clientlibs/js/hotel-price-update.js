$(document).ready(function() {
  var hotelData = {};
  $.ajax({
    url: "/etc/tajhotels/in/en/our-hotels/mumbai.infinity.json",
    cache: false,
    success: function(data) {
      $("div[id^=fullHotelCard]").each(function() {
        hotelData.id = this.id;
        hotelData.name = $("#" + hotelData.id).find('.mr-list-hotel-title').html().trim();
        $.each(data, function(index, element) {
          if (element.name === hotelData.name) {
            $("#" + hotelData.id).find('.current-rate').html(element.averagePrice);
            $("#" + hotelData.id).find('.discounted-rate').html(element.higestPrice);

          }
        });
      });

      $("div[id^=hotelcard]").each(function() {
        hotelData.id = this.id;
        hotelData.name = $("#" + hotelData.id).find('.mapHotel-title').html().trim();
        $.each(data, function(index, element) {
          if (element.name === hotelData.name) {
            $("#" + hotelData.id).find('.current-rate').html(element.averagePrice);
            $("#" + hotelData.id).find('.discounted-rate').html(element.higestPrice);

          }
        });
      });

      $("div[id^=carouselHotelcard]").each(function() {
        hotelData.id = this.id;
        hotelData.name = $("#" + hotelData.id).find('.hotelDetailsHeading').html().trim();
        $.each(data, function(index, element) {
          if (element.name === hotelData.name) {
            $("#" + hotelData.id).find('.current-rate').html(element.averagePrice);
            $("#" + hotelData.id).find('.discounted-rate').html(element.higestPrice);

          }
        });
      });
    }
  });
});
