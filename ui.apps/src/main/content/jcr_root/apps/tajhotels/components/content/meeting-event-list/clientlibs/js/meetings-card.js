$(document)
        .ready(
                function() {

                    $('.event-card-container').showMore();

/*
                    if($("#moreSeating").text()){
                        document.getElementById("moreSeatingContent").innerHTML = $("#moreSeating").text();
                	}
                    $('#showMoreSeating').on('click', function() {
                        console.log("Click of more seating",$(this));
                         var valueToBeShown = $(this).siblings('#moreSeatingContent').children('div').html();
						 $(this).siblings('#displayContent').html(valueToBeShown).toggle();
                        console.log("valueToBeShown",valueToBeShown);
                        //$(this).siblings('#moreSeatingContent').children('span').html();
                    });
                    */
                    attachShowMoreEvent();

                    $('.events-card-desc').each(function() {
                        $(this).cmToggleText({
                            charLimit : 180,
                        })
                    });
                    if($('.events-cards-view-amenities-btn').attr('data-view-all-amentities') == 'open'){
                        $('.events-cards-view-amenities-btn').attr('data-view-all-amentities','close');
                        $('.event-card-container').delegate(".events-cards-view-amenities-btn", "click", function(e) {
                            e.stopPropagation();
                            $(this).find('.icon-drop-down-arrow').toggleClass('events-inverted');
                            $(this).toggleClass('events-cards-view-amenities-btn-selected');
                            $(this).parents().eq(2).siblings('.event-expansion-con').toggleClass('visible');
                            $('.events-services-entertainment-section').removeClass('visible');
                            $('.events-exp-show-more').text('Show More');
                        })
                    }
                    else{
                        $('.events-cards-view-amenities-btn').attr('data-view-all-amentities','open');
                    }

                    $('.event-expansion-con').click(function(e) {
                        e.stopPropagation();
                    })

                    $('.events-exp-show-more').click(function() {
                        if ($(this).text() == 'Show More') {
                            $(this).text('Show Less');
                            $(this).siblings('.events-services-entertainment-section').addClass('visible');
                        } else {
                            $(this).text('Show More');
                            $(this).siblings('.events-services-entertainment-section').removeClass('visible');
                        }
                    })

                    $('.events-card-other-show-more').click(
                            function() {
                                var element = $(this).closest('.events-card-other-details-con').find(
                                        '.events-card-other-details-sub');
                                element.toggleClass('events-card-other-details-sub-show-more');
                                $(this).find('.icon-drop-down-arrow').toggleClass('events-inverted');
                                $(this).find('.events-card-other-show-more-text').toggleClass('cm-hide');
                            })

                    $('.specific-hotels-events-page')
                            .click(
                                    function(e) {
                                        $('.events-cards-view-amenities-btn')
                                                .html(
                                                        'VIEW DETAILS<img src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt  = "drop-down-arrow-icon">')
                                        $('.events-cards-view-amenities-btn').removeClass(
                                                'events-cards-view-amenities-btn-selected')
                                        $('.event-expansion-con').removeClass('visible');
                                        $('.events-services-entertainment-section').removeClass('visible');
                                        $('.events-exp-show-more').text('Show More');
                                    })
                    // addHotelDetailsToHref();

                });

function addHotelDetailsToHref() {
    var hrefArr = $('a.events-cards-request-btn');
    $(hrefArr).each(function() {
        var _href = $(this).attr("href");
        $(this).attr("href", _href + '&hotelWas=' + JSON.stringify(pageLevelData));
    })
}

function setRequestQuoteSessionObj(venueName, hotelName, roomCapacity, requestQuoteEmailId) {

    var requestQuoteObj = {};
    requestQuoteObj.venueName = venueName;
    requestQuoteObj.hotelName = hotelName;
    requestQuoteObj.roomCapacity = roomCapacity;
    requestQuoteObj.requestQuoteEmailId = requestQuoteEmailId;

    dataCache.session.setData('requestQuoteDetails', requestQuoteObj);

    console.log("requestQuoteDetailObj :: > ", dataCache.session.getData("requestQuoteDetails"));

}


function attachShowMoreEvent() {
    var elems = document.querySelectorAll("#showMoreSeating");
    elems.forEach(function(elem){
		$(elem).on('click', function() {
            $(this).children('i').toggleClass('events-inverted');
            var valueToBeShown = $(this).siblings('#moreSeatingContent').children('div').html();
            $(this).siblings('#displayContent').html(valueToBeShown).toggle();
        });
    });
}
