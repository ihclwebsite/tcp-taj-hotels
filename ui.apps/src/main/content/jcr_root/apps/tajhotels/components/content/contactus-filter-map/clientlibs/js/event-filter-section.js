window.addEventListener('load', function() {

    $('#sizeDropdown').selectBoxIt({
        populate: ['Medium', 'Large', 'Small']
    });
    $('#capacityDropdown').selectBoxIt({
        populate: ['1000', '10000', '25000']
    });

    $('.events-filter-icon-mobile').on('click', function() {

        $('.events-filter-subsection').css('display', 'block');
    })

    $('.events-filter-back-arrow img').click(function() {
        $('.events-filter-subsection').css('display', 'none');
    })

    $('.events-apply-btn').click(function() {
        $('.events-filter-subsection').css('display', 'none');
        if ($('#sizeDropdown').find("option:selected").text() != 'Size' || $('#capacityDropdown').find("option:selected").text() != 'Capacity') {
            if ($('.events-filter-icon-mobile').html() == '<img src="../../../assets/images/filter-icon.svg" alt = "filter-icon">') {
                $('.events-filter-icon-mobile').html('<img src="../../../assets/images/filter-applied.svg" alt = "filter-applied">')
            }
        }
    })

    $('.specific-hotels-events-page .events-request-quote-btn').click(function() {
        document.location.href = "../../pages/hotels/specific-hotels-events-request-quote.html#Events";
    })
    $('.global-events-page .events-request-quote-btn').click(function() {
        document.location.href = "../../pages/events/global-events-request-quote.html";
    })

})
