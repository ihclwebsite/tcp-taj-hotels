$( 'document' ).ready( function() {
    $( '.counter-sub' ).click( function() {
        var value = parseInt( $( this ).siblings( '.counter-value' ).text() );
        if ( value == 1 ) {
            $( this ).parents().eq( 2 ).siblings( '.add-ons-img' ).children( '.add-ons-img-overlay' ).removeClass( 'visible' );
            $( this ).siblings( '.counter-value' ).text( 'ADD' );
        } else if ( value > 1 ) {
            $( this ).siblings( '.counter-value' ).text( --value );
        }

    } )
    $( '.counter-add' ).click( function() {
        var value = parseInt( $( this ).siblings( '.counter-value' ).text() );

        if ( isNaN( value ) ) {
            $( this ).parents().eq( 2 ).siblings( '.add-ons-img' ).children( '.add-ons-img-overlay' ).addClass( 'visible' );
            $( this ).siblings( '.counter-value' ).text( 1 );
        } else {
            $( this ).siblings( '.counter-value' ).text( ++value );
        }

    } )
} )