window.addEventListener('load', function() {
    var headerDDInitCon = $('.cm-header-dd-options-con');
    var headerDropdowns = $('.header-warpper .cm-header-dropdowns');
    var headerArrows = $('.header-dropdown-image');

    headerDropdowns.each(function() {
        $(this).on('click', function(e) {
            e.stopPropagation();
            var arrow = $(this).closest('.nav-item').find('.header-dropdown-image');
            var target = $(this).closest('.nav-item').find('.cm-header-dd-options-con');
            if (target.hasClass('active')) {
                target.removeClass('active');
                arrow.removeClass('header-dropdown-image-selected');
                $(this).removeClass('nav-link-expanded');
                return;
            }
            headerDropdowns.removeClass('nav-link-expanded')
            headerDDInitCon.removeClass('active');
            headerArrows.removeClass('header-dropdown-image-selected');
            target.addClass('active');
            arrow.addClass('header-dropdown-image-selected');
            $(this).addClass('nav-link-expanded')
        });
    });

    $('body').on('click', function() {
        headerDDInitCon.removeClass('active');
    });

    $('.header-currency-options .cm-each-header-dd-item').on('click', function() {

        var elDDCurrencySymbol = $(this).find('.header-dd-option-currency');
        var elDDCurrency = $(this).find('.header-dd-option-text');

        var elActiveCurrSymbol = $(this).closest('.nav-item').find('.selected-currency');
        var elActiveCurrency = $(this).closest('.nav-item').find('.selected-txt');

        var currencySymbol = elDDCurrencySymbol.text();
        var currency = elDDCurrency.text();

        var activeCurrSymbol = elActiveCurrSymbol.text();
        var activeCurrency = elActiveCurrency.text();

        elDDCurrencySymbol.text(activeCurrSymbol);
        elDDCurrency.text(activeCurrency);

        elActiveCurrSymbol.text(currencySymbol);
        elActiveCurrency.text(currency);
    });

    $('.header-profile-wrp').click(function(e) {
        e.stopPropagation();
        $('.profile-options').show();
        $('.profile-name-wrp .dropdown-image').addClass('cm-disable-rotate');
    });

    $('.cm-page-container').click(function() {
        $('.profile-options').hide();
        $('.profile-name-wrp .dropdown-image').removeClass('cm-disable-rotate');
    });

    $('.header-mobile-back-btn').click(function() {
        $('.navbar-collapse').removeClass('show');
    })
});
