$(document).ready(function() {
    if (window.location.search.substr(1).includes('hotelName')) {
        $('#searchInputEvents').val('');
        $('#searchInputEvents').removeAttr('readonly');
    } else {
        createDynamicDropdown();
        $('#venuePlaceholder').hide();
    }
    genericRequestQuote();

});

function genericRequestQuote() {

    $('#special-requests').keypress(function(e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    var venueDetail;
    var requestDetailsSessionObj = dataCache.session.getData("requestQuoteDetails");

    var hotelName = "";
    var venueName = "";
    var requestQuoteMailId = "";
    var adminEmail = $('#adminEmail').val();

    if (requestDetailsSessionObj) {
        // if in same domain session datas are picked and set in the Request Quote Form
        hotelName = requestDetailsSessionObj.hotelName;
        venueName = requestDetailsSessionObj.venueName;
        requestQuoteMailId = requestDetailsSessionObj.requestQuoteEmailId;
    } else {
        // cross-sell logic hotelName,VenueName and requestQuoteMailId picked from URL parameters
        hotelName = decodeURIComponent(getQueryParameter('hotelName'));
        venueName = decodeURIComponent(getQueryParameter('venueName'));
        requestQuoteMailId = decodeURIComponent(getQueryParameter('requestQuoteMailId'));
    }
    if (venueName != null && venueName != '') {
        venueDetail = hotelName + " - " + venueName;
    } else {
        venueDetail = hotelName;
    }
    $('.venue-search-input.sub-form-input-element.sub-form-mandatory').val(venueDetail);

    // analytics code
    var jsonObj = {};
    jsonObj.eventStartDate = "";
    jsonObj.eventEndDate = "";
    jsonObj.venueName = getParam('venueName');

    jsonObj.guestCount = "";
    jsonObj.hotelName = dataLayerData.hotelName;
    jsonObj.guestEmailId = "";
    jsonObj.phoneNumber = "";
    jsonObj.guestName = "";

    prepareJsonForEvents("Venue Selected", jsonObj);

    var validateRequestQuoteElements = function() {
        $('.event-request-quote-form .sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
            } else {
                $(this).removeClass('invalid-input');
            }
        });
    };

    // Request quote submit handler
    $('.request-quote-submit-btn').click(function() {
        validateRequestQuoteElements();
        if ($('.event-request-quote-form .sub-form-input-element').hasClass('invalid-input')) {
            $('.event-request-quote-form .invalid-input').first().focus();
        } else {
            requestGenericBooking();
        }
    });

    // datepicker script
    var today = new Date();

    $('#meeting-events-arrival-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($(this).hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD MMM YYYY");
            $('.event-quote-arrival-date-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $(this).siblings('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
        }
    });

    $('#meeting-events-arrival-date').datepicker('setDate', new Date());

    $('#meeting-events-departure-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($(this).hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD MMM YYYY");
            $('.event-quote-departure-date-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $(this).siblings('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
        }
    });

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $(this).siblings('.jiva-spa-date').addClass('visible');
    });

    $('body').click(function(e) {
        $('.jiva-spa-date').removeClass('visible');
    });

    $('.hotels-event-confirmation-overlay, .event-confirmation-close-icon, .event-confirmation-close-btn').click(
            function() {
                $('.hotels-event-confirmation-popup').hide();
            });
    $('.hotels-event-confirmation-inner-wrp').click(function(e) {
        e.stopPropagation();
    });

    $('.cm-page-container').blur(function() {
        $('.jiva-spa-date').removeClass('visible');
    });

    $('.event-search-close').click(function() {
        $(this).removeClass('visible');
        $(".venue-search-input-wrp > input").attr("placeholder", "Search");
    });

    $(document).on("click", function(e) {
        $('.meeting-events-search-list').css('display', 'none');
    });

    function requestGenericBooking(key) {

        var sessionData = dataCache.session.getData("requestQuoteDetails");
        $('.pg-spinner-con').css('display', "block");

        var hotelName = "";
        var venueName = "";
        var requestQuoteMailId = "";

        if (requestDetailsSessionObj) {
            hotelName = requestDetailsSessionObj.hotelName;
            venueName = requestDetailsSessionObj.venueName;
            requestQuoteMailId = requestDetailsSessionObj.requestQuoteEmailId;
        }

        if (null == requestQuoteMailId || requestQuoteMailId == "") {
            requestQuoteMailId = pageLevelData.hotelEmailId;
        }

        var eventQuoteArrivalDateValue = $('#event-quote-arrival-date-value').val();

        var eventQuoteDepartureDateValue = $('#event-quote-departure-date-value').val();

        var bookingArrivalDate = moment(eventQuoteArrivalDateValue, "DD MMM YYYY").format("DD/MM/YYYY");

        var bookingDepartureDate = moment(eventQuoteDepartureDateValue, "DD MMM YYYY").format("DD/MM/YYYY");

        var guest = $('#meeting-events-guest').val();

        var rooms = $('#meeting-events-rooms').val();

        var singleRooms = $('#meeting-events-single-rooms').val() ? $('#meeting-events-single-rooms').val() : 'NA';

        var doubleRooms = $('#meeting-events-double-rooms').val() ? $('#meeting-events-double-rooms').val() : 'NA';

        var tripleRooms = $('#meeting-events-triple-rooms').val() ? $('#meeting-events-triple-rooms').val() : 'NA';

        var name = $('#customer-name').val();

        var phoneNumber = $('#phone-Number').val();

        var guestEmailId = $('#customer-email-id').val();

        var tentativeBudget = $('#tentative-budget').val() || 'NA';

        var specialRequests = $('#special-requests').val();

        var eventType = $('#meeting-events-type').val();

        var purposeOfTheEvent = $('#event-purposeSelectBoxItText').text();

        var timeOfTheEvent = 'NA';

        var requestQuoteConfLink = $('.request-quote-conf-link').val();

        var requestQuoteErrorLink = $('.request-quote-error-link').val();
        if (!hotelName) {
            hotelName = $('#event-items').val() ? $('#event-itemsSelectBoxItText').text() : $('#event-typeSelectBoxIt')
                    .text();
        }
        if (!requestQuoteMailId) {
            requestQuoteMailId = $('#event-itemsSelectBoxItText').attr('data-val')
                    || $('#event-typeSelectBoxItOptions .selectboxit-option.selectboxit-focus').attr('data-email')
                    || '';
        }
        if (adminEmail) {
            requestQuoteMailId = requestQuoteMailId.concat("," + adminEmail);
        } else {
            if (!requestQuoteMailId) {
                requestQuoteMailId = "";
            }
        }

        var isGeneric = $('#isGeneric').val();

        var requestString = "hotelName=" + hotelName + "&hotelEmailId=" + requestQuoteMailId + "&bookingArrivalDate="
                + bookingArrivalDate + "&bookingDepartureDate=" + bookingDepartureDate + "&noOfGuests=" + guest
                + "&noOfRooms=" + rooms + "&singleRooms=" + singleRooms + "&doubleRooms=" + doubleRooms
                + "&tripleRooms=" + tripleRooms + "&name=" + name + "&phoneNumber=" + phoneNumber + "&guestEmailId="
                + guestEmailId + "&venueName=" + venueName + "&timeOfTheEvent=" + timeOfTheEvent + "&tentativeBudget="
                + tentativeBudget + "&specialRequests=" + specialRequests + "&purposeOfTheEvent=" + eventType
                + "&isGeneric=" + isGeneric;

        var meetingRoomRequest = prepareBookingData(hotelName, bookingArrivalDate, guest);
        tigger_meeting_quote(meetingRoomRequest)

        // analytics code
        var jsonObj = {};
        jsonObj.eventStartDate = bookingArrivalDate;
        jsonObj.eventEndDate = "";
        jsonObj.venueName = venueName;
        jsonObj.guestCount = guest;
        jsonObj.hotelName = hotelName;
        jsonObj.guestEmailId = guestEmailId;
        jsonObj.phoneNumber = phoneNumber;
        jsonObj.guestName = name;
        jsonObj.timeOfTheEvent = timeOfTheEvent;
        jsonObj.purposeOfTheEvent = purposeOfTheEvent;
        prepareJsonForEvents("Request for a Quote", jsonObj);

        return $.ajax({
            method : "GET",
            url : "/bin/requestQuote",
            data : requestString
        }).done(function(res) {
            console.info("ajax call success");
            window.location.assign(requestQuoteConfLink);
        }).fail(function() {
            console.error("ajax call fail function.");
            window.location.assign(requestQuoteErrorLink);
            dataCache.session.setData("confStatus", "Error");
        });
    }

    function prepareBookingData(hotelName, bookingDate, guest) {
        var jivaBookingData = {};
        jivaBookingData.hotelName = hotelName;
        jivaBookingData.bookingDate = bookingDate;
        jivaBookingData.numOfGuests = guest;
        return jivaBookingData;
    }

    function getParam(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return results[1];
    }
}

// analytics code
function prepareJsonForEvents(stepName, jsonObj) {

    var meetingEventJson = {};
    var pageHirarchy = [];

    if (stepName != undefined) {
        meetingEventJson.pageTitle = stepName;
        if (jsonObj != undefined) {
            jsonObj.venueName = decodeURI(jsonObj.venueName)
            meetingEventJson.hotelOfferingName = jsonObj.venueName;
            meetingEventJson.hotelName = jsonObj.hotelName;
            meetingEventJson.hotelOffering = stepName;
            meetingEventJson.eventType = "";
            meetingEventJson.eventStartDate = jsonObj.eventStartDate;
            meetingEventJson.eventEndDate = jsonObj.eventEndDate;
            meetingEventJson.venueBreakoutRooms = "";
            meetingEventJson.venueCity = dataLayerData.hotelCity;
            meetingEventJson.venueCountry = dataLayerData.hotelCountry;
            meetingEventJson.venueEvent = jsonObj.venueName;
            meetingEventJson.venueGuests = jsonObj.guestCount;
            meetingEventJson.timeOfTheEvent = jsonObj.timeOfTheEvent;
            meetingEventJson.purposeOfTheEvent = jsonObj.purposeOfTheEvent;
            meetingEventJson.venueSleepingRooms = "";

        }

        if (dataLayerData != undefined) {
            meetingEventJson.pageAuthor = dataLayerData.pageAuthor;
            meetingEventJson.pageLanguage = dataLayerData.pageLanguage;

            pageHirarchy = dataLayerData.pageHierarchy;

            if (pageHirarchy != undefined) {
                if (stepName == "Venue Selected") {
                    pageHirarchy.push("Venue Finder");
                }
            }
            if (stepName == "Request for a Quote") {
                pageHirarchy.push("Request for a Quote");
            }
            meetingEventJson.pageHirarchy = pageHirarchy;

            meetingEventJson.hotelCountry = dataLayerData.hotelCountry;
            meetingEventJson.hotelCode = dataLayerData.hotelCode;
            meetingEventJson.hotelCity = dataLayerData.hotelCity;
        }
    }

    pushHotelJson(meetingEventJson);
}

function createDynamicDropdown() {
    $('.pg-spinner-con').css('display', "block");
    $
            .ajax({
                method : "GET",
                url : "/bin/genericVenueSearch.data/results/venueSearchCache.json",
            })
            .done(function(res) {
                addTypeResults(res);
                intializeClickToDestDrpdwn(res);
            })
            .fail(
                    function() {
                        console.error('Type venue call failed');
                        warningBox({
                            description : "We are currently experiencing technical difficulties with our application. Our team is actively working to fix the issue. We hope to have this resolved soon. Thank you for your patience."
                        });
                    }).always(function() {
                $('.pg-spinner-con').css('display', "none");
            });

}

function intializeClickToDestDrpdwn(res) {
    $('#event-type').change(function() {
        $('select#event-items').data('selectBox-selectBoxIt').remove();
        var selectedDestination = $(this).val();
        if (selectedDestination) {
            addItemResults(res, selectedDestination);
        } else {
            populateDefaultHotel('', "Any Hotel");
        }
    });
}

function addTypeResults(destinations) {
    var destinationList = Object.keys(destinations);
    if (destinationList.length) {
        destinationList.sort(function(a, b) {
            var a = a.toLowerCase();
            var b = b.toLowerCase();
            if (a < b) {
                return -1;
            }
            return 0;
        });
        destinationList.forEach(function(destination) {
            $('select#event-type').data('selectBox-selectBoxIt').add({
                value : destination,
                text : destination,
                'data-email' : destinations[destination]['destinationMail']
            });
        });
    }
}

function addItemResults(results, destination) {
    if (results) {
        var hotel;
        var hotelList = results[destination]['hotels'];
        if (hotelList) {
            var hotelsCount = Object.keys(hotelList).length;
            if (hotelsCount > 1) {
                populateDefaultHotel('', "Any Hotel");
            }
            for (hotel in hotelList) {
                populateDefaultHotel(hotelList[hotel], hotel);
            }
        }
    }
}

function populateDefaultHotel(value, text) {
    $('select#event-items').data('selectBox-selectBoxIt').add({
        value : value,
        text : text
    });
}
