document.addEventListener( 'DOMContentLoaded', function() {

    var options = {};
    var $sortHotelByFilter = $( '#sortByHotelFilter' );
    var dropDowns = {
        sortByHotel: {
            options: ['Popularity', 'Price', 'Rating' ],
            elem: $sortHotelByFilter,
            default: null,
            selected: '',
            dependent: {
                elem: null
            }
        }
    };

    initDropdown.prototype.initialize = function() {
        var itrLength = this.options.length;
        for ( i = 0; i < itrLength; i++ ) {
            this.targetElem.append( '<option>' + this.options[ i ] + '</option>' )
        };

        //Initializes selectboxit on the rendered dropdown
        this.targetElem.selectBoxIt();

        // Add listeners to each selectbox change
        this.listenDDChange();
    };

    initDropdown.prototype.listenDDChange = function() {
        var dTarget = this.targetBlock;
        dTarget.elem.change( function() {
            var selectedOption = ( dTarget.elem ).find( "option:selected" );
            dTarget.selected = selectedOption.text();
            if ( dTarget.dependent.elem ) {
                dTarget.dependent.elem.selectBoxIt( 'selectOption', 0 );
                if ( dTarget.selected != dTarget.default ) {
                    dTarget.dependent.elem.prop( "disabled", false );
                } else {
                    dTarget.dependent.elem.prop( "disabled", true );
                }
            } else {
                if ( dTarget.selected != dTarget.default ) {
                    // "dropDowns" Gives the selected values for each dropdown options
                }
            }
        } );
    };


    function initSortHotels() {

        var sortHotelsBy = new initDropdown( $sortHotelByFilter, dropDowns.sortByHotel );
        sortHotelsBy.initialize();
    };

    initSortHotels();

  $('.mr-mapView-layout').hide();

	$( '.mr-list-switch' ).on( 'click', function() {
	    $( '.mr-list-switch' ).addClass( 'mr-view-toggler-style ' );
	    $( '.mr-map-switch' ).removeClass( 'mr-view-toggler-style ' );
	    $( '.mr-listView-layout' ).show();
	    $( '.mr-mapView-layout' ).hide();
	} );
	
	$( '.mr-map-switch' ).on( 'click', function() {
	    $( '.mr-map-switch' ).addClass( 'mr-view-toggler-style ' );
	    $( '.mr-list-switch' ).removeClass( 'mr-view-toggler-style ' );
	    $( '.mr-listView-layout' ).hide();
	    $( '.mr-mapView-layout' ).show();
	});
});

