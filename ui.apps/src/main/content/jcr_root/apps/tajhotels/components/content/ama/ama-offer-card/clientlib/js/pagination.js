$(document).ready(function(){
	try{
	init_pagination_ama();
	}catch(err){
		console.error('exception caught in pagination js of ama offer card component');
		console.error(err);
	}

});

init_pagination_ama= function()
{
	var $offersContainer = $(".offers-container");	
	$offersContainer.siblings('.jiva-spa-show-more').remove();
    var viewallButton = $offersContainer.siblings('.align-items').length;
    if(viewallButton != 0){
        $offersContainer.showMore(6,3);
		$offersContainer.siblings('.jiva-spa-show-more').remove();
    }else{
		$offersContainer.showMore(6,6);
		var filteredOffersLength = $offersContainer.find(".row .offers-page-card-component").length;
		$('.no-offers-found').toggle(filteredOffersLength==0);
    }
    $('.ama-theme .ama-offer-card-wth-car .cm-inner-carousal-cards').removeClass('col-lg-4 col-md-4 col-xs-12');

}