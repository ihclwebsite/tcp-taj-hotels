$(document).ready(function () {
    initContactUs();
    setTimeout(function () {
        var $filterDrpdwnWrp = $('.filter-dropdown-wrap.contact-us select');
        if ($filterDrpdwnWrp.length) {
            $filterDrpdwnWrp.data("selectBox-selectBoxIt").refresh();
        }
    }, 5000);
});

var filterPathTaj = "/content/dam/tajhotels/icons/style-icons/filter-icon-blue.svg";
var filterPathAppliedTaj = "/content/dam/tajhotels/icons/style-icons/filter-applied.svg";
var filterPathIhcl = "/content/dam/tajhotels/ihcl/style-icons/filter-ihcl.svg";
var filterPathAppliedIhcl = "/content/dam/tajhotels/icons/style-icons/filter-applied-blue.svg";

var clearInputTaj = "/content/dam/tajhotels/icons/style-icons/close.svg";
var clearInputIhcl = "/content/dam/tajhotels/ihcl/style-icons/close-ihcl.svg";


var optionsTaj = {
    imagePath: '/content/dam/tajhotels/icons/style-icons/cluster',

};
var optionsIhcl = {
    imagePath: '/content/dam/tajhotels/ihcl/style-icons/cluster-ihcl',
};

var markerCluster;
var allMarkers = [];
var options;


var activeButton;
var activeFilter;
var activeFiltervalue;
var filterSet = {};

var filteredMarkers = [];
var particMarker;
var dataForHotels;
var dataForSales;
var filteredMobCards = [];
var lastOpenedInfoWindow;
var dataKey;
var searchValue;
var searchSet = ["city", "country", "name", "address"];
var filterCountryList;


var activeBtn;
var activeFilter;
var hideMarkerList = [];
var onSearchMarker = [];
var noResult;

function checkTheme() {
    if ($('.mr-contact-us-map-with-search').hasClass('IHCL-section')) {
        options = optionsIhcl;

        filter = filterPathIhcl;
        filterApplied = filterPathAppliedIhcl;
        clearInput = clearInputIhcl;
        $('.filter-on-mobile').find('img').attr("src", filter);
        $('.contact-us-search-container .clear-input-icon').attr("src", clearInput);

    } else {
        options = optionsTaj;
        filter = filterPathTaj;
        filterApplied = filterPathAppliedTaj;
        clearInput = clearInputTaj;
    }


}


function eventHandlerDesk() {

    $('.contact-us-btn-wrapper  .cm-btn-secondary').on('click', function () {
        if (!$(this).hasClass('active')) {
            $('.contact-us-btn-wrapper  .cm-btn-secondary.active').removeClass('active');
            $(this).addClass('active');
            globalFilter();
        }
    })
    // on desktop

    $(' #country ').on('change', function () {
        globalFilter();
    })
    $(".searchbar-input.hotel-search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        searchValue = value;
        if (value.length === 0) {
            $('.clear-input-icon').removeClass('show-clear-input');
            globalFilter();
        } else {
            $('.clear-input-icon').addClass('show-clear-input');
        }
        if (value.length > 2) {
            $('.filter-dropdown-wrap.contact-us select')[0].selectedIndex = 0;
            $('.filter-dropdown-wrap.contact-us select').data("selectBox-selectBoxIt").refresh();
            globalFilter();
        } else {
        }
    });
}

function createAllCardsMob(data, wrapper) {
    for (var i = 0; i < data.length; i++) {

        var obj = {
            type: data[i].type,
            country: data[i].country,
            city: data[i].city,
            name: data[i].name,
            address: data[i].address
        }


        var tempCard = '<div class="contact-us-mob-card "  id="' + data[i].type + "uniq" + i + '">' +
            '<div class="contact-us-content-wrapper clearfix">' +
            '<div class="image-container"><img src="../../../assets/images/local-card-map.jpg" alt = "local-card-map-icon"/></div>' +
            '<div class="desc-container">' +
            '<div class="desc-city each-desc">' + data[i].city + '</div>' +
            '<div class="desc-name each-desc">' + data[i].name + '</div>' +
            '<div class="desc-address each-desc">' + data[i].address + '</div>' +
            '<div class="desc-phone each-desc "><a class="ho-contact-number hide-in-lg inline-block" href="tel:022-28765643"><img class="ho-cm-icon ho-contact-icon inline-block" src="../../../assets/images/phone-enabled.svg" alt = "phone enabled icon"><div class="inline-block ho-mail-to-txt cm-view-link-txt ho-cm-link-txt">' + data[i].phone + '</div></a></div>' +
            '</div>' +
            '</div>' +
            '</div>';


        wrapper.append(tempCard);
        $('#' + data[i].type + "uniq" + i).attr('data-key', JSON.stringify(obj))
    }
}


function globalFilter() {

    filterSet = {};
    filterSetSearch = {};

    if (window.screen.width > 991) {
        var activeBtn = $('.contact-us-btn-wrapper .cm-btn-secondary.active');
        var activeFilter = $('.filter-dropdown-wrap.contact-us select');

    } else {
        var activeBtn = $('.filter-button-mob-container .cm-btn-primary.cm-holiday-filter.active');
        var activeFilter = $('.filter-dropdown-wrap.contact-us-mob select');

    }


    if (activeFilter[0].selectedIndex != 0) {
        var activeFiltervalue = activeFilter.val();
        var activeFilterId = activeFilter.attr('id');
        filterSet.country = activeFiltervalue;

    }

    if (activeBtn.hasClass('hotels')) {
        activeButton = "hotels";
        data = dataForHotels;

    } else {
        activeButton = "sales";
        data = dataForSales;
    }

    filterSet.type = activeButton;

    cmnUpdateMap(filterSet);

    if (window.screen.width < 992) {

        // on mobile notify filter has been applied
        if (($('.filter-dropdown-wrap.contact-us-mob select')[0].selectedIndex == 0) && $('.btn-mob-wrapper .cm-btn-primary.cm-holiday-filter.active').hasClass('hotels')) {

            $('.filter-on-mobile').find('img').attr("src", filter);

        } else {
            $('.filter-on-mobile').find('img').attr("src", filterApplied);
        }
        filterMobileCards(filterSet);

    }
}

function cmnSearchEvent() {

    $(".searchbar-input.hotel-search").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        searchValue = value;
        if (value.length === 0) {
            $('.clear-input-icon').removeClass('show-clear-input');
            $('.map-no-search-result').removeClass('map-no-search-result-visible');
            noResult = false;
            globalFilter();
        } else {
            $('.clear-input-icon').addClass('show-clear-input');
        }
        if (value.length > 2) {

            globalFilter();
        }
    });
    // clear the input
    $('.contact-us-search-container .clear-input-icon').on('click', function () {
        $(this).prev('.hotel-search').val("");
        $('.clear-input-icon').removeClass('show-clear-input');
        $('.map-no-search-result').removeClass('map-no-search-result-visible');
        noResult = false;
        searchValue = "";
        globalFilter();
    })

}


function cmnUpdateMap(filterSet) {
    hideMarkerList = [];
    filteredMarkers = [];
    if (lastOpenedInfoWindow)
        lastOpenedInfoWindow.close();
    filteredMarkers = allMarkers.filter(function (value, index) {
        value.setIcon(value.iconNormal);
        var thisObjlength = Object.keys(filterSet);
        for (var key in filterSet) {
            if (value.data[key].toLowerCase() != filterSet[key].toLowerCase()) {
                hideMarkerList.push(value);
                return false;
                break;
            }
            if (key == thisObjlength[thisObjlength.length - 1]) {
                return true;
            }
        }

    });


    if (searchValue)
        filteredMarkers = cmnSearchedMarkers(filteredMarkers);


    hideParticularMarker(hideMarkerList);
    showZoomLevelHandle(filteredMarkers);
    if (noResult == true) {
        $('.map-no-search-result').addClass('map-no-search-result-visible');
        noResult = false;
    }


}

var finalMarkerShowList = [];

function cmnSearchedMarkers(filteredMarkers) {

    onSearchMarker = [];
    for (var i = 0; i < filteredMarkers.length; i++) {
        for (var j = 0; j < searchSet.length; j++) {
            if (filteredMarkers[i].data[searchSet[j]] != undefined) {
                if (filteredMarkers[i].data[searchSet[j]].toLowerCase().indexOf(searchValue.toLowerCase()) > -1) {
                    onSearchMarker.push(filteredMarkers[i]);
                    break;

                } else {
                    if (searchSet[j] == searchSet[searchSet.length - 1]) {
                        hideMarkerList.push(filteredMarkers[i]);
                    }
                }
            }
        }
    }

    if (onSearchMarker.length != 0) {


        noResult = false;
    } else {
        for (i = 0; i < allMarkers.length; i++) {
            onSearchMarker.push(allMarkers[i]);
        }

        noResult = true;
    }

    return onSearchMarker;
    return noResult;
}


function showZoomLevelHandle(fitBoundsMarkers) {
    markerCluster.clearMarkers();
    var page = $('.mr-contact-us-map-with-search.cm-contect-blocks.mr-taj-exclusity-map').data('brand-name');
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < fitBoundsMarkers.length; i++) {
        fitBoundsMarkers[i].setVisible(true);
        bounds.extend(fitBoundsMarkers[i].getPosition());
    }
    mapContactUs.fitBounds(bounds);
    markerCluster.addMarkers(fitBoundsMarkers);
    // zoom level handled for 1 marker and 0 marker
    if ((fitBoundsMarkers.length) == 1) {
        mapContactUs.setZoom(mapContactUs.getZoom() - 5);
    } else if ((fitBoundsMarkers.length) == 0) {
        mapContactUs.setZoom(5);
    } else if ((fitBoundsMarkers.length) > 1) {
        if ($(window).length < 992) {
            if (page.match('gateway')) {
                mapContactUs.setZoom(4);
            }
            if (page.match('contact-us')) {
                mapContactUs.setZoom(1);
            }
        }
        if (page.match('vivanta')) {
            mapContactUs.setZoom(4);
        } else if (page.match('seleqtions')) {
            mapContactUs.setZoom(4);
        }
    }


}

function init() {
    var type = $('.mr-contact-us-map-with-search.cm-contect-blocks.mr-taj-exclusity-map').data('brand-name');

    if (type.match("vivanta"))
        type = "vivanta";
    else if (type.match("gateway"))
        type = "gateway";
    else if (type.match("seleqtions"))
        type = "seleqtions";
    else if (type.match("taj"))
        type = "taj";
    else if (type.match("contact-us"))
        type = "corporates";
    else if (type != null || type == "hotels")
        type = "hotels";
    var requestString = "type=" + type;

    $.ajax({
        type: "GET",
        url: '/bin/contactUs',
        data: requestString,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            dataForHotels = data;


            createMarker(dataForHotels.data);
            markerCluster = new MarkerClusterer(mapContactUs, allMarkers, options);
            if (window.screen.width < 992) {
                createAllCardsMob(dataForHotels.data, $('.hotels.all-card'));
            }
            globalFilter();
        },
        error: function (errormessage) {
            console.error('AJAX Response Error:-> Request Status: ' + errormessage.status + ', Status Text: '
                + errormessage.statusText + ', Response Text: ' + errormessage.responseText);
        }

    });


    $.ajax({
        type: "GET",
        url: '/bin/contactUs?type=sales',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            dataForSales = data;
            countryData = dataForSales.country;// List of country is attached
            // with sales JSON


            filterCountryList = dataForSales.country;
            createMarker(dataForSales.data);
            markerCluster = new MarkerClusterer(mapContactUs, allMarkers, options);
            if (window.screen.width < 992) {
                createAllCardsMob(dataForSales.data, $('.sales.all-card'));
            }
            globalFilter();
        },
        error: function (errormessage) {
            console.error('AJAX Response Error:-> Request Status: ' + errormessage.status + ', Status Text: '
                + errormessage.statusText + ', Response Text: ' + errormessage.responseText);
        }

    });
};


function initFilter() {
    var Data = {
        "country": ['All', 'Bhutan', 'Malaysia', 'South Africa', 'Nepal', 'Sri Lanka', 'United States Of America', 'United Arab Emirates', 'Zambia', 'India', 'Maldives', 'United Kingdom'],
    };
    var $country = $('#country');
    var countryMob = $('#countryMobile');
    var dropDowns = {
        country: {
            options: Data.country,
            elem: $country,
            default: 'All',
            selected: 'All',
            dependent: {}
        }

    }
    var country = new initDropdown($country, dropDowns.country);
    var countryMobDD = new initDropdown(countryMob, dropDowns.country);
    country.initialize();
    countryMobDD.initialize();

    $('.selectboxit-options.selectboxit-list li').each(function () {

        if ($(this).attr('data-val') == "United States Of America") {
            $(this).attr('data-val', "US");
        }

        if ($(this).attr('data-val') == "United Arab Emirates") {
            $(this).attr('data-val', "Dubai");
        }

        if ($(this).attr('data-val') == "United Kingdom") {
            $(this).attr('data-val', "London");
        }

    });

    $('select#country option').each(function () {
        if ($(this).attr('value') == "United States Of America") {
            $(this).attr('value', "US");
        }

        if ($(this).attr('value') == "United Arab Emirates") {
            $(this).attr('value', "Dubai");
        }

        if ($(this).attr('value') == "United Kingdom") {
            $(this).attr('value', "London");
        }
    })

}


function createMarker(data) {
    var detail = {};
    for (var i = 0; i < data.length; i++) {
        detail.lat = data[i].coordinates.lat;
        detail.lng = data[i].coordinates.lng;

        var pinIconTaj = {
            url: "/content/dam/tajhotels/icons/style-icons/marker_local.svg",
            scaledSize: new google.maps.Size(46, 58)
        };

        var pinIconLargeTaj = {
            url: "/content/dam/tajhotels/icons/style-icons/marker_localLarge.svg",
            scaledSize: new google.maps.Size(64, 84)
        };


        var pinIconIhcl = {
            url: "/content/dam/tajhotels/ihcl/style-icons/marker-ihcl.svg",
            scaledSize: new google.maps.Size(36, 48),
        };

        var pinIconLargeIhcl = {
            url: "/content/dam/tajhotels/ihcl/style-icons/marker-focused-ihcl.svg",
            scaledSize: new google.maps.Size(54, 70),
        };

        if ($('.mr-contact-us-map-with-search').hasClass('IHCL-section')) {
            pinIcon = pinIconIhcl;
            pinIconLarge = pinIconLargeIhcl;

        } else {
            pinIcon = pinIconTaj;
            pinIconLarge = pinIconLargeTaj;
        }


        marker = new google.maps.Marker({
            position: detail,
            map: mapContactUs,
            animation: google.maps.Animation.DROP,
            icon: pinIcon
        });


        marker.iconNormal = pinIcon;
        marker.iconFocused = pinIconLarge;
        marker.data = data[i];
        marker.iconSizeFlag = false;
        marker.uniqId = data[i].type + "uniq" + i;
        allMarkers.push(marker);

        google.maps.event.addListener(marker, 'click', function () {
            if (this.iconSizeFlag == false) {
                if ((window.screen.width) < 992) {
                    openMapOverlay();

                } else {


                }
                createInfoWinodow(this);
                cmnMarkerSizeChange(this);

            }
        })
    }
}

function activeMobCard(marker) {
    $('.carousel-inner').find('.contact-us-mob-card.carousel-item.active').removeClass('active');
    $('.contact-us-mob-card.carousel-item').each(function () {
        $(this).toggleClass('active', $(this).attr('id') == marker.uniqId);
    })


}


function cmnMarkerSizeChange(particMarker) {
    for (var i = 0; i < filteredMarkers.length; i++) {
        if (filteredMarkers[i] == particMarker || filteredMarkers[i].uniqId == particMarker) {
            filteredMarkers[i].setIcon(filteredMarkers[i].iconFocused);
            filteredMarkers[i].iconSizeFlag = true;

        } else {
            filteredMarkers[i].setIcon(filteredMarkers[i].iconNormal);
            filteredMarkers[i].iconSizeFlag = false;
        }

    }
}


function hideParticularMarker(arary) {
    for (var i = 0; i < arary.length; i++) {
        arary[i].setVisible(false);
    }

}

function createInfoWinodow(marker) {
    if (lastOpenedInfoWindow)
        lastOpenedInfoWindow.close();


    var content = "<div class='contact-us-hotel-card-wrapper map-info-window'>" +
        "<div class='contact-us-wrapper'>" +
        "<div class='cont-us-hotel-image-wrap'>" +
        "<a href=\"" + marker.data.hotelPath + "\"><div class='image-container'><img alt = 'hotel map images' src=" + encodeURI(marker.data.imagePath) + " />" +
        "<div class='map-info-window-image-txt'>" +
        " <div class='image-desc-cont'>" +
        "<div class='image-desc-header'>" + marker.data.name + "</div>" +
        " <div class='image-desc-sub-header'>" + marker.data.city + "</div>" +
        " </div>" +
        "</div>" +
        "</div></div></a>" +
        " <div class='cont-us-hotel-details-wrap'>" +
        "<div class='hotel-detaills-cont'>" +
        " <div class='hotel-details-address'>" +
        "<div class='map-card-address'>" + marker.data.address + "</div>" +
        " <div class='map-card-contact map-card-tel'>Phone : " + marker.data.phone + "</div>";
    if (!marker.data.fax.includes("null")) {
        content += "<div class='map-card-fax map-card-contact'> Fax : " + marker.data.fax + "</div>";
    }

    content += "</div>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>";


    var infowindow = new google.maps.InfoWindow({
        content: content,
        maxWidth: 198
    });

    lastOpenedInfoWindow = infowindow;
    infowindow.setPosition(marker.latLng);
    infowindow.open(mapContactUs, marker);

    // when closing the info window it will close the corresponding marker

    google.maps.event.addListener(infowindow, 'closeclick', (function (i) {

        marker.setIcon(marker.iconNormal);
        marker.iconSizeFlag = false;
    }));
}

function filterMobileCards(filterSet) {
    var caraouselWrap = $('.contact-us-card-carousel');
    $('.contact-us-card-carousel').empty();

    var targetfilterWrap = $('.cont-us-card-mob-wrap').find("." + filterSet.type);
    targetfilterWrap.find('.contact-us-mob-card').each(function () {
        var data = $(this).data('key');
        var thisObjlength = Object.keys(filterSet);
        for (var key in filterSet) {
            if (data[key].toLowerCase() != filterSet[key].toLowerCase()) {
                break;
            }
            if (key == thisObjlength[thisObjlength.length - 1]) {
                caraouselWrap.append($(this).clone().addClass('carousel-item'));
            }
        }
    })
    // $('.carousel-item:first-child').addClass('active');

    if (searchValue)
        seachedCardsMob();
}

function seachedCardsMob() {
    var searchSet = ["city", "country", "name", "address"];
    $('.contact-us-card-carousel').find('.contact-us-mob-card').each(function () {
        var data = $(this).data('key');
        for (var i = 0; i < searchSet.length; i++) {
            if (data[searchSet[i]].toLowerCase().indexOf(searchValue.toLowerCase()) > -1) {
                break;

            } else {
                if (searchSet[i] == searchSet[searchSet.length - 1]) {
                    $('.contact-us-card-carousel').find($(this).remove());
                }
            }

        }
    })
    $('.carousel-item:first-child').addClass('active');
}


function openMapOverlay() {
    if (!$('.contact-us-map-container').hasClass('map-overlay-contact-us')) {
        $('.contact-us-map-container').toggleClass('map-overlay-contact-us', true);
        $('.contact-us-map-container .map-back-icon').show();
        $('.contact-us-map-container .cont-us-card-mob-wrap').show();
        $('.search-container.search-hotels-container.contact-us').toggleClass('on-moblie-default', false);
        $('.search-container.contact-us  .searchBar-wrap').toggleClass('show-on-map-view', true);
    }

}

function eventHandlerMob() {
    mapContactUs.addListener('click', function () {
        openMapOverlay();
    });

    ;


    // on click on back btn on mobile-on map
    $('.contact-us-map-container .map-back-icon').on('click', function () {
        $('.contact-us-map-container .map-back-icon').hide();
        $('.contact-us-map-container').toggleClass('map-overlay-contact-us', false);
        $('.search-container.search-hotels-container.contact-us').toggleClass('on-moblie-default', true);
        $('.contact-us-map-container .cont-us-card-mob-wrap').hide();
    })

    // on click on filter icon
    $('.filter-on-mobile img').on('click', function () {
        $('.events-filter-subsection').show();


    })

    // on click on back btn on mobile filter
    $('.filter-with-search-header .img-leftArrow').on('click', function () {
        $('.events-filter-subsection').hide();
    })

    // on click on apply filter on mobile
    $('.cm-btn-secondary.cm-holiday-filter').on('click', function () {
        $('.events-filter-subsection').hide();
        globalFilter();

    })
    // all mobile event handler
    $('.filter-button-mob-container .cm-btn-primary.cm-holiday-filter').on('click', function () {
        if (!$(this).hasClass('active')) {
            $('.filter-button-mob-container .cm-btn-primary.cm-holiday-filter.active').removeClass('active');
            $(this).addClass('active');
        }
    })
    // caousel functionality in mob
    // stop auto carousel movement
    $('#contact-us-carousel').carousel({
        interval: false
    });

    // search-filters-container contact-us-filter
    $('.events-filter-back-arrow img').click(function () {
        $('.events-filter-subsection').css('display', 'none');
    })
    // swap functionality
    $("#contact-us-carousel").on("touchstart", function (event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function (event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');

            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');

            }
            if ($('.carousel-inner.contact-us-card-carousel').children().length > 1) {
                onMobileSwipe();
            }
        });

        $("#contact-us-carousel").on("touchend", function () {
            $(this).off("touchmove");

        });

    });


}


function initMap(mapId) {
    var latlng = new google.maps.LatLng(20.5937, 78.9629);
    mapContactUs = new google.maps.Map(document.getElementById(mapId), {
        zoom: 5,
        center: latlng,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
        }
    });
};


function onMobileSwipe() {
    var uniqId = $('.carousel-inner').find('.contact-us-mob-card.carousel-item.active').attr('id');
    markerSizeChange(uniqId);
}


// caousel functionality ends here
function initContactUs() {

    window.addEventListener('load', function () {
        checkTheme();
        initMap("mapContactUs");
        initFilter();
        init();

        if ((window.screen.width) < 992) {
            eventHandlerMob();

            eventHandlerDesk();
            cmnSearchEvent();
        } else {
            eventHandlerDesk();
            cmnSearchEvent();
        }
    })
}


