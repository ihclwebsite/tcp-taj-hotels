$(document).ready(function() {

    $('.summary-soldout-message').hide();
    if($('.cm-page-container').hasClass('ama-theme')){
        if(dataCache.session["data"]["bookingOptions"]["selection"][0]["isBungalow"] == true){
			$(".ama-theme .carts-add-room").hide();
        }
    }
    if(dataCache.session["data"]["checkInTime"] && dataCache.session["data"]["checkOutTime"]){
        var dataCheckIn =dataCache.session["data"]["checkInTime"];
        var dataCheckOut  =dataCache.session["data"]["checkOutTime"];
        if( dataCheckIn != "" &&  dataCheckOut != ""){
            $(".checkInTime ").text("Check-in("+dataCheckIn+")");
            $(".checkOutTime").text(" Check-out("+dataCheckOut+")");
        }
    }

    // [IHCLCB]
    // summary details card accordion
    if ($(window).width() < 768) {
        $('.ihcl-theme .summary-room-title, .ihcl-theme .summary-charges-heading').click(function() {
            $(this).parent().toggleClass('ihclcb-open-checkout-accordion');
        });
    }
    // summary details card accordion ends
});

function cartSumaryCardComponent() {
    var couponCode = {
        value : null
    }

    $('.summary-card-arrow').click(function(e) {
        e.stopPropagation();
        $(this).toggleClass('expanded');
        $('.summary-room-exp').toggleClass('visible');
        $('.summary-charges').toggleClass('visible');
        $('.summary-room-mobile').toggleClass('hidden');
    });

    $('.summary-charges, .summary-room-exp').click(function(e) {
        e.stopPropagation();
    })

    $('body').click(function(e) {
        $('.summary-card-arrow').removeClass('expanded');
        $('.summary-room-exp').removeClass('visible');
        $('.summary-charges').removeClass('visible');
        $('.summary-room-mobile').removeClass('hidden');
    })

    $('.coupon-code').on("keyup", function() {
        var couponCodeInput = $(this).val();
        if (couponCodeInput.length > 0) {
            $('.apply-coupon-code-btn').show();
            $('.coupon-code-clear-input').show();
        } else {
            $('.apply-coupon-code-btn').hide();
            $('.coupon-code-clear-input').hide();
        }
    });

    $('.apply-coupon-code-btn').on("click", function() {
        validateCouponcode(couponCode);
    });

    $('.coupon-code-clear-input').on("click", function() {
        $('.coupon-code').val("");
        $(this).hide();
        $('.apply-coupon-code-btn').hide();
        $('.couponcode-status-text').text("");
        couponCode.value = null;
        dataCache.session.data.bookingOptions.couponCode = null;
    });
    var couponCodeSelected = dataCache.session.data.bookingOptions.couponCode;
    if (couponCodeSelected) {
        $('.checkout-layout .coupon-show-on-checkout-page').removeClass('cm-hide');
        $('.summary-applied-coupn').text(couponCodeSelected);
    } else {
        $('.checkout-layout .coupon-show-on-checkout-page').addClass('cm-hide');
    }
}

function showNightlyRates() {

    $(".nightly-rates").html("");
    var previousCurrencySymbol = dataCache.session.getData('selectedCurrency');
    var sessionData = dataCache.session.getData("bookingOptions");
    var userData = getUserData();
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    if (sessionData.selection[0]) {
        var currencySymbol;
        if (previousCurrencySymbol != undefined) {
            currencySymbol = previousCurrencySymbol.trim();
        }
        for (var s = 0; s < sessionData.selection.length; s++) {

            var roomNumber = s + 1;

            var roomHtml = '<div class="room-nightly-rates"> <span class="room-info"> Room ' + roomNumber + '</span>';

            for (var i = 0; i < sessionData.selection[s].nightlyRates.length; i++) {

                var indNightRateData = sessionData.selection[s].nightlyRates[i];

                var rateAfter = '';
                // [TIC-FLOW]
                if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {

                    if (previousCurrencySymbol != 'INR' && previousCurrencySymbol != '₹') {
                        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                        if (ticRoomRedemptionObjectSession
                                && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                            var conversionRate = parseFloat(currencyRateConversionString[previousCurrencySymbol
                                    + '_INR']);
                            indNightRateData.priceWithFeeAndTax = Math.round(Math
                                    .ceil(indNightRateData.priceWithFeeAndTax)
                                    * conversionRate);
                        }

                    }
                    if (userData && userData.card && userData.card && userData.card.type) {
                        if (userData.card.type.includes("TIC")) {
                            rateAfter = roundPrice(Math.round(indNightRateData.priceWithFeeAndTax)) + ' TIC Points';
                        } else if (userData.card.type === "TAP") {
                            rateAfter = roundPrice(Math.round(indNightRateData.priceWithFeeAndTax)) + " TAP";
                        } else if (userData.card.type === "TAPPMe") {
                            rateAfter = roundPrice(Math.round(indNightRateData.priceWithFeeAndTax)) + " TAPP Me";
                        }
                    }
                    currencySymbol = '';
                } else {
                    rateAfter = Number(indNightRateData.price).toFixed(2);
                }

                var indNightRateHtml = '<div class="ind-nightly-rate">' + '<span class="ind-night-date">'
                        + moment(indNightRateData.date, "MM/DD/YYYY").format("DD MMM YYYY") + '</span>'
                        + '<span class="ind-night-rate">' + currencySymbol + ' ' + rateAfter + '</span>' + '</div>';

                roomHtml = roomHtml + indNightRateHtml;
            }
            roomHtml = roomHtml + '</div>';

            $('.nightly-rates').append(roomHtml);

        }
    }
}
function showAllTypeOfTaxes() {

    $(".all-type-of-taxes").html("");
    var previousCurrencySymbol = dataCache.session.getData('selectedCurrency');
    var sessionData = dataCache.session.getData("bookingOptions");

    if (sessionData && sessionData.selection[0]) {
        var currencySymbol;
        if (previousCurrencySymbol != undefined) {
            currencySymbol = previousCurrencySymbol.trim();
        }
        for (var s = 0; s < sessionData.selection.length; s++) {

            var roomNumber = s + 1;

            var roomTaxHtml = '<div class="room-level-tax"> <span class="room-info"> Room ' + roomNumber + '</span>';

            for (var i = 0; sessionData.selection[s].taxes && i < sessionData.selection[s].taxes.length; i++) {

                var indTaxData = sessionData.selection[s].taxes[i];

                var price = Number(indTaxData.taxAmount).toFixed(2);

                var indTaxHtml = '<div class="ind-tax-Details">' + '<span class="ind-tax-name">' + indTaxData.taxName
                        + '</span>' + '<span class="ind-tax-amount">' + currencySymbol + ' ' + price + '</span>'
                        + '</div>';

                roomTaxHtml = roomTaxHtml + indTaxHtml;
            }
            roomTaxHtml = roomTaxHtml + '</div>';

            $('.all-type-of-taxes').append(roomTaxHtml);

        }
    }
}

function getCouponCodeFromCache() {
    var couponCodeList = $($.find("[data-coupon-code]")[0]).data();
    // var couponCodeList = dataCache.session.getData( "couponCodes");
    if (couponCodeList) {
        return couponCodeList;
    }
    return couponCodeList ? couponCodeList : [ 'KOOPON' ];
}

// ie fall back for object.values
function extractObjectValues(objectName) {
    return (Object.keys(objectName).map(function(objKey) {
        return objectName[objKey]
    }))
}

function validateCouponcode(couponCode) {
    var couponCodeInput = $('.coupon-code').val();
    var couponCodeStatus = false;
    var couponCodeList = getCouponCodeFromCache();
    if ((couponCodeList != '') && (extractObjectValues(couponCodeList).indexOf(couponCodeInput) != -1)) {
        couponCodeStatus = true;
    }
    if (couponCodeStatus) {
        $('.couponcode-status-text').text("Coupon code selected: " + couponCodeInput);
        couponCode.value = couponCodeInput;
        dataCache.session.data.bookingOptions.couponCode = couponCode.value;
    } else {
        $('.couponcode-status-text').text("Coupon code invalid.");
        couponCode.value = null;
        dataCache.session.data.bookingOptions.couponCode = null;
    }
}

