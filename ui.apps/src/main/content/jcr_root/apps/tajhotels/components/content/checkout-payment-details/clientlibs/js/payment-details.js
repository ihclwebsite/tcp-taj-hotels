var corporateBooking = false;
var guaranteedByCreditCard = false;
var paymentOptValue="Pay at hotel";
var guaranteeAmount = 0;
$( document ).ready( function() {

    // [IHCLCB start]
    var isIHCLCBSiteFlag = isIHCLCBSite();
    if(isIHCLCBSiteFlag) {
        // set active status Bill to company
        $("#BILLTOCOMPANY").closest('.ihclcb-payment-opt-btn').addClass('selected-ihclcb-payment-btn')
        
         // payment option card active status change
        $('.ihclcb-payment-opt-btn .payment-radio-btn').change(function(){
            $(this).closest('.ihclcb-payment-opt-btn').addClass('selected-ihclcb-payment-btn').siblings().removeClass('selected-ihclcb-payment-btn');
        });
    }

    // [IHCLCB end]


    var contentRootPath = $('#content-root-path').val();
    if (contentRootPath == "" || contentRootPath == null || contentRootPath == 'undefined') {
        contentRootPath = "/content/tajhotels";
    }

    $('#bookingalert').hide();
    $('#cancel-spin').hide();


    // payment type button listener
    $( '.payment-radio-btn' ).change( function() {

        if($("#PAYATHOTEL").is(":checked")){
            $('.payment-radio-content').addClass('cm-hide');
            $('.credit-card-payment-wrp').removeClass('cm-hide');
        }else if($("#GUARANTEEPAYMENT").is(":checked")){
            $('.payment-radio-content').addClass('cm-hide');
            $('.guarantee-payment-wrp').removeClass('cm-hide');
            $('.guarantee-amount').text(roundPrice(guaranteeAmount));
        }else if($("#PAYONLINENOW").is(":checked")){
            $('.payment-radio-content').addClass('cm-hide');
            // [IHCLCB-FLOW]
        }else if($("#BILLTOCOMPANY").is(":checked")){
            $('.payment-radio-content').removeClass('cm-hide');
            // $('.credit-card-payment-wrp').addClass('cm-hide');
            $('.gift-card-payment-wrp').addClass('cm-hide');
        }
        // [IHCLCB-END]
        // var selectedIndex = $( this ).index( '.payment-radio-btn' );
        // $( '.payment-radio-content' ).addClass( 'cm-hide' );
        // $( $( '.payment-radio-content' )[ selectedIndex ] ).removeClass( 'cm-hide' );

        /*
         * if($("#PAYONLINENOW").is(":checked")){ $('.pay-now-button ').css({ "opacity" : 1 });
         * $('.pay-now-button').attr("disabled", false); }else { $('.pay-now-button ').css({ "opacity" : 0.33 });
         * $('.pay-now-button').attr("disabled", true); }
         */

        if(!guaranteedByCreditCard){
            $('.payment-radio-content').addClass('cm-hide');
        }

    } );



    // preventing propagation within signin form
    $( '.checkout-member-signin-container' ).click( function( e ) {
        e.stopPropagation();
    } );

    // booking for someone else checkbox handler

    $( '.booking-others-checkbox' ).change( function() {

        if ( this.checked ) {
            $( '.biller-details-wrp' ).show();
        } else {

            $( '.biller-details-wrp' ).hide().find( '.sub-form-mandatory' ).removeClass( 'invalid-input' );
        }
    } );

    
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    checkMemberTypeforPaymentOptions();

    // [TIC-FLOW]
    var userDetails = getUserData();
    $( '.booking-others-tic-checkbox' ).change( function() {
        if ( this.checked ) {
            if(!isIHCLCBSiteFlag) {
                ticRoomRedemptionObjectSession.isTicBookForSomeoneElse = true;
                $('#bookingGuestTitle').prop('disabled', false);
                $('#guest-firstName').prop('disabled', false);
                $('#guest-lastName').prop('disabled', false);
                $('#guest-Email').prop('disabled', false);
                $('#guest-PhoneNumber').prop('disabled', false);
                $('#guest-MembershipNumber').prop('disabled', false);
                $('#guest-MembershipNumber').addClass('d-none');
                $('.guest-MembershipNumber').addClass('d-none');
                dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObjectSession);
            } else {
             // [IHCLCB]
    
                $('#tic-member-id-ihclcb').prop('disabled',true).val("");            
                inputFieldDisabler(false);
            }
        }else{
            if(!isIHCLCBSiteFlag) {
                $('#bookingGuestTitle').prop('disabled', true);
                if(userDetails.title) {
                    $('#bookingGuestTitle').selectBoxIt('selectOption', userDetails.title);
                }
                $('#guest-firstName').prop('disabled', true);
                $('#guest-firstName').val(userDetails.firstName);
                $('#guest-lastName').prop('disabled', true);
                $('#guest-lastName').val(userDetails.lastName);
                $('#guest-Email').prop('disabled', true);
                $('#guest-Email').val(userDetails.email);
                if(userDetails.mobile && userDetails.mobile.length > 0 ){
                    $('#guest-PhoneNumber').prop('disabled', true);
                    $('#guest-PhoneNumber').val(userDetails.mobile);
                }
                $('#guest-MembershipNumber').prop('disabled', true);
                $('#guest-MembershipNumber').val(userDetails.membershipId);
                $('#guest-MembershipNumber').removeClass('d-none');
                $('.guest-MembershipNumber').removeClass('d-none');
                ticRoomRedemptionObjectSession.isTicBookForSomeoneElse = false;
                dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObjectSession);
            } else {
                // [IHCLCB]
                $('#tic-member-id-ihclcb').prop('disabled',false).val("");
                inputFieldDisabler(true);
            }
        }
    } );
    
    function inputFieldDisabler(flag) {
        $('.guest-detail-ihclcb' ).find('.sub-form-mandatory' ).removeClass( 'invalid-input' );
        $("#tic-member-id-ihclcb").removeClass('invalid-input');
        $('#corporate-ihclcb-checkout #bookingGuestTitle').prop('disabled', flag);
        $('#corporate-ihclcb-checkout #guest-firstName').prop('disabled', flag).val("");
        $('#corporate-ihclcb-checkout #guest-lastName').prop('disabled', flag).val("");
        $('#corporate-ihclcb-checkout #guest-Email').prop('disabled', flag).val("");
        $('#corporate-ihclcb-checkout #guest-PhoneNumber').prop('disabled', flag).val("");
    }


    // validateEnterDetailsElements
    var validateEnterDetailsElements = function() {
        var flag = true;
        if($('#guest-VoucherCode:visible')){
            if($('#guest-VoucherCode').val().length<16&&$('#guest-VoucherCode').val().length>0){
                console.log($('#guest-VoucherCode').val().length);
                $('#guest-VoucherCode').addClass( 'invalid-input' );
                invalidWarningMessage( $('#guest-VoucherCode') );
            }
            if($('#guest-VoucherPin').val().length<6&&$('#guest-VoucherPin').val().length>0){
                console.log($('#guest-VoucherPin').val().length);
                $('#guest-VoucherPin').addClass( 'invalid-input' );
                invalidWarningMessage( $('#guest-VoucherPin') );
            }
        }
        if ( $( '.booking-others-checkbox' )[ 0 ].checked ) {
            $( '.guest-details-wrp, .biller-details-wrp' ).find( '.sub-form-mandatory:visible' ).each( function() {
                if ( $( this ).val() == "" ) {
                    $( this ).addClass( 'invalid-input' );
                    flag = false;
                    invalidWarningMessage( $( this ) );
                }
            } );
        } else {
            $( '.guest-details-wrp .sub-form-mandatory:visible' ).each( function() {
                if ( $( this ).val() == "" ) {
                    $( this ).addClass( 'invalid-input' );
                    flag = false;
                    invalidWarningMessage( $( this ) );
                }
            } );
        }
        var privacyCheckBox = $('input[name="privacyPolicy"]');
        var privacyCheckBoxValue = $('input[name="privacyPolicy"]').is(':checked');
        if(privacyCheckBoxValue==false){
            flag = false;
            privacyCheckBox.closest('.policy-terms-external-wrapper').find('.policy-terms-warning-message').show();
        }
        var termsCheckBox = $('input[name="termsAndConditions"]');
        var termsCheckBoxValue = $('input[name="termsAndConditions"]').is(':checked');
        if(termsCheckBoxValue==false){
            flag = false;
            termsCheckBox.closest('.policy-terms-external-wrapper').find('.policy-terms-warning-message').show();
        }

        if(isIHCLCBSiteFlag) {
            return flag;
        }

        var $guestTitleDD = $( '#bookingGuestTitle' );
        var $countryDD = $( '#bookingGuestCountry' );
        if(!$guestTitleDD.val() || $guestTitleDD.val() == "") {
            flag = false;
            $guestTitleDD.closest('.sub-form-input-wrp').find('.dd-error-msg').css('display', 'block');
            $guestTitleDD.closest('.sub-form-input-element').addClass('invalid-input');
        }else{
            $guestTitleDD.closest('.sub-form-input-element').removeClass('invalid-input');
        }

        if(!$countryDD.val() || $countryDD.val() == "") {
            flag = false;
            $countryDD.closest('.sub-form-input-wrp').find('.dd-error-msg').css('display', 'block');
            $countryDD.closest('.sub-form-input-element').addClass('invalid-input');
        }else{
            $countryDD.closest('.sub-form-input-element').removeClass('invalid-input');
        }

        var $membershipNumberInput = $('[name="guestMembershipNumber"]');
        if($membershipNumberInput.val()) {
            var userloggedin=getUserData();
            if(userloggedin && userloggedin.tier && userloggedin.card && userloggedin.card.type.includes("TIC")){
                if(!(/^101\d{9}$/).test($membershipNumberInput.val())) {
                    $membershipNumberInput.addClass("invalid-input");
                    $membershipNumberInput.next().html("Please enter a valid number");
                    flag = false;
                } else {
                    $membershipNumberInput.removeClass("invalid-input");
                }
            }
            else{
                if(!(/^[A-Za-z\d-]+$/).test($membershipNumberInput.val())) {
                    $membershipNumberInput.addClass("invalid-input");
                    $membershipNumberInput.next().html("Please enter a valid number");
                    flag = false;
                } else {
                    $membershipNumberInput.removeClass("invalid-input");
                }
            }

        } else {
            $membershipNumberInput.removeClass("invalid-input");
        }

        return flag;
    }

    // proceed button validation
    $( '.proceed-payment-button' ).click( function() {

        if(validateEnterDetailsElements()){
            if ( $( '.sub-form-input-element' ).hasClass( 'invalid-input' ) ) {
                $( '.invalid-input' ).first().focus();
                var billerDetailsHasInvalidInput = ( $( '.biller-details-wrp .sub-form-input-element' ).hasClass( 'invalid-input' ) );
                if ( billerDetailsHasInvalidInput == false ) {
                    if ( ( $( '#bookingGuestTitle' )[ 0 ].value == "" ) ) {
                        $( '.selectboxit' ).trigger( 'click' );
                    }
                }
            } else {

                var bookingData=JSON.parse(extractBookingDetails());
                var guestDetails=extractGuestDetails();
                trigger_guestDetails(bookingData,guestDetails);

                // getting the booking data
                var roomsData= dataCache.session.getData( "bookingOptions" );
                var ihclCbBookingObject= dataCache.session.getData("ihclCbBookingObject")
                if(roomsData && roomsData.selection
                        && roomsData.selection[0] && roomsData.selection[0].currencyString){
                    if(roomsData.selection[0].currencyString != "INR"){
                        // [IHCLCB start]
                        if(ihclCbBookingObject!=undefined && ihclCbBookingObject.isIhclCbBookingFlow){
                                 $('#PAYONLINENOW').parent().parent().remove();
                            } 
                        // [IHCLCB end]
                        $('#PAYONLINENOW').parent().parent().remove();

                    }
                }

                // registering an event for following function
                $('#PAYATHOTEL,#PAYONLINENOW').click(function() {
                    paymentOptValue= $(this).val();
                });

                /*
                 * This function pushes the analytics data to dataLayer this is the guest details validation step ("1")
                 */
                payingAtCart(roomsData, "1");

                if(isIHCLCBSiteFlag){
                    $("#BILLTOCOMPANY").prop("checked", true);
                 // set active status Bill to company
                    $("#BILLTOCOMPANY").closest('.ihclcb-payment-opt-btn').addClass('selected-ihclcb-payment-btn').siblings().removeClass('selected-ihclcb-payment-btn');
                }
                else{
                    var voucherRedemption = dataCache.session.getData('voucherRedemption');
                    if(voucherRedemption && voucherRedemption == 'true') {
						hidePaymentVoucherRedemption();
                    } else {
                        $("#PAYONLINENOW").prop("checked", true);
                        $('.payment-radio-content').addClass('cm-hide');
                    }
                }

                /* $('.payment-radio-btn').first().attr("checked",true); */
                $( '.booking-details-wrp' ).hide();
                // $( '#PAYONLINENOW' ).hide();
                $( '.payment-details-wrp' ).show();
                $('.checkout-enter-details-header.payment').removeClass('cm-hide');
                $('.checkout-enter-details-header.booking').addClass(' cm-hide');
                // $('.payMethod').html($("input[name='payment-method']:checked").val());
                var totalCartPrice = dataCache.session.getData( "bookingOptions" ).totalCartPrice;
                var totalCartTax = dataCache.session.getData( "bookingOptions" ).totalCartTax;

                // Removing PAYATHOTEL if Guarantee payment required
                var roomDataSelection = roomsData.selection;
                var payFullGuaranteeAmount = true;
                var isGuaranteePaymentRequired = false;
                for(var mm = 0; mm < roomDataSelection.length; mm++){
                    if(roomDataSelection[mm].currencyString == "INR" && (roomDataSelection[mm].guaranteeAmount || roomDataSelection[mm].guaranteePercentage)){
                        console.log("index: " + mm + ", guaranteeAmount: " + roomDataSelection[mm].guaranteeAmount + ", guaranteePercentage: " + roomDataSelection[mm].guaranteePercentage);
                        isGuaranteePaymentRequired = true;
                        
                        if(roomDataSelection[mm].guaranteePercentage != 100){
                            payFullGuaranteeAmount = false;
                            guaranteeAmount = guaranteeAmount + parseFloat(roomDataSelection[mm].guaranteeAmount);
                        }else if(roomDataSelection[mm].guaranteeAmount){
                            guaranteeAmount = guaranteeAmount + parseFloat(roomDataSelection[mm].roomBaseRate) + parseFloat(roomDataSelection[mm].roomTaxRate);
                        }
                    }else{
                        payFullGuaranteeAmount = false;
                    }
                    if(roomDataSelection[mm].isCreditCardRequired ){
                        guaranteedByCreditCard = true;
                    }
                }
                
                if(isGuaranteePaymentRequired){
                    if(payFullGuaranteeAmount){  
                        $("#PAYONLINENOW").prop("checked", true);
                        $("#GUARANTEEPAYMENT").parent().parent().remove();
                    }else{
                        $('#PAYATHOTEL').parent().parent().remove();
                    }
                    if(isIHCLCBSiteFlag){
                        $("#PAYONLINENOW").closest('.ihclcb-payment-opt-btn').addClass('selected-ihclcb-payment-btn').siblings().removeClass('selected-ihclcb-payment-btn');
                    }
                    $('.payment-radio-content').addClass('cm-hide');
                    $('#PAYATHOTEL').parent().parent().remove();
                }else{
                    $("#GUARANTEEPAYMENT").parent().parent().remove();
                }
  
                if(!guaranteedByCreditCard){
                    $('.payment-radio-content').addClass('cm-hide');
                }

                // [TIC-FLOW]
                var userDetails = getUserData();
                var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                var userSelectedCurrency =  dataCache.session.getData("selectedCurrency");
                if (userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
                    var totalCartTax  = 0;
                    roomsData.selection.forEach(function(item, index) {
                        totalCartTax = totalCartTax + item.roomTaxRate;
                    })

                    if (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹') {
                        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                            var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency + '_INR']);
                            totalCartPrice = Math.round(totalCartPrice * conversionRate);
                            totalCartTax  = Math.round(totalCartTax * conversionRate);
                        }

                    }

                    $( '.credit-card-total-amount' ).html(roundPrice(Math.ceil(totalCartPrice- totalCartTax)) +" TIC/ "+ roundPrice(Math.ceil(totalCartPrice- totalCartTax) /2)+" EPICURE");
                    $( '.rupee-symbol' ).html(' ');
                }else{
                    $( '.credit-card-total-amount' ).html( roundPrice(totalCartPrice) ).digits();
                }
                
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(".checkout-payment-details-container ").offset().top
                }, 600);

                $( '.checkout-form-step-wrp' ).css( 'pointer-events', 'auto' );
            }
        }

    } );

    // redeem points checkbox handler

    $( '.redeem-points-checkbox' ).change( function() {
        if ( this.checked ) {
            $( '.redeem-checkbox-wrp' ).addClass( 'redeem-points-checked' );
        } else {
            $( '.redeem-checkbox-wrp' ).removeClass( 'redeem-points-checked' );
        }
    } );


    /*
     * $('.payMethod').html($("input[name='payment-method']:checked").val());
     * $('input[type=radio][name=payment-method]').change(function() { $('.payMethod').html(this.value); });
     */

    // Booking guest dropdown
    var options = {};
    var $bookingGuestTitle = $( '#bookingGuestTitle' );
    var $redeemTicDropdown = $( '#redeemTicDropdown' );
    var $bookingGuestCountry = $('#bookingGuestCountry');
    var $membershipNumberInput = $('[name="guestMembershipNumber"]');

    var dropDowns = {
            titles: {
                options: [ ],
                elem: $bookingGuestTitle,
                default: '',
                selected: null,
                dependent: {
                    elem: null
                }
            },
            tic: {
                options: [ 'TIC plus Credit Card', 'Epicure plus Credit Card' ],
                elem: $redeemTicDropdown,
                default: '',
                selected: null,
                dependent: {
                    elem: null
                }
            },
            country: {
                options: (getCountryList()).map(countryWithCodes),
                elem: $bookingGuestCountry,
                default: '',
                selected: null,
                dependent: {
                    elem: null
                }
            }
    }


    function countryWithCodes(countries) {
        var countryNCodeVal =  countries.name + " (+"+ countries.code+")";
        return countryNCodeVal;
    }

    function initbookingGuestTitle() {
        // receiving titles from AEM Author

        var bookingGuestTitle = new initDropdown( $bookingGuestTitle,dropDowns.titles );
        bookingGuestTitle.initialize();

        var countryList = new initDropdown( $bookingGuestCountry, dropDowns.country );
        countryList.initialize();
              
        selectCountryDropdown($bookingGuestCountry);  
        
        // var redeemTicDropdown = new initDropdown( $redeemTicDropdown,
        // dropDowns.tic ); redeemTicDropdown.initialize();

    };
    
    function selectCountryDropdown($ele, value) {
        if($ele.length!=0){
            $ele.val(value || 'India (+91)');
            $ele.data("selectBox-selectBoxIt").refresh();
        } 
    }
    
    function initCountryDropdown($ele, value) {
        var countries=getCountryList();
        $.each(countries,function(index,country){
            $ele.append($('<option value="'+country.name+'">'+country.name+'</option>'));
        });
        $ele.selectBoxIt();
        selectCountryDropdown($ele, 'India');        
    }

    initbookingGuestTitle();
    initCountryDropdown($('#FTOCountry'));

    // Title dropdown change
    $bookingGuestTitle.on('change', function() {
        if(!$(this).val() || $(this).val() == "" ){

            if($(this).parent().find('.dd-error-msg').length > 0){
                $(this).parent().find('.dd-error-msg').css('display', 'block');
                $(this).closest('.sub-form-input-element').addClass('invalid-input');
            }
            else{
                $(this).parent().siblings('.dd-error-msg').css('display', 'block');
                $(this).closest('.sub-form-input-element').addClass('invalid-input');
            }
        } else {
            if($(this).parent().find('.dd-error-msg').length > 0){
                $(this).parent().find('.dd-error-msg').css('display', 'none');
                $(this).closest('.sub-form-input-element').removeClass('invalid-input');
            }
            else{
                $(this).parent().siblings('.dd-error-msg').css('display', 'none');
                $(this).closest('.sub-form-input-element').removeClass('invalid-input');
            }
        }
    });

    // Country dropdown change
    $bookingGuestCountry.on('change', function() {
        if(!$(this).val() || $(this).val() == "" ){
            $(this).parent().find('.dd-error-msg').css('display', 'block');
            $(this).closest('.sub-form-input-element').addClass('invalid-input');
        } else {
            $(this).parent().find('.dd-error-msg').css('display', 'none');
// getCountryDetail('name');
// getCountryDetail('code');
            $(this).closest('.sub-form-input-element').removeClass('invalid-input');
        }
    });

    // Validating the membership number
    $membershipNumberInput.on('keyup', function() {
        if($membershipNumberInput.val()) {
            var userloggedin=getUserData();
            if(userloggedin && userloggedin.tier && userloggedin.card && userloggedin.card.type.includes("TIC")){
                if(!(/^101\d{9}$/).test($membershipNumberInput.val())) {
                    $membershipNumberInput.addClass("invalid-input");
                    $membershipNumberInput.next().html("Please enter a valid number");
                } else {
                    $membershipNumberInput.removeClass("invalid-input");
                }
            }
            else{
                if(!(/^[A-Za-z\d-]+$/).test($membershipNumberInput.val())) {
                    $membershipNumberInput.addClass("invalid-input");
                    $membershipNumberInput.next().html("Please enter a valid number");
                } else {
                    $membershipNumberInput.removeClass("invalid-input");
                }
            }

        } else {
            $membershipNumberInput.removeClass("invalid-input");
        }
    });

    var cardType = "";
    // credit card type identifier
    $( '.credit-card-number' ).on( "keyup focusin", function() {
        var cardNumber = $( this ).val();
        var creditCardType = getCreditCardType( cardNumber );
        if ( creditCardType ) {
            var code = creditCardTypeCode( creditCardType.type );
            if( code == ""){
                $( '.credit-card-type' ).html( "This credit card is not accepted. Please use Amex/Diners/Master/Visa." );
            }else{
                cardType = code;
                $( '.credit-card-type' ).html( creditCardType.type );
                $( this ).removeClass( "invalid-input" );
                $( '.credit-card-type.sub-form-input-warning' ).html( creditCardType.type ).show().removeClass( "cm-scale-animation" );
            }

        } else {
            $( this ).addClass( "invalid-input" );
            invalidWarningMessage( $( this ) );
            $( '.credit-card-type.sub-form-input-warning' ).html( "Please enter a valid credit card number." ).addClass( "cm-scale-animation" );
        }
    } );

    function validatePaymentInputFields() {
        // [TIC-FLOW]
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
            return true;
        }

        var contentToValidate = false;
        if(!guaranteedByCreditCard || $( '#PAYONLINENOW').is(':checked') || $( '#GUARANTEEPAYMENT').is(':checked')){
            return true;
        }else{
            contentToValidate = $('.credit-card-payment-wrp');
        }
        if(contentToValidate){
            contentToValidate.find( '.sub-form-mandatory' ).each( function() {
                console.log( $( this ) );
                if ( $( this ).val() == "" ) {
                    $( this ).addClass( "invalid-input" );
                    invalidWarningMessage( $( this ) );
                }
            } );
            return (contentToValidate.find( '.sub-form-mandatory.invalid-input' ).length == 0)
        }
        return contentToValidate;
    }

    $('.policy-terms-external-wrapper .mr-filter-checkbox').change(function(){
        if($(this).is(':checked')){
            $(this).closest('.policy-terms-external-wrapper').find('.policy-terms-warning-message').hide();
        }
    });

    var creditCardStartDate = new Date();
    var bookingOptionCache = dataCache.session.getData( "bookingOptions" );
    if(bookingOptionCache){
        creditCardStartDate = moment(creditCardStartDate,"MMM Do YY")["_d"];
    }

    $( "#creditCardExpiryDate" ).datepicker( {
        container:$('.cm-page-container'),        
        format: 'mm/yy',
        viewMode: "months",
        minViewMode: "months",
        startDate: "-0m",
        forceParse: false
    } ).on( 'changeDate', function( ev ) {

        $( this ).datepicker( 'hide' );
        $( "#creditCardExpiryDate" ).removeClass( 'invalid-input' );
    } );

    $( "#creditCardExpiryDate" ).on( "keyup paste", validateCreditCardInput );

    function validateCreditCardInput() {
        var creditCardValue = $( "#creditCardExpiryDate" ).val();
        var creditCardRegex = /^(0[1-9]|1[0-2])\/([0-9]{2})$/;
        if ( creditCardRegex.test( creditCardValue ) ) {
            var selectedDate = moment( creditCardValue, "MM/YY" )[ "_d" ];
            $( this ).datepicker( "setDate", selectedDate );
            if ( $( this ).datepicker( "getDate" ) ) {
                $( this ).removeClass( 'invalid-input' );
                return;
            }
        }
        $( this ).addClass( 'invalid-input' );
        invalidWarningMessage( $( this ) );
    }
    $( '.form-step' ).on( "click", function() {
        var formIndex = $( this ).index();
        if ( formIndex == 1 ) {
            $( '.proceed-payment-button' ).trigger( "click" );
        } else {
            $( '.form-step-content' ).hide();
            $( $( '.form-step-content' )[ formIndex ] ).show();
            $( '.checkout-enter-details-header' ).addClass( 'cm-hide' );
            $( $( '.checkout-enter-details-header' )[ formIndex ] ).removeClass( 'cm-hide' );
        }
    } );

    $( '.special-flight-common-header' ).on( 'click', function() {
        $( this ).siblings( '.special-flight-common-content' ).toggle();
        $( this ).find( '.special-flight-arrow' ).toggleClass( 'cm-rotate-show-more-icon' );
    } );

    $( '#creditCardExpiryDate' ).on( 'keyup', function( e ) {
        var inputLength = $( this ).val().length;
        if ( e.keyCode == 8 || e.which == 8 ) {
            // when input is backspace
            if ( inputLength == 3 ) {
                $( this ).val( $( this ).val().slice( 0, -1 ) );
            }
        } else {
            if ( inputLength == 2 ) {
                $( this ).val( $( this ).val() + "/" );
            }
        }
    } );
// //////////////////////// Modified Here ////////////////////////////////


    function payingAtCart(roomsData, step){
        var ecommerce= {};
        // var roomsData= dataCache.session.getData( "bookingOptions" );

        var checkout = {}
        var actionField = {};
        actionField.step= step;

        if(step === "2"){
            if(paymentOptValue=="Pay at hotel"){

                actionField.option= "VISA"
            }else if(paymentOptValue=="Pay Online now"){

                actionField.option= "PAY_ONLINE"
            }else {
                actionField.option= paymentOptValue;
            }
        }
        checkout.actionField=actionField;
        checkout.products=prepareJsonRoom(roomsData);
        ecommerce.checkout= checkout;
        pushRoomsJsonOnClick("checkout", ecommerce);

    }


    function prepareJsonRoom(roomsData){

        var products =[];

        var count= 0;
        var variant;

        roomDataArray= roomsData.selection;
        $(roomDataArray).each(function(index){
            var roomCardObj= {};

            roomCardObj.name=this.title;
            roomCardObj.id= roomsData.hotelId+"-"+this.roomTypeCode+"-"+this.ratePlanCode;
            roomCardObj.price=this.selectedRate;
            roomCardObj.brand= roomsData.targetEntity+"-"+roomsData.hotelId+"-"+"Indian Hotels Company Limited&quot";
            variant= $($(roomsData.selection[count].details).find('.more-rate-amenities .more-rate-title')).text();
            roomCardObj.category="Rooms/"+variant+"/"+this.title;
            roomCardObj.variant = variant;
            roomCardObj.quantity=count+1;
            count++;
            products.push(roomCardObj);

        });

        return products;


    }




    $('.pay-now-button, .pay-points-plus-cash, .pay-points-only').click(function() {
        
        // [TIC-FLOW]
        if(!ticPaymentFlow(this)){
            return;
        }
        
        // [TIC-FLOW]
        var bookOptionsSession = dataCache.session.getData("bookingOptions");
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        var successRedirctUrl ="";
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
            successRedirctUrl  = "/bin/redeemtransaction";
        }

        // debugger;
        var roomsData= dataCache.session.getData( "bookingOptions" );

        if(validatePaymentInputFields()){

            /*
             * This function pushes the analytics data to dataLayer this is the payment option step ("2")
             */
            payingAtCart(roomsData, "2");

            $( '.pay-now-button, .pay-points-plus-cash, .pay-points-only' ).hide();
            $( '.pay-now-spinner' ).show();
            // [TIC-FLOW LOADER]
            $('body').showLoader();
            var modifyBookingState = dataCache.session.getData('modifyBookingState');
            if(modifyBookingState && modifyBookingState!='modifyAddRoom'){
                var modifiedBookingOptions = dataCache.session.getData('bookingDetailsRequest');

                $.ajax({
                    type: 'GET',
                    url: '/bin/modifyReservation',
                    data: {
                        'modifyJson': modifiedBookingOptions
                    },
                    success: function (data) {
                        console.log(typeof data);
                        console.log(data);  
                        var responseObject = JSON.parse(data);                     
                        if(data == undefined || data == "" || data.length == 0){
                            var popupParams = {
                                    title: 'Edit Booking Failed!',
                                    description: 'Modify Booking response is empty.',
                            }
                            warningBox( popupParams );
                            hideSpinShowPaynowBTN();
                        }else if(responseObject.success){                                                    
                            dataCache.session.setData('bookingDetailsRequest',JSON.stringify(responseObject));
                            window.location.assign(contentRootPath+"/en-in/booking-confirmation.html?fromFindReservation=true");
                        } else if(!responseObject.success){
                            var res = JSON.parse(data);

                            var popupParams = {
                                    title: 'Edit Booking Failed!',
                                    description: res.errorMessage,
                            }
                            warningBox( popupParams );
                            hideSpinShowPaynowBTN();
                        }                     
                    },
                    fail: function(rrr){
                        var popupParams = {
                                title: 'Edit Booking Failed!',
                                description: 'Modify Booking service calling failed. Please try again later.',

                        }
                        warningBox( popupParams );
                        hideSpinShowPaynowBTN();
                        console.error("failed to call ajax post");
                    },
                    error: function(xhr){
                        var popupParams = {
                                title: 'Edit Booking Failed!',
                                description: 'Error occured while calling modifying service. Please try aftersome time.',

                        }
                        warningBox( popupParams );
                        hideSpinShowPaynowBTN();
                    }           
                });
            }else{                
                var failure = function(err) {
                    alert("Unable to retrive data "+err);
                };

                var billerDetails=extractBillerDetails();
                var guestDetails=extractGuestDetails();
                var paymentsDetails=extractPaymentDetails();
                var flightDetails=extractFlightDetails();
                var ticpoints=extractTicPointsData();

                paymentsDetails.ticpoints = ticpoints;

                var bookingDetailsRequest = {};
                bookingDetailsRequest.billerDetails = billerDetails;
                bookingDetailsRequest.guestDetails = guestDetails;
                bookingDetailsRequest.paymentsDetails = paymentsDetails;
                var cacheText = JSON.stringify(dataCache.session.getData( "bookingOptions" ));

                // converting text to JSON
                var cacheJSONData = JSON.parse(cacheText);
                paymentsDetails.currencyCode= cacheJSONData.currencySelected !=null ? cacheJSONData.currencySelected : 'INR' ;
                // paymentsDetails.totalCartPrice= cacheJSONData.totalCartPrice;
                var checkInDate = moment(cacheJSONData.fromDate,"MMM Do YY").format("YYYY-MM-DD");
                var checkOutDate = moment(cacheJSONData.toDate,"MMM Do YY").format("YYYY-MM-DD");
                var rooms = cacheJSONData.rooms;
                var totalCartPrice = cacheJSONData.totalCartPrice;
                var targetEntity = cacheJSONData.targetEntity;
                var selectionCount = cacheJSONData.selectionCount;
                var roomCount = cacheJSONData.roomCount;
                var selection = cacheJSONData.selection;
                var hotelChainCode = cacheJSONData.hotelChainCode;
                var hotelId = cacheJSONData.hotelId;
                var promotionCode= '' ;
                var appiedCoupon=cacheJSONData.couponCode !=null ? cacheJSONData.couponCode : '' ;
                var roomDetails = [];
                var hotelLocationId='';
                for (var i = 0; i < selection.length; i++) {
                    var roomDetail = {};
                    hotelId = selection[i].hotelId;
                    roomDetail["hotelId"] = selection[i].hotelId;
                    hotelLocationId = selection[i].hotelLocationId;
                    roomDetail["noOfAdults"] = selection[i].adults;
                    roomDetail["noOfChilds"] = selection[i].children;
                    roomDetail["roomTypeCode"] = selection[i].roomTypeCode;
                    roomDetail["roomCostAfterTax"] = selection[i].selectedRate;
                    roomDetail["bedType"] = selection[i].roomBedType;
                    roomDetail["roomTypeName"] = selection[i].title;
                    roomDetail["ratePlanCode"] = selection[i].ratePlanCode;
                    // [TIC-FLOW]
                    if(selection[i].selectedFilterTitle === 'TIC ROOM REDEMPTION RATES' || selection[i].selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'){
                        if(selection[i].ratePlanCode === 'NTCC'){
                            roomDetail["ratePlanCode"] = 'H00M';
                            roomDetail["promoCode"] = 'H00M';
                        }else if(selection[i].ratePlanCode === 'NTMS'){
                            roomDetail["ratePlanCode"] = 'H00N';
                            roomDetail["promoCode"] = 'H00N';
                        }else if(selection[i].ratePlanCode === 'NTMG'){
                            roomDetail["ratePlanCode"] = 'H00O';
                            roomDetail["promoCode"] = 'H00O';
                        }else if(selection[i].ratePlanCode === 'NTMP'){
                            roomDetail["ratePlanCode"] = 'H00P';
                            roomDetail["promoCode"] = 'H00P';
                        }else if(selection[i].ratePlanCode === 'NTMH'){
                            roomDetail["ratePlanCode"] = 'H0SN';
                            roomDetail["promoCode"] = 'H0SN';
                        }
                    }else if(selection[i].selectedFilterTitle === 'TAP ROOM REDEMPTION RATES'){
                        if(selection[i].ratePlanCode === 'NTAP'){
                            roomDetail["ratePlanCode"] = 'XO9';
                            roomDetail["promoCode"] = 'XO9';
                        }
                    }else if(selection[i].selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES'){
                            if(selection[i].ratePlanCode === 'NTAP'){
                                roomDetail["ratePlanCode"] = 'XO9';
                                roomDetail["promoCode"] = 'XO9';
                            }
                    }else{
                        roomDetail["promoCode"] = selection[i].promoCode;
                    }
                    roomDetails.push(roomDetail)
                }

                var loggedInUser;
                if(getUserData()){
                    loggedInUser = loggedInUserDetailsToGuest(getUserData());
                    // For Booker Profile Id
                    loggedInUser.profileId = (getUserData()).profileId;
                }

                var voucherCode = cacheJSONData.usedVoucherCode;
                // invoking datalayer call for tracking
                trigger_bookingCheckout(cacheJSONData,guestDetails,paymentsDetails);
                // alert('roomDetails:-> : '+ JSON.stringify(roomDetails));
                var guest = guestDetailsToGuest(guestDetails);
                var itineraryNumber = '';
                if(modifyBookingState=='modifyAddRoom'){
                    var modifiedBookingOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
                    itineraryNumber = modifiedBookingOptions.itineraryNumber;
                }
                // [TIC-FLOW]
                var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                var userSelectedCurrency = dataCache.session.getData("selectedCurrency");
                if(ticRoomRedemptionObjectSession &&  ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
                    var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
                    if(bookingOptionsSessionData && bookingOptionsSessionData.selection){
                        ticRoomRedemptionObjectSession.selection = bookingOptionsSessionData.selection;
                        ticRoomRedemptionObjectSession.ticpoints = ticpoints ;
                        var userdata=getUserData();
                        ticRoomRedemptionObjectSession.currencySelected = userSelectedCurrency;
                        dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObjectSession);
                    }
                }
                var ticSession = dataCache.session.getData("ticRoomRedemptionObject");
                if((ticSession && ticSession.isTicRoomRedemptionFlow)){
                    hotelLocationId = "";
                }
                
                if(hotelId == hotelLocationId){
                    console.log("HotelId and LocationId both are same.");
                    hotelLocationId = "";
                }
                if(isIHCLCBSiteFlag){
                    corporateBooking = true;
                }
          
                $.ajax({
                    type: 'post',
                    url : '/bin/bookHotelInitiateServlet', 
                    data: 'guestDetails='+JSON.stringify(guest)
                    +'&paymentsDetails='+JSON.stringify(paymentsDetails)
                    +"&roomDetails="+JSON.stringify(roomDetails)
                    +"&flightDetails="+JSON.stringify(flightDetails)
                    +"&checkInDate="+checkInDate
                    +"&checkOutDate="+checkOutDate
                    +"&hotelId="+hotelId
                    +"&hotelLocationId="+hotelLocationId
                    +"&hotelChainCode="+hotelChainCode
                    +"&promotionCode="+promotionCode
                    +"&appliedCoupon="+appiedCoupon
                    +"&voucherCode="+voucherCode
                    +"&corporateBooking="+corporateBooking
                    +"&itineraryNumber="+itineraryNumber
                    +"&successRedirctUrl="+successRedirctUrl
                    +"&loggedInUser="+JSON.stringify(loggedInUser),
                    success: function(returnmessage){
                        if(returnmessage == undefined || returnmessage == "" || returnmessage.length == 0){
                            var message = 'Something went Wrong. Refreshing Page';
                            var popupParams = {
                                    title: 'Booking Failed!',
                                    description: message
                            }
                            warningBox( popupParams );
                            hideSpinShowPaynowBTN();
                            location.reload();
                        }else if(returnmessage.includes("form action")){
                            var returnCheck;
                            if(voucherCode){
                                returnCheck=redeemVoucherAjaxCall();
                                if(returnCheck){
                                    var $form=$(returnmessage);
                                    $('body').append($form);
                                    $form.submit();
                                }
                            }else {
                                var $form=$(returnmessage);
                                $('body').append($form);
                                $form.submit();
                            }
                        }else if(returnmessage.includes("success\":true")){
                            var returnJson = JSON.parse(returnmessage)
                            if(returnJson.partialBooking){
                                $('#paymentid').val(returnmessage);
                                var roomStatustoJson = returnJson;
                                /*
                                 * $('#bookingform').hide(); $('#bookingalert').show();
                                 */
                                var rooms = [];
                                roomStatustoJson.roomList.forEach( function( value ) {

                                    var roomType = "";
                                    if(value.roomTypeName){
                                        roomType = value.roomTypeName;
                                    }else{
                                        roomType = value.roomTypeCode;
                                    }

                                    var statusValue = "";
                                    if(value.bookingStatus){
                                        statusValue = "AVAILABLE";
                                    }else{
                                        statusValue = "UNAVAILABLE";
                                    }
                                    rooms.push( {
                                        'type': roomType,
                                        'status': statusValue,
                                        'adults': value.noOfAdults,
                                        'children': value.noOfChilds

                                    } );
                                } );
                                var popupParams = {
                                        title: 'Not all the rooms are Available. Would you like to continue with this booking?',
                                        callBack: paymentProcessOfVoucher.bind(),
                                        callBackSecondary: ignoreBooking.bind(),
                                        needsCta: true,
                                        isForRoomAvailability: true,
                                        rooms: rooms
                                }
                                warningBox( popupParams );
                            }else if(!returnJson.partialBooking){
                                if(voucherCode){
                                    if(redeemVoucherAjaxCall()){
                                        if(returnJson.payments.payAtHotel){
                                            $('#paymentid').val(returnmessage);
                                            doCommit();
                                        }
                                    }
                                }else{
                                    if(returnJson.payments.payAtHotel){
                                        $('#paymentid').val(returnmessage);
                                        doCommit();
                                    }
                                }

                            }else {
                                /* alert(returnmessage); */
                                var popupParams = {
                                        title: 'Booking Failed!',
                                        description: returnmessage,
                                }
                                warningBox( popupParams );
                                hideSpinShowPaynowBTN();
                            }
                        }else if(returnmessage.includes("success\":false")){
                            var returnJSON = JSON.parse(returnmessage);
                            var warningPopUP = {
                                    title: 'Booking Failed!'
                            }
                            if(returnJSON.warnings){
                                warningPopUP.description = returnJSON.warnings;
                            }else {
                                warningPopUP.description = 'All Rooms are unavailable. Please try after sometime.'; 
                            }
                            warningBox( warningPopUP );
                            hideSpinShowPaynowBTN();
                        }else{
                            var exceptionPopupParams = {
                                    title: 'Booking Failed!',
                                    description: returnmessage,
                            }
                            warningBox( exceptionPopupParams );
                            hideSpinShowPaynowBTN();
                        }
                    },
                    fail: function(rrr){
                        var popupParams = {
                                title: 'Booking Failed!',
                                description: 'Booking service calling failed. Please try again later.',

                        }
                        warningBox( popupParams );
                        hideSpinShowPaynowBTN();
                        console.error("failed to call ajax post");
                    },
                    error: function(xhr){
                        var popupParams = {
                                title: 'Booking Failed!',
                                description: 'Error occured while calling  booking service. Please try aftersome time.',

                        }
                        warningBox( popupParams );
                        hideSpinShowPaynowBTN();
                    }
                });
            }
        }
    });

// ////////////////////////Modified Till Here ////////////////////////////////
    function getCountryDetail(value) {        
       // var selectedValue = $( '#bookingGuestCountry' ).val();
        var selectedValue = $bookingGuestCountry.val();
        var selectedValueLength = selectedValue.length;
        var indexOfPlus = selectedValue.indexOf('+');
        var countryName = selectedValue.substr(0,indexOfPlus-1).trim();
        var countryCode = selectedValue.substr(indexOfPlus,selectedValueLength).replace(')','');
        if(value=='name')
            return countryName;
        if(value == 'code')
            return countryCode;        
        return '';
    }

    function extractBookingDetails(value)
    {
        return JSON.stringify(dataCache.session.getData( "bookingOptions" ));
    }

    function extractGuestDetails()
    {
        var guestTitle = $('#bookingGuestTitle option:selected').text();
        var guestFirstName = $('input[name=guestFirstName]').val() ;
        var guestLastName = $('input[name=guestLastName]').val() ;
        var guestEmail = $('input[name=guestEmail]').val() ;
        var guestPhoneNumber = getCountryDetail('code') + $('input[name=guestPhoneNumber]').val() ;
        var guestGSTNumber = $('input[name=guestGSTNumber]').val() ;
        /* var guestCountry = $('input[name=guestCountry]').val() ; */
        var guestCountry = getCountryDetail('name');
        var guestMembershipNumber = $('input[name=guestMembershipNumber]').val() ;
        var specialRequest = $('textarea[name=specialRequest-data]').val().trim();
        // [IHCLCB START]
        var paymentNotes = getTextValueOf('.ihclcb-payment-notes-text-area');
        if(isIHCLCBSiteFlag) {            
            userDetails = getUserData();
            if($('#sendNotificationToId').is(':checked')) {
                storeGuestEmailToSession(guestEmail, false);
            } else {
                storeGuestEmailToSession(guestEmail, true);
            }
            guestEmail = userDetails.email;
        }
        var trimmedPaymentNotes = paymentNotes.trim();
        if(trimmedPaymentNotes){
            specialRequest = specialRequest + ' ' + trimmedPaymentNotes;
            if(isIHCLCBSiteFlag) {
                specialRequest += ' ' + $('#FTO-comment').val().trim() + ' ' + $('#FTOCountry').val();
            }
        }
        var autoEnrollCheckbox = $('input[name=autoEnrollCheckbox]').is( ":checked" );
        var gdprCheckbox = $('input[name=gdprCheckbox]').is( ":checked" );
        var airportPickupCheckbox = $('input[name=airportPickupCheckbox]').is( ":checked" );
        var redeemTicOrEpicurePoints = $('input[name=redeemTicOrEpicurePoints]').is( ":checked" );
        var ticValue = $("#ticValue").text();
        var epicureValue = $('#epicureValue').text();
        var guestDetails = {};
        guestDetails.guestTitle = guestTitle;
        guestDetails.guestFirstName = guestFirstName;
        guestDetails.guestLastName = guestLastName;
        guestDetails.guestEmail = guestEmail;
        guestDetails.guestPhoneNumber = guestPhoneNumber;
        guestDetails.guestGSTNumber = guestGSTNumber;
        guestDetails.guestCountry = guestCountry;
        guestDetails.guestMembershipNumber = guestMembershipNumber;
        guestDetails.specialRequests=specialRequest;
        guestDetails.autoEnrollTic=autoEnrollCheckbox;
        guestDetails.gdpr=gdprCheckbox;
        guestDetails.airportPickup=airportPickupCheckbox;
        return guestDetails;
    }

    function storeGuestEmailToSession(email, emailToOthersOnly) {
       var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
       if(ihclCbBookingObject) {
           ihclCbBookingObject.sendNotification = email;
		   ihclCbBookingObject.emailToOthersOnly = emailToOthersOnly;
           dataCache.session.setData("ihclCbBookingObject",ihclCbBookingObject);
       }
    }
    
    function getTextValueOf(cssSelelctor) {
        var textValue = "";
        var domElement = $(cssSelelctor);
        if(domElement) {
            var domElementValue = domElement.val();
            if(domElementValue) {
                textValue = domElementValue;
            }
        }
        return textValue;
    }

    function guestDetailsToGuest(guestDetails){
        var guest = {};
        guest.title = guestDetails.guestTitle;
        guest.firstName = guestDetails.guestFirstName;
        guest.lastName =  guestDetails.guestLastName;
        guest.email = guestDetails.guestEmail;
        guest.phoneNumber = guestDetails.guestPhoneNumber;
        guest.gstNumber = guestDetails.guestGSTNumber;
        guest.country = guestDetails.guestCountry;
        guest.membershipId = guestDetails.guestMembershipNumber;
        guest.specialRequests=guestDetails.specialRequests;
        guest.autoEnrollTic=guestDetails.autoEnrollCheckbox;
        guest.gdpr=guestDetails.gdprCheckbox;
        guest.airportPickup=guestDetails.airportPickupCheckbox;

        // For Booker Id::
        if(window.location.pathname.search('corporate-booking') > -1){
            if(getUserData() && (getUserData()).profileId) {
                guest.specialRequests = guest.specialRequests + ',profileId:' + (getUserData()).profileId;
            }
            var adminElem = $('#adminEmail');
            if(adminElem && adminElem.text()) {
                guest.email = guest.email + ',' + adminElem.text();
            }
        }

        // handel voucher code and voucher pin input in special request
        var voucherRedemption = dataCache.session.getData('voucherRedemption');
        if(voucherRedemption && voucherRedemption == 'true'){
            guest.specialRequests = guest.specialRequests +';voucherCode:#'+$('#guest-VoucherCode').val()+'#;voucherPin:'+$('#guest-VoucherPin').val();
        }
        return guest;
    }

    function loggedInUserDetailsToGuest(guestDetails){
        var guest = {};
        if(guestDetails.firstName){
            guest.firstName = guestDetails.firstName;
            guest.lastName = guestDetails.lastName;
        }else if(guestDetails.name){
            guest.lastName = guestDetails.name;
        }
        guest.email = guestDetails.email;
        guest.phoneNumber = guestDetails.phoneNumber;
        guest.country = guestDetails.country;
        guest.cdmReferenceId = guestDetails.cdmReferenceId;
        return guest;
    }

    function extractBillerDetails()
    {
        var bookingDetailsCheckbox = $('input[name=bookingOthersCheckbox]').is( ":checked" )
        var billerName = $('input[name=billerName]').val() ;
        var billerEmail = $('input[name=billerEmail]').val() ;
        var billerPhoneNumber = $('input[name=billerPhoneNumber]').val() ;
        var billerGstNumber = $('input[name=billerGstNumber]').val() ;
        var billerDetails = {};
        billerDetails.billerName = billerName;
        billerDetails.billerEmail = billerEmail;
        billerDetails.billerPhoneNumber = billerPhoneNumber;
        billerDetails.billerGstNumber = billerGstNumber;
        return billerDetails;

    }

    function extractPaymentDetails()
    {
        // ///// Payment RadioButton - fields //////
        var paymentMethod = $('input[name = payment-method]:checked').val();
        // [TIC-FLOW]
        var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        var payAtHotel = false;
        var payOnlineNow = false;
        var payGuaranteeAmount = false;
        var payUsingGiftCard = false;
        var payUsingCreditCard = false;
        // [TIC-FLOW]
        if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
            var ticFlowSelectedRadioId = $('input[type=radio][name=room-redeemption]:checked').attr('id');
            if(ticFlowSelectedRadioId === 'redeem-with-point'){
                payAtHotel = true;
            }else if(ticFlowSelectedRadioId === 'redeem-with-point-plus-cash'){
                payOnlineNow = true;
            }
        }else{
            payAtHotel = $("#PAYATHOTEL").is(":checked");
            payOnlineNow = $("#PAYONLINENOW").is(":checked") || $("#GUARANTEEPAYMENT").is(":checked");
            payGuaranteeAmount = $("#GUARANTEEPAYMENT").is(":checked");
            payUsingGiftCard = $("#PAYUSINGGIFTCARD").is(":checked");
            payUsingCreditCard = $("#PAYUSINGCREDITCARD").is(":checked");
            if($("#BILLTOCOMPANY").is(":checked")){
                payAtHotel = $("#BILLTOCOMPANY").is(":checked");
            }
        }

        // ///// Payment card Details - fields //////
        var nameOnCard = $('input[name=nameOnCard]').val() ;
        var numberOncard = $('input[name=numberOncard]').val() ;
        var expiryDateOnCard = $('input[name=expiryDateOnCard]').val() ;

        var paymentsDetails = {};
        // paymentsDetails.paymentMethod = paymentMethod;
        paymentsDetails.payAtHotel = payAtHotel;
        paymentsDetails.payOnlineNow = payOnlineNow;
        paymentsDetails.payGuaranteeAmount = payGuaranteeAmount;
        paymentsDetails.payUsingGiftCard = payUsingGiftCard;
        paymentsDetails.payUsingCerditCard = payUsingCreditCard;
        paymentsDetails.creditCardRequired = guaranteedByCreditCard;
        paymentsDetails.cardType = cardType;
        paymentsDetails.nameOnCard = nameOnCard;
        paymentsDetails.cardNumber = numberOncard;
        paymentsDetails.expiryMonth = expiryDateOnCard;
        return paymentsDetails;
    }

    function extractFlightDetails(){
        var arrivalFlightNo=$('input[name=flightNumber]').val() ;
        var flightArrivalTime=$('input[name=flightArrivalTime]').val() ;
        var arrivalAirportName=$('input[name=airportName]').val() ;
        var flightDetails={};

        flightDetails.arrivalFlightNo=arrivalFlightNo;
        flightDetails.flightArrivalTime=flightArrivalTime;
        flightDetails.arrivalAirportName=arrivalAirportName;
        return flightDetails;
    }
    // [TIC-FLOW] Updating DOM WITH TIC AND EPICURE POINTS
    updateTICandEPICURE();
    ticRoomRedemptionTranscation();
/*
 * // [TIC-FLOW] $('input[type=redeemTicInput]').blur(function (e) { var bookOptions =
 * dataCache.session.getData("bookingOptions"); var totalCostOfReservationWithoutTax = 0;
 * bookOptions.selection.forEach(function(item, index) { totalCostOfReservationWithoutTax =
 * totalCostOfReservationWithoutTax + item.roomBaseRate; }) });
 * 
 * $('input[type=redeemEpicureInput]').blur(function (e) { var bookOptions =
 * dataCache.session.getData("bookingOptions"); var totalCostOfReservationWithoutTax = 0;
 * bookOptions.selection.forEach(function(item, index) { totalCostOfReservationWithoutTax =
 * totalCostOfReservationWithoutTax + item.roomBaseRate; }) });
 */
    

    // [TIC-FLOW]
    ticPaymentDetailsFlow();
} );

function hideSpinShowPaynowBTN(){
    $('body').hideLoader();
    $( '.pay-now-spinner' ).hide();
    $( '.pay-now-button, .pay-points-plus-cash, .pay-points-only ' ).show();
}

function paymentProcessOfVoucher()
{
    if(getIhclIssuedVoucherNumber()){
        if(redeemVoucherAjaxCall()){
            paymentProcess();
        }
    }else{
        paymentProcess();
    }
}

function paymentProcess(){
    var paymentobject=$('input[name=paymentid]').val();

    if(paymentobject.includes("payAtHotel\":true")){
        doCommit();
    }else{
        var dataForPayment = encodeURIComponent(paymentobject);
        $.ajax({
            type: 'post',
            url : '/bin/paymentProcessing', 
            data: 'bookingDetailsRequest='+dataForPayment,
            success: function(returnmessage){
                if(returnmessage == undefined || returnmessage == "" || returnmessage.length == 0){
                    var message = 'Something went Wrong. Refreshing Page';
                    var popupParams = {
                            title: 'Payment process Failed!',
                            description: message
                    }
                    warningBox( popupParams );
                }else if(returnmessage.includes("form")){
                    var $form=$(returnmessage);
                    $('body').append($form);
                    $form.submit();
                }else{
                    var popupParams = {
                            title: 'Payment process Failed!',
                            description: 'Something went wrong. Please try again later'
                    }
                    warningBox( popupParams );
                }
            },
            fail: function(rrr){
                console.error("failed to call ajax post");
                var popupParams = {
                        title: 'Payment process Failed!',
                        description: 'Call to service failed. Please try again later.',

                }
                warningBox( popupParams );
            },
            error: function(xhr){
                var popupParams = {
                        title: 'Payment process Failed!',
                        description: 'Error occured while calling service. Please try aftersome time.'

                }
                warningBox( popupParams );
            }           
        });
    }
}

function doCommit(){
    var paymentObject=$('input[name=paymentid]').val();
    var bookingConfirmationURL = $('.booking-confirmation').attr("data-bookingConfirmationURL");
    if(bookingConfirmationURL == undefined || bookingConfirmationURL == "" || bookingConfirmationURL.length == 0) {
        bookingConfirmationURL = "/en-in/booking-confirmation";
    }
    var bookingFailedURL = $('.booking-cart').attr("data-bookingFailedURL");
    if(bookingFailedURL == undefined || bookingFailedURL == "" || bookingFailedURL.length == 0) {
        bookingFailedURL = "/en-in/booking-cart";
    }
    if(dataCache.session.getData('modifyBookingState')=="modifyAddRoom"){
        var paymentJSONObject = JSON.parse(paymentObject);
        paymentJSONObject.itineraryNumber= null;
        paymentObject = JSON.stringify(paymentJSONObject);
    }
    var postPaymentURL = '/bin/postPaymentServlet';
    if(corporateBooking){
        postPaymentURL = '/bin/corporatePostPaymentServlet';
    }
    
    var dataForAjax = encodeURIComponent(paymentObject);
    $.ajax({
        type : 'POST',
        url : postPaymentURL,
        data: "bookingObjectRequest="+dataForAjax,
        dataType:'text',
        beforeSend: function(){
            $( '.pay-now-spinner' ).show();
        },
        success: function(returnmessage){
            $( '.pay-now-spinner' ).hide();
            if(returnmessage == undefined || returnmessage == "" || returnmessage.length == 0){
                var popupParams = {
                        title: 'Something went Wrong!',
                        description: 'Could not find confirmation details.',
                }
                warningBox( popupParams );
                sendIgnore(paymentObject,bookingFailedURL);
            }else if(returnmessage.includes('ExceptionFound')){
                var popupParams = {
                        title: 'Booking Failed!',
                        description: 'Booking service calling failed. Please try again later.',

                }
                warningBox( popupParams );
                hideSpinShowPaynowBTN();
            }else{
                sessionStorage.setItem('bookingDetailsRequest', returnmessage);
                window.location.assign(bookingConfirmationURL);
            }

        },
        error: function(errormessage){
            $( '.pay-now-spinner' ).hide();
            var popupParams = {
                    title: 'Booking Failed!',
                    description: 'Booking Failed. Please check with support team.',
            }
            warningBox( popupParams );
            sendIgnore(paymentObject,bookingFailedURL);
        }
    });
}

function ignoreBooking(){

    $('#Cancelbooking').hide();
    $('#cancel-spin').show();
    var bookingFailedURL = $('.booking-cart').attr("data-bookingFailedURL");
    if(bookingFailedURL == undefined || bookingFailedURL == "" || bookingFailedURL.length == 0) {
        bookingFailedURL = "/en-in/booking-cart";
    }
    var paymentobject=$('input[name=paymentid]').val();
    sendIgnore(paymentobject,bookingFailedURL);
}

function sendIgnore(ignoreObject, afterRedirectUrl){
    var dataForIgnore = encodeURIComponent(ignoreObject)
    var status="pass";
    $.ajax({
        type : 'POST',
        url : '/bin/bookHotelIgnoreServlet',
        data: 'bookingDetailsRequest='+dataForIgnore,
        success: function(returnmessage){
            $('#cancel-spin').hide();
        },
        fail: function(rrr){
            status="fail";
        },
        error: function( xhr){
            status="fail";
        } ,
        complete: function(data){
            window.location.assign(afterRedirectUrl);
        }
    });

    if(status.localeCompare("fail")==0){
        console.warn("Ignore Request failed");
    }
}


// following 2 functions are made for analytics code
function payingAtCart(roomsData, step){
    var ecommerce= {};
    var checkout = {}
    var actionField = {};
    actionField.step= step;

    if(step === "2"){
        if(paymentOptValue=="Pay at hotel"){

            actionField.option= "VISA"
        }else if(paymentOptValue=="Pay Online now"){

            actionField.option= "PAY_ONLINE"
        }else {
            actionField.option= paymentOptValue;
        }
    }
    checkout.actionField=actionField;
    checkout.products=prepareJsonRoom(roomsData);
    ecommerce.checkout= checkout;
    pushRoomsJsonOnClick("checkout", ecommerce);

}


function prepareJsonRoom(roomsData){
    var products =[];
    var count= 0;
    var variant;
    roomDataArray= roomsData.selection;

    $(roomDataArray).each(function(index){
        var roomCardObj= {};
        roomCardObj.name=this.title;
        roomCardObj.id= roomsData.hotelId+"-"+this.roomTypeCode+"-"+this.ratePlanCode;
        roomCardObj.price=this.selectedRate;
        roomCardObj.brand= roomsData.targetEntity+"-"+roomsData.hotelId+"-"+"Indian Hotels Company Limited";
        variant= $($(roomsData.selection[count].details).find('.more-rate-amenities .more-rate-title')).text();
        roomCardObj.category="Rooms/"+variant+"/"+this.title;
        roomCardObj.variant = variant;
        roomCardObj.quantity=count+1;
        count++;
        products.push(roomCardObj);

    });
    return products;
}
function getIhclIssuedVoucherNumber() {
    var bookingOptionsCache = dataCache.session.getData('bookingOptions');
    var ihclIssuedVoucherNumber = bookingOptionsCache.usedVoucherCode;
    return ihclIssuedVoucherNumber;
}
/**
 * @returns
 */
function redeemVoucherAjaxCall(){
    var returnCheck = false;
    var voucherCheck=false;
    var returnData=JSON.parse(returnmessage);
    if(returnData && returnData.roomList && returnData.roomList.length!=0){
        for(var p=0;p<returnData.roomList.length;p++)
        {
            if(returnData.roomList[p].ratePlanCode =='X5'){
                voucherCheck=true;
            }
        }
    }
    if(voucherCheck){

        $.ajax({

            type : 'GET',
            url : '/bin/redeemVouchers',
            data : "IHCLIssuedVoucherNumber=" + getIhclIssuedVoucherNumber(),
            success : function(res) {
                var redeemDetails = JSON.parse(res);
                if (redeemDetails.message) {
                    returnCheck = true;
                    var popupParams = {
                            title : 'You have successfully redeemed the voucher'
                                + redeemDetails.data[0].ihclissuedVoucherNumber
                    };
                    warningBox(popupParams);
                    $(".cm-warning-box").addClass("hide-warn-symbol");
                    /*
                     * var voucherdata={}; voucherdata=redeemDetails.data[0]; returnData.voucher={};
                     * returnData.voucher=voucherdata; dataCache.session.setData('bookingDetailsRequest',
                     * JSON.stringify(returnData)); window.location.assign(bookingConfirmationURL);
                     */
                } else {
                    warningBox({
                        title : 'Something went wrong while redeeming'
                    });
                }
            },
            error : function(res) {
                warningBox({
                    title : 'Something went wrong while calling the redeem service'
                });
            }
        });

    }
}

// [TIC-FLOW]
function updateTICandEPICURE(){
    var userDetails =  getUserData();
    var bookingOptionsSession = dataCache.session.getData( "bookingOptions" );
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    var userSelectedCurrency = dataCache.session.getData("selectedCurrency");
    if(userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession &&  ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
  var membertype=userDetails.card.type;
        if(membertype.includes("TIC")){
        $('.paynow-paylater-block').hide();
        $(".tic-point-value").html(userDetails.ticPoints);
        $(".epicure-point-value").html(userDetails.epicurePoints);
 }else if(membertype === "TAP"){
            $(".tic-point-value").html(userDetails.tapPoints); // shows users available tap points
        }else if(membertype === "TAPPMe"){
            $(".tic-point-value").html(userDetails.tappmePoints);// shows user's available tapp me points
        }else{
            console.log("Unknown Member Type");
        }
        $('input[name=redeemTicOrEpicurePoints]').prop('checked', true);
        $('.redeem-checkbox-wrp').addClass('redeem-points-checked');

        var totalReservationCost = 0;
        bookingOptionsSession.selection.forEach(function(item, index) {
            totalReservationCost = totalReservationCost + item.roomBaseRate + item.roomTaxRate;
        })

        if (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹') {
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency + '_INR']);
                totalReservationCost = Math.round(totalReservationCost * conversionRate);
            }

        }


        var totalTicPointsNeededForReservation = Math.ceil(totalReservationCost);
        var totalEpicurePointsNeededForReservation = Math.ceil(totalReservationCost / 2);
        $(".tic-point-value-redeemable").html(totalTicPointsNeededForReservation);
        $(".epicure-point-value-redeemable").html(totalEpicurePointsNeededForReservation);
    }else{
        $('.redeem-checkbox-wrp').hide();
    }
}

// [TIC-FLOW]
function ticRoomRedemptionTranscation(){

    var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
        $('.payment-type-radio-wrp').hide();
        $('.payment-radio-content').hide();
        $('.pay-now-button-wrp').hide();

        $("#redeem-with-point").prop("checked", true);
        $("#redeem-with-tic-point").prop("checked", true);
        $("#redeem-with-tic-point-plus-cash").prop("checked", true);
        $('.room-redeemption').change( function() {
            if(this.id == 'redeem-with-point'){
                $(".tic-room-redemption-points-selection").removeClass('d-none');
                $(".tic-room-redemption-points-plus-cash-selection").addClass('d-none');
                validatePointsRequiredForBooking();
            }else{
                $(".tic-room-redemption-points-selection").addClass('d-none');
                $(".tic-room-redemption-points-plus-cash-selection").removeClass('d-none');
                validatePointsAndCashBooking();
                $('[value=TIC-Point-Cash]').trigger('click');
                $('[value=TIC-Point-Cash]').trigger('change');
            }
        } );

        setDefaultTicRoomRedemptionValue();
        validatePointsRequiredForBooking();
        validatePointsAndCashBooking();
        checkPointsPlusCachedClick();
    }
}

function validatePointsRequiredForBooking() {
    $(".pay-points-only").prop("disabled",false);
    $(".pay-points-only").css({ "opacity" : 1 });
    $("#message-for-less-points").addClass('d-none');

    var availableTicValue = parseFloat($("#ticValue").text());
    var availableEpicureValue = parseFloat($("#epicureValue").text());
    var ticPointsNeededForReservation = parseFloat($(".tic-point-value-redeemable").html());
    var epicurePointsNeededForReservation = parseFloat($(".epicure-point-value-redeemable").html());

    if($('#redeem-with-tic-point').prop('checked')) {
        if(ticPointsNeededForReservation > availableTicValue) {
            $(".pay-points-only").prop("disabled",true);
            $(".pay-points-only").css({ "opacity" : 0.5 });
            $("#message-for-less-points").removeClass('d-none');
            $("#message-for-less-points").text("You don't have sufficient TIC points, select Point + Cash option instead.");
        }
        $( "#redeemTicInput" ).keyup();
    }

    if($('#redeem-with-epicure-point').prop('checked')) {
        if(epicurePointsNeededForReservation > availableEpicureValue) {
            $(".pay-points-only").prop("disabled",true);
            $(".pay-points-only").css({ "opacity" : 0.5 });
            $("#message-for-less-points").removeClass('d-none');
            $("#message-for-less-points").text("You don't have sufficient Epicure points, select Point + Cash option instead.");
        }
        $( "#redeemEpicureInput" ).keyup();
    }
}

function validatePointsAndCashBooking() {
    $(".pay-points-plus-cash").prop("disabled",false);
    $(".pay-points-plus-cash").css({ "opacity" : 1 });
    $("#message-for-less-points-and-cash").addClass('d-none');

    var availableTicValue = parseFloat($("#ticValue").text());
    var availableEpicureValue = parseFloat($("#epicureValue").text());
    var ticPointsAndCashNeededForReservation = parseFloat($("#redeemTicInput").val());
    var epicurePointsAndCashNeededForReservation = parseFloat($("#redeemEpicureInput").val());

    if($('#redeem-with-tic-point-plus-cash').prop('checked')) {
        if(ticPointsAndCashNeededForReservation >= availableTicValue) {
            $(".pay-points-plus-cash").prop("disabled",true);
            $(".pay-points-plus-cash").css({ "opacity" : 0.5 });
            $("#message-for-less-points-and-cash").removeClass('d-none');
            $("#message-for-less-points-and-cash").text("You don't have sufficient TIC points.");
        }
        $( "#redeemTicInput" ).keyup();
    }

    if($('#redeem-with-epciure-point-plus-cash').prop('checked')) {
        if(epicurePointsAndCashNeededForReservation >= availableEpicureValue) {
            $(".pay-points-plus-cash").prop("disabled",true);
            $(".pay-points-plus-cash").css({ "opacity" : 0.5 });
            $("#message-for-less-points-and-cash").removeClass('d-none');
            $("#message-for-less-points-and-cash").text("You don't have sufficient Epicure points.");
        }
        $( "#redeemEpicureInput" ).keyup();
    }
}

function extractTicPointsData(){
    var bookOptions = dataCache.session.getData("bookingOptions");
    var userSelectedCurrency = dataCache.session.getData("selectedCurrency");
    var redeemTicOrEpicurePoints = $('input[name=redeemTicOrEpicurePoints]').is( ":checked" );
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
var membertype=getUserData();
var ticpoints={};
if(membertype && membertype.card && membertype.card.type.includes("TIC")){
    var ticValue = $("#ticValue").text();
    var epicureValue = $('#epicureValue').text();
    var ticpoints={};
    ticpoints.redeemTicOrEpicurePoints = redeemTicOrEpicurePoints;
    ticpoints.ticPlusCreditCard = ticValue;
    ticpoints.epicurePlusCreditCard = epicureValue;
    ticpoints.pointsPlusCash = false;
    ticpoints.currencyConversionRate = 1;

    var ticFlowSelectedRadioId = $('input[type=radio][name=room-redeemption]:checked').attr('id');
    if(ticFlowSelectedRadioId === 'redeem-with-point'){
        var ticFlowRedeemWithPointRadioId = $('input[type=radio][name=redeem-with-point]:checked').attr('id');
        if(ticFlowRedeemWithPointRadioId === 'redeem-with-tic-point'){
            ticpoints.pointsType = 'TIC';
            ticpoints.noTicPoints = parseFloat($('.tic-point-value-redeemable').html());;
        }else if(ticFlowRedeemWithPointRadioId === 'redeem-with-epicure-point'){
            ticpoints.pointsType = 'EPICURE';
            ticpoints.noEpicurePoints = parseFloat($('.epicure-point-value-redeemable').html());;
        }
        ticpoints.pointsPlusCash = false;
    }else if(ticFlowSelectedRadioId === 'redeem-with-point-plus-cash'){
        var ticFlowRedeemWithPointPlusCashRadioId = $('input[type=radio][name=redeem-with-point-plus-cash]:checked').attr('id');
        if(ticFlowRedeemWithPointPlusCashRadioId === 'redeem-with-tic-point-plus-cash'){
            ticpoints.pointsType = 'TIC';
            ticpoints.noTicPoints = $('#redeemTicInput').val();;
        }else if(ticFlowRedeemWithPointPlusCashRadioId === 'redeem-with-epciure-point-plus-cash'){
            ticpoints.pointsType = 'EPICURE';
            ticpoints.noEpicurePoints = $('#redeemEpicureInput').val();
        }
        ticpoints.pointsPlusCash  = true;
    }
  }else if(membertype && membertype.card && membertype.card.type === "TAP"){
        ticpoints.pointsType = 'TAP';
        ticpoints.noTicPoints = parseFloat($('.tic-point-value-redeemable').html());
    }else if(membertype && membertype.card && membertype.card.type === "TAPPMe"){
        ticpoints.pointsType = 'TAPPMe';
        ticpoints.noTicPoints = parseFloat($('.tic-point-value-redeemable').html());
    }else{
        console.log("Unknown Member Type");
    }

    if (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹') {
        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
            var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency + '_INR']);
            ticpoints.currencyConversionRate = conversionRate;
        }

    }



    return ticpoints;
}

function getTotalTicFlowRoomPrice(){
    var bookOptionsSessionData = dataCache.session.getData("bookingOptions");
    var userSelectedCurrency = dataCache.session.getData("selectedCurrency");
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    var totalCardPrice = 0;
    if(bookOptionsSessionData && bookOptionsSessionData.selection){
        for (var i = 0; i < bookOptionsSessionData.selection.length; i++) {
            var roomData = bookOptionsSessionData.selection[i];
            totalCardPrice = totalCardPrice + roomData.roomBaseRate + roomData.roomTaxRate;
        }
    }

    if ( (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹' ) && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
            var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency + '_INR']);
            totalCardPrice = Math.round(totalCardPrice * conversionRate);
    }

    return Math.round(totalCardPrice);
}

function setDefaultTicRoomRedemptionValue(){
    var totalTicMoneyCharged = 0;
    var totalEpicureMoneyCharged = 0;
    var totalTicCartCharged = getTotalTicFlowRoomPrice();
    var totalEpicureCartCharged = Math.ceil(totalTicCartCharged/2);

    totalTicMoneyCharged  = totalTicCartCharged - Math.ceil(totalTicCartCharged/2);
    totalEpicureMoneyCharged = totalEpicureCartCharged - Math.ceil(totalEpicureCartCharged/2);

    $(".required-tic").html(totalTicCartCharged);
    $(".entered-tic").html(Math.ceil(totalTicCartCharged/2));
    $("#redeemTicInput").val(Math.ceil(totalTicCartCharged/2));
    $(".tic-cash-adjusted").html(totalTicMoneyCharged * 1);
    $(".required-epicure").html(totalEpicureCartCharged);
    $(".entered-epicure").html(Math.ceil(totalEpicureCartCharged/2));
    $(".epicure-cash-adjusted").html(totalEpicureMoneyCharged * 10);
    $("#redeemEpicureInput").val(Math.ceil(totalTicCartCharged/4));


    $( "#redeemTicInput" ).bind("keyup change", function() {
        var ticValueEntered = Math.ceil($("#redeemTicInput").val());
        $(".entered-tic").html(ticValueEntered);
        totalTicMoneyCharged  = totalTicCartCharged - ticValueEntered;
        $(".tic-cash-adjusted").html(totalTicMoneyCharged * 1);
        if(ticValueEntered <0){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#tic-higher-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(totalTicCartCharged < ticValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#tic-lesser-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(!ticValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#tic-no-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(Math.ceil(totalTicCartCharged/2) > ticValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#tic-half-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(Math.ceil(totalTicCartCharged/2) <= ticValueEntered){
            if(ticValueEntered > $("#ticValue").text() ) {
                $(".tic-room-redemption-error").addClass('d-none');
                $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
            }
            else {
                $(".tic-room-redemption-error").addClass('d-none');
                $(".pay-points-plus-cash").prop("disabled",false).css("opacity","1");
            }
        }else if(Math.ceil(totalTicMoneyCharged) === 0) {
            $(".tic-room-redemption-error").addClass('d-none');
            $("#tic-zero-cash").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }
        else{
            $(".tic-room-redemption-error").addClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",false).css("opacity","1");
        }

    });

    $( "#redeemEpicureInput" ).bind("keyup change click", function() {
        var epicureValueEntered = Math.ceil($("#redeemEpicureInput").val());
        $(".entered-epicure").html(epicureValueEntered);
        totalEpicureMoneyCharged = totalEpicureCartCharged - epicureValueEntered;
        $(".epicure-cash-adjusted").html(totalEpicureMoneyCharged * 10);

        if(epicureValueEntered <0){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#epicure-higher-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(totalEpicureCartCharged < epicureValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#epicure-lesser-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(!epicureValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#epicure-no-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(Math.ceil(totalEpicureCartCharged/2) > epicureValueEntered){
            $(".tic-room-redemption-error").addClass('d-none');
            $("#epicure-half-points").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else if(Math.ceil(totalEpicureCartCharged/2) <= epicureValueEntered){
            if(epicureValueEntered > $("#epicureValue").text() ) {
                $(".tic-room-redemption-error").addClass('d-none');
                $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
            }
            else {
                $(".tic-room-redemption-error").addClass('d-none');
                $(".pay-points-plus-cash").prop("disabled",true).css("opacity","1");
            }
        }else if(Math.ceil(totalEpicureMoneyCharged) === 0) {
            $(".tic-room-redemption-error").addClass('d-none');
            $("#epicure-zero-cash").removeClass('d-none');
            $(".pay-points-plus-cash").prop("disabled",true).css("opacity","0.5");
        }else{
            $(".tic-room-redemption-error").addClass('d-none');
            $(".pay-points-plus-cash").prop('disabled',false).css("opacity","1");
        }

    });

    $( ".quantity-left-minus , .quantity-right-plus" ).bind("click", function() {
        var minusPlus = $(this);
        var className = minusPlus.attr('class');
        var id = minusPlus.closest('.card-input').find("input:first").attr('id');
        if(id === 'redeemEpicureInput' || id === 'redeemTicInput'){
            var valueDom = minusPlus.closest('.card-input').find("input.bas-quantity");
            var boxValue = parseInt($(valueDom).val(), 10);
            if(className.indexOf('quantity-left-minus') != -1){
                valueDom.val(boxValue - 1).change();
            }else if(className.indexOf('quantity-right-plus') != -1){
                valueDom.val(boxValue + 1).change();
            }
        }
    });

}


function ticPaymentFlow(button){

    // [TIC-FLOW]
    var memberType=getUserData();
    if(memberType && memberType.card && memberType.card.type.includes("TIC")){
        if(button.className.indexOf('pay-points-plus-cash') != -1){
            if (!$('.points-plus-cash-t-and-c').is(":checked")){
                $('.points-plus-cash-t-and-c-error').removeClass('d-none');
                return false;
            }else{
                $('.points-plus-cash-t-and-c-error').addClass('d-none');
            }
        }
    }

    return true;
}

// [TIC-FLOWs]
function ticPaymentDetailsFlow(){
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    if(ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow){
        $(".cm-page-container").addClass('tic-room-redemption-fix');
    }

    $(".Terms-cond-head-wrapper").click(function(){
        $(this).siblings(".terms-desc").toggle();
        $(this).find('.image-view').toggleClass("terms-drop-rotate");
    });

}

function checkMemberTypeforPaymentOptions(){
    var userDetails=getUserData();
    if(userDetails && userDetails.card){
        var membertype=userDetails.card.type;
        if(membertype.includes("TIC")){
            $( "#tap-redeem" ).remove();
            $( "#tappme-redeem" ).remove();
        }else if(membertype === "TAP"){
            $( "#tic-redeem" ).remove();
            $( "#tappme-redeem" ).remove();
        }else if(membertype === "TAPPMe"){
            $( "#tic-redeem" ).remove();
            $( "#tap-redeem" ).remove();
        }else{
            console.log("Unknown Member Type");
        }
    }
}


function checkPointsPlusCachedClick(){    
    $('input[type=radio][name=redeem-with-point-plus-cash]').change( function() {
        var ticFlowSelectedRadioId = $(this).attr('id');
        if(ticFlowSelectedRadioId === 'redeem-with-tic-point-plus-cash'){
            $(".redeem-with-tic-point-plus-cash-info-bar").removeClass('d-none');
            $(".redeem-with-epicure-point-plus-cash-info-bar").addClass('d-none');
        }else if(ticFlowSelectedRadioId === 'redeem-with-epciure-point-plus-cash'){
            $(".redeem-with-tic-point-plus-cash-info-bar").addClass('d-none');
            $(".redeem-with-epicure-point-plus-cash-info-bar").removeClass('d-none');
        }
    });
}


// Hide all other payment methods except pay at hotel
function hidePaymentVoucherRedemption() {
    $('.redeem-checkbox-wrp').hide();
    $('#PAYONLINENOW').closest('.ihclcb-payment-opt-btn').hide();
    $('#PAYATHOTEL').prop('checked', 'true');
    $('.payment-radio-content').addClass('cm-hide');
    $('.credit-card-payment-wrp').removeClass('cm-hide');
}