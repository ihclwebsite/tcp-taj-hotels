$(document).on("change", function (e) {
    handleServiceAmenitySelectionChanged(e);

    function handleServiceAmenitySelectionChanged(e) {
        var target = e.target;
        if (target != undefined) {
            var tagName = target.tagName;
            if (tagName != undefined && tagName == 'CORAL-SELECT') {
                handleChangedSelection(target);
            }
        }
    }

    function handleChangedSelection(coralSelect) {
        var selectName = $(coralSelect).data('name');
        var selectedItem = $(coralSelect.selectedItem);
        var selectedItemName = $(selectedItem).data('name');
        if (selectedItemName != undefined) {
        	var updatedName = selectName + '/./' + selectedItemName;
        } else {
			var updatedName = selectName;
        }
        coralSelect.name = updatedName;
    }
});
