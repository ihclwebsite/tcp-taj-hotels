window.addEventListener('load', function() {

    $(document).ready(function() {
        var clickElement = $('.sitemap-category-heading');
        var expandBtnGeneral = $('.sitemap-category-expand-btn');
        var expansionGeneral = $('.sitemap-category-details');
        clickElement.each(function() {
            $(this).click(function() {

                var expandBtn = $(this).children();
                var expansion = $(this).siblings('.sitemap-category-details');
                if (expandBtn.html() == '+') {
                    expandBtnGeneral.html('+');
                    expandBtn.html('-');
                    expansionGeneral.removeClass('visible');
                    expansion.addClass('visible');
                } else {
                    expandBtn.html('+');
                    expansion.removeClass('visible');
                }
            })
        })
    });
});
