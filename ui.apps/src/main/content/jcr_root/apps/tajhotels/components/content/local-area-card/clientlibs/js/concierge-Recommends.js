window.addEventListener('load', function() {
    $('.showMap-close').click(function() {
     	$('.cm-page-container').removeClass("prevent-page-scroll");
        $('.show-popUp-local-place-details').hide();
        $('.cm-local-details-container').hide();
    });
    var check = false;
    setTimeout(function() {
        var recomCrds=$('.concierge-Recommends');
        recomCrds.each(function(){
            $(this).showMore();
        });
        $('.ho-description-txt').each(function() {
            $(this).cmToggleText({
                charLimit : 200,
            })
        });
    }, 100);
    
    $('.local-details-close').click(function() {
		$('.cm-page-container').removeClass("prevent-page-scroll");
        $('.cm-local-details-container').hide();        

        if (check==true) {
            var parent = $(this).closest('.concierge-local');
			parent.css('display','none');
    	}
    });

    $(document).keydown(function(e) {
        if (e.which == 27) {
            $('.local-details-close').trigger('click');
        }  
    });
});

function myFunction(obj) {
    var parent = $(obj).data("con-name");
 	$('.cm-page-container').addClass("prevent-page-scroll");
    $(".cm-page-container").addClass("prevent-page-scroll"); 

    if ($('.specific-city-showMap-popup').css('display')=="block") {
        if ($("#"+parent).parent(".concierge-local").css('display')=="block") {
            check = false;

        }
        else {
            $("#"+parent).parent(".concierge-local").css('display','block');
			check = true;
        }
    }
    $('#' + parent).children('.show-popUp-local-place-details').show();
    $('#' + parent).children('.cm-local-details-container').show();
}
