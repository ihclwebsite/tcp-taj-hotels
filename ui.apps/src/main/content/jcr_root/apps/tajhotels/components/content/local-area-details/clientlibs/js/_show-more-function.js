(function($) {
    $.fn.showMore = function() {
        var element = this;
        var count = element.children().children().length;

        var counter = 1;
        if ($(window).width() > 991) {
            showMoreBasic(3);
        } else if ($(window).width() > 767 && $(window).width() < 992) {
            showMoreBasic(2);
        } else if ($(window).width() < 768) {
            showMoreBasic(1);
        }

        function showMoreBasic(num) {
           // alert("inside 17");
            for (i = 0; i < num; i++) {

                $(element.children().children().eq(i)).css('display', 'block');
            }

            if (count > num) {

                element.parent().append('<div class="jiva-spa-show-more">SHOW MORE<img src="/apps/tajhotels/clientlibs/taj-hotels-site/components/resources/images/drop-down-arrow.svg" alt = "drop-down-arrow-icon"/></div>')
            }

            var limit = Math.ceil(count / num);

            (element.siblings('.jiva-spa-show-more')).on('click', function() {
                if ($(this).children().hasClass('show-more-inverted')) {
                    for (i = num; i <= count; i++) {
                        $(element.children().children().eq(i)).css('display', 'none');
                    }
                    $(this).html('SHOW MORE<img src="/apps/tajhotels/clientlibs/taj-hotels-site/components/resources/images/drop-down-arrow.svg" alt  = "drop-down-arrow-icon"/>')
                } else {

                    if (counter < limit) {
                        for (i = (counter * num); i < ((counter + 1) * num); i++) {
                            $(element.children().children().eq(i)).css('display', 'block');
                        }
                        if (counter == (limit - 1)) {
                            $(this).html('SHOW LESS<img class="show-more-inverted" src="/apps/tajhotels/clientlibs/taj-hotels-site/components/resources/images/drop-down-arrow.svg" alt  = "drop-down-arrow-icon"/>')
                            counter = 0;
                        }
                    }
                    counter++;
                }
            })

        }

    }
}(jQuery));
