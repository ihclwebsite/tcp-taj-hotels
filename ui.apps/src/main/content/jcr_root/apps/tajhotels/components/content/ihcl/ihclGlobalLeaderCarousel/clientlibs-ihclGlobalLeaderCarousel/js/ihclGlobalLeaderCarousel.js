$(document).ready(function() {
    ihclImageCarousel();
});

function ihclImageCarousel() {

    $.fn.extend({
        scrollRight : function(val) {
            if (val === undefined) {
                return this[0].scrollWidth - (this[0].scrollLeft + this[0].clientWidth);
            }
            return this.scrollLeft(this[0].scrollWidth - this[0].clientWidth - val);
        }
    });

    // $('.ihcl-image-carousel-wrp').customSlide(7);

    if (deviceDetector.checkDevice() == "small") {

        var $container = $('.ihcl-image-carousel-wrp-legacy');
        var $innerContainer = $('.ihcl-image-carousel-wrp-legacy .clearfix');
        var oneCardWidth = $(".ihcl-image-carousel-wrp-legacy").width();
        var maxScroll = $innerContainer.width();
        var $carouselCnt = $('.ihcl-image-carousel-container')
        $(".mr-global-leaders-wrapper").each(function() {
            $(this).css("min-width", oneCardWidth - 16 + "px");
        });
        $innerContainer.width(oneCardWidth * $(".mr-global-leaders-wrapper").length);
        $carouselCnt
                .prepend('<div class="leftArrowMob" style="display:none"><span class="icon-carousel-arrow-coloured-left"></span></div>');
        $carouselCnt
                .append('<div class="rightArrowMob"><span class="icon-carousel-arrow-coloured-right"></span></div>');

        var $rightArrowMob = $('.rightArrowMob');
        var $leftArrowMob = $('.leftArrowMob');

        $rightArrowMob.click(function() {
            var presentScroll;
            $container.scrollLeft($container.scrollLeft() + oneCardWidth);
            presentScroll = $container.scrollRight();
            $leftArrowMob.css("display", "block");
            if (presentScroll < 10) {

                $(this).css("display", "none");
            }
        });

        $leftArrowMob.click(function() {
            var presentScroll;
            $container.scrollLeft($container.scrollLeft() - oneCardWidth);
            presentScroll = $container.scrollLeft();
            $rightArrowMob.css("display", "block");
            if (presentScroll <= 0) {
                $(this).css("display", "none");
            }
        });

    }

}
