var listContainer;
var mapConatiner;
var mobileCardContainer;

var allListCards;
var allMobileCards;
var allMapCards;

var isDestinationLandingPage = false;
var initialCardLoad = 3;
var showMoreCardOffset = 2;
var isAmaPropertyInOtherDomain = $('#isAmaPropertyInOtherDomain').val();

window.addEventListener('load', function() {
    var showMoreCardOffsetCount = $('[data-show-more-card-offset]').data('showMoreCardOffset');
    var initialCardLoadCount = $('[data-initial-card-load]').data('initialCardLoad');

    if (showMoreCardOffsetCount) {
        showMoreCardOffset = showMoreCardOffsetCount;
    }
    if (initialCardLoadCount) {
        initialCardLoad = initialCardLoadCount;
    }
    $('.room-rate-unavailable').hide();
    $('.mapHotels-rate').hide();
    $('.map-carousal-rate').hide();
    if ($('.cm-page-container').hasClass('ama-theme')) {
        invokeRoomFetchAjaxCall();
    } else {
        invokeRateFetcherAjaxCall();

        $('.mr-list-current-discounted-rates-wrap').hide();
    }
    listContainer = $('.mr-listsMapHotels-Main-Wrapper-others .mr-lists-view-hotels-Container');
    mapConatiner = $('.mr-listsMapHotels-Main-Wrapper .mr-map-view-hotels-Container');
    mobileCardContainer = $('.carousel-inner.card-carousel.mr-mapView-card-carousel');
    listContainer.parent().showMore(showMoreCardOffset, initialCardLoad);
    if (templateName == "tajhotels/components/structure/destination-landing-page") {
        isDestinationLandingPage = true;
    }
});

function invokeRoomFetchAjaxCall() {
    $('.waiting-spinner').hide();
    $('.room-rates-wrap.static-rates').show();
    var param = window.location.search;
    if (param) {
        var arr = param.split('?');
        var query = arr[1];
        if (query.includes('checkAvail')) {
            $('.waiting-spinner').show();
            $('.room-rates-wrap.static-rates').hide();
            var requestData = {};
            var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));
            var cacheJSONData = JSON.parse(cacheText);
            var checkInDate = fromDate(cacheJSONData.fromDate);
            var checkOutDate = toDate(cacheJSONData.toDate);
            var rooms = cacheJSONData.rooms;

            var rateFilters = [ "STD" ];
            var rateCodes = [];
            var hotelCode = $(".mr-lists-view-hotels-Container .list-view-wrapper").attr('data-hotelid');
            var shortCurrencyStringInDisplay = 'NAN';

            trigger_hotelSearch(getBookingOptionsSessionData());
            console.info('Attempting to invoke ajax call with data. ');

            ROOM_OCCUPANCY_RESPONSE = {};
            $.ajax({
                type : 'GET',
                url : '/bin/room-rates/rooms-availability.rates/' + hotelCode + '/' + checkInDate + '/' + checkOutDate
                        + '/' + shortCurrencyStringInDisplay + '/' + rooms + '/' + JSON.stringify(rateFilters) + '/'
                        + JSON.stringify(rateCodes) + '/ratesCache',
                dataType : 'json',
                data : requestData,
                error : handleFailureToFetchRates,
                success : successHandler
            });
        }
    }
}

function invokeAmaInOtherDomainFetchAjaxCall() {
    try {
        $('.waiting-spinner').hide();
        $('.room-rates-wrap.static-rates').show();

        $('.waiting-spinner').show();
        $('.room-rates-wrap.static-rates').hide();
        var requestData = {};
        var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));
        var cacheJSONData = JSON.parse(cacheText);
        var checkInDate = fromDate(cacheJSONData.fromDate);
        var checkOutDate = toDate(cacheJSONData.toDate);
        var rooms = cacheJSONData.rooms;

        var rateFilters = [ "STD" ];
        var rateCodes = [];
        var hotel = $('.row.mr-listView-layout-row').next().find('.mr--typehotel')

        var hotelCode = [];
        hotel.each(function() {
            hotelCode.push($(this).data('hotelid'));
        });
        hotelCode = jQuery.unique(hotelCode.sort(function(a, b) {
            return a - b;
        }));
        var shortCurrencyStringInDisplay = 'NAN';

        trigger_hotelSearch(getBookingOptionsSessionData());
        console.info('Attempting to invoke ajax call with data. ');

        ROOM_OCCUPANCY_RESPONSE = {};
        $.each(hotelCode, function(index, value) {
            $.ajax({
                type : 'GET',
                url : '/bin/room-rates/rooms-availability.rates/' + value + '/' + checkInDate + '/' + checkOutDate
                        + '/' + shortCurrencyStringInDisplay + '/' + rooms + '/' + JSON.stringify(rateFilters) + '/'
                        + JSON.stringify(rateCodes) + '/ratesCache',
                dataType : 'json',
                data : requestData,
                error : handleFailureToFetchRates,
                success : successAmaInOtherDomainHandler

            });
        });
    } catch (err) {
        console.log('caught exception inside invokeAmaInOtherDomainFetchAjaxCall in destination-landing js');
        console.log(err);
    }

}

function successAmaInOtherDomainHandler(response, isError) {
    var $responseHotelCodes = response[1].rateFilters.STD;
    var $hotelElems = $('.mr-lists-view-hotels-Container .mr--typehotel');
    var $hotelMapElems = $('.mr-map-view-hotelsLists-wrap.mr--typehotel');
    $hotelElems.each(function() {
        var structureHotelElem = $(this);
        // var structureHotelMapElem = $(this);
        $.each($responseHotelCodes, function(key, value) {
            var responseHotelElem = $(this);
            if (structureHotelElem[0].dataset.hotelid == responseHotelElem[0].hotelCode) {
                var $roomsAvailabilityElems = responseHotelElem[0].roomsAvailability;
                $.each($roomsAvailabilityElems, function(key, value) {
                    if (key == structureHotelElem[0].dataset.hotelroomcode) {
                        var roomPrice = $(this)[0].lowestPrice;
                        structureHotelElem.find('.mr-list-current-discounted-rates-wrap').css('display', 'block');
                        structureHotelElem.find('.waiting-spinner.waiting-spinner-destination-cards').css('display',
                                'none');
                        structureHotelElem.find('.mr-list-current-discounted-rates-wrap').html(
                                '<div class="room-rates-wrap"><span class="mr-rupee-list-hotel">₹</span><span class="current-rate-ama">'
                                        + roomPrice + '<span class="current-rate-asterisk">*</span></span></div>');
                        structureHotelElem.attr("data-price", roomPrice);
                    }

                })
                structureHotelElem.find('.waiting-spinner.waiting-spinner-destination-cards').css('display', 'none');

            }
        });
    })

    $hotelMapElems.each(function() {
        var structureHotelMapElem = $(this);
        $.each($responseHotelCodes, function(key, value) {
            var responseHotelElem = $(this);
            if (structureHotelMapElem[0].dataset.hotelid == responseHotelElem[0].hotelCode) {
                var $roomsAvailabilityElems = responseHotelElem[0].roomsAvailability;
                $.each($roomsAvailabilityElems, function(key, value) {
                    if (key == structureHotelMapElem[0].dataset.hotelroomcode) {
                        var roomPrice = $(this)[0].lowestPrice;

                        structureHotelMapElem.attr("data-price", roomPrice);
                        structureHotelMapElem.find('.map-card-current-rate').html(roomPrice);
                    }

                })

            }
        });
    })

    $('.waiting-spinner').hide();

    console.info('Ajax call for servlet succeeded');
}
function fromDate(fromDate) {
    var checkInDate = moment(fromDate, 'MMM Do YY').format('YYYY-MM-DD');
    return checkInDate
}

function toDate(toDate) {
    var checkOutDate = moment(toDate, 'MMM Do YY').format('YYYY-MM-DD');
    return checkOutDate
}

function successHandler(response, isError) {
    var $responseHotelCodes = response[1].rateFilters.STD;
    var $hotelElems = $('.mr-lists-view-hotels-Container .mr--typehotel');
    var amaTheme = $('div').hasClass('ama-theme');
    $hotelElems
            .each(function() {
                var structureHotelElem = $(this);
                $
                        .each(
                                $responseHotelCodes,
                                function(key, value) {
                                    var responseHotelElem = $(this);
                                    if (structureHotelElem[0].dataset.hotelid == responseHotelElem[0].hotelCode) {
                                        var $roomsAvailabilityElems = responseHotelElem[0].roomsAvailability;
                                        $
                                                .each(
                                                        $roomsAvailabilityElems,
                                                        function(key, value) {
                                                            if (key == structureHotelElem[0].dataset.hotelroomcode) {
                                                                var roomPrice = $(this)[0].lowestPrice;
                                                                structureHotelElem.find(
                                                                        '.mr-list-current-discounted-rates-wrap').css(
                                                                        'display', 'block');
                                                                structureHotelElem
                                                                        .find(
                                                                                '.waiting-spinner.waiting-spinner-destination-cards')
                                                                        .css('display', 'none');
                                                                structureHotelElem
                                                                        .find('.mr-list-current-discounted-rates-wrap')
                                                                        .html(
                                                                                '<div class="room-rates-wrap"><span class="mr-rupee-list-hotel ama-rupee-symbol">₹</span> '
                                                                                        + roomPrice
                                                                                        + '<span class="room-rate-per-night-text"> Per Night</span></div>');
                                                                structureHotelElem.attr("data-price", roomPrice);
                                                            }

                                                        })
                                        var loadingSpinner = structureHotelElem.find('.waiting-spinner').css('display');
                                        if (amaTheme && loadingSpinner == 'block') {
                                            structureHotelElem.find('.card-wrapper').addClass('soldOut')
                                            structureHotelElem
                                                    .find('.card-wrapper-descritpion')
                                                    .prepend(
                                                            "<div class='soldOut-text'>Please check for alternate dates and occupancy combinations.</div>")
                                            structureHotelElem.find('.mr-list-current-discounted-rates-wrap').css(
                                                    'display', 'block');
                                            structureHotelElem.find(
                                                    '.waiting-spinner.waiting-spinner-destination-cards').css(
                                                    'display', 'none');
                                            structureHotelElem.find('.mr-list-current-discounted-rates-wrap').html(
                                                    '<div class="room-rates-wrap"><span> Sold Out</span></div>');
                                        }
                                    }
                                });
            })
    // Analytics Data Call For AMA
    if (amaTheme) {
        var availHotelCount = '';
        var HotelList = [];
        var hotelsList = $('.mr-lists-view-hotels-Container .list-view-wrapper');
        $.each(hotelsList, function(index, value) {
            var hotelCardObj = {}
            try {
                hotelCardObj.Price = $(this).find(".room-rates-wrap").text().match(/\d+/)[0];
                availHotelCount++;
            } catch (e) {
            }
            HotelList.push(hotelCardObj);
        });
        try {
            amaDestinationLayerData.AvailHotels = availHotelCount;
            amaDestinationLayerData.HotelList.forEach(function(index, item) {
                index.Price = HotelList[item]["Price"];

            });
        } catch (e) {
        }
    }
    console.info('Ajax call for servlet succeeded');
}
function handleFailureToFetchRates(response) {
    console.error("Ajax call was successful, but the server returned a failure for room rates.");
}
function getCardsAfterAjax() {
    allMobileCards = mobileCardContainer.find('.mr-mapCarousel').clone(true);
    allMapCards = mapConatiner.find('.mr-map-view-hotelsLists-wrap').clone(true);
    allListCards = listContainer.find('.list-view-wrapper').clone(true);

}

$(document).on('currency:changed', function(e, currency) {
    var popupParams = {
        title : 'Booking Alert!',
        description : 'Showing currency is Hotel Default currency. Now You will not be  allowed to change Currency. ',
    }
    warningBox(popupParams);
});

function invokeRateFetcherAjaxCall() {

    var hotelID = [];
    $('[data-hotelid]').each(function() {
        hotelID.push($(this).attr("data-hotelid"));
    })

    var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));
    var cacheJSONData = JSON.parse(cacheText);
    var checkInDate = cacheJSONData.fromDate;
    var checkOutDate = cacheJSONData.toDate;
    var rooms = cacheJSONData.rooms;
    var selectionCount = cacheJSONData.selectionCount;
    var roomCount = cacheJSONData.roomCount;
    var selection = (cacheJSONData.selection.length <= 0) ? cacheJSONData.roomOptions : cacheJSONData.selection;
    var hotelId = cacheJSONData.hotelCode;
    var roomDetails = [];

    roomDetails.push(selection[0].adults);
    roomDetails.push(selection[0].children)

    checkInDate = moment(checkInDate, "MMM Do YY").format("YYYY-MM-DD");
    checkOutDate = moment(checkOutDate, "MMM Do YY").format("YYYY-MM-DD");
    var websiteId = $('#siteId').val();
    var destinationTag = $('#destinationTag').val();
    while (destinationTag.includes('/')) {
        destinationTag = destinationTag.replace('/', '+')
    }
    // [IHCl_CB START]
    var rateFilter = "";
    var corporateCode = "";
    var ihclCbUserDetails = dataCache.local.getData("userDetails");
    if (ihclCbUserDetails && (ihclCbUserDetails.selectedEntity || ihclCbUserDetails.selectedEntityAgent)) {
        if (ihclCbUserDetails.selectedEntity && ihclCbUserDetails.selectedEntity.partyNumber) {
            corporateCode = ihclCbUserDetails.selectedEntity.partyNumber;
        } else {
            corporateCode = ihclCbUserDetails.selectedEntityAgent.partyNumber;
        }
    } else {
        rateFilter = "STD";
    }
    // [IHCl_CB END]

    if ($('#synxis-downtime-check').val() == "true") {
        $('.waiting-spinner-destination-cards').hide();
        $('.mr-map-rating-wrap').hide();
        $(".map-last-few-rooms").hide();
        $('.waiting-spinner').hide();
        $('.map-carousel-waiting-spinner').hide();
        $('.map-card-waiting-spinner').hide();
        $(".mr-starting-rate").html("");
    } else {
        $.ajax({
            type : 'GET',
            url : '/bin/hotel-rates/destination-hotel-rates.rates/' + websiteId + '/' + destinationTag + '/'
                    + checkInDate + '/' + checkOutDate + '/' + roomDetails + '/' + rateFilter + '/' + corporateCode
                    + '/destinationRatesCache',
            dataType : 'json',
            data : "",
            success : function(response) {
                var successResponse = JSON.stringify(response.responseCode);
                var successMessage = successResponse.substring(1, successResponse.length - 1);
                if (successMessage == "SUCCESS") {
                    // var hotelDetailsList = response.hotelDetails;
                    $(".mr-starting-rate").html("Rates Currently Not Available");
                    setRatesForHotel(response);
                    getCardsAfterAjax();
                    setUpSessionData();
                    pushEvent("hotels", "hotellist", prepareGlobalHotelListJS())
                } else {
                    $('.waiting-spinner').hide();
                    $('.map-carousel-waiting-spinner').hide();
                    $('.map-card-waiting-spinner').hide();
                    $(".mr-starting-rate").html("Rates Currently Not Available");
                    var warningPopupParams = {
                        title : 'Availability Failed!',
                        description : response.message,
                    }
                    warningBox(warningPopupParams);
                }
            },
            error : function(error) {
                console.log("Failed to get rate for availability" + error)
                pushEvent("hotels_price_unavailable", "hotels_price_unavailable", prepareGlobalHotelListJS())
                $('.waiting-spinner-destination-cards').hide();
                $('.mr-map-rating-wrap').hide();
                $(".map-last-few-rooms").hide();
                $('.waiting-spinner').hide();
                $('.map-carousel-waiting-spinner').hide();
                $('.map-card-waiting-spinner').hide();
                $(".mr-starting-rate").html("Rates Currently Not Available");
                if (error && error.status == 412 && error.responseText) {
                    var warningPopupParams = {
                        title : 'Availability Failed!',
                        description : error.responseText,
                    }
                    warningBox(warningPopupParams);
                }
                setUpSessionData();
            },
            complete : function() {
                if (isAmaPropertyInOtherDomain) {
                    invokeAmaInOtherDomainFetchAjaxCall();
                }
            }

        });
    }
}

var curCheck;

function setRatesForHotel(response) {

    var currencyData;
    var check = true;
    hotelDetailsList = JSON.parse(response.hotelDetails);
    for (i = 0; i < hotelDetailsList.length; i++) {
        currencyData = hotelDetailsList[i].currencyCode;
        if (currencyData && currencyData.currencyString && check) {
            curCheck = setActiveCurrencyWithResponseValue(currencyData.currencyString);
            check = false;
        }
        enablePriceViewforRoomsWithRate(hotelDetailsList[i]);
        enablePriceViewforRoomsWithRateMapCard(hotelDetailsList[i]);
        enablePriceViewforMapCarouselCard(hotelDetailsList[i]);
    }

    $('.waiting-spinner').hide();
    $('.map-carousel-waiting-spinner').hide();
    $('.map-card-waiting-spinner').hide();
}

function enablePriceViewforRoomsWithRate(hotelDetail) {

    // TIC FLOW
    var userDetails = getUserData();

    var cachecCurrency;
    if (curCheck) {
        cachecCurrency = getCurrencyCache().currencySymbol.trim();
    } else if (hotelDetail && hotelDetail.currencyCode) {
        cachecCurrency = hotelDetail.currencyCode.currencyString;
    }
    var currentHotelRef = $("[data-hotelid='" + hotelDetail.hotelCode + "']");
    var hotelDetailsRate = currentHotelRef.find(".mr-starting-rate");
    var bookingButtonContainer = currentHotelRef.find(".mr-list-current-discounted-rates-wrap");
    var currentRate = bookingButtonContainer.find(".current-rate");
    var currentSymbol = bookingButtonContainer.find(".mr-rupee-list-hotel");
    var currentNotAvailWrap = bookingButtonContainer.siblings('.mr-rates-not-available-wrap');
    var priceNotAvailable = currentHotelRef.find(".room-rate-unavailable");
    var lowestPrice = hotelDetail.lowestPrice;
    var discountedPrice = hotelDetail.lowestDiscountedPrice;
    bookingButtonContainer.show();
    currentNotAvailWrap.hide();
    hotelDetailsRate.html("Starting Rate/Night");
    priceNotAvailable.hide();
    currentSymbol.html(cachecCurrency);

    if (lowestPrice == 0) {
        priceNotAvailable.show();
        bookingButtonContainer.find('.mr-list-hotels-delrate').hide();
        bookingButtonContainer.find('.mr-current-rate-list').hide();
        bookingButtonContainer.find('.mr-current-tic-epicure').hide();
        hotelDetailsRate.hide();
    } else if (discountedPrice == 0 || lowestPrice == discountedPrice) {
        if (userDetails && userDetails.card && userDetails.card.tier
                && dataCache.session.getData("bookingOptions").isTicRoomRedemptionFlow) {
            bookingButtonContainer.find('.mr-current-rate-list').hide();
            bookingButtonContainer.find('.mr-current-tic-epicure').hide();
            bookingButtonContainer.find('.mr-list-hotels-delrate').hide();
            var currentRateList = bookingButtonContainer.find('.mr-current-tic-epicure');
            currentRateList.find('.tic-points').html(Math.ceil(lowestPrice));
            currentRateList.find('.epicure-points').html(Math.ceil(lowestPrice / 2));
        } else {
            currentRate.html(getCommaFormattedNumber(lowestPrice));
            bookingButtonContainer.find('.mr-list-hotels-delrate').hide();
            bookingButtonContainer.find('.mr-current-tic-epicure').hide();
            currentHotelRef.attr("data-price", lowestPrice);
        }
    } else {
        currentRate.html(getCommaFormattedNumber(discountedPrice));
        bookingButtonContainer.find('.mr-current-tic-epicure').hide();
        discountedRoomRef = bookingButtonContainer.find(".discounted-rate");
        bookingButtonContainer.find(".discounted-rate").html(cachecCurrency);
        discountedRoomRef.html(discountedPrice);
        currentHotelRef.attr("data-price", lowestPrice);
    }

}

function enablePriceViewforRoomsWithRateMapCard(hotelDetail) {
    var cacheCurrencyData;
    if (curCheck) {
        cacheCurrencyData = getCurrencyCache().currencySymbol.trim();
    } else if (hotelDetail && hotelDetail.currencyCode) {
        cacheCurrencyData = hotelDetail.currencyCode.currencyString;
    }
    var mr_map_view_hotels_Container = $(".mr-map-view-hotels-Container");
    var currentHotelRef = mr_map_view_hotels_Container.find("[data-hotelid='" + hotelDetail.hotelCode + "']");
    var hotelDetailsRate = currentHotelRef.find(".mapHotels-rateperNightText");
    var bookingButtonContainer = currentHotelRef.find(".mapHotels-rate");
    var currentRate = bookingButtonContainer.find(".map-card-current-rate");
    var currencySybol = bookingButtonContainer.find(".newRate-rupee");
    var currentNotAvailWrap = bookingButtonContainer.siblings('.mr-rates-not-available-wrap');
    var lowestPrice = hotelDetail.lowestPrice;
    var discountedPrice = hotelDetail.lowestDiscountedPrice;
    bookingButtonContainer.show();
    currentNotAvailWrap.hide();
    hotelDetailsRate.html("Starting Rate/Night");
    currencySybol.html(cacheCurrencyData);
    bookingButtonContainer.find(".mr-mapRupee").html(cacheCurrencyData);

    if (lowestPrice == 0) {
        hotelDetailsRate.hide();
        bookingButtonContainer.find(".mr-map-Newrate").hide();
        bookingButtonContainer.find(".mapHotels-oldRate").hide()
    } else if (discountedPrice == 0 || lowestPrice == discountedPrice) {
        currentRate.html(getCommaFormattedNumber(lowestPrice));
        bookingButtonContainer.find('.mapHotels-oldRate').hide();
    } else {
        currentRate.html(getCommaFormattedNumber(discountedPrice));
        discountedRoomRef = bookingButtonContainer.find(".map-card-discounted-rate");
        discountedRoomRef.html(discountedPrice);
    }

}

function enablePriceViewforMapCarouselCard(hotelDetail) {
    var cacheCurrencyData;
    if (curCheck) {
        cacheCurrencyData = getCurrencyCache().currencySymbol.trim();
    } else if (hotelDetail && hotelDetail.currencyCode) {
        cacheCurrencyData = hotelDetail.currencyCode.currencyString;
    }
    var mr_mapView_card_carousel = $(".mr-mapView-card-carousel");
    var currentHotelRef = mr_mapView_card_carousel.find("[data-hotelid='" + hotelDetail.hotelCode + "']");
    var hotelDetailsRate = currentHotelRef.find(".hotelDetailsRate");
    var map_carousal_rate = currentHotelRef.find(".map-carousal-rate");
    var bookingButtonContainer = currentHotelRef.find(".bookingButtonContainer");
    var currentRate = bookingButtonContainer.find(".current-rate");
    var currencySybol = bookingButtonContainer.find(".rate-currency-symbol");
    var priceNotAvailable = bookingButtonContainer.find(".room-rate-unavailable");

    var lowestPrice = hotelDetail.lowestTotalPrice;
    var discountedPrice = hotelDetail.lowestDiscountedPrice;
    bookingButtonContainer.show();
    map_carousal_rate.show();
    // hotelDetailsRate.html("Starting Rate/Night");
    currencySybol.html(cacheCurrencyData);
    // bookingButtonContainer.find(".mr-mapRupee").html(cacheCurrencyData);
    if (lowestPrice == 0) {
        priceNotAvailable.show();
        bookingButtonContainer.find('.mr-list-hotels-delrate').hide();
        bookingButtonContainer.find('.mr-current-rate-list').hide();
        bookingButtonContainer.find('.mr-current-tic-epicure').hide();
        hotelDetailsRate.hide();
    }
    if (discountedPrice == 0) {
        currentRate.html(getCommaFormattedNumber(lowestPrice));
        bookingButtonContainer.find('.mr-list-hotels-delrate').hide();
    } else {
        currentRate.html(getCommaFormattedNumber(discountedPrice));
        discountedRoomRef = bookingButtonContainer.find(".map-card-discounted-rate");
        discountedRoomRef.html(discountedPrice);
    }

}

function getBookingOptionsSessionData() {
    return dataCache.session.getData("bookingOptions");
}

function setCurrencyInSessionStorage(currency) {
    var bookingOptions = getBookingOptionsSessionData();
    bookingOptions.currencySelected = currency;
    dataCache.session.setData("bookingOptions", bookingOptions);
}

function getCommaFormattedNumber(number) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number.toFixed();

    } else {
        formattedNumber = number.toFixed().toLocaleString('en-IN')
    }
    return formattedNumber;
}

function replaceRoomsLink(hotelsLinkArr) {
    var ROOMS_SUITES_SUFFIX = "/rooms-and-suites";
    $(hotelsLinkArr).each(function() {
        var linkExists = $(this).find(" >a").attr("href");

        console.log("Link found : ", linkExists);
        if (linkExists != "" && linkExists != undefined && linkExists != null) {
            linkExists = linkExists.replace(".html", "");
            var linkReplaced = linkExists + ROOMS_SUITES_SUFFIX + ".html";
            linkReplaced = linkReplaced.replace("//", "/");

            console.log("Link replaced : ", linkReplaced)

            $(this).find(" >a").attr("href", linkReplaced);
        }
    })
}
