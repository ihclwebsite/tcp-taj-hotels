$( document ).ready( function() {
    // membership login popup function
	alert("BookingDetails - JavaScript");
    $( '.membership-signin' ).click( function( e ) {
        $( '.membership-signin-popup' ).show();
    } );

    // closing popup on clicking overlay
    $( '.membership-sigin-overlay, .checkout-signin-close-icon' ).click( function( e ) {
        e.stopPropagation();
        $( '.membership-signin-popup' ).hide();
    } );

    // preventing propagation within signin form
    $( '.checkout-member-signin-container' ).click( function( e ) {
        e.stopPropagation();
    } );

    // booking for someone else checkbox handler

    $( '.booking-others-checkbox' ).change( function() {
        if ( this.checked ) {
            $( '.biller-details-wrp' ).show();
        } else {
            $( '.biller-details-wrp' ).hide();
        }
    } );

    // validateEnterDetailsElements
    var validateEnterDetailsElements = function() {
        if ( $( '.booking-others-checkbox' )[ 0 ].checked ) {
            $( '.sub-form-mandatory' ).each( function() {
                if ( $( this ).val() == "" ) {
                    $( this ).addClass( 'invalid-input' );
                }
            } );
        } else {
            $('.guest-details-wrp .sub-form-mandatory:visible').each( function() {
                if ( $( this ).val() == "" ) {
                    $( this ).addClass('invalid-input');
                }
            } );
        }
    }


    // proceed button validation
    $( '.proceed-payment-button' ).click( function() {
        validateEnterDetailsElements();
        if ( $( '.sub-form-input-element:visible' ).hasClass( 'invalid-input' ) ) {
            $( '.invalid-input' ).first().focus();
            console.log($('#guest-VoucherCode').val().length());
            console.log($('#guest-VoucherPin').val().length());
            var billerDetailsHasInvalidInput = ( $( '.biller-details-wrp .sub-form-input-element:visible' ).hasClass( 'invalid-input' ) );
            if ( billerDetailsHasInvalidInput == false ) {
                if ( ( $( '#bookingGuestTitle' )[ 0 ].value == "" ) ) {
                    $( '.selectboxit' ).trigger( 'click' );
                }
            }
        } else {
            $( '.booking-details-wrp' ).hide();
            $( '.payment-details-wrp' ).show();
            $( '.checkout-form-title' ).text( 'Payment details' );
            $( '.form-step' ).toggleClass( 'active-form-step' )
        }

    } );

    // redeem points checkbox handler

    $( '.redeem-points-checkbox' ).change( function() {
        if ( this.checked ) {
            $( '.redeem-checkbox-wrp' ).addClass( 'redeem-points-checked' );
        } else {
            $( '.redeem-checkbox-wrp' ).removeClass( 'redeem-points-checked' );
        }
    } );
} );

// Booking guest dropdown
var options = {};
var $bookingGuestTitle = $( '#bookingGuestTitle' );
var $redeemTicDropdown = $( '#redeemTicDropdown' );
var dropDowns = {
    /*
     * titles: { options: [ 'Mr.', 'Ms.', 'Mrs.' ], elem: $bookingGuestTitle, default: '', selected: null, dependent: {
     * elem: null } },
     */
    tic: {
        options: [ 'TIC plus Credit Card', 'Epicure plus Credit Card' ],
        elem: $redeemTicDropdown,
        default: '',
        selected: null,
        dependent: {
            elem: null
        }
    }
}

initDropdown.prototype.initialize = function() {
    var itrLength = this.options.length;
    for ( i = 0; i < itrLength; i++ ) {
        this.targetElem.append( '<option>' + this.options[ i ] + '</option>' )
    };

    // Initializes selectboxit on the rendered dropdown
    this.targetElem.selectBoxIt();

    // Add listeners to each selectbox change
    this.listenDDChange();
};
initDropdown.prototype.listenDDChange = function() {
    var dTarget = this.targetBlock;
    dTarget.elem.change( function() {
        var selectedOption = ( dTarget.elem ).find( "option:selected" );
        dTarget.selected = selectedOption.text();
        if ( dTarget.dependent.elem ) {
            dTarget.dependent.elem.selectBoxIt( 'selectOption', 0 );
            if ( dTarget.selected != dTarget.default ) {
                dTarget.dependent.elem.prop( "disabled", false );
            } else {
                dTarget.dependent.elem.prop( "disabled", true );
            }
        } else {
            if ( dTarget.selected != dTarget.default ) {
                // "dropDowns" Gives the selected values for each dropdown options
            }
        }
    } );
};

function initDropdown( element, target ) {
    this.targetBlock = target;
    this.options = target.options;
    this.targetElem = element;
};

function initbookingGuestTitle() {

    var bookingGuestTitle = new initDropdown( $bookingGuestTitle, dropDowns.titles );
    var redeemTicDropdown = new initDropdown( $redeemTicDropdown, dropDowns.tic );

    bookingGuestTitle.initialize();
    redeemTicDropdown.initialize();
    

};

initbookingGuestTitle();
// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jaGVja291dC1lbnRlci1kZXRhaWxzL2NoZWNrb3V0LWVudGVyLWRldGFpbHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJCggZG9jdW1lbnQgKS5yZWFkeSggZnVuY3Rpb24oKSB7XHJcbiAgICAvLyBtZW1iZXJzaGlwIGxvZ2luIHBvcHVwIGZ1bmN0aW9uXHJcbiAgICAkKCAnLm1lbWJlcnNoaXAtc2lnbmluJyApLmNsaWNrKCBmdW5jdGlvbiggZSApIHtcclxuICAgICAgICAkKCAnLm1lbWJlcnNoaXAtc2lnbmluLXBvcHVwJyApLnNob3coKTtcclxuICAgIH0gKTtcclxuXHJcbiAgICAvLyBjbG9zaW5nIHBvcHVwIG9uIGNsaWNraW5nIG92ZXJsYXlcclxuICAgICQoICcubWVtYmVyc2hpcC1zaWdpbi1vdmVybGF5LCAuY2hlY2tvdXQtc2lnbmluLWNsb3NlLWljb24nICkuY2xpY2soIGZ1bmN0aW9uKCBlICkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgJCggJy5tZW1iZXJzaGlwLXNpZ25pbi1wb3B1cCcgKS5oaWRlKCk7XHJcbiAgICB9ICk7XHJcblxyXG4gICAgLy8gcHJldmVudGluZyBwcm9wYWdhdGlvbiB3aXRoaW4gc2lnbmluIGZvcm1cclxuICAgICQoICcuY2hlY2tvdXQtbWVtYmVyLXNpZ25pbi1jb250YWluZXInICkuY2xpY2soIGZ1bmN0aW9uKCBlICkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICB9ICk7XHJcblxyXG4gICAgLy8gYm9va2luZyBmb3Igc29tZW9uZSBlbHNlIGNoZWNrYm94IGhhbmRsZXJcclxuXHJcbiAgICAkKCAnLmJvb2tpbmctb3RoZXJzLWNoZWNrYm94JyApLmNoYW5nZSggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCB0aGlzLmNoZWNrZWQgKSB7XHJcbiAgICAgICAgICAgICQoICcuYmlsbGVyLWRldGFpbHMtd3JwJyApLnNob3coKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCAnLmJpbGxlci1kZXRhaWxzLXdycCcgKS5oaWRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSApO1xyXG5cclxuICAgIC8vIHZhbGlkYXRlRW50ZXJEZXRhaWxzRWxlbWVudHNcclxuICAgIHZhciB2YWxpZGF0ZUVudGVyRGV0YWlsc0VsZW1lbnRzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCAkKCAnLmJvb2tpbmctb3RoZXJzLWNoZWNrYm94JyApWyAwIF0uY2hlY2tlZCApIHtcclxuICAgICAgICAgICAgJCggJy5zdWItZm9ybS1tYW5kYXRvcnknICkuZWFjaCggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoICQoIHRoaXMgKS52YWwoKSA9PSBcIlwiICkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoIHRoaXMgKS5hZGRDbGFzcyggJ2ludmFsaWQtaW5wdXQnICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCAnLmd1ZXN0LWRldGFpbHMtd3JwIC5zdWItZm9ybS1tYW5kYXRvcnknICkuZWFjaCggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoICQoIHRoaXMgKS52YWwoKSA9PSBcIlwiICkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoIHRoaXMgKS5hZGRDbGFzcyggJ2ludmFsaWQtaW5wdXQnICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gcHJvY2VlZCBidXR0b24gdmFsaWRhdGlvblxyXG4gICAgJCggJy5wcm9jZWVkLXBheW1lbnQtYnV0dG9uJyApLmNsaWNrKCBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YWxpZGF0ZUVudGVyRGV0YWlsc0VsZW1lbnRzKCk7XHJcbiAgICAgICAgaWYgKCAkKCAnLnN1Yi1mb3JtLWlucHV0LWVsZW1lbnQnICkuaGFzQ2xhc3MoICdpbnZhbGlkLWlucHV0JyApICkge1xyXG4gICAgICAgICAgICAkKCAnLmludmFsaWQtaW5wdXQnICkuZmlyc3QoKS5mb2N1cygpO1xyXG4gICAgICAgICAgICB2YXIgYmlsbGVyRGV0YWlsc0hhc0ludmFsaWRJbnB1dCA9ICggJCggJy5iaWxsZXItZGV0YWlscy13cnAgLnN1Yi1mb3JtLWlucHV0LWVsZW1lbnQnICkuaGFzQ2xhc3MoICdpbnZhbGlkLWlucHV0JyApICk7XHJcbiAgICAgICAgICAgIGlmICggYmlsbGVyRGV0YWlsc0hhc0ludmFsaWRJbnB1dCA9PSBmYWxzZSApIHtcclxuICAgICAgICAgICAgICAgIGlmICggKCAkKCAnI2Jvb2tpbmdHdWVzdFRpdGxlJyApWyAwIF0udmFsdWUgPT0gXCJcIiApICkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoICcuc2VsZWN0Ym94aXQnICkudHJpZ2dlciggJ2NsaWNrJyApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJCggJy5ib29raW5nLWRldGFpbHMtd3JwJyApLmhpZGUoKTtcclxuICAgICAgICAgICAgJCggJy5wYXltZW50LWRldGFpbHMtd3JwJyApLnNob3coKTtcclxuICAgICAgICAgICAgJCggJy5jaGVja291dC1mb3JtLXRpdGxlJyApLnRleHQoICdQYXltZW50IGRldGFpbHMnICk7XHJcbiAgICAgICAgICAgICQoICcuZm9ybS1zdGVwJyApLnRvZ2dsZUNsYXNzKCAnYWN0aXZlLWZvcm0tc3RlcCcgKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICB9ICk7XHJcblxyXG4gICAgLy8gcmVkZWVtIHBvaW50cyBjaGVja2JveCBoYW5kbGVyXHJcblxyXG4gICAgJCggJy5yZWRlZW0tcG9pbnRzLWNoZWNrYm94JyApLmNoYW5nZSggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgaWYgKCB0aGlzLmNoZWNrZWQgKSB7XHJcbiAgICAgICAgICAgICQoICcucmVkZWVtLWNoZWNrYm94LXdycCcgKS5hZGRDbGFzcyggJ3JlZGVlbS1wb2ludHMtY2hlY2tlZCcgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCAnLnJlZGVlbS1jaGVja2JveC13cnAnICkucmVtb3ZlQ2xhc3MoICdyZWRlZW0tcG9pbnRzLWNoZWNrZWQnICk7XHJcbiAgICAgICAgfVxyXG4gICAgfSApO1xyXG59ICk7XHJcblxyXG4vLyBCb29raW5nIGd1ZXN0IGRyb3Bkb3duIFxyXG52YXIgb3B0aW9ucyA9IHt9O1xyXG52YXIgJGJvb2tpbmdHdWVzdFRpdGxlID0gJCggJyNib29raW5nR3Vlc3RUaXRsZScgKTtcclxudmFyICRyZWRlZW1UaWNEcm9wZG93biA9ICQoICcjcmVkZWVtVGljRHJvcGRvd24nICk7XHJcbnZhciBkcm9wRG93bnMgPSB7XHJcbiAgICB0aXRsZXM6IHtcclxuICAgICAgICBvcHRpb25zOiBbICdNci4nLCAnTXMuJywgJ01ycy4nIF0sXHJcbiAgICAgICAgZWxlbTogJGJvb2tpbmdHdWVzdFRpdGxlLFxyXG4gICAgICAgIGRlZmF1bHQ6ICcnLFxyXG4gICAgICAgIHNlbGVjdGVkOiBudWxsLFxyXG4gICAgICAgIGRlcGVuZGVudDoge1xyXG4gICAgICAgICAgICBlbGVtOiBudWxsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIHRpYzoge1xyXG4gICAgICAgIG9wdGlvbnM6IFsgJ1RJQyBwbHVzIENyZWRpdCBDYXJkJywgJ0VwaWN1cmUgcGx1cyBDcmVkaXQgQ2FyZCcgXSxcclxuICAgICAgICBlbGVtOiAkcmVkZWVtVGljRHJvcGRvd24sXHJcbiAgICAgICAgZGVmYXVsdDogJycsXHJcbiAgICAgICAgc2VsZWN0ZWQ6IG51bGwsXHJcbiAgICAgICAgZGVwZW5kZW50OiB7XHJcbiAgICAgICAgICAgIGVsZW06IG51bGxcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmluaXREcm9wZG93bi5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIGl0ckxlbmd0aCA9IHRoaXMub3B0aW9ucy5sZW5ndGg7XHJcbiAgICBmb3IgKCBpID0gMDsgaSA8IGl0ckxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgIHRoaXMudGFyZ2V0RWxlbS5hcHBlbmQoICc8b3B0aW9uPicgKyB0aGlzLm9wdGlvbnNbIGkgXSArICc8L29wdGlvbj4nIClcclxuICAgIH07XHJcblxyXG4gICAgLy9Jbml0aWFsaXplcyBzZWxlY3Rib3hpdCBvbiB0aGUgcmVuZGVyZWQgZHJvcGRvd25cclxuICAgIHRoaXMudGFyZ2V0RWxlbS5zZWxlY3RCb3hJdCgpO1xyXG5cclxuICAgIC8vIEFkZCBsaXN0ZW5lcnMgdG8gZWFjaCBzZWxlY3Rib3ggY2hhbmdlXHJcbiAgICB0aGlzLmxpc3RlbkREQ2hhbmdlKCk7XHJcbn07XHJcbmluaXREcm9wZG93bi5wcm90b3R5cGUubGlzdGVuRERDaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuICAgIHZhciBkVGFyZ2V0ID0gdGhpcy50YXJnZXRCbG9jaztcclxuICAgIGRUYXJnZXQuZWxlbS5jaGFuZ2UoIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBzZWxlY3RlZE9wdGlvbiA9ICggZFRhcmdldC5lbGVtICkuZmluZCggXCJvcHRpb246c2VsZWN0ZWRcIiApO1xyXG4gICAgICAgIGRUYXJnZXQuc2VsZWN0ZWQgPSBzZWxlY3RlZE9wdGlvbi50ZXh0KCk7XHJcbiAgICAgICAgaWYgKCBkVGFyZ2V0LmRlcGVuZGVudC5lbGVtICkge1xyXG4gICAgICAgICAgICBkVGFyZ2V0LmRlcGVuZGVudC5lbGVtLnNlbGVjdEJveEl0KCAnc2VsZWN0T3B0aW9uJywgMCApO1xyXG4gICAgICAgICAgICBpZiAoIGRUYXJnZXQuc2VsZWN0ZWQgIT0gZFRhcmdldC5kZWZhdWx0ICkge1xyXG4gICAgICAgICAgICAgICAgZFRhcmdldC5kZXBlbmRlbnQuZWxlbS5wcm9wKCBcImRpc2FibGVkXCIsIGZhbHNlICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkVGFyZ2V0LmRlcGVuZGVudC5lbGVtLnByb3AoIFwiZGlzYWJsZWRcIiwgdHJ1ZSApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKCBkVGFyZ2V0LnNlbGVjdGVkICE9IGRUYXJnZXQuZGVmYXVsdCApIHtcclxuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGRyb3BEb3ducyk7IC8vIFwiZHJvcERvd25zXCIgR2l2ZXMgdGhlIHNlbGVjdGVkIHZhbHVlcyBmb3IgZWFjaCBkcm9wZG93biBvcHRpb25zXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9ICk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBpbml0RHJvcGRvd24oIGVsZW1lbnQsIHRhcmdldCApIHtcclxuICAgIHRoaXMudGFyZ2V0QmxvY2sgPSB0YXJnZXQ7XHJcbiAgICB0aGlzLm9wdGlvbnMgPSB0YXJnZXQub3B0aW9ucztcclxuICAgIHRoaXMudGFyZ2V0RWxlbSA9IGVsZW1lbnQ7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBpbml0Ym9va2luZ0d1ZXN0VGl0bGUoKSB7XHJcblxyXG4gICAgdmFyIGJvb2tpbmdHdWVzdFRpdGxlID0gbmV3IGluaXREcm9wZG93biggJGJvb2tpbmdHdWVzdFRpdGxlLCBkcm9wRG93bnMudGl0bGVzICk7XHJcbiAgICB2YXIgcmVkZWVtVGljRHJvcGRvd24gPSBuZXcgaW5pdERyb3Bkb3duKCAkcmVkZWVtVGljRHJvcGRvd24sIGRyb3BEb3ducy50aWMgKTtcclxuXHJcbiAgICBib29raW5nR3Vlc3RUaXRsZS5pbml0aWFsaXplKCk7XHJcbiAgICByZWRlZW1UaWNEcm9wZG93bi5pbml0aWFsaXplKCk7XHJcbn07XHJcblxyXG5pbml0Ym9va2luZ0d1ZXN0VGl0bGUoKTsiXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NoZWNrb3V0LWVudGVyLWRldGFpbHMvY2hlY2tvdXQtZW50ZXItZGV0YWlscy5qcyJ9

