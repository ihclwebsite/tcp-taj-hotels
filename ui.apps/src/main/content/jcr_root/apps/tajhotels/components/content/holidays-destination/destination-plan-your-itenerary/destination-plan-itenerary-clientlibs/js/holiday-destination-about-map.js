window.addEventListener('load', function() {

    var allMarkers = [];
    var detail = {};
    var markerSizeFlag = false;
    var hotelType;
    var markerSizeFlag = false;
    var windowWidth = $(window).width();
    var jsonData = $($.find('[data-mapdata]')[0]).text();

    if (jsonData) {
        var data = JSON && JSON.parse(jsonData) || $.parseJSON(jsonData);
    }

    function hotelCardClicked(targetHotelCard) {
        hotelCardActivate(targetHotelCard.id);
        markerSizeChange(targetHotelCard.id);
    }

    function createCards() {
        try {
            // dynamically create the card here
            for (var i = 0; i < data.data[0].localArea.length; i++) {
                var wrapperCard = '<div id="' + data.data[0].localArea[i].id + '" class="about-dest-map-wrap"></div>';
                $('.mr-map-view-about-dest').append(wrapperCard);
                var timeHTML;
                if (data.data[0].localArea[i].openingTime === "" || data.data[0].localArea[i].closingTime === "") {
                    timeHTML = '';
                } else {
                    timeHTML = '<div>Time - ' + data.data[0].localArea[i].openingTime + ' - '
                            + data.data[0].localArea[i].closingTime + '</div>';
                }

                var localAreaCard = '<div class="holiday-about-dest-wrapper"> '
                        + '<div class="holiday-about-dest-image-cont"><img src="' + data.data[0].localArea[i].imagePath
                        + '" /></div>' + '<div class="holiday-about-dest-details-cont">'
                        + '<div class="details-header">' + data.data[0].localArea[i].name + '</div>'
                        + '<div class="details-description">' + data.data[0].localArea[i].description + '</div>'
                        + '<div class="details-time">' + timeHTML + '</div></div></div>';
                $("#" + data.data[0].localArea[i].id + ".about-dest-map-wrap").append(localAreaCard);
                createMarker(data.data[0].localArea[i]);

                $("#" + data.data[0].localArea[i].id + ".about-dest-map-wrap").click(function() {
                    hotelCardClicked(this);
                })

                $("#" + data.data[0].localArea[i].id + ".about-dest-map-wrap").find('.details-description')
                        .cmToggleText({
                            charLimit : 120
                        });
            }

            // on desktop first card will be activated
            $('.about-dest-map-wrap:first-child').addClass("active");
            allMarkers[0].setIcon(allMarkers[0].iconFocused);
            allMarkers[0].set('labelClass', 'markerlabels active');
            allMarkers[0].markerSizeFlag = true;
        } catch (error) {
            console.error("Error in creating cards ", error);
        }
    }

    function hotelCardActivate(hotelId) {
        if (($("#" + hotelId + ".about-dest-map-wrap").hasClass("active") == false)) {
            if ($('.about-dest-map-wrap.active').length > 0)
                $('.about-dest-map-wrap.active').removeClass("active");
        }
        $("#" + hotelId + ".about-dest-map-wrap").addClass("active");
    }

    function markerSizeChange(targetMarkerId) {
        for (var i = 0; i < allMarkers.length; i++) {
            if (allMarkers[i].localId == targetMarkerId) {
                if (allMarkers[i].markerSizeFlag == false) {
                    allMarkers[i].setIcon(allMarkers[i].iconFocused);
                    allMarkers[i].set('labelClass', 'markerlabels active');
                    allMarkers[i].markerSizeFlag = true;
                }
            } else {
                allMarkers[i].setIcon(allMarkers[i].iconNormal);
                allMarkers[i].set('labelClass', 'markerlabels');
                allMarkers[i].markerSizeFlag = false;
            }
        }
    }

    function createMarker(elem) {
        var localId = elem.id;
        detail.lat = parseFloat(elem.coordinates.lat);
        detail.lng = parseFloat(elem.coordinates.lng);

        var pinIcon = {
            url : "/content/dam/tajhotels/icons/style-icons/marker_local.svg",
            scaledSize : new google.maps.Size(46, 58),
        };

        var pinIconLarge = {
            url : "/content/dam/tajhotels/icons/style-icons/marker_localLarge.svg",
            scaledSize : new google.maps.Size(64, 84),
        }
        marker = new MarkerWithLabel({
            position : detail,
            map : mapDestAbout,
            animation : google.maps.Animation.DROP,
            icon : pinIcon,
            labelContent : elem.name,
            labelAnchor : new google.maps.Point(50, 0),
            labelClass : "markerlabels",
            labelInBackground : true,
            optimized : false
        });

        marker.localId = localId;
        marker.markerSizeFlag = markerSizeFlag;
        marker.iconNormal = pinIcon;
        marker.iconFocused = pinIconLarge;

        allMarkers.push(marker);

        google.maps.event.addListener(marker, 'click', function() {
            if ((window.screen.width) < 992) {
                if (this.markerSizeFlag == false) {
                    cardsToDisplayMobile(this.localId);
                    markerSizeChange(this.localId);
                    hotelCardActivate(this.localId);
                }
            } else {
                markerSizeChange(this.localId);
                hotelCardActivate(this.localId);
                cardsToDisplayMobile(this.localId);
            }
        });
    }

    $('.mr-map-switch').on('click', function() {
        if ((window.screen.width) < 992) {
            createCardsForMobileMapView();
            allMarkers[0].setIcon(allMarkers[0].iconFocused);
            allMarkers[0].set('labelClass', 'markerlabels active');
        }
    })

    function createCardsForMobileMapView() {
        try {
            $('.carousel-inner.card-carousel.mr-aboutDest-map-carousel').empty();
            for (var i = 0; i < data.data[0].localArea.length; i++) {
                var card = '<div class="carousel-item">' + '<div class="hotel-parent">'
                        + '<div class="hotel-wrapper" id="' + data.data[0].localArea[i].id + '">' + '</div>' + '</div>'
                        + '</div>';
                if (data.data[0].localArea[i].openingTime === "" || data.data[0].localArea[i].closingTime === "") {
                    timeHTML = '';
                } else {
                    timeHTML = '<div>Time - ' + data.data[0].localArea[i].openingTime + ' - '
                            + data.data[0].localArea[i].closingTime + '</div>';
                }

                var localAreaCard = '<div class="holiday-about-dest-wrapper"> '
                        + '<div class="holiday-about-dest-image-cont"><img src="' + data.data[0].localArea[i].imagePath
                        + '" /></div>' + '<div class="holiday-about-dest-details-cont">'
                        + '<div class="details-header">' + data.data[0].localArea[i].name + '</div>'
                        + '<div class="details-description">' + data.data[0].localArea[i].description + '</div>'
                        + '<div class="details-time">' + timeHTML + '</div></div></div>';

                $('.carousel-inner.card-carousel').append(card);
                $("#" + data.data[0].localArea[i].id + ".hotel-wrapper").append(localAreaCard);
                $("#" + data.data[0].localArea[i].id + ".hotel-wrapper").find('.details-description').cmToggleText({
                    charLimit : 120
                });
            }
            $('.carousel-item:first-child').addClass('active');
        } catch (error) {
            console.error(error);
        }
    }

    function cardsToDisplayMobile(hotelId) {
        $(".carousel-inner.card-carousel.mr-aboutDest-map-carousel").children(".carousel-item.active").removeClass(
                "active");
        $("#" + hotelId + ".hotel-wrapper").parent().parent().addClass("active");

    }

    // for mobile
    function activeElemnt() {
        var hotelid = $('.carousel-inner.mr-aboutDest-map-carousel').find('.carousel-item.active').children()
                .children().attr('id');
        markerSizeChange(hotelid);
    }

    // for mobile
    $('.carousel').carousel({
        interval : false
    });

    // swap functionality
    $("#mapviewAboutDest").on("touchstart", function(event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function(event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');
            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');
            }
            if ($('.carousel-inner.card-carousel.mr-aboutDest-map-carousel').children().length > 1) {
                activeElemnt();
            }
        });

        $("#mapviewAboutDest").on("touchend", function() {
            $(this).off("touchmove");
        });
    });

    function initMap() {
        try {
            var latOfCity = parseFloat(data.data[0].coordinates.lat);
            var lngOfCity = parseFloat(data.data[0].coordinates.lng);
            var latlng = new google.maps.LatLng(latOfCity, lngOfCity);
            mapDestAbout = new google.maps.Map(document.getElementById('mapDestinationAbout'), {
                zoom : 10,
                center : latlng
            });
        } catch (error) {
            console.error("Error in map js ", error);
        }
    }

    $(window).resize(function() {
        if ($(window).width() != windowWidth) {
            var currentWidth = $(window).width();
            // only ipad potrait mode
            if (currentWidth == 768) {
                createCardsForMobileMapView();
            }
            windowWidth = currentWidth;
        }
    });

    function initHolidayDestinationMap() {
        initMap();
        createCards();
    }
    if (document.getElementById('mapDestinationAbout')) {
        initHolidayDestinationMap();
    }
});
