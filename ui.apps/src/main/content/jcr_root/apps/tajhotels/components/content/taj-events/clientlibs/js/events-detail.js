$(document).ready(function() {
    try {
        toggleDescText();
        checkSoldOut();
        initTicketEvents();
        var address = $('.venue-details').text();
        getLatitudeLongitude(returnLatlng, address);
    } catch (error) {
        console.error("Error in Taj events js ", error);
    }
});

function toggleDescText() {
    if ($('.placeholder-title p').length > 2) {
        $('.placeholder-title').append('<div class="cm-view-txt">Show more</div>');
        $('.cm-view-txt').click(function() {
            if ($(this).text() == "Show more") {
                $('.placeholder-title').addClass('placeholder-title-display-all');
                $(this).text("Show less");
            } else {
                $('.placeholder-title').removeClass('placeholder-title-display-all');
                $(this).text("Show more");
            }
        });

    }
}

function returnLatlng(results) {
    var mapAddress = "https://www.google.com/maps/search/?api=1&query=Eiffel%20Tower&query_place_id="
            + results.place_id;
    $("a.veiw-map").attr('href', mapAddress);
}

function getLatitudeLongitude(callback, address) {
    var geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address' : address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }

}

function checkSoldOut() {
    var availableSeats = $('#availableTicket').text();
    if (parseInt(availableSeats) == 0 || availableSeats == 'SOLD OUT') {
        $('#quantityC').val(0);
        $('.booking-submit-btn').attr("disabled", true);
        $('.booking-submit-btn ').css({
            "opacity" : 0.4
        });
        warningBox({
            title : 'Tickets for this event are sold out. Please try some other event.'
        });
    }
}

function initTicketEvents() {
    // adding quantity script starts
    $(".no-of-tickets .increment-tickets").on("click", incrementSelectionCountEvent);
    // adding quantity script ends
    $('#maxSeats').text();
    // subtracting quantity script starts
    $(".no-of-tickets .decrement-tickets").on("click", decrementSelectionCountEvent);
    // subtracting quantity script ends

    // $(".no-of-tickets-wrp .input-group-btn").click(ticketCountChanged);

    $(".booking-submit-btn").on("click", function() {
        var selectedTickets = parseInt($('.tic-ticket-count').val());
        var availableTickets = parseInt($('#availableTicket').text());
        if (selectedTickets <= availableTickets) {
            createBookingDetails();
        } else {
            warningBox({
                title : 'Number of selected tickets exceeds available tickets. Please recheck and submit again.'
            });
        }

    });
}

function incrementSelectionCountEvent(e) {
    e.preventDefault();
    var availableSeats = parseInt($('#availableTicket').text()) || 0;
    var maxSeats = $('#maxSeats').text();
    var $button = $(this);
    var inputCounter = $button.parent().parent().find("input");
    var quantity = parseInt(inputCounter.val());
    ++quantity;
    if (quantity > availableSeats) {
        quantity = availableSeats;
    } else if (quantity > maxSeats) {
        quantity = maxSeats;
    }
    inputCounter.val(quantity);
    // inputCounter.text(quantity);
    var x = $button.parent().parent().parent().attr("class");
    updateTicketAmount();
}

function decrementSelectionCountEvent(e) {
    e.preventDefault();
    var availableSeats = $('#availableTicket').text() || 0;
    var $button = $($(this)[0]);
    var inputCounter = $button.parent().parent().find("input");
    var quantity = parseInt(inputCounter.val());
    var x = $button.parent().parent().parent().attr("class");
    quantity > 1 ? --quantity : quantity;
    inputCounter.val(quantity);
    updateTicketAmount();
}

function updateTicketAmount() {
    var ticketCount = parseInt($('#quantityC').val());
    var basAmount = parseInt($(".baseTicPoints").attr("data-ticPoints"));
    var totalAmount = ticketCount ? ticketCount * basAmount : basAmount;
    $('#pointsidTIC').text(totalAmount);
}

function validateInputFields() {
    var flag = true;
    $('.event-booking-form .sub-form-mandatory').each(function() {
        if ($(this).val() == "" || $(this).hasClass('invalid-input')) {
            $(this).addClass('invalid-input');
            flag = false;
            invalidWarningMessage($(this));
        }
    });
    return flag;
}

function createBookingDetails() {
    if (validateInputFields()) {

        var eventname = $('.event-title').text() || '';
        var eventvenue = $('.venue-details').text() || '';
        var totalAmount = $('#pointsidTIC').text() || '';
        var resourcePath = $('#eventpath').text() || '';
        var productname = $('#productname').text() || 'Event';
        var propertyCode = $('#propertycode').text() || '';
        var eventdate = $('.date-details').text() || '';
        var membermail = $('#customerEmail').val() || '';
        var membername = $('#customerName').val() || '';
        var currency = $('#currency').text() || '';
        var mobileNumber = $('#customerMobile').val() || '';
        var hotelId = $($.find('[data-hotel-id]')).data().hotelId || '';
        var totalTickets = $('#quantityC').val() || '';
        var availableSeats = $('#availableTicket').text() || '';
        var subAccountId = $($.find('[data-sub-account-id]')).data().subAccountId || '';

        var eventDetailsData = {
            "totalAmount" : totalAmount,
            "customerEmail" : membermail,
            "eventVenue" : eventvenue,
            "eventName" : eventname,
            "eventPath" : resourcePath,
            "productName" : productname,
            "currency" : currency,
            "customerName" : membername,
            "eventDate" : eventdate,
            "phoneNumber" : mobileNumber,
            "availableSeats" : availableSeats,
            "tickets" : totalTickets,
            "propertyCode" : propertyCode
        }
        var eventDetailsData = JSON.stringify(eventDetailsData);
        if (eventDetailsData.indexOf('&') > -1) {
            var searchStr = "&";
            var replaceStr = "%26";
            var re = new RegExp(searchStr, "g");
            var result = eventDetailsData.replace(re, replaceStr);
            callEventBookingAjax(result, hotelId, subAccountId);
        } else {
            callEventBookingAjax(eventDetailsData, hotelId, subAccountId);
        }

    } else {
        $('.event-booking-form .invalid-input').eq(0).focus();
    }
}

function callEventBookingAjax(eventDetailsData, hotelId, subAccountId) {
    var popupParams = {};
    $.ajax({
        type : 'POST',
        url : '/bin/eventBookingServlet',
        data : 'eventDetailsData=' + eventDetailsData + '&hotelId=' + hotelId + '&subAccountId=' + subAccountId,
        success : function(successResponse) {
            console.log("successResponse: " + successResponse);
            if (successResponse == undefined || successResponse == "" || successResponse.length == 0) {
                $('body').hideLoader();
                popupParams.description = 'Something went Wrong. Refreshing Page';
                warningBox(popupParams);
                location.reload();
            } else {
                successResponse = JSON.parse(successResponse);
                if (successResponse.status) {
                    $('body').hideLoader();
                    var $form = $(successResponse.CCAvenueForm);
                    $('body').append($form);
                    $form.submit();
                } else {
                    $('body').hideLoader();
                    popupParams.description = successResponse.message;
                    warningBox(popupParams);
                }
            }
        },
        fail : function(failedResponse) {
            $('body').hideLoader();
            console.log("failedResponse: " + failedResponse);
            popupParams.description = 'Event Booking failed. Please try again later.';
            warningBox(popupParams);

        },
        error : function(errorResponse) {
            $('body').hideLoader();
            console.log("errorResponse: " + errorResponse);
            popupParams.description = 'Event Booking interrupted. Please try again later.';
            warningBox(popupParams);
            location.reload();
        }
    });

}
