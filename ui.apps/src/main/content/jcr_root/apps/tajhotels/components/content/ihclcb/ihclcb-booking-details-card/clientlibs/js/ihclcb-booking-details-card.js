$(document).ready(function() {

					var bookingResponseJSON = {
						"items" : [
								{
									"Id" : "300000286231588",
									"RowType" : "",
									"City" : "Bangalore",
									"RecordName" : "BLRWE1000047371209",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:09:41+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:38+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "BLRWE100004737",
									"CHANNEL_c" : "SYDC",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100004737",
									"AM_BOOKINGDATE_c" : "2018-06-14",
									"SOURCECODE_c" : "BOOKINGDC",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-06-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-06-17",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "6914221",
									"MARKETCODE_c" : "TEC",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000105827143",
									"GastNR_dcl_c" : "Guddi Nalovalia",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "300000750688606",
									"TRAVEL_dcl_c" : "BOOKING.COM BV",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "407496",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "17079.64",
									"RoomSpendEarning_c" : "46163",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "0",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "CS1",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "3",
									"RateCode_c" : "NB15",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1209",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "3",
									"Day_c" : "77",
									"FinancialYear_c" : "2018-03-17",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "2",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000449696501",
												"RowType" : "",
												"RecordName" : "SR-3031420-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.055+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "4200"
											},
											{
												"Id" : "300000449696502",
												"RowType" : "",
												"RecordName" : "SR-3031421-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.073+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "220",
												"Amount_c" : "800"
											},
											{
												"Id" : "300000449696503",
												"RowType" : "",
												"RecordName" : "SR-3031422-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25.008+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.091+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "96526"
											},
											{
												"Id" : "300000449696504",
												"RowType" : "",
												"RecordName" : "SR-3031423-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.109+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "250",
												"Amount_c" : "6200"
											},
											{
												"Id" : "300000449696505",
												"RowType" : "",
												"RecordName" : "SR-3031424-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25.015+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.128+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "200",
												"Amount_c" : "5879.64"
											},
											{
												"Id" : "300000449696506",
												"RowType" : "",
												"RecordName" : "SR-3031425-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:16:25.018+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:16:25.146+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-79374.64"
											},
											{
												"Id" : "300000449696694",
												"RowType" : "",
												"RecordName" : "SR-3031783-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:17:46+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:17:46.036+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000449697465",
												"RowType" : "",
												"RecordName" : "SR-3031788-180618",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-06-18T00:17:48+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-06-18T00:17:48.039+00:00",
												"Reservation_Id_c" : "300000286231588",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "0"
											} ]
								},
								{
									"Id" : "300000286231566",
									"RowType" : "",
									"City" : "Bangalore",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-04-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-04-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231577",
									"RowType" : "",
									"City" : "Mumbai",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-06-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-06-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231599",
									"RowType" : "",
									"City" : "Bangalore",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-05-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-05-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231511",
									"RowType" : "",
									"City" : "Delhi",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-06-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-06-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231522",
									"RowType" : "",
									"City" : "Delhi",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-04-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-04-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231533",
									"RowType" : "",
									"City" : "Mumbai",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-04-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-04-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231544",
									"RowType" : "",
									"City" : "Delhi",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-05-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-05-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231555",
									"RowType" : "",
									"City" : "Delhi",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-07-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-07-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								},
								{
									"Id" : "300000286231500",
									"RowType" : "",
									"City" : "Mumbai",
									"RecordName" : "BLRWE1000057491419",
									"CreatedBy" : "Datacentre",
									"CreationDate" : "2018-04-06T08:08:43+00:00",
									"LastUpdatedBy" : "Datacentre",
									"LastUpdateDate" : "2019-09-09T07:29:55.237+00:00",
									"LastUpdateLogin" : "",
									"UserLastUpdateDate" : "",
									"CurrencyCode" : "INR",
									"CurcyConvRateType" : "",
									"CorpCurrencyCode" : "",
									"SourceType" : "RESERVATION_C",
									"TAJRESERVID_c" : "",
									"CHANNEL_c" : "GDS",
									"ANZ_NUMBEROFROOM_c" : "1",
									"REGISTER_c" : "100005749",
									"AM_BOOKINGDATE_c" : "2018-04-05",
									"SOURCECODE_c" : "GDS",
									"HotelCode_c" : "BLRWE",
									"HotelName_c" : "",
									"ANREISE_CHECKINDATE_c" : "2019-05-14",
									"ABREISE_CHECKOUTDATE_c" : "2019-05-18",
									"COMPANY_txt_c" : "",
									"TRAVEL_txt_c" : "100124028",
									"MARKETCODE_c" : "TRA",
									"BOOKINGDATE_c" : "2018-04-05",
									"SOURCE_NAME_c" : "Opera",
									"GastNR_dcl_Id_c" : "100000049466231",
									"GastNR_dcl_c" : "Peter Teh",
									"COMPANY_dcl_Id_c" : "",
									"COMPANY_dcl_c" : "",
									"TRAVEL_dcl_Id_c" : "100000000394052",
									"TRAVEL_dcl_c" : "GBT AUSTRALIA PTY LTD",
									"Hotel_dcl_Id_c" : "300000002316639",
									"Hotel_dcl_c" : "Taj West End Bengaluru",
									"GastNR_txt_c" : "98064599",
									"NonEarningSpends_c" : "",
									"BanquetSpendsNonEarning_c" : "",
									"FBSpendEarning_c" : "1200",
									"RoomSpendEarning_c" : "59500",
									"SettlementNonEarning_c" : "",
									"OtherSpendsEarning_c" : "7700",
									"TaxesNonEarning_c" : "",
									"CheckOutYear_c" : "AKX",
									"CheckOutMonth_c" : "",
									"CheckoutPhysical_c" : "2018",
									"RoomNights_c" : "4",
									"RateCode_c" : "T08",
									"Action_c" : "CHECKED OUT",
									"RoomNumber_c" : "1419",
									"ReservationCreationDate_c" : "",
									"GroupName_c" : "",
									"FiscalYear_c" : "",
									"FiscalYearNumField_c" : "2018",
									"FiscalMonth_c" : "1",
									"Day_c" : "17",
									"FinancialYear_c" : "2018-01-18",
									"Resv_Master_flag_c" : "Y",
									"Resv_Share_Number_c" : "",
									"Adults_c" : "1",
									"ContactName_Id_c" : "100000036005307",
									"ContactName_c" : "Gaurav Ramachandran",
									"WindowRevenueCollection_c" : [
											{
												"Id" : "300000289885985",
												"RowType" : "",
												"RecordName" : "SR-1961595-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.082+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "210",
												"Amount_c" : "0"
											},
											{
												"Id" : "300000289885986",
												"RowType" : "",
												"RecordName" : "SR-1961596-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.005+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.102+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "230",
												"Amount_c" : "1200"
											},
											{
												"Id" : "300000289885987",
												"RowType" : "",
												"RecordName" : "SR-1961597-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.007+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.121+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "100",
												"Amount_c" : "112200"
											},
											{
												"Id" : "300000289885988",
												"RowType" : "",
												"RecordName" : "SR-1961598-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.010+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.148+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "920",
												"Amount_c" : "3550"
											},
											{
												"Id" : "300000289885989",
												"RowType" : "",
												"RecordName" : "SR-1961599-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.012+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.172+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "420",
												"Amount_c" : "4150"
											},
											{
												"Id" : "300000289885990",
												"RowType" : "",
												"RecordName" : "SR-1961600-180418",
												"CreatedBy" : "Datacentre",
												"CreationDate" : "2018-04-18T23:01:29.013+00:00",
												"LastUpdatedBy" : "Datacentre",
												"LastUpdateDate" : "2018-04-18T23:01:29.200+00:00",
												"Reservation_Id_c" : "300000286231566",
												"LastUpdateLogin" : "",
												"UserLastUpdateDate" : "",
												"CurrencyCode" : "INR",
												"CurcyConvRateType" : "Corporate",
												"CorpCurrencyCode" : "INR",
												"RevenueType_c" : "900",
												"Amount_c" : "-86200.5"
											} ]
								}, ]
					};

					// fetchCorporateBookingDetails();
					populateBookingDetailsData(bookingResponseJSON.items);
					$(document)
							.on(
									'click',
									'.ihclcb-show-more-cont .show-more-text,.ihclcb-show-more-cont .icon-drop-down-arrow',
									function() {
										var currentCls = $(this).parent(".ihclcb-show-more-cont").find(".show-more-text");
										var currentText = currentCls.text();
										$(this)
												.closest(
														'.ihclcb-booking-details-wrp')
												.toggleClass(
														'ihclcb-booking-details-wrp-open');
										currentCls.text($.trim(currentText) == 'Show Details' ? 'Hide Details' : 'Show Details');
									});
					
					//Click of copy icon to copy text
					$(".ihclcb-booking-details-card-wrap .copyText").click(function() {
						var copyText = $(this).parent().siblings(".ihclcb-accordion-data");
						var $temp = $("<input>");
						$("body").append($temp);
						$temp.val(copyText.text()).select();
						document.execCommand("copy");
						$temp.remove();
						$(this).siblings(".copy-tootip").show().delay(1000).fadeOut();
					});

					// Filter Dropdown
					$(".ihclcb-bookings-filter-container .dropdown-toggle").click(function() {
						$(".ihclcb-bookings-filter-container .dropdown-menu").toggle();
					});
					$(
							".ihclcb-bookings-filter-container .dropdown-menu .dropdown-item")
							.click(
									function(event) {
										event.preventDefault();
										event.stopPropagation();
										var selectedCity = $(this).text();
										$(this).parent().hide();
										var choosedEle = $(this).closest(
												'[data-filter-drpdwn]').attr(
												'data-filter-drpdwn');
										$(
												'[data-filtered-value='
														+ choosedEle + ']')
												.addClass('ihcl-filter-applied')
												.find('.filtered-value').text(
														selectedCity);
										$(this).attr("data-selected", true)
												.siblings().attr(
														"data-selected", false);
										$(
												".ihclcb-bookings-filter-container .dropdown-toggle")
												.text(selectedCity);
									});

					// datepicker

					intiateFilterDatepicker();
					// datepicker ends
					
					//reset Particular Filter
					$(".reset-particular-filtertype").click(function() {
						var toBeHidden = $(this).closest('.ihcl-filter-applied').attr('data-filtered-value');
						if(toBeHidden == "filter-checkin-ref" || toBeHidden == "filter-checkout-ref") {
							$('[data-filtered-value=filter-checkin-ref],[data-filtered-value=filter-checkout-ref]').removeClass("ihcl-filter-applied");
							$("#ihclcb-filter-from-cal,#ihclcb-filter-to-cal").val("dd M yy");
						}

						else {
							$(this).parents(".ihcl-filter-applied").removeClass("ihcl-filter-applied");
							$(".ihclcb-bookings-filter-container .dropdown-toggle").text("City");
							$(".ihclcb-bookings-filter-container .dropdown-item").attr("data-selected",false);
						}
						filterDetails(bookingResponseJSON.items);
					});
					
					//Click of apply filter button
					$(".ihclcb-apply-filter button").click(function() {
						$(".ihclcb-filter-pop-up").hide();
						$(".cm-page-container").removeClass("prevent-page-scroll");
						filterDetails(bookingResponseJSON.items);
					});
					
					//Click of reset Filters to remove filter functionality
					$(".ihclcb-reset-filter button").click(function(){
						$(".ihclcb-bookings-filter-container .ihcl-filter-applied").removeClass("ihcl-filter-applied");
						$("#ihclcb-filter-from-cal,#ihclcb-filter-to-cal").val("dd M yy");
						$(".ihclcb-bookings-filter-container .dropdown-toggle").text("City");
						$(".ihclcb-bookings-filter-container .dropdown-item").attr("data-selected",false);
						$(".ihclcb-filter-pop-up").hide();
						$(".cm-page-container").removeClass("prevent-page-scroll");
						filterDetails(bookingResponseJSON.items);
					})
					
					//On Click of outside popup calling filterDetails list to appear
					$(document).mouseup(function(e) {
						if ($(".ihclcb-filter-pop-up").is(":visible")) {
							var container = $(".ihclcb-filter-pop-up *");
							var dropdownMenu = $(".ihclcb-bookings-filter-container .dropdown .dropdown-menu");
							if(!dropdownMenu.is(e.target)) {
								dropdownMenu.hide();
							}
							if (!container.is(e.target)) {
								$(".ihclcb-filter-pop-up").hide();
								$(".cm-page-container").removeClass("prevent-page-scroll");
							}
						}

					});
					
				});
//function to display selected filter options
function filterDetails(filterProcessJson) {
	$(".filtered-details").hide();
	$(".ihclcb-bookings-filter-none").show();
	var filterConditions = [];
	$(".filter-result").each(function() {
		if ($(this).hasClass("ihcl-filter-applied")) {
			$(".filtered-details").css("display", "flex");
			$(".ihclcb-bookings-filter-none").hide();
			$(this).show();
			filterConditions.push({type:$(this).find(".filtertype").text(),value:$(this).find(".filtered-value").text()});
		} else {
			$(this).hide();
		}
	});
	applyFilter(filterConditions,filterProcessJson);
}
//Function to process filtering based on selected filter
function applyFilter(filterItem,fullJson) {
	var filteredResponse = fullJson;
	var filterDateFrom;
	var filterDateTo;
	filterItem.forEach(function(condition) {
		filteredResponse = filteredResponse.filter(function(data) {
			if(condition.type == "City:") {
				if(condition.value == "All") {
					return true;
				}
				else {
					return data.City == condition.value;
				}
				
			}
			if(condition.type == "From:") {
				filterDateFrom = condition.value;
				var msofFilterDateFrom = parseInt(moment(filterDateFrom).format('x'));
				var msofCheckinDate = parseInt(moment(data.ANREISE_CHECKINDATE_c).format('x'));
				return (msofFilterDateFrom <= msofCheckinDate);
			}
			if(condition.type == "To:") {
				filterDateTo = condition.value;
				var msofFilterDateTo = parseInt(moment(filterDateTo).format('x'));
				var msofCheckoutDate = parseInt(moment(data.ABREISE_CHECKOUTDATE_c).format('x'));
				return (msofFilterDateTo >= msofCheckoutDate);
			}
		});
	});
	populateBookingDetailsData(filteredResponse);
}
//Fetch data from API
function fetchCorporateBookingDetails() {
	$('body').showLoader(true);
	//var api_url = 'https://tajics-a455764.integration.us2.oraclecloud.com/integration/flowapi/rest/MDMFETCHRESERVATION/v01/FetchReservation?q=ContactName_Id_c=100000036005307';
      var ihclcbMDMFetchReservationHostUrl = $('ihcl-past-bookings-outer-wrap').attr('data-bookingHost');
      var ihclcbMDMFechReservationEndpointUrl = $('ihcl-past-bookings-outer-wrap').attr('data-ihclcbBookingEndpointUrl');
	USERNAME = 'raj.srinivasan@innovacx.com';
	PASSWORD = 'Smile@25';

	$.ajax({
		type : "GET",
		url : ihclcbMDMFetchReservationHostUrl + ihclcbMDMFechReservationEndpointUrl,
		dataType : 'json',
		headers : {
			"Authorization" : "Basic " + btoa(USERNAME + ":" + PASSWORD)
		},
		success : function(result) {
			populateDetails(result);
			$('body').hideLoader(true);
		}
	});
}
//Populate datas in DOM
function populateBookingDetailsData(responseResult) {
	var groupPastBookingDetailsDiv = $('.ihcl-past-bookings-outer-contents');
	var groupUpcomingBookingDetailsDiv = $('.ihcl-upcoming-bookings-outer-contents');
	var itemsPerPage ;
	$(groupPastBookingDetailsDiv).empty();
	$(groupUpcomingBookingDetailsDiv).empty();
	var responseItems = responseResult;
	var multiBookingDetails = '';
	if(responseItems.length) {
		for (var i = 0; i < responseItems.length; i++) {
			var checkinDate = moment(responseItems[i].ANREISE_CHECKINDATE_c).format('DD MMM YY');
			var checkoutDate = moment(responseItems[i].ABREISE_CHECKOUTDATE_c).format('DD MMM YY');
			var totalCost = parseFloat(responseItems[i].FBSpendEarning_c)
					+ parseFloat(responseItems[i].RoomSpendEarning_c)
					+ parseFloat(responseItems[i].OtherSpendsEarning_c);

			var template = '<div class="row ihclcb-booking-details-card-wrap">\
	        <div class="col-md-4 ihclcb-desktop-only">\
	            <div class="ihclcb-booking-details-img-cont">\
	                <picture> <img src="" alt="alternate text for image" /> </picture>\
	            </div>\
	        </div>\
	        <div class="col-12 col-md-8 ihclcb-booking-details-wrp">\
	            <div class="ihclcb-booking-details-data-cont">\
	                <div class="ihclcb-hotel-heading-cont">\
	                    <h2 class="ihclcb-hotel-heading"> '
					+ responseItems[i].Hotel_dcl_c
					+ '</h2>\
	                    <div class="ihclcb-btn-cnt">\
	                        <button class="cm-btn-primary ihclcb-cancel-booking-btn">CANCEL BOOKING</button>\
	                        <button class="ihclcb-edit-booking-icon">\
	                            <img src="/content/dam/tajhotels/ihclcb-icons/ic_edit.svg" alt="edit icon">\
	                        </button>\
	                    </div>\
	                </div>\
	                <div class="ihclcb-guest-shortnd">\
	                    <span class="ihclcb-label">Guest</span>\
	                    <span class="ihclcb-label"> '
					+ responseItems[i].Hotel_dcl_c
					+ '</span>\
	                </div>\
	                <div class="row ihclcb-accordion-data-cont">\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Guest</div>\
	                        <div class="ihclcb-accordion-data">'
					+ responseItems[i].ContactName_c
					+ '</div>\
	                    </div>\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Booked On Date</div>\
	                        <div class="ihclcb-accordion-data">'
					+ responseItems[i].BOOKINGDATE_c
					+ '</div>\
	                    </div>\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Booking ID:<img src="/content/dam/tajhotels/ihclcb-icons/ic_copy.svg" class="copyText" alt="copy-icon" />\
							</div>\
	                        <div class="ihclcb-accordion-data">'
					+ 'To be updated'
					+ '</div>\
	                    </div>\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Email ID:<img src="/content/dam/tajhotels/ihclcb-icons/ic_copy.svg" class="copyText" alt="copy-icon" />\
							</div>\
	                        <div class="ihclcb-accordion-data"> '
					+ 'To be updated'
					+ '</div>\
	                    </div>\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Guests</div>\
	                        <div class="ihclcb-accordion-data">'
					+ responseItems[i].Adults_c
					+ '</div>\
	                    </div>\
	                    <div class="col-6 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">GSTIN 18%</div>\
	                        <div class="ihclcb-accordion-data"> '
					+ 'To be updated'
					+ '</div>\
	                    </div>\
	                </div>\
	                <div class="row ihclcb-checkin-data-cont">\
	                    <div class="col-6 col-md-3 ihclcb-accordion-data-ind ">\
	                        <div class="ihclcb-label">Check In <span class="hotel-timing">(2am)</span></div>\
	                        <div class="ihclcb-accordion-data ihclcb-add-right-arrow">'
					+ checkinDate
					+ '</div>\
	                    </div>\
	                    <div class="col-6 col-md-3 ihclcb-accordion-data-ind ">\
	                        <div class="ihclcb-label">Check Out <span class="hotel-timing">(12pm)</span></div>\
	                        <div class="ihclcb-accordion-data">'
					+ checkoutDate
					+ '</div>\
	                    </div>\
	                    <div class="col-6 col-md-3 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Entity</div>\
	                        <div class="ihclcb-accordion-data">'
					+ responseItems[i].Hotel_dcl_c
					+ '</div>\
	                    </div>\
	                    <div class="col-6 col-md-3 ihclcb-accordion-data-ind">\
	                        <div class="ihclcb-label">Total Cost</div>\
	                        <div class="ihclcb-accordion-data ihclcb-price">\
	                            <span>₹</span>\
	                            '
					+ totalCost
					+ '\
	                        </div>\
	                        <div class="ihclcb-price-info">Amount is billed to the company</div>\
	                    </div>\
	                </div>\
	            </div>\
	            <div class="ihclcb-show-more-cont">\
	                <span class="show-more-text">\
	                    Show Details \
	                </span><i class="inline-block icon-drop-down-arrow"> </i>\
	            </div>\
	        </div>\
	    </div>';

			// var currentDateTime = new Date();
			// debugger;
			// if(new Date(currentDateTime) > new
			// Date(responseItems[i].BOOKINGDATE_c)) {
			$(groupPastBookingDetailsDiv).append(template);
			// } else {
			// $(groupUpcomingBookingDetailsDiv).append(template);
			// }

		}
		if(responseItems.length < 5) {
			var heightofWrpr = $(".ihclcb-booking-details-card-wrap").outerHeight()*responseItems.length;
			$(".ihcl-past-bookings-outer-contents").css("min-height",heightofWrpr);
		}
		createPagination(itemsPerPage);
			
	}
	else {
		var noResultsTemplate = '<div class="ihclcb-noresults-container"><h2>No Results Found</h2></div>';
		$(groupPastBookingDetailsDiv).append(noResultsTemplate);
		
		//To destroy Jpages
		$(".holder").jPages("destroy");
	}
}
//Function to create Pagination
function createPagination() {
	$("div.holder").jPages({
		containerID : "ihcl-past-bookings-contents",
		perPage : 5,
		previous : "Back",
		next : "NEXT",
		keyBrowse : true,
		animation : "fadeInDown"
	});
}
//Initialize DatePicker
function intiateFilterDatepicker() {

	$('.ihclcb-bookings-filter-container .input-daterange').datepicker({
		format : 'dd M yy',
		autoclose : true,
		container : $('.ihclcb-filter-datepicker-cont'),
		todayHighlight : true,
		templates : {
            leftArrow : '<i class="icon-prev-arrow inline-block"></i>',
            rightArrow : '<i class="icon-prev-arrow inline-block cm-rotate-icon-180 v-middle"></i>'
        }
	});

	var today = new Date();
	var d2 = moment(today).add(1, 'days')['_d'];
	// $("#ihclcb-filter-from-cal").datepicker("setDate", today);
	// $("#ihclcb-filter-to-cal").datepicker("setDate", d2);

	$('.ihclcb-bookings-filter-container #ihclcb-filter-from-cal')
			.on(
					'change',
					function(e) {
						var currVal = $(this).val();
						var nextDate = moment(currVal, "D MMM YY").add(1,'days')['_d'];
						$(".ihclcb-bookings-filter-container #ihclcb-filter-to-cal").datepicker("setDate", nextDate);
						$('.ihclcb-bookings-filter-container #ihclcb-filter-to-cal').focus();
						var choosedfromDate = $(this).closest(
								'[data-filter-drpdwn]').attr(
								'data-filter-drpdwn');
						$('[data-filtered-value=' + choosedfromDate + ']')
								.addClass('ihcl-filter-applied').find(
										'.filtered-value').text(currVal);
					});
	$('.ihclcb-bookings-filter-container #ihclcb-filter-to-cal').on(
			'change',
			function(e) {
				var nextVal = $(this).val();
				var choosedtoDate = $(this).closest('[data-filter-drpdwn]')
						.attr('data-filter-drpdwn');
				$('[data-filtered-value=' + choosedtoDate + ']').addClass(
						'ihcl-filter-applied').find('.filtered-value').text(
						nextVal);
			});
}
