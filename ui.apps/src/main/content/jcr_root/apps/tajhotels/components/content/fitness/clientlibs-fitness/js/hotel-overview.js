$( document ).ready( function() {

    var elServicesCollapsed = $( '.ho-services-collapsed' );
    var elServicesExpanded = $( '.ho-services-expanded' );
    var btnShowSm = $( '.ho-services-sm-show-all' );
    var btnHideSm = $( '.ho-services-sm-show-less' );
    var hotelOverlayDescriptionTxt = $( '.ho-description-txt' );
    if ( hotelOverlayDescriptionTxt.length > 0 ) {
        hotelOverlayDescriptionTxt.cmToggleText( {
            charLimit: 200,
            showVal: "Show More",
            hideVal: "Show Less",
        } );
    }

    function expandServices() {
        if ( elServicesExpanded.hasClass( 'cm-hide' ) ) {
            setTimeout( function() {
                btnShowSm.addClass( 'cm-hide' );
                btnHideSm.removeClass( 'cm-hide' );
                elServicesCollapsed.addClass( 'cm-hide' );
                elServicesExpanded.removeClass( 'cm-hide' );
            }, 100 );
        }
    };

    function collapseServices() {
        if ( elServicesCollapsed.hasClass( 'cm-hide' ) ) {
            setTimeout( function() {
                btnShowSm.removeClass( 'cm-hide' );
                btnHideSm.addClass( 'cm-hide' );
                elServicesCollapsed.removeClass( 'cm-hide' );
                elServicesExpanded.addClass( 'cm-hide' );
            }, 100 );
        }
    };

    $( '.ho-services-show-all' ).on( 'click', function() {
        expandServices();
    } );

    $( '.ho-services-show-less' ).on( 'click', function() {
        collapseServices();
    } );

    btnShowSm.on( 'click', function() {
        expandServices();
    } );

    btnHideSm.on( 'click', function() {
        collapseServices();
    } );
    var hotelLatitude = 18.9217;
    var hotelLongitude = 72.8330;
    var mapViewLink = "https://www.google.com/maps/search/?api=1&query=" + hotelLatitude + "," + hotelLongitude;

    $( '.ho-hotel-location-link' ).attr( 'href', mapViewLink );


    //For Hotel carousel
    $( '.ho-photo-carousel-link, .ho-view-gallery-txt' ).on( 'click', function() {
        $( '.hotel-overview-container' ).find( '.mr-menu-carousel-overlay' ).removeClass( 'mr-overlay-initial-none' );
    } );
        

} );