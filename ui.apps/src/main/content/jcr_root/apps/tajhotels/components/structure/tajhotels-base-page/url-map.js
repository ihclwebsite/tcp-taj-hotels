use(function() {
    if (this.reqUrl) {
        var res1=pageManager.getPage(this.reqUrl)
        if(res1!=null){
            response = request.resourceResolver.map(this.reqUrl+".html");
        }else{
			log.info("Not a mapped URL returning the same :"+this.reqUrl);
			response = this.reqUrl;
        }
    } else {
        response = this.reqUrl
    }

    if (response!=null && response.startsWith('https://www.tajhotels.com')) {
        response = response.substring(25)
    } else if (response!=null && response.startsWith('https://www.vivantahotels.com')) {
        response = response.substring(29)
    } else if (response!=null && response.startsWith('https://www.seleqtionshotels.com')) {
        response = response.substring(32)
    }

    return {
        mappedUrl : response
    };
});