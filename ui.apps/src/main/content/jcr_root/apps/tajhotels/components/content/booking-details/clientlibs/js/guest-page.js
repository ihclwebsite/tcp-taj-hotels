$(document).ready(
        function() {

            // Handle voucher related fields
            var voucherRedemptions = dataCache.session.getData('voucherRedemption');
            if(voucherRedemptions && voucherRedemptions == 'true'){
                $('#guest-VoucherCode').parent('.col-md-4.sub-form-input-wrp').show();
                $('#guest-VoucherPin').parent('.col-md-4.sub-form-input-wrp').show();
            }
            
            if (dataCache.session.getData('bookingOptions') === null
                    && sessionStorage.getItem('bookingOptions') === null) {
                console.info('reservation details not found');
            } else {
                if ($('#corporate-ihclcb-checkout').attr('data-corporate-checkout') == 'false'
                        || !$('#corporate-ihclcb-checkout').attr('data-corporate-checkout')) {
                    // $('.guest-detail-ihclcb').remove();
                    buildGuestPage();
                } else {
                    ihclcbBuildGuestPage();
                }
            }
            // Populate Booker ID in form
            var bookingDetailsBooker = getUserData();
            if(bookingDetailsBooker && bookingDetailsBooker.profileId) {
				$("#tic-booker-id-ihclcb").val(bookingDetailsBooker.profileId);
            }
        });

function buildGuestPage() {
    try {
        var reservationDetailsJson = dataCache.session.getData('bookingOptions');
        var modifyBookingState = dataCache.session.getData('modifyBookingState');
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');

        var contentRootPath = $('#content-root-path').val();
        if (contentRootPath == "" || contentRootPath == null || contentRootPath == 'undefined') {
            contentRootPath = "/content/tajhotels";
        }
        if (reservationDetailsJson != 'null') {

            var guestDetails = reservationDetailsJson.guest;
            if (guestDetails) {
                $('#bookingGuestTitle').find('option[value="' + guestDetails.title + '"]').attr('selected', 'selected');
                $('#bookingGuestTitle').data("selectBox-selectBoxIt").refresh();
                $('#guest-firstName').val(guestDetails.firstName);
                $('#guest-lastName').val(guestDetails.lastName);
                $('#guest-Email').val(guestDetails.email);
                $('#bookingGuestCountry').val(guestDetails.country);
                $('#guest-PhoneNumber').val(guestDetails.phoneNumber);
                $('#guest-MembershipNumber').val(guestDetails.membershipNo);
                $('#special-request').val(guestDetails.specialRequests);
                if (typeof guestDetails.gstNumber != 'udefined' || guestDetails.gstNumber != '') {
                    $('#guest-GSTNumber').val(guestDetails.gstNumber);
                }
            }
            // [TIC-FLOW]
            var userDetails = getUserData();
            if (userDetails) {
                if (userDetails.title) {
                    $('#bookingGuestTitle').selectBoxIt('selectOption', userDetails.title);
                    $('#bookingGuestTitle').prop('disabled', true);
                }
                if (userDetails.firstName) {
                    $('#guest-firstName').val(userDetails.firstName);
                    $('#guest-firstName').prop('disabled', true);
                }
                if (userDetails.lastName) {
                    $('#guest-lastName').val(userDetails.lastName);
                    $('#guest-lastName').prop('disabled', true);
                }
                if (userDetails.email) {
                    $('#guest-Email').val(userDetails.email);
                    $('#guest-Email').prop('disabled', true);
                }
                if (userDetails.mobile) {
                    // $('#bookingGuestCountry').val(userDetails.country);
                    $('#guest-PhoneNumber').val(userDetails.mobile);
                    $('#guest-PhoneNumber').prop('disabled', true);
                }
                if (userDetails.membershipId) {
                    $('#guest-MembershipNumber').val(userDetails.membershipId);
                    $('#guest-MembershipNumber').prop('disabled', true);
                }
                if (userDetails.card && userDetails.card.type) {
                    var membership = userDetails.card.type;
                    if (membership === "TAP") {
                        $('.guest-MembershipNumber ').text("TAP Membership Number");
                    } else if (membership === "TAPPMe") {
                        $('.guest-MembershipNumber ').text("TAPP Me Membership Number");
                    }
                }

                if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
                    $(".tic-info-label").removeClass('d-none');
                    $(".book-for-tic-wrp").removeClass('d-none');
                    $(".cm-page-container").addClass('tic-room-redemption-fix');
                }

            }

            if (modifyBookingState == "modifyGuest") {
                $('.policy-terms-external-wrapper').hide();
                $("#btn-save").html("SAVE");
                var button = document.getElementById("btn-save");
                button.style.display = "inline-block";
                $('#btn-proceed').hide();

                // ajax call to save the modified the guestDetails

                $("#btn-save")
                        .click(
                                function() {
                                    $(this).hide();
                                    $(this).siblings('.taj-loader').show();
                                    var jsonObject = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
                                    var guest = jsonObject.guest;
                                    guest.title = document.getElementById("bookingGuestTitle").value;
                                    guest.firstName = document.getElementById("guest-firstName").value;
                                    guest.lastName = document.getElementById("guest-lastName").value;
                                    guest.email = document.getElementById("guest-Email").value;
                                    guest.country = document.getElementById("bookingGuestCountry").value;
                                    guest.phoneNumber = document.getElementById("guest-PhoneNumber").value;
                                    guest.membershipNo = document.getElementById("guest-MembershipNumber").value;
                                    guest.gstNumber = document.getElementById("guest-GSTNumber").value;
                                    guest.specialRequests = document.getElementById("special-request").value;

                                    jsonObject.guest = guest;

                                    $
                                            .ajax({
                                                type : 'GET',
                                                url : '/bin/modifyReservation',
                                                data : {
                                                    'modifyJson' : JSON.stringify(jsonObject),
                                                    'contentRootPath' : contentRootPath
                                                },
                                                success : function(data) {
                                                    var cancelJson = data;
                                                    dataCache.session.setData('bookingDetailsRequest', data);
                                                    window.location
                                                            .assign("/content/tajhotels/en-in/booking-confirmation.html?fromFindReservation=true");

                                                },
                                                failure : function(data) {
                                                    console.log('failure');
                                                    $('.statusdetails').html("<strong>XML not updated</strong>");
                                                    $(this).show();
                                                    $(this).siblings('.taj-loader').hide();
                                                    var popupParams = {
                                                        title : 'Guest Details Not Updated. Try again.',
                                                        callBack : '',
                                                        needsCta : false,
                                                        isWarning : true
                                                    }
                                                    warningBox(popupParams);
                                                }
                                            });
                                });
            }
        }
    } catch (error) {
        console.error(error);
    }
}

function ihclcbBuildGuestPage() {

    // $('.guest-detail-non-ihclcb').remove();
    var userDetailsIhclcb = getUserData();
    // if (userDetailsIhclcb && userDetailsIhclcb.email) {
    // $('.ihclcb-booker-email').text(userDetailsIhclcb.email);
    // }
    var userDetailsIHCLCB = getUserData();
    var isIhclcbFlagSet = dataCache.session.getData("ihclCbBookingObject");
    var $membershipIdInput = $("#tic-member-id-ihclcb")
    $membershipIdInput.prop('disabled', false);

    if (userDetailsIHCLCB && isIhclcbFlagSet && isIhclcbFlagSet.isIHCLCBFlow) {
        try {
            $('.guest-detail-ihclcb #bookingGuestTitle').prop('disabled', true);
            $('.guest-detail-ihclcb #bookingGuestTitle').selectBoxIt('selectOption', 1);
            $('.guest-detail-ihclcb #guest-firstName').prop('disabled', true);
            $('.guest-detail-ihclcb #guest-lastName').prop('disabled', true);
            $('.guest-detail-ihclcb #guest-Email').prop('disabled', true);
            $('.guest-detail-ihclcb #guest-PhoneNumber').prop('disabled', true);

            $membershipIdInput.blur(function() {

                var enteredValue = $(this).val();
                if (enteredValue === "" || enteredValue == undefined || enteredValue == null) {
                    return;
                }
                // var userDetailsIHCLCB = getUserData();
                // if (userDetailsIHCLCB) {
                if (userDetailsIHCLCB.enteredMemberShipId && userDetailsIHCLCB.enteredMemberShipId == enteredValue) {
                    autoFillData(userDetailsIHCLCB.enteredGuestDetails);
                    return;
                }
                // }

                var custLoyaltyJSON = {
                    "CustLoyalty" : enteredValue
                }

                var ihclcbB2CHostApiUrl = $('.Ihclcb-Corporate-Booking-GuestApi').attr('data-ihclcbApiHostUrl')
                var ihclcbB2CEndpointUrl = $('.Ihclcb-Corporate-Booking-GuestApi').attr('data-ihclcbGuestApiUrl');

                $('body').showLoader();
                $.ajax({

                    url : ihclcbB2CHostApiUrl + ihclcbB2CEndpointUrl,
                    headers : {
                        "Authorization" : "Basic cmFqLnNyaW5pdmFzYW5AaW5ub3ZhY3guY29tOlNtaWxlQDI1",
                        "content-Type" : "application/json; charset=UTF-8"
                    },
                    type : "POST",
                    dataType : 'json',
                    data : JSON.stringify(custLoyaltyJSON),
                }).done(
                        function(res) {
                            if (res && res.GuestDetails && res.GuestDetails[0]) {
                                $membershipIdInput.removeClass('invalid-input');
                                storeGuestDetailsInLocal(res.GuestDetails[0], enteredValue);
                                autoFillData(res.GuestDetails[0]);
                            } else {
                                $membershipIdInput.addClass('invalid-input');
                                clearGuestData();
                                console.log('Sorry! Cant fetch the data for entered input');
                                $('#tic-member-id-ihclcb + .sub-form-input-warning').text(
                                        'Sorry! Cant fetch the data for entered input');
                            }
                        }).fail(function(res) {
                    clearGuestData();
                    $membershipIdInput.addClass('invalid-input');
                }).always(function() {
                    $('body').hideLoader();
                    $([ document.documentElement, document.body ]).animate({
                        scrollTop : $(".checkout-payment-details-container ").offset().top
                    }, 600);
                });
            });
        } catch (error) {
            console.error(error);
        }
    }

    function clearGuestData() {
        $('#corporate-ihclcb-checkout #bookingGuestTitle').prop('disabled', true);
        $('#corporate-ihclcb-checkout #guest-firstName').prop('disabled', true).val("");
        $('#corporate-ihclcb-checkout #guest-lastName').prop('disabled', true).val("");
        $('#corporate-ihclcb-checkout #guest-Email').prop('disabled', true).val("");
        $('#corporate-ihclcb-checkout #guest-PhoneNumber').prop('disabled', true).val("");
    }

    function storeGuestDetailsInLocal(res, enteredMemberShipId) {
        var userDetailsIHCLCB = getUserData();
        if (userDetailsIHCLCB) {
            var enteredGuestDetails = new Object();
            enteredGuestDetails.FirstName = res.FirstName;
            enteredGuestDetails.LastName = res.LastName;
            enteredGuestDetails.Email = res.Email;
            enteredGuestDetails.Phone = res.Phone;
            enteredGuestDetails.title = res.SalutoryIntroduction;
            userDetailsIHCLCB.enteredGuestDetails = enteredGuestDetails;
            userDetailsIHCLCB.enteredMemberShipId = enteredMemberShipId;
            dataCache.local.setData("userDetails", userDetailsIHCLCB);
        }
    }

    function autoFillData(guestDetails) {
        try {
            var title = guestDetails.title || guestDetails.SalutoryIntroduction;
            if (title) {
                $('.guest-detail-ihclcb #bookingGuestTitle').selectBoxIt('selectOption', title);
                $('.guest-detail-ihclcb #bookingGuestTitle').prop('disabled', true);
            }
            if (guestDetails.FirstName) {
                $('.guest-detail-ihclcb #guest-firstName').prop('disabled', true).val(guestDetails.FirstName);
            }

            if (guestDetails.LastName) {
                $('.guest-detail-ihclcb #guest-lastName').prop('disabled', true).val(guestDetails.LastName);
            }
            if (guestDetails.Email) {
                for (var i = 0; i < guestDetails.Email.length; i++) {
                    if (guestDetails.Email[i].PrimaryFlag && guestDetails.Email[i].EmailAddress) {
                        $('.guest-detail-ihclcb #guest-Email').prop('disabled', true).val(
                                guestDetails.Email[i].EmailAddress);
                    }
                }
            }
            if (guestDetails.Phone) {
                for (var j = 0; j < guestDetails.Phone.length; j++) {
                    if (guestDetails.Phone[j].PrimaryFlag && guestDetails.Phone[j].PhoneNumber) {
                        $('.guest-detail-ihclcb #guest-PhoneNumber').prop('disabled', true).val(
                                guestDetails.Phone[j].PhoneNumber);
                    }
                }
            } else {
                $('.guest-detail-ihclcb #guest-PhoneNumber').prop('disabled', false);
            }
        } catch (error) {
            console.error(error);
        }
    }
}
