$( document ).ready( function() {
    $( '.card-show-more-button' ).unbind().click( function( e ) {
        var wrapper = $( this ).siblings( '.show-more-card-wrapper' );
        var cards = $( wrapper ).find( '.show-more-card-element' );
        var cardsNum = cards.length;
        var rowNum;
        var windowWidth = $(window).width();
        var reduceHt = false;
        if(windowWidth < 768){
            rowNum = 1;
        }
        else if(windowWidth < 992){
            rowNum = 2;
            reduceHt = true;
        }
        else{
            rowNum =3;
        }
        var singleCard = $(wrapper).find('.show-more-card-element:first');
        var cardHeight = singleCard.outerHeight(true);
        var heightLimit = reduceHt ? cardHeight*5-5: cardHeight*5;
        var totalHeight = (cardsNum/rowNum)*cardHeight;
        $( cards ).toggleClass( 'show-more-card-element-active' );

        var textReference = $( this ).find( 'span' );
        var iconReference = $( this ).find( 'img' );
        if ( textReference.text() == "SHOW MORE" ) {
            wrapperHeight = wrapper.height();
            if(totalHeight <= wrapperHeight){
                textReference.text( "SHOW LESS" );
                iconReference.toggleClass( 'cm-rotate-show-more-icon' );
                wrapper.css('maxHeight', 'unset');
            }
            else{
                wrapper.css('maxHeight', wrapperHeight + heightLimit);
                if(totalHeight <= wrapper.height()){
                    textReference.text( "SHOW LESS" );
                    iconReference.toggleClass( 'cm-rotate-show-more-icon' );
                }
            }
        } else {
            textReference.text( "SHOW MORE" );
            iconReference.toggleClass( 'cm-rotate-show-more-icon' );
            wrapper.css('maxHeight', heightLimit);
            this.scrollIntoView();
        }
    } );
    setInitialHeights();
    $(window).on('resize', setInitialHeights);
    function setInitialHeights(){
        var showMoreContents = $('.card-show-more-button');
        showMoreContents.each(function(){
            var showMoreWrapper = $(this).siblings( '.show-more-card-wrapper' );
            var cardsNum = $( showMoreWrapper ).find('.show-more-card-element').length;
            var singleCard = $(showMoreWrapper).find('.show-more-card-element:first');
            var cardHeight = singleCard.outerHeight(true);
            var rowNum;
            var reduceHt = false;
            var textReference = $( this ).find( 'span' );
            var iconReference = $( this ).find( 'img' );
            console.log("text: " , textReference);
            if(textReference.text() == "SHOW LESS"){
                console.log("inside here");
                iconReference.toggleClass( 'cm-rotate-show-more-icon' );
            }
            textReference.text( "SHOW MORE" );
            if($(window).width() < 768){
                rowNum = 1;
            }
            else if(768 <= $(window).width() < 992){
                rowNum = 2;
                reduceHt = true;
            }
            else if( $(window).width() >= 992){
                rowNum =3;
            }
            var heightLimit = reduceHt ? cardHeight*5-5: cardHeight*5;
            var totalHeight = (cardsNum/rowNum)*cardHeight;
            if(totalHeight > heightLimit){
                showMoreWrapper.css('maxHeight', heightLimit);
                $(this).removeClass('no-display-show-more-btn');
            }
        })
    }
} );
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHRzL2NhcmQtc2hvdy1tb3JlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoIGRvY3VtZW50ICkucmVhZHkoIGZ1bmN0aW9uKCkge1xyXG4gICAgJCggJy5jYXJkLXNob3ctbW9yZS1idXR0b24nICkuY2xpY2soIGZ1bmN0aW9uKCBlICkge1xyXG4gICAgICAgIHZhciB3cmFwcGVyID0gJCggdGhpcyApLnNpYmxpbmdzKCAnLnNob3ctbW9yZS1jYXJkLXdyYXBwZXInICk7XHJcbiAgICAgICAgdmFyIGNhcmRzID0gJCggd3JhcHBlciApLmZpbmQoICcuc2hvdy1tb3JlLWNhcmQtZWxlbWVudCcgKTtcclxuICAgICAgICAkKCBjYXJkcyApLnRvZ2dsZUNsYXNzKCAnc2hvdy1tb3JlLWNhcmQtZWxlbWVudC1hY3RpdmUnICk7XHJcblxyXG4gICAgICAgIHZhciB0ZXh0UmVmZXJlbmNlID0gJCggdGhpcyApLmZpbmQoICdzcGFuJyApO1xyXG4gICAgICAgIHZhciBpY29uUmVmZXJlbmNlID0gJCggdGhpcyApLmZpbmQoICdpbWcnICk7XHJcbiAgICAgICAgaWYgKCB0ZXh0UmVmZXJlbmNlLnRleHQoKSA9PSBcIlNIT1cgTU9SRVwiICkge1xyXG4gICAgICAgICAgICB0ZXh0UmVmZXJlbmNlLnRleHQoIFwiU0hPVyBMRVNTXCIgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0ZXh0UmVmZXJlbmNlLnRleHQoIFwiU0hPVyBNT1JFXCIgKTtcclxuICAgICAgICAgICAgdGhpcy5zY3JvbGxJbnRvVmlldygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpY29uUmVmZXJlbmNlLnRvZ2dsZUNsYXNzKCAnY20tcm90YXRlLXNob3ctbW9yZS1pY29uJyApO1xyXG4gICAgfSApO1xyXG59ICk7Il0sImZpbGUiOiJzY3JpcHRzL2NhcmQtc2hvdy1tb3JlLmpzIn0=
