$('document').ready(function ($) {	
        $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');
            
            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });

    	checkUserDetailsForHeader();

        /* mobile menu */
        $(document).on('click','.mobNavBtn',function(){
            $('.mobNavLink').addClass('menuBoxIn');
        });

        $(document).on('click','.navCloseBtn, .blkOvrly',function(){
            $('.mobNavLink').removeClass('menuBoxIn');
        });

        var user = userCacheExists();
        if (user) {
            showHeaderUserProfile(user.name);
        } else {
            checkUserDetailsForHeader();
        }

    	/* For Sign in and fetch details */
        $('.sign-in-btn').click(function() {
        	console.log('clicked for sign-in');
            $('body').trigger('taj:sign-in');
        });

        $('body').on('taj:loginSuccess', function(event, id, name) {
            showHeaderUserProfile(name);
        });
        
        $('body').on('taj:pointsUpdated', function(event) {
            showUserPoints();
        });

        function showHeaderUserProfile(name) {
            $('.sign-in-btn').addClass('cm-hide');
            $('.header-profile').removeClass('cm-hide').addClass('cm-show');
            $('.header-profile .profile-username, .navbar-brand .profile-username').text(name);
            showUserPoints();
        }

        function showUserPoints() {
            var userDetails = dataCache.local.getData("userDetails");
            if (userDetails.membershipId) {
                $('.header-profile .points-cont').removeClass('d-none');
                $('[data-component-id="enrol-btn"]').remove(); // remove enrol buttons for users having membership id
                $('.header-profile .edit-profile').hide();
                if (userDetails.tier) {
                    $('.header-profile .tic-tier span').text(userDetails.tier);
                    $('.header-profile .tic-tier').show();
                } else {
                    $('.header-profile .tic-tier').hide();
                }
                $('.header-profile .tic-points').text(userDetails.ticPoints);
                $('.header-profile .epicure-points').text(userDetails.epicurePoints);
                $('.header-profile .tap-points').text(userDetails.tapPoints);
                $('.header-profile .tappme-points').text(userDetails.tappmePoints);
                if (userDetails.card) {
                    var accountType = userDetails.card.type.toLowerCase() + "-points";
                    accountType = accountType.includes("tic") ? "tic-points" : accountType;
                    $('.prof-content-value').each(function() {
                        $(this).attr("id") === accountType ? $(this).parent().show() : $(this).parent().hide();
                    });
                    
                    if (userDetails.card.type === "TAP" || userDetails.card.type === "TAPPMe") {
                        $('.tic-tier').remove();
                    }
                    $('.prof-tic-content').show();
                } else {
                    console.log("unable to retrieve user card details");
                    $('.prof-tic-content').hide();
                }
            } else {
                $('.header-profile .points-cont').addClass('d-none');
            }
        }


    	/* Logout */
    	$('.header-profile .logout-btn').on('click', function(event) {
            event.stopPropagation();
            tajLogout();
        });
    
        $('body').on('taj:logout', function() {
            tajLogout();
        });
    	$('body').on('taj:sessionLogout', function(){
			logoutWithoutReloding();
    	});	

    
        function tajLogout() {
            var signOutRedirect = $("[data-redirect-link]").data("redirect-link");
            logoutSuccess(signOutRedirect);
        }

    	function logoutSuccess(redirectUrl) {
            logoutWithoutReloding();
            document.location.reload();
        }


    	function logoutWithoutReloding() {
            var isCorporateLogin = userCacheExists() ? userCacheExists().isCorporateLogin : false;
            dataCache.local.removeData("userDetails");
    		showSignInAndEnroll();
            if (!isCorporateLogin) {

                if($.removeCookie) {
                    $.removeCookie('ihcl-sso-token', {
                        path : '/',
                        domain : location.hostname
                    });
                }
                else {
					document.cookie = "ihcl-sso-token=" + ";max-age=0" +";path=/" + ";domain=" + location.hostname;
                }
                
                var imgTagForCookieCalls = $('#single-sign-out');
                $(".sso-urls-to-set-cookie").each(
                    function() {
                        var indTagForCookieCall = '<img src="'
                        + $(this).text()
                        + $('.single-sign-out-sevlet-uri').text()
                        + '" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;"></img>';
                        imgTagForCookieCalls.append(indTagForCookieCall);
                        console.log(indTagForCookieCall);
                        console.log(indTagForCookieCall);
                    });
                $('#single-sign-out').html(imgTagForCookieCalls);
                console.log('single-sign-out: logoutSuccess() and isCorporateLogin: false');
            } else {
                dataCache.session.removeData("ihclCbBookingObject");
            }
        }


    //SSO
    	function checkUserDetailsForHeader() {
            var user = userCacheExists();
            var isCorporateLogin = false;
            var showSignIN = true;
            hideSignInAndEnroll();
			var ihclSSOToken;

            if($.cookie) {
				ihclSSOToken = $.cookie('ihcl-sso-token');
            }
            else {
				ihclSSOToken = getSSOTokenCookie('ihcl-sso-token');
            }
            if (isIHCLCBSite()) {
                console.log('isIHCLCBSite: true');
                if (user) {
                    isCorporateLogin = user.isCorporateLogin;
                    showHeaderUserProfile(user.name);
                } else {
                    console.log('user.isCorporateLogin: false');
                    dataCache.local.removeData("userDetails");
                    clearRoomSelections();
                    showSignInAndEnroll();
                }
            } else if (user && user.authToken && !user.isCorporateLogin) {
                console.log('user.authToken: true && isCorporateLogin: false');
                if (user.authToken === ihclSSOToken) {
                    console.log('user.authToken === ihclSSOToken: true');
                    showHeaderUserProfile(user.name);
                }
                /*
                else if(user && user.authToken){
                    console.log('user.authToken != ihclSSOToken; authtoken is present');
                    showHeaderUserProfile(user.name);
                }
                else if (ihclSSOToken) {
                    console.log('ihclSSOToken: true');
                    getUserDetailsUsingToken(ihclSSOToken);
                }
                */
                else {
                    console.log('user.authToken === ihclSSOToken: false && ihclSSOToken: false');
                    dataCache.local.removeData("userDetails");
                    clearSelectionAndLogout();
                    showSignInAndEnroll();
                }
            } else if (ihclSSOToken) {
                console.log('ihclSSOToken: true');
                getUserDetailsUsingToken(ihclSSOToken);
            } else {
                console.log('SSO final else condition');
                showSignInAndEnroll();
            }

    	}

    	// fallback function for getting cookie value
        function getSSOTokenCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }

        function hideSignInAndEnroll() {
            $('.sign-in-btn').addClass('cm-hide');
            $('[data-component-id="enrol-btn"]').addClass('cm-hide');
        }
        
        function showSignInAndEnroll() {
            $('.sign-in-btn').removeClass('cm-hide');
            $('[data-component-id="enrol-btn"]').removeClass('cm-hide');
            hideProfileDetails();
        }

        function hideProfileDetails(){
    		$('.header-profile').addClass('cm-hide').removeClass('cm-show');
        }
        
        function getUserDetailsUsingToken(ihclSSOToken) {
            showLoader();
            var showSignIN = true;
            $.ajax({
                type : "POST",
                url : "/bin/fetchUserDetails",
                data : "authToken=" + encodeURIComponent(ihclSSOToken)
            }).done(function(res) {
                res = JSON.parse(res);
                if (res.userDetails && res.userDetails.name) {
                    updateLoginDetails(res);
                    showSignIN = false;
                }
                hideLoader();
            }).fail(function(res) {
                if(res.status === 401){
                    forceLogoutAfterUnauthorized();
                } 
            }).always(function() {
                if (showSignIN) {
                    showSignInAndEnroll();
                }
                hideLoader();
            });
        }
    
    
        function updateLoginDetails(res) {
            if (res.authToken) {
                var userDetails = res.userDetails;
                var authToken = res.authToken;
                incorrectLoginCount = 0;
                successHandler(authToken, userDetails);
            } else if (res.errorCode === "INVALID_USER_STATUS" && res.status === "504" && !entityLogin) {
                // user activation flow
                invokeActivateAccount();
            } else if (res.errorCode === "INVALID_USER_STATUS" && res.status === "506" && !entityLogin) {
                // migrated user
                var error = res.error;
                var errorCtaText = "RESET PASSWORD";
                var errorCtaCallback = invokeForgotPassword;
                $('body').trigger('taj:loginFailed', [ error, errorCtaText, errorCtaCallback ]);
            } else {
                if (entityLogin) {
                    forgotPasswordLinkWrp.show();
                    $('.ihclcb-login-error').text(res.error).show();
                }
            }
        }
    
    
        function successHandler(authToken, userDetails) {
            var isentityLogin = $('#corporate-booking-login').attr('data-corporate-isCorporateLogin') == "true";
            localUserDetails(authToken, userDetails);
            var id = userDetails.membershipId;
            var name = userDetails.name;
            $('.generic-signin-close-icon').trigger("click");
            $('body').trigger('taj:loginSuccess', [ id, name ]);
            
            if (id) {
                $('body').trigger('taj:fetch-profile-details', [ true ]);
            } else {
                $('body').trigger('taj:login-fetch-complete');
            }
            if (!isentityLogin) { // added by sarath
                $('body').trigger('taj:tier');
            }
            hideLoader();
        }


        function localUserDetails(authToken, userDetails) {
            var isentityLogin = $('#corporate-booking-login').attr('data-corporate-isCorporateLogin') == "true";
            var user = {
                authToken : authToken,
                name : userDetails.name,
                firstName : userDetails.firstName,
                lastName : userDetails.lastName,
                gender : userDetails.gender,
                email : userDetails.email,
                countryCode : userDetails.countryCode,
                mobile : userDetails.mobile,
                cdmReferenceId : userDetails.cdmReferenceId,
                membershipId : userDetails.membershipId,
                googleLinked : userDetails.googleLinked,
                facebookLinked : userDetails.facebookLinked,
                title : userDetails.title
            };
            if(userDetails.tier) {
                user.tier =  userDetails.tier
            } else if(userDetails.card && userDetails.card.tier){
                user.tier = userDetails.card.tie;
            }
            if (isentityLogin) {
                user.partyId = userDetails.cdmReferenceId
            }
            dataCache.local.setData("userDetails", user);
            if ($('.mr-contact-whole-wrapper').length > 0) {
                window.location.reload();
            }
        }

});


