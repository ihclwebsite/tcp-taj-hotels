$('document').ready(function() {

    var spaButtons = $('.jiva-spa-details-book-btn');
    spaButtons.each(function() {
        $(this).on('click', function() {
            setSpaAndHotelDataInSession($(this));
        });
    });
    $('.events-cards-request-btn').each(function(element) {
        $(this).hide();
    });
    var requestQuote = $('.events-cards-request-btn, .meeting-request-quote-btn');
    requestQuote.each(function() {
        $(this).on('click', function() {
            setRequestQuoteDataInSession($(this));
        });
    });
    $('.meeting-card-wait-spinner').each(function(element) {
        $(this).hide();
    });
    $('.events-cards-request-btn').each(function(element) {
        $(this).show();
    });
});

function setRequestQuoteDataInSession(clickEvent) {
    var meetingRequestQuoteData = {};

    var ind = $(clickEvent).closest(".events-pg-filter-inner-wrap");

    // Hotel properties
    var requestQuoteEmailId = $(ind).find(".request-quote-email-id").text();

    meetingRequestQuoteData.requestQuoteEmailId = requestQuoteEmailId;
    dataCache.session.setData('meetingRequestQuoteData', meetingRequestQuoteData);
}

function setSpaAndHotelDataInSession(clickEvent) {
    var spaOptions = {};

    var ind = $(clickEvent).closest(".jiva-spa-card-details-wrap");
    var spaName = $(ind).find(".jiva-spa-details-heading").text();
    var spaDuration = $(ind).find(".spa-duration").text();
    var spaCurr = $(ind).find(".spaCurr").text();
    var spaAmount = $(ind).find(".spaAmount").text();
    var spaAmountDetails = spaCurr + spaAmount;

    // Hotel properties
    var hotelName = $(ind).find(".hotel-name").text();
    var hotelCity = $(ind).find(".hotel-city").text();
    var hotelEmailId = $(ind).find(".hotel-email-id").text();
    var jivaSpaEmailId = $(ind).find(".jiva-spa-email-id").text();
    var requestQuoteEmailId = $(ind).find(".request-quote-email-id").text();
    spaOptions.spaName = spaName;
    spaOptions.spaDuration = spaDuration;
    spaOptions.spaAmount = spaAmountDetails;
    spaOptions.hotelName = hotelName;
    spaOptions.hotelCity = hotelCity;
    spaOptions.hotelEmailId = hotelEmailId;
    spaOptions.jivaSpaEmailId = jivaSpaEmailId;
    spaOptions.requestQuoteEmailId = requestQuoteEmailId;
    dataCache.session.setData('spaOptions', spaOptions);
}
