function redeemFormValidate() {
    $('.mr-selectbox').selectBoxIt();
    var $mailinput = $("#mailinput");
    var $addressInput = $("#addressinput");
    var $addressOnlineInput = $("#memberAddress");
    var $suggestionDivision = $(".suggestion-division");
    var $selectBtn = $('.mr-select-btn');
    var $Addressaddition = $('.mr-address-addition');
    var flag;
    var $suggestionList = $('.suggestion-list');
    var $inputField = $('.mr-input-field');

    $Addressaddition.on('click', function() {
        $addressInput.trigger('focus');
        $addressInput.val("");
        $addressOnlineInput.trigger('focus');
        $addressOnlineInput.val("");
        $suggestionDivision.removeClass('warning-display');
    });

    $suggestionDivision.delegate(".mr-select-btn", 'click', function() {
        var selectedAddress = $(this).prev($suggestionDivision).text();
        $(this).closest($suggestionDivision).siblings($addressInput).val(selectedAddress);
        $(this).closest($suggestionDivision).siblings($addressOnlineInput).val(selectedAddress);
    });

    $addressInput.on('click', function(e) {
        $suggestionDivision.addClass('warning-display');
        e.stopPropagation();
    });

    $addressOnlineInput.on('click', function(e) {
        $suggestionDivision.addClass('warning-display');
        e.stopPropagation();
    });

    $(document).click(function() {
        $suggestionDivision.removeClass('warning-display');
    });

    $($suggestionDivision).click(function(event) {
        event.stopPropagation();
        console.log('the addresses is changed');
        var array = $('#addressinput').val().split(',');
        if (array.length == 5 && array.every(function(element) {
            return !!element;
        })) {

            $('#stateinput').val(array[3]);
            $('#cityinput').val(array[2]);
            $('#zipcodeinput').val(array[4]);
        }
       
    })
    $("#addressinput").keyup(function() {
            var array = $('#addressinput').val().split(',');
            if (array.length == 5 && array.every(function(element) {
                return !!element;
            })) {

                $('#stateinput').val(array[3]);
                $('#cityinput').val(array[2]);
                $('#zipcodeinput').val(array[4]);
            }
        });

    $mailinput.on('focus', function() {
        $(this).parent().siblings('.warning-text').removeClass('warning-display');
    })
    $mailinput.on('blur', function() {
        var emailaddress = $mailinput.val();
        if (!validateEmail(emailaddress)) {
            $(this).parent().siblings('.warning-text').addClass('warning-display');
        }
    });

    function validateInputs() {
        var flag = true;
        $inputField.each(function() {
            if ($(this).val() == "") {
                $(this).siblings('.validate-inputs-txt').addClass('warning-display');
                flag = false;

            }
        });
        return flag;
    }
    $inputField.on('focus', function() {
        $(this).siblings('.validate-inputs-txt').removeClass('warning-display');
    });
    // validateInputs();
    $('.pay-btn').on(
            'click',
            function() {
                validateInputs();
                var newAddress = $addressInput.val();
                if (newAddress != "") {
                    var addressText = $suggestionDivision.find('.mr-suggestion-address').text();
                    if (addressText.indexOf(newAddress) == -1) {
                        $suggestionDivision.prepend('<li class="suggestion-list"><div class="mr-suggestion-address"> '
                                + newAddress + '</div><div class="mr-select-btn">' + "Select" + '</div></li>');
                    }
                }
            });
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}
