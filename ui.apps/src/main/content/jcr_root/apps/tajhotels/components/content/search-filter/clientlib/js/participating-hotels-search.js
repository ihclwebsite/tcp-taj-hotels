//adocument.addEventListener( 'DOMContentLoaded', function() {
//
//    var searchSuggestions = [ 'Taj Mahal Towers, Colaba-Mumbai', 'Taj Mahal Palace, Colaba-Mumbai', 'Taj Wellington, Santacruz-Mumbai', 'Taj Lands End, Bandra-Mumbai' ];
//
//    var options = [ 'Option1', 'Option2', 'Option3', 'Option4' ];
//
//    //appending suggestions for the search
//    for ( i = 0; i < searchSuggestions.length; i++ ) {
//        $( '.search-input-suggestions-list' ).append( '<li><label>' + searchSuggestions[ i ] + '</label></li>' )
//    }
//
//    //display the suggestions and update the selection
//    $( ".search-input-section > input" ).click( function( e ) {
//        e.stopPropagation();
//        $( '.search-input-suggestions-list' ).css( 'display', 'block' );
//        $( '.search-input-suggestions-list li' ).on( 'click', function() {
//            var value = ( $( this ).children() ).text();
//            $( ".search-input-section > input" ).attr( "placeholder", value );
//            $( '.search-input-suggestions-list' ).css( 'display', 'none' );
//        } );
//    } );
//
//    $( document ).on( "click", function( e ) {
//        $( '.search-input-suggestions-list' ).css( 'display', 'none' );
//    } );
//
//    //to filter out suggestions based on user input
//    $( ".search-input-section > input" ).on( "keyup", function() {
//        var g = $( this ).val().toLowerCase();
//        $( ".search-input-suggestions-list li label" ).each( function() {
//            var s = $( this ).text().toLowerCase();
//            $( this ).closest( '.search-input-suggestions-list li' )[ s.indexOf( g ) !== -1 ? 'show' : 'hide' ]();
//        } );
//    } );
//
//    //appending options for the filter
//    for ( i = 0; i < options.length; i++ ) {
//        $( '.search-filter-section-choose > .selectSubContainer > select' ).append( '<option>' + options[ i ] + '</option>' );
//    };
//
//    $( 'select' ).selectBoxIt();
//
//} );
////# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9wYXJ0aWNpcGF0aW5nLWhvdGVscy1zZWFyY2gvcGFydGljaXBhdGluZy1ob3RlbHMtc2VhcmNoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoICdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgdmFyIHNlYXJjaFN1Z2dlc3Rpb25zID0gWyAnVGFqIE1haGFsIFRvd2VycywgQ29sYWJhLU11bWJhaScsICdUYWogTWFoYWwgUGFsYWNlLCBDb2xhYmEtTXVtYmFpJywgJ1RhaiBXZWxsaW5ndG9uLCBTYW50YWNydXotTXVtYmFpJywgJ1RhaiBMYW5kcyBFbmQsIEJhbmRyYS1NdW1iYWknIF07XHJcblxyXG4gICAgdmFyIG9wdGlvbnMgPSBbICdPcHRpb24xJywgJ09wdGlvbjInLCAnT3B0aW9uMycsICdPcHRpb240JyBdO1xyXG5cclxuICAgIC8vYXBwZW5kaW5nIHN1Z2dlc3Rpb25zIGZvciB0aGUgc2VhcmNoXHJcbiAgICBmb3IgKCBpID0gMDsgaSA8IHNlYXJjaFN1Z2dlc3Rpb25zLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICQoICcuc2VhcmNoLWlucHV0LXN1Z2dlc3Rpb25zLWxpc3QnICkuYXBwZW5kKCAnPGxpPjxsYWJlbD4nICsgc2VhcmNoU3VnZ2VzdGlvbnNbIGkgXSArICc8L2xhYmVsPjwvbGk+JyApXHJcbiAgICB9XHJcblxyXG4gICAgLy9kaXNwbGF5IHRoZSBzdWdnZXN0aW9ucyBhbmQgdXBkYXRlIHRoZSBzZWxlY3Rpb25cclxuICAgICQoIFwiLnNlYXJjaC1pbnB1dC1zZWN0aW9uID4gaW5wdXRcIiApLmNsaWNrKCBmdW5jdGlvbiggZSApIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICQoICcuc2VhcmNoLWlucHV0LXN1Z2dlc3Rpb25zLWxpc3QnICkuY3NzKCAnZGlzcGxheScsICdibG9jaycgKTtcclxuICAgICAgICAkKCAnLnNlYXJjaC1pbnB1dC1zdWdnZXN0aW9ucy1saXN0IGxpJyApLm9uKCAnY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdmFyIHZhbHVlID0gKCAkKCB0aGlzICkuY2hpbGRyZW4oKSApLnRleHQoKTtcclxuICAgICAgICAgICAgJCggXCIuc2VhcmNoLWlucHV0LXNlY3Rpb24gPiBpbnB1dFwiICkuYXR0ciggXCJwbGFjZWhvbGRlclwiLCB2YWx1ZSApO1xyXG4gICAgICAgICAgICAkKCAnLnNlYXJjaC1pbnB1dC1zdWdnZXN0aW9ucy1saXN0JyApLmNzcyggJ2Rpc3BsYXknLCAnbm9uZScgKTtcclxuICAgICAgICB9ICk7XHJcbiAgICB9ICk7XHJcblxyXG4gICAgJCggZG9jdW1lbnQgKS5vbiggXCJjbGlja1wiLCBmdW5jdGlvbiggZSApIHtcclxuICAgICAgICAkKCAnLnNlYXJjaC1pbnB1dC1zdWdnZXN0aW9ucy1saXN0JyApLmNzcyggJ2Rpc3BsYXknLCAnbm9uZScgKTtcclxuICAgIH0gKTtcclxuXHJcbiAgICAvL3RvIGZpbHRlciBvdXQgc3VnZ2VzdGlvbnMgYmFzZWQgb24gdXNlciBpbnB1dFxyXG4gICAgJCggXCIuc2VhcmNoLWlucHV0LXNlY3Rpb24gPiBpbnB1dFwiICkub24oIFwia2V5dXBcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGcgPSAkKCB0aGlzICkudmFsKCkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAkKCBcIi5zZWFyY2gtaW5wdXQtc3VnZ2VzdGlvbnMtbGlzdCBsaSBsYWJlbFwiICkuZWFjaCggZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHZhciBzID0gJCggdGhpcyApLnRleHQoKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICAkKCB0aGlzICkuY2xvc2VzdCggJy5zZWFyY2gtaW5wdXQtc3VnZ2VzdGlvbnMtbGlzdCBsaScgKVsgcy5pbmRleE9mKCBnICkgIT09IC0xID8gJ3Nob3cnIDogJ2hpZGUnIF0oKTtcclxuICAgICAgICB9ICk7XHJcbiAgICB9ICk7XHJcblxyXG4gICAgLy9hcHBlbmRpbmcgb3B0aW9ucyBmb3IgdGhlIGZpbHRlclxyXG4gICAgZm9yICggaSA9IDA7IGkgPCBvcHRpb25zLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICQoICcuc2VhcmNoLWZpbHRlci1zZWN0aW9uLWNob29zZSA+IC5zZWxlY3RTdWJDb250YWluZXIgPiBzZWxlY3QnICkuYXBwZW5kKCAnPG9wdGlvbj4nICsgb3B0aW9uc1sgaSBdICsgJzwvb3B0aW9uPicgKTtcclxuICAgIH07XHJcblxyXG4gICAgJCggJ3NlbGVjdCcgKS5zZWxlY3RCb3hJdCgpO1xyXG5cclxufSApOyJdLCJmaWxlIjoibWFya3VwL2NvbXBvbmVudHMvcGFydGljaXBhdGluZy1ob3RlbHMtc2VhcmNoL3BhcnRpY2lwYXRpbmctaG90ZWxzLXNlYXJjaC5qcyJ9
