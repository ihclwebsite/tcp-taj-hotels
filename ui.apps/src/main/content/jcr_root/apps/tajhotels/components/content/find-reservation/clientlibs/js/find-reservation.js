$(document).ready(function() {
    $('.email-input').blur(function() {
        var email = $(this).val();
        if (!IsValidEmail(email)) {
            $(this).addClass('invalid-input');
            $(this).siblings('.sub-form-input-warning').text('Please enter a valid Email');
        } else {
            $(this).removeClass('invalid-input');
            $(this).siblings('.sub-form-input-warning').text('');
        }
    });

	
	var redirect=document.getElementById('redirectUrl').value + ".html?fromFindReservation=true";


    function validateInput(element) {

        if (element.val() == "") {
            element.addClass('invalid-input');
            var text = $(element.closest('.find-reserve-cat').find('.find-reserve-name-text')).text();
            element.siblings('.sub-form-input-warning').text('Please enter ' + text);
        } else {
            element.removeClass('invalid-input');
            element.siblings('.sub-form-input-warning').text('');
        }

    }


    $('.confirmation-num-input').blur(function() {
        var element = $(this);
        validateInput(element);
    });




    $('.find-reserve-btn-con').click(function() {

        $('.find-reserve-input').each(function() {
            var element = $(this);
            validateInput(element);
        });

        // setTimeout(function() {
        if ($('.find-reserve-input').hasClass('invalid-input')) {
            $('.invalid-input').first().focus();
        } else {
			var confirmationID = document.getElementById("confirmation-number").value;
            var emailAddress = document.getElementById("email").value;
            var hotelChainCode = document.getElementById("chainCode").value;
            var hotelSubstring = document.getElementById('hotelsubstring').value;
            var contentRootPath = document.getElementById('rootcontentpath').value;
            $('.find-reserve-btn-con').hide();            
            $('.taj-loader').show();
            $.ajax({
                type: 'GET',
                url: '/bin/findReservation',
                data: {
                    'confirmationNo': confirmationID,
                    'emailAddress': emailAddress,
                    'chainCode': hotelChainCode,
                    'hotelSubstring': hotelSubstring,
                    'contentRootPath': contentRootPath
                },
                success: function(data) {
                    
                    var jsonObject = JSON.parse(data);
                    if(jsonObject.guest)
                        var guest = jsonObject.guest;

                    if (typeof guest == 'undefined' || guest=='') {

                        var popupParams = {
           					title: 'No Reservation Found!',
           					callBack: '',
           					needsCta: false,
           					isWarning: true
       					}
           				warningBox( popupParams );
                        $('.find-reserve-btn-con').show();            
                        $('.taj-loader').hide();
                    } else {

                    	dataCache.session.setData("bookingDetailsRequest", data);
                   		window.location.assign(redirect);
                    }

                },
                failure: function(data) {
                    $('.find-reserve-btn-con').show();            
                    $('.taj-loader').hide();
                    $('.statusdetails').html("<strong>XML not updated</strong>");
                }
            });

        }
        // }, 1000);
    });

})