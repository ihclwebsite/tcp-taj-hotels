$(document).ready(function() {

    hideRateLabels();
    var bookingOptionsSelected = dataCache.session.getData("bookingOptions") || {
        fromDate : moment(new Date()).add(1, 'days').format("MMM Do YY"),
        toDate : moment(new Date()).add(2, 'days').format("MMM Do YY"),
        rooms : 1,
        nights : 1,
        roomOptions : [ {
            adults : 1,
            children : 0
        } ],
        selection : [],
        promoCode : null,
        hotelChainCode : null,
        hotelId : null
    };
    // updateHotelChainCodeAndHoteID( bookingOptionsSelected );
    dataCache.session.setData("bookingOptions", bookingOptionsSelected);

    $(document).on('currency:changed', function(e, currency) {
        currencySelected = currency;

        if (currencySelected != undefined) {
            setCurrencyInSessionStorage(currencySelected);
        }
    });
    checkForCurrencyStringInCache();
});

function checkForCurrencyStringInCache() {
    var bookingOptions = getBookingOptionsSessionData();
    if (bookingOptions.currencySelected === undefined) {
    	try{
        setCurrencyInSessionStorage(getCurrentCurrencyInHeader());
    	}catch(error){
    		console.log('caught error in fetching currency');
    	}
    }
}

function getCurrentCurrencyInHeader() {
    return $($.find("[data-selected-currency]")[0]).data().selectedCurrency;
}

$(window).load(function() {
    hideRateLabels();

    var isRateRequired = $("#hide-rates").data("hide-rate-container");
    if (!(isRateRequired == true)) {
        invokeRateFetcherAjaxCall();
    }

});
var popupCheck = true;
var participatingHotelsResponse = [];
function invokeRateFetcherAjaxCall() {

    $('.check-wait-spinner').css('display', "block");
    console.log("inside invokeRateFetcherAjaxCall");

    var hotelID = [];

    $('[data-hotelid]').each(function() {
        hotelID.push($(this).attr("data-hotelid"));
    })

    var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));
    var cacheJSONData = JSON.parse(cacheText);
    var checkInDate = cacheJSONData.fromDate;
    var checkOutDate = cacheJSONData.toDate;
    var rooms = cacheJSONData.rooms;
    var selectionCount = cacheJSONData.selectionCount;
    var roomCount = cacheJSONData.roomCount;
    var selection = (cacheJSONData.selection.length <= 0) ? cacheJSONData.roomOptions : cacheJSONData.selection;
    var hotelId = cacheJSONData.hotelCode;

    var roomDetails = [];
    for (i = 0; i < selection.length; i++) {
        var roomDetail = {};
        roomDetail["numberOfAdults"] = selection[i].adults;
        roomDetail["numberOfChildren"] = selection[i].children;
        roomDetails.push(roomDetail);
    }

    var checkInDate = moment(checkInDate, "MMM Do YY").format("YYYY-MM-DD");
    var checkOutDate = moment(checkOutDate, "MMM Do YY").format("YYYY-MM-DD");

    var hotelIds = [];
    var isSuccess = true;
    var lengthOfString = hotelID.length;
    console.log("total length of hotels ::" + lengthOfString);
    var i = 1;
    setNonAvailableToAllRoomCards("Rates Not Available");
    while (i < lengthOfString) {
        var ids = [];

        for (i; i < lengthOfString; i++) {

            if (i % 10 != 0) {
                ids.push(hotelID[i]);
            } else {
                ids.push(hotelID[i]);
                break;
            }

        }
        console.log(ids);
        $.ajax({
            type : 'GET',
            url : '/bin/fetch/rooms-prices',
            dataType : 'json',
            data : "hotelIds=" + JSON.stringify(ids) + "&checkInDate=" + checkInDate + "&checkOutDate=" + checkOutDate
                    + "&roomDetails=" + JSON.stringify(roomDetails) + "&roomCount=" + rooms,
            success : function(response) {
                var successResponse = JSON.stringify(response.responseCode);
                var successMessage = successResponse.substring(1, successResponse.length - 1);
                if (successMessage == "SUCCESS") {
                    isSuccess = true;
                    $('.check-wait-spinner').css('display', "none");
                    var hotelDetailsList = JSON.parse(response.hotelDetails);
                    participatingHotelsResponse.push(hotelDetailsList);
                    setRatesForHotel(hotelDetailsList);
                } else {
                    if (popupCheck) {
                        popupCheck = false;
                        $('.spinner_wait_con').hide();
                        showRateLabel();
                        var warningPopupParams = {
                            title : 'Availability Failed!',
                            description : response.message,
                        }
                        warningBox(warningPopupParams);
                    }
                }
            },
            error : function(error) {
                isSuccess = false;
                console.error("Failed to get rate for availability" + error);
            }
        })
        hotelIds.push(ids);
        i = i + 1;
    }

    if (isSuccess) {
        // setRatesForHotel(hotelDetailsList);
    } else {
        console.log("Error occured while trying to fetch the hotel details");
    }

}

function setRatesForHotel(hotelDetailsList) {
    hideLoadingSpinner();
    showRateLabel();
    if (hotelDetailsList.length > 0) {
        for (i = 0; i < hotelDetailsList.length; i++) {
            enablePriceViewforRoomsWithRate(hotelDetailsList[i]);
        }
    } else {
        setNonAvailableToAllRoomCards("Rates Not Available");
    }
}

function setNonAvailableToAllRoomCards(status) {

    $(".participating-hotels .row .hotelCard-container-outer-wrap").each(function() {
        $(this).find(".hotelDetailsRate").html(status);
        $(this).find(".bookingButtonContainer").hide();
    })
}

function enablePriceViewforRoomsWithRate(hotelDetail) {
    var currentHotelRef = $(".participating-hotels .row").find("[data-hotelid='" + hotelDetail.hotelCode + "']");
    var hotelDetailsRate = currentHotelRef.find(".hotelDetailsRate");
    var bookingButtonContainer = currentHotelRef.find(".bookingButtonContainer");
    var currentRate = bookingButtonContainer.find(".current-rate")
    var rateCurrencySymbol = bookingButtonContainer.find(".rate-currency-symbol");
    var totalPrice = hotelDetail.lowestTotalPrice;
    var discountedPrice = hotelDetail.lowestDiscountedPrice;
    var currencySymbol = "";
    if (hotelDetail.currencyCode) {
        currencySymbol = hotelDetail.currencyCode.currencyString;
    }
    var checking;
    checking = setActiveCurrencyWithResponseValue(currencySymbol);

    if (checking) {
        currencySymbol = getCurrencyCache().currencySymbol.trim();
    }

    if (totalPrice > 0) {
        bookingButtonContainer.show();
        hotelDetailsRate.html("Starting Rate/Night")
        rateCurrencySymbol.html(currencySymbol.trim());
        if (discountedPrice == 0) {
            currentRate.html(getCommaFormattedNumber(totalPrice));
            bookingButtonContainer.find(".hotelDetailsRateStriked").hide();
        } else {
            currentRate.html(getCommaFormattedNumber(discountedPrice));
            discountedRoomRef = bookingButtonContainer.find(".hotelDetailsRateStriked");
            discountedRoomRef.html(hotelDetail.lowestTotalPrice);
        }
    }

}

function getBookingOptionsSessionData() {
    return dataCache.session.getData("bookingOptions");
}

function setCurrencyInSessionStorage(currency) {
    var bookingOptions = getBookingOptionsSessionData();
    bookingOptions.currencySelected = currency;

    dataCache.session.setData("bookingOptions", bookingOptions);
}

function getCommaFormattedNumber(number) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number;
    } else {
        formattedNumber = number.toLocaleString('en-IN')
    }
    return formattedNumber;
}

function hideRateLabels() {
    $('.rate_con').hide();
}
function showRateLabel() {
    $('.rate_con').show();
}
function hideLoadingSpinner() {
    $('.waiting-spinner-participating-hotels').hide();
}
function showLoadingSpinner() {
    $('.rate_con').show();
}
