document
        .addEventListener(
                'DOMContentLoaded',
                function() {
                    try {
                        $('#sizeDropdown').selectBoxIt();
                        $('#capacityDropdown').selectBoxIt();
                        $('.events-filter-icon-mobile').on('click', function() {
                            $('.events-filter-subsection').css('display', 'block');
                        })
                        $('.events-filter-back-arrow .icon-prev-arrow').click(function() {
                            $('.events-filter-subsection').css('display', 'none');
                        })
                        $('.events-apply-btn')
                                .click(
                                        function() {
                                            $('.events-filter-subsection').css('display', 'none');
                                            if ($('#sizeDropdown').find("option:selected").text() != 'Size'
                                                    || $('#capacityDropdown').find("option:selected").text() != 'Capacity') {
                                                if ($('.events-filter-icon-mobile').html() == '<img src="/content/dam/tajhotels/icons/style-icons/filter-icon.svg" alt  = "filter-icon">') {
                                                    $('.events-filter-icon-mobile')
                                                            .html(
                                                                    '<img src="/content/dam/tajhotels/icons/style-icons/filter-applied.svg" alt  = "filter-applied-icon">')
                                                }
                                            }
                                        })
                    } catch (error) {
                        console.error("Error in wedding filter", error);
                    }

                })

$(document).ready(function() {

    try {
        var hotelName = $(".mr-hotel-details").data("hotel-name");
        var oldUrl = $('.events-request-quote-btn').find('a').attr('href');
        var newUrl = oldUrl + "?" + "hotelName=" + hotelName;
        $('.events-request-quote-btn').find('a').attr('href', newUrl);
        $('.clear-input-icon').click(function(e) {
            $(this).prev('.hotel-search').val("");
            $('.clear-input-icon').removeClass('show-clear-input');
            clearVenueSearchResults();
            $('#wedding-check-availaility-search-results').hide();
        });

        $(".searchbar-input.hotel-search").on("keyup", function() {

            var value = $(this).val().toLowerCase();
            if (value.length > 3) {
                $('.clear-input-icon').addClass('show-clear-input');
                performSearchForVenues(value)
            } else {
                $('.clear-input-icon').removeClass('show-clear-input');
                clearVenueSearchResults();
                $('#wedding-check-availaility-search-results').hide();
            }
        });

        $('#wedding-check-availaility-search-results').on("click", '.search-result-item', function() {
            $('#search-venue-input').val($(this).text());
            $('#wedding-check-availaility-search-results').hide();
        })
        $('#wedding-check-availaility-search-results').click(function(e) {
            e.stopPropagation();
        })
        $(document).click(function(e) {
            $('#wedding-check-availaility-search-results').hide();
        });
    } catch (error) {
        console.error("Error in wedding filter", error);
    }
});
function noResultsCallback() {
    try {
        $('#ca-global-re-direct').attr('href', "#");
    } catch (error) {
        console.error("Error in noResultsCallback", error);
    }
}

function performSearchForVenues(key) {
    $.ajax({
        method : "GET",
        url : "/bin/venueSearch",
        data : {
            searchText : key,
        }
    }).success(function(response) {
        succcesCallBack(response);
    }).fail(function(error) {
        console.error("search failed : ", error);
    });
}

function succcesCallBack(response) {
    clearVenueSearchResults();
    var $hotelSearchResults = $('#check-availaility-search-results-hotels');
    $('#wedding-check-availaility-search-results').show();
    $('.search-comp-results-sub-container').show();
    resultBuilder(response, $hotelSearchResults);
    if (Object.keys(response.websites).length == 0) {
        $('.search-comp-no-results').show();
    } else {
        $('.search-comp-no-results').hide();
    }
}

function clearVenueSearchResults() {
    $('#check-availaility-search-results-hotels').empty();
    $('#check-availaility-search-results-destinations').empty();
}

function resultBuilder(results, $resultsList) {
    var websiteHotels = results.websites;
    var websiteResults = $('#check-availaility-search-results-hotels');
    var destinationResults = $('#check-availaility-search-results-destinations');
    var destinationLs = results.destination;
    if ((Object.keys(websiteHotels).length)) {
        websiteHotels.forEach(function(obj) {
            websiteResults.append('<li class="search-result-item">' + obj.title + '</li>');
        });
        destinationLs.forEach(function(obj) {
            destinationResults.append('<li class="search-result-item" >' + obj + '</li>');
        });
    } else {
        $resultsList.closest('.search-comp-results-sub-container').hide();
    }
}

function redirectToResultPage() {
    try {
        var redirectPage = $('#venue-filter-go').data('redirecturl');
        if (redirectPage) {
            var searchKey = $('#search-venue-input').val();
            var capacity = $("#capacityDropdown").val();
            var filterBy = $("#filterBy").val();
            var newRedirectUrl = redirectPage + ".html";
            if (searchKey) {
                newRedirectUrl = updateQueryString("searchKey", searchKey, newRedirectUrl)
            }
            if (filterBy && capacity) {
                if (filterBy !== "all") {
                    newRedirectUrl = updateQueryString("filterBy", filterBy, newRedirectUrl);
                }
                if (capacity !== "Capacity") {
                    newRedirectUrl = updateQueryString("filterBy", filterBy, newRedirectUrl);
                    newRedirectUrl = updateQueryString("capacity", capacity, newRedirectUrl);
                }
            }
        }
        window.location.assign(newRedirectUrl);
    } catch (error) {
        console.error("Error in redirectToResultPage function", error);
    }
}
