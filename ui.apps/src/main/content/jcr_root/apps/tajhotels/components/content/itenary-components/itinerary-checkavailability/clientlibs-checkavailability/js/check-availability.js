$(document).ready(function(){
	console.log('are bhai mai yaha hu...');
});

function selectedHotelData(){
  	var categoryFound = true;
    var nightsPerCategory = {};
	$('.package-search-check-availability-wrp .multipleHotel').each(function() {
        var allSelectedData = [];
        var selectedData = {};
        var hotelCategory = $(this).find(".searchbar-input").attr('data-category');
        var formattedFromDate = moment($(this).find(".enquiry-from-value").val(), "DD/MM/YYYY").format("MM/DD/YYYY");
        var fromDate = new Date(formattedFromDate);
        var formattedToDate = moment($(this).find(".enquiry-to-value").val(), "DD/MM/YYYY").format("MM/DD/YYYY");
        var toDate = new Date(formattedToDate);
        var noOfNights = (toDate.getTime() - fromDate.getTime())/ (1000 * 3600 * 24); 
        console.log("inside selectedHotelData"+ noOfNights);
        if(nightsPerCategory[hotelCategory]){
			nightsPerCategory[hotelCategory] = noOfNights + nightsPerCategory[hotelCategory];
        }else{
               nightsPerCategory[hotelCategory] = noOfNights;
        }
        });
    dataCache.session.setData('nightsPerCategory', nightsPerCategory);
    var ItineraryDetails = dataCache.session.getData('ItineraryDetails');
    var allHotelsData = ItineraryDetails.allHotelsData;
    var minNightsData = ItineraryDetails.minNightsData;
    var hotelsInCategory = {};
    var missingCategory;
    var description = "";
    var selectOne;
    for(let i=0; i<minNightsData.length;i++){
        var singleCategoryHotel = [];
                for(let j=0; j<ItineraryDetails.allHotelsData.length;j++){
                    if(ItineraryDetails.allHotelsData[j].category == minNightsData[i].categoryName){
                        var key = minNightsData[i].categoryName;
                        singleCategoryHotel.push(ItineraryDetails.allHotelsData[j].hotelName)
						hotelsInCategory[minNightsData[i].categoryName] = singleCategoryHotel;
                    }
                	}
		let category = nightsPerCategory[minNightsData[i].categoryName];
        if(!category) {
           categoryFound = false;
           console.log("Not Matched");
            missingCategory = minNightsData[i].categoryName;
            break;
        }
    }
    if(!categoryFound){
		console.log("Category "+missingCategory+" Not Found");
        description = "Select atleast one hotel from "+hotelsInCategory[missingCategory]+"<br>";
        var popupParams = {
                        title : 'Required Hotels not selected',
                        description : description
                    }
                    warningBox(popupParams);
        return;
    }
    else{
        for(let i=0; i<minNightsData.length;i++){
            if(nightsPerCategory[minNightsData[i].categoryName] < minNightsData[i].categoryMinNights){
				console.log("min nights error for "+minNightsData[i].categoryName + minNightsData[i].categoryMinNights)

                	var error = "Select atleast "+minNightsData[i].categoryMinNights+" nights from "+hotelsInCategory[minNightsData[i].categoryName]+" combined <br>";
					description = description.concat(error);
					//console.log("Select atleast "+minNightsData[i].categoryMinNights+" nights from "+hotelsInCategory[minNightsData[i].categoryName]+" combined")
                var popupParams = {
                        title : 'Required nights not selected',
                        description : description
                    }
                    warningBox(popupParams);
                return;
            	}

        	}
        }
    console.log(hotelsInCategory);
    datePickerValidation()
}
function datePickerValidation(){
    var maxBreakBreached = true;
    var packageData = dataCache.session.getData('packageData');
    var allHotelsWithDate = getAllHotelsWithDate();
    for(let i=0; i<allHotelsWithDate.length;i++){
        for(let j=0; j<allHotelsWithDate.length;j++){
            if (i === j) {
              continue;
            }
            if(Math.abs(moment(allHotelsWithDate[i].fromDate).diff(moment(allHotelsWithDate[j].toDate), 'days')) > packageData.maxBreak){
                console.log(moment(allHotelsWithDate[i].fromDate).diff(moment(allHotelsWithDate[j].toDate), 'days'))
                maxBreakBreached = true;
            }else{
                console.log(moment(allHotelsWithDate[i].fromDate).diff(moment(allHotelsWithDate[j].toDate), 'days'))
                maxBreakBreached = false;
                break;
            }
           // if(maxBreakBreached){
			//	break;
            //}
        }
    }
    if(maxBreakBreached){
        console.log("Max days Breached!!!")
        var description = "Maximum gap between two hotels cannot be greater than "+packageData.maxBreak+"days";
        var popupParams = {
                        title : 'Max Gap error',
                        description : description
                    }
                    warningBox(popupParams);
        return;
            	}
}
function getAllHotelsWithDate(){
    var allHotelsWithDate = [];
	$('.package-search-check-availability-wrp .multipleHotel').each( function () {
        //console.log("datePickerValidation call");
        var hotelsWithDate = {};
        var hotelId = $(this).find(".searchbar-input").attr('data-hotel-id');
        var formattedFromDate = moment($(this).find(".enquiry-from-value").val(), "DD/MM/YYYY").format("MM/DD/YYYY");
        var fromDate = new Date(formattedFromDate);
        var formattedToDate = moment($(this).find(".enquiry-to-value").val(), "DD/MM/YYYY").format("MM/DD/YYYY");
        var toDate = new Date(formattedToDate);
        hotelsWithDate.hotelId = hotelId;
		hotelsWithDate.fromDate = fromDate;
        hotelsWithDate.toDate = toDate;
        allHotelsWithDate.push(hotelsWithDate);
   });
    return allHotelsWithDate;
}

function verifyDateRange(element, date){
	var allHotelsWithDate = getAllHotelsWithDate();
    console.log(element,date)
}
