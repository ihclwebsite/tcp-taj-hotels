$(window)
        .load(
                function() {

                    IhclLocDetailsInit();

                    $('.ihcl-loc-number').each(function() {
                        $(this).text(parseInt($(this).text()).toLocaleString('en-IN'));
                    });

                    var apiKey = "";
                    var symbol = "MSFT";
                    var interval = "1min";

                    var url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=NSE:INDHOTEL&interval=1min&apikey=035XHI3GQJKXIEXJ";

                    $.ajax({
                        type : 'GET',
                        url : url,
                        dataType : "json",
                        contentType : "application/json",
                        success : function(data) {

                            var symbol = data['Meta Data']['2. Symbol'];
                            var lastRefreshed = data['Meta Data']['3. Last Refreshed']

                            var lastTradePriceOnly = data['Time Series (Daily)'][lastRefreshed]['4. close']

                            var indexOf = lastTradePriceOnly.indexOf(".");

                            var lastTradePrice = lastTradePriceOnly.substring(0, indexOf);

                            var lastTradePriceAfterDecimal = lastTradePriceOnly.substring(indexOf + 1, indexOf + 3);

                            var timeSeriesData = data['Time Series (Daily)']

                            var timeSeriesKeys = Object.keys(timeSeriesData);

                            var prevDate = timeSeriesKeys[1];

                            var prevCloseTradedPrice = data['Time Series (Daily)'][prevDate]['4. close']

                            var priceDifference = lastTradePriceOnly - prevCloseTradedPrice;

                            priceDifference = roundUp(priceDifference, 2);

                            var percentageDIfference = [ priceDifference / prevCloseTradedPrice ] * 100;

                            percentageDIfference = roundUp(percentageDIfference, 2);

                            $('.ihcl-loc-details-desc-con').prepend(
                                    '<div class="ihcl-loc-details-desc-value"> <span>' + lastTradePrice
                                            + '</span><span class="ihcl-loc-details-desc-value-span">.'
                                            + lastTradePriceAfterDecimal
                                            + ' INR</span><span class="ihcl-loc-details-desc-value-perc">'
                                            + priceDifference + ' (' + percentageDIfference + '%)*</span></div>')
                            if (priceDifference < 0) {
                                $('.ihcl-loc-details-desc-value-perc').addClass('ihcl-loc-details-desc-value-perc-neg')
                            }
                        }

                    });

                });

function roundUp(num, precision) {
    precision = Math.pow(10, precision)
    return Math.ceil(num * precision) / precision
}

function IhclLocDetailsInit() {
    $(document).ready(
            function() {
                var IHCLExpansionData = {
                    "countries" : [ "India", "Srilanka", "Malaysia", "Nepal", "Bhutan", "Maldives", "South Africa",
                            "Zambia", "UAE", "UK", "USA" ],
                    "hotels" : [ 84, 3, 1, 1, 1, 2, 1, 1, 1, 3, 2 ]

                }

                for (i = 0; i < IHCLExpansionData.countries.length; i++) {
                    var hotelVar;
                    if (IHCLExpansionData.hotels[i] > 1) {
                        hotelVar = 'Hotels';

                    } else {
                        hotelVar = 'Hotel';

                    }
                    $('#ihcl-loc-details-exp-country').append(
                            '<div class="ihcl-loc-details-exp-val"><div class="ihcl-loc-details-exp-anchor">'
                                    + IHCLExpansionData.countries[i]
                                    + '</div><div class="ihcl-loc-details-exp-country-list clearfix"><span>'
                                    + IHCLExpansionData.hotels[i] + ' ' + hotelVar + '</span></div></div>');
                }

                $('.ihcl-loc-details-exp-wrap').width($(window).width());

                $("#ihcl-loc-details-cat-country").click(function(e) {

                    $(this).children('.ihcl-loc-details-cat').toggleClass('ihcl-loc-details-cat-selected');
                    $(this).children('.ihcl-loc-details-exp-wrap').toggleClass('visible');
                });

                $('.ihcl-loc-details-exp-heading-img').click(function(e) {
                    e.stopPropagation();
                    $('.ihcl-loc-details-exp-wrap').removeClass('visible');
                    $('.ihcl-loc-details-cat').removeClass('ihcl-loc-details-cat-selected');
                });

            });

    $('.ihcl-loc-details-desc-con').click(function() {
        window.location.href = "/content/tajhotels/ihcl/investors.html";
    })
}
