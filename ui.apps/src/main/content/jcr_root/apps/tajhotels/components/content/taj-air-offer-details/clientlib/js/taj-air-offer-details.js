$(document).ready(function() {
    offerData()
});

function offerData() {
    $(".taj-air-offer-booking-btn").click(function() {
        var $OfferContainer = $(this).closest(".offer-content-wrp");
        var innerContent = $OfferContainer.find(".package-details")[0].innerHTML;
        var baggageReg = /Baggage Per Person:&nbsp;([\d]*)/i;
        var noOFPersonReg = /No of Person:&nbsp;([\d,]*)/i;
        var price = /Price per Person: ([\d,]*)/i;
        var bookingData = {};

        bookingData.baggage = innerContent.match(baggageReg)[1];
        bookingData.noOFPerson = innerContent.match(noOFPersonReg)[1];
        bookingData.price = innerContent.match(price)[1];
        bookingData.packageName = $OfferContainer.find(".s-card-title")[0].innerText;
        bookingData.packagePrice = $OfferContainer.find(".rate-details-wrap .price").text();
        dataCache.session.setData("bookingOffer", bookingData);

        window.location.href = "../Taj-Air-Home/offer-book-a-jet.html?wcmmode=disabled";
    });
}
