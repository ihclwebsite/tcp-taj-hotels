$(document).ready(

        function() {
            initDescriptionShowMore();

            if ($('.diningAveragePrice').text()) {
                if ($('.diningAveragePrice').text().indexOf('*') > -1) {
                    $('.ind-dining-rate-tax-disclaimer').show();
                } else {
                    $('.ind-dining-rate-tax-disclaimer').hide();
                }
            } else {
                $('.ind-dining-rate-tax-disclaimer').hide();
            }
            var reservedDate, reservedTableDate, reservedTableMonth, reservedTableYear, reservedFinalDate = '';

            var numberOfPeopleSelected = $(
                    '.reserveTable-dining-PageLevel #mr-reserveTable-number-of-people option:selected').text();
            var mealTypeSelected = $('.reserveTable-dining-PageLevel #mr-reserveTable-meal-type option:selected')
                    .text();
            var interfaceId = $('#interfaceId').val();
            $('.mr-reserve-table-button-dining')
                    .on(
                            'click',
                            function() {
                                reservedDate = $('#jiva-spa-date.jiva-spa-date.dining-reserveTable-Date').datepicker(
                                        'getDate');
                                reservedTableDate = reservedDate.getDate() <= 9 ? "0" + reservedDate.getDate()
                                        : reservedDate.getDate();
                                reservedTableMonth = reservedDate.getMonth();
                                reservedTableYear = reservedDate.getYear();
                                reservedTableYear += 1900;
                                reservedFinalDate = reservedTableDate
                                        + '/'
                                        + ((reservedTableMonth + 1) <= 9 ? "0" + (reservedTableMonth + 1)
                                                : (reservedTableMonth + 1)) + '/' + reservedTableYear;

                                // window.location.href =
                                // '/markup/pages/destination/destination-reserve-table.html';
                                dataCache.session.setData("PeopleCount", numberOfPeopleSelected);
                                dataCache.session.setData("MealType", mealTypeSelected);
                                dataCache.session.setData("DiningDate", reservedFinalDate);
                                dataCache.session.setData("InterfaceId", interfaceId);
                            });

            if (typeof (Storage) !== "undefined") {
                $('.reserveTable-dining-PageLevel #mr-reserveTable-meal-type').on('change', function() {
                    mealTypeSelected = ($(this).find("option:selected").text());
                    dataCache.session.setData("MealType", mealTypeSelected);
                });

                $('.reserveTable-dining-PageLevel #mr-reserveTable-number-of-people').on('change', function() {
                    numberOfPeopleSelected = ($(this).find("option:selected").text());
                    dataCache.session.setData("PeopleCount", numberOfPeopleSelected);
                });

            }
            $('.mr-reserveTable-date-styling').click(function(e) {
                e.stopPropagation();
            });
            $(document).click(function() {
                $('.dining-reserveTable-Date').removeClass('visible');
            });
            $('#mr-reserveTable-number-of-people').selectBoxIt({
                populate : [ '2', '3', '4', '5', '6', '7', '8', '9', '10' ]
            });

            $('#mr-reserveTable-meal-type').selectBoxIt();

            if ($('.placeholder-title')) {
                $('.placeholder-title').cmToggleText({
                    charLimit : 200
                });
            }
            $('.regulation-answer-cuisine').each(function() {
                $(this).text($(this).text().replace(/,/g, ', '));
            });

            // datepicker script
            var today = new Date();

            $('#jiva-spa-date').datepicker({
                format : 'dd/mm/yyyy',
                startDate : today
            }).on('changeDate', function(element) {
                if ($('.jiva-spa-date').hasClass('visible')) {
                    var minDate = (new Date(element.date.valueOf()));
                    // Added by Dinesh for Event quote
                    // datepicker
                    // var selectedDate =
                    // (minDate.getDate() + '/' +
                    // (minDate.getMonth() + 1) + '/' +
                    // minDate.getFullYear());
                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                    $('.event-quote-date-value').val(selectedDate).removeClass('invalid-input');
                    $('.jiva-spa-date-value').val(selectedDate);
                    $('.jiva-spa-date').removeClass('visible');
                    $('.jiva-spa-date-value').removeClass('jiva-spa-not-valid');
                }
            });
            $('#jiva-spa-date').datepicker('setDate', new Date());

            $('.jiva-spa-date-con').click(function(e) {
                e.stopPropagation();
                $('.jiva-spa-date').addClass('visible');
            });
            // addHotelDetailsToHref();
        });


$(document).on('currency:changed', function(e, currency) {
    currencySelected = currency;
    if (currencySelected != undefined) {
        injectCurrencyInDom(currencySelected);
    }
});

function injectCurrencyInDom(currencySelected) {
    currencySymbol = $('.selected-currency').text();
    currencyDoms = $('.mr-rupee-symbol-font').text(currencySymbol);

}

function addHotelDetailsToHref() {
    var _href = $(".mr-reserve-table-button-wrap >a").attr("href");
    $(".mr-reserve-table-button-wrap >a").attr("href", _href + '&hotelWas=' + JSON.stringify(pageLevelData));
}

function initDescriptionShowMore() {
    $.each($('.mr-dining-description>span>p, .mr-dining-description>span[itemprop="description"]'), function(i, value) {
        $(value).cmToggleText({
            charLimit : 125,
            showVal : 'Show More',
            hideVal : 'Show Less',
        });
    })
}

