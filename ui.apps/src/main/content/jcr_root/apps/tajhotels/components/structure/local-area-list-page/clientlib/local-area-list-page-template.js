$(window).load(function() {
    invokeRateFetcherAjaxCall();
});

function invokeRateFetcherAjaxCall() {
    var hotelID = [];
    $('[data-hotelid]').each(function() {
        hotelID.push($(this).attr("data-hotelid"));
    })

    var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));
    /*
     * var cacheText = '{' + '"fromDate": "Sep 17th 18",' + '"toDate": "Sep 21th 18",' + '"rooms": 3,' + '"selection": [' +
     * '{' + '"adults": 2,' + '"children": 1,' + '"title": "XTX",' + '"selectedRate": 20542' + '},' + '{' + '"adults":
     * 1,' + '"children": 0,' + '"title": "XTX",' + '"selectedRate": 20542' + '},' + '{' + '"adults": 1,' + '"children":
     * 0,' + '"title": "XTX",' + '"selectedRate": 20542' + ' }' + '],' + '"targetEntity": "Taj Lands End",' +
     * '"selectionCount": 3,' + '"roomCount": 4,' + '"totalCartPrice": 75000,' + '"hotelCode": 95166,' +
     * '"hotelChainCode": 1524637,' + '"ratePlanCode":"T04"' + '}';
     */
    var cacheJSONData = JSON.parse(cacheText);
    var checkInDate = cacheJSONData.fromDate;
    var checkOutDate = cacheJSONData.toDate;
    var rooms = cacheJSONData.rooms;
    var selectionCount = cacheJSONData.selectionCount;
    var roomCount = cacheJSONData.roomCount;
    var selection = (cacheJSONData.selection.length <= 0) ? cacheJSONData.roomOptions : cacheJSONData.selection;
    var hotelId = cacheJSONData.hotelCode;
    var roomDetails = [];
    for (i = 0; i < selection.length; i++) {
        var roomDetail = {};
        roomDetail["numberOfAdults"] = selection[i].adults;
        roomDetail["numberOfChildren"] = selection[i].children;
        roomDetails.push(roomDetail);
    }

    var checkInDate = moment(checkInDate, "MMM Do YY").format("YYYY-MM-DD");
    var checkOutDate = moment(checkOutDate, "MMM Do YY").format("YYYY-MM-DD");

    $.ajax({
        type : 'GET',
        url : '/bin/fetch/rooms-prices',
        dataType : 'json',
        data : "hotelIds=" + JSON.stringify(hotelID) + "&checkInDate=" + checkInDate + "&checkOutDate=" + checkOutDate
                + "&roomDetails=" + JSON.stringify(roomDetails) + "&roomCount=" + rooms,
        success : function(response) {

            var successResponse = JSON.stringify(response.responseCode);
            var successMessage = successResponse.substring(1, successResponse.length - 1);
            if (successMessage == "SUCCESS") {
                var hotelDetailsList = JSON.parse(response.hotelDetails);

                fetchHotelDetails(hotelDetailsList);
            } else {
                var warningPopupParams = {
                    title : 'Availability Failed!',
                    description : response.message,
                }
                warningBox(warningPopupParams);
            }

        }
    })
}
function fetchHotelDetails(hotelDetailsList) {

    for (i = 0; i < hotelDetailsList.length; i++) {
        var currentRoomRef = $(".mr-current-rate-list").find("[data-hotelid='" + hotelDetailsList[i].hotelCode + "']");
        var value = hotelDetailsList[i].lowestTotalPrice;
        if (hotelDetailsList[i].lowestDiscountedPrice == "0") {
            currentRoomRef.html(hotelDetailsList[i].lowestTotalPrice);
            currentRoomRef.closest(".mr-list-current-discounted-rates-wrap").find(".mr-list-hotels-delrate").hide();

        } else {
            currentRoomRef.html(hotelDetailsList[i].lowestDiscountedPrice);
            discountedRoomRef = currentRoomRef.closest(".mr-list-current-discounted-rates-wrap").find(
                    ".discounted-rate");
            discountedRoomRef.html(hotelDetailsList[i].lowestTotalPrice);
        }

    }
}
