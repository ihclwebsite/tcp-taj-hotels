$(document).ready(function(){
	//On page load functions only.
	try{
		$("[data-component-id='inspiration-gallery']").infiniteScrollLazyLoad(true);
	}catch(error){
		console.error(error);
	}
});

/**
 *Method to filter only videos from the list given and display only videos.  
 * 
 * @param imagesListAfterAddingColClass
 */
function tagSelectionBasedOnGalleryFormat(imagesListAfterAddingColClass){
	var damFormat;
	for(var i=0;i<imagesListAfterAddingColClass.length;i++){
		
		if(imagesListAfterAddingColClass[i].hasClass("video/mp4")){
			imagesListAfterAddingColClass[i].find("picture").css("display", "none");
			imagesListAfterAddingColClass[i].find("video").css("display", "block");
		}
	}

}

/**
 * Method to add alignment col classes the given list of images. 
 * 
 * @param imagesList
 */
function getColValuesBasedOnIndex(imagesList){
	removeExistingColumnClasses(imagesList);
	var imagesListAfterAddingColClass = [];
	var value;
	 for (var index=0;index<imagesList.length;index++) {
		 if(index % 6 == 0){
			 value='col-lg-12 col-12';
		 }else if(index % 6 == 1 || index % 6 == 2){
			 value='col-lg-6 col-6';
		 }else {
			 value='col-lg-4 col-4';
		 }
		var imageAtIndex=$(imagesList).get(index);
		$(imageAtIndex).addClass(value);
		imagesListAfterAddingColClass.push($(imageAtIndex));
	 }

	 tagSelectionBasedOnGalleryFormat(imagesListAfterAddingColClass);
}

/**
 * Method to remove col classes from the list of images given before calling the reallignement method.
 * 
 * @param imagesList
 */
function removeExistingColumnClasses(imagesList){
	$(imagesList).removeClass('col-lg-12');
	$(imagesList).removeClass('col-12');
	$(imagesList).removeClass('col-lg-6');
	$(imagesList).removeClass('col-6');
	$(imagesList).removeClass('col-lg-4');
	$(imagesList).removeClass('col-4');
}
