$(document).ready(function() {
    function showPopUp() {
        $('.specific-city-showMap-popup').show();
        $('.cm-specific-showMap-con').show();
    }

    function hidePopUp() {
        $('.specific-city-showMap-popup').hide();
        $('.cm-specific-showMap-con').hide();
    }

    $('.specific-city-local-show-map').click(function() {
        showPopUp();

    });

    $('.cm-map-btn.loc-city').click(function() {
        showPopUp();
    })

    $('.map-local-back').click(function() {
        hidePopUp();
    })

    $('.showMap-close').click(function() {
        hidePopUp();

    })

    $('.cm-map-btn.loc-city').click(function() {
        $('.specific-city-showMap-mobileView').show();
    });


    var targetTxt = $('.spec-city-desc > p');
    if ((window.screen.width) < 768) {
        /*var concatTxt = '';
        targetTxt.each(function() {
            concatTxt = concatTxt + $(this).text() + '<br/><br/>';
        });

        $('.spec-city-desc').html(concatTxt);
        $('.spec-city-desc').cmToggleText({
            charLimit: 200,
            showVal: "Show More",
            hideVal: "Show Less",
        });*/

        $('.spec-city-desc > p').last().cmToggleText({
            charLimit: 200,
            showVal: "Show More",
            hideVal: "Show Less",
        });

    } else {
        $('.spec-city-desc > p').last().cmToggleText({
            charLimit: 800,
            showVal: "Show More",
            hideVal: "Show Less",
        });

    }

});