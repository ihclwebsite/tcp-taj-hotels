use(function() {
    var returnJson = {};
    var imagesJson = this.imagesJson;
    var imagesJsonArray = JSON.parse(imagesJson);
    var imagePaths = [];
    for (var i = 0; i < imagesJsonArray.length; i++) {
        imagePaths.push(imagesJsonArray[i]['imagePath']);
    }
    returnJson = {imagePaths: imagePaths};

    return returnJson
});
