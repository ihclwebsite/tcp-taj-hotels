$('document').ready(
        function() {
            try {
                if ($('#book-a-stay').data('theme') == 'ama-theme') {
                    createSelectPlaceHolder();
                } else {
                    createSearchPlaceHolder();
                }
            } catch (error) {
                console.error("Error in /apps/tajhotels/components/content/book-a-stay/clientlibs/js/searchBar.js ",
                        error.stack);
            }

        });

function createSelectPlaceHolder() {
    var contentRootPath = $('#contentRootPath').val();
    $.ajax({
        method : "GET",
        url : "/bin/search.data/" + contentRootPath.replace(/\/content\//g, ":") + "//" + "/result/searchCache.json"
    }).done(function(res) {
        // populate search result in banner search bar
        addSelectionResultsInBanner(res);
        updateCheckAvailabilityAma();

        // populate search result in book a stay popup
        addSelectionResults(res);
        updateCheckAvailability();
    }).fail(function() {
        console.error('Ajax call failed.')
    });

    function addSelectionResults(res) {
        if (Object.keys(res.destinations).length) {
            var destinations = res.destinations;
            destinations.forEach(function(destination) {
                var destRedirectPath = destination.path;
                var destinationString = destination.title;
                var destHtml = createDestResult(destination.title, destRedirectPath);
                $('#select-results').append(destHtml);
                var websiteHotels = res.hotels.website;
                if (Object.keys(websiteHotels).length) {
                    websiteHotels.forEach(function(hotel) {
                        var hotelDestination = hotel.title.split(', ');
                        if (hotelDestination[1] == destinationString) {
                            var reDirectToRoomPath = hotel.path.concat("accommodations/");
                            var hotelHtml = createHotelResult(hotel.title, reDirectToRoomPath, hotel.id, hotel.isOnlyBungalowPage);
                            $('#select-results').append(hotelHtml);
                        }
                    });
                }
            });
        }
    }

    function updateCheckAvailability() {

        var placeHolder = $("#book-a-stay .dropdown-input").text();
        var items = $('#book-a-stay #select-results li');
        items.each(function() {
            if (placeHolder == $(this).text()) {
                updateDestination($(this).find('a'));
            }
        });
    }

    function updateCheckAvailabilityAma() {
        var amaTextItem = $('.ama-info-strip').prev();
        if (amaTextItem.hasClass("ama-text")) {
            var placeHolder = amaTextItem.text().trim();
            var parent = $('.check-avblty-wrap .dropdown');

            var items = $('.check-avblty-wrap .dropdown .dropdown-menu li');
            items.each(function() {
                var $this = $(this);
                var itemText = $this.attr('id');
                if ($this.hasClass('hotel-item')) {
                    var arr = $this.attr('id').split(',');
                    itemText = arr[0];
                }
                if (placeHolder.includes(itemText)) {
                    updateDestination($(this), parent);
                }
            });
        }
    }

    function addSelectionResultsInBanner(res) {
        if (Object.keys(res.destinations).length) {
            var destinations = res.destinations;
            destinations.forEach(function(destination) {
                var destHtml = createDestResultBanner(destination.title, destination.path);
                $('.destination-hotels').append(destHtml);
                var websiteHotels = res.hotels.website;
                if (Object.keys(websiteHotels).length) {
                    websiteHotels.forEach(function(hotel) {
                        var hotelDestination = hotel.title.split(', ');
                        if (hotelDestination[1] == destination.title) {
                            var reDirectToRoomPath = hotel.path.concat("accommodations/");
                            var hotelHtml = createHotelResultBanner(hotel.title, reDirectToRoomPath, hotel.id,
                                    hotel.maxGuests, hotel.maxBeds, hotel.id);
                            $('.destination-hotels').append(hotelHtml);

                        }
                    });
                }
            });
        }
    }
    function createDestResult(title, path) {
        return '<li class="dest-item"><a class="select-result-item" data-redirect-path="' + path + '">' + title
                + '</a></li>';
    }
    function createHotelResult(title, path, hotelId, isOnlyBungalow) {
        return '<li class="hotel-item"><a class="select-result-item" data-hotelId="' + hotelId
                + '"data-isOnlyBungalow="'+ isOnlyBungalow + '" data-redirect-path="' + path + '">' + title + '</a></li>';
    }

    function createDestResultBanner(title, path) {
        return '<li id="' + title + '" class="dest-item" data-redirect-path="' + path + '">' + title + '</li>';
    }

    function createHotelResultBanner(title, path, hotelId, maxGuests, maxBeds, hotelId) {
        return '<li id="' + title + '" class="hotel-item" data-hotelid = "' + hotelId + '" data-max-guests="'
                + maxGuests + '" data-max-beds="' + maxBeds  +  '" data-redirect-path="' + path + '">' + title + '</li>';
    }

    $('.search-bar-wrapper-container').click(function() {
        $(this).toggleClass('rotate-arrow');
        $('.suggestions-wrap').toggle();
    });

    $('.bas-date-container-main-wrap, .bas-best-available-rate-container clearfix').click(function() {
        $('.suggestions-wrap').hide();
    });

    $('#bas-checkbox').click(function() {
        var $this = $(this);
        if (!$this.attr('checked')) {
            $this.attr('checked', true);
        } else {
            $this.removeAttr('checked');
        }
    });

    $('#book-a-stay').on('click', 'a.select-result-item', function() {
        updateDestination($(this));
    });
}
function updateDestination(el) {
    var amaSearchResult = $('.select-dest-placeholder')
    var selectedLocation = el.text();
    var selectedHotelId = '';
    var subtitlePlaceholder = $('#checkAvailSubtitle');
	var isOnlyBungalow= false;
    subtitlePlaceholder.text('');
    amaSearchResult.text(selectedLocation);
    if (el.attr('data-hotelid')) {
        selectedHotelId = el.attr('data-hotelid');
    }
	
    var reDirectPath = el.data("redirect-path");
    amaSearchResult.attr('data-selected-search-value', selectedLocation);
    $("#hotelIdFromSearch").text(selectedHotelId);
    $('.suggestions-wrap').hide();
    $('.search-bar-wrapper-container').removeClass('rotate-arrow');
    if (el.data('max-guests') && el.data('max-beds')) {
        var subtitleText = el.data('max-guests') + ' | ' + el.data('max-beds');
        subtitlePlaceholder.text(subtitleText);
    }
    enableBestAvailableButton(reDirectPath);
}

function createSearchPlaceHolder() {
    var searchSelector = "#booking-search";
    var searchWidget = $(searchSelector);
    var searchInput = $(searchSelector).find(".searchbar-input");
    var searchBarWrap = searchInput.closest(".searchBar-wrap");
    var suggestionsContainer = searchBarWrap.siblings('.suggestions-wrap');
    var suggestionsWrapper = suggestionsContainer.find('.suggestions-container');
    var searchSuggestionsContainer = suggestionsWrapper.children('.search-suggestions-container');
    var trendingSuggestionsContainer = suggestionsWrapper.children('.trending-suggestions-container');
    var wholeWrapper = searchBarWrap.closest('.search-and-suggestions-wrapper');
    var closeIcon = searchInput.siblings('.close-icon');
    var hotelResultCont = searchWidget.find('#hotel-result-cont');
    var hotelResults = hotelResultCont.find('#hotel-result');
    var websiteHotelResults = hotelResultCont.find('#website-hotel-result');
    var otherHotelResults = hotelResultCont.find('#others-hotel-result');
    var destResultCont = searchWidget.find('#dest-result-cont');
    var destResults = destResultCont.find('#dest-result');
    var restaurantResultCont = searchWidget.find('#restrnt-result-cont');
    var restaurantResults = restaurantResultCont.find('#restrnt-result');
    var nosearchTextBooking = $('#booking-search').find('#NoResults');
    var isTic = $('.cm-page-container').hasClass('tic');
    var keys = {
        37 : 1,
        38 : 1,
        39 : 1,
        40 : 1,
        32 : 1,
        33 : 1,
        34 : 1,
        35 : 1,
        36 : 1
    };

    var holidayResultsCont = searchWidget.find('#holiday-result-cont');
    var holidayResults = holidayResultsCont.find('#holiday-result');
    var pageScopeData = $('#page-scope').attr('data-pagescope');

    var holidayHotelResultsCont = searchWidget.find('#holiday-hotel-result-cont');
    var holidayHotelResults = holidayHotelResultsCont.find('#holiday-hotel-result');

    var SEARCH_INPUT_DEBOUNCE_RATE = 1000;

    var preventDefault = function(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    var preventDefaultForScrollKeys = function(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function hideSuggestionsContainer() {
        if (!$(suggestionsContainer).hasClass('display-none')) {
            $(suggestionsContainer).addClass('display-none');
        }
        $(document).off("keyup", searchSelector);
        $(document).off("click", searchSelector);
    }

    function showSuggestionsContainer() {
        $(suggestionsContainer).removeClass('display-none');
        $(document).on("keyup", searchSelector, function(e) {
            // Esc pressed
            if (e.keyCode === 27) {
                $(searchInput).blur();
                hideSuggestionsContainer();
            }
        });

        $(document).on("click", searchSelector, function(e) {
            e.stopPropagation();
            hideSuggestionsContainer();
            if (!$(searchSuggestionsContainer).hasClass('display-none')) {
                $(searchSuggestionsContainer).addClass('display-none');
            }
            if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
                $(trendingSuggestionsContainer).addClass('display-none');
            }
            $(wholeWrapper).removeClass('input-scroll-top');
        });
    }

    $(suggestionsWrapper).on("click", function(event) {
        event.stopPropagation();
    });

    $(closeIcon).on("click", function(e) {
        e.stopPropagation();
        $(wholeWrapper).removeClass('input-scroll-top');
        $(closeIcon).addClass('display-none');
        $(closeIcon).css("display", "none");
        if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
            $(trendingSuggestionsContainer).addClass('display-none');
        }
        if (!$(searchSuggestionsContainer).hasClass('display-none')) {
            $(searchSuggestionsContainer).addClass('display-none');
        }
        hideSuggestionsContainer();
    });

    $(searchInput).on('click', function(e) {
        e.stopPropagation();
    });

    $(searchInput).on("keyup", debounce(function(e) {
        e.stopPropagation();
        if (e.keyCode !== 27 && e.keyCode !== 9 && e.keyCode !== 40 && e.keyCode !== 38 && e.keyCode !== 13) {
            if (searchInput.val().length > 1) {
                clearSearchResults();
                performSearch(searchInput.val()).done(function() {
                    showSuggestionsContainer();
                    if (!$(trendingSuggestionsContainer).hasClass('display-none')) {
                        $(trendingSuggestionsContainer).addClass('display-none');
                    }
                    $(searchSuggestionsContainer).removeClass('display-none');
                });
            } else {
                nosearchTextBooking.hide();
                hideSuggestionsContainer();
                if (!$(searchSuggestionsContainer).hasClass('display-none')) {
                    $(searchSuggestionsContainer).addClass('display-none');
                }
                $(trendingSuggestionsContainer).removeClass('display-none');
            }
        } else {
            chooseDownUpEnterList(e);
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            $(closeIcon).removeClass('display-none');
            $(closeIcon).css('display', 'inline-block');
        }
    }, SEARCH_INPUT_DEBOUNCE_RATE));

    // Seach List to key up, down to show
    function chooseDownUpEnterList(e) {
        var $listItems = $('.individual-trends:visible');
        var $selected = $listItems.filter('.active');
        var $current = $selected;
        var currentIndex = 0;
        var listLength = $listItems.length;
        if (e.keyCode == 40) {
            $listItems.removeClass('active');
            if ($selected.length == 0) {
                $current = $listItems.first();
            } else {
                currentIndex = $listItems.index($current);
                currentIndex = (currentIndex + 1) % listLength;
                $current = $listItems.eq(currentIndex);
            }
            $current.addClass('active');
            $(".suggestions-container").scrollTop(0);
            $(".suggestions-container").scrollTop($($current).offset().top - $(".suggestions-container").height());
        }
        if (e.keyCode == 38) {
            $listItems.removeClass('active');
            if ($selected.length == 0) {
                $current = $listItems.last();
            } else {
                currentIndex = $listItems.index($current);
                currentIndex = (currentIndex - 1) % listLength;
                $current = $listItems.eq(currentIndex);
            }
            $current.addClass('active');
            $(".suggestions-container").scrollTop(0);
            $(".suggestions-container").scrollTop($($current).offset().top - $(".suggestions-container").height());
        }
        if (e.keyCode == 13) {
            if ($current.hasClass("active")) {
                $($current).focus().trigger('click');
                // var getText = $($current).text();
                // $(searchInput).val(getText);
            }
        }
    }

    function performSearch(key) {

        var contentRootPath = $('#contentRootPath').val();
        var otherWebsitePath = $('#basOtherWebsitePath').val();
        var appendDestName = $('#appendDestName').val();

        var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
        if (ihclCbBookingObject) {
            if (ihclCbBookingObject.isIHCLCBFlow) {
                contentRootPath = '/content/ihclcb';
                otherWebsitePath = otherWebsitePath + '_:ihclcb';
            }
        }

        return $.ajax(
                {
                    method : "GET",
                    url : "/bin/search.data/" + contentRootPath.replace(/\/content\//g, ":") + "/"
                            + otherWebsitePath.replace(/\/content\//g, ":").replace(",", "_") + "/" + key
                            + "/result/searchCache.json"
                }).done(function(res) {

            // [TIC-FLOW]
            var userDetails = getUserData();
            var bookingOptionsSessionData = dataCache.session.getData("bookingOptions");
            if (userDetails && userDetails.tier && bookingOptionsSessionData && bookingOptionsSessionData.flow) {
                bookingOptionsSessionData.flow = '';
                dataCache.session.setData("bookingOptions", bookingOptionsSessionData);
            }

            clearSearchResults();
            removeRedirectionForBestAvailableRatesButton();
            addHotelSearchResults(res.hotels);
            if (!isTic) {
                addDestSearchResults(res.destinations);
            }
            addHolidaySearchResults(res.holidays);
            addHolidayHotelSearchResults(res.holidayHotels);
            holidayFunction(res);
        }).fail(function() {
            clearSearchResults();
        });
    }

    function isHolidayResultAvailable() {
        if (holidayResults.children().length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function isHolidayHotelResultAvailable() {
        if (holidayHotelResults.children().length > 0) {
            return true;
        } else {
            return false;
        }
    }

    function holidayFunction(res) {
        if (pageScopeData == "Taj Holidays") {
            // hide all tab and restaurant,experience contains in holiday page
            ifHolidayPage = true;
            if (isHolidayResultAvailable() || isHolidayHotelResultAvailable()) {
                holidayResultsCont.show();
                holidayHotelResultsCont.show();

                hotelResultCont.hide();
                destResultCont.hide();

                if (!isHolidayResultAvailable())
                    holidayResultsCont.hide();

                else if (!isHolidayHotelResultAvailable())

                    holidayHotelResultsCont.hide();

            }
            showNoResultsHoliday(res);

        } else {
            showNoResults(res);
        }
    }

    function showNoResults(res) {
        if ((Object.keys(res.hotels.website).length == 0) && (Object.keys(res.hotels.others).length == 0)
                && (Object.keys(res.destinations).length == 0)) {
            nosearchTextBooking.show();
        } else {
            nosearchTextBooking.hide();
        }
    }

    function showNoResultsHoliday(res) {
        if ((Object.keys(res.hotels.website).length == 0) && (Object.keys(res.hotels.others).length == 0)
                && (Object.keys(res.destinations).length == 0) && (Object.keys(res.holidays).length == 0)
                && (Object.keys(res.holidayHotels).length == 0)) {
            nosearchTextBooking.show();
        } else {
            nosearchTextBooking.hide();
        }

    }

    function removeRedirectionForBestAvailableRatesButton() {
        return $("#global-re-direct").removeAttr("href");
    }

    function clearSearchResults() {
        hotelResultCont.hide();
        otherHotelResults.empty();
        websiteHotelResults.empty();
        destResultCont.hide();
        destResults.empty();
        restaurantResultCont.hide();
        restaurantResults.empty();
        holidayResultsCont.hide();
        holidayResults.empty();
        holidayHotelResultsCont.hide();
        holidayHotelResults.empty();
    }

    function addHotelSearchResults(hotels) {
        if (Object.keys(hotels).length) {
            var websiteHotels = hotels.website;
            var otherHotels = hotels.others;
            if (!isIHCLCBSite()) {
                if (Object.keys(websiteHotels).length) {
                    websiteHotels.forEach(function(hotel) {
                        var ROOMS_PATH = "rooms-and-suites/";
                        var reDirectToRoomPath = hotel.path.replace(".html", "");
                        reDirectToRoomPath = reDirectToRoomPath + ROOMS_PATH;
                        if (reDirectToRoomPath != "" && reDirectToRoomPath != null && reDirectToRoomPath != undefined) {
                            reDirectToRoomPath = reDirectToRoomPath.replace("//", "/");
                        }
                        var resultHtml = createSearchResult(hotel.title, reDirectToRoomPath, hotel.id, hotel.sameDayCheckout);
                        websiteHotelResults.append(resultHtml);
                        hotelResultCont.find('.website-result').show();
                    });
                }
                if (otherHotels && Object.keys(otherHotels).length) {
                    otherHotels.forEach(function(hotel) {
                        var ROOMS_PATH = "rooms-and-suites/";
                        var reDirectToRoomPath = hotel.path.replace(".html", "");
                        reDirectToRoomPath = reDirectToRoomPath + ROOMS_PATH;
                        if (reDirectToRoomPath != "" && reDirectToRoomPath != null && reDirectToRoomPath != undefined) {
                            if (!reDirectToRoomPath.includes('https')) {
                                reDirectToRoomPath = reDirectToRoomPath.replace("//", "/");
                            }
                        }
                        var resultHtml = createSearchResult(hotel.title, reDirectToRoomPath, hotel.id, hotel.sameDayCheckout);
                        otherHotelResults.append(resultHtml);
                        hotelResultCont.find('.others-result').show();
                    });
                }
            } else if (isIHCLCBSite() && otherHotels && Object.keys(otherHotels).length) {
                // [IHCLCB]iterating over result.
                otherHotels.forEach(function(hotel) {
                    var ROOMS_PATH = "rooms-and-suites/";
                    var reDirectToRoomPath = hotel.path.replace(".html", "");
                    reDirectToRoomPath = reDirectToRoomPath + ROOMS_PATH;
                    if (reDirectToRoomPath != "" && reDirectToRoomPath != null && reDirectToRoomPath != undefined
                            && reDirectToRoomPath.includes('/corporate-booking/')
                            && !reDirectToRoomPath.includes('/ama-trails/')) {
                        if (!reDirectToRoomPath.includes('https')) {
                            reDirectToRoomPath = reDirectToRoomPath.replace("//", "/");
                        }
                        var resultHtml = createSearchResult(hotel.title, reDirectToRoomPath, hotel.id, hotel.sameDayCheckout);
                        websiteHotelResults.append(resultHtml);
                        hotelResultCont.find('.website-result').show();
                    }
                    // [IHCLCB]this is intentional hide for ihclccb
                    hotelResultCont.find('.others-result').hide();
                });
            }
            if (websiteHotels && Object.keys(websiteHotels).length == 0) {
                hotelResultCont.find('.website-result').hide();
            }
            if (otherHotels && Object.keys(otherHotels).length == 0) {
                hotelResultCont.find('.others-result').hide();
            }
            hotelResultCont.show();
        }
    }

    function addDestSearchResults(dests) {
        var destinationObject = [];
        if (Object.keys(dests).length) {
            dests.forEach(function(dest) {
                var destRedirectPath = dest.path;
                console.log(dest);
                if (isIHCLCBSite() && dest && dest.ihclOurHotelsBrand) {
                    if (dest.path.includes('/corporate-booking/')) {
                        var destinationFlag = false;
                        for (var i = 0; i < destinationObject.length; i++) {
                            if (destinationObject[i] === dest.path) {
                                destinationFlag = true;
                                break;
                            }
                        }
                        if (!destinationFlag) {
                            destinationObject.push(dest.path);
                            var resultHtml = createSearchResult(dest.title, destRedirectPath);
                            destResults.append(resultHtml);
                        }
                    }
                } else {
                    var resultHtml = createSearchResult(dest.title, destRedirectPath);
                    destResults.append(resultHtml);
                }
            });
            destResultCont.show();
        }
    }

    function addHolidaySearchResults(holidays) {
        if (Object.keys(holidays).length) {
            holidays.forEach(function(holidays) {
                if (holidays.title != null) {
                    var resultHtml = createSearchResult(holidays.title, holidays.path);
                    holidayResults.append(resultHtml);
                }
            });
        }
    }

    function addHolidayHotelSearchResults(holidayHotel) {
        if (Object.keys(holidayHotel).length) {
            holidayHotel.forEach(function(holidayHotel) {
                var resultHtml = createSearchResult(holidayHotel.title, holidayHotel.path);
                holidayHotelResults.append(resultHtml);
            });
        }
    }

    function createSearchResult(title, path, hotelId, sameDayCheckout) {
        return '<a class="individual-trends" data-hotelId="' + hotelId + '" data-redirect-path="' + path + '" data-sameDayCheckout="' + sameDayCheckout + '">' + title
                + '</a>';
    }

    $('#booking-search .trending-search').on("click", '.individual-trends', function() {
        hideSuggestionsContainer();
        if ($(this).parents('.trending-searches-in-taj').attr('id') == 'others-hotel-result') {
            domainChangeFlag = true;
        } else {
            domainChangeFlag = false;
        }
        var selectedLocation = $(this).text();
        var selectedHotelId = "";
        if ($(this).attr('data-hotelid') != 'undefined') {
            selectedHotelId = $(this).attr('data-hotelid');
        }
        dataCache.session.setData("sameDayCheckout", $(this).attr('data-sameDayCheckout'));
        setHotelIdFromSearch(selectedHotelId);
        setSearchBarText(selectedLocation);
        var reDirectPath = $(this).data("redirect-path");
        searchInput.attr('data-selected-search-value', selectedLocation);
        enableBestAvailableButton(reDirectPath);
    });

    function setSearchBarText(textValue) {
        return searchInput.val(textValue);

    }

    function setHotelIdFromSearch(hotelId) {
        $("#hotelIdFromSearch").text(hotelId);
    }
}

var initiateNavPreloginSearch = function() {
    var navPreloginSearch = $('.header-nav-prelogin-search');
    $('.gb-search-con').click(function() {
        navPreloginSearch.show().promise().then(function() {
            navPreloginSearch.find('.searchbar-input').click();
        });
    });
    $('.nav-prelogin-close, .closeIconImg ,.cm-overlay').click(function() {
        navPreloginSearch.hide();
    });
}

initiateNavPreloginSearch();
