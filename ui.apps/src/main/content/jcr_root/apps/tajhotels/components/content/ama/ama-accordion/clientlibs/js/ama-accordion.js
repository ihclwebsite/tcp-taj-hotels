$(document).ready(function() {
    $(".set > .description-heading").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).siblings(".content").slideUp(200);
            $(".set > .description-heading span").removeClass("up-arrow").addClass("down-arrow");
        } else {
            $(".set > .description-heading span").removeClass("up-arrow").addClass("down-arrow");
            $(this).find("span").removeClass("down-arrow").addClass("up-arrow");
            $(".set > .description-heading").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp(200);
            $(this).siblings(".content").slideDown(200);
        }
    });
});
