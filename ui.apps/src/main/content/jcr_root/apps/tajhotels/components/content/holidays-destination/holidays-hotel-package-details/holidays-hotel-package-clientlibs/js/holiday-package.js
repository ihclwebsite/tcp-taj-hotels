$(document).ready(function() {
    $(".Offer-terms-cond-head-wrapper").click(function() {
        if ($(".Terms-cond-desc").hasClass("open")) {
            $(".image-view").removeClass("up");
            $(".Terms-cond-desc").slideUp(200);
            ;
            $(".Terms-cond-desc").removeClass('open');

        } else {
            $(".Terms-cond-desc").slideDown(200);
            ;
            $(".image-view").addClass("up");
            $(".Terms-cond-desc").addClass('open');
        }
    });
});
function setEnquirySessionObj(packageName, hotelName, city) {

    console.log("packageName " + packageName);
    console.log("hotelName " + hotelName);
    console.log("city " + city);

    var enquiryDetails = {};
    var locations = [];
    locations.push(hotelName);
    enquiryDetails.packageName = packageName;
    enquiryDetails.packageDestination = city;
    enquiryDetails.selectedHotels = locations;
    dataCache.session.setData('enquiryObject', enquiryDetails);
}
