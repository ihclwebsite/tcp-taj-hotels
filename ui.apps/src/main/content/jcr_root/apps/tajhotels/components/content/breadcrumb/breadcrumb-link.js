use(function () {
    // you can reference the parameters via the this keyword.
    var title = this.navTitle;
    if(title=="Taj")
    	{
    		retValue="/en-in/";
    	}
    else if(title=="Vivanta")
    	{
    		retValue="/en-in/";
    	}
    else if(title=="Gateway")
		{
    	retValue="/en-in/the-gateway-hotels/";
		}
    else if(title=="SeleQtions")
		{
    	retValue="/en-in/";
		}
    else
    	{
    	retValue=this.navURL;
    	}

    return {
    	newNavLink: retValue
    };
});