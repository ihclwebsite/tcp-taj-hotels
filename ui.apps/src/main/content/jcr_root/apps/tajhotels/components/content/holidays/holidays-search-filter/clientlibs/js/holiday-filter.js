var labelArray = [];
var options;
var dummyOptions = [];

$(document).ready(function() {
    labelArray = [];
    options;
    dummyOptions = $('#filter-json-string').val() ? JSON.parse($('#filter-json-string').val()) : '';
    addLabel();
    initHolidayFilter();
});

function addLabel() {
    var labelvalue;
    var i = 0;
    var $titleLink = $('.holidays-dest-popularDest');
    $titleLink.each(function() {
        var labelObject = {};
        labelvalue = $(this).find('.cm-header-label').text();
        labelObject.label = labelvalue;
        labelObject.value = labelvalue;
        labelArray.push(labelObject);
        i++;
        labelvalue = "";
    });
}

var applyOnSamePage = false;

function addOption() {
    for (var i = 0; i < options.length; i++) {
        var temp = '<div class="filter-catagory">'
                + '<div class="filter-header">'
                + options[i].selector
                + '</div>'
                + '<div class="filter-dropDD"  id='
                + options[i].selector
                + '><select aria-label="filter drop down" name="filter-drop-down" class="cm-multi-dd" multiple="multiple"></select></div>'
                + '</div>';
        $('.filter-wrap-catagory').append(temp);
    }
}

// for landing page show search bar with filter
$('.holiday-filter-wrapper .filter-searchBar-wrap.cm-hide').removeClass('cm-hide');
$('.filter-landing-cont-image').addClass(' cm-hide');

function initDD(options) {
    $('#holidaysFilterNoResults').hide();
    $('.filter-wrap-catagory').find('.cm-multi-dd').each(function(i) {
        $(this).multiselectDropdown(options[i]);
    });
    var valueofOpt = dataCache.session.getData('landingOptions');
    if (applyOnSamePage && valueofOpt) {
        $('.filter-wrap-catagory').find('.cm-multi-dd').not("span").each(function(i) {
            if (valueofOpt[i].selectedOptionList) {
                $(this).val(valueofOpt[i].selectedOptionList);
                $(this).multiselect("refresh");
            }
        });
        filterSetOnDestFromLand(valueofOpt);
    } else {
        dataCache.session.removeData('landingOptions');
    }
    $('.dropdown-toggle').dropdown();
}

function onLoadTocheckURL() {
    if ($('.cm-page-container').hasClass('holidays-homepage-layout') || $('.cm-page-container').hasClass('weddings')) {
        applyOnSamePage = false;
    } else {
        applyOnSamePage = true;
    }
}

function actionFilter() {
    $('.filter-searchBar-wrap, #holidayFilterSearch, .events-filter-back-arrow').on('click', function() {
        $('.events-filter-subsection').toggle();
    });
}

function initHolidayFilter() {
    initializeOptions();
    addOption();
    onLoadTocheckURL();
    initDD(options);
    actionFilter();
    applyFilterOptions();
}

function initializeOptions() {
    // var hiddenDivContents =$($.find("[data-dropdown='json']")[0]).text();
    // options=JSON && JSON.parse(hiddenDivContents) || $.parseJSON(hiddenDivContents);
    options = dummyOptions;
}

function applyFilter(isAutomaticallyLoading) {
    if (isHolidayPage()) {
        filterIndividualDestination();
    } else if (applyOnSamePage) {
        if ($('.cm-page-container').hasClass('weddings-list')) {
            filterOnWedding();
        } else {
            // call this function for same page filter func
            filterOnDestination();
        }
    } else {
        if (!isAutomaticallyLoading) {
            dataCache.session.setData('landingOptions', options);
            var redirectPage = $('.filter-go-container').data('redirecturl');
            window.location.assign(redirectPage + ".html");
        }
    }
    // formatHotelCardUI();
}

function isHolidayPage() {
    return $('.cm-page-container').hasClass('holidays-experiences-layout')
            || $('.cm-page-container').hasClass('holidays-theme-layout')
            || $('.cm-page-container').hasClass('holidays-packages-layout');
}

function applyFilterOptions() {
    var filterOptions = dataCache.session.getData('filterOptions');
    if (filterOptions) {
        filterOptions.forEach(function(item) {
            var selectedOptionList = item.selectedOptionList;
            if (selectedOptionList) {
                selectedOptionList.forEach(function(item) {
                    var $optionToSelect = $('input[value="' + item + '"]');
                    if (!$optionToSelect.prop('checked')) {
                        $optionToSelect.trigger('click');
                    }
                })
            }
        });
        setTimeout(function() {
            applyFilter(true);
        }, 200);
    }
}
