function ihclDescription() {
    $(document).ready(function() {
        if ($('.concierge-det p').text().length > 150) {
            $('.concierge-det p').cmToggleText({
                charLimit : 200,
                showVal : "Show More",
                hideVal : "Show Less",
            });
        }
    });
}
