$(document).ready(function () {

    if (dataCache.session.getData('bookingDetailsRequest') === null
            && sessionStorage.getItem('bookingDetailsRequest') === null) {
        console.info('reservation details not found');
    } else {
        buildCancelPage();
    }
    
	$('.cancel-booking-header .cancel-booking-details').cmToggleText({
		charLimit: 180
	});

	$('.guest-details-expansion').click(function () {
		var content = $('.guest-detail-value-mob');
		if ($(this).hasClass('guest-details-expansion-inverted')) {
			content.css('display', 'none');
			$(this).removeClass('guest-details-expansion-inverted')

		} else {
			content.css('display', 'inline-block');
			s
			$(this).addClass('guest-details-expansion-inverted');
		}
	});

	$('input[name="cancel-itinerary-checkbox"]').click(function(){
		if($(this).is(':checked')){
			$('input[name="reservation-checkbox"]').prop( "checked" ,true);
		}
	})

});


function buildCancelPage() {


	var jsonObject = dataCache.session.getData('bookingDetailsRequest');
	dataCache.session.setData('selectedCurrency', jsonObject.currencyCode);
	var roomsBooked = jsonObject.roomList;
	var totalCostWithoutTax, totalCostWithTax;
	var redirect = document.getElementById('redirectUrl').value + ".html";
	var htmlCode = '';
	$.each(roomsBooked, function (index, data) {

		totalCostWithTax = data.roomCostAfterTax;
		totalCostWithoutTax = data.roomCostBeforeTax;
		var isKingBed = false;
		var bed = data.bedtype;

		if (typeof bed == 'undefined' || bed == 'king') {
            bed = 'King';
			isKingBed = true;
        }

		htmlCode += `<div class="cancel-selected-room-checkbox"><input type="checkbox" name="reservation-checkbox" data-reservationNum = "${data.reservationNumber}"><label></label></div>
               <div class="confirmation-selected-rooms-card">
                  <div class="cart-selected-rooms-header clearfix">
                     <div class="confirmation-room-details">
                        <div class="heading-con">
                           <div class="heading">Reservation Number: ${data.reservationNumber}</div>
                           <div class="cancel-indication">CANCELLED</div>
                           <img class="cancel-ind-arrow" src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt="dropdown">
                        </div>
                           <div class="room-details"> <span class="room-details-colored">Room ${index+1} - </span><span class="room-details-margin">${data.noOfAdults} Adults, ${data.noOfChilds} Child</span><span class="bed-spec">${bed}</span>
								<span class="bedtype-rooms"><img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon">
								</span>
						   </div>
                     </div>
                     <div class="confirmation-room-price">
                        <div class="confirmation-banner">
                           <div class="total-price-deal-section">
                              <div class="best-deal-banner"> <img src="/content/dam/tajhotels/icons/style-icons/left-right-clipped.svg" alt="background"><span>BEST DEAL</span></div>
                              <div class="best-deal-desc">Congratulations! You've bagged the best deal for the day</div>
                           </div>
                        </div>
                        <div class="confirmation-price-con"><span>Price:</span><span class="rupee-text">₹</span><span class="best-deal-price">${data.roomCostAfterTax}</span></div>
                     </div>
                  </div>
                  <div class="cart-selected-rooms-content">
                     <div class="confirmation-selected-rooms-amenities">
						                
                         <div class="cart-amenities-title">${data.roomTypeName}<img class="cart-amenities-img" src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow.svg" alt="dropdown"></div>
                        <div class="cart-amenities-description">${data.roomTypeDesc}</div>
                        <div class="cart-available-wrp">

                           
                        </div>
                     </div>
                     <div class="cart-selected-info-wrp">
                        <div class="description-cancel-wrp">
							 <div class="rate-description"><span class="bold-title">Rate description: </span><span class="conf-rate-description-details">${data.rateDescription}</span></div>
                           <div class="cancel-policy"><span class="bold-title">Cancellation policy: </span><span class="confirmation-canc-details">${data.cancellationPolicy}</span></div>
                           <div class="pet-policy"><span class="bold-title">Pet policy: </span><span>Allowed</span></div>
                        </div>
                     </div>
                     <a class="confirmation-cancel" href="{redirect}"><button class="cm-btn-primary confirmation-cancel-btn">Cancel</button></a>
                  </div>
               </div>`;
		var roomTypeIcon = `<img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon"><img class="bed-spec-img" src="/content/dam/tajhotels/icons/style-icons/rooms-amenity-bed.svg" alt="icon">`;
		if (!isKingBed) {
			$(".bedtype-rooms").html(roomTypeIcon);
		}
	});	
	$('#itineraryNumber').text(jsonObject.itineraryNumber);
	$('[data-itinerarynumber]').attr('data-itinerarynumber',jsonObject.itineraryNumber)
	$(".jsonData-rooms").html(htmlCode);

	var hotelId = jsonObject.hotelId;
	var totalTaxes = totalCostWithTax - totalCostWithoutTax;

	var checkinDate = jsonObject.checkInDate;
	var checkoutDate = jsonObject.checkOutDate;

	var checkInDateFormatted = moment(checkinDate, "YYYY-MM-DD").format("DD MMM YYYY");
	var checkOutDateFormatted = moment(checkoutDate, "YYYY-MM-DD").format("DD MMM YYYY");
	$("#check-in-date, #sm-checkin-date").html(checkInDateFormatted);
	$("#check-out-date, #sm-checkout-date").html(checkOutDateFormatted);


	if (jsonObject.guest) {
		if (jsonObject.guest.name != "") {
			$("#name").html(jsonObject.guest.name);
		} else {
			$("#name").html(jsonObject.guest.title + " " + jsonObject.guest.name);
		}
	}


	$("#mail").html(jsonObject.guest.email);
	$("#phoneNo").html(jsonObject.guest.phoneNumber);
	$("#membershipNumber").html(jsonObject.guest.membershipNumber);


	$("#hotelHeading").html(jsonObject.hotel.hotelName);
	$("#sm-hotel-phone,#hotel-phone").html(jsonObject.hotel.hotelPhoneNumber);
	$("#hotel-mail-id").html(jsonObject.hotel.hotelEmail);
	$("#ho-contact-number").attr("href", "tel:"+jsonObject.hotel.hotelPhoneNumber);
	$("#ho-mail-id").attr("href", "mailto:"+jsonObject.hotel.hotelEmail);


/* Summary Charges section */
	var container = $('.summary-charges-con');
   var noOfRooms = jsonObject.roomList.length;
   var totalTax = parseInt(jsonObject.totalAmountAfterTax) - parseInt(jsonObject.totalAmountBeforeTax);
   var totalNoOfDays = 0;
   var couponamount=0;
   var couponname='';
   /*
     * if(jsonObject.appliedCoupon){ couponamount=parseInt(jsonObject.appliedCoupon.couponAmountBeforeTax);
     * couponname=jsonObject.appliedCoupon.appliedCoupon; }
     */
   var discountAmount = couponamount * noOfRooms;
   var totalAmount = parseInt(jsonObject.totalAmountAfterTax) - discountAmount;
   availableRoomnumber=0;var roomNumber = 1;var taxHtml;var taxCheck = false;
   for (i = 0; i < noOfRooms; i++) {
       if (jsonObject.roomList[i].bookingStatus == true) {
           availableRoomnumber=availableRoomnumber+1;
           var noOfAdults = jsonObject.roomList[i].noOfAdults;
           var noOfChilds = jsonObject.roomList[i].noOfChilds;
           var fromDate = jsonObject.checkInDate;
           var toDate = jsonObject.checkOutDate;
           var noOfDays = ((moment(toDate) - moment(fromDate)) / 1000 / 60 / 60 / 24);
           totalNoOfDays = totalNoOfDays + noOfDays;

           var currencySymbolForRate = dataCache.session.getData('selectedCurrency');

           var roomData = '(1 Room x ' + noOfDays + ' Nights - ' + noOfAdults + ' Adults & ' + noOfChilds + ' Children) ';
           var roomDataExpand = '<div class="summary-charges-room-total-price summary-charges-item-con"><div class="summary-charges-item-name">Room ' + availableRoomnumber + '<span>' + roomData + '</span><i class="nightly-rates-dropdown-image icon-drop-down-arrow inline-block"></i></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + jsonObject.roomList[i].roomCostBeforeTax + '</span></div><div class="nightly-rates">';
           for(var d=0; d<jsonObject.roomList[i].nightlyRates.length; d++){
                
                
                var indNightRateData = jsonObject.roomList[i].nightlyRates[d];
                
                var rateAfter = indNightRateData.price;
                    rateAfter = Math.round( rateAfter * 100 ) / 100;
                    rateAfter = roundPrice( rateAfter );
                
                var indNightRateHtml = '<div class="ind-nightly-rate">'+
                '<span class="ind-night-date">'+moment(indNightRateData.date,"MM/DD/YYYY").format("DD MMM YYYY")+'</span>'+
                '<span class="ind-night-rate">'+currencySymbolForRate+' '+rateAfter+'</span>'+
                '</div>';
                
                roomDataExpand = roomDataExpand + indNightRateHtml;
            }
           roomDataExpand = roomDataExpand + '</div></div>';
           container.append(roomDataExpand);
           
           if(jsonObject.roomList[i].taxes){
        	   var roomTaxHtml = '<div class="room-level-tax"> <span class="room-info"> Room ' + roomNumber +'</span>';

	   			for(var ii=0; ii<jsonObject.roomList[i].taxes.length; ii++){
	   	
	   				var indTaxData = jsonObject.roomList[i].taxes[ii];
	   				
	   				var price = roundPrice(indTaxData.taxAmount)
	   				
	   				var indTaxHtml = '<div class="ind-tax-Details">'+
	   				'<span class="ind-tax-name">'+indTaxData.taxName+'</span>'+
	   				'<span class="ind-tax-amount">'+currencySymbolForRate+' '+price+'</span>'+
	   				'</div>';
	   				
	   				roomTaxHtml = roomTaxHtml + indTaxHtml;
	   			}
              roomTaxHtml = roomTaxHtml + '</div>';

              if(taxHtml == null){
            	  taxHtml = roomTaxHtml;
              }else{
            	  taxHtml = taxHtml + roomTaxHtml;
              }
              roomNumber = roomNumber+1;
              taxCheck = true;
           }
           
       }
   }

   var totalRoomData = '(' + availableRoomnumber + ' Room x ' + totalNoOfDays + ' Nights)'
   container.append('<div id="summary-taxes-charges" class="summary-charges-item-con"><div class="summary-charges-item-name">Taxes and fees <i class="taxes-dropdown-image inline-block icon-drop-down-arrow"></i></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalTax) + '</span></div><div class="all-type-of-taxes">'+taxHtml+'</div></div><div class="summary-charges-item-con" id="appliedCouponDiv"><div class="summary-charges-item-name">Applied Coupon<div class="input-disabled summary-coupon-applied">'+couponname+'</div></div><div class="summary-charges-discount-value"><span>- </span><span><span class="cart-currency-symbol rupee-symbol"></span></span><span class="cost-with-commas">' + roundPrice(discountAmount) + '</span></div></div><div class="summary-charges-item-con position"><div class="summary-charges-item-name">Total <span class="room-count">' + totalRoomData + '</span></div><div class="summary-charges-item-value"><span class="cart-currency-symbol rupee-symbol"></span><span class="cost-with-commas">' + roundPrice(totalAmount) + '</span><span class="total-charge-asterik">*</span></div></div><div class="disclaimer ">* inclusive of all taxes.</div>');

   $('.cost-with-commas').digits();
   
   if(discountAmount <= 0){
	   $('#appliedCouponDiv').hide();
   }
   
   if(!taxCheck){
	   $('#summary-taxes-charges').hide();
   }
    var ind;
    jQuery('.summary-charges-room-total-price').click(function () {
        var index = $(this).index();
        if(ind != index){
            var newTarget = jQuery('.nightly-rates').eq(index-1).slideDown();
            jQuery('.nightly-rates').not(newTarget).slideUp();
            var newImgTarget = jQuery('.nightly-rates-dropdown-image').eq(index-1).css({'transform': 'rotate(-180deg)'});
            jQuery('.nightly-rates-dropdown-image').not(newImgTarget).css({'transform': 'rotate(-360deg)'});
            ind = index;
        }else{
            jQuery('.nightly-rates').slideUp();
            jQuery('.nightly-rates-dropdown-image').css({'transform': 'rotate(-360deg)'});
            ind = 0;
        }
        
    });
    
    $('#summary-taxes-charges').on( "click", function() {
    	$('.taxes-dropdown-image').toggleClass("cm-rotate-icon-180");
    	$('.all-type-of-taxes').slideToggle();
    });
/* end of summary charges */

	/* getting the hotel details from langitude and latitude */

	processAddressUsingLongLatitude(jsonObject.hotel);

	function processAddressUsingLongLatitude(jsonHotelObject) {
		getReverseGeocodingData(jsonHotelObject.hotelLatitude,jsonHotelObject.hotelLongitude);
	}

	function getReverseGeocodingData(lat, lng) {
        try {
			var latlng = new google.maps.LatLng(lat, lng);
			var address ="";
			// This is making the Geocode request
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'latLng': latlng }, function (results, status) {
				if (status !== google.maps.GeocoderStatus.OK) {

				}
        		// This is checking to see if the Geoeode Status is OK before
				// proceeding
				if (status == google.maps.GeocoderStatus.OK) {
					address = (results[0].formatted_address);
					setAddressinDOM(address);
				}
			});
        } catch(e) {
            console.warn(e);
        }



	}

	function setAddressinDOM(address) {
		return $($.find('.ho-location-address')[0]).text(address);
	}
 
	function setRedirectionURLforMAPinDOM(jsonHotelObject) {
		var url = formMapRedirectURL(jsonHotelObject.hotelLatitude,jsonHotelObject.hotelLongitude);
		return $($.find('.ho-hotel-location-link')[0]).attr('href',url);
	}
 
	function formMapRedirectURL(lattitude,longitude) {
		var url = "https://www.google.com/maps/search/?api=1&query="+lattitude+","+longitude ;
		return url;
	}


	$(".proceed-btn").click(function (e) {
		cancelReservation();
	});

	var itineraryNumberChecked = '';
	var checkedRooms = [];
	var userMail;

	function cancelReservation() {

		var minOneChecked;	
		var $cancelItineraryChecked = $('input[name="cancel-itinerary-checkbox"]:checked')
		if($cancelItineraryChecked.length!=0){
			itineraryNumberChecked = $($cancelItineraryChecked).attr("data-itinerarynumber");
			minOneChecked = true;
		}
		$('input[name="reservation-checkbox"]:checked').each(function () {
			minOneChecked = true;

			checkedRooms.push($(this).attr("data-reservationNum"));

		});
		userMail = $("#mail").text();


		if (minOneChecked) {
			var popupParams = {
				title: 'Do you want to cancel your reservation?',
				callBack: cancelRequest,
				needsCta: true,
				isWarning: true
			}
			warningBox(popupParams);
		} else {
			var popupParams = {
				title: 'Please select the booking to cancel!',
				callBack: '',
				needsCta: false,
				isWarning: true
			}
			warningBox(popupParams);
		}

	}

	function cancelRequest() {
		var obj = new Object();
		
		obj.itineraryNumber = itineraryNumberChecked		
   		obj.checkedRooms = checkedRooms;
   		obj.emailAddress  = userMail;
   		obj.hotelId = hotelId;
   		var cancelObj= JSON.stringify(obj);

        dataCache.session.setData("cancelResponse", cancelObj);
        sessionStorage.setItem("cancelResponse", cancelObj);

        window.location.assign(redirect);
		
		$('a.proceed-btn-con').attr('href', redirect);
		
	}


}