function onOfferDetailsHotelSelection(hotelPath) {
    var user = getUserData();
    var offerCategory = $("[data-offer-rate-code]").data("offer-category");
	var memberOnlyOffer = $("[data-offer-rate-code]").data("offer-member-only");

    if((memberOnlyOffer == true || offerCategory.includes('member')) && !(user && user.membershipId)){
		dataCache.session.setData("memberOnlyOffer", "true");
    } else {
		dataCache.session.setData("memberOnlyOffer", null);
    }
    try {
        var ROOMS_PATH = "";
        var nights = '';
        var startsFrom = '';
        var endsOn = '';
        var today = moment().format('MMM Do YY');
        var tomorrow = '';
        var dayAfterTomorrow = '';
        var navPath = hotelPath.replace(".html", "");
        var bookingOptions = dataCache.session.getData('bookingOptions');
        var rooms = bookingOptions.roomOptions ? bookingOptions.roomOptions.length : 1;

        if ($('.cm-page-container').hasClass('ama-theme')) {
            ROOMS_PATH = "accommodations.html";
        } else {
            ROOMS_PATH = "rooms-and-suites.html";
        }
        navPath = navPath + ROOMS_PATH;

        var offerRateCode = $("[data-offer-rate-code]").data("offer-rate-code");
        var comparableOfferRateCode = $("[data-comparable-offer-rate-code]").data("comparable-offer-rate-code");
        var offerTitle = $("[data-offer-title]").data("offer-title");
        var noOfNights = $("[data-offer-no-of-nights]").data("offer-no-of-nights");
        var roundTheYear = $("[data-offer-round-the-year]").data("offer-round-the-year");
        var offerStartsFrom = $("[data-offer-starts-from]").data("offer-starts-from");
        var offerEndsOn = $("[data-offer-ends-on]").data("offer-ends-on");

        // If no of nights are not present, nights is 1 (default)
        if (noOfNights && noOfNights != "" && noOfNights != '0') {
            nights = noOfNights;
        } else {
            nights = 1;
        }

        // override default t+15 booking date for custom start and end dates and adding nights
        if (offerRateCode && !roundTheYear) {
            if (comparableOfferRateCode) {
                offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
            }
            if (offerStartsFrom && offerEndsOn) {
                startsFrom = moment(offerStartsFrom).format('MMM Do YY');
                endsOn = moment(offerEndsOn).format('MMM Do YY');
                if (moment(startsFrom, 'MMM Do YY').isSameOrBefore(moment(today, 'MMM Do YY'))
                        && moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                } else if (moment(today, 'MMM Do YY').isSameOrBefore(moment(startsFrom, 'MMM Do YY'))) {
                	tomorrow = moment(startsFrom, 'MMM Do YY').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }
            } else if (!offerStartsFrom && offerEndsOn) {
                endsOn = moment(offerEndsOn).format('MMM Do YY');
                if (moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }

                // default t+15 booking dates and adding nights
            } else {
                tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
                dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
            }

            // round the year offer with t+15 dates and nights
        } else {
            tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
            dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
        }
        navPath = updateQueryString("overrideSessionDates", "true", navPath);
        navPath = updateQueryString("from", tomorrow, navPath);
        navPath = updateQueryString("to", dayAfterTomorrow, navPath);
        navPath = updateQueryString("rooms", rooms, navPath);
        navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
        navPath = updateQueryString("offerTitle", offerTitle, navPath);

        // creating the URL for the button
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if ((!navPath.includes("http://") && navPath.includes("http:/"))
                || (!navPath.includes("https://") && navPath.includes("https:/"))) {
            navPath = navPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        window.location.href = navPath;
    } catch (err) {
        console.error('error caught in function onOfferSelection');
        console.error(err);
    }
}
