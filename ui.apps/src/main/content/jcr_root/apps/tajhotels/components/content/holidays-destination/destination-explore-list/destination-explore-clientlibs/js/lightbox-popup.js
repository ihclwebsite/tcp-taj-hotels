window.addEventListener('load', function() {
    $('.showMap-close').click(function() {
        $('.show-popUp-local-place-details').hide();
        $('.cm-local-details-container').hide();
        $('body').css('overflow','auto');
    })
    setTimeout(function() {
        var recomCrds=$('.concierge-Recommends');
        recomCrds.each(function(){
            $(this).showMore();
        });
        $('.ho-description-txt').each(function() {
            $(this).cmToggleText({
                charLimit : 200,
            })
        });
    }, 100);
});

function myFunction(obj) {
    var parent = $(obj).data("con-name");
    $('#' + parent).children('.show-popUp-local-place-details').show();
    $('#' + parent).children('.cm-local-details-container').show();
    $('body').css('overflow','hidden');
}
