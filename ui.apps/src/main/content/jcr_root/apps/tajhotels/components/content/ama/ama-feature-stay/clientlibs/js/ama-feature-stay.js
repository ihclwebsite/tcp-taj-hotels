$(document).ready(function() {

    if ($(window).width() < 767) {
      	$('.feature-stay-container').owlCarousel({
            loop:true,
            center: false,
            autoWidth :false,
            autoplay:true,
            autoplayTimeout:4000,
            smartSpeed: 1000,
            margin:10,
            items:1,
            nav:true,
            navText: ["<img src='/content/dam/tajhotels/icons/ama/arrow-left.png'>","<img src='/content/dam/tajhotels/icons/ama/arrow-left.png'>"],
        });
    }
    else {
        $('.feature-stay-container').owlCarousel({
            loop:true,
            center: true,
            autoplay:true,
            autoplayTimeout:4000,
            smartSpeed: 1000,
            autoWidth :true,
            margin:50,
            nav:true,
            navText: ["<img src='/content/dam/tajhotels/icons/ama/arrow-left.png'>","<img src='/content/dam/tajhotels/icons/ama/arrow-left.png'>"],
        });
	}
});
