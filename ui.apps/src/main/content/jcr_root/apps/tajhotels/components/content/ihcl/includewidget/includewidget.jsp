<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="com.adobe.granite.ui.components.Config" %><%

    Config cfg = cmp.getConfig();

    String path = cfg.get("path", String.class);
	String fieldLabel = cfg.get("fieldLabel", String.class);
	String fieldDescription = cfg.get("fieldDescription", String.class);
	String required = cfg.get("required", String.class);
	String name = cfg.get("name", String.class);
	Boolean addParagraphTag = cfg.get("addParagraphTag", Boolean.class);
    if (path == null) {
        return;
    }

    // Get the resource using resourceResolver so that the search path is applied.
    Resource targetResource = resourceResolver.getResource(path);

    if (targetResource == null) {
        return;
    }
    try{
        javax.jcr.Node node = targetResource.adaptTo(javax.jcr.Node.class);
            if (fieldLabel != null && !"".equals(fieldLabel.trim())) {
                node.setProperty("fieldLabel", fieldLabel);
            }if (fieldDescription != null && !"".equals(fieldDescription.trim())) {
                node.setProperty("fieldDescription", fieldDescription);
            }if (required != null && !"".equals(required.trim())) {
                node.setProperty("required", required);
            }if (name != null && !"".equals(name.trim())) {
                node.setProperty("name", name);
            }node.setProperty("addParagraphTag", addParagraphTag);


    }catch(Exception e){
    }
    cmp.include(targetResource, cfg.get("resourceType", String.class), cmp.consumeTag());
%>