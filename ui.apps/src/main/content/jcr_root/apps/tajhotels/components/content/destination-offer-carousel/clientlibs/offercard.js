function onOfferSelection(offerDetailsPath, offerRateCode, offerTitle) {
    var hotelPath = $("[data-hotel-path]").data("hotel-path");
    if (hotelPath) {

        var ROOMS_PATH = "/rooms-and-suites.html";
        var navPath = hotelPath.replace(".html", "");
        navPath = navPath + ROOMS_PATH;
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if (offerRateCode) {
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }
        window.location.href = navPath;
    } else {
        window.location.href = offerDetailsPath;
    }
}
