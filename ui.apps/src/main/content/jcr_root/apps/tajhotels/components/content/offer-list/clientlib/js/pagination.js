$(document).ready(function(){
	init_pagination();
});

init_pagination= function()
{
	var $offersContainer = $(".offers-container");
	$offersContainer.siblings('.jiva-spa-show-more').remove();
	$offersContainer.showMore(6,3);
	var filteredOffersLength = $offersContainer.find(".row .offers-page-card-component").length;
	$('.no-offers-found').toggle(filteredOffersLength==0);	
}