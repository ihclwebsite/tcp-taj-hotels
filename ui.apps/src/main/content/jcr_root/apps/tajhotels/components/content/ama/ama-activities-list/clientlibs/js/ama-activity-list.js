$(document).ready(function() {
    // activity dropdown filter
    $('#actfilterBy').change(function() {
        var value = this.value;
        filterActResults(value);
        setInitialHeights();
    });

    $('.activities-show-more').unbind().click(function(e) {
        var wrapper = $('.our-activities .act-cards-container');
        var cards = $(wrapper).find('.combo-div:visible');
        var cardsNum = cards.length;
        var cardsNumInRow;
        if ($(window).width() < 767) {
            cardsNumInRow = 1;
        } else {
            cardsNumInRow = 2;
        }
        var singleCard = $(wrapper).find('.combo-div:visible:first');
        var cardHeight = singleCard.outerHeight(true);
        var heightLimit = cardHeight * 2;
        var totalHeight = (cardsNum / cardsNumInRow) * cardHeight;
        // $( cards ).toggleClass( 'show-more-card-element-active' );

        var textReference = $(this).find('span');
        var iconReference = $(this).find('i');
        if (textReference.text() == "SHOW MORE") {
            wrapperHeight = wrapper.height();
            console.log("total ht: " + totalHeight + "wrapper ht: " + wrapperHeight);
            if (totalHeight <= wrapperHeight) {
                textReference.text("SHOW LESS");
                iconReference.toggleClass('cm-rotate-show-more-icon');
                wrapper.css('maxHeight', 'unset');
            } else {
                var setHeight = wrapperHeight + heightLimit
                wrapper.css('maxHeight', setHeight);
                console.log("maxheight: " + setHeight);
                if (totalHeight <= wrapper.height()) {
                    console.log("set show less");
                    textReference.text("SHOW LESS");
                    iconReference.toggleClass('cm-rotate-show-more-icon');
                }
            }
        } else {
            textReference.text("SHOW MORE");
            iconReference.toggleClass('cm-rotate-show-more-icon');
            wrapper.css('maxHeight', heightLimit);
            this.scrollIntoView();
        }
    });
    setInitialHeights();
    $(window).on('resize', setInitialHeights);
});
function setInitialHeights() {
    var showMoreContents = $('.activities-show-more');
    console.log("inside initial ht fn each fn");
    var showMoreWrapper = $('.act-cards-container');
    var cardsNum = $(showMoreWrapper).find('.combo-div:visible').length;
    var singleCard = $(showMoreWrapper).find('.combo-div:visible:first');
    var cardHeight = singleCard.outerHeight(true);
    var cardsNumInRow;
    var reduceHt = false;
    var textReference = showMoreContents.find('span');
    var iconReference = showMoreContents.find('i');
    if (textReference.text() == "SHOW LESS") {
        console.log("inside here");
        iconReference.toggleClass('cm-rotate-show-more-icon');
    }
    textReference.text("SHOW MORE");
    console.log("window width: " + $(window).width());
    if ($(window).width() < 767) {
        cardsNumInRow = 1;
    } else {
        cardsNumInRow = 2;
    }
    var heightLimit = cardHeight * 2;
    var totalHeight = (cardsNum / cardsNumInRow) * cardHeight;
    console.log("ht limit: " + heightLimit + ", total ht: " + totalHeight + ", cardsnum: " + cardsNum
            + ", cardsnuminrow: " + cardsNumInRow + ", cards ht: " + cardHeight);
    if (totalHeight > heightLimit) {
        showMoreWrapper.css('maxHeight', heightLimit);
        $('.activities-show-more').removeClass('no-display-show-more');
    } else {
        $('.activities-show-more').addClass('no-display-show-more');
    }
}

function filterActResults(searchKey) {

    var filteredActList = $(".our-activities .card-wrapper").filter(function() {
        console.log('print the data value and searchkey' + $(this).data('destination-type') + "::" + searchKey);
        if (searchKey == "all") {
            $(this).parent().show();
        } else {
            if ($(this).data('destination-type').includes(searchKey)) {
                $(this).parent().show();
            } else {
                $(this).parent().hide();
            }
        }
    });
}

function popUpActivityList(elem) {
    var presentScroll = $(window).scrollTop();
    var thisLightBox = $(elem).closest('.card-wrapper').next().css('display', 'block').addClass('active-popUp');
    $(".cm-page-container").addClass('prevent-page-scroll');
    $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');

    $(document).keydown(function(e) {
        if (($('.active-popUp').length) && (e.which === 27)) {
            $('.showMap-close').trigger("click");
        }
    });
    $('.showMap-close').click(function() {

        thisLightBox.hide().removeClass('active-popUp');
        $(".cm-page-container").removeClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
        $(window).scrollTop(presentScroll);

    })

}
