$(document).ready(function() {
    processData();
    var json = dataCache.session.getData("enquiryObject");
    if (json) {
        autofillFieldsFromCache(json);
    }
})

function submitEnquiry() {

    var json = dataCache.session.getData("enquiryObject");
    dataCache.session.removeData('bookingDetailsRequest');
    var bookingObject = getBookingDetails(json);
    dataCache.session.setData("userEnquiryForm", bookingObject);
    sendEnquiry(bookingObject);
}

function autofillFieldsFromCache(json) {
    try {
        $('input[name=packageName]').val(json.packageName);
        var selectedHotelsList = "";
        var hotelsArray = json.selectedHotels;
        for (i = 0; i < hotelsArray.length; i++) {
            if (i == (hotelsArray.length - 1)) {
                selectedHotelsList += hotelsArray[i];
            } else {
                selectedHotelsList += hotelsArray[i] + ",\n";
            }

        }
        $('textarea[name=selectedHotels]').val(selectedHotelsList);
    } catch (error) {
        console.error(error);
    }

}

function sendEnquiry(bookingObject) {

    var value = $(submitButton).data('submitlink');

    $.ajax({
        type : 'GET',
        url : '/bin/HolidaysEnquiryServlet',
        data : 'holidaysEnquiry=' + JSON.stringify(bookingObject),
        success : function(returnmessage) {
            console.info("successully sent email");

            window.location.assign(value);
        },
        error : function(errormessage) {
            console.log("ajax error function");
            var popupParams = {
                title : 'Booking Failed!',
                description : 'Booking Failed. Please check with support team.',
            }
            warningBox(popupParams);
        },
    });
}

function getBookingDetails(json) {

    var bookingDetails = {};
    bookingDetails.packageName = json.packageName;
    bookingDetails.packageDestination = json.packageDestination;
    bookingDetails.selectedHotelsWithCity = json.selectedHotels;
    bookingDetails.userName = $("input[name=userName]").val();
    bookingDetails.gender = $('#enquiryGender option:selected').text();
    bookingDetails.contactNum = $("input[name=phoneNumberCode]").val() + $("input[name=userPhoneNumber]").val();
    bookingDetails.city = $("input[name=userCity]").val();
    bookingDetails.country = $("input[name=userCountry]").val();
    bookingDetails.areaCode = $("input[name=areaCode]").val();
    bookingDetails.userEmail = $("input[name=userEmail]").val();
    bookingDetails.fromDate = $(".enquiry-from-value").val();
    bookingDetails.toDate = $(".enquiry-to-value").val();
    bookingDetails.oncallFlag = $('input[name=holidayOnCallCheckbox]').is(":checked");
    return bookingDetails;
}

function getPreviousPage() {
    var pageUrl = dataCache.session.getData("lastLocation");
    if (pageUrl === undefined) {
        $('.cm-breadcrumb-wrapper').hide();
    } else {
        window.location.assign(pageUrl);
    }
    return pageUrl;
}
