$(document).ready(function() {
	prepareGlobalHotelListJS();
	hotel_carousel();
	popUpProfileIhcl();
});

function hotel_carousel() {
    $('.desc-sectionleader').each(function() {
        $(this).parallelShowMoreFn();
    });
}

function popUpProfileIhcl() {
    window.addEventListener('load', function() {
        $('.view-button').click(function() {
            $("body").css("overflow", "hidden!important");
            $('.profile-popup').show();
            $('.mr-profile-popup').addClass('active-popUp').show();
        });

        $('.showMap-close').click(function() {
            $('.profile-popup').hide();
            $('.mr-profile-popup').removeClass('active-popUp').hide();
            $("body").css("overflow", "scroll");
        })

        $(document).keydown(function(e) {
            if (($('.active-popUp').length) && (e.which === 27)) {
                $('.showMap-close').trigger("click");
            }
        });

    });
}
