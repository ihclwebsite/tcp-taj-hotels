$(document).ready(function(){

    console.log( 'latitude :' +lat) 
});

var map;
    var mapId = document.getElementById('map');
    // if ($(window).width() > 768) {
    //   var mapId = document.getElementById('map');
    //   var serchId = document.getElementById('pac-input');
    // } else {
    //   var mapId = document.getElementById('map2');
    //   var serchId = document.getElementById('pac-input2');
    // }
    // $(window).resize(function () {
    //   if ($(window).width() > 768) {
    //     var mapId = document.getElementById('map');
    //     var serchId = document.getElementById('pac-input');
    //   } else {
    //     var mapId = document.getElementById('map2');
    //     var serchId = document.getElementById('pac-input2');
    //   }
    // });
    function initMap() {
      map = new google.maps.Map(mapId, {
        zoom: 12,
  		scaleControl: false,
  streetViewControl: false,
  fullscreenControl: true,
        center: new google.maps.LatLng(lat, lon),
        styles: [
          {
            "featureType": "all",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#ebe3cd"
              }
            ]
          },
          {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#523735"
              }
            ]
          },
          {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#f5f1e6"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#c9b2a6"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#dcd2be"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#ae9e90"
              }
            ]
          },
          {
            "featureType": "landscape.natural",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dfd2ae"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dfd2ae"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#93817c"
              }
            ]
          },
          {
            "featureType": "poi.attraction",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "poi.attraction",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.attraction",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "on"
              },
              {
                "weight": 7
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "labels",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "labels.text",
            "stylers": [
              {
                "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.business",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#a5b076"
              }
            ]
          },
          {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#447530"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#f5f1e6"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#f8c967"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#e9bc62"
              }
            ]
          },
          {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e98d58"
              }
            ]
          },
          {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#db8555"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#fdfcf8"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#806b63"
              }
            ]
          },
          {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dfd2ae"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#8f7d77"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#ebe3cd"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dfd2ae"
              }
            ]
          },
          {
            "featureType": "transit.station.bus",
            "elementType": "all",
            "stylers": [
              {
                "visibility": "on"
              },
              {
                "weight": 1.5
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#b9d3c2"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#92998d"
              }
            ]
          }
        ]
      });



      var markers = [];
      // Listen for the event fired when the user selects a prediction and retrieve
      // more details for that place.

      var icons = {
        default: {
          icon: icn
        },
        selected: {
          icon: icn
        }
      };

      var workshops = [{ "address": address, "contact": phone, "lat": lat, "lng": lon }]

      var selectedMarker = null
      var selectedInfowindow = null

      // Create markers.
      workshops.forEach(function (workshop) {
        if (!workshop.lat || !workshop.lng) {
          return
        }

       var contentString = "<div class='maplocationInfo'><div class='mapAddress'>"+address+"</div><div class='mapPhone' id='mapPhoneId'>"+phoneNumbers+"</div></div>";



        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 400
        });

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(workshop.lat, workshop.lng),
          icon: icons['default'].icon,
          map: map,
          title: workshop.name
        });

        marker.addListener('click', function () {
          if (selectedMarker) {
            selectedMarker.setIcon(icons['default'].icon)
          }

          if (selectedInfowindow) {
            selectedInfowindow.close()
          }

          marker.setIcon(icons['selected'].icon)
          infowindow.open(map, marker)

          selectedMarker = marker
          selectedInfowindow = infowindow
        });
      });
    }