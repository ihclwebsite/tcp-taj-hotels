$(document).ready(function() {
    var captchaValidation = false;
    $('.mr-selectbox').selectBoxIt();
    var $fromDate = $('#from-date');
    var $toDate = $('#to-date');
    var $selectedFromDate = $('.selected-start-date');
    var $selectedToDate = $('.selected-end-date');
    var $inputField = $('.mr-input-field');
    var $checkboxField = $('.mr-checkbox-field');
    var $mailinput = $('#emailinput');
    var $alterMailInput = $('#alter-emailinput');

    $selectedFromDate.datepicker().on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var searchFromDate = (new Date(element.date.valueOf()));
            var formattedSearchFromDate = moment(searchFromDate).format('DD/MM/YYYY');
            $fromDate.val(formattedSearchFromDate);
            $(this).removeClass('visible');
            var searchToDate = $selectedToDate.datepicker('getDate');
            if (!searchToDate || (searchFromDate >= searchToDate)) {
                $selectedToDate.datepicker('setDate', searchFromDate);
                $toDate.val(formattedSearchFromDate);
            }
        }
    });

    $selectedToDate.datepicker().on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var searchToDate = (new Date(element.date.valueOf()));
            var formattedSearchToDate = moment(searchToDate).format('DD/MM/YYYY');
            $toDate.val(formattedSearchToDate);
            $(this).removeClass('visible');
            var searchFromDate = $selectedFromDate.datepicker('getDate');
            if (!searchFromDate || (searchToDate <= searchFromDate)) {
                $selectedFromDate.datepicker('setDate', searchToDate);
                $fromDate.val(formattedSearchToDate);
            }
        }
    });

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $(this).siblings('.jiva-spa-date').addClass('visible');
    });

    $mailinput.on('blur', function() {
        var mailaddress = $mailinput.val();
        if (!IsValidEmail(mailaddress)) {
            $(this).siblings('.warning-txt').addClass('warning-display');
        }
    });

    $alterMailInput.on('blur', function() {
        var altermailaddress = $alterMailInput.val();
        if (altermailaddress != "") {
            if (!IsValidEmail(altermailaddress)) {
                $(this).siblings('.warning-txt').addClass('warning-display');
            }
        }
    });

    function validateInputs() {
        var inputMandatoryFlag = true;
        $inputField.each(function() {
            if ($(this).val() == "") {
                if ($(this).siblings().hasClass("validate-inputs-txt")) {
                    $(this).siblings('.validate-inputs-txt').addClass('warning-display');
                    inputMandatoryFlag = false;
                }
            }
        });
        return inputMandatoryFlag;
    }
    $inputField.on('focus', function() {
        $(this).siblings('.validate-inputs-txt').removeClass('warning-display');
        $(this).siblings('.warning-txt').removeClass('warning-display');
    });

    $('.submitbtn').on('click', function() {
        if (validateInputs()) {
            /*if (captchaValidation == false) {
                $('.captcha-warning').css('display', 'block');
            } else {
                $('.captcha-warning').css('display', 'none');*/
                console.info('good to go');
                safariEnquiry();
                $('.submitbtn').css('opacity', '0.5');
                clearInputs();
           // }
        } else {
            // Do Nothing
        }
    });

    function clearInputs() {
        $inputField.each(function() {
            $(this).val("");
        });
        $checkboxField.each(function() {
            $(this).prop("checked", false);
        })
    }
    $('.clrbtn').on('click', function() {
        clearInputs();
    });
});

function safariEnquiry() {

    var originalUrl = window.location.href;

    var values = window.location.href.slice(window.location.href.indexOf('?') + 1) // post_id=239&category=Angular

    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    var params = [];
    for (var i = 0; i < url.length; i++) {
        params[i] = url[i].split("=");
        console.log(params[i]);
    }

    var source = '';
    var medium = '';
    var campaign = '';
    for (var i = 0; i < params.length; i++) {

        if (params[i][0] == "source") {
            source = params[i][1];
        }
        if (params[i][0] == "medium") {
            medium = params[i][1];
        }
        if (params[i][0] == "campaign") {
            campaign = params[i][1];
        }
    }

    var title = $('#safari-enquiry-form-title').val();

    var firstName = $('#firstnameinput').val();

    var lastName = $('#lastnameinput').val();

    var nationality = $('#safari-enquiry-form-nationality').val();

    var emailId = $('#emailinput').val();

    var alternateEmail = $('#alter-emailinput').val();

    var contactNumber = $('#contactinput').val();

    var checkedValue = [];
    var inputElements = $("input[name='bookingOthersCheckbox']:checked");
    for (var i = 0; inputElements[i]; ++i) {
        if (inputElements[i].checked) {
            checkedValue.push(inputElements[i].value);
            // break;
        }
    }

    console.log(checkedValue);

    var comments = $('#comments-input').val();

    var travelPlanFromDate = $('#from-date').val();

    var travelPlanToDate = $('#to-date').val();

    var numberOfAdults = $('#number-of-adults').val();

    var numberOfChildren = $('#number-of-children').val();

    var requestString = "title=" + title + "&firstName=" + firstName + "&lastName=" + lastName + "&nationality="
            + nationality + "&emailId=" + emailId + "&alternateEmail=" + alternateEmail + "&contactNumber="
            + contactNumber + "&checkedValue=" + checkedValue + "&comments=" + comments + "&travelPlanFromDate="
            + travelPlanFromDate + "&travelPlanToDate=" + travelPlanToDate + "&numberOfAdults=" + numberOfAdults
            + "&numberOfChildren=" + numberOfChildren + "&source=" + source + "&medium=" + medium + "&campaign="
            + campaign;

    var safariEnquiryConfLink = $('.safari-enquiry-conf-link').val() + ".html";

    var safariEnquiryErrorLink = $('.safari-enquiry-error-link').val() + ".html";

    console.log(requestString);

    return $.ajax({
        method : "GET",
        url : "/bin/safariEnquiry",
        data : requestString
    }).done(function(res) {
        console.info("bookAppointment success");
        window.location.assign(safariEnquiryConfLink);
    }).fail(function() {
        console.error("bookAppointment failed ");
        window.location.assign(safariEnquiryErrorLink);
        dataCache.session.setData("confStatus", "Error");
    });

};
