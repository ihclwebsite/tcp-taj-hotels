function getOfferData() {
    $(document).ready(function() {
        getOfferData()
        var bookingDetails = dataCache.session.getData("bookingOffer");

        $(".package-cst-wrp span").text(bookingDetails.packagePrice);
        $(".package-price-wrp span").text(bookingDetails.price);
        $(".package-baggage-wrp span").text(bookingDetails.baggage);
        $(".package-no-of-person span").text(bookingDetails.noOFPerson);
        $(".package-from-to").text(bookingDetails.packageName);
    });
}
