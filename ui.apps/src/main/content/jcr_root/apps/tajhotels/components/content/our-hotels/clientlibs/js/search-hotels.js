document.addEventListener( 'DOMContentLoaded', function() {
    var options = {};
    var $countryDropdownFilter = $( '#countryFilter' );
    var $filterBy = $( '#filterBy' );
    var dropDowns = {
        country: {
            elem: $countryDropdownFilter,
            default: 'Country',
            selected: null,
            dependent: {
                elem: null
            }
        },
        filterBy: {
            elem: $filterBy,
            default: 'filterBy',
            selected: null,
            dependent: {
                elem: null
            }
        }
    }
	


    function initHotelsPageFilter() {
        var countryDropdownFilter = new initDropdown( $countryDropdownFilter, dropDowns.country );
        var filterBy = new initDropdown( $filterBy, dropDowns.filterBy );

        countryDropdownFilter.initialize();
        filterBy.initialize();
    };

    initHotelsPageFilter();

    //show map functionality
    // desktop
    $( '.search-row-wrap-desktop .show-map-button' ).click( function() {
        $(this).addClass('d-none');
        $('.search-row-wrap-desktop .hide-map-button').removeClass('d-none');
        $( '.map-overlay' ).removeClass('d-none');
        $('.hotel-allContent-wrapper').removeClass('full-width');
    });
    $( '.search-row-wrap-desktop .hide-map-button' ).click( function() {
        $(this).addClass('d-none');
        $('.search-row-wrap-desktop .show-map-button').removeClass('d-none');
        $( '.map-overlay' ).addClass('d-none');
        $('.hotel-allContent-wrapper').addClass('full-width');
    });

    // mobile
    $( '.search-row-wrap-mobile .show-map-button' ).click( function() {
        $( '.map-overlay' ).show();
        $( '.searchBar-wrap' ).addClass( 'show-on-map-view' );
        $( '.search-row-wrap-mobile' ).hide();
    });
    $( '.map-back-icon' ).click( function() {
        $( '.map-overlay' ).hide();
        $( '.searchBar-wrap' ).removeClass( 'show-on-map-view' );
        $( '.selected-location-hotels' ).hide();
        $( '.search-row-wrap-mobile' ).show();
    });


    //preventing closing of search-popup when selecting a filter
    $('.searchBar-wrap').click( function( e ) {
        e.stopPropagation();
    });

    // clearing input text in search bar
    $('.clear-input-icon').click(function(e){
        $(this).prev('.hotel-search').val("");
        $('.clear-input-icon').removeClass('show-clear-input');
        filterResults("", null, null);
    });

    // hotel type dropdown filter
    $('#filterBy').change(function() {
        var value = this.value.toLowerCase();
        filterResults(null, null, value);
    });

    // country dropdown filter
    $('#countryFilter').change(function() {
        var value = this.value.toLowerCase();
        filterResults(null, value, null);
    });

    // Keyword search
    $(".searchbar-input.hotel-search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        if(value.length === 0){
            $('.clear-input-icon').removeClass('show-clear-input');
        } else{
            $('.clear-input-icon').addClass('show-clear-input');
        }
        if (value.length > 2) {
            var filteredDest = filterResults(value, null, null);
        } else {
            filterResults(null, null, null);
        }
    });

    function filterResults(searchKey, country, hotelType) {
        var searchKey = searchKey || "";
        var country = country || $('#countryFilter').val().toLowerCase();
        var hotelType = hotelType || $('#filterBy').val().toLowerCase();
		$(".website-brand").show();
        $(".other-brand").show();
        var filteredDest = $(".our-hotels-container .card-wrapper").filter(function() {
			var websiteCount = 0;
			var otherCount = 0;
            // filter based on country
            if (country === "all" || ($(this).data('city') && ($(this).data('city').indexOf(country) > -1))) {
                // filter hotels based on types
                var filteredHotels = $(this).find(".hotel-properties-container .hotel-container").filter(function() {
                    if(hotelType === "all" || ($(this).data('hoteltypes') && ($(this).data('hoteltypes').indexOf(hotelType) > -1))) {
                       $(this).show();
					   if ($(this).hasClass('website-hotels')) {
						   websiteCount++;
					   } else if ($(this).hasClass('other-hotels')) {
						   otherCount++;
					   }
                       return true;
                    }
                    $(this).hide();
                    return false;
                });
                if(filteredHotels.length) {
                    // check filtered hotels for keyword search if any
                    var showDest = performDestKeyWordSearch(searchKey, $(this), filteredHotels);
					hideShowOtherTitle(otherCount);
					setDestinationHotelCount($(this), websiteCount, otherCount);
                    $(this).toggle(showDest);
                    return showDest;
                } else {
                    $(this).hide();
                    return false;
                }
            } else {
                $(this).hide();
                return false;
            }
        });
        // perform any post filter activity
        postFilterActivity(filteredDest);
        hideShowBrand();
        return filteredDest;

    }

    function postFilterActivity(filteredDest) {
        if(filteredDest.length === 1) {
            changePosition(null, $(filteredDest[0]));
        } else {
            updateMap();
        }
    }

	function hideShowBrand() {
		var websiteBrand =  $(".website-brand .card-wrapper").is(":visible");
		var otherBrand = $(".other-brand .card-wrapper").is(":visible");
		if (websiteBrand == true && otherBrand == true) {
			$(".website-brand").show();
			$(".other-brand").show();
		} else if (websiteBrand == false && otherBrand == true) {
			$(".website-brand").hide();
			$(".other-brand").show();
		} else if (websiteBrand == true && otherBrand == false) {
			$(".website-brand").show();
			$(".other-brand").hide();
		} else if (websiteBrand == false && otherBrand == false) {
			$(".website-brand").hide();
			$(".other-brand").hide();
		} 
		
	}
	
	function hideShowOtherTitle(otherHotel) {
        if (otherHotel != 0) {
			$(".other-ihcl-hotels").show();
            $(".other-title").show();
		} else {
			$(".other-ihcl-hotels").hide();
            $(".other-title").hide();
		}
	}

    function setDestinationHotelCount(destination, websiteHotelsCount, otherHotelsCount) {
		destination.find('.website-hotel-count').html(websiteHotelsCount);
		destination.find('.other-hotel-count').html(otherHotelsCount);
    }

    // Perform search on a single destination and its hotels
    function performDestKeyWordSearch(keyword, destination, hotels) {
        var keyFilteredHotels = [];
        var match = (destination.data('title').toLowerCase().indexOf(keyword) > -1);
        if (!match) {
            keyFilteredHotels = hotels.filter(function() {
                $(this).toggle($(this).data('title').toLowerCase().indexOf(keyword) > -1);
                return ($(this).data('title').toLowerCase().indexOf(keyword) > -1);
            });
            if (hotels.length > keyFilteredHotels.length) {
                destination.find(".show-more-imageUnderline").show();
            }
        } else {
            destination.find(".show-more-imageUnderline").hide();
        }
        return (match || !!keyFilteredHotels.length);
    }

});
