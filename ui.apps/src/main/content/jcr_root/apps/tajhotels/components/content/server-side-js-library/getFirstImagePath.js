use(function () {
    var returnJson = {};
    var imagePaths = this.imagePaths;
    var imagesJson = JSON.parse(imagePaths);
    var firstImage = imagesJson[0];
    if (firstImage != undefined) {
        var firstImagePath = firstImage['imagePath'];
        returnJson = {options: firstImagePath}
    }

    return returnJson
});
