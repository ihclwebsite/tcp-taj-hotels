$(document).ready(function() {
    var targetTxt = $('.destination-city-desc > p');
    if ((window.screen.width) < 991) {
        var concatTxt = '';
        targetTxt.each(function() {
            concatTxt = concatTxt + $(this).text() + '<br/><br/>';
        });

        $('.destination-city-desc').html(concatTxt);

        $('.destination-city-desc').cmToggleText({
            charLimit : 100,
            showVal : "Show More",
            hideVal : "Show Less",
        });

    } else {
        $('.destination-city-desc > p').last().cmToggleText({
            charLimit : 200,
            showVal : "Show More",
            hideVal : "Show Less",
        });

    }
    $('.mr-specific-City-carousel-wrapper').customSlide(1, true);

    socialShare();

});

function socialShare() {

    var url = window.location.href;
    var text = $('.share-btn-wrapper').attr("data-share-text");

    $("#socials").jsSocials({
        url : url,
        text : text,
        showCount : false,
        showLabel : false,
        shares : [ {
            share : "facebook",
            logo : "/content/dam/tajhotels/icons/social-icons/ic-facebook@2x.png"
        }, {
            share : "twitter",
            logo : "/content/dam/tajhotels/icons/style-icons/twitter.svg"
        }, {
            share : "googleplus",
            logo : "/content/dam/tajhotels/icons/social-icons/google+.png"
        } ]

    })

}
