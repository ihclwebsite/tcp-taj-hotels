var croppedUrlForHtml = "";
function ajaxCropServlet(ele) {
    var errorStringOne = "Image can't be requested(pre-defined Message)";

    var id = ele.id;
    var suffix = $('#' + id).data('suffix');
    var imageUrl = $('#image-crop-selector' + '-' + suffix).attr("src");
    var x = $('#xaxis' + '-' + suffix).val();
    var y = $('#yaxis' + '-' + suffix).val();
    var width = $('#width' + '-' + suffix).val();
    var height = $('#height' + '-' + suffix).val();

    var requestString = "incomingImageUrl=" + imageUrl + "&x=" + x + "&y=" + y + "&width=" + width + "&height="
            + height;

    $.ajax({
        type : 'GET',
        url : '/bin/crop/requestedImage',
        data : requestString,
        success : function(responseMessage) {
            var jsonResponse = JSON.stringify(responseMessage);
            console.info("Ajax Call Success.");
            croppedUrlForHtml = responseMessage.croppedImageUrl;
            document.getElementById('link' + '-' + suffix).value = croppedUrlForHtml;
        },
        error : function(errormessage) {
            console.error('AJAX Response Error:-> Request Status: ' + errormessage.status + ', Status Text: '
                    + errormessage.statusText + ', Response Text: ' + errormessage.responseText);
        }
    });
}
