document
        .addEventListener(
                'DOMContentLoaded',
                function() {

                    $('.footer-destination-expand-button').click(function() {
                        if ($(this).text() == '+') {
                            $('.footer-destination-list').slideDown(100);
                            $(this).text('-');
                        } else {
                            $(this).text('+');
                            $('.footer-destination-list').slideUp(100);
                        }
                    })

                    $('.footer-tic-expand-button').click(function() {
                        if ($(this).find('button').text() == '+') {
                            $('.footer-brands-list').slideDown(100);
                            $(this).find('button').text('-');
                        } else {
                            $(this).find('button').text('+');
                            $('.footer-brands-list').slideUp(100);
                        }
                    });

                    $('#newsletter').click(function() {

                    });
                    updateBrandSpecificSocialLinks();

                    // The below function call is declared at dining-filter js
                    try {
                        populateFilterFromHtml();
                    } catch (e) {
                        // Dining filter is not available in the page
                        // console.log("The function[populateFilterFromHtml()]
                        // can't be called. Dining filter is not available in
                        // the page ")
                    }
                    toggleFooterPadding();
                });

function updateBrandSpecificSocialLinks() {
    var $pageContainer = $('.cm-page-container');
    var $facebookLink = $('.facebook-redirect');
    var $instagramLink = $('.instagram-redirect');
    var $twitterLink = $('.twitter-redirect');
    var $youtubeLink = $('.youtube-redirect')
    if ($pageContainer.hasClass('vivanta-theme')) {
        $facebookLink.attr('href', 'https://www.facebook.com/VivantaHotels');
        $instagramLink.attr('href', 'https://www.instagram.com/vivantahotels');
        $twitterLink.attr('href', 'https://twitter.com/vivantahotels');
        $youtubeLink.attr('href', 'https://www.youtube.com/user/VivantabyTaj');
    } else if ($pageContainer.hasClass('gateway-theme')) {
        $facebookLink.attr('href', 'https://www.facebook.com/TheGatewayHotel');
        $instagramLink.attr('href', 'https://www.instagram.com/thegatewayhotels');
        $twitterLink.attr('href', 'https://twitter.com/TheGatewayHotel');
        $youtubeLink.attr('href', 'https://www.youtube.com/user/TheGatewayHotel');
    }
}

function toggleFooterPadding(){
	if($('.book-ind-container').length!=0){
		$('.footer').addClass('footer-padding-for-cart-info');
	}
}