$(window).load(function() {
	if(!$(".cm-page-container").hasClass("ama-theme")){
		if ($("#booking-search-campaign").is(":visible") == false) {
	        updateBookingCache();
	    }
	}
    
});

function updateBookingCache() {
    var redirectPath = "";
    var hotelId = "";
    var today = new Date();
    today.setDate(today.getDate() + 16);
    var tommorow = moment(new Date()).add(17, 'days')['_d'];
    var dayAfterTommorow = moment(new Date()).add(18, 'days')['_d'];

    var checkInDate = moment(tommorow).format("MMM Do YY");
    var checkOutDate = moment(dayAfterTommorow).format("MMM Do YY");
    var numberOfRooms = 1;
    var selectedRoomOptions = [];

    var packageNoOfAdults = 1;
    var packageNoOfChildren = 0;
    var roomInfo = {
        adults: packageNoOfAdults,
        children: packageNoOfChildren,
        initialRoomIndex: 1
    }
    selectedRoomOptions.push(roomInfo);

    var bookingOptions = dataCache.session.getData("bookingOptions");
    if (bookingOptions != undefined) {
        bookingOptions.fromDate = checkInDate;
        bookingOptions.toDate = checkOutDate;
        bookingOptions.roomOptions = selectedRoomOptions;
        bookingOptions.isAvailabilityChecked = false;
        bookingOptions.nights = 1;
        dataCache.session.setData("bookingOptions", bookingOptions);
    }

}