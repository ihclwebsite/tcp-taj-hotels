$(document).ready(function() {

    var isRateRequired = $("#hide-rates").data("hide-rate-container");
    if (isRateRequired == true) {
        $('.hotelCard_hotelDetails .taj-loader.spinner_wait_con').hide();
        $('.rate_con').hide();
        $('.hotelCard_hotelDetails .hotelDetails-name-wrap').width("90%");
    }
})

function onOfferDetailsHotelSelection(hotelPath, hotelId) {

    var redirectToSynXis = $("#synxis-settings").data("redirecttosynxis");
    if (redirectToSynXis == true) {
        goToSynXis(hotelId);
    } else {
        goToHotelRoomsPage(hotelPath, hotelId);

    }

}

function goToHotelRoomsPage(hotelPath, hotelId) {

    var ROOMS_PATH = "rooms-and-suites.html";
    if ($('.cm-page-container').hasClass('ama-theme') || hotelPath.includes('amastaysandtrails.com')) {
        ROOMS_PATH = "accommodations.html";
    }
    var offerRateCode = $("[data-offer-rate-code]").data("offer-rate-code");
    var comparableOfferRateCode = $("[data-offer-rate-code]").data("comparable-offer-rate-code");
    var offerTitle = $("[data-offer-rate-code]").data("offer-title");
    var noOfNights = $("[data-offer-rate-code]").data("offer-no-of-nights");
    var offerStartsFrom = $("[data-offer-rate-code]").data("offer-starts-from");

	// to check member signin in rooms and suits page
    var dataOfferCategory = $("[data-offer-rate-code]").data("offer-category");
	var memberOnlyOffer = $("[data-offer-rate-code]").data("offer-member-only");

    if((dataOfferCategory && dataOfferCategory.toLowerCase().includes('member')) || 
        (memberOnlyOffer && memberOnlyOffer == true)){
		dataCache.session.setData("memberOnlyOffer", "true");
    }


    var bookOptions = dataCache.session.getData("bookingOptions");
    var fromDate = "";
    if (bookOptions != undefined) {
        fromDate = moment(bookOptions.fromDate, "MMM Do YY");
    }
    if (offerStartsFrom) {
        var startsFrom = moment(offerStartsFrom).format('MMM Do YY');
        if (!moment(startsFrom, 'MMM Do YY').isSameOrBefore(moment(fromDate, 'MMM Do YY'))) {
            fromDate = startsFrom;
            bookOptions.fromDate = fromDate;
            var nextDate = moment(fromDate, "MMM Do YY").add(1, 'days').format("MMM Do YY");
            bookOptions.toDate = nextDate;

        }
    }
    var nights;
    if (noOfNights && noOfNights != '0') {
        nights = noOfNights;
        var toDate = moment(fromDate, "MMM Do YY").add(parseInt(nights), 'days').format("MMM Do YY");
        bookOptions.toDate = toDate;

    }
    if (bookOptions != undefined) {
        bookOptions.nights = parseInt(moment.duration(
                moment(moment(bookOptions.toDate, "MMM Do YY")).diff(moment(bookOptions.fromDate, "MMM Do YY")))
                .asDays());
    }
    dataCache.session.setData("bookingOptions", bookOptions);

    var navPath = hotelPath.replace(".html", "");
    if (hotelPath.slice(-1) === '/') {
        navPath = navPath + ROOMS_PATH;
    } else {
        navPath = navPath + '/' + ROOMS_PATH;
    }
    if (navPath != "" && navPath != null && navPath != undefined && !navPath.includes("https")) {
        navPath = navPath.replace("//", "/");
    }
    var from = moment(bookOptions.fromDate, "MMM Do YY").format('D/MM/YYYY');
    var to = moment(bookOptions.toDate, "MMM Do YY").format('D/MM/YYYY');
    if (offerRateCode) {
        if (comparableOfferRateCode) {
            offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
        }
        navPath = updateQueryString("overrideSessionDates", "true", navPath);
        navPath = updateQueryString("from", from, navPath);
        navPath = updateQueryString("to", to, navPath);
        navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
        navPath = updateQueryString("offerTitle", offerTitle, navPath);
    }

    addNavigateToHotelDataToDataLayer(hotelId, hotelPath);

    window.open(navPath, '_blank');
}

function goToSynXis(hotelId) {

    var offerRateCode = $(".offers-room-container").data("offer-rate-code");
    var synXisParamName = $("#synxis-settings").data("synxisparam");
    var startDate = $(".offers-room-container").data("offer-starts-from");
    var rooms = 1;
    var adult = 1;
    var child = 0;
    var arrive = moment(startDate).format('YYYY-MM-DD');
    var depart = moment(arrive, "YYYY-MM-DD").add(1, 'days').format("YYYY-MM-DD");
    var chain = $("#synxis-settings").data("chain");
    var currency = $("#synxis-settings").data("currency");
    var level = $("#synxis-settings").data("level");
    var locale = $("#synxis-settings").data("locale");
    var sbe_ri = $("#synxis-settings").data("sberi");
    var synXisParamValue = $("#synxis-settings").data("offercode");
    var hotel = hotelId;

    var synxisLink = "https://be.synxis.com/";

    redirectPath = synxisLink + "?adult=" + adult + "&arrive=" + arrive + "&chain=" + chain + "&child=" + child
            + "&currency=" + currency + "&depart=" + depart + "&hotel=" + hotel + "&level=" + level + "&locale="
            + locale + "&rooms=" + rooms + "&sbe_ri=" + sbe_ri;

    if (synXisParamName && synXisParamName != '') {
        if (synXisParamValue && synXisParamValue != '') {
            redirectPath = redirectPath + "&" + synXisParamName + "=" + synXisParamValue;
        }
    } else if (synXisParamValue && synXisParamValue != '') {
        redirectPath = redirectPath + "&promo=" + synXisParamValue;
    } else if (offerRateCode && offerRateCode != '') {
        redirectPath = redirectPath + "&offerRateCode=" + offerRateCode;
    }
    window.open(redirectPath, "_blank");
}


//updated for global data layer
function addNavigateToHotelDataToDataLayer(hotelId, hotelPath) {
    if(location.pathname.includes('/en-in/offers')) {
        try {
            var offerDetails = $('.offerdetails');
            var offerName = offerDetails.find('.cm-header-label').text();
            var offerCode = offerDetails.find('.offers-room-container').data('offer-rate-code');
            var offerValidity = offerDetails.find('.validity-container .validity-content').text().trim();
            var offerCategory = offerDetails.find('.offers-room-container').data('offer-category');
            var eventName = offerName.split(' ').join('') + '_ParticipatingHotels_KnowMore_OffersPage_ViewHotel';
            var offerObj = {};
            offerObj.hotelName = hotelPath.split('/')[5];
            offerObj.hotelCode = hotelId;
            offerObj.offerName = offerName;
            offerObj.offerCode = offerCode;
            offerObj.offerValidity = offerValidity;
            offerObj.offerCategory = offerCategory;
        
            addParameterToDataLayerObj(eventName, offerObj);
        }
        catch(err) {
            console.log('error in adding data to datalayer');
        }
    }
}