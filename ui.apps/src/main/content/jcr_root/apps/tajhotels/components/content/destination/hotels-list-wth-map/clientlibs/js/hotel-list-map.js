function getBookingOptionsSessionDataMapCard() {
    return dataCache.session.getData("bookingOptions");
}

function setCurrencyInSessionStorageMapCard(currency) {
    var bookingOptions = getBookingOptionsSessionDataMapCard();
    bookingOptions.currencySelected = currency;
    dataCache.session.setData("bookingOptions", bookingOptions);
}

function getCommaFormattedNumberMapCard(number) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number;

    } else {
        formattedNumber = number.toLocaleString('en-IN')
    }
    return formattedNumber;
}