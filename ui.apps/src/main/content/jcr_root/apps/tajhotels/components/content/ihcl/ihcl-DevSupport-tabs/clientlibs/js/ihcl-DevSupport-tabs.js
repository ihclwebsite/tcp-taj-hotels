$(document).ready(function() {
    switchingContent();
});

function switchingContent() {

    var tabswitch = document.getElementById("mr-tab-id");
    if (tabswitch != null) {
        var divide = tabswitch.getElementsByClassName("mr-switch-content");
        for (var i = 0; i < divide.length; i++) {
            divide[i].addEventListener("click", function() {
                var currentElement = document.getElementsByClassName("tab-switch-active");
                currentElement[0].className = currentElement[0].className.replace("tab-switch-active", "");
                this.className += " tab-switch-active";

            });
        }
    }

    $('.mr-switch-content').click(function() {
        var currentDiv = $(this);
        var contentElementDiv = currentDiv.parent().children(), positions = contentElementDiv.index(currentDiv);

        var targetsWrapper = $('.mr-content-description-wrap').eq(positions);

        if (!targetsWrapper.hasClass('mr-active-content')) {
            $('.mr-content-description-wrap.mr-active-content').toggleClass('mr-active-content', false);
            targetsWrapper.toggleClass('mr-active-content', true);
        }

    });
    if (deviceDetector.checkDevice() == "small") {
        $('.mr-content-desc').each(function() {
            $(this).cmToggleText({
                charLimit : 400
            })
        });
    }

};