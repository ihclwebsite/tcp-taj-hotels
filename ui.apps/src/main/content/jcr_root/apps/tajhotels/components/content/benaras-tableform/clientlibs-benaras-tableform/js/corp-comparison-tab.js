
    $( document ).ready( function() {
        $( '.consolidate-wrapper' ).hide();
        $( '.tab-click' ).click( function() {
            $( this ).siblings().removeClass( 'tab-selected' );
            $( this ).addClass( 'tab-selected' );

            if ( $( this ).index() == 0 ) {
                $( '.consolidate-wrapper' ).hide();
                $( '.standalone-wrapper' ).show();
            } else if ( $( this ).index() == 1 ) {
                $( '.consolidate-wrapper' ).show();
                $( '.standalone-wrapper' ).hide();
            }
        } )
    } )

