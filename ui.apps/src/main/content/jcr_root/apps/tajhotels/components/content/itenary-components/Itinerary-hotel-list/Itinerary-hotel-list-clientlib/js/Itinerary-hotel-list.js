$(document).ready(function () {
    var app = new Vue({
        data: {
            feeds: []
        },
        filters: {
            moment: function (date) {
                if (date != undefined) {
                    return date.split(" ")[0];
                }
            }
        },
        methods: {
            updatePoints: function (response) {
                console.log('Ajax call for servlet succeeded and response received as:');
                console.log(response.data.length);
                this.feeds = response.data;
                for(let i=0; i<this.feeds.length; i++){
					this.feeds[i].dateTime = new Date(this.feeds[i].timestamp).toDateString();
                }
            },
            handleFetchFailure: function (response) {
				console.log('error in api call...', response);
            },
            fetchPointsTransactions: function () {
                var token = $('#token-long').val();
                if(token) {
                    $.ajax({
                        type: 'GET',
                        url: 'https://graph.instagram.com/me/media',
                        data : {
                            "access_token" : token,
                            "fields" : "id,caption,media_url,permalink,thumbnail_url,timestamp,media_type,username,children"
                        },
                        error: this.handleFetchFailure,
                        success: this.updatePoints,
                    });
                }
            }
        },
        updated: function () {
            if (this.feeds && this.feeds.length == 0) {
                console.log('no data received...')
            }
        }
    });

    app.$mount('.instagram-feeds-card');

    function addFetchTriggerListener() {
		app.fetchPointsTransactions();
    }
	
	addFetchTriggerListener();
});
