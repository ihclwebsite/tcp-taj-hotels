$( document ).ready(function() {

    var enquiryObject=dataCache.session.getData("userEnquiryForm");
    if(enquiryObject !=null){

        var title = getTitle(enquiryObject.gender);
	    $('#userName').text(title.concat(" ",enquiryObject.userName));
    	$('.confirmation-email-bold').text(enquiryObject.userEmail);
    }else{
		$('#userName').hide();
        $('.confirmation-email').hide();
    }

    function getTitle(gender){
		var title="";
       	switch(gender) {
    		case "Female":
       			 title = "Ms.";
       			 break;
   			default:
        		title = "Mr.";
		}
        return title;
    }

    var urlSocial;
    urlSocial = window.location.href;
    var text = $('.share-btn-wrapper').attr("data-share-text");

    $("#socials").jsSocials({
        url : urlSocial,
        text : text,
        showCount : false,
        showLabel : false,
        shares : [ {
            share : "facebook",
            logo : "/content/dam/tajhotels/icons/social-icons/ic-facebook@2x.png"
        }, {
            share : "twitter",
            logo : "/content/dam/tajhotels/icons/style-icons/twitter.svg"
        }, {
            share : "googleplus",
            logo : "/content/dam/tajhotels/icons/social-icons/google+.png"
        }]

    })
});
