$( 'document' ).ready( function() {
    /*//Click event for selecting type of bed
	$( '.cart-room-bed' ).click( function() {
		$(this).closest('.cart-room-bed-type').find( '.cart-room-bed' ).not( this ).removeClass( 'selected-bed-type' );
		$( this ).addClass( 'selected-bed-type' );
	} );
	// Script to hanlde Count of Adults and Children
	$( '.cart-increase-count' ).click( function() {
		var currentCountNode = $( this ).siblings( '.cart-count-value' );
		var currentCount = parseInt( currentCountNode.text() ) + 1;
		currentCountNode.text( currentCount );
	} );
	$( '.cart-decrease-count' ).click( function() {
		var currentCountNode = $( this ).siblings( '.cart-count-value' );
		var currentCount = parseInt( currentCountNode.text() ) - 1;
		if ( currentCount < 0 )
			currentCount = 0;
		currentCountNode.text( currentCount );
	} );
	// Deleting a selected room
	$( '.cart-room-delete-icon' ).click( function() {
		$( this ).closest( '.cart-selected-rooms-card' )
		.hide( 'slow', function() {
			$( this ).remove();
		} );
	} );*/
    
//    [IHCLCB start] 
    if($(window).width() < 767) {
        $('.ihcl-theme .cart-selected-rooms-card-container').on('click', '.cart-room-number', function() {       
            $(this).closest('.cart-selected-rooms-card').toggleClass('selected-rooms-cart-open-accr');
        });
    }
//    [IHCLCB End]
});
function selectedRoomDelete(){
	var deleteWarningData = $($.find("[data-warning-type='DELETEPOPUP']")[0]);
	 var popupParams = {
        title: deleteWarningData.data("warning-heading") || "Are you sure you want to delete this Room?",
        description: deleteWarningData.data("warning-description") || null,
    }
	return popupParams;
}