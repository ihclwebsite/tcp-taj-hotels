$(document).ready(function() {
    var checkitem = function() {
        var $this;
        $this = $("#bannerCarousel");
        if (window.matchMedia('(max-width: 767px)').matches) {
            $this.children(".carousel-control-prev").hide();
            $this.children(".carousel-control-next").hide();
        }
    };

    checkitem();
    $("#bannerCarousel").on("slid.bs.carousel", "", checkitem);

    $("#bannerCarousel").on("touchstart", function(event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function(event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');
            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');
            }
        });
        $("#bannerCarousel").on("touchend", function() {
            $(this).off("touchmove");
        });
    });
    $('.carousel').each(function() {
        var carouselItemCount = $(this).find('.carousel-item').length;
        if (carouselItemCount < 2) {
            $(this).find('.carousel-control').hide();
        }
    });

	if($('#bannerHeightAuto')){
        var heightAuto = $('#bannerHeightAuto').val();
        if(heightAuto == 'true'){
            $('.carousel-item').find('.bannerImage img').attr("style", "height: auto !important");
        }
    }

   
});
