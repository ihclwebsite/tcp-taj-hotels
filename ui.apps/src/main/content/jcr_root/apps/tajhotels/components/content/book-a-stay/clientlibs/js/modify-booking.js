$(document).ready(function(){		
	modifyBookingState = dataCache.session.getData('modifyBookingState');
	var modifyBookingQuery = getQueryParameter('modifyBooking');
	if( modifyBookingQuery =="true" && modifyBookingState){		
	 	if(modifyBookingState!='modifyRoomType'){					
			$('.book-stay-btn').trigger('click');
		}		
	}else{
		console.log("Booking modification is not invoked")
	}
	if(modifyBookingState && modifyBookingState!='modifyAddRoom'){
		$('.cart-room-delete-icon, .cart-addRoom').remove();
	}
	$('.carts-book-now').on('click',updateBookedRoom);
});

function updateBookedRoom(){
	if(modifyBookingState){
		var bookedRoomsModify ={};
		var modifiedBookingOptions = dataCache.session.getData('bookingOptions');
		var selection = modifiedBookingOptions.selection;
		var bookedRoomsData = modifiedBookingOptions.bookedRoomsModify;
		var modifiedCheckInDate = moment(modifiedBookingOptions.fromDate,"MMM Do YY").format("YYYY-MM-DD");
        var modifiedCheckOutDate = moment(modifiedBookingOptions.toDate,"MMM Do YY").format("YYYY-MM-DD");
		var totalAmountAfterTax = 0;
		var totalAmountBeforeTax = 0;
		$(selection).each(function(index,data){
			var reservationNumber = bookedRoomsData[index].reservationNumber;
			var modifiedRoomData = {
				bedType: data.roomBedType,
				bookingStatus: true,
				cancellable: true,
				cancellationPolicy: "Staggered cancel policy",
				discountedNightlyRates: null,
				hotelId: data.hotelId,
				modifyRoom: false,
				nightlyRates: data.nightlyRates,
				noOfAdults: parseInt(data.adults),
				noOfChilds: parseInt(data.children),
				petPolicy: null,
				promoCode: null,
				rateDescription: data.rateDescription,
				ratePlanCode: data.ratePlanCode,
				resStatus: "Committed",
				reservationNumber: reservationNumber,
				roomCostAfterTax: data.roomTaxRate + data.roomBaseRate,
				roomCostBeforeTax: data.roomBaseRate,
				roomTypeCode: data.roomTypeCode,
				roomTypeDesc: "",
				roomTypeName: data.title
			}	
			bookedRoomsModify[reservationNumber] = modifiedRoomData;			
		});
		var bookingDetailsResponse = "";
		if(dataCache.session.getData('bookingDetailsRequest')) {
			bookingDetailsResponse = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
			bookingDetailsResponse.checkInDate = modifiedCheckInDate;
			bookingDetailsResponse.checkOutDate = modifiedCheckOutDate;
			var bookingRoomList = bookingDetailsResponse.roomList;
			$(bookingRoomList).each(function(index){
				var reservationNumber = this.reservationNumber;
				if(bookedRoomsModify[reservationNumber]){
					bookingRoomList[index] = bookedRoomsModify[reservationNumber];
				}
				totalAmountBeforeTax += parseFloat(this.roomCostBeforeTax);
				totalAmountAfterTax += parseFloat(this.roomCostAfterTax);
			});
			bookingDetailsResponse.roomList = bookingRoomList;
			bookingDetailsResponse.totalAmountBeforeTax = totalAmountBeforeTax;
			bookingDetailsResponse.totalAmountAfterTax = totalAmountAfterTax;
			dataCache.session.setData('bookingDetailsRequest',JSON.stringify(bookingDetailsResponse));
		}
	}	
}
function modifyBookingInBookAStay(modifyBookingState){
	var bookedOptions = "";
	if(dataCache.session.getData('bookingDetailsRequest')) {
		bookedOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
	}
	var $bookingSearchContainer = $('#booking-search');
	var $basDateOccupancyPromoWrapper = $('.bas-date-container-main-wrap');
	var $basDateContainer = $('.bas-date-container-main');
	var $basOccupancyContainer = $('.bas-hotel-details-container');
	var $basPromoCodeContainer = $('.bas-specialcode-container');
	var $basAddRoomOption = $('.bas-add-room');
	$basAddRoomOption.addClass('bas-hide');
	$bookingSearchContainer.addClass('modify-booking-disabled-state');
	$basDateOccupancyPromoWrapper.children().addClass('modify-booking-disabled-state');
	if(modifyBookingState == 'modifyDate'){
		$basDateContainer.removeClass('modify-booking-disabled-state');				
	}else if(modifyBookingState == 'modifyRoomOccupancy'){
		$basOccupancyContainer.removeClass('modify-booking-disabled-state');
	}else if(modifyBookingState == 'modifyAddRoom'){
		$basOccupancyContainer.removeClass('modify-booking-disabled-state');
		$basAddRoomOption.removeClass('bas-hide');
	}
}