document.addEventListener('DOMContentLoaded', function() {
    var scrollTop = $(window).scrollTop();
    $('.experience-details-close').click(function() {
        $('.experience-card-details-wrap').hide();
        $(".cm-page-container").removeClass('prevent-page-scroll');
        $(window).scrollTop(scrollTop);
    });
    $.each($('.experience-details-desc'), function(i, value) {
        $(value).cmToggleText({
            charLimit : 250
        })
    });

    $('.all-experiences-list').showMore();

    $('.mr-experinces-details-location').cmToggleText({
        charLimit : 35,
        showVal : "",
        hideVal : ""
    });
    
    $('.experience-details-short-desc').each(function(){
        $(this).cmToggleText({
            charLimit :135
        });
    });
});

function myFunction(obj) {
    var parent = $(obj).data("experience-name");
    var spaces = parent.replace(/ /g, '');
    $('#' + spaces).children('.experience-card-details-wrap').show();
    $(".cm-page-container").addClass('prevent-page-scroll');
    trigger_experience_view(parent);
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9qaXZhLXNwYS1jYXJkcy9qaXZhLXNwYS1jYXJkcy1jb21wLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoIGRvY3VtZW50ICkucmVhZHkoIGZ1bmN0aW9uKCkge1xyXG4gICAgJCggJy5zaWduYXR1cmUtZXhwLXdyYXAnICkuc2hvd01vcmUoKTtcclxuICAgICQoICcuc3BhLWluZHVsZ2VuY2Utd3JhcCcgKS5zaG93TW9yZSgpO1xyXG4gICAgJCggJy5ib2R5LXNjcnViLXdyYXAnICkuc2hvd01vcmUoKTtcclxufSApOyJdLCJmaWxlIjoibWFya3VwL2NvbXBvbmVudHMvaml2YS1zcGEtY2FyZHMvaml2YS1zcGEtY2FyZHMtY29tcC5qcyJ9

