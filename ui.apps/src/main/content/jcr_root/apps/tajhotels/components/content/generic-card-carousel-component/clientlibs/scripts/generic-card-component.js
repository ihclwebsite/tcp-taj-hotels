$(document).ready(function() {
    console.log("In generic-card-component.js");
    try {
        initiateGenericCardCarousel();
    } catch (error) {
        console.error(error);
    }
});

function initiateGenericCardCarousel() {
    var rootComponentClass = ".generic-card-component-root-carousel";
    var componentRootCarousels = $(rootComponentClass);
    var numOfRootCarousels = $(componentRootCarousels).length;
    for (var i = 0; i < numOfRootCarousels; i++) {
        setupCarouselFor(componentRootCarousels[i]);
    }

    function setupCarouselFor(element) {
        var fetchFromUrl = $(element).attr("data-fetch-from");
        $.ajax({
            type: "GET",
            url: fetchFromUrl,
            data: '',
            dataType: "json",
            beforeSend: function(event) {
                showSpinners(element);
            },
            success: function(data) {
                injectInformationToCards(element, data);
                hideSpinners(element);
            },
            error: function(textStatus, errorThrown) {
                showError(element);
            }
        });
    };

    function addCarousels(element) {
        var numOfCards = $(element).attr("data-num-of-cards");
        var uniqueId = $(element).attr("data-unique-id");
        var selector = rootComponentClass + " ." + uniqueId;
        var numOfCards = $(this).attr("data-num-of-cards");
        $(element).customSlide(numOfCards);
    }

    function showError(element) {
        $('.spinner-con').hide();
        $('.enquiry-form-error-popup').show();
    }

    function showSpinners(element) {
        console.log("Attempting to show spinners at the element:");
        console.log(element);
    };

    function hideSpinners(element) {
        console.log("Attempting to hide spinners at the element:");
        console.log(element);
    };

    function injectInformationToCards(element, cards) {
        var example1 = new Vue({
            el: $(element)[0],
            data: {cards: cards},
            mounted: function() {
                $(this.$el).customSlide(4);
            }
        });
    };
}
