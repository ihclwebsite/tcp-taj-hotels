/*
 * @Author Srikanta
 * ImageCropSelector 1.1.1
 */
function letsCrop(e, x, y, width, height) {
    if (x == 0 && width == 0) {
        x = 200;
        y = 200;
        width = 600;
        height = 400;
    }
    var dataObj = {
        "x" : x,
        "y" : y,
        "width" : width,
        "height" : height,
        "rotate" : 0,
        "scaleX" : 1,
        "scaleY" : 1
    };
    var id = e.id;
    var suffix = $('#' + id).data('suffix');
    var aspect = $('#' + id).data('aspect');
    var $image = $('#' + e.id);
    if ($image != undefined && $image.length > 0) {
        $cropperObj = $image.cropper({
            // standard aspect ratios:- 16.9,4:3,1:1,2:3,NAN[open ratio]
            // NaN=free Ratio
            aspectRatio : aspect,
            data : dataObj,
            movable : false,
            cropBoxResizable : true,
            dragMode : 'move',
            zoomOnWheel : false,
            viewMode : 3,
            zoom : false,
            crop : function(event) {
                // update the x, y, width, height according to the cropper
                updateValue(Math.round(event.detail.x), Math.round(event.detail.y), Math.round(event.detail.width),
                        Math.round(event.detail.height), suffix);
            },

        });
    }
}

// function to update the x, y, width, height values frequently
function updateValue(x, y, w, h, suffix) {
    document.getElementById('xaxis' + '-' + suffix).value = x;
    document.getElementById('yaxis' + '-' + suffix).value = y;
    document.getElementById('width' + '-' + suffix).value = w;
    document.getElementById('height' + '-' + suffix).value = h;

}
