$(document).ready(function() {
        $('.holidays-dest-package-card-desc').each(function() {
            $(this).cmToggleText({
                charLimit: 150,
            });
        });

        $('.holidays-dest-package-card-inclusions-show-more').each(function() {
            $(this).click(function(e) {
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
                $(this).parents().eq(1).addClass('holidays-inclusions-on-show');
            })
        });
        $('.holidays-dest-package-card-inclusions-close').each(function() {
            $(this).click(function(e) {
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
            })
        });
        $('.holidays-dest-package-card-inclusions-mobile-heading > img').each(function() {
            $(this).click(function(e) {
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
            })
        });

        $('.holidays-dest-more-packages, .holidays-dest-package-card-inclusions-content').click(function(e) {
            e.stopPropagation();
        })

        $('body').click(function(e) {
            $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
        })
        
    });

function formTheRedirectionURL(authoredURL) {
    var url =authoredURL;
    if(!url.includes('https')) {
       var url = url + ".html"
    }
  window.location.href =url;     
}