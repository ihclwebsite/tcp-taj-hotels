function trendingCards() {
    $(document).ready(function() {
        $('.inclusion-con.cm-show-more-inclusion').on('click', function() {
            var parentOffset = $('.cm-page-container').offset();
            var targetOffset = $(this).closest('.offers-card-inner-wrapper').offset();
            $('html,body').animate({
                scrollTop : ((parentOffset.top* -1) + targetOffset.top) - 100
            }, 300);
            $(this.closest(".holiday-trending.rooms-and-suites-card")).find(".inclusion-popUp-wrapper").find(".inlcusion-wrapper").show();
        });

        $('img.inclusion-back-arrow, img.inclusion-close').on('click', function() {
            $(this.closest(".inlcusion-wrapper")).hide();
        });
        $('.rooms-and-suites-card.holiday-trending .cm-btn-secondary').on('click', function() {
            //will go to offers page 
        });

        //show-mpre show-less paragraph
        $('.holiday-trending').find('.room-description.holiday-desc').each(function() {
            $(this).cmToggleText({
                charLimit: 100
            });
        })
    })
}

$(document).ready(function() {
trendingCards();
});

function addToSession(nights){
if(nights)	{
    
	var bookOptions = dataCache.session.getData("bookingOptions");
	var fromDate = moment(bookOptions.fromDate, "MMM Do YY");
	var nextDate = moment(fromDate, "MMM Do YY").add(parseInt(nights), 'days').format("MMM Do YY");
	bookOptions.toDate = nextDate;
	bookOptions.nights = parseInt(nights);
    bookOptions.roomOptions[0].adults = 2;
	dataCache.session.setData("bookingOptions", bookOptions);
}
}