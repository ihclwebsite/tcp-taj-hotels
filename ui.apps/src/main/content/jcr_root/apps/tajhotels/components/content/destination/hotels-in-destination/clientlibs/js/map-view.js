window.addEventListener('load', function() {
    var hotelTypeCheckBox = '';
    var selectedFiltered;
    var filterOrder;
    var hotelData = {};

    var detail = {};
    var hotelMasterDataJSON;

    var allMarkers = [];
    var markerSizeFlag = false;
    var imageList = {
        "taj": ["/etc.clientlibs/tajhotels/components/content/destination/hotels-list-wth-map/clientlibs/resources/tajMarker.png",
            "/etc.clientlibs/tajhotels/components/content/destination/hotels-list-wth-map/clientlibs/resources/tajLarge.png"
        ],
        "vivanta": ["/content/dam/tajhotels/icons/style-icons/vivanta-small.png",
            "/content/dam/tajhotels/icons/style-icons/vivanta-large.png"
        ],
        "gateway": ["/etc.clientlibs/tajhotels/components/content/destination/hotels-list-wth-map/clientlibs/resources/gatewayMarker.png",
            "/etc.clientlibs/tajhotels/components/content/destination/hotels-list-wth-map/clientlibs/resources/gatewayLarge.png"
        ],
        "seleqtions": ["/content/dam/tajhotels/icons/style-icons/seleqtions-small.png",
            "/content/dam/tajhotels/icons/style-icons/seleqtions-large.png"
        ]
    };
    var hotelType;
    hotelCardClicked = function(targetHotelCard) {

        hotelCardActivate(targetHotelCard.getAttribute('data-hotelid'));
        markerSizeChange(targetHotelCard.getAttribute('data-hotelid'));
    }

    function hotelCardActivate(hotelId) {
        if (($("#" + hotelId + ".mr-map-view-hotelsLists-wrap").hasClass("active") == false)) {

            if ($('.mr-map-view-hotelsLists-wrap.active').length > 0)
                $('.mr-map-view-hotelsLists-wrap.active').removeClass("active");
        }
        $("#" + hotelId + ".mr-map-view-hotelsLists-wrap").addClass("active");

    }

    function markerSizeChange(targetMarkerId) {

        for (var i = 0; i < allMarkers.length; i++) {
            if (allMarkers[i].hotelId == targetMarkerId) {
                if (allMarkers[i].markerSizeFlag == false) {
                    allMarkers[i].setIcon(allMarkers[i].iconFocused);

                    allMarkers[i].markerSizeFlag = true;
                }
            } else {
                allMarkers[i].setIcon(allMarkers[i].iconNormal);
                allMarkers[i].markerSizeFlag = false;
            }
        }
    }

    function createMarker(hotelData) {

        var marker;
        detail.lat = hotelData.lat;
        detail.lng = hotelData.lng;
        hotelType = hotelData.type;

        if (hotelType == "taj") {
            imagePath = imageList.taj[0];
            imagePathLarge = imageList.taj[1];

        } else if (hotelType == "vivanta") {

            imagePath = imageList.vivanta[0];
            imagePathLarge = imageList.vivanta[1];
        } else if (hotelType == "gateway") {
            imagePath = imageList.gateway[0];
            imagePathLarge = imageList.gateway[1];
        }else {
            imagePath = imageList.seleqtions[0];
            imagePathLarge = imageList.seleqtions[1];

        }

        var pinIcon = {
            url: imagePath,
            scaledSize: new google.maps.Size(46, 58),
        };

        var pinIconLarge = {
            url: imagePathLarge,
            scaledSize: new google.maps.Size(64, 84),
        }
        marker = new google.maps.Marker({
            position: detail,
            map: mapDestination,
            icon: pinIcon,
            animation: google.maps.Animation.DROP,
        });
        marker.hotelId = hotelData.id;
        marker.markerSizeFlag = markerSizeFlag;
        marker.iconNormal = pinIcon;
        marker.iconFocused = pinIconLarge;
        marker.hotelType = hotelType;

        allMarkers.push(marker);
        google.maps.event.addListener(marker, 'click', function () {
            if ((window.screen.width) < 767) {
                if (this.markerSizeFlag == false) {
                    $('.mr-mapview-carousel-wrap').show();
                    markerSizeChange(this.hotelId);
                    $('.carousel-item.mr-mapCarousel.active').removeClass('active');
                    $('#' + this.hotelId + '.carousel-item.mr-mapCarousel').addClass('active');
                }

            } else {
                markerSizeChange(this.hotelId);
                hotelCardActivate(this.hotelId);
            }
        });


    };

    function initMapDestination() {
        var latInit = parseFloat($('.mr-map-view-hotelsLists-wrap:first-child').find('.mapHotel-lat').html());
        var longInit = parseFloat($('.mr-map-view-hotelsLists-wrap:first-child').find('.mapHotel-lan').html());
        var latlng = new google.maps.LatLng(latInit, longInit);
        mapDestination = new google.maps.Map(document.getElementById('mapDestination'), {
            zoom: 11,
            minZoom: 1.5,
            center: latlng
        });


    };
    var hotelTypeClass = '';

    function afterCardsRender() {
        //to create Marker 
        $('.mr-map-view-hotelsLists-wrap').each(function() {

            hotelData.id = $(this)[0].getAttribute('data-hotelid');
            hotelData.type = $(this)[0].getAttribute('data-hotelbrand');
            hotelData.name = $(this).find('.mapHotel-title').html()
            hotelData.lat = parseFloat($(this).find('.mapHotel-lat').html());
            hotelData.lng = parseFloat($(this).find('.mapHotel-lan').html());
            createMarker(hotelData);
        });

        allMarkers[0].setIcon(allMarkers[0].iconFocused);
        allMarkers[0].markerSizeFlag = true;

        $('.carousel-item.mr-mapCarousel:first-child').addClass('active');
        $('.mr-map-view-hotelsLists-wrap:first-child').addClass('active');
    }

    //for mobile swipe functionality
    $('.mr-mapView-layout .carousel').carousel({
        interval: false
    });

    //swap functionality
    $("#mapviewCarousel").on("touchstart", function(event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function(event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');

            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');

            }
            if ($('.carousel-inner.card-carousel').children().length > 1) {
                activeElemnt();
            }
        });

        $("#mapviewCarousel").on("touchend", function() {
            $(this).off("touchmove");

        });

    });


    function init() {
        initMapDestination();
        afterCardsRender();
    };

    var active = {};
    //for mobile
    function activeElemnt() {
        var hotelid = $('.carousel-inner').find('.carousel-item.active').attr('id');
        markerSizeChange(hotelid);

    }

    function updateMarker(checkedBrandList) {
        var showMarkers = [];
        for (var i = 0; i < allMarkers.length; i++) {
            if (checkedBrandList.indexOf(allMarkers[i].hotelType) > -1) {
                allMarkers[i].setVisible(true);

                showMarkers.push(allMarkers[i]);

            } else {
                allMarkers[i].setVisible(false);
            }
        }
        handleZoom(showMarkers);
    }

    function handleZoom(showMarkers) {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < showMarkers.length; i++) {
            bounds.extend(showMarkers[i].getPosition());
        }
        mapDestination.fitBounds(bounds);
        if (mapDestination.getZoom() > 18) {
            mapDestination.setZoom(11);
        }
    }

    function sortBy() {

        $('#sortByHotelFilterDest').on('change', function() {
            selectedFiltered = ($(this).val()).toLowerCase();
            
            if(selectedFiltered=="all" || selectedFiltered=="price"){
            	 filterOrder = "asc";
            }
            else{
            	 filterOrder = "desc";
            }
            	  if (window.screen.width < 768) {
                      sortMeBy($('.carousel-inner.card-carousel.mr-mapView-card-carousel'), $('.carousel-item.mr-mapCarousel'), filterOrder);
                      $('.carousel-item.mr-mapCarousel.active').removeClass('active');
                      $('.carousel-item.mr-mapCarousel:first-child').addClass('active');

                  } else {

                      sortMeBy($('.mr-map-view-hotels-Container'), $('.mr-map-view-hotelsLists-wrap'), filterOrder);
                  }
                  sortMeBy($('.mr-lists-view-hotels-Container'), $('.list-view-wrapper'), filterOrder);
                  $('.mr-lists-view-hotels-Container').parent().showMore();
            
         
        })

    }

    function sortMeBy(sel, elem, order) {
        var $selector = $(sel),

            $element = $selector.children(elem);
        $selector.parent().next('.jiva-spa-show-more').remove();
        var filterVal = selectedFiltered;
        $element.sort(function(a, b) {
            var aelem = parseInt(a.getAttribute("data-" + selectedFiltered));
            var belem = parseInt(b.getAttribute("data-" + selectedFiltered));
            if (order == "asc") {
                if (aelem > belem)
                    return 1;
                if (aelem < belem)
                    return -1;
            } else if (order == "desc") {
                if (aelem < belem)
                    return 1;
                if (aelem > belem)
                    return -1;
            }
            return 0;
        });
        $element.removeAttr('style').detach().appendTo($selector);
    }


    //on click on filter 

    $('.mr-filterIconWrap .mobilefilter-wrap').on('click', function() {
        $('.mr-filter-sort-wrap.hotel-destination-page .filter-checkbox-wrapper').addClass('popup-in-mob');
    })

    $('.filter-checkbox-wrapper .filterCheckbox-backArrow').on('click', function() {

        $('.mr-filter-sort-wrap.hotel-destination-page .filter-checkbox-wrapper').toggleClass('popup-in-mob', false);
    })


    function cmnFilterFunctionality(container, cardList, checkedBrandList) {
 
        container.empty();
        cardList.each(function() {
            var dataset = $(this).data('key');
            var dataBrand = $(this)[0].getAttribute("data-hotelbrand");

            if (checkedBrandList.indexOf(dataBrand) > -1) {
                container.append($(this).removeAttr('style'));

            }
        })
        updateMarker(checkedBrandList);

    }
    
  

    function cmnFilterFunctionalityForMap(checkedBrandList) {
        $('.mr-map-view-hotelsLists-wrap.mr-map-view-mobile-none').each(function() {
            var dataBrand = $(this)[0].getAttribute("data-hotelbrand");

            if (checkedBrandList.indexOf(dataBrand) > -1) {
                $(this).toggleClass('cm-hide', false);
            } else {
                $(this).toggleClass('cm-hide', true);
            }
        })
        updateMarker(checkedBrandList);
    }

    function cmnCheckBoxFilter() {
            var checkedBrandList = []
            $('.selectSubContainer.selectcontainerSortBy select')[0].selectedIndex = 0;
            $('.selectSubContainer.selectcontainerSortBy select').data("selectBox-selectBoxIt").refresh();

            $('.mr-Filter-checkbox-wrap input.mr-filter-checkbox').each(function() {
                if ($(this).is(':checked')) {
                    checkedBrandList.push($(this).val());

                }
            })

            if (window.screen.width > 768) {

                cmnFilterFunctionalityForMap(checkedBrandList);

            } else {
                cmnFilterFunctionality(mobileCardContainer, allMobileCards, checkedBrandList);
            }
            $('.mr-listsMapHotels-Main-Wrapper').next('.jiva-spa-show-more').remove();
            cmnFilterFunctionality(listContainer, allListCards, checkedBrandList);
            listContainer.parent().showMore();

    }
  

    $('.mr-Filter-checkbox-wrap input.mr-filter-checkbox').on('change', function() {
    	cmnCheckBoxFilter();
    	
    })

    $('.map-back-icon').on('click', function() {
        $('.mr-listView-layout').show();
        $('.mr-mapView-layout').hide();
        $('.mr-map-switch').toggleClass('mr-view-toggler-style', false);
        $('.mr-list-switch').toggleClass('mr-view-toggler-style', true);
    })


    function initFilter() {
        var Data = {
            "option": ['All', 'Price'],
        };

        var $destFilter = $('#sortByHotelFilterDest');
        var dropDowns = {
            option: {
                options: Data.option,
                elem: $destFilter,
                default: 'relevant',
                selected: 'relevant',
                dependent: {}
            }

        }
        var filter = new initDropdown($destFilter, dropDowns.option);
        filter.initialize();

    }

    if (document.getElementById('mapDestination')) {
        init();
        initFilter();
        sortBy();
    }

});