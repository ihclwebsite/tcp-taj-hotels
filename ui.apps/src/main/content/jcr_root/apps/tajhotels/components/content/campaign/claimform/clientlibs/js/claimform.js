$(document).ready(

        function() {

            var elementInfo1=document.getElementById('price-info-screenshot')||false;
            if(elementInfo1){elementInfo1.addEventListener('change', readSingleFile, false);}

            $('#best-rate-guar-preferred-mode').selectBoxIt();
            var bookingCurrency = [ 'United States Dollar', 'Euro', 'British Pound Sterling', 'Japanese Yen',
                    'South Korean Won', 'Indian Rupee' ];
            for (i = 0; i < bookingCurrency.length; i++) {
                $('#best-rate-guar-booking-currency').append('<option>' + bookingCurrency[i] + '</option>')
            }

            var searchlist = $('.jiva-spa-search-list');
            $('#best-rate-guar-booking-currency').selectBoxIt();
            var currency = [ 'Euro', 'Dirham', 'Rupee', 'Dollar' ];
            for (i = 0; i < currency.length; i++) {
                $('#best-rate-guar-currency').append('<option>' + currency[i] + '</option>')
            }
            $('#best-rate-guar-currency').selectBoxIt();
            $('#best-rate-guar-rooms').selectBoxIt();
            $('#best-rate-guar-adults-per-room').selectBoxIt();
            $('#best-rate-guar-children').selectBoxIt();
            var approTime = [ '1:00', '2:00', '3:00', '4:00', '5:00', '6:00', '7:00', '8:00', '9:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00',
                    '23:00', '24:00' ]
            for (i = 0; i < approTime.length; i++) {
                $('#best-rate-guar-appr-time').append('<option>' + approTime[i] + '</option>')
            }
            $('#best-rate-guar-appr-time').selectBoxIt();

            /* jivaspa datepicker starts */

            $(' #best-rate-guar-departure-date, #best-rate-guar-approximate-date').datepicker('setStartDate', null).on(
                    'changeDate', function(element) {
                        if ($(this).hasClass('visible')) {
                            var minDate = (new Date(element.date.valueOf()));
                            var selectedDate = moment(minDate).format("DD/MM/YYYY");
                            var target = $(this).closest(".jiva-spa-date-section");
                            target.find('.jiva-input-date').val(selectedDate).removeClass('invalid-input');
                            target.find('.jiva-spa-date-value').text(selectedDate);
                            target.find('.jiva-spa-date').removeClass('visible');
                            target.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
                        }
                    });
            $('#best-rate-guar-arrival-date').datepicker('setStartDate', null).on('changeDate', function(element) {
                if ($(this).hasClass('visible')) {
                    var minDate = (new Date(element.date.valueOf()));
                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                    var target = $(this).closest(".jiva-spa-date-section");
                    target.find('.jiva-input-date').val(selectedDate).removeClass('invalid-input');
                    target.find('.jiva-spa-date-value').text(selectedDate);
                    target.find('.jiva-spa-date').removeClass('visible');
                    target.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
                    $('#best-rate-guar-departure-date').datepicker('setStartDate', minDate);
                }
            });

            $('.best-rate-guar-arrival-date , .best-rate-guar-departure-date, .best-rate-guar-approximate-date')
                    .datepicker('setStartDate', null);

            $('.jiva-spa-date-con').click(function(e) {
                e.stopPropagation();
                $(this).siblings('.jiva-spa-date').addClass('visible');
            });

            $('body').click(function(e) {
                $('.jiva-spa-date').removeClass('visible');
            });

            /* jivaspa datepicker ends */

            /* submit btn click starts */
            function bestRateFormInputValidation(wrapper) {
                var bestRateFormValid = true;
                $(wrapper).find('input.sub-form-mandatory , select.sub-form-mandatory').each(function() {
                    if ($(this).val() == "") {
                        $(this).addClass('invalid-input');
                        invalidWarningMessage($(this));
                        bestRateFormValid = false;
                    }
                });
                return bestRateFormValid;
            }
            $(".best-rate-guar-submit-btn").click(
                    function() {
                        if (bestRateFormInputValidation(".best-rate-guar-form-con")
                                && $('.mr-accept-checkbox').prop('checked')) {
                            $(this).attr("disabled", true);
                            $(this).css('opacity', '0.4');
                            if (captchaValidation == false) {
                                $('.captcha-warning').css('display', 'block');
                            } else {
                                console.info('good to go');
                                submitClaimForm();
                            }
                        } else {
                            if (!$('.mr-accept-checkbox').prop('checked')) {
                                $('.best-rate-form-footer .sub-form-input-warning').css('display', "block")
                            }
                            if (captchaValidation == false) {
                                $('.captcha-warning').css('display', 'block');
                            }
                        }
                    });

            var SEARCH_INPUT_DEBOUNCE_RATE = 3000;

            $('#searchInputJiva').on("keyup", debounce(function(e) {

                if ($(this).val().length > 2) {
                    clearSearchResults();
                    performSearch($(this).val()).done(function() {
                        var _target = $('.jiva-spa-search-list');
                        _target.removeClass('display-none');
                        _target.css('display', 'block');
                        _inputInTarget = $(".jiva-spa-hotel-input > input");
                        $('.jiva-spa-search-list li').on('click', function() {

                            var value = ($(this).children()).text();
                            _inputInTarget.val(value);
                            _inputInTarget.attr('data-email-id', ($(this)).attr('data-email-id'));
                            $("#jiva-spa-city").attr("value", $('#searchInputJiva').attr('data-hotel-city'));
                            _inputInTarget.removeClass('jiva-spa-not-valid');
                            $("#jiva-spa-city").attr("value", ($(this)).attr('hotel-city-name'));
                            _target.css('display', 'none');
                            $('.jiva-search-close').addClass('visible');
                        });
                    });

                } else {
                    clearSearchResults();
                    if (!$('.jiva-spa-search-list').hasClass('display-none')) {

                        $('.jiva-spa-search-list').addClass('display-none');
                    }
                }
            }, SEARCH_INPUT_DEBOUNCE_RATE));

            $('.jiva-search-close').click(function() {
                $(this).removeClass('visible');
                $(".jiva-spa-hotel-input > input").attr("placeholder", "Search");

            });

            $(document).on("click", function(e) {
                $('.jiva-spa-search-list').css('display', 'none');
            });

            $(".mr-accept-checkbox").click(function() {
                var target = $('.best-rate-form-footer .sub-form-input-warning')
                target.css('display', "none");
                if (!$(this).prop('checked')) {
                    target.css('display', "block")
                }
            });
            $(".upload-screenshot").change(function() {
                var target = $(".file-upload .sub-form-input-warning");
                target.css('display', "none");
                if (!$(this).val()) {
                    target.css('display', "block");
                }
            });

            //

        });
var r = new FileReader();
function readSingleFile(evt) {
    file = evt.target.files[0];

    if (file) {
        r.readAsBinaryString(file);
        if (r.readyState == 2) {
            console.log("File is loaded");
        }
    } else {
        console.log("Failed to load file");
    }
};

function clearSearchResults() {
    $('.jiva-spa-search-list').empty();

};

function performSearch(key) {

    return $.ajax({
        method : "GET",
        url : "/bin/hotel-search",
        data : {
            searchText : key
        }
    }).done(function(res) {
        clearSearchResults();
        addHotelSearchResults(res.hotels);
    }).fail(function() {
        clearSearchResults();
    });
};

function addHotelSearchResults(hotels) {
    if (hotels.length) {
        hotels.forEach(function(hotel) {

            $('.jiva-spa-search-list').append(
                    '<li data-email-id="' + hotel.spaEmailId + '" data-hotel-city="' + hotel.city + '" ><label>'
                            + hotel.title + '</label></li>');
        });

    }

};

function submitClaimForm() {

    var claimFormSuccess = $('.claim-form-conf-link').val() + ".html";
    var claimFormFailure = $('.claim-form-error-link').val() + ".html";

    // Contact info
    var firstName = $('#contact-info-first-name').val();
    var lastName = $('#contact-info-last-name').val();
    var ticNumber = $('#contact-info-TIC-number').val();
    var email = $('#contact-info-email').val();
    var preferredModeToContact = $('#best-rate-guar-preferred-mode').val();

    // Booking info
    var bookingId = $('#booking-info-booking-id').val();
    var hotelName = $('#searchInputJiva').val();
    var arrivalDate = $('#booking-info-arrival-date').val();
    var departureDate = $('#booking-info-departure-date').val();
    var specialTerms = $('#booking-info-special-terms').val();
    var bookingRate = $('#booking-info-booking-rate').val();
    var numberOfRooms = $('#best-rate-guar-rooms').val();
    var adultsPerRoom = $('#best-rate-guar-adults-per-room').val();
    var childrenPerRoom = $('#best-rate-guar-children').val();
    var currencySelected = $('#best-rate-guar-booking-currency').val();

    // Lower price info
    var lowerPrice = $('#price-info-lower-rate').val();
    var lowerPriceCurrency = $('#best-rate-guar-currency').val();
    var approximateDate = $('#price-info-approx-date').val();
    var approximateTime = $('#best-rate-guar-appr-time').val();
    var websiteName = $('#price-info-website-name').val();
    var websiteUrl = $('#price-info-website-url').val();
    var screenshot = $('#price-info-screenshot').val();
    var additionalInfo = $('#additional-info').val();

    var form = new FormData();
    form.append("firstName", firstName);
    form.append("lastName", lastName);
    form.append("ticNumber", ticNumber);
    form.append("email", email);
    form.append("preferredModeToContact", preferredModeToContact);
    form.append("hotelName", hotelName);
    form.append("arrivalDate", arrivalDate);
    form.append("departureDate", departureDate);
    form.append("specialTerms", specialTerms);
    form.append("bookingRate", bookingRate);
    form.append("numberOfRooms", numberOfRooms);
    form.append("adultsPerRoom", adultsPerRoom);
    form.append("childrenPerRoom", childrenPerRoom);
    form.append("currencySelected", currencySelected);
    form.append("lowerPrice", lowerPrice);
    form.append("lowerPriceCurrency", lowerPriceCurrency);
    form.append("approximateDate", approximateDate);
    form.append("approximateTime", approximateTime);
    form.append("websiteName", websiteName);
    form.append("websiteUrl", websiteUrl);
    form.append("additionalInfo", additionalInfo);
    form.append("bookingId", bookingId);

    var content = file;
    var fileName = file.name;

    var blob = new Blob([ content ], {
        type : file.type
    });
    form.append("screenshot", blob);
    form.append("fileName", fileName);

    var requestString = "firstName=" + firstName + "&lastName=" + lastName + "&ticNumber=" + ticNumber + "&email="
            + email + "&preferredModeToContact=" + preferredModeToContact + "&bookingId=" + bookingId + "&hotelName="
            + hotelName + "&arrivalDate=" + arrivalDate + "&departureDate=" + departureDate + "&specialTerms="
            + specialTerms + "&bookingRate=" + bookingRate + "&numberOfRooms=" + numberOfRooms + "&adultsPerRoom="
            + adultsPerRoom + "&childrenPerRoom=" + childrenPerRoom + "&currencySelected=" + currencySelected
            + "&lowerPrice=" + lowerPrice + "&lowerPriceCurrency=" + lowerPriceCurrency + "&approximateDate="
            + approximateDate + "&approximateTime=" + approximateTime + "&websiteName=" + websiteName + "&websiteUrl="
            + websiteUrl + "&additionalInfo=" + additionalInfo + "&fileName=" + fileName + "&screenshot=" + r.result;

    var request = new XMLHttpRequest();

    request.open("POST", "/bin/claimFormSubmit", true);
    request.send(form);
    request.onreadystatechange = function(e) {
        if (e.target.readyState == XMLHttpRequest.DONE && e.target.status == 200) {
            window.location.assign(claimFormSuccess);
        } else if (e.target.readyState == XMLHttpRequest.DONE) {
            window.location.assign(claimFormFailure);
        }
    }
};
