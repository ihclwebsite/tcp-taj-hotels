$(document).ready( function() {
	console.log('inside document ready');
    orderGiftHamper();
});

function orderGiftHamper() {
    $('.gift-hamper-payment-button').click(
            function(e) {
            	console.log('clicked');
                var inputValid = true;
                var isGDPRCompliance = $('[data-form-id="guest-GDPRCompliance"]').is(":checked");
                if (inputValid) {
                	console.log("valid")
                    var popupParams = {};
                    popupParams.title = $("#GiftHamperPopUPMessages").attr("data-hamper-failure-title")
                            || 'Donation failed!';
                    $('body').showLoader();
                    //var senderDetails = extractGiftHamperDetails("donationForm");
                    //console.log("senderDetails", senderDetails);
                    //setCookie('senderDetails', JSON.stringify(senderDetails), 1);
                    //setCookie('senderDetails');
                    var senderDetail = getCookie("senderDetails");
                    console.log("senderDetails", senderDetails);
                    var senderDetails = atob(senderDetail);
                    $.ajax({
                        type : 'post',
                        url : '/bin/Donation',
                        //type : 'post',
                       // url : '/bin/donationConfirmation',
                        data : 'senderDetails=' + senderDetails,
                        success : function(successResponse) {
                            console.log("successResponse: ", successResponse);
                            if (successResponse == undefined || successResponse == "" || successResponse.length == 0) {
                                $('body').hideLoader();
                                popupParams.description = 'Something went Wrong. Refreshing Page';
                                warningBox(popupParams);
                                //location.reload();
                            } else {
                                //successResponse = JSON.parse(successResponse);
                                if (successResponse.status) {
                                    $('body').hideLoader();
                        			var ccAvenueData = successResponse.CCAvenueData;
                                    var $form = $(ccAvenueData.CCAvenueForm);
                                    $('body').append($form);
                                    $form.submit();
                                } else {
                                    $('body').hideLoader();
                                    popupParams.description = successResponse.message;
                    				warningBox(popupParams);
                                }

                            }
                        },
                        fail : function(failedResponse) {
                            $('body').hideLoader();
                            console.log("failedResponse: ", failedResponse);
                            popupParams.description = 'Donation failed. Please try again later.';
                            warningBox(popupParams);

                        },
                        error : function(errorResponse) {
                            $('body').hideLoader();
                            console.log("errorResponse: ", errorResponse);
                            popupParams.description = 'Donation interrupted. Please try again later.';
                            warningBox(popupParams);
                            //location.reload();
                        }
                    });
                } else {
                	console.log("not valid");
                }
            });
}

function giftHamperInputValidation() {
    var validationResult = true;
    var $giftHamperInputElems = $('form.gift-hamper-form').find('input.sub-form-mandatory');
    $giftHamperInputElems.each(function() {
        if ($(this).val() == "") {
            $(this).addClass('invalid-input');
            validationResult = false;
            invalidWarningMessage($(this));
        }
    });
    if ($giftHamperInputElems.hasClass('invalid-input')) {
        var $firstInvalidElem = $('form.gift-hamper-form').find('.invalid-input').first();
        ($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();
        validationResult = false;

    }
    return validationResult;
}

function extractGiftHamperDetails(formType) {
    var data = $("#" + formType);
    return {
        //"title" : $(data).find('#guestTitle option:selected').text() || "",
        "firstName" : $(data).find('input[name=guestFirstName]').val() || "",
        "lastName" : $(data).find('input[name=guestLastName]').val() || "",
        "email" : $(data).find('input[name=guestEmail]').val() || "",
        //"phoneNumber" : "9876543210",
        "city" : $(data).find('input[name=guestCity]').val() || "",
        "pincode" : $(data).find('input[name=guestPincode]').val() || "",
        //"state" : $(data).find('input[name=guestState]').val() || "",
        "country" : "India",
        "donation" : $(data).find('input[name=amountToDonate]').val() || "",
        "panCardNumber" : $(data).find('input[name=guestPanCard]').val() || "",
        "address1" : $(data).find('input[name=guestAddressLine1]').val() || "",
        "address2" : $(data).find('input[name=guestAddressLine2]').val() || "",
		"isGDPRCompliance" : $('[data-form-id="guest-GDPRCompliance"]').is(":checked") || false,
    }

}
var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
var getCookie = function(name) {
    // Split cookie string and get all individual name=value pairs in an array
    var cookieArr = document.cookie.split(";");
    
    // Loop through the array elements
    for(var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");
        
        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if(name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }
    
    // Return null if not found
    return null;
}
