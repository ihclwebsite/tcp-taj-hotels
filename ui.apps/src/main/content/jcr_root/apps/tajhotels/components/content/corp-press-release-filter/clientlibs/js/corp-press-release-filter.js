$(document).ready(function() {
    pressRoomFilter();
});

function pressRoomFilter() {
    $(document)
            .ready(
                    function() {

                        var options = [ {
                            selector : "Brand",
                            selectorValue : "Brand",
                            option : [ {
                                label : 'IHCL',
                                value : 'IHCL'
                            }, {
                                label : 'Taj',
                                value : 'Taj'
                            }, {
                                label : 'SeleQtions',
                                value : 'SeleQtions'
                            }, {
                                label : 'Vivanta',
                                value : 'Vivanta'
                            }, {
                                label : 'Ginger',
                                value : 'Ginger'
                            }, {
                                label : 'Expressions',
                                value : 'Expressions'
                            } ],
                            selectedOptionList : null,
                            onChange : function(list) {
                                // do
                            }
                        }, {
                            selector : "Theme",
                            selectorValue : "Theme",

                            option : [ {
                                label : 'Food & Drink',
                                value : 'Food & Drink'
                            }, {
                                label : 'New Openings',
                                value : 'New Openings'
                            }, {
                                label : 'Spa & Wellness',
                                value : 'Spa & Wellness'
                            }, {
                                label : 'Experiences',
                                value : 'Experiences'
                            }, {
                                label : 'Kids & Family',
                                value : 'Kids & Family'
                            }, {
                                label : 'Art Architecture & Design',
                                value : 'Art Architecture & Design'
                            }, {
                                label : 'Events',
                                value : 'Events'
                            }, {
                                label : 'History',
                                value : 'History'
                            }, {
                                label : 'Promotions',
                                value : 'Promotions'
                            }, {
                                label : 'Corporate News',
                                value : 'Corporate News'
                            }, {
                                label : 'CSR',
                                value : 'CSR'
                            }, {
                                label : 'Appointments and People',
                                value : 'Appointments and People'
                            }, {
                                label : 'Loyalty',
                                value : 'Loyalty'
                            }, {
                                label : 'New Hotels and Developments',
                                value : 'New Hotels and Developments'
                            }

                            ],
                            selectedOptionList : null,
                            onChange : function(list) {
                                // do
                            }
                        }, {
                            selector : "Year",
                            selectorValue : "Year",

                            option : [ {
                                label : '2019',
                                value : '2019'
                            }, {
                                label : '2018',
                                value : '2018'
                            } ],
                            selectedOptionList : null,
                            onChange : function(list) {
                                // do
                            }
                        } ];
                        var selectedData = {};
                        var searchValue;
                        var filterResultWrapper = $('.press-release-cards-container .pressCard-wrapper');
                        var filterOptionsWrap = $('.filter-wrap-catagory');
                        var filterOnSearchBar = $('.pressRoom-filter .search-filters-container');
                        var searchOnCards;
                        var filteredCards;

                        // storing initial CARDS WITH data
                        var allInitialCards = $('.press-release-cards-container .pressCard-wrapper').clone(true);

                        $('.press-release-cards-container').showMore();

                        function eventsOnFilter() {
                            $('.cm-search-with-filter .filter-landing-cont-image .only-icon').on('click', function() {
                                $('.events-filter-subsection').css('display', 'block');
                            });

                            $(
                                    '.press-room-filter-wrapper .filter-go-con ,.press-room-filter-wrapper .cm-press-room-filter')
                                    .on(
                                            'click',
                                            function() {
                                                globalMultipleFilterLocal(options);

                                                // filtered applied in mob
                                                if ((window.screen.width < 992)
                                                        && ($('.cm-multi-dd option:selected').length > 0)) {
                                                    $('.events-filter-subsection').css('display', 'none');
                                                    $('.filter-landing-cont-image')
                                                            .find('img')
                                                            .attr("src",
                                                                    "/content/dam/tajhotels/icons/style-icons/filter-applied-blue.svg");
                                                } else {
                                                    $('.filter-landing-cont-image')
                                                            .find('img')
                                                            .attr("src",
                                                                    "/content/dam/tajhotels/icons/style-icons/filter-icon-blue.svg");
                                                }

                                            })
                        }

                        function cmSearchOnPress() {
                            $(".pressRoom-filter .searchbar-input.hotel-search").on("keyup", function() {
                                var value = $(this).val().toLowerCase();
                                searchValue = value;
                                if (value.length === 0) {
                                    $('.clear-input-icon').removeClass('show-clear-input');
                                    cmnSearch();
                                } else {
                                    $('.clear-input-icon').addClass('show-clear-input');
                                }
                                if (value.length > 2) {
                                    cmnSearch();
                                }
                            });

                            // clear the input
                            $('.pressRoom-filter .clear-input-icon').on('click', function() {
                                $(this).prev('.hotel-search').val("");
                                $('.clear-input-icon').removeClass('show-clear-input');
                                searchValue = "";
                                cmnSearch();

                            })
                        }

                        $(".events-filter-back-arrow").click(function() {
                            $('.events-filter-subsection').css('display', 'none');
                        });

                        function cmnSearch() {
                            var searchSet = [ "name", "date", "Brand" ];

                            $('.press-releases-wrapper .jiva-spa-show-more').remove();
                            $('.press-release-cards-container .cm-cards-spacing').empty();

                            // reset filter selected value
                            $('.cm-multi-dd option:selected').each(function() {
                                $(this).prop('selected', false);
                            })
                            $('.cm-multi-dd').multiselect('refresh');

                            $('.filter-landing-cont-image').find('img').attr("src",
                                    "/content/dam/tajhotels/icons/style-icons/filter-icon-blue.svg");

                            allInitialCards.each(function() {
                                var data = $(this).data('key');
                                for (var i = 0; i < searchSet.length; i++) {
                                    if (data[searchSet[i]].toLowerCase().indexOf(searchValue.toLowerCase()) > -1) {
                                        $('.press-release-cards-container .cm-cards-spacing').append(
                                                $(this).removeAttr('style'));
                                        break;
                                    }
                                }
                            });
                            $('.press-release-cards-container').showMore();
                            filteredCards = $('.press-release-cards-container .pressCard-wrapper').clone(true);
                        }

                        function globalMultipleFilterLocal(options) {

                            if (searchValue) {

                                searchOnCards = filteredCards;
                            } else {
                                searchOnCards = allInitialCards;
                            }

                            $('.press-releases-wrapper .jiva-spa-show-more').remove();
                            $('.press-release-cards-container .cm-cards-spacing').empty();

                            var selectedData = {};

                            for (var i = 0; i < options.length; i++) {
                                if (options[i].selectedOptionList && options[i].selectedOptionList.length > 0)
                                    selectedData[options[i].selectorValue] = options[i].selectedOptionList;
                            }

                            searchOnCards
                                    .each(function() {
                                        if (Object.keys(selectedData).length == 0) {
                                            $('.press-release-cards-container .cm-cards-spacing').append(
                                                    $(this).removeAttr('style'));
                                        } else {
											var dataStr = $(this)[0].dataset.key.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
                                            var data = JSON.parse(dataStr);
                                            for ( var key in selectedData) {
                                                if (selectedData[key].indexOf(data[key]) == -1) {
                                                    break;
                                                } else {
                                                    var objLength = Object.keys(selectedData).length;
                                                    if (key == Object.keys(selectedData)[Object.keys(selectedData).length - 1]) {
                                                        $('.press-release-cards-container .cm-cards-spacing').append(
                                                                $(this).removeAttr('style'));
                                                    }
                                                }
                                            }
                                        }
                                    });
                            selectedData = {};
                            $('.press-release-cards-container').showMore();
                        }

                        function init() {
                            createFilterOptions(filterOptionsWrap, options);
                            eventsOnFilter();
                            cmSearchOnPress();
                            $('.press-release-cards-container-without-search').showMore();
                        }
                        init();
                        preSelectedFilters();
                        function preSelectedFilters() {
                            var brand = getQueryParameter('brand');
                            if (brand) {
                                switch (brand) {
                                    case "Taj":
                                        var newOptionLs = createOptionList(options, "Brand", "Taj");
                                        globalMultipleFilterLocal(newOptionLs);
                                        setTextValuesInFilterDisplay("Brand", "Taj");
                                        break;
                                    case "SeleQtions":
                                        var newOptionLs = createOptionList(options, "Brand", "SeleQtions");
                                        globalMultipleFilterLocal(newOptionLs);
                                        setTextValuesInFilterDisplay("Brand", "SeleQtions");
                                        break;
                                    case "Vivanta":
                                        var newOptionLs = createOptionList(options, "Brand", "Vivanta");
                                        globalMultipleFilterLocal(newOptionLs);
                                        setTextValuesInFilterDisplay("Brand", "Vivanta");
                                        break;
                                }
                            }
                        }

                        function createOptionList(preExistingOptions, selector, filter) {
                            var newOptions = JSON.parse(JSON.stringify(options));
                            var filterArray = [];
                            filterArray.push(filter);
                            // console.log("filterArray ",filterArray);
                            for (var i = 0; i < newOptions.length; i++) {
                                if (newOptions[i].selector === selector) {
                                    newOptions[i].selectedOptionList = filterArray;
                                }
                            }
                            // console.log("NewOption ", newOptions);
                            return newOptions;
                        }
                        function setTextValuesInFilterDisplay(selector, filter) {
                            $($($.find("#" + selector)).find('.multiselect-selected-text')).text(filter)
                            $($($.find("#" + selector)).find('.multiselect dropdown-toggle btn btn-link')).attr(
                                    'title', filter);
                            $($($("label:contains(" + filter + ")")).parent().parent()).addClass("active");
                        }
                    })
                    $('.dropdown-toggle').dropdown();
};