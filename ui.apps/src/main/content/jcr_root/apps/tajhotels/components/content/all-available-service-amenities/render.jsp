<%@page import="java.util.Enumeration"%>
<%@include file="/libs/granite/ui/global.jsp"%>
<%@page session="false" import="java.util.List"%>
<%@page session="false" import="java.util.ArrayList"%>
<%@page session="false" import="java.util.Arrays"%>
<%@page session="false" import="com.adobe.granite.ui.components.Config"%>
<%@page session="false" import="com.adobe.granite.ui.components.Field"%>
<%@page session="false" import="com.adobe.granite.ui.components.Value"%>
<%@page session="false" import="com.ihcl.core.models.serviceamenity.ServiceAmenity"%>
<%@page session="false" import="com.ihcl.core.services.serviceamenity.IAllAvailableServiceAmenitiesFetcher"%>
<%@page session="false" import="org.apache.sling.api.resource.ResourceResolver"%>


<input type="hidden" name='./serviceAmenities@Delete'/>

<%
    Config cfg = cmp.getConfig();

    String contentPath = request.getAttribute(Value.CONTENTPATH_ATTRIBUTE).toString();
    ResourceResolver resolver = slingRequest.getResourceResolver();
    Resource contentPathResource = resolver.getResource(contentPath + "/serviceAmenities");

    List<String> valueList = new ArrayList();

    if (contentPathResource != null) {
        ValueMap valueMap = contentPathResource.adaptTo(ValueMap.class);
        String key = "otherConveniences";
        if (valueMap != null && valueMap.containsKey(key)) {
            String[] values = valueMap.get(key, String[].class);
            if (values != null) {
                valueList.addAll(Arrays.asList(values));
            }
        }
        key = "wellnessAmenities";
        if (valueMap != null && valueMap.containsKey(key)) {
            String[] values = valueMap.get(key, String[].class);
            if (values != null) {
                valueList.addAll(Arrays.asList(values));
            }
        }

        key = "bedAndBathExperience";
        if (valueMap != null && valueMap.containsKey(key)) {
            String[] values = valueMap.get(key, String[].class);
            if (values != null) {
                valueList.addAll(Arrays.asList(values));
            }
        }

        key = "suiteFeatures";
        if (valueMap != null && valueMap.containsKey(key)) {
            String[] values = valueMap.get(key, String[].class);
            if (values != null) {
                valueList.addAll(Arrays.asList(values));
            }
        }

        key = "hotelFacilities";
        if (valueMap != null && valueMap.containsKey(key)) {
            String[] values = valueMap.get(key, String[].class);
            if (values != null) {
                valueList.addAll(Arrays.asList(values));
            }
        }

    }

    IAllAvailableServiceAmenitiesFetcher availableServiceAmenityFetcherService = sling
            .getService(IAllAvailableServiceAmenitiesFetcher.class);
    List<ServiceAmenity> services = availableServiceAmenityFetcherService.getServiceAmenities();
    for (int i = 0; i < services.size(); i++) {
        ServiceAmenity service = services.get(i);
        String serviceName = service.getName();
        String servicePath = service.getPath();
        String group = service.getGroup();
        if (group == null || group.isEmpty()) {
            group = "otherConveniences";
        }
        String checkedValue = "";
        if (valueList.contains(servicePath)) {
            checkedValue = "checked";
        }
%>
<input type="checkbox" name='./serviceAmenities/./<%=group%>' value=<%=servicePath%> <%=checkedValue%>><%=serviceName%>
<br />
<%
    }
%>
