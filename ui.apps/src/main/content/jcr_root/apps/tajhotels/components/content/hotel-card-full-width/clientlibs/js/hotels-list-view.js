$( document ).ready( function() {
    trimAmenityNames();
    initDescriptionShowMore();
    $( '.mr-showMore-text-hotelsListView' ).on( 'click', function() {
        $( '.list-view-wrapper:nth-child(n+6)' ).toggle();
        if ( $( this ).text() == 'SHOW MORE' ) {
            $( this ).text( 'SHOW LESS' );
            $( '.mr-showMore-hotels-arrow-img' ).css( {
                'transform': 'rotate(' + 180 + 'deg)'
            } );
        } else {
            $( this ).text( 'SHOW MORE' );
            $( '.mr-showMore-hotels-arrow-img' ).css( {
                'transform': 'rotate(' + 180 + 'deg)'
            } );
        }
    } )
//    replaceRoomsLinkInDest()
} );

function trimAmenityNames(){
	$.each($('.destination-hotel-amenity-description'), function(i, value) {
		$(value).cmTrimText({
			charLimit : 22,
		});
	});
}

function initDescriptionShowMore() {
    $.each($('.mr-list-hotel-about>p'), function(i, value) {
        $(value).cmToggleText({
            charLimit: 125,
            showVal: 'Show More',
            hideVal: 'Show Less',
        });
    })
};
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9ob3RlbHMtbGlzdC12aWV3L2hvdGVscy1saXN0LXZpZXcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJCggZG9jdW1lbnQgKS5yZWFkeSggZnVuY3Rpb24oKSB7XHJcbiAgICAkKCAnLm1yLXNob3dNb3JlLXRleHQtaG90ZWxzTGlzdFZpZXcnICkub24oICdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICQoICcubGlzdC12aWV3LXdyYXBwZXI6bnRoLWNoaWxkKG4rNiknICkudG9nZ2xlKCk7XHJcbiAgICAgICAgaWYgKCAkKCB0aGlzICkudGV4dCgpID09ICdTSE9XIE1PUkUnICkge1xyXG4gICAgICAgICAgICAkKCB0aGlzICkudGV4dCggJ1NIT1cgTEVTUycgKTtcclxuICAgICAgICAgICAgJCggJy5tci1zaG93TW9yZS1ob3RlbHMtYXJyb3ctaW1nJyApLmNzcygge1xyXG4gICAgICAgICAgICAgICAgJ3RyYW5zZm9ybSc6ICdyb3RhdGUoJyArIDE4MCArICdkZWcpJ1xyXG4gICAgICAgICAgICB9ICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJCggdGhpcyApLnRleHQoICdTSE9XIE1PUkUnICk7XHJcbiAgICAgICAgICAgICQoICcubXItc2hvd01vcmUtaG90ZWxzLWFycm93LWltZycgKS5jc3MoIHtcclxuICAgICAgICAgICAgICAgICd0cmFuc2Zvcm0nOiAncm90YXRlKCcgKyAxODAgKyAnZGVnKSdcclxuICAgICAgICAgICAgfSApO1xyXG4gICAgICAgIH1cclxuICAgIH0gKVxyXG59ICk7Il0sImZpbGUiOiJtYXJrdXAvY29tcG9uZW50cy9ob3RlbHMtbGlzdC12aWV3L2hvdGVscy1saXN0LXZpZXcuanMifQ==

function replaceRoomsLinkInDest(){
	var hotelsLinkArr=$(".mr-list-price-wrap .mr-list-hotel-price-tic");
	replaceRoomsLink(hotelsLinkArr)
}
