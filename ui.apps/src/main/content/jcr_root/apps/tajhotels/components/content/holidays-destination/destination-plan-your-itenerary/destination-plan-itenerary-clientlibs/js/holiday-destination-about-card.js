$('document').ready(function() {
    $('.mr-holidays-Destination-about-description-content').each(function(){
		$(this).cmToggleText({
            charLimit: 120
        })	
    });

    $( '.mr-list-switch, .map-local-back' ).on( 'click', function() {
         $('.mr-holiday-destination-about-cards-wrapper').toggleClass('cm-hide',false);
         $('.mr-holiday-aboutDest-map').toggleClass('cm-hide',true);
         $('.mr-view-toggler-style').removeClass('mr-view-toggler-style');
         $(this).addClass('mr-view-toggler-style');
	} );

	$( '.mr-map-switch' ).on( 'click', function() {
		$('.mr-holiday-destination-about-cards-wrapper').toggleClass('cm-hide',true);
	     $('.mr-holiday-aboutDest-map').toggleClass('cm-hide',false);
	     $('.mr-view-toggler-style').removeClass('mr-view-toggler-style');
	     $(this).addClass('mr-view-toggler-style');
	} );
    $('.mr-holiday-destination-about-cards-wrapper .iteniary-list-view').each(function(){
		$(this).showMore();
    });
});




