function openCity( evt, cityName ) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName( "corporate-gov-tabcontent" );
    for ( i = 0; i < tabcontent.length; i++ ) {
        tabcontent[ i ].style.display = "none";
    }
    tablinks = document.getElementsByClassName( "tablinks" );
    for ( i = 0; i < tablinks.length; i++ ) {
        tablinks[ i ].className = tablinks[ i ].className.replace( " active", "" );
    }
    document.getElementById( cityName ).style.display = "block";
    evt.currentTarget.className += " active";
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWNvbW1pdHRlZXMtb2YtdGhlLWJvYXJkL2NvcnAtY29tbWl0dGVlcy1vZi10aGUtYm9hcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gb3BlbkNpdHkoIGV2dCwgY2l0eU5hbWUgKSB7XG4gICAgdmFyIGksIHRhYmNvbnRlbnQsIHRhYmxpbmtzO1xuICAgIHRhYmNvbnRlbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCBcImNvcnBvcmF0ZS1nb3YtdGFiY29udGVudFwiICk7XG4gICAgZm9yICggaSA9IDA7IGkgPCB0YWJjb250ZW50Lmxlbmd0aDsgaSsrICkge1xuICAgICAgICB0YWJjb250ZW50WyBpIF0uc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgIH1cbiAgICB0YWJsaW5rcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoIFwidGFibGlua3NcIiApO1xuICAgIGZvciAoIGkgPSAwOyBpIDwgdGFibGlua3MubGVuZ3RoOyBpKysgKSB7XG4gICAgICAgIHRhYmxpbmtzWyBpIF0uY2xhc3NOYW1lID0gdGFibGlua3NbIGkgXS5jbGFzc05hbWUucmVwbGFjZSggXCIgYWN0aXZlXCIsIFwiXCIgKTtcbiAgICB9XG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoIGNpdHlOYW1lICkuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcbiAgICBldnQuY3VycmVudFRhcmdldC5jbGFzc05hbWUgKz0gXCIgYWN0aXZlXCI7XG59Il0sImZpbGUiOiJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWNvbW1pdHRlZXMtb2YtdGhlLWJvYXJkL2NvcnAtY29tbWl0dGVlcy1vZi10aGUtYm9hcmQuanMifQ==
