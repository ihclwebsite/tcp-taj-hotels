function registerSignup(componentId, redirectUrl, errorList, socialForm) {
    $(document).ready(function() {
            var popupParams = {};
            popupParams.title = "Failed";
    
            var component = $('[data-component-id="health_consent"]');
            var paymentConsentForm = $(component).find('.payment-navigation-confirm-form');
            paymentConsentForm.hide();
            var signUpform = $(component).find('.member-signup-form');
            var globalMembershipId = "";
            var epicureconfirmationUrl = $('.epicure-confirm-url').text();
            var campaignEpicure = "";
            var form = $(component).find('[data-form-id="member-signup-form"]');

    
            component.find('[data-form-id="signup-title"]').selectBoxIt();
            component.find('[data-form-id="signup-gender"]').selectBoxIt();
    
            var titleDropDown = component.find('[data-form-id="signup-title"]');

            titleDropDown.selectBoxIt().change(function() {
                var titleVal = $(this).val();
            });
            var countryDropDown = component.find('[data-form-id="health-country"]');
            var stateDropDown = component.find('[data-form-id="health-state"]');
            var cityDropDown = component.find('[data-form-id="health-city"]');
            var countryOptions = [];
            
            countryOptions = locations.map(function(location) {
                return location.countryName;
            });
    
            countryDropDown.selectBoxIt({
                populate : countryOptions
            }).change(function() {
                try {
                    var selectedCountry = $(this).val();
                    console.log(selectedCountry);
                    var arrIndiaCalendar = $("[data-form-id='health-arrivalIndia']");
                    if(selectedCountry != 'INDIA'){
                        if(!arrIndiaCalendar.hasClass('sub-form-mandatory')){
                            arrIndiaCalendar.addClass('sub-form-mandatory');
                        }
                    } else {
                        arrIndiaCalendar.removeClass('sub-form-mandatory');
                    }
                    var countryIndex = locations.findIndex(function(location) {
                        return location.countryName === selectedCountry;
                    });
    
                    var states = locations[countryIndex].states;
                    var statesList = states.map(function(location) {
                        return location.state;
                    });
                    var cityList = states[0].cities.map(function(location) {
                        return location.cityName;
                    });
    
                    var stateSelectBoxIt = stateDropDown.data("selectBox-selectBoxIt");
                    if (!stateSelectBoxIt) {
                        stateDropDown.selectBoxIt();
                        stateSelectBoxIt = stateDropDown.data("selectBox-selectBoxIt");
                    }
                    stateSelectBoxIt.remove();
                    stateSelectBoxIt.add(statesList);
                                    
                    var citySelectBoxIt = cityDropDown.data("selectBox-selectBoxIt");
                    if (!citySelectBoxIt) {
                        cityDropDown.selectBoxIt();
                        citySelectBoxIt = cityDropDown.data("selectBox-selectBoxIt");
                    }
                    citySelectBoxIt.remove();
                    citySelectBoxIt.add(cityList);
                } catch (err) {
                    console.log("failed to fetch country");
                }
            });
    
            var stateSelectBoxIt = stateDropDown.selectBoxIt();
            stateSelectBoxIt.change(function() {
                try {
                    var selectedCountry = countryDropDown.val();
                    var selectedState = $(this).val();
                    var countryIndex = locations.findIndex(function(location) {
                        return location.countryName === selectedCountry;
                    });
                    var states = locations[countryIndex].states;
                    var stateIndex = states.findIndex(function(location) {
                        return location.state === selectedState;
                    });
                    var cityList = states[stateIndex].cities.map(function(location) {
                        return location.cityName;
                    });
                    
                    var citySelectBoxIt = cityDropDown.data("selectBox-selectBoxIt");
                    if (!citySelectBoxIt) {
                        cityDropDown.selectBoxIt();
                        citySelectBoxIt = cityDropDown.data("selectBox-selectBoxIt");
                    }
                    citySelectBoxIt.remove();
                    citySelectBoxIt.add(cityList);
                } catch (err) {
                    console.log("failed to fetch states");
                }
            });
    
    
            // datepicker script
            var arrDataInIndiaTimestamp = 0;
            var arrDateInHotelTimestamp = 0;
            component.find('.arrivalIndia-datepicker-container').datepicker({
                startDate : '-1000y',
                endDate : '1y',
                autoclose : true,
            }).on('changeDate',function(element) {
                if ($(this).hasClass('visible')) {
                    arrDataInIndiaTimestamp = element.date.valueOf();
                    var minDate = (new Date(element.date.valueOf()));
                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                    var dateSection = $(this).closest(".jiva-spa-date-section");
                    dateSection.find('[data-form-id="health-arrivalIndia"]').val(selectedDate).removeClass(
                        'invalid-input');
                    dateSection.find('.jiva-spa-date-value').text(selectedDate);
                    dateSection.find('.jiva-spa-date').removeClass('visible');
                    dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
    
                    //validations
                    if(arrDateInHotelTimestamp) {
                        var diff = arrDataInIndiaTimestamp - arrDateInHotelTimestamp;
                        removeValidationErrorInDatePicker('hotel', diff, 'before');
                    }
                }
            });
    
            component.find('.arrivalHotel-datepicker-container').datepicker({
                startDate : '-1000y',
                endDate : '1y',
                autoclose : true,
            }).on('changeDate',function(element) {
                if ($(this).hasClass('visible')) {
                    arrDateInHotelTimestamp = element.date.valueOf();
                    var minDate = (new Date(element.date.valueOf()));
                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                    var dateSection = $(this).closest(".jiva-spa-date-section");
                    dateSection.find('[data-form-id="health-arrivalHotel"]').val(selectedDate).removeClass(
                        'invalid-input');
                    dateSection.find('.jiva-spa-date-value').text(selectedDate);
                    dateSection.find('.jiva-spa-date').removeClass('visible');
                    dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
    
                    //validations
                    if(arrDataInIndiaTimestamp) {
                        var diff = arrDataInIndiaTimestamp - arrDateInHotelTimestamp;
                        removeValidationErrorInDatePicker('India', diff, 'after');
                    }
                }
            });
    
    
            function removeValidationErrorInDatePicker(from, diff, aftbfr) {
                var hotelElem = $('[data-form-id="health-arrivalHotel"]');
                var indiaElem = $('[data-form-id="health-arrivalIndia"]');
                //console.log('diff', diff);
                var selectedElem = hotelElem;
                if(from=='hotel'){
                    selectedElem = indiaElem;
                }
                if(diff > 0 && !selectedElem.hasClass('invalid-input')){
                    selectedElem.addClass('invalid-input');
                    selectedElem.siblings('.sub-form-input-warning').text('Please select date ' + aftbfr+' arrival date in ' + from);
                } else {
                    hotelElem.removeClass('invalid-input');
                    hotelElem.siblings('.sub-form-input-warning').text('');
                    indiaElem.removeClass('invalid-input');
                    indiaElem.siblings('.sub-form-input-warning').text('');
                }
            }
    
    
            component.find('.depart-datepicker-container').datepicker({
                startDate : '1y',
                autoclose : true,
            }).on('changeDate',function(element) {
                if ($(this).hasClass('visible')) {
                    var minDate = (new Date(element.date.valueOf()));
                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                    var dateSection = $(this).closest(".jiva-spa-date-section");
                    dateSection.find('[data-form-id="health-depart"]').val(selectedDate).removeClass(
                        'invalid-input');
                    dateSection.find('.jiva-spa-date-value').text(selectedDate);
                    dateSection.find('.jiva-spa-date').removeClass('visible');
                    dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
                }
            });
            //datepicker
    
    
            $('.jiva-spa-date-con').click(function(e) {
                e.stopPropagation();
                $(this).siblings('.jiva-spa-date').addClass('visible');
            });
            
            component.find('[data-form-id="sign-up-btn"]').on('click',function(e) {
                var selected = [];
     
                //Reference the Table.
                var tblSymptoms = document.getElementById("tblSymptoms");
         
                //Reference all the CheckBoxes in Table.
                var chks = tblSymptoms.getElementsByTagName("INPUT");
    
                // Loop and push the checked CheckBox value in Array.
                for (var i = 0; i < chks.length; i++) {
                    if (chks[i].checked) {
                        selected.push(chks[i].value);
                    }
                }
    
                e.stopPropagation();
                //console.log('inside event called');
                if (validateSignupInputFields(selected)) {
                    $('body').showLoader();
                    var consentDetails = getInputData(selected.join(","));
                    handleHealthConsentFormSubmit(consentDetails);
                }
                e.preventDefault();
            });

        	closeDatePicker();

            function handleHealthConsentFormSubmit(consentDetails) {
                $.ajax({
                    type : 'post',
                    url : '/bin/health-consent',
                    data : 'consentData=' + JSON.stringify(consentDetails),
                    success : handleSuccess,
                    fail : handleFailure ,
                    error : handleError
                });
            }
    
            function handleSuccess(res){
                $('body').hideLoader();
                console.log(res);
                if(res.StatusCode == 200){
                    popupParams.title = "Success";
                    popupParams.description = 'Data has been submitted successfully';
                    resetHealthConsentForm();
                }
                else {
                    popupParams.description = 'Error in submitting data.';
                }
                warningBox(popupParams);
                $('.warning-box-close').on('click', function(){
                    location.reload(true);
                });
            }
            function handleFailure(res){
                $('body').hideLoader();
                console.error(res);
                popupParams.description = 'Please check connection and try again.';
                warningBox(popupParams);
                $('.warning-box-close').on('click', function(){
                    location.reload(true);
                });
            }
            function handleError(res){
                $('body').hideLoader();
                console.error(res);
                popupParams.description = 'Currently server is not running. Please try again';
                warningBox(popupParams);
                $('.warning-box-close').on('click', function(){
                    location.reload(true);
                });
            }
    
            function getInputData(selectedSymptoms) {            
                var healthDepart = form.find('[data-form-id="health-depart"]').val();
                var formattedHealthDepart = moment(healthDepart, "DD/MM/YYYY").format("YYYY-MM-DD");
                var finalHealthDepart = moment(formattedHealthDepart).format("Do MMMM, YYYY");
                var arrivalHotel = form.find('[data-form-id="health-arrivalHotel"]').val();
                var formattedArrivalHotel = moment(arrivalHotel, "DD/MM/YYYY").format("YYYY-MM-DD");
                var finalArrivalHotel = moment(formattedArrivalHotel).format("Do MMMM, YYYY");
                var arrivalIndia = form.find('[data-form-id="health-arrivalIndia"]').val();
                var formattedArrivalIndia = moment(arrivalIndia, "DD/MM/YYYY").format("YYYY-MM-DD");
                var finalArrivalIndia = moment(formattedArrivalIndia).format("Do MMMM, YYYY");
                if(finalArrivalIndia === "Invalid date"){
                    finalArrivalIndia = "";
                }
                var receiveEmail ="no";
                if(form.find('[data-form-id="emailCheck"]').is(":checked")){
                    receiveEmail = "yes";
                }
                else{
                    receiveEmail = "no";
                }
    
                var pagePath = location.pathname;
                var brandSite = "www.tajhotels.com";
                if(pagePath.includes('amastaysandtrails')){
                    brandSite = 'www.tajhotels.com';
                }else if(pagePath.includes('seleqtions')){
                    brandSite = 'www.seleqtionshotels.com';
                }else if(pagePath.includes('vivanta')){
                    brandSite = 'www.vivantahotels.com';
                }
    

    
                var consentData = {
                    title : form.find('select[data-form-id="health-title"]').val(),
                    firstName : form.find('[data-form-id="health-firstname"]').val(),
                    lastName : form.find('[data-form-id="health-lastname"]').val(),
                    email : form.find('[data-form-id="health-email"]').val(),
                    phoneNumber : form.find('[data-form-id="health-mobile"]').val(),
                    address : form.find('[data-form-id="health-address1"]').val(),
                    country : form.find('select[data-form-id="health-country"]').val(),
                    state : form.find('select[data-form-id="health-state"]').val(),
                    city : form.find('select[data-form-id="health-city"]').val(),
                    pincode : form.find('[data-form-id="health-pincode"]').val(),
                    passportNo : form.find('[data-form-id="health-passport"]').val(),
                    organization : form.find('[data-form-id="health-organization"]').val(),
                    relativesName : form.find('[data-form-id="health-relativeName"]').val(),
                    relativePhoneNumber : form.find('[data-form-id="health-relativeMobile"]').val(),
                    arrivingFrom : form.find('[data-form-id="health-arrivingFrom"]').val(),
                    departingTo : form.find('[data-form-id="health-departingTo"]').val(),
                    departureDate : finalHealthDepart,
                    arrivalDateInHotel : finalArrivalHotel,
                    arrivalDateInIndia : finalArrivalIndia,
                    listedSymptoms : selectedSymptoms,
                    contactWithPatient : form.find('select[data-form-id="health-q2"]').val(),
                    quarantined : form.find('select[data-form-id="health-q3"]').val(),
                    confirmedPatient : form.find('select[data-form-id="health-q4"]').val(),
                    beenToRedZone : form.find('select[data-form-id="health-q5"]').val(),                
                    hotelEmail : $('#hotel-email').text(),
                    hotelName : $('#hotel-name').text(),
                    hotelCity : $('#hotel-city').text(),
                    hotelArea : $('#hotel-area').text(),
                    hotelPin  : $('#hotel-pin').text(),
                    hotelStd : $('#hotel-std').text(),
                    hotelFax : $('#hotel-fax').text(),
                    hotelPhone : $('#hotel-phone').text(),
                    brandSite : brandSite,
                    signature : signaturePad.toDataURL('image/png') ? encodeURIComponent(signaturePad.toDataURL('image/png').split(',')[1]) : "",
                    mailToSelf : receiveEmail,   
                };
                for(let key in consentData){
                    if(key != 'signature'){
						consentData[key] = encodeURIComponent(consentData[key]);
                    }
                }
                return consentData
            }
    
            function validateSignupInputFields(selectedSymptoms) {
                var flag = true;
                form.find('input.sub-form-mandatory,select.sub-form-mandatory').each(function() {
                    if ($(this).val() == "" || $(this).hasClass('invalid-input')) {
                        $(this).addClass('invalid-input');
                        flag = false;
                        invalidWarningMessage($(this));
                    }
                });
                if(signaturePad.isEmpty()){
                    $('.sub-warning-signature').removeClass('d-none');
                    flag = false;
                } else {
                    $('.sub-warning-signature').addClass('d-none');
                }
                var selectedSymptomsErr = $('.selectedSymptoms');
                if(selectedSymptoms && selectedSymptoms.length > 0){
                    if(!selectedSymptomsErr.hasClass('d-none')){
                        selectedSymptomsErr.addClass('d-none');
                    }
                } else {
                    selectedSymptomsErr.removeClass('d-none')
                    flag = false;
                }
                return flag;
            }
    
            function validateSignupInputFieldsLoggedIn() {
                var flag = true;
                form.find('input.sub-form-mandatory:visible,select.sub-form-mandatory').each(function() {
                    if ($(this).val() == "" || $(this).hasClass('invalid-input')) {
                        $(this).addClass('invalid-input');
                        flag = false;
                        invalidWarningMessage($(this));
                    }
                });
                return flag;
            }

            function resetHealthConsentForm() {
                signaturePad.clear();
                form.find('[data-form-id^="health"]').each(function(){
                    $(this).val("");
                });
            }
            //canvas Signature and download
            var canvas = document.getElementById('signature-pad');
    
            function resizeCanvas() {
                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * (ratio);
                canvas.height = canvas.offsetHeight * (ratio);
                canvas.getContext("2d").scale(ratio, ratio);
            }
            
            //window.onresize = resizeCanvas;
            //resizeCanvas();
    
            var signaturePad = new SignaturePad(canvas, {
                backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
            });
            
            document.getElementById('clear').addEventListener('click', function (ev) {
                signaturePad.clear();
                ev.preventDefault();
            });

            function closeDatePicker() {
    			$("body:not(.depart-datepicker-container, .arrivalIndia-datepicker-container, .arrivalHotel-datepicker-container)")
                	.on('click', function(){
                        $('.depart-datepicker-container').removeClass('visible');
                        $('.arrivalIndia-datepicker-container').removeClass('visible');
                        $('.arrivalHotel-datepicker-container').removeClass('visible');
                });
            }

     });

}
