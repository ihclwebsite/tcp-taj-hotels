(function(document, $) {
	$(document).on('foundation-contentloaded', function(e) {
	});

    $(document).on('focusout',"foundation-autocomplete[name='./bannerImage'] input", function(e) {
        imageSelectionChanged(e);
    });

})(document,Granite.$);

function imageSelectionChanged(e) {
    var holder = $('.image-crop-selector');
    holder.attr('src', e.target.value);
    holder.find("img").each(function() {
        $(this).attr('src', e.target.value);
    });
}
