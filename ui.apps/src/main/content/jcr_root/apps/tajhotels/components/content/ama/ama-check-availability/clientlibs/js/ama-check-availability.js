$(document).ready(
        function() {
            try {

                if($("#isOnlyBungalow").text()){
					var bookingOptions = dataCache.session.getData("bookingOptions");
                    bookingOptions.isOnlyBungalowPage = true;
                    dataCache.session.setData("bookingOptions", bookingOptions);
                }
                amaBookingObject = getInitialBookAStaySessionObject();
                amaBookingObject.isAmaCheckAvailabilitySelected = false;
                amaBookingObject.roomType = "room";
                var $guestDropdownWrp = $('.guests-dropdown-wrap');
                autoPopulateBannerBookAStay();

                $guestDropdownWrp.on('click', '.roomHeading', function() {
                    $(this).parent().toggleClass('hideDiv');

                });
                $('.check-avblty-guests-input').click(function() {
                    showGuestSelectionDropdown();
                    $('.check-avblty-guests-input').toggleClass('eNone');
                });
                var isEndDateTriggered;
                $('#ama-cal-img-to').on('click', function() {
                    $('#input-box-to').focus();
                    $('#input-box-to').click();
                });
                $('#ama-cal-img-from').on('click', function() {
                    $('#input-box-from').focus();
                    $('#input-box-from').click();
                });
                $('#input-box-from').on('change', function(e) {
                    var $nextInput = $('.input-box-ama.date-explore').not($(this));
                    var currVal = $(this).val();
                    var nextVal = $nextInput.val();
                    amaBookingObject.fromDate = moment(new Date(currVal)).format('MMM D YY');
                    amaBookingObject.isAmaCheckAvailabilitySelected = true;

                    setTimeout(function() {
                        $(this).blur();
                        $('#input-box-to').focus();
                        $('#input-box-to').click();
                        $('.bas-left-date-wrap-ama').removeClass('active');
                        $nextInput.focus();
                        if ($('#input-box-from').datepicker('getDate') >= $('#input-box-to').datepicker('getDate')) {
                            var nextDate = moment(currVal).add(1, 'days').format("D MMM YYYY");
                            $nextInput.datepicker('setDate', new Date(nextDate));
                            isEndDateTriggered = true;
                            amaBookingObject.toDate = moment(new Date(nextDate)).format('MMM D YY');
                            amaBookingObject.isAmaCheckAvailabilitySelected = true;
                        }
                        CloseDatePickerIfRequired();
                    }, 100);
                });

                $('#input-box-to').on('change', function(e) {
                    setTimeout(function() {
                        if (isEndDateTriggered) {
                            isEndDateTriggered = false;
                        } else {
                            $(this).blur();
                            $('.check-avblty-input-wrap .input-daterange#ama-ca-datepicker input').each(function() {
                                $(this).blur();
                            });
                            $('.input-box-wrapper-ama').hide();
                            $(document).click();
                        }
                        amaBookingObject.toDate = moment(new Date($('#input-box-to').val())).format('MMM D YY');
                        amaBookingObject.isAmaCheckAvailabilitySelected = true;
                        CloseDatePickerIfRequired();
                    }, 100);
                });
                // function in book a stay js
                initializeDatepickerForBookAStay($('.check-avblty-input-wrap .input-daterange#ama-ca-datepicker'),
                        $('.input-box-wrapper-ama'));

                $('#input-box-from').on('click', function() {
                    showCalenderCheckAvailAma($(this), $('.bas-left-date-wrap-ama'));
                });

                $('#input-box-to').on('click', function() {
                    showCalenderCheckAvailAma($(this), $('.bas-right-date-wrap-ama'));
                });

                function showCalenderCheckAvailAma(_this, checkinoutCont) {
                    _this.focus();
                    checkinoutCont.addClass('active').siblings('.bas-single-wrap').removeClass('active');
                    $('.input-box-wrapper-ama').show();
                    $('.bas-calander-container-ama').css('display', 'flex');
                }

                $guestDropdownWrp.on('click', ' .adult-dec, .child-dec, .adult-inc, .child-inc',
                        function() {
                            var item = $(this);
                            var parentItem = item.parent().parent();
                            var count = item.siblings('.counter').text();
                            var isAdultWrp = parentItem.hasClass('adult-wrap');
                            if (item.attr('class').includes('inc')) {
                                if (isBungalowSelected()) {
                                    if ((isAdultWrp && (count > 0 && count < 15))
                                            || (!isAdultWrp && (count > -1 && count < 8))) {
                                        changeGuestCounter(item);
                                    }
                                } else {
                                    if ((isAdultWrp && (count > 0 && count < 7))
                                            || (!isAdultWrp && (count > -1 && count < 7))) {
                                        changeGuestCounter(item);
                                    }
                                }
                            } else if (item.attr('class').includes('dec')) {
                                if ((isAdultWrp && count > 1) || (!isAdultWrp && count > 0)) {
                                    changeGuestCounter(item);
                                }
                            }
                            updateIndividualRoomGuestCount($(this));
                            updateGuestPlaceholder();
                            amaBookingObject.isAmaCheckAvailabilitySelected = true;
                            amaBookingObject.roomOptions = getRoomOptionsSelectedAma();

                        });

                var $guestDropdwnAddBtn = $('.add-room-button');
                $guestDropdownWrp.on('click', '.close-current-room', function() {
                    var roomCounter = $('.guests-dropdown-wrap .guest-room-header').length;
                    var deletedRoom = $(this).closest(".guest-room-header");
                    var deletedRoomIndex = deletedRoom.index();
                    deleteRoomInCartAndUpdateSelectionData(deletedRoomIndex);
                    deletedRoom.nextAll('.guest-room-header').each(function() {
                        deletedRoomIndex++;
                        var _this = $(this);
                        _this.attr('id', 'roomGuestDetails' + deletedRoomIndex);
                        _this.attr('data-room-index', deletedRoomIndex);
                        _this.find('.guest-room-count').text(deletedRoomIndex);
                    });
                    deletedRoom.remove();
                    if (deletedRoomIndex < 5) {
                        $guestDropdwnAddBtn.removeClass('add-room-button-remove');
                    }
                    updateGuestPlaceholder();
                    amaBookingObject.isAmaCheckAvailabilitySelected = true;
                    amaBookingObject.roomOptions = getRoomOptionsSelectedAma();
                });

                $('#addButton').on('click', function() {
                    var roomCounter = $('.guests-dropdown-wrap .guest-room-header').length;
                    if (roomCounter < 5) {
                        roomCounter++;
                        var roomGuestDetails = $(this).prev();
                        var clonedRoomGuestDetails = roomGuestDetails.clone();
                        clonedRoomGuestDetails.find('.noOfPeople').text("(1 Guest)");
                        roomGuestDetails.after(clonedRoomGuestDetails);
                        var cloned = $(this).prev();
                        cloned.find('.guest-room-count').text(roomCounter);
                        cloned.find('.adult-wrap .counter').text(1);
                        cloned.find('.children-wrap .counter').text(0);
                        cloned.attr('data-room-index', roomCounter);
                        cloned.attr('id', 'roomGuestDetails' + roomCounter)
                        cloned.find('.close-current-room').removeClass('display-none');
                    }
                    if (roomCounter > 4) {
                        $guestDropdwnAddBtn.addClass('add-room-button-remove');
                    }
                    updateGuestPlaceholder();

                    amaBookingObject.isAmaCheckAvailabilitySelected = true;
                    amaBookingObject.roomOptions = getRoomOptionsSelectedAma();
                });

                $('#checkAvailability').click(function() {
                    var path = $(this).attr('hrefvalue');
                    isAmaCheckAvailability = true;
                    if (path) {
                        if (numberOfNightsSelectedCheck()) {
                            onClickOnCheckAvailabilty();
                        } else {
                            numberOfNightsExcessWarning(); // function in book a stay js
                        }
                    }

                });

                setTimeout(function() {
                    /* Dropdown Menu */
                    $('.ama-check-availability .dropdown').click(function() {
                        $(this).attr('tabindex', 1).focus();
                        $(this).toggleClass('active');
                        $(this).find('.dropdown-menu').slideToggle(300);
                    });
                    $('.ama-check-availability .dropdown').focusout(function() {
                        $(this).removeClass('active');
                        $(this).find('.dropdown-menu').slideUp(300);
                    });
                    /* End Dropdown Menu */
                }, 3000);

                $('.check-avblty-wrap').on('click', '.dest-item, .hotel-item', function() {
                    updateDestination($(this)); // function in searchBar js
                });

                // radio button click events
                $('.check-avblty-container .radio-container input[type=radio]').change(function() {
                    bungalowRadioSelector();
                    resetAdultChildCount('1', '0');
                });

                $('.book-stay-popup-radio-btn #onlyBungalowBtn').change(function() {
                    // function in book a stay js
                    removePopulatedRoomsBookAStay($(".bas-room-no"));
                    removePopulatedRoomsBookAStay($(".bas-room-details"));
                    $(".bas-room-no").click();
                    selectedRoomsCount = $('.fc-add-package-con').length;
                    if (selectedRoomsCount > 1) {
                        deleteSeletedRoomsInCartAma();
                    }
                });

                disableRoomsRadioBtnInBungalowPage();

            } catch (err) {
                console.error('caught exception in ama checkAvailability js', err);
            }

        });

function disableRoomsRadioBtnInBungalowPage() {
    var isOnlyBungalow = isOnlyBungalowAvailable();
    var currentURL = window.location.pathname;
    if (currentURL.includes('accommodations')) {
        if (isOnlyBungalow) {
            updateOnlyBungalowInSession(true);
            updateBungalowGuest();
        } else {
            updateOnlyBungalowInSession(false);
            updateGuests();
        }
    } else if ($('.cm-page-container').hasClass('home-page-layout')) {
        updateOnlyBungalowInSession(false);
        updateGuests();
    }
    updateRadioBtnStatus();
    updateGuestPlaceholder();
}

function updateBungalowGuest() {
    amaBookingObject.isAmaCheckAvailabilitySelected = false;
    var bookingOptions = dataCache.session.getData('bookingOptions');
    resetAdultChildCount(bookingOptions.roomOptions[0].adults, bookingOptions.roomOptions[0].children);
}
function updateRadioBtnStatus() {
    if (isOnlyBungalowPageInSession()) {
        $('#onlyRoom, #onlyRoomBtn').parent('.radio-container').addClass('disable-radiobtn');
        $('.check-avblty-container .radio-container #onlyBungalow, .book-stay-popup-radio-btn #onlyBungalowBtn')
                .click();
    } else {
        $('#onlyRoom, #onlyRoomBtn').parent('.radio-container').removeClass('disable-radiobtn');
    }
}

function updateOnlyBungalowInSession(isOnlyBungalow) {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    if (bookingOptions) {
        bookingOptions.isOnlyBungalowPage = isOnlyBungalow;
        if (isOnlyBungalow) {
            bookingOptions.BungalowType = "onlyBungalow";
            if (bookingOptions.previousDates) {
                bookingOptions.previousDates.BungalowType = "onlyBungalow";
            }
            bookingOptions.rooms = 1;
            var roomOptions = changeRoomGuestToBungalow(bookingOptions.roomOptions);
            bookingOptions.roomOptions = [ roomOptions ];
        }
        dataCache.session.setData("bookingOptions", bookingOptions);
    }
}

function isOnlyBungalowPageInSession() {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    if (bookingOptions && bookingOptions.isOnlyBungalowPage) {
        return true;
    }
    return false;
}

function autoPopulateBannerBookAStay() {
    updateDate();
    populateRadioButton();
    // updateGuests();
    // updateGuestPlaceholder();
}

function deleteSeletedRoomsInCartAma() {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    bookingOptions.selection = [];
    bookingOptions.rooms = 1;
    bookingOptions.roomOptions = getInitialRoomOption();
    dataCache.session.setData("bookingOptions", bookingOptions);
    $('.fc-add-package-con').each(function() {
        $(this).remove();
    });
    var floatingCartAma = $('.book-ind-container');
    floatingCartAma.find('.checkout-num').text('0');
    floatingCartAma.find('.cart-total-price').text('0');
    floatingCartAma.css('display', 'none');
    $('.cm-bas-con .cm-bas-content-con').css('bottom', '4%');
}

function getRoomOptionsSelectedAma() {
    var roomsSelector = $('.guests-dropdown-wrap .guest-room-header');
    var roomOptions = [];
    roomsSelector.each(function() {
        var $this = $(this);
        var index = parseInt($this.data('room-index')) - 1;
        roomOptions.push({
            "adults" : $this.find('.adult-wrap .counter').text(),
            "children" : $this.find('.children-wrap .counter').text(),
            "initialRoomIndex" : index
        });
    });
    return roomOptions;
}

function CloseDatePickerIfRequired() {
    if (!$('.input-box-wrapper-ama').is(':visible')) {
        $('.bas-calander-container-ama').css('display', 'none');
    }
}

function updateIndividualRoomGuestCount(_this) {
    var room = _this.closest('.guest-room-header');
    var count = parseInt(room.find('.adult-wrap .counter').text())
            + parseInt(room.find('.children-wrap .counter').text());
    room.find('.noOfPeople').text('(' + count + ' ' + createGuestWordAma(+count, 'Guest') + ')');
}

function changeGuestCounter(element) {
    var counter = element.siblings('.counter').text();
    if (element.attr('class').includes('inc')) {
        counter++;
    } else if (element.attr('class').includes('dec')) {
        counter--;
    }
    element.siblings('.counter').text(counter);
    var currentAdult = $('.adult-wrap .counter').text();
    var currentChild = $('.children-wrap .counter').text();
    var guestUpdate = element.parent().parent().parent().siblings(".roomHeading").children(".noOfPeople");
    var totalGuest = parseInt(currentAdult) + parseInt(currentChild);
}

function updateDate() {
    var bookedOptions = fetchBookAStayDataToPopulate();
    if (bookedOptions) {
        var bookedCheckInDate = moment(bookedOptions.fromDate, "MMM Do YY").format("DD MMM YYYY");
        var bookedCheckOutDate = moment(bookedOptions.toDate, "MMM Do YY").format("DD MMM YYYY");
        $('#input-box-from').val(bookedCheckInDate);
        $('#input-box-to').val(bookedCheckOutDate);
    }

}

function updateGuests() {
    var bookingOptions = fetchBookAStayDataToPopulate();
    var adults;
    var children;
    var rooms = bookingOptions.roomOptions.length;
    removePopulatedRoomsBookAStay($('.check-avblty-wrap .guest-room-header'));
    if (rooms > 1) {
        var index = 1;
        adults = bookingOptions.roomOptions[index - 1].adults;
        children = bookingOptions.roomOptions[index - 1].children;
        $('.guests-dropdown-wrap .adult-wrap .counter').text(adults);
        $('.guests-dropdown-wrap .children-wrap .counter').text(children);
        while (index < rooms) {
            var roomGuestDetails = $('#addButton').prev();
            roomGuestDetails.after(roomGuestDetails.clone());
            adults = bookingOptions.roomOptions[index].adults;
            children = bookingOptions.roomOptions[index].children;
            var cloned = $('#addButton').prev();
            index++;
            cloned.attr("id", "roomGuestDetails" + index);
            cloned.find('.guest-room-count').text(index);
            cloned.find('.adult-wrap .counter').text(adults);
            cloned.find('.children-wrap .counter').text(children);
            cloned.attr('data-room-index', index);
            cloned.find('.close-current-room').removeClass('display-none');
            var guestCountOfThisRoom = +adults + +children;
            cloned.find('.noOfPeople').text(
                    '(' + guestCountOfThisRoom + ' ' + createGuestWordAma(+guestCountOfThisRoom, 'Guest') + ')');

        }
        if (rooms == 5) {
            $('.check-avblty-wrap #addButton').addClass('add-room-button-remove');
        } else {
            $('.check-avblty-wrap #addButton').removeClass('add-room-button-remove');
        }
    } else {
        adults = bookingOptions.roomOptions[0].adults;
        children = bookingOptions.roomOptions[0].children;
        resetAdultChildCount(adults, children);
    }
}

function fetchBookAStayDataToPopulate() {
    return amaBookingObject.isAmaCheckAvailabilitySelected ? amaBookingObject : dataCache.session
            .getData('bookingOptions');
}

function showGuestSelectionDropdown() {
    $('.guests-dropdown-wrap').toggleClass('display-block');
    $('.check-avblty-guests-input .icon-drop-down-arrow').toggleClass('rotate-arrow');
}

function updateGuestPlaceholder() {
    var roomGuestDetails = $('.guests-dropdown-wrap .guest-room-header');
    var rooms = roomGuestDetails.length;
    var adults = 0;
    var children = 0;
    roomGuestDetails.each(function() {
        adults = adults + parseInt($(this).find('.adult-wrap .counter').text());
        children = children + parseInt($(this).find('.children-wrap .counter').text());
    })
    var guests = adults + children;
    var guestsCount = guests + ' ' + createGuestWordAma(guests, "Guest") + ' ' + rooms + ' '
            + createGuestWordAma(guests, "Room");
    $('.guest-title-wrap').text(guestsCount);
}

function createGuestWordAma(count, word) {
    if (count > 1) {
        return word + 's';
    }
    return word;
}

function parseDate(selectedDateValue) {
    return moment(selectedDateValue).format("MMM Do YY");
}

function bungalowRadioSelector() {
    var roomHeading = $('.check-avblty-wrap .roomHeading');
    var addRoom = $('.check-avblty-wrap #addButton');
    var roomElements = $('.guests-dropdown-wrap .guest-room-header');
    amaBookingObject.isAmaCheckAvailabilitySelected = true;
    if (isBungalowSelected()) {
        amaBookingObject.roomType = "onlyBungalow";
        roomHeading.hide();
        addRoom.addClass('add-room-button-remove');
        var selectedRoomsCount = $('.fc-add-package-con').length;
        if (selectedRoomsCount > 1) {
            deleteSeletedRoomsInCartAma()
        }
        if (roomElements) {
            removePopulatedRoomsBookAStay(roomElements); // function present in book a stay js
        }
    } else {
        amaBookingObject.roomType = "IndividualRoom";
        roomHeading.show();
        addRoom.removeClass('add-room-button-remove');
    }
    setTimeout(function() {
        updateGuestPlaceholder();
    }, 100);
}

function resetAdultChildCount(adults, children) {
    $('.adult-wrap .counter').text(adults);
    $('.children-wrap .counter').text(children);
}

function populateRadioButton() {
    var bookingOptions = fetchBookAStayDataToPopulate();
    var bungalow = $('.check-avblty-container .radio-container #onlyBungalow, .book-stay-popup-radio-btn #onlyBungalowBtn');
    var room = $('.check-avblty-container .radio-container #onlyRoom, .book-stay-popup-radio-btn #onlyRoomBtn');
    var addRoom = $('.check-avblty-wrap #addButton');
    if (bookingOptions && bookingOptions["BungalowType"] && bookingOptions["BungalowType"] == "onlyBungalow") {
        bungalow.click();
        addRoom.addClass('add-room-button-remove');
    } else {
        room.click();
        addRoom.removeClass('add-room-button-remove');
    }
}

function numberOfNightsSelectedCheck() {
    var currentDate = parseSelectedDate($("#input-box-from").datepicker("getDate"));
    var nextDate = parseSelectedDate($("#input-box-to").datepicker("getDate"));
    var numberOFNights = moment(nextDate, "MMM Do YY").diff(moment(currentDate, "MMM Do YY"), 'days');
    if (numberOFNights > 10 && $('#checkAvailability').hasClass('enabled'))
        return false;
    else
        return true;
}

function isOnlyBungalowAvailable() {
    var roomsList = $('.rate-cards-container .rate-card-wrap');
    var roomIterator = 0;
    var roomCount = roomsList.length;
    if (!roomCount) {
        return false;
    }
    for (roomIterator = 0; roomIterator < roomCount; roomIterator++) {
        if ($(roomsList[roomIterator]).attr('data-room-type') != "bungalow") {
            return false;
        }
    }
    return true;
}

$(document).mouseup(function(e) {
    var container = $(".guests-dropdown-wrap");
    var datepickerContainer = $(".input-box-wrapper-ama");

    if (datepickerContainer.is(":visible")) {
        if (!datepickerContainer.is(e.target) && datepickerContainer.has(e.target).length === 0) {
            $('.bas-calander-container-ama').css('display', 'none');
        }
    } else {
        $('.bas-calander-container-ama').css('display', 'none');
    }
    // if the target of the click isn't the container nor a descendant of the container
    if (container.is(":visible")) {
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.toggleClass('display-block');
            $('.check-avblty-guests-input').toggleClass('eNone');
            $('.check-avblty-guests-input .icon-drop-down-arrow').toggleClass('rotate-arrow');
        }
    }
});
