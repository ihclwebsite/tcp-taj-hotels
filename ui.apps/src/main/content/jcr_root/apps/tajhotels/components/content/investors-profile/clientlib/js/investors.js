$(document).ready(function() {
    hotel_carousel();
    popUpProfileIhcl();
});
function hotel_carousel() {
    $('.desc-sectionleader').each(function() {
        $(this).parallelShowMoreFn();
    });
}

function popUpProfileIhcl() {
    window.addEventListener('load', function() {
        var presentScroll;
        $('.view-button').click(function() {
            presentScroll = $(window).scrollTop();
            $('.profile-popup').show().addClass('active-popUp');
            $(".cm-page-container").addClass('prevent-page-scroll');
            $('.mr-profile-popup').show();
        });
        $(document).keydown(function(e) {
            if (($('.active-popUp').length) && (e.which === 27)) {
                $('.showMap-close').trigger("click");
            }
        });

        $('.showMap-close').click(function() {
            $('.profile-popup').hide().removeClass('active-popUp');
            $(".cm-page-container").removeClass('prevent-page-scroll');
            $('.mr-profile-popup').hide();
            $(window).scrollTop(presentScroll);
        })

    });
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWhvdGVscy1jYXJvdXNlbC1jYXJkL2NvcnAtaG90ZWxzLWNhcm91c2VsLWNhcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gaG90ZWxfY2Fyb3VzZWwoKSB7XHJcbiAgICAkKCBkb2N1bWVudCApLnJlYWR5KCBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIGRldmljZURldGVjdG9yLmNoZWNrRGV2aWNlKCkgPT0gXCJzbWFsbFwiICkge1xyXG4gICAgICAgICAgICAkKCAnLmFib3V0LWhvdGVscy1jb250YWluZXIgcCcgKS5lYWNoKCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoIHRoaXMgKS5jbVRvZ2dsZVRleHQoIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDIwMFxyXG4gICAgICAgICAgICAgICAgfSApXHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9ICk7XHJcbn0iXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NvcnAtaG90ZWxzLWNhcm91c2VsLWNhcmQvY29ycC1ob3RlbHMtY2Fyb3VzZWwtY2FyZC5qcyJ9

/*
 * function switchingPage() { var switching = document.getElementById( "mr-reports-wrapper-id" ); if ( switching != null ) {
 * var division = switching.getElementsByClassName( "rate-tab" ); for ( var i = 0; i < division.length; i++ ) {
 * division[ i ].addEventListener( "click", function() { var currentdiv = document.getElementsByClassName(
 * "tab-selected" ); currentdiv[ 0 ].className = currentdiv[ 0 ].className.replace( "tab-selected", "" ); this.className += "
 * tab-selected";
 *  } ); } } }
 * 
 * function report_content() { $( '.rate-tab' ).click( function() { var current = $( this ); var contentElement =
 * current.parent().children(), position = contentElement.index( current ); $( '.mr-investors-report-documents-wrapper'
 * ).removeClass( 'content-active' ).eq( position ) .addClass( 'content-active' ); } ); }
 */
// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWludmVzdG9ycy10aXRsZS1jYXJkL2NvcnAtaW52ZXN0b3JzLXRpdGxlLWNhcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gc3dpdGNoaW5nUGFnZSgpIHtcclxuICAgIHZhciBzd2l0Y2hpbmcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggXCJtci1yZXBvcnRzLXdyYXBwZXItaWRcIiApO1xyXG4gICAgaWYgKCBzd2l0Y2hpbmcgIT0gbnVsbCApIHtcclxuICAgICAgICB2YXIgZGl2aXNpb24gPSBzd2l0Y2hpbmcuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSggXCJyYXRlLXRhYlwiICk7XHJcbiAgICAgICAgZm9yICggdmFyIGkgPSAwOyBpIDwgZGl2aXNpb24ubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgICAgIGRpdmlzaW9uWyBpIF0uYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHZhciBjdXJyZW50ZGl2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSggXCJ0YWItc2VsZWN0ZWRcIiApO1xyXG4gICAgICAgICAgICAgICAgY3VycmVudGRpdlsgMCBdLmNsYXNzTmFtZSA9IGN1cnJlbnRkaXZbIDAgXS5jbGFzc05hbWUucmVwbGFjZSggXCJ0YWItc2VsZWN0ZWRcIiwgXCJcIiApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGFzc05hbWUgKz0gXCIgdGFiLXNlbGVjdGVkXCI7XHJcblxyXG4gICAgICAgICAgICB9ICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiByZXBvcnRfY29udGVudCgpIHtcclxuICAgICQoICcucmF0ZS10YWInICkuY2xpY2soIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBjdXJyZW50ID0gJCggdGhpcyApO1xyXG4gICAgICAgIHZhciBjb250ZW50RWxlbWVudCA9IGN1cnJlbnQucGFyZW50KCkuY2hpbGRyZW4oKSxcclxuICAgICAgICAgICAgcG9zaXRpb24gPSBjb250ZW50RWxlbWVudC5pbmRleCggY3VycmVudCApO1xyXG4gICAgICAgICQoICcubXItaW52ZXN0b3JzLXJlcG9ydC1kb2N1bWVudHMtd3JhcHBlcicgKS5yZW1vdmVDbGFzcyggJ2NvbnRlbnQtYWN0aXZlJyApLmVxKCBwb3NpdGlvbiApXHJcbiAgICAgICAgICAgIC5hZGRDbGFzcyggJ2NvbnRlbnQtYWN0aXZlJyApO1xyXG4gICAgfSApO1xyXG59Il0sImZpbGUiOiJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWludmVzdG9ycy10aXRsZS1jYXJkL2NvcnAtaW52ZXN0b3JzLXRpdGxlLWNhcmQuanMifQ==
