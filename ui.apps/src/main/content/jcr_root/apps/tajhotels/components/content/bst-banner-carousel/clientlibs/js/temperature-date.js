$(document).ready(function() {
    setDateInBanner();
    setTempInBanner();
});

function setDateInBanner() {
    try {
        var apiKey = $('.hotel-carousel-section').data('apikey');
        var hotellat = $('#banner-temperature').data('hotellat');
        var hotellon = $('#banner-temperature').data('hotellon');

        if (hotellat != undefined && hotellon != undefined) {
            var queryURL = "https://api.openweathermap.org/data/2.5/weather?lat=" + hotellat + "&lon=" + hotellon
                    + "&appid=" + apiKey + "&units=metric";

            $.getJSON(queryURL, function(data) {
                var temp = data.main.temp;
                var tempRound = parseFloat(temp).toFixed();
                if (tempRound != 'NaN') {
                    $('#bannerCarousel #temperature-update').append(tempRound);
                } else {
                    $('.hotel-banner-temperature').hide();
                }
            });
        }
    } catch (error) {
        console.log("Lat and long can't found.");
    }
}
function setTempInBanner() {
    try {
        var currentDate = moment(new Date()).format('Do MMM YYYY');
        $('#bannerCarousel').find('.banner-date').text(currentDate);
    } catch (error) {
        console.log("Setting Date in banner failed.");
    }
}
