$("document").ready(function() {
    var isErrorPg = dataCache.session.getData("confStatus");
    var _elErrorBoxWrap = $(".the-conf-page-inner");
    if(isErrorPg) {
        _elErrorBoxWrap.addClass("is-error-page");
    }
    setTimeout(function() {
        dataCache.session.removeData("confStatus");
    }, 10);
});