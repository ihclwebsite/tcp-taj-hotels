window.addEventListener('load', function() {

    $('.description-text-brand p').each(function() {
        $(this).cmToggleText({
            charLimit : 150,
        })
    });

});

function loadBrandPage(obj) {
    var parent = $(obj).data("url-id");
    window.location.href = parent;
}
