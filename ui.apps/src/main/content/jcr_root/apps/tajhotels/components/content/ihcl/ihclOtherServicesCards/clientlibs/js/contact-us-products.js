function popUpOtherCards(elem) {

    var thisLightBox = $(elem).closest('.concierge-local').next().css('display', 'block').addClass('active-popUp');
    $(document).keydown(function(e) {
        if (($('.active-popUp').length) && (e.which === 27)) {
            $('.showMap-close').trigger("click");
        }
    });
    $('.showMap-close').click(function() {

        thisLightBox.hide().removeClass('active-popUp');
    })

}

function stopAnchorProp(obj) {
    if (window.location.href.includes('en-in/taj-air')) {
        var title = obj.getAttribute('data-title');
        if (title == 'WALKTHROUGH') {
            prepareQuickQuoteJsonForClick("Walkthrough_More");
        } else if (title == 'LEISURE') {
            prepareQuickQuoteJsonForClick("Leisure_More");
        }
    }
    return true;
}

$(document).ready(function() {

    $('.desc-section-otherservices').each(function() {
        $(this).parallelShowMoreFn();
    });

});
