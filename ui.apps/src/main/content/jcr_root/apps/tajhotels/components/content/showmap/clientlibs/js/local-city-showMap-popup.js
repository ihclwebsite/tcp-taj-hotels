$(document).ready(function() {

	var jsonArray = $('#json').val();;
	var allMarkers = [];
	var detail = {};
	var markerSizeFlag = false;
	var hotelType;
	var counter = 0;
	var markerSizeFlag = false;
	var windowWidth = $(window).width();
	var data = "";
	if(jsonArray != undefined || jsonArray != null) {
	    var data = JSON.parse(jsonArray);
	}
	
	var labeldata = [];

	function hotelCardClicked(targetHotelCard) {
		hotelCardActivate(targetHotelCard.id);
		markerSizeChange(targetHotelCard.id);
	}

	function createCards() {

		if (data.data[0].localArea) {
			for (var i = 0; i < data.data[0].localArea.length; i++) {
				counter++;
				var temp = "";
				var wrapperCard = '<div tabindex = "0" id="' + data.data[0].localArea[i].id +
					'" class="loc-cards-wrapper ' + data.data[0].localArea[i].tagValue +
					'"></div>';
				var popupIdArray = (data.data[0].localArea[i].name).split(' ');
				var popupId = "";
				for (j = 0; j < popupIdArray.length; j++) {
					popupId = popupId + popupIdArray[j]
				};
				$('.loc-showMap-cards').append(wrapperCard);
				temp = '<div class="local-city-card-wrap clearfix">' +
					'<div class="local-city-image-cont"><img src="' +
					data.data[0].localArea[i].locationImage +
					'" alt="Local-Area-icon" /></div>' +
                        '<div class="local-city-details-cont" style="padding-left:10px">' +
					'<div class="loc-city-header">' +
					data.data[0].localArea[i].name +
					'</div>';
				
				if (data.data[0].localArea[i].address != "") {
				temp +=	'<div class="loc-city-location"  tabindex = "0" role = "button"><span class="icon-location ho-location-icon ho-cm-icon inline-block"></span>' +
					'<div>' +
					data.data[0].localArea[i].address +
					'</div>' +
					'</div>';
				}
				if (data.data[0].localArea[i].phoneNo != "") {
				
				temp +=	'<div class="loc-city-contact"><a class="ho-contact-number hide-in-lg inline-block" href="tel:' +
					data.data[0].localArea[i].phoneNo +
					'">' +
					'<span class="icon-phone-enabled ho-cm-icon ho-contact-icon inline-block"></span>' +
					'<div class="inline-block ho-mail-to-txt cm-view-link-txt ho-cm-link-txt">' +
					data.data[0].localArea[i].phoneNo +
					'</div></a>' +
					'<div class="ho-contact-number hide-in-sm hide-in-md inline-block disabled" tabindex = "0" role = "button"><span class="icon-phone-disabled ho-cm-icon ho-contact-icon inline-block"></span>' +
					'<div class="inline-block ho-mail-to-txt">' +
					data.data[0].localArea[i].phoneNo +
					'</div>' +
					'</div>' +
					'</div>';
				}
				
				temp +=	'<div class="loc-city-time-activity-cont">';
					
				if (data.data[0].localArea[i].openingTime == "" || data.data[0].localArea[i].closingTime == "") {	
				temp +=	'<div class="loc-city-time">Open : 9:00 AM - 6:00 PM</div>';
				} else {
				temp +=	'<div class="loc-city-time">Open :' +
					data.data[0].localArea[i].openingTime +
					'-' +
					data.data[0].localArea[i].closingTime +
					'</div>';
				}
				temp +=	'<div class="loc-city-activity clearfix">' +
					'<div class="mr-experinces-Button-wrap local-city-card"><button class="cm-btn-secondary experience-button-styles local-area-map-det-btn" data-con-name="' +
					popupId +
					'" onclick="myFunction(this)">VIEW ACTIVITY</button></div>' +
					'</div>' +
					'</div>' +

					'</div>' +

					'<div style="display: none;" class="cards">' +
					data.data[0].localArea[i].tagValue + '</div>' + '</div>'

				$("#" + data.data[0].localArea[i].id + ".loc-cards-wrapper").append(temp);
				createMarker(data.data[0].localArea[i]);

				$("#" + data.data[0].localArea[i].id + ".loc-cards-wrapper").click(function() {
					hotelCardClicked(this);
				})

			}
		} else {
			$('.specific-city-show-map').hide();
		}

		// on desktop first card will be activated

		createMarker(data.data[0]);
		allMarkers[allMarkers.length - 1].set('labelContent', counter + ' activities around the place');

		$('.loc-showMap-cards').children(":first").addClass("active");
		allMarkers[0].setIcon(allMarkers[0].iconFocused);
		allMarkers[0].set('labelClass', 'markerlabels active');
		allMarkers[0].markerSizeFlag = true;
		mapLocal.panTo(allMarkers[0].getPosition());

	}

	function hotelCardActivate(hotelId) {

		if (($("#" + hotelId + ".loc-cards-wrapper").hasClass("active") == false)) {
			if ($('.loc-cards-wrapper.active').length > 0)
				$('.loc-cards-wrapper.active').removeClass("active");
		}

		$("#" + hotelId + ".loc-cards-wrapper").addClass("active");

	}

	function hotelCardScroll(hotelId) {
		$('.loc-showMap-cards').scrollTop(0);
		var targetLocation = $("#" + hotelId + ".loc-cards-wrapper").offset();
		$('.loc-showMap-cards').animate({
			scrollTop: (targetLocation.top - $("#" + hotelId + ".loc-cards-wrapper").height())
		}, 'slow');
	}

	function hotelCardScrollMobile(hotelId) {
		$('.hotel-list .carousel-item').removeClass('active');
		$('#' + hotelId + '.hotel-wrapper').closest('.carousel-item').addClass('active');
	}

	function markerSizeChange(targetMarkerId) {
		for (var i = 0; i < allMarkers.length - 1; i++) {

			if (allMarkers[i].localId == targetMarkerId) {
				if (allMarkers[i].markerSizeFlag == false) {

					if (data.data[0].localArea[i].coordinates.lat) {
						allMarkers[i].set('labelClass', 'markerlabels active');
						allMarkers[i].setIcon(allMarkers[i].iconFocused);
						allMarkers[i].markerSizeFlag = true;
						allMarkers[allMarkers.length - 1].setIcon(allMarkers[i].iconNormal);
						allMarkers[allMarkers.length - 1].markerSizeFlag = false;
						allMarkers[allMarkers.length - 1].set('labelClass', 'markerlabels');
						mapLocal.panTo(allMarkers[i].getPosition());
						allMarkers[allMarkers.length - 1].set('labelContent', counter +
							' activities around the place');
					} else {
						allMarkers[allMarkers.length - 1].setIcon(allMarkers[i].iconFocused);
						allMarkers[allMarkers.length - 1].markerSizeFlag = true;
						allMarkers[allMarkers.length - 1].set('labelClass', 'markerlabels active');
						mapLocal.panTo(allMarkers[allMarkers.length - 1].getPosition());
						allMarkers[allMarkers.length - 1].set('labelContent',
							data.data[0].localArea[i].name);
					}
				}

			} else {
				allMarkers[i].setIcon(allMarkers[i].iconNormal);

				if (data.data[0].localArea[i].coordinates.lat) {
					allMarkers[i].set('labelClass', 'markerlabels');
					allMarkers[i].markerSizeFlag = false;
				} else {
					allMarkers[i].set('labelClass', 'markerLabelsNoData');
					// allMarkers[allMarkers.length-1].markerSizeFlag = false;

				}

			}
		}
	}

	function createMarker(elem) {

		if (elem.coordinates.lat && elem.coordinates.lat) {
			var localId = elem.id;
			detail.lat = parseFloat(elem.coordinates.lat);
			detail.lng = parseFloat(elem.coordinates.lng);
			var pinIcon = {
				url: "/content/dam/tajhotels/icons/style-icons/marker_local.svg",
				scaledSize: new google.maps.Size(46, 58),
			};

			var pinIconLarge = {
				url: "/content/dam/tajhotels/icons/style-icons/marker_localLarge.svg",
				scaledSize: new google.maps.Size(64, 84),
			}

			var classToBeAdded = "markerlabels";

			if (detail.lat) {
				
				classToBeAdded = "markerlabels";

			} else {

				classToBeAdded = "markerLabelsNoData";
			}

			marker = new MarkerWithLabel({
				position: detail,
				map: mapLocal,
				animation: google.maps.Animation.DROP,
				icon: pinIcon,
				labelContent: elem.name,
				labelAnchor: new google.maps.Point(50, 0),
				labelClass: classToBeAdded,
				labelInBackground: true,
				optimized: false
			});

			marker.localId = localId;
			marker.markerSizeFlag = markerSizeFlag;
			marker.iconNormal = pinIcon;
			marker.iconFocused = pinIconLarge;

			allMarkers.push(marker);

			google.maps.event.addListener(marker, 'click', function() {

				if ((window.screen.width) < 992) {
					if (this.markerSizeFlag == false) {
						cardsToDisplayMobile(this.localId);
						markerSizeChange(this.localId);
						hotelCardActivate(this.localId);
						hotelCardScrollMobile(this.localId);

					}

				} else {
					markerSizeChange(this.localId);
					hotelCardActivate(this.localId);
					cardsToDisplayMobile(this.localId);
					hotelCardScroll(this.localId);
				}

			});

		}
	}

	$('.cm-map-btn.loc-city').on('click', function() {
		if ((window.screen.width) < 992) {
			createCardsForMobileMapView();
			allMarkers[0].setIcon(allMarkers[0].iconFocused);
			allMarkers[0].set('labelClass', 'markerlabels active');

		}
	})

	function createCardsForMobileMapView() {
		if (data.data[0].localArea) {
			$('.carousel-inner.card-carousel.local-place').empty();
			for (var i = 0; i < data.data[0].localArea.length; i++) {

				var card = '<div class="carousel-item">' + '<div class="hotel-parent">' +
					'<div class="hotel-wrapper" id="' + data.data[0].localArea[i].id + '">' +
					'</div>' + '</div>' + '</div>';

				$('.carousel-inner.card-carousel').append(card);
				var popupIdArray = (data.data[0].localArea[i].name).split(' ');
				var popupId = "";
				for (j = 0; j < popupIdArray.length; j++) {
					popupId = popupId + popupIdArray[j]
				};

				var mobCard = '<div class="local-city-card-wrap clearfix">' +
					'<div class="local-city-image-cont"><img src="' +
					data.data[0].localArea[i].locationImage +
					'" alt="Local-Area-icon" /></div>' +
					'<div class="local-city-details-cont">' +
					'<div class="loc-city-header">' +
					data.data[0].localArea[i].name +
					'</div>' +
					'<div class="loc-city-location"><span class="icon-location ho-location-icon ho-cm-icon inline-block"></span>' +
					'<div>' +
					data.data[0].localArea[i].address +
					'</div>' +
					'</div>' +
					'<div class="loc-city-contact"><a class="ho-contact-number hide-in-lg inline-block" href="tel:' +
					data.data[0].localArea[i].phoneNo +
					'"><span class="icon-phone-enabled ho-cm-icon ho-contact-icon inline-block"></span><div class="inline-block ho-mail-to-txt cm-view-link-txt ho-cm-link-txt">' +
					data.data[0].localArea[i].phoneNo +
					'</div></a>' +
					'<div class="ho-contact-number hide-in-sm hide-in-md inline-block disabled"><span class="icon-phone-disabled ho-cm-icon ho-contact-icon inline-block"></span>' +
					'<div class="inline-block ho-mail-to-txt">' +
					data.data[0].localArea[i].phoneNo +
					'</div>' +
					'</div>' +
					'</div>' +
					'<div class="loc-city-time-activity-cont">' +
					'<div class="loc-city-time">Open :' +
					data.data[0].localArea[i].openingTime +
					'-' +
					data.data[0].localArea[i].closingTime +
					'</div>' +
					'<div class="loc-city-activity clearfix">' +
					'<div class="mr-experinces-Button-wrap local-city-card"><button class="cm-btn-secondary experience-button-styles" data-con-name="' +
					popupId +
					'" onclick="myFunction(this)">VIEW ACTIVITY</button></div>' +
					'</div>' +
					'</div>' +
					'</div>' +
					'<div style="display: none;" class="cards">' +
					data.data[0].localArea[i].tagValue + '</div>' + '</div>'

				$("#" + data.data[0].localArea[i].id + ".hotel-wrapper").append(mobCard);

			}
			$('.carousel-item:first-child').addClass('active');
		} else {
			$('.cm-btn-primary.cm-map-btn.loc-city').hide();
		}

	}

	function cardsToDisplayMobile(hotelId) {
		$(".carousel-inner.card-carousel.local-place").children(".carousel-item.active").removeClass(
			"active");
		$("#" + hotelId + ".hotel-wrapper").parent().parent().addClass("active");

	}

	// for mobile
	function activeElemnt() {
		var hotelid = $('.carousel-inner').find('.carousel-item.active').children().children().attr(
			'id');

		markerSizeChange(hotelid);
	}

	// for mobile
	$('.carousel').carousel({
		interval: false
	});

	// swap functionality
	$("#localCityCarousel").on("touchstart", function(event) {
		var xClick = event.originalEvent.touches[0].pageX;
		$(this).one("touchmove", function(event) {
			var xMove = event.originalEvent.touches[0].pageX;
			if (Math.floor(xClick - xMove) > 5) {
				$(this).carousel('next');

			} else if (Math.floor(xClick - xMove) < -5) {
				$(this).carousel('prev');

			}
			if ($('.carousel-inner.card-carousel.local-place').children().length > 1) {
				setTimeout(function() {
					activeElemnt();
				}, 1000);
			}
		});

		$("#localCityCarousel").on("touchend", function() {
			$(this).off("touchmove");

		});

	});

	function initMap() {
		console.log(data);
		var lat = data.data[0].coordinates.lat;
		var lng = data.data[0].coordinates.lng;
		if (lat && lng) {
			var latlng = new google.maps.LatLng(lat, lng);
			mapLocal = new google.maps.Map(document.getElementById('mapLocal'), {
				zoom: 13,
				center: latlng
			});
		}

	}

	$(window).resize(function() {
		if ($(window).width() != windowWidth) {
			var currentWidth = $(window).width();
			// only ipad potrait mode
			if (currentWidth == 768) {
				createCardsForMobileMapView();
			}
			windowWidth = currentWidth;
		}
	});

    if (document.getElementById('mapLocal')) {
		init();
	}
	
	function init() {
		initMap();
		createCards();
	}

});