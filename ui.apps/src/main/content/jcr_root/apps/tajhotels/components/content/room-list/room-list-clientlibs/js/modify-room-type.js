$(document).ready(function(){	
	bookedRoomType ={};
	bookedRoomType.value =false;	
	var modifyBookingState = dataCache.session.getData('modifyBookingState');
	if(modifyBookingState=='modifyRoomType'){
		var bookingOptionsCache = dataCache.session.getData('bookingOptions');
		delete bookingOptionsCache.roomOptions[0].userSelection;
		bookingOptionsCache.selection = [];
		dataCache.session.setData('bookingOptions',bookingOptionsCache);
		var bookedOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
		bookedRoomType.value = bookedOptions.roomTypeName;
		var $checkAvailabilityContainer = $('.check-availability-main-wrap');
		var $editBookingDetailsIcon = $('.btn-edit-booking-details');
		var $bookStayButton = $('.book-stay-btn');
		$checkAvailabilityContainer.css('pointer-events','none');
		$editBookingDetailsIcon.hide();
		$bookStayButton.hide();
	}else{
		console.log("Booking modification is not invoked")
	}	
});
function showBookedRoomType(){
	if(bookedRoomType.value){        
        var roomTypeName = bookedRoomType.value;
        roomTypeName = roomTypeName.replace('Edit','');
        roomTypeName = roomTypeName.replace('King Bed','');
        roomTypeName = roomTypeName.replace('Twin Bed','');
        roomTypeName = $.trim(roomTypeName);
		var $bookedRoom = $('.rate-card-wrap[data-room-type-name="'+roomTypeName+'"]');
        if($bookedRoom.length>0){
			$bookedRoom.addClass('booked-room-style');
            var parentOffset = $( '.cm-page-container' ).offset();
            var targetOffset = $bookedRoom.offset();
            $( 'html, body' ).animate( {
                scrollTop: ( parentOffset.top * -1 ) + targetOffset.top - 100
            }, 'slow' );
        }
	}	
}