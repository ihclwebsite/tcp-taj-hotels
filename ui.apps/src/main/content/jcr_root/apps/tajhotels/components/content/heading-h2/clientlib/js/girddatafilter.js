/*Component - Filter dropdowns*/

/*The four keys of "dropDowns" object has a key "selected"
All the selected values for the corresponding dropdown will be available in 
selected key of each dropdown object in "dropDowns".*/


window.addEventListener('load', function () { 
	var options = {};
	var $countryDropdown = $('#countryDropdown');
	var $cityDropdown = $('#cityDropdown');
	var $hotelDropdown = $('#hotelDropdown');
	var $suitsDropdown = $('#suitsDropdown');
	var $filterSectionButton = $('.filterSectionButton');
	var $filter_mobileView = $('.filter_mobileView');

	var dropDowns = {
		country: {
			options: ['America','India','Indonesia','Japan'],
			elem: $countryDropdown,
			default: 'Country',
			selected: null,
			dependent: {
				elem: $cityDropdown
			}
		},
		city: {
			options: ['Bangalore','Pune','Delhi'],
			elem: $cityDropdown,
			default: 'City',
			selected: null,
			dependent: {
				elem: $hotelDropdown
			}
		},
		hotel: {
			options: ['Name1','Name2','Name3','Name4'],
			elem: $hotelDropdown,
			default: 'Hotel',
			selected: null,
			dependent: {
				elem: $suitsDropdown
			}
		},
		suits: {
			options: ['Name1','Name2','Name3','Name4'],
			elem: $suitsDropdown,
			default: 'Rooms & Suits',
			selected: null,
			dependent: {
				elem: null
			}
		}
	}

	initDropdown.prototype.initialize = function() {
    var itrLength = this.options.length;
    for ( i = 0; i < itrLength; i++ ) {
        this.targetElem.append( '<option>' + this.options[ i ] + '</option>' )
    };

    //Initializes selectboxit on the rendered dropdown
    this.targetElem.selectBoxIt();

    // Add listeners to each selectbox change
    this.listenDDChange();
};

initDropdown.prototype.listenDDChange = function() {
    var dTarget = this.targetBlock;
    dTarget.elem.change( function() {
        var selectedOption = ( dTarget.elem ).find( "option:selected" );
        dTarget.selected = selectedOption.text();
        if ( dTarget.dependent.elem ) {
            dTarget.dependent.elem.selectBoxIt( 'selectOption', 0 );
            if ( dTarget.selected != dTarget.default ) {
                dTarget.dependent.elem.prop( "disabled", false );
            } else {
                dTarget.dependent.elem.prop( "disabled", true );
            }
        } else {
            if ( dTarget.selected != dTarget.default ) {
                // "dropDowns" Gives the selected values for each dropdown options
            }
        }
    } );
};	
	
	function init() {

		var countryDropdown = new initDropdown($countryDropdown, dropDowns.country);
		var cityDropdown = new initDropdown($cityDropdown, dropDowns.city);
		var hotelDropdown = new initDropdown($hotelDropdown, dropDowns.hotel);
		var suitsDropdown = new initDropdown($suitsDropdown, dropDowns.suits);

		countryDropdown.initialize();
		cityDropdown.initialize();
		hotelDropdown.initialize();
		suitsDropdown.initialize();	

		$cityDropdown.prop("disabled", true);
		$hotelDropdown.prop("disabled", true);
		$suitsDropdown.prop("disabled", true);

	};

	init();

	$('.filterSectionButton').click(function() {
		$filterSectionButton.css('display','none');
		$filter_mobileView.css('display','block');
	});

	$('.filterBackArrow').add('.filter-button').click(function() {
		$filterSectionButton.css('display','block');
		$filter_mobileView.css('display','none');
		if ($countryDropdown.find("option:selected").text() != 'Country') {
			if ($('.filterSectionButton').html()=='<img src="../../../assets/images/filter-icon.svg" alt  = "filter-icon">') {
				$('.filterSectionButton').html('<img src="../../../assets/images/filter-applied.svg" alt = "filter-applied-icon">')
			}
		}
	});


});

function initDropdown(element, target) {
	this.targetBlock = target;
	this.options = target.options;
	this.targetElem = element;
};

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLWNvbXBvbmVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKkNvbXBvbmVudCAtIEZpbHRlciBkcm9wZG93bnMqL1xuXG4vKlRoZSBmb3VyIGtleXMgb2YgXCJkcm9wRG93bnNcIiBvYmplY3QgaGFzIGEga2V5IFwic2VsZWN0ZWRcIlxuQWxsIHRoZSBzZWxlY3RlZCB2YWx1ZXMgZm9yIHRoZSBjb3JyZXNwb25kaW5nIGRyb3Bkb3duIHdpbGwgYmUgYXZhaWxhYmxlIGluIFxuc2VsZWN0ZWQga2V5IG9mIGVhY2ggZHJvcGRvd24gb2JqZWN0IGluIFwiZHJvcERvd25zXCIuKi9cblxuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24gKCkgeyBcblx0dmFyIG9wdGlvbnMgPSB7fTtcblx0dmFyICRjb3VudHJ5RHJvcGRvd24gPSAkKCcjY291bnRyeURyb3Bkb3duJyk7XG5cdHZhciAkY2l0eURyb3Bkb3duID0gJCgnI2NpdHlEcm9wZG93bicpO1xuXHR2YXIgJGhvdGVsRHJvcGRvd24gPSAkKCcjaG90ZWxEcm9wZG93bicpO1xuXHR2YXIgJHN1aXRzRHJvcGRvd24gPSAkKCcjc3VpdHNEcm9wZG93bicpO1xuXHR2YXIgJGZpbHRlclNlY3Rpb25CdXR0b24gPSAkKCcuZmlsdGVyU2VjdGlvbkJ1dHRvbicpO1xuXHR2YXIgJGZpbHRlcl9tb2JpbGVWaWV3ID0gJCgnLmZpbHRlcl9tb2JpbGVWaWV3Jyk7XG5cblx0dmFyIGRyb3BEb3ducyA9IHtcblx0XHRjb3VudHJ5OiB7XG5cdFx0XHRvcHRpb25zOiBbJ0FtZXJpY2EnLCdJbmRpYScsJ0luZG9uZXNpYScsJ0phcGFuJ10sXG5cdFx0XHRlbGVtOiAkY291bnRyeURyb3Bkb3duLFxuXHRcdFx0ZGVmYXVsdDogJ0NvdW50cnknLFxuXHRcdFx0c2VsZWN0ZWQ6IG51bGwsXG5cdFx0XHRkZXBlbmRlbnQ6IHtcblx0XHRcdFx0ZWxlbTogJGNpdHlEcm9wZG93blxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0Y2l0eToge1xuXHRcdFx0b3B0aW9uczogWydCYW5nYWxvcmUnLCdQdW5lJywnRGVsaGknXSxcblx0XHRcdGVsZW06ICRjaXR5RHJvcGRvd24sXG5cdFx0XHRkZWZhdWx0OiAnQ2l0eScsXG5cdFx0XHRzZWxlY3RlZDogbnVsbCxcblx0XHRcdGRlcGVuZGVudDoge1xuXHRcdFx0XHRlbGVtOiAkaG90ZWxEcm9wZG93blxuXHRcdFx0fVxuXHRcdH0sXG5cdFx0aG90ZWw6IHtcblx0XHRcdG9wdGlvbnM6IFsnTmFtZTEnLCdOYW1lMicsJ05hbWUzJywnTmFtZTQnXSxcblx0XHRcdGVsZW06ICRob3RlbERyb3Bkb3duLFxuXHRcdFx0ZGVmYXVsdDogJ0hvdGVsJyxcblx0XHRcdHNlbGVjdGVkOiBudWxsLFxuXHRcdFx0ZGVwZW5kZW50OiB7XG5cdFx0XHRcdGVsZW06ICRzdWl0c0Ryb3Bkb3duXG5cdFx0XHR9XG5cdFx0fSxcblx0XHRzdWl0czoge1xuXHRcdFx0b3B0aW9uczogWydOYW1lMScsJ05hbWUyJywnTmFtZTMnLCdOYW1lNCddLFxuXHRcdFx0ZWxlbTogJHN1aXRzRHJvcGRvd24sXG5cdFx0XHRkZWZhdWx0OiAnUm9vbXMgJiBTdWl0cycsXG5cdFx0XHRzZWxlY3RlZDogbnVsbCxcblx0XHRcdGRlcGVuZGVudDoge1xuXHRcdFx0XHRlbGVtOiBudWxsXG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0aW5pdERyb3Bkb3duLnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGl0ckxlbmd0aCA9IHRoaXMub3B0aW9ucy5sZW5ndGg7XG4gICAgZm9yICggaSA9IDA7IGkgPCBpdHJMZW5ndGg7IGkrKyApIHtcbiAgICAgICAgdGhpcy50YXJnZXRFbGVtLmFwcGVuZCggJzxvcHRpb24+JyArIHRoaXMub3B0aW9uc1sgaSBdICsgJzwvb3B0aW9uPicgKVxuICAgIH07XG5cbiAgICAvL0luaXRpYWxpemVzIHNlbGVjdGJveGl0IG9uIHRoZSByZW5kZXJlZCBkcm9wZG93blxuICAgIHRoaXMudGFyZ2V0RWxlbS5zZWxlY3RCb3hJdCgpO1xuXG4gICAgLy8gQWRkIGxpc3RlbmVycyB0byBlYWNoIHNlbGVjdGJveCBjaGFuZ2VcbiAgICB0aGlzLmxpc3RlbkREQ2hhbmdlKCk7XG59O1xuXG5pbml0RHJvcGRvd24ucHJvdG90eXBlLmxpc3RlbkREQ2hhbmdlID0gZnVuY3Rpb24oKSB7XG4gICAgdmFyIGRUYXJnZXQgPSB0aGlzLnRhcmdldEJsb2NrO1xuICAgIGRUYXJnZXQuZWxlbS5jaGFuZ2UoIGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgc2VsZWN0ZWRPcHRpb24gPSAoIGRUYXJnZXQuZWxlbSApLmZpbmQoIFwib3B0aW9uOnNlbGVjdGVkXCIgKTtcbiAgICAgICAgZFRhcmdldC5zZWxlY3RlZCA9IHNlbGVjdGVkT3B0aW9uLnRleHQoKTtcbiAgICAgICAgaWYgKCBkVGFyZ2V0LmRlcGVuZGVudC5lbGVtICkge1xuICAgICAgICAgICAgZFRhcmdldC5kZXBlbmRlbnQuZWxlbS5zZWxlY3RCb3hJdCggJ3NlbGVjdE9wdGlvbicsIDAgKTtcbiAgICAgICAgICAgIGlmICggZFRhcmdldC5zZWxlY3RlZCAhPSBkVGFyZ2V0LmRlZmF1bHQgKSB7XG4gICAgICAgICAgICAgICAgZFRhcmdldC5kZXBlbmRlbnQuZWxlbS5wcm9wKCBcImRpc2FibGVkXCIsIGZhbHNlICk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRUYXJnZXQuZGVwZW5kZW50LmVsZW0ucHJvcCggXCJkaXNhYmxlZFwiLCB0cnVlICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoIGRUYXJnZXQuc2VsZWN0ZWQgIT0gZFRhcmdldC5kZWZhdWx0ICkge1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGRyb3BEb3ducyk7IC8vIFwiZHJvcERvd25zXCIgR2l2ZXMgdGhlIHNlbGVjdGVkIHZhbHVlcyBmb3IgZWFjaCBkcm9wZG93biBvcHRpb25zXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9ICk7XG59O1x0XG5cdFxuXHRmdW5jdGlvbiBpbml0KCkge1xuXG5cdFx0dmFyIGNvdW50cnlEcm9wZG93biA9IG5ldyBpbml0RHJvcGRvd24oJGNvdW50cnlEcm9wZG93biwgZHJvcERvd25zLmNvdW50cnkpO1xuXHRcdHZhciBjaXR5RHJvcGRvd24gPSBuZXcgaW5pdERyb3Bkb3duKCRjaXR5RHJvcGRvd24sIGRyb3BEb3ducy5jaXR5KTtcblx0XHR2YXIgaG90ZWxEcm9wZG93biA9IG5ldyBpbml0RHJvcGRvd24oJGhvdGVsRHJvcGRvd24sIGRyb3BEb3ducy5ob3RlbCk7XG5cdFx0dmFyIHN1aXRzRHJvcGRvd24gPSBuZXcgaW5pdERyb3Bkb3duKCRzdWl0c0Ryb3Bkb3duLCBkcm9wRG93bnMuc3VpdHMpO1xuXG5cdFx0Y291bnRyeURyb3Bkb3duLmluaXRpYWxpemUoKTtcblx0XHRjaXR5RHJvcGRvd24uaW5pdGlhbGl6ZSgpO1xuXHRcdGhvdGVsRHJvcGRvd24uaW5pdGlhbGl6ZSgpO1xuXHRcdHN1aXRzRHJvcGRvd24uaW5pdGlhbGl6ZSgpO1x0XG5cblx0XHQkY2l0eURyb3Bkb3duLnByb3AoXCJkaXNhYmxlZFwiLCB0cnVlKTtcblx0XHQkaG90ZWxEcm9wZG93bi5wcm9wKFwiZGlzYWJsZWRcIiwgdHJ1ZSk7XG5cdFx0JHN1aXRzRHJvcGRvd24ucHJvcChcImRpc2FibGVkXCIsIHRydWUpO1xuXG5cdH07XG5cblx0aW5pdCgpO1xuXG5cdCQoJy5maWx0ZXJTZWN0aW9uQnV0dG9uJykuY2xpY2soZnVuY3Rpb24oKSB7XG5cdFx0JGZpbHRlclNlY3Rpb25CdXR0b24uY3NzKCdkaXNwbGF5Jywnbm9uZScpO1xuXHRcdCRmaWx0ZXJfbW9iaWxlVmlldy5jc3MoJ2Rpc3BsYXknLCdibG9jaycpO1xuXHR9KTtcblxuXHQkKCcuZmlsdGVyQmFja0Fycm93JykuYWRkKCcuZmlsdGVyLWJ1dHRvbicpLmNsaWNrKGZ1bmN0aW9uKCkge1xuXHRcdCRmaWx0ZXJTZWN0aW9uQnV0dG9uLmNzcygnZGlzcGxheScsJ2Jsb2NrJyk7XG5cdFx0JGZpbHRlcl9tb2JpbGVWaWV3LmNzcygnZGlzcGxheScsJ25vbmUnKTtcblx0XHRpZiAoJGNvdW50cnlEcm9wZG93bi5maW5kKFwib3B0aW9uOnNlbGVjdGVkXCIpLnRleHQoKSAhPSAnQ291bnRyeScpIHtcblx0XHRcdGlmICgkKCcuZmlsdGVyU2VjdGlvbkJ1dHRvbicpLmh0bWwoKT09JzxpbWcgc3JjPVwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9maWx0ZXItaWNvbi5zdmdcIj4nKSB7XG5cdFx0XHRcdCQoJy5maWx0ZXJTZWN0aW9uQnV0dG9uJykuaHRtbCgnPGltZyBzcmM9XCIuLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2ZpbHRlci1hcHBsaWVkLnN2Z1wiPicpXG5cdFx0XHR9XG5cdFx0fVxuXHR9KTtcblxuXG59KTtcblxuZnVuY3Rpb24gaW5pdERyb3Bkb3duKGVsZW1lbnQsIHRhcmdldCkge1xuXHR0aGlzLnRhcmdldEJsb2NrID0gdGFyZ2V0O1xuXHR0aGlzLm9wdGlvbnMgPSB0YXJnZXQub3B0aW9ucztcblx0dGhpcy50YXJnZXRFbGVtID0gZWxlbWVudDtcbn07XG4iXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2ZpbHRlci9maWx0ZXItY29tcG9uZW50LmpzIn0=
