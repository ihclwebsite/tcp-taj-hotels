$('document').ready(function() {
    var flag = localStorage.getItem("websiteCookieStickyNoteFlag");

    if (flag == "true") {
        $('.cookie-sticky-details-wrap').hide();
    }

    $('.sticky-details-close').on('click', function() {
        setFlagForCookieSticky($(this));
    });
});

function setFlagForCookieSticky(clickEvent) {
    websiteCookieStickyNoteFlag = "true";
    $('.cookie-sticky-details-wrap').hide();
    localStorage.setItem('websiteCookieStickyNoteFlag', websiteCookieStickyNoteFlag);
}
