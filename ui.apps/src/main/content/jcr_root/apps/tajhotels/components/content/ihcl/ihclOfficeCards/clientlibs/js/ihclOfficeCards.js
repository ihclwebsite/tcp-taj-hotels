$(document).ready(function() {
	var showMoreCurrentValue = $('.cont-us-details-container').attr("data-showMoreOption");
	if(showMoreCurrentValue === "false" || showMoreCurrentValue === undefined) {
		$('.ichlOfficeCardInnerWrap').each(function() {
		    $(this).showMore();
		});
	}
	
	if(showMoreCurrentValue === "true") {
		var element = $('.divWithoutShowMore');
		$(element.children().children()).css('display','block');
	}
});