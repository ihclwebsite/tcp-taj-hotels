$(document).ready(
        function() {
            // [IHCLCB Start]
            // send email to the user if Send notification to this ID checkbox is selected
            if (isIHCLCBSite()) {
                var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
                if (ihclCbBookingObject && ihclCbBookingObject.emailToOthersOnly) {
                    sendEmailToThisId(ihclCbBookingObject.sendNotification, true, "yes");
                }
                else if(ihclCbBookingObject) {
					sendEmailToThisId(ihclCbBookingObject.sendNotification, true, "no");
                }
            }
            // [IHCLCB end]

            $('.email-share-popup').hide();
            $('.pay-now-spinner').hide();
            var presentScroll;
            $('.download-options .download-options-email').click(function() {
                presentScroll = $(window).scrollTop();
                $('.email-share-popup').show().addClass('active-popUp');
                $('.email-share-popup .cm-local-details-container').show().addClass('active-popUp');
                $(".cm-page-container").addClass('prevent-page-scroll');
                $('.mr-profile-popup').show();
                $('#btn-email-share').show();
            });
            $(document).keydown(function(e) {
                if (($('.active-popUp').length) && (e.which === 27)) {
                    $('.showMap-close').trigger("click");
                }
            });

            $('.showMap-close').click(function() {
                $('.profile-popup').hide().removeClass('active-popUp');
                $(".cm-page-container").removeClass('prevent-page-scroll');
                $('.specific-city-header-wrapper').removeClass("d-none");
                $('.share-email-fail').addClass("d-none");
                $('.share-email-success').addClass("d-none");
                $('#btn-email-share').show();
                $('.mr-profile-popup').hide();
                $('[name="userEmailForShare"]').val("");
                $(window).scrollTop(presentScroll);
            });
            $("#btn-email-share").click(
                    function() {
                        if ($('[name="userEmailForShare"]').val() == ""
                                || $('[name="userEmailForShare"]').hasClass("invalid-input")) {
                            $('[name="userEmailForShare"]').focus();
                        } else {
                            sendEmailToThisId($('[name="userEmailForShare"]').val());
                        }
                    });

            function sendEmailToThisId(email, isEmailSendingAutomatically, mailToOthersOnly) {

                var bookingdata = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
                var hotelAddress = encodeURIComponent(dataCache.session.getData('hotelAddress'));
                var chainCode = $('#hotelChainCode').val() || "21305";
                var hotelSubstring = $('#hotelSubstring').val() || "5";
                var contentRootPath = $('#contentRootPath').val() || "/content/tajhotels";
                var currency = dataCache.session.getData('currencyCache');
                currency = (currency) ? currency.currencyId : "INR";
                if (bookingdata && bookingdata.itineraryNumber && bookingdata.guest && bookingdata.guest.email) {
                    $.ajax({
                        type : 'post',
                        url : '/bin/shareOnEmail',
                        data : "confirmationNo=" + bookingdata.itineraryNumber + "&emailAddress="
                                + bookingdata.guest.email + '&hotelAddress=' + hotelAddress + '&recipientEmail='
                                + email + "&chainCode=" + chainCode + "&hotelSubstring=" + hotelSubstring
                                + "&contentRootPath=" + contentRootPath + "&currency=" + currency + "&mailToOthers=" + mailToOthersOnly,
                        beforeSend : function() {
                            $('#btn-email-share').hide();
                            $('.pay-now-spinner').show();
                        },
                        success : function(returnmessage) {
                            $('.pay-now-spinner').hide();
                            if ($(".cm-page-container").hasClass("ama-theme")) {
                                $('#recipient-email-id').text("Reservation details sent successfully to" + email);
                            } else {
                                $('#recipient-email-id').text(email);
                            }
                            if (!isEmailSendingAutomatically) {
                                $('.specific-city-header-wrapper').addClass("d-none");
                                $('.share-email-success').removeClass("d-none");
                            }
                        },
                        error : function(errormessage) {
                            $('.pay-now-spinner').hide();
                            if (!isEmailSendingAutomatically) {
                                $('.specific-city-header-wrapper').addClass("d-none");
                                $('.share-email-fail').removeClass("d-none");
                            }
                        },
                    });
                }
            }
        })
