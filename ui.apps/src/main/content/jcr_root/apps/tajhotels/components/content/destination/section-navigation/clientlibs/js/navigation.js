$(document).ready(function() {
	var url = window.location.href;  
	var pageName='';
	
		if(url.indexOf('dining')!= -1){
		 	pageName='Dining';
		}else if(url.indexOf('about')!= -1){
			 pageName='About';
		}else if(url.indexOf('experiences')!= -1){ 
		 	pageName='Experiences';
		}else{
			pageName='Hotels';
		}
	
	$('nav .tab-child-container').each(function(i) {
		$(this).removeClass("selected");
	});
	
	$('nav .tab-child-container').each(function(i) {
		if($(this).html() === pageName){
			$(this).addClass("selected");
		}
	});
});