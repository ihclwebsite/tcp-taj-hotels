document
        .addEventListener(
                'DOMContentLoaded',
                function() {
                    $('.footer-destination-expand-button').click(function() {
                        if ($(this).text() == '+') {
                            $('.footer-destination-list').slideDown(100);
                            $(this).text('-');
                        } else {
                            $(this).text('+');
                            $('.footer-destination-list').slideUp(100);
                        }
                    })

                    function isEmail(email) {
                        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        return regex.test(email);
                    }

                    var email;
                    var flag;

                    $('footer .email-input').blur(function() {
                        email = $(this).val();
                        if (!isEmail(email)) {
                            flag = false;

                            $(".result-message-for-newsletter").text("Invalid Email-ID");
                        } else {
                            flag = true;
                            $(this).css('border', 'none');

                        }
                    });

                    $('.subscribe-btn')
                            .click(
                                    function() {
                                        if (flag == true) {
                                            var popupParams = {
                                                title : 'We will use your personal data only in-line with the following principles: 1. Lawfulness, fairness and transparency, 2. Purpose limitation, 3. Data minimisation, 4. Accuracy, 5. Storage limitation, 6. Integrity and confidentiality. This is the only principle that deals explicitly with security. The GDPR states that personal data must be “processed in a manner that ensures appropriate security of the personal data, including protection against unauthorised or unlawful processing and against accidental loss, destruction or damage, using appropriate technical or organisational measures”.',
                                                callBack : requestNewsletterSubscribe.bind(),
                                                needsCta : true
                                            }
                                            warningBox(popupParams);
                                        }
                                    });

                    function requestNewsletterSubscribe() {
                        var sourceBrand = "ihcl";
                        $('.waiting-loader-blue').show();
                        $('.subscribe-btn').attr("disabled", true);
                        $('.result-message-for-newsletter').text("");
                        $('.result-message-for-newsletter').removeClass("subscription-success");
                        $('.result-message-for-newsletter').removeClass("subscription-failure");
                        email = $('footer .email-input').val();

                        return $.ajax({
                            method : "GET",
                            url : "/bin/newsletterSubscribe",
                            data : {
                                emailId : email,
                                sourceBrand : sourceBrand
                            }
                        }).done(function(res) {

                            $('.waiting-loader-blue').hide();
                            $('.subscribe-btn').attr("disabled", false);
                            $('.result-message-for-newsletter').addClass("subscription-success");
                            $(".result-message-for-newsletter").text("Subscription Successful");
                        }).fail(function() {

                            $('.waiting-loader-blue').hide();
                            $('.subscribe-btn').attr("disabled", false);
                            $('.result-message-for-newsletter').addClass("subscription-failure");
                            $(".result-message-for-newsletter").text("Subscription Failed");
                        });
                    }
                });
