$(document).ready(function() {
    aboutUsContentToggle();
    aboutUsCorporateTab();
    aboutUsListToggle();
});

function aboutUsCorporateTab() {
    $(document).ready(function() {
        $('.associates-wrapper, .joint-venture-wrapper').hide();
        $('.corporate-tabs .rate-tab').click(function() {
            $(this).siblings().removeClass('tab-selected');
            $(this).addClass('tab-selected');
            if ($(this).index() == 0) {
                $('.associates-wrapper, .joint-venture-wrapper').hide();
                $('.subsidiaries-container').show();
            } else if ($(this).index() == 1) {
                $('.associates-wrapper').show();
                $('.subsidiaries-container, .joint-venture-wrapper').hide();
            } else if ($(this).index() == 2) {
                $('.joint-venture-wrapper').show();
                $('.subsidiaries-container, .associates-wrapper').hide();
            }
        })
    })
}

function aboutUsListToggle() {
    $(document).ready(function() {
        $('.show-less-button,  .show-more-button,  .arrow-style').hide();

        if (deviceDetector.checkDevice() == "small") {
            $(' .show-more-button, .arrow-style').show();

            $('.domestic-subsidiaries ul').each(function() {
                $(this).each(function() {
                    $(this).children('li:gt(3)').hide();
                })

            });
            $('.domestic-subsidiaries .show-more-button').click(function() {

                var $target = $(this).parent().siblings('.hotel-overview-wrapper');
                $target.find('ul li:gt(3)').show()
                $(this).siblings('.show-less-button').show();
                $(this).hide();
                $(" .arrow-style").css({
                    'transform' : 'rotate(-180deg)'
                });
            })

            $('.domestic-subsidiaries .show-less-button').click(function() {

                var $target = $(this).parent().siblings('.hotel-overview-wrapper');
                $target.find('ul li:gt(3)').hide()
                $(this).siblings(' .show-more-button').show();
                $(this).hide();
                $(" .arrow-style").css({
                    'transform' : 'rotate(360deg)'
                });
            })

        }

    });
}

function aboutUsContentToggle() {
    $(document).ready(function() {
        if (deviceDetector.checkDevice() == "small") {
            $('.holding-content p').each(function() {
                $(this).cmToggleText({
                    charLimit : 200,
                })
            });
        }
    });
}
