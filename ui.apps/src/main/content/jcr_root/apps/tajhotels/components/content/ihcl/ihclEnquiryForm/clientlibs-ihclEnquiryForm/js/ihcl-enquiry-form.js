    $( document ).ready( function() {
        
       $('#personalInfoTitle').selectBoxIt();
       $('#professionalInfoCompanyType').selectBoxIt();
       $('#professionalInfoRegion').selectBoxIt();
       $('#professionalInfoProjectType').selectBoxIt();
       $('#professionalInfoOwnershipType').selectBoxIt();

    $('.submit-button').on("click", function() {
        if(enquiryFormInputValidation()) {
            $('.spinner-con').show();
            initiateServletCall();
        }else{
            scrollToElement( $('.invalid-input').first());            
        }
    });

    $( '.enquiry-form-confirmation-overlay, .enquiry-form-confirmation-close-icon, .enquiry-form-confirmation-close-btn' )
        .click( function() {
            $( '.enquiry-form-confirmation-popup' ).hide();
        });
    $( '.enquiry-form-confirmation-inner-wrp' ).click( function( e ) {
        e.stopPropagation();
    });
	
	$( '.enquiry-form-error-overlay, .enquiry-form-error-close-icon, .enquiry-form-error-close-btn' )
        .click( function() {
            $( '.enquiry-form-error-popup' ).hide();
        });
    $( '.enquiry-form-error-inner-wrp' ).click( function( e ) {
        e.stopPropagation();
    });
    });

function enquiryFormInputValidation(){  
    var enquiryFormValid = true;
    $( '.personal-info-wrp, .professional-info-wrp' ).find( 'input.sub-form-mandatory , select.sub-form-mandatory' ).each( function() {
        if ( $( this ).val() == "" ) {           
            $( this ).addClass( 'invalid-input' );
            invalidWarningMessage( $( this ) );
            enquiryFormValid = false;                      
        }
    });
    return enquiryFormValid;
}

function scrollToElement(elementFocus) {
    var elementFocusWrapper = $(elementFocus).closest('.sub-form-input-wrp');    
    $([document.documentElement, document.body]).animate({
        scrollTop: $(elementFocusWrapper).offset().top
    }, 1000);
    $(elementFocus).focus();
}

function initiateServletCall() {
	var requestData = {
        			firstName :$('#personalInfoTitleSelectBoxItText').text(),
					lastName:$('#form-name').val(),
					emailId:$('#form-email').val(),
					phoneNumber:$('#form-phone').val(),
					companyName:$('#form-company').val(),
					companyType:$('#professionalInfoCompanyTypeSelectBoxItText').text(),
					region:$('#professionalInfoRegion :selected ').text(),
					projectType:$('#professionalInfoProjectType :selected ').text(),
					ownerShipType:$('#professionalInfoOwnershipType :selected ').text(),
					description:$('#form-description').val()
    			};

           $.ajax({
               type: "GET",
               url: '/bin/aboutusenquiry',
               data: requestData,
               dataType: "json",
               success: function(data) {

                   $('.spinner-con').hide();
                   $( '.enquiry-form-confirmation-popup' ).show();                   
               },
               error: function(textStatus, errorThrown) {
                   $('.spinner-con').hide();
                   $( '.enquiry-form-error-popup' ).show();        
               }
           });
}
