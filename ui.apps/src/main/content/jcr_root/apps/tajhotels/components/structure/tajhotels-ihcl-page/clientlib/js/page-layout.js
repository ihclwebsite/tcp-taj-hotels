window.addEventListener('load', function() {

    $(".retail-services-carousel-wrp").customSlide(4);
    $('.executive-leader-profile-card-carousel-wrp').each(function() {
        $(this).showMore();
    });
    $('.mr-initiative-card-container').showMore();
    $('.mr-tajness-card-container').customSlide(3);
    $('.mr-employee-container').customSlide(3);
    $('.ihcl-image-carousel-wrp').customSlide(6);
    $('.ihcl-image-carousel-wrp-legacy').customSlide(5);

    $('.mr-pdf-container').each(function() {
        $(this).showMore();
    });
    $('.desc-section-imagedesc').each(function() {
		
		$(this).parallelShowMoreFn();
	});
    $('.desc-section-corporatelist').each(function() {

		$(this).parallelShowMoreFn();
	});
  /*  $('.ichlOfficeCardInnerWrap').each(function() {
        $(this).showMore(11);
    });*/
    
     $('.desc-individual-details').each(function() {

        $(this).parallelShowMoreFn();
    });
    
});
