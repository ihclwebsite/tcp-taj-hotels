var ROOM_OCCUPANCY_RESPONSE;
var multiOfferCodes;
var adjacentHotelCount = 0;
var rateTabCode = "";

$(document).ready(function(){
	// saving voucherRedemption Data in session object
    dataCache.session.setData('voucherRedemption', getQueryParameter('voucherRedemption'));
});

window.addEventListener('load', function () {
    var locator = location.href;
    var selectedTab = locator.split("#").pop();

    roomHotelId = $($.find('[data-hotel-id]')[0]).data('hotel-id');
    hotelName = $($.find('[data-hotel-name]')[0]).data('hotel-name');

    roomDetailsShowHide();
    onlyBungalowsShowHide();

    checkInCheckOutTime();
    // following function call makes analytics data ready
    // for each room clicked
    $('.more-rates-button').click(function () {
        prepareRoomsJsonForClick($(this).closest('.rate-card-wrap'));
    });

});

/*
 * This function fetches checkInDate,checkOutDate,roomOccupancy from QueryParameters, if the parameters are missing
 * session storage values are picked up Sample Query Paramter that needs to passed are as follows
 * checkInDate=10/05/2019&checkOutDate=13/05/2019 Both checkInDate & checkOutDate needs to be passed , validation is
 * done on that nights ie the difference between checkOutDate & checkInDate is calculated if its greater than zero,
 * subsequently values are picked up for the AJAX call
 */
function fetchRoomsQueryParameters() {
    var bookingOptions = getBookingOptionsSessionData();
    var checkInDate = getQueryParameter('from');
    var checkOutDate = getQueryParameter('to');
    var rooms = getQueryParameter('rooms');
    var adults = getQueryParameter('adults');
    var children = getQueryParameter('children');
    var nights = getQueryParameter('nights');
    var promoCode = getQueryParameter('promoCode');
    if (checkInDate && nights && !checkOutDate) {
        if (nights > 0) {
            var sessionCheckInDate = moment(bookingOptions.fromDate, "MMM Do YY");
            var queryParamCheckInDate = moment(checkInDate, 'DD/MM/YYYY');
            if (queryParamCheckInDate >= sessionCheckInDate) {
                checkInDate = moment(checkInDate, 'DD/MM/YYYY').format('MMM Do YY');
                var toDate = moment(checkInDate, 'MMM Do YY').add(parseInt(nights), 'days').format("MMM Do YY");
                bookingOptions.fromDate = checkInDate;
                bookingOptions.toDate = toDate;
                bookingOptions.nights = parseInt(nights);
            } else {
                var fromDate = moment(bookingOptions.fromDate, 'MMM Do YY').format('MMM Do YY');
                var toDate = moment(fromDate, 'MMM Do YY').add(parseInt(nights), 'days').format('MMM Do YY');
                bookingOptions.fromDate = fromDate;
                bookingOptions.toDate = toDate;
                bookingOptions.nights = parseInt(nights);
            }
        }
    } else if (checkInDate && checkOutDate && !nights) {
        if (verifyFromAndToDate(checkInDate, checkOutDate)) {
            var nights = moment(checkOutDate, "DD.MM.YYYY").diff(moment(checkInDate, "DD.MM.YYYY"), 'days');
            checkInDate = moment(checkInDate, 'DD/MM/YYYY').format('MMM Do YY');
            checkOutDate = moment(checkOutDate, 'DD/MM/YYYY').format('MMM Do YY');
            bookingOptions.fromDate = checkInDate;
            bookingOptions.toDate = checkOutDate;
            bookingOptions.nights = parseInt(nights)
        }
    } else if(!checkInDate && !checkOutDate && nights){
        var fromDate = moment(bookingOptions.fromDate, 'MMM Do YY').format('MMM Do YY');
        var toDate = moment(fromDate, 'MMM Do YY').add(parseInt(nights), 'days').format('MMM Do YY');
        bookingOptions.fromDate = fromDate;
        bookingOptions.toDate = toDate;
        bookingOptions.nights = parseInt(nights);
    }
    if (rooms && adults && children) {
        if (validateRoomsQueryParams(rooms, adults, children)) {
            var roomOptions = [];
            var adultsArr = adults.split(",");
            var childArr = children.split(",");
            for (var index = 0; index < rooms; index++) {
                roomOptions.push({
                    "adults": adultsArr[index],
                    "children": childArr[index],
                    "initialRoomIndex": index
                });
            }
            bookingOptions.roomOptions = roomOptions;
            bookingOptions.rooms=rooms;
        }
    }
    var redirectFromAmp = getQueryParameter('redirectFromAmp');
    if(redirectFromAmp) {
        var promoCode =getQueryParameter('promoCode');
        var hotelId =getQueryParameter('hotelId');
        var targetEntity =getQueryParameter('targetEntity');
        var isAvailabilityChecked =getQueryParameter('isAvailabilityChecked');
        var onlyBungalows =getQueryParameter('onlyBungalows');
        if(!promoCode){
            promoCode = "";
        }
        bookingOptions.promoCode = promoCode;
        if(!hotelId){
            hotelId = null;
        }
        bookingOptions.hotelId = hotelId;
        if(!targetEntity){
            targetEntity = null;
        }
        bookingOptions.targetEntity = targetEntity;
        if(!isAvailabilityChecked){
            isAvailabilityChecked = false;
        }
        bookingOptions.isAvailabilityChecked = isAvailabilityChecked;
        if(!onlyBungalows){
            bookingOptions.BungalowType = "IndividualRoom";
        }else {
            bookingOptions.BungalowType = "onlyBungalow";
        }     
    }
    dataCache.session.setData('bookingOptions', bookingOptions);
    removeDateOccupancyParamFromUrl();
}

/*
 * This function validates the rooms,adults,children String passed in the query parameters, Validation is done for
 * number of rooms ie min value is 5 , no of adults for each room is limited to 7 and min value 1 , no of children for
 * each room is limited to 7 and min value is 0 & also validation is done on adults,children,rooms mapping if the
 * validation fails defaults session storage values are picked up sample query parameter that needs to passed is
 * rooms=3&adults=1,2,1&children=0,1,2 So room1 will have 1 adult & 0 children ,room2 will have 2 adults and 1 children ,
 * room3 will have 1 adult and 2 children
 */
function validateRoomsQueryParams(rooms, adults, children) {
    var isValid = false;
    if (isInt(rooms)) {
        if (rooms > 0 && rooms <= 5) {
            if (adults.split(",").length == rooms && children.split(",").length == rooms) {
                if (isOccupantsParamValidFor(adults, 1, 7) && isOccupantsParamValidFor(children, 0, 7)) {
                    isValid = true;
                } else {
                    isValid = false;
                    console
                        .error("Non Integer parameters passed in Adults/Children or Min/Max Adults[1,7]/Childrens[0,7] occupancy validation failed");
                }
            } else {
                isValid = false;
                console.error("No of Adults and Childrens not matching with No of Rooms");
            }
        } else {
            isValid = false;
            console.error("Min/Max No of Rooms [1,5] validation failed");
        }
    } else {
        isValid = false;
        console.error("Invalid Input Parameter passed as rooms");
    }
    return isValid;
}

function isOccupantsParamValidFor(occupants, minValue, maxValue) {
    var isValid = occupants.split(",").every(function (x) {
        if (isInt(x)) {
            if (x >= minValue && x <= maxValue) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    return isValid;
}

function isInt(value) {
    return !isNaN(value) && (function (x) {
        return (x | 0) === x;
    })(parseFloat(value))
}

function verifyFromAndToDate(checkInDate, checkOutDate) {

    var bookingOptions = getBookingOptionsSessionData();
    var overrideSessionDates = getQueryParameter('overrideSessionDates');
    var sessionCheckInDate = moment(bookingOptions.fromDate, "MMM Do YY");
    var nights = moment(checkOutDate, "DD.MM.YYYY").diff(moment(checkInDate, "DD.MM.YYYY"), 'days');
    var queryParamCheckInDate = moment(checkInDate, 'DD/MM/YYYY');
    if (nights > 0) {
        if (queryParamCheckInDate >= sessionCheckInDate) {
            console.log("Date Validation Successfull");
            return true;
        } else {
            if (overrideSessionDates === "true") {
                console.log("Overriding sessionCheckInDate with passedCheckInDate")
                return true;
            }
            console.error("Passed CheckInDate is before SessionCheckInDate");
            return false;
        }
    } else {
        console.error("CheckOutDate is before CheckInDate");
        return false;
    }
}

function registerFilterTabClick() {
    // 1. for rate filter
    $('.rate-tab').click(function () {
        prepareRoomsImpression();
    });
}

$(window).load(function () {
    checkLoginWithMemberRate();
	getRateTabCode();
});

function promptTicLogin() {
    if (shouldPromptTicLogin()) {
        $('body').trigger('taj:sign-in');
        registerLoginListener(function () {
            //refreshPage();
        });
    }
}

function refreshPage() {
    if (window.location) {
        window.location.reload(true);
    } else if (location) {
        location.reload(true);
    }
}

function shouldPromptTicLogin() {
    var url = window.location.href;
    //if (url.indexOf('tajinnercircle.com') != -1) {
        if (!getUserData()) {
            return true;
        }
    //}
    return false;
}

$(document).ready(function () {
    $(".room-list-close").click(function () {
        $(".cm-sold-out-wrap").removeClass("active");
    });
    hideBreadcrumbAndNavigationBar();
    checkHolidayThemeOnLoad();
    fetchRoomsQueryParameters();
    try{
    checkForCurrencyStringInCache();
    }catch(err){
        console.log('failed to fetch currency',err);
    }
    var currencySelected;
    var previousCurrency1 = getCurrencyCache();
    $(document)
        .on(
            'currency:changed',
            function (e, currency) {
                var popupParams = {
                    title: 'Booking Alert!',
                    description: 'Showing currency is Hotel Default currency. Now You will not be  allowed to change Currency. ',
                };
                warningBox(popupParams);
                if (previousCurrency1 != undefined) {
                    dataCache.session.setData('currencyCache', previousCurrency1);
                    setActiveCurrencyInDom(previousCurrency1.currencySymbol,
                        previousCurrency1.currency, previousCurrency1.currencyId);
                }
            });
    // [TIC-FLOW]
    ticRoomListFlow();
    getSelectedCurrencySymbol();
    var globalRateFilters;
    var globalDom;
    setupCarousel();
    setupPromoCodeTab();
    setupNegotiatedRateTab();
    setupRateTab();
    setCurrencyStringInRateTab();
    expandViewDetailsIfSpecified();
    invokeRoomFetchAjaxCall();
    trimServiceAmenityText();
    ShowMoreShowLessRateDescription();
    ShowMoreShowRateCardRoomDescription();
    ShowMoreShowRateCardRoomDetailsDescription();
    // allHotelsUnderDestination();

    // [TIC-FLOW]
    $(".rate-tab").click(
        function () {
            var pointsMsg = $('.points-redeemption-msg');
            var taxDisclaimer = $('.rate-class-disclaimer');
            ($(this).text().includes("TIC ROOM REDEMPTION RATES") || $(this).text().includes(
                "TAJ HOLIDAY PACKAGES")) ? pointsMsg.show() : (pointsMsg.hide() , taxDisclaimer.show());
        });

    function hideBreadcrumbAndNavigationBar() {
        if (window.location.href.includes('tajinnercircle.com')) {
            $('.cm-nav-tab-con').hide();
            $('.specific-hotels-breadcrumb').hide();
        }
    }

});

function checkingCart() {
    var bookingOptions = getBookingOptionsSessionData();
    return dataCache.session.getData('bookingOptions').selection.length;
}

function getCurrencyCache() {
    return dataCache.session.getData('currencyCache');
}

function checkForCurrencyStringInCache() {
    var bookingOptions = getBookingOptionsSessionData();
    if (bookingOptions && bookingOptions.currencySelected === undefined) {
        setCurrencyInSessionStorage(getCurrentCurrencyInHeader());
    }
}

function getCurrentCurrencyInHeader() {
    return $($.find("[data-selected-currency]")[0]).data().selectedCurrency;
}

function ShowMoreShowLessRateDescription() {
    $('.rate-plan-description-txt').cmToggleText({
        charLimit: 125,
        showVal: 'Show More',
        hideVal: 'Show Less',
    });
}

function setCurrencyInSessionStorage(currency) {
    var bookingOptions = getBookingOptionsSessionData();
    bookingOptions.currencySelected = currency;
    dataCache.session.setData("selectedCurrencyString", currency);
    dataCache.session.setData("bookingOptions", bookingOptions);
}

function trimRoomDescriptions() {
    $.each($('.rate-card-room-description'), function (i, value) {
        $(value).cmTrimText({
            charLimit: 125,
        });
    });
}

function trimServiceAmenityText() {
    $.each($('.rate-card-room-details .rate-card-room-facilities span span'), function (i, value) {
        $(value).cmTrimText({
            charLimit: 25,
            ellipsisText: '..',
        });
    });
}

function setupCarousel() {
    var carouselDisplayTriggerElements = $.find('[data-gallery-display-trigger]');

    for (var index in carouselDisplayTriggerElements) {
        var carouselDisplayTriggerDom = $(carouselDisplayTriggerElements).get(index);
        var carouselDisplayData = $(carouselDisplayTriggerDom).data('gallery-display-trigger');
        var carouselDom = $.find('[data-gallery-carousel-overlay=\'' + carouselDisplayData + '\']');
        addCarouselDisplayListenerTo($(carouselDisplayTriggerDom), carouselDom);
    }
}

function setupPromoCodeTab() {
    var promoCode = getPromoCode();
    var promoCodeName = getPromoCodeName();
    // remove any existing promo tab
    $('[data-promo-code]').remove();
    if (promoCode) {
        var promoTab = '<div data-promo-code=\'' + promoCode + '\' class=\'rate-tab\'>' + '<div>' + promoCodeName
        + '</div>' + '<div class="starting-from" style="visibility:hidden"><p style=\'display: inline; color: #9b9b9b;\'>From '
        + '<div data-rate-filter-price style=\'display: inline;\'></div> <span data-injector-key="currency-string"></span><span class="per-night-text"> per night </span></p></div></div> ';
        $('#rate-tab-container').prepend(promoTab);
    }
}

function setupNegotiatedRateTab() {
    var negotiatedCode = getNegotiatedCode();
    var negotiatedRateTabName = "Negotiated Rates";
    // remove any existing promo tab
    $('[data-negotiation-code]').remove();
    if (negotiatedCode) {
        var negotiationRatesTab = '<div data-negotiation-code=\'' + negotiatedCode + '\' class=\'rate-tab\'>' + '<div>' + negotiatedRateTabName
            + '</div>' + '<div class="starting-from" style="visibility:hidden"><p style=\'display: inline; color: #9b9b9b;\'>From '
            + '<div data-rate-filter-price style=\'display: inline;\'></div> <span data-injector-key="currency-string"></span> per night </p></div></div> ';
        $('#rate-tab-container').prepend(negotiationRatesTab);
    }
}

function getNegotiatedCode(){
    var ihclCbUserDetails = dataCache.local.getData("userDetails");
    if(ihclCbUserDetails && (ihclCbUserDetails.selectedEntity || ihclCbUserDetails.selectedEntityAgent)){
        if (ihclCbUserDetails.selectedEntity && ihclCbUserDetails.selectedEntity.partyNumber) {
            return ihclCbUserDetails.selectedEntity.partyNumber;
        } else {
            return ihclCbUserDetails.selectedEntityAgent.partyNumber;
        }
    }
    return "";
}

function setupRateTab() {
    $rateTabElement = $('.rate-tab:visible');
    $rateTabElement.click(function () {
        $(this).siblings().removeClass('tab-selected');
        $(this).addClass('tab-selected');

        var tempRetIndexVal = roomIndexForCurrentPackage();
        var nextRoomNumber = parseInt(tempRetIndexVal.initialRoomIndex) + 1;

        if (ROOM_OCCUPANCY_RESPONSE[nextRoomNumber]) {
            processRoomOccupancyRates(nextRoomNumber);
        }

    });
    $rateTabElement.first().addClass('tab-selected');

    // For voucherRedemption only show offer rate code tab
	var voucherRedemption = getQueryVoucherRedemption();
	if(voucherRedemption && voucherRedemption == 'true'){
		$rateTabElement.not(":eq(0)").hide();
	}
}

function successHandler(response, isError) {
    if (!isError)
        console.info('Ajax call for servlet succeeded');
    else {
        console.info('Error Fetching rates');
    }
    if (response.responseCode.toLowerCase() == 'success') {

        injectResponseToSelector(response);
        
        // Analytic data call
            pushEvent("rooms", "rooms", prepareGlobalHotelListJS())
        if (globalBookingOption.selection != undefined) {
            var selectionArr1 = globalBookingOption.selection;
            selectionArr1.forEach(function (item, index) {
                selectionAr = selectionArr1[index] || item;
                prepareOnRoomSelect(ViewName, event);
            })
        }

        prepareRoomsImpression();
        registerFilterTabClick();
        $('.rate-tab:visible').first().click();
        getRateTabCode();

    } else {
        handleFailureToFetchRates();
    }
    setDefaultRateFilerAfterRateFetch();
    checkHolidayThemeAfterRateFetch();
    showBookedRoomType();

}

function invokeRoomFetchAjaxCall() {
    var requestData = {};

    var rateFilters = getAllRateFilters();
    var promoCode = getPromoCode();
    var negotiatedRateCode= getNegotiatedCode();
    var rateCodes = getAllRatePlanCodes();
    multiOfferCodes = rateCodes;
    promoCode ? requestData.promoCode = promoCode : '';
    negotiatedRateCode ? requestData.corporateCode = negotiatedRateCode :'';
    
    var cacheJson = '';
    if (requestData.promoCode || requestData.corporateCode) {
        cacheJson = 'ratesCache' + '/';
    } else {
        cacheJson = 'ratesCache';
    }

    trigger_hotelSearch(getBookingOptionsSessionData());
    console.info('Attempting to invoke ajax call with data. ');

    ROOM_OCCUPANCY_RESPONSE = {};
    if($('#synxis-downtime-check').val() == "true"){
        $('.rate-card-wait-spinner').hide();
        
        $('.room-rate-unavailable').addClass('visible');
        
        var bo = dataCache.session.getData('bookingOptions');
        var checkInDate = moment(bo.fromDate, "MMM Do YY").format("YYYY-MM-DD");
        var checkOutDate = moment(bo.toDate, "MMM Do YY").format("YYYY-MM-DD");
        var currentHotelId = bo.hotelId;
       
        var roomOptions = bo.roomOptions;
        var roomOptionsLength = roomOptions.length;
        var adults, children;
        for (var r = 0; r < roomOptionsLength; r++) {
            var adlt = roomOptions[r].adults;
            var chldrn = roomOptions[r].children;
            if (r == 0) {
                adults = adlt;
                children = chldrn;
            } else {
                adults = adults + ',' + adlt;
                children = children + ',' + chldrn;
            }
        }
        var synxisRedirectLink = 'https://be.synxis.com/?' + 'arrive=' + checkInDate + '&depart='
        + checkOutDate + '&rooms=' + roomOptionsLength + '&adult=' + adults + '&child=' + children
        + '&hotel=' + currentHotelId + '&chain=21305' + '&currency=' + '&level=chain' + '&locale=en-US'
        + '&sbe_ri=0';
        
        $('.rate-tab-container').hide();
        $('.room-rate-wrap').addClass('visible');
        $('.rate-card-actual-rate').addClass('hidden');
        $('.rate-card-striked-rate').addClass('hidden');
        $('.rate-card-text-container').addClass('hidden');
        
        $($('.more-rates-button')).click( function() {
            window.location.href = synxisRedirectLink;
        });
    }else{
        $.ajax({
            type: 'GET',
            url: '/bin/room-rates/rooms-availability.rates/' + getHotelCode() + '/' + getCheckInDateInDisplay() + '/'
                + getCheckOutDateInDisplay() + '/' + getShortCurrencyStringInDisplay() + '/' + getRoomOccupantOptions()
                + '/' + JSON.stringify(rateFilters) + '/' + JSON.stringify(rateCodes) + '/' + cacheJson,
            dataType: 'json',
            data: requestData,
            error: handleFailureToFetchRates,
            success: successHandler
        });
    }
}

function handleFailureToFetchRates(response) {
    console.error("Ajax call was successful, but the server returned a failure for room rates.");
    pushEvent("rooms_unavailable", "rooms_unavailable", prepareGlobalHotelListJS())
    $('.rate-card-wait-spinner').hide();
    $('.room-rate-wrap').removeClass('visible');
    $('.room-rate-unavailable').addClass('visible');
    checkHolidayThemeAfterRateFetch();
    checkMemberTypeAfterRateFetch();
    if(response && response.status && response.status == 412){
        var redirectLink = $.find('.synxis-configurations .redirect-page-path');
        if(redirectLink != undefined){
            if(redirectLink instanceof Array){
                $(redirectLink)[0].click();
            }else {
                $(redirectLink).click();
            }
        }
    } else {
        if($(".cm-page-container").hasClass("ama-theme")){
            setWarningInDom("Rates are not available for the your particular request. Please try again or after some time.");
        }else{
            setWarningInDom("Oops! Something went wrong. Please Try Again");
        }
    }
}

function injectResponseToSelector(response) {
    ROOM_OCCUPANCY_RESPONSE = response;
    /* processRoomOccupancyRates(response); */
    var roomNumber = getRoomNumberToBeShown();
    processRoomOccupancyRates(roomNumber);
}

function processRoomOccupancyRates(targetRoomNum) {
    /* var index = getRoomNumberToBeShown(); */

    /* var roomOccupancyRates = ROOM_OCCUPANCY_RESPONSE[index]; */
    var roomOccupancyRates = ROOM_OCCUPANCY_RESPONSE[targetRoomNum];
    if (roomOccupancyRates) {
        injectLowestPrice(roomOccupancyRates.rateFilters, roomOccupancyRates.rateCodes, roomOccupancyRates.promoCode);
        showRateCardsIfHidden();
        processSelectedRateTab(roomOccupancyRates.rateFilters, roomOccupancyRates.rateCodes,
            roomOccupancyRates.promoCode);
        showSoldOutMessage(roomOccupancyRates);

        roomFirstClick();
    } else {
        handleFailureToFetchRates();
    }
}

function getRoomNumberToBeShown() {
    var bookingOptions = getBookingOptionsSessionData();
    var currentSelectionLength = bookingOptions.selection.length;
    return currentSelectionLength >= bookingOptions.rooms ? currentSelectionLength : currentSelectionLength + 1;
}

function showErrors(errors) {
    var outString = "";
    errors.forEach(function (error) {
        outString += (error + "\n");
    });
    setWarningInDom(outString);
}

var roomCurCheck;

function injectLowestPrice(rateFilterInfo, rateCodesInfo, promoCodeInfo) {
    try {
        var voucherRedemption = getQueryVoucherRedemption();
        var rateFilterTabs = $.find('[data-rate-filter-code]');
        var currencyForHeader;
        var numOfRateFilters = rateFilterTabs.length;

        // this condition is for handleling voucher redemption
        if(!(voucherRedemption && voucherRedemption == 'true')) {
            for (var k = 0; k < numOfRateFilters; k++) {
                var rateFilterTab = rateFilterTabs[k];
                var rateFilterCode = $(rateFilterTab).attr('data-rate-filter-code');
                var rateFilterRates = rateFilterInfo[rateFilterCode];
                if (!rateFilterRates || !rateFilterRates[getHotelCode()]) {
                    $(rateFilterTab).hide();
                    continue;
                }
                var hotelRates = rateFilterRates[getHotelCode()];
                var lowestPrice;
                if ($('.cm-page-container').hasClass('ama-theme')) {
                    var query = window.location.search;
                    var lowestRoomCode = $('#lowestRoomCode').val();
                    var bungalowCode = $('#bungalowCode').val();
                    var roomRateCode = hotelRates.roomsAvailability[lowestRoomCode];
                    var bungalowRateCode = hotelRates.roomsAvailability[bungalowCode];
                    if (roomRateCode && !query.includes('onlyBungalows')) {
                        if (roomRateCode.lowestDiscountedPrice == 0) {
                            lowestPrice = roomRateCode.lowestPrice;
                        } else {
                            lowestPrice = roomRateCode.lowestDiscountedPrice;
                        }
                    } else if (bungalowRateCode && query.includes('onlyBungalows')) {
                        if (bungalowRateCode.lowestDiscountedPrice == 0) {
                            lowestPrice = bungalowRateCode.lowestPrice;
                        } else {
                            lowestPrice = bungalowRateCode.lowestDiscountedPrice;
                        }
                    }
                } else {
                    if (hotelRates.lowestDiscountedPrice == 0 || hotelRates.lowestDiscountedPrice > hotelRates.lowestPrice) {
                        lowestPrice = hotelRates.lowestPrice;
                    } else {
                        lowestPrice = hotelRates.lowestDiscountedPrice;
                    }
                }
    
                var currencyString = hotelRates.currencyString;
    
                if (currencyString) {
                    currencyForHeader = currencyString;
                    $(rateFilterTab).find('[data-injector-key]').text(currencyString);
                    $(rateFilterTab).find('[data-rate-filter-price]').text(getCommaFormattedNumber(lowestPrice));
                    $(rateFilterTab).show();
                } else {
                    $(rateFilterTab).hide();
                }
                $(rateFilterTab).find('.starting-from').css('visibility', 'visible');
    
            }

            previousCurrency1 = getCurrencyCache();
            
            if (rateCodesInfo) {
                var offerRateTab = $('[data-offer-rate-code]')[0];
                var offerRateCode = $(offerRateCode).data('offerRateCode');
                var offerRates = rateCodesInfo[getHotelCode()];
                var roomsAvailability = offerRates.roomsAvailability;
                var multiOfferCheck = false;
                for (var r in roomsAvailability) {
                    if (roomsAvailability[r].rooms && !multiOfferCheck
                        && (multiOfferCodes.length == roomsAvailability[r].rooms.length)) {
                        multiOfferCheck = true;
                    }
        
                }
                if (!multiOfferCheck) {
                    offerRates.currencyString = null
                    offerRates.roomsAvailability = {};
                    offerRates.lowestPrice = 0
                    offerRates.lowestDiscountedPrice = 0
                }
        
                /*
                 * offerRates.forEach(function(data, index) { console.log(offerRates.rooms); })
                 */
        
                var offerRatesCurrency = offerRates.currencyString;
                if (offerRatesCurrency) {
                    currencyForHeader = offerRatesCurrency;
                    $(offerRateTab).find('.starting-from').css('visibility', 'visible');
                } else {
                    $(offerRateTab).find('.starting-from').css('visibility', 'hidden');
                }
                processNonFilterTab(offerRateTab, rateCodesInfo);
            }

        }
        // handle voucher redemption

        if (promoCodeInfo) {
            var negotiationTab = $('[data-negotiation-code]')[0];
            var promoTab = $('[data-promo-code]')[0];
            var promoCurrencyType = promoCodeInfo[getHotelCode()].currencyString;
            if(negotiationTab){
                processNonFilterTab(negotiationTab, promoCodeInfo);
                if(promoCurrencyType){ 
                    currencyForHeader = promoCurrencyType;
                    $(negotiationTab).find('.starting-from').css('visibility', 'visible');
                }
            }else{
                // Voucher related handle for amount
				//console.log('before', promoCodeInfo[getHotelCode()]);
                if(voucherRedemption && voucherRedemption == 'true'){
					promoCodeInfo = makeAvailabilityAsPerVoucher(promoCodeInfo)
                    textChangesForVoucherRedemotion();
                }
                //console.log('after', promoCodeInfo[getHotelCode()]);

                processNonFilterTab(promoTab, promoCodeInfo);
                if(promoCurrencyType){
                    $(promoTab).find('.starting-from').css('visibility', 'visible');
                }
            }
        }
        
        if (currencyForHeader) {
            roomCurCheck = setActiveCurrencyWithResponseValue(currencyForHeader);
        }
        
    } catch (error) {
        console.error(error);
    }
}

function processRateFilterTab(dom, code, info) {
    var filterRate = info[code];
    return processNonFilterTab(dom, filterRate);
}

function processNonFilterTab(dom, info) {
    try {
    var voucherRedemption = getQueryVoucherRedemption();
    var hotelId = getHotelCode();
    var lPrice;
    if (info[hotelId].lowestDiscountedPrice == 0) {
        lPrice = info[hotelId].lowestPrice;
    } else {
        lPrice = info[hotelId].lowestDiscountedPrice;
    }
    
    if($('.cm-page-container').hasClass('ama-theme')){
        var hotelRates = info[hotelId];
        var query = window.location.search;
        var lowestRoomCode = $('#lowestRoomCode').val();
        if(hotelRates){
            var roomRateCode = hotelRates.roomsAvailability[lowestRoomCode];
            if (roomRateCode && query.includes('offerRateCode')) {
                if (roomRateCode.lowestDiscountedPrice == 0) {
                    lPrice = roomRateCode.lowestPrice;
                } else {
                    lPrice = roomRateCode.lowestDiscountedPrice;
                }
    		}
    	}
    }
    if (info[hotelId].currencyString) {
        $(dom).find('[data-injector-key]').text(info[hotelId].currencyString);
    } else {
        $(dom).find('[data-injector-key]').text("");
    }
	if(voucherRedemption && voucherRedemption == 'true'){
        var tabElement = $('.rate-tab.tab-selected')
		$(dom).find('[data-injector-key]').text("");
		tabElement.find('p').hide();
		$(dom).find('[data-injector-key]').closest('span').hide();
		return $($(dom).find("[data-rate-filter-price]")[0]).text('Redeem Voucher');
	}

    return $($(dom).find("[data-rate-filter-price]")[0]).text(getCommaFormattedNumber(lPrice));
    }catch(error){
        console.error('error in method processNonFilterTab ');
        console.error(error);
    }
}

function setWarningInDom(warning) {
    tajToaster(warning);
}

function showRateCardsIfHidden() {
    
        $('.rate-card-wait-spinner').hide();
        $('.room-rate-wrap').addClass('visible');
        $('.room-rate-unavailable').removeClass('visible');
    
}

function processSelectedRateTab(rateFilters, rateCodesPrices, promoCodePrices) {
    $('.rate-card-wrap').find('.rate-card-more-rates-section').remove();
    var rateTabInDisplay = getRateTabInDisplay();
    var rates;
    var hotelCode = getHotelCode();
    if (isOfferRateSelected()
        || (userIsTicBased() && ((rateTabInDisplay === 'TIC ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAP ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAJ HOLIDAY PACKAGES')))) {
        rates = rateCodesPrices;
    } else if (isPromoTabSelected()) {
        rates = promoCodePrices;
    } else if (isNegotiationRateTabSelected()){
        rates = promoCodePrices;
    } else {
        rates = rateFilters[rateTabInDisplay];
    }

    if (!rates) {
        handleFailureToFetchRates();
    }

    var hotelRoot = rates[hotelCode];

    if (hotelRoot.error && hotelRoot.error.length) {
        showErrors(hotelRoot.error);
    }

    if (hotelRoot.warning && hotelRoot.warning.length) {
        showErrors(hotelRoot.warning);
    }
    var roomsAvailability = hotelRoot['roomsAvailability'];
    injectValuesForRoomCards(roomsAvailability);
    setupListenerForAddRoom();
}

function isOfferRateSelected() {
    var selectedTabs = $('.rate-tab-wrap').find('.tab-selected')
    if (selectedTabs.length == 0) {
        selectedTabs = $('.rate-tab[data-offer-rate-code]').addClass('tab-selected');
    }
    return $(selectedTabs).first().attr('data-offer-rate-code');
}

function isNegotiationRateTabSelected() {
    var selectedTabs = $('.rate-tab-wrap').find('.tab-selected')
    return $(selectedTabs).first().attr('data-negotiation-code');
}

function isPromoTabSelected() {
    var selectedTabs = $('.rate-tab-wrap').find('.tab-selected')
    return $(selectedTabs).first().attr('data-promo-code');
}

function injectValuesForRoomCards(roomsAvailabilityData) {
    var allRoomContainersInDom = getAllRoomContainersFromDom();
    $(allRoomContainersInDom).each(
        function (key, roomContainerInDom) {
            try {
                var roomBedTypeMap = $(this).data("bed-room-map");
                var kingBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.king];
                var doubleBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.double];
                var queenBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.queen];
                var twinBedRoomAvail = roomsAvailabilityData[roomBedTypeMap.twin];
                var availableBedOptions = updateBedTypeAvailability(roomBedTypeMap, kingBedRoomAvail, doubleBedRoomAvail,
                    queenBedRoomAvail, twinBedRoomAvail);
                $(this).attr("data-bed-type-opts", JSON.stringify(availableBedOptions));
                var onlineAvailable = $($($(this)).find('.rate-card-room-rate-container')).data(
                    "online-booking-available");
                if (onlineAvailable == undefined) {
                    $(this).find('.room-rate-wrap').removeClass('visible');
                    showElementUnderWithClass($(this), '.online-booking-unavailable');
                    $(this).data('sort-order', 9999999999999999999999999999 + key);
                    removeRoomUnavailability($(this));
                } else if (kingBedRoomAvail || doubleBedRoomAvail || queenBedRoomAvail || twinBedRoomAvail) {
                    injectIntoRoomContainer(this, kingBedRoomAvail || doubleBedRoomAvail || queenBedRoomAvail || twinBedRoomAvail);
                    // $(this).show();
                    removeRoomUnavailability($(this));
                } else {
                    $(this).find('.room-rate-wrap').removeClass('visible');
                    setRoomUnavailability($(this));
                    $(this).data('sort-order', 99999999999999999999999999999 + key);
                }
            } catch (error) {
                console.error(error);
            }
        }).sort(function (a, b) {
        if ($(a).data('sort-order') === $(b).data('sort-order')) {
            return 0;
        }
        return $(a).data('sort-order') > $(b).data('sort-order') ? 1 : -1;
    }).detach().appendTo('.rate-cards-container.cm-content-blocks .cm-room-options').show();
    onlyBungalowsShowHide();
}

function onlyBungalowsShowHide() {
    var parameters = window.location.search;
    if (parameters) {
        var query = parameters.split('?');
        var param = query[1];
        if (param.includes('onlyBungalows')) {
            $('.rate-card-wrap').each(function() {
                if(!$(this).attr('data-room-type')) {
                    $(this).hide();
                }
            });
        }
    }
}

function removeRoomUnavailability(roomCard) {
    var amaTheme = $('.cm-page-container').hasClass('ama-theme');
    if (amaTheme) {
        var soldOut = $(roomCard).find('.rate-card-main-section');
        soldOut.removeClass('soldOut');
    }
}

function setRoomUnavailability(roomCard) {
    var rateContainer = $(roomCard).find('.rate-card-room-rate-container');
    var onlineAvailable = $(rateContainer).data("online-booking-available");
    var amaTheme = $('.cm-page-container').hasClass('ama-theme');
    if (onlineAvailable == undefined) {
        showElementUnderWithClass(roomCard, '.online-booking-unavailable');
    } else if (isSelectionLessThanMinimum(rateContainer)) {
        showElementUnderWithClass(roomCard, '.minimum-booking-days');
    } else {
        showElementUnderWithClass(roomCard, '.room-rate-unavailable');
        if (amaTheme) {
            var soldOut = $(roomCard).find('.rate-card-main-section');
            $(roomCard).find('.room-rate-unavailable').removeClass('visible');
            soldOut.addClass('soldOut');
        }
    }

    // Analytics Data For AMA
    AvailRooms = 0;
    var rooms = $(".rate-card-wrap");
    rooms.each(function () {
        if(!$(this).children().hasClass("soldOut")){
            AvailRooms++;
        };
    });
    try {
        amaDataLayerData.AvailRooms = AvailRooms;
    } catch (e) { }
}

function showElementUnderWithClass(element, cssClass) {
    var onlineUnavailableDom = $(element).find(cssClass);
    $(onlineUnavailableDom).addClass('visible');
    $(onlineUnavailableDom).removeClass('hidden');
}

function isSelectionLessThanMinimum(rateContainer) {
    var minDaysForBooking = $(rateContainer).data("min-days-for-booking");
    var checkInDate = getCheckInDateInDisplay();
    var checkOutDate = getCheckOutDateInDisplay();
    var numOfDaysSelected = moment(checkOutDate).diff(moment(checkInDate), 'days');

    return numOfDaysSelected < minDaysForBooking;
}

function injectIntoRoomContainer(roomContainer, data) {
    injectPricesToRoomContainer(roomContainer, data);
    var numOfAvailableUnits = data.availableUnits;
    setAvailabilityExpression(roomContainer, numOfAvailableUnits);
    injectRatePlansToRoomContainer(roomContainer, data["rooms"], numOfAvailableUnits);
}

function setAvailabilityExpression(roomContainer, numOfAvailableUnits) {
    var availabilityContainer = $(roomContainer).find('[data-injector-key="availability-expression"]');
    if (numOfAvailableUnits < 5) {
        $(availabilityContainer).show();
        $(availabilityContainer).removeClass("hidden");
    } else {
        $(availabilityContainer).hide();
        $(availabilityContainer).addClass("hidden");
    }
}

function findDataInjectionContainerUnder(key, root) {
    if (root == undefined) {
        root = $('body');
    }
    var container = $(root).find('[data-injection-container=\'' + key + '\']');
    return container;
}

function findRatePlanCardsContainer() {
    var ratePlansContainer = $.find("[data-injection-container='" + "rate-plan-cards-container" + "']");
    return ratePlansContainer;
}

function injectRatePlansToRoomContainer(container, ratePlanData, numOfAvailableUnits) {
    var ratePlansContainerElement = getRatePlansContainerElement();
    var ratePlanDomTemplate = getRatePlanTemplateUnder(ratePlansContainerElement);
    var shouldIncludeTax = $("#room-list").data("tax");
    for (index in ratePlanData) {
        var ratePlan = ratePlanData[index];
        var ratePlanCard = ratePlanDomTemplate.clone();
        injectValuesToRatePlanCard(ratePlanCard, ratePlan, shouldIncludeTax);
        setAvailabilityExpression(ratePlanCard, numOfAvailableUnits);
        $(ratePlansContainerElement).append(ratePlanCard);
    }
    $(container).append(ratePlansContainerElement);
}

function getRatePlanTemplateUnder(container) {
    var domElement = $(container).find('[data-injection-container=\'rate-plan-card\']');
    var ratePlanDomTemplate = cloneFirstElementIn(domElement);
    $(domElement).remove();
    var ratePlanAmenities = findDataInjectionContainerUnder('rate-plan-amenities', ratePlanDomTemplate);
    $(ratePlanAmenities).children().remove();
    return ratePlanDomTemplate;
}

function getRatePlansContainerElement() {
    var ratePlanCardsContainer = findRatePlanCardsContainer();
    var ratePlanCardsContainerTemplate = cloneFirstElementIn(ratePlanCardsContainer);
    return ratePlanCardsContainerTemplate;
}

function injectValuesToRatePlanCard(ratePlanCard, ratePlan, shouldIncludeTax) {

    checkTicFlow(ratePlanCard);

    setTextInDom(ratePlanCard, "rate-plan-title", ratePlan.ratePlanTitle);
    setTextInDom(ratePlanCard, "cancellation-policy", ratePlan.cancellationPolicy);
    var ratePlanTitleDom = $(ratePlanCard).find(".more-rate-title");
    $(ratePlanTitleDom).attr('data-rate-plan-code', ratePlan.ratePlanCode);

    var basePriceForCart;

    if ((ratePlan.discountedPrice != ratePlan.price) && (ratePlan.discountedPrice > 0)) {
        basePriceForCart = displayDiscountedPrice(ratePlanCard, ratePlan, shouldIncludeTax);
    } else {
        basePriceForCart = displayPrice(ratePlanCard, ratePlan, shouldIncludeTax, false);
    }

    var nightlyRates;
    if (ratePlan.nightlyRates && ratePlan.nightlyRates != null) {
        nightlyRates = JSON.stringify(ratePlan.nightlyRates);
    }

    var discountedNightlyRates;
    if (ratePlan.discountedNightlyRates && ratePlan.discountedNightlyRates != null) {
        discountedNightlyRates = JSON.stringify(ratePlan.discountedNightlyRates);
    }

    var taxes;
    if (ratePlan.taxes && ratePlan.taxes != null) {
        taxes = JSON.stringify(ratePlan.taxes);
    }

    setTextInDom(ratePlanCard, 'base-price', basePriceForCart, ratePlan.currencyString);
    setTextInDom(ratePlanCard, 'tax', ratePlan.tax, ratePlan.currencyString);
    setHtmlInDom(ratePlanCard, 'rate-description', ratePlan.rateDescription);
    setTextInDom(ratePlanCard, 'taxes', taxes, ratePlan.currencyString);
    setTextInDom(ratePlanCard, 'nightly-rates', nightlyRates, ratePlan.currencyString);
    setTextInDom(ratePlanCard, 'discounted-nightly-rates', discountedNightlyRates, ratePlan.currencyString)
    setTextInDom(ratePlanCard, 'avg-taxes', ratePlan.avgTax, ratePlan.currencyString)
    setTextInDom(ratePlanCard, 'guarantee-code', ratePlan.guaranteeCode);
    setHtmlInDom(ratePlanCard, 'credit-card-required', ratePlan.creditCardRequired);
    setTextInDom(ratePlanCard, 'guarantee-amount', ratePlan.guaranteeAmount);
    setTextInDom(ratePlanCard, 'guarantee-percentage', ratePlan.guaranteePercentage);
    setTextInDom(ratePlanCard, 'guarantee-description', ratePlan.guaranteeDescription);
    setHtmlInDom(ratePlanCard, 'guarantee-amount-tax-inclusive', ratePlan.guaranteeAmountTaxInclusive);
    setTextInDom(ratePlanCard, 'taxes', taxes, ratePlan.currencyString);
    setTextInDom(ratePlanCard, 'nightly-rates', nightlyRates, ratePlan.currencyString);
    setTextInDom(ratePlanCard, 'discounted-nightly-rates', discountedNightlyRates, ratePlan.currencyString)
    setTextInDom(ratePlanCard, 'avg-taxes', ratePlan.avgTax, ratePlan.currencyString)
    injectAmenitiesToRatePlan(ratePlanCard, ratePlan.ratePlanAmenities);
}

function setHtmlInDom(container, injectorKey, htmlData) {
    var domElement = $(container).find('[data-injector-key=' + injectorKey + ']');
    $(domElement).html(htmlData);
}

function injectAmenitiesToRatePlan(ratePlanCard, ratePlanAmenities) {
    var amenitiesContainer = findDataInjectionContainerUnder('rate-plan-amenities', ratePlanCard);
    var numOfAmenities = ratePlanAmenities.length;
    for (var i = 0; i < numOfAmenities; i++) {
        var amenity = ratePlanAmenities[i];
        var amenityRoot = $('<div>');
        $(amenityRoot).addClass('amenities-avail-list');

        var amenityImage = $('<img>');
        $(amenityImage).attr('src', amenity.amenityIconPath);
        $(amenityImage).addClass('amenities-list-icon');
        $(amenityRoot).append(amenityImage);

        var amenityName = $('<div>');
        $(amenityName).addClass('amenities-list-label');
        $(amenityName).text(amenity.amenityName);
        $(amenityName).addClass('amenities-list-label');
        $(amenityRoot).append(amenityName);

        $(amenitiesContainer).append(amenityRoot);
    }
}

function displayDiscountedPrice(ratePlanCard, ratePlan, shouldIncludeTax) {
    var tax = 0;
    if (shouldIncludeTax) {
        tax = ratePlan.tax;
    }
    var discountedPrice = ratePlan.discountedPrice
    var inclusiveOfTax = discountedPrice + tax;
    var basePriceForCart = ratePlan.totalPrice + tax;
    setTextInDom(ratePlanCard, 'price', ratePlan.price + tax);
    setTextInDom(ratePlanCard, 'discounted-price', inclusiveOfTax);
    return basePriceForCart;
}

function displayPrice(ratePlanCard, ratePlan, shouldIncludeTax, isDiscountAvailable) {
    var basePriceForCart;
    var tax = 0;
    if (shouldIncludeTax) {
        tax = ratePlan.tax;
    }
    if (isDiscountAvailable) {
        setTextInDom(ratePlanCard, 'price', ratePlan.price + tax);
        basePriceForCart = ratePlan.discountedPrice;
        showStrikedPrice(ratePlanCard);
    } else {
        setTextInDom(ratePlanCard, "discounted-price", ratePlan.price + tax);
        basePriceForCart = ratePlan.totalPrice;
        hideStrikedPrice(ratePlanCard);
    }
    return basePriceForCart;
}

function cloneFirstElementIn(elements) {
    return $(elements).first().clone();
}

function injectAmenitiesToRatePlan(ratePlanCard, ratePlanAmenities) {
    var amenitiesContainer = findDataInjectionContainerUnder('rate-plan-amenities', ratePlanCard);
    var numOfAmenities = ratePlanAmenities.length;
    for (var i = 0; i < numOfAmenities; i++) {
        var amenity = ratePlanAmenities[i];
        var amenityRoot = $('<div>');
        $(amenityRoot).addClass('amenities-avail-list');

        var amenityImage = $('<img>');
        $(amenityImage).attr('src', amenity.amenityIconPath);
        $(amenityImage).addClass('amenities-list-icon');
        $(amenityRoot).append(amenityImage);

        var amenityName = $('<div>');
        $(amenityName).addClass('amenities-list-label');
        $(amenityName).text(amenity.amenityName);
        $(amenityName).addClass('amenities-list-label');
        $(amenityRoot).append(amenityName);

        $(amenitiesContainer).append(amenityRoot);
    }
}

function displayDiscountedPrice(ratePlanCard, ratePlan, shouldIncludeTax) {
    var tax = 0;
    if (shouldIncludeTax) {
        tax = ratePlan.tax;
    }
    var discountedPrice = ratePlan.discountedPrice
    var inclusiveOfTax = discountedPrice + tax;
    var basePriceForCart = ratePlan.totalPrice + tax;
    setTextInDom(ratePlanCard, 'price', ratePlan.price + tax);
    setTextInDom(ratePlanCard, 'discounted-price', inclusiveOfTax);
    return basePriceForCart;
}

function displayPrice(ratePlanCard, ratePlan, shouldIncludeTax, isDiscountAvailable) {
    var basePriceForCart;
    var tax = 0;
    if (shouldIncludeTax) {
        tax = ratePlan.tax;
    }
    if (isDiscountAvailable) {
        setTextInDom(ratePlanCard, 'price', ratePlan.price + tax);
        basePriceForCart = ratePlan.discountedPrice;
        showStrikedPrice(ratePlanCard);
    } else {
        if (ratePlan
            && ratePlan.ratePlanTitle
            && (ratePlan.ratePlanTitle.indexOf('TIC Room Redemption Points') != -1
                || ratePlan.ratePlanTitle.indexOf('TAP and TAPPMe Room Redemptions') != -1
                || ratePlan.ratePlanTitle.indexOf('Taj Holidays Redemption') != -1)) {
            setTextInDom(ratePlanCard, "discounted-price", ratePlan.price + ratePlan.avgTax, ratePlan.currencyString);
        } else {
            setTextInDom(ratePlanCard, "discounted-price", ratePlan.price + tax);
        }
        basePriceForCart = ratePlan.totalPrice;
        hideStrikedPrice(ratePlanCard);
    }
    return basePriceForCart;
}

function cloneFirstElementIn(elements) {
    return $(elements).first().clone();
}

function injectPricesToRoomContainer(roomContainer, data) {
    // [TIC-FLOW]
    checkTicFlow(roomContainer);
    var rateTabInDisplay = getRateTabInDisplay();
    var discountedPrice = data.lowestDiscountedPrice;
    var lowestPrice =data.lowestPrice;

    // handle voucher redemption price

	var voucherRedemption = getQueryVoucherRedemption();
	if(voucherRedemption && voucherRedemption == 'true'){
	}


    if ((lowestPrice != discountedPrice) && (discountedPrice > lowestPrice)){
        [discountedPrice, lowestPrice] = [lowestPrice, discountedPrice];
        if ((rateTabInDisplay === 'TIC ROOM REDEMPTION RATES') || (rateTabInDisplay === 'TAP ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES') || rateTabInDisplay === 'TAJ HOLIDAY PACKAGES') {
            var price = discountedPrice;
            if (data.lowestPriceTax) {
                price = price + data.lowestPriceTax;
            }
            setTextInDom(roomContainer, "lowest-discounted-price", discountedPrice);
        } else {
            setTextInDom(roomContainer, "lowest-discounted-price", discountedPrice);
        }
        hideStrikedPrice(roomContainer);
        $(roomContainer).data('sort-order', lowestPrice);
    }else if ((lowestPrice != discountedPrice) && (discountedPrice > 0)) {
        if ((rateTabInDisplay === 'TIC ROOM REDEMPTION RATES') || (rateTabInDisplay === 'TAP ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES') || rateTabInDisplay === 'TAJ HOLIDAY PACKAGES') {
            var price = discountedPrice;
            if (data.lowestPriceTax) {
                price = price + data.lowestPriceTax;
            }
            setTextInDom(roomContainer, "lowest-discounted-price", discountedPrice);
        } else {
            setTextInDom(roomContainer, "lowest-discounted-price", discountedPrice);
        }
        showStrikedPrice(roomContainer);
        $(roomContainer).data('sort-order', discountedPrice);
        setPriceMicroData(roomContainer, "lowest-discounted-price", discountedPrice);
        setTextInDom(roomContainer, "lowest-price", lowestPrice, data.currencyString);
        setPriceMicroData(roomContainer, "lowest-price", lowestPrice);
    }
    else{
        if ((rateTabInDisplay === 'TIC ROOM REDEMPTION RATES') || (rateTabInDisplay === 'TAP ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES') || rateTabInDisplay === 'TAJ HOLIDAY PACKAGES') {
            setTextInDom(roomContainer, "lowest-discounted-price", lowestPrice + data.lowestPriceTax,
                data.currencyString);
            setPriceMicroData(roomContainer, "lowest-discounted-price", lowestPrice + data.lowestPriceTax);
        } else {
            setTextInDom(roomContainer, "lowest-discounted-price", lowestPrice, data.currencyString);
            setPriceMicroData(roomContainer, "lowest-discounted-price", lowestPrice);
        }
        hideStrikedPrice(roomContainer);
        $(roomContainer).data('sort-order', lowestPrice);
    }
    setCurrencySymbolInDoms(data);
}

function setCurrencySymbolInDoms(roomData) {
    var currencySymbolDomList = $.find("[data-injector-key='currency-symbol']");
    var currencySymbol;
    if (roomCurCheck) {
        currencySymbol = getCurrencyCache().currencySymbol.trim();
    } else {
        currencySymbol = roomData.currencyString;
    }
    for (i in currencySymbolDomList) {
        var currencySymbolDom = currencySymbolDomList[i];
        $(currencySymbolDom).text(currencySymbol);
    }
}

function setCurrencyStringInRateTab() {
    var rateFilterDomList = $.find("[data-injector-key='currency-string']");
    var currencyString = getCurrencyStringInDisplay();
    for (i in rateFilterDomList) {
        var rateFilterDom = rateFilterDomList[i];
        $(rateFilterDom).text(currencyString);
    }
}

function showStrikedPrice(container) {
    $(container).find('.rate-card-striked-rate').show();
}

function hideStrikedPrice(container) {
    $(container).find('.rate-card-striked-rate').hide();
}

function setTextInDom(container, injectorKey, text, currencyString) {
    var domElement = $(container).find('[data-injector-key=\'' + injectorKey + '\']');
    $(domElement).text(getCommaFormattedNumber(text));

    // [TIC-FLOW]
    var userDetails = getUserData();
    var rateTabInDisplay = getRateTabInDisplay();
    if (userDetails
        && userDetails.card
        && userDetails.card.tier
        && ((rateTabInDisplay === 'TIC ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAP ROOM REDEMPTION RATES')
            || (rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES') || rateTabInDisplay === 'TAJ HOLIDAY PACKAGES')) {

        if (currencyString && (currencyString != 'INR' && currencyString != '₹')) {
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                var conversionRate = parseFloat(currencyRateConversionString[currencyString + '_INR']);
                text = Math.round(text) * conversionRate;
            }

        }

        $(container).find('.rate-card-actual-rate').find('.tic-points').html(getCommaFormattedNumber(Math.round(text)));
        $(container).find('.rate-card-actual-rate').find('.epicure-points').html(
            getCommaFormattedNumber(Math.round(text / 2)));
        $(container).find('.rate-card-actual-rate').find('.tap-points').html(getCommaFormattedNumber(Math.round(text)));
        $(container).find('.rate-card-actual-rate').find('.tappme-points').html(getCommaFormattedNumber(Math.round(text)));
        // this is for rate card section
        if (injectorKey === 'discounted-price') {
            // Since only one rate code will come for TIC for each room
            $(container).find('.more-rate-info-wrp').find('.rate-info-wrp').find('.present-rate').find('.tic-points')
                .html(getCommaFormattedNumber(Math.round(text)));
            $(container).find('.more-rate-info-wrp').find('.rate-info-wrp').find('.present-rate').find(
                '.epicure-points').html(getCommaFormattedNumber(Math.round(text / 2)));
            $(container).find('.more-rate-info-wrp').find('.rate-info-wrp').find('.present-rate').find('.tap-points')
                .html(getCommaFormattedNumber(Math.round(text)));
            $(container).find('.more-rate-info-wrp').find('.rate-info-wrp').find('.present-rate')
                .find('.tappme-points').html(getCommaFormattedNumber(Math.round(text)));

        }
    }
}

function setPriceMicroData(container, injectorKey, price) {
    var domElement = $(container).find('[data-injector-key=\'' + injectorKey + '\']');
    $(domElement).attr("itemprop", "price");
    $(domElement).attr("content", price);
}

function getCommaFormattedNumber(number) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number;
    } else {
        number = Math.round(number);
        formattedNumber = number.toLocaleString('en-IN')
    }
    return formattedNumber;
}

function getAllRoomContainersFromDom() {
    return $.find('[data-component-id="room-card"]');
}

function getRoomContainersFromDom(roomIdentifier) {
    return $.find("[data-room-type-name='" + roomIdentifier + "']");
}

function getRateTabInDisplay() {
    var rateTabInDisplay = "";
    var selectedTabs = $('.rate-tab-wrap').find('.tab-selected')
    // [TIC-FLOW]
    var userDetails = getUserData();
    if ($(selectedTabs).first().attr("data-rate-tic-filter-code") && userDetails && userDetails.card
        && userDetails.card.tier) {
        rateTabInDisplay = $('#member-rate-title').text();
    } else {
        rateTabInDisplay = $(selectedTabs).first().data("rate-filter-code");
    }

    return rateTabInDisplay;
}

function getHotelCode() {
    var hotelIdDomArray = $.find("[data-hotel-id]");
    var hotelIdContainer = "";
    if (hotelIdDomArray != undefined) {
        hotelIdContainer = hotelIdDomArray[0];
    }
    return $(hotelIdContainer).data().hotelId;
}

function getCheckInDateInDisplay() {
    var sessionData = getBookingOptionsSessionData();
    var fromDate=undefined;
    if(sessionData){
        fromDate = moment(sessionData.fromDate, 'MMM Do YY').format('YYYY-MM-DD');
    }
    return fromDate;
}

function getCheckOutDateInDisplay() {
    var sessionData = getBookingOptionsSessionData();

        var fromDate = moment(sessionData.toDate, 'MMM Do YY').format('YYYY-MM-DD');
        return fromDate;



}

function getRoomOccupantOptions() {
    var sessionData = getBookingOptionsSessionData();
    var roomOptions = [];

    sessionData.roomOptions.forEach(function (value) {
        roomOptions.push(value.adults);
        roomOptions.push(value.children)
    });
    // return sessionData.roomOptions;
    return roomOptions;
}

function getShortCurrencyStringInDisplay() {
    var sessionData = getBookingOptionsSessionData();
    return sessionData.currencySelected;
}

function getCurrencyStringInDisplay() {
    var sessionData = getBookingOptionsSessionData();
    return sessionData.currencySelected;
}

function getSelectedCurrencySymbol() {
    var sessionData = getBookingOptionsSessionData();
    key = sessionData.currencySelected;
    var parentCurrencyContainer = $.find("[data-currency-id='" + key + "']")[0];
    var currencySymbolDom = $(parentCurrencyContainer).find('.header-dd-option-currency')[0];
    var selectedCurrencySymbol = $(currencySymbolDom).text();
    return selectedCurrencySymbol;
}

function expandViewDetailsIfSpecified() {
    var roomIdentifier = getParameterByName('room-name');
    if (roomIdentifier) {
        var roomContainerDom = getRoomContainersFromDom(roomIdentifier);
        var viewDetailsButton = $(roomContainerDom).find(".rate-card-view-detials-container");
        onClickViewDetailsButton(viewDetailsButton);
    }
}

function getParameterByName(name) {
    var paramValue = '';
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results != null) {
        paramValue = decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    return paramValue;
}

function getAllRateFilters() {
    var allRateFilters = [];
    var allRateFiltersInDom = $.find("[data-rate-filter-code]");
    for (index in allRateFiltersInDom) {
        var rateFilterDom = $(allRateFiltersInDom).get(index);
        var rateFilterCode = $(rateFilterDom).data("rate-filter-code");
        allRateFilters.push(rateFilterCode);
    }
    return allRateFilters;
}

/*
 * This function is modified to fetch all the offerRateCode which are comma-separated in query parameter and is
 * subsequently converted to JS array and is passed in to the RommsAvailabilityFetchServlet
 */

function getAllRatePlanCodes() {
    var ratePlanArray = [];
    var allOfferRateTabsInDom = $.find("[data-offer-rate-code]");
    var rateplanCodes = $(allOfferRateTabsInDom).data("offerRateCode");
    if (rateplanCodes) {
        rateplanCodes = new String(rateplanCodes);
        ratePlanArray = rateplanCodes.split(",");
    } 
    return ratePlanArray;
}

function getPromoCode() {
    return getQueryParameter('promoCode');
}

function getPromoCodeName() {
    return $("#promo-rate-tab-name").text() || "PROMOTIONAL RATES";
}

function getBookingOptionsSessionData() {
    return dataCache.session.getData("bookingOptions");
}

function refreshRoomAvailability(e, bookingOptions) {
    setupPromoCodeTab();
    setupNegotiatedRateTab();
    setupRateTab();
    invokeRoomFetchAjaxCall();
}

function ShowMoreShowRateCardRoomDescription() {
    $.each($('.rate-card-room-description'), function (i, value) {
        $(value).cmToggleText({
            charLimit: 170,
            showVal: 'Show More',
            hideVal: 'Show Less',
        })
    })
};

function ShowMoreShowRateCardRoomDetailsDescription() {
    $.each($('.rate-card-room-details-wrap .content'), function (i, value) {
        $(value).cmToggleText({
            charLimit: 220,
            showVal: 'Show More',
            hideVal: 'Show Less',
        });
    })
};

function updateBedTypeAvailability(roomBedTypeMap, kingBedRoomAvail, doubleBedRoomAvail, queenBedRoomAvail, twinBedRoomAvail) {
    var obj = {};
    if (kingBedRoomAvail)
        obj.king = roomBedTypeMap.king;
    if (doubleBedRoomAvail)
        obj.double = roomBedTypeMap.double;
    if (queenBedRoomAvail)
        obj.queen = roomBedTypeMap.queen;
    if (twinBedRoomAvail)
        obj.twin = roomBedTypeMap.twin;
    return obj;
}

var isHolidayTheme = false;

function checkHolidayThemeOnLoad() {
    isHolidayTheme = getQueryParameter('holidaysOffer');
    if (isHolidayTheme) {
        $('.rate-tab-container').hide();
    }
}

function setDefaultRateFilerAfterRateFetch() {
    var selectDefaultRateFilter = getQueryParameter('rateTab');
    if(selectDefaultRateFilter) {
        switch (selectDefaultRateFilter)
        {
            case 'PKG':
                $("[data-rate-filter-code='PKG']:visible").trigger('click');
                break;
            case 'STD':
                $("[data-rate-filter-code='STD']:visible").trigger('click');
                break;
            case 'TIC':
                $("[data-rate-filter-code='TIC']:visible").trigger('click');
                break;
            case 'PROMOCODE':
                $("[data-promo-code]:visible").trigger('click');
                break;
        }
    }
}

function checkHolidayThemeAfterRateFetch() {
    if (isHolidayTheme) {
        $('.rate-tab-container').show();
        if ($('[data-offer-rate-code]').length > 0) {
            $('.rate-tab').not('[data-offer-rate-code]').hide();
        } else if ($("[data-rate-filter-code='PKG']:visible").length > 0) {
            $("[data-rate-filter-code='PKG']:visible").trigger('click');
            $('.rate-tab').not('[data-rate-filter-code="PKG"]').hide();
        } else {
            $('.rate-tab').not('[data-rate-filter-code="STD"]').hide();
        }
    }
}

function checkMemberTypeAfterRateFetch() {
    var member = getUserData();

    if (member) {
        if (member.card && member.card.type) {
            var memberType = member.card.type;
            if (memberType && (memberType === "TAP" || memberType === "TAPPMe" || memberType.indexOf('TIC') != -1)) {
                console.log("member logged in, removing unrelated tabs");
                $('[data-rate-filter-code="PKG"]').remove();
                $('[data-rate-filter-code="STD"]').remove();
                $('[data-rate-filter-code="TIC"]').remove();
            }
        }
    }
}

function checkTicFlow(roomContainer) {
    // [TIC-FLOW]
    var userDetails = getUserData();
    var rateTabInDisplay = getRateTabInDisplay();

    if (userDetails && userDetails.card && userDetails.card.tier
        && (rateTabInDisplay === 'TIC ROOM REDEMPTION RATES' || rateTabInDisplay === 'TAJ HOLIDAY PACKAGES')) {
        // Show/Hide of Room CARDS
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-symbol').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-value').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-highlight').addClass('hidden');
        // for TIC flow, we dont need striked Prices
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tic-points').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.epicure-points').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tic').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.epicure').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.slash').removeClass('hidden');

        // Show/Hide of Rate CARDS
        $(roomContainer).find('.rate-info-wrp').find('.rc-selected-currency').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rc-total-amount').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-asterik').addClass('hidden');

        $(roomContainer).find('.rate-info-wrp').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tic-points').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.epicure-points').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tic').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.epicure').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.slash').removeClass('hidden');

        $('.points-redeemption-msg').show();
        $('.rate-class-disclaimer').hide();
        $('.cm-page-container').addClass("tic-room-redemption-fix");

    } else if (userDetails && userDetails.card && userDetails.card.tier
        && rateTabInDisplay === 'TAP ROOM REDEMPTION RATES') {

        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-symbol').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-value').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-highlight').addClass('hidden');

        // for TAP flow, we don't need striked Prices
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tap-points').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tap').removeClass('hidden');

        // Show/Hide of Rate CARDS
        $(roomContainer).find('.rate-info-wrp').find('.rc-selected-currency').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rc-total-amount').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-asterik').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tap-points').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tap').removeClass('hidden');
        $('.points-redeemption-msg').remove();
        $('.cm-page-container').addClass("tic-room-redemption-fix");
    } else if (userDetails && userDetails.card && userDetails.card.tier
        && rateTabInDisplay === 'TAPPMe ROOM REDEMPTION RATES') {
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-symbol').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-value').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-highlight').addClass('hidden');

        // for TAP flow, we don't need striked Prices
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tappme-points').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tappme').removeClass('hidden');

        // Show/Hide of Rate CARDS
        $(roomContainer).find('.rate-info-wrp').find('.rc-selected-currency').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rc-total-amount').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-asterik').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-card-striked-rate').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tappme-points').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tappme').removeClass('hidden');
        $('.points-redeemption-msg').remove();
        $('.cm-page-container').addClass("tic-room-redemption-fix");
    } else {
        // Show/Hide of Room CARDS
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-symbol').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.current-currency-value').removeClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-highlight').removeClass('hidden');
        // for NON TIC flow, we need striked Prices
        $(roomContainer).find('.rate-card-actual-rate').find('.rate-card-striked-rate').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tic-points').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.epicure-points').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.tic').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.epicure').addClass('hidden');
        $(roomContainer).find('.rate-card-actual-rate').find('.slash').addClass('hidden');

        // Show/Hide of Rate CARDS
        $(roomContainer).find('.rate-info-wrp').find('.rc-selected-currency').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rc-total-amount').removeClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.rate-asterik').removeClass('hidden');

        $(roomContainer).find('.rate-info-wrp').find('.rate-card-striked-rate').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tic-points').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.epicure-points').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.tic').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.epicure').addClass('hidden');
        $(roomContainer).find('.rate-info-wrp').find('.slash').addClass('hidden');
        $('.cm-page-container').removeClass("tic-room-redemption-fix");
    }
}

// [TIC-FLOW]
function ticRoomListFlow() {
    // [TIC-FLOW]
    var userDetails = getUserData();
    var url = window.location.href;

    // [TIC-FLOW]
    // this is for TIC holidays flow
    var tajHolidayRedemption = false;
    if (url && url.indexOf('taj-holiday-redemption') != -1) {
        tajHolidayRedemption = true;
    }
    var globalBookingOption = dataCache.session.getData("bookingOptions");
    var userDetails = getUserData();
    if (globalBookingOption && userDetails && userDetails.card && userDetails.card.tier && tajHolidayRedemption) {
        globalBookingOption.flow = "taj-holiday-redemption";
        dataCache.session.setData("bookingOptions", globalBookingOption);
    } else {
        globalBookingOption.flow = "";
        dataCache.session.setData("bookingOptions", globalBookingOption);
    }

    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');

    globalBookingOption = dataCache.session.getData("bookingOptions");
    var ticRoomRedemptionObject = {};
    var isTicRoomRedemptionFlow = false;
    if (globalBookingOption && globalBookingOption.selection) {
        globalBookingOption.selection
            .forEach(function (item, index) {
                if (item.selectedFilterTitle
                    && ((item.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES')
                        || (item.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
                        || (item.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES') || (item.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
                    isTicRoomRedemptionFlow = true;
                    if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                        ticRoomRedemptionObject.currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                    }
                    $(".cm-page-container").addClass('tic-room-redemption-fix');
                }
            });
    }

    if ((url.indexOf('offerRateCode') != -1) || !userDetails) {
        $(".tic-room-redemption-tier").hide();
        $(".tic-room-redemption-rates").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier
        && globalBookingOption.flow === 'taj-holiday-redemption') {
        $('#member-rate-title').text("TAJ HOLIDAY PACKAGES");
        $(".tic-room-redemption-tier").removeClass('d-none')
        $(".tic-room-redemption-tier-silver").remove();
        $(".tic-room-redemption-tier-gold").remove();
        $(".tic-room-redemption-tier-copper").remove();
        $(".tic-room-redemption-tier-platinum").remove();
        $(".tic-room-redemption-tier-tap").remove();
        $(".tic-room-redemption-tier-tappme").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier === 'Copper') {
        $(".tic-room-redemption-tier").removeClass('d-none');
        $(".tic-room-redemption-tier-silver").remove();
        $(".tic-room-redemption-tier-gold").remove();
        $(".tic-room-redemption-tier-platinum").remove();
        $(".tic-room-redemption-tier-tap").remove();
        $(".tic-room-redemption-tier-tappme").remove();
        $(".tic-room-redemption-tier-hoilday-redemption").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier === 'Silver') {
        $(".tic-room-redemption-tier").removeClass('d-none')
        $(".tic-room-redemption-tier-copper").remove();
        $(".tic-room-redemption-tier-gold").remove();
        $(".tic-room-redemption-tier-platinum").remove();
        $(".tic-room-redemption-tier-tap").remove();
        $(".tic-room-redemption-tier-tappme").remove();
        $(".tic-room-redemption-tier-hoilday-redemption").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier === 'Gold') {
        $(".tic-room-redemption-tier").removeClass('d-none')
        $(".tic-room-redemption-tier-silver").remove();
        $(".tic-room-redemption-tier-copper").remove();
        $(".tic-room-redemption-tier-platinum").remove();
        $(".tic-room-redemption-tier-tap").remove();
        $(".tic-room-redemption-tier-tappme").remove();
        $(".tic-room-redemption-tier-hoilday-redemption").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier === 'Platinum') {
        $(".tic-room-redemption-tier").removeClass('d-none')
        $(".tic-room-redemption-tier-silver").remove();
        $(".tic-room-redemption-tier-gold").remove();
        $(".tic-room-redemption-tier-copper").remove();
        $(".tic-room-redemption-tier-tap").remove();
        $(".tic-room-redemption-tier-tappme").remove();
        $(".tic-room-redemption-tier-hoilday-redemption").remove();
    } else if (userDetails && userDetails.card && userDetails.card.tier === 'Blue') {
        if (userDetails && userDetails.card && userDetails.card.type === 'TAP') {
            $('#member-rate-title').text("TAP ROOM REDEMPTION RATES");
            $(".tic-room-redemption-tier").removeClass('d-none')
            $(".tic-room-redemption-tier-silver").remove();
            $(".tic-room-redemption-tier-gold").remove();
            $(".tic-room-redemption-tier-copper").remove();
            $(".tic-room-redemption-tier-platinum").remove();
            $(".tic-room-redemption-tier-tappme").remove();
            $(".tic-room-redemption-tier-hoilday-redemption").remove();
        } else {
            $('#member-rate-title').text("TAPPMe ROOM REDEMPTION RATES");
            $(".tic-room-redemption-tier").removeClass('d-none')
            $(".tic-room-redemption-tier-silver").remove();
            $(".tic-room-redemption-tier-gold").remove();
            $(".tic-room-redemption-tier-copper").remove();
            $(".tic-room-redemption-tier-platinum").remove()
            $(".tic-room-redemption-tier-tap").remove();
            $(".tic-room-redemption-tier-hoilday-redemption").remove();
        }
    } else {
        if(userDetails){
			$(".tic-room-redemption-tier").removeClass('d-none');
        } else {
        	$(".tic-room-redemption-tier").hide();
        }
        $(".tic-room-redemption-rates").remove();
    }

    ticRoomRedemptionObject.isTicRoomRedemptionFlow = isTicRoomRedemptionFlow;
    ticRoomRedemptionObject.selection = {};
    dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObject);

    getCurrencyConversion();
}

// [TIC-FLOW]
function userIsTicBased() {
    var userDetails = getUserData();
    if (userDetails && userDetails.card && userDetails.card.tier) {
        return true;
    }
    return false;
}

function getCurrencyConversion() {
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString === undefined) {
        $.ajax({
            type: 'GET',
            url: "/bin/getCurrencyRateConversion",
            success: function (data) {
                ticRoomRedemptionObjectSession.currencyRateConversionString = data;
                dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObjectSession);
            }
        });
    }
}

function disablePackageInTicRoomRedemption() {
    var rateTabInDisplay = getRateTabInDisplay();
    // [TIC-FLOW]
    var tabInDisplay = '';
    if (rateTabInDisplay) {
        tabInDisplay = rateTabInDisplay.replace(/ /g, "-");
        $('.rate-cards-container').attr('class', 'rate-cards-container cm-content-blocks');
        $('.rate-cards-container').addClass(tabInDisplay);
    }
    var userDetails = getUserData();
    var url = window.location.href;
    var ticRoomRateSelected = false;
    var otherRateSelected = false;
    var selectedRateTitle = "";
    if (userDetails && userDetails.card && userDetails.card.tier && tabInDisplay.length > 0) {
        var globalBookingOption = dataCache.session.getData("bookingOptions");
        globalBookingOption.selection.forEach(function (item, index) {
            if (item.selectedFilterTitle && ((item.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES')
                || (item.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
                || (item.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES')
                || (item.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
                ticRoomRateSelected = true;
                selectedRateTitle = item.selectedFilterTitle.replace(/ /g, "-");
            } else {
                // for other flow , we are using selectedFilterCode instead of selectedFilterTitle
                otherRateSelected = true;
                selectedRateTitle = item.selectedFilterCode.replace(/ /g, "-");
            }
        });
        if (url.indexOf('offerRateCode') != -1) {
            $('.' + tabInDisplay + ' .more-rates-select-btn').prop('disabled', false);
            $('.' + tabInDisplay + ' .more-rates-select-btn').css('opacity', '1');
        } else if (ticRoomRateSelected && tabInDisplay != selectedRateTitle) {
            $('.' + tabInDisplay + ' .more-rates-select-btn').prop("disabled", true);
            $('.' + tabInDisplay + ' .more-rates-select-btn').css('opacity', '0.5');
        } else if (otherRateSelected && ((tabInDisplay === 'TIC-ROOM-REDEMPTION-RATES')
            || (tabInDisplay === 'TAP-ROOM-REDEMPTION-RATES')
            || (tabInDisplay === 'TAPPMe-ROOM-REDEMPTION-RATES')
            || (tabInDisplay === 'TAJ-HOLIDAY-PACKAGES'))) {
            $('.' + tabInDisplay + ' .more-rates-select-btn').prop('disabled', true);
            $('.' + tabInDisplay + ' .more-rates-select-btn').css('opacity', '0.5');
        } else {
            $('.' + tabInDisplay + ' .more-rates-select-btn').prop('disabled', false);
            $('.' + tabInDisplay + ' .more-rates-select-btn').css('opacity', '1');
        }
    }
}

function showSoldOutMessage(roomOccupancyRates) {
    var rateCodeLength = 0;
    var rateFiltersLength = 0;
    var promoCodeLength = 0;
    var zeroRoomsAvailabilityCount = 0;
    var hotelCode = getHotelCode();
    var roomCount = 0;
    for (var data in roomOccupancyRates.rateCodes) {
        rateCodeLength++;
        if (roomOccupancyRates.rateCodes[data].roomsAvailability) {
            roomCount = 0;
            for (var room in roomOccupancyRates.rateCodes[data].roomsAvailability) {
                roomCount++;
            }
            if (roomCount === 0) {
                zeroRoomsAvailabilityCount++;
            }
        }
    }

    for (var data in roomOccupancyRates.rateFilters) {
        rateFiltersLength++;
        if (roomOccupancyRates.rateFilters[data][hotelCode]
            && roomOccupancyRates.rateFilters[data][hotelCode].roomsAvailability) {
            roomCount = 0;
            for (var room in roomOccupancyRates.rateFilters[data][hotelCode].roomsAvailability) {
                roomCount++;
            }
            if (roomCount === 0) {
                zeroRoomsAvailabilityCount++;
            }
        }
    }
    
    for (var data in roomOccupancyRates.promoCode) {
        promoCodeLength++;
        if (roomOccupancyRates.promoCode[data].roomsAvailability) {
            roomCount = 0;
            for (var room in roomOccupancyRates.promoCode[data].roomsAvailability) {
                roomCount++;
            }
            if (roomCount === 0) {
                zeroRoomsAvailabilityCount++;
            }
        }
    }

    var totalFilters = rateCodeLength + rateFiltersLength + promoCodeLength;
    if($(".hotels-under-destination").hasClass('empty')){
        $(".hotels-under-destination").removeClass('empty');
        allHotelsUnderDestination(zeroRoomsAvailabilityCount, totalFilters);
    }
}

function allHotelsUnderDestination(zeroRoomsAvailabilityCount, totalFilters) {

    var urlPathName = window.location.pathname;
    var requestData = {
        url: urlPathName
    };

    $
        .ajax({
            type: "GET",
            url: "/bin/hotelsUnderDestination",
            data: requestData,
            contentType: "application/json"
        })
        .done(
            function (res) {
                $
                    .each(
                        res,
                        function (i, item) {
                            if (item.destinationName) {
                                $(".destination-name").append(item.destinationName);
                            } else if (item.baseHotelName) {
                                $(".hotel-name").append(item.baseHotelName);
                            } else {
                                adjacentHotelCount++;
                                var redirectUrl = "";
                                if(item.hotelShortUrl){
                                    redirectUrl = item.hotelShortUrl;
                                }else if (item.brand === 'seleqtions') {
                                    redirectUrl = "/en-in/" + item.hotelNodeName+"/rooms-and-suites";
                                } else {
                                    redirectUrl = "/en-in/" + item.brand + '/' + item.hotelNodeName+"/rooms-and-suites";
                                }
								if(item.hotelName && item.hotelNodeName) {
									var html = '<div class="row"><div class="col-md-2 hotel-img-wrp"> <img alt = "hotel image" src="'
										+ item.imagePath
										+ '" /> </div> <div class="col-md-10  details-wrp"> <span class="hotel-title">'
										+ item.hotelName
										+ '</span> <div class="hotel-details"><span class="hotel-area">'
										+ item.hotelArea
										+ ' '
										+ item.hotelCity
										+ ' '
										+ item.hotelPinCode
										+ '</span></div><div class="hotel-link-btn-wrp"><a href="'
										+ redirectUrl
										+ '"class="hotel-links cm-btn-secondary">View Hotel</a></div></div></div>';
									// console.log(item);
									$(".hotels-under-destination").append(html);
								}
                            }
                        });

                if (zeroRoomsAvailabilityCount === totalFilters && adjacentHotelCount > 0) {
                    $('#sold-out').addClass('active');
                }

            }).fail(function () {
    });
}

function roomFirstClick(){
    $(".ama-theme .cm-room-options .rate-card-wrap:first-child .more-rates-button").trigger('click');
}
function checkInCheckOutTime(){
    var checkInTime = $(".check-in-time").val();
    var checkOutTime =$(".check-out-time").val();
    var bookDetails =dataCache.session["data"];
    bookDetails.checkInTime =checkInTime;
    bookDetails.checkOutTime=checkOutTime;
    dataCache.session.setData(bookDetails); 
}

function roomDetailsShowHide() {
    try{
    if($('.cm-page-container').hasClass('ama-theme')){
        $('.rate-card-disclaimer').text('Excluding taxes and Fees');
        $('.last-few-room-text').addClass('last-few-rooms-text');
        $('.last-few-rooms-text').removeClass('last-few-room-text');
        $('.last-few-rooms-text').text('Excluding taxes and Fees');
        $('.last-few-rooms-icon').hide();
    }
    }catch(err){
        console.error('caught exception in roomDetailsShowHide function');
        console.error(err);
    }
}

function checkLoginWithMemberRate() {
    $('.rate-tab').on('click', function() {
		getRateTabCode();
    });
}

function getRateTabCode() {
    var selectedTab = $('.rate-tab.tab-selected');
    rateTabCode = selectedTab.attr("data-rate-filter-code");
    var memberOnlyOffer = dataCache.session.getData("memberOnlyOffer");
    if(!rateTabCode && selectedTab.attr("data-offer-rate-code") && memberOnlyOffer && (memberOnlyOffer == "true")){
        rateTabCode = "TIC";
    }
    console.log('rateTabCode', rateTabCode);
}

// Check for voucher redemptoin parameter in query based on that will only display offer tab
function getQueryVoucherRedemption() {
	return dataCache.session.getData('voucherRedemption');
}


// Handle Voucher Redemption price 
function makeAvailabilityAsPerVoucher(promoCodeInfo) {
    var promoCodeResp = promoCodeInfo[getHotelCode()];
    promoCodeResp.lowestPrice = 0;
	promoCodeResp.lowestDiscountedPrice = 0;
    for(var key in promoCodeResp.roomsAvailability){
        promoCodeResp.roomsAvailability[key].lowestPrice = 0;
		promoCodeResp.roomsAvailability[key].lowestDiscountedPrice = 0;
        var roomsArray = promoCodeResp.roomsAvailability[key].rooms;
        if(roomsArray && roomsArray.length){
            for(var i=0; i<roomsArray.length; i++){
                roomsArray[i].totalPrice = 0;
                roomsArray[i].price = 0;
				roomsArray[i].totalDiscountedPrice = 0;
				roomsArray[i].discountedPrice = 0;
                var nightlyRates = roomsArray[i].nightlyRates;
                if(nightlyRates && nightlyRates.length){
                    for(var j=0; j<nightlyRates.length; j++){
                        nightlyRates[j].priceWithFeeAndTax = nightlyRates[j].tax;
                        nightlyRates[j].price = 0;
                    }
                	roomsArray[i].nightlyRates = nightlyRates;
                }
            }
        	promoCodeResp.roomsAvailability[key].rooms = roomsArray;
        }
    }
    promoCodeInfo[getHotelCode()] = promoCodeResp;
    return promoCodeInfo;
}


function textChangesForVoucherRedemotion() {
    $("[data-injector-key='lowest-discounted-price']:visible").each(function(){
		$(this).hide();
	});
    $('.rate-info-label').each(function(){
		$(this).text('Taxes Extra');
    });
    $('.rate-highlight:visible').each(function(){
        $(this).hide();
    });
    $('.rate-card-text-container:visible').each(function(){
        $(this).text('Taxes Extra');
    });
    $('.rate-asterik').each(function(){
		$(this).hide();
    });
    $("[data-injector-key='discounted-price']").each(function(){
		$(this).hide();
	});
    $('[data-injector-key="currency-symbol"]:visible').each(function(){
		$(this).hide();
    });
    $('#redeem-voucher').each(function(){
		$(this).removeClass('d-none');
    });
    $('[data-injector-key="currency-symbol"]').each(function(){
		$(this).hide();
	});
    $('#redeem-voucher-room').each(function(){
		$(this).removeClass('d-none');
	});
    $('.per-night-text:visible').hide();
}