$(document).ready(
        function() {
            var elServicesCollapsed = $('.ho-services-collapsed');
            var elServicesExpanded = $('.ho-services-expanded');
            var btnShowSm = $('.ho-services-sm-show-all');
            var btnHideSm = $('.ho-services-sm-show-less');
    $('.carousel-image-holder').css('cursor', 'pointer'); /*Changing the mouse pointer while hovering over the image*/

    $('.carousel-image-holder').click(function(){

        var imageJsonArray=[];
        var rearrangedJSONArray=[];
        var imageHolderDom =null;
        var indexOfSelectedImage = $('.carousel-image-holder').index(this);
        imageHolderDom = $($.find("[data-gallery-images]")).clone();
        imageJsonArray = $(imageHolderDom).data('gallery-images');
        var imageJsonArrayCopy = JSON.parse(JSON.stringify(imageJsonArray)); /*Avoiding shallow-copy issue*/
        var rearrangedJSONArray=reindexGalleryArray(imageJsonArrayCopy,indexOfSelectedImage,imageJsonArrayCopy.length);
        $(imageHolderDom).data('gallery-images',rearrangedJSONArray);
        showGallery(imageHolderDom);
    });

    /*Accepts JSON array object and an index, based on index value, JSON array Object is re-ordered where objects are clockwise left rotated ,index number of times */
    function reindexGalleryArray(arr,index,arrSize){
        for(var i=0;i<index;i++) {
            arr=leftShiftByOnePosition(arr,arrSize);
        }
        return arr;
    }

    function leftShiftByOnePosition(arr,arrSize) {
        var i;
        var temp;
        temp=arr[0];
        for(i=0;i<arrSize-1;i++){
            arr[i] = arr[i + 1];
        }
        arr[i] = temp;
        return arr;
    }

            function toggleServices() {
                $(".ho-each-items").each(function(index) {
                    if (index > 4) {
                        $(this).toggle();
                    }
                });
            }

            $('.ho-description-txt').cmToggleText({
                charLimit : 200,
                showVal : "Show More",
                hideVal : "Show Less",
            });

            function expandServices() {
                if (elServicesExpanded.hasClass('cm-hide')) {
                    setTimeout(function() {
                        btnShowSm.addClass('cm-hide');
                        btnHideSm.removeClass('cm-hide');
                        elServicesCollapsed.addClass('cm-hide');
                        elServicesExpanded.removeClass('cm-hide');
                    }, 100);
                }
            }
            ;

            function collapseServices() {
                if (elServicesCollapsed.hasClass('cm-hide')) {
                    setTimeout(function() {
                        btnShowSm.removeClass('cm-hide');
                        btnHideSm.addClass('cm-hide');
                        elServicesCollapsed.removeClass('cm-hide');
                        elServicesExpanded.addClass('cm-hide');
                    }, 100);
                }
            }
            ;

            $('.ho-services-show-all-con').on('click', function() {
                expandServices();
                // toggleServices();

                if ($(this).hasClass('is-active')) {
                    $(this).removeClass('is-active');
                } else {
                    $(this).addClass('is-active');
                }
            });

            $('.ho-services-show-less').on('click', function() {
                collapseServices();
            });

            btnShowSm.on('click', function() {
                expandServices();
            });

    btnHideSm.on('click', function() {
        collapseServices();
    });
    
    //Added by Nixon - Do not remove during conflict resolve 
    $(".ho-photo-carousel-link").on('click', function() {
        //$('body').css('overflow', 'hidden');
        var presentScroll = $(window).scrollTop();
        $('.cm-page-container').addClass("prevent-page-scroll");
        $('.icon-close.mr-carousel-close-icon').on('click', function() {
            //$('body').css('overflow', 'auto');
            $('.cm-page-container').removeClass("prevent-page-scroll");
            $(window).scrollTop(presentScroll);
        });
    });


    if(deviceDetector.checkDevice() != "large") {
        $('.img-section-lg').remove();
    } else {
        $(".overview-images-section.hide-in-lg").remove();
    }
    
    //----/Nixon----//

            // For Hotel carousel
            $('.ho-photo-carousel-link, .ho-view-gallery-txt').on(
                    'click',
                    function() {
                        $('.hotel-overview-container').find('.mr-menu-carousel-overlay').removeClass(
                                'mr-overlay-initial-none');
                    });
            // toggleServices();
            setGallerySectionHeight();
            $('[onclick]').each(function() {
                var handler = $(this).prop('onclick');
                $(this).removeProp('onclick');
                $(this).click(handler);
            });
            socialShare();



            displayShowMoreShowLess();

            expandCollapseFeatures();

            // Analytic call
            // prepareGlobalHotelListJS()
            if(!$('.cm-page-container').hasClass('ama-theme')){
            	pushEvent("hotelsselect", "hotelsselected", prepareGlobalHotelListJS())
           }
        });

function socialShare() {

    var url = window.location.href;
    var text = $('.share-btn-wrapper').attr("data-share-text");

    $("#socials").jsSocials({
        url : url,
        text : text,
        showCount : false,
        showLabel : false,
        shares : [ {
            share : "facebook",
            logo : "/content/dam/tajhotels/icons/social-icons/ic-facebook@2x.png"
        }, {
            share : "twitter",
            logo : "/content/dam/tajhotels/icons/style-icons/twitter.svg"
        }, {
            share : "googleplus",
            logo : "/content/dam/tajhotels/icons/social-icons/google+.png"
        } ]

    })

}

function showAllAmenitiesClickHandler() {
    var presentScroll = $(window).scrollTop();
    $('.all-amenities-details-wrap').show();
    $('.hotel-amenities-link').css('display', 'block');
    $('.cm-page-container').addClass("prevent-page-scroll");
    $('.all-amenities-details-close').click(function() {
        $('.all-amenities-details-wrap').hide();
        $('.cm-page-container').removeClass("prevent-page-scroll");
        $(window).scrollTop(presentScroll);
    });
}

function setShowCtaVisibility(count) {
    var elEachFeaturesBlock = $(".ho-each-ameneties-sec");
    var elTriggerCta = $('.show-more-less-cta-con');
    if (elEachFeaturesBlock.length > count) {
        elTriggerCta.show();
    } else {
        elTriggerCta.hide();
    }
}

function displayShowMoreShowLess() {
    if (deviceDetector.checkDevice() == 'small') {
        setShowCtaVisibility(2);
    } else {
        setShowCtaVisibility(4);
    }
}

function expandCollapseFeatures() {
    var elTarget = $('.cta-selling-sec');
    var elFeaturesCon = $('.ho-ameneties-blocks-con');

    elTarget.on('click', function() {
        elTarget.show();
        $(this).hide();

        if ($(this).hasClass('cta-sellings-show-more')) {
            elFeaturesCon.addClass('active');
        } else {
            elFeaturesCon.removeClass('active');
        }
    });

}
