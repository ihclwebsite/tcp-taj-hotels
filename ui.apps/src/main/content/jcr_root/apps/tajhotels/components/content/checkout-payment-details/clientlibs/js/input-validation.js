$( document ).ready( function() {
    // numeric input validation
	//alert("Input Validation");
    $( '.only-numeric-input' ).keypress( function( e ) {
        if ( String.fromCharCode( e.keyCode ).match( /[^0-9]/g ) ) {
            return false;
        }
    } );

    // aplhabet input validation
    $( '.only-alpha-input' ).keypress( function( e ) {
        if ( String.fromCharCode( e.keyCode ).match( /[^a-zA-Z ]/g ) ) {
            return false;
        }
    } );

    // email validation
    function isEmail( email ) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test( email );
    }

    $( '.email-input' ).blur( function() {
        var email = $( this ).val();
        if ( !isEmail( email ) ) {
            $( this ).addClass( 'invalid-input' );
        } else {
            $( this ).removeClass( 'invalid-input' );
        }
    } );

    // Condition to toggle invalid-input based on value

    $( '.sub-form-mandatory' ).not( '.email-input' ).blur( function() {
        if ( $( this ).val() != "" ) {
            $( this ).removeClass( 'invalid-input' );
        } else {
            $( this )
                .addClass( 'invalid-input' );
        }
    } );

    // Focusing input
    $('.sub-form-input-element').on('focus',function(){        
        this.focus({preventScroll:false});
    });
} );