var globalSelectedDates=[];
var cardsDomArray;
$(document).ready(function(){
	cardsDomArray =$.find("[data-title]");
	$('#filter-search').on('click', function() {
        globalSelectedDates=[];
		var selectDates =getSelectedDatesFromDom();
        resetCardDoms(cardsDomArray);

        splitDates(selectDates);
        if(globalSelectedDates.length == 0) {
			resetCardDoms(cardsDomArray);
        }
        else {
            filterCardsUsingDates(cardsDomArray,globalSelectedDates);
        }
    });
     $(document).on('multiselect:reset', function(e) {
      resetCardDoms(cardsDomArray);
   });
});

function splitDates(datesArray) {
    for(i in datesArray) {
        var dates=[];
        var temp=datesArray[i].split('-');
        for (j in temp){
			if($.inArray(temp[j],globalSelectedDates) == -1){
					globalSelectedDates.push(temp[j]);
            }
        }
    }

}

function filterCardsUsingDates(allCardsDomArray,selectedDatesArray) {
    for (i in allCardsDomArray) {
        var dom = allCardsDomArray[i];
        var title = ($(dom).data()).title;
        var flag=false;
        for (j in selectedDatesArray) {

            if(title.includes(selectedDatesArray[j])){
                flag=true;
            }
        }
        if(flag===false){
            $(dom).hide();
        }
    }
}

function resetCardDoms(allCardsDomArray) {
	 for (i in allCardsDomArray) {
		var dom = allCardsDomArray[i];
         $(dom).show();
     }
}





