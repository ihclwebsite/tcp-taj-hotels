use(function() {
    if (this.reqUrl) {
        response = request.resourceResolver.map(this.reqUrl)
    } else {
        response = this.reqUrl
    }

    return {
        mappedUrl : response
    };
});
