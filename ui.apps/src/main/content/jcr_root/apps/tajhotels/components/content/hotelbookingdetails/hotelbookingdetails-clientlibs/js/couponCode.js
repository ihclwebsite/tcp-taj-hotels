$( document ).ready( function() {
	/*var couponCodesList = getCouponCodeFromData();
	if(JSON.parse(couponCodesList).length == 0){
		dataCache.session.setData( "couponCodes" , JSON.parse(couponCodesList) );
	}*/
	setCouponCodeToCache();
} );

function getCouponCodeFromData() {
    var couponCodeList = $( $.find( "[data-coupon-code]" )[ 0 ] ).data();
    if ( couponCodeList ) {
        return couponCodeList.couponCode.couponCodes;
    }
    return null;
}

function setCouponCodeToCache(){
	var couponCodeList = getCouponCodeFromData();
	if(couponCodeList){
		dataCache.session.setData( "couponCodes" , couponCodeList );
	}
}
