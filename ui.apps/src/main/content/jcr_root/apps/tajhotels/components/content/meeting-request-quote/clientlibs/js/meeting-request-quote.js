$(document).ready(function() {
    if (window.location.search.substr(1).includes('hotelName')) {
        $('#searchInputEvents').val('');
        $('#searchInputEvents').removeAttr('readonly');
    }
    $('#event-quote-date-value').removeAttr('disabled');
    roomSpecificRequestQuote();

});

function roomSpecificRequestQuote() {
    var venueDetail;
    var requestDetailsSessionObj = dataCache.session.getData("requestQuoteDetails");

    var hotelName = "";
    var venueName = "";
    var requestQuoteMailId = "";
    var adminEmail = $('#adminEmail').val();
    if (requestDetailsSessionObj) {
        // if in same domain session datas are picked and set in the Request Quote Form
        hotelName = requestDetailsSessionObj.hotelName;
        venueName = requestDetailsSessionObj.venueName;
        requestQuoteMailId = requestDetailsSessionObj.requestQuoteEmailId;
    } else {
        // cross-sell logic hotelName,VenueName and requestQuoteMailId picked from URL parameters
        hotelName = decodeURIComponent(getQueryParameter('hotelName'));
        venueName = decodeURIComponent(getQueryParameter('venueName'));
        requestQuoteMailId = decodeURIComponent(getQueryParameter('requestQuoteMailId'));
    }
    // var maxCapacity = decodeURI(getParam('maxCapacity'));
    if (venueName != null && venueName != '') {
        venueDetail = hotelName + " - " + venueName;
    } else {
        venueDetail = hotelName;
    }
    $('.venue-search-input.sub-form-input-element.sub-form-mandatory').val(venueDetail);
    // $('.limit-no-of-guest').attr('max', maxCapacity);
    // analytics code
    var jsonObj = {};
    jsonObj.eventStartDate = "";
    jsonObj.eventEndDate = "";
    jsonObj.venueName = getParam('venueName');

    jsonObj.guestCount = "";
    jsonObj.hotelName = dataLayerData.hotelName;
    jsonObj.guestEmailId = "";
    jsonObj.phoneNumber = "";
    jsonObj.guestName = "";

    prepareJsonForEvents("Venue Selected", jsonObj);

    var SEARCH_INPUT_DEBOUNCE_RATE = 3000;
    var jivaDateWidth = $('#meeting-events-date').outerWidth();
    var validateRequestQuoteElements = function() {
        $('.event-request-quote-form .sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
            } else {
                $(this).removeClass('invalid-input');
            }
        });
    };

    // Request quote submit handler
    $('.request-quote-submit-btn').click(function() {
        validateRequestQuoteElements();
        if ($('.event-request-quote-form .sub-form-input-element').hasClass('invalid-input')) {
            $('.event-request-quote-form .invalid-input').first().focus();
        } else {

            requestBooking();
        }
    });

    // datepicker script
    var today = new Date();

    $('#meeting-events-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            // Added by Dinesh for Event quote datepicker
            // var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' +
            // minDate.getFullYear());
            var selectedDate = moment(minDate).format("DD MMM YYYY");
            $('.event-quote-date-value').val(selectedDate).removeClass('invalid-input');
            $('.jiva-spa-date-value').text(selectedDate);
            $('.jiva-spa-date').removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
        }
    });

    $('#meeting-events-date').datepicker('setDate', new Date());

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $('.jiva-spa-date').addClass('visible');
    });

    $('body').click(function(e) {
        $('.jiva-spa-date').removeClass('visible');
    });

    $('.hotels-event-confirmation-overlay, .event-confirmation-close-icon, .event-confirmation-close-btn').click(
            function() {
                $('.hotels-event-confirmation-popup').hide();
            });
    $('.hotels-event-confirmation-inner-wrp').click(function(e) {
        e.stopPropagation();
    });

    $('.cm-page-container').blur(function() {
        $('.jiva-spa-date').removeClass('visible');
    });

    /*
     * function clearSearchResults() { $('.meeting-events-search-list').empty(); }
     * 
     * function performSearch(key) { return $.ajax({ method : "GET", url : "/bin/search", data : { searchText : key }
     * }).done(function(res) { clearSearchResults(); addHotelSearchResults(res.hotels); }).fail(function() {
     * clearSearchResults(); }); }
     * 
     * function addHotelSearchResults(hotels) { if (hotels.length) { hotels.forEach(function(hotel) {
     * 
     * $('.meeting-events-search-list').append('<li ><label>' + hotel.title + '</label></li>'); }); } }
     */
    $('.event-search-close').click(function() {
        $(this).removeClass('visible');
        $(".venue-search-input-wrp > input").attr("placeholder", "Search");
    });

    $(document).on("click", function(e) {
        $('.meeting-events-search-list').css('display', 'none');
    });

    function requestBooking(key) {

        var sessionData = dataCache.session.getData("requestQuoteDetails");
        $('.pg-spinner-con').css('display', "block");

        var hotelName = "";
        var venueName = "";
        var requestQuoteMailId = "";

        if (requestDetailsSessionObj) {
            hotelName = requestDetailsSessionObj.hotelName;
            venueName = requestDetailsSessionObj.venueName;
            requestQuoteMailId = requestDetailsSessionObj.requestQuoteEmailId;
        }

		console.log("request quote email", pageLevelData.requestQuoteEmailId);
		
		
        if (null == requestQuoteMailId || requestQuoteMailId == "") {
            //requestQuoteMailId = pageLevelData.hotelEmailId;
			requestQuoteMailId = pageLevelData.requestQuoteEmailId || pageLevelData.hotelEmailId;
        }

        var eventQuoteDateValue = $('#event-quote-date-value').val();
        var bookingDate = moment(eventQuoteDateValue, "DD MMM YYYY").format("DD/MM/YYYY");
        if (!requestQuoteMailId) {
            requestQuoteMailId = "";
        }
        if (adminEmail) {
            requestQuoteMailId = requestQuoteMailId.concat("," + adminEmail);
        }
        var guest = $('#meeting-events-guest').val();

        var name = $('#customer-name').val();

        var phoneNumber = $('#phone-Number').val();

        var guestEmailId = $('#customer-email-id').val();

        var purposeOfTheEvent = $('#event-purposeSelectBoxItText').text();

        var timeOfTheEvent = $('#event-time-slotSelectBoxItText').text();

        var requestQuoteConfLink = $('.request-quote-conf-link').val() // + '?hotelWas='
        // + JSON.stringify(pageLevelData);

        var requestQuoteErrorLink = $('.request-quote-error-link').val();

        var requestString = "hotelName=" + hotelName + "&hotelEmailId=" + requestQuoteMailId + "&bookingDate="
                + bookingDate + "&noOfGuests=" + guest + "&name=" + name + "&phoneNumber=" + phoneNumber
                + "&guestEmailId=" + guestEmailId + "&venueName=" + venueName + "&timeOfTheEvent=" + timeOfTheEvent
                + "&purposeOfTheEvent=" + purposeOfTheEvent

        var meetingRoomRequest = prepareBookingData(hotelName, bookingDate, guest);
        tigger_meeting_quote(meetingRoomRequest)

        // analytics code
        var jsonObj = {};
        jsonObj.eventStartDate = bookingDate;
        jsonObj.eventEndDate = "";
        jsonObj.venueName = venueName;
        jsonObj.guestCount = guest;
        jsonObj.hotelName = hotelName;
        jsonObj.guestEmailId = guestEmailId;
        jsonObj.phoneNumber = phoneNumber;
        jsonObj.guestName = name;
        jsonObj.timeOfTheEvent = timeOfTheEvent;
        jsonObj.purposeOfTheEvent = purposeOfTheEvent;
        prepareJsonForEvents("Request for a Quote", jsonObj);

        return $.ajax({
            method : "GET",
            url : "/bin/requestQuote",
            data : requestString
        }).done(function(res) {
            console.info("ajax call success");
            window.location.assign(requestQuoteConfLink);
        }).fail(function() {
            console.error("ajax call fail function.");
            window.location.assign(requestQuoteErrorLink);
            dataCache.session.setData("confStatus", "Error");
        });
    }

    function prepareBookingData(hotelName, bookingDate, guest) {
        var jivaBookingData = {};
        jivaBookingData.hotelName = hotelName;
        jivaBookingData.bookingDate = bookingDate;
        jivaBookingData.numOfGuests = guest;
        return jivaBookingData;
    }

    function getParam(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return results[1];
    }
}

// analytics code
function prepareJsonForEvents(stepName, jsonObj) {

    var meetingEventJson = {};
    var pageHirarchy = [];

    if (stepName != undefined) {
        meetingEventJson.pageTitle = stepName;
        if (jsonObj != undefined) {
            jsonObj.venueName = decodeURI(jsonObj.venueName)
            meetingEventJson.hotelOfferingName = jsonObj.venueName;
            meetingEventJson.hotelName = jsonObj.hotelName;
            meetingEventJson.hotelOffering = stepName;
            meetingEventJson.eventType = "";
            meetingEventJson.eventStartDate = jsonObj.eventStartDate;
            meetingEventJson.eventEndDate = jsonObj.eventEndDate;
            meetingEventJson.venueBreakoutRooms = "";
            meetingEventJson.venueCity = dataLayerData.hotelCity;
            meetingEventJson.venueCountry = dataLayerData.hotelCountry;
            meetingEventJson.venueEvent = jsonObj.venueName;
            meetingEventJson.venueGuests = jsonObj.guestCount;
            meetingEventJson.timeOfTheEvent = jsonObj.timeOfTheEvent;
            meetingEventJson.purposeOfTheEvent = jsonObj.purposeOfTheEvent;
            meetingEventJson.venueSleepingRooms = "";

        }

        if (dataLayerData != undefined) {
            meetingEventJson.pageAuthor = dataLayerData.pageAuthor;
            meetingEventJson.pageLanguage = dataLayerData.pageLanguage;

            pageHirarchy = dataLayerData.pageHierarchy;

            if (pageHirarchy != undefined) {
                if (stepName == "Venue Selected") {
                    pageHirarchy.push("Venue Finder");
                }
            }
            if (stepName == "Request for a Quote") {
                pageHirarchy.push("Request for a Quote");
            }
            meetingEventJson.pageHirarchy = pageHirarchy;

            meetingEventJson.hotelCountry = dataLayerData.hotelCountry;
            meetingEventJson.hotelCode = dataLayerData.hotelCode;
            meetingEventJson.hotelCity = dataLayerData.hotelCity;
        }
    }

    pushHotelJson(meetingEventJson);
}
