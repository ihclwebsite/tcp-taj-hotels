$(document).ready(function() {
    $('.mr-menu-carousel-overlay').addClass('mr-overlay-initial-none');
    $('.mr-carousel-close-icon-wrap').click(function() {
        $('.mr-menu-carousel-overlay').addClass('mr-overlay-initial-none');
        $('.cm-page-container').removeClass("prevent-page-scroll");
    });
});

function showGallery(obj) {
    var presentScroll = $(window).scrollTop();
    var imagesList = $(obj).data("gallery-images");
    var totalImages = imagesList.length;
    var carouselDom = findCarouselOverlayDom();
    insertImagesToDom(imagesList, carouselDom);
    insertIndicatorsToDom(imagesList.length, carouselDom);
    displayCarouselOverlayDomFunc(carouselDom, totalImages);
    $('.cm-page-container').addClass("prevent-page-scroll");
    $('.mr-carousel-close-icon-wrap').click(function() {
        $('.mr-menu-carousel-overlay').addClass('mr-overlay-initial-none');
        $('.cm-page-container').removeClass("prevent-page-scroll");
        $(window).scrollTop(presentScroll);
    });
}

function insertIndicatorsToDom(numOfIndicators, carouselDom) {
    var indicatorsList = $(carouselDom).find("ol.carousel-indicators");
    var existingListItems = $(indicatorsList).find("li");
    $(existingListItems).remove();
    for (var i = 0; i < numOfIndicators; i++) {
        var listItem = constructIndicatorListItem(i);
        $(indicatorsList).append(listItem);
    }
}

function constructIndicatorListItem(index) {
    var item = $("<li>");
    $(item).attr("data-target", "#mr-menu-carousel");
    $(item).attr("data-slide-to", index + "");
    return item;
}

function displayCarouselOverlayDomFunc(carouselOverlayDom, totalImages) {
    setActiveImage(carouselOverlayDom);
    setActiveIndicator(carouselOverlayDom);
    hookCarousel(totalImages);
    $(carouselOverlayDom).removeClass('mr-overlay-initial-none');
}

function setActiveIndicator(carouselOverlayDom) {
    var indicatorList = $(carouselOverlayDom).find("[data-target='#mr-menu-carousel']");
    $(indicatorList).removeClass('active');
    $(indicatorList).first().addClass('active');
}

function setActiveImage(carouselOverlayDom) {
    $(carouselOverlayDom).find(".carousel-item").removeClass('active');
    $(carouselOverlayDom).find(".carousel-item").first().addClass('active');
}

function findCarouselOverlayDom() {
    var carouselContainerDom = $('.gallery-carousel-container');
    if ($(carouselContainerDom.length != undefined)) {
        carouselContainerDom = $(carouselContainerDom).get(0);
    }
    var carouselDom = $(carouselContainerDom).find('.mr-menu-carousel-overlay');
    return carouselDom;
}

function insertImagesToDom(imagesArray, carouselDom) {
    var carouselInnerWrap = $(carouselDom).find('.carousel-inner');
    var existingCarouselItemDom = $(carouselDom).find('.carousel-item');
    $(existingCarouselItemDom).remove();
    for ( var i in imagesArray) {
        var imageJson = imagesArray[i];

        var carouselItemDom = $("<div>");
        $(carouselItemDom).addClass("carousel-item");

        var titleDom = $("<div>");
        $(titleDom).addClass("mr-carousel-title-wrap");
        $(titleDom).text(imageJson["imageTitle"]);
        $(carouselItemDom).append(titleDom);

        var descriptionDom = $("<div>");
        $(descriptionDom).addClass("mr-carousel-Content-wrap");
        $(descriptionDom).text(imageJson["imageDescription"]);
        $(carouselItemDom).append(descriptionDom);

        var carouselImageWrap = $("<div>");
        $(carouselImageWrap).addClass("mr-carousel-images-wrap")

        $(carouselItemDom).append(carouselImageWrap);

        var imageDom = $("<img>");
        $(imageDom).addClass("mr-overlay-carousel-images");
        var imageFormat = getImageExtension(imageJson["imagePath"]);
        var renditionPath = '/jcr:content/renditions/cq5dam.web.756.756.' + imageFormat;
        $(imageDom).attr("src", imageJson["imagePath"] + renditionPath);
        $(carouselImageWrap).append(imageDom);

        $(carouselInnerWrap).append(carouselItemDom);
    }
}

function getImageExtension(imgPath) {
    var extension = imgPath.trim().split('.').pop().toLowerCase();
    return extension != 'png' ? 'jpeg' : 'png';
}

function hookCarousel(totalImages) {
    var carouselDom = $('#mr-menu-carousel.carousel');
    $(carouselDom).carousel();

    $('.mr-carousel-close-icon-wrap').on('click', function() {
        $('.mr-menu-carousel-overlay').addClass('mr-overlay-initial-none');
        $('.cm-page-container').removeClass("prevent-page-scroll");
    });

    var carouselCount = totalImages, currentCarousel = 0;
    $('.mr-currentCarousel').text('1');
    $('.mr-carouselTotalCount').text(totalImages);
    $('#mr-menu-carousel.carousel').on('slid.bs.carousel', function(e) {
        $('.mr-currentCarousel').text(($('.mr-menu-carousel-indicators ol li.active').data('slideTo')) + 1);
    });
}
