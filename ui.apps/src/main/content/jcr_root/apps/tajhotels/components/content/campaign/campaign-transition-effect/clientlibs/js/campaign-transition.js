$(document).ready(function() {

    if($('.transition-enabled')){
		$(window).on('scroll', check_if_in_view);
    }

    function check_if_in_view() {
          let $animation_elements = $('.bounce-up');
          let $animation_elements_left = $('.bounce-left');
          let $window = $(window);
          var window_height = $window.height();
          var window_top_position = $window.scrollTop();
          var window_bottom_position = (window_top_position + window_height);

          $.each($animation_elements, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
              	$element.addClass('in-view');
                //$element.slideUp();
            } else {
              $element.removeClass('in-view');
            }
   		});

        $.each($animation_elements_left, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);

            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
              	$element.addClass('in-view-left');
                //$element.slideUp();
            } else {
              $element.removeClass('in-view-left');
            }
   		});
	}

    if($('.bg-image-wrapper')){
		let $bgimageelements = $('.bg-image-wrapper');
        $.each($bgimageelements, function() {
            var $element = $(this);
			var src = $element.find('.image-for-bg').attr('src');
            $element.find('.image-for-bg').hide();
            $element.css('background',"url(" + src + ") no-repeat 0 50%");
            $element.css('background-repeat', "no-repeat");
            $element.css('background-size', "cover");
			//var tintDiv = '<div style="position: absolute;top: 0; left: 0;bottom: 0;right: 0; text-align: center;opacity: 0.2;background: #000;"></div>';    
			$element.find('.banner-image-tint').attr("style", "opacity: 0.4 !important");
   		});
    }

    var popupSourceElem = $(".source-btn");
    var targetPopup = $("#target-popup");
    var wellnessIcon = "/content/dam/tic/spa-icon-03.png";
    if(popupSourceElem && popupSourceElem.length) {
        popupSourceElem.each(function(){
            $(this).on('click', function(){
				console.log($(this).text());
                var parentContainer = $(this).parent().parent();
                var longDesc = parentContainer.siblings("#long-desc").html();
                var imageSrc = parentContainer.siblings(".image-for-bg").attr('src');
                var title = parentContainer.find("h3").text();
                var targetContainer = targetPopup.find("#popup-container");
				targetContainer.append(createPopup(longDesc, imageSrc, title, wellnessIcon));
                targetPopup.removeClass('d-none');
        	});
		});
    }

    function createPopup(longDesc, imageSrc, title, wellnessIcon) {
        return "<div><div class='row align-items-center pb-3'>"+"<img style='width:60px' src=" + wellnessIcon +"><span class='popup-title'>"+title+"</span>"+
            "<div onclick='closePopup()' style='flex:1;cursor: pointer;' class='justify-content-end d-flex'><span class='warning-box-close-icon icon-close'></span></div></div>" + "<div class='row justify-content-center'>" +
            "<div class='col-md-6 col-sm-11 col-12' style='overflow:hidden'>" + 
            "<img src=" +imageSrc+ " style='width: 100%; height: 100%;'></div><div class='col-md-6 col-sm-11 col-12 pl-md-5 p-3 pr-md-3 py-md-4 popup-desc text-left'>"+longDesc+"</div></div></div>";
    }

});

function closePopup() {
    $("#target-popup").addClass('d-none');
    $("#target-popup").find("#popup-container").html('');
}
