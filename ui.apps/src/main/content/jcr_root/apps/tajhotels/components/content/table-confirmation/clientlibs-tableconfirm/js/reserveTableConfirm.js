window.addEventListener('load', function () {

			fetchValues();
			$('.mr-cancelreserveTable-btn').on('click', function() {
				invokeCancelReservationAjaxCall();
			});
			$('.mr-modifyreserveTable-btn, .mr-close-icon-reserve-table-confirmation').on(
					'click',
					function() {

						var customerID = getParam('customerID');
						var restaurantID = getParam('restaurantID');
						var reservationID = getParam('reservationID');
						var firstname = getParam('firstname');
						var emailID = getParam('emailID');
						var modify = "UPDATE";
						previoiusPage(customerID, restaurantID, reservationID,
								firstname, emailID, modify);
					});

		});
function getParam(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return results[1];
}

	var name ="", firstName = "", status = "", emailID ="",
	peopleCount ="", dateValue = "", modifiedDate = "",
	trimmedDate = "", diningName = "", restaurantName = "", reservationTime ="", res="";
function fetchValues() {
    name = getParam('firstname');
    	firstName = decodeURIComponent(name);
    	status = decodeURIComponent(getParam('bookingStatus'));
    	emailID = getParam('emailID');
    	peopleCount = getParam('peopleCount');
    	dateValue = getParam('dateValue');
    	modifiedDate = moment(dateValue).format('LLLL');
    	trimmedDate = modifiedDate.slice(0, -9);
    	diningName = getParam('restaurantName');
    	restaurantName = decodeURIComponent(diningName);
    	reservationTime = getParam('reservationTime').replace('%20', ' ');

	    res = "" + trimmedDate + ',&nbsp;' + peopleCount
			+ '&nbsp;guests,&nbsp;' + reservationTime + '&nbsp;for&nbsp;'
			+ firstName;

    if(status === ""){

        unsuccessfulTransaction();
    }else{

        successTransaction();
    }
}

    function unsuccessfulTransaction(){
    	$('.mr-booking-Status').html("The Booking Operation was unsucessful");
    		$('.mr-reserve-table-booker').html("Please contact the restaurant for booking ");
    		$('.ReserveTable-booking-details').hide();
    		$('.mr-reserve-Table-CancelModify-btn').hide();
    		$('.mr-reservationTable-mail-sent').hide();
    }
    function successTransaction(){
        if (status == 'True') {
            status = 'Confirmed';
        }
    	$('.mr-booking-Status').html("Booking&nbsp;" + status);
    	$('.mr-restaurant-name').html(restaurantName);
    	$('.mr-reservation-table-details').html(res);
    	$('.mr-email-details').html(emailID);
    	$('.mr-restaurant-name').html(restaurantName);
    }
function invokeCancelReservationAjaxCall() {
	var customerID = getParam('customerID');
	var restaurantID = getParam('restaurantID');
	var reservationID = getParam('reservationID');

	$.ajax({
		type : 'GET',
		url : '/bin/cancelTableReservation',
		data : "customerID=" + customerID + "&restaurantID=" + restaurantID
				+ "&reservationID=" + reservationID,
		success : function(response) {

			if (response.responseCode == "SUCCESS") {
				$('.mr-booking-Status').html(response.message);
				$('.mr-reserve-table-booker').hide();
				$('.ReserveTable-booking-details').hide();
				$('.mr-reserve-Table-CancelModify-btn').hide();
				$('.mr-reservationTable-mail-sent').hide();
			}
		}, error: function(response){
                                      
       }
	})

}

function previoiusPage(customerID, restaurantID, reservationID,
		firstname, emailID, modify) {
	
	location.href = document.referrer + "&customerID=" + customerID
			+ "&restaurantID=" + restaurantID + "&reservationID="
			+ reservationID + "&requestType=" + modify + "&firstname="
			+ firstname + "&emailID=" + emailID;
} 