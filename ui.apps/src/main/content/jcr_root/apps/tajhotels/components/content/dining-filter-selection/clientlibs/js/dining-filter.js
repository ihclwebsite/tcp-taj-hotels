window.addEventListener('load', function() {
    try {
        var diningSearchInput = $("#searchbar-Dining-input");
        // var keys = {
        // 37 : 1,
        // 38 : 1,
        // 39 : 1,
        // 40 : 1,
        // 32 : 1,
        // 33 : 1,
        // 34 : 1,
        // 35 : 1,
        // 36 : 1
        // };

        var SEARCH_INPUT_DEBOUNCE_RATE = 3000;

        $(diningSearchInput).on('click', function(e) {
            e.stopPropagation();
        });

        $(diningSearchInput).on("keyup", debounce(function(e) {
            e.stopPropagation();
            if (e.keyCode !== 27 && e.keyCode !== 40 && e.keyCode !== 38 && e.keyCode !== 13) {
                if (diningSearchInput.val().length > 2) {
                    showSearchDiningCards();
                } else {
                    showFilteredDiningCards();
                }
            }
        }, SEARCH_INPUT_DEBOUNCE_RATE));

        $("#search-filter-section-dropdown").change(function() {
            if (diningSearchInput.val()) {
                // fetchSearchFilterList($(this), true);
                fetchFilterList($(this), true);
            } else {
                fetchFilterList($(this), false);
            }
        });
        $('.brands-toggle-switch-checkbox').change(function() {
            $('.filter-by-brands-toggle-switch .hotel-name').toggleClass('not-active-hotel');
            $("#search-filter-section-dropdown").trigger('change');
        });
    } catch (error) {
        console.error("Error in dining-filter-selection/clientlibs/js/dining-filter.js", error);
    }
});

$(document).ready(function() {
    try {
        $('.filter-by-brands-toggle-switch .current-hotel').text(findCurrentBrand());
        showFilteredDiningCards();
    } catch (error) {
        console.error("Error in dining-filter-selection/clientlibs/js/dining-filter.js", error);
    }
});

function showFilteredDiningCards() {
    var diningListWrapper = $('.dining-list-fitered-wrapper .list-item');
    diningListWrapper.empty();
    var currentTheme = findCurrentBrand();
    $('.dining-list-wrapper .unique-dining .mr-dining-card-wrapper').each(function() {
        var isShowable = checkBrandFilter($(this), currentTheme);
        if ($(this).css("display") == "block" && isShowable) {
            var uniqueDining = $(this).closest('.unique-dining').clone(true);
            diningListWrapper.append(uniqueDining);
        }
    });
    $('.dining-list-fitered-wrapper').siblings('.jiva-spa-show-more').remove();
    $('.dining-list-fitered-wrapper').showMore(6, 6);

    var diningListWrapper2 = $('.dining-list-fitered-wrapper2 .list-item');
    diningListWrapper2.empty();
    $('.dining-list-wrapper2 .unique-dining .mr-dining-card-wrapper').each(function() {
        var isShowable = checkBrandFilter($(this), currentTheme);
        if ($(this).css("display") == "block" && isShowable) {
            var uniqueDining = $(this).closest('.unique-dining').clone(true);
            diningListWrapper2.append(uniqueDining);
        }

    });

    $('.dining-list-fitered-wrapper2').siblings('.jiva-spa-show-more').remove();
    $('.dining-list-fitered-wrapper2').showMore(6, 6);

    showHideItems();

}

function fetchFilterList(filterID, hasSearchInput) {
    var value = $(filterID).val().toLowerCase();
    if (value == '' || value == 'all') {
        $(".unique-dining .mr-dining-card-wrapper").show();
    } else {
        // var currentTheme = findCurrentBrand();
        $(".dining-list-wrapper .unique-dining .mr-dining-card-wrapper").filter(function() {
            // $(this).toggle($(this).find(".mr-regulation-answer").text().toLowerCase().indexOf(value) > -1)
            // var isShowable = checkBrandFilter($(this), currentTheme);
            $(this).toggle($(this).find(".mr-regulation-answer").text().toLowerCase().indexOf(value) > -1)
        });
    }
    var filteredLength = $(".unique-dining .mr-dining-card-wrapper :visible").length;
    if (filteredLength == 0) {
        console.info("No result to display");
    }

    if (hasSearchInput) {
        showSearchDiningCards();
    } else {
        showFilteredDiningCards();
    }
}

function showSearchDiningCards() {

    var searchKey = $('#searchbar-Dining-input').val().toLowerCase();
    var diningListWrapper = $('.dining-list-fitered-wrapper .list-item');
    diningListWrapper.empty();
    var currentTheme = findCurrentBrand();
    $('.dining-list-wrapper .unique-dining .mr-dining-card-wrapper').each(
            function() {
                if ($(this).css("display") == "block") {
                    var uniqueDining = $(this).closest('.unique-dining').clone(true);
                    var name = $(this).data('dining') ? $(this).data('dining').toString().toLowerCase() : '';
                    var dArea = $(this).data('area') ? $(this).data('area').toString().toLowerCase() : '';
                    var city = $(this).data('city') ? $(this).data('city').toString().toLowerCase() : '';
                    var country = $(this).data('country') ? $(this).data('country').toString().toLowerCase() : '';
                    var hotel = $(this).data('name') ? $(this).data('name').toString().toLowerCase() : '';
                    var cuisine = $(this).data('cuisine') ? $(this).data('cuisine').toString().toLowerCase() : '';
                    /*
                     * var brand = $(this).data('brand') ? $(this).data('brand').toString().toLowerCase() : true; var
                     * selectedBrand = findselectedBrand(); var isShown = false; var currentTheme = findCurrentBrand();
                     * if(selectedBrand && selectedBrand == "others") { isShown = brand != currentTheme; } else
                     * if(selectedBrand && selectedBrand != "others") { isShown = brand == selectedBrand; } else {
                     * isShown = true; }
                     */
                    var isShowable = checkBrandFilter($(this), currentTheme);
                    if ((name.includes(searchKey) || dArea.includes(searchKey) || city.includes(searchKey)
                            || country.includes(searchKey) || hotel.includes(searchKey) || cuisine.includes(searchKey))
                            && isShowable) {
                        diningListWrapper.append(uniqueDining);
                    }
                }

            });
    $('.dining-list-fitered-wrapper').siblings('.jiva-spa-show-more').remove();
    $('.dining-list-fitered-wrapper').showMore(6, 6);
    var diningListWrapper2 = $('.dining-list-fitered-wrapper2 .list-item');
    diningListWrapper2.empty();
    $('.dining-list-wrapper2 .unique-dining .mr-dining-card-wrapper').each(
            function() {
                if ($(this).css("display") == "block") {
                    var uniqueDining = $(this).closest('.unique-dining').clone(true);
                    var name = $(this).data('dining') ? $(this).data('dining').toString().toLowerCase() : '';
                    var dArea = $(this).data('area') ? $(this).data('area').toString().toLowerCase() : '';
                    var city = $(this).data('city') ? $(this).data('city').toString().toLowerCase() : '';
                    var country = $(this).data('country') ? $(this).data('country').toString().toLowerCase() : '';
                    var hotel = $(this).data('name') ? $(this).data('name').toString().toLowerCase() : '';
                    var cuisine = $(this).data('cuisine') ? $(this).data('cuisine').toString().toLowerCase() : '';
                    var isShowable = checkBrandFilter($(this), currentTheme);
                    if (name.includes(searchKey) || dArea.includes(searchKey) || city.includes(searchKey)
                            || country.includes(searchKey) || hotel.includes(searchKey) || cuisine.includes(searchKey)
                            && isShowable) {
                        diningListWrapper2.append(uniqueDining);
                    }
                }

            });
    $('.dining-list-fitered-wrapper2').siblings('.jiva-spa-show-more').remove();
    $('.dining-list-fitered-wrapper2').showMore(6, 6);

    showHideItems();

}

function showHideItems() {
    var destinationFlag = $('#destinationPage').val();
    var website = $('.website-brand');
    var other = $('.other-brand');
    var noResults = $('#noResults');
    if (destinationFlag == "") {
        var websiteCount = $('.dining-list-fitered-wrapper .list-item').children().length;
    } else {
        var websiteCount = $('.website-brand .list-item').children().length;
    }
    var otherCount = $('.other-brand .list-item').children().length;

    if (websiteCount > 0 && otherCount > 0) {
        website.show();
        other.show();
        // noResults.hide();
    } else if (websiteCount == 0 && otherCount > 0) {
        website.hide();
        other.show();
        // noResults.hide();
    } else if (websiteCount > 0 && otherCount == 0) {
        website.show();
        other.hide();
        // noResults.hide();
    } else if (websiteCount == 0 && otherCount == 0) {
        website.hide();
        other.hide();
        // noResults.show();
    }

    var listCount = $('.dining-list-fitered-wrapper .list-item').children().length;
    var listCount2 = $('.dining-list-fitered-wrapper2 .list-item').children().length;
    if (listCount > 0 || listCount2 > 0) {
        noResults.hide();
    } else {
        noResults.show();
    }

}

function findCurrentBrand() {
    var brands = document.querySelector('.cm-page-container').classList;
    var brandsLength = brands.length, i;
    for (i = 0; i < brandsLength; i++) {
        var className = brands[i].trim();
        if (className === "taj") {
            return className;
        } else if (className.includes('-theme')) {
            return className.split('-')[0];
        }
    }
}

function findSelectedBrand() {
    var toggeBtn = $('.brands-toggle-switch-checkbox');
    var selectedTheme;
    if (toggeBtn.attr('type')) {
        if (toggeBtn.prop('checked')) {
            selectedTheme = "others";
        } else {
            selectedTheme = $('.filter-by-brands-toggle-switch .current-hotel').text().toLowerCase();
        }
        return selectedTheme;
    }
    return null;
}

function checkBrandFilter(_this, currentTheme) {
    var cardBrand = _this.attr('data-brand').split('/')[2];
    var selectedBrand = findSelectedBrand();
    if (cardBrand && selectedBrand) {
        if (cardBrand == "gateway")
            cardBrand = "taj";
        if (selectedBrand == "others" && cardBrand == currentTheme || selectedBrand != "others"
                && cardBrand != selectedBrand) {
            return false;
        } else if (selectedBrand != "others" && cardBrand == selectedBrand) {
            return true;
        }
    }
    return true;
}
