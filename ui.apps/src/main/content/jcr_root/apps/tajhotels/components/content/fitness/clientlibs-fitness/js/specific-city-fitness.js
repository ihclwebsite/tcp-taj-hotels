window.addEventListener('load', function() {

    $('.spec-fitness-desc').each(function() {
        $(this).cmToggleText({
            charLimit : 150
        })
    });

    $('.ho-fourth-sect-fitness').click(function() {
        $('.specCity-timeTable-popUp-wrap').children('.cm-specific-showMap-con').show();
    })

    $('.view-photos-container').click(function() {
        $('.specific-hotels-page').find('.mr-menu-carousel-overlay').removeClass('mr-overlay-initial-none');
    })
    $('.showMap-close').click(function() {
        $('.cm-specific-showMap-con.spec-city-timeTable').hide();

    })

})
