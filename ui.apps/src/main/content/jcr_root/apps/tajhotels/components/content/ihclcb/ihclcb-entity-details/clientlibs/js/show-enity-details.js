$(document).ready(function() {
    var userDetails = dataCache.local.getData("userDetails")
    var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
    $('#corporate-ihclcb-resetpopup').css('display', 'none');

    if (userDetails && ihclCbBookingObject && ihclCbBookingObject.isIHCLCBFlow) {

        var entitySessionDetails = {}
        if (userDetails) {
            if (userDetails.selectedEntity) {
                selectedEntity = userDetails.selectedEntity;
            } else {
                selectedEntity = userDetails.selectedEntityAgent;
            }
        }
        console.log("entitySessionDetails:", selectedEntity);

        if (selectedEntity) {
            if (selectedEntity.partyName) {
                $('.cart-entity-details span').append(selectedEntity.partyName)
            }

            if (selectedEntity.city) {
                $('.cart-city-details span').append(selectedEntity.city)
            }
            if (selectedEntity.gSTNTaxID_c) {
                $('.cart-gstin-details span').append(selectedEntity.gSTNTaxID_c)
            }
            if (selectedEntity.iataNumber) {
                $('.cart-iata-details span').append(selectedEntity.iataNumber)
            }

        }
    }

});
