$('document').ready(function(){	
    goToEnquiry();
	$('.amount').each(function(){
		$(this).text(parseInt($(this).text()).toLocaleString('en-IN'));
	})
});

function addDetails(){

	$( 'input[name=select-package-checkbox]' ).on( 'change', function( e ) {
		var selectedPackagesCount = $( 'input[name="select-package-checkbox"]:checked' ).length;
        $( '.selected-packages-count' ).text( selectedPackagesCount );
    } );
}

function goToEnquiry(){
	var locations=[];
    var allHotels=[];
    var allHotelsData = [];
    var minNightsData = [];
    $("input[name=select-package-checkbox]").each( function () {
        if($(this).prop("checked") == true){
            locations.push($(this).val());
        }
   });
    $("input[name=select-package-checkbox]").each( function () {
            allHotels.push($(this).val());
   });
    $(".category-details").each( function () {
		var packageMinNightsData = {};
        var categoryName = $(this).find('.categoryName').text();
        var categoryMinNights = $(this).find('.categoryMinNights').text();
        packageMinNightsData.categoryName = categoryName;
        packageMinNightsData.categoryMinNights = +categoryMinNights;
        minNightsData.push(packageMinNightsData)
   });
    $('.incredible-escape-hotel-card').each( function() {
		//console.log("incredible-escape-hotel-card found");
        var hotelData = {};
        var isCityHotel = $(this).find('.isCityHotel').text();
        var hotelId = $(this).find('.hotelId').text();
        var requiredHotel = $(this).find('.requiredHotel').text();
        var roomCode = $(this).find('.roomCode').text();
        var hotelName = $(this).find('.incredible-escape-hotel-card-title').text();
        var category = $(this).find('.category').text();
		hotelData.hotelName = hotelName;
        hotelData.hotelId = hotelId;
        hotelData.isCityHotel = isCityHotel;
        hotelData.requiredHotel = requiredHotel;
        hotelData.roomCode = roomCode;
        hotelData.category = category;
        allHotelsData.push(hotelData);
		console.log(allHotelsData);
    });
    //allHotelsData.push(minNightsData);
    dataCache.session.setData('allHotelsObject', allHotels);

    var rateCode = $('.rateCode').text();
    var amount = +$('.amount').first().text();

    var onlineItineraryDetails = {
        "allHotelsData": allHotelsData,
        "minNightsData": minNightsData,
        "rateCode":rateCode,
        "basePrice":amount
    }
	dataCache.session.setData('ItineraryDetails', onlineItineraryDetails);
    //dataCache.session.setData('allHotelsData', allHotelsData);

    var packageData = {};
	var minNights = $('.minNights').text();
	var maxBreak = +$('.maxBreak').text();
    var rateCode = $('.rateCode').text();
	packageData.minNights = minNights;
    packageData.maxBreak = maxBreak;
    packageData.rateCode = rateCode;
    dataCache.session.setData('packageData', packageData);

    var result=setEnquiryObject(locations);
    if(result){
        console.info("enquiry sent successfully");
    	/*window.location.assign(enquiryPage+".html?packagepage="+window.location.pathname);*/		
    }else{
        var popupParams = {
            title: 'Booking Failed!',
            description: "Failed to book your Package. Please contact the support team.",
        }
        warningBox( popupParams );
    }
}    

function setEnquiryObject(locations){

    var enquiryDetails = {};
    try{
  	  var name=$('.package-properties').data('packagename');
      var destination=$('.package-properties').data('destinationname');

      if((name===undefined) || (destination===undefined)){
          throw("Package name or destination name missing");
          return false;
      }else{
  		enquiryDetails.packageName=name;
  		enquiryDetails.packageDestination=destination;
		enquiryDetails.selectedHotels=locations;
	    dataCache.session.setData('enquiryObject', enquiryDetails);
      	return true;
      }

  }catch(err){
		return false;
  }
}