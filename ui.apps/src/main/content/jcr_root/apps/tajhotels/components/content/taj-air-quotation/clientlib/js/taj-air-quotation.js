var w1 = window.outerWidth;
var w2 = window.innerWidth;
var dollarRate = '73'; // document.getElementById("dollarRate").value;

if (window.XMLHttpRequest) {
    xhttp = new window.XMLHttpRequest();
} else { // Internet Explorer 5/6
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
}
xhttp.open("GET", "/content/dam/list/airrates.xml", false);
xhttp.send("");
// alert("else");
xmlDoc1 = xhttp.responseXML;
var getQuoteObj = {};
// reset funxtion
function Resetfn() {
    try {
        var dropDown = document.getElementById("journey");
        dropDown.selectedIndex = 0;
        document.getElementById("squery").options.length = 0;
        document.getElementById("squery").options[0] = new Option("SELECT", "0");
        document.getElementById("mquery").value = ""; // new Option("-------Select---------","0");
        document.getElementById("returnRadio").selected = false;
        // document.getElementById("oneRadio").selected = true;
        // document.getElementById("oneRadio").options[0] = new Option("oneway", "0");
        // document.getElementById("oneRadio").selectedIndex = 0;
        document.getElementById("resultDiv").innerHTML = "";
        $('#quoteDiv #squery').data("selectBox-selectBoxIt").refresh();
        $('#quoteDiv #mquery').data("selectBox-selectBoxIt").refresh();
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }
}

$(document)
        .ready(
                function() {

                    $('.pricing-card-details-wrap').hide();

                    // onlclick-quick-quote-open-overlay-code
                    $(".quick-quote-btn").click(function() {
                        $(".quickQuoteMain").show();

                        prepareQuickQuoteJsonForClick("QuickQuote");
                    });

                    // close-button-code
                    $(".fa-remove").click(function() {
                        $(".quickQuoteMain").hide();
                    });

                    if ($(".quickQuoteMain").display == "block") {
                        $("body").click(function() {
                            $(".quickQuoteMain").hide();
                        });
                    }

                    $(".travelDetails").hide();

                    $("#getQuoteBtn").click(function() {
                        // $(".travelDetails").slideUp("fast");
                        if (!validateMe()) {
                            $(".travelDetails").slideUp("fast");
                            return false;
                        }

                        // for analytics
                        getQuoteObj.tripType = $('#journey').val();
                        prepareQuickQuoteJsonAfterClick("GetQuote", getQuoteObj);
                        // analytics end

                        $(".travelDetails").slideDown("fast");
                    });

                    $(".resetBtn").click(function() {
                        $(".travelDetails").slideUp("fast");
                    });

                    $('.pricing-expl').click(function() {
                        $('#screen, #modal').show();
                    });

                    $('#screen, #close-price-expl').click(function() {
                        $('#screen, #modal').hide();
                    });

                    $('.myButtonWalkthru').click(function() {
                        $('#screen-walkthru, #modal-walkthru').show();
                    });

                    $('#screen-walkthru, #close-walkthru').click(function() {
                        $('#screen-walkthru, #modal-walkthru').hide();
                    });

                    $('.pricing-expl').click(function() {
                        $('.pricing-card-details-wrap').show();
                    });

                    $('.pricing-details-close').click(function() {
                        $('.pricing-card-details-wrap').hide();
                    });

                    $("#bookajet, .requestQuote").click(function() {
                        var d1 = $("#mquery").val();
                        d1 = d1.split("[");
                        d1 = d1[0].trim(" ");
                        var d2 = $("#squery").val();
                        d2 = d2.split("[");
                        d2 = d2[0].trim(" ");
                        var d3 = $("#journey").val();

                        if (typeof (Storage) !== "undefined") {
                            localStorage.setItem("departureCity", d1);
                            localStorage.setItem("destinationCity", d2);
                            localStorage.setItem("flightType", d3);
                        } else {
                            alert('Please update your Browser.');
                        }
                        getQuoteObj.flight_tripPriceUSD = $(document).find('.secondary-rupee-ui').text();
                        getQuoteObj.flight_tripPriceINR = $(document).find('.primary-rupee-ui').text();
                        prepareQuickQuoteJsonAfterClick("BookJet_QuoteSeen", getQuoteObj);
                    });

                    $("#quoteDiv,.flight-journey-details-cnt")
                            .on(
                                    'click keyup keydown change ',
                                    '#mquery, #cityName option',
                                    function(event) {
                                        try {
                                            init = 1;
                                            // alert("clalled on");

                                            document.getElementById("squery").options.length = null;
                                            var cityarr = new Array();
                                            var cnt = 0;

                                            if ($(this).val() == "" || $(this).val() == undefined) {
                                                document.getElementById("squery").options[0] = new Option("SELECT", "0");
                                            } else {

                                                var xmlCity = xmlDoc1.getElementsByTagName("tajairrates");
                                                var xmlCity1 = xmlCity[0].getElementsByTagName("Flight");

                                                for (f = 0; f < xmlCity1.length; f++) {

                                                    if (xmlCity1[f].getAttribute("Enable") == 1) {

                                                        xmlItm1 = xmlCity1[f].getElementsByTagName("item")
                                                        for (m = 0; m < xmlItm1.length; m++) {

                                                            if (xmlItm1[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                                    .toLowerCase() == $(this).val().toLowerCase()) {

                                                                if (cityarr.length > 0) {

                                                                    if (cityarr
                                                                            .valueOf()
                                                                            .toString()
                                                                            .indexOf(
                                                                                    xmlItm1[m]
                                                                                            .getElementsByTagName("to")[0].childNodes[0].nodeValue) == -1) {
                                                                        cityarr[cnt] = xmlItm1[m]
                                                                                .getElementsByTagName("to")[0].childNodes[0].nodeValue;
                                                                        cnt++;
                                                                    }

                                                                } else {
                                                                    cityarr[cnt] = xmlItm1[m]
                                                                            .getElementsByTagName("to")[0].childNodes[0].nodeValue;
                                                                    cnt++;
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                                cityarr.sort();
                                                var cmb = $(this).closest('.single-flight-journey-detail-wrp').find(
                                                        '#squery')[0];
                                                // var cmb = document.getElementById( 'squery' );
                                                cmb.options[0] = new Option("SELECT", "SELECT");
                                                if (cityarr.length > 0) {
                                                    for (f = 0; f < cityarr.length; f++) {
                                                        cmb.options[init] = new Option(cityarr[f].toUpperCase(),
                                                                cityarr[f].toUpperCase());
                                                        init++;
                                                    }
                                                }
                                                cmb.options[0].selected = true;
                                                $(this).closest('.single-flight-journey-detail-wrp').find(
                                                        '#squerySelectBoxItContainer').remove();
                                                $(this).closest('.single-flight-journey-detail-wrp').find('#squery')
                                                        .data("selectBox-selectBoxIt").refresh();
                                            }
                                        } catch (error) {
                                            console.error("Error in taj-air-quotation.js ", error);
                                        }
                                    });

                });

// function change() {
// $("#mquery").keyup( function() {
// $(document.body).delegate('#mquery', 'change', function() {

// $("#mquery").onSelect( function(e) {

function validateMe() {
    try {
        if (document.getElementById("mquery").value == "" || document.getElementById("mquery").value == undefined) {
            alert("Please select the origin and destination to get a quick quote");
            document.getElementById("mquery").focus();
            return false;
        }

        if (document.getElementById("squery").options[document.getElementById("squery").selectedIndex].text == "SELECT"
                || document.getElementById("squery").options[document.getElementById("squery").selectedIndex].value == "0") {
            alert("Please select the destination to get a quick quote");
            document.getElementById("squery").focus();
            return false;
        }

        var departureFrom = document.getElementById("mquery").value;

        var matches1 = departureFrom.match(/\[(.*?)\]/);
        if (matches1) {
            var submatch1 = matches1[1];
        }

        var arrivingAt = document.getElementById("squery").options[document.getElementById("squery").selectedIndex].value;
        var matches = arrivingAt.match(/\[(.*?)\]/);

        if (matches) {
            var submatch = matches[1];
        }

        getQuoteObj.flight_ToCity = submatch;
        getQuoteObj.flight_FromCity = submatch1;
        // getFlightDetails(document.getElementById("mquery").options[document.getElementById("mquery").selectedIndex].value,document.getElementById("squery").options[document.getElementById("squery").selectedIndex].value,"INR");
        // setTimeout("getFlightDetails('" + document.getElementById("mquery").value + "','"
        // + document.getElementById("squery").options[document.getElementById("squery").selectedIndex].value
        // + "','INR');", 500);
        setTimeout("getFlightDetails('" + departureFrom + "','" + arrivingAt + "','INR');", 500);
        return true;
        // var winname1 = "loadme.asp?" + "mquery=" +
        // document.getElementById("mquery").options[document.getElementById("mquery").selectedIndex].value + "&squery="
        // +
        // document.getElementById("squery").options[document.getElementById("squery").selectedIndex].value +
        // "&rdJourney="
        // + document.support.rdJourney.value + "&rdPay"

        // window.open()
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }

}

var strResf = null;
var strRes = null;

function getFlightDetails() {
    try {
        strRes = null;

        // if ( window.XMLHttpRequest ) {
        // xhttp = new window.XMLHttpRequest();
        // } else { // Internet Explorer 5/6
        // xhttp = new ActiveXObject( "Microsoft.XMLHTTP" );
        // }
        // xhttp.open( "GET", "/content/dam/list/airrates.xml", false );
        // xhttp.send( "" );
        // xmlDoc = xhttp.responseXML;
        var xmlItem1 = xmlDoc1.getElementsByTagName("tajairrates");
        var xmlItem = xmlItem1[0].getElementsByTagName("Flight");

        for (f = 0; f < xmlItem.length; f++) {

            if (xmlItem[f].getAttribute("Enable") == 1) {

                xmlItm = xmlItem[f].getElementsByTagName("item")
                for (m = 0; m < xmlItm.length; m++) {

                    if (xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue.toLowerCase() == arguments[0]
                            .toLowerCase()
                            && xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue.toLowerCase() == arguments[1]
                                    .toLowerCase()) {
                        strRes = "<div class='jet-name'>" + xmlItem[f].getAttribute("name") + "</div>";
                        strRes += "<div class='jet-description'>"
                                + xmlItm[m].getElementsByTagName("description")[0].childNodes[0].nodeValue + "</div>";
                        if (arguments[2] == "INR") {
                            if (strRes != null)
                                strRes += "<div class=\"inr\"><div class=\"div1 text-right\">"
                                        + "<span class='primary-rupee-ui'><span class='rupee'>&#x20B9;</span> "
                                        + calculateAmt(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm)
                                        + "</span><span class='secondary-rupee-ui'>US$ "
                                        + calculateAmtUSD(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm) + "</span></div>";
                            else
                                strRes = "<div class=\"inr\"><div class=\"div1 text-right\">"
                                        + "<span class='primary-rupee-ui'><span class='rupee'>&#x20B9;</span> "
                                        + calculateAmt(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm)
                                        + "</span><span class='secondary-rupee-ui'>US$ "
                                        + calculateAmtUSD(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm) + "</span></div>";
                        } else {
                            if (strRes != null)
                                strRes += "<div class=\"inr\"><div class=\"div1 text-right\">"
                                        + "<span class='primary-rupee-ui'>US$ "
                                        + calculateAmt(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm) + "</span></div>";
                            else
                                strRes = "<div class=\"inr\"><div class=\"div1 text-right\">"
                                        + "<span class='primary-rupee-ui'>US$ "
                                        + calculateAmt(
                                                xmlItm[m].getElementsByTagName("rupees")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("mins")[0].childNodes[0].nodeValue,
                                                xmlItm[m].getElementsByTagName("from")[0].childNodes[0].nodeValue
                                                        .toLowerCase(),
                                                xmlItm[m].getElementsByTagName("to")[0].childNodes[0].nodeValue
                                                        .toLowerCase(), xmlItm) + "</span></div>";
                        }
                        strRes += "</div>";
                    }

                }

            }

        }

        if (strRes != null)
            document.getElementById("resultDiv").innerHTML = strRes;
        else
            document.getElementById("resultDiv").innerHTML = "No results found for searched criteria.";
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }
}

function calculateAmt() {
    try {
        var amt = arguments[0];
        var hrs = arguments[1];
        var from = arguments[2];
        var to = arguments[3];
        var xmlIt = arguments[4];
        var valmins = parseInt(hrs) + parseInt(hrs)
        var calculatedAmt;
        // alert("hrs=" + hrs);
        // alert("valmin=" + valmins);
        // if(document.getElementById("oneRadio").checked)
        if (from == "mumbai [bom]") {
            if (valmins < 60)
                valmins = 60;
            // alert(valmins);
            // alert(parseInt(amt)/ 60);
            calculatedAmt = Math.floor(valmins * (parseInt(amt) / 60))
        } else if (to == "mumbai [bom]") {
            if (!document.getElementById("returnRadio").selected) {
                if (valmins < 60)
                    valmins = 60
                else
                    valmins = hrs
                calculatedAmt = Math.floor(valmins * ((parseInt(amt) * 2) / 60))
                // strResult = "INR " & clng((cdbl(valmins) * cdbl(objHdl.childNodes(3).text) * 2) / 60)

            } else {
                if ((parseInt(hrs) * 4) < 60)
                    valmins = 60;
                else
                    valmins = parseInt(hrs);
                calculatedAmt = Math.floor(valmins * ((parseInt(amt) * 4) / 60))

                // strResult = "INR " & clng((cdbl(valmins) * cdbl(objHdl.childNodes(3).text) * 4) / 60)
            }

        } else {
            var getminutes = null; // Get the traveling time between mumbai and selected city
            var getminutes1 = null;

            for (g = 0; g < xmlIt.length; g++) {
                if (xmlIt[g].getElementsByTagName("from")[0].childNodes[0].nodeValue.toLowerCase() == "mumbai [bom]"
                        && xmlIt[g].getElementsByTagName("to")[0].childNodes[0].nodeValue.toLowerCase() == from) {
                    getminutes = xmlIt[g].getElementsByTagName("mins")[0].childNodes[0].nodeValue
                    // alert('from mumbai [bom] to ' + from + "=" + getminutes);
                    break;
                }

            }

            for (g = 0; g < xmlIt.length; g++) {

                if (xmlIt[g].getElementsByTagName("from")[0].childNodes[0].nodeValue.toLowerCase() == "mumbai [bom]"
                        && xmlIt[g].getElementsByTagName("to")[0].childNodes[0].nodeValue.toLowerCase() == to) {
                    getminutes1 = xmlIt[g].getElementsByTagName("mins")[0].childNodes[0].nodeValue
                    // alert('from '+ to +' to mumbai [bom]=' + getminutes1);
                    break;
                }

            }

            if (!document.getElementById("returnRadio").selected) {

                valmins = parseInt(getminutes) + parseInt(hrs) + parseInt(getminutes1);
                // alert('onewayvalmin=' +valmins );
                if (valmins < 60)
                    valmins = 60;
                calculatedAmt = Math.floor((valmins * parseInt(amt)) / 60)
            } else {
                valmins = (parseInt(getminutes) + parseInt(hrs)) * 2;
                // alert("getminutes=" + getminutes);
                if (valmins < 60)
                    valmins = 60;
                else
                    valmins = parseInt(getminutes) + parseInt(hrs);
                // alert('returnvalmin=' +valmins );
                calculatedAmt = Math.floor(((valmins * parseInt(amt)) * 2) / 60)

            }

        }

        // alert("amt=" + amt);
        // alert("'"+calculatedAmt+".00'");
        var ammt = calculatedAmt.toString() + ".00";
        // alert(ammt);
        calculatedAmt = CommaFormatted(ammt);
        // alert("calamt=" + calculatedAmt);
        return calculatedAmt;
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }
}

function calculateAmtUSD() {
    try {
        var amt = arguments[0];
        var hrs = arguments[1];
        var from = arguments[2];
        var to = arguments[3];
        var xmlIt = arguments[4];
        var valmins = parseInt(hrs) + parseInt(hrs)
        var calculatedAmt;
        // alert("hrs=" + hrs);
        // alert("valmin=" + valmins);
        // if(document.getElementById("oneRadio").checked)
        if (from == "mumbai [bom]") {
            if (valmins < 60)
                valmins = 60;
            // alert(valmins);
            // alert(parseInt(amt)/ 60);
            calculatedAmt = Math.floor(valmins * (parseInt(amt) / 60))
        } else if (to == "mumbai [bom]") {
            if (!document.getElementById("returnRadio").selected) {
                if (valmins < 60)
                    valmins = 60
                else
                    valmins = hrs
                calculatedAmt = Math.floor(valmins * ((parseInt(amt) * 2) / 60))
                // strResult = "INR " & clng((cdbl(valmins) * cdbl(objHdl.childNodes(3).text) * 2) / 60)

            } else {
                if ((parseInt(hrs) * 4) < 60)
                    valmins = 60;
                else
                    valmins = parseInt(hrs);
                calculatedAmt = Math.floor(valmins * ((parseInt(amt) * 4) / 60))

                // strResult = "INR " & clng((cdbl(valmins) * cdbl(objHdl.childNodes(3).text) * 4) / 60)
            }

        } else {
            var getminutes = null; // Get the traveling time between mumbai and selected city
            var getminutes1 = null;

            for (g = 0; g < xmlIt.length; g++) {
                if (xmlIt[g].getElementsByTagName("from")[0].childNodes[0].nodeValue.toLowerCase() == "mumbai [bom]"
                        && xmlIt[g].getElementsByTagName("to")[0].childNodes[0].nodeValue.toLowerCase() == from) {
                    getminutes = xmlIt[g].getElementsByTagName("mins")[0].childNodes[0].nodeValue
                    // alert('from mumbai [bom] to ' + from + "=" + getminutes);
                    break;
                }

            }

            for (g = 0; g < xmlIt.length; g++) {

                if (xmlIt[g].getElementsByTagName("from")[0].childNodes[0].nodeValue.toLowerCase() == "mumbai [bom]"
                        && xmlIt[g].getElementsByTagName("to")[0].childNodes[0].nodeValue.toLowerCase() == to) {
                    getminutes1 = xmlIt[g].getElementsByTagName("mins")[0].childNodes[0].nodeValue
                    // alert('from '+ to +' to mumbai [bom]=' + getminutes1);
                    break;
                }

            }

            if (!document.getElementById("returnRadio").selected) {

                valmins = parseInt(getminutes) + parseInt(hrs) + parseInt(getminutes1);
                // alert('onewayvalmin=' +valmins );
                if (valmins < 60)
                    valmins = 60;
                calculatedAmt = Math.floor((valmins * parseInt(amt)) / 60)
            } else {
                valmins = (parseInt(getminutes) + parseInt(hrs)) * 2;
                // alert("getminutes=" + getminutes);
                if (valmins < 60)
                    valmins = 60;
                else
                    valmins = parseInt(getminutes) + parseInt(hrs);
                // alert('returnvalmin=' +valmins );
                calculatedAmt = Math.floor(((valmins * parseInt(amt)) * 2) / 60)

            }

        }
        // alert("amt=" + amt);
        // alert("'"+calculatedAmt+".00'");
        var ammt = calculatedAmt.toString() + ".00";
        // alert(ammt);
        // alert("calamt=" + (parseInt(calculatedAmt) / 54));
        // calculatedAmt = CommaFormatted(ammt);
        // calculatedAmt = Math.floor(parseInt(calculatedAmt) / 65);
        calculatedAmt = Math.floor(parseInt(calculatedAmt) / dollarRate);
        calculatedAmt = CommaFormatted(calculatedAmt.toString() + ".00");
        return calculatedAmt;
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }
}

function CommaFormatted(amount) {
    try {
        var delimiter = ","; // replace comma if desired
        var a = amount.split('.', 2)
        var d = a[1];
        var i = parseInt(a[0]);
        if (isNaN(i)) {
            return '';
        }
        var minus = '';
        if (i < 0) {
            minus = '-';
        }
        i = Math.abs(i);
        var n = new String(i);
        var a = [];
        while (n.length > 3) {
            var nn = n.substr(n.length - 3);
            a.unshift(nn);
            n = n.substr(0, n.length - 3);
        }
        if (n.length > 0) {
            a.unshift(n);
        }
        n = a.join(delimiter);
        if (d.length < 1) {
            amount = n;
        } else {
            amount = n;
        }
        amount = minus + amount;
        return amount;
    } catch (error) {
        console.error("Error in taj-air-quotation.js ", error);
    }
}
