$(document).ready(function() {
    /*
    $('.image-popup-vertical-fit').magnificPopup({
        type : 'image',
        mainClass : 'mfp-with-zoom',
        gallery : {
            enabled : true
        },

        zoom : {
            enabled : false,
            duration : 300, // duration of the effect, in milliseconds
            easing : 'ease-in-out', // CSS transition easing function
            opener : function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        },
        callbacks: {
            elementParse: function(item) {
                if(item.el[0].classList[0] == 'video') {
                    item.type = 'iframe',
                    item.iframe = {
                       patterns: {
                         youtube: {
                           index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).            
                           id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            // Or null - full URL will be returned
                            // Or a function that should return %id%, for example:
                            // id: function(url) { return 'parsed id'; } 
            
                           src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
                         },
                         vimeo: {
                           index: 'vimeo.com/',
                           id: '/',
                           src: '//player.vimeo.com/video/%id%?autoplay=1'
                         },
                         gmaps: {
                           index: '//maps.google.',
                           src: '%id%&output=embed'
                         }
                       }
                    }
                  } else {
                     item.type = 'image',
                     item.tLoading = 'Loading image #%curr%...',
                     item.mainClass = 'mfp-with-zoom',
                     item.image = {
                       tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                     }
                 }
            }
        }
    });
    */
    updateRendition();
});


function updateRendition() {
    $(".singleImg").each(function() {
        var source = $(this).find('img').attr('src') || $(this).find('video source').attr('src');        
        if (!source) {
            $(this).parent().hide();
        }
        var zoomImgPath = $(this).find('.image-popup-vertical-fit').attr('href');
        if (zoomImgPath.includes('png')) {
            zoomImgPath = zoomImgPath.replace('jpeg', 'png');
        }
        $(this).find('.image-popup-vertical-fit').attr('href', zoomImgPath);
    });
}




function showImgCarousel($elem) {
	console.log('here..');
	var imglist = $elem.attr('data-imgList');
	$('.' + imglist).magnificPopup({
        type : 'image',
        mainClass : 'mfp-with-zoom',
        gallery : {
            enabled : true
        },

        zoom : {
            enabled : false,
            duration : 300, // duration of the effect, in milliseconds
            easing : 'ease-in-out', // CSS transition easing function
            opener : function(openerElement) {
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        },
        callbacks: {
            elementParse: function(item) {
                if(item.el[0].classList[0] == 'video') {
                    item.type = 'iframe',
                    item.iframe = {
                       patterns: {
                         youtube: {
                           index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).            
                           id: 'v=', // String that splits URL in a two parts, second part should be %id%
                            // Or null - full URL will be returned
                            // Or a function that should return %id%, for example:
                            // id: function(url) { return 'parsed id'; } 
            
                           src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
                         },
                         vimeo: {
                           index: 'vimeo.com/',
                           id: '/',
                           src: '//player.vimeo.com/video/%id%?autoplay=1'
                         },
                         gmaps: {
                           index: '//maps.google.',
                           src: '%id%&output=embed'
                         }
                       }
                    }
                  } else {
                     item.type = 'image',
                     item.tLoading = 'Loading image #%curr%...',
                     item.mainClass = 'mfp-with-zoom',
                     item.image = {
                       tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                     }
                 }
            }
        }
    }).magnificPopup('open');
}