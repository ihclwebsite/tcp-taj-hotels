$(document).ready(function() {

    // Hide apply coupon code for voucher redemption
	handleCouponCodeField();

    var bOptions = dataCache.session.getData("bookingOptions");

    if (bOptions && bOptions.selection && (bOptions.selection.length > 0)) {
        checkAvailabilityInCartPage();
    }

    $('.price-warning-box-close').on("click", function() {
        $('.cart-price-diff-bar').hide('slow');
    });

});

function addAnotherRoom() {
    var dataBooking = dataCache.session.getData("bookingOptions");
    var bookingSelection = [];

    var roomcount = dataBooking.roomCount;
    for (var m = 0; m < dataBooking.selection.length; m++) {
        if (dataBooking.selection[m].soldout) {
            delete dataBooking.roomOptions[m].userSelection;
            roomcount = roomcount - 1
        } else {
            bookingSelection.push(dataBooking.selection[m]);
        }
    }
    dataBooking.roomCount = roomcount;
    dataBooking.selection = bookingSelection;
    dataCache.session.setData("bookingOptions", dataBooking);

    var guestSoldOutRoomPath = dataCache.session.getData("guestRoomPath") || "#";
    window.location.href = guestSoldOutRoomPath;
}

function checkAvailabilityInCartPage() {
    var cartPageBookingOptions = dataCache.session.getData("bookingOptions");
    var checkInDate = moment(cartPageBookingOptions.fromDate, "MMM Do YY").format("YYYY-MM-DD");
    var checkOutDate = moment(cartPageBookingOptions.toDate, "MMM Do YY").format("YYYY-MM-DD");
    var selection = cartPageBookingOptions.selection;
    var selectionHotelId;
    var roomDetailsList = [];
    for (var i = 0; i < selection.length; i++) {
        var roomDetail = {};
        var roomOccupants = {};
        roomOccupants["numberOfAdults"] = selection[i].adults;
        roomOccupants["numberOfChildren"] = selection[i].children;
        roomDetail["roomOccupants"] = roomOccupants;

        selectionHotelId = selection[i].hotelId;
        roomDetail["roomTypeCode"] = selection[i].roomTypeCode;
        roomDetail["ratePlanCode"] = selection[i].ratePlanCode;

        roomDetailsList.push(roomDetail);
    }

    $('body').showLoader(true);

    $.ajax({
        type : 'post',
        url : '/bin/bookCartAvailabilityServlet',
        data : 'hotelId=' + selectionHotelId + '&checkInDate=' + checkInDate + '&checkOutDate=' + checkOutDate
                + '&roomDetailsList=' + JSON.stringify(roomDetailsList),
        success : function(returnmessage) {
            $('body').hideLoader(true);
            if (returnmessage == undefined || returnmessage == "" || returnmessage.length == 0) {
                var popupParams = {
                    title : 'Availability Failed!',
                    description : 'Something went Wrong. Refreshing Page'
                }
                warningBox(popupParams);
            } else if (returnmessage.includes("success\":")) {
                maintainAjaxSuccess(returnmessage)
            } else {
                var warningPopupParams = {
                    title : 'Availability Failed!',
                    description : returnmessage,
                }
                warningBox(warningPopupParams);
            }
        },
        fail : function(rrr) {
            $('body').hideLoader(true);
            var popupParams = {
                title : 'Availability Failed!',
                description : 'Availability service calling failed. Please try again later.',

            }
            warningBox(popupParams);
        },
        error : function(xhr) {
            $('body').hideLoader(true);
            var popupParams = {
                title : 'Availability Error!',
                description : 'Error occured while calling  availability service. Please try aftersome time.',

            }
            $('body').hideLoader(true);
            warningBox(popupParams);
        }
    });
}

function maintainAjaxSuccess(response) {
    console.log('AJAX Response: ', response);
    var returnResponseJSON = JSON.parse(response);
	var voucherRedemption = dataCache.session.getData('voucherRedemption');
	if(voucherRedemption && voucherRedemption == 'true'){
		returnResponseJSON = makeAvailabilityAsPerVoucher(returnResponseJSON);
	}
    roomAvailabilityCheck(returnResponseJSON);
}

function roomAvailabilityCheck(responseJSON) {
    var falseCheck = false;
    var indexesOfRooms = [];
    for (var h = 0; h < responseJSON.length; h++) {
        if (responseJSON[h].success) {
            doIfRoomAvailabilityStatusTRUE(responseJSON[h], h);
            indexesOfRooms.push(false);
        } else {
            doIfRoomAvailabilityStatusFALSE(responseJSON[h], h);
            indexesOfRooms.push(true);
            falseCheck = true;
        }
    }

    var bookCache = dataCache.session.getData("bookingOptions");
    for (var d = 0; d < indexesOfRooms.length; d++) {
        bookCache.selection[d].soldout = indexesOfRooms[d];
    }
    dataCache.session.setData("bookingOptions", bookCache);

    if (falseCheck) {
        $('.summary-charges-div').hide();
        $('.summary-soldout-message').show();
        $('.cart-footer-fixed-total-price').hide();
        $('.total-prize-confirm').hide();
        $('.total-prize-con').hide();
        $('.total-price-deal-section').hide();
    }
    updateSummaryAndOther();
}

function updateSummaryAndOther() {

    var _globals = intGlobals();
    serveSummaryCard(_globals);
    showNightlyRates();
    showAllTypeOfTaxes();
}

function doIfRoomAvailabilityStatusTRUE(indResObject, indResObjectIndex) {
    var cartBookingOptions = dataCache.session.getData("bookingOptions");
    var indSelectionObj = cartBookingOptions.selection[indResObjectIndex];

    console.log("indSelectionObj.selectedRate: " + Number(indSelectionObj.selectedRate).toFixed()
            + ", indResObject.totalPrice: " + Number(indResObject.totalPrice).toFixed()
            + "|| indSelectionObj.totalTax: " + Number(indSelectionObj.roomTaxRate).toFixed()
            + ", indResObject.roomTaxRate: " + Number(indResObject.totalTax).toFixed());

    if ((Number(indSelectionObj.selectedRate).toFixed() != Number(indResObject.totalPrice).toFixed())
            || (Number(indSelectionObj.roomTaxRate).toFixed() != Number(indResObject.totalTax).toFixed())) {

        console.log("totalPrice or totalTax had a difference");
        var currentCard = $('.cart-selected-rooms-card')[indResObjectIndex];
        $(currentCard).find('.room-amount').text(roundPrice(Math.ceil(indResObject.totalPrice))).digits();
        updatingPriceDiffInCartRoom(indSelectionObj, indResObject, indResObjectIndex);
        $('.cart-price-diff-bar').show('slow');
        purgeRateCache();
    }
}

function purgeRateCache() {
    try {
        var bookingOptions = dataCache.session.getData("bookingOptions");
        if (bookingOptions) {
            var hotelId = bookingOptions.hotelId;
            if (hotelId) {
                $.ajax({
                    type : 'get',
                    url : '/bin/refreshRateCache',
                    data : 'hotelId=' + hotelId,
                    success : function(successData) {
                        console.log("RateCache has been refreshed successfully");
                    },
                    fail : function(failData) {
                        console.error("Failure while purging cache");
                    },
                    error : function(errData) {
                        console.error("Error occured while purging cache");
                    }
                });
            }
        }
    } catch (err) {
        console.error("Error occured while purging cache ", err);
    }
}

function updatingPriceDiffInCartRoom(indSelectionObj, indResObject, indResObjectIndex) {
    indSelectionObj.currencyString = indResObject.currencyString;
    indSelectionObj.dailyNightlyRate = roundPrice(indResObject.averagePrice);
    indSelectionObj.dailyNightlyRateWithTax = indResObject.averagePrice + indResObject.averageTax;
    indSelectionObj.nightlyRates = indResObject.nightlyRates;
    indSelectionObj.roomBaseRate = indResObject.totalPrice;
    indSelectionObj.roomTaxRate = indResObject.totalTax;
    indSelectionObj.selectedRate = indResObject.totalPrice;
    indSelectionObj.taxes = indResObject.taxes;
    var bookingOptionData = dataCache.session.getData("bookingOptions");
    bookingOptionData.selection[indResObjectIndex] = indSelectionObj;
    dataCache.session.setData("bookingOptions", bookingOptionData);

}

function doIfRoomAvailabilityStatusFALSE(indResObject, indResObjectIndex) {
    // deleteCartRoomAfterAvailabilityCheck(indResObjectIndex + 1);

    var currentCard = $('.cart-selected-rooms-card')[indResObjectIndex];
    $(currentCard).find('.cart-room-cost').text("Sold out");
    var soldoutRoomName = '<div class="soldout-room-name">' + $(currentCard).find('.cart-amenities-title').text()
            + '</div>';
    $('.summary-of-soldout-rooms').append(soldoutRoomName);
    $(currentCard).find('.cart-selected-info-wrp').text("");
    $(currentCard).find('.cart-selected-info-wrp').append(
            '<div class="soldout-room-name">' + $('.soldout-summary-info').text() + '<br><br></div>'
                    + '<div class="sold-out-addRoom" onclick="addAnotherRoom()">' + '<a href="#">'
                    + '<button class="cm-btn-primary sold-out-add-room-button">'
                    + $($('.sold-out-add-room-button')[0]).text() + '</button>' + '</a>' + '</div>');

}

function deleteCartRoomAfterAvailabilityCheck(deletingCardindex) {

    var _globals = intGlobals();

    var currenctCard = $('.cart-selected-rooms-card')[deletingCardindex];

    $(currenctCard).hide('slow', function() {
        var thisCardInitialRoomIndex = _globals.bookingSelections.selection[deletingCardindex].initialRoomIndex;
        droppingRoomFromCart(dataCache.session.getData("bookingOptions"), deletingCardindex);
        $('.cart-room-number:gt(' + deletingCardindex + ')').each(function() {
            var currentRoomNumber = $(this).find("span").text();
            --currentRoomNumber;
            $(this).find("span").html(currentRoomNumber);
        });
        $(this).remove();
        delete _globals.bookingSelections.roomOptions[thisCardInitialRoomIndex].userSelection;
        _globals.bookingSelections.selection.splice(deletingCardindex, 1);
        /*
         * _globals.bookingSelections.roomOptions.splice( deletingCardindex, 1 );
         */
        _globals.bookingSelections.roomCount = _globals.bookingSelections.roomCount - 1;

        updateAddRoomBtnVisibility(_globals);

        checkForEmptyCart(_globals);

        // Update the selections on the summary cards and
        // then browser cache from there
        serveSummaryCard(_globals);
        // dataCache.session.setData( "bookingOptions",
        // _globals.bookingSelections );
        showNightlyRates();
        showAllTypeOfTaxes();
    });
    addRoomButtonVisibility();
}


// handle voucher redemption prices
function makeAvailabilityAsPerVoucher(returnResponseJSON) {
	if(returnResponseJSON && returnResponseJSON.length){
		for(var i=0; i<returnResponseJSON.length; i++) {
			returnResponseJSON[i].averagePrice = 0;
			returnResponseJSON[i].totalPrice = 0;
			returnResponseJSON[i].averageDiscountedPrice = 0;
			returnResponseJSON[i].totalDiscountedPrice = 0;
			var nightlyRates = returnResponseJSON[i].nightlyRates;
			if(nightlyRates && nightlyRates.length) {
				for(let j=0; j<nightlyRates.length; j++){
					nightlyRates[j].price = 0;
					nightlyRates[j].priceWithFeeAndTax = nightlyRates[j].tax;
				}
			}
			returnResponseJSON[i].nightlyRates = nightlyRates;
		}
	}
	return returnResponseJSON;
}



// handle coupon code for voucher redemption flow
function handleCouponCodeField() {
	var voucherRedemption = dataCache.session.getData('voucherRedemption');
    if(voucherRedemption && voucherRedemption == 'true'){
		$('.coupon-hide-on-checkout-page').hide();
    }
}