
    $(document).ready(function() {
        investorDOMReady();

    });


function investorDOMReady() {
    var options = [{
        selector: "Select Financial Year",
        selectorValue: "Year",
        option: [
            { label: '2017-2018', value: '2017-2018' },
            { label: '2016-2017', value: '2016-2017' },
            { label: '2015-2016', value: '2015-2016' },
            { label: '2014-2015', value: '2014-2015' },
            { label: '2013-2014', value: '2013-2014' },
            { label: '2012-2013', value: '2012-2013' },
            { label: '2011-2012', value: '2011-2012' },
            { label: '2010-2011', value: '2010-2011' }
        ],
        selectedOptionList: null,
        onChange: function(list) {
            //do
        }
    }];
    var selectedData = {};
    var filterOptionsWrap = $('.filter-wrap-catagory');
    var filterOnSearchBar = $('.pressRoom-filter .search-filters-container');

    createFilterOptions(filterOptionsWrap, options);
    //amalgamtionCards();
    switchingPage();
    report_content(options);
    eventsOnFilter(filterOnSearchBar, filterOptionsWrap, options);

};


function eventsOnFilter(filterOnSearchBar, filterOptionsWrap, options) {
    $('.cm-search-with-filter .filter-landing-cont-image .only-icon').on('click', function() {
        $('.events-filter-subsection').css('display', 'block');
    });

    $('.press-room-filter-wrapper .filter-go-con,.press-room-filter-wrapper .cm-press-room-filter').on('click', function() {
        var filterResultWrapper = $('.mr-investors-report-documents-wrapper.content-active .mr-audit-statement-template');

    })

    $('.pressRoom-filter .searchbar-input').on("click", function() {
        filterOnSearchBar.addClass("cm-hide");
    })

    $('.trending-search .individual-trends').click(function() {
        if (filterOnSearchBar.length > 0) {
            filterOnSearchBar.toggleClass('cm-hide', false);
        }
    })
};


function switchingPage() {
    var switching = document.getElementById("mr-reports-wrapper-id");
    if (switching != null) {
        var division = switching.getElementsByClassName("rate-tab");
        for (var i = 0; i < division.length; i++) {
            division[i].addEventListener("click", function() {
                var currentdiv = document.getElementsByClassName("tab-selected");
                currentdiv[0].className = currentdiv[0].className.replace("tab-selected", "");
                this.className += " tab-selected";

            });
        }
    }
};

function filterReset(options) {
    $(document).trigger('multiselect:reset');
    $('.filter-wrap-catagory').find('.cm-multi-dd').each(function(i) {
        $(this).multiselectDDreset();
    });
    $('.mr-investors-report-documents-wrapper.content-active .mr-audit-statement-template').each(function() {
        $(this).toggleClass('cm-hide', false);
    })

    for (var i = 0; i < options.length; i++) {
        options[i].selectedOptionList = null;
    }

};


function report_content(options) {
    $('.rate-tab').click(function() {
        var current = $(this);
        var contentElement = current.parent().children(),
            position = contentElement.index(current);

        var targetWrapper = $('.mr-investors-report-documents-wrapper').eq(position);

        if (!targetWrapper.hasClass('content-active')) {
            $('.mr-investors-report-documents-wrapper.content-active').toggleClass('content-active', false);
            targetWrapper.toggleClass('content-active', true);
            filterReset(options);
        }

    });
};
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWludmVzdG9ycy10aXRsZS1jYXJkL2NvcnAtaW52ZXN0b3JzLXRpdGxlLWNhcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gaW5pdEludmVzdG9yc1BhZ2UoKSB7XHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuICAgICAgICBpbnZlc3RvckRPTVJlYWR5KCk7XHJcbiAgICB9KTtcclxufTtcclxuXHJcbmZ1bmN0aW9uIGludmVzdG9yRE9NUmVhZHkoKSB7XHJcblxyXG4gICAgdmFyIG9wdGlvbnMgPSBbe1xyXG4gICAgICAgIHNlbGVjdG9yOiBcIlNlbGVjdCBGaW5hbmNpYWwgWWVhclwiLFxyXG4gICAgICAgIHNlbGVjdG9yVmFsdWU6IFwiWWVhclwiLFxyXG4gICAgICAgIG9wdGlvbjogW1xyXG4gICAgICAgICAgICB7IGxhYmVsOiAnMjAxNy0yMDE4JywgdmFsdWU6ICcyMDE3LTIwMTgnIH0sXHJcbiAgICAgICAgICAgIHsgbGFiZWw6ICcyMDE2LTIwMTcnLCB2YWx1ZTogJzIwMTYtMjAxNycgfSxcclxuICAgICAgICAgICAgeyBsYWJlbDogJzIwMTUtMjAxNicsIHZhbHVlOiAnMjAxNS0yMDE2JyB9XHJcbiAgICAgICAgXSxcclxuICAgICAgICBzZWxlY3RlZE9wdGlvbkxpc3Q6IG51bGwsXHJcbiAgICAgICAgb25DaGFuZ2U6IGZ1bmN0aW9uKGxpc3QpIHtcclxuICAgICAgICAgICAgLy9kb1xyXG4gICAgICAgIH1cclxuICAgIH1dO1xyXG4gICAgdmFyIHNlbGVjdGVkRGF0YSA9IHt9O1xyXG4gICAgdmFyIGZpbHRlck9wdGlvbnNXcmFwID0gJCgnLmZpbHRlci13cmFwLWNhdGFnb3J5Jyk7XHJcbiAgICB2YXIgZmlsdGVyT25TZWFyY2hCYXIgPSAkKCcucHJlc3NSb29tLWZpbHRlciAuc2VhcmNoLWZpbHRlcnMtY29udGFpbmVyJyk7XHJcblxyXG4gICAgY3JlYXRlRmlsdGVyT3B0aW9ucyhmaWx0ZXJPcHRpb25zV3JhcCwgb3B0aW9ucyk7XHJcbiAgICBhbWFsZ2FtdGlvbkNhcmRzKCk7XHJcbiAgICBzd2l0Y2hpbmdQYWdlKCk7XHJcbiAgICByZXBvcnRfY29udGVudChvcHRpb25zKTtcclxuICAgIGV2ZW50c09uRmlsdGVyKGZpbHRlck9uU2VhcmNoQmFyLCBmaWx0ZXJPcHRpb25zV3JhcCwgb3B0aW9ucyk7XHJcblxyXG59O1xyXG5cclxuXHJcbmZ1bmN0aW9uIGV2ZW50c09uRmlsdGVyKGZpbHRlck9uU2VhcmNoQmFyLCBmaWx0ZXJPcHRpb25zV3JhcCwgb3B0aW9ucykge1xyXG4gICAgJCgnLmNtLXNlYXJjaC13aXRoLWZpbHRlciAuZmlsdGVyLWxhbmRpbmctY29udC1pbWFnZSAub25seS1pY29uJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJCgnLmV2ZW50cy1maWx0ZXItc3Vic2VjdGlvbicpLmNzcygnZGlzcGxheScsICdibG9jaycpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgJCgnLnByZXNzLXJvb20tZmlsdGVyLXdyYXBwZXIgLmZpbHRlci1nby1jb24sLnByZXNzLXJvb20tZmlsdGVyLXdyYXBwZXIgLmNtLXByZXNzLXJvb20tZmlsdGVyJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGZpbHRlclJlc3VsdFdyYXBwZXIgPSAkKCcubXItaW52ZXN0b3JzLXJlcG9ydC1kb2N1bWVudHMtd3JhcHBlci5jb250ZW50LWFjdGl2ZSAubXItYXVkaXQtc3RhdGVtZW50LXRlbXBsYXRlJyk7XHJcbiAgICAgICAgZ2xvYmFsTXVsdGlwbGVGaWx0ZXIoZmlsdGVyUmVzdWx0V3JhcHBlciwgb3B0aW9ucyk7XHJcbiAgICB9KVxyXG5cclxuICAgICQoJy5wcmVzc1Jvb20tZmlsdGVyIC5zZWFyY2hiYXItaW5wdXQnKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGZpbHRlck9uU2VhcmNoQmFyLmFkZENsYXNzKFwiY20taGlkZVwiKTtcclxuICAgIH0pXHJcblxyXG4gICAgJCgnLnRyZW5kaW5nLXNlYXJjaCAuaW5kaXZpZHVhbC10cmVuZHMnKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoZmlsdGVyT25TZWFyY2hCYXIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmaWx0ZXJPblNlYXJjaEJhci50b2dnbGVDbGFzcygnY20taGlkZScsIGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICB9KVxyXG59O1xyXG5cclxuXHJcbmZ1bmN0aW9uIHN3aXRjaGluZ1BhZ2UoKSB7XHJcbiAgICB2YXIgc3dpdGNoaW5nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtci1yZXBvcnRzLXdyYXBwZXItaWRcIik7XHJcbiAgICBpZiAoc3dpdGNoaW5nICE9IG51bGwpIHtcclxuICAgICAgICB2YXIgZGl2aXNpb24gPSBzd2l0Y2hpbmcuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInJhdGUtdGFiXCIpO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGl2aXNpb24ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgZGl2aXNpb25baV0uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRkaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwidGFiLXNlbGVjdGVkXCIpO1xyXG4gICAgICAgICAgICAgICAgY3VycmVudGRpdlswXS5jbGFzc05hbWUgPSBjdXJyZW50ZGl2WzBdLmNsYXNzTmFtZS5yZXBsYWNlKFwidGFiLXNlbGVjdGVkXCIsIFwiXCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGFzc05hbWUgKz0gXCIgdGFiLXNlbGVjdGVkXCI7XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn07XHJcblxyXG5mdW5jdGlvbiBmaWx0ZXJSZXNldChvcHRpb25zKSB7XHJcbiAgICAkKCcuZmlsdGVyLXdyYXAtY2F0YWdvcnknKS5maW5kKCcuY20tbXVsdGktZGQnKS5lYWNoKGZ1bmN0aW9uKGkpIHtcclxuICAgICAgICAkKHRoaXMpLm11bHRpc2VsZWN0RERyZXNldCgpO1xyXG4gICAgfSk7XHJcbiAgICAkKCcubXItaW52ZXN0b3JzLXJlcG9ydC1kb2N1bWVudHMtd3JhcHBlci5jb250ZW50LWFjdGl2ZSAubXItYXVkaXQtc3RhdGVtZW50LXRlbXBsYXRlJykuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdjbS1oaWRlJywgZmFsc2UpO1xyXG4gICAgfSlcclxuXHJcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG9wdGlvbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBvcHRpb25zW2ldLnNlbGVjdGVkT3B0aW9uTGlzdCA9IG51bGw7XHJcbiAgICB9XHJcblxyXG59O1xyXG5cclxuXHJcbmZ1bmN0aW9uIHJlcG9ydF9jb250ZW50KG9wdGlvbnMpIHtcclxuXHJcbiAgICAkKCcucmF0ZS10YWInKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgY3VycmVudCA9ICQodGhpcyk7XHJcbiAgICAgICAgdmFyIGNvbnRlbnRFbGVtZW50ID0gY3VycmVudC5wYXJlbnQoKS5jaGlsZHJlbigpLFxyXG4gICAgICAgICAgICBwb3NpdGlvbiA9IGNvbnRlbnRFbGVtZW50LmluZGV4KGN1cnJlbnQpO1xyXG5cclxuICAgICAgICB2YXIgdGFyZ2V0V3JhcHBlciA9ICQoJy5tci1pbnZlc3RvcnMtcmVwb3J0LWRvY3VtZW50cy13cmFwcGVyJykuZXEocG9zaXRpb24pO1xyXG5cclxuICAgICAgICBpZiAoIXRhcmdldFdyYXBwZXIuaGFzQ2xhc3MoJ2NvbnRlbnQtYWN0aXZlJykpIHtcclxuICAgICAgICAgICAgJCgnLm1yLWludmVzdG9ycy1yZXBvcnQtZG9jdW1lbnRzLXdyYXBwZXIuY29udGVudC1hY3RpdmUnKS50b2dnbGVDbGFzcygnY29udGVudC1hY3RpdmUnLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIHRhcmdldFdyYXBwZXIudG9nZ2xlQ2xhc3MoJ2NvbnRlbnQtYWN0aXZlJywgdHJ1ZSk7XHJcbiAgICAgICAgICAgIGZpbHRlclJlc2V0KG9wdGlvbnMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9KTtcclxufTsiXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NvcnAtaW52ZXN0b3JzLXRpdGxlLWNhcmQvY29ycC1pbnZlc3RvcnMtdGl0bGUtY2FyZC5qcyJ9
