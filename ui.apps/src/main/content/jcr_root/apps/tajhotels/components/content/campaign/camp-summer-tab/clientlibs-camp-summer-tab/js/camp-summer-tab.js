$(document).ready(function() {
    CampPackageTabs();
});

function CampPackageTabs() {
    $(".rate-tab ").eq(0).addClass("tab-selected");
    $(".package-rate-tab-content-wrapper .package-rate-tab-content.container").eq(0).css("display", "block");
    $(".package-rate-tab-content-wrapper .package-rate-tab-content").not(".container").eq(0).css("display", "block");
    $(".rate-tab").click(function() {
        var a = $(this).parent().parent().find(".package-rate-tab-content-wrapper").eq(0).children().not(".container");
        var b = $(this).parent().parent().find(".package-rate-tab-content-wrapper").eq(0).children();

        $(this).siblings().removeClass("tab-selected");
        $(this).addClass("tab-selected");
        a.each(function() {
            $(this).hide()
        });
        b.each(function() {
            $(this).hide()
        });
        $(a[$(this).index()]).show();
        $(b[$(this).index()]).show()
    })
};

