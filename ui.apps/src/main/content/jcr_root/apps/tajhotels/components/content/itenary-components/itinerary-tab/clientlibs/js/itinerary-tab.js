function initializePopup() {
    var id = $('.popup-comp-body').data('popup-id');
    var presentScroll
    $('#' + id).on('taj:popup-show', function() {
        presentScroll = $(window).scrollTop();
        $(this).removeClass('d-none');

        $(".cm-page-container").addClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');
        $(document).keydown(function(e) {
            if (($('#' + id + ':visible').length) && (e.which === 27)) {
                $('#' + id).trigger("taj:popup-hide");
            }
        });
    });

    $('#' + id).on('taj:popup-hide', function() {
        $(this).addClass('d-none');
        $(".cm-page-container").removeClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
        $(window).scrollTop(presentScroll);
    });

    $('#' + id).on('click', '.popup-comp-close', function() {
        $('#' + id).trigger('taj:popup-hide')
        $('#' + id).addClass('d-none');

    });
}
