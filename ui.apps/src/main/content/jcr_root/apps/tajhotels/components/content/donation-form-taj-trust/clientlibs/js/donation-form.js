$(document).ready( function() {
	console.log('inside document ready');
    orderGiftHamper();
	$('.donate-now-scroll').click(function(){
		document.getElementById("donationForm").scrollIntoView();
	});
});

function orderGiftHamper() {
    var sessionToken;
    getSessionToken().then(function(result){
		sessionToken = result;
    });
    $('.gift-hamper-payment-button').click(
            function(e) {
            	console.log('clicked');
                console.log('sessionToken',sessionToken.sessionToken);
                var inputValid = giftHamperInputValidation();
                var isGDPRCompliance = $('[data-form-id="guest-GDPRCompliance"]').is(":checked");
                if (inputValid && captchaValidation) {
                	console.log("valid")
                    var popupParams = {};
                    popupParams.title = $("#GiftHamperPopUPMessages").attr("data-hamper-failure-title")
                            || 'Donation failed!';
                    $('body').showLoader();
                    var senderDetail = extractGiftHamperDetails("donationForm");

					console.log("senderdetail", senderDetail);

                    var senderDetails = JSON.stringify(senderDetail);

                    var encodedDetail = btoa(senderDetails);

                    console.log("Encoded senderdetail", senderDetails);

                    var senderDetailsEncoded = encodeURIComponent(senderDetails);

                    var encrypt = new JSEncrypt();
                    encrypt.setPublicKey(sessionToken.sessionToken);
                    var ciphertext = encrypt.encrypt(senderDetails);

                    console.log("senderDetails", senderDetails);

                    console.log("ciphertext", ciphertext);

                    setCookie('senderDetails', encodedDetail, 1);
                    $.ajax({
                        type : 'post',
                        url : '/bin/Donation',
                       // type : 'post',
                        //url : '/bin/donationConfirmation',
                        data : 'senderDetails=' + ciphertext,
                        success : function(successResponse) {
                            console.log("successResponse: ", successResponse);
                            if (successResponse == undefined || successResponse == "" || successResponse.length == 0) {
                                $('body').hideLoader();
                                popupParams.description = 'Something went Wrong. Refreshing Page';
                                warningBox(popupParams);
                                //location.reload();
                            } else {
                                //successResponse = JSON.parse(successResponse);
                                if (successResponse.status) {
                                    $('body').hideLoader();
                        			var ccAvenueData = successResponse.CCAvenueData;
                                    var $form = $(ccAvenueData.CCAvenueForm);
                                    $('body').append($form);
                                    $form.submit();
                                } else {
                                    $('body').hideLoader();
                                    popupParams.description = successResponse.message;
                    				warningBox(popupParams);
                                }

                            }
                        },
                        fail : function(failedResponse) {
                            $('body').hideLoader();
                            console.log("failedResponse: ", failedResponse);
                            popupParams.description = 'Donation failed. Please try again later.';
                            warningBox(popupParams);

                        },
                        error : function(errorResponse) {
                            $('body').hideLoader();
                            console.log("errorResponse: ", errorResponse);
                            popupParams.description = 'Donation interrupted. Please try again later.';
                            warningBox(popupParams);
                            //location.reload();
                        }
                    });
                } else {
                	console.log("not valid");
                }
            });
}

function giftHamperInputValidation() {
    var validationResult = true;
    var $giftHamperInputElems = $('form.gift-hamper-form').find('input.sub-form-mandatory');
    $giftHamperInputElems.each(function() {
        if ($(this).val() == "") {
            $(this).addClass('invalid-input');
            validationResult = false;
            invalidWarningMessage($(this));
        }
    });
    if ($giftHamperInputElems.hasClass('invalid-input')) {
        var $firstInvalidElem = $('form.gift-hamper-form').find('.invalid-input').first();
        ($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();
        validationResult = false;

    }
    return validationResult;
}

function extractGiftHamperDetails(formType) {
    var data = $("#" + formType);
    var d = new Date();
    return {
        "firstName" : $(data).find('input[name=guestFirstName]').val() || "",
        "lastName" : $(data).find('input[name=guestLastName]').val() || "",
        "email" : $(data).find('input[name=guestEmail]').val() || "",
        "city" : $(data).find('input[name=guestCity]').val() || "",
        "pincode" : $(data).find('input[name=guestPincode]').val() || "",
        "country" : "India",
        "donation" : $(data).find('input[name=amountToDonate]').val() || "",
        "panCardNumber" : $(data).find('input[name=guestPanCard]').val() || "",
        "address1" : $(data).find('input[name=guestAddressLine1]').val() || "",
        "address2" : $(data).find('input[name=guestAddressLine2]').val() || "",
		"isGDPRCompliance" : $('[data-form-id="guest-GDPRCompliance"]').is(":checked") || false,
        "timeStamp":d.getTime(),
    }

}
var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getSessionToken(){

    return new Promise(function(resolve,reject){
     $.ajax({
                        type : 'get',
                        url : '/bin/Session',
                        success : function(successResponse) {
                            console.log('successResponse',successResponse);
                            sessionToken=successResponse;
        					resolve(successResponse);
                        },
                        fail : function(failedResponse) {
                            $('body').hideLoader();
                            console.log("failedResponse: ", failedResponse);
                            warningBox(popupParams);

                        },
                        error : function(errorResponse) {
                            $('body').hideLoader();
                            console.log("errorResponse: ", errorResponse);
                            warningBox(popupParams);
                            //location.reload();
                        }

		});
});
}

