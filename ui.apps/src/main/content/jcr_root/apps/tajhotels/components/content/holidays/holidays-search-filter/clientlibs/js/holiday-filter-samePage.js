var $alldestCard = [];
var $noResult;
var $individualDestinationCards = [];

$(document).ready(function() {
    $noResult = $('#dest-no-result');
    if ($('.cm-page-container').hasClass('weddings-list')) {
        $('.weddings-wrap-spec').each(function() {
            $(this).showMore(12, 9);
        });
        // keep all initial card in one variable
        $('.weddings-wrap-spec').each(function() {
            $alldestCard.push($(this).find('.events-card-wrap').clone(true));
        });

        $('.destination-package-card-container').each(function(index) {
            $individualDestinationCards.push($(this).clone("true"));
        });
        if ($('.cm-page-container').hasClass('weddings-list')) {
            filterOnWedding();
        }
        $('.jiva-spa-show-more').hide();
    } else {
        $('.popular-destination-wrap-spec').each(function() {
            $(this).showMore(12, 9);
        });
        // keep all initial card in one variable
        $('.popular-destination-wrap-spec').each(function() {
            $alldestCard.push($(this).find('.popularDest-wrap').clone(true));
        });

        $('.destination-package-card-container').each(function(index) {
            $individualDestinationCards.push($(this).clone("true"));
        });
    }
});

function filterIndividualPackages($hotelWrapper) {
    var $packageWrapper = $hotelWrapper.find('.holidays-dest-more-packages');
    var packages = $packageWrapper.find('.more-rate-card-container');
    globalMultipleFilterLocal(options, $packageWrapper, packages, '.more-rate-card-container');
}

function filterIndividualHotels($destinationWrapper) {
    var $hotelsOfDestination = $destinationWrapper.find('.holidays-dest-package-card-outer-wrap');
    $hotelsOfDestination.each(function() {
        filterIndividualPackages($(this));
        var $packages = $(this).find('.more-rate-card-container');
        if ($packages.length == 0) {
            $(this).remove();
        } else {
            var minRate = Number.POSITIVE_INFINITY;
            $packages.each(function() {
                var packagePrice = parseFloat($(this).data('price'));
                if (packagePrice < minRate) {
                    minRate = packagePrice;
                }
            });
            $(this).find('.holidays-dest-package-card-rate-actual .amount').text(minRate.toLocaleString('en-IN'));
        }
    });
    return $destinationWrapper;
}

function filterIndividualDestination() {
    var totalFilterResults = 0;
    $($individualDestinationCards).each(function(index) {
        var $currentWrapper = $('.destination-package-card-container').eq(index);
        var $currentWrapperContainer = $currentWrapper.parent();
        var $currentTitleWrapper = $currentWrapperContainer.prev('.title-link');
        var $filteredWrapper = filterIndividualHotels($(this).clone("true"));
        $currentWrapper.children('.row').replaceWith($filteredWrapper.children('.row'));
        $currentWrapper.find('.holidays-dest-package-card-outer-wrap').show();
        var resultsHotelsCount = $currentWrapper.find('.holidays-dest-package-card-outer-wrap').length;
        $currentWrapperContainer.toggle(resultsHotelsCount > 0);
        $currentTitleWrapper.toggle(resultsHotelsCount > 0);
    })
    if (options[5] && options[5].selectedOptionList) {
        filterDestinations(options[5].selectedOptionList);
    }
    totalFilterResults += $('.more-rate-card-container').length;
    $("#holidaysFilterNoResults").toggle(totalFilterResults == 0);
}

function filterOnDestination(options) {
    try {
        var totalFilterResults = 0;
        if (!options) {
            options = window.options;
        }
        if ($alldestCard.length == 0) {
            $('.popular-destination-wrap-spec').each(function() {
                $alldestCard.push($(this).find('.popularDest-wrap').clone(true));
            });
        }
        var $popularDestSpecWrp = $('.popular-destination-wrap-spec');
        if ($popularDestSpecWrp.length) {
            $popularDestSpecWrp.each(function(index) {
                globalMultipleFilterLocal(options, $(this), $alldestCard[index], '.popularDest-wrap');
                var destinationsInCountry = $(this).find('.popularDest-wrap').length;
                $(this).closest('.holiday-destinations').toggle(destinationsInCountry > 0);
                $(this).closest('.holiday-destinations').prev('.cmp-title').toggle(destinationsInCountry > 0);
            });
            totalFilterResults += $('.popularDest-wrap').length;
            $("#holidaysFilterNoResults").toggle(totalFilterResults == 0);
        }
    } catch (error) {
        console.error("Error in holiday filter ", error);
    }
}

function globalMultipleFilterLocal(options, $cardsWrapper, $cardsList, cardSelector) {
    $cardsWrapper.next('.jiva-spa-show-more').remove();
    $cardsWrapper.children('.row').empty();

    var selectedData = {};

    for (var i = 0; i < options.length; i++) {
        if (options[i].selectedOptionList && options[i].selectedOptionList.length > 0)
            selectedData[options[i].selectorCode] = options[i].selectedOptionList;
    }
    $cardsList.each(function() {
        var match;
        if (Object.keys(selectedData).length == 0) {
            $cardsWrapper.children('.row').append($(this).removeAttr('style'));
        } else {
            try {
                var data = JSON.parse($(this)[0].dataset.key);
                for ( var key in selectedData) {
                    if (match == false)
                        break;
                    else {
                        if (key.toLowerCase() == "season") {
                            match = seasonFilter(data[key.toLowerCase()], selectedData);
                        } else if (key.toLowerCase() == "price") {
                            var priceList = data[key.toLowerCase()].split(',');
                            $(priceList).each(function() {
                                match = priceFilter(parseFloat(this), selectedData, match);
                                if (match)
                                    return false;
                            })
                            // match=priceFilter(data[key.toLowerCase()],selectedData,match);
                        } else if (key.toLowerCase() == "stay") {
                            match = stayFilter(data[key.toLowerCase()], selectedData.stay);

                        } else {
                            if (key.toLowerCase() == 'destination') {
                                match = true;
                                break;
                            } else {
                                for (var j = 0; j < selectedData[key].length; j++) {
                                    if (data[key.toLowerCase()].indexOf(selectedData[key][j]) > -1) {
                                        match = true;
                                        break;
                                    } else if (j == selectedData[key].length - 1) {
                                        match = false;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (error) {
                console.error(error);
            }
            // after for
            if (match == true) {
                $cardsWrapper.children('.row').append($(this).removeAttr('style'));
            }
        }
    });

    selectedData = {};
    if (cardSelector != ".more-rate-card-container")
        $cardsWrapper.showMore(12, 9);
}

function stayFilter(nights, stay) {
    var nightsArray = nights.split(',');
    var nightsLength = nightsArray.length;
    var nightsStatus = false;
    $(stay).each(function() {
        var stayArray = this.split(',');
        var stayLength = stayArray.length;
        if (nightsLength > 0 && stayLength > 0) {
            var startNight = parseInt(nightsArray[0]);
            var endNight = parseInt(nightsArray[nightsLength - 1]);
            for (var itr = startNight; itr <= endNight; itr++) {
                if (itr >= parseInt(stayArray[0]) && itr <= parseInt(stayArray[stayLength - 1])) {
                    nightsStatus = true;
                    break;
                    // return false is to break the each loop
                    return false;
                }
            }
        }
    });
    return nightsStatus;
}

function filterDestinations(destinations) {
    var fLength = destinations.length;
    var $titleLinkTitle = $('.holidays-dest-popularDest');
    $titleLinkTitle
            .each(function() {
                var filterDestination = false;
                var packagesFilteredCount = $(this).parent().next('.holidays-hotel-list').find(
                        '.more-rate-card-container').length;
                if (packagesFilteredCount > 0) {
                    for (var i = 0; i < fLength; i++) {
                        if (destinations[i] == $(this).find('.cm-header-label').text()) {
                            filterDestination = true;
                            break;
                        }
                    }
                }
                $(this).parent().toggle(filterDestination)
                $(this).parent().next('.holidays-hotel-list').toggle(filterDestination);
            });
}

function seasonFilter(season, selectedData) {
    var filterSeason = false;
    if (season == "")
        return false;
    if (selectedData['season']) {
        selectedData['season'].forEach(function(item) {
            if (item.toLowerCase() == season.toLowerCase())
                filterSeason = true;
        })
    }
    return filterSeason;
}

function priceFilter(priceOncard, selectedData, match) {
    var price = priceOncard;
    for (var j = 0; j < selectedData["price"].length; j++) {
        if (selectedData["price"][j] == "below") {
            if (price <= 39999) {
                return true;
            }
        } else if (selectedData["price"][j] == "between") {
            if (price >= 40000 && price <= 80000) {
                return true;
            }
        } else {
            if (price >= 80000) {
                return true;
            }
        }
    }
    return false;
}

// filter intitiate on destination page
function filterSetOnDestFromLand(options) {
    filterOnDestination(options);
    // globalMultipleFilterLocal(options,$('.popular-destination-wrap-spec'), $alldestCard , '.popularDest-wrap');
}

function filterOnWedding() {
    $('#holidaysFilterNoResults').hide();
    var selectedWeddingFilter = [];
    $('.multiselect-container .active input').each(function() {
        selectedWeddingFilter.push($(this).val());
    });
    $('.weddings-wrap-spec .events-card-wrap').each(function() {
        var cardData = $(this).attr('data-venuetypes');
        var currentWeddingCard = $(this);
        var capacityFlag;
        var countryFlag;
        var weddingFlag = 0;
        var _this = $(this);
        if (selectedWeddingFilter) {
            weddingFlag = 1;
            capacityFlag = filterOnCapacity($(this));
            countryFlag = filterOnCountry($(this));
        }

        if (weddingFlag && capacityFlag && countryFlag) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function filterOnCapacity(_this) {
    var selectedCapacityFilter = [];
    var capacityFlag = 0;
    $('#Max .multiselect-container .active input').each(function() {
        selectedCapacityFilter.push($(this).val());
    });
    selectedCapacityFilter.forEach(function(capacityFilter) {
        var cardCapacity = _this.attr('data-capacity');
        if (cardCapacity) {
            var lowerLimit = 0, upperLimit = 100;
            if (capacityFilter.includes('-')) {
                var filterValues = capacityFilter.split('-');
                lowerLimit = filterValues[0];
                upperLimit = filterValues[1];
            } else if (capacityFilter.includes('+')) {
                var filterValues = capacityFilter.split('+');
                upperLimit = filterValues[0];
            }
            if (Number(cardCapacity) >= Number(lowerLimit) && Number(cardCapacity) <= Number(upperLimit)) {
                capacityFlag++;
            }
        }
    });
    if (selectedCapacityFilter.length == 0) {
        capacityFlag = 1;
    }
    return capacityFlag;
}

function filterOnCountry(_this) {
    var selectedCountryFilter = [];
    var countryFlag = 0;
    $('#Country .multiselect-container .active input').each(function() {
        selectedCountryFilter.push($(this).val());
    });
    selectedCountryFilter.forEach(function(countryFilter) {
        if (_this.attr('data-location') && _this.attr('data-location').includes(countryFilter.toLowerCase())) {
            countryFlag++;
        }
    });
    if (selectedCountryFilter.length == 0) {
        countryFlag = 1;
    }
    return countryFlag;
}
