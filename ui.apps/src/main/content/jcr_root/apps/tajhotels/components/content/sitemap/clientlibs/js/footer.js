window.addEventListener('load', function() {
    $('.footer-destination-expand-button').click(function() {
        if ($(this).text() == '+') {
            $('.footer-destination-list').slideDown(100);
            $(this).text('-');
        } else {
            $(this).text('+');
            $('.footer-destination-list').slideUp(100);
        }
    })

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    $('footer .email-input').blur(function() {
        var email = $(this).val();
        if (!isEmail(email)) {
            $(this).css('border', 'solid 1px var(--primaryColor)');
        } else {
            $(this).css('border', 'none');
        }
    });
});
