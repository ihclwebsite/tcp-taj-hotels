$(document).ready(function() {
    contactUsMiscellaneous();
});

function contactUsMiscellaneous() {
    $('.contact-us-miscellaneous-wrapper .contact-us-container').each(function() {
        var misDetails = $(this).find(".contact-us-miscellaneous-details-wrapper");
        var misHeader = $(this).find(".contact-us-miscellaneous-header-wrapper");
        $(this).click(function() {
            if (misHeader.hasClass("open")) {

                $(this).find(".image-view").removeClass("up");
                misDetails.slideUp(200);
                misDetails.removeClass('open');
                misHeader.removeClass('open');
            } else {
                misDetails.slideDown(200);
                $(this).find(".image-view").addClass("up");
                misHeader.addClass('open');
            }
        })
    })
}
