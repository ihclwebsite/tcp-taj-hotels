// close cancel reservation popup
function handleClosePopup() {
	$('#confirmReservationCancel').addClass('d-none');
}

function cancelReservation() {
    //var cancelObj = JSON.parse(dataCache.session.getData('cancelResponse'));
    var cancelStr = dataCache.session.getData('bookingDetailsRequest');
    var redirect = document.getElementById('redirectUrl').value + ".html";
    if(cancelStr) {
        var cancelJson = JSON.parse(cancelStr);
        handleClosePopup();
        showLoader();
        var popupParams = {
            title : 'Unable to cancel the reservation. Please try after sometime.',
            callBack : '',
            needsCta : false,
            isWarning : true
        }
        $.ajax({
            type : 'POST',
            url : '/bin/cancelReservation',
            data : {
                'itineraryNumber' : cancelJson.itineraryNumber,
                'checkedRooms' : cancelJson.checkedRooms,
                'emailAddress' : cancelJson.userId,
                'hotelID' : cancelJson.hotelId,
                'cancelJson' : cancelStr
            },
            success : function(data) {
                if (!data.success) {
					popupParams.title = "Unable to cancel transaction. Please contact our customer support.";
                	warningBox(popupParams);

                } else if (data.success) {
                    dataCache.session.setData('bookingDetailsRequest', JSON.stringify(data));
					popupParams.title = 'Reservation has been cancelled successfully.'
					popupParams.isWarning = false;
                	warningBox(popupParams);
                    $('.warning-box-close').hide();
                    $('.warning-icon-con').hide();
                    $('.cm-warning-box-inner-wrap').append("<button class='btn cm-btn-secondary' onclick='redirectToPage()'>Continue</div>");
                }
                hideLoader();
            },
            failure : function(data) {
                hideLoader();
                console.error('failure function in ajax call');
                warningBox(popupParams);
            }
        });
        
    }
    else {
        console.log('data for cancel not present');
    }
}


function redirectToPage() {
	window.location.href = $("[data-cancel-redirect]").data('cancel-redirect') + ".html";
}