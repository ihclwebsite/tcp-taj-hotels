$(document)
        .ready(
                function() {
                    orderGiftHamper();
                    var giftHamperResponse;
                    if (sessionStorage.getItem('giftHamperResponseString')) {
                        var sessionRequest = sessionStorage.getItem('giftHamperResponseString');
                        dataCache.session.setData('giftHamperResponseString', sessionRequest);
                        sessionStorage.removeItem('giftHamperResponseString');
                        giftHamperResponse = JSON.parse(dataCache.session.getData('giftHamperResponseString'));
                    }

                    if (giftHamperResponse) {
                        if (giftHamperResponse.paymentStatus) {
                            giftHamperCreationStatusPopup(giftHamperResponse);
                        } else {
                            var ghPopupParams = {
                                title : $("#GiftHamperPopUPMessages").attr("data-hamper-payment-failed-title")
                                        || 'Payment Transaction Failed!',
                                description : $("#GiftHamperPopUPMessages").attr("data-hamper-payment-failed")
                                        || 'It appears the transaction has failed at the payment gateway. In case the amount has been debited it will be refunded back to your payment method automatically within 3 working days. For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .',
                            }
                            warningBox(ghPopupParams);
                        }
                    }

                    var subTotalAmount = $('.gift-hamper-popup').attr("data-hamper-original-price");
                    var shippingAmount = $('.gift-hamper-popup').attr("data-hamper-shipping-price");
                    var totalAmount = parseInt(subTotalAmount);
                    if (shippingAmount) {
                        totalAmount += parseInt(shippingAmount);
                    }
                    $('.totalAmount').text(totalAmount);

                });

function giftHamperCreationStatusPopup(giftHamperResponse) {
    var popupParams = {};
    if (giftHamperResponse.status) {
        popupParams.title = $("#GiftHamperPopUPMessages").attr("data-hamper-success-title")
                || 'Order placed successfully';
        var ghSuccessMessage = $("#GiftHamperPopUPMessages").attr("data-hamper-success")
                || 'Taj Gift hamper confirmation mail is on its way to your inbox and should be delivered shortly. Your payment tracking ID is $CCAvenueTrackingId$. For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .';
        popupParams.description = ghSuccessMessage.replace("$CCAvenueTrackingId$",
                giftHamperResponse.ccAvenueTrackingId);
    } else {
        popupParams.title = $("#GiftHamperPopUPMessages").attr("data-hamper-failure-title")
                || 'Gift hamper order failed!';
        var ghFailureMessage = $("#GiftHamperPopUPMessages").attr("data-hamper-failure")
                || 'It appears the gift hamper transaction has failed. Confirmation mail will be sent to your inbox within 3 working days. Your payment tracking ID is $CCAvenueTrackingId$. In case the amount you would prefer a refund, contact us to have the refund processed to your payment method within 3 working days. For further assistance contact support team at reservations@tajhotels.com / 1800 111 825 .';

        popupParams.description = ghFailureMessage.replace("$CCAvenueTrackingId$",
                giftHamperResponse.ccAvenueTrackingId);
    }
    warningBox(popupParams);
}

function orderGiftHamper() {
    $('.gift-hamper-payment-button').click(
            function(e) {
                var inputValid = giftHamperInputValidation();
                if (inputValid) {
                    var popupParams = {};
                    popupParams.title = $("#GiftHamperPopUPMessages").attr("data-hamper-failure-title")
                            || 'Gift hamper order failed!';
                    $('body').showLoader();
                    var senderDetails = extractGiftHamperDetails("senderForm");
                    var shippingDetails = extractGiftHamperDetails("shippingForm");
                    shippingDetails.state = "Maharashtra";
                    shippingDetails.country = "India";
                    shippingDetails.delivery = true;
                    var specialInstructions = $($("#senderForm")).find('input[name=specialInstructions]').val() || '';
                    var yourMessage = $($("#shippingForm")).find('input[name=yourMessage]').val() || '';
                    var additionalValues = specialInstructions + "_giftHamperAdditionalMessage_" + yourMessage;

                    var subAccountID = $('#subAccountID').attr("data-gift-hamper-subAccountID")
                    var hotelId = $($.find('[data-hotel-id]')).data().hotelId;
                    $.ajax({
                        type : 'post',
                        url : '/bin/giftHamperServlet',
                        data : 'senderDetails=' + JSON.stringify(senderDetails) + "&shippingDetails="
                                + JSON.stringify(shippingDetails) + '&hotelId=' + hotelId + '&subAccountID='
                                + subAccountID,
                        success : function(successResponse) {
                            console.log("successResponse: " + successResponse);
                            setFormCookieValue("formadditionaldetails", additionalValues, 1);
                            if (successResponse == undefined || successResponse == "" || successResponse.length == 0) {
                                $('body').hideLoader();
                                popupParams.description = 'Something went Wrong. Refreshing Page';
                                warningBox(popupParams);
                                location.reload();
                            } else {
                                successResponse = JSON.parse(successResponse);
                                if (successResponse.status) {
                                    $('body').hideLoader();
                                    var $form = $(successResponse.CCAvenueForm);
                                    $('body').append($form);
                                    $form.submit();
                                } else {
                                    $('body').hideLoader();
                                    popupParams.description = successResponse.message;
                                    warningBox(popupParams);
                                }
                            }
                        },
                        fail : function(failedResponse) {
                            $('body').hideLoader();
                            console.log("failedResponse: " + failedResponse);
                            popupParams.description = 'Gift hamper order failed. Please try again later.';
                            warningBox(popupParams);

                        },
                        error : function(errorResponse) {
                            $('body').hideLoader();
                            console.log("errorResponse: " + errorResponse);
                            popupParams.description = 'Gift hamper order interrupted. Please try again later.';
                            warningBox(popupParams);
                            location.reload();
                        }
                    });

                    function setFormCookieValue(cname, cvalue, exdays) {

                        var d = new Date();
                        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                        var expires = "expires=" + d.toGMTString();
                        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                    }
                }
            });
}

function giftHamperInputValidation() {
    var validationResult = true;
    var $giftHamperInputElems = $('form.gift-hamper-form').find('input.sub-form-mandatory');
    $giftHamperInputElems.each(function() {
        if ($(this).val() == "") {
            $(this).addClass('invalid-input');
            validationResult = false;
            invalidWarningMessage($(this));
        }
    });
    if ($giftHamperInputElems.hasClass('invalid-input')) {
        var $firstInvalidElem = $('form.gift-hamper-form').find('.invalid-input').first();
        ($firstInvalidElem.closest('.sub-form-input-wrp'))[0].scrollIntoView();
        validationResult = false;

    }
    return validationResult;
}

function extractGiftHamperDetails(formType) {
    var data = $("#" + formType);
    return {
        "title" : $(data).find('#guestTitle option:selected').text() || "",
        "firstName" : $(data).find('input[name=guestFirstName]').val() || "",
        "lastName" : $(data).find('input[name=guestLastName]').val() || "",
        "email" : $(data).find('input[name=guestEmail]').val() || "",
        "phoneNumber" : $(data).find('input[name=guestPhoneNumber]').val() || "",
        "address" : $(data).find('input[name=guestAddress]').val() || "",
        "city" : $(data).find('input[name=guestCity]').val() || "",
        "pincode" : $(data).find('input[name=guestPincode]').val() || "",
        "state" : $(data).find('input[name=guestState]').val() || "",
        "country" : $(data).find('input[name=guestCountry]').val() || "",
        "code" : $('.gift-hamper-popup').attr("data-hamper-code") || "",
        "shippingAmount" : $('.gift-hamper-popup').attr("data-hamper-shipping-price") || "",
        "subTotalAmount" : $('.gift-hamper-popup').attr("data-hamper-original-price") || "",
        "pickup" : $(data).find('#PICKUP').is(':checked'),
        "delivery" : $(data).find('#DELIVERY').is(':checked')
    }

}
