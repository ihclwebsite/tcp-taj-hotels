function validateInputFields() {
    var flag = true;
    $('.mr-ihcl-login-details-popup-overlay .sub-form-mandatory').each(function() {
        if ($(this).val() == "" || $(this).hasClass('invalid-input')) {
            $(this).addClass('invalid-input');
            flag = false;
            invalidWarningMessage($(this));
        }
    });
    $("#termAndCond").prop('checked') ? $(".ihcl-terms-warning").hide() : $(".ihcl-terms-warning").show();
    return flag && $("#termAndCond").prop('checked');

}
$(document).ready(
        function() {
            var loggedIn = checkLoggedInStatus('ihclOwnersUser');
            if (loggedIn)
                $('.ihcl-login').hide();
            else {
                var presentScroll = $(window).scrollTop();
                $(".cm-page-container").addClass('prevent-page-scroll');
                $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');
                $("#ihcl-login-btn").click(
                        function() {
                            if (validateInputFields()) {
                                $.ajax({
                                    type : "POST",
                                    url : "/bin/ihcl-security-check",
                                    data : {
                                        j_username : $("#ihcl-login-email").val(),
                                        j_password : $("#ihcl-login-pass").val()
                                    // , j_validate : "true"
                                    },
                                    success : function(data, textStatus, jqXHR) {
                                        if (data == "valid") {
                                            console.log("Valid Username Password");
                                            // alert("success"+textStatus);
                                            $(".mr-ihcl-login-details-popup-overlay").hide();
                                            $(".cm-page-container").removeClass('prevent-page-scroll');
                                            var ihclOwnersUser = {
                                                username : $("#ihcl-login-email").val(),
                                                loggedIn : true
                                            }
                                            setCookie('ihclOwnersUser', ihclOwnersUser, 1)

                                        } else {
                                            console.log("Invalid Username Password in fail");
                                            $('.mr-ihcl-login-details-popup-overlay .sub-form-mandatory.invalid-input')
                                                    .eq(0).focus();
                                        }

                                    },
                                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                                        console.log("InValid Username Password");
                                        $(".ihcl-invalid-email-warning").show();
                                        $("#ihcl-login-pass").val("");
                                        $("#ihcl-login-email").focus();
                                    }
                                });

                            } else {
                                console.log("Valid Username Password overall");
                                $('.mr-ihcl-login-details-popup-overlay .sub-form-mandatory.invalid-input').eq(0)
                                        .focus();
                            }
                        });

                $(".ihcl-terms-and-cond-popup").click(
                        function() {
                            var presentScroll = $(window).scrollTop();
                            var thisLightBox = $(".mr-ihcl-terms-wrp-details-popup-overlay").css('display', 'block')
                                    .addClass('active-popUp');
                            $(".cm-page-container").addClass('prevent-page-scroll');
                            $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');

                            $(document).keydown(function(e) {
                                if (($('.active-popUp').length) && (e.which === 27)) {
                                    $('.showMap-close').trigger("click");
                                }
                            });
                            $('.showMap-close').click(function() {
                                thisLightBox.hide().removeClass('active-popUp');
                                $(".cm-page-container").removeClass('prevent-page-scroll');
                                $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
                                $(window).scrollTop(presentScroll);
                            })

                        })
            }

        });

// Function for setting cookies at login page
var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Function for getting cookies at login page
var getCookie = function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Function for checking cookies at login page
var checkLoggedInStatus = function(cookieParam) {
    var user = getCookie(cookieParam);
    if (user != "")
        return true;
    return false;
}
