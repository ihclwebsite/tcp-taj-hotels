$('document').ready(function() {
    $('#jiva-spa-gender').selectBoxIt();
    $('#jiva-spa-best-time').selectBoxIt();
    var jivaDateWidth = $('#jiva-spa-date').outerWidth();
    $('.dropdown-menu').css('width', '300px');

    //datepicker script
    var today = new Date();

    $('#jiva-spa-date').datepicker({
        startDate: today
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            // Added by Dinesh for Event quote datepicker
            var selectedDate = ((minDate.getDate()<9?"0"+minDate.getDate(): minDate.getDate())+ '/' + ((minDate.getMonth() + 1)<9?"0"+(minDate.getMonth() + 1):(minDate.getMonth() + 1)) + '/' + minDate.getFullYear());

            $('.event-quote-date-value')
                .val(selectedDate)
                .removeClass('invalid-input');
            $('.jiva-spa-date-value').text(selectedDate);
            $('.jiva-spa-date').removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
        }
    });


    $('#jiva-spa-date').datepicker('setDate', new Date());

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $('.jiva-spa-date').addClass('visible');
    });

    $('.jiva-spa-lead-gen-page').click(function(e) {
        $('.jiva-spa-date').removeClass('visible');
    })

    //script for the suggestion list on the hotel search button
    var jivaSearchSuggestions = ['Taj Mahal Towers, Colaba-Mumbai', 'Taj Mahal Palace, Colaba-Mumbai', 'Taj Wellington, Santacruz-Mumbai', 'Taj Lands End, Bandra-Mumbai'];

    for (i = 0; i < jivaSearchSuggestions.length; i++) {
        $('.jiva-spa-search-list').append('<li><label>' + jivaSearchSuggestions[i] + '</label></li>')
    }

    $(".jiva-spa-hotel-input > input").on("keyup", function() {
        var g = $(this).val().toLowerCase();
        $(".jiva-spa-search-list li label").each(function() {
            var s = $(this).text().toLowerCase();
            $(this).closest('.jiva-spa-search-list li')[s.indexOf(g) !== -1 ? 'show' : 'hide']();
        });
    });

    $(".jiva-spa-hotel-input > input").click(function(e) {
        e.stopPropagation();
        $('.jiva-spa-search-list').css('display', 'block');
        $('.jiva-spa-search-list li').on('click', function() {
            var value = ($(this).children()).text();
            $(".jiva-spa-hotel-input > input").val(value);
            $(".jiva-spa-hotel-input > input").removeClass('jiva-spa-not-valid');
            $('.jiva-spa-search-list').css('display', 'none');
            $('.jiva-search-close').addClass('visible');
        });
    });
    // $('.jiva-search-close').click(function() {
    //     $(this).removeClass('visible');
    //     $( ".jiva-spa-hotel-input > input" ).attr( "placeholder", "Search" );
    // });

    $(document).on("click", function(e) {
        $('.jiva-spa-search-list').css('display', 'none');
    });

    //validation script
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    $('.jiva-spa-mand-input').each(function() {
        $(this).focusout(function() {
            if ($(this).val() == "") {
                $(this).addClass('jiva-spa-not-valid');
            } else {
                $(this).removeClass('jiva-spa-not-valid');
            }
        })
    })

    $('.jiva-spa-email').blur(function() {
        var email = $(this).val();
        if (!isEmail(email)) {
            $(this).addClass('jiva-spa-not-valid');
        } else {
            $(this).removeClass('jiva-spa-not-valid');
        }
    });



    $('.jiva-spa-form-btn').click(function() {

        $('.jiva-spa-mand').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('jiva-spa-not-valid');
            }
        })
        if ($('.jiva-spa-search-man').val() == "") {
            $('.jiva-spa-search-man').addClass('jiva-spa-not-valid');
        } else {
            $('.jiva-spa-search-man').removeClass('jiva-spa-not-valid');
        }
        if ($('.jiva-spa-date-value').text() == "Date") {
            $('.jiva-spa-date-con').addClass('jiva-spa-not-valid');
        }

        if ($('.jiva-spa-mand').hasClass('jiva-spa-not-valid')) {
            // Do nothing
        } else {
           // window.location.href = "../../pages/hotels/specific-hotels-jivaspa-lead-gen-conf.html#Spa";
        }

    })
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9qaXZhLXNwYS1sZWFkLWdlbi9qaXZhLXNwYS1sZWFkLWdlbi1jb21wLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoJ2RvY3VtZW50JykucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgICAkKCcjaml2YS1zcGEtZ2VuZGVyJykuc2VsZWN0Qm94SXQoKTtcclxuICAgICQoJyNqaXZhLXNwYS1iZXN0LXRpbWUnKS5zZWxlY3RCb3hJdCgpO1xyXG4gICAgdmFyIGppdmFEYXRlV2lkdGggPSAkKCcjaml2YS1zcGEtZGF0ZScpLm91dGVyV2lkdGgoKTtcclxuICAgICQoJy5kcm9wZG93bi1tZW51JykuY3NzKCd3aWR0aCcsICczMDBweCcpO1xyXG5cclxuICAgIC8vZGF0ZXBpY2tlciBzY3JpcHRcclxuICAgIHZhciB0b2RheSA9IG5ldyBEYXRlKCk7XHJcblxyXG4gICAgJCgnI2ppdmEtc3BhLWRhdGUnKS5kYXRlcGlja2VyKHtcclxuICAgICAgICBzdGFydERhdGU6IHRvZGF5XHJcbiAgICB9KS5vbignY2hhbmdlRGF0ZScsIGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuICAgICAgICBpZiAoJCgnLmppdmEtc3BhLWRhdGUnKS5oYXNDbGFzcygndmlzaWJsZScpKSB7XHJcbiAgICAgICAgICAgIHZhciBtaW5EYXRlID0gKG5ldyBEYXRlKGVsZW1lbnQuZGF0ZS52YWx1ZU9mKCkpKTtcclxuICAgICAgICAgICAgLy8gQWRkZWQgYnkgRGluZXNoIGZvciBFdmVudCBxdW90ZSBkYXRlcGlja2VyXHJcbiAgICAgICAgICAgIHZhciBzZWxlY3RlZERhdGUgPSAobWluRGF0ZS5nZXREYXRlKCkgKyAnLycgKyAobWluRGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLycgKyBtaW5EYXRlLmdldEZ1bGxZZWFyKCkpO1xyXG4gICAgICAgICAgICAkKCcuZXZlbnQtcXVvdGUtZGF0ZS12YWx1ZScpXHJcbiAgICAgICAgICAgICAgICAudmFsKHNlbGVjdGVkRGF0ZSlcclxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnaW52YWxpZC1pbnB1dCcpO1xyXG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZS12YWx1ZScpLnRleHQoc2VsZWN0ZWREYXRlKTtcclxuICAgICAgICAgICAgJCgnLmppdmEtc3BhLWRhdGUnKS5yZW1vdmVDbGFzcygndmlzaWJsZScpO1xyXG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZS1jb24nKS5yZW1vdmVDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG5cclxuICAgICQoJyNqaXZhLXNwYS1kYXRlJykuZGF0ZXBpY2tlcignc2V0RGF0ZScsIG5ldyBEYXRlKCkpO1xyXG5cclxuICAgICQoJy5qaXZhLXNwYS1kYXRlLWNvbicpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICQoJy5qaXZhLXNwYS1kYXRlJykuYWRkQ2xhc3MoJ3Zpc2libGUnKTtcclxuICAgIH0pO1xyXG5cclxuICAgICQoJy5qaXZhLXNwYS1sZWFkLWdlbi1wYWdlJykuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICQoJy5qaXZhLXNwYS1kYXRlJykucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKTtcclxuICAgIH0pXHJcblxyXG4gICAgLy9zY3JpcHQgZm9yIHRoZSBzdWdnZXN0aW9uIGxpc3Qgb24gdGhlIGhvdGVsIHNlYXJjaCBidXR0b25cclxuICAgIHZhciBqaXZhU2VhcmNoU3VnZ2VzdGlvbnMgPSBbJ1RhaiBNYWhhbCBUb3dlcnMsIENvbGFiYS1NdW1iYWknLCAnVGFqIE1haGFsIFBhbGFjZSwgQ29sYWJhLU11bWJhaScsICdUYWogV2VsbGluZ3RvbiwgU2FudGFjcnV6LU11bWJhaScsICdUYWogTGFuZHMgRW5kLCBCYW5kcmEtTXVtYmFpJ107XHJcblxyXG4gICAgZm9yIChpID0gMDsgaSA8IGppdmFTZWFyY2hTdWdnZXN0aW9ucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICQoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCcpLmFwcGVuZCgnPGxpPjxsYWJlbD4nICsgaml2YVNlYXJjaFN1Z2dlc3Rpb25zW2ldICsgJzwvbGFiZWw+PC9saT4nKVxyXG4gICAgfVxyXG5cclxuICAgICQoXCIuaml2YS1zcGEtaG90ZWwtaW5wdXQgPiBpbnB1dFwiKS5vbihcImtleXVwXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBnID0gJCh0aGlzKS52YWwoKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICQoXCIuaml2YS1zcGEtc2VhcmNoLWxpc3QgbGkgbGFiZWxcIikuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgdmFyIHMgPSAkKHRoaXMpLnRleHQoKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCBsaScpW3MuaW5kZXhPZihnKSAhPT0gLTEgPyAnc2hvdycgOiAnaGlkZSddKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKFwiLmppdmEtc3BhLWhvdGVsLWlucHV0ID4gaW5wdXRcIikuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1saXN0JykuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XHJcbiAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1saXN0IGxpJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHZhciB2YWx1ZSA9ICgkKHRoaXMpLmNoaWxkcmVuKCkpLnRleHQoKTtcclxuICAgICAgICAgICAgJChcIi5qaXZhLXNwYS1ob3RlbC1pbnB1dCA+IGlucHV0XCIpLnZhbCh2YWx1ZSk7XHJcbiAgICAgICAgICAgICQoXCIuaml2YS1zcGEtaG90ZWwtaW5wdXQgPiBpbnB1dFwiKS5yZW1vdmVDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgICAgICQoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCcpLmNzcygnZGlzcGxheScsICdub25lJyk7XHJcbiAgICAgICAgICAgICQoJy5qaXZhLXNlYXJjaC1jbG9zZScpLmFkZENsYXNzKCd2aXNpYmxlJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuICAgIC8vICQoJy5qaXZhLXNlYXJjaC1jbG9zZScpLmNsaWNrKGZ1bmN0aW9uKCkge1xyXG4gICAgLy8gICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKTtcclxuICAgIC8vICAgICAkKCBcIi5qaXZhLXNwYS1ob3RlbC1pbnB1dCA+IGlucHV0XCIgKS5hdHRyKCBcInBsYWNlaG9sZGVyXCIsIFwiU2VhcmNoXCIgKTtcclxuICAgIC8vIH0pO1xyXG5cclxuICAgICQoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICQoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCcpLmNzcygnZGlzcGxheScsICdub25lJyk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvL3ZhbGlkYXRpb24gc2NyaXB0XHJcbiAgICBmdW5jdGlvbiBpc0VtYWlsKGVtYWlsKSB7XHJcbiAgICAgICAgdmFyIHJlZ2V4ID0gL14oW2EtekEtWjAtOV8uKy1dKStcXEAoKFthLXpBLVowLTktXSkrXFwuKSsoW2EtekEtWjAtOV17Miw0fSkrJC87XHJcbiAgICAgICAgcmV0dXJuIHJlZ2V4LnRlc3QoZW1haWwpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAkKCcuaml2YS1zcGEtbWFuZC1pbnB1dCcpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJCh0aGlzKS5mb2N1c291dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9KVxyXG5cclxuICAgICQoJy5qaXZhLXNwYS1lbWFpbCcpLmJsdXIoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGVtYWlsID0gJCh0aGlzKS52YWwoKTtcclxuICAgICAgICBpZiAoIWlzRW1haWwoZW1haWwpKSB7XHJcbiAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2ppdmEtc3BhLW5vdC12YWxpZCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2ppdmEtc3BhLW5vdC12YWxpZCcpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuXHJcblxyXG4gICAgJCgnLmppdmEtc3BhLWZvcm0tYnRuJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICQoJy5qaXZhLXNwYS1tYW5kJykuZWFjaChmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICAgIGlmICgkKCcuaml2YS1zcGEtc2VhcmNoLW1hbicpLnZhbCgpID09IFwiXCIpIHtcclxuICAgICAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1tYW4nKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1tYW4nKS5yZW1vdmVDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgkKCcuaml2YS1zcGEtZGF0ZS12YWx1ZScpLnRleHQoKSA9PSBcIkRhdGVcIikge1xyXG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZS1jb24nKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoJCgnLmppdmEtc3BhLW1hbmQnKS5oYXNDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJykpIHtcclxuICAgICAgICAgICAgLy8gRG8gbm90aGluZ1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gXCIuLi8uLi9wYWdlcy9ob3RlbHMvc3BlY2lmaWMtaG90ZWxzLWppdmFzcGEtbGVhZC1nZW4tY29uZi5odG1sI1NwYVwiO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9KVxyXG59KTsiXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2ppdmEtc3BhLWxlYWQtZ2VuL2ppdmEtc3BhLWxlYWQtZ2VuLWNvbXAuanMifQ==
