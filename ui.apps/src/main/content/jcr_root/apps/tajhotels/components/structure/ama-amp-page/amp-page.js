use(function () {
    var canonicalUrl = this.pageUrl.replace('/amp', '');

    return {
        canonicalUrl: canonicalUrl
    };
});
