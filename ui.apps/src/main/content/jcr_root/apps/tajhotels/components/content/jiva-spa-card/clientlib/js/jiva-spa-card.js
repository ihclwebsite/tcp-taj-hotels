$(document).ready(function() {

    var allspaAmounts = $.find('.spaAmount')
    for ( var dom in allspaAmounts) {

        var amount = $(allspaAmounts[dom]).text();
        if (amount.includes(","))
            amount = amount.replace(/\,/g, "");
        if (amount.includes("/")) {
            var total_amount = amount.split("/");
            var first_amount = parseInt(total_amount[0]).toLocaleString('en-IN');
            var second_amount = parseInt(total_amount[1]).toLocaleString('en-IN');
            $(allspaAmounts[dom]).text(first_amount + "/" + second_amount);
        } else if (!Number.isInteger(parseInt(amount))) {
            amount = amount.toLocaleString('en-IN');
            $(allspaAmounts[dom]).text(amount);
            $(allspaAmounts[dom]).siblings().hide();
        } else {
            amount = parseInt(amount);
            amount = amount.toLocaleString('en-IN');
            $(allspaAmounts[dom]).text(amount);
        }
    }
    $('.jiva-spa-show-more-con').each(function() {
        $(this).showMore();
    });

    $('.menu-carousel-container').click(function() {
        // $('.mr-overlay-initial-none').hide();

    });

    $.each($('.jiva-spa-detail-long-desc-card'), function(i, value) {
        $(value).cmToggleText({
            charLimit : 60,
        });
    });

    $.each($('.jiva-spa-card-desc'), function(i, value) {
        $(value).cmToggleText({
            charLimit : 60,
        });
    });

    $.each($('.jiva-spa-details-desc'), function(i, value) {
        $(value).cmToggleText({
            charLimit : 125,
        });
    });

    $('.jiva-spa-rate-value').each(function() {
        var currentRateValue = $(this).text();
        var taxesIndex = currentRateValue.indexOf("+taxes");
        if (taxesIndex != -1) {
            currentRateValue = currentRateValue.substr(0, taxesIndex);
            var taxInclusiveText = currentRateValue + "<span class='jiva-spa-rate-value-tax'>+taxes</span>";
            $(this).html(taxInclusiveText);
        }
    });
});

function jivaSpaClickHandler(obj) {

    var presentScroll = $(window).scrollTop();
    var parent = $(obj).data("spa-id");

    $("#" + parent).find('.jiva-spa-detail-long-desc-detail').cmToggleText({
        charLimit : 150,
    });

    $('#' + parent).children('.jiva-spa-card-details-wrap').show();
    if (deviceDetector.checkDevice() != "large") {
        $('.jiva-spa-mobile-photo-gallery').each(function() {
            $(this).customSlide(1, true);
        })

    }
    $(".cm-page-container").addClass('prevent-page-scroll');
    trigger_jiva_view(parent);

    $('.jiva-spa-details-close').click(function() {
        $('.jiva-spa-card-details-wrap').hide();
        $(".cm-page-container").removeClass('prevent-page-scroll');
        $(window).scrollTop(presentScroll);
    });
}

function goToBookingPage(obj) {
    window.location.assign(obj);
}
