$(document).ready(
        function() {

            $('#mr-reserveTable-number-of-people').selectBoxIt({
                populate : [ '2', '3', '4', '5', '6', '7', '8', '9', '10' ]
            });
            $('#mr-reserveTable-meal-type').selectBoxIt();

            if ($('.mr-reserve-table-form-container').length > 0) {

                // Fix added for THU-1621 all options were having selected by default
                $('.mr-reserve-table-form-container select option:selected').removeAttr('selected')
                $('.mr-reserve-table-form-container #mr-reserveTable-number-of-people').val(
                        dataCache.session.getData("PeopleCount"));
                $('.mr-reserve-table-form-container #mr-reserveTable-number-of-people').data("selectBox-selectBoxIt")
                        .refresh();
                $('.mr-reserve-table-form-container #mr-reserveTable-meal-type').val(
                        dataCache.session.getData("MealType"));
                $('.mr-reserve-table-form-container #mr-reserveTable-meal-type').data("selectBox-selectBoxIt")
                        .refresh();
                $('#jiva-spa-date.jiva-spa-date.dining-reserveTable-Date.mr-form-dining-reservation-date').datepicker(
                        'update', '' + dataCache.session.getData("DiningDate") + '');
                $('.event-quote-date-value').val(dataCache.session.getData("DiningDate")).removeClass('invalid-input');

                var jsonObj = {};
                jsonObj.restaurantID = getParam('diningid');
                jsonObj.hotelRestaurantName = getParam('diningName');
                jsonObj.dateValue = dataCache.session.getData("DiningDate");
                jsonObj.mealType = dataCache.session.getData("MealType");
                jsonObj.interfaceId = dataCache.session.getData("InterfaceId");
                jsonObj.peopleCount = dataCache.session.getData("PeopleCount");
                prepareJsonForCheckAvailibility("reserveTableCheckAvailibility", jsonObj);
            }

        });
