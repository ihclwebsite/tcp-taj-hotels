$(document).ready(function() {
    try {
        $('.offers-card-description').each(function() {
            $(this).cmToggleText({
                charLimit : 135
            });
        });
    } catch (err) {
        console.error('error caught in function cmToggleText');
        console.error(err);
    }
	// valid till for refrenced offer
    try {
		$('.offer-validity-offerendson').each(function(){
            console.log('ends on::', $(this).text())
            if($(this).text()) {
            	$(this).text(moment($(this).text()).format('Do MMM YYYY'));
				$(this).removeClass('d-none');
            }
        })
    } catch(err){
		console.error('offer ends on date ', err);
    }
});

function onOfferSelection(navPath, offerRateCode, offerRoundTheYear, offerTitle, noOfNights, offerStartDate,
        offerEndDate, comparableOfferRateCode, offerCategory, memberOnlyOffer) {

    var user = getUserData();
    if((memberOnlyOffer == 'true' || (offerCategory && offerCategory.includes('member'))) && !(user && user.membershipId)){
        $('body').trigger('taj:sign-in');
        return ;
    }
    try {
        //updated for global data layer
        var offerDetails = {};
        offerDetails.offerCode = offerRateCode;
        offerDetails.offerStartDate = offerStartDate;
        offerDetails.offerName = offerTitle;
		offerDetails.offerValidity = 
        offerDetails.offerCategory = "";
        prepareOfferAndPromotionsJsonAfterSumbitClick(offerTitle +'_Booking_HomePage_BookNow', offerDetails);

        // offer details functionality
        var ROOMS_PATH = "";
        var nights = '';
        var startsFrom = '';
        var endsOn = '';
        var today = moment().format('MMM Do YY');
        var tomorrow = '';
        var dayAfterTomorrow = '';
        var hotelPath = $("[data-hotel-path]").data("hotel-path");

        if ($('.cm-page-container').hasClass('ama-theme')) {
            ROOMS_PATH = "accommodations.html";
        } else {
            ROOMS_PATH = "rooms-and-suites.html";
        }

        if (noOfNights && noOfNights != "" && noOfNights != '0') {
            nights = noOfNights;
        } else {
            nights = 1;
        }

        // override default t+15 booking date for custom start and end dates and adding nights
        if (offerRateCode && !offerRoundTheYear) {
            if (comparableOfferRateCode) {
                offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
            }
            if (offerStartDate && offerEndDate) {
                startsFrom = moment(offerStartDate).format('MMM Do YY');
                endsOn = moment(offerEndDate).format('MMM Do YY');
                if (moment(startsFrom, 'MMM Do YY').isSameOrBefore(moment(today, 'MMM Do YY'))
                        && moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                } else if (moment(today, 'MMM Do YY').isSameOrBefore(moment(startsFrom, 'MMM Do YY'))) {
                	tomorrow = moment(startsFrom, 'MMM Do YY').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }
            } else if (!offerStartDate && offerEndDate) {
                endsOn = moment(offerEndDate).format('MMM Do YY');
                if (moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }

                // default t+15 booking dates and adding nights
            } else {
                tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
                dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
            }

            // round the year offer with t+15 dates and nights
        } else {
            tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
            dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
        }
        if (hotelPath) {
            navPath = hotelPath.replace(".html", "");
            navPath = navPath + ROOMS_PATH;
            navPath = updateQueryString("overrideSessionDates", "true", navPath);
            navPath = updateQueryString("from", tomorrow, navPath);
            navPath = updateQueryString("to", dayAfterTomorrow, navPath);
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }

        // creating the URL for the button
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if ((!navPath.includes("http://") && navPath.includes("http:/"))
                || (!navPath.includes("https://") && navPath.includes("https:/"))) {
            navPath = navPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        window.location.href = navPath;
    } catch (err) {
        console.error('error caught in function onOfferSelection');
        console.error(err);
    }
}

function onViewDetailsOfferSelection(navPath) {
    try {
        window.location.href = navPath;
    } catch (err) {
        console.error('error caught in function onViewDetailsOfferSelection');
        console.error(err);
    }
}
