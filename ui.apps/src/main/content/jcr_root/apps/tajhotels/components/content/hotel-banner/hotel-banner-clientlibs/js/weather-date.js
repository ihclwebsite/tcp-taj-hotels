/*
by Srikanta@moonraft  
open weather api to fetch weather-forcastv1.0.0

 */

$(document).ready(function() {
    setCurrentDateInBanner();
});
(function() {
    var apiKey = $('.banner-container').data('apikey');
    var lat = $('#hotel-banner-temperature').data('lat');
    var lon = $('#hotel-banner-temperature').data('lon');
    var hotellat = $('#hotel-banner-temperature').data('hotellat');
    var hotellon = $('#hotel-banner-temperature').data('hotellon');

    if (lat == undefined && hotellat != undefined) {
        lat = hotellat;
        lon = hotellon;
    }
    if (lat != undefined && lon != undefined) {
        var queryURL = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid="
                + apiKey + "&units=metric";

        $.getJSON(queryURL, function(data) {
            // var location = data.id.name; // not returned in response
            var temp = data.main.temp;

            var tempRound = parseFloat(temp).toFixed();
            if (tempRound != 'NaN') {
                $('#weather-update').append(tempRound);
            } else {
                $('.hotel-banner-temperature').hide();
                $('.hotel-banner-date').removeClass('inline-block');
            }
        });
    } else {
        console.log("Lat and long can't found")
        $('.banner-label-below-title-sm').hide();
        $('.banner-btn-con.inline-block').hide()
    }
}());

function setCurrentDateInBanner() {
    var currentDate = new Date();
    currentDate = moment(currentDate).format('Do MMM YYYY');
    console.log("Todays Date ", currentDate);
    var systemDateDom = $.find('.system-date')[0];
    if (systemDateDom) {
        $(systemDateDom).text(currentDate);
    }
}
