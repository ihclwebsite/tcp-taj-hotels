$(window).load(function() {
    fetchSpaData();
    $('.check-wait-spinner').hide();

    var SEARCH_INPUT_DEBOUNCE_RATE = 3000;

    $('#jiva-spa-gender').selectBoxIt();
    $('#jiva-spa-best-time').selectBoxIt();
    $('#hotel-names-dropdown').selectBoxIt();

    var jivaDateWidth = $('#jiva-spa-date').outerWidth();
    $('.dropdown-menu').css('width', '300px');

    // datepicker script
    var today = new Date();

    $('#jiva-spa-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            // Added by Dinesh for Event quote datepicker
            // var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' + minDate.getFullYear());
            var selectedDate = moment(minDate).format("DD MMM YYYY");
            $('.event-quote-date-value').val(selectedDate).removeClass('invalid-input');
            $('.jiva-spa-date-value').val(selectedDate);
            $('.jiva-spa-date').removeClass('visible');
            $('.jiva-spa-date-value').removeClass('jiva-spa-not-valid');
        }
    });

    $('#jiva-spa-date').datepicker('setDate', new Date());

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $('.jiva-spa-date').addClass('visible');
    });

    $(document).click(function(e) {
        $('.jiva-spa-date').removeClass('visible');
    });

    $("#hotel-names-dropdown").change(function() {
        var hotelCity = $(this).find(':selected').attr('data-hotel-city');
        if (hotelCity) {
            $("#jiva-spa-city").attr("value", hotelCity).prop('disabled', true);
        } else {
            $("#jiva-spa-city").attr("value", '').prop('disabled', false);
        }
    });

    $('#searchInputJiva').on("keyup", debounce(function(e) {

        if ($(this).val().length > 2) {
            clearSearchResults();
            performSearch($(this).val()).done(function() {
                var _target = $('.jiva-spa-search-list');
                _target.removeClass('display-none');
                _target.css('display', 'block');
                _inputInTarget = $(".jiva-spa-hotel-input > input");
                $('.jiva-spa-search-list li').on('click', function() {

                    var value = ($(this).children()).text();
                    _inputInTarget.val(value);
                    _inputInTarget.attr('data-email-id', ($(this)).attr('data-email-id'));
                    $("#jiva-spa-city").attr("value", $('#searchInputJiva').attr('data-hotel-city'));
                    _inputInTarget.removeClass('jiva-spa-not-valid');
                    $("#jiva-spa-city").attr("value", ($(this)).attr('hotel-city-name'));
                    _target.css('display', 'none');
                    $('.jiva-search-close').addClass('visible');
                });
            });

        } else {
            clearSearchResults();
            if (!$('.jiva-spa-search-list').hasClass('display-none')) {

                $('.jiva-spa-search-list').addClass('display-none');
            }
        }
    }, SEARCH_INPUT_DEBOUNCE_RATE));

    $('.jiva-search-close').click(function() {
        $(this).removeClass('visible');
        $(".jiva-spa-hotel-input > input").attr("placeholder", "Search");

    });

    $(document).on("click", function(e) {
        $('.jiva-spa-search-list').css('display', 'none');
    });

    $('.jiva-spa-mand-input').each(function() {
        $(this).focusout(function() {
            if ($(this).val() == "") {
                $(this).addClass('jiva-spa-not-valid');
            } else {
                $(this).removeClass('jiva-spa-not-valid');
            }
        })
    });

    $('.jiva-spa-email').blur(function() {
        var email = $(this).val();
        if (!isEmail(email)) {
            $(this).addClass('jiva-spa-not-valid');
            invalidWarningMessage($(this));
        } else {
            $(this).removeClass('jiva-spa-not-valid');
        }
    });

    $('.jiva-spa-form-btn').click(function() {

        $('input.jiva-spa-mand, select.jiva-spa-mand').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('jiva-spa-not-valid');
                invalidWarningMessage($(this));
            } else {
                $(this).removeClass('jiva-spa-not-valid');
                $(this).closest('.sub-form-input-wrp').find('.sub-form-input-warning').hide();
            }
        })
        if ($('.jiva-spa-search-man').val() == "") {
            $('.jiva-spa-search-man').addClass('jiva-spa-not-valid');
            invalidWarningMessage($(this));
        } else {
            $('.jiva-spa-search-man').removeClass('jiva-spa-not-valid');
        }
        /*
         * if ($('.jiva-spa-date-value').text() == "Date") { $('.jiva-spa-date-con').addClass('jiva-spa-not-valid'); }
         */

        if ($('.jiva-spa-mand').hasClass('jiva-spa-not-valid')) {
            // Do nothing
        } else {
            requestBooking();
        }

    });
});

function fetchSpaData() {
    try {
        var spadata = dataCache.session.getData("spaOptions");
        if (spadata) {
            $("#spa-name").attr("value", spadata.spaName);
            $("#spa-duration").attr("value", spadata.spaDuration);
            $("#spa-amount").attr("value", spadata.spaAmount);
            $("#searchInputJiva").attr("value", spadata.hotelName);
            if (spadata.hotelCity != "") {
                $("#jiva-spa-city").attr("value", spadata.hotelCity);
            }
            $("#searchInputJiva").attr("data-email-id", spadata.hotelEmailId);
            if ($('#searchInputJiva').val()) {
                $(".searchInputJivaDropDown").css('display', "none");
                $('.jiva-spa-mand#hotel-names-dropdown').removeClass("jiva-spa-mand");
            } else {
                $(".hotel-name-input-field").css('display', "none");
                $(".spa-details-row").css('display', "none");
                $(".spa-details-row").find(".jiva-spa-mand.jiva-spa-mand-input").removeClass(
                        "jiva-spa-mand jiva-spa-mand-input");
                $('.jiva-spa-mand.jiva-spa-mand-input.mr-hotel-level').removeClass("jiva-spa-mand jiva-spa-mand-input");
            }
        }
    } catch (error) {
        console.error("Error in jiva spa booking js ", error);
    }

}

// validation script
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function requestBooking(key) {
    var sessionData = dataCache.session.getData("spaOptions");

    $('.pg-spinner-con').css('display', "block");
    var spaName = getParam('jivaSpaName');

    var hotelName = $('#searchInputJiva').val();

    if (hotelName == "") {
        hotelName = $('#searchInputJivaDropDown').find(':selected').val()
    }

    var hotelEmailId = sessionData.jivaSpaEmailId;

    if (!hotelEmailId) {
        hotelEmailId = $('#searchInputJiva').attr('data-email-id');
    }

    if (hotelEmailId == "") {
        hotelEmailId = $('#searchInputJivaDropDown').find(':selected').attr('data-email-id');
    }

    var hotelCity = $('#jiva-spa-city').attr('data-hotel-city');

    var jivaSpaDateValue = $('.jiva-spa-date-value').val();
    var bookingDate = moment(jivaSpaDateValue, "DD MMM YYYY").format("DD/MM/YYYY");

    var city = $('#jiva-spa-city').val();

    var name = $('#customer-name').val();

    var gender = $('#jiva-spa-gender').val();

    var phoneNumber = $('#phone-Number').val();

    var guestEmailId = $('#customer-email-id').val();

    var timeToContact = $('#jiva-spa-best-time').val();

    var jivaSpaConfLink = $('.jiva-spa-conf-link').val();

    var jivaSpaErrorLink = $('.jiva-spa-error-link').val();

    var jivaspaStartTime = $('#jiva-spa-start-time').val();
    var spaDuration = $('#spa-duration').val();
    if (spaDuration == undefined) {
        spaDuration = "";
    }

    var spaAmount = $('#spa-amount').val();

    if (spaAmount == undefined) {
        spaAmount = "";
    }
    if (!hotelEmailId) {
        hotelEmailId = "";
    }
    var requestString = "hotelName=" + hotelName + "&hotelEmailId=" + hotelEmailId + "&bookingDate=" + bookingDate
            + "&city=" + city + "&name=" + name + "&gender=" + gender + "&phoneNumber=" + phoneNumber
            + "&guestEmailId=" + guestEmailId + "&jivaSpaName=" + spaName + "&timeToContact=" + timeToContact
            + "&spaDuration=" + spaDuration + "&spaAmount=" + spaAmount + "&spaStartTime=" + jivaspaStartTime;

    var jivabookingData = prepareBookingData(hotelName, hotelEmailId, bookingDate, city, gender, timeToContact,
            spaName, spaDuration, spaAmount);
    tigger_jiva_book_appointment(jivabookingData)
    return $.ajax({
        method : "GET",
        url : "/bin/bookAppointment",
        data : requestString
    }).done(function(res) {
        console.info("bookAppointment success");
        window.location.assign(jivaSpaConfLink);
    }).fail(function() {
        console.error("bookAppointment failed ");
        window.location.assign(jivaSpaErrorLink);
        dataCache.session.setData("confStatus", "Error");
    });
};

function prepareBookingData(hotelName, hotelEmailId, bookingDate, city, gender, timeToContact, spaName, spaDuration,
        spaAmount) {
    var jivaBookingData = {};
    jivaBookingData.hotelName = hotelName;
    jivaBookingData.bookingDate = bookingDate;
    jivaBookingData.city = city;
    jivaBookingData.gender = gender;
    jivaBookingData.timeToContact = timeToContact;
    jivaBookingData.hotelEmailId = hotelEmailId;
    jivaBookingData.spaName = spaName;
    jivaBookingData.spaDuration = spaDuration;
    jivaBookingData.spaAmount = spaAmount;
    return jivaBookingData;
};

function clearSearchResults() {
    $('.jiva-spa-search-list').empty();

}

function performSearch(key) {

    return $.ajax({
        method : "GET",
        url : "/bin/hotel-search",
        data : {
            searchText : key
        }
    }).done(function(res) {
        clearSearchResults();
        addHotelSearchResults(res.hotels);
    }).fail(function() {
        clearSearchResults();
    });
}

function addHotelSearchResults(hotels) {
    if (hotels.length) {
        hotels.forEach(function(hotel) {

            $('.jiva-spa-search-list').append(
                    '<li data-email-id="' + hotel.spaEmailId + '" data-hotel-city="' + hotel.city + '" ><label>'
                            + hotel.title + '</label></li>');
        });

    }

}

function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9qaXZhLXNwYS1sZWFkLWdlbi9qaXZhLXNwYS1sZWFkLWdlbi1jb21wLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoJ2RvY3VtZW50JykucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnI2ppdmEtc3BhLWdlbmRlcicpLnNlbGVjdEJveEl0KCk7XG4gICAgJCgnI2ppdmEtc3BhLWJlc3QtdGltZScpLnNlbGVjdEJveEl0KCk7XG4gICAgdmFyIGppdmFEYXRlV2lkdGggPSAkKCcjaml2YS1zcGEtZGF0ZScpLm91dGVyV2lkdGgoKTtcbiAgICAkKCcuZHJvcGRvd24tbWVudScpLmNzcygnd2lkdGgnLCAnMzAwcHgnKTtcblxuICAgIC8vZGF0ZXBpY2tlciBzY3JpcHRcbiAgICB2YXIgdG9kYXkgPSBuZXcgRGF0ZSgpO1xuXG4gICAgJCgnI2ppdmEtc3BhLWRhdGUnKS5kYXRlcGlja2VyKHtcbiAgICAgICAgc3RhcnREYXRlOiB0b2RheVxuICAgIH0pLm9uKCdjaGFuZ2VEYXRlJywgZnVuY3Rpb24oZWxlbWVudCkge1xuICAgICAgICBpZiAoJCgnLmppdmEtc3BhLWRhdGUnKS5oYXNDbGFzcygndmlzaWJsZScpKSB7XG4gICAgICAgICAgICB2YXIgbWluRGF0ZSA9IChuZXcgRGF0ZShlbGVtZW50LmRhdGUudmFsdWVPZigpKSk7XG4gICAgICAgICAgICAvLyBBZGRlZCBieSBEaW5lc2ggZm9yIEV2ZW50IHF1b3RlIGRhdGVwaWNrZXJcbiAgICAgICAgICAgIC8vIHZhciBzZWxlY3RlZERhdGUgPSAobWluRGF0ZS5nZXREYXRlKCkgKyAnLycgKyAobWluRGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLycgKyBtaW5EYXRlLmdldEZ1bGxZZWFyKCkpO1xuICAgICAgICAgICAgdmFyIHNlbGVjdGVkRGF0ZSA9IG1vbWVudChtaW5EYXRlKS5mb3JtYXQoXCJERC9NTS9ZWVlZXCIpO1xuICAgICAgICAgICAgJCgnLmV2ZW50LXF1b3RlLWRhdGUtdmFsdWUnKVxuICAgICAgICAgICAgICAgIC52YWwoc2VsZWN0ZWREYXRlKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnaW52YWxpZC1pbnB1dCcpO1xuICAgICAgICAgICAgJCgnLmppdmEtc3BhLWRhdGUtdmFsdWUnKS50ZXh0KHNlbGVjdGVkRGF0ZSk7XG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZScpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7XG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZS1jb24nKS5yZW1vdmVDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuXG4gICAgJCgnI2ppdmEtc3BhLWRhdGUnKS5kYXRlcGlja2VyKCdzZXREYXRlJywgbmV3IERhdGUoKSk7XG5cbiAgICAkKCcuaml2YS1zcGEtZGF0ZS1jb24nKS5jbGljayhmdW5jdGlvbihlKSB7XG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICQoJy5qaXZhLXNwYS1kYXRlJykuYWRkQ2xhc3MoJ3Zpc2libGUnKTtcbiAgICB9KTtcblxuICAgICQoJy5qaXZhLXNwYS1sZWFkLWdlbi1wYWdlJykuY2xpY2soZnVuY3Rpb24oZSkge1xuICAgICAgICAkKCcuaml2YS1zcGEtZGF0ZScpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7XG4gICAgfSlcblxuICAgIC8vc2NyaXB0IGZvciB0aGUgc3VnZ2VzdGlvbiBsaXN0IG9uIHRoZSBob3RlbCBzZWFyY2ggYnV0dG9uXG4gICAgdmFyIGppdmFTZWFyY2hTdWdnZXN0aW9ucyA9IFsnVGFqIE1haGFsIFRvd2VycywgQ29sYWJhLU11bWJhaScsICdUYWogTWFoYWwgUGFsYWNlLCBDb2xhYmEtTXVtYmFpJywgJ1RhaiBXZWxsaW5ndG9uLCBTYW50YWNydXotTXVtYmFpJywgJ1RhaiBMYW5kcyBFbmQsIEJhbmRyYS1NdW1iYWknXTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBqaXZhU2VhcmNoU3VnZ2VzdGlvbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1saXN0JykuYXBwZW5kKCc8bGk+PGxhYmVsPicgKyBqaXZhU2VhcmNoU3VnZ2VzdGlvbnNbaV0gKyAnPC9sYWJlbD48L2xpPicpXG4gICAgfVxuXG4gICAgJChcIi5qaXZhLXNwYS1ob3RlbC1pbnB1dCA+IGlucHV0XCIpLm9uKFwia2V5dXBcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBnID0gJCh0aGlzKS52YWwoKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAkKFwiLmppdmEtc3BhLXNlYXJjaC1saXN0IGxpIGxhYmVsXCIpLmVhY2goZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgcyA9ICQodGhpcykudGV4dCgpLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCBsaScpW3MuaW5kZXhPZihnKSAhPT0gLTEgPyAnc2hvdycgOiAnaGlkZSddKCk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgJChcIi5qaXZhLXNwYS1ob3RlbC1pbnB1dCA+IGlucHV0XCIpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1saXN0JykuY3NzKCdkaXNwbGF5JywgJ2Jsb2NrJyk7XG4gICAgICAgICQoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCBsaScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHZhbHVlID0gKCQodGhpcykuY2hpbGRyZW4oKSkudGV4dCgpO1xuICAgICAgICAgICAgJChcIi5qaXZhLXNwYS1ob3RlbC1pbnB1dCA+IGlucHV0XCIpLnZhbCh2YWx1ZSk7XG4gICAgICAgICAgICAkKFwiLmppdmEtc3BhLWhvdGVsLWlucHV0ID4gaW5wdXRcIikucmVtb3ZlQ2xhc3MoJ2ppdmEtc3BhLW5vdC12YWxpZCcpO1xuICAgICAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1saXN0JykuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgICAgICQoJy5qaXZhLXNlYXJjaC1jbG9zZScpLmFkZENsYXNzKCd2aXNpYmxlJyk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xuICAgIC8vICQoJy5qaXZhLXNlYXJjaC1jbG9zZScpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgIC8vICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7XG4gICAgLy8gICAgICQoIFwiLmppdmEtc3BhLWhvdGVsLWlucHV0ID4gaW5wdXRcIiApLmF0dHIoIFwicGxhY2Vob2xkZXJcIiwgXCJTZWFyY2hcIiApO1xuICAgIC8vIH0pO1xuXG4gICAgJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICQoJy5qaXZhLXNwYS1zZWFyY2gtbGlzdCcpLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgfSk7XG5cbiAgICAvL3ZhbGlkYXRpb24gc2NyaXB0XG4gICAgZnVuY3Rpb24gaXNFbWFpbChlbWFpbCkge1xuICAgICAgICB2YXIgcmVnZXggPSAvXihbYS16QS1aMC05Xy4rLV0pK1xcQCgoW2EtekEtWjAtOS1dKStcXC4pKyhbYS16QS1aMC05XXsyLDR9KSskLztcbiAgICAgICAgcmV0dXJuIHJlZ2V4LnRlc3QoZW1haWwpO1xuICAgIH1cblxuXG4gICAgJCgnLmppdmEtc3BhLW1hbmQtaW5wdXQnKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpLmZvY3Vzb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYgKCQodGhpcykudmFsKCkgPT0gXCJcIikge1xuICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2ppdmEtc3BhLW5vdC12YWxpZCcpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICB9KVxuXG4gICAgJCgnLmppdmEtc3BhLWVtYWlsJykuYmx1cihmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIGVtYWlsID0gJCh0aGlzKS52YWwoKTtcbiAgICAgICAgaWYgKCFpc0VtYWlsKGVtYWlsKSkge1xuICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcbiAgICAgICAgfVxuICAgIH0pO1xuXG5cblxuICAgICQoJy5qaXZhLXNwYS1mb3JtLWJ0bicpLmNsaWNrKGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICQoJy5qaXZhLXNwYS1tYW5kJykuZWFjaChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmICgkKHRoaXMpLnZhbCgpID09IFwiXCIpIHtcbiAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSlcbiAgICAgICAgaWYgKCQoJy5qaXZhLXNwYS1zZWFyY2gtbWFuJykudmFsKCkgPT0gXCJcIikge1xuICAgICAgICAgICAgJCgnLmppdmEtc3BhLXNlYXJjaC1tYW4nKS5hZGRDbGFzcygnaml2YS1zcGEtbm90LXZhbGlkJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKCcuaml2YS1zcGEtc2VhcmNoLW1hbicpLnJlbW92ZUNsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoJCgnLmppdmEtc3BhLWRhdGUtdmFsdWUnKS50ZXh0KCkgPT0gXCJEYXRlXCIpIHtcbiAgICAgICAgICAgICQoJy5qaXZhLXNwYS1kYXRlLWNvbicpLmFkZENsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICgkKCcuaml2YS1zcGEtbWFuZCcpLmhhc0NsYXNzKCdqaXZhLXNwYS1ub3QtdmFsaWQnKSkge1xuICAgICAgICAgICAgLy8gRG8gbm90aGluZ1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBcIi4uLy4uL3BhZ2VzL2hvdGVscy9zcGVjaWZpYy1ob3RlbHMtaml2YXNwYS1sZWFkLWdlbi1jb25mLmh0bWwjU3BhXCI7XG4gICAgICAgIH1cblxuICAgIH0pXG59KTsiXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2ppdmEtc3BhLWxlYWQtZ2VuL2ppdmEtc3BhLWxlYWQtZ2VuLWNvbXAuanMifQ==
