//[IHCL-CB START]
$(document).ready(function() {
    console.log('ready');
    $.each($(".mr-list-view-hotels-button"), function(index, value) {
        if (isIHCLCBSite()) {
            var hrefValue = $(this).parent().attr('href');
            if (hrefValue && !hrefValue.includes('rooms-and-suites')) {
                if ($(this).parents('.list-view-wrapper').data('hotelbrand') == 'taj:hotels/brands/Ama') {
                    hrefValue = hrefValue + 'accommodations/';
                } else {
                    hrefValue = hrefValue + 'rooms-and-suites/';
                }
            }

            $(this).parent().attr('href', hrefValue);
        }
    });

   	//attachAccommodation();

});


// [IHCL-CB END]

function modifyRoomsURL(element) {
    try {
        var hrefValue = $(element).attr('href');
        if (hrefValue.includes("?")) {
            if (hrefValue.charAt(hrefValue.length - 1) === "?") {
                hrefValue += fetchDateOccupancyAsQueryString();
            } else {
                hrefValue += "&" + fetchDateOccupancyAsQueryString();
            }
        } else {
            hrefValue += "?" + fetchDateOccupancyAsQueryString();
        }
        $(element).attr('href', hrefValue);
    } catch (error) {
        console.error("Error occured while setting the dateOccupancy Combination as query string");
    }
}


/*
// fixes for villa siolim redirect ama properties to accomodation pages
function attachAccommodation() {
    console.log('attached accomodation');
    if(location.pathname.includes('/en-in/destination/') && !isIHCLCBSite()){	      
    $(".mr-list-hotel-price-tic").each(function(index, value) {
            var btnElem = $(this).find('.button-view-details');
            var hrefValue = btnElem.attr('href');
            if (hrefValue && hrefValue.includes('ama') && hrefValue.includes('rooms-and-suites')) {
            hrefValue = hrefValue.replace('rooms-and-suites', 'accommodations');
            	btnElem.attr('href', hrefValue);
            }
        });
    }
}
// fixes for villa siolim redirect ama properties to accomodation pages
*/