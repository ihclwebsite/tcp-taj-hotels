$(document).ready(function() {
	
    function showPopUp() {
    	$('.cm-page-container').addClass("prevent-page-scroll");
        $('.specific-city-showMap-popup').show();
        $('.cm-specific-showMap-con').show();
    }

    function hidePopUp() {
    	$('.cm-page-container').removeClass("prevent-page-scroll");
        $('.specific-city-showMap-popup').hide();
        $('.cm-specific-showMap-con').hide();
    }

    $('.specific-city-local-show-map').click(function() {
        showPopUp();

    });

    $('.cm-map-btn.loc-city').click(function() {
 
        showPopUp();
    })

    $('.map-local-back').click(function() {
        hidePopUp();
    })

    $('.showMap-close').click(function() {
        hidePopUp();

    })

    $('.cm-map-btn.loc-city').click(function() {
        $('.specific-city-showMap-mobileView').show();
    });
    if (window.screen.width < 991) {
        $('.spec-city-desc > p').cmToggleText({
            charLimit : 200,
        });

    } else {
        $('.spec-city-desc > p').cmToggleText({
            charLimit :600,
        });
    }
    
});
