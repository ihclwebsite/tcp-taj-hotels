function onOfferDetailsHotelSelection(hotelPath) {
    var ROOMS_PATH = "/rooms-and-suites";
	var offerRateCode = $("[data-offer-rate-code]").data("offer-rate-code");
	var offerTitle = $("[data-offer-rate-code]").data("offer-title");
	var noOfNights= $("[data-offer-no-of-nights]").data("offer-no-of-nights");

	if(noOfNights){
		var bookOptions = dataCache.session.getData("bookingOptions");
		var fromDate = moment(bookOptions.fromDate, "MMM Do YY");
		var nextDate = moment(fromDate, "MMM Do YY").add(parseInt(noOfNights), 'days').format("MMM Do YY");
		bookOptions.toDate = nextDate;
		bookOptions.nights = parseInt(noOfNights);
		dataCache.session.setData("bookingOptions", bookOptions);
	}
	var navPath = hotelPath + ROOMS_PATH;
	if (offerRateCode) {
		navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
		navPath = updateQueryString("offerTitle", offerTitle, navPath);
	}
	window.location.href = navPath;

}

