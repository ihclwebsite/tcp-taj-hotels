$(document).ready(function() {
    $('[data-trigger-event]').click(function(){
        var event = $(this).data('trigger-event');
        $('body').trigger(event);
    });
});