var slot = '';
$(window).load(function() {

    invokecheckAvailabilityAjaxCall();
    var requestType = getParam('requestType');
    if (requestType == "UPDATE") {

        var previousName = getParam('firstname');
        var previousemailID = getParam('emailID');

        $("#fullName").html(previousName);
        $("#emailID").html(previousemailID);
    }
    $('.mr-reservetable-submit').on('click', function() {
        if (validateReserveTable()) {
            $('.pg-spinner-con').css('display', "block");
            var requestType = getParam('requestType');
            if (requestType != "UPDATE" && requestType == "") {
                invokeTableReservationAjaxCall();
            } else {
                invokeModifyReservationAjaxCall();
            }
        }

    });
    $('.mr-reserve-table-button-dining').on('click', function() {
        invokecheckAvailabilityAjaxCall();
        // analytic data
        var jsonObj = {};
        jsonObj.restaurantID = getParam('diningid');
        jsonObj.hotelRestaurantName = getParam('diningName');
        jsonObj.dateValue = $('.mr-date-reserveTable-input').val();
        jsonObj.mealType = $('#mr-reserveTable-meal-type').val();
        jsonObj.peopleCount = $('#mr-reserveTable-number-of-people').val();
        prepareJsonForCheckAvailibility("reserveTableNextBtn", jsonObj);
    });

});

function validateReserveTable() {
    var validInputs = true;
    $('.mr-reserve-table-form .sub-form-mandatory').each(function() {
        if ($(this).val() == "") {
            $(this).addClass('invalid-input');
            invalidWarningMessage($(this));
        }
    });

    if (!$('input[name="meal-timing"]:checked').val()) {
        $('.mr-breakFast-slot-Timing').addClass('error');
        validInputs = false;
    }

    if ($('.sub-form-mandatory.invalid-input').length > 0) {
        validInputs = false;
    }
    return validInputs;
}

function convertTime24to12(time24) {
    var tmpArr = time24.split('.'), time12;
    if (+tmpArr[0] == 12) {
        time12 = tmpArr[0] + ':' + tmpArr[1] + ' pm';
    } else {
        if (+tmpArr[0] == 00) {
            time12 = '12:' + tmpArr[1] + ' am';
        } else {
            if (+tmpArr[0] > 12) {
                time12 = (+tmpArr[0] - 12) + ':' + tmpArr[1] + ' pm';
            } else {
                time12 = (+tmpArr[0]) + ':' + tmpArr[1] + ' am';
            }
        }
    }
    return time12;
}

function populateDiningData(mealType, peopleCount, diningID, diningName, bookingDate, slot, firstName, eventName) {
    var diningReservationData = {};
    diningReservationData.mealType = mealType;
    diningReservationData.peopleCount = peopleCount;
    diningReservationData.diningID = diningID;
    diningReservationData.diningName = diningName;
    diningReservationData.bookingDate = bookingDate;
    diningReservationData.slot = slot;
    diningReservationData.firstName = firstName;
    diningReservationData.eventName = eventName;
    return diningReservationData;
}

function invokecheckAvailabilityAjaxCall() {
    var dateValue = $('.mr-date-reserveTable-input').val();
    var dateArray = dateValue.split("/");
    modifieddate = dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0];

    var mealType = $('#mr-reserveTable-meal-type').val();
    var peopleCount = $('#mr-reserveTable-number-of-people').val();
    var diningID = getParam('diningid');
    var diningName = getParam('diningName');
    var interfaceId = dataCache.session.getData("InterfaceId");

    diningReservationData = populateDiningData(mealType, peopleCount, diningID, diningName, dateValue);
    trigger_diningAvailability(diningReservationData);

    $('.mr-availability-SubTitle-header').empty();
    $('.mr-radio-mealType-wrapper').empty();
    $('.check-wait-spinner').show();

    $.ajax({
        type : 'GET',
        url : '/bin/checkAvailableTimeSlot',
        data : "dateVal=" + modifieddate + "&mealType=" + mealType + "&peopleCount=" + peopleCount + "&restaurantID="
                + diningID + "&interfaceId=" + interfaceId,
        success : function(response) {

            dataCache.session.setData("DiningDate", dateValue);
            dataCache.session.setData("PeopleCount", peopleCount);
            dataCache.session.setData("MealType", mealType);
            dataCache.session.setData("InterfaceId", interfaceId);

            if (response.AvailableSlots != null && response.responseCode == "SUCCESS") {
                $('.mr-availability-SubTitle-header').show();
                $('.mr-availability-SubTitle-header').empty();
                var availableTimeSlotMessage = 'Pick an available slot for ' + mealType;
                $('.mr-availability-SubTitle-header').append(availableTimeSlotMessage);
                var responseTime = response.AvailableSlots;
                displayAvailableSlots(responseTime);

                // calling analytic function to get the time slots
                timeSlotSelected();
            } else {
                displayNoTimeSlotAvailable(response);
            }
        },
        error : function(response) {
            displayNoTimeSlotAvailable(response);
        }
    });

}

function displayNoTimeSlotAvailable(response) {
    $('.check-wait-spinner').hide();
    $('.mr-availability-SubTitle-header').hide();
    if ($('.mr-radio-mealType-wrapper').find('.radio-inline.mr-mealTime-radio-label').length > 0) {
        $('.mr-radio-mealType-wrapper').find('.radio-inline.mr-mealTime-radio-label').remove();
    }
    var results = '<div class="radio-inline mr-mealTime-radio-label"><h6></h6></div>';
    $('.mr-radio-mealType-wrapper').append(results);
    var messages = response.message;
    $('.mr-radio-mealType-wrapper').find('.radio-inline.mr-mealTime-radio-label').append(messages);
    $('#dining-details-input-wrapper').hide();
}

function displayAvailableSlots(stringValue) {

    $('#dining-details-input-wrapper').show();
    var stringArray = stringValue.split(",");
    var repsonseVal = stringValue.split(",");
    var currentDiv = $.find(".mr-radio-mealType-wrapper");
    if ($('.mr-radio-mealType-wrapper').find('.radio-inline.mr-mealTime-radio-label').length > 0) {
        $('.mr-radio-mealType-wrapper').find('.radio-inline.mr-mealTime-radio-label').remove();

    }

    $('.check-wait-spinner').hide();
    for (var i = 0; i < stringArray.length; i++) {
        stringArray[i] = convertTime24to12(stringArray[i]);

        var modified = stringArray[i].replace(/\s/g, '');
        var temp = '<div class="radio-inline mr-mealTime-radio-label"><input class="mr-mealType-radio" id="breakfast-'
                + modified + 'radio" type="radio" value=' + repsonseVal[i]
                + ' name="meal-timing" /><label for="breakfast-' + modified + 'radio">' + stringArray[i]
                + '</label></div>';

        $('.mr-radio-mealType-wrapper').append(temp);

    }

}
function getParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function invokeTableReservationAjaxCall() {
    var dateValue = $('.mr-date-reserveTable-input').val();

    var dateArray = dateValue.split("/");
    modifieddate = dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0];

    var mealType = $('#mr-reserveTable-meal-type').val();

    var peopleCount = $('#mr-reserveTable-number-of-people').val();
    var diningID = getParam('diningid');
    var diningName = getParam('diningName');

    var firstName = $("#fullName").val();

    var emailID = $("#emailID").val();

    var mobileNumber = $("#mobileNumber").val();

    var reservationTime = $(".mr-mealType-radio:checked").val();
    var eventName = $(".mr-filter-checkbox:checked").val();

    var diningConfLink = $('.dining-conf-link').val();

    var requestobj = "dateVal=" + modifieddate + "&mealType=" + mealType + "&peopleCount=" + peopleCount
            + "&restaurantID=" + diningID + "&firstName=" + firstName + "&emailID=" + emailID + "&mobileNumber="
            + mobileNumber + "&reservationTime=" + reservationTime + "&eventName=" + eventName;

    diningReservationData = populateDiningData(mealType, peopleCount, diningID, diningName, modifieddate,
            reservationTime, firstName, eventName);
    trigger_tableReservation(diningReservationData);

    // analytic data
    var jsonObj = {};
    jsonObj.restaurantID = getParam('diningid');
    jsonObj.hotelRestaurantName = diningName;
    jsonObj.dateValue = $('.mr-date-reserveTable-input').val();
    jsonObj.mealType = $('#mr-reserveTable-meal-type').val();
    jsonObj.peopleCount = $('#mr-reserveTable-number-of-people').val();
    jsonObj.mobileNumber = mobileNumber;
    jsonObj.eventName = eventName;

    prepareJsonForCheckAvailibility("reservationTableSubmit", jsonObj);

    $.ajax({
        type : 'GET',
        url : '/bin/tableBookingReservation',
        data : "dateVal=" + modifieddate + "&mealType=" + mealType + "&peopleCount=" + peopleCount + "&restaurantID="
                + diningID + "&firstName=" + firstName + "&emailID=" + emailID + "&mobileNumber=" + mobileNumber
                + "&reservationTime=" + reservationTime + "&eventName=" + eventName,
        success : function(response) {

            var reservationID = response.ReservationId;
            var firstName = response.firstName;
            var bookingStatus = response.ReservationStatus;
            var emailID = response.emailID;
            var peopleCount = response.peopleCount;
            var dateValue = response.dateValue;
            var reservationTime = response.reservationTIme;
            var customerID = response.CustomerID;
            var restaurantID = response.restaurantID;
            if (response.responseCode == "SUCCESS") {

                ConfirmationPage(firstName, bookingStatus, emailID, peopleCount, dateValue, reservationTime,
                        customerID, reservationID, restaurantID);
            } else {
                window.location.assign(diningConfLink);
                $('.mr-booking-Status').empty();
                $('.mr-booking-Status').append("The Booking Operation was unsucessful");
                $('.mr-reserve-table-booker').hide();
                $('.ReserveTable-booking-details').hide();
                $('.mr-reserve-Table-CancelModify-btn').hide();
                $('.mr-reservationTable-mail-sent').hide();
            }

        },
        error : function(response) {

            window.location.assign(diningConfLink);
        }
    })
}

function ConfirmationPage(firstName, bookingStatus, emailID, peopleCount, dateValue, reservationTime, customerID,
        reservationID, restaurantID) {

    var modifiedTime = convertTime24to12(reservationTime);
    var diningName = getParam('diningName');
    var restaurantName = decodeURIComponent(diningName);

    var diningConfLink = $('.dining-conf-link').val();
    window.location.assign(diningConfLink + '?&firstname=' + firstName + '&bookingStatus=' + bookingStatus
            + '&emailID=' + emailID + '&peopleCount=' + peopleCount + '&dateValue=' + dateValue + '&reservationTime='
            + modifiedTime + '&restaurantName=' + restaurantName + '&customerID=' + customerID + '&restaurantID='
            + restaurantID + '&reservationID=' + reservationID + '&hotelWas=' + JSON.stringify(pageLevelData));

}

function invokeModifyReservationAjaxCall() {

    var customerID = getParam('customerID');
    var restaurantID = getParam('restaurantID');
    var reservationID = getParam('reservationID');
    var dateValue = $('.mr-date-reserveTable-input').val();
    var dateArray = dateValue.split("/");
    modifieddate = dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0];

    var mealType = $('#mr-reserveTable-meal-type').val();

    var peopleCount = $('#mr-reserveTable-number-of-people').val();

    var firstName = $("#fullName").val();

    var emailID = $("#emailID").val();

    var mobileNumber = $("#mobileNumber").val();

    var reservationTime = $(".mr-mealType-radio:checked").val();
    var eventName = $(".mr-filter-checkbox:checked").val();

    $.ajax({
        type : 'GET',
        url : '/bin/modifyTableBookingReservation',
        data : "dateVal=" + modifieddate + "&mealType=" + mealType + "&peopleCount=" + peopleCount + "&restaurantID="
                + restaurantID + "&firstName=" + firstName + "&emailID=" + emailID + "&mobileNumber=" + mobileNumber
                + "&reservationTime=" + reservationTime + "&eventName=" + eventName + "&customerID=" + customerID
                + "&reservationID=" + reservationID,
        success : function(response) {

            var reservationID = response.ReservationId;
            var firstName = response.firstName;
            var bookingStatus = response.message;
            var emailID = response.emailID;
            var peopleCount = response.peopleCount;
            var dateValue = response.dateValue;
            var reservationTime = response.reservationTIme;
            var customerID = response.CustomerID;

            var restaurantID = response.restaurantID;

            if (response.responseCode == "SUCCESS") {
                ConfirmationPage(firstName, bookingStatus, emailID, peopleCount, dateValue, reservationTime,
                        customerID, reservationID, restaurantID);
            }
        }
    })
}

function timeSlotSelected() {
    // slot=$("input[name='meal-timing']:checked").val();
    $('.radio-inline.mr-mealTime-radio-label .mr-mealType-radio').bind('click', function() {
        slot = $("input[name='meal-timing']:checked").val();
        $('.mr-breakFast-slot-Timing.error').removeClass('error');
        return slot;
    });
}

// analytics data
function prepareJsonForCheckAvailibility(stepName, jsonObj) {

    var bookingInfo = {};
    var pageHierarchy = [];

    bookingInfo.event = stepName;
    if (jsonObj != undefined) {
        jsonObj.hotelRestaurantName = decodeURI(jsonObj.hotelRestaurantName);
        bookingInfo["diningId"] = jsonObj.restaurantID;
        bookingInfo.guestDiningDate = jsonObj.dateValue;
        bookingInfo.guestDiningTime = slot;
        bookingInfo.guestEventType = "TableReservation";
        bookingInfo.guestMealType = jsonObj.mealType;
        bookingInfo.guestTotal = jsonObj.peopleCount;
        if (stepName == "reservationTableSubmit") {

            bookingInfo.guestMobileNo = jsonObj.mobileNumber;
            bookingInfo.guestOccasion = jsonObj.eventName;
            bookingInfo.guestSpecialRequest = "";

        }

        if (dataLayerData != undefined) {
            bookingInfo.hotelCity = dataLayerData.hotelCity;
            bookingInfo.hotelCode = dataLayerData.hotelCode;
            bookingInfo.hotelCountry = dataLayerData.hotelCountry;
            bookingInfo.hotelName = dataLayerData.hotelName;
            bookingInfo.hotelRegion = dataLayerData.hotelCity;
            bookingInfo.hotelRestaurantName = jsonObj.hotelRestaurantName;

            if (pageHierarchy.length > 0) {
                pageHierarchy = dataLayerData.pageHierarchy;
                if (stepName == "reserveTableCheckAvailibility"
                        && (pageHierarchy[(pageHierarchy.length - 1)]) != "Booking Information") {
                    pageHierarchy.push("Booking Information");
                } else if (stepName == "reserveTableNextBtn"
                        && (pageHierarchy[(pageHierarchy.length - 1)]) != "Personal Details") {
                    pageHierarchy.push("Personal Details");
                } else if (stepName == "reservationTableSubmit"
                        && (pageHierarchy[(pageHierarchy.length - 1)]) != "Confirmation") {
                    pageHierarchy.push("Confirmation");
                }
            }
            bookingInfo.pageHirarchy = pageHierarchy
            bookingInfo.pageLanguage = dataLayerData.pageLanguage;
            bookingInfo.pageTitle = dataLayerData.pageTitle;
        }

        pushHotelJson(bookingInfo);
    }
}
