function createModifyBookingDetailsJson(modifyBookingState) {
        var modifyBookedOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest')) || new Object();
        var bookingOptionsObject = dataCache.session.getData('bookingOptions') || {};
        bookingOptionsObject.hotelId = modifyBookedOptions.hotelId;
        var checkinDate=modifyBookedOptions.checkInDate;
        var checkoutDate=modifyBookedOptions.checkOutDate;
        bookingOptionsObject.fromDate = moment(checkinDate, "YYYY-MM-DD").format("MMM Do YY");
        bookingOptionsObject.toDate = moment(checkoutDate,"YYYY-MM-DD").format("MMM Do YY");
        var roomsBooked = modifyBookedOptions.roomList;
        var rooms =0;
        var roomOptions=[];
        var selection,totalAdults,totalChildren;
        var totalCostWithTax, totalCostWithoutTax;
        var totalNights;
        var selectionArray = [];            
        $.each(roomsBooked, function (index, data) {
            if(data.modifyRoom == modifyBookingState && data.modifyRoom!=false && data.resStatus!='Cancelled'){

                //creating rooms bookingOptionsObject               
                var obj = new Object();
                obj.children = data.noOfChilds;
                obj.adults = data.noOfAdults;
                obj.initialRoomIndex = rooms;
                obj.reservationNumber = data.reservationNumber;

                var userSelection = new Object();
                var bedOpt = new Object();
                var bedType = data.bedType;
                bedOpt[bedType] = data.roomTypeCode;
                userSelection.bedTypeOptions = bedOpt;
                userSelection.title = data.roomTypeName;
                userSelection.roomShortDescription = data.roomTypeDesc;
                userSelection.roomLongDescription = data.roomTypeDesc;
                userSelection.roomBedType = data.bedType;
                userSelection.hotelId = modifyBookedOptions.hotelId;
                userSelection.currencyString = modifyBookedOptions.currencyCode;
                userSelection.roomTypeCode = data.roomTypeCode;
                userSelection.ratePlanCode = data.ratePlanCode;
                userSelection.rateDescription = data.rateDescription;
                userSelection.dailyNightlyRate = data.nightlyRates.price;
                userSelection.dailyNightlyRateWithTax = data.nightlyRates.priceWithFeeAndTax;
                userSelection.roomIndex = rooms;
                userSelection.adults = data.noOfAdults;
                userSelection.children = data.noOfChilds;
                userSelection.initialRoomIndex = rooms;
                userSelection.roomBaseRate = data.roomCostBeforeTax;
                userSelection.selectedRate = data.roomCostBeforeTax;
                userSelection.roomTaxRate = (parseFloat(data.roomCostAfterTax) - parseFloat(data.roomCostBeforeTax)); 
                userSelection.nightlyRates = data.nightlyRates;
                userSelection.taxes = data.taxes;
                obj.userSelection = userSelection;              
                selectionArray.push(userSelection);                                 
                roomOptions.push(obj);
                rooms+1;
                
                totalCostWithoutTax = data.roomCostBeforeTax;
                        totalCostWithTax = data.roomCostAfterTax;
                totalNights = data.nightlyRates.length || 0;
                bookingOptionsObject.bookedRoomsModify = [];
                bookingOptionsObject.bookedRoomsModify.push(data);
            }
        });
        //bookingOptions.nights = ;     
        bookingOptionsObject.rooms = rooms;
        bookingOptionsObject.nights = totalNights;
        bookingOptionsObject.roomOptions = roomOptions;
        bookingOptionsObject.selection = selectionArray;
        bookingOptionsObject.hotelChainCode = modifyBookedOptions.chainCode;
        bookingOptionsObject.hotelResourcePath = modifyBookedOptions.hotel.hotelResourcePath;
        bookingOptionsObject.targetEntity = modifyBookedOptions.hotel.hotelName;
        bookingOptionsObject.currencySelected = modifyBookedOptions.currencyCode;
        bookingOptionsObject.totalCartPrice = modifyBookedOptions.totalAmountAfterTax;
        bookingOptionsObject.totalPriceWithoutTax = modifyBookedOptions.totalAmountBeforeTax;
        bookingOptionsObject.totalCartTax = (parseFloat(modifyBookedOptions.totalAmountAfterTax) - parseFloat(modifyBookedOptions.totalAmountBeforeTax));
        bookingOptionsObject.roomCount = rooms;

        bookingOptionsObject.guest = modifyBookedOptions.guest;


//      var bookingOptions= JSON.stringify(bookingOptionsObject);

        dataCache.session.setData("bookingOptions", bookingOptionsObject);


    }
    
    function dateOrdinal(dom) {
    if (dom == 31 || dom == 21 || dom == 1) return dom + "st";
    else if (dom == 22 || dom == 2) return dom + "nd";
    else if (dom == 23 || dom == 3) return dom + "rd";
    else return dom + "th";
    };

function clearModifyBookingCache(){
    dataCache.session.setData('modifyBookingState',false);
    var bookingOptionsCache = dataCache.session.getData('bookingOptions');  
    bookingOptionsCache.bookedRoomsModify = [];
    bookingOptionsCache.roomOptions = [{
        adults: "1",
        children: "0",
        initialRoomIndex: 0
    }];
    bookingOptionsCache.selection = [];
    dataCache.session.setData('bookingOptions',bookingOptionsCache);
    var bookingDetailsResponseCache = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
    $(bookingDetailsResponseCache.roomList).each(function(index){
        bookingDetailsResponseCache.roomList[index].modifyRoom = false;
    });
    dataCache.session.setData('bookingDetailsRequest',JSON.stringify(bookingDetailsResponseCache));

}
$("document").ready(function(){
    if($('.cm-page-container').hasClass('ama-theme')){
            var dataCheckIn =dataCache.session["data"]["checkInTime"];
            var dataCheckOut  =dataCache.session["data"]["checkOutTime"];
            if( dataCheckIn != "" &&  dataCheckOut != ""){
                $(".checkInTime ").text("Check-in("+dataCheckIn+")");
                $(".checkOutTime").text(" Check-out("+dataCheckOut+")");
            }
        }
    });