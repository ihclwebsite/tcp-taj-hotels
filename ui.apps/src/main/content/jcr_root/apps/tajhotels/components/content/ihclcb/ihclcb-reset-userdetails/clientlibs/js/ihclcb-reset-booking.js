$(document).ready(
        function() {
            var $popupContainer = $('.ihclcb-reset-booking-overlay');
            $('.ihcl-theme .ihcl-header .navbar-brand a').click(
                    function(e) {
                        var userDetailsIhclcb = getUserDetailsIhclcb();
                        var bookingDetailsRequest = dataCache.session.getData("bookingDetailsRequest");
                        if (!bookingDetailsRequest && !isIHCLCBHomePage() && userDetailsIhclcb
                                && (userDetailsIhclcb.selectedEntity || userDetailsIhclcb.selectedEntityAgent)) {
                            $popupContainer.addClass('ihclcb-show-popup');
                            e.preventDefault();
                            $('.ihclcb-reset-booking-back-btn , .ihclcb-reset-booking-popup-close-icon').click(
                                    function(e) {
                                        $popupContainer.removeClass('ihclcb-show-popup');
                                    });
                            $('.ihclcb-reset-booking-confirm-btn').click(function() {
                                userDetailsIhclcb.selectedEntity = null;
                                userDetailsIhclcb.selectedEntityAgent = null;
                                dataCache.local.setData("userDetails", userDetailsIhclcb);
                                var bookingOptionsIhclcb = getBookingDetailsIhclcb();
                                if (bookingOptionsIhclcb) {
                                    dataCache.session.removeData("bookingOptions");
                                }
                                window.location.href = ihclcbDashboardPage;
                            });
                        }
                    });
        });

function getUserDetailsIhclcb() {
    var userDetailsIhclcb = dataCache.local.getData("userDetails");
    if (userDetailsIhclcb) {
        return userDetailsIhclcb;
    }
    return false;
}

function getBookingDetailsIhclcb() {
    var bookingOptionsIhclcb = dataCache.session.getData("bookingOptions");
    if (bookingOptionsIhclcb) {
        return bookingOptionsIhclcb;
    }
    return false;
}
