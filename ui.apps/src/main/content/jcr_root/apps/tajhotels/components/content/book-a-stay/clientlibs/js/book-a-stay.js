var availabilityObj = {
    initialSelecton : '',
    isCheckParamsUpdated : false,
    isRoomTypeUpdated : false
};
var domainChangeFlag = false;
var isToDateClickedTriggered;
bookedRoomsCount = 0;
var isTajHolidays = false;
var isAmaCheckAvailability = false;
var isAmaSite = false;
var amaBookingObject = {};
$('document').ready(
        function() {
            try {
                isAmaSite = $('.cm-page-container').hasClass('ama-theme');

				dataCache.session.setData("sameDayCheckout", $('.mr-hotel-details').attr('data-samedaycheckout'));

//                sameDayCheckout = $('.mr-hotel-details').attr('data-samedaycheckout');
//               console.log("sameDayCheckout::::::::", sameDayCheckout);

                initializeCheckAvailability();
                initializeDatePicker();
                disableBestAvailableButton();
                // setBookAStaySessionObject();
                autoPopulateBookAStayWidget();
                if (!verifyIfRoomsPage()) {
                    fetchDateOccupancyParametersFromURL();
                }

                _globalListOfPromoCode = getPromoCodeFromData();
                var url = window.location.href;
                if (($('#page-scope').attr('data-pagescope') == 'Taj Holidays')
                        || (url && url.indexOf('taj-holiday-redemption') != -1)) {
                    checkHolidayScope()
                    isTajHolidays = true;
                }
                if (dataCache.session.getData('bookingDetailsRequest')) {
                    bookedOptions = JSON.parse(dataCache.session.getData('bookingDetailsRequest'));
                    bookedCheckInDate = moment(bookedOptions.checkInDate, "YYYY-MM-DD").format("MMM Do YY");
                    bookedCheckOutDate = moment(bookedOptions.checkOutDate, "YYYY-MM-DD").format("MMM Do YY");
                    bookedAdultsOccupancy = bookedOptions.roomList[0].noOfAdults;
                    bookedChildrenOccupancy = bookedOptions.roomList[0].noOfChilds;
                    bookedRoomsCount = bookedOptions.roomList.length || 0;
                }
                if ((window.location.href.indexOf('rooms-and-suites') != -1) || isTajHolidays
                        || $('.cm-page-container').hasClass('destination-layout')) {
                    dateOccupancyInfoSticky();
                }

                // [IHCL_CB START]
                initIHCLCBBookAStay();
                // [IHCL_CB ENDS]

            } catch (error) {
                console.error("Error in /apps/tajhotels/components/content/book-a-stay/clientlibs/js/book-a-stay.js ",
                        error.stack);
            }
        });

function initIHCLCBBookAStay() {
    if (isIHCLCBSite()) {
        fetchIHCLCBEntityDetails();
        addEntityDropDownEventsForIHCLCB();
    }
}

function fetchDateOccupancyParametersFromURL() {
    try {
        var bookingOptions = {};
        var _globalPromoCode = {
            name : null,
            value : null
        };
        var checkInDate = getQueryParameter('from');
        var checkOutDate = getQueryParameter('to');
        var rooms = getQueryParameter('rooms');
        var adults = getQueryParameter('adults');
        var children = getQueryParameter('children');
        if (checkInDate && checkOutDate) {
            var nights = moment(checkOutDate, "DD.MM.YYYY").diff(moment(checkInDate, "DD.MM.YYYY"), 'days');
            checkInDate = moment(checkInDate, 'DD/MM/YYYY').format('MMM Do YY');
            checkOutDate = moment(checkOutDate, 'DD/MM/YYYY').format('MMM Do YY');
            bookingOptions.fromDate = checkInDate;
            bookingOptions.toDate = checkOutDate;
            bookingOptions.nights = parseInt(nights)

        }
        if (rooms && adults && children) {
            if (validateRoomsQueryParams(rooms, adults, children)) {
                var roomOptions = [];
                var adultsArr = adults.split(",");
                var childArr = children.split(",");
                for (var index = 0; index < rooms; index++) {
                    roomOptions.push({
                        "adults" : adultsArr[index],
                        "children" : childArr[index],
                        "initialRoomIndex" : index
                    });
                }
                bookingOptions.roomOptions = roomOptions;
            }
        }
        if (checkInDate && checkOutDate && rooms && adults && children) {
            bookingOptions.isAvailabilityChecked = true;
            if (bookingOptions.rooms == 0) {
                bookingOptions.rooms = 1;
            }
            bookingOptions.previousRooms = bookingOptions.roomOptions;
            bookingOptions.previousDates = {
                fromDate : bookingOptions.fromDate,
                toDate : bookingOptions.toDate
            };
            bookingOptions.selection = [];
            bookingOptions.promoCode = _globalPromoCode.value;
            bookingOptions.promoCodeName = _globalPromoCode.name;
            bookingOptions.hotelChainCode = null;
            bookingOptions.hotelId = null;
            var redirectFromAmp = getQueryParameter('redirectFromAmp');
            if (redirectFromAmp) {
                var promoCode = getQueryParameter('promoCode');
                var hotelId = getQueryParameter('hotelId');
                var targetEntity = getQueryParameter('targetEntity');
                var isAvailabilityChecked = getQueryParameter('isAvailabilityChecked');
                if (!promoCode) {
                    promoCode = "";
                }
                bookingOptions.promoCode = promoCode;
                if (!hotelId) {
                    hotelId = null;
                }
                bookingOptions.hotelId = hotelId;
                if (!targetEntity) {
                    targetEntity = null;
                }
                bookingOptions.targetEntity = targetEntity;
                if (!isAvailabilityChecked) {
                    isAvailabilityChecked = false;
                }
                bookingOptions.isAvailabilityChecked = isAvailabilityChecked;
            }
            dataCache.session.setData('bookingOptions', bookingOptions);
            updateCheckAvailability();
            removeDateOccupancyParamFromUrl();
        }
    } catch (err) {
        console.error(err);
    }
}

function validateRoomsQueryParams(rooms, adults, children) {
    var isValid = false;
    if (isInt(rooms)) {
        if (rooms > 0 && rooms <= 5) {
            if (adults.split(",").length == rooms && children.split(",").length == rooms) {
                if (isOccupantsParamValidFor(adults, 1, 7) && isOccupantsParamValidFor(children, 0, 7)) {
                    isValid = true;
                } else {
                    isValid = false;
                    console
                            .error("Non Integer parameters passed in Adults/Children or Min/Max Adults[1,7]/Childrens[0,7] occupancy validation failed");
                }
            } else {
                isValid = false;
                console.error("No of Adults and Childrens not matching with No of Rooms");
            }
        } else {
            isValid = false;
            console.error("Min/Max No of Rooms [1,5] validation failed");
        }
    } else {
        isValid = false;
        console.error("Invalid Input Parameter passed as rooms");
    }
    return isValid;
}

function isOccupantsParamValidFor(occupants, minValue, maxValue) {
    var isValid = occupants.split(",").every(function(x) {
        if (isInt(x)) {
            if (x >= minValue && x <= maxValue) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    return isValid;
}

function verifyIfRoomsPage() {
    var presentUrl = window.location.href;
    if (presentUrl.includes("rooms-and-suites") || presentUrl.includes("accommodations")
            || presentUrl.includes("booking-cart")) {
        return true;
    }
    return false;
}

function removeDateOccupancyParamFromUrl() {
    var presentUrl = window.location.href;
    var paramsToRemoveArr = [ "from", "to", "rooms", "adults", "children", "overrideSessionDates" ];
    var refinedUrl = '';
    paramsToRemoveArr.forEach(function(param, index) {
        presentUrl = removeURLParameter(presentUrl, param);
    });
    refinedUrl = presentUrl;
    window.history.replaceState({}, document.title, refinedUrl);

}

function removeURLParameter(url, parameter) {
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        for (var i = pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }
        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}

function isInt(value) {
    return !isNaN(value) && (function(x) {
        return (x | 0) === x;
    })(parseFloat(value))
}

/*
 * If query params contains redirectFromAmp, we initialize the bookingOptions from query params.
 */
function getBookAStayUrlParams() {
    try {
        if (URLSearchParams) {
            var params = new URLSearchParams(location.search);
            if (params.has("redirectFromAmp")) {
                var fromDate = checkFalseString(params.get("fromDate"))
                        || moment(new Date()).add(14, 'days').format("MMM Do YY");
                //var toDate = checkFalseString(params.get("toDate")) || moment(new Date()).add(15, 'days').format("MMM Do YY");

				var toDate = checkFalseString(params.get("toDate")) || initialBookingSetup();

                   bookingOptions = {
                    fromDate : fromDate,
                    toDate : toDate,
                    rooms : 1,
                    nights : moment(toDate, "MMM Do YY").diff(moment(fromDate, "MMM Do YY"), 'days'),
                    roomOptions : [ {
                        adults : checkFalseString(params.get("adults")) || 1,
                        children : checkFalseString(params.get("children")) || 0,
                        initialRoomIndex : 0
                    } ],
                    selection : [],
                    promoCode : checkFalseString(params.get("promoCode")) || "",
                    hotelId : checkFalseString(params.get("hotelId")) || null,
                    targetEntity : checkFalseString(params.get("targetName")) || null,
                    isAvailabilityChecked : false
                };

                dataCache.session.setData("bookingOptions", bookingOptions);
            }
        }
    } catch (error) {
        console.error(error);
    }
}

// Value returned from URLSearchParams can be "null" so explicit check for that.
function checkFalseString(value) {
    if (value !== "null") {
        return value;
    }
    return false;
}

function getPromoCodeFromData() {
    var promoCodeList = $($.find("[data-promo-code]")[0]).data();
    if (promoCodeList) {
        return promoCodeList.promoCode.promoCodes;
    } else {
        $('.bas-specialcode-container').remove();
    }
    return null;
}

function initializeCheckAvailability() {
    $('#cmCheckAvailability, #cmCheckAvailabilitySmallDevice, .book-stay-btn ').off('click').on('click', function(e) {
        e.stopPropagation();
        dataCache.session.setData("sameDayCheckout", $('.mr-hotel-details').attr('data-samedaycheckout'));
        isAmaCheckAvailability = false;
        $('.cm-page-container').addClass("prevent-page-scroll");
        $('.cm-bas-con').addClass('active');
        $(".cm-bas-content-con").css("max-height", 0.95 * (window.innerHeight));
        autoPopulateBookAStayWidget();
        initiateCalender();
        modifyBookingState = dataCache.session.getData('modifyBookingState');
        if (modifyBookingState && modifyBookingState != 'modifyRoomType') {
            modifyBookingInBookAStay(modifyBookingState);
        }
        resetPromoCodeTab();
    });

    var openBookAStay = getUrlParameter("openBookAStay")
    if (openBookAStay == "true") {
        $(".book-stay-btn").click();
    }

};

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName, i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function promptToSelectDate() {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    var elDatePrompt = $(".mr-checkIn-checkOut-Date .mr-checkIn-checkOut-Date-hover");
    if (!bookingOptions.isAvailabilityChecked) {
        elDatePrompt.css('display', 'block');
        elDatePrompt.addClass('cm-scale-animation');
    } else {
        elDatePrompt.css('display', 'none');
        elDatePrompt.removeClass('cm-scale-animation');
    }
}

function initializeDatePicker() {
    _globalPromoCode = {
        name : null,
        value : null
    };

    $('.bas-overlay').on('click', function(e) {
        e.stopPropagation();
        destroyDatepicker();
    });

    // pop up close
    $(".bas-close").click(function(e) {
        e.stopPropagation();
        destroyDatepicker();
    });

    $(".trigger-promo-code-input").on("click", function() {
        $(".promo-code-wrap").css("display", "block");
        $(this).parent().hide();
    });

    $(".close-promocode-section").on("click", function() {
        $(".promo-code-wrap").hide();
        $(".bas-have-promo-code").show();
    });

    $('.special-code-wrap .bas-aroow-btn').on('click', function() {
        $('.bas-code-wrap.clearfix').toggleClass('active');
        $(this).toggleClass('arrow-btn');
    });

    $(".input-daterange input").each(function(e) {
        $(this).focus(function(e) {
            $('.bas-calander-container').addClass('active');
            $(".bas-left-date-wrap").removeClass("active");
            $(".bas-right-date-wrap").removeClass("active");
            $(this).parent().parent().addClass("active");
        });
    });

    // adding quantity script starts
    $(".bas-room-details-container").delegate(".quantity-right-plus", "click", incrementSelectionCount);
    // adding quantity script ends

    // subtracting quantity script starts
    $(".bas-room-details-container").delegate(".quantity-left-minus", "click", decrementSelectionCount);
    // subtracting quantity script ends

    // SCRIPT for addroom button starts

    $(".bas-add-room").on('click', function() {
        addRoomModule();
        if (isIHCLCBHomePage()) {
            createguestCountContainer();
            // activate last room
            $('.bas-room-no').last().click();
        }
    });

    // SCRIPT for addroom button ends

    // adding active class when click on room no starts
    $(".bas-room-details-container").on("click", ".bas-room-no", function() {
        makeRoomActiveModule($(this));
    });
    // adding active class when click on room no ends

    // delete button script starts
    $(".bas-room-details-container").on("click", ".bas-room-delete-close", function(e) {
        e.stopPropagation();
        if ($(".bas-room-details").length > 1) {
            var roomDeleteIndex = $(this).closest('.bas-room-no').index('.bas-room-no');
            deleteRoomModule(roomDeleteIndex);
        }
    });

    // script by arvind ends
    $('.input-daterange input').blur(function(e) {
        $('.bas-hotel-details-container').removeClass('active');
    });

    $("#input-box1 , #input-box2").on('change', function(e) {
        updateFinalDatePlanModule();
        amaBookingObject.isAmaCheckAvailabilitySelected = true;
    });

    $('.active #input-box1').on('change', function(e) {
        var $nextInput = $('.input-box.date-explore').not($(this));
        var compareVal = $nextInput.val();

        var $self = $(this);
        var currVal = $(this).val();

        var bookingOptionsSelected = dataCache.session.getData("bookingOptions");
        var fromDateSelecetd = moment(bookingOptionsSelected.fromDate, "MMM Do YY").format("D MMM YY");

        if (currVal == fromDateSelecetd) {
            return;
        }

        setTimeout(function() {
            $(this).blur();
            $('.bas-calander-container').children().remove();
            $('#input-box2').focus();

            //var nextDate = moment(currVal, "D MMM YY").add(1, 'days');
			var nextDate = checkSameDayCheckout(currVal);

            if (isTajHolidays || getQueryParameter('holidaysOffer')) {
                var holidaysNights = 2;
                var holidaysCacheData = dataCache.session.getData('bookingOptions');
                if (holidaysCacheData && holidaysCacheData.nights)
                    holidaysNights = holidaysCacheData.nights;
                nextDate = moment(currVal, "D MMM YY").add(holidaysNights, 'days');
            }

            $nextInput.focus();
            $nextInput.datepicker('setDate', new Date(nextDate.toDate()));
            isToDateClickedTriggered = true;

            var $dateTable = $(".datepicker .datepicker-days table");
            updateFinalDatePlanModule();

        }, 100);

    });

    $('#input-box2').on('change', function(e) {
        var $self = $(this);
        var isTargetInput = $self.closest('.bas-right-date-wrap').hasClass('active');
        var $nextInput = $('.input-box.date-explore').not($self);
        var compareVal = $nextInput.val();
        var currVal = $self.val();

        var bookingOptionsSelected = dataCache.session.getData("bookingOptions");
        var toDateSelecetd = moment(bookingOptionsSelected.toDate, "MMM Do YY").format("D MMM YY");

        if (isTargetInput) {

            setTimeout(function() {
                if (!isToDateClickedTriggered) {
                    hideCalenderInBookAStayPopup()
                    $self.parent().parent().removeClass("active");
                } else {
                    isToDateClickedTriggered = false;
                }

                $self.blur();

                if ((compareVal == currVal) && (compareVal != "") && (currVal != "")) {

                    //var nextDate = moment(currVal, "D MMM YY").add(1, 'days');
					var nextDate = checkSameDayCheckout(currVal);

                    if (isTajHolidays || getQueryParameter('holidaysOffer')) {
                        var holidaysNights = 2;
                        var holidaysCacheData = dataCache.session.getData('bookingOptions');
                        if (holidaysCacheData && holidaysCacheData.nights)
                            holidaysNights = holidaysCacheData.nights;
                        nextDate = moment(currVal, "D MMM YY").add(holidaysNights, 'days');
                    }

                    $self.datepicker('setDate', new Date(nextDate.toDate()));

                    var $dateTable = $(".datepicker .datepicker-days table");
                }
            }, 100);

            updateFinalDatePlanModule();

        }
    });

    // Click on Best available rate to proceed further
    $('.best-avail-rate').on('click', function(e) {
        var noOfNight = numberOfNightsCheck();
        if (noOfNight == false) {
            numberOfNightsExcessWarning();
        } else
            onClickOnCheckAvailabilty();
    });

    // removing links from search suggestion within booking widget
    $('.cm-bas-content-con').find('.suggestions-wrap').find('a').each(function() {
        $(this).removeAttr('href');
    });

    $('.promo-code').on("keyup", function() {
        var promoCodeInput = $(this).val();
        if (promoCodeInput.length > 0) {
            $('.apply-promo-code-btn').show();
            $('.promo-code-clear-input').show();
        } else {
            $('.apply-promo-code-btn').hide();
            $('.promo-code-clear-input').hide();
        }
    });

    $('.apply-promo-code-btn').on("click", function() {
        validatePromocode(_globalPromoCode);
    });

    $('.promo-code-clear-input').on("click", function() {
        $('.promo-code').val("");
        $(this).hide();
        $('.apply-promo-code-btn').hide();
        $('.promocode-status-text').text("");
        _globalPromoCode.value = null;
        _globalPromoCode.name = null;
    });

    $(".cm-bas-content-con .searchbar-input").keyup(function() {
        disableBestAvailableButton();

        $(this).attr("data-selected-search-value", "");
    });

    // Event listeners ends here
}

function numberOfNightsExcessWarning() {
    var popupParams = {
        title : 'Are you sure? You have selected more than 1O night.',
        callBack : onClickOnCheckAvailabilty.bind(),
        // callBackSecondary: secondaryFn.bind( _self ),
        needsCta : true,
        isWarning : true
    }
    warningBox(popupParams);
}

function navigateToRoomsPage(validationResult, clearExistingCart) {
    if (validationResult) {
        var nextPageHref;
        if (isAmaCheckAvailability) {
            nextPageHref = $('#checkAvailability').attr('hrefvalue');
        } else {
            nextPageHref = $('#global-re-direct').attr('hrefValue');
        }

        var bookingOptions = dataCache.session.getData("bookingOptions");
        if (clearExistingCart) {
            $(bookingOptions.roomOptions).each(function(index) {
                delete bookingOptions.roomOptions[index].userSelection;
            });
            bookingOptions.selection = [];
            $('.book-ind-container').css('display', 'none');
            $('.room-details-wrap, .bic-rooms').html('');
        }
        bookingOptions.isAvailabilityChecked = true;
        if (bookingOptions.rooms == 0) {
            bookingOptions.rooms = 1;
        }
        bookingOptions.previousRooms = bookingOptions.roomOptions;
        var BungalowType = bookingOptions.BungalowType
        if (!BungalowType) {
            BungalowType = null;
        }
        bookingOptions.previousDates = {
            fromDate : bookingOptions.fromDate,
            toDate : bookingOptions.toDate,
            BungalowType : BungalowType
        };
        dataCache.session.setData("bookingOptions", bookingOptions);
        var offerCodeIfAny = null;
        var offerTitleIfAny = false;
        var holidaysOfferQueryParam = false;
        // [TIC-FLOW]
        if (!isTicBased()) {
            offerCodeIfAny = $('[data-offer-rate-code]').data('offer-rate-code');
            // HolidayOfferTitle
            offerTitleIfAny = getQueryParameter('offerTitle');
            holidaysOfferQueryParam = getQueryParameter('holidaysOffer');
        }
        var usedVoucherCode = $('#usedVoucher').text();
        if (usedVoucherCode != undefined && usedVoucherCode != "" && usedVoucherCode != " ") {
            var bookingOptionsCache = dataCache.session.getData('bookingOptions');
            if (bookingOptionsCache.usedVoucherCode == usedVoucherCode) {
                offerCodeIfAny = "X5";
            }

        }
        if ($('#page-scope').attr('data-pagescope') == 'Taj Holidays')
            dataCache.session.setData("checkHolidayAvailability", true);
        else
            dataCache.session.setData("checkHolidayAvailability", false);

        if ($('.rate-code').val() || $('.promo-code').val() || $('.agency-id').val()) {
            var bookingSessionData = dataCache.session.getData("bookingOptions");
            var checkInDate = moment(bookingSessionData.fromDate, "MMM Do YY").format("YYYY-MM-DD");
            var checkOutDate = moment(bookingSessionData.toDate, "MMM Do YY").format("YYYY-MM-DD");

            var roomOptions = bookingSessionData.roomOptions;
            var roomOptionsLength = roomOptions.length;
            var adults, children;
            var searchHotelId = $("#hotelIdFromSearch").text();
            var rateCode = $('.rate-code').val();
            var agencyId = $('.agency-id').val();
            var promoAccessCode = $('.promo-code').val();
            for (var r = 0; r < roomOptionsLength; r++) {
                var adlt = roomOptions[r].adults;
                var chldrn = roomOptions[r].children;
                if (r == 0) {
                    adults = adlt;
                    children = chldrn;
                } else {
                    adults = adults + ',' + adlt;
                    children = children + ',' + chldrn;
                }
            }
            var synxisRedirectLink = 'https://be.synxis.com/?' + 'arrive=' + checkInDate + '&depart=' + checkOutDate
                    + '&rooms=' + roomOptionsLength + '&adult=' + adults + '&child=' + children + '&hotel='
                    + searchHotelId + '&chain=21305' + '&currency=' + '&level=chain' + '&locale=en-US' + '&sbe_ri=0';

            synxisRedirectLink = synxisRedirectLink + (!!agencyId ? '&agencyid=' + agencyId : '')
                    + (!!rateCode ? '&rate=' + rateCode : '') + (!!promoAccessCode ? '&promo=' + promoAccessCode : '')
            nextPageHref = synxisRedirectLink;
        }
        if (isAmaSite) {
            var offerRateCode = $('[data-offer-rate-code]').data('offer-rate-code');
            if (offerRateCode) {
                nextPageHref = nextPageHref.concat('?offerRateCode=' + offerRateCode + '&checkAvail=true');
            } else if (promoAccessCode == null || promoAccessCode == '') {
                nextPageHref = nextPageHref.concat('?checkAvail=true');
            } else {
                nextPageHref = nextPageHref.concat('&checkAvail=true');
            }
            if (isAmaCheckAvailability) {
                if (isBungalowSelected()) {
                    nextPageHref += "&onlyBungalows=true";
                }
            } else {
                if ($('#onlyBungalowBtn').is(':checked')) {
                    nextPageHref += "&onlyBungalows=true";
                }
            }

        } else {
            var promoTabParamIfAny = getQueryParameter('promoCode');
            var rataTabParamIfAny = getQueryParameter('rateTab');
            var pageParam = (!!offerCodeIfAny ? 'offerRateCode=' + offerCodeIfAny : '')
                    + (offerTitleIfAny ? '&offerTitle=' + offerTitleIfAny : '')
                    + (holidaysOfferQueryParam ? '&holidaysOffer=' + holidaysOfferQueryParam : '')
                    + (promoTabParamIfAny ? 'promoCode=' + promoTabParamIfAny : '')
                    + (rataTabParamIfAny ? '&rateTab=' + rataTabParamIfAny : '');

            if (pageParam) {
                nextPageHref = nextPageHref + "?" + pageParam;
            }
        }
        if (nextPageHref.includes('amastaysandtrails.com')) {
            nextPageHref = nextPageHref.replace('rooms-and-suites', 'accommodations');
        }
        if (domainChangeFlag) {
            if (nextPageHref.includes("?")) {
                if (nextPageHref.charAt(nextPageHref.length - 1) === "?") {
                    nextPageHref += fetchDateOccupancyAsQueryString();
                } else {
                    nextPageHref += "&" + fetchDateOccupancyAsQueryString();
                }
            } else {
                nextPageHref += "?" + fetchDateOccupancyAsQueryString();
            }
        }

		// handle page refresh for voucher redemption
		var voucherRedemption = dataCache.session.getData('voucherRedemption');
		if(voucherRedemption && voucherRedemption == 'true'){
			nextPageHref = nextPageHref + "&voucherRedemption=true"
		}

        window.location.href = nextPageHref;

        $(".cm-bas-con").removeClass("active");
    }
    $('#book-a-stay').trigger('taj:fetchRates', [ bookingOptions ]);
};

function onClickOnCheckAvailabilty() {
    var validationResult = validateSearchInput();

    if (isIHCLCBHomePage() && !(checkboxCheckedStatus() && isEntitySelected())) {
        return;
    }

    var bookingOptions = updateBookingOptionsInStorage(_globalPromoCode);

    var getHotelId;
    getHotelId = $("#hotelIdFromSearch").text();
    var dataId = dataCheck();
    checkPreviousSelection();
    var cartCheck = cartEmptyCheck();
    var check = false;
    if (getHotelId != dataId) {
        check = true;
    }
    var _self = this;
    if (modifyBookingState == 'modifyDate') {
        var modifiedCheckInDate = moment($('#input-box1').datepicker('getDate')).format("MMM Do YY");
        var modifiedCheckOutDate = moment($('#input-box2').datepicker('getDate')).format("MMM Do YY");
        if (modifiedCheckInDate != bookedCheckInDate || modifiedCheckInDate != bookedCheckOutDate) {
            var warnMsg = 'You have changed Check-In/Check-Out date. Are you sure you want to continue?';
            showWarningMessage(_self, warnMsg, validationResult);
        } else {
            $('.bas-close').trigger('click');
            // or we will redirect to confirmation page with previous
            // booking details
        }
    } else if (modifyBookingState == 'modifyRoomOccupancy') {
        var modifiedAdultsOccupancy = $('.bas-no-of-adults input').val();
        var modifiedChildrenOccupancy = $('.bas-no-of-child input').val();
        if (modifiedAdultsOccupancy != bookedAdultsOccupancy || modifiedChildrenOccupancy != bookedChildrenOccupancy) {
            var warnMsg = 'You have changed Adult/Child occupancy. Are you sure you want to continue?';
            showWarningMessage(_self, warnMsg, validationResult);
        } else {
            $('.bas-close').trigger('click');
            // or we will redirect to confirmation page with previous
            // booking details
        }
    } else if (modifyBookingState == 'modifyAddRoom') {
        var clearExistingCart = true;
        var warnMsg = 'You have added room. Are you sure you want to continue?';
        showWarningMessage(_self, warnMsg, validationResult);
    } else if (!cartCheck && (check || availabilityObj.isCheckParamsUpdated || availabilityObj.isDatesUpdated)) {
        var clearExistingCart = true;
        var warnMsg = 'Your existing cart values, if any, will be lost because you have updated your selection. Do you want to proceed?';
        showWarningMessage(_self, warnMsg, validationResult);
    } else if (isAmaSite && availabilityObj.isRoomTypeUpdated) {
        var warnMsg = 'You have updated your room type. Do you want to proceed?';
        showWarningMessage(_self, warnMsg, validationResult);
    } else {
        navigateToRoomsPage(validationResult);
    }
}

function showWarningMessage(_self, msg, validationResult) {
    var popupParams = {
        title : msg,
        callBack : navigateToRoomsPage.bind(_self, validationResult, true),
        // callBackSecondary: secondaryFn.bind( _self ),
        needsCta : true,
        isWarning : true
    }
    warningBox(popupParams);
}

function updateRoomNumberText() {
    var room = "Room";
    if ($(".bas-room-details").length > 1)
        room = "Rooms";
    else
        room = "Room";
    $(".room-border").text($(".bas-room-details").length + " " + room);
}

function dataCheck() {
    var cartHotelId;
    var bookingData = dataCache.session.getData("bookingOptions");
    if (bookingData && bookingData.selection && (bookingData.selection.length > 0)) {
        cartHotelId = bookingData.selection[0].hotelId;
    }
    return cartHotelId;
}

// _globalListOfPromoCode=['PROMO1','PROMOX'];
function validatePromocode(_globalPromoCode) {
    var promoCodeInput = $('.promo-code').val();
    promoCodeInput = promoCodeInput.toUpperCase();
    var promoCodeStatus = true;
    if (promoCodeStatus) {
        $('.promocode-status-text').text("Promo code selected: " + promoCodeInput);
        _globalPromoCode.value = promoCodeInput;
        _globalPromoCode.name = promoCodeInput;
    } else {
        $('.promocode-status-text').text("Promo code invalid.");
        _globalPromoCode.value = null;
        _globalPromoCode.name = null;
    }
}

function cartEmptyCheck() {
    var dataa = dataCache.session.getData("bookingOptions");
    if (dataa.selection && (dataa.selection.length == 0)) {
        return true;
    } else {
        return false;
    }
}

function incrementSelectionCount(e) {
    // Stop acting like a button
    e.preventDefault();
    amaBookingObject.isAmaCheckAvailabilitySelected = true;
    // Get the field name
    var $button = $(this);
    var quantity = parseInt($button.parent().parent().find("input").val());
    ++quantity;
    if (isAmaSite) {
        if ($(".ama-theme .bas-about-room-container").css("display") == "block") {
            if (quantity > 7) {
                quantity = 7;
            }
        } else {
            if ($button.hasClass('quantity-right-plus1')) {
                if (quantity > 15) {
                    quantity = 15;
                }
            } else {
                if (quantity > 8) {
                    quantity = 8;
                }
            }
        }
    } else {
        if (quantity > 7) {
            quantity = 7;
        }
    }
    $button.parent().parent().find("input").val(quantity);
    $button.parent().parent().find("input").text(quantity);
    var x = $button.parent().parent().parent().attr("class");
    // script for adult count starts
    updateTotalAdultsCountModule();
    // script for adult count ends
};

function decrementSelectionCount(e) {
    // Stop acting like a button
    amaBookingObject.isAmaCheckAvailabilitySelected = true;
    e.preventDefault();
    // Get the field name
    var $button = $($(this)[0])
    var quantity = parseInt($button.parent().parent().find("input").val());
    var x = $button.parent().parent().parent().attr("class");
    if (quantity > 0) {
        if (x === "bas-no-of-adults" && quantity == 1) {
            quantity = 2;
        }
        $button.parent().parent().find("input").val(quantity - 1);
    }
    // script for adult count starts
    updateTotalAdultsCountModule();
    // script for adult count ends
};

// module to update total adults count in booking widget
function updateTotalAdultsCountModule() {
    var totalAdultsCount = 0;
    $('.bas-no-of-adults, .bas-no-of-child').find('input').each(function() {
        totalAdultsCount += parseInt($(this).val());
    });
    var guestSuffix = (totalAdultsCount > 1) ? 's' : '';
    $(".bas-count-of-adults").text(totalAdultsCount + " Guest" + guestSuffix);
}

function checkPreviousSelection() {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    if (bookingOptions.isAvailabilityChecked) {
        bookingOptions.previousRooms
                .forEach(function(value, index) {
                    if (bookingOptions.roomOptions[index]
                            && ((value.adults != bookingOptions.roomOptions[index].adults) || (value.children != bookingOptions.roomOptions[index].children))) {
                        availabilityObj.isCheckParamsUpdated = true;
                    }
                });

        if ((bookingOptions.fromDate != bookingOptions.previousDates.fromDate)
                || (bookingOptions.toDate != bookingOptions.previousDates.toDate)) {
            availabilityObj.isDatesUpdated = true;
        }
        if (isAmaSite && bookingOptions.BungalowType && bookingOptions.previousDates
                && bookingOptions.previousDates.BungalowType
                && bookingOptions.previousDates.BungalowType != bookingOptions.BungalowType) {
            availabilityObj.isRoomTypeUpdated = true;
        }

    }
}

// module to add room
function addRoomModule() {

    var room_no = $(".bas-room-no").length + 1;
    if (room_no < 6) {
        $(".bas-add-room")
                .before(
                        '<li class="bas-room-no" id=room'
                                + room_no
                                + '><button class="btn-only-focus" aria-label = "bas room"><span class="bas-room bas-desktop">Room &nbsp</span><span class="bas-span-room-no">'
                                + room_no
                                + '</span></button><div class="bas-room-delete-close"><i class="icon-close"><button class="btn-only-focus" aria-label = "close icon"></button></i></div></li>');

        $($(".bas-room-details")[0]).clone().appendTo(".bas-room-details-container").addClass("bas-hide");

        var x = room_no;
        room_no--;
        $($(".bas-room-details")[room_no]).attr("id", "room" + x + "Details");
        $($(".bas-room-details")[room_no]).find("input").val("1");
        $($($(".bas-room-details")[room_no]).find("input")[1]).val("0");
        updateTotalAdultsCountModule();

        // update text for number of rooms
        updateRoomNumberText();
        var elRoomNumber = $(".bas-about-room-container").find('.bas-room-no');
        if (elRoomNumber.length >= 5) {
            $(".bas-add-room").addClass("bas-hide");
        } else {
            $(".bas-add-room").removeClass("bas-hide");
        }
        // script to make newly added room active

        makeRoomActiveModule($('.bas-room-no').last());
        $('.bas-room-delete-close .icon-close').removeClass('cm-hide');
    }
};

function isIHCLCBHomePage() {
    var currentPage = window.location.pathname;
    return currentPage.includes('/dashboard');
}

function deleteRoomInCartAndUpdateSelectionData(deleteRoomNumber) {
    var deleteRoomIndex = deleteRoomNumber - 1;
    $($('.fc-add-package-con')[deleteRoomIndex]).find('.selection-delete').trigger('click');
    var bookingOptionsInStorage = dataCache.session.getData("bookingOptions");
    var roomOptionsModified = bookingOptionsInStorage.roomOptions;
    roomOptionsModified.splice(deleteRoomIndex, 1);
    bookingOptionsInStorage.roomOptions = roomOptionsModified;
    --bookingOptionsInStorage.rooms;
    dataCache.session.setData("bookingOptions", bookingOptionsInStorage);
    updateCheckAvailability();
    $(".fc-add-package-con").eq(deleteRoomIndex).remove();
    updateRoomIndexForSelectedPackages();
    setCartPanelRoomsConWidth();
    updateSelectionInstruction();
}

function updateRoomIndexForSelectedPackages() {
    var bookingOptionsInStorage = dataCache.session.getData("bookingOptions");
    bookingOptionsInStorage.selection.forEach(function(value, index) {
        value.roomIndex = index;
    });
    dataCache.session.setData("bookingOptions", bookingOptionsInStorage);
}

// module to delete room
function deleteRoomModule(roomDeleteIndex) {
    $('.bas-room-no').eq(roomDeleteIndex).remove();
    $('.bas-room-details').eq(roomDeleteIndex).remove();
    var totalRoomsAfterDeletion = $('.bas-room-details').length;
    if (totalRoomsAfterDeletion < 2) {
        $('.bas-room-delete-close .icon-close').addClass('cm-hide');
    }
    var roomID = 1;
    $('.bas-room-no').each(function() {
        $(this).attr('id', 'room' + roomID);
        $(this).find('.bas-span-room-no').text(roomID);
        ++roomID;
    });
    roomID = 1;
    $('.bas-room-details').each(function() {
        $(this).attr('id', 'room' + roomID + "Details");
        ++roomID;
    });

    if ($('.bas-room-no').hasClass('bas-active') == false) {
        if ($('.bas-room-no').eq(roomDeleteIndex).length > 0) {
            makeRoomActiveModule($('.bas-room-no').eq(roomDeleteIndex));
        } else {
            makeRoomActiveModule($('.bas-room-no').eq(roomDeleteIndex - 1));
        }
    }
    deleteRoomInCartAndUpdateSelectionData(roomDeleteIndex + 1);

    // update for adults count starts
    updateTotalAdultsCountModule();
    // update for adults count ends
    // update text for number of rooms
    updateRoomNumberText();
    $(".bas-add-room").removeClass("bas-hide");
};

// module to make room active
function makeRoomActiveModule(elem) {
    $(".bas-room-no").each(function() {
        $(this).removeClass("bas-active");
    });

    var new_room_no = elem.attr("id") + "Details";

    $(".bas-room-details-container").find("ul").each(function() {
        $(this).addClass("bas-hide");
    });
    $($(".bas-room-details-container").find("ul")[0]).removeClass("bas-hide");
    $(".bas-room-details-container").find("ul#" + new_room_no).removeClass("bas-hide");

    elem.addClass("bas-active");
}

// module to update final date plan
function updateFinalDatePlanModule() {

    var currentDate = moment($("#input-box1").datepicker("getDate")).format("D MMM YY");
    var nextDate = moment($("#input-box2").datepicker("getDate")).format("D MMM YY");
    var finalDatePlan = currentDate + " - " + nextDate;

    $(".final-date-plan").text(finalDatePlan);
};

// module to update booking storage data
function updateBookingOptionsInStorage(_globalPromoCode) {
    var bookingOptions = dataCache.session.getData("bookingOptions") || getInitialBookAStaySessionObject();
    bookingOptions.promoCode = _globalPromoCode.value;
    bookingOptions.promoCodeName = _globalPromoCode.name;
    var currentDate;
    var nextDate;
    var totalRoomsOpted;
    var targetEntity;
    if (isAmaCheckAvailability) {
        var roomsSelector = $('.guests-dropdown-wrap .guest-room-header');
        currentDate = parseDate($("#input-box-from").val());
        nextDate = parseDate($("#input-box-to").val());
        totalRoomsOpted = roomsSelector.length;
        targetEntity = $('.check-avblty-wrap .select-dest-placeholder').text();
        bookingOptions = fetchRoomOptionsSelected(bookingOptions);
        if (isBungalowSelected()) {
            bookingOptions["BungalowType"] = "onlyBungalow";
        } else {
            bookingOptions["BungalowType"] = "IndividualRoom";
        }
    } else {
        currentDate = parseSelectedDate($("#input-box1").datepicker("getDate"));
        nextDate = parseSelectedDate($("#input-box2").datepicker("getDate"));
        if (isIHCLCBSite()) {
            totalRoomsOpted = $('.ihclcb-bas-room-no').length;
        } else {
            totalRoomsOpted = $('.bas-room-no').length;
        }
        targetEntity = $('.searchbar-input').val();
        bookingOptions = fetchRoomOptionsSelected(bookingOptions);
        if (isAmaSite) {
            bookingOptions["BungalowType"] = fetchRadioButtonSelectedAma();
            targetEntity = $('#booking-search .dropdown-input').text();
        }
    }
    if (isAmaSite && bookingOptions.BungalowType && bookingOptions.previousDates
            && bookingOptions.previousDates.BungalowType
            && bookingOptions.BungalowType != bookingOptions.previousDates.BungalowType) {
        bookingOptions.selection = [];
    }
    bookingOptions.fromDate = currentDate;
    bookingOptions.toDate = nextDate;
    bookingOptions.rooms = totalRoomsOpted;
    bookingOptions.targetEntity = targetEntity;
    bookingOptions.nights = moment(nextDate, "MMM Do YY").diff(moment(currentDate, "MMM Do YY"), 'days');
    updateHotelChainCodeAndHoteID(bookingOptions);
    dataCache.session.setData("bookingOptions", bookingOptions);

    // Updates the check availability component's data
    updateCheckAvailability();

    updateFinalDatePlanModule();

    // Update the floating cart values on updation of inputs from BAS widget
    var elFloatingCart = $('.book-ind-container');

    return bookingOptions;
};

function fetchRadioButtonSelectedAma() {
    if ($('.book-stay-popup-radio-btn #onlyBungalowBtn').is(':checked')) {
        return "onlyBungalow";
    }
    return "IndividualRoom";
}

function fetchRoomOptionsSelected(bookingOptions) {
    var selectedRoomOption = extractObjectValues(bookingOptions.roomOptions);
    bookingOptions.roomOptions = [];
    var userSelectionList = [];
    $(selectedRoomOption).each(function() {
        userSelectionList.push(this.userSelection);
    });
    if (isAmaSite && isAmaCheckAvailability) {
        return updateRoomOptionsBookAStayAma(bookingOptions, userSelectionList);
    } else {
        return updateRoomOptionsBookAStay(bookingOptions, userSelectionList);
    }
}

function updateRoomOptionsBookAStayAma(bookingOptions, userSelectionList) {
    $('.guests-dropdown-wrap .guest-room-header').each(function(index) {
        var $this = $(this);
        var adultsCount = $this.find('.adult-wrap .counter').text();
        var childrenCount = $this.find('.children-wrap .counter').text();
        var selectedObject = {
            'adults' : adultsCount,
            'children' : childrenCount,
            'initialRoomIndex' : index,
            'userSelection' : userSelectionList[index]
        };
        bookingOptions.roomOptions.push(selectedObject);
        if (bookingOptions.selection.length > 0 && bookingOptions.selection[index]) {
            bookingOptions.selection[index].adults = adultsCount;
            bookingOptions.selection[index].children = childrenCount;
        }
    });
    return bookingOptions;
}

function updateRoomOptionsBookAStay(bookingOptions, userSelectionList) {
    var $roomDetailsCont = $('.bas-room-details');
    if (isIHCLCBHomePage()) {
        $roomDetailsCont = $('.ihclcb-bas-room-details');
    }
    $roomDetailsCont.each(function(index) {
        var selectedHeadCount = $(this).find('.bas-quantity');
        var adultsCount = selectedHeadCount[0].value;
        var childrenCount = selectedHeadCount[1].value;
        var selectedObject = {
            'adults' : adultsCount,
            'children' : childrenCount,
            'initialRoomIndex' : index,
            'userSelection' : userSelectionList[index]
        };
        bookingOptions.roomOptions.push(selectedObject);
        if (bookingOptions.selection.length > 0 && bookingOptions.selection[index]) {
            bookingOptions.selection[index].adults = adultsCount;
            bookingOptions.selection[index].children = childrenCount;
        }
    });
    return bookingOptions;
}

function isBungalowSelected() {
    return $('.check-avblty-container .radio-container #onlyBungalow').is(':checked');
}

// ie fall back for object.values
function extractObjectValues(objectName) {
    return (Object.keys(objectName).map(function(objKey) {
        return objectName[objKey]
    }))
}
// changing date to "MMM Do YY" format
function parseSelectedDate(selectedDateValue) {
    var formatedDate = moment(selectedDateValue, "D MMM YY").format("MMM Do YY");
    return formatedDate;
}

function getInitialRoomOption() {
    return [ {
        adults : 1,
        children : 0,
        initialRoomIndex : 0
    } ];
}

// return session Object for book a stay
function getInitialBookAStaySessionObject() {
    var toDateForBooking = initialBookingSetup();
    return {
        fromDate : moment(new Date()).add(14, 'days').format("MMM Do YY"),
        toDate : toDateForBooking,
        rooms : 1,
        nights : 1,
        roomOptions : getInitialRoomOption(),
        selection : [],
        promoCode : null,
        hotelChainCode : null,
        hotelId : null,
        isAvailabilityChecked : false
    };
}

// set book a stay session data
// function setBookAStaySessionObject() {
// var bookingOptionsSelected = dataCache.session.getData("bookingOptions") || getInitialBookAStaySessionObject();
// dataCache.session.setData("bookingOptions", bookingOptionsSelected);
// }

function removePopulatedRoomsBookAStay(_this) {
    _this.each(function(i, ele) {
        if (i > 0) {
            ele.remove();
        }
    });
}

function populateAmaRoomTypeRadioButton(bookingOptionsSelected) {
    var roomType = bookingOptionsSelected.roomType || bookingOptionsSelected.BungalowType;
    if (roomType && roomType == "onlyBungalow") {
        $('#book-a-stay .radio-button #onlyBungalowBtn').click();
    } else {
        $('#book-a-stay .radio-button #onlyRoomBtn').click();
    }
}

// auto updating booking widget with respect to storage data
function autoPopulateBookAStayWidget() {
    var bookingOptionsSelected;
    if (isAmaSite && amaBookingObject.isAmaCheckAvailabilitySelected) {
        bookingOptionsSelected = Object.assign({}, amaBookingObject);
        populateAmaRoomTypeRadioButton(bookingOptionsSelected);
    } else {
        bookingOptionsSelected = dataCache.session.getData("bookingOptions") || getInitialBookAStaySessionObject();
        populateAmaRoomTypeRadioButton(bookingOptionsSelected);
        updateHotelChainCodeAndHoteID(bookingOptionsSelected);
        dataCache.session.setData("bookingOptions", bookingOptionsSelected);
    }

    // var fromDateSelecetd = moment(bookingOptionsSelected.fromDate, "MMM Do YY").format("D MMM YY");
    // var toDateSelecetd = moment(bookingOptionsSelected.toDate, "MMM Do YY").format("D MMM YY");

    // $('#input-box1').val(fromDateSelecetd);
    // $('#input-box2').val(toDateSelecetd);

    var roomOptionsSelected = bookingOptionsSelected.roomOptions;

    removePopulatedRoomsBookAStay($(".bas-room-no"));
    removePopulatedRoomsBookAStay($(".bas-room-details"));

    roomOptionsSelected.forEach(function(val, index) {
        if (index > 0) {
            addRoomModule();
        }

        var currentIndexRoom = $($('.bas-room-details')[index]);
        $(currentIndexRoom[0]).find('.bas-no-of-adults input').val(val.adults);
        $(currentIndexRoom[0]).find('.bas-child-no-container input').val(val.children);
    });

    if (bookingOptionsSelected.promoCode) {
        $('.promo-code').val(bookingOptionsSelected.promoCode);
        $('.promocode-status-text').text("Promo code selected: " + bookingOptionsSelected.promoCode);
    }

    if ($('.cm-page-container').hasClass('specific-hotels-page')
            || $('.cm-page-container').hasClass('destination-layout')) {
        var targetEntitySelected = $('.banner-titles .cm-header-label').text();
        $('.specific-hotels-page, .destination-layout').find('.cm-bas-content-con .searchbar-input').val(
                targetEntitySelected);
        var rootPath = fetchRootPath();
        if (rootPath) {
            rootUrl = rootPath;
            if ($.find("[data-hotel-id]")[0]) {
                $('#hotelIdFromSearch').text($($.find("[data-hotel-id]")[0]).data().hotelId);
            }
            enableBestAvailableButton(rootUrl);
        }
        $('.cm-bas-content-con .searchbar-input').attr("data-selected-search-value", targetEntitySelected);
    }

    // Updates the check availability component's data
    updateCheckAvailability();

    makeRoomActiveModule($('.bas-room-no').first());
    promptToSelectDate();

    updateTotalAdultsCountModule();
}

// destroy datepicker
function destroyDatepicker() {
    $('.bas-datepicker-container .input-daterange').datepicker('destroy');
    $(".cm-bas-con").removeClass("active");
    $('.cm-page-container').removeClass("prevent-page-scroll");
};

// inititalize calendar
function initiateCalender() {
    availabilityObj = {};
    // calender input date
    var fromDate = dataCache.session.data.bookingOptions.fromDate;
    var toDate = dataCache.session.data.bookingOptions.toDate;
    if (isAmaSite && amaBookingObject.isAmaCheckAvailabilitySelected) {
        fromDate = amaBookingObject.fromDate;
        toDate = amaBookingObject.toDate;
    }
    var storageFromDate = moment(fromDate, "MMM Do YY").toDate() || null;
    var storageToDate = moment(toDate, "MMM Do YY").toDate() || null;

    // var tommorow = moment(new Date()).add(1, 'days')['_d'];
    var d1 = storageFromDate || moment(new Date()).add(14, 'days')['_d'];
    //var d2 = storageToDate || moment(new Date()).add(15, 'days')['_d'];

	var d2 = storageToDate || toDateForBooking();

    // const
    // monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

    var output1 = moment(d1).format("D MMM YY");
    var output2 = moment(d2).format("D MMM YY");

    initializeDatepickerForBookAStay($('.bas-datepicker-container .input-daterange'), $('.bas-calander-container'));

    $("#input-box1").datepicker("setDate", new Date(moment(output1, "D MMM YY").toDate()));
    $("#input-box2").datepicker("setDate", new Date(moment(output2, "D MMM YY").toDate()));

    setTimeout(function() {
        $("#input-box1").blur();
        $("#input-box2").blur();
    }, 105);
};

function initializeDatepickerForBookAStay(ele, container) {
    ele.datepicker({
        container : container,
        format : 'd M yy',
        startDate : new Date(),
        templates : {
            leftArrow : '<i class="icon-prev-arrow inline-block"></i>',
            rightArrow : '<i class="icon-prev-arrow inline-block cm-rotate-icon-180 v-middle"></i>'
        }
    });
}

function hideCalenderInBookAStayPopup() {
    $('.bas-calander-container').removeClass('active');
    $('.bas-calander-container>.datepicker').css('display', 'none');
    $('.bas-hotel-details-container').removeClass('active');
}

function validateSearchInput() {
    // validate using data value in searcbar input
    var searchInputValue = "";
    if ($('#book-a-stay').data('theme') == 'ama-theme') {
        var dropdownText = isAmaCheckAvailability ? $('.check-avblty-wrap .select-dest-placeholder').text() : $(
                '.cm-bas-content-con .dropdown-input').text();
        if (dropdownText != "Select Destinations/Bungalows")
            searchInputValue = dropdownText;
    } else {
        searchInputValue = $('.cm-bas-content-con .searchbar-input').attr("data-selected-search-value");
    }
    if (!searchInputValue) {
        $('.cm-bas-content-con .searchbar-input').effect('shake', {
            "distance" : "5"
        });
        disableBestAvailableButton();
        return false;
    }
    return true;
}

function disableBestAvailableButton() {
    changeBookStayButtonStatus();
    $('.bas-final-detail-wrap').css("display", "none");
    $('#global-re-direct').removeAttr('hrefValue');
}

function changeBookStayButtonStatus() {
    $('.best-avail-rate ').css({
        "opacity" : "0.5"
    });
    $(".best-avail-rate").toggleClass('best-avail-rate-disable', true);
    $(".best-avail-rate").toggleClass('best-avail-rate-enable', false);
}

function getHotelRoomsRootPath() {
    var hiddenDom = $.find("[data-hotel-roomspath]")[0];
    if (hiddenDom != undefined) {
        var hotelRoomspath = $(hiddenDom).data().hotelRoomspath;
        if (hotelRoomspath != undefined && hotelRoomspath.startsWith('https://www.tajhotels.com')) {
            hotelRoomspath = hotelRoomspath.substring(25)
        } else if (hotelRoomspath != undefined && hotelRoomspath.startsWith('https://www.vivantahotels.com')) {
            hotelRoomspath = hotelRoomspath.substring(29)
        } else if (hotelRoomspath != undefined && hotelRoomspath.startsWith('https://www.seleqtionshotels.com')) {
            hotelRoomspath = hotelRoomspath.substring(32)
        }

        return hotelRoomspath;
    }
}

function getDestinationRootPath() {
    var hiddenDom = $.find("[data-destination-rootpath]")[0];
    if (hiddenDom != undefined)
        return $(hiddenDom).data().destinationRootpath
}

function fetchRootPath() {
    var destinationRootPath = getDestinationRootPath();
    var hotelRoomsPath = getHotelRoomsRootPath();
    if (destinationRootPath != undefined)
        return destinationRootPath;

    return hotelRoomsPath;
}

function enableBestAvailableButton(redirectUrl, noUrlStatus) {
    if (!noUrlStatus) {
        $('#global-re-direct, #checkAvailability').attr('hrefValue', redirectUrl);
    }
    if (isAmaSite) {
        $('#checkAvailability').addClass('enabled');
    }
    if (isIHCLCBHomePage() && !(checkboxCheckedStatus() && isEntitySelected())) {
        return;
    }

    $('.best-avail-rate ').css({
        "opacity" : "1",
    });

    $(".best-avail-rate").toggleClass('best-avail-rate-disable', false);
    $(".best-avail-rate").toggleClass('best-avail-rate-enable', true);
    $('.bas-final-detail-wrap').css("display", "inline-block");
    if (retainHolidaysFlow(redirectUrl)) {
        redirectUrl = redirectUrl + "?taj-holiday-redemption";
    }

}

function checkboxCheckedStatus() {
    return $('.ihclcb-checkbox-cont .mr-filter-checkbox').prop("checked");
}

function fetchDateOccupancyAsQueryString() {
    var queryString = '';
    try {
        var bookingOptions = dataCache.session.getData('bookingOptions');
        var from = moment(bookingOptions.fromDate, "MMM Do YY").format('DD/MM/YYYY');
        var to = moment(bookingOptions.toDate, "MMM Do YY").format('DD/MM/YYYY');
        var roomOptionArr = bookingOptions.roomOptions;
        var rooms = bookingOptions.roomOptions.length;
        var adults = '';
        var children = '';
        if (roomOptionArr) {
            roomOptionArr.forEach(function(roomObj, index) {
                adults += roomObj.adults + ",";
                children += roomObj.children + ",";
            });
            adults = adults.substring(0, adults.length - 1);
            children = children.substring(0, children.length - 1);
        }
        queryString = "from=" + from + "&to=" + to + "&rooms=" + rooms + "&adults=" + adults + "&children=" + children;
    } catch (err) {
        console.error("Error Occured while forming query String using session data ", err);
    }
    return queryString;
}

function retainHolidaysFlow(redirectUrl) {
    var retainHolidays = false;
    var substringUrl = redirectUrl;
    if (redirectUrl.endsWith(".html")) {
        substringUrl = redirectUrl.substring(0, redirectUrl.length - 5);
    }

    var currentUrl = window.location.href;
    if (currentUrl.indexOf("?taj-holiday-redemption") != -1) {
        if (currentUrl.indexOf(substringUrl) != -1) {
            retainHolidays = true;
        }
    }
    return retainHolidays;
}

function updateHotelChainCodeAndHoteID(bookingOptions) {
    var hotelIdAndChainCodeData = $($.find("[data-hotel-id]")[0]).data();
    if (hotelIdAndChainCodeData) {
        bookingOptions.hotelChainCode = hotelIdAndChainCodeData.hotelChainCode || CHAIN_ID;
        bookingOptions.hotelId = hotelIdAndChainCodeData.hotelId;
    }
}

function numberOfNightsCheck() {
    var currentDate = parseSelectedDate($("#input-box1").datepicker("getDate"));
    var nextDate = parseSelectedDate($("#input-box2").datepicker("getDate"));
    var numberOFNights = moment(nextDate, "MMM Do YY").diff(moment(currentDate, "MMM Do YY"), 'days');
    if (numberOFNights > 10 && $('.best-avail-rate ').hasClass('best-avail-rate-enable'))
        return false;
    else
        return true;

}

function checkHolidayScope() {
    if (!dataCache.session.getData("checkHolidayAvailability")) {
        var bookingOptionsHoliday = dataCache.session.getData("bookingOptions");

        var fromDate = bookingOptionsHoliday.fromDate;
        if (!fromDate || fromDate == '') {
            fromDate = moment(new Date()).add(3, 'days').format("MMM Do YY");
            bookingOptionsHoliday.fromDate = fromDate;
        }

        var toDate = bookingOptionsHoliday.toDate;
        if (!toDate || toDate == '') {
            toDate = moment(new Date()).add(5, 'days').format("MMM Do YY");
            bookingOptionsHoliday.toDate = toDate;
        }

        bookingOptionsHoliday.nights = moment(toDate, "MMM Do YY").diff(moment(fromDate, "MMM Do YY"), 'days');

        var rooms = bookingOptionsHoliday.rooms;
        if (!rooms) {
            rooms = 1;
            bookingOptionsHoliday.rooms = rooms;
        }

        if (!bookingOptionsHoliday.roomOptions) {
            bookingOptionsHoliday.roomOptions = [ {
                adults : 2,
                children : 0,
                initialRoomIndex : 0
            } ];
        }
        dataCache.session.setData("bookingOptions", bookingOptionsHoliday);
        updateCheckAvailability();
    }
}

function dateOccupancyInfoSticky() {
    if ($('.search-container.check-availability-main-wrap').length > 0) {
        var stickyOffset = $('.search-container.check-availability-main-wrap ').offset().top;

        $(window).scroll(function() {
            var sticky = $('.search-container.check-availability-main-wrap '), scroll = $(window).scrollTop();

            if (scroll >= stickyOffset)
                sticky.addClass('mr-stickyScroll');
            else
                sticky.removeClass('mr-stickyScroll');
        });

    }
}

// [TIC-FLOW]
function isTicBased() {
    // checking if any rate code is there in dom which is of TIC Room redemption
    // flow
    var userDetails = getUserData();
    if (userDetails && userDetails.card && userDetails.card.tier && $(".tic-room-redemption-rates").length) {
        return true;
    }

    return false;
}

function resetPromoCodeTab() {
    $('.promo-code').val("");
    $('.apply-promo-code-btn').hide();
    $('.promocode-status-text').text("");
    _globalPromoCode = {
        name : null,
        value : null
    };
}

// [IHCL-FLOW]
var entityDetailsObj = new Object();
function fetchIHCLCBEntityDetails() {
    var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
    if (ihclCbBookingObject && ihclCbBookingObject.isIHCLCBFlow) {
        var entityDetailsData = ihclCbBookingObject.entityDetails;
        if (!entityDetailsData) {
            return;
        }
        for (var entity = 0; entity < entityDetailsData.length; entity++) {
            var partyObj = {};
            var cityObj = {};
            var entityCity = entityDetailsData[entity].city;
            var entityType = entityDetailsData[entity].AccountType_c;
            var entityExisting = entityDetailsObj[entityType];
            if (entityExisting) {
                var exisitingEle = null; // = entityDetailsObj[entityType];
                for ( var cty in entityDetailsObj[entityType]) {
                    if (Object.keys(entityDetailsObj[entityType][cty])[0] == entityCity) {
                        exisitingEle = JSON.parse(JSON.stringify(entityDetailsObj[entityType][cty][entityCity]));
                        break;
                    }
                }
            }
            partyObj['partyName'] = {
                'name' : entityDetailsData[entity].partyName,
                'gstinNo' : entityDetailsData[entity].gSTNTaxID_c,
                'iataNo' : entityDetailsData[entity].iATANumber,
                'partyNo' : entityDetailsData[entity].partyNumber
            }
            // change the names for better reference
            var newArr = [];
            if (exisitingEle) {
                newArr = exisitingEle;
            }
            newArr.push(partyObj);
            cityObj[entityCity] = newArr;
            var existingEntityDetailsObj = entityDetailsObj[entityType] ? JSON.parse(JSON
                    .stringify(entityDetailsObj[entityType])) : null;
            var newEntityDetailsObj = [];
            if (existingEntityDetailsObj) {
                newEntityDetailsObj = existingEntityDetailsObj;
                for ( var cty in existingEntityDetailsObj) {
                    if (Object.keys(existingEntityDetailsObj[cty])[0] == entityCity) {
                        newEntityDetailsObj.splice(cty, 1);
                    }
                }
            }
            newEntityDetailsObj.push(cityObj);
            entityDetailsObj[entityType] = newEntityDetailsObj;
        }
        // console.log(entityDetailsObj);
        populateEntityDropdown(entityDetailsObj);
    }
}

function populateEntityDropdown(entityDetailsObj) {
    var entityType = '';
    Object.keys(entityDetailsObj).forEach(function(entType) {
        entityType += createEntityDropdownList(entType);
        createEntityCityList(entType);
        // createPartyAndGSTNNoDropdown(entType);
    });
    // $('.entity-results#entityDetailsCity').html(entityCity);
    $('.entity-results#entityTypeDetails').html(entityType);
}

function createEntityDropdownList(value) {
    if (value) {
        return '<a class="individual-entity-result">' + value + '</a>';
    }
    return "";
}

function createEntityCityList(entityType, actDrpwn) {
    var entityCity = '';
    entityDetailsObj[entityType].forEach(function(cty) {
        // createEntityCityList(city);
        var extractedCity = Object.keys(cty)[0];
        entityCity += createEntityDropdownList(extractedCity);
        createPartyAndGSTNNoDropdown(cty, actDrpwn);
    });
    if (actDrpwn) {
        $('.active-dropdwn .entity-results#entityDetailsCity').html(entityCity)
    } else {
        $('.entity-results#entityDetailsCity').html(entityCity)
    }
}

function clearEntityDropdown(drpdwnSel) {
    var removableDrpdwn = drpdwnSel.closest('[data-drpdwn-target]').attr('data-drpdwn-target');
    var $caller = $('[data-drpdwn-caller = ' + removableDrpdwn + ']');
    $caller.text($caller.attr('data-entity-category').replace(/([A-Z])/g, ' $1').capitalize());
    $caller.attr('data-entity-selected', '');
    drpdwnSel.html('');
}

function createPartyAndGSTNNoDropdown(city, actDrpwn) {
    var entityPartyName = '';
    var entityGSTNNo = '';
    var entityIATANo = '';
    Object.values(city)[0].forEach(function(obj) {
        entityPartyName += createEntityDropdownList(obj.partyName.name);
        entityGSTNNo += createEntityDropdownList(obj.partyName.gstinNo);
        entityIATANo += createEntityDropdownList(obj.partyName.iataNo);
    });
    if (!actDrpwn) {
        $('.entity-results#entityDetailsPartyName').append(entityPartyName);
        $('.entity-results#entityDetailsGST').append(entityGSTNNo);
        $('.entity-results#entityDetailsIATA').append(entityIATANo);
    } else {
        $('.active-dropdwn .entity-results#entityDetailsPartyName').append(entityPartyName);
        $('.active-dropdwn .entity-results#entityDetailsGST').append(entityGSTNNo);
        $('.active-dropdwn .entity-results#entityDetailsIATA').append(entityIATANo);
    }
}

function isEntitySelected() {
    var userDetailsIHCLCB = dataCache.local.getData("userDetails");
    if (userDetailsIHCLCB
            && ((userDetailsIHCLCB.selectedEntity && userDetailsIHCLCB.selectedEntity.partyName) || (userDetailsIHCLCB.selectedEntityAgent && userDetailsIHCLCB.selectedEntityAgent.partyName))) {
        return true;
    }
    return false;
}

function addEntityDropDownEventsForIHCLCB() {
    try {
        var $entityTypeDropdown = $('#entityTypeDropdownMenu, #entityTypeDropdownMenu1');
        var $entityNameDropdown = $('#entityNameDropdownMenu, #entityNameDropdownMenu1');
        var $entityCityDropdown = $('#entityCityDropdownMenu, #entityCityDropdownMenu1');
        var $iataDropdown = $('#iataDropdownMenu');
        var $gstinDropdown = $('#gstinDropdownMenu, #gstinDropdownMenu1');

        // open popup
        $('[data-drpdwn-caller]').click(
                function(e) {
                    e.stopPropagation();
                    $(this).closest('.ihclcb-book-a-stay-dropdwn-cont').addClass('active-dropdwn').siblings(
                            '.ihclcb-book-a-stay-dropdwn-cont').removeClass('active-dropdwn');
                    var clickedDropdwn = $(this).attr('data-drpdwn-caller');
                    var $drpdwnCont = $('[data-drpdwn-target = ' + clickedDropdwn + ']');
                    $drpdwnCont.closest('.entity-dropdown-cnt-cont').addClass('active-dropdwn').siblings(
                            '.entity-dropdown-cnt-cont').removeClass('active-dropdwn');
                    if ($($drpdwnCont).hasClass('show-dropdown')) {
                        $drpdwnCont.removeClass('show-dropdown');
                    } else {
                        $('[data-drpdwn-target]').removeClass('show-dropdown');
                        $drpdwnCont.addClass('show-dropdown');
                    }
                });

        // disable the entity dropdown for the existing book a stay
        if (!isIHCLCBHomePage()) {
            $('[data-drpdwn-caller].ihclcb-mandetory-field').addClass('entity-dropdown-disabled');
            var userDetailsIHCLCB = dataCache.local.getData('userDetails');
            var selectedEntity = userDetailsIHCLCB.selectedEntity || userDetailsIHCLCB.selectedEntityAgent;
            if (userDetailsIHCLCB && selectedEntity) {
                // var selectedEntity = userDetailsIHCLCB.selectedEntity;
                // if (userDetailsIHCLCB && selectedEntity) {
                $entityTypeDropdown.text(selectedEntity.entityType);
                $entityCityDropdown.text(selectedEntity.city);
                $entityNameDropdown.text(selectedEntity.partyName);
                $gstinDropdown.text(selectedEntity.gSTNTaxID_c);
                // $iataDropdown.text(selectedEntity.iataNumber);
                // }
            }
            return;
        }

        var currentEntityDetails = [];
        var ihclSessionData = dataCache.session.getData("ihclCbBookingObject");
        var relatedPartyNo;
        var extractedObj;
        // get the selected dropdown value
        $('.entity-dropdown-cnt-cont').on(
                'click',
                '.entity-dropdown .individual-entity-result',
                function() {
                    $(this).closest('.entity-dropdown-cnt-cont').addClass('active-dropdwn').siblings(
                            '.entity-dropdown-cnt-cont').removeClass('active-dropdwn')
                    var openedDrpdwn = $(this).closest('.entity-dropdown-container').removeClass('show-dropdown').attr(
                            'data-drpdwn-target');
                    var $dropdwnCalledBy = $('[data-drpdwn-caller = ' + openedDrpdwn + ']');
                    var selectedValue = $(this).text();
                    $dropdwnCalledBy.text(selectedValue);
                    $dropdwnCalledBy.attr('data-entity-selected', selectedValue);

                    var $entityCity = $('.active-dropdwn #entityDetailsCity');
                    var $entityPartyName = $('.active-dropdwn #entityDetailsPartyName');
                    var $entityGSTNNO = $('.active-dropdwn #entityDetailsGST');
                    var $entityIATA = $('.active-dropdwn #entityDetailsIATA');
                    if ($dropdwnCalledBy.attr('data-entity-category') === "entityType") {
                        $entityCityDropdown.closest('.active-dropdwn').find('[data-entity-category="entityCity"]')
                                .removeClass('entity-dropdown-disabled');
                        $entityNameDropdown.closest('.active-dropdwn .ihclcb-book-a-stay-inner-wrp').nextAll().find(
                                '.ihclcb-selected-data').addClass('entity-dropdown-disabled');

                        clearEntityDropdown($entityCity);
                        clearEntityDropdown($entityPartyName);
                        clearEntityDropdown($entityGSTNNO);
                        // clearEntityDropdown($entityIATA);
                        createEntityCityList(selectedValue, true);
                    } else if ($dropdwnCalledBy.attr('data-entity-category') === "entityCity") {
                        $entityNameDropdown.closest('.active-dropdwn').find('[data-entity-category="entityName"]')
                                .removeClass('entity-dropdown-disabled');
                        $entityNameDropdown.closest('.active-dropdwn .ihclcb-book-a-stay-inner-wrp').nextAll().find(
                                '.ihclcb-selected-data').addClass('entity-dropdown-disabled');
                        clearEntityDropdown($entityPartyName);
                        clearEntityDropdown($entityGSTNNO);
                        // clearEntityDropdown($entityIATA);
                        var selectedEntityType = $entityTypeDropdown.closest('.active-dropdwn').find(
                                '[data-entity-category="entityType"]').attr('data-entity-selected');
                        var selectedEntityCity = $entityCityDropdown.closest('.active-dropdwn').find(
                                '[data-entity-category="entityCity"]').attr('data-entity-selected');
                        entityDetailsObj[selectedEntityType].forEach(function(cty) {
                            var extractedCity = Object.keys(cty)[0];
                            if (extractedCity == selectedEntityCity) {
                                extractedObj = Object.assign({}, cty);
                                createPartyAndGSTNNoDropdown(cty, true);
                            }
                        });
                        // createPartyAndGSTNNoDropdown(selectedValue);

                    } else if ($dropdwnCalledBy.attr('data-entity-category') === "entityName") {
                        clearEntityDropdown($entityGSTNNO);
                        // clearEntityDropdown($entityIATA);
                        var name;
                        if (extractedObj) {
                            var data = extractedObj[$entityCityDropdown.closest('.active-dropdwn').find(
                                    '[data-entity-category="entityCity"]').text()];
                        }
                        var clickedEntityNameIndex = $(this).index();
                        for (name = 0; name < data.length; name++) {
                            if (data[name].partyName.name == selectedValue && name == clickedEntityNameIndex) {
                                var gstDropdownList = createEntityDropdownList(data[name].partyName.gstinNo);
                                // var iataDropdownList = createEntityDropdownList(data[name].partyName.iataNo);
                                relatedPartyNo = data[name].partyName.partyNo;
                                if (gstDropdownList) {
                                    $('.entity-results#entityDetailsGST').html(gstDropdownList);
                                    $gstinDropdown.closest('.active-dropdwn').find('[data-entity-category="gstinNo"]')
                                            .removeClass('entity-dropdown-disabled');
                                } else {
                                    $gstinDropdown.addClass('entity-dropdown-disabled');
                                }
                                // if (iataDropdownList) {
                                // $('.entity-results#entityDetailsIATA').html(iataDropdownList);
                                // $iataDropdown.removeClass('entity-dropdown-disabled');
                                // } else {
                                // $iataDropdown.addClass('entity-dropdown-disabled');
                                // }
                                break;
                            }
                        }
                    }

                    var attributeID = $dropdwnCalledBy.attr("id");
                    var selectedEntityType, selectedEntityCity, selectedPartyName, selectedGSTNo;
                    var typeOfEntitySelection;
                    if (attributeID.includes("1")) {
                        typeOfEntitySelection = "agent";
                        selectedEntityType = $('#entityTypeDropdownMenu1').attr('data-entity-selected') || '';
                        selectedEntityCity = $('#entityCityDropdownMenu1').attr('data-entity-selected') || '';
                        selectedPartyName = $('#entityNameDropdownMenu1').attr('data-entity-selected') || '';
                        selectedGSTNo = $('#gstinDropdownMenu1').attr('data-entity-selected');

                        var selectedEntityAgent = new Object();
                        selectedEntityAgent.entityType = selectedEntityType;
                        selectedEntityAgent.city = selectedEntityCity;
                        selectedEntityAgent.partyName = selectedPartyName;
                        selectedEntityAgent.gSTNTaxID_c = selectedGSTNo;
                        selectedEntityAgent.partyNumber = relatedPartyNo;
                        selectedEntityAgent.iataNumber = selectedIATA;

                        addSelectedValueToIHCLCBSession(selectedEntityAgent);
                    } else {
                        typeOfEntitySelection = "corporate";
                        selectedEntityType = $('#entityTypeDropdownMenu').attr('data-entity-selected') || '';
                        selectedEntityCity = $('#entityCityDropdownMenu').attr('data-entity-selected') || '';
                        selectedPartyName = $('#entityNameDropdownMenu').attr('data-entity-selected') || '';
                        selectedGSTNo = $('#gstinDropdownMenu').attr('data-entity-selected');

                        var selectedEntity = new Object();
                        selectedEntity.entityType = selectedEntityType;
                        selectedEntity.city = selectedEntityCity;
                        selectedEntity.partyName = selectedPartyName;
                        selectedEntity.gSTNTaxID_c = selectedGSTNo;
                        selectedEntity.partyNumber = relatedPartyNo;
                        selectedEntity.iataNumber = selectedIATA;

                        addSelectedValueToIHCLCBSession(selectedEntity);
                    }

                    var selectedIATA;
                    // var selectedIATA = $iataDropdown.attr('data-entity-selected') || $iataDropdown.text();
                    // if ($entityNameDropdown.attr('data-entity-selected') ||
                    // $gstinDropdown.attr('data-entity-selected')
                    // || $iataDropdown.attr('data-entity-selected')) {
                    if ($entityTypeDropdown.closest('.active-dropdwn').find('[data-entity-category="entityType"]')
                            .attr('data-entity-selected')
                            && $entityCityDropdown.closest('.active-dropdwn').find(
                                    '[data-entity-category="entityCity"]').attr('data-entity-selected')
                            && $entityNameDropdown.closest('.active-dropdwn').find(
                                    '[data-entity-category="entityName"]').attr('data-entity-selected')) {
                        addSelectedValueToIHCLCBSession(typeOfEntitySelection, selectedEntityType, selectedEntityCity,
                                selectedPartyName, selectedGSTNo, selectedIATA);
                        if (isEntityDropdownSelected() && checkboxCheckedStatus()) {
                            enableBestAvailableButton('', true);
                        }
                    } else {
                        changeBookStayButtonStatus();
                    }
                });

        // close the entity dropdown while clicking outside;
        $(window).click(function(e) {
            var container = $(".entity-dropdown-container");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('.show-dropdown').removeClass('show-dropdown');
            }
        });

        function addSelectedValueToIHCLCBSession(entityObject) {

            var userDetails = dataCache.local.getData("userDetails");
            if (userDetails) {
                if (entityObject.entityType === "Travel Agent") {
                    userDetails.selectedEntityAgent = entityObject;
                } else if (entityObject.entityType === "Corporate") {
                    userDetails.selectedEntity = entityObject;
                }
            }
            dataCache.local.setData("userDetails", userDetails);
        }
    } catch (error) {
        console.error("Error in /apps/tajhotels/components/content/book-a-stay/clientlibs/js/book-a-stay.js ",
                error.stack);
    }
}

function isEntityDropdownSelected() {
    var entityDrpDwn = $('.ihclcb-corporate-book-stay .active-dropdwn .ihclcb-mandetory-field');
    var i;
    for (i = 0; i < entityDrpDwn.length; i++) {
        if (!$(entityDrpDwn[i]).attr('data-entity-selected')) {
            return false;
        }
    }
    return true;
}

// [IHCL_CB ENDS]


// check for same day checkin checkout
function checkSameDayCheckout(currVal){
	var sameDayCheckout = dataCache.session.getData("sameDayCheckout");
    if(sameDayCheckout && sameDayCheckout === "true"){
       	return moment(currVal, "D MMM YY");
    } else {
        return moment(currVal, "D MMM YY").add(1, 'days');
    }
}


function initialBookingSetup(){
	var sameDayCheckout = dataCache.session.getData("sameDayCheckout");
    if(sameDayCheckout && sameDayCheckout === "true"){
       	return moment(new Date()).add(14, 'days').format("MMM Do YY");
    } else {
		return moment(new Date()).add(15, 'days').format("MMM Do YY");
    }
}
