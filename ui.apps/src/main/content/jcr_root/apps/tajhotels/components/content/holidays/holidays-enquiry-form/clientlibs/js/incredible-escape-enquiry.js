function processData() {

    var jivaDateWidth = $('.jiva-spa-date').outerWidth();
    var today = new Date();

    $('#enquiry-from-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            // Added by Dinesh for Event quote datepicker
            var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' + minDate.getFullYear());
            $('.enquiry-from-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
            $('#enquiry-to-date').datepicker('setStartDate', minDate);
        }
    });

    $('#enquiry-from-date').datepicker('setDate', new Date());

    $('#enquiry-to-date').datepicker({
        startDate : today
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            // Added by Dinesh for Event quote datepicker
            var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' + minDate.getFullYear());
            $('.enquiry-to-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
            $('#enquiry-from-date').datepicker('setEndDate', minDate);
        }
    });

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $('.jiva-spa-date').removeClass('visible');
        $(this).siblings('.jiva-spa-date').addClass('visible');
    });

    var validateEnquiryElements = function() {
        $('.sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
            }
        });
    };

    // Request quote submit handler
    $('.enquiry-submit-btn').click(function() {
        $('.sub-form-mandatory').each(function() {
            if ($(this).hasClass('invalid-input')) {
                $(this).removeClass('invalid-input');
            }
        });
        validateEnquiryElements();
        if ($('.sub-form-input-element').hasClass('invalid-input')) {
            $('.invalid-input').first().focus();
        } else {
            if (captchaValidation) {
                submitEnquiry();
            }
        }
    });

    $('.cm-page-container').click(function() {
        $('.jiva-spa-date').removeClass('visible');
    });

    $('#enquiryGender').selectBoxIt();
}
