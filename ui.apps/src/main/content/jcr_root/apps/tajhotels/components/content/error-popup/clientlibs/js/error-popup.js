$(document).ready(function() {
    var titleMsg = $('#warning-msg').val();
    var isPopUpEnabled = $('#pop-up-enabled').val();
    if(isPopUpEnabled == 'true') {
    var popupParams = {
				title: titleMsg,
				isWarning: true
			}
	warningBox(popupParams);
    }
})