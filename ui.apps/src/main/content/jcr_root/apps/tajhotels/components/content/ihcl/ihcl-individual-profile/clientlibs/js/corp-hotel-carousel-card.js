$(document).ready(function() {
    addShowMoreforIndividualProfile()
    hotel_carousel();
    popUpProfileIhcl();

});

function addShowMoreforIndividualProfile() {
    if ($(window).width() < 992) {
        $('.desc-sectionleader .rooms-suites-description').each(function() {
            $(this).cmToggleText({
                charLimit : 450
            });
        })
    }
}

function hotel_carousel() {
    $('.desc-sectionleader').each(function() {
        $(this).parallelShowMoreFn();
    });
}

function popUpProfileIhcl() {
    window.addEventListener('load', function() {
        var presentScroll;
        $('.view-button').click(function() {
            presentScroll = $(window).scrollTop();
            $('.profile-popup').show().addClass('active-popUp');
            $(".cm-page-container").addClass('prevent-page-scroll');
            $('.mr-profile-popup').show();
        });
        $(document).keydown(function(e) {
            if (($('.active-popUp').length) && (e.which === 27)) {
                $('.showMap-close').trigger("click");
            }
        });

        $('.showMap-close').click(function() {
            $('.profile-popup').hide().removeClass('active-popUp');
            $(".cm-page-container").removeClass('prevent-page-scroll');
            $('.mr-profile-popup').hide();
            $(window).scrollTop(presentScroll);
        })

    });
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLWhvdGVscy1jYXJvdXNlbC1jYXJkL2NvcnAtaG90ZWxzLWNhcm91c2VsLWNhcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gaG90ZWxfY2Fyb3VzZWwoKSB7XHJcbiAgICAkKCBkb2N1bWVudCApLnJlYWR5KCBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIGRldmljZURldGVjdG9yLmNoZWNrRGV2aWNlKCkgPT0gXCJzbWFsbFwiICkge1xyXG4gICAgICAgICAgICAkKCAnLmFib3V0LWhvdGVscy1jb250YWluZXIgcCcgKS5lYWNoKCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoIHRoaXMgKS5jbVRvZ2dsZVRleHQoIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDIwMFxyXG4gICAgICAgICAgICAgICAgfSApXHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9ICk7XHJcbn0iXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NvcnAtaG90ZWxzLWNhcm91c2VsLWNhcmQvY29ycC1ob3RlbHMtY2Fyb3VzZWwtY2FyZC5qcyJ9
