$(document).ready(
        function() {
            // initiate calender for checkin and checkout
            initiateCalender();

            initiateGuestDrpdwn();

            updateRoomGuestsCount();

            fetchEntityDetailsOrNotHandler();

            $('.ihclcb-guest-dropdown-cont').click(function(e) {
                e.stopPropagation();
            });

            $(".ihclcb-guest-dropdown-cont .ihclcb-bas-room-details-container").delegate(".ihclcb-guest-inc", "click",
                    guestCountUpdater);

            $(".ihclcb-guest-dropdown-cont .ihclcb-bas-room-details-container").delegate(".ihclcb-guest-dec", "click",
                    guestCountUpdater);

            $('#bookStayConfirmEntity').change(
                    function() {
                        if (this.checked) {
                            if ($('#booking-search .searchbar-input').attr('data-selected-search-value')
                                    && isEntityDropdownSelected()) {
                                enableBestAvailableButton('', true);
                            }
                        } else {
                            changeBookStayButtonStatus();
                        }
                    });

            // close the entity dropdown while clicking outside;
            $(window).click(function(e) {
                var container = $(".entity-dropdown-container");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    // get Event here
                    $('.show-dropdown').removeClass('show-dropdown');
                }
            });

        });

function updateIHCLCBEntityDataInSession(entityKey, value) {
    var ihclcbData = {}
    if (!dataCache.session.getData("ihclcbData")) {
        ihclcbData.entityDetails = {}
    } else {
        ihclcbData = dataCache.session.getData("ihclcbData");
    }
    ihclcbData.entityDetails[entityKey] = value;
    dataCache.session.setData("ihclcbData", ihclcbData);

}

function CheckInputFieldFilledStatus() {
    var notFilled = 0
    $('.ihclcb-mandetory-field').each(function() {
        if (!$(this).val()) {
            notFilled++;
        }
    });
    return notFilled == 0;
}

function updateRoomGuestsCount() {
    try {
        var bookingOptions = dataCache.session.getData("bookingOptions");
        if (bookingOptions && bookingOptions.roomOptions.length > 0) {
            var selectionText = '';
            var totalGuests = 0;
            var guestPerRoom = [];
            bookingOptions.roomOptions.forEach(function(value, index) {
                totalGuests = parseInt(value.children) + parseInt(value.adults);
                guestPerRoom.push(totalGuests);
            });

        }
        $('.ihclcb-guest-dropdown-cont .ihclcb-bas-room-no').each(
                function(ind, ele) {
                    var guestCount = guestPerRoom[ind];
                    var createEle = '<span class="guestCount">  (' + guestCount + ' ' + createGuestWord(guestCount)
                            + ')</span>';
                    ele.append($(createEle)[0]);
                });
    } catch (error) {
        console.error(error);
    }
}

function createGuestWord(guestCount) {
    var guestWord = '';
    if (guestCount > 1) {
        guestWord = 'Guests';
    } else {
        guestWord = 'Guest';
    }
    return guestWord;
}

function guestCountUpdater() {

    var guestcount = 0;
    $(this).closest('.ihclcb-bas-room-details').children('li').each(function() {
        guestcount += parseInt($(this).find('input').val());
    });
    var $guestsEleSelector = $('.ihclcb-guest-dropdown-cont .ihclcb-bas-room-no.bas-active .guestCount');
    if ($guestsEleSelector.length) {
        $guestsEleSelector.text(' (' + guestcount + ' ' + createGuestWord(guestcount) + ')');
    }
}

function createguestCountContainer() {
    var createEle = '<span class="guestCount"> (1 Guest) </span>';
    $('.ihclcb-guest-dropdown-cont .ihclcb-bas-room-no.bas-active').append($(createEle)[0]);
}

function initiateGuestDrpdwn() {
    $(".ihclcb-bas-add-room").on('click', function() {
        addRoomModuleIhclcb();
        createguestCountContainer();
        // activate last room
        $('.ihclcb-bas-room-no').last().click();
    });

    $(".ihclcb-bas-room-details-container").on("click", ".ihclcb-bas-room-no", function() {
        makeRoomActiveModuleIhclcb($(this));
    });
    
    $(".ihclcb-bas-room-details-container").on("click", ".ihclcb-bas-room-no", function() {
        var clickedRoom = $(this).attr('id') + "Details";
        var $currentRoomDetails = $(".ihclcb-bas-room-details-container").find("ul#" + clickedRoom);
        $(this).after($currentRoomDetails);
    });

    $(".ihclcb-bas-room-details-container").on("click", ".ihclcb-bas-room-delete-close", function(e) {
        e.stopPropagation();
        if ($(".ihclcb-bas-room-details").length > 1) {
            var roomDeleteIndex = $(this).closest('.ihclcb-bas-room-no').index('.ihclcb-bas-room-no');
            deleteRoomIhclcb(roomDeleteIndex);
        }
    });

    $(".ihclcb-bas-room-details-container").delegate(".ihclcb-guest-inc", "click", incGuestCountIhclcb);

    $(".ihclcb-bas-room-details-container").delegate(".ihclcb-guest-dec", "click", decGuestCountIhclcb);
}

function deleteRoomIhclcb(roomDeleteIndex) {

    $('.ihclcb-bas-room-no').eq(roomDeleteIndex).remove();
    $('.ihclcb-bas-room-details').eq(roomDeleteIndex).remove();
    var totalRoomsAfterDeletion = $('.ihclcb-bas-room-details').length;
    if (totalRoomsAfterDeletion < 2) {
        $('.ihclcb-bas-room-delete-close .icon-close').addClass('cm-hide');
    }
    var roomID = 1;
    $('.ihclcb-bas-room-no').each(function() {
        $(this).attr('id', 'room' + roomID);
        $(this).find('.bas-span-room-no').text(roomID);
        ++roomID;
    });
    roomID = 1;
    $('.ihclcb-bas-room-details').each(function() {
        $(this).attr('id', 'room' + roomID + "Details");
        ++roomID;
    });

    if ($('.ihclcb-bas-room-no').hasClass('bas-active') == false) {
        if ($('.ihclcb-bas-room-no').eq(roomDeleteIndex).length > 0) {
            makeRoomActiveModuleIhclcb($('.ihclcb-bas-room-no').eq(roomDeleteIndex));
        } else {
            makeRoomActiveModuleIhclcb($('.ihclcb-bas-room-no').eq(roomDeleteIndex - 1));
        }
    }

    updateTotalAdultsIhclcb();
    updateRoomNumberIhclcb();
    $(".ihclcb-bas-add-room").removeClass("bas-hide");

}

function updateRoomNumberIhclcb() {
    var room = "Room";
    if ($(".ihclcb-bas-room-details").length > 1)
        room = "Rooms";
    else
        room = "Room";
    $(".ihclcb-room-border").text($(".ihclcb-bas-room-details").length + " " + room);
}

function decGuestCountIhclcb(e) {
    e.preventDefault();

    var $button = $($(this)[0])
    var quantity = parseInt($button.parent().parent().find("input").val());
    var x = $button.parent().parent().parent().attr("class");
    if (quantity > 0) {
        if (x === "ihclcb-bas-no-of-adults" && quantity == 1) {
            quantity = 2;
        }
        $button.parent().parent().find("input").val(quantity - 1);
    }

    updateTotalAdultsIhclcb();
}

function incGuestCountIhclcb(e) {
    e.preventDefault();

    var $button = $(this);
    var quantity = parseInt($button.parent().parent().find("input").val());
    ++quantity;

    if (quantity > 7) {
        quantity = 7;
    }

    $button.parent().parent().find("input").val(quantity);

    updateTotalAdultsIhclcb();
}

function addRoomModuleIhclcb() {

    var room_no = $(".ihclcb-bas-room-no").length + 1;
    if (room_no < 6) {
        $(".ihclcb-bas-add-room")
                .before(
                        '<li class="ihclcb-bas-room-no" id=room'
                                + room_no
                                + '><button class="btn-only-focus" aria-label = "bas room"><span class="bas-room bas-desktop">Room &nbsp</span><span class="bas-span-room-no">'
                                + room_no
                                + '</span></button><div class="ihclcb-bas-room-delete-close"><i class="icon-close"><button class="btn-only-focus" aria-label = "close icon"></button></i></div></li>');

        $($(".ihclcb-bas-room-details")[0]).clone().appendTo(".ihclcb-bas-room-details-container").addClass("bas-hide");

        var x = room_no;
        room_no--;
        $($(".ihclcb-bas-room-details")[room_no]).attr("id", "room" + x + "Details");
        $($(".ihclcb-bas-room-details")[room_no]).find("input").val("1");
        $($($(".ihclcb-bas-room-details")[room_no]).find("input")[1]).val("0");

        updateTotalAdultsIhclcb();
        updateRoomNumberIhclcb();
        var elRoomNumber = $(".ihclcb-bas-about-room-container").find('.ihclcb-bas-room-no');
        if (elRoomNumber.length >= 5) {
            $(".ihclcb-bas-add-room").addClass("bas-hide");
        } else {
            $(".ihclcb-bas-add-room").removeClass("bas-hide");
        }
        // script to make newly added room active

        makeRoomActiveModuleIhclcb($('.ihclcb-bas-room-no').last());
        $('.ihclcb-bas-room-delete-close .icon-close').removeClass('cm-hide');
    }
}

function updateTotalAdultsIhclcb() {
    var totalAdultsCount = 0;
    $('.ihclcb-bas-no-of-adults, .ihclcb-bas-no-of-child').find('input').each(function() {
        totalAdultsCount += parseInt($(this).val());
    });
    var guestSuffix = (totalAdultsCount > 1) ? 's' : '';
    $(".ihclcb-bas-count-of-adults").text(totalAdultsCount + " Guest" + guestSuffix);
}

// module to make room active
function makeRoomActiveModuleIhclcb(elem) {
    $(".ihclcb-bas-room-no").each(function() {
        $(this).removeClass("bas-active");
    });

    var new_room_no = elem.attr("id") + "Details";

    $(".ihclcb-bas-room-details-container").find("ul").each(function() {
        $(this).addClass("bas-hide");
    });
    $($(".ihclcb-bas-room-details-container").find("ul")[0]).removeClass("bas-hide");
    $(".ihclcb-bas-room-details-container").find("ul#" + new_room_no).removeClass("bas-hide");

    elem.addClass("bas-active");
}
function fetchEntityDetailsOrNotHandler() {
    var ihclCbBookingObject = dataCache.session.getData("ihclCbBookingObject");
    if (ihclCbBookingObject && !ihclCbBookingObject.entityDetails) {
        fetchEntityDetails();
    }
}

function addEntityResponseToSession(entityResponse) {
    var entityDetails = [];
    var ihclBookingSessionData = dataCache.session.getData("ihclCbBookingObject");
    var entityItems = entityResponse.Corp_Booking_tool_Output;
    if (ihclBookingSessionData && ihclBookingSessionData.isIHCLCBFlow) {
        for (var entity = 0; entity < entityItems.length; entity++) {
            var city = entityItems[entity].City;
            var partyName = entityItems[entity].PartyName;
            var gSTNTaxID_c = entityItems[entity].GSTNTaxID_c;
            var partyNumber = entityItems[entity].PartyNumber;
            var iATANumber = entityItems[entity].IATANO_c;
            var AccountType_c = entityItems[entity].AccountType_c;
            if (city !== "") {
                var entityDetailsObj = new Object();
                entityDetailsObj.city = city;
                entityDetailsObj.partyName = partyName;
                entityDetailsObj.gSTNTaxID_c = gSTNTaxID_c;
                entityDetailsObj.partyNumber = partyNumber;
                entityDetailsObj.iATANumber = iATANumber;
                entityDetailsObj.AccountType_c = AccountType_c;
                entityDetails.push(entityDetailsObj);
            }
        }
    }
    ihclBookingSessionData.entityDetails = entityDetails;
    dataCache.session.setData("ihclCbBookingObject", ihclBookingSessionData);
	// For Booker Id
    var bookerData = getUserData();
	bookerData.profileId = entityResponse["Profile Id "];
    console.log('bookerData::', bookerData);
    setUserData(bookerData);
    fetchIHCLCBEntityDetails();
}

function fetchEntityDetails() {
    var corporateBookStayCont = $('.ihclcb-corporate-book-stay');
    var ihclcbEntityFetchApiHostUrl = corporateBookStayCont.attr('data-ihclcbHostApiUrl');
    var localIHCLstorage = JSON.parse(localStorage.tajData);
    var entityPartyId = localIHCLstorage.userDetails.partyId;
    var ihclcbFetchEntityEndpointUrl = corporateBookStayCont.attr('data-ihclcbEntityApiUrl');
    var apiIHCLCBRequestBodyJSON = {
        "partyid" : entityPartyId
    };
    $('body').showLoader();
    $.ajax({
        url : ihclcbEntityFetchApiHostUrl + ihclcbFetchEntityEndpointUrl,
        headers : {
            "Authorization" : "Basic cmFqLnNyaW5pdmFzYW5AaW5ub3ZhY3guY29tOlNtaWxlQDI1",
            "content-Type" : "application/json; charset=UTF-8"
        },
        type : "POST",
        dataType : 'json',
        data : JSON.stringify(apiIHCLCBRequestBodyJSON),
    }).done(function(apiIHCLCBResponseBodyJSON) {
        addEntityResponseToSession(apiIHCLCBResponseBodyJSON);
    }).fail(function(res) {
        alert("Error loading entity details Reload Again!");
    }).always(function() {
        $('body').hideLoader();
    });
}
