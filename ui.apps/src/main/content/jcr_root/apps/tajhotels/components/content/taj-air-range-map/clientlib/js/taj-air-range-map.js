var w1 = window.outerWidth;
var w2 = window.innerWidth;

$(function() {
    $(".range-map-container").mousemove(function(e) {

        if (w1 < 420 || w2 < 420) {

            $('.pointer-cursor').offset({
                left : e.pageX - 150,
                top : e.pageY - 100
            });
        } else if ((w1 > 420 || w2 > 420) && (w1 < 800 || w2 < 800)) {
            $('.pointer-cursor').offset({
                left : e.pageX - 370,
                top : e.pageY - 250
            });
        } else {
            $('.pointer-cursor').offset({
                left : e.pageX - 505,
                top : e.pageY - 330
            });
        }

        $('#status').html(e.pageX + ', ' + e.pageY);

    });
});
