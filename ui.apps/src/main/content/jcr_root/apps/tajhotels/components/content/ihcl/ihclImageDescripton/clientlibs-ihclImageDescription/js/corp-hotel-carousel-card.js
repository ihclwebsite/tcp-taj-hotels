$(document).ready(function() {
    hotel_carousel();
    popUpimageDescription();
});

function hotel_carousel() {
    $('.desc-sectionleader').each(function() {
        $(this).parallelShowMoreFn();
    });
}

function popUpimageDescription() {
    var presentScroll;
    $('#pop-up .pop-up.button-view-details').off().on('click', function() {
        presentScroll = $(window).scrollTop();
        $(this).closest('.about-specific-hotel').next().show().find('.mr-profile-popup2').show();
        $(".cm-page-container").addClass('prevent-page-scroll');

    });

    $(document).keydown(function(e) {
        if (($('.mr-profile-popup2').length) && (e.which === 27)) {
            $('.showMap-close').trigger("click");
        }
    });

    $('.showMap-close').off().on('click', function() {

        $(".cm-page-container").removeClass('prevent-page-scroll');
        $('.mr-profile-popup2').hide();
        $(window).scrollTop(presentScroll);
    })

}
