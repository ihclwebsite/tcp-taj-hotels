/*function setSession(nights) {
  if(nights)   {
       var bookOptions = dataCache.session.getData("bookingOptions");
       bookOptions.nights = parseInt(nights);
       bookOptions.roomOptions[0].adults = 2;
       dataCache.session.setData("bookingOptions", bookOptions);
   }
}*/

$( document ).ready( function() {
   $('.holidays-tic-carousel').each(function(){
       $(this).customSlide(3);
   })
   $('.hotel-offer-inclusions-show-more').click(function(){
   	var $inclusionPopup = $(this).closest('.hotel-offer-hotel-card').find('.hotel-offer-inclusions-popup');
   	$inclusionPopup.show();
   	$([document.documentElement, document.body]).animate({
           scrollTop: $inclusionPopup.offset().top
       }, 600);
   });
   $('.hotel-offer-inclusion-close').click(function(){
   	$(this).closest('.hotel-offer-inclusions-popup').hide();
   });
});