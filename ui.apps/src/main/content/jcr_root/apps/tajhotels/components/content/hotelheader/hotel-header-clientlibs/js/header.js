$(document).ready(function() {

    var headerDDInitCon = $('.cm-header-dd-options-con');
    var headerDropdowns = $('.header-warpper .cm-header-dropdowns');
    var headerArrows = $('.header-dropdown-image');
    var entityLogin = $('#corporate-booking-login').attr('data-corporate-isCorporateLogin') == "true";
    profileFetchListener(showUserPoints);

    if (isCurrencyCacheExists()) {
        var cacheObject = getCurrencyCache()
        setActiveCurrencyInDom(cacheObject.currencySymbol, cacheObject.currency, cacheObject.currencyId);
    } else {
        setActiveCurrencyWithDefaultValues();
    }

	checkUserDetailsForHeader();

    function isCurrencyCacheExists() {
        var currencyCache = dataCache.session.getData("currencyCache");
        if (!currencyCache)
            return false;
        else
            return true;
    }

    if (deviceDetector.isIE() == "IE11") {
        $(".brand-logo-wrapper img").addClass('.ie-tajLogo-img');
    }

    scrollToViewIn();
    function setActiveCurrencyWithDefaultValues() {
        try {
            var dropDownDoms = $.find('.header-currency-options');
            var individualDropDownDoms = $(dropDownDoms).find('.cm-each-header-dd-item');
            var firstDropDownDom;
            if (individualDropDownDoms && individualDropDownDoms.length) {
                firstDropDownDom = individualDropDownDoms[0];
            }

            var currencyId = $(firstDropDownDom).data().currencyId;
            var currencySymbol = $($(firstDropDownDom).find('.header-dd-option-currency')).text();
            var currency = $($(firstDropDownDom).find('.header-dd-option-text')).text();

            if (currencySymbol != undefined && currency != undefined && currencyId != undefined) {
                setActiveCurrencyInDom(currencySymbol, currency, currencyId);
                setCurrencyCache(currencySymbol, currency, currencyId);
            }
        } catch (error) {
            console.error(error);
        }
    }

    $('.header-warpper .cm-header-dropdowns').each(function() {
        $(this).on('click', function(e) {
            e.stopPropagation();
            var arrow = $(this).closest('.nav-item').find('.header-dropdown-image');
            var target = $(this).closest('.nav-item').find('.cm-header-dd-options-con');
            if (target.hasClass('active')) {
                target.removeClass('active');
                arrow.removeClass('header-dropdown-image-selected');
                $(this).removeClass('nav-link-expanded');
                return;
            }
            headerDropdowns.removeClass('nav-link-expanded')
            headerDDInitCon.removeClass('active');
            headerArrows.removeClass('header-dropdown-image-selected');
            target.addClass('active');
            arrow.addClass('header-dropdown-image-selected');
            $(this).addClass('nav-link-expanded')
        });
    });

    $('body').on('click', function() {
        headerDDInitCon.removeClass('active');
    });

    var windowWidth = $(window).width();
    if (windowWidth < 992) {
        $('.ihcl-header .navbar-toggler').addClass('navbar-dark');
        if (windowWidth < 768) {
            var bookAStayBtn = $('.header-warpper a.book-stay-btn .book-stay-btn')
            if (bookAStayBtn.text().trim() == "Book your dream wedding") {
                bookAStayBtn.text("BOOK A VENUE");
            }
        }
    }

    $('.collapse').on('show.bs.collapse', function() {
        $(".cm-page-container").addClass('prevent-page-scroll');
    });

    $('.header-currency-options').on('click', '.cm-each-header-dd-item', function() {
        try {
            var elDDCurrencySymbol = $(this).find('.header-dd-option-currency');
            var elDDCurrency = $(this).find('.header-dd-option-text');

            var elActiveCurrSymbol = $(this).closest('.nav-item').find('.selected-currency');
            var elActiveCurrency = $(this).closest('.nav-item').find('.selected-txt');

            var currencySymbol = elDDCurrencySymbol.text();
            var currency = elDDCurrency.text();
            var currencyId = $(this).data('currency-id');

            if (currencySymbol != undefined && currency != undefined && currencyId != undefined) {
                setCurrencyCache(currencySymbol, currency, currencyId);
            }

            elActiveCurrSymbol.text(currencySymbol);
            elActiveCurrSymbol.attr("data-selected-currency", currencyId)
            elActiveCurrency.text(currency);
            $(document).trigger('currency:changed', [ currency ]);
        } catch (error) {
            console.error(error);
        }
    });

    $('.profile-name-wrp').click(function(e) {
        e.stopPropagation();
        $('.profile-options').toggle();
        $('.profile-name-wrp .header-dropdown-image').toggleClass('cm-rotate-show-more-icon');
    });

    $('.cm-page-container').click(function() {
        $('.profile-options').hide();
        $('.profile-name-wrp .header-dropdown-image').removeClass('cm-rotate-show-more-icon');
    });

    $('.header-mobile-back-btn').click(function() {
        $('.navbar-collapse').removeClass('show');
        $(".cm-page-container").removeClass('prevent-page-scroll');
    })

    $('.sign-in-btn').click(function() {

        $('body').trigger('taj:sign-in');
    });

    $('body').on('taj:loginSuccess', function(event, id, name) {
        showHeaderUserProfile(name);
    });

    $('body').on('taj:pointsUpdated', function(event) {
        showUserPoints();
    });

    function showHeaderUserProfile(name) {
        $('.sign-in-btn').addClass('cm-hide');
        $('.header-profile').removeClass('cm-hide').addClass('cm-show');
        $('.header-profile .profile-username, .navbar-brand .profile-username').text(name);
        showUserPoints();
    }

    function showUserPoints() {
        var userDetails = dataCache.local.getData("userDetails");
        if (userDetails.membershipId) {
            $('.header-profile .points-cont').removeClass('d-none');
            $('[data-component-id="enrol-btn"]').remove(); // remove enrol buttons for users having membership id
            $('.header-profile .edit-profile').hide();
            if (userDetails.tier) {
                $('.header-profile .tic-tier span').text(userDetails.tier);
                $('.header-profile .tic-tier').show();
            } else {
                $('.header-profile .tic-tier').hide();
            }
            $('.header-profile .tic-points').text(userDetails.ticPoints);
            $('.header-profile .epicure-points').text(userDetails.epicurePoints);
            $('.header-profile .tap-points').text(userDetails.tapPoints);
            $('.header-profile .tappme-points').text(userDetails.tappmePoints);
            if (userDetails.card) {
                var accountType = userDetails.card.type.toLowerCase() + "-points";
                accountType = accountType.includes("tic") ? "tic-points" : accountType;
                $('.prof-content-value').each(function() {
                    $(this).attr("id") === accountType ? $(this).parent().show() : $(this).parent().hide();
                });

                if (userDetails.card.type === "TAP" || userDetails.card.type === "TAPPMe") {
                    $('.tic-tier').remove();
                }
                $('.prof-tic-content').show();
            } else {
                console.log("unable to retrieve user card details");
                $('.prof-tic-content').hide();
            }
        } else {
            $('.header-profile .points-cont').addClass('d-none');
        }
    }

    $('.header-profile .logout-btn').on('click', function(event) {
        event.stopPropagation();
        checkToClearSelections();
    });

    $('body').on('taj:logout', function() {
        tajLogout();
    });
    $('body').on('taj:sessionLogout', function(){
		logoutWithoutReloding();
    });

    // SSO logout 
    function checkToClearSelections() {
        var bOptions = dataCache.session.getData('bookingOptions');
        if (bOptions.selection && (bOptions.selection.length > 0)) {
            var popupParams = {
                title : $(".sign-out-clear-selections-popupMessage").text()
                || 'Sign Out will clear room slections?',
                callBack : clearSelectionAndLogout.bind(),
                // callBackSecondary: secondaryFn.bind( _self ),
                needsCta : true,
                isWarning : true
            }
            warningBox(popupParams);
        } else {
            tajLogout();
        }
    }

    function clearSelectionAndLogout() {
        clearRoomSelections();
        tajLogout();
    }

    function clearRoomSelections() {
        var boptions = dataCache.session.getData("bookingOptions");
        if (boptions && boptions.roomOptions) {
            var rOptions = boptions.roomOptions;
            var roomOptArray = [];
            for (var d = 0; d < rOptions.length; d++) {
                var roomOpt = {
                    adults : rOptions[d].adults,
                    children : rOptions[d].children,
                    initialRoomIndex : d
                };
                roomOptArray.push(roomOpt);
            }
            boptions.previousRooms = roomOptArray
            boptions.roomOptions = roomOptArray;
            boptions.rooms = boptions.roomOptions.length;
            boptions.selection = [];
            dataCache.session.setData("bookingOptions", boptions);
        }
    }


    function tajLogout() {
        logoutBot();
        facebookLogout();
        googleLogout();
        var signOutRedirect = $("[data-redirect-link]").data("redirect-link");
        logoutSuccess(signOutRedirect);
    }

    function googleLogout() {
        try {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function() {
                console.info('User signed out.');
            });
        } catch (error) {
            console.error("Attempt for google logout failed.")
            console.error(error);
        }
    }

    function facebookLogout() {
        try {
            FB.logout(function(response) {
                // user is now logged out
                console.info("user is now logged out");
            });
        } catch (error) {
            console.error("Attempt for facebook logout failed.")
            console.error(error);
        }
    }

    function logoutSuccess(redirectUrl) {
		logoutBot();
        logoutWithoutReloding();
        //formTheRedirectionURL(redirectUrl);
        document.location.reload();
    }


    	function logoutWithoutReloding() {
            var isCorporateLogin = userCacheExists() ? userCacheExists().isCorporateLogin : false;
            dataCache.local.removeData("userDetails");
            logoutBot();
			showSignInAndEnroll();
            if (!isCorporateLogin) {  

                if($.removeCookie){
                    $.removeCookie('ihcl-sso-token', {
                        path : '/',
                        domain : location.hostname
                    });
                } 
                else {
                    document.cookie = "ihcl-sso-token=" + ";max-age=0" +";path=/" + ";domain=" + location.hostname;
                } 
                
                var imgTagForCookieCalls = $('#single-sign-out');
                $(".sso-urls-to-set-cookie").each(
                    function() {
                        var indTagForCookieCall = '<img src="'
                        + $(this).text()
                        + $('.single-sign-out-sevlet-uri').text()
                        + '" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;"></img>';
                        imgTagForCookieCalls.append(indTagForCookieCall);
                        console.log(indTagForCookieCall);
                        console.log(indTagForCookieCall);
                    });
                $('#single-sign-out').html(imgTagForCookieCalls);
                console.log('single-sign-out: logoutSuccess() and isCorporateLogin: false');
            } else {
                dataCache.session.removeData("ihclCbBookingObject");
            }
        }


		// SSO changes 
        function checkUserDetailsForHeader() {
                var user = userCacheExists();
                var isCorporateLogin = false;
                var showSignIN = true;
                hideSignInAndEnroll();
                var ihclSSOToken;

                if($.cookie){
                    ihclSSOToken = $.cookie('ihcl-sso-token');
                } else {
    				ihclSSOToken = getSSOTokenCookie('ihcl-sso-token');
                }


                if (isIHCLCBSite()) {
                    console.log('isIHCLCBSite: true');
                    //if (user && user.isCorporateLogin) {
                    if(user) {
                    	isCorporateLogin = user.isCorporateLogin;
                        showHeaderUserProfile(user.name);
                    } else {
                        console.log('user.isCorporateLogin: false');
                        dataCache.local.removeData("userDetails");
                        clearRoomSelections();
                        showSignInAndEnroll();
                    }
                } else if (user && user.authToken && !user.isCorporateLogin) {
                    console.log('user.authToken: true && isCorporateLogin: false');
                    if (user.authToken === ihclSSOToken) {
                        console.log('user.authToken === ihclSSOToken: true');
                        showHeaderUserProfile(user.name);
                    }
                    /*
                    else if(user && user.authToken){
                        console.log('user.authToken != ihclSSOToken; authtoken is present');
                        showHeaderUserProfile(user.name);
                    }
                    */
                    else if (ihclSSOToken) {
                        console.log('ihclSSOToken: true');
                        getUserDetailsUsingToken(ihclSSOToken);
                    }
                    else {
                        console.log('user.authToken === ihclSSOToken: false && ihclSSOToken: false');
                        dataCache.local.removeData("userDetails");
                        clearSelectionAndLogout();
                        showSignInAndEnroll();
                    }
                } else if (ihclSSOToken) {
                    console.log('ihclSSOToken: true');
                    getUserDetailsUsingToken(ihclSSOToken);
                } else {
                    console.log('SSO final else condition');
                    showSignInAndEnroll();
                }
        
        }

    	// fallback function for getting cookie value
        function getSSOTokenCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }
        
        function hideSignInAndEnroll() {
            $('.sign-in-btn').addClass('cm-hide');
            $('[data-component-id="enrol-btn"]').addClass('cm-hide');
        }
        
        function showSignInAndEnroll() {
            $('.sign-in-btn').removeClass('cm-hide');
            $('[data-component-id="enrol-btn"]').removeClass('cm-hide');
            hideProfileDetails();
        }

    	function hideProfileDetails(){
    		$('.header-profile').addClass('cm-hide').removeClass('cm-show');
        }

        function showLoader() {
    		$('body').showLoader();
        }
		function hideLoader() {
            $('body').hideLoader();
        }

        function getUserDetailsUsingToken(ihclSSOToken) {
            showLoader();
            var showSignIN = true;
            $.ajax({
                type : "POST",
                url : "/bin/fetchUserDetails",
                data : "authToken=" + encodeURIComponent(ihclSSOToken)
            }).done(function(res) {
				if(res){
					res = JSON.parse(res);
					if (res.userDetails && res.userDetails.name) {
						updateLoginDetails(res);
						showSignIN = false;
					}
				}
                hideLoader();
            }).fail(function(res) {
                if(res.status === 401){
                    forceLogoutAfterUnauthorized();
                } 
            }).always(function() {
                if (showSignIN) {
                    showSignInAndEnroll();
                }
                hideLoader();
            });
        }


        function updateLoginDetails(res) {
            if (res && res.authToken) {
                var userDetails = res.userDetails;
                var authToken = res.authToken;
                incorrectLoginCount = 0;
                successHandler(authToken, userDetails);
            } else if (res.errorCode === "INVALID_USER_STATUS" && res.status === "504" && !entityLogin) {
                // user activation flow
                invokeActivateAccount();
            } else if (res.errorCode === "INVALID_USER_STATUS" && res.status === "506" && !entityLogin) {
                // migrated user
                var error = res.error;
                var errorCtaText = "RESET PASSWORD";
                var errorCtaCallback = invokeForgotPassword;
                $('body').trigger('taj:loginFailed', [ error, errorCtaText, errorCtaCallback ]);
            } else {
                if (entityLogin) {
                    forgotPasswordLinkWrp.show();
                    $('.ihclcb-login-error').text(res.error).show();
                }
            }
        }


        function successHandler(authToken, userDetails) {
            var isentityLogin = $('#corporate-booking-login').attr('data-corporate-isCorporateLogin') == "true";
            localUserDetails(authToken, userDetails);

            // defined in user.js
            dataToBot();

            var id = userDetails.membershipId;
            var name = userDetails.name;
            $('.generic-signin-close-icon').trigger("click");
            $('body').trigger('taj:loginSuccess', [ id, name ]);
            
            if (id) {
                $('body').trigger('taj:fetch-profile-details', [ true ]);
            } else {
                $('body').trigger('taj:login-fetch-complete');
            }
            if (!isentityLogin) { // added by sarath
                $('body').trigger('taj:tier');
            }
            hideLoader();
        }


        function localUserDetails(authToken, userDetails) {
            var isentityLogin = $('#corporate-booking-login').attr('data-corporate-isCorporateLogin') == "true";
            var user = {
                authToken : authToken,
                name : userDetails.name,
                firstName : userDetails.firstName,
                lastName : userDetails.lastName,
                gender : userDetails.gender,
                email : userDetails.email,
                countryCode : userDetails.countryCode,
                mobile : userDetails.mobile,
                cdmReferenceId : userDetails.cdmReferenceId,
                membershipId : userDetails.membershipId,
                googleLinked : userDetails.googleLinked,
                facebookLinked : userDetails.facebookLinked,
                title : userDetails.title
            };
            if(userDetails.tier) {
                user.tier =  userDetails.tier
            } else if(userDetails.card && userDetails.card.tier){
                user.tier = userDetails.card.tie;
            }
            if (isentityLogin) {
                user.partyId = userDetails.cdmReferenceId
            }
            dataCache.local.setData("userDetails", user);
            if ($('.mr-contact-whole-wrapper').length > 0) {
                window.location.reload();
            }
        }

});

function getSelectedCurrency() {
    return dataCache.session.getData("selctedCurrency");
}

function getCurrencyCache() {
    return dataCache.session.getData("currencyCache");
}

function setActiveCurrencyInDom(currencySymbol, currency, currencyId) {

    $($.find("[data-inject-key='currencySymbol']")[0]).text(currencySymbol);
    $($.find("[data-inject-key='currency']")[0]).text(currency);
    $($.find("[data-selected-currency]")[0]).attr("data-selected-currency", currencyId);
}

function setCurrencyCache(currencySymbol, currency, currencyId) {
    var currencyCache = {};
    currencyCache.currencySymbol = currencySymbol;
    currencyCache.currency = currency;
    currencyCache.currencyId = currencyId;
    dataCache.session.setData("currencyCache", currencyCache);
}

function setCurrencyCacheToBookingOptions() {
    var bookingOptions = getBookingOptionsSessionData();
    bookingOptions.currencySelected = dataCache.session.getData('currencyCache').currencyId;
    dataCache.session.setData("bookingOptions", bookingOptions);
}

function setActiveCurrencyWithResponseValue(currencyType) {

    var infor = false;
    var dropDownDoms = $.find('.header-currency-options');
    var individualDropDownDoms = $(dropDownDoms).find('.cm-each-header-dd-item');
    var firstDropDownDom;
    if (individualDropDownDoms && individualDropDownDoms.length) {
        for (var m = 0; m < individualDropDownDoms.length; m++) {
            if ($(individualDropDownDoms[m]).data().currencyId == currencyType) {
                firstDropDownDom = individualDropDownDoms[m];
                infor = true;
            }
        }
    }
    if (firstDropDownDom) {
        var currencyId = $(firstDropDownDom).data().currencyId;
        var currencySymbol = $($(firstDropDownDom).find('.header-dd-option-currency')).text();
        var currency = $($(firstDropDownDom).find('.header-dd-option-text')).text();

        if (currencySymbol != undefined && currency != undefined && currencyId != undefined) {
            setActiveCurrencyInDom(currencySymbol, currency, currencyId);
            setCurrencyCache(currencySymbol, currency, currencyId);
            setCurrencyCacheToBookingOptions();
        }
    }
    return infor;
}

function formTheRedirectionURL(authoredURL) {
    var url = authoredURL;
    if (!isIHCLCBSite() && !url.includes('https')) {
        var url = url + ".html"
    } else if (isIHCLCBSite()) {
        dataCache.session.removeData("ihclCbBookingObject");
    }
    window.location.href = url;
}
function stopAnchorPropNav(obj) {
    if (window.location.href.includes('en-in/taj-air')) {
        var attr = obj.text;
        prepareQuickQuoteJsonForClick(attr);
    }
    return true;
}


function scrollToViewIn() {
    console.log('binded');
    var scrollElem = $(".scrollView");
    if(scrollElem && scrollElem.length > 0) {
        $(".scrollView").each(function(){
            $(this).on('click', function() {
                var classStr = $(this).attr('class').slice(11);
                $('html, body').animate({
                    scrollTop: $('#'+classStr).offset().top
                }, 1000);
            });
        });
    }
}


//updated for global data layer.
function gtmDataLayerFromHeader(){
    $('#navbarSupportedContent .navbar-nav>.nav-item>a').each(function(){
        $(this).click(function(){
            var eventType = "" ;                 
			switch($(this).text().toLowerCase()) {
              	case 'home':
					eventType = 'TopMenu_KnowMore_HomePage_Home';
                	break;
              	case 'benefits':
					eventType = 'TopMenu_KnowMore_HomePage_Benefits';
                	break;
            	case 'epicure':
					eventType = 'TopMenu_KnowMore_HomePage_Epicure';
                	break;
            	case 'redeem':
					eventType = 'TopMenu_KnowMore_HomePage_Redeem';
                	break;
                case 'events':
                    eventType = 'TopMenu_KnowMore_HomePage_Events';
                	break;
                case 'our hotels':
					eventType = 'TopMenu_KnowMore_HomePage_OurHotels';
               		break;
            	case 'help':
					eventType = 'TopMenu_KnowMore_HomePage_Help';
               		break;
            	case 'enrol':
        			eventType = 'TopMenu_Enrollment_HomePage_TICEnrol';
               		break;
            	case 'sign in':
					eventType = 'TopMenu_SignIn_HomePage_SignIn';
               		break;
              	default:
        			eventType = '';
            }
        	if(eventType){
        		addParameterToDataLayerObj(eventType, {});
            }
        });
    });
}