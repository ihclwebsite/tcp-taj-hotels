$(document).ready(function() {
    holidaysIncredibleEscapePackageInclusion()
    if (window.screen.width > 992) {
        var count = $(".package-inclusion-title-content").children().length;
        var width = (100 / count) - .5;
        $('.package-inclusion-li').css('width', width + "%");
    }

});

function holidaysIncredibleEscapePackageInclusion() {
    if ($('.package-inclusion-li').length < 5) {
        $('.package-inclusion-show-more').remove();
    } else {
        $('.package-inclusion-show-more').click(function() {
            $('.package-inclusion-show-more').toggleClass('cm-hide');
            $('.package-inclusion-li').toggleClass('show-all-package-inclusion');
        });
    }
}
