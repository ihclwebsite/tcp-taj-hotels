$(document).ready(function(){

    $('.offer-hotel-details-cnt').each(function() {
        $(this).init_pagination()
    });

    $( ".package-hotel-card-show-more" ).click(
        function() {
            if ( deviceDetector.checkDevice() == "small" ) {
                $( this ).prev().find( ".packages-hotelCard-container-outer-wrap" ).each( function() {
                    $( this ).css( "display", "block" );
                } );
                $( this ).hide();
            } else {
                var outerContainer = $( this ).prev().find( ".package-offer-details-card-wrp" );
                outerContainer.attr( "id", "package-hotel-card-show-cnt" );
                var heightOfWrp = outerContainer.height();
                for ( i = 0; i < 4; i++ ) {
                    $( "#package-hotel-card-show-cnt .packages-hotelCard-container-outer-wrap:first-child" ).clone().appendTo( "#package-hotel-card-show-cnt" );
                }
                $( this ).hide();
                outerContainer.parent().css( "overflow-y", "scroll" ).css( "height", heightOfWrp );
                outerContainer.attr( "id", "" );
            }
        } );

});

$.fn.init_pagination = function()

{	

    var element = this;

    var card = element.find('.hotelCard-container-outer-wrap');

    var list_size = element.find('.hotelCard-container-outer-wrap').length;


    paginate_interval=6;

    current_offset=paginate_interval;


    if(list_size < paginate_interval)

        {

        element.siblings('.package-hotel-card-show-more').hide()

        }

    else

        {


        element.closest('.package-hotel-card-show-more').show();

        $(card).not(':lt('+current_offset+')').hide();

        }   


    $('.participating-hotels-show-more').click(function()

            {

            current_offset=(current_offset+paginate_interval < list_size) ? current_offset+paginate_interval :(list_size)

            $('.offer-hotel-details-wrp .offer-hotel-details-cnt .row .hotelCard-container-outer-wrap:lt('+current_offset+')').show()

            if(current_offset==list_size)

                        {

                                $(this).hide();

                        }

            });

}