function onOfferSelection(navPath, offerRateCode, offerRoundTheYear, offerTitle, noOfNights, offerStartDate,
        offerEndDate, comparableOfferRateCode) {
    try {

        // offer details functionality
        var ROOMS_PATH = "";
        var nights = '';
        var startsFrom = '';
        var endsOn = '';
        var today = moment().format('MMM Do YY');
        var tomorrow = '';
        var dayAfterTomorrow = '';
        var hotelPath = $("[data-hotel-path]").data("hotel-path");

        if ($('.cm-page-container').hasClass('ama-theme')) {
            ROOMS_PATH = "accommodations.html";
        } else {
            ROOMS_PATH = "rooms-and-suites.html";
        }

        if (noOfNights && noOfNights != "" && noOfNights != '0') {
            nights = noOfNights;
        } else {
            nights = 1;
        }
        // override default t+15 booking date for custom start and end dates and adding nights
        if (offerRateCode && !offerRoundTheYear) {
            if (comparableOfferRateCode) {
                offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
            }
            if (offerStartDate && offerEndDate) {
                startsFrom = moment(offerStartDate).format('MMM Do YY');
                endsOn = moment(offerEndDate).format('MMM Do YY');
                if (moment(startsFrom, 'MMM Do YY').isSameOrBefore(moment(today, 'MMM Do YY'))
                        && moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }
            } else if (!offerStartDate && offerEndDate) {
                endsOn = moment(offerEndDate).format('MMM Do YY');
                if (moment(today, 'MMM Do YY').isSameOrBefore(moment(endsOn, 'MMM Do YY'))) {
                    tomorrow = moment().add(1, 'days').format('D/MM/YYYY');
                    dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format("D/MM/YYYY");
                }

                // default t+15 booking dates and adding nights
            } else {
                tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
                dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
            }

            // round the year offer with t+15 dates and nights
        } else {
            tomorrow = moment().add(14, 'days').format('D/MM/YYYY');
            dayAfterTomorrow = moment(tomorrow, "D/MM/YYYY").add(parseInt(nights), 'days').format('D/MM/YYYY');
        }
        if (hotelPath) {
            navPath = hotelPath.replace(".html", "");
            navPath = navPath + ROOMS_PATH;
            navPath = updateQueryString("overrideSessionDates", "true", navPath);
            navPath = updateQueryString("from", tomorrow, navPath);
            navPath = updateQueryString("to", dayAfterTomorrow, navPath);
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }

        // creating the URL for the button
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if ((!navPath.includes("http://") && navPath.includes("http:/"))
                || (!navPath.includes("https://") && navPath.includes("https:/"))) {
            navPath = navPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        window.location.href = navPath;
    } catch (err) {
        console.error('error caught in function onOfferSelection');
        console.error(err);
    }
}

function onOfferViewDetailsSelection(offerDetailsPath, offerRateCode, offerTitle, noOfNights, startsFrom,
        comparableOfferRateCode) {
    try {
        // code replaced to navigate to rooms page instead of view details
        if (offerRateCode) {
            navigateToRooms(offerDetailsPath, offerRateCode, offerTitle, noOfNights, startsFrom,
                    comparableOfferRateCode);
        } else {
            window.location.href = offerDetailsPath;
        }
    } catch (err) {
        console.error('error caught in function onOfferViewDetailsSelection');
        console.error(err);
    }
}

function navigateToRooms(offerDetailsPath, offerRateCode, offerTitle, noOfNights, startsFrom, comparableOfferRateCode) {
    try {
        var hotelPath = offerDetailsPath.split("offers-and-promotions")[0];
        var ROOMS_PATH = "rooms-and-suites.html";
        var navPath = hotelPath.replace(".html", "");
        navPath = navPath + ROOMS_PATH;
        if (navPath != "" && navPath != null) {
            navPath = navPath.replace("//", "/");
        }
        if (offerRateCode) {
            if (comparableOfferRateCode) {
                offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
            }
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }
        navPath = navPath.replace("//", "/");
        if ((!navPath.includes("http://") && navPath.includes("http:/"))
                || (!navPath.includes("https://") && navPath.includes("https:/"))) {
            navPath = navPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        // console.log(navPath)
        window.location.href = navPath;
    } catch (err) {
        console.error('error caught in function navigateToRooms');
        console.error(err);
    }
}
