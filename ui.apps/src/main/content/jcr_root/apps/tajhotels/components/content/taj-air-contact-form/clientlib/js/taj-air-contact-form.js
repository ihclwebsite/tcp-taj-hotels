$(document).ready(function() {

    $("#submit-btn-contact-us").click(function() {
        if (requestFormInputValidation(".taj-air-contact-us-form") && captchaValidation) {
            initiateContactUsServletCall();
            $('.event-confirmation-close-icon , .event-confirmation-close-btn').click(function() {
                location.reload()
            });
            $(".event-error-close-icon").click(function() {
                $(".hotels-event-error-popup").hide();
            });
        }
    });

    $('.hotels-event-error-overlay .event-error-close-icon,.hotels-event-error-overlay .event-confirmation-close-btn ,.hotels-event-confirmation-overlay .event-confirmation-close-icon,.hotels-event-confirmation-overlay .event-confirmation-close-btn').click(
        function() {
            $('.hotels-event-error-popup').hide();
            $(".hotels-event-confirmation-popup").hide();
            $(".cm-page-container").removeClass('prevent-page-scroll');
        });
    $(document)
        .keydown(
                function(e) {
                    if ((($('.hotels-event-error-overlay').is(':visible')) || ($('.hotels-event-confirmation-overlay')
                            .is(':visible')))
                            && (e.which === 27)) {
                        $('.event-confirmation-close-btn:visible').trigger("click");
                    }

                });

    function requestFormInputValidation(wrapper) {
        var requestFormValid = true;
        $(wrapper).find('input.sub-form-mandatory:visible , select.sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
                requestFormValid = false;
            }
        });
        return requestFormValid;
    }
    $(".taj-air-contact-us-form .clear-btn").click(function(){
        $(".taj-air-contact-us-form .contact-us-form-cont .sub-form-input-element").each(function(){
            $(this).removeClass("invalid-input").val("");
        });
    });
});

function initiateContactUsServletCall() {
    $(".spinner-con").show();
    var request = {
        fullName : $("#form-name").val(),
        phoneNumber : $("#form-phone").val(),
        emailId : $("#form-email").val(),
        subject : $("#form-subject").val(),
        country : $("#form-country").val(),
        organization : $("#form-organization").val(),
        comments : $("#form-feedback").val()
    };
    $.ajax({
        type : "GET",
        url : "/bin/contactustajair",
        data : request,
        dataType : "json",
        success : function(a) {
            $(".spinner-con").hide();
            $(".hotels-event-confirmation-popup").show();
            $(".taj-air-contact-us-form .clear-btn").trigger("click");
            $(".cm-page-container").addClass("prevent-page-scroll")
        },
        error : function(a, b) {
            
            $(".spinner-con").hide();
            $(".hotels-event-error-popup").show();
            $(".cm-page-container").addClass("prevent-page-scroll")
        }
    })
}
