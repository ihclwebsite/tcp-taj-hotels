$(document).ready(function() {
    initConfirmationPage();
});

function initConfirmationPage() {
    $('.expan-img').unbind('click').on('click', (function(e) {
        console.log("expan-img");
        e.stopPropagation();
        $('.download-options').toggleClass('visible');
    }));

    $('.checkout-confirmation-layout')
            .click(
                    function(e) {
                        $('.download-options').removeClass('visible');
                        $('.booking-details-heading i').removeClass('inverted');
                        $('.booking-details-con').removeClass('hidden');
                        $(
                                '.guest-details-con, .hotel-details-con, .cart-available-wrp, .cart-selected-info-wrp, .payment-detials-con-mob, .entity-acc-mob-ihclcb')
                                .removeClass('visible');
                        $('.payment-dropdown').toggleClass('payment-dropdown-inverted');
                    });

    $('.booking-details-heading').unbind('click').on('click', (function(e) {
        e.stopPropagation();
        $('.booking-details-heading i').toggleClass('inverted');
        $('.booking-details-con').toggleClass('hidden');
        $('.guest-details-con, .hotel-details-con, .entity-acc-mob-ihclcb').toggleClass('visible');
    }));

    $('.guest-details-con, .hotel-details-con, .payment-detials-con-mob').click(function(e) {
        e.stopPropagation();
    });

    $('.payment-dropdown').unbind('click').on('click', (function(e) {
        e.stopPropagation();
        $(this).toggleClass('payment-dropdown-inverted');
        $('.payment-detials-con-mob').toggleClass('visible');
    }));

    /*
     * var bookingDetials = { "billerDetails": { "billerName": "", "billerPhoneNumber": "", "billerEmail": "",
     * "billerGstNumber": "" }, "guestDetails": { "guestTitle": "Mr.", "guestName": "Ram", "guestEmail":
     * "ram@gmail.com", "guestCountry": "India", "guestPhoneNumber": "917677586138", "guestGSTNumber": "",
     * "guestMembershipNumber": "", "specialRequests": "", "autoEnrollTic": false, "gdpr": false, "airportPickup": false },
     * "flightDetails": null, "paymentsDetails": { "payAtHotel": true, "nameOnCard": "", "cardNumber": "",
     * "expiryMonth": "", "payOnlineNow": false, "payUsingGiftCard": false, "payUsingCerditCard": false,
     * "payableamount": null, "ticpoints": { "redeemTicOrEpicurePoints": false, "ticPlusCreditCard": "10000",
     * "epicurePlusCreditCard": "7000" }, "cardType": "" }, "promotionCode": "AAA10", "bookingId": "95166IC001292",
     * "roomTypeCode": "XTX", "ratePlanCode": "ADVPAY", "ratePlanId": null, "hotelId": "11206", "resStatus": "Initiate",
     * "noOfChilds": "1", "noOfAdults": "2", "hotelChainCode": "9139", "checkInDate": "2019-06-07", "checkOutDate":
     * "2019-06-08", "duration": 0, "noOfUnits": 1, "roomDetailsList": [{ "reservationNumber": "95166IC001292",
     * "roomId": null, "hotelId": "11206", "noOfAudults": 2, "noOfChilds": 1, "ratePlanCode": "ADVPAY", "checkInDate":
     * "2019-06-06", "checkOutDate": "2019-06-08", "roomTypeCode": "XTX", "bookingStatus": "AVAILABLE", "roomCost":
     * "25000", "rateDescription": "some text", "cancellationPolicy": "one night as penality", "petPolicy": "Not
     * allowed" }, { "reservationNumber": "95166IC001292", "roomId": null, "hotelId": "11206", "noOfAudults": 2,
     * "noOfChilds": 1, "ratePlanCode": "ADVPAY", "checkInDate": "2019-06-07", "checkOutDate": "2019-06-10",
     * "roomTypeCode": "XTX", "bookingStatus": "AVAILABLE", "roomCost": "25000", "rateDescription": "some text",
     * "cancellationPolicy": "one night as penality", "petPolicy": "Not allowed" } ], "payableAmount": "50000.0",
     * "appliedCoupon": { "appliedCoupon": "ENT50", "couponAmount": "2000.0" } } //have to add down name where is json
     * coming if (jsonObject.paymentsDetails.payAtHotel == true) { // console.log('inside');
     * $('.payment-details-section').removeClass('payment-details-section-on-online-paytment'); }
     * 
     * var noOfRooms = jsonObject.roomDetailsList.length; var noOfDays = 0; for (i = 0; i < noOfRooms; i++) { fromDate =
     * jsonObject.roomDetailsList[i].checkInDate; toDate = jsonObject.roomDetailsList[i].checkOutDate; noOfDays =
     * noOfDays + ((moment(toDate) - moment(fromDate)) / 1000 / 60 / 60 / 24); } var discountAmount =
     * parseInt(jsonObject.appliedCoupon.couponAmount) * noOfRooms; var totalAmount = parseInt(jsonObject.payableAmount) -
     * discountAmount;
     * 
     * $('.summary-charges-item-value > .total-amount').text(totalAmount); $('.summary-charges-item-name >
     * .room-count').text('( ' + noOfRooms + ' Rooms x ' + noOfDays + ' Nights)');
     */
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb25maXJtYXRpb24tZGV0YWlscy9jb25maXJtYXRpb24tZGV0YWlscy1jb21wLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGluaXRDb25maXJtYXRpb25QYWdlKCkge1xyXG4gICAgJCgnLmV4cGFuLWltZycpLmNsaWNrKGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICQoJy5kb3dubG9hZC1vcHRpb25zJykudG9nZ2xlQ2xhc3MoJ3Zpc2libGUnKTtcclxuICAgIH0pXHJcblxyXG4gICAgJCgnLmNoZWNrb3V0LWNvbmZpcm1hdGlvbi1sYXlvdXQnKS5jbGljayhmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgJCgnLmRvd25sb2FkLW9wdGlvbnMnKS5yZW1vdmVDbGFzcygndmlzaWJsZScpO1xyXG4gICAgICAgICQoJy5ib29raW5nLWRldGFpbHMtaGVhZGluZyBpbWcnKS5yZW1vdmVDbGFzcygnaW52ZXJ0ZWQnKTtcclxuICAgICAgICAkKCcuYm9va2luZy1kZXRhaWxzLWNvbicpLnJlbW92ZUNsYXNzKCdoaWRkZW4nKTtcclxuICAgICAgICAkKCcuZ3Vlc3QtZGV0YWlscy1jb24sIC5ob3RlbC1kZXRhaWxzLWNvbiwgLmNhcnQtYXZhaWxhYmxlLXdycCwgLmNhcnQtc2VsZWN0ZWQtaW5mby13cnAsIC5wYXltZW50LWRldGlhbHMtY29uLW1vYicpLnJlbW92ZUNsYXNzKCd2aXNpYmxlJyk7XHJcbiAgICAgICAgJCgnLnBheW1lbnQtZHJvcGRvd24nKS50b2dnbGVDbGFzcygncGF5bWVudC1kcm9wZG93bi1pbnZlcnRlZCcpO1xyXG4gICAgfSlcclxuXHJcbiAgICAkKCcuYm9va2luZy1kZXRhaWxzLWhlYWRpbmcnKS5jbGljayhmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAkKCcuYm9va2luZy1kZXRhaWxzLWhlYWRpbmcgaW1nJykudG9nZ2xlQ2xhc3MoJ2ludmVydGVkJyk7XHJcbiAgICAgICAgJCgnLmJvb2tpbmctZGV0YWlscy1jb24nKS50b2dnbGVDbGFzcygnaGlkZGVuJyk7XHJcbiAgICAgICAgJCgnLmd1ZXN0LWRldGFpbHMtY29uLCAuaG90ZWwtZGV0YWlscy1jb24nKS50b2dnbGVDbGFzcygndmlzaWJsZScpO1xyXG4gICAgfSlcclxuXHJcbiAgICAkKCcuZ3Vlc3QtZGV0YWlscy1jb24sIC5ob3RlbC1kZXRhaWxzLWNvbiwgLnBheW1lbnQtZGV0aWFscy1jb24tbW9iJykuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICB9KVxyXG5cclxuICAgICQoJy5wYXltZW50LWRyb3Bkb3duJykuY2xpY2soZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygncGF5bWVudC1kcm9wZG93bi1pbnZlcnRlZCcpO1xyXG4gICAgICAgICQoJy5wYXltZW50LWRldGlhbHMtY29uLW1vYicpLnRvZ2dsZUNsYXNzKCd2aXNpYmxlJyk7XHJcbiAgICB9KVxyXG5cclxuICAgIHZhciBib29raW5nRGV0aWFscyA9IHtcclxuICAgICAgICBcImJpbGxlckRldGFpbHNcIjoge1xyXG4gICAgICAgICAgICBcImJpbGxlck5hbWVcIjogXCJcIixcclxuICAgICAgICAgICAgXCJiaWxsZXJQaG9uZU51bWJlclwiOiBcIlwiLFxyXG4gICAgICAgICAgICBcImJpbGxlckVtYWlsXCI6IFwiXCIsXHJcbiAgICAgICAgICAgIFwiYmlsbGVyR3N0TnVtYmVyXCI6IFwiXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIFwiZ3Vlc3REZXRhaWxzXCI6IHtcclxuICAgICAgICAgICAgXCJndWVzdFRpdGxlXCI6IFwiTXIuXCIsXHJcbiAgICAgICAgICAgIFwiZ3Vlc3ROYW1lXCI6IFwiUmFtXCIsXHJcbiAgICAgICAgICAgIFwiZ3Vlc3RFbWFpbFwiOiBcInJhbUBnbWFpbC5jb21cIixcclxuICAgICAgICAgICAgXCJndWVzdENvdW50cnlcIjogXCJJbmRpYVwiLFxyXG4gICAgICAgICAgICBcImd1ZXN0UGhvbmVOdW1iZXJcIjogXCI5MTc2Nzc1ODYxMzhcIixcclxuICAgICAgICAgICAgXCJndWVzdEdTVE51bWJlclwiOiBcIlwiLFxyXG4gICAgICAgICAgICBcImd1ZXN0TWVtYmVyc2hpcE51bWJlclwiOiBcIlwiLFxyXG4gICAgICAgICAgICBcInNwZWNpYWxSZXF1ZXN0c1wiOiBcIlwiLFxyXG4gICAgICAgICAgICBcImF1dG9FbnJvbGxUaWNcIjogZmFsc2UsXHJcbiAgICAgICAgICAgIFwiZ2RwclwiOiBmYWxzZSxcclxuICAgICAgICAgICAgXCJhaXJwb3J0UGlja3VwXCI6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBcImZsaWdodERldGFpbHNcIjogbnVsbCxcclxuICAgICAgICBcInBheW1lbnRzRGV0YWlsc1wiOiB7XHJcbiAgICAgICAgICAgIFwicGF5QXRIb3RlbFwiOiBmYWxzZSxcclxuICAgICAgICAgICAgXCJuYW1lT25DYXJkXCI6IFwiXCIsXHJcbiAgICAgICAgICAgIFwiY2FyZE51bWJlclwiOiBcIlwiLFxyXG4gICAgICAgICAgICBcImV4cGlyeU1vbnRoXCI6IFwiXCIsXHJcbiAgICAgICAgICAgIFwicGF5T25saW5lTm93XCI6IHRydWUsXHJcbiAgICAgICAgICAgIFwicGF5VXNpbmdHaWZ0Q2FyZFwiOiBmYWxzZSxcclxuICAgICAgICAgICAgXCJwYXlVc2luZ0NlcmRpdENhcmRcIjogZmFsc2UsXHJcbiAgICAgICAgICAgIFwicGF5YWJsZWFtb3VudFwiOiBudWxsLFxyXG4gICAgICAgICAgICBcInRpY3BvaW50c1wiOiB7XHJcbiAgICAgICAgICAgICAgICBcInJlZGVlbVRpY09yRXBpY3VyZVBvaW50c1wiOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIFwidGljUGx1c0NyZWRpdENhcmRcIjogXCIxMDAwMFwiLFxyXG4gICAgICAgICAgICAgICAgXCJlcGljdXJlUGx1c0NyZWRpdENhcmRcIjogXCI3MDAwXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgXCJjYXJkVHlwZVwiOiBcIlwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBcInByb21vdGlvbkNvZGVcIjogXCJBQUExMFwiLFxyXG4gICAgICAgIFwiYm9va2luZ0lkXCI6IFwiOTUxNjZJQzAwMTI5MlwiLFxyXG4gICAgICAgIFwicm9vbVR5cGVDb2RlXCI6IFwiWFRYXCIsXHJcbiAgICAgICAgXCJyYXRlUGxhbkNvZGVcIjogXCJBRFZQQVlcIixcclxuICAgICAgICBcInJhdGVQbGFuSWRcIjogbnVsbCxcclxuICAgICAgICBcImhvdGVsSWRcIjogXCIxMTIwNlwiLFxyXG4gICAgICAgIFwicmVzU3RhdHVzXCI6IFwiSW5pdGlhdGVcIixcclxuICAgICAgICBcIm5vT2ZDaGlsZHNcIjogXCIxXCIsXHJcbiAgICAgICAgXCJub09mQWR1bHRzXCI6IFwiMlwiLFxyXG4gICAgICAgIFwiaG90ZWxDaGFpbkNvZGVcIjogXCI5MTM5XCIsXHJcbiAgICAgICAgXCJjaGVja0luRGF0ZVwiOiBcIjIwMTktMDYtMDdcIixcclxuICAgICAgICBcImNoZWNrT3V0RGF0ZVwiOiBcIjIwMTktMDYtMDhcIixcclxuICAgICAgICBcImR1cmF0aW9uXCI6IDAsXHJcbiAgICAgICAgXCJub09mVW5pdHNcIjogMSxcclxuICAgICAgICBcInJvb21EZXRhaWxzTGlzdFwiOiBbe1xyXG4gICAgICAgICAgICAgICAgXCJyZXNlcnZhdGlvbk51bWJlclwiOiBcIjk1MTY2SUMwMDEyOTJcIixcclxuICAgICAgICAgICAgICAgIFwicm9vbUlkXCI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBcImhvdGVsSWRcIjogXCIxMTIwNlwiLFxyXG4gICAgICAgICAgICAgICAgXCJub09mQXVkdWx0c1wiOiAyLFxyXG4gICAgICAgICAgICAgICAgXCJub09mQ2hpbGRzXCI6IDEsXHJcbiAgICAgICAgICAgICAgICBcInJhdGVQbGFuQ29kZVwiOiBcIkFEVlBBWVwiLFxyXG4gICAgICAgICAgICAgICAgXCJjaGVja0luRGF0ZVwiOiBcIjIwMTktMDYtMDZcIixcclxuICAgICAgICAgICAgICAgIFwiY2hlY2tPdXREYXRlXCI6IFwiMjAxOS0wNi0wOFwiLFxyXG4gICAgICAgICAgICAgICAgXCJyb29tVHlwZUNvZGVcIjogXCJYVFhcIixcclxuICAgICAgICAgICAgICAgIFwiYm9va2luZ1N0YXR1c1wiOiBcIkFWQUlMQUJMRVwiLFxyXG4gICAgICAgICAgICAgICAgXCJyb29tQ29zdFwiOiBcIjI1MDAwXCIsXHJcbiAgICAgICAgICAgICAgICBcInJhdGVEZXNjcmlwdGlvblwiOiBcInNvbWUgdGV4dFwiLFxyXG4gICAgICAgICAgICAgICAgXCJjYW5jZWxsYXRpb25Qb2xpY3lcIjogXCJvbmUgbmlnaHQgYXMgcGVuYWxpdHlcIixcclxuICAgICAgICAgICAgICAgIFwicGV0UG9saWN5XCI6IFwiTm90IGFsbG93ZWRcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBcInJlc2VydmF0aW9uTnVtYmVyXCI6IFwiOTUxNjZJQzAwMTI5MlwiLFxyXG4gICAgICAgICAgICAgICAgXCJyb29tSWRcIjogbnVsbCxcclxuICAgICAgICAgICAgICAgIFwiaG90ZWxJZFwiOiBcIjExMjA2XCIsXHJcbiAgICAgICAgICAgICAgICBcIm5vT2ZBdWR1bHRzXCI6IDIsXHJcbiAgICAgICAgICAgICAgICBcIm5vT2ZDaGlsZHNcIjogMSxcclxuICAgICAgICAgICAgICAgIFwicmF0ZVBsYW5Db2RlXCI6IFwiQURWUEFZXCIsXHJcbiAgICAgICAgICAgICAgICBcImNoZWNrSW5EYXRlXCI6IFwiMjAxOS0wNi0wN1wiLFxyXG4gICAgICAgICAgICAgICAgXCJjaGVja091dERhdGVcIjogXCIyMDE5LTA2LTEwXCIsXHJcbiAgICAgICAgICAgICAgICBcInJvb21UeXBlQ29kZVwiOiBcIlhUWFwiLFxyXG4gICAgICAgICAgICAgICAgXCJib29raW5nU3RhdHVzXCI6IFwiQVZBSUxBQkxFXCIsXHJcbiAgICAgICAgICAgICAgICBcInJvb21Db3N0XCI6IFwiMjUwMDBcIixcclxuICAgICAgICAgICAgICAgIFwicmF0ZURlc2NyaXB0aW9uXCI6IFwic29tZSB0ZXh0XCIsXHJcbiAgICAgICAgICAgICAgICBcImNhbmNlbGxhdGlvblBvbGljeVwiOiBcIm9uZSBuaWdodCBhcyBwZW5hbGl0eVwiLFxyXG4gICAgICAgICAgICAgICAgXCJwZXRQb2xpY3lcIjogXCJOb3QgYWxsb3dlZFwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdLFxyXG4gICAgICAgIFwicGF5YWJsZUFtb3VudFwiOiBcIjUwMDAwLjBcIixcclxuICAgICAgICBcImFwcGxpZWRDb3Vwb25cIjoge1xyXG4gICAgICAgICAgICBcImFwcGxpZWRDb3Vwb25cIjogXCJFTlQ1MFwiLFxyXG4gICAgICAgICAgICBcImNvdXBvbkFtb3VudFwiOiBcIjIwMDAuMFwiXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChib29raW5nRGV0aWFscy5wYXltZW50c0RldGFpbHMucGF5QXRIb3RlbCA9PSB0cnVlKSB7XHJcbiAgICAgICAgJCgnLnBheW1lbnQtZGV0YWlscy1zZWN0aW9uJykucmVtb3ZlQ2xhc3MoJ3BheW1lbnQtZGV0YWlscy1zZWN0aW9uLW9uLW9ubGluZS1wYXl0bWVudCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBub09mUm9vbXMgPSBib29raW5nRGV0aWFscy5yb29tRGV0YWlsc0xpc3QubGVuZ3RoO1xyXG4gICAgdmFyIG5vT2ZEYXlzID0gMDtcclxuICAgIGZvciAoaSA9IDA7IGkgPCBub09mUm9vbXM7IGkrKykge1xyXG4gICAgICAgIGZyb21EYXRlID0gYm9va2luZ0RldGlhbHMucm9vbURldGFpbHNMaXN0W2ldLmNoZWNrSW5EYXRlO1xyXG4gICAgICAgIHRvRGF0ZSA9IGJvb2tpbmdEZXRpYWxzLnJvb21EZXRhaWxzTGlzdFtpXS5jaGVja091dERhdGU7XHJcbiAgICAgICAgbm9PZkRheXMgPSBub09mRGF5cyArICgobW9tZW50KHRvRGF0ZSkgLSBtb21lbnQoZnJvbURhdGUpKSAvIDEwMDAgLyA2MCAvIDYwIC8gMjQpO1xyXG4gICAgfVxyXG4gICAgdmFyIGRpc2NvdW50QW1vdW50ID0gcGFyc2VJbnQoYm9va2luZ0RldGlhbHMuYXBwbGllZENvdXBvbi5jb3Vwb25BbW91bnQpICogbm9PZlJvb21zO1xyXG4gICAgdmFyIHRvdGFsQW1vdW50ID0gcGFyc2VJbnQoYm9va2luZ0RldGlhbHMucGF5YWJsZUFtb3VudCkgLSBkaXNjb3VudEFtb3VudDtcclxuXHJcbiAgICAkKCcuc3VtbWFyeS1jaGFyZ2VzLWl0ZW0tdmFsdWUgPiAudG90YWwtYW1vdW50JykudGV4dCh0b3RhbEFtb3VudCk7XHJcbiAgICAkKCcuc3VtbWFyeS1jaGFyZ2VzLWl0ZW0tbmFtZSA+IC5yb29tLWNvdW50JykudGV4dCgnKCAnICsgbm9PZlJvb21zICsgJyBSb29tcyB4ICcgKyBub09mRGF5cyArICcgTmlnaHRzKScpO1xyXG59XHJcbiJdLCJmaWxlIjoibWFya3VwL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLWRldGFpbHMvY29uZmlybWF0aW9uLWRldGFpbHMtY29tcC5qcyJ9
