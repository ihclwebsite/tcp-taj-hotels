$.fn.cmToggleText = function(options) {

	var defaultVal = {
		charLimit : 580,
		showVal : "Show More",
		hideVal : "Show Less",
		ellipsisText : "..."
	};
	options = $.extend({}, defaultVal, options);
	var charLimit = options.charLimit;
	var content = this.html();
	if (content != undefined) {
		if (content.length > options.charLimit) {
			var show = content.substr(0, options.charLimit);
			var hide = content.substr(options.charLimit - 1);

			var html1 = '<span>' + show + '</span><span>'
					+ options.ellipsisText
					+ '</span><span class="show-more-ell cm-view-txt">'
					+ options.showVal + '</span>'

			this.html(html1);
		}
	}

	var self = this;

	this.on('click', '.show-more-ell', function() {
		self.html(content + '<span class="show-less cm-view-txt">'
				+ options.hideVal + '</span>');
	});

	this.on('click', '.show-less', function() {
		self.html(content.substr(0, options.charLimit) + '</span><span>'
				+ options.ellipsisText
				+ '</span><span class="show-more-ell cm-view-txt">'
				+ options.showVal + '</span>');
	});
};