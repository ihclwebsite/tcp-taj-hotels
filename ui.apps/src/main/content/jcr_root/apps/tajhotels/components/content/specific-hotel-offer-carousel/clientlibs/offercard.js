function onOfferSelection(offerDetailsPath, offerRateCode, offerTitle) {
    var hotelPath = $("[data-hotel-path]").data("hotel-path");
    if (hotelPath) {
        var ROOMS_PATH = "/rooms-and-suites.html";
        var navPath = hotelPath.replace(".html", "");
        navPath = navPath + ROOMS_PATH;
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if (offerRateCode) {
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }
        navPath = navPath.replace("//", "/");
         if((!navPath.includes("http://")&& navPath.includes("http:/")) || (!navPath.includes("https://")&& navPath.includes("https:/"))){
             navPath=navPath.replace("http:/", "http://").replace("https:/", "https://");
         }
         window.location.href = navPath;
     } else {
         offerDetailsPath = offerDetailsPath.replace("//", "/");
         if((!offerDetailsPath.includes("http://")&& offerDetailsPath.includes("http:/")) || (!offerDetailsPath.includes("https://")&& offerDetailsPath.includes("https:/"))){
             offerDetailsPath=offerDetailsPath.replace("http:/", "http://").replace("https:/", "https://");
         }
        window.location.href = offerDetailsPath;
    }
}
