function showLink(element) {
    var parentOfCurrentSelection = $(element).parent();
    $(parentOfCurrentSelection).find("#addLink").show();
    $(parentOfCurrentSelection).find("#subHeader").hide();
    event.preventDefault();
};

function showSubHeader(element) {
    var parentOfCurrentSelection = $(element).parent();
    $(parentOfCurrentSelection).find("#subHeader").show();
    $(parentOfCurrentSelection).find("#addLink").hide();
    event.preventDefault();
};