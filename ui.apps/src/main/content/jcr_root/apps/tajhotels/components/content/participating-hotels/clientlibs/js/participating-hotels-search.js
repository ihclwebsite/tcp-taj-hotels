$(document).ready(function(){
	
	//participating-hotels-search
	
	 if($('.cm-page-container').hasClass('ama-theme')){
		 $('.participating-hotels-search').hide();
	 }
    var searchFilteredLength=0;
    var $participatingHotelsRow = $(".participating-hotels .row.participating-hotels-row");
    var $participatingHotelsFilterRow = $participatingHotelsRow.clone("true");
    $participatingHotelsRow.after($participatingHotelsFilterRow);
    $participatingHotelsFilterRow.hide();
    $participatingHotelsFilterRow
    	.removeClass("participating-hotels-row")
    	.addClass("participating-hotels-filter-row");    	
    $(".search-input-section input").on("keyup", function() {
        disable_pagination()
        var value = $(this).val().toLowerCase();        
        $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
        searchFilteredLength= $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap:visible").length;
        if(searchFilteredLength==0)
        {

            resultsWarningText(false);
        }else{
        	resultsWarningText(true);
        }
        if(value=='')
        {   
        init_pagination();
        }
       
  });
    
    $(".hotelTypes").on('change',function(){
        var value = $(this).val().toLowerCase();

        var $filteredHotels;
        if(value=="choose"){
        	$(".participating-hotels .row.participating-hotels-filter-row .hotelCard-container-outer-wrap").addClass("filtered-hotel-card");
        }else{
        	$(".participating-hotels .row.participating-hotels-filter-row .hotelCard-container-outer-wrap").each(function() {                
                $(this).toggleClass("filtered-hotel-card",$(this).data('hoteltypes').indexOf(value) > -1)
            });
        }        
        $filteredHotels = $(".participating-hotels .row.participating-hotels-filter-row .hotelCard-container-outer-wrap.filtered-hotel-card").clone("true");
        $participatingHotelsRow.empty().append($filteredHotels);
        if($(".sortTypes").val().toLowerCase() != "choose"){
        	sortParticipatingHotels();
        }else{
        	init_pagination();
            $(".search-input-section input").trigger("keyup");
        }
    });
    
    $(".sortTypes").on('change',function(){
    	sortParticipatingHotels();
    });
    
    $('#search-filter-section-dropdown').selectBoxIt();
    $('#search-sort-section-dropdown').selectBoxIt();
});

function sortParticipatingHotels(){
	var value = $(".sortTypes").val().toLowerCase();

	var $sortedHotels;
	if(value=="choose"){
			$(".hotelTypes").trigger("change");
    		$sortedHotels = $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap.filtered-hotel-card");
    	if($sortedHotels.length == 0){
    		$sortedHotels = $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap");
    	}
    }else if(value=="reviewrating"){
    	var reviewRatings = []; 
    	var $ratingContainer = $('.rating-container');
    	for(var i = 0;i<$ratingContainer.length;i++)
    	{
    		var reviewRating = $($ratingContainer[i]).find('.rating').text();
    		if($.inArray(reviewRating, reviewRatings) === -1) reviewRatings.push(reviewRating);
    	}
    	reviewRatings = reviewRatings.sort().reverse();
    	
    	var participatedHotelCards = $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap.filtered-hotel-card");
    	
    	if(participatedHotelCards.length == 0){
    		participatedHotelCards = $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap");
    	}
    	
    	for(var r=0;r<reviewRatings.length;r++){
    		participatedHotelCards.each(function() {
        		if(reviewRatings[r] == $(this).find('.rating').text()){
        			if($sortedHotels){
        				$sortedHotels = $sortedHotels + $(this).context.outerHTML;
        			}else{
        				$sortedHotels = $(this).context.outerHTML;
        			}
        		}
        	});
    	}
	}
	if($sortedHotels){
		$(".participating-hotels .row.participating-hotels-row").empty().append($sortedHotels);		
	}
	init_pagination();
    $(".search-input-section input").trigger("keyup"); 

}

function resultsWarningText(isSuccess){	
	var $resultWarningWrapper = $(".results-warning-text-wrapper");
	var $successResult = $(".results-warning-text-wrapper .results-success");
	var $failedResult = $(".results-warning-text-wrapper .results-failed");
	var $resultsWarningValue = $(".results-warning-value");
	var warningResultText = "";
	var searchBarValue = $(".search-input-section input").val();
	var filterByValue  = $(".hotelTypes option:selected").text();		
	if(searchBarValue.length >0){
		searchBarValue = 'for "'+ searchBarValue+'"';
		warningResultText += searchBarValue;
	} 
	if(filterByValue!="Choose" && filterByValue.length > 0){
		filterByValue = ' with filter "'+ filterByValue+'"';
		warningResultText += filterByValue; 
	}
	$resultWarningWrapper.toggle(warningResultText!="");
	$resultsWarningValue.text(warningResultText);	
	$successResult.toggle(isSuccess);
	$failedResult.toggle(!isSuccess);	
}