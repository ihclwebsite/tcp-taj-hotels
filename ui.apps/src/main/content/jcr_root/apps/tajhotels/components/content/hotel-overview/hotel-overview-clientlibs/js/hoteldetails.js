function setGallerySectionHeight(element) {

	var methodName = "setGallerySectionHeight"
	console
			.log("Entry: "
					+ methodName
					+ " at /apps/tajhotels/components/content/hoteldetails/clientlibs/js/hoteldetails.js");
	var overviewSectionHeight = $(".ho-section-wrapper").height();


	$(".gallery-section-wrapper").height(overviewSectionHeight);

	console
			.log("Exit: "
					+ methodName
					+ " at /apps/tajhotels/components/content/hoteldetails/clientlibs/js/hoteldetails.js");
}
