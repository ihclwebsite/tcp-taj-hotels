function setSessionRequestQuoteFilter() {
    var hotelName = $(".mr-hotel-details").data("hotel-name");
    var requestQuoteObj = {};
    requestQuoteObj.hotelName = hotelName;
    dataCache.session.setData('requestQuoteDetails', requestQuoteObj);
}

var $eventCards;
$(document)
        .ready(
                function() {
                    try {
                        $('#sizeDropdown').selectBoxIt();
                        $('#capacityDropdown').selectBoxIt();

                        $('.events-filter-icon-mobile').on('click', function() {
                            $('.events-filter-subsection').css('display', 'block');
                        })

                        $('.events-filter-back-arrow .icon-prev-arrow').click(function() {
                            $('.events-filter-subsection').css('display', 'none');
                        })

                        $('.events-apply-btn')
                                .click(
                                        function() {
                                            $('.events-filter-subsection').css('display', 'none');
                                            if ($('#sizeDropdown').find("option:selected").text() != 'Size'
                                                    || $('#capacityDropdown').find("option:selected").text() != 'Capacity') {
                                                if ($('.events-filter-icon-mobile').html() == '<img src="/content/dam/tajhotels/icons/style-icons/filter-icon.svg" alt  = "filter-icon">') {
                                                    $('.events-filter-icon-mobile')
                                                            .html(
                                                                    '<img src="/content/dam/tajhotels/icons/style-icons/filter-applied.svg" alt  = "filter-applied-icon">')
                                                }
                                            }
                                        });

                        $eventCards = $(".event-card-container .row .events-card-wrap").clone("true");
                        $('.clear-input-icon').click(function(e) {
                            $(this).prev('.hotel-search').val("");
                            $('.clear-input-icon').removeClass('show-clear-input');
                            fetchFilterList();
                        });

                        $("#filterBy").change(function() {
                            fetchFilterList();
                        });

                        $("#capacityDropdownlp").change(function() {
                            fetchFilterForlp();
                        });

                        $("#capacityDropdown").change(function() {
                            fetchFilterList();
                        });

                        $(".searchbar-input.hotel-search").on("keyup", function() {
                            var value = $(this).val().toLowerCase();
                            if (value.length === 0) {
                                $('.clear-input-icon').removeClass('show-clear-input');
                            } else {
                                $('.clear-input-icon').addClass('show-clear-input');
                            }
                            var filteredDest = fetchFilterList();
                        });

                        fetchFilterListFromQueryParam();

                    } catch (error) {
                        console.error("Error in meeting filter ", error);
                    }
                });

function fetchFilterForlp() {

    var capacity = $("#capacityDropdownlp").val();
    var capacityRangeObj = getCapacityRangeValue(capacity);
    var stnum = capacityRangeObj['start'];
    var edNum = capacityRangeObj['end'];
    var $filteredCards = [];
    clearFilteredCards();
    $eventCards.each(function() {
        var eventCapacity = parseInt($(this).data('capacity'));
        if (eventCapacity >= stnum && eventCapacity <= edNum) {
            $filteredCards.push($(this));
        }
    });
    populateFilteredResults($filteredCards);
}

function getCapacityRangeValue(capacity) {
    var startNum = 0;
    var endNum = Number.POSITIVE_INFINITY;
    if (capacity && capacity != "Capacity") {
        var capacityRange = capacity.split("-");
        var startNum = parseInt(capacityRange[0]);
        endNum = capacityRange[1];
        if (endNum == "") {
            endNum = Number.POSITIVE_INFINITY;
        } else {
            endNum = parseInt(capacityRange[1]);
        }
    }
    return {
        'start' : startNum,
        'end' : endNum
    };
}

function fetchFilterList() {

    var searchKey = $('.searchbar-input.hotel-search').val().toLowerCase();
    var filterKey = $('#filterBy').val().toLowerCase();
    if (filterKey == "all") {
        filterKey = "";
    }
    var capacityKey = $('#capacityDropdown').val();
    var capacityRangeObj = getCapacityRangeValue(capacityKey);
    var startNum = capacityRangeObj['start'];
    var endNum = capacityRangeObj['end'];

    var $filteredEventCards = [];
    clearFilteredCards();
    $eventCards.each(function() {
        var eventCardLocation = $(this).data('location');
        var eventCardFilterData = $(this).data('venuetypes');
        var eventCardCapacity = parseInt($(this).data('capacity'));
        if ((eventCardLocation && eventCardLocation.indexOf(searchKey) > -1)
                && (eventCardFilterData && eventCardFilterData.indexOf(filterKey) > -1)
                && (eventCardCapacity >= startNum && eventCardCapacity <= endNum)) {
            $filteredEventCards.push($(this));
        }
    });
    populateFilteredResults($filteredEventCards);
}

function performEventSearch(keyword, destination, hotels) {
    keyFilteredHotels = hotels.filter(function() {
        $(this).toggle($(this).data('location').toLowerCase().indexOf(keyword) > -1);
        return ($(this).data('location').toLowerCase().indexOf(keyword) > -1);
    });
}

function fetchFilterListFromQueryParam() {

    var searchKey = getQueryParameter('searchKey');
    var capacityKey = getQueryParameter('capacity');
    var filterKey = getQueryParameter('filterBy') || '';
    autoPopulateCapacityFilter(capacityKey);

    if (filterKey) {
        $('#filterBy').selectBoxIt();
        $('#filterBy').selectBoxIt('selectOption', filterKey);
    }

    if (filterKey == "all") {
        filterKey = "";
    }
    if (searchKey || capacityKey) {
        $('.clear-input-icon').addClass('show-clear-input')
        if (!searchKey) {
            searchKey = "";
            $('.clear-input-icon').removeClass('show-clear-input');
        }
        searchKey = decodeURIComponent(searchKey);
        $(".searchbar-input.hotel-search").val(searchKey);
        searchKey = searchKey.toLowerCase();

        var capacityRangeObj = getCapacityRangeValue(capacityKey);
        var startNum = capacityRangeObj['start'];
        var endNum = capacityRangeObj['end'];

        var $filteredEventCards = [];
        clearFilteredCards();
        $eventCards.each(function() {
            var eventCardLocation = $(this).data('location');
            var eventCardFilterData = $(this).data('venuetypes');
            var eventCardCapacity = parseInt($(this).data('capacity'));
            if ((eventCardLocation && eventCardLocation.indexOf(searchKey) > -1)
                    && (eventCardFilterData && eventCardFilterData.indexOf(filterKey) > -1)
                    && (eventCardCapacity >= startNum && eventCardCapacity <= endNum)) {
                $filteredEventCards.push($(this));
            }
        });
        populateFilteredResults($filteredEventCards);

    }
}

function clearFilteredCards() {
    $(".event-card-container .row").empty();
    $(".event-card-container").siblings(".jiva-spa-show-more").remove();
}

function populateFilteredResults($filteredEventCards) {
    if ($filteredEventCards == 0) {
        $(".event-card-container .row").html("No Results found.");
    } else {
        $(".event-card-container .row").append($filteredEventCards);
        $(".event-card-container").showMore();
    }
}

function autoPopulateCapacityFilter(capacityKey) {
    if (capacityKey && capacityKey != "Capacity") {
        if (capacityKey.includes("500-")) {
            capacityKey = capacityKey.replace("-", "+");
        }
        $('#capacityDropdown').selectBoxIt('selectOption', capacityKey);
    }

}
