$(document).ready(function() {
    $('.sustainability-card-wrap p').each(function() {
        $(this).cmToggleText({
            charLimit : 350
        })
    });
    if (deviceDetector.checkDevice() == "small") {
        $('.sustainability-card-wrap p').each(function() {
            $(this).cmToggleText({
                charLimit : 200
            })
        });
    }
});

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLVN1c3RhaW5hYmlsaXR5LWNhcmQvY29ycC1TdXN0YWluYWJpbGl0eS1jYXJkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGFib3V0VXNTdXN0YWluYWJpbGl0eUNhcmQoKSB7XHJcbiAgICAkKCBkb2N1bWVudCApLnJlYWR5KCBmdW5jdGlvbigpIHtcclxuICAgICAgICAkKCAnLnN1c3RhaW5hYmlsaXR5LWNhcmQtd3JhcCBwJyApLmVhY2goIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKCB0aGlzICkuY21Ub2dnbGVUZXh0KCB7XHJcbiAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDM1MFxyXG4gICAgICAgICAgICB9IClcclxuICAgICAgICB9ICk7XHJcbiAgICAgICAgaWYgKCBkZXZpY2VEZXRlY3Rvci5jaGVja0RldmljZSgpID09IFwic21hbGxcIiApIHtcclxuICAgICAgICAgICAgJCggJy5zdXN0YWluYWJpbGl0eS1jYXJkLXdyYXAgcCcgKS5lYWNoKCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoIHRoaXMgKS5jbVRvZ2dsZVRleHQoIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDIwMFxyXG4gICAgICAgICAgICAgICAgfSApXHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9ICk7XHJcbn0iXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NvcnAtU3VzdGFpbmFiaWxpdHktY2FyZC9jb3JwLVN1c3RhaW5hYmlsaXR5LWNhcmQuanMifQ==
