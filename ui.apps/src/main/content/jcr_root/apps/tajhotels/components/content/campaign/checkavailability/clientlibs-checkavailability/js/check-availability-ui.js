//initializing with default values
var _globalDateOccupancyCombinationString = fetchDateOccupancyAsQueryString();
$(document).ready(function() {
    if (!$(".cm-page-container").hasClass("ama-theme")) {
        packageSearchCheckAvailability();
    }

    if(window.location.pathname.indexOf('4dtravel') != -1){
        $('.ihclButton button').click(function(e){
            var rateCode = document.getElementById("offerCode").innerHTML;
            var fortnightAway = new Date(Date.now() + 12096e5);
            var fromDate= fortnightAway.getDate()+'/'+(fortnightAway.getMonth() + 1)+ '/' + fortnightAway.getFullYear();
            var fortnightAway2 = new Date(Date.now() + 12966e5);
            var toDate= fortnightAway2.getDate()+'/'+(fortnightAway2.getMonth() + 1 )+ '/' + fortnightAway2.getFullYear();
			var href = e.currentTarget.parentElement.href;
			window.location = href + '?overrideSessionDates=true&from='+fromDate+'&to='+toDate+'&offerRateCode='+rateCode;
            e.stopPropagation();
            e.stopImmediatePropagation();
            return false;
       });

        var fortnightAway = new Date(Date.now() + 12096e5);
        var fortnightAway2 = new Date(Date.now() + 12966e5);
        var fromDate= fortnightAway.getDate()+'/'+(fortnightAway.getMonth() +1 )+ '/' + fortnightAway.getFullYear();
        var toDate= fortnightAway2.getDate()+'/'+ (fortnightAway2.getMonth() + 1)+ '/' + fortnightAway2.getFullYear();
        $('.package-check-in-date input').val(fromDate);
        $('.package-check-out-date input').val(toDate);


        var fromDate = moment().add(14, 'day');
        var toDate = moment().add(15, 'day');
        $('#enquiry-from-date').datepicker('setDate', fromDate['_d']);
        $('#enquiry-to-date').datepicker('setDate', toDate['_d']);

    }


});

function setInitialValForInput() {
    $('#check-availaility-searchbar-input').val("");
    var selectBoxIt2 = $('#packageNoOfRooms').data("selectBoxIt");
    if (selectBoxIt2) {
        selectBoxIt2.selectOption('1');
    }
    disableCheckAvailabilityButton();
}

function packageSearchCheckAvailability() {
    var redirectPath = "";
    var hotelId = "";

    var checkNoOfNights = $('#checkNoOfNights').val();
    var noOfNights = $('#noOfNights').val();
	var fromDateTimestamp = 0;
	var toDateTimestamp = 0;

    PackageSearchDropdownsInit();
    setInitialValForInput();

    var isAuthorable = $('#isAuthorable').val();
    var tommorow = new Date();
    var dayAfterTommorow = new Date();
    var calStartDate = moment(new Date()).add(1, 'days')['_d'];
    var calStartToDate = moment(new Date()).add(2, 'days')['_d'];
    if (isAuthorable != 'true') {
        tommorow = moment(new Date()).add(17, 'days')['_d'];
		
		fromDateTimestamp = new Date(tommorow).getTime();
        // check for the min no of nights criteria
        if(checkNoOfNights && checkNoOfNights == 'true'){
			dayAfterTommorow = moment(new Date()).add(+noOfNights+17, 'days')['_d'];
        } 
        else {
        	dayAfterTommorow = moment(new Date()).add(18, 'days')['_d'];
        }
		toDateTimestamp = new Date(dayAfterTommorow).getTime();
        
        $('.enquiry-from-value').val(moment(tommorow).format("DD/MM/YYYY"));
        $('.enquiry-to-value').val(moment(dayAfterTommorow).format("DD/MM/YYYY"));
    } else {
        var checkinDateString = $('#checkinDate').val();
        var arr = checkinDateString.split('/');
        tommorow.setDate(arr[0]);
        tommorow.setMonth(arr[1] - 1);
        tommorow.setYear(arr[2]);

        var checkoutDateString = $('#checkoutDate').val();
        var arr2 = checkoutDateString.split('/');
        dayAfterTommorow.setDate(arr2[0]);
        dayAfterTommorow.setMonth(arr2[1] - 1);
        dayAfterTommorow.setYear(arr2[2]);
        calStartDate = tommorow < calStartDate ? tommorow : calStartDate;
        calStartToDate = dayAfterTommorow < calStartToDate ? dayAfterTommorow : calStartToDate;
        $('.enquiry-from-value').val(moment(tommorow).format("DD/MM/YYYY"));
        $('.enquiry-to-value').val(moment(dayAfterTommorow).format("DD/MM/YYYY"));

    }
    $('#enquiry-from-date').datepicker({
        startDate : calStartDate
    }).on('changeDate', function(element) {
        if ($(this).hasClass('visible')) {
			fromDateTimestamp = element.date.valueOf();
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD/MM/YYYY");
            var nextDate = moment(minDate).add(1, 'day');
            $('.enquiry-from-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
            if ($('#enquiry-from-date').datepicker("getDate") >= $('#enquiry-to-date').datepicker("getDate")) {
                $('#enquiry-to-date').datepicker('setDate', nextDate['_d']);
                $('.enquiry-to-value').val(nextDate.format("DD/MM/YYYY"));
            }
			handleNoOfNights()
            isSynxisCheck();
        }
    });

    $('#enquiry-from-date').datepicker('setDate', tommorow);

    $('#enquiry-to-date').datepicker({
        startDate : calStartToDate
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
			toDateTimestamp = element.date.valueOf();
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD/MM/YYYY");
            var prevDate = moment(minDate).subtract(1, 'day');
            $('.enquiry-to-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
            if ($('#enquiry-to-date').datepicker("getDate") <= $('#enquiry-from-date').datepicker("getDate")) {
                $('#enquiry-from-date').datepicker('setDate', prevDate['_d']);
                $('.enquiry-from-value').val(prevDate.format("DD/MM/YYYY"));
            }
			handleNoOfNights();
            isSynxisCheck();
        }
    });
    $('#enquiry-to-date').datepicker('setDate', dayAfterTommorow);

    var validateEnquiryElements = function() {
        $('.package-search-form').find('.sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
            }
        });
    }
    // Request quote submit handler
    $('.search-submit-btn').click(function() {
        // updateBookingDetailsInStorage();
        validateEnquiryElements();
        var invalidCheck = $('.package-search-form .invalid-input');
        if (invalidCheck.length) {
            invalidCheck.first().focus();
            return false;
        } else {
            updateGlobalDateOccupancy();
            setCheckAvailaibilityRoomsPath();
            return true;
        }
    });

    $(".jiva-spa-search-list li").click(function() {
        $(this).closest('.jiva-spa-hotel-input').find('.jiva-spa-mand').removeClass("invalid-input");
    });

    $(document).click(function() {
        $(".jiva-spa-date").removeClass("visible");
    });

    $(".jiva-spa-date-section").click(function(e) {
        e.stopPropagation();
    });

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $(".jiva-spa-date").removeClass("visible");
        $(this).next(".jiva-spa-date").addClass("visible");
    })

    var checkAvailabilitySearch = new searchComponent('#check-availaility-searchbar-input',
            '#check-availaility-search-results', '#check-availaility-search-results-destinations',
            '.search-destination', noResultsCallback);

    function noResultsCallback() {
        // toggleCheckAvailable("#", false);
        $('#ca-global-re-direct').attr('href', "#");
    }

    function PackageSearchDropdownsInit() {
        var $PackageRooms = $('#packageNoOfRooms');
        $PackageRooms.selectBoxIt().change(function() {
            var roomsIndex = parseInt($(this).val());
            $('.package-no-of-adults').hide();
            $('.package-no-of-adults:lt(' + roomsIndex + ')').show();
            $('.package-no-of-children').hide();
            $('.package-no-of-children:lt(' + roomsIndex + ')').show();

            isSynxisCheck();
        });
        $('.package-occupancy-count-row select').each(function() {
            $(this).selectBoxIt().change(function() {
                isSynxisCheck();
            });
        });
    }

    function isSynxisCheck() {
        var isSynxis = $("#isSynxis").text().trim();
        if (isSynxis == 'true') {
            updateRedirectPath();
            return true;
        }
    }

    $('#check-availaility-promo-code-input').on('keyup', function() {
        updateRedirectPath();
    });

    $('#check-availaility-rate-code-input').on('keyup', function() {
        updateRedirectPath();
    });

    $('#check-availaility-coupon-code-input').on('keyup', function() {
        updateRedirectPath();
    });

    $('#check-availaility-search-results').on("click", '.search-result-item', function() {
        enableCheckAvailabilityButton();
        var offerCodeFromId = $('#offerCode').text().trim();
        var codelabel = $('#codelabel').text().trim();

        redirectPath = $(this).attr('data-redirect-path');
        hotelId = $(this).attr('data-hotel-id').trim();
        if (!isSynxisCheck()) {
            if (offerCodeFromId) {
                var rateTab = "";
                if (codelabel === "promoCode") {
                    rateTab = "&rateTab=PROMOCODE";
                }
                redirectPath = redirectPath + "?" + codelabel + "=" + offerCodeFromId + rateTab;
                if($('#voucherRedemption').val()){
					redirectPath = redirectPath +"&voucherRedemption=" +$('#voucherRedemption').val();
                }
                $('#ca-global-re-direct').attr('href', redirectPath);
            }
        }
        $('#check-availaility-search-results').css("display", "none");
    })

    // function toggleCheckAvailable(redirectPath, redirectStatus) {
    // $('#ca-global-re-direct').attr('href', redirectPath).toggleClass('re-direct-disabled', !redirectStatus);
    // }

    function updateRedirectPath() {
        var checkInDate = moment($('#enquiry-from-date').datepicker("getDate")).format("YYYY-MM-DD");
        var checkOutDate = moment($('#enquiry-to-date').datepicker("getDate")).format("YYYY-MM-DD");
        var numberOfRooms = $('#packageNoOfRooms').val();
        var adults = "";
        var children = "";
        var isPromoCode = $("#promocode").text().trim();
        var promoCode = "";
        if (isPromoCode == 'true') {
            promoCode = $("#check-availaility-promo-code-input").val().trim();
        }
        for (var i = 0; i < numberOfRooms; i++) {
            var noOfAdults = $("#packageNoOfAdults" + (i + 1)).val().trim();
            var noOfChildren = $("#packageNoOfChildren" + (i + 1)).val().trim();
            adults = adults + noOfAdults + ",";
            children = children + noOfChildren + ",";
        }
        adults = adults.substring(0, (adults.length - 1));
        children = children.substring(0, (children.length - 1));
        var adult = adults;
        var arrive = checkInDate;
        var chain = $("#chain").text().trim();
        var child = children;
        var currency = $("#currency").text().trim();
        var depart = checkOutDate;
        var hotel = hotelId;
        var level = $("#level").text().trim();
        var locale = $("#locale").text().trim();
        var rooms = numberOfRooms;
        var sbe_ri = $("#sberi").text().trim();

        var isCorporateAccess = $("#isCorporateAccess").text().trim();
        var preOfferCode = $('#offerCode').text().trim();

        var couponCode= $('#check-availaility-coupon-code-input') && $('#check-availaility-coupon-code-input').val() ?
            $('#check-availaility-coupon-code-input').val().trim() : '';

        var synxisLink = "https://be.synxis.com/";
        redirectPath = synxisLink + "?adult=" + adult + "&arrive=" + arrive + "&chain=" + chain + "&child=" + child
                + "&currency=" + currency + "&depart=" + depart + "&hotel=" + hotel + "&level=" + level + "&locale="
                + locale + "&rooms=" + rooms + "&sbe_ri=" + sbe_ri;

        if(couponCode){
            redirectPath += "&coupon="+couponCode;
        }

        if (isCorporateAccess == 'true') {
            var rateCode = $("#check-availaility-rate-code-input").val().trim();
            if (rateCode && rateCode != '') {
                redirectPath = redirectPath + "&rate=" + rateCode;
            }
            if (promoCode && promoCode != '') {
                redirectPath = redirectPath + "&promo=" + promoCode;
            }
        } else {
            var paramName = $('#paramName').text().trim();
            if (paramName && paramName != '') {
                if (preOfferCode && preOfferCode != '') {
                    redirectPath = redirectPath + "&" + paramName + "=" + preOfferCode;
                }
            } else {
                if (preOfferCode && preOfferCode != '') {
                    redirectPath = redirectPath + "&promo=" + preOfferCode;
                }
            }
        }

        $('#ca-global-re-direct').attr('href', redirectPath);
        $('#ca-global-re-direct').attr('target', "_blank");
    }

    function updateGlobalDateOccupancy() {
        var checkInDate = moment($('#enquiry-from-date').datepicker("getDate")).format('DD/MM/YYYY');
        var checkOutDate = moment($('#enquiry-to-date').datepicker("getDate")).format('DD/MM/YYYY');
        var numberOfRooms = $('#packageNoOfRooms').val();
        var selectedRoomOptions = [];
        for (var i = 0; i < numberOfRooms; i++) {
            var packageNoOfAdults = $('#packageNoOfAdults' + (i + 1)).val();
            var packageNoOfChildren = $('#packageNoOfChildren' + (i + 1)).val();
            var roomInfo = {
                adults : packageNoOfAdults,
                children : packageNoOfChildren,
            }
            selectedRoomOptions.push(roomInfo);
        }
        var adults = '';
        var children = '';
        if (selectedRoomOptions) {
            selectedRoomOptions.forEach(function(roomObj, index) {
                adults += roomObj.adults + ",";
                children += roomObj.children + ",";
            });
            adults = adults.substring(0, adults.length - 1);
            children = children.substring(0, children.length - 1);
        }
        _globalDateOccupancyCombinationString = "from=" + checkInDate + "&to=" + checkOutDate + "&rooms="
                + numberOfRooms + "&adults=" + adults + "&children=" + children;

        addCheckAvailabilityFromGlobalOfferDataToDataLayer(checkInDate, checkOutDate, numberOfRooms, adults, children);
    }

    // handle minimum no of nights stay
	function handleNoOfNights() {
		if(checkNoOfNights && checkNoOfNights == 'true') {
			if((toDateTimestamp - fromDateTimestamp) <= (+noOfNights-1)*24*60*60*1000){
				disableCheckAvailabilityButton();
				handleLessThenNoOfNights(true);
			} else {
				if($('#check-availaility-searchbar-input').val()) {
					enableCheckAvailabilityButton();
				}
				handleLessThenNoOfNights(false);
			}
		}
	}
	
	function handleLessThenNoOfNights(show) {
		if(show) {
			$('.enquiry-to-value').siblings('.sub-form-input-warning')
				.text('Please select checkout date atleast ' + noOfNights + ' days after checkin date').show();
		} else {
			$('.enquiry-to-value').siblings('.sub-form-input-warning').text('').hide();
		}
	}

}

function disableCheckAvailabilityButton() {
    $('.search-submit-btn').prop('disabled', true);
    $('.search-submit-btn').css('background-image', 'none');
}

function enableCheckAvailabilityButton() {
    $('.search-submit-btn').prop('disabled', false);
    $('.search-submit-btn').css('background-image',
            'linear-gradient(to top, rgb(123, 85, 25), rgb(170, 121, 56) 52%, rgb(210, 152, 81))');
}

function setCheckAvailaibilityRoomsPath() {
    try {
        var hrefValue = $('#ca-global-re-direct').attr('href');
        if (hrefValue.includes("?")) {
            if (hrefValue.charAt(hrefValue.length - 1) === "?") {
                hrefValue += _globalDateOccupancyCombinationString;
                hrefValue += "&overrideSessionDates=true";
            } else {
                hrefValue += "&" + _globalDateOccupancyCombinationString;
                hrefValue += "&overrideSessionDates=true";
            }
        } else {
            hrefValue += "?" + _globalDateOccupancyCombinationString;
            hrefValue += "&overrideSessionDates=true";
        }

        $('#ca-global-re-direct').attr('href', hrefValue);
    } catch (error) {
        console.error("Error occured while setting the dateOccupancy Combination as query string");
    }
}


function addCheckAvailabilityFromGlobalOfferDataToDataLayer(checkInDate, checkOutDate, numberOfRooms, adults, children) {
    if(location.pathname.includes('/en-in/offers')) {
        try {
            //updated for global data layer
            var offerDetails = $('.offerdetails');
            var offerName = offerDetails.find('.cm-header-label').text();
            var offerCode = offerDetails.find('.offers-room-container').data('offer-rate-code');
            var offerValidity = offerDetails.find('.validity-container .validity-content').text().trim();
            var offerCategory = offerDetails.find('.offers-room-container').data('offer-category');
            var eventName =  offerName.split(' ').join('') + '_Offers&Promotions_Booking_OffersPage_CheckAvailibility';
            var offerObj = {};
			offerObj.checkInDate = checkInDate; 
            offerObj.checkOutDate = checkOutDate;
            offerObj.numberOfRooms = numberOfRooms;
            offerObj.adults = adults;
            offerObj.children = children;
            offerObj.hotelName = $('#check-availaility-searchbar-input').val();

            offerObj.offerName = offerName;
            offerObj.offerCode = offerCode;
			offerObj.offerValidity = offerValidity;
			offerObj.offerCategory = offerCategory;

            addParameterToDataLayerObj(eventName, offerObj);
        }
        catch(err){
			consolelog('error in adding to datalayer');
        }
    }
}