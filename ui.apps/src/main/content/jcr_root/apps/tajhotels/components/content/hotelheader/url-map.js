use(function() {
    if (this.reqUrl) {
        var res1=pageManager.getPage(this.reqUrl)
        if(res1!=null){
            response = request.resourceResolver.map(this.reqUrl+".html");
        }else{
			log.info("Not a mapped URL returning the same :"+this.reqUrl);
			response = this.reqUrl;
        }
    } else {
        response = this.reqUrl
    }
    return {
        mappedUrl : response
    };
});