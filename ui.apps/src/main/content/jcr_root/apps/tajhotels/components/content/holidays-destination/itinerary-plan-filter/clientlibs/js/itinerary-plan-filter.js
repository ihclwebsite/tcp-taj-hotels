function itineraryPlans() {

    window.addEventListener('load', function() {

        // on click on map
        $(' .mr-map-switch').on('click', function() {

            $('.cm-content-blocks.mr-holiday-destination-about-cards-wrapper').addClass('cm-hide');
            $('.cm-content-blocks.mr-holiday-aboutDest-map').removeClass('cm-hide');
            $('.details-description').cmToggleText({
                charLimit : 120
            });
        })

        // on clik on grid
        $(' .mr-view-Toggler-wrap .mr-list-switch').on('click', function() {
            $('.cm-content-blocks.mr-holiday-destination-about-cards-wrapper').removeClass('cm-hide');
            $('.cm-content-blocks.mr-holiday-aboutDest-map').addClass('cm-hide');

        })

        $('.itinerary-plan-filter-icon img').click(function() {
            $('.mr-destination-about-itinerary-plan-wrapper').removeClass('hide-in-sm');
        });

        $('.itinerary-filter-mob-back img').click(function() {
            $('.mr-destination-about-itinerary-plan-wrapper').addClass('hide-in-sm');
        })

        $('.titinerary-apply-btn-mobile').click(function() {
            $('.mr-destination-about-itinerary-plan-wrapper').addClass('hide-in-sm');
        })

    })
}

// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9pdGluZXJhcnktcGxhbi1maWx0ZXIvaXRpbmVyYXJ5LXBsYW4tZmlsdGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGl0aW5lcmFyeVBsYW5zKCkge1xyXG5cclxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmdW5jdGlvbigpIHtcclxuXHJcbiAgICAgICAgLy9vbiBjbGljayBvbiBtYXAgIFxyXG4gICAgICAgICQoJyAubXItbWFwLXN3aXRjaCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICAgICAgICAgJCgnLmNtLWNvbnRlbnQtYmxvY2tzLm1yLWhvbGlkYXktZGVzdGluYXRpb24tYWJvdXQtY2FyZHMtd3JhcHBlcicpLmFkZENsYXNzKCdjbS1oaWRlJyk7XHJcbiAgICAgICAgICAgICQoJy5jbS1jb250ZW50LWJsb2Nrcy5tci1ob2xpZGF5LWFib3V0RGVzdC1tYXAnKS5yZW1vdmVDbGFzcygnY20taGlkZScpO1xyXG4gICAgICAgICAgICAkKCcuZGV0YWlscy1kZXNjcmlwdGlvbicpLmNtVG9nZ2xlVGV4dCh7XHJcbiAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDEyMFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KVxyXG5cclxuXHJcblxyXG4gICAgICAgIC8vb24gY2xpayBvbiBncmlkIFxyXG4gICAgICAgICQoJyAubXItdmlldy1Ub2dnbGVyLXdyYXAgLm1yLWxpc3Qtc3dpdGNoJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoJy5jbS1jb250ZW50LWJsb2Nrcy5tci1ob2xpZGF5LWRlc3RpbmF0aW9uLWFib3V0LWNhcmRzLXdyYXBwZXInKS5yZW1vdmVDbGFzcygnY20taGlkZScpO1xyXG4gICAgICAgICAgICAkKCcuY20tY29udGVudC1ibG9ja3MubXItaG9saWRheS1hYm91dERlc3QtbWFwJykuYWRkQ2xhc3MoJ2NtLWhpZGUnKTtcclxuXHJcblxyXG5cclxuICAgICAgICB9KVxyXG5cclxuICAgICAgICAkKCcuaXRpbmVyYXJ5LXBsYW4tZmlsdGVyLWljb24gaW1nJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoJy5tci1kZXN0aW5hdGlvbi1hYm91dC1pdGluZXJhcnktcGxhbi13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ2hpZGUtaW4tc20nKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCgnLml0aW5lcmFyeS1maWx0ZXItbW9iLWJhY2sgaW1nJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoJy5tci1kZXN0aW5hdGlvbi1hYm91dC1pdGluZXJhcnktcGxhbi13cmFwcGVyJykuYWRkQ2xhc3MoJ2hpZGUtaW4tc20nKTtcclxuICAgICAgICB9KVxyXG5cclxuICAgICAgICAkKCcudGl0aW5lcmFyeS1hcHBseS1idG4tbW9iaWxlJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQoJy5tci1kZXN0aW5hdGlvbi1hYm91dC1pdGluZXJhcnktcGxhbi13cmFwcGVyJykuYWRkQ2xhc3MoJ2hpZGUtaW4tc20nKTtcclxuICAgICAgICB9KVxyXG5cclxuICAgIH0pXHJcbn1cclxuIl0sImZpbGUiOiJtYXJrdXAvY29tcG9uZW50cy9pdGluZXJhcnktcGxhbi1maWx0ZXIvaXRpbmVyYXJ5LXBsYW4tZmlsdGVyLmpzIn0=
