$(document)
        .ready(
                function() {
                    var apiKey2 = $('.banner-container').data('apikey');
                    var destinationLocation = $('.banner-container').data('dest-loc');
                    $.ajax({
                        url : "http://api.openweathermap.org/data/2.5/weather?q=" + destinationLocation + "&appid="
                                + apiKey2 + "&units=metric",
                        cache : false,
                        success : function(data) {
                            var temp = data.main.temp;
                            var tempRound = parseFloat(temp).toFixed();
                            if (tempRound != 'NaN') {
                                $('#weather-update').append(tempRound);
                            } else {
                                $('.hotel-banner-temperature').hide();
                                $('.hotel-banner-date').removeClass('inline-block');
                            }
                        }
                    });

                    var objToday = new Date();
                    var dayOfMonth = objToday.getDate();
                    var months = new Array('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT',
                            'NOV', 'DEC');
                    var curMonth = months[objToday.getMonth()];
                    var curYear = objToday.getFullYear();
                    var today = dayOfMonth + " " + curMonth + " " + curYear;

                    $(".system-date").html(today);
                });
