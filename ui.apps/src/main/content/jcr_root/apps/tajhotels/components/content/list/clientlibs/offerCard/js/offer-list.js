$(document).ready(function(){
	
	var countryContainer=$(document).find(".countryDrop").closest('.selectSubContainer');
	var cityContainer=$(document).find(".destinationDrop").closest('.selectSubContainer');
	var hotelContainer=$(document).find(".hotelsDrop").closest('.selectSubContainer');
	var offerTypes=$(document).find(".offersTypeDrop");
	var offerListContainer=$(document).find(".offers-container");
	var offerListContainerRow= $(".offers-container .row");
	var offerListArray = offerListContainer.find(".row .offers-page-card-component"); 
	var offerFilterloader = $('.offer-filter-loader');
	var filterComponentWrap = $(".filter-component-wrap");
	countryContainer.find(".countryDrop").selectBoxIt()
	cityContainer.find(".destinationDrop").selectBoxIt()
	hotelContainer.find(".hotelsDrop").selectBoxIt()
	offerTypes.selectBoxIt();
	
	filterComponentWrap.on('change','.countryDrop',function(){
		var self = $(this);
		$.ajax({
	         type: 'GET',    
	         url:'/en-in/offers/?country='+this.value,
	         beforeSend: function() {
        	    offerFilterloader.removeClass("cm-hide");        	    
        	 },
	         success: function(msg){
	        	var resp= $.parseHTML(msg)
	        	var cityDropDown=$(resp).find(".destinationDrop").closest('.selectSubContainer')
	        	var hotelDropDown=$(resp).find(".hotelsDrop").closest('.selectSubContainer')
	        	cityContainer.html(cityDropDown.html())
	        	cityContainer.find(".destinationDrop").selectBoxIt()
	        	
	        	hotelContainer.html($(hotelDropDown).html())
	        	hotelContainer.find(".hotelsDrop").selectBoxIt()
	        	
	        	var offersList=$(resp).find(".offers-container")
	        	offerListContainer.html($(offersList).html());
	        	offerListArray = offerListContainer.find(".row .offers-page-card-component");    
	         }
	     }).done(function(){
     		init_pagination();
     		customizeSelectDropDownArrow();
     		offerFilterloader.addClass("cm-hide");
     	});
	});
	
	filterComponentWrap.on('change','.destinationDrop',function(){
		$.ajax({
	         type: 'GET',    
	         url:'/en-in/offers/?destination='+this.value,
	         beforeSend: function() {
	        	 offerFilterloader.removeClass("cm-hide");
        	 },
	         success: function(msg){
	        		var resp= $.parseHTML(msg)
		        	var hotelDropDown=$(resp).find(".hotelsDrop").closest('.selectSubContainer')
		        	
		        	hotelContainer.html($(hotelDropDown).html())
		        	hotelContainer.find(".hotelsDrop").selectBoxIt()
		        	
		        	var offersList=$(resp).find(".offers-container")
		        	offerListContainer.html($(offersList).html());
	        		offerListArray = offerListContainer.find(".row .offers-page-card-component");		        	
	         }
	     }).done(function(){
     		init_pagination();
     		customizeSelectDropDownArrow();
     		offerFilterloader.addClass("cm-hide");
     	});
	});
	
	filterComponentWrap.on('change','.hotelsDrop',function(){
		$.ajax({
	         type: 'GET',    
	         url:'/en-in/offers/?hotel='+this.value,
	         beforeSend: function() {        	    
	        	 offerFilterloader.removeClass("cm-hide");      	    
        	 },
	         success: function(msg){
	        		var resp= $.parseHTML(msg)
		        	var offersList=$(resp).find(".offers-container")
		        	offerListContainer.html($(offersList).html());
	        		offerListArray = offerListContainer.find(".row .offers-page-card-component");
		        	//offerTypes.trigger("change");	        		
	         }
	    }).done(function(){
     		init_pagination();
     		customizeSelectDropDownArrow();
     		offerFilterloader.addClass("cm-hide");
     	});
	});
	
	filterComponentWrap.on('change','.offersTypeDrop',function(){
		var value = $(this).val().toLowerCase();
		if(value==""){
			offerListContainerRow.html(offerListArray);
		}else{
			offerListContainerRow.empty();
			offerListArray.each(function(){
				if($(this).data('offertype'))
				{
				 if(($(this).data('offertype').indexOf(value) > -1)){
					 offerListContainerRow.append(this);
				 }				 
				}
			});
		}
		init_pagination();
	});

});
