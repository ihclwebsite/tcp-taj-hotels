
   $(document).ready(function() {

       function getInvestorsFilterData(keywordData, successfn, errorfn) {
               var requestData = {
        			searchText : getSearchTextFromDom(),
                    dateFilters: JSON.stringify(getSelectedDatesFromDom()),
                    damPaths:JSON.stringify(getDamPaths())
    			};
           $.ajax({
               type: "GET",
               url: '/bin/investors-search',
               data: requestData,
               dataType: "json",
               success: function(data) {
                   console.info("Ajax call successfull");
                   populateInvestorsFilterOptions(data);                   
               },
               error: function(textStatus, errorThrown) {
                    console.error("Error received in ajax call.")
               }
           });
       }


       function populateInvestorsFilterOptions(data) {
           var filterData = data;
           var parentContainer = $('.mr-investors-financial-reports');
           var container = parentContainer.find('.trending-suggestions-container');
			container.empty();
           for (i = 0; i < data.reportsSearchResults.length; i++) {
               container.append('<div class="individual-trends" data="' + data.reportsSearchResults[i].path + '">' + data.reportsSearchResults[i].name + '</div>')
           }
           $('.individual-trends').each(function() {
               $(this).on('click', function() {
                   var pdfLink = $(this).attr('data');
                   location.href = pdfLink;
               })
           })
       }


       $('.mr-investors-financial-reports .searchbar-input').keyup(function(e) {
			e.stopPropagation();
           getInvestorsFilterData(populateInvestorsFilterOptions);
       })

       getInvestorsFilterData(populateInvestorsFilterOptions);


       $('.mr-investors-financial-reports .searchbar-input').focus(function() {
           $('.suggestions-wrap').addClass('visible');
       });

       $('.suggestions-overlay').click(function(e) {
			$('.suggestions-wrap').removeClass('visible');
           $('.search-filters-container').removeClass('cm-hide');
           if($('.mr-investors-financial-reports .searchbar-input').val()) {
               $('.clear-input-icon').addClass('show-clear-input');
           }
       })
       $('.clear-input-icon').click(function() {
			$('.mr-investors-financial-reports .searchbar-input').val("");
			$(this).removeClass('show-clear-input');
       })
   })




function  getSearchTextFromDom(){


    return  $('.mr-investors-financial-reports .searchbar-input').val();
}

function getSelectedDatesFromDom() {
    var selectedDates =[];
    var dropDownDoms = $('.multiselect-container')[0];
    var selectedDropDownDoms =$(dropDownDoms).find('.active');
    if (selectedDropDownDoms.length !=0) {

    for (var i=0;i<selectedDropDownDoms.length;i++) {
        var dom = selectedDropDownDoms[i];
        if(dom.length !=0) {
            var date = $(dom).text();
            selectedDates.push(date.trim());
        }
    }
    return selectedDates;
    }
    else
        return selectedDates;

}

function getDamPaths(){
   var damArray = [];
   var damPathDoms = $.find("[data-dam-path]");
    for (i in damPathDoms) {

        idamPath=$(damPathDoms[i]).data().damPath;
		damArray.push(idamPath);
    }

    return damArray;
}

