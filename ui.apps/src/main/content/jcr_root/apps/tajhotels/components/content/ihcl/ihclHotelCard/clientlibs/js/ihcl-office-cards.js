var $hotelCards;
$(document).ready(function() {
    $('.mr-contact-us-products-container').customSlide(3);
    getHotelsJson();
});

window.addEventListener('load', function() {
    $hotelCards = $(".ichlOfficeCardInnerWrap .row .offers-page-card-component").clone("true");
    $('.clear-input-icon').click(function(e) {
        $(this).prev('.hotel-search').val("");
        $('.clear-input-icon').removeClass('show-clear-input');
        fetchFilterList();
    });
    $(".searchbar-input.hotel-search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        if (value.length === 0) {
            $('.clear-input-icon').removeClass('show-clear-input');
        } else {
            $('.clear-input-icon').addClass('show-clear-input');
        }
        var filteredDest = fetchFilterList();
    });
});

function popUpIhclTaj(elem) {
    var presentScroll = $(window).scrollTop();
    var thisLightBox = $(elem).closest('.popularDest-containers').next().css('display', 'block').addClass(
            'active-popUp');
    $(".cm-page-container").addClass('prevent-page-scroll');
    $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');

    $(document).keydown(function(e) {
        if (($('.active-popUp').length) && (e.which === 27)) {
            $('.showMap-close').trigger("click");
        }
    });
    $('.showMap-close').click(function() {

        thisLightBox.hide().removeClass('active-popUp');
        $(".cm-page-container").removeClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
        $(window).scrollTop(presentScroll);

    })

}
function getHotelsJson() {
    var searchPath = $('#hotelRootPath').val();
    var requestString = "type=" + "hotels" + "&hotelRootPath=" + searchPath;

    $.ajax({
        type : "GET",
        url : '/bin/contactUs',
        data : requestString,
        dataType : "json",
        contentType : "application/json; charset=utf-8",
        success : function(data) {
            dataForHotels = data;
            if (window.screen.width < 992) {
                // createAllCardsMob(dataForHotels.data, $('.hotels.all-card'));
            }
            createCard(dataForHotels.data);
        },
        error : function(errormessage) {
            console.error('AJAX Response Error:-> Request Status: ' + errormessage.status + ', Status Text: '
                    + errormessage.statusText + ', Response Text: ' + errormessage.responseText);
        }

    });
}
var content;
function createCard(dataForHotelsData) {
    dataForHotelsData
            .forEach(function(dataForHotelsDataItem) {
                content = "<div class='offers-page-card-component col-lg-4 col-md-6 col-12 show-more-card-element cm-card-showmore-spacing' data-hotelname=\""
                        + dataForHotelsDataItem.name
                        + " "
                        + dataForHotelsDataItem.country
                        + " "
                        + dataForHotelsDataItem.city
                        + "\">"
                        + "<div class='mr-contact-us-wrap'>"
                        + "<div class='cont-us-details-container'>"
                        + "<div class='cont-us-details-header add-header-style'>"
                        + dataForHotelsDataItem.name
                        + "</div>"
                        + prepareContactString(dataForHotelsDataItem.phone, dataForHotelsDataItem.hotelEmail,
                                dataForHotelsDataItem.fax) + "</div></div></div></div>"
                $('#ihclHotelCardInnerWrap').append(content);
            })

    $('#ihclHotelCardsList').showMore(6, 9);

}

function prepareContactString(phone, email, fax) {

    var phoneString = '';
    var emailString = '';
    var faxString = '';
    if (phone != null && phone != '' && !phone.includes('null')) {
        phoneString = "<div><a class='ho-contact-number hide-in-lg inline-block' href='tel:dataForHotelsDataItem.phone'>"
                + "<span>Phone:-</span>"
                + "<div class='inline-block ho-mail-to-txt cm-view-link-txt ho-cm-link-txt' itemprop='telephone'>"
                + phone
                + "</div>"
                + "</a></div><div class='inline-block ho-mail-to-txt hide-in-sm hide-in-md'>Phone:- "
                + phone
                + "</div>";
    }
    if (email != null && email != '' && !email.includes('null')) {
        emailString = "<div><a class='ho-mail-to inline-block add-font-style' href='email'><span>Email :- </span>"
                + "<div class='inline-block ho-mail-to-txt cm-view-link-txt ho-cm-link-txt add-font-style'>" + email
                + "</div></a></div>";
    }
    if (fax != null && fax != '' && !fax.includes('null')) {
        faxString = "<div class='add-font-style'>Fax :- " + fax + "</div>";
    }

    var contactString = "<div class='cont-us-contact-wrap'>" + "<div class='cont-us-phone-number'>" + "<div>"
            + "<div class='ho-contact-options-con add-font-style'>" + phoneString + emailString + faxString
            + "</div></div></div></div>";
    return contactString;
}

function fetchFilterList() {

    var searchKey = $('.searchbar-input.hotel-search').val().toLowerCase();
    var $filteredHotelCards = [];
    $(".ichlOfficeCardInnerWrap .row").empty();
    $(".ichlOfficeCardInnerWrap").siblings(".jiva-spa-show-more").remove();
    $hotelCards.each(function() {
        var hotelName = $(this).data('hotelname').toLowerCase();
        if ((hotelName && hotelName.indexOf(searchKey) > -1)) {
            $filteredHotelCards.push($(this));
        }
    });
    if ($filteredHotelCards == 0) {
        $(".ichlOfficeCardInnerWrap .row").html("No Results found.");
    } else {
        $(".ichlOfficeCardInnerWrap .row").append($filteredHotelCards);
        $(".ichlOfficeCardInnerWrap").showMore(6, 9);
    }
}
