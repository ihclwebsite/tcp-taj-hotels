document
		.addEventListener(
				'DOMContentLoaded',
				function() {
					if($('.rate-card-view-detials-container').attr('data-room-details') == 'open'){
                        $('.rate-card-view-detials-container').attr('data-room-details','close');
                        $('.rate-card-view-detials-container').click(function(e) {
                            onClickViewDetailsButton(this, e);
                        })
                    }
                    else{
						$('.rate-card-view-detials-container').attr('data-room-details','open');
                    }
                    if($('.more-rates-button').attr('data-view-details') == 'open'){
                        $('.more-rates-button').attr('data-view-details','close');
						$('.more-rates-button')
							.click(
									function(e) {
										e.stopPropagation();
										$(this).toggleClass(
												'more-rates-button-selected');
										$(this)
												.parents()
												.eq(3)
												.siblings(
														'.rate-card-more-rates-section')
												.toggleClass('visible');
										$(this).parents().eq(3).toggleClass('select-border');
										$(this).find('.icon-drop-down-arrow-white').toggleClass('cm-rotate-icon-180');										
										$(this).parents().eq(3).siblings(
												'.rate-card-details-section')
												.removeClass('visible-table');
										$('.rate-card-view-detials-container')
												.removeClass(
														'rate-card-view-detials-selected')
									})
                    }
                    else{
						$('.more-rates-button').attr('data-view-details','open');
                    }
					$('.service-details-subsection-show-more')
							.click(
									function(e) {
										e.stopPropagation();
										if ($(this).text() == "Show More") {
											$(this).text("Show Less");
											$(this)
													.siblings(
															'.service-details-subsection-container')
													.css('display', 'block');
										} else {
											$(this).text("Show More");
											$(this)
													.siblings(
															'.service-details-subsection-container')
													.css('display', 'none');
										}
									});

					$(
							'.rate-card-details-section, .rate-card-more-rates-section')
							.click(function(e) {
								e.stopPropagation();
							})

					$('.room-details-show-more').click(function() {
						$('.content').css('max-height', 'none');
						$(this).css('display', 'none');
						$('.room-details-show-less').css('display', 'block');
					})
					$('.room-details-show-less').click(function() {
						$('.content').css('max-height', '78px');
						$(this).css('display', 'none');
						$('.room-details-show-more').css('display', 'block');
					})
					
					 /*$.each($('.rate-card-room-description'), function(i, value) {
					        $(value).cmTrimText({
					            charLimit : 150,
					        });
					    });*/
				});

function addCarouselDisplayListenerTo(triggerer, carouselOverlayDom) {
	$(triggerer).click(function() {
		displayCarouselOverlayDom(carouselOverlayDom);
	})
}

function displayCarouselOverlayDom(carouselOverlayDom) {
	$(carouselOverlayDom).removeClass('mr-overlay-initial-none');
	$(carouselOverlayDom).find(".carousel-item").removeClass('active');
	$(carouselOverlayDom).find(".carousel-item").first().addClass('active');
}

function onClickViewDetailsButton(viewDetailsButton, e) {
	if (e != undefined) {
		e.stopPropagation();
	}
	$(viewDetailsButton).toggleClass('rate-card-view-detials-selected');
	var $content = $(viewDetailsButton).parents().eq(2).siblings(
			'.rate-card-details-section');
	$content.toggleClass('visible-table');
	$(viewDetailsButton).parents().eq(2).siblings(
			'.rate-card-more-rates-section').removeClass('visible');
	$('.more-rates-button').removeClass('more-rates-button-selected');
	if(!isIHCLCBSite()) {
	    $('.more-rates-button')
			.html(
					'VIEW RATES<img src="/content/dam/tajhotels/icons/style-icons/drop-down-arrow-white.svg" alt = "drop down arrow white icon">');
	}
	var parentOffset = $('.cm-page-container').offset();
	var targetOffset = $(viewDetailsButton).offset();
	$('body').animate({
		scrollTop : (parentOffset.top * -1) + targetOffset.top
	}, 'slow');
	
	if ($(viewDetailsButton).hasClass('rate-card-view-detials-selected')) {
		$(viewDetailsButton).parents().eq(2).addClass('select-border');
	} else {
		$(viewDetailsButton).parents().eq(2).removeClass('select-border');
	}

}
