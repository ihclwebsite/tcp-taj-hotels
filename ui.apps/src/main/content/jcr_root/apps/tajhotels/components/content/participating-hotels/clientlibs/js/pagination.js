$(document).ready(function(){
	init_pagination();
	bind_paginate_function();
  });

init_pagination= function()
{
	 
	list_size= $(".participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap").length;
	paginate_interval=6;
	current_offset=paginate_interval;
	if(list_size < paginate_interval)
		{
		disable_pagination()
		}
	else
		{
		$('.participating-hotels-show-more').show();
		$('.participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap').not(':lt('+current_offset+')').hide();
		}	
}

bind_paginate_function= function()
{
	$('.participating-hotels-show-more').click(function()
			{
			current_offset=(current_offset+paginate_interval < list_size) ? current_offset+paginate_interval :(list_size)
			$('.participating-hotels .row.participating-hotels-row .hotelCard-container-outer-wrap:lt('+current_offset+')').show()
			if(current_offset==list_size)
						{
								disable_pagination();
						}
			});
}

disable_pagination=function()
{
	$('.participating-hotels-show-more').hide();
}





