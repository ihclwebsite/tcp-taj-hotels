window.addEventListener('load', function() {

    var allMarkers = [];
    var fitBoundsMarkers = [];
    var options = {
        imagePath : '/content/dam/tajhotels/icons/style-icons/cluster'
    };

    var imageList = {
        "taj" : [ "/content/dam/tajhotels/icons/style-icons/taj.svg",
                "/content/dam/tajhotels/icons/style-icons/tajLarge.svg" ],
        "vivanta" : [ "/content/dam/tajhotels/icons/style-icons/vivanta-small.png",
                "/content/dam/tajhotels/icons/style-icons/vivanta-large.png" ],
        "gateway" : [ "/content/dam/tajhotels/icons/style-icons/gateway.svg",
                "/content/dam/tajhotels/icons/style-icons/gatewayLarge.svg" ],
        "seleqtions" : [ "/content/dam/tajhotels/icons/style-icons/seleqtions-small.png",
                "/content/dam/tajhotels/icons/style-icons/seleqtions-large.png" ],
		"ama" : [ "/content/dam/tajhotels/icons/style-icons/amaSmall.png",
                "/content/dam/tajhotels/icons/style-icons/amaLarge.png" ]
    };

    var markerSizeFlag = false;
    var markerCluster;

    var hideMarker = [];

    // if map element is present initialize map api
    if (document.getElementById('map')) {
        initMapsApi();
    }

    function initMapsApi() {

        initMap();
        $('div[id^=dest]').each(function() {
            var markerData = {};
            markerData.id = this.id;
            $(this).find('a[id^=hotel]').each(function() {
                markerData.hotelId = this.id;
                markerData.latitude = parseFloat(this.dataset.lat);
                markerData.longitude = parseFloat(this.dataset.lng);
                markerData.title = this.dataset.title;
                markerData.brandType = this.dataset.brand;
                createMarker(markerData);
            });
        });
        initiateClustor();
    }

    function initMap() {
        var latlng = new google.maps.LatLng(0, 0);

        map = new google.maps.Map(document.getElementById('map'), {
            zoom : 2,
            minZoom : 1.5,
            center : latlng
        });

        google.maps.event.addListener(map, 'click', function() {
            if ($('.card-wrapper.clicked').length > 0) {
                $('.card-wrapper.clicked').find('.hotel-properties-container').slideUp(50).removeClass('open');
                $('.card-wrapper.clicked').removeClass('clicked');
                $('.hotel-container.expand-hotels.activeHotel').removeClass('activeHotel');
                markerSizeChange();
            }
        });

    }

    function createMarker(markerData) {
        var type = markerData.brandType;
        var detail = {};
        detail.lat = markerData.latitude;
        detail.lng = markerData.longitude;

        if (type === "taj") {
            imagePath = imageList.taj[0];
            imagePathLarge = imageList.taj[1];
        } else if (type === "vivanta") {
            imagePath = imageList.vivanta[0];
            imagePathLarge = imageList.vivanta[1];
        } else if (type == "gateway") {
            imagePath = imageList.gateway[0];
            imagePathLarge = imageList.gateway[1];

        } else if (type == "ama") {
            imagePath = imageList.ama[0];
            imagePathLarge = imageList.ama[1];

        } else if (type == "seleqtions") {
            imagePath = imageList.seleqtions[0];
            imagePathLarge = imageList.seleqtions[1];
        }

        var pinIcon = {
            url : imagePath,
            scaledSize : new google.maps.Size(46, 58),
        };

        var pinIconLarge = {
            url : imagePathLarge,
            scaledSize : new google.maps.Size(64, 84),
        }

        marker = new MarkerWithLabel({
            position : detail,
            map : map,
            animation : google.maps.Animation.DROP,
            icon : pinIcon,
            labelContent : markerData.title,
            labelAnchor : new google.maps.Point(50, 0),
            labelClass : "markerlabels",
            labelInBackground : true,
            optimized : false
        });

        marker.id = markerData.id;
        marker.hotelId = markerData.hotelId;
        marker.markerSizeFlag = markerSizeFlag;
        marker.iconNormal = pinIcon;
        marker.iconFocused = pinIconLarge;

        allMarkers.push(marker);

        google.maps.event.addListener(marker, 'click', function() {
            if ((window.screen.width) < 992) {
                if (this.markerSizeFlag == false) {
                    createCardsForMobile(this.id, this.hotelId);
                    $('.selected-location-hotels').show();
                    markerSizeChange(this.id, this.hotelId);
                }
            } else {
                if (this.markerSizeFlag == true) {
                    focusedMarker();
                } else {
                    activeHotelShow(this);
                    markerSizeChange(this.id, this.hotelId);
                }
            }
        });
    }

    function initiateClustor() {
        markerCluster = new MarkerClusterer(map, allMarkers, options);
    }

    function hideParticularMarker(markerArr) {
        for (var i = 0; i < markerArr.length; i++) {
            markerArr[i].setVisible(false);
        }
    }

    function createCardsForMobile(id, hotelId) {
        $('.carousel-inner.card-carousel').empty();
        var cards = $(".carousel-data-cards").find('#' + id).clone();
        $('.carousel-inner.card-carousel').append(cards);
        if (hotelId) {
            var hotelIdNum = hotelId.split("-")[1];
            cards
                    .each(function() {
                        $(this)
                                .toggleClass(
                                        'active',
                                        ($(this).find('.hotel-parent .hotelCard-container-outer-wrap').attr(
                                                'data-hotelId') === hotelIdNum));
                    });
        } else {
            $('.carousel-item:first-child').addClass('active');
        }
    }

    changePosition = function(event, targetDest) {
        var target = targetDest || $(event.currentTarget);

        updateMap(target);

        hotelId = target.find('a[id^=hotel]')[0].id;
        markerSizeChange(target[0].id, hotelId);
        targetDest ? expandTarget(target) : slideToggle(target);
    }

    function slideToggle(elem) {
        if ((elem.hasClass('clicked') === true)) {
            elem.find('.hotel-properties-container').slideUp(50).removeClass('open');
            elem.removeClass('clicked');
            $('.hotel-container.expand-hotels.activeHotel').removeClass('activeHotel');
        } else {
            expandTarget(elem);
        }
    }

    function expandTarget(elem) {
        if (!elem.hasClass('clicked')) {
            var clickedCard = $('.card-wrapper.clicked');
            if (clickedCard.length > 0) {
                clickedCard.find('.hotel-properties-container').slideUp(50).removeClass('open');
                clickedCard.removeClass('clicked');
                $('.hotel-container.expand-hotels.activeHotel').removeClass('activeHotel');
            }
            elem.find('.hotel-properties-container').slideDown(50).addClass('open');
            elem.find('.hotel-container.expand-hotels:first-child').addClass('activeHotel');
            elem.addClass('clicked');
        }
    }

    function showZoomLevelHandle(fitBoundsMarkers) {
        markerCluster.addMarkers(fitBoundsMarkers);
        var bounds = new google.maps.LatLngBounds();

        for (var i = 0; i < fitBoundsMarkers.length; i++) {
            fitBoundsMarkers[i].setVisible(true);
            bounds.extend(fitBoundsMarkers[i].getPosition());
        }

        map.fitBounds(bounds);
        if ((fitBoundsMarkers.length) == 1) {
            map.setZoom(map.getZoom() - 5);
        }
    }

    function markerSizeChange(cityId, hotelId) {
        for (var i = 0; i < allMarkers.length; i++) {
            if ((allMarkers[i].id == cityId) && (allMarkers[i].hotelId == hotelId)) {
                if (allMarkers[i].markerSizeFlag == true) {
                    allMarkers[i].setIcon(allMarkers[i].iconNormal);
                    allMarkers[i].set('labelClass', 'markerlabels');
                    allMarkers[i].markerSizeFlag = false;
                } else {
                    allMarkers[i].setIcon(allMarkers[i].iconFocused);
                    allMarkers[i].set('labelClass', 'markerlabels active');
                    allMarkers[i].markerSizeFlag = true;
                }
            } else {
                allMarkers[i].setIcon(allMarkers[i].iconNormal);
                allMarkers[i].set('labelClass', 'markerlabels');
                allMarkers[i].markerSizeFlag = false;
            }
        }
    }

    function activeHotelShow(current) {
        var hotelId = current.hotelId;
        var destElem = $("#" + current.id);
        if (destElem.hasClass('clicked')) {
            $('.hotel-properties-container.open').children('.activeHotel').removeClass('activeHotel');
            $('.hotel-properties-container.open').children("#" + hotelId).addClass('activeHotel');
            focusedMarker();
        } else {
            slideToggle(destElem);
            $('.card-wrapper.clicked').children().find("#" + hotelId).addClass('activeHotel')
        }
    }

    function focusedMarker() {
        $('.hotel-container.expand-hotels.activeHotel').addClass('focused');
        setTimeout(function() {
            $('.hotel-container.expand-hotels.activeHotel').removeClass('focused');
        }, 1000);
    }

    // click on show map button on mobile
    $('.show-map-button').click(function() {
        // when some city card is opened
        if ($('.card-wrapper.clicked').length > 0) {
            var id = $('.card-wrapper.clicked').attr('id');
            createCardsForMobile(id);
            $('.selected-location-hotels').show();

        }
    });

    // stop auto carousel movement
    $('#hotels-carousel').carousel({
        interval : false
    });

    // swap functionality
    $("#hotels-carousel").on("touchstart", function(event) {
        var xClick = event.originalEvent.touches[0].pageX;
        $(this).one("touchmove", function(event) {
            var xMove = event.originalEvent.touches[0].pageX;
            if (Math.floor(xClick - xMove) > 5) {
                $(this).carousel('next');

            } else if (Math.floor(xClick - xMove) < -5) {
                $(this).carousel('prev');

            }
            if ($('.carousel-inner.card-carousel').children().length > 1) {
                activeElement();
            }
        });
        $("#hotels-carousel").on("touchend", function() {
            $(this).off("touchmove");
        });
    });

    // for mobile
    function activeElement() {
        var hotelId = $('.carousel-inner').find('.carousel-item.active').children('.hotel-parent').children(
                '.hotel-wrapper').attr('id');
        var cityId = $('.carousel-inner').find('.carousel-item.active').attr('id');
        markerSizeChange(cityId, hotelId);
    }

    var showMoreFlag = false;

    showMore = function(e) {
        e.stopPropagation();
        var target = $(e.currentTarget);
        var currentDest = $(target.parent());
        var currentHotels = currentDest.find('.hotel-container.expand-hotels');
        showMoreFlag = !showMoreFlag;
        if (showMoreFlag == true) {
            for (var i = 1; i < currentHotels.length; i++) {
                currentHotels.slice(i).css('display', 'block');
                currentDest.find(".show-more-imageUnderline.com-hide img").css('transform', 'rotate(180deg)');
                currentDest.find(".show-more-imageUnderline.com-hide div").html("show less");
            }
        } else {
            for (var i = 1; i < currentHotels.length; i++) {
                currentHotels.slice(i).css('display', 'none');
                currentDest.find(".show-more-imageUnderline.com-hide img").css('transform', 'rotate(0deg)')
                currentDest.find(".show-more-imageUnderline.com-hide div").html("show more");
            }
        }
    }

    /*
     * Updates the map with relevant markers if target destination is provided, it checks if its hotels are visible and
     * then shows it on map if target destination is not provided, it checks all visible destinations and visible hotels
     * and shows it on map
     */
    updateMap = function(targetDest) {
        if (markerCluster) {
            markerCluster.clearMarkers();
            var visibleHotels = [];
            if (targetDest) {
                visibleHotels = targetDest.find('a[id^=hotel]').filter(function() {
                    return $(this).css('display') != 'none';
                });
            } else {
                visibleHotels = $('div[id^=dest]:visible a[id^=hotel]').filter(function() {
                    return $(this).css('display') != 'none';
                });
            }
            fitBoundsMarkers = [];
            hideMarker = [];
            for (var i = 0; i < allMarkers.length; i++) {
                for (var j = 0; j < visibleHotels.length; j++) {
                    if (allMarkers[i].hotelId == visibleHotels[j].id) {
                        fitBoundsMarkers.push(allMarkers[i]);
                    } else {
                        hideMarker.push(allMarkers[i]);
                    }
                }
            }

            hideParticularMarker(hideMarker);
            showZoomLevelHandle(fitBoundsMarkers);
        }
    }
    var element = $('.our-hotels-container.inner');
    for (i = 0; i < element.length; i++) {
        if (element[i].innerText == '') {
            $(element[i]).css('display', 'none');
        }
    }
});