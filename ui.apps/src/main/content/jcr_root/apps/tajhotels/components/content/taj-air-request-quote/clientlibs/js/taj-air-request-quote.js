$(document)
        .ready(
                function() {
                    $("#to-be-displayed-after-add").css('display', 'none');
                    $("#meeting-events-guest").blur(function() {
                        if ($("#meeting-events-guest").val() > 9) {
                            $(this).addClass('invalid-input');
                            invalidWarningMessage($(this));
                            $('#meeting-events-guest').val('');
                        }
                    });
                    $(".request-quote-submit-btn").click(function() {
                        if (requestFormInputValidation(".taj-air-event-request-quote")) {
                            initiateServletCall();
                        }

                    });

                    $(".rate-tab").click(
                            function() {
                                if ($(this).text() === 'MULTI-CITY') {
                                    $('.taj-air-event-request-quote').removeClass('return-package').removeClass(
                                            'one-way-package').addClass("multi-package");
                                } else if ($(this).text() === 'RETURN') {

                                    $('.taj-air-event-request-quote').removeClass('multi-package').removeClass(
                                            'one-way-package').addClass("return-package");

                                } else {
                                    $('.taj-air-event-request-quote').removeClass('return-package').removeClass(
                                            "multi-package").addClass('one-way-package');
                                }
                            });

                    /* add button js starts */
                    var content = $('.flight-journey-details').eq(0).clone();
                    $(".add-city-btn")
                            .on(
                                    "click",
                                    function() {
                                        $("#to-be-displayed-after-add").css('display', 'block');
                                        var destinationNo = $(".flight-journey-details").length;
                                        /* content.addClass("flight-journey-details" + destinationNo); */
                                        $(".remove-city-btn").removeClass("re-quote-disabled");
                                        var clonedContent = content.clone();
                                        clonedContent.find('.specify-time-select').remove();
                                        $(".flight-journey-details-wrp").append(
                                                clonedContent.addClass(
                                                        "multi-city-display flight-journey-details" + destinationNo)
                                                        .find("#event-quote-date-value0").attr("id",
                                                                "event-quote-date-value" + destinationNo).parent()
                                                        .parent().find("#meeting-events-date0").attr("id",
                                                                "meeting-events-date" + destinationNo).closest(
                                                                '.flight-journey-details'));
                                        $(".flight-journey-details" + destinationNo + " #customer-time").selectBoxIt();
                                        /* customizeSelectDropDownArrow() */
                                        /* js for datepicker */

                                        var today = new Date();

                                        $(
                                                '.flight-journey-details' + destinationNo + ' #meeting-events-date'
                                                        + destinationNo)
                                                .datepicker({
                                                    startDate : today
                                                })
                                                .on(
                                                        'changeDate',
                                                        function(element) {
                                                            if ($(this).hasClass('visible')) {
                                                                var minDate = (new Date(element.date.valueOf()));
                                                                // Added by Dinesh for Event quote datepicker
                                                                // var selectedDate = (minDate.getDate() + '/' +
                                                                // (minDate.getMonth() +
                                                                // 1) + '/' +
                                                                // minDate.getFullYear());
                                                                var selectedDate = moment(minDate).format("DD/MM/YYYY");
                                                                $(this).closest(".jiva-spa-date-section").find(
                                                                        '.event-quote-date-value').val(selectedDate)
                                                                        .removeClass('invalid-input');
                                                                $(this).closest(".jiva-spa-date-section").find(
                                                                        '.jiva-spa-date-value').text(selectedDate);
                                                                $(this).closest(".jiva-spa-date-section").find(
                                                                        '.jiva-spa-date').removeClass('visible');
                                                                $(this).closest(".jiva-spa-date-section").find(
                                                                        '.jiva-spa-date-con').removeClass(
                                                                        'jiva-spa-not-valid');
                                                            }
                                                        });

                                        $(
                                                '.flight-journey-details' + destinationNo + ' #event-quote-date-value'
                                                        + destinationNo).datepicker();

                                        $('.jiva-spa-date-con').click(function(e) {
                                            e.stopPropagation();
                                            $('.jiva-spa-date').removeClass('visible');
                                            $(this).siblings('.jiva-spa-date').addClass('visible');
                                        });

                                        $('body').click(function(e) {
                                            $('.jiva-spa-date').removeClass('visible');
                                        });
                                        customizeSelectDropDownArrow();

                                    });

                    $(".remove-city-btn").on("click", function() {
                        if ($(".flight-journey-details:visible").length > 2) {
                            $('.flight-journey-details:visible').last().remove();
                        } else if ($(".flight-journey-details:visible").length > 1) {
                            $('.flight-journey-details:visible').last().remove();
                            $(this).addClass("re-quote-disabled");
                        }
                    });
                    /* add button js ends */

                    /* js for the request quote starts */

                    function requestFormInputValidation(wrapper) {
                        var requestFormValid = true;
                        $(wrapper).find('input.sub-form-mandatory:visible').each(function() {
                            requestFormValid = addRequestFormInputWarning($(this)) && requestFormValid;
                            // if ($(this).val() == "") {
                            // $(this).addClass('invalid-input');
                            // invalidWarningMessage($(this));
                            // requestFormValid = false;
                            // }
                        });
                        $(wrapper).find('.selectboxit.sub-form-mandatory:visible').each(function() {
                            var $elem = $(this).closest(".selectSubContainer").find("select.sub-form-mandatory")
                            requestFormValid = addRequestFormInputWarning($elem) && requestFormValid;
                            // if ($elem.val() == "") {
                            // $elem.addClass('invalid-input');
                            // invalidWarningMessage($elem);
                            // requestFormValid = false;
                            // }
                        });

                        return requestFormValid;
                    }

                    function addRequestFormInputWarning(_this) {
                        var limitGuest = false;
                        if (_this.hasClass('limit-no-of-guest')) {
                            limitGuest = !isValidNumberOfGuest(_this, 9);
                        }
                        if (_this.val() == "" || limitGuest) {
                            _this.addClass('invalid-input');
                            invalidWarningMessage(_this);
                            return false;
                        }
                        return true;
                    }

                    function isValidNumberOfGuest(ele, count) {
                        if (ele.val() > count) {
                            ele.val('');
                            ele.attr("placeholder", "Maximum count is " + count);
                            return false;
                        }
                        return true;
                    }

                    /* js for the request quote starts */

                    /*
                     * $('.venue-search-input.sub-form-input-element.sub-form-mandatory').attr("value",
                     * dataLayerData.hotelName); //analytics code var jsonObj= {}; jsonObj.eventStartDate="";
                     * jsonObj.eventEndDate=""; jsonObj.venueName=getParam('venueName'); jsonObj.guestCount="";
                     * jsonObj.hotelName=dataLayerData.hotelName; jsonObj.guestEmailId=""; jsonObj.phoneNumber="";
                     * jsonObj.guestName="";
                     * 
                     * prepareJsonForEvents("Venue Selected", jsonObj);
                     * 
                     * var SEARCH_INPUT_DEBOUNCE_RATE = 300; var jivaDateWidth =
                     * $('#meeting-events-date').outerWidth();?
                     */
                    /*
                     * var validateRequestQuoteElements = function() { $('.sub-form-mandatory').each(function() { if
                     * ($(this).val() == "") { $(this).addClass('invalid-input'); invalidWarningMessage($(this)); } else {
                     * $(this).removeClass('invalid-input'); } }); }; // Request quote submit handler
                     * $('.request-quote-submit-btn').click(function() { validateRequestQuoteElements(); if
                     * ($('.sub-form-input-element').hasClass('invalid-input')) { $('.invalid-input').first().focus();
                     * $("failure-cnt").css("display","block") } else { $("success-cnt").css("display","block");
                     * requestBooking(); } });
                     */

                    // datepicker script
                    var today = new Date();
                    $('#meeting-events-date0 ').datepicker({
                        startDate : today
                    }).on(
                            'changeDate',
                            function(element) {
                                if ($(this).hasClass('visible')) {
                                    var minDate = (new Date(element.date.valueOf()));
                                    // Added by Dinesh for Event quote datepicker
                                    // var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' +
                                    // minDate.getFullYear());
                                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                                    $(this).closest(".jiva-spa-date-section").find('.event-quote-date-value').val(
                                            selectedDate).removeClass('invalid-input');
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date-value').text(
                                            selectedDate);
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date').removeClass(
                                            'visible');
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date-con').removeClass(
                                            'jiva-spa-not-valid');
                                    $(this).closest(".single-flight-journey-detail-wrp").find(".meeting-events-date1")
                                            .datepicker('setStartDate', minDate);
                                }
                            });
                    $('.meeting-events-date1').datepicker({
                        startDate : today
                    }).on(
                            'changeDate',
                            function(element) {
                                if ($(this).hasClass('visible')) {
                                    var minDate = (new Date(element.date.valueOf()));
                                    // Added by Dinesh for Event quote datepicker
                                    // var selectedDate = (minDate.getDate() + '/' + (minDate.getMonth() + 1) + '/' +
                                    // minDate.getFullYear());
                                    var selectedDate = moment(minDate).format("DD/MM/YYYY");
                                    $(this).closest(".jiva-spa-date-section").find('.event-quote-date-value').val(
                                            selectedDate).removeClass('invalid-input');
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date-value').text(
                                            selectedDate);
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date').removeClass(
                                            'visible');
                                    $(this).closest(".jiva-spa-date-section").find('.jiva-spa-date-con').removeClass(
                                            'jiva-spa-not-valid');
                                }
                            });

                    $('#event-quote-date-value0 ,#event-quote-date-value1').datepicker();

                    $('.jiva-spa-date-con').click(function(e) {
                        e.stopPropagation();
                        $(this).siblings('.jiva-spa-date').addClass('visible');
                    });

                    $('body').click(function(e) {
                        $('.jiva-spa-date').removeClass('visible');
                    });
                    $(
                            '.hotels-event-confirmation-overlay .event-confirmation-close-icon,.hotels-event-confirmation-overlay .event-confirmation-close-btn')
                            .click(function() {
                                location.reload();
                            });
                    $('.hotels-event-confirmation-inner-wrp').click(function(e) {
                        e.stopPropagation();
                    });

                    $(
                            '.hotels-event-error-overlay .event-error-close-icon,.hotels-event-error-overlay .event-confirmation-close-btn')
                            .click(function() {
                                $('.hotels-event-error-popup').hide();
                                $(".cm-page-container").removeClass('prevent-page-scroll');
                            });
                    $('.hotels-event-error-inner-wrp').click(function(e) {
                        e.stopPropagation();
                    });

                    $(document)
                            .keydown(
                                    function(e) {
                                        if ((($('.hotels-event-error-overlay').is(':visible')) || ($('.hotels-event-confirmation-overlay')
                                                .is(':visible')))
                                                && (e.which === 27)) {
                                            $('.event-confirmation-close-btn:visible').trigger("click");
                                        }

                                    });

                    /*
                     * $('.hotels-event-confirmation-overlay, .event-confirmation-close-icon,
                     * .event-confirmation-close-btn') .click(function() { $('.hotels-event-confirmation-popup').hide();
                     * }); $('.hotels-event-confirmation-inner-wrp').click(function(e) { e.stopPropagation(); });
                     * 
                     * $('.cm-page-container').blur(function() { $('.jiva-spa-date').removeClass('visible'); });
                     * 
                     * //disabling the following code for disabling the change in hotel name at lead capture form /*
                     * $('#searchInputEvents').on("keyup", debounce(function(e) { if
                     * ($('#searchInputEvents').val().length > 2) { clearSearchResults();
                     * performSearch($('#searchInputEvents').val()).done(function() {
                     * $('.meeting-events-search-list').css('display', 'block');
                     * $('.meeting-events-search-list').removeClass('display-none'); $('.meeting-events-search-list
                     * li').on('click', function() { var value = ($(this).children()).text(); $(".venue-search-input-wrp >
                     * input").val(value); $(".venue-search-input-wrp > input").removeClass('meeting-events-not-valid');
                     * $('.meeting-events-search-list').css('display', 'none');
                     * $('.event-search-close').addClass('visible'); }); }); } else { clearSearchResults(); if
                     * (!$('.meeting-events-search-list').hasClass('display-none')) { console.log();
                     * $('.meeting-events-search-list').addClass('display-none'); } } }, SEARCH_INPUT_DEBOUNCE_RATE));
                     */
                    function clearSearchResults() {
                        $('.meeting-events-search-list').empty();

                    }

                    function performSearch(key) {
                        return $.ajax({
                            method : "GET",
                            url : "/bin/search",
                            data : {
                                searchText : key
                            }
                        }).done(function(res) {
                            clearSearchResults();
                            addHotelSearchResults(res.hotels);
                        }).fail(function() {
                            clearSearchResults();
                        });
                    }

                    function addHotelSearchResults(hotels) {
                        if (hotels.length) {
                            hotels
                                    .forEach(function(hotel) {
                                        console.log(hotel.title);
                                        $('.meeting-events-search-list').append(
                                                '<li ><label>' + hotel.title + '</label></li>');
                                    });
                        }
                    }

                    /*
                     * $(".venue-search-input-wrp > input").click(function(e) { e.stopPropagation();
                     * $('.meeting-events-search-list').css('display', 'block'); $('.meeting-events-search-list
                     * li').on('click', function() { var value = ($(this).children()).text(); $(".venue-search-input-wrp >
                     * input").val(value); $(".venue-search-input-wrp > input").removeClass('meeting-events-not-valid');
                     * $('.meeting-events-search-list').css('display', 'none');
                     * $('.event-search-close').addClass('visible'); }); });
                     */
                    $('.event-search-close').click(function() {
                        $(this).removeClass('visible');
                        $(".venue-search-input-wrp > input").attr("placeholder", "Search");
                    });

                    $(document).on("click", function(e) {
                        $('.meeting-events-search-list').css('display', 'none');
                    });

                    function requestBooking(key) {
                        $('.pg-spinner-con').css('display', "block");
                        var venueName = getParam('venueName');

                        var hotelName = $('#searchInputEvents').val();

                        var hotelEmailId = $('#hotel-email-id').val();

                        var bookingDate = $('#event-quote-date-value').val();

                        var guest = $('#meeting-events-guest').val();

                        var name = $('#customer-name').val();

                        var phoneNumber = $('#phone-Number').val();

                        var officePhoneNumber = $('#office-phone-Number').val();

                        var guestEmailId = $('#customer-email-id').val();

                        var requestQuoteConfLink = $('.request-quote-conf-link').val() + ".html" + '?hotelWas='
                                + JSON.stringify(pageLevelData);

                        var requestQuoteErrorLink = $('.request-quote-error-link').val() + ".html";

                        var requestString = "hotelName=" + hotelName + "&hotelEmailId=" + hotelEmailId
                                + "&bookingDate=" + bookingDate + "&noOfGuests=" + guest + "&name=" + name
                                + "&phoneNumber=" + phoneNumber + "&officePhoneNumber=" + officePhoneNumber
                                + "&guestEmailId=" + guestEmailId + "&venueName=" + venueName
                        console.log(requestString);

                        var meetingRoomRequest = prepareBookingData(hotelName, bookingDate, guest);
                        tigger_meeting_quote(meetingRoomRequest)

                        // analytics code
                        var jsonObj = {};
                        jsonObj.eventStartDate = bookingDate;
                        jsonObj.eventEndDate = "";
                        jsonObj.venueName = venueName;
                        jsonObj.guestCount = guest;
                        jsonObj.hotelName = hotelName;
                        jsonObj.guestEmailId = guestEmailId;
                        jsonObj.phoneNumber = phoneNumber;
                        jsonObj.guestName = name;

                        prepareJsonForEvents("Request for a Quote", jsonObj);

                        return $.ajax({
                            method : "GET",
                            url : "/bin/requestQuote",
                            data : requestString
                        }).done(function(res) {
                            console.log("success");
                            window.location.assign(requestQuoteConfLink);
                        }).fail(function() {
                            console.log("fail");
                            window.location.assign(requestQuoteErrorLink);
                            dataCache.session.setData("confStatus", "Error");
                        });
                    }

                    function prepareBookingData(hotelName, bookingDate, guest) {
                        var jivaBookingData = {};
                        jivaBookingData.hotelName = hotelName;
                        jivaBookingData.bookingDate = bookingDate;
                        jivaBookingData.numOfGuests = guest;
                        return jivaBookingData;
                    }

                    function getParam(name) {
                        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                        var regexS = "[\\?&]" + name + "=([^&#]*)";
                        var regex = new RegExp(regexS);
                        var results = regex.exec(window.location.href);
                        if (results == null)
                            return "";
                        else
                            return results[1];
                    }
                    if ($(".mr-reports-wrap").length) {
                        $(".taj-air-event-request-quote").addClass("multi-package");

                    }

                });
// #
// sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9ob3RlbHMtZXZlbnQtcmVxdWVzdC1xdW90ZS9ob3RlbHMtZXZlbnQtcmVxdWVzdC1xdW90ZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpe1xuXHR2YXIgdmFsaWRhdGVSZXF1ZXN0UXVvdGVFbGVtZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdCQoICcuc3ViLWZvcm0tbWFuZGF0b3J5JyApLmVhY2goIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYgKCAkKCB0aGlzICkudmFsKCkgPT0gXCJcIiApIHtcbiAgICAgICAgICAgICAgICAkKCB0aGlzICkuYWRkQ2xhc3MoICdpbnZhbGlkLWlucHV0JyApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9ICk7XG5cdH07XG5cblx0Ly8gUmVxdWVzdCBxdW90ZSBzdWJtaXQgaGFuZGxlclxuXHQkKCcucmVxdWVzdC1xdW90ZS1zdWJtaXQtYnRuJykuY2xpY2soZnVuY3Rpb24oKXtcblx0XHR2YWxpZGF0ZVJlcXVlc3RRdW90ZUVsZW1lbnRzKCk7XG5cdFx0aWYgKCAkKCAnLnN1Yi1mb3JtLWlucHV0LWVsZW1lbnQnICkuaGFzQ2xhc3MoICdpbnZhbGlkLWlucHV0JyApICkge1xuICAgICAgICAgICAgJCggJy5pbnZhbGlkLWlucHV0JyApLmZpcnN0KCkuZm9jdXMoKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgIFx0JCgnLmhvdGVscy1ldmVudC1jb25maXJtYXRpb24tcG9wdXAnKS5zaG93KCk7XG4gICAgICAgIH1cblx0fSk7XG5cblx0JCgnLmhvdGVscy1ldmVudC1jb25maXJtYXRpb24tb3ZlcmxheSwgLmV2ZW50LWNvbmZpcm1hdGlvbi1jbG9zZS1pY29uLCAuZXZlbnQtY29uZmlybWF0aW9uLWNsb3NlLWJ0bicpXG5cdFx0LmNsaWNrKGZ1bmN0aW9uKCl7XG5cdFx0XHQkKCcuaG90ZWxzLWV2ZW50LWNvbmZpcm1hdGlvbi1wb3B1cCcpLmhpZGUoKTtcblx0XHR9KTtcblx0JCgnLmhvdGVscy1ldmVudC1jb25maXJtYXRpb24taW5uZXItd3JwJykuY2xpY2soZnVuY3Rpb24oZSl7XG5cdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcblx0fSk7XG5cblx0JCgnLmNtLXBhZ2UtY29udGFpbmVyJykuYmx1cihmdW5jdGlvbigpe1xuXHRcdCQoJy5qaXZhLXNwYS1kYXRlJykucmVtb3ZlQ2xhc3MoJ3Zpc2libGUnKTtcblx0fSk7XG59KTsiXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2hvdGVscy1ldmVudC1yZXF1ZXN0LXF1b3RlL2hvdGVscy1ldmVudC1yZXF1ZXN0LXF1b3RlLmpzIn0=

// analytics code
function prepareJsonForEvents(stepName, jsonObj) {
    console.log("dataLayerData on this page : ", dataLayerData);
    var meetingEventJson = {};
    var pageHirarchy = [];

    if (stepName != undefined) {
        meetingEventJson.pageTitle = stepName;
        if (jsonObj != undefined) {
            jsonObj.venueName = decodeURI(jsonObj.venueName)
            meetingEventJson.hotelOfferingName = jsonObj.venueName;
            meetingEventJson.hotelName = jsonObj.hotelName;
            meetingEventJson.hotelOffering = stepName;
            meetingEventJson.eventType = "";
            meetingEventJson.eventStartDate = jsonObj.eventStartDate;
            meetingEventJson.eventEndDate = jsonObj.eventEndDate;
            meetingEventJson.venueBreakoutRooms = "";
            meetingEventJson.venueCity = dataLayerData.hotelCity;
            meetingEventJson.venueCountry = dataLayerData.hotelCountry;
            meetingEventJson.venueEvent = jsonObj.venueName;
            meetingEventJson.venueGuests = jsonObj.guestCount;
            meetingEventJson.venueSleepingRooms = "";

        }

        if (dataLayerData != undefined) {
            meetingEventJson.pageAuthor = dataLayerData.pageAuthor;
            meetingEventJson.pageLanguage = dataLayerData.pageLanguage;

            pageHirarchy = dataLayerData.pageHierarchy;

            if (stepName == "Venue Selected") {
                pageHirarchy.push("Venue Finder");
            }
            if (stepName == "Request for a Quote") {
                pageHirarchy.push("Request for a Quote");
            }
            meetingEventJson.pageHirarchy = pageHirarchy;

            meetingEventJson.hotelCountry = dataLayerData.hotelCountry;
            meetingEventJson.hotelCode = dataLayerData.hotelCode;
            meetingEventJson.hotelCity = dataLayerData.hotelCity;
        }
    }
    // console.log("meetingEventJson : ", meetingEventJson);
    // console.log("dataLayerData : ", dataLayerData );

    pushHotelJson(meetingEventJson);
}

function initiateServletCall() {
    var tripDetails = [];
    $(".flight-journey-details:visible").each(function() {
        var trip = {};
        trip.departureDate = $(this).find('.departure-date:visible').val();
        trip.departureTime = $(this).find('#customer-departure-time').val();
        trip.departureFromCity = $(this).find('.departure-city-from').val();
        trip.departureToCity = $(this).find('.departure-city-to').val();
        tripDetails.push(trip);
    });
    var requestData = {
        fullName : $('.customer-name-real:visible').val(),
        emailId : $('.customer-email-id:visible').val(),
        mobile : $('.phone-Number:visible').val(),
        numberOfGuests : $('.meeting-events-guest-value:visible').val(),
        organization : $('.organization-name:visible').val(),
        officePhoneNumber : $('.form-office-no:visible').val(),
        anyNonIndianPassengers : $('select.nonIndians').val(),
        country : $('.country-name:visible').val(),
        timingStandard : $('.package-rate-tab-content:visible select.customer-time-standards').val(),
        comments : $('.customer-name-comments:visible').val()

    };

    if (tripDetails && tripDetails.length > 0) {
        // one way and basic trip
        requestData.departureDate = $('.departure-date:visible').val();
        requestData.departureTime = tripDetails[0].departureTime;
        requestData.departureFromCity = tripDetails[0].departureFromCity;
        requestData.departureToCity = tripDetails[0].departureToCity;
        // Return trip
        requestData.returnDate = $('.return-date:visible').val();
        requestData.returnTime = $('.flight-journey-details:visible ').find('#customer-return-time-journey').val();
        // Multi trips
        requestData.tripValues = JSON.stringify(tripDetails);
    }

    $('.spinner-con').show();

    console.log("Initiating AJAX call with requestData :: ", requestData);

    $.ajax({
        type : "GET",
        url : '/bin/requestquotes',
        data : requestData,
        dataType : "json",
        success : function(data) {
            console.log("Ajax call successfull");
            $('.spinner-con').hide();
            $('.hotels-event-confirmation-popup').show();
            $(".cm-page-container").addClass('prevent-page-scroll');
        },
        error : function(textStatus, errorThrown) {
            console.log("Ajax call fail");
            $('.spinner-con').hide();
            $('.hotels-event-error-popup').show();
            $(".cm-page-container").addClass('prevent-page-scroll');
        }

    });

    requestData.tripType = $('.tab-selected').text();
    prepareRequestQuote("requestQuote", requestData);
}
