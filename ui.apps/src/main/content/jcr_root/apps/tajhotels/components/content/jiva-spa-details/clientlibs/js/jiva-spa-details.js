window.addEventListener('load', function() {

    $('.jiva-spa-details-long-desc').cmToggleText({
        charLimit : 150
    });

});

$(document).ready(function() {

    var jivaspaAmount = $("#jivaspaAmount").text();
    if (jivaspaAmount.includes(","))
        jivaspaAmount = jivaspaAmount.replace(/\,/g, "");
    jivaspaAmount = parseInt(jivaspaAmount);
    jivaspaAmount = jivaspaAmount.toLocaleString('en-IN');
    $("#jivaspaAmount").text(jivaspaAmount);

});
