$( document ).ready( function() {

       $('.placeholder-title').each(function() {

       $(this).cmToggleText({
           charLimit: 250,
       });
   });

   var jivaDateWidth = $( '.jiva-spa-date' ).outerWidth();
   var today = new Date();

   $( '.enquiry-from-date' ).datepicker( {
           startDate: today,
           format: 'Do MMM'
   } ).on( 'changeDate', function( element ) {
          if ( $( '.jiva-spa-date' ).hasClass( 'visible' ) ) {
              var minDate = ( new Date( element.date.valueOf() ) );
              // Added by Dinesh for Event quote datepicker                
              var selectedDate = moment( minDate ).format( "Do MMM" );
              $( '.enquiry-from-value' )
                  .val( selectedDate )
                  .removeClass( 'invalid-input' );
              $( this ).removeClass( 'visible' );
              $( '.jiva-spa-date-con' ).removeClass( 'jiva-spa-not-valid' );
              $( '.enquiry-to-date' ).datepicker( 'setStartDate', minDate );
           }
   } );

   $( '.enquiry-from-date' ).datepicker( 'setDate', new Date() );

      $( '.enquiry-to-date' ).datepicker( {
          startDate: today,
          format: 'Do MMM'
     } ).on( 'changeDate', function( element ) {
            if ( $( '.jiva-spa-date' ).hasClass( 'visible' ) ) {
	              var minDate = ( new Date( element.date.valueOf() ) );
    	          // Added by Dinesh for Event quote datepicker                
        	      var selectedDate = moment( minDate ).format( "Do MMM" );
            	  $( '.enquiry-to-value' )
                    .val( selectedDate )
                    .removeClass( 'invalid-input' );
                $( this ).removeClass( 'visible' );
                $( '.jiva-spa-date-con' ).removeClass( 'jiva-spa-not-valid' );
                $( '.enquiry-from-date' ).datepicker( 'setEndDate', minDate );
            }
        } );

     $( '.cm-page-container' ).click( function() {
            $( '.jiva-spa-date' ).removeClass( 'visible' );
     } );
	socialShare();
	
});

function socialShare() {

    var url = window.location.href;    

    $("#socials").jsSocials({
        url : url,        
        showCount : false,
        showLabel : false,
        shares : [ {
            share : "facebook",
            logo : "/content/dam/tajhotels/icons/social-icons/ic-facebook@2x.png"
        }, {
            share : "twitter",
            logo : "/content/dam/tajhotels/icons/style-icons/twitter.svg"
        }, {
            share : "googleplus",
            logo : "/content/dam/tajhotels/icons/social-icons/google+.png"
        }]

    })

}


