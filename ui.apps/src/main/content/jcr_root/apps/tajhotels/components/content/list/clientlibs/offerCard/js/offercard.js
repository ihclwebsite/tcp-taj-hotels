$(document).ready(function() {
    $('.offers-card-description').each(function() {
        $(this).cmToggleText({
            charLimit : 135
        });
    });
});


function onOfferSelection(offerDetailsPath, offerRateCode, offerTitle, noOfNights, offerValidity) {
    // [TIC-DATA-LAYER]
    var offerDetails = {};
    offerDetails.offerCode = offerRateCode;
    offerDetails.offerValidity = offerValidity;
    offerDetails.offerName = offerTitle;
    prepareOfferAndPromotionsJsonAfterSumbitClick('Offers&Promotions', offerDetails);

    var hotelPath = $("[data-hotel-path]").data("hotel-path");
    // default dates on selecting an offer setting to tommorow and (tommorow+offerNights)

    var bookOptions = dataCache.session.getData("bookingOptions");

    if(bookOptions == undefined) {
        bookOptions = {};
    }

    var fromDate = moment(new Date()).add(16, 'days').format("MMM Do YY");
    var nextDate = moment(new Date()).add(17, 'days').format("MMM Do YY");

    bookOptions.fromDate = fromDate;
    bookOptions.toDate = nextDate;
    var websiteId = $('#websiteid').val();
	if (websiteId == 'taj-inner-circle') {
		if (offerStartsFrom) {
			var startsFrom = moment(offerStartsFrom).format('MMM Do YY');
			if (!moment(startsFrom, 'MMM Do YY').isSameOrBefore(
					moment(fromDate, 'MMM Do YY'))) {
				fromDate = startsFrom;
				bookOptions.fromDate = fromDate;
				nextDate = moment(fromDate, "MMM Do YY").add(1, 'days').format(
						"MMM Do YY");
				bookOptions.toDate = nextDate;

			}
		}
	}
    var nights;
    if (noOfNights && noOfNights != '0') {
        nights = noOfNights;
        nextDate = moment(fromDate, "MMM Do YY").add(parseInt(nights), 'days').format("MMM Do YY");
        bookOptions.toDate = nextDate;

    }
    bookOptions.nights = parseInt(moment.duration(
            moment(moment(bookOptions.toDate, "MMM Do YY")).diff(moment(bookOptions.fromDate, "MMM Do YY"))).asDays());

    dataCache.session.setData("bookingOptions", bookOptions);

	var comparableOfferRateCode;

    if (hotelPath) {
        var ROOMS_PATH = "/rooms-and-suites.html";
        var navPath = hotelPath.replace(".html", "");
        navPath = navPath + ROOMS_PATH;
        if (navPath != "" && navPath != null && navPath != undefined) {
            navPath = navPath.replace("//", "/");
        }
        if (offerRateCode) {
            if (comparableOfferRateCode) {
                offerRateCode = offerRateCode + ',' + comparableOfferRateCode;
            }
            navPath = updateQueryString("offerRateCode", offerRateCode, navPath);
            navPath = updateQueryString("offerTitle", offerTitle, navPath);
        }
        navPath = navPath.replace("//", "/");
        if ((!navPath.includes("http://") && navPath.includes("http:/"))
                || (!navPath.includes("https://") && navPath.includes("https:/"))) {
            navPath = navPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        window.location.href = navPath;
    } else {
        offerDetailsPath = offerDetailsPath.replace("//", "/");
        if ((!offerDetailsPath.includes("http://") && offerDetailsPath.includes("http:/"))
                || (!offerDetailsPath.includes("https://") && offerDetailsPath.includes("https:/"))) {
            offerDetailsPath = offerDetailsPath.replace("http:/", "http://").replace("https:/", "https://");
        }
        window.location.href = offerDetailsPath;
    }
}

function onViewDetailsOfferSelection(offerDetailsPath) {
    window.location.href = offerDetailsPath;
}
