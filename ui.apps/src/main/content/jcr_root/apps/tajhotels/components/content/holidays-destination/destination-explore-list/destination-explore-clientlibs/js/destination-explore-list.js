$(document).ready(function(){ 
	fetchAllAddressDoms();
	$('.mr-holiday-destination-about-cards-wrapper').each(function(){
		$(this).showMore();
	});
});


function fetchAllAddressDoms() {
    var addressDomArray = $.find('[data-lat-lng]');

    for (i in addressDomArray) {
        var spanDom = addressDomArray[i];
        var latLng = $(spanDom).data().latLng;
        var lat = latLng.split(",")[0];
        var lng = latLng.split(",")[1];


        getReverseGeocodingData(lat,lng,spanDom);
    }
}



function getReverseGeocodingData(lat,lng,spanDom) {
      var latlng = new google.maps.LatLng(lat, lng);
      var address ="";
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'latLng': latlng }, function (results, status) {
          if (status !== google.maps.GeocoderStatus.OK) {
          }
          if (status == google.maps.GeocoderStatus.OK) {
              for ( var i in results) {
                  if(!results[i].formatted_address.includes("Unnamed Road")) {
						address=results[i].formatted_address;

                      	injectAddressInToDom(spanDom,address);
                       	break;
                  }
              }
          }
      })
  }

function injectAddressInToDom(spanDom,address) {
    return $(spanDom).text(address);
}