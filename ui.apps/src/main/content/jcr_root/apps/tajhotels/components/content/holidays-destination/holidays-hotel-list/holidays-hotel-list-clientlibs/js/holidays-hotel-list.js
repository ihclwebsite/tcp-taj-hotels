$(document).ready(function() {
        formatHotelCardUI();        
        $('.destination-package-card-container').on('click','.holidays-dest-package-card-inclusions-show-more',function(e) {            
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
                $(this).parents().eq(1).addClass('holidays-inclusions-on-show');            
        });
        $('.destination-package-card-container').on('click','.holidays-dest-package-card-inclusions-close',function(e) {            
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');            
        });
        $('.destination-package-card-container').on('click','.holidays-dest-package-card-inclusions-mobile-heading > img',function(e) {            
                e.stopPropagation();
                $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');            
        });

        $('.destination-package-card-container').on('click','.holidays-dest-package-card-btn',function(e) {
            e.stopPropagation();
            if ($(this).html() == 'VIEW PACKAGES') {
                $(this).html('HIDE PACKAGES');
                $(this).parents().eq(2).siblings().addClass('visible');
            } else {
                $(this).html('VIEW PACKAGES');
                $(this).parents().eq(2).siblings().removeClass('visible');
            }
        });
        $('.destination-package-card-container').on('click','.holidays-dest-more-packages, .holidays-dest-package-card-inclusions-content',function(e) {
            e.stopPropagation();
        })

        $('body').click(function(e) {
            $('.holidays-dest-package-card-inclusions-wrap').removeClass('holidays-inclusions-on-show');
            //$('.holidays-dest-package-card-btn').html('VIEW PACKAGES');
            //$('.holidays-dest-more-packages').removeClass('visible');
        })
        $('.destination-package-card-container').each(function() {
            $(this).showMore(4,4);
        });

        $('.destination-package-card-container').on('click','.image-view',function(e){
            $(this).toggleClass('rotate-arrow');
            $(this).siblings('.rate-description-content-second').children('.terms-list').toggleClass('show-list');
        });

        filterBasedOnHolidayCategory();
});

function filterBasedOnHolidayCategory(){    
    if(($('[data-holidaycategory]').length>0) && ($('.cm-page-container').hasClass('holidays-theme-layout'))){
        var holidayCategory = $('[data-holidaycategory]').data().holidaycategory;       
        $('.holidays-dest-more-packages .more-rate-card-container').each(function(){
            if( ($(this).data('theme')===undefined) || $(this).data('theme')!=holidayCategory){
                $(this).remove();
            }
        });
        $('.holidays-dest-package-card-outer-wrap').each(function(){
            var packages = $(this).find('.more-rate-card-container').length;
            if(packages==0) $(this).remove();
        });
        $('.holidays-hotel-list').each(function(){
            var hotels = $(this).find('.holidays-dest-package-card-outer-wrap').length;
            if(hotels==0){
                $(this).prev('.title-link').remove();
                $(this).remove();                   
            }
        });
        $('.holidays-dest-package-card-outer-wrap').each(function(){
            var minRate = Number.POSITIVE_INFINITY;
            var $packages = $(this).find('.more-rate-card-container')
            $packages.each( function() {
                var packagePrice = parseFloat( $( this ).data( 'price' ) );
                if ( packagePrice < minRate ) {
                    minRate = packagePrice;
                }
            } );
            $( this ).find( '.holidays-dest-package-card-rate-actual .amount' ).text( minRate.toLocaleString( 'en-IN' ) );
        });
        $individualDestinationCards = [];
        $( '.destination-package-card-container' ).each( function( index ) {
            $individualDestinationCards.push( $( this ).clone( "true" ) );
        } );        
    }
}
function filterBasedOnNight(nights){    
    $('.holidays-dest-more-packages .more-rate-card-container').each(function(){
        var packageNights = $(this).data('nightsduration');
        $(this).toggle(packageNights==nights);
    });
}
/*function setSession(nights,packageStartDate,packageEndDate) {
   if(nights)   {
        var bookOptions = dataCache.session.getData("bookingOptions");
        var fromDate = moment(bookOptions.fromDate, "MMM Do YY");       
        var packageStart = moment(packageStartDate,'DD/MM/YYYY');       
        if(fromDate<packageStart){
            fromDate = packageStart;
            bookOptions.fromDate = moment(fromDate).format('"MMM Do YY"');
        }
        var nextDate = moment(fromDate, "MMM Do YY").add(parseInt(nights), 'days').format("MMM Do YY");
        bookOptions.toDate = nextDate;
        bookOptions.nights = parseInt(nights);
        bookOptions.roomOptions[0].adults = 2;
        dataCache.session.setData("bookingOptions", bookOptions);
    }
}*/

function setEnquirySessionObj(packageName,hotelName,city) {
    
    console.log("packageName " + packageName);
    console.log("hotelName " + hotelName);
    console.log("city " + city);

    var enquiryDetails = {};
    var locations=[];
    locations.push(hotelName);
    enquiryDetails.packageName=packageName;
    enquiryDetails.packageDestination=city;
	enquiryDetails.selectedHotels=locations;
	dataCache.session.setData('enquiryObject', enquiryDetails);
}

function formatHotelCardUI(){
    $('.holidays-dest-package-card-desc').each(function() {
        $(this).cmToggleText({
            charLimit: 150,
        });
    });
    $('.amount, .rc-bid-amount, .rc-total-amount').each(function(){        
        var amount = parseInt($(this).text().replace(/,/g, ''));
        var formattedAmount = amount.toLocaleString('en-IN');
        $(this).text(formattedAmount);
    })
}