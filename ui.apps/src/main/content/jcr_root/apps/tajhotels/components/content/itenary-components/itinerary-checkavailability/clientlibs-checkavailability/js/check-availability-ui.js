//initializing with default values
var _globalDateOccupancyCombinationString = fetchDateOccupancyAsQueryString();

$(document).ready(function(){
	console.log('hereee.');
	packageSearchCheckAvailability();

});

function addToDatePicker($elem){
    $elem.parent().siblings('#enquiry-to-date').addClass('visible');
    $elem.parent().siblings('#enquiry-to-date').datepicker().on('changeDate',function(element) {
		arrDataInIndiaTimestamp = element.date.valueOf();
		var minDate = (new Date(element.date.valueOf()));
		var selectedDate = moment(minDate).format("DD/MM/YYYY");
		var dateSection = $(this).closest(".jiva-spa-date-section");
		var prevDate = moment(minDate).subtract(1, 'day');
		var nextDate = moment(minDate).add(1, 'day');
		dateSection.find('.check-out-date').val(selectedDate).removeClass('invalid-input');
		dateSection.find('.jiva-spa-date-value').text(selectedDate);
		dateSection.find('.jiva-spa-date').removeClass('visible');
		dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');

		//validations
		dateSection.find('#enquiry-to-date').removeClass('visible');
		enableCheckAvailabilityButton();

		var $fromElem = $(this).parent().parent().siblings('.package-check-in-date-wrp');

        verifyDateRange($(this),minDate);
		if ($(this).datepicker("getDate") >= $fromElem.find('#enquiry-from-date').datepicker("getDate")) {
			$fromElem.find('#enquiry-to-date').datepicker('setDate', nextDate['_d']);
			$fromElem.find('.enquiry-to-value').val(nextDate.format("DD/MM/YYYY"));
		}

	});
}
function addFromDatePicker($elem){
    $elem.parent().siblings('#enquiry-from-date').addClass('visible');
    $elem.parent().siblings('#enquiry-from-date').datepicker().on('changeDate',function(element) {
		arrDataInIndiaTimestamp = element.date.valueOf();
		var minDate = (new Date(element.date.valueOf()));
		var selectedDate = moment(minDate).format("DD/MM/YYYY");
		var dateSection = $(this).closest(".jiva-spa-date-section");
		var nextDate = moment(minDate).add(1, 'day');
		dateSection.find('.check-in-date').val(selectedDate).removeClass('invalid-input');
		dateSection.find('.jiva-spa-date-value').text(selectedDate);
		dateSection.find('.jiva-spa-date').removeClass('visible');
		dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');

		//validations
		dateSection.find('#enquiry-from-date').removeClass('visible');
        var $toElem = $(this).parent().parent().siblings('.package-check-out-date-wrp');
		verifyDateRange($(this),minDate);
		if ($(this).datepicker("getDate") <= $toElem.find('#enquiry-in-date').datepicker("getDate")) {
			$toElem.find('#enquiry-from-date').datepicker('setDate', prevDate['_d']);
			$toElem.find('.enquiry-from-value').val(prevDate.format("DD/MM/YYYY"));
		}
	});
}


function setInitialValForInput() {
    $('#check-availaility-searchbar-input').val("");
    var selectBoxIt2 = $('#packageNoOfRooms').data("selectBoxIt");
    if (selectBoxIt2) {
        selectBoxIt2.selectOption('1');
    }
    disableCheckAvailabilityButton();
}

function packageSearchCheckAvailability() {
    var redirectPath = "";
    var hotelId = "";

    PackageSearchDropdownsInit();
    setInitialValForInput();

    var isAuthorable = $('#isAuthorable').val();
    var tommorow = new Date();
    var dayAfterTommorow = new Date();
    var calStartDate = moment(new Date()).add(1, 'days')['_d'];
    var calStartToDate = moment(new Date()).add(2, 'days')['_d'];
    if (isAuthorable != 'true') {
        tommorow = moment(new Date()).add(17, 'days')['_d'];
        dayAfterTommorow = moment(new Date()).add(18, 'days')['_d'];
        $('.enquiry-from-value').val(moment(tommorow).format("DD/MM/YYYY"));
        $('.enquiry-to-value').val(moment(dayAfterTommorow).format("DD/MM/YYYY"));
    } else {
        var checkinDateString = $('#checkinDate').val();
        var arr = checkinDateString.split('/');
        tommorow.setDate(arr[0]);
        tommorow.setMonth(arr[1] - 1);
        tommorow.setYear(arr[2]);

        var checkoutDateString = $('#checkoutDate').val();
        var arr2 = checkoutDateString.split('/');
        dayAfterTommorow.setDate(arr2[0]);
        dayAfterTommorow.setMonth(arr2[1] - 1);
        dayAfterTommorow.setYear(arr2[2]);
        calStartDate = tommorow < calStartDate ? tommorow : calStartDate;
        calStartToDate = dayAfterTommorow < calStartToDate ? dayAfterTommorow : calStartToDate;
        $('.enquiry-from-value').val(moment(tommorow).format("DD/MM/YYYY"));
        $('.enquiry-to-value').val(moment(dayAfterTommorow).format("DD/MM/YYYY"));

    }
    $('#enquiry-from-date').datepicker({
        startDate : calStartDate
    }).on('changeDate', function(element) {
        if ($(this).hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD/MM/YYYY");
            var nextDate = moment(minDate).add(1, 'day');
            $(this).siblings().children('.enquiry-from-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $(this).siblings().removeClass('jiva-spa-not-valid');
            var $toelem = $(this).parent().parent().siblings('.package-check-out-date-wrp');
            verifyDateRange($(this),minDate);
            if ($(this).datepicker("getDate") >= $toelem.find('#enquiry-to-date').datepicker("getDate")) {
                $toelem.find('#enquiry-to-date').datepicker('setDate', nextDate['_d']);
                $toelem.find('.enquiry-to-value').val(nextDate.format("DD/MM/YYYY"));
            }

            isSynxisCheck();
        }
    });

    $('#enquiry-from-date').datepicker('setDate', tommorow);

    $('#enquiry-to-date').datepicker({
        startDate : calStartToDate
    }).on('changeDate', function(element) {
        if ($('.jiva-spa-date').hasClass('visible')) {
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD/MM/YYYY");
            var prevDate = moment(minDate).subtract(1, 'day');
            $(this).siblings().children('.enquiry-to-value').val(selectedDate).removeClass('invalid-input');
            $(this).removeClass('visible');
            $(this).siblings().removeClass('jiva-spa-not-valid');
            var $fromElem = $(this).parent().parent().siblings('.package-check-in-date-wrp');
            verifyDateRange($(this),minDate);
            if ($(this).datepicker("getDate") <= $fromElem.find('#enquiry-from-date').datepicker("getDate")) {
                $fromElem.find('#enquiry-from-date').datepicker('setDate', prevDate['_d']);
                $fromElem.find('.enquiry-from-value').val(prevDate.format("DD/MM/YYYY"));
            }

            isSynxisCheck();
        }
    });
    $('#enquiry-to-date').datepicker('setDate', dayAfterTommorow);

    var validateEnquiryElements = function() {
        $('.package-search-form').find('.sub-form-mandatory').each(function() {
            if ($(this).val() == "") {
                $(this).addClass('invalid-input');
                invalidWarningMessage($(this));
            }
        });
    }
    // Request quote submit handler
    $('.itinerary-submit-btn').click(function() {
        selectedHotelData();
		console.log('clicked....')
        validateEnquiryElements();
        var invalidCheck = $('.package-search-form .invalid-input');
        if (invalidCheck.length) {
            invalidCheck.first().focus();
            return false;
        } else {
            return true;
        }
    });

    $(".jiva-spa-search-list li").click(function() {
        $(this).closest('.jiva-spa-hotel-input').find('.jiva-spa-mand').removeClass("invalid-input");
    });

    $(document).click(function() {
        $(".jiva-spa-date").removeClass("visible");
    });

    $(".jiva-spa-date-section").click(function(e) {
        e.stopPropagation();
    });

    $('.jiva-spa-date-con').click(function(e) {
        e.stopPropagation();
        $(".jiva-spa-date").removeClass("visible");
        $(this).next(".jiva-spa-date").addClass("visible");
    })

    var checkAvailabilitySearch = new searchComponent('#check-availaility-searchbar-input',
            '#check-availaility-search-results', '#check-availaility-search-results-destinations',
            '.search-destination', noResultsCallback);

    function noResultsCallback() {
        // toggleCheckAvailable("#", false);
        $('#ca-global-re-direct').attr('href', "#");
    }

    function PackageSearchDropdownsInit() {
        var $PackageRooms = $('#packageNoOfRooms');
        $PackageRooms.selectBoxIt().change(function() {
            var roomsIndex = parseInt($(this).val());
            $('.package-no-of-adults').hide();
            $('.package-no-of-adults:lt(' + roomsIndex + ')').show();
            $('.package-no-of-children').hide();
            $('.package-no-of-children:lt(' + roomsIndex + ')').show();

            isSynxisCheck();
        });
        $('.package-occupancy-count-row select').each(function() {
            $(this).selectBoxIt().change(function() {
                isSynxisCheck();
            });
        });
    }

    function isSynxisCheck() {
        var isSynxis = $("#isSynxis").text().trim();
        if (isSynxis == 'true') {
            return true;
        }
    }

    $('#check-availaility-search-results').on("click", '.search-result-item', function() {
        enableCheckAvailabilityButton();
        var offerCodeFromId = $('#offerCode').text().trim();
        var codelabel = $('#codelabel').text().trim();

        redirectPath = $(this).attr('data-redirect-path');
        hotelId = $(this).attr('data-hotel-id').trim();
        if (!isSynxisCheck()) {
            if (offerCodeFromId) {
                var rateTab = "";
                if (codelabel === "promoCode") {
                    rateTab = "&rateTab=PROMOCODE";
                }
                redirectPath = redirectPath + "?" + codelabel + "=" + offerCodeFromId + rateTab;
                if($('#voucherRedemption').val()){
					redirectPath = redirectPath +"&voucherRedemption=" +$('#voucherRedemption').val();
                }
                $('#ca-global-re-direct').attr('href', redirectPath);
            }
        }
    })

}

function disableCheckAvailabilityButton() {
    $('.itinerary-submit-btn').prop('disabled', true);
    $('.itinerary-submit-btn').css('background-image', 'none');
}

function enableCheckAvailabilityButton() {
    $('.itinerary-submit-btn').prop('disabled', false);
    $('.itinerary-submit-btn').css('background-image',
            'linear-gradient(to top, rgb(123, 85, 25), rgb(170, 121, 56) 52%, rgb(210, 152, 81))');
}

function removeElement(elem){
    elem.parent().parent().remove()
}
function populateHotels(elem){
    var ItineraryDetails = dataCache.session.getData('ItineraryDetails');
    if(ItineraryDetails) {
        var hotelData = ItineraryDetails.allHotelsData;
        succcesCallBack(hotelData, elem);
    }
}
function succcesCallBack(response, elem) {
    clearSearchResults();
    updateResults(response, true, elem);
}
function clearSearchResults() {
    //$searchResutlsHotels.empty();
}

function addHotel() {
	var elemToInject = $('#availability-template').html();
    $(".packages-dropdwn-wrp").before(elemToInject);
    updateDatepicker()
}

    function updateResults(results, isHotels, elem) {
        if (isHotels) {
            var websiteResults = elem.parent().find('.search-comp-results-list.website');
                websiteResults.html("");
            for(let i=0; i<results.length; i++){
                websiteResults.append("<li class='search-result-item' onclick='event.stopPropagation(); popularResults($(this));' data-hotel-id='" + results[i].hotelId + "' data-category='" + results[i].category
                                        +"'>" + results[i].hotelName 
                                        + "</li>");
            }
            elem.parent().find('.search-comp-results-container-itinerary').show();
        }
    }

function popularResults($elem) {
	var $searchBarInput = $elem.parents('#check-availaility-search-results-itinerary').parent().find('#check-availaility-searchbar-input');
	
	$searchBarInput.val($elem.text());
	$searchBarInput.attr('data-category', $elem.data('category'));
	$searchBarInput.attr('data-hotel-id', $elem.data('hotel-id'));
	
    enableCheckAvailabilityButton();
	$('.search-comp-results-container-itinerary').hide();
    $('#addHotelTemp').prop('disabled',false);
}

function updateDatepicker(){
    var component = $(".package-search-check-availability-wrp");
    component.find('.enquiry-from-date').datepicker({
        startDate : '-1000y',
        endDate : '1y',
        autoclose : true,
    }).on('changeDate',function(element) {
        if ($(this).hasClass('visible')) {
            arrDataInIndiaTimestamp = element.date.valueOf();
            var minDate = (new Date(element.date.valueOf()));
            var selectedDate = moment(minDate).format("DD/MM/YYYY");
            var dateSection = $(this).closest(".jiva-spa-date-section");
            dateSection.find('[data-form-id="check-availaility-searchbar-input"]').val(selectedDate).removeClass(
                'invalid-input');
            dateSection.find('.jiva-spa-date-value').text(selectedDate);
            dateSection.find('.jiva-spa-date').removeClass('visible');
            dateSection.find('.jiva-spa-date-con').removeClass('jiva-spa-not-valid');
            
            //validations
            if(arrDateInHotelTimestamp) {
                var diff = arrDataInIndiaTimestamp - arrDateInHotelTimestamp;
                removeValidationErrorInDatePicker('hotel', diff, 'before');
            }
        }
    });
}