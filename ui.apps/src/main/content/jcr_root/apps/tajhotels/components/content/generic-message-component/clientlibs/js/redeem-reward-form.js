$(document)
        .ready(
                function() {
                    var redeemObj = dataCache.session.getData("boxValueOfRedeemCard");
                    if (!redeemObj || redeemObj === "") {
                        $('.no-card-selected').addClass('active');
                        $('.mr-product-detalis-wrapper').hide();
                        $('.mr-redeem-form-wrapper').hide();
                    }
                    var finalPoints;
                    var finalPrice;

                    deleteCookie("senderDetails", null);
                    autoPopulate();
                    populateDropDowns();
                    redeemFormValidate();

                    $('.pay-btn-points').on('click', function() {
                        var redeemCardDom = $('.redeem-card-popup-wrapper.row');
                        var redemptionType = $(redeemCardDom).data('card-type');
                        if (validateRedeemInputs(redemptionType) && validatePointsRedeemed()) {
                            redeemPoints($(this), redemptionType);
                        }
                    });

                    $(".suggestion-division").on(
                            "click",
                            ".suggestion-list",
                            function() {

                                if ($(this).hasClass("mr-address-addition-list")) {
                                    $(".new-address-fields").removeClass("mr-hide");
                                    $(".new-address-fields-hide").addClass("mr-hide");
                                    $("#addressinput").val("New Address")
                                } else {
                                    if (($(this).data("country") === "INDIA")
                                            || ($(this).data("country") === "UNITED STATES")
                                            || ($(this).data("country") === "UNITED KINGDOM")) {
                                        $(".new-address-fields").addClass("mr-hide");
                                        $(".new-address-fields-hide").removeClass("mr-hide");
                                        document.querySelector('#stateinput').value = $(this).data("state");
                                        document.querySelector('#cityinput').value = $(this).data("city");
                                        document.querySelector('#zipcodeinput').value = $(this).data("zipcode");
                                        document.querySelector('#countryinput').value = $(this).data("country");
                                    }
                                }
                            });

                    function validateRedeemInputs(redemptionType) {
                        var flag = true;
                        if (redemptionType == "online") {
                            $('.mr-input-field-online:visible').each(function() {
                                if ($(this).val() == "") {
                                    $(this).siblings('.validate-inputs-txt').addClass('warning-display');
                                    flag = false;

                                }
                            });
                        } else {
                            $('.mr-input-field-offline:visible').each(function() {
                                if ($(this).val() == "") {
                                    $(this).siblings('.validate-inputs-txt').addClass('warning-display');
                                    flag = false;

                                }
                            });
                        }
                        return flag;
                    }

                    function validatePointsRedeemed() {
                        var flag = true;
                        var finalPoint = $('#finalpoint').text();
                        if (finalPoint == "0") {
                            flag = false;
                        }
                        return flag;
                    }

                    function autoPopulate() {
                        var userDetails = getUserData();
                        if (userDetails) {
                            $('#memberEmail').val(userDetails.email);

                            $(".suggestion-division")
                                    .append(
                                            '<li class="suggestion-list mr-address-addition-list"> <div class="mr-address-addition">Ship to New Address +</div>  </li>');
                            // Fill Address Dropdown.
                            var addresses = userDetails.addresses;
                            $("#countryinput").val('India');
                            generateAddressDropDown(addresses);

                            var alternatePhoneNumber = "";
                            var alternatePhoneNumbers = userDetails.alternatePhoneNumbers;
                            if (alternatePhoneNumbers) {
                                alternatePhoneNumbers.forEach(function(phone) {
                                    var primary = phone.primary;
                                    if (primary == true) {
                                        alternatePhoneNumber = phone.alternatePhone;
                                    }
                                });
                            }
                            if (alternatePhoneNumber != undefined && alternatePhoneNumber != "") {
                                $('#contactnumberinput').val(alternatePhoneNumber);
                            }

                            $('#memberNumber').val(userDetails.membershipId);
                            $('#memberNumber').attr("disabled", true);
                            $('.membership-name').val(userDetails.firstName + " " + userDetails.lastName).attr(
                                    'disabled', true);
                            $('#memberEmail').val(userDetails.email).attr('disabled', true);
                        }

                    }

                    function generateAddressDropDown(addresses) {

                        for ( var address in addresses) {
                            if (addresses.hasOwnProperty(address)
                                    && ((addresses[address].country === "INDIA")
                                            || (addresses[address].country === "UNITED STATES") || (addresses[address].country === "UNITED KINGDOM"))) {
                                var val = addresses[address];

                                document.querySelector('#stateinput').value = val.state;
                                document.querySelector('#cityinput').value = val.city;
                                document.querySelector('#zipcodeinput').value = val.postalCode;

                                $(".suggestion-division").append(
                                        '<li class="suggestion-list" data-country="' + val.country + '" data-city="'
                                                + val.city + '" data-state="' + val.state + '" data-zipcode="'
                                                + val.postalCode + '"> <div class="mr-suggestion-address">'
                                                + val.street1 + ', ' + val.street2 + ', ' + val.city + ', ' + val.state
                                                + ', ' + val.postalCode
                                                + '</div> <div class="mr-select-btn">Select</div>  </li>');

                            }
                        }
                    }

                    function onlyUnique(value, index, self) {
                        return self.indexOf(value) === index;
                    }

                    function redeemPoints(pointsRedeemedType, redemptionType) {
                        var userDetails = getUserData();
                        var redeemCardDom = $('.redeem-card-popup-wrapper.row');
                        var country;
                        var state;
                        var city;
                        var address;
                        var email = $('#memberEmail').val();
                        var zipCode = $('#zipcodeinput').val();
                        var contactNo = $('#contactnumberinput').val();
                        var memberNumber = userDetails.membershipId;
                        var currency = $("#voucher-currency option:selected").text();
                        var denomination = $("#voucher-denomination option:selected").text();

                        var reciepientEmailId = $("#recipientEmail").val();
                        var reciepientName = $("#recipientName").val();

                        // [TIC-DATA-LAYER]
                        // prepareGifCardPayJsonForClick('PayforGiftCards', redeemObj);
                        // Member details
                        var memberName = userDetails.name;
                        var valueGiftCertificate = $(".receivable-voucher-value").val();

                        if ($(".new-address-fields-hide:visible").length) {
                            country = $('.redeem-country-input:visible').val();
                            state = $('.redeem-stateinput:visible').val();
                            city = $('.redeem-cityinput:visible').val();
                            address = $('#addressinput').val();
                        } else {
                            country = $('select.redeem-country-input').val();
                            state = $('select.redeem-stateinput').val();
                            city = $('select.redeem-cityinput').val();
                            address = $("#ep-address-1").val() + "," + $("#ep-address-2").val() + "," + city + ","
                                    + state + "," + country + "," + zipCode;
                        }

                        var redeemPoints = $('#finalpoint').text();

                        if (redemptionType == 'offline') {
                            if (address == "" || country == "" || state == "" || city == "" || zipCode == ""
                                    || contactNo == "") {
                                return;
                            }
                        } else if (redemptionType == 'online') {
                            country = $('#countryinput').val();
                            state = $('#stateinput').val();
                            city = $('#cityinput').val();
                            address = city + ", " + state + ", " + country + ", " + zipCode;
                        }

                        var productType = $('#productName');
                        var pointsRedeemedType = $('#pointType');
                        var quantityInDom = $('input.gift-quantity').val();
                        var quantity = quantityInDom == undefined ? 1 : quantityInDom;
                        var formData = new FormData();
                        formData.append("MemberNumber", memberNumber);
                        formData.append("RedeemPoints", redeemPoints);
                        formData.append("Quantity", quantity);
                        formData.append("PointsRedeemedType", pointsRedeemedType.text());
                        formData.append("ProductName", productType.text());
                        formData.append("mode", redemptionType);
                        formData.append("streetAddress1", address);
                        formData.append("city", city);
                        formData.append("zipcode", zipCode);
                        formData.append("state", state);
                        formData.append("country", country);
                        formData.append("email", email);
                        formData.append("memberName", memberName), formData.append("valueGiftCertificate", $(
                                redeemCardDom).data('price'));
                        formData.append("reciepientEmailId", reciepientEmailId);
                        formData.append("reciepientName", reciepientName);
                        formData.append("firstName", userDetails.firstName);
                        formData.append("lastName", userDetails.lastName);
                        formData.append("tier", userDetails.card.tier);
                        var selectedPoints = $("input[name='select-points-type']:checked").val()
                        var selectedPointsJson = JSON.parse(selectedPoints);
                        formData.append("point_type", selectedPointsJson.pointsType);
                        formData.append("productType", $(redeemCardDom).data('product-type'));
                        formData.append("itemcode", $(redeemCardDom).data('itemcode'));
                        formData.append("sal", userDetails.title);
                        formData.append("mobile", contactNo);
                        formData.append("currency", currency);
                        formData.append("denomination", denomination);

                        // [TIC-ANALYTICS]
                        var redeemPayformData = {};
                        redeemPayformData.RedeemPoints = redeemPoints;
                        redeemPayformData.Quantity = quantity;
                        redeemPayformData.PointsRedeemedType = pointsRedeemedType.text();
                        redeemPayformData.ProductName = productType.text();
                        redeemPayformData.valueGiftCertificate = $(redeemCardDom).data('price');
                        redeemPayformData.reciepientEmailId = reciepientEmailId;
                        redeemPayformData.reciepientName = reciepientName;
                        redeemPayformData.point_type = selectedPointsJson.pointsType;
                        redeemPayformData.itemcode = $(redeemCardDom).data('itemcode');

                        $('body').showLoader(true);
                        $.ajax({
                            cache : false,
                            contentType : false,
                            processData : false,
                            type : 'POST',
                            url : '/bin/giftcard',
                            data : formData,
                            success : function(response) {
                                var showVar = JSON.parse(response);
                                if (showVar.errorMsg) {
                                    var popupParams = {
                                        title : showVar.errorMsg,
                                    }
                                    warningBox(popupParams);

                                } else {
                                    // [TIC-ANALYTICS]
                                    prepareGifCardPayJsonForClick('PayforGiftCards', redeemPayformData);

                                    var trans = ' Your transactionId is ::' + showVar.transactionId + ' orderID is ::'
                                            + showVar.orderId;
                                    $('.ajaxres').text(trans);
                                    $('.card-name').html(productType);
                                    $('.card-redemption-succesfull').addClass('active');
                                    $('.mr-product-detalis-wrapper').hide();
                                    $('.mr-redeem-form-wrapper').hide();
                                    sessionStorage.setItem("boxValueOfRedeemCard", "");
                                    fetchMemberPoints(userDetails.membershipId);
                                }
                            },
                            error : function(response) {
                            },
                            complete : function() {
                                $('body').hideLoader(true);
                            }
                        })
                    }
                    function populateDropDowns() {
                        /* This method gets country state city dropdown values from location.js file */
                        var countryDropDown = $('#newAddressCountry');
                        var stateDropDown = $('#NewAddressState');
                        var cityDropDown = $('#NewAddressCity');
                        var redeemGift = dataCache.session.getData("boxValueOfRedeemCard");
                        var countryOptions = [];
                        countryOptions = [ 'INDIA', 'UNITED STATES', 'UNITED KINGDOM' ]

                        countryDropDown.selectBoxIt({
                            populate : countryOptions
                        }).change(function() {
                            var selectedCountry = $(this).val();
                            var countryIndex = locations.findIndex(function(location) {
                                return location.countryName === selectedCountry;
                            });
                            var statesList = locations[countryIndex].states.map(function(location) {
                                return location.state;
                            });
                            stateDropDown.data("selectBox-selectBoxIt").remove();
                            cityDropDown.data("selectBox-selectBoxIt").remove();
                            stateDropDown.data("selectBox-selectBoxIt").add(statesList);

                            $("#finalpoint").html(finalPoints + " " + payButton);
                        });

                        stateDropDown.selectBoxIt().change(function() {
                            var selectedCountry = countryDropDown.val();
                            var selectedState = $(this).val();
                            var countryIndex = locations.findIndex(function(location) {
                                return location.countryName === selectedCountry;
                            });
                            var states = locations[countryIndex].states;
                            var stateIndex = states.findIndex(function(location) {
                                return location.state === selectedState;
                            });
                            var cityList = states[stateIndex].cities.map(function(location) {
                                return location.cityName;
                            });
                            cityDropDown.data("selectBox-selectBoxIt").remove();
                            cityDropDown.data("selectBox-selectBoxIt").add(cityList);
                        });
                    }

                    function deleteCookie(cname, cvalue){
                        var d = new Date();
                        d.setTime(d.getTime() + 10000));
                        var expires = "expires=" + d.toGMTString();
                        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                    }

                });

function sendToSelf(input) {
    if (input.is(":checked")) {
        $('#recipientEmail').val($('#memberEmail').val()).attr("disabled", true);
        $('#recipientName').val($('.membership-name').val()).attr("disabled", true);
    } else {
        $('#recipientEmail').val('').attr("disabled", false);
        $('#recipientName').val('').attr("disabled", false);
    }
}

function ticRedeemRewardDetailsForAnalytics() {
    var redeemObj = dataCache.session.getData("boxValueOfRedeemCard");

    if (redeemObj) {
        giftCardDetails = {
            giftCardType : redeemObj.heading,
            pointsType : redeemObj.pointType.pointsType,
            giftCardValue : redeemObj.finalPrice,
            ponitsToBeRedeemed : redeemObj.finalPoints,
            giftCardQuantity : redeemObj.cardQuantity
        }
    }
    return giftCardDetails;
}
