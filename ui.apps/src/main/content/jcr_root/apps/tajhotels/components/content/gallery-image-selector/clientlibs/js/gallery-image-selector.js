var GALLERY_SELECTOR_IMAGE_STATIC_LIST_DIV_ID = "gallery-image-selector-static-option";
var GALLERY_SELECTOR_IMAGE_STATIC_LIST_OPTION_VALUE = "static";

var GALLERY_SELECTOR_IMAGE_CHILDREN_DIV_ID = "gallery-image-selector-list-option";
var GALLERY_SELECTOR_IMAGE_CHILDREN_OPTION_VALUE = "children";

$(document).on(
        "dialog-ready",
        function() {
            galleryImageSelectorDialogReady();
        });

function galleryImageSelectorDialogReady() {
    defineVariables();
    gallerySourceSelectionChanged($.find(".coral-Form-field[name='./gallery/./galleryImageSource']"));

}

/**
 * When the script is loaded in dialog, it is NOT defining the above variables, hence adding a method that shall be
 * called to define them.
 * 
 * @returns
 */
function defineVariables() {
    GALLERY_SELECTOR_IMAGE_STATIC_LIST_OPTION_VALUE = "static";
    GALLERY_SELECTOR_IMAGE_STATIC_LIST_DIV_ID = "gallery-image-selector-static-option";

    GALLERY_SELECTOR_IMAGE_CHILDREN_DIV_ID = "gallery-image-selector-list-option";
    GALLERY_SELECTOR_IMAGE_CHILDREN_OPTION_VALUE = "children";
};

function gallerySourceSelectionChanged(element) {
    var elementValue = $(element).val();
    var selectedOption = '';
    if(elementValue == undefined || elementValue == ''){
        var firstElement = $(element).get(0);
        if(firstElement != undefined){
            var selectedItem = firstElement.selectedItem;
            if(selectedItem != undefined){
                selectedOption = selectedItem.value;
            }
        }
    } else {
        selectedOption = elementValue;
    }

    var parentOfCurrentSelection = $(element).parents(".gallery-image-selector");

    if (selectedOption == GALLERY_SELECTOR_IMAGE_CHILDREN_OPTION_VALUE) {
        $(parentOfCurrentSelection).find("#" + GALLERY_SELECTOR_IMAGE_STATIC_LIST_DIV_ID).hide();
        $(parentOfCurrentSelection).find("#" + GALLERY_SELECTOR_IMAGE_CHILDREN_DIV_ID).show();
    } else if (selectedOption == GALLERY_SELECTOR_IMAGE_STATIC_LIST_OPTION_VALUE) {
        $(parentOfCurrentSelection).find("#" + GALLERY_SELECTOR_IMAGE_STATIC_LIST_DIV_ID).show();
        $(parentOfCurrentSelection).find("#" + GALLERY_SELECTOR_IMAGE_CHILDREN_DIV_ID).hide();
    }
}
