var contextualBanners = [];

$(document).ready(
        function() {
//            ihclImageCarousel();
            hideControlsMobile();
            hideControlsSingleBanner();
            $("#bannerCarousel").on("slid.bs.carousel", "", hideControlsMobile);

            /*
            $("#bannerCarousel").on("touchstart", function(event) {
                var xClick = event.originalEvent.touches[0].pageX;
                $(this).one("touchmove", function(event) {
                    var xMove = event.originalEvent.touches[0].pageX;
                    if (Math.floor(xClick - xMove) > 5) {
                        $(this).carousel('next');
                    } else if (Math.floor(xClick - xMove) < -5) {
                        $(this).carousel('prev');
                    }
                });
                $("#bannerCarousel").on("touchend", function() {
                    $(this).off("touchmove");
                });
                return false;
            });
            */

                var isSafari = navigator.userAgent.indexOf('Safari') != -1 && navigator.vendor ==  "Apple Computer, Inc.";
                if (isSafari && document.getElementById("videoPlaySafari")){
                    document.getElementById("videoPlaySafari").style.visibility = "visible";
                } 

                var videoPlaySafari = document.getElementById("videoPlaySafari");
                if(videoPlaySafari){
                $("#videoPlaySafari").on("touchstart click",function(){
                    var vid = document.getElementById("videoAudio");
                    //vid.play();
                    document.getElementById("videoPlaySafari").style.display = "none";
                        /*  if(vid.muted){
                        var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/mute-48.png";
                            vid.muted = false;
                        }else{
                            var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/volume-up-2-48.png";
                            vid.muted = true;
                        }
                        */
                     var playPromise = vid.play();
                      if (playPromise !== undefined) {
                        playPromise.then(function(d) {
                          document.getElementById("videoPlaySafari").style.display = "none";
                          if(vid.muted){
                        var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/mute-48.png";
                            vid.muted = false;
                        }else{
                            var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/volume-up-2-48.png";
                            vid.muted = true;
                        }
                        }, function(err){
							console.log('auto play is muted');
                        })
                    }
                    return false;
                });
                }
    
                var muteVideo = document.getElementById("muteVideo");
                if(muteVideo){
                $("#muteVideo").on("touchstart click",function(){
                    var vid = document.getElementById("videoAudio");
                    vid.play();
                     if(vid.muted){
                        var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/mute-48.png";
                            vid.muted = false;
                        }else{
                            var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/volume-up-2-48.png";
                            vid.muted = true;
                        }
                     /*var playPromise = vid.play();
                      if (playPromise !== undefined) {
                        playPromise.then(_ => {
                          if(vid.muted){
                        var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/mute-48.png";
                            vid.muted = false;
                        }else{
                            var volumeImage = document.getElementById("muteVideo");
                            volumeImage.src= "/content/dam/tajhotels/icons/style-icons/volume-up-2-48.png";
                            vid.muted = true;
                        }
                        })
                        .catch(error => {
                          // Auto-play was prevented
                          // Show paused UI.
                        });
                    }*/
                    return false;
                });
                }
            
            if(document.getElementById("fullHeight")){

			var fullheight= document.getElementById("fullHeight").value;
            console.log(fullheight);
            if( fullheight=="true")
            {
                $(".carousel, .mr-carousel-wrap, .mr-carousel-wrap .banner-carousel-each-wrap .bannerImage img, .mr-carousel-wrap .banner-carousel-each-wrap, .mr-carousel-wrap img.img-hotel, .banner-carousel>.banner-container-wrapper>.hotel-carousel-section .carousel-item").css('height', '93vh');

                /*
                $(".carousel",".mr-carousel-wrap",".mr-carousel-wrap .banner-carousel-each-wrap img",
                 ".mr-carousel-wrap .banner-carousel-each-wrap",".mr-carousel-wrap img.img-hotel",
                 "#bannerCarousel .carousel-item").css("height","93vh");
				*/

            }

            }


            // hide left, right control on mobile viewport
            function hideControlsMobile() {
                var $this = $("#bannerCarousel");
                if (window.matchMedia('(max-width: 767px)').matches) {
                    $this.children(".carousel-control-prev").hide();
                    $this.children(".carousel-control-next").hide();
                }
            }
            ;

            // hide controls if there is a single banner
            function hideControlsSingleBanner() {
                var $this = $("#bannerCarousel");
                var banners = $this.find(".carousel-item");
                var indicators = $this.find(".carousel-indicators");
                if (banners.length === 1) {
                    indicators.hide();
                    $this.children(".carousel-control-prev").hide();
                    $this.children(".carousel-control-next").hide();
                }
            }

            // Login related
            if ($("#bannerCarousel").data("login-support")) {
                registerLoginListener(updateUserCarousel);
                if (userCacheExists()) {
                    updateUserCarousel();
                }
            }

            // Update the first banner slide with user name
            function updateUserCarousel() {
                var banner = $("#bannerCarousel");
                var userData = getUserData();
                var firstSlide = banner.find(".carousel-item").get(0);
                $(firstSlide).find(".tic-banner-main-heading").text("Welcome " + userData.name);
            }

            if (contextualBanners.length > 0) {
                profileFetchListener(updateContextualBanner)
                if (userCacheExists()) {
                    updateContextualBanner();
                }
            }

            function updateContextualBanner() {
                var userData = getUserData();
                contextualBanners.forEach(function(banner) {
                    if (userData.tier && banner.context.toLowerCase().includes(userData.tier.toLowerCase())) {
                        $("#bannerCarousel .carousel-inner").prepend(banner.dom);
                        var ind = $('#bannerCarousel .carousel-indicators li');
                        $('#bannerCarousel .carousel-indicators').append(
                                '<li data-target="#bannerCarousel" data-slide-to="' + (ind.length) + '"></li>');
                    }
                });
                $('#bannerCarousel').carousel(0);
            }

            getBannerDataForDataLayer();
        });

function registerContextualBanner(contextBannerKey, context) {
    var banner = document.getElementById(contextBannerKey);
    var obj = {
        context : context,
        dom : banner
    };
    contextualBanners.push(obj);
}


//updated for global data layer
function getBannerDataForDataLayer(){
    $('.mr-visit-hotel').each(function(){
        $(this).click(function(){
            try {
                var carousalBtnTxt = $(this).text().split(' ').join('');
                var bannerLabel = $(this).parent('a').siblings('.cm-header-label').text().split(' ').join('');
                var eventName =  bannerLabel+'_CarouselBanner_'+carousalBtnTxt+'_HomePage_'+carousalBtnTxt;
                addParameterToDataLayerObj(eventName, {});
            }
            catch(err){
				console.log('error in creating eventName');
            }
        });
	});

}