/*Component - Filter dropdowns*/

/*The four keys of "dropDowns" object has a key "selected"
All the selected values for the corresponding dropdown will be available in 
selected key of each dropdown object in "dropDowns".*/


document.addEventListener( 'DOMContentLoaded', function() {
    var options = {};
    var $countryDropdown = $( '#countryDropdown' );
    var $cityDropdown = $( '#cityDropdown' );
    var $hotelDropdown = $( '#hotelDropdown' );
    var $suitsDropdown = $( '#suitsDropdown' );
    var $filterSectionButton = $( '.filterSectionButton' );
    var $filter_mobileView = $( '.filter_mobileView' );

    var dropDowns = {
        country: {
            options: [ 'America', 'India', 'Indonesia', 'Japan' ],
            elem: $countryDropdown,
            default: 'Country',
            selected: null,
            dependent: {
                elem: $cityDropdown
            }
        },
        city: {
            options: [ 'Bangalore', 'Pune', 'Delhi' ],
            elem: $cityDropdown,
            default: 'City',
            selected: null,
            dependent: {
                elem: $hotelDropdown
            }
        },
        hotel: {
            options: [ 'Name1', 'Name2', 'Name3', 'Name4' ],
            elem: $hotelDropdown,
            default: 'Hotel',
            selected: null,
            dependent: {
                elem: $suitsDropdown
            }
        },
        suits: {
            options: [ 'Name1', 'Name2', 'Name3', 'Name4' ],
            elem: $suitsDropdown,
            default: 'Rooms & Suits',
            selected: null,
            dependent: {
                elem: null
            }
        }
    }

    initDropdown.prototype.initialize = function() {
        var itrLength = this.options.length;
        for ( i = 0; i < itrLength; i++ ) {
            this.targetElem.append( '<option>' + this.options[ i ] + '</option>' )
        };

        //Initializes selectboxit on the rendered dropdown
        this.targetElem.selectBoxIt();

        // Add listeners to each selectbox change
        this.listenDDChange();
    };

    initDropdown.prototype.listenDDChange = function() {
        var dTarget = this.targetBlock;
        dTarget.elem.change( function() {
            var selectedOption = ( dTarget.elem ).find( "option:selected" );
            dTarget.selected = selectedOption.text();
            if ( dTarget.dependent.elem ) {
                dTarget.dependent.elem.selectBoxIt( 'selectOption', 0 );
                if ( dTarget.selected != dTarget.default ) {
                    dTarget.dependent.elem.prop( "disabled", false );
                } else {
                    dTarget.dependent.elem.prop( "disabled", true );
                }
            } else {
                if ( dTarget.selected != dTarget.default ) {
                    // "dropDowns" Gives the selected values for each dropdown options
                }
            }
        } );
    };

    function init() {

        var countryDropdown = new initDropdown( $countryDropdown, dropDowns.country );
        var cityDropdown = new initDropdown( $cityDropdown, dropDowns.city );
        var hotelDropdown = new initDropdown( $hotelDropdown, dropDowns.hotel );
        var suitsDropdown = new initDropdown( $suitsDropdown, dropDowns.suits );

        countryDropdown.initialize();
        cityDropdown.initialize();
        hotelDropdown.initialize();
        suitsDropdown.initialize();

        $cityDropdown.prop( "disabled", true );
        $hotelDropdown.prop( "disabled", true );
        $suitsDropdown.prop( "disabled", true );

    };

  //  init();

    $( '.filterSectionButton' ).click( function() {
        $filterSectionButton.css( 'display', 'none' );
        $filter_mobileView.css( 'display', 'block' );
    } );

    $( '.filterBackArrow' ).add( '.filter-button' ).click( function() {
        $filterSectionButton.css( 'display', 'block' );
        $filter_mobileView.css( 'display', 'none' );
        if ( $countryDropdown.find( "option:selected" ).text() != 'Country' ) {
            $( '.filterSectionButton' ).html( '<img src="/content/dam/tajhotels/icons/style-icons/filter-applied.svg" alt  = "filter-applied-icon">' );
        }
    } );


} );

function initDropdown( element, target ) {
    this.targetBlock = target;
    this.options = target.options;
    this.targetElem = element;
};