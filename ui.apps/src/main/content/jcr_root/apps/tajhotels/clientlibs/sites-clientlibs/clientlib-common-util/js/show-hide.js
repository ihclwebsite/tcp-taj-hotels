$.fn.cmToggleText = function( options ) {
    var charRangePercent = 40;
    var defaultVal = {
        charLimit: 580,        
        showVal: "Show More",
        hideVal: "Show Less",
        ellipsisText: "..."
    };
    options = $.extend( {}, defaultVal, options );
    charLimit = options.charLimit;
    var content = this.html();    
    if(content){        
        var contentLength = content.length;
        var contentPercentTextLength = parseInt((contentLength * charRangePercent)/100); 
        var contentTotalLength =  options.charLimit + contentPercentTextLength;

        if (( contentLength > contentTotalLength ) ) {
            var show = content.substr( 0, options.charLimit );
            var hide = content.substr( options.charLimit - 1 );

            var html1 = '<span>' + show + '</span><span>' + options.ellipsisText + '</span><span class="show-more-ell cm-view-txt"><button class="btn-only-focus">' + options.showVal + '</button></span>'

            this.html( html1 );
            this.append( '<span class="full-description-text cm-hide">' + content + '</span>' );
        }

        var self = this;

        this.on( 'click', '.show-more-ell', function() {
            var parentReference = $( this ).parent();
            parentReference.html( content + '<span class="show-less cm-view-txt"><button class="btn-only-focus">' + options.hideVal + '</button></span>' );
        } );

        this.on( 'click', '.show-less', function() {
            var parentReference = $( this ).parent();
            parentReference.html( content.substr( 0, options.charLimit ) + '</span><span>' + options.ellipsisText + '</span><span class="show-more-ell cm-view-txt"><button class="btn-only-focus">' + options.showVal + '</button></span>' );
        } );
    }
};
