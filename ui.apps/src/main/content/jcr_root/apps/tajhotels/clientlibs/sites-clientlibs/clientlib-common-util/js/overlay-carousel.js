$( document ).ready( function() {
    $( '#mr-menu-carousel.carousel' ).carousel( "pause" );


    $( "#mr-menu-carousel" ).on( "touchstart", function( event ) {
        var xClick = event.originalEvent.touches[ 0 ].pageX;
        $( this ).one( "touchmove", function( event ) {
            var xMove = event.originalEvent.touches[ 0 ].pageX;
            if ( Math.floor( xClick - xMove ) > 5 ) {
                $( this ).carousel( 'next' );
            } else if ( Math.floor( xClick - xMove ) < -5 ) {
                $( this ).carousel( 'prev' );
            }
        } );
        $( "#mr-menu-carousel" ).on( "touchend", function() {
            $( this ).off( "touchmove" );
        } );
    } );

    $( '.mr-carousel-close-icon-wrap' ).on( 'click', function() {
        setTimeout( function() {
            $( '.mr-menu-carousel-overlay' ).addClass( 'mr-overlay-initial-none ' );
        } );
    } );

    var carouselCount = $( '.carousel-inner .carousel-item' ).length,
        currentCarousel = 0;
    $( '.mr-currentCarousel' ).text( '1' );
    $( '.mr-carouselTotalCount' ).text( carouselCount );
    $( '#mr-menu-carousel.carousel' ).on( 'slid.bs.carousel', function( e ) {
        $( '.mr-currentCarousel' ).text( ( $( '.mr-menu-carousel-indicators ol li.active' ).data( 'slideTo' ) ) + 1 );
    } );
    
    $(document).keydown(function(e) {
        if ($('.mr-menu-carousel-overlay').hasClass('mr-overlay-initial-none')) {
            //do nothing;
        } else {
            switch (e.which) {
                case 39:
                    $('.mr-carousel-rightArrow').trigger("click");
                    break;
                case 37:
                    $('.mr-carousel-leftArrow').trigger("click");
                    break;
                case 27:
                    $('.mr-carousel-close-icon').trigger("click");
                    break;
            }
        }
    });
    
    $('#mr-menu-carousel.carousel').bind('slid.bs.carousel', function (e) {

        $('ol.mr-carousel-indicators-overlay li').hide();
        $('ol.mr-carousel-indicators-overlay li').addClass("cm-minimise-bullets");
        var activeCarouselDot = $('ol.mr-carousel-indicators-overlay li.active');
        //activeCarouselDot.prev().show();
        activeCarouselDot.show().removeClass("cm-minimise-bullets");
        
        var prevDot = activeCarouselDot.prev();
        var nextDot = activeCarouselDot.next();
        
        if((prevDot.length > 0) && (nextDot.length > 0)) {
        	prevDot.show();
        	nextDot.show();
        } else if((prevDot.length == 0)&& (nextDot.length > 0)) {
        	nextDot.show();
        	nextDot.next().show();
        } else if ((prevDot.length > 0) && (nextDot.length == 0)) {
        	prevDot.show();
        	prevDot.prev().show();
        } else {
        	prevDot.show();
        	nextDot.show();
        }
        /*activeCarouselDot.next().removeClass("cm-minimise-bullets");
        activeCarouselDot.prev().removeClass("cm-minimise-bullets");*/
        //activeCarouselDot.prev().prev().show();
    });
} );