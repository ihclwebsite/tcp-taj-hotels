$.fn.cmTrimText = function(options) {
    var defaultVal = {
        charLimit : 200,
        showVal : "Show More",
        hideVal : "Show Less",
        ellipsisText : "..."
    };
    options = $.extend({}, defaultVal, options);
    charLimit = options.charLimit;
    var content = $.trim(this.html());
    if (content && (content.length > options.charLimit)) {
        var show = content.substr(0, options.charLimit);
        var html1 = show + options.ellipsisText;
        this.html(html1);
    }
};
