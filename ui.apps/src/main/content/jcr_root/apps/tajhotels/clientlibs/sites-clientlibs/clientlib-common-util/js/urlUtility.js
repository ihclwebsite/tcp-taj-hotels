function getCorrectedURL(inputURL) {
    if (inputURL != "" && inputURL != null && inputURL != undefined) {
        inputURL = inputURL.replace("//", "/").replace("//", "/");
    }

    if ((!inputURL.includes("http://") && inputURL.includes("http:/"))
            || (!inputURL.includes("https://") && inputURL.includes("https:/"))) {
        inputURL = inputURL.replace("http:/", "http://").replace("https:/", "https://");
    }

    return inputURL;
}

function getShortUrl(inputURL) {
    if (inputURL.indexOf('/en-in/') != -1) {
        return inputURL.substr(inputURL.indexOf('/en-in/')).replace('.html', '');
    }

    return inputURL;
}
