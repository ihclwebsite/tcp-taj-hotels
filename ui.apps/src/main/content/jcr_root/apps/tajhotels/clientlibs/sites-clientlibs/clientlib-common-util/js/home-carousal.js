( function( $ ) {
    $.fn.customSlide = function( slidesperView, notEndToEndCarousel ) {

        //notEndToEndCarousel : This parameter is passed for the cards that are to be made
        //as carousel inside a component. i.e, these cards will not appear as carousel 
        //on the page directly. For the cards which appears as a carousel on page directly
        //"notEndToEndCarousel" params is not needed.
        //"notEndToEndCarousel" - cards are not rendering between device left and right end.

        this.addClass( 'the-page-carousel' );
        var cards = this.children().children();
        var totalCardsInSlide = cards.length;

        var slideElement = this;
        var slidesperView = slidesperView;
        var windowWidth = $( window ).width();
        var leftArrow;
        var rightArrow;

        this.parent().css( 'position', 'relative' );
        if ( totalCardsInSlide > slidesperView ) {
        	this.parent().find('.leftArrow').remove();
        	this.parent().find('.rightArrow').remove();
            this.parent().prepend( '<div class="leftArrow"><span class="icon-carousel-arrow-coloured-left"></span></div>' );
            this.parent().append( '<div class="rightArrow"><span class="icon-carousel-arrow-coloured-right"></span></div>' );
            leftArrow = this.siblings( ".leftArrow" );
            rightArrow = this.siblings( ".rightArrow" );

            leftArrow.css( 'display', 'none' );
        }


        var containerWidth = this.width();
        if(containerWidth==0){
            containerWidth = $('.content-wrapper.container').width();
        }


        var marginToBeUsed = 16;

        if ( slidesperView == 1 ) {
            marginToBeUsed = 5;
        }

        var eachCardWidth = ( containerWidth - ( marginToBeUsed * ( slidesperView - 1 ) ) ) / slidesperView;


        if ( windowWidth > 991 ) {
            cards.css( 'width', eachCardWidth );
            cards.css( 'marginRight', marginToBeUsed );

            $( cards[ cards.length - 1 ] ).css( 'marginRight', 0 );
        }

        var totalCards = cards.length;
        var cardWidth = cards.outerWidth( true );

        if ( windowWidth <= 991 ) {
            marginToBeUsed = 16;
            var cardConWidthAdd = 0;
            cards.css( 'marginLeft', marginToBeUsed );
            if ( windowWidth <= 767 ) {
                var singleCardWidth = notEndToEndCarousel ? ( 100 + '%' ) : ( 92 + '%' );
                cardWidth = ( containerWidth * 0.80 );
                cardConWidthAdd = marginToBeUsed;
                $( cards[ cards.length - 1 ] ).css( 'marginRight', marginToBeUsed );

                if ( totalCardsInSlide == 1 ) {
                    cards.css( 'width', singleCardWidth );
                    cards.css( 'margin', 'auto' );
                    cards.css( 'float', 'none' );
                    return;
                }
            } else {
                if ( !notEndToEndCarousel ) {
                    cardWidth = ( containerWidth * 0.40 );
                } else {
                    cardWidth = ( containerWidth * 0.80 );
                }
                $( cards[ 0 ] ).css( 'marginLeft', 0 );
                cardConWidthAdd = -16;
            }

            cards.css( 'width', ( cardWidth ) );

            if ( notEndToEndCarousel ) {
                $( cards[ cards.length - 1 ] ).css( 'marginRight', 0 );
                slideElement.children().css( 'width', ( ( cardWidth * totalCards ) - 16 ) + 'px' );
            } else {
                slideElement.children().css( 'width', ( ( cardWidth * totalCards ) + cardConWidthAdd ) + 'px' );
            }

        } else {
            totalCards == 1?marginToBeUsed =0:marginToBeUsed; 
            slideElement.children().css( 'width', ( ( totalCards * cardWidth ) - marginToBeUsed ) + 'px' );
        }


        slideElement.children().css( 'display', 'flex' );

        var limit = Math.floor( totalCards / slidesperView );
        var lastSlide = Math.floor( ( totalCards ) % slidesperView );
        if ( rightArrow && ( limit == 1 && lastSlide == 0 ) ) {
            rightArrow.css( 'display', 'none' );
        }

        var counter = 1;
        if ( rightArrow ) {
            rightArrow.on( 'click', function() {
                leftArrow.css( 'display', 'block' );
                slideElement.animate( {
                    scrollLeft: '+=' + ( cardWidth * slidesperView )
                }, 400 );;
                if ( counter == ( limit - 1 ) && ( lastSlide == 0 ) ) {
                    rightArrow.css( 'display', 'none' );
                } else if ( counter == limit ) {
                    rightArrow.css( 'display', 'none' );
                }
                counter++;
            } );
        }

        if ( leftArrow ) {
            leftArrow.on( 'click', function() {
                rightArrow.css( 'display', 'block' );
                slideElement.animate( {
                    scrollLeft: '-=' + ( cardWidth * slidesperView )
                }, 400 );
                if ( counter == 2 ) {
                    leftArrow.css( 'display', 'none' );
                }
                counter--;
            } );
        }

        return this;
    }
}( jQuery ) );