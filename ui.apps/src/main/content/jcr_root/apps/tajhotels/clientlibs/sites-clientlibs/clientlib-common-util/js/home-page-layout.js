document
        .addEventListener(
                'DOMContentLoaded',
                function() {
                    var holidayTheme = dataCache.session.getData("holidayTheme");
                    if ($('.cm-page-container').hasClass('holiday-theme') || holidayTheme) {
                        dataCache.session.setData("holidayTheme", true);
                    } else {
                        dataCache.session.setData("holidayTheme", false);
                    }
                    $('.executive-leader-profile-card-carousel-wrp').each(function() {
                        $(this).showMore();
                    });

                    $(".calender-events-wrap").customSlide(3);
                    $(".profile-card-carousel-wrp").customSlide(3);
                    $('.awardscontainer-wrap').customSlide(3);
                    $('.awardscontainer-wrap').children().css('background', '#fff');
                    $('.popular-destination-wrap').each(function() {
                        $(this).customSlide(4);
                    });
                    $('.offers-and-deals-wrap').customSlide(3);
                    $('.hotel-overview-carousel-wrap').customSlide(4);
                    // for holiday trending section
                    $('.holiday-trending-offers-wrapper').each(function() {
                        $(this).customSlide(3);
                    });
                    $('.discover-more-wrapper').each(function() {
                        $(this).customSlide(2);
                    });

                    // for holiday destination page
                    $('.holiday-popularDest-wrap').customSlide(3);
                    $('.holiday-otherDest-wrap').showMore();
                    $('.holiday-international-wrap').customSlide(3);
                    $(
                            '.mr-destination-dining-search .search-input-section input, .mr-specific-dining-search .search-input-section input')
                            .on('click', function() {
                                if (window.matchMedia('(max-width:767px)').matches) {
                                    $('.search-filter-section').hide();
                                    $('.search-input-section').width('99%');
                                }
                            });

                    if (deviceDetector.checkDevice() != "small") {
                        $('.mr-our-brands-container-inner').customSlide(3);
                    }

                    $(
                            '.mr-destination-dining-search .search-input-section input, .mr-specific-dining-search .search-input-section input')
                            .on('blur', function() {
                                if (window.matchMedia('(max-width:767px)').matches) {
                                    $('.search-filter-section').show();
                                    $('.search-input-section').width('40%');
                                }
                            });

                    // international getaway holiday -on click view details
                    $(
                            '.holidays-dest-international .cm-btn-primary.offers-button ,.holidays-dest-international .cm-header-offers')
                            .on(
                                    'click',
                                    function() {
                                        location.href = '../../../markup/pages/holidays/holidays-international-destination.html';
                                    })

                    // holiday-landing view all popular destination
                    // international getaway holiday -on click view details
                    $(
                            '.holidays-land-popularDest .cm-btn-primary.offers-button ,.holidays-land-popularDest .cm-header-offers')
                            .on('click', function() {
                                location.href = '../../../markup/pages/holidays/holidays-destination.html';
                            })
                    // for Sustainability cards and for who-we are landing page

                    $(".sustainability-container").each(function() {
                        $(this).showMore();
                    });
                    // Partners flying

                    $(".flying-partner-container-col-two").showMore();
                    $(".flying-partner-container-col-three").showMore();

                    // corporate governance

                    if (deviceDetector.checkDevice() == "small") {
                        $('.corporate-gov-title-text').cmToggleText({
                            charLimit : 250,
                            showVal : "Show More",
                            hideVal : "Show Less",
                        });
                    } else {
                        $('.corporate-gov-title-text').cmToggleText({
                            charLimit : 900,
                            showVal : "show More",
                            hideVal : "Show Less",
                        });
                    }

                    customizeSelectDropDownArrow();

                    $('.datepicker-days').each(function() {
                        $(this).find('th.prev').html('<i class="icon-prev-arrow inline-block"/>');
                        $(this).find('th.next').html('<i class="icon-prev-arrow inline-block cm-rotate-icon-180"/>');
                    })

                    // trigger click on keyboard enter
                    $(document).on('keydown', '[tabindex]', function(event) {
                        if (event.keyCode === 13) {
                            event.preventDefault();
                            $(this).trigger('click');
                        }
                    })
                });
function customizeSelectDropDownArrow() {
    $("select").each(function() {
        $(this).selectBoxIt({
            downArrowIcon : "icon-drop-down-arrow"
        });
    });
    $('.selectboxit-default-arrow').each(function() {
        $(this).parent('.selectboxit-arrow-container').append('<span class="icon-drop-down-arrow"></span>');
        $(this).remove();
    });
}
