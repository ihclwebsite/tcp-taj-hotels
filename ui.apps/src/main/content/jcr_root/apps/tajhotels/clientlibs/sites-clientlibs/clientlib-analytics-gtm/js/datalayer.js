//[TIC-DATA-LAYER] Variable
var deviceType = '';

/* document.addEventListener("DOMContentLoaded", function(event) { */
$(document).ready(function() {
    try {
        if (dataLayerData && dataLayerData.hotelName && !(dataLayerData.hotelName.includes(dataLayerData.hotelCity))) {
            dataLayerData.hotelName = dataLayerData.hotelName + ", " + dataLayerData.hotelCity;
            console.debug("dataLayerData.hotelName doesn't contain city. Adding it", pageLevelData.hotelName);
        }
        globalBookingOption = dataCache.session.getData("bookingOptions");
    } catch (err) {
        console.error('exception caught in datalayer js');
        console.error(err);
    }
});

function pushHotelJson(ecommerce) {
    dataLayer.push(ecommerce);
}
function pushRoomsJsonOnClick(eventClick, ecommerce) {
    var roomClickjson = {};
    roomClickjson.event = eventClick;
    roomClickjson.ecommerce = ecommerce;
    dataLayer.push(roomClickjson);
    setUpSessionData();
}

function setUpSessionData() {
    var modifyBookingOption;
    if (dataCache.session.getData("bookingOptions") != undefined) {
        modifyBookingOption = dataCache.session.getData("bookingOptions");
        modifyBookingOption.hotelChainCode = CHAIN_ID || pageLevelData.hotelChainCode;
        dataCache.session.setData("bookingOptions", modifyBookingOption);
        dataCache.session.setData("CHAIN_ID", CHAIN_ID);
        HName = modifyBookingOption.targetEntity;
        CurrCode = modifyBookingOption.currencySelected;
        globalBookingOption = modifyBookingOption;
    }
    ConvertedCurrCode = CurrCode;
    SelectedCurrCode = CurrCode;
}

function pushEvent(eventName, viewName, eventData) {
    event = eventName;
    ViewName = viewName;
    if (eventData != undefined) {
        eventData.event = eventName;
        eventData.ViewName = viewName;
        dataLayer.push(eventData);
    }
    setUpSessionData();
}
function pushEventAma(eventName, viewName, amaDestinationLayerData) {
    event = eventName;
    ViewName = viewName;
    amaDestinationLayerData.event = eventName;
    amaDestinationLayerData.ViewName = viewName;
    dataLayer.push(amaDestinationLayerData);
    setUpSessionData();
}

function countBookAStayGuests(globalBookingOption) {
    var adultQuantity = 0;
    var childQuantity = 0;
    roomOptions = globalBookingOption.roomOptions;
    if (globalBookingOption.rooms > 0) {
        $.each(roomOptions, function(index, value) {
            adultQuantity = parseInt(adultQuantity) + parseInt(value.adults);
            childQuantity = parseInt(childQuantity) + parseInt(value.children)
        });
        AdultQty = adultQuantity;
        ChildQty = childQuantity;
        GuestQty = parseInt(AdultQty) + parseInt(ChildQty);
    }
}
function getArrivalDepartDtDiff(globalBookingOption) {
    ArrivalDt = globalBookingOption.fromDate || globalBookingOption.checkInDate;
    ArrivalDtMMDDYYYY = getArrivalDt(ArrivalDt);
    DepartDt = globalBookingOption.toDate || globalBookingOption.checkOutDate;
    DepartDtMMDDYYYY = getDepartDt(DepartDt);
    DaysToCheckIn = diffInDate(ArrivalDtMMDDYYYY, DepartDtMMDDYYYY);
    return DaysToCheckIn;
}
function getArrivalDt(ArrivalDt) {
    var tempArrivalDt = '';
    tempArrivalDt = ArrivalDt.replace("st", "").replace("nd", "").replace("rd", "").replace("th", "");
    ArrivalDtDDMMYYYY = GetFormattedDate(tempArrivalDt, "DD/MM/YYYY");
    ArrivalDtMMDDYYYY = GetFormattedDate(tempArrivalDt, "MM/DD/YYYY");
    ArrivalDtYYYYMMDD = GetFormattedDate(tempArrivalDt, "YYYY/MM/DD");
    return ArrivalDtMMDDYYYY;
}
function getDepartDt(DepartDt) {
    var tempDepartDt = '';
    tempDepartDt = DepartDt.replace("st", "").replace("nd", "").replace("rd", "").replace("th", "");
    DepartDtDDMMYYYY = GetFormattedDate(tempDepartDt, "DD/MM/YYYY");
    DepartDtMMDDYYYY = GetFormattedDate(tempDepartDt, "MM/DD/YYYY");
    DepartDtYYYYMMDD = GetFormattedDate(tempDepartDt, "YYYY/MM/DD");
    return DepartDtMMDDYYYY;
}
function prepareGlobalHotelListJS() {
    var eventData = {};
    globalBookingOption = dataCache.session.getData("bookingOptions");
    if (globalBookingOption != undefined) {
        countBookAStayGuests(globalBookingOption);
        DaysToCheckIn = getArrivalDepartDtDiff(globalBookingOption);
        HOTEL_ID = globalBookingOption.hotelId;
        CHAIN_ID = CHAIN_ID || globalBookingOption.hotelChainCode;
        AvailabilityFilters = pageLevelData ? pageLevelData.pageTitle : "";
        if ((HOTEL_ID == null || HOTEL_ID == undefined || HOTEL_ID == "")
                && templateName == 'tajhotels/components/structure/destination-landing-page') {
            HotelList = prepareHotelsJson(); // prepares Hotels list

        } else if ((HOTEL_ID != null && HOTEL_ID != undefined && HOTEL_ID != "")) {
            HotelList = prepareRoomsListJson(); // prepares Hotels list
        }
        if (LangCode != undefined || LangCode != "") {
            LangCode = pathLocale || pageLevelData ? pageLevelData.pageLanguage : "";
        }
        if (isAmaTheme) {
            SelectedCurrCode = 'INR';
        } else {
            SelectedCurrCode = $('.selected-currency').data('selected-currency');
        }
        var hotelData = {};
        if (templateName == 'tajhotels/components/structure/hotels-landing-page') {
            eventData.SelectedHotel = SelectedHotel;

            hotelData = pageLevelData ? pageLevelData : {};
        } else {
            hotelData = dataLayerData;
        }

        NightsQty = globalBookingOption.nights || DaysToCheckIn;
        RoomNightsQty = NightsQty || DaysToCheckIn;
        RmQty = globalBookingOption.rooms;
        eventData.AdultQty = AdultQty;
        eventData.ChildQty = ChildQty;
        eventData.GuestQty = GuestQty;
        eventData.ArrivalDt = ArrivalDt;
        eventData.ArrivalDtDDMMYYYY = ArrivalDtDDMMYYYY;
        eventData.ArrivalDtMMDDYYYY = ArrivalDtMMDDYYYY;
        eventData.ArrivalDtYYYYMMDD = ArrivalDtYYYYMMDD;
        eventData.AvailabilityFilters = AvailabilityFilters;
        eventData.CHAIN_ID = CHAIN_ID;
        eventData.ChainNm = ChainNm;
        eventData.DepartDt = DepartDt;
        eventData.DepartDtDDMMYYYY = DepartDtDDMMYYYY;
        eventData.DepartDtMMDDYYYY = DepartDtMMDDYYYY;
        eventData.DepartDtYYYYMMDD = DepartDtYYYYMMDD;
        eventData.DaysToCheckIn = DaysToCheckIn;
        eventData.HOTEL_ID = HOTEL_ID;
        eventData.HotelList = HotelList;
        eventData.LangCode = LangCode;
        eventData.SelectedCurrCode = SelectedCurrCode;
        eventData.NightsQty = NightsQty;
        eventData.RmQty = RmQty;
        eventData.RoomNightsQty = RoomNightsQty;
        eventData.hotelCity = hotelData.hotelCity;
        eventData.hotelCode = hotelData.hotelCode;
        eventData.hotelCountry = hotelData.hotelCountry;
        eventData.hotelName = hotelData.hotelName;
        eventData.brandName = hotelData.hotelBrand;
        eventData.pageAuthor = hotelData.pageAuthor;
        eventData.pageHierarchy = pageHierarchy;
        eventData.pageLanguage = hotelData.pageLanguage;
        eventData.pageTitle = pageTitle;
        if (dataCache.local.getData("userDetails")) {
            eventData.membershipId = dataCache.local.getData("userDetails").membershipId;
        }
    }
    return eventData;
}
function prepareAmaDataLayer() {
    try {
        globalBookingOption = dataCache.session.getData("bookingOptions");
        if (globalBookingOption != undefined) {
            countBookAStayGuests(globalBookingOption);
            DaysToCheckIn = getArrivalDepartDtDiff(globalBookingOption);
            CHAIN_ID = CHAIN_ID || globalBookingOption.hotelChainCode;
            AvailabilityFilters = pageLevelData.pageTitle;
            if ((HOTEL_ID == null || HOTEL_ID == undefined || HOTEL_ID == "")
                    && templateName == 'tajhotels/components/structure/destination-landing-page') {
                HotelList = prepareHotelsJson(); // prepares Hotels list
            } else if ((HOTEL_ID != null && HOTEL_ID != undefined && HOTEL_ID != "")) {
                HotelList = prepareRoomsListJson(); // prepares Hotels list
            }
            if (LangCode != undefined || LangCode != "") {
                LangCode = pathLocale || pageLevelData.pageLanguage;
            }
            if (isAmaTheme) {
                SelectedCurrCode = 'INR';
            } else {
                SelectedCurrCode = $('.selected-currency').data('selected-currency');
            }
            NightsQty = globalBookingOption.nights || DaysToCheckIn;
            RoomNightsQty = NightsQty || DaysToCheckIn;
            RmQty = globalBookingOption.rooms;
            amaDestinationLayerData.AdultQty = AdultQty;
            amaDestinationLayerData.ChildQty = ChildQty;
            amaDestinationLayerData.GuestQty = GuestQty;
            amaDestinationLayerData.ArrivalDt = ArrivalDt;
            amaDestinationLayerData.ArrivalDtDDMMYYYY = ArrivalDtDDMMYYYY;
            amaDestinationLayerData.ArrivalDtMMDDYYYY = ArrivalDtMMDDYYYY;
            amaDestinationLayerData.ArrivalDtYYYYMMDD = ArrivalDtYYYYMMDD;
            amaDestinationLayerData.AvailabilityFilters = AvailabilityFilters;
            amaDestinationLayerData.CHAIN_ID = CHAIN_ID;
            amaDestinationLayerData.ChainNm = ChainNm;
            amaDestinationLayerData.DepartDt = DepartDt;
            amaDestinationLayerData.DepartDtDDMMYYYY = DepartDtDDMMYYYY;
            amaDestinationLayerData.DepartDtMMDDYYYY = DepartDtMMDDYYYY;
            amaDestinationLayerData.DepartDtYYYYMMDD = DepartDtYYYYMMDD;
            amaDestinationLayerData.DaysToCheckIn = DaysToCheckIn;
            amaDestinationLayerData.HOTEL_ID = HOTEL_ID;
            amaDestinationLayerData.HotelList = HotelList;
            amaDestinationLayerData.LangCode = LangCode;
            amaDestinationLayerData.SelectedCurrCode = SelectedCurrCode;
            amaDestinationLayerData.NightsQty = NightsQty;
            amaDestinationLayerData.RmQty = RmQty;
            amaDestinationLayerData.RoomNightsQty = RoomNightsQty;
            if (dataLayerData) {
                amaDestinationLayerData.hotelCity = dataLayerData.hotelCity;
                amaDestinationLayerData.hotelCode = dataLayerData.hotelCode;
                amaDestinationLayerData.hotelCountry = dataLayerData.hotelCountry;
                amaDestinationLayerData.hotelName = dataLayerData.hotelName;
                amaDestinationLayerData.brandName = dataLayerData.hotelBrand;
                amaDestinationLayerData.pageAuthor = dataLayerData.pageAuthor;
                amaDestinationLayerData.pageHierarchy = pageHierarchy;
                amaDestinationLayerData.pageLanguage = dataLayerData.pageLanguage;
                amaDestinationLayerData.pageTitle = pageTitle;
            }
        }
    } catch (err) {
        console.error('caught exception in function prepareAmaDataLayer');
    }
    return amaDestinationLayerData;
}
function prepareRoomsListJson() {
    HotelList = [];
    var hotelCardObj = {};
    var position, hotelId, hotelBrand, hotelName;
    hotelName = dataLayerData.hotelName || $("div").find('[data-hotel-name]').data('hotel-name') || "";
    hotelCardObj.CHAIN_ID = CHAIN_ID;
    hotelCardObj.HOTEL_ID = dataCache.session.getData('bookingOptions').hotelId
            || $("div").find('[data-hotel-id]').data('hotel-id');
    hotelCardObj.HName = hotelName;
    if (dataCache.session.getData('bookingOptions').rooms > 0) {
        hotelCardObj.DatesAvailable = true;
    }

    hotelCardObj.City = $("div").find('[data-hotel-name]').data('hotel-city') || dataLayerData.hotelCity;
    hotelCardObj.Country = $("div").find('[data-hotel-name]').data('hotelcountry') || dataLayerData.hotelCountry;
    hotelCardObj.CurrCode = $('.selected-currency').data('selected-currency');
    if ($('.cm-page-container').hasClass('ama-theme')) {
        hotelCardObj.CurrCode = 'INR';
    }
    if (isAmaTheme) {
        hotelCardObj.BunglowCode = $("div").find('[data-bunglowcode]').data('bunglowcode') || dataLayerData.bunglowCode;
    }
    // hotelCardObj.Price = $(this).data('price');
    hotelCardObj.StateProv = $("div").find('[data-hotel-name]').data('hotel-state') || dataLayerData.hotelState;
    hotelCardObj.Zip = $("div").find('[data-hotel-name]').data('zipcode') || dataLayerData.hotelPinCode;
    SelectedHotel.City = hotelCardObj.City;
    SelectedHotel.Country = hotelCardObj.Country;
    SelectedHotel.DestinationList = SelectedHotel.City + ", ALL THPRS HOTELS";
    SelectedHotel.HName = hotelName;
    SelectedHotel.HOTEL_ID = hotelCardObj.HOTEL_ID;
    SelectedHotel.StateProv = hotelCardObj.StateProv;
    SelectedHotel.Zip = hotelCardObj.Zip;
    HotelList.push(hotelCardObj);
    return HotelList;
}

function prepareHotelsJson() {
    HotelList = [];
    var hotelsList = $('.mr-lists-view-hotels-Container .list-view-wrapper');

    $.each(hotelsList, function(index, value) {
        var hotelCardObj = {};
        var position, hotelId, hotelBrand, hotelName;
        if (isAmaTheme) {
            hotelName = $(this).find('.card-wrapper-title-only-mob').text().trim();
        } else {
            hotelName = $(this).find('.mr-list-hotel-title-offer-wrap .row .col-8 .mr-list-hotel-title').text().trim();
        }
        hotelCardObj.CHAIN_ID = CHAIN_ID;
        hotelCardObj.HOTEL_ID = $(this).data('hotelid')
        hotelCardObj.HName = hotelName;
        if (AvailHotels > 0) {
            hotelCardObj.DatesAvailable = true;
        }
        if (isAmaTheme) {
            hotelCardObj.City = $(this).data('hotel-city');
            hotelCardObj.BunglowCode = $(this).data('hotelroomcode');
        } else {
            hotelCardObj.City = $('.banner-titles .cm-header-label-con .cm-header-label').text();
        }
        hotelCardObj.Country = $(this).data('hotelcountry');
        if (isAmaTheme) {
            hotelCardObj.CurrCode = 'INR';
        } else {
            hotelCardObj.CurrCode = $('.selected-currency').data('selected-currency');
        }
        hotelCardObj.Price = $(this).data('price');
        hotelCardObj.StateProv = $(this).data('state');
        hotelCardObj.Zip = $(this).data('zipcode');
        HotelList.push(hotelCardObj);
    });
    return HotelList;
}

// cart-layout.js
function prepareRoomsDataOnCart(roomsData) {
    // var roomsData= dataCache.session.getData( "bookingOptions" );
    var products = [];
    var count = 0;
    var variant;
    roomDataArray = roomsData.selection;
    $(roomDataArray).each(
            function(index) {
                var roomCardObj = {};

                roomCardObj.name = this.title;
                roomCardObj.id = this.roomTypeCode + this.ratePlanCode;
                if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
                    roomCardObj.price = (parseInt(this.selectedRate)).toFixed(2).toString();
                } else {
                    roomCardObj.price = (parseInt(this.selectedRate) / parseInt(roomsData.nights)).toFixed(2)
                            .toString();
                }
                roomCardObj.brand = dataLayerData.hotelName;
                var ratePlanNameVar = $(
                        $(roomsData.selection[count].details).find('.more-rate-amenities .more-rate-title')).text();
                roomCardObj.category = ratePlanNameVar;
                roomCardObj.variant = this.promoCode || "";
                roomCardObj.position = count + 1;
                count++;
                products.push(roomCardObj);

                // [TIC-FLOW]
                var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
                if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
                    $('#summary-taxes-charges').addClass('hidden');
                }
            });
    return products;
}

function addingRoomToCart(roomsData) {

    var ecommerce = {};
    ecommerce.currencyCode = roomsData.currencySelected;
    if ($('.cm-page-container').hasClass('ama-theme')) {
        ecommerce.currencyCode = 'INR'
    }
    ecommerce.products = prepareRoomsDataOnCart(roomsData);
    pushRoomsJsonOnClick("addToCart", ecommerce);
    console.log("ecommerce BOOKING CART added: ", ecommerce);
}

function droppingRoomFromCart(roomsData, indexOfCard) {
    var ecommerce = {};
    var count = 0;
    var variant;
    var roomDataAtIndex = roomsData.selection[indexOfCard];
    var roomCardObj = {};
    roomCardObj.name = roomDataAtIndex.title;
    roomCardObj.id = roomDataAtIndex.roomTypeCode + roomDataAtIndex.ratePlanCode;
    if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
        roomCardObj.price = (parseInt(roomDataAtIndex.selectedRate)).toFixed(2).toString();
    } else {
        roomCardObj.price = (parseInt(roomDataAtIndex.selectedRate) / parseInt(roomsData.nights)).toFixed(2).toString();
    }
    roomCardObj.brand = dataLayerData.hotelName;
    var ratePlanNameVar = $($(roomsData.selection[count].details).find('.more-rate-amenities .more-rate-title')).text();
    roomCardObj.category = ratePlanNameVar;
    roomCardObj.variant = this.promoCode || "";
    roomCardObj.position = indexOfCard + 1;
    ecommerce.remove = roomCardObj;
    pushRoomsJsonOnClick("removeFromCart", ecommerce);
    console.log("ecommerce BOOKING CART removed: ", ecommerce);
}
// booking-flow.js
// Analytic data preparing
function prepareRoomsJsonForClick(rateCardWrap) {
    var ecommerce = {};
    var click = {};
    var actionField = {};
    actionField.list = 'Room Results';
    click.actionField = actionField;
    click.products = prepareRoomsJson(rateCardWrap);
    ecommerce.click = click;
    pushRoomsJsonOnClick("productClick", ecommerce);
}

function prepareRoomsJson(rateCardWrap) {
    var products = [];
    var positionCount;
    var roomPosition, roomHotelBrand, roomName, price, category, variant, ratePlanCode, roomTypeCode, roomTypeCodeObj;
    roomName = $(rateCardWrap).data('room-type-name');
    roomTypeCodeObj = $(rateCardWrap).data('bed-room-map');
    for ( var keyPart in roomTypeCodeObj) {
        roomTypeCode = roomTypeCodeObj[keyPart];
    }
    var moreRatesCardList = $(rateCardWrap).find('.more-rate-card-container');
    $(moreRatesCardList).each(
            function(index) {
                var roomCardObj = {};
                var moreRatesCardTitleElement = $(this).find('.more-rate-card .more-rate-amenities .more-rate-title');
                var moreRatesCardRatePlanCode = moreRatesCardTitleElement.data('rate-plan-code');

                roomCardObj.name = roomName;
                roomCardObj.id = roomTypeCode + moreRatesCardRatePlanCode;
                if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
                    roomCardObj.price = (parseInt($(this).find(
                            '.more-rate-card .more-rate-info-wrp .rate-info-wrp div .present-rate .rc-total-amount')
                            .text().replace(/,/g, ""))).toFixed(2).toString() || false;
                } else {
                    roomCardObj.price = (parseInt($(this).find(
                            '.more-rate-card .more-rate-info-wrp .rate-info-wrp div .present-rate .rc-total-amount')
                            .text().replace(/,/g, "")) / NightsQty).toFixed(2).toString() || false;
                }
                roomCardObj.brand = dataLayerData.hotelName;
                roomCardObj.category = moreRatesCardTitleElement.text();
                roomCardObj.variant = this.promoCode || "";
                roomCardObj.position = index + 1;
                products.push(roomCardObj);
            });
    return products;
}

function prepareRoomsImpression() {
    var impressions = [];
    var positionCount = 0;
    var currencyCode = $('.nav-item .nav-link .selected-currency.inline-block').data('selected-currency');
    var selectedFilter = {};
    var selectedFilterDetails = {};
    selectedFilterDetails.rateFilterCode = $('.rate-tab-container .rate-tab.tab-selected').data("rate-filter-code");
    selectedFilterDetails.rateFilterTitle = $('.rate-tab-container .rate-tab.tab-selected div').html();
    selectedFilter.selectedFilter = selectedFilterDetails;
    var roomsRateArr = $('.rate-card-wrap');
    $(roomsRateArr)
            .each(
                    function() {
                        var rateCardWrap = this;
                        var roomPosition, roomHotelBrand, roomName, price, priceWithoutTax, category, variant, ratePlanCode, roomTypeCode, roomTypeCodeObj, roomCardObj2 = {};
                        roomName = $(rateCardWrap).data('room-type-name');
                        roomCardObj2.RoomShortDesc = $(this).find('.rate-card-room-description span:first').text()
                                .replace("...Show More", "...");
                        roomCardObj2.RoomName = roomName;
                        roomCardObj2.RoomLongDesc = $(this).find('.full-description-text.cm-hide span:first').text()
                                || roomCardObj2.RoomShortDesc;
                        roomTypeCodeObj = $(rateCardWrap).data('bed-room-map');
                        for ( var keyPart in roomTypeCodeObj) {
                            roomTypeCode = roomTypeCodeObj[keyPart];
                        }
                        var moreRatesCardList = $(rateCardWrap).find('.more-rate-card-container');
                        $(moreRatesCardList)
                                .each(
                                        function(index) {
                                            if (index == 0) {
                                                var roomCardObj = {};
                                                var moreRatesCardTitleElement = $(this).find(
                                                        '.more-rate-card .more-rate-amenities .more-rate-title');
                                                var moreRatesCardRatePlanCode = moreRatesCardTitleElement
                                                        .data('rate-plan-code');
                                                roomCardObj.name = roomName;
                                                roomCardObj.id = roomTypeCode + moreRatesCardRatePlanCode;
                                                if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
                                                    roomCardObj.price = (parseInt($(this)
                                                            .find(
                                                                    '.more-rate-card .more-rate-info-wrp .rate-info-wrp div .present-rate .rc-total-amount')
                                                            .text().replace(/,/g, ""))).toFixed(2).toString() || false;
                                                } else {
                                                    roomCardObj.price = (parseInt($(this)
                                                            .find(
                                                                    '.more-rate-card .more-rate-info-wrp .rate-info-wrp div .present-rate .rc-total-amount')
                                                            .text().replace(/,/g, "")) / NightsQty).toFixed(2)
                                                            .toString() || false;
                                                }
                                                priceWithoutTax = (parseInt($(this).find(
                                                        '.rate-info-wrp .rate-tax-info').text()) / NightsQty)
                                                        .toFixed(2).toString()
                                                        || "";
                                                roomCardObj.brand = dataLayerData.hotelName;
                                                roomCardObj.category = moreRatesCardTitleElement.text();
                                                roomCardObj.variant = this.promoCode || "";
                                                roomCardObj.position = positionCount + 1;
                                                positionCount++;
                                                // creating different roomCardObject for the global JS variable
                                                if (globalBookingOption.selection != undefined) {
                                                    roomCardObj2.CategoryCode = $(
                                                            '.rate-tab-container .rate-tab.tab-selected').data(
                                                            "rate-filter-code");
                                                    roomCardObj2.CategoryDescription = "";
                                                    roomCardObj2.CategoryName = $(
                                                            '.rate-tab-container .rate-tab.tab-selected div').html();
                                                    roomCardObj2.InventoryWarning = "";
                                                }
                                                roomCardObj2.Price = roomCardObj.price;
                                                roomCardObj2.PriceType = currencyCode;
                                                roomCardObj2.PriceWithTax = parseInt(((roomCardObj.price + "").replace(
                                                        ",", "")))
                                                        + parseInt((priceWithoutTax + "").replace(",", "")).toFixed(2)
                                                                .toString();
                                                roomCardObj2.RateCode = moreRatesCardRatePlanCode;
                                                roomCardObj2.RateName = moreRatesCardTitleElement.text();
                                                roomCardObj2.RateShortDesc = $(this)
                                                        .find(
                                                                '.rate-description .rate-description-content-first .rate-plan-description-txt')
                                                        .text();
                                                roomCardObj2.RateLongDesc = roomCardObj2.RateShortDesc || "";
                                                roomCardObj2.RoomCode = roomTypeCode;
                                                impressions.push(roomCardObj);
                                                AvailableRooms.push(roomCardObj2);
                                            }
                                        });
                    });
    HName = dataLayerData.hotelName;
    ConvertedCurrCode = currencyCode;
    CurrCode = currencyCode;
    var ecommerce = {};
    if ($('.cm-page-container').hasClass('ama-theme')) {
        ecommerce.CurrCode = 'INR';
        ecommerce.ConvertedCurrCode = 'INR';
        ecommerce.AvailableRooms = AvailableRooms;

    }

    if (currencyCode != undefined) {
        ecommerce.currencyCode = currencyCode;
    } else if (currencyCode == undefined) {
        ecommerce.currencyCode = "";
    }
    ecommerce.impressions = impressions;
    pushHotelJson(selectedFilter);
    pushHotelJson(ecommerce);
    pushEvent(event);
    disablePackageInTicRoomRedemption();
}
function prepareOnRoomSelect(viewName, eventString) {
    ViewName = viewName;
    event = eventString;
    globalBookingOption = dataCache.session.getData("bookingOptions");
    var selectedRoomsCache = globalBookingOption.selection;
    var currentAddedRoom = selectedRoomsCache[selectedRoomsCache.length - 1];

    if ((templateName == 'tajhotels/components/structure/tajhotels-rooms-listing-page'
            || templateName == "tajhotels/components/structure/tajhotels-booking-cart-page" || templateName == "tajhotels/components/structure/tajhotels-booking-checkout-page")
            && selectionAr != undefined) {
        currentAddedRoom = selectionAr;
    }
    var cartValue = {}
    cartValue.AddOnCost = 0;
    cartValue.AddOnCostWithTax = 0;
    cartValue.AdultQty = currentAddedRoom.adults;
    cartValue.ArrivalDt = ArrivalDt;
    cartValue.ArrivalDtDDMMYYYY = ArrivalDtDDMMYYYY;
    cartValue.ArrivalDtMMDDYYYY = ArrivalDtMMDDYYYY;
    cartValue.ArrivalDtYYYYMMDD = ArrivalDtYYYYMMDD;
    cartValue.BookDt = GetFormattedDate(new Date(), "YYYY/MM/DD");
    cartValue.CHAIN_ID = CHAIN_ID;
    cartValue.CategoryCode = currentAddedRoom.selectedFilterCode;
    cartValue.CategoryName = currentAddedRoom.selectedFilterTitle;
    cartValue.ChainNm = "Indian Hotels Company Limited";
    cartValue.ChildQty = currentAddedRoom.children;
    cartValue.ConfirmNo = "";
    cartValue.ConvertedCurrCode = currentAddedRoom.currencyString
    cartValue.CurrCode = currentAddedRoom.currencyString;
    cartValue.CurrPrefix = dataCache.session.getData("selectedCurrency").trim();
    cartValue.DailyRate = currentAddedRoom.dailyNightlyRate;
    ItineraryDailyRate = cartValue.DailyRate;
    cartValue.DaysToCheckIn = DaysToCheckIn;
    cartValue.DepartDt = DepartDt;
    cartValue.DepartDtDDMMYYYY = DepartDtDDMMYYYY;
    cartValue.DepartDtMMDDYYYY = DepartDtMMDDYYYY;
    cartValue.DepartDtYYYYMMDD = DepartDtYYYYMMDD;
    cartValue.GuestQty = (parseInt(cartValue.AdultQty) + parseInt(cartValue.ChildQty));
    if (dataLayerData) {
        cartValue.HName = dataLayerData.hotelName;
    } else if (bookedOptions.hotel) {
        cartValue.HName = bookedOptions.hotel.hotelName;
    } else {
        cartValue.HName = "";
    }
    cartValue.HOTEL_ID = HOTEL_ID || globalBookingOption.hotelId;
    cartValue.NightsQty = currentAddedRoom.nightlyRates.length;
    cartValue.PriceWithTax = (parseInt((currentAddedRoom.dailyNightlyRate + "").replace(/,/g, "")) * NightsQty)
            + parseInt(currentAddedRoom.roomTaxRate);
    cartValue.PriceWithoutAddons = cartValue.PriceWithTax;
    cartValue.RateCode = currentAddedRoom.ratePlanCode;
    cartValue.RateLongDesc = currentAddedRoom.rateDescription;
    cartValue.RateName = currentAddedRoom.rateName;
    cartValue.RateShortDesc = (currentAddedRoom.rateDescription).trim();
    cartValue.RmQty = globalBookingOption.selection.length;
    cartValue.RoomCode = currentAddedRoom.roomTypeCode;
    cartValue.RoomLongDesc = currentAddedRoom.roomLongDescription;
    cartValue.RoomName = currentAddedRoom.title;
    cartValue.RoomNightsQty = cartValue.NightsQty;
    cartValue.RoomShortDesc = (currentAddedRoom.roomShortDescription).trim();
    cartValue.SelectedCurrCode = cartValue.CurrCode;
    cartValue._id = reservationNumber;
    var totalTaxPerRoom = 0;
    var nightlyRatesArr = [];
    nightlyRatesArr = currentAddedRoom.nightlyRates
    if (nightlyRatesArr && nightlyRatesArr.length > 0) {
        $(nightlyRatesArr).each(function(index) {
            totalTaxPerRoom = totalTaxPerRoom + parseInt(nightlyRatesArr[index].tax);
        });
    }
    // totalTax=totalTax+totalTaxPerRoom;
    var totalTax = 0;
    var taxArr = [];
    var selectedRoomArr2 = globalBookingOption.selection;
    if (selectedRoomArr2) {
        $(selectedRoomArr2).each(function(index2) {
            taxArr = selectedRoomArr2[index2].taxes;
            if (taxArr && taxArr.length > 0) {
                $(taxArr).each(function(index) {
                    totalTax = totalTax + parseInt(taxArr[index].taxAmount);
                });
            }
        });
    }
    globalBookingOption = dataCache.session.getData("bookingOptions");
    globalBookingOption.totalCartTax = totalTax;
    dataCache.session.setData("bookingOptions", globalBookingOption);
    cartValue.Taxes = parseInt($(
            '.summary-charges .summary-charges-item-con.summary-charges-tax-con .summary-charges-item-value .cart-total-tax')
            .text().replace(/,/g, ""))
            || totalTax;
    cartValue.DailyRateWithTax = (parseInt((cartValue.DailyRate + "").replace(/,/g, "")) + currentAddedRoom.roomTaxRate
            / NightsQty).toFixed(2).toString();
    ItineraryDailyRateWithTax = cartValue.DailyRateWithTax;
    cartValue.TotalCost = globalBookingOption.totalCartPrice - cartValue.Taxes;
    cartValue.TotalCostWithTax = globalBookingOption.totalCartPrice;
    Cart.push(cartValue);
    SelectedRoom.push(cartValue);
    RmQty = selectedRoomsCache.length;
    RoomNightsQty = cartValue.RoomNightsQty;
    AdultQty = 0, ChildQty = 0;
    $(selectedRoomsCache).each(
            function(index) {
                AdultQty = parseInt(AdultQty) + parseInt(selectedRoomsCache[index].adults);
                ChildQty = parseInt(ChildQty) + parseInt(selectedRoomsCache[index].children);
                GuestQty = parseInt(AdultQty) + parseInt(ChildQty);
                TotalCostWithTax = globalBookingOption.totalCartPrice;
                Taxes = $('.summary-charges-tax-con .summary-charges-item-value .cart-total-tax').html()
                        || globalBookingOption.totalCartTax || totalTax || '';
                TotalCost = TotalCostWithTax - Taxes;
            });

}

function getAllAvailableRooms(selectedRoomsCache) {

    var selectedRooms = [];
    try {
        selectedRoomsCache.forEach(function(rooms) {
            var selectedRoom = {};
            selectedRoom.CategoryCode = rooms.selectedFilterCode;
            ;
            selectedRoom.CategoryDescription = "";
            selectedRoom.CategoryName = rooms.selectedFilterTitle;
            ;
            selectedRoom.DailyPrice = rooms.dailyNightlyRate;

            selectedRoom.InventoryWarning = ""
            selectedRoom.RateCode = rooms.ratePlanCode;
            selectedRoom.RateLongDesc = rooms.rateDescription;
            selectedRoom.RateName = rooms.rateName;
            selectedRoom.RateShortDesc = (rooms.rateDescription).trim();
            selectedRoom.RoomCode = rooms.roomTypeCode;
            selectedRoom.RoomLongDesc = rooms.roomLongDescription;
            selectedRoom.RoomName = rooms.title;
            selectedRoom.RoomShortDesc = (rooms.roomShortDescription).trim();
            selectedRooms.push(selectedRoom);
        });
    } catch (err) {
        console.error('caught error in getAllAvailableRooms');
    }
    return selectedRooms;
}
// payment-details.js
function payingAtCart(roomsData, step) {
    var ecommerce = {};
    var checkout = {}
    var actionField = {};
    actionField.step = step;
    if (step === "2") {
        if (paymentOptValue == "Pay at hotel") {
            actionField.option = "VISA"
        } else if (paymentOptValue == "Pay Online now") {

            actionField.option = "PAY_ONLINE"
        } else {
            actionField.option = paymentOptValue;
        }
    }
    checkout.actionField = actionField;
    checkout.products = prepareJsonRoom(roomsData);
    ecommerce.checkout = checkout;
    pushRoomsJsonOnClick("checkout", ecommerce);
}

function prepareJsonRoom(roomsData) {
    var products = [];
    var count = 0;
    var variant;
    roomDataArray = roomsData.selection;
    $(roomDataArray).each(
            function(index) {
                var roomCardObj = {};
                roomCardObj.name = this.title;
                roomCardObj.id = this.roomTypeCode + this.ratePlanCode;
                if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
                    roomCardObj.price = (this.selectedRate).toFixed(2).toString();
                } else {
                    roomCardObj.price = (this.selectedRate / this.nightlyRates.length).toFixed(2).toString();
                }

                roomCardObj.brand = dataLayerData.hotelName;
                var ratePlanNameVar = $(
                        $(roomsData.selection[count].details).find('.more-rate-amenities .more-rate-title')).text();
                roomCardObj.category = ratePlanNameVar;
                roomCardObj.variant = this.promoCode || "";
                roomCardObj.quantity = this.nightlyRates.length.toString();
                count++;
                products.push(roomCardObj);
            });
    return products;
}

// bookHotelConfirmation.js
function prepareConfirmationData(jsonObj, bODataLayer) {
    var ecommerce = {};
    var count = 0;
    var variant;
    var multiRatePlanCode = "";
    var multiRatePlanname = ""

    SelectedCurrCode = $('.nav-item .nav-link .selected-currency.inline-block').data('selected-currency');

    ConvertedCurrCode = SelectedCurrCode;
    CurrCode = SelectedCurrCode;
    roomDataArray = jsonObj.roomList;
    var size = roomDataArray.length;
    var rateplanArr = [];
    rateplanArr = $('.selected-room-details .confirmation-selected-rooms-card')
    console.log("arr size : ", rateplanArr.length);
    $(roomDataArray)
            .each(
                    function(index) {
                        var roomCardObj = {};
                        if (this.reservationNumber != "Not Available") {
                            if (reservationNumber == undefined || reservationNumber == "" && count == 0) {
                                reservationNumber = this.reservationNumber;
                            } else if (reservationNumber != undefined && count < size && count > 0) {
                                reservationNumber = reservationNumber + "," + this.reservationNumber;
                            }
                            roomCardObj.reservationNumber = this.reservationNumber;
                            roomCardObj.name = this.roomTypeName;
                            roomCardObj.id = this.roomTypeCode + this.ratePlanCode;
                            if (templateName == "tajhotels/components/structure/tajhotels-rooms-listing-page") {
                                roomCardObj.price = (parseInt(this.roomCostBeforeTax.replace(/,/g, ""))).toFixed(2)
                                        .toString();
                            } else {
                                roomCardObj.price = (parseInt(this.roomCostBeforeTax.replace(/,/g, "")) / this.nightlyRates.length)
                                        .toFixed(2).toString();
                            }
                            roomCardObj.brand = bODataLayer.hotelName;
                            var resrvationNumFromUi = $(rateplanArr[index]).find(
                                    '.confirmation-room-details .heading .room-reservation-number').text();
                            var ratePlanNameVar = $(rateplanArr[index]).find('.cart-amenities-description').text();
                            var ratePlanDesc = $(rateplanArr[index]).find('.conf-rate-description-details').text()
                            if (ratePlanDesc == this.rateDescription) {
                                console.log("rate plan Desc is in sync in synxixResponse vs UI data.");
                            }
                            roomCardObj.variant = this.promoCode || "";
                            roomCardObj.category = ratePlanNameVar;
                            roomCardObj.coupon = this.ratePlanCode + "," + roomCardObj.category;
                            if (multiRatePlanCode == "" || multiRatePlanname == "") {
                                multiRatePlanCode = this.ratePlanCode;
                                multiRatePlanname = roomCardObj.category;
                            } else {
                                multiRatePlanCode = multiRatePlanCode + "|" + this.ratePlanCode;
                                multiRatePlanname = multiRatePlanname + "|" + roomCardObj.category
                            }
                            // actionFieldCoupon.push(roomCardObj.coupon);
                            roomCardObj.quantity = this.nightlyRates.length.toString();
                            count++;
                            products.push(roomCardObj);
                        } else {
                            count = count - 1;
                        }
                    });
    reservationNumber = reservationNumber.toString();
    totalRevenue = jsonObj.totalAmountAfterTax.toString();
    totalTax = ((jsonObj.totalAmountAfterTax - jsonObj.totalAmountBeforeTax).toFixed(2)).toString();
    Taxes = totalTax;
    TotalCost = jsonObj.totalAmountBeforeTax;
    TotalCostWithTax = jsonObj.totalAmountAfterTax;
    RmQty = products.length;
    var ecommercePurchase = {};
    var purchase = {};
    var actionField = {};
    ecommercePurchase.currencyCode = $('.nav-item .nav-link .selected-currency.inline-block').data('selected-currency');
    if (dataCache.session.getData("themeValue") == "ama-theme") {
        ecommercePurchase.currencyCode = dataCache.session.getData("selectedCurrencyString");
    }
    actionField.id = jsonObj.hotelId + " - " + jsonObj.itineraryNumber;
	var arrVal = jsonObj.hotel && jsonObj.hotel.hotelResourcePath ? jsonObj.hotel.hotelResourcePath.split("/") : [];
    if (window.location.href.includes('tajinnercircle')) {
        actionField.affiliation = 'TajInnerCircle';
    } else {
		if(arrVal && arrVal.length > 0) {
			actionField.affiliation = (arrVal[arrVal.length - 2]).capitalize();
		}
    }
    actionField.revenue = jsonObj.totalAmountAfterTax;
    TotalCostWithTax = actionField.revenue;
    actionField.tax = jsonObj.totalAmountAfterTax - jsonObj.totalAmountBeforeTax;
    Taxes = actionField.tax.toFixed(2);
    actionField.coupon = (multiRatePlanCode + "," + multiRatePlanname) || "";
    TotalCost = parseInt(TotalCostWithTax) - parseInt(Taxes);
    purchase.actionField = actionField;
    purchase.products = products;
    ecommercePurchase.purchase = purchase;
    ecommerce.ecommerce = ecommercePurchase;
    var reloadCount = dataCache.session.getData("reloadCount")
    var itineraryFromSession = dataCache.session.getData("itineraryAtSession")
    if (window.location.href.includes('www.amastaysandtrails.com')) {
        pushEvent("confirmation", "confirmation", prepareGlobalJSAtConfirmationPage(jsonObj, bODataLayer))
        pushRoomsJsonOnClick("trigger_bookingConfirm", ecommerce);
        dataCache.session.setData("reloadCount", 1)
        dataCache.session.setData("itineraryAtSession", jsonObj.itineraryNumber);
    } else {
        if (reloadCount && itineraryFromSession == jsonObj.itineraryNumber) {
            reloadCount++
            dataCache.session.setData("reloadCount", reloadCount);
            Cart = [];
        } else {
            pushEvent("confirmation", "confirmation", prepareGlobalJSAtConfirmationPage(jsonObj, bODataLayer))
            pushRoomsJsonOnClick("trigger_bookingConfirm", ecommerce);
            dataCache.session.setData("reloadCount", 1)
            dataCache.session.setData("itineraryAtSession", jsonObj.itineraryNumber);
        }
    }
}

function prepareGlobalJSAtConfirmationPage(bookingDetailsRequest, bODataLayer) {
    console.log("Function Entry : data-layer.js >> prepareGlobalJSAtConfirmationPage()")
    var adultQuantity = 0;
    var childQuantity = 0;
    var tempArrivalDt = '';
    var tempDepartDt = '';
    var eventData = {};
    if (bookingDetailsRequest != undefined) {
        ItineraryNo = bookingDetailsRequest.itineraryNumber;
        DaysToCheckIn = getArrivalDepartDtDiff(bookingDetailsRequest)
        AvailabilityFilters = pageLevelData.pageTitle;
        CHAIN_ID = CHAIN_ID || bookingDetailsRequest.chainCode;
        HOTEL_ID = bookingDetailsRequest.hotelId;
        if (LangCode != undefined || LangCode != "") {
            LangCode = pathLocale || pageLevelData.pageLanguage;
        }
        SelectedCurrCode = bookingDetailsRequest.currencyCode;
        NightsQty = bookingDetailsRequest.roomList[0].nightlyRates.length || DaysToCheckIn;
        RmQty = bookingDetailsRequest.roomList.length;
        RoomNightsQty = NightsQty || DaysToCheckIn;
        roomOptions = bookingDetailsRequest.roomList;
        Cart = [];
        if (bookingDetailsRequest.roomList.length > 0) {
            $
                    .each(
                            roomOptions,
                            function(index, value) {
                                var cartValue = {}
                                adultQuantity = parseInt(adultQuantity) + parseInt(this.noOfAdults);
                                childQuantity = parseInt(childQuantity) + parseInt(this.noOfChilds);
                                if (ItineraryDailyRate == "" && ItineraryDailyRateWithTax == "") {
                                    ItineraryDailyRate = this.roomCostBeforeTax;
                                    ItineraryDailyRateWithTax = this.roomCostAfterTax;
                                } else {
                                    ItineraryDailyRate = parseInt(ItineraryDailyRate)
                                            + parseInt(this.roomCostBeforeTax);
                                    ItineraryDailyRateWithTax = parseInt(ItineraryDailyRateWithTax)
                                            + parseInt(this.roomCostAfterTax);
                                }
                                cartValue.AddOnCost = this.AddOnCost || 0;
                                cartValue.AddOnCostWithTax = this.AddOnCostWithTax || 0;
                                cartValue.AdultQty = this.noOfAdults;
                                cartValue.ArrivalDt = ArrivalDt;
                                cartValue.ArrivalDtDDMMYYYY = ArrivalDtDDMMYYYY;
                                cartValue.ArrivalDtMMDDYYYY = ArrivalDtMMDDYYYY;
                                cartValue.ArrivalDtYYYYMMDD = ArrivalDtYYYYMMDD;
                                cartValue.BookDt = GetFormattedDate(new Date(), "YYYY/MM/DD");
                                cartValue.CHAIN_ID = CHAIN_ID;
                                cartValue.CancelConfirmNo = CancelConfirmNo || "";
                                cartValue.CategoryCode = this.roomTypeCode;
                                cartValue.CategoryName = this.roomTypeName;
                                cartValue.ChainNm = "Indian Hotels Company Limited";
                                cartValue.ChildQty = this.noOfChilds;
                                cartValue.ConfirmNo = ItineraryNo || this.reservationNumber;
                                cartValue.ConfirmationAction = "added";
                                cartValue.CurrCode = SelectedCurrCode;
                                cartValue.CurrPrefix = dataCache.session.getData("selectedCurrency").trim();
                                if (this.nightlyRates) {
                                    cartValue.DailyRate = this.nightlyRates[0].price;
                                }
                                ItineraryDailyRate = cartValue.DailyRate;
                                cartValue.DaysToCheckIn = DaysToCheckIn;
                                cartValue.DepartDt = DepartDt;
                                cartValue.DepartDtDDMMYYYY = DepartDtDDMMYYYY;
                                cartValue.DepartDtMMDDYYYY = DepartDtMMDDYYYY;
                                cartValue.DepartDtYYYYMMDD = DepartDtYYYYMMDD;
                                cartValue.GuestQty = (parseInt(cartValue.AdultQty) + parseInt(cartValue.ChildQty));
                                if (bookingDetailsRequest.hotel) {
                                    cartValue.HName = bookingDetailsRequest.hotel.hotelName;
                                } else {
                                    cartValue.HName = "";
                                }
                                cartValue.HOTEL_ID = HOTEL_ID;
                                cartValue.NightsQty = this.nightlyRates.length;
                                cartValue.PriceWithTax = this.roomCostAfterTax;
                                cartValue.PriceWithoutAddons = cartValue.PriceWithTax;
                                cartValue.RateCode = this.ratePlanCode;
                                cartValue.RateLongDesc = this.rateDescription;
                                cartValue.RateName = this.ratePlanName;
                                cartValue.RateShortDesc = this.rateDescription;
                                cartValue.RmQty = RmQty;
                                cartValue.RoomCode = this.roomTypeCode;
                                cartValue.RoomLongDesc = this.roomTypeDesc;
                                cartValue.RoomName = this.roomTypeName;
                                cartValue.RoomNightsQty = cartValue.NightsQty;
                                cartValue.RoomShortDesc = this.roomTypeDesc;
                                cartValue.SelectedCurrCode = cartValue.CurrCode;
                                if (this.taxes) {
                                    cartValue.DailyRateWithTax = (parseInt(cartValue.DailyRate) + (parseInt(this.taxes[0].taxAmount) / NightsQty))
                                            .toFixed(2).toString();
                                    ItineraryDailyRateWithTax = cartValue.DailyRateWithTax;
                                }
                                cartValue.TotalCost = bookingDetailsRequest.totalAmountBeforeTax || "";
                                cartValue.TotalCostWithTax = bookingDetailsRequest.totalAmountAfterTax || "";
                                cartValue.Taxes = parseInt(cartValue.TotalCostWithTax) - parseInt(cartValue.TotalCost)
                                        || "";
                                cartValue._id = this.reservationNumber;
                                Cart.push(cartValue);
                            });
            AdultQty = adultQuantity;
            ChildQty = childQuantity;
            GuestQty = parseInt(AdultQty) + parseInt(ChildQty);
        }

        if (dataCache.local.getData("userDetails")) {
            eventData.membershipId = dataCache.local.getData("userDetails").membershipId;
        }
        eventData.ItineraryNo = ItineraryNo;
        eventData.ItineraryDailyRate = ItineraryDailyRate || '';
        eventData.ItineraryDailyRateWithTax = ItineraryDailyRateWithTax || '';
        eventData.AdultQty = AdultQty;
        eventData.ChildQty = ChildQty;
        eventData.GuestQty = GuestQty;
        eventData.ArrivalDt = ArrivalDt;
        eventData.ArrivalDtDDMMYYYY = ArrivalDtDDMMYYYY;
        eventData.ArrivalDtMMDDYYYY = ArrivalDtMMDDYYYY;
        eventData.ArrivalDtYYYYMMDD = ArrivalDtYYYYMMDD;
        eventData.AvailabilityFilters = AvailabilityFilters;
        eventData.CHAIN_ID = CHAIN_ID;
        eventData.Cart = Cart;
        eventData.DepartDt = DepartDt;
        eventData.DepartDtDDMMYYYY = DepartDtDDMMYYYY;
        eventData.DepartDtMMDDYYYY = DepartDtMMDDYYYY;
        eventData.DepartDtYYYYMMDD = DepartDtYYYYMMDD;
        eventData.DaysToCheckIn = DaysToCheckIn;
        eventData.HOTEL_ID = HOTEL_ID;
        eventData.LangCode = LangCode;
        eventData.SelectedCurrCode = SelectedCurrCode;
        eventData.NightsQty = NightsQty;
        eventData.RmQty = RmQty;
        eventData.RoomNightsQty = RoomNightsQty;
        eventData.hotelCity = bODataLayer.hotelCity;
        eventData.hotelCode = bODataLayer.hotelCode;
        eventData.hotelCountry = bODataLayer.hotelCountry;
        eventData.hotelName = bODataLayer.hotelName;
        HName = eventData.hotelName;
        eventData.HName = bODataLayer.hotelName;
        eventData.brandName = bODataLayer.hotelBrand;
        eventData.pageAuthor = bODataLayer.pageAuthor;
        eventData.pageHierarchy = pageHierarchy;
        eventData.pageLanguage = bODataLayer.pageLanguage;
        eventData.pageTitle = pageTitle;
    }
    return eventData;
}
// This analytic data mainly effect on the below mentioned file
// datalayer.html, room-list.js, hotel-overview.js, cart-layout.js,
// bookHotelConfirmation.js, booking-flow.js, destination-landing.js, data-layer.js

function prepareEnrollJsonForClick(eventName, {}) {
    var enroll = {};
    enroll.event = eventName;
    enroll.isMembershipId = 'No';
    enroll.membershipId = '';
    enroll.membershipType = '';
    enroll.memberEmailId = '';
    enroll.memberMobileNo = '';
    enroll.memberGender = '';
    enroll.memberDOB = '';
    enroll.memberCountry = '';
    enroll.memberState = '';
    enroll.memberCity = '';
    enroll.memberPincode = '';
    enroll.brandName = 'TajInnerCircle';
    enroll.pageCategory = 'Rewards&Privilages';
    if (eventName == 'TICEnrol') {
        enroll.pageSubCategory = 'top-enrollment-click : terms-and-conditions'; // ; this should clicked based.
    } else {
        enroll.pageSubCategory = 'form-enrollment-click : terms-and-conditions'; // ; this should clicked based.
    }
    enroll.device = deviceType;
    enroll.pageName = 'terms-and-conditions';
    enroll.pageTitle = 'TIC - Terms and Conditions';
    enroll.pageLanguage = 'en-in';

    //dataLayer.push(enroll);
    //updated for global data layer
    addParameterToDataLayerObj(eventName, enroll);
}

function prepareEnrollJsonAfterSumbitClick(eventName, enrolData, membershipId, userLoggedIn) {
    // membershipId
    var enroll = {};
    enroll.event = eventName;
    enroll.isMembershipId = 'Yes';
    enroll.membershipId = membershipId;
    enroll.membershipType = 'Cooper';
    enroll.memberEmailId = window.btoa(enrolData.email); // should be encrypted
    enroll.memberMobileNo = window.btoa(enrolData.mobile); // should be encrypted
    enroll.memberGender = enrolData.gender;
    enroll.memberDOB = enrolData.dateOfBirth;
    enroll.memberCountry = enrolData.country;
    enroll.memberState = enrolData.state;
    enroll.memberCity = enrolData.city;
    enroll.memberPincode = enrolData.pincode;
    enroll.brandName = 'TajInnerCircle';
    enroll.pageCategory = 'Rewards&Privilages';
    enroll.pageSubCategory = 'enrollment-submit : tic-enrol';
    enroll.device = deviceType;
    enroll.pageName = 'en-in : tic-enrol';
    enroll.pageTitle = 'Taj Inner Circle Enrol';
    enroll.pageLanguage = 'en-in';

    //dataLayer.push(enroll);
	//updated for new data layer
    addParameterToDataLayerObj(eventName, enroll);
}

function prepareLoginJsonAfterSumbitClick(eventName, userDetails) {
    var login = {};
    login.event = eventName;
    login.isMembershipId = 'Yes';
    login.membershipId = userDetails.membershipId;
    login.membershipType = 'Cooper';
    login.memberEmailId = window.btoa(userDetails.email); // should be encrypted
    login.memberMobileNo = window.btoa(userDetails.mobile); // should be encrypted
    login.memberGender = userDetails.gender;
    if (userDetails && userDetails.addresses) {
        for (var i = 0; i < userDetails.addresses.length; i++) {
            if (userDetails.addresses[i].primary) {
                login.memberCountry = userDetails.addresses[i].country;
                login.memberState = userDetails.addresses[i].state;
                login.memberCity = userDetails.addresses[i].city;
                login.memberPincode = userDetails.addresses[i].postalCode;
                break;
            }
        }
    } else {
        login.memberCountry = ''; // dosn't get in login call to syzgey
        login.memberState = ''; // dosn't get in login call to syzgey
        login.memberCity = ''; // dosn't get in login call to syzgey
        login.memberPincode = ''; // dosn't get in login call to syzgey
    }
    login.memberDOB = userDetails.dob; // dosn't get in login call to syzgey

    login.brandName = 'TajInnerCircle';
    login.pageCategory = 'Rewards&Privilages';
    login.pageSubCategory = 'login-successfully : home-page';
    login.device = deviceType;
    login.pageName = 'en-in : home-page';
    login.pageTitle = 'Taj InnerCircle HomePage';
    login.pageLanguage = 'en-in';

    dataLayer.push(login);
}
// cannot get info like enrolData.dateOfBirth;,enrolData.gender;,enrolData.country; etc. Need to check
function prepareActivateTicAccountJsonAfterSumbitClick(eventName, userDetails) {
    var login = {};
    login.event = eventName;
    login.isMembershipId = 'Yes';
    login.membershipId = userDetails.membershipId;
    login.membershipType = 'Cooper';
    login.memberEmailId = window.btoa(userDetails.email); // should be encrypted
    login.memberMobileNo = window.btoa(userDetails.mobile); // should be encrypted
    login.memberGender = userDetails.gender;
    login.memberDOB = ''; // dosn't get in login call to syzgey
    login.memberCountry = ''; // dosn't get in login call to syzgey
    login.memberState = ''; // dosn't get in login call to syzgey
    login.memberCity = ''; // dosn't get in login call to syzgey
    login.memberPincode = ''; // dosn't get in login call to syzgey
    login.brandName = 'TajInnerCircle';
    login.pageCategory = 'Rewards&Privilages';
    login.pageSubCategory = 'profile-created-successfully : home-page';
    login.device = deviceType;
    login.pageName = 'en-in : home-page';
    login.pageTitle = 'Taj InnerCircle HomePage';
    login.pageLanguage = 'en-in';

    dataLayer.push(login);
}

function prepareOfferAndPromotionsJsonAfterSumbitClick(eventName, offerDetails) {
    var offers = {};
    offers.pageCategory = 'Rewards&Privilages';
    offers.pageSubCategory = 'profile-created-successfully : home-page';
    offers.pageName = 'en-in : home-page';
    offers.pageTitle = 'Taj InnerCircle HomePage';
    offers.offerCategory = 'Taj InnerCircle Special Offer';
    offers.offerName = offerDetails.offerName;
    offers.offerValidity = offerDetails.offerValidity;
    offers.offerCode = offerDetails.offerCode;

    //updated for global data layer
    addParameterToDataLayerObj(eventName, offers);
    //dataLayer.push(offers);
}

function prepareEpicureBuyNowJsonAfterSumbitClick(eventName) {
    var epicureBuy = {};

    epicureBuy.pageCategory = 'Rewards&Privilages';
    epicureBuy.pageSubCategory = 'enroll-epicure-membership : epicure-memebrship-benefits-page';
    epicureBuy.pageName = 'en-in : epicure';
    epicureBuy.pageTitle = 'Epicure Dining Plans - Taj InnerCircle';

    //updated for global data layer
    addParameterToDataLayerObj(eventName, epicureBuy);

}

function prepareRedeemNowJsonForClick(eventName, redeeemObject) {
    var redeemNow = {};
    redeemNow.pageCategory = 'Rewards&Privilages';
    redeemNow.pageSubCategory = 'book-and-redeem : points-redemption';
    redeemNow.device = deviceType;
    redeemNow.pageName = 'book-and-redeem';
    redeemNow.pageTitle = 'Book & Redeem-Taj InnerCircle';
    redeemNow.redemptionType = 'Online Redemption';
    redeemNow.redemptionName = redeeemObject.heading;
    redeemNow.redemptionDescription = redeeemObject.description;
    //dataLayer.push(redeemNow);

    //updated for global data layer
    addParameterToDataLayerObj(eventName, redeemNow)
}

function prepareReserveAndBookCardJsonForClick(eventName, cardObject) {
    var userDetails = getUserData();
    var redeemCardObject = {};

    redeemCardObject.event = eventName;
    if (userDetails) {
        redeemCardObject.isMembershipId = 'Yes';
        if (userDetails.card && userDetails.card.type) {
            redeemCardObject.membershipType = userDetails.card.type;
        } else {
            redeemCardObject.membershipType = '';
        }
        redeemCardObject.membershipId = userDetails.membershipId;
        redeemCardObject.memberEmailId = window.btoa(userDetails.email); // should be encrypted
        redeemCardObject.memberMobileNo = window.btoa(userDetails.mobile); // should be encrypted
        redeemCardObject.memberGender = userDetails.gender;
        redeemCardObject.memberDOB = userDetails.dob;

        if (userDetails.addresses) {
            for (var i = 0; i < userDetails.addresses.length; i++) {
                if (userDetails.addresses[i].primary) {
                    redeemCardObject.memberCountry = userDetails.addresses[i].country;
                    redeemCardObject.memberState = userDetails.addresses[i].state;
                    redeemCardObject.memberCity = userDetails.addresses[i].city;
                    redeemCardObject.memberPincode = userDetails.addresses[i].postalCode;
                    break;
                }
            }
        } else {
            redeemCardObject.memberCountry = ''; // dosn't get in login call to syzgey
            redeemCardObject.memberState = ''; // dosn't get in login call to syzgey
            redeemCardObject.memberCity = ''; // dosn't get in login call to syzgey
            redeemCardObject.memberPincode = ''; // dosn't get in login call to syzgey
        }

    } else {
        redeemCardObject.isMembershipId = 'No';
        redeemCardObject.membershipType = '';
        redeemCardObject.membershipId = '';
        redeemCardObject.memberEmailId = '';
        redeemCardObject.memberMobileNo = '';
        redeemCardObject.memberGender = '';
        redeemCardObject.memberDOB = '';
        redeemCardObject.memberCountry = '';
        redeemCardObject.memberState = '';
        redeemCardObject.memberCity = '';
        redeemCardObject.memberPincode = '';

    }

    redeemCardObject.brandName = 'TajInnerCircle';
    redeemCardObject.pageCategory = 'Rewards&Privilages';
    redeemCardObject.pageSubCategory = 'book-and-redeem : points-redemption';
    redeemCardObject.device = deviceType;
    redeemCardObject.pageName = 'en-in : book-and-redeem';
    redeemCardObject.pageTitle = 'Book & Redeem - Taj InnerCircle';
    redeemCardObject.pageLanguage = 'en-in';
    redeemCardObject.redemptionType = cardObject.ctaText;
    redeemCardObject.redemptionName = cardObject.heading;
    redeemCardObject.redemptionDescription = cardObject.description;
    dataLayer.push(redeemCardObject);
}

function prepareEventsCardJsonForClick(eventName, cardObject) {
    var userDetails = getUserData();
    var eventCardObject = {};

    eventCardObject.event = eventName;
    if (userDetails) {
        eventCardObject.isMembershipId = 'Yes';
        if (userDetails.card && userDetails.card.type) {
            eventCardObject.membershipType = userDetails.card.type;
        } else {
            eventCardObject.membershipType = '';
        }
        eventCardObject.membershipId = userDetails.membershipId;
        eventCardObject.memberEmailId = window.btoa(userDetails.email); // should be encrypted
        eventCardObject.memberMobileNo = window.btoa(userDetails.mobile); // should be encrypted
        eventCardObject.memberGender = userDetails.gender;
        eventCardObject.memberDOB = userDetails.dob;

        if (userDetails.addresses) {
            for (var i = 0; i < userDetails.addresses.length; i++) {
                if (userDetails.addresses[i].primary) {
                    eventCardObject.memberCountry = userDetails.addresses[i].country;
                    eventCardObject.memberState = userDetails.addresses[i].state;
                    eventCardObject.memberCity = userDetails.addresses[i].city;
                    eventCardObject.memberPincode = userDetails.addresses[i].postalCode;
                    break;
                }
            }
        } else {
            eventCardObject.memberCountry = ''; // dosn't get in login call to syzgey
            eventCardObject.memberState = ''; // dosn't get in login call to syzgey
            eventCardObject.memberCity = ''; // dosn't get in login call to syzgey
            eventCardObject.memberPincode = ''; // dosn't get in login call to syzgey
        }
    } else {
        eventCardObject.isMembershipId = 'No';
        eventCardObject.membershipType = '';
        eventCardObject.membershipId = '';
        eventCardObject.memberEmailId = '';
        eventCardObject.memberMobileNo = '';
        eventCardObject.memberGender = '';
        eventCardObject.memberDOB = '';
        eventCardObject.memberCountry = '';
        eventCardObject.memberState = '';
        eventCardObject.memberCity = '';
        eventCardObject.memberPincode = '';
    }

    eventCardObject.brandName = 'TajInnerCircle';
    eventCardObject.pageCategory = 'Rewards&Privilages';
    eventCardObject.pageSubCategory = 'events: upcoming-events';
    eventCardObject.device = deviceType;
    eventCardObject.pageName = 'en-in : events';
    eventCardObject.pageTitle = 'Events - Taj InnerCircle';
    eventCardObject.pageLanguage = 'en-in';
    eventCardObject.eventType = 'Upcoming Events';
    eventCardObject.eventName = cardObject.eventName;
    eventCardObject.eventPlace = cardObject.eventPlace;
    eventCardObject.eventDate = cardObject.eventDate;
    dataLayer.push(eventCardObject);
}

function prepareEventsPayJsonForClick(eventName, cardObject) {
    var userDetails = getUserData();
    var eventCardObject = {};

    eventCardObject.event = eventName;
    if (userDetails) {
        eventCardObject.isMembershipId = 'Yes';
        if (userDetails.card && userDetails.card.type) {
            eventCardObject.membershipType = userDetails.card.type;
        } else {
            eventCardObject.membershipType = '';
        }
        eventCardObject.membershipId = userDetails.membershipId;
        eventCardObject.memberEmailId = window.btoa(userDetails.email); // should be encrypted
        eventCardObject.memberMobileNo = window.btoa(userDetails.mobile); // should be encrypted
        eventCardObject.memberGender = userDetails.gender;
        eventCardObject.memberDOB = userDetails.dob;

        if (userDetails.addresses) {
            for (var i = 0; i < userDetails.addresses.length; i++) {
                if (userDetails.addresses[i].primary) {
                    eventCardObject.memberCountry = userDetails.addresses[i].country;
                    eventCardObject.memberState = userDetails.addresses[i].state;
                    eventCardObject.memberCity = userDetails.addresses[i].city;
                    eventCardObject.memberPincode = userDetails.addresses[i].postalCode;
                    break;
                }
            }
        } else {
            eventCardObject.memberCountry = ''; // dosn't get in login call to syzgey
            eventCardObject.memberState = ''; // dosn't get in login call to syzgey
            eventCardObject.memberCity = ''; // dosn't get in login call to syzgey
            eventCardObject.memberPincode = ''; // dosn't get in login call to syzgey
        }

    } else {
        eventCardObject.isMembershipId = 'No';
        eventCardObject.membershipType = '';
        eventCardObject.membershipId = '';
        eventCardObject.memberEmailId = '';
        eventCardObject.memberMobileNo = '';
        eventCardObject.memberGender = '';
        eventCardObject.memberDOB = '';
        eventCardObject.memberCountry = '';
        eventCardObject.memberState = '';
        eventCardObject.memberCity = '';
        eventCardObject.memberPincode = '';
    }

    eventCardObject.brandName = 'TajInnerCircle';
    eventCardObject.pageCategory = 'Rewards&Privilages';
    eventCardObject.pageSubCategory = cardObject.pageSubCategory;
    eventCardObject.device = deviceType;
    eventCardObject.pageName = cardObject.pageName;
    eventCardObject.pageTitle = cardObject.pageTitle;
    eventCardObject.pageLanguage = 'en-in';
    eventCardObject.eventType = 'Upcoming Events';
    eventCardObject.eventName = cardObject.eventName;
    eventCardObject.eventPlace = cardObject.eventPlace;
    eventCardObject.eventDate = cardObject.eventDate;
    eventCardObject.eventsTicketsQty = cardObject.eventsTicketsQty;
    dataLayer.push(eventCardObject);
}

function prepareGiftCardJsonForClick(eventName, cardObject) {
    var giftCardObject = {};
    giftCardObject.pageCategory = 'Rewards&Privilages';
    giftCardObject.pageSubCategory = 'book-and-redeem : gift-card';
    giftCardObject.pageName = 'en-in : book-and-redeem : gift-card';
    giftCardObject.pageTitle = 'Redeem or Buy Taj Gift cards - Taj InnerCircle';
    giftCardObject.giftCardType = cardObject.giftCardType;
    giftCardObject.giftCardValue = cardObject.giftCardValue ? cardObject.giftCardValue.toString() : "";
	giftCardObject.giftCardQuantity = giftCardObject.giftCardQuantity;

	//updated for global data layer
	addParameterToDataLayerObj(eventName, giftCardObject);
}

function prepareGifCardRedemptionJsonForClick(eventName, cardObject) {
    var giftCardObject = {};
    giftCardObject.pageCategory = 'Rewards&Privilages';
    giftCardObject.pageSubCategory = 'book-and-redeem:gift-card';
    giftCardObject.pageName = 'en-in : book-and-redeem : gift-card';
    giftCardObject.pageTitle = 'Redeem or Buy Taj Gift cards-Taj InnerCircle';
	giftCardObject.giftCardValue = cardObject.totalPointsToBeRedeemed
    giftCardObject.giftCardQuantity = cardObject.giftCardQuantity ? cardObject.giftCardQuantity : "1";
    giftCardObject.giftCardType = cardObject.cardType
    giftCardObject.redemptionType = cardObject.redemptionType
    giftCardObject.redemptionName = cardObject.product
    giftCardObject.redemptionDescription = cardObject.redemptionDescription ? cardObject.redemptionDescription : "";
	giftCardObject.pointsType = cardObject.pointsTypeOfRedemption
    giftCardObject.ponitstobeRedeemed = cardObject.totalPointsToBeRedeemed ? cardObject.totalPointsToBeRedeemed.toString() : "";

    //updated for global data layer
    addParameterToDataLayerObj(eventName, giftCardObject);
}

function prepareGifCardPayJsonForClick(eventName, cardObject) {
    var giftCardObject = {};
    giftCardObject.pageCategory = 'Rewards&Privilages';
    giftCardObject.pageSubCategory = 'book-and-redeem : redeem-this-reward';
    giftCardObject.pageName = 'en-in : book-and-redeem : redeem-this-reward';
    giftCardObject.pageTitle = 'Redeem This Reward';
    giftCardObject.giftCardType = cardObject.ProductName;
    giftCardObject.giftCardValue = cardObject.valueGiftCertificate;
    giftCardObject.giftCardQuantity = cardObject.Quantity;

	giftCardObject.redemptionType = cardObject.PointsRedeemedType
    giftCardObject.redemptionName = cardObject.ProductName
    giftCardObject.redemptionDescription = cardObject.redemptionDescription ? cardObject.redemptionDescription : "";
    giftCardObject.pointsType = cardObject.point_type
    giftCardObject.ponitstobeRedeemed = cardObject.valueGiftCertificate

    //updated for global data layer
    addParameterToDataLayerObj(eventName, giftCardObject)
}

function prepareEpicureBuyNowJsonAfterSuccesfullEnroll(eventName, enrollResponse, transcationId, userDetails) {
    var membershipId = "";
    if (userDetails) {
        membershipId = userDetails.membershipId; // TIC membership Id
    }
    var products = [];
    products.push({
        'name' : "Epicure Plus", // Program Name
        'id' : "Epicure Plus 12 months", // Program Description
        'brand' : "Taj Epicure InnerCircle", // Program Brand
        'category' : "Epicure", // Program Category
        'price' : 15930.00, // Total cost
        'quantity' : 1
    });
    var actionField = {};
    actionField.id = transcationId; // Transaction Id
    actionField.affiliation = 'Taj InnerCircle'; // Brand Name
    actionField.revenue = 15930.00; // Total Transaction Value (incl. tax and shipping) of all rooms.
    actionField.tax = 0; // Total Transaction Tax Value of all rooms.

    var purchase = {};
    purchase.actionField = actionField;
    purchase.products = products;

    var ecommerce = {};
    ecommerce.currencyCode = 'INR';
    ecommerce.purchase = purchase;

    var epicureBuy = {};
    epicureBuy.event = eventName;
    epicureBuy.ecommerce = ecommerce;

    dataLayer.push(epicureBuy);
}

// [TIC-DATA-LAYER]
$(document).ready(
        function() {
            deviceType = deviceDetector.checkDevice();
            if (deviceType === 'small') {
                deviceType = 'mobile';
            } else if (deviceType === 'medium') {
                deviceType = 'tab';
            } else if (deviceType === 'large') {
                deviceType = 'desktop';
            }

            $('[data-component-id="enrol-btn"]').click(function() {
                prepareEnrollJsonForClick("TICEnrol");
            });
            // updated for global data layer
            $('[data-component-id="enrol-btn-login-popup"]').click(function() {
                prepareEnrollJsonForClick("TIC_FormLogin_Enrollment_HomePage_TICEnrol", {});
            });
            // updated for global data layer
			$('span.forgot-password-button').click(function(){
				addParameterToDataLayerObj('TIC_FormLogin_Passwordreset_HomePage_ForgotPassword', {});
			});

            $('.tic-partners-card-button').click(function() {
                var button = $(this);
                var heading = button.closest('.redemption-inner-wrapper').find('.redemption-heading').text().split(' ').join('');
                var description = button.closest('.redemption-inner-wrapper').find('.redemption-description').text();
                var redeeemObject = {};
                redeeemObject.heading = heading;
                redeeemObject.description = description;
                //updated for global data layer
                prepareRedeemNowJsonForClick(heading+'_OnlinePoints_Redemption_Book&RedeemPage_RedeemNow', redeeemObject);
            });

            $('.reserve-and-book-card').click(
                    function() {
                        var button = $(this);
                        var heading = button.closest('.recommended-experiences-wrap').find('.mr-experinces-title')
                                .text();
                        var description = button.closest('.recommended-experiences-wrap').find(
                                '.mr-experinces-details-location').text();
                        var ctaText = button.closest('.recommended-experiences-wrap').find('.reserve-and-book-card')
                                .text();
                        var cardObject = {};
                        cardObject.heading = heading;
                        cardObject.description = description;
                        cardObject.ctaText = ctaText;
                        prepareReserveAndBookCardJsonForClick("PointsRedemption_ReserveNow", cardObject);
                    });

            //updated for global data layer
            $('.anlaytics-gift-card').click(function() {
                var button = $(this);
                var heading = button.closest('.mr-card-details-wrap').find('.mr-gift-card-header').text();
				var giftCardValue = button.closest('.mr-gift-card-innerwrap').find('.mr-rate-wrap').children('.mr-details.epicure-desc').text();
                var redeeemObject = {};
                redeeemObject.giftCardType = heading;
				redeeemObject.giftCardValue = giftCardValue;
				redeeemObject.giftCardQuantity = "1";
				var eventName = "_PartnerCertificates_Redemption_CertificatePage_ViewDetails";
				if(heading.includes("Taj Salon")){
					eventName = 'TajSalonGift' + eventName;
				}
				else if(heading.includes('Ginger')){
					eventName = 'GingerHotelGift' + eventName;
				}
				else if(heading.includes('Surat Diamond')){
					eventName = 'SuratDiamondGift' + eventName;
				}				
				else if(heading.includes('Westside')){
					eventName = 'WestsideGift' + eventName;
				}				
				else if(heading.includes('Shoppers Stop')){
					eventName = 'ShoppersStopGift' + eventName;
				}
				else if(heading.includes('TAPPMe')){
					eventName = 'TAPPMeGift' + eventName;
				}
                prepareGiftCardJsonForClick(eventName, redeeemObject);
            });
        });

// taj-air-analytics
function prepareQuickQuoteJsonForClick(eventName) {
    var quickquote = {};
    quickquote.event = eventName;
    quickquote.brandName = 'TajAir';
    quickquote.PageCategory = 'TajAir_' + pageLevelData.pageTitle;
    quickquote.flight_route = '';
    quickquote.flight_ToCity = '';
    quickquote.flight_FromCity = '';
    quickquote.flight_triptype = '';
    quickquote.flight_tripPriceUSD = '';
    quickquote.flight_tripPriceINR = '';
    quickquote.flight_Category = '';
    quickquote.flight_SubCategory = '';
    dataLayer.push(quickquote);

};

function prepareQuickQuoteJsonAfterClick(eventName, tripData) {
    var quickquote = {};
    quickquote.event = eventName;
    quickquote.brandName = 'TajAir';
    quickquote.PageCategory = 'TajAir_' + pageLevelData.pageTitle;
    quickquote.flight_route = ''
    quickquote.flight_ToCity = tripData.flight_ToCity;
    quickquote.flight_FromCity = tripData.flight_FromCity;
    quickquote.flight_triptype = tripData.tripType;
    quickquote.flight_tripPriceUSD = tripData.flight_tripPriceUSD;
    quickquote.flight_tripPriceINR = tripData.flight_tripPriceINR;
    quickquote.flight_Category = '';
    quickquote.flight_SubCategory = '';
    dataLayer.push(quickquote);
};

function prepareRequestQuote(eventName, requestData) {
    var requestquote = {};
    var flightToCity = [];
    var flightFromCity = [];
    var flightDepTime = [];
    var depDate = [];
    var tripDetails = {};
    if (requestData.tripValues) {
        tripDetails = JSON.parse(requestData.tripValues);

        tripDetails.forEach(function(trip) {
            flightToCity.push(trip.departureToCity);
            flightFromCity.push(trip.departureFromCity);
            flightDepTime.push(trip.departureTime);
            depDate.push(trip.departureDate);

        });
    }
    requestquote.event = eventName;
    requestquote.brandName = 'TajAir';
    requestquote.PageCategory = 'TajAir_' + pageLevelData.pageTitle;
    requestquote.flight_route = '';
    requestquote.flight_ToCity = flightToCity;
    requestquote.flight_FromCity = flightFromCity;
    requestquote.flight_DepartureDate = depDate;
    requestquote.flight_DepartureTime = flightDepTime, requestquote.flight_triptype = requestData.tripType,
            requestquote.noOfPassengers = requestData.numberOfGuests, requestquote.emailId = requestData.emailId,
            requestquote.phoneNumber = requestData.mobile, dataLayer.push(requestquote);
}
