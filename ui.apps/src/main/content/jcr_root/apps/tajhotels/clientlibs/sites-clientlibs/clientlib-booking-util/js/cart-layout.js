function initCartPage() {
	$(document)
			.ready(
					function() {
						setThemeValueToPageContainer();
						var guestRoomPath = dataCache.session
								.getData("guestRoomPath")
								|| "#";
						$('.cart-layout .total-price-back a').attr("href",
								guestRoomPath);
						$('.cart-layout .cart-addRoom a').attr("href",
								guestRoomPath);
//						[IHCLCB ]
						$('.ihcl-theme .cart-layout .generic-back-link a').attr("href",
                                guestRoomPath);
//                      [IHCLCB End]						
						// $('.cart-layout .sold-out-addRoom a').attr("href",
						// guestRoomPath);

						$('#summary-charges-total-price').on(
								"click",
								function() {
									showNightlyRates();
									$('.nightly-rates-dropdown-image')
											.toggleClass("cm-rotate-icon-180");
									$('.nightly-rates').slideToggle();
								});

						$('#summary-taxes-charges').on(
								"click",
								function() {
									showAllTypeOfTaxes();
									$('.taxes-dropdown-image').toggleClass(
											"cm-rotate-icon-180");
									$('.all-type-of-taxes').slideToggle();
								});


                         $('#entity-details-summary').on("click", function() {
                             // $('.entity-details-show').show()
                            $('.entity-dropdown-image').toggleClass("cm-rotate-icon-180");
                            $('.all-entity').slideToggle();
                        });


						var _globals = intGlobals();

						var isCartEmpty = checkForEmptyCart(_globals);
						if (isCartEmpty) {
							return;
						}
						var previousCurrencySymbol = dataCache.session
								.getData('selectedCurrency');

						if (previousCurrencySymbol != undefined) {
							$(".cart-currency-symbol").text(
									previousCurrencySymbol.trim());
						}

						// [TIC-FLOW]
						var ticRoomRedemptionObjectSession = dataCache.session
								.getData('ticRoomRedemptionObject');
						if (ticRoomRedemptionObjectSession
								&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
							$(".cart-currency-symbol").text("");
							$(".tic-coupon-code").addClass('d-none');
							$(".tic-room-redemption-terms-and-condition")
									.removeClass('d-none');
							$(".cm-page-container").addClass(
									'tic-room-redemption-fix');
							$(".tic-redeemed-row").removeClass('d-none');

						}

						$(document)
								.on(
										'currency:changed',
										function(e, currency) {
											if (!checkForEmptyCart(_globals)) {
												var popupParams = {
													title : 'Booking Flow in Progress!',
													description : 'You have selected packages in your cart, Now You will not be  allowed to change Currency. ',
												}
												warningBox(popupParams);
												if (previousCurrency1 != undefined) {
													dataCache.session.setData(
															'currencyCache',
															previousCurrency1);
													setActiveCurrencyInDom(
															previousCurrency1.currencySymbol,
															previousCurrency1.currency,
															previousCurrency1.currencyId);
												}
											} else {
												setCurrencyCacheToBookingOptions();
											}
										});
						trigger_roomSelection(dataCache.session
								.getData("bookingOptions"));
						var roomsData = dataCache.session
								.getData("bookingOptions");
						addingRoomToCart(roomsData);

						// Renders Summary card
						serveSummaryCard(_globals);

						// Renders each room card
						if (_globals.elemRoomCardsCon) {
							initiRoomsCard(_globals);
						}

						// Ensure whether to show add room btn on cart page or
						// not
						updateAddRoomBtnVisibility(_globals);

						_globals.elemCartAddRoomBtn
								.each(function() {
									$(this)
											.on(
													'click',
													function() {
														console
																.log('btn clicked');
														var bookingOptions = _globals.bookingSelections
																|| {
																	fromDate : moment(
																			new Date())
																			.format(
																					"MMM Do YY"),
																	toDate : moment(
																			new Date())
																			.add(
																					1,
																					'days')
																			.format(
																					"MMM Do YY"),
																	rooms : 1,
																	roomOptions : [ {
																		adults : 1,
																		children : 0
																	} ],
																	selection : [],
																	promoCode : null
																};
														bookingOptions.isAddRoomClicked = true;
														dataCache.session
																.setData(
																		"bookingOptions",
																		bookingOptions);

													});
								});

						// [TIC-FLOW]
						// ticCartLayoutFlow();
					});
};

function getCurrencyCache() {
	return dataCache.session.getData('currencyCache');
}

function setCurrencyCacheToBookingOptions() {
	var bookingOptions = dataCache.session.getData("bookingOptions");
	bookingOptions.currencySelected = dataCache.session
			.getData('currencyCache').currencyId;
	console.log("setCurrencyInSessionStorage : "
			+ bookingOptions.currencySelected);
	dataCache.session.setData("bookingOptions", bookingOptions);
}

function initCheckoutCartCard() {
	$(document).ready(function() {
		// find the DOM elements and initialize required variables
		var _globals = intGlobals();

		// Renders Summary card
		serveSummaryCard(_globals);
	});
};

function intGlobals() {
    var _elemSummaryCard = $('.summary-card-wrap');
    var _globals = {
        bookingSelections : dataCache.session.getData("bookingOptions"),
        elemSummaryCard : _elemSummaryCard,
        elemSelTitle : _elemSummaryCard.find('.summary-room-title'),
        elemRoomSummaryCon : _elemSummaryCard.find('.selected-rooms-details'),
        summaryOnlyRoomPrice : _elemSummaryCard.find('.cart-room-price'),
        summaryTaxCon : _elemSummaryCard.find('.summary-charges-tax-con'),
        summaryTaxes : _elemSummaryCard.find('.cart-total-tax '),
        summaryTotalPrice : _elemSummaryCard.find('.cart-total-price'),
      elemCartCheckinDate : _elemSummaryCard.find('.cart-checkin-date'),
        elemCartCheckoutDate : _elemSummaryCard.find('.cart-checkout-date'),
        elemCartFinalPrice : $('.total-prize-con'),
        elemCartAddRoomBtn : $('.carts-add-room'),

		elemRoomCardsCon : $('.cart-selected-rooms-card-container')
	};
	if (_globals.bookingSelections) {
		_globals.bookingSelections.isAddRoomClicked = false;
	}

	return _globals;
};

function serveSummaryCard(_globals) {
	// [TIC-FLOW]
	var _elemSummaryCard = $('.summary-card-wrap');
	var userDetails = getUserData();

	var totalBasePrice = 0;
	var totaltaxes = 0;
	// var totalPrice = _globals.bookingSelections.totalCartPrice;
	var totalPrice = 0;
	_globals.elemRoomSummaryCon.html('');

	$('.summary-rooms-count').html(_globals.bookingSelections.selection.length);
	if (_globals.bookingSelections.selection.length) {
		$('.summary-rooms-count-suffix').show();
	} else {
		$('.summary-rooms-count-suffix').hide();
	}
	_globals.elemSelTitle.html(_globals.bookingSelections.targetEntity);
	// _globals.elemSelTitle.append('<i class="summary-card-arrow inline-block
	// icon-drop-down-arrow"/>');

	var _fullYearCheckinDate = moment(_globals.bookingSelections.fromDate,
			"MMM Do YY").format('DD MMM YYYY');
	var _fullYearCheckoutDate = moment(_globals.bookingSelections.toDate,
			"MMM Do YY").format('DD MMM YYYY');

	_globals.elemCartCheckinDate.html(_fullYearCheckinDate);

    $('.summary-checkin-date').html(_fullYearCheckinDate);
    _globals.elemCartCheckoutDate.html(_fullYearCheckoutDate);
    $('.summary-checkout-date').html(_fullYearCheckoutDate);
        _globals.bookingSelections.selection
                .forEach(function(item, index) {



					var numRoom = index + 1;

                        var templateEachRoom = '<div class="selected-room-detail">'
                                + '<i class="icon-king-size-bed-not-selected icon-king-size-bed-basic-color inline-block"/><span class="number-of-room">'
						+ 'ROOM:' + numRoom + '</span><span>' + item.title
						+ ' - ' + item.adults + ' Adult'
						+ (item.adults > 1 ? 's' : '') + ' | ' + item.children
						+ ' Child' + (item.children > 1 ? 'ren' : '')
						+ '</span></div>'
                        _globals.elemRoomSummaryCon.append(templateEachRoom);
					if($('#corporate-summary').attr('data-corporate-summary') == 'false' || !$('#corporate-summary').attr('data-corporate-summary')){
						$('.entity-details-show').hide();
						$("#entity-details-summary").hide();


                    }else{
						_globals.elemRoomSummaryCon.hide();
                    	//$(".coupon-label").hide();
//						[IHCLCB FLOW]
						$(".ihclcb-coupon-hide").hide();

						$("#inclusive-taxes").hide();
//						[IHCLCB FLOW END]
                    }
                    /*
				 * totalPrice = totalPrice + parseInt( $( item.details ).find(
				 * '.present-rate .rc-total-amount' ).html() );
                     */
                    // totalBasePrice = totalBasePrice + parseFloat( $( item.details
                    // ).find( '.rate-base-price' ).html().replace( /,/g, "" ) );
                    // [TIC-FLOW]
				var ticRoomRedemptionObjectSession = dataCache.session
						.getData('ticRoomRedemptionObject');
				var userSelectedCurrency = dataCache.session
						.getData("selectedCurrency");
				if (userDetails
						&& userDetails.card
						&& userDetails.card.tier
						&& ticRoomRedemptionObjectSession
                            && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
                        totalBasePrice = totalBasePrice
							+ parseFloat((item.roomBaseRate + item.roomTaxRate + "")
									.replace(/,/g, ""));
                    } else {
					totalBasePrice = totalBasePrice
							+ parseFloat((item.roomBaseRate + "").replace(/,/g,
									""));
                    }
                    // var thisTaxData = $( item.details ).find( '.rate-tax-info'
                    // ).html().replace( /,/g, "" );
                    var thisTaxData = (item.roomTaxRate + "").replace(/,/g, "");
                    if (thisTaxData) {
                        totaltaxes = totaltaxes + parseFloat(thisTaxData);
                    }

                });

	// [TIC-FLOW]
	var ticRoomRedemptionObjectSession = dataCache.session
			.getData('ticRoomRedemptionObject');
	var userSelectedCurrency = dataCache.session.getData("selectedCurrency");
	if (userDetails && userDetails.card && userDetails.card.tier
			&& ticRoomRedemptionObjectSession
			&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
		if (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹') {
			var ticRoomRedemptionObjectSession = dataCache.session
					.getData('ticRoomRedemptionObject');
			if (ticRoomRedemptionObjectSession
					&& ticRoomRedemptionObjectSession.currencyRateConversionString) {
				var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
				var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency
						+ '_INR']);
				totalBasePrice = Math.round(totalBasePrice * conversionRate);
			}

		}
		var userObjectSession = getUserData();
		if (userObjectSession && userObjectSession.card
				&& userObjectSession.card.type
				&& userObjectSession.card.type.includes("TIC")) {
			_globals.summaryOnlyRoomPrice.html('<div class="cart-tic-points">'
					+ roundPrice(Math.ceil(totalBasePrice))
					+ ' TIC Points </div> ');

			_globals.summaryOnlyRoomPrice.find(".cart-tic-points").digits();
		} else if (userObjectSession && userObjectSession.card
				&& userObjectSession.card.type
				&& userObjectSession.card.type === "TAP") {
			_globals.summaryOnlyRoomPrice.html(roundPrice(Math
					.ceil(totalBasePrice))
					+ ' TAP');
			_globals.summaryOnlyRoomPrice.digits();
		} else if (userObjectSession && userObjectSession.card
				&& userObjectSession.card.type
				&& userObjectSession.card.type === "TAPPME") {
			_globals.summaryOnlyRoomPrice.html(roundPrice(Math
					.ceil(totalBasePrice))
					+ ' TAPPMe');
			_globals.summaryOnlyRoomPrice.digits();
		}
	} else {
		_globals.summaryOnlyRoomPrice.html(roundPrice(totalBasePrice));
		_globals.summaryOnlyRoomPrice.digits();
	}

	if (totaltaxes) {
		// [TIC-FLOW]
		if (userDetails && userDetails.card && userDetails.card.tier
				&& ticRoomRedemptionObjectSession
				&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
			if (userSelectedCurrency != 'INR' && userSelectedCurrency != '₹') {
				var ticRoomRedemptionObjectSession = dataCache.session
						.getData('ticRoomRedemptionObject');
				if (ticRoomRedemptionObjectSession
						&& ticRoomRedemptionObjectSession.currencyRateConversionString) {
					var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
					var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency
							+ '_INR']);
					totaltaxes = Math.round(totaltaxes * conversionRate);
				}

			}

			_globals.summaryTaxes.html(roundPrice(Math.ceil(totaltaxes)));
			_globals.summaryTaxes.digits();

		} else {
			_globals.summaryTaxes.html(roundPrice(totaltaxes));
			_globals.summaryTaxes.digits();
		}
	} else {
		_globals.summaryTaxes.css('display', 'none');
	}

	totalPrice = totalBasePrice + totaltaxes;

	// [TIC-FLOW]
	if (userDetails && userDetails.card && userDetails.card.tier) {
		var guestRoomPath = dataCache.session.getData("guestRoomPath");
		$(".cart-redirect-url").attr("href", guestRoomPath);
	}
	if (userDetails && userDetails.card && userDetails.card.tier
			&& ticRoomRedemptionObjectSession
			&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
		// overriding totalPrice , as we dont include taxes in room redemption
		// flow
		totalPrice = totalBasePrice;
		var memberType = userDetails.card.type;
		if (memberType.includes("TIC")) {
			_globals.summaryTotalPrice
					.html('<div class="summary-card-total-tic-points">'
							+ roundPrice(Math.ceil(totalBasePrice))
							+ ' TIC Points </div> ');
		} else if (memberType === "TAP") {
			_globals.summaryTotalPrice.html(roundPrice(Math
					.ceil(totalBasePrice))
					+ " TAP");
		} else if (memberType === "TAPPMe") {
			_globals.summaryTotalPrice.html(roundPrice(Math
					.ceil(totalBasePrice))
					+ " TAPPMe");
		} else {
			console.log("Error with member type");
		}
		if (memberType.includes("TIC")) {
			_globals.summaryTotalPrice.find(".summary-card-total-tic-points")
					.digits();
		} else {
			_globals.summaryTotalPrice.digits();
		}
	} else {
		_globals.summaryTotalPrice.html(roundPrice(totalPrice));
		_globals.summaryTotalPrice.digits();
	}

	_globals.elemSummaryCard.find('.coupon-code').val(
			_globals.bookingSelections.couponCode).trigger("keyup");
	if (_globals.bookingSelections.couponCode) {
		$('.couponcode-status-text').text(
				"Coupon code selected: "
						+ _globals.bookingSelections.couponCode);
	}

	$('.summary-card-rooms-count').text($('.selected-room-detail').length);
	if (_globals.bookingSelections.rooms > 1) {
		$('.summary-card-rooms-count-suffix').show();
	} else {
		$('.summary-card-rooms-count-suffix').hide();
	}
	$('.summary-card-nights-count').text(_globals.bookingSelections.nights);
	if (_globals.bookingSelections.nights > 1) {
		$('.summary-card-nights-count-suffix').show();
	} else {
		$('.summary-card-nights-count-suffix').hide();
	}
	var elemCartFinalPriceAmount = _globals.elemCartFinalPrice
			.find('.cart-final-price');
	// [TIC-FLOW] , checking if the rooms selected are from TIC flow or not.
	if (ticRoomRedemptionObjectSession
			&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow
			&& userDetails) {
		memberType = userDetails.card.type;
		_globals.elemCartFinalPrice.find('.cart-currency-symbol').addClass(
				'hidden');
		_globals.elemCartFinalPrice.find('.astrick-symbol').addClass('hidden');
		if (memberType.includes("TIC")) {
			elemCartFinalPriceAmount.html('<div class="cart-tic-points">'
					+ roundPrice(Math.ceil(totalBasePrice))
					+ ' TIC Points </div> ');
		} else if (memberType === "TAP") {
			elemCartFinalPriceAmount.html(roundPrice(Math.ceil(totalBasePrice))
					+ " TAP");
		} else if (memberType === "TAPPMe") {
			elemCartFinalPriceAmount.html(roundPrice(Math.ceil(totalBasePrice))
					+ " TAPPMe");
		} else {
			console.log("Member type invalid");
		}

	} else {
		_globals.elemCartFinalPrice.find('.cart-currency-symbol').removeClass(
				'hidden');
		_globals.elemCartFinalPrice.find('.astrick-symbol').removeClass(
				'hidden');
		elemCartFinalPriceAmount.html(roundPrice(totalPrice));
	}

    if ( userDetails && userDetails.card && userDetails.card.type && userDetails.card.type.includes("TIC")){
		elemCartFinalPriceAmount.find(".cart-tic-points").digits();
		elemCartFinalPriceAmount.find(".cart-epicure-points").digits();
	} else {
		elemCartFinalPriceAmount.digits();
	}
	_globals.bookingSelections.totalCartPrice = totalPrice;

	dataCache.session.setData("bookingOptions", _globals.bookingSelections);
	taxAndFees();
	/* below method is in cart-summary-card-comp.js file */
	cartSumaryCardComponent();
	addRoomButtonVisibility();


	var selectionArr1 = globalBookingOption.selection;
	selectionArr1.forEach(function(item, index) {
		selectionAr = selectionArr1[index] || item;
		prepareOnRoomSelect(ViewName, event);
	})
	// Analytic data call
	if (pageLevelData.pageTitle == "booking-cart") {
		if ($('.cm-page-container').hasClass('ama-theme')) {
			pushEvent("rooms.add", "rooms.add", prepareGlobalHotelListJS())
		} else {
			pushEvent("bookingCart", "bookingCart", prepareGlobalHotelListJS())
		}
	} else if (pageLevelData.pageTitle == "booking-checkout") {
		if ($('.cm-page-container').hasClass('ama-theme')) {
			pushEvent("checkout", "checkout", prepareGlobalHotelListJS())
		} else {
			pushEvent("bookingCheckout", "bookingCheckout",
					prepareGlobalHotelListJS())
		}
	}

};

function taxAndFees() {

	var cacheText = JSON.stringify(dataCache.session.getData("bookingOptions"));

	var totalRoomBaseRate = 0;
	if (cacheText) {
		var cacheJSONData = JSON.parse(cacheText);
		var totalCartPrice = cacheJSONData.totalCartPrice;
		var selection = cacheJSONData.selection;
		for (i = 0; i < selection.length; i++) {

			totalRoomBaseRate = (parseFloat(selection[i].roomBaseRate))
					+ totalRoomBaseRate;

		}

		if (totalRoomBaseRate == totalCartPrice) {

			$(".summary-charges-tax-con").hide();
		}
	}
}

function initiRoomsCard(_globals) {
	var _templateEachCards = _globals.elemRoomCardsCon.html();

	_globals.elemRoomCardsCon.html('');
	_globals.bookingSelections.selection
			.forEach(function(item, index) {
				var cachedDetails = $(item.details);
				_globals.elemRoomCardsCon.append(_templateEachCards);
				var elemEachRoomCard = _globals.elemRoomCardsCon.find(
						'.cart-selected-rooms-card').eq(index);
				var elAmenitiesCon = elemEachRoomCard
						.find('.cart-available-wrp');
				var elAmenitiesDesc = elemEachRoomCard
						.find('.cart-amenities-description');
				var elemRateDesc = elemEachRoomCard
						.find('.description-cancel-wrp');
				var elEachRoomoomTitle = elemEachRoomCard
						.find('.cart-amenities-title');
				var elEachRoomAdults = $(elemEachRoomCard
						.find('.cart-count-value')[0]);
				var elEachRoomChild = $(elemEachRoomCard
						.find('.cart-count-value')[1]);
				var elEachCardPrice = $(elemEachRoomCard.find('.room-amount'));
				var elEachRoomBedType = $(elemEachRoomCard
						.find('.cart-room-bed'));
				var elEachRoomNumber = elemEachRoomCard
						.find('.cart-room-number');

				elEachRoomNumber.html("Room <span>" + (index + 1) + "</span>");
				elEachRoomoomTitle.html(item.title);
				elAmenitiesDesc.html(cachedDetails.find('.more-rate-title')
						.html());
				elAmenitiesCon.html(cachedDetails.find(
						'.more-rate-available-wrp').html());
				elemRateDesc.html(cachedDetails.find('.description-cancel-wrp')
						.html());
				elEachRoomAdults.html(parseInt(item.adults));
				elEachRoomChild.html(parseInt(item.children));

				// [TIC-FLOW]
				var _elemSummaryCard = $('.summary-card-wrap');
				var userDetails = getUserData();
				var ticRoomRedemptionObjectSession = dataCache.session
						.getData('ticRoomRedemptionObject');
				var userSelectedCurrency = dataCache.session
						.getData("selectedCurrency");
				if (userDetails
						&& userDetails.card
						&& userDetails.card.tier
						&& ticRoomRedemptionObjectSession
						&& ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
					var roomPrice = item.selectedRate + item.roomTaxRate;

					if (userSelectedCurrency != 'INR'
							&& userSelectedCurrency != '₹') {
						var ticRoomRedemptionObjectSession = dataCache.session
								.getData('ticRoomRedemptionObject');
						if (ticRoomRedemptionObjectSession
								&& ticRoomRedemptionObjectSession.currencyRateConversionString) {
							var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
							var conversionRate = parseFloat(currencyRateConversionString[userSelectedCurrency
									+ '_INR']);
							roomPrice = Math.round(roomPrice * conversionRate);
						}

					}

					if (userDetails && userDetails.card
							&& userDetails.card.type
							&& userDetails.card.type.includes("TIC")) {
						elEachCardPrice
								.html("<div class='card-selected-total-tic-points'>"
										+ Math.ceil(roomPrice)
										+ ' TIC Points </div> ');
					} else if (userDetails && userDetails.card
							&& userDetails.card.type
							&& userDetails.card.type === "TAP") {
						elEachCardPrice.html(Math.ceil(roomPrice) + " TAP");
					} else if (userDetails && userDetails.card
							&& userDetails.card.type
							&& userDetails.card.type === "TAPPMe") {
						elEachCardPrice.html(Math.ceil(roomPrice) + " TAPPMe");
					} else {
						console.log("Member type invalid");
					}

					if (userDetails && userDetails.card
							&& userDetails.card.type
							&& userDetails.card.type.includes("TIC")) {
						elEachCardPrice.find(".card-selected-total-tic-points")
								.digits();
						elEachCardPrice.find(
								".card-selected-total-epicure-points").digits();
					} else {
						elEachCardPrice.digits();
					}
					elemEachRoomCard.find('.cart-room-cost-wrp').find(
							'.astrik-sign').hide()
				} else {
					elEachCardPrice.html(cachedDetails.find(
							'.present-rate .rc-total-amount').html());
					elEachCardPrice.digits();
				}

				for (bedType in item.bedTypeOptions) {
					var bedOption = elemEachRoomCard.find("[data-bed-type="
							+ bedType + "]");
					if (bedType === item.roomBedType) {
						bedOption.addClass('selected-bed-type');
					}
					bedOption.removeClass("cm-hide");
				}
				regEventsRoomCards(_globals, elemEachRoomCard);
				elemRateDesc.find('.cm-view-txt').each(
						function() {
							var elemDescriptionContent = $(this).siblings(
									'.full-description-text').clone();
							var thisParent = $(this).parent();

							$(this).parent().children().remove();
							thisDescripton = thisParent
									.append(elemDescriptionContent);
							thisDescripton.children().removeClass('cm-hide')
									.cmToggleText({
										charLimit : 150,
										showVal : "Show More",
										hideVal : "Show Less",
									});
						});
			});
};

function regEventsRoomCards(_globals, thisRoomCard) {
	thisRoomCard
			.find('.cart-room-bed')
			.click(
					function() {
						$(this).closest('.cart-room-bed-type').find(
								'.cart-room-bed').not(this).removeClass(
								'selected-bed-type');
						$(this).addClass('selected-bed-type');
						var selectedBedType = $(this).data('bed-type');
						var thisCardIndex = thisRoomCard.index();
						_globals.bookingSelections.selection[thisCardIndex].roomBedType = selectedBedType;
						_globals.bookingSelections.selection[thisCardIndex].roomTypeCode = _globals.bookingSelections.selection[thisCardIndex].bedTypeOptions[selectedBedType];
						dataCache.session.setData("bookingOptions",
								_globals.bookingSelections);
					});
	// Script to hanlde Count of Adults and Children
	thisRoomCard.find('.cart-increase-count').each(function() {
		$(this).click(function() {
			var currentCountNode = $(this).siblings('.cart-count-value');
			var currentCount = parseInt(currentCountNode.text());
			if (currentCount < 2) {
				currentCount = currentCount + 1;
			}
			currentCountNode.text(currentCount);
			var thisCardIndex = thisRoomCard.index();

			// Update the values for corresponding category
			updatePersonSelection($(this), thisCardIndex, currentCount);
		});
	});
	thisRoomCard.find('.cart-decrease-count').each(function() {
		$(this).click(function() {
			var currentCountNode = $(this).siblings('.cart-count-value');
			var currentCount = parseInt(currentCountNode.text()) - 1;
			var thisCardIndex = thisRoomCard.index();
			var theSelectedPerson = getPersonType($(this), thisCardIndex);
			if (theSelectedPerson == 'adults') {
				if (currentCount < 1) {
					currentCount = 1;
				}

			} else {
				if (currentCount < 0) {
					currentCount = 0;
				}

			}

			currentCountNode.text(currentCount);

			// Update the values for corresponding category
			updatePersonSelection($(this), thisCardIndex, currentCount);
		});
	});

	// Deleting a selected room
	thisRoomCard.find('.cart-room-delete-icon').click(function() {
		// Set params for warning popup to be dispalyed on attempt to delete
		/*
		 * var deleteWarningData =
		 * $($.find("[data-warning-type='DELETEPOPUP']")[0]).data("warning-description") ||
		 * null; console.log(deleteWarningData); var _self = this; var
		 * popupParams = { title: deleteWarningData, callBack:
		 * deleteCartRoomCards.bind( _self, _globals ), needsCta: true,
		 * isWarning: true } warningBox( popupParams );
		 */
		var _self = this;
		var popupParams = selectedRoomDelete();
		popupParams.callBack = deleteCartRoomCards.bind(_self, _globals);
		popupParams.needsCta = true;
		popupParams.isWarning = true;
		warningBox(popupParams);
	});

	function deleteCartRoomCards(_globals) {
		// debugger;

		var deletingRoomContent = $(this).closest('.cart-selected-rooms-card');

		$(this)
				.closest('.cart-selected-rooms-card')
				.hide(
						'slow',
						function() {
							var thisCardIndex = $(this).index();
							var thisCardInitialRoomIndex = _globals.bookingSelections.selection[thisCardIndex].initialRoomIndex;
							droppingRoomFromCart(dataCache.session
									.getData("bookingOptions"), $(this).index());
							$('.cart-room-number:gt(' + thisCardIndex + ')')
									.each(
											function() {
												var currentRoomNumber = $(this)
														.find("span").text();
												--currentRoomNumber;
												$(this).find("span").html(
														currentRoomNumber);
											});
							$(this).remove();
							delete _globals.bookingSelections.roomOptions[thisCardInitialRoomIndex].userSelection;
							_globals.bookingSelections.selection.splice(
									thisCardIndex, 1);
							/*
							 * _globals.bookingSelections.roomOptions.splice(
							 * thisCardIndex, 1 );
							 */
							_globals.bookingSelections.roomCount = _globals.bookingSelections.roomCount - 1;

							updateAddRoomBtnVisibility(_globals);

							checkForEmptyCart(_globals);

							// Update the selections on the summary cards and
							// then browser cache from there
							serveSummaryCard(_globals);
							// dataCache.session.setData( "bookingOptions",
							// _globals.bookingSelections );
							showNightlyRates();
							showAllTypeOfTaxes();
						});
		addRoomButtonVisibility();

		if ($(deletingRoomContent).find('.cart-room-cost').text() == "Sold out") {
			location.reload();
		}

	}

	function getBedType(elem) {
		var bedType = {};
		if (elem.hasClass('king-bed')) {
			bedType.type = 'bedType';
			bedType.index = 0;
		} else if (elem.hasClass('two-twins-bed-wrp')) {
			bedType.type = 'two-twins-bed';
			bedType.index = 1;
		}
		return bedType;
	}
	;

	function getPersonType(elem, index) {
		var targetParent = elem.closest('.cart-count-wrapper').parent();
		if (targetParent.hasClass('cart-adult-count-wrp')) {
			return 'adults';
		} else if (targetParent.hasClass('cart-children-count-wrp')) {
			return 'children';
		}
	}
	;

	function updatePersonSelection(elem, index, count) {
		var targetParent = elem.closest('.cart-count-wrapper').parent();
		if (targetParent.hasClass('cart-adult-count-wrp')) {
			_globals.bookingSelections.selection[index].adults = count;
		} else if (targetParent.hasClass('cart-children-count-wrp')) {
			_globals.bookingSelections.selection[index].children = count;
		}

		// Update the selections on the summary cards and then browser cache
		// from there
		serveSummaryCard(_globals);
		// dataCache.session.setData( "bookingOptions",
		// _globals.bookingSelections );
	}
	;
};

function checkForEmptyCart(_globals) {

	var editDetailsJson = dataCache.session.getData('jsonResponse');
	if (!editDetailsJson) {
		if (!_globals.bookingSelections
				|| _globals.bookingSelections.selection.length == 0) {
			_globals.elemSummaryCard.css('display', 'none');
			$('.checkout-payment-details-container').css('display', 'none');
			$('.checkout-page-back-link').css('display', 'none');
			$('.cart-footer-fixed-total-price').css('display', 'none');
			$('.cart-selected-rooms-addons-container').css('display', 'none');
			$('.total-price-header, .cart-cfp-right-con')
					.css('display', 'none');
			$('.cart-empty-state-con').addClass('active');
			$('.cart-empty-back').removeClass('cm-hide');
			$('body').animate({
				scrollTop : 0
			}, 'slow');
			return true;
		}
	}
};

function updateAddRoomBtnVisibility(_globals) {
	// Hide the Add Room option if selected packeges are 5
	if (_globals.bookingSelections
			&& _globals.bookingSelections.selection.length == 5) {
		/*
		 * _globals.elemCartAddRoomBtn.each( function() { $( this ).css(
		 * 'display', 'none' ); } );
		 */
		$(_globals.elemCartAddRoomBtn[1]).css('display', 'none');
	} else {
		$(_globals.elemCartAddRoomBtn[1]).css('display', 'block');
	}
};

// add room not visible if 5 rooms are selected
function addRoomButtonVisibility() {
	var numberOfRooms = $('.selected-room-detail').length;
	if (numberOfRooms < 5) {
		$('.cart-addRoom').show();
	} else {
		$('.cart-addRoom').hide();
	}
}

// [TIC-FLOW]
function ticCartLayoutFlow() {
	// [TIC-FLOW]
	var globalBookingOption = dataCache.session.getData("bookingOptions");
	var ticRoomRedemptionObjectSession = dataCache.session
			.getData('ticRoomRedemptionObject');
	var isTicRoomRedemptionFlow = false;
	if (ticRoomRedemptionObjectSession
			&& !ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
		globalBookingOption.selection
				.forEach(function(item, index) {
					if (item.selectedFilterTitle
							&& ((item.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES')
									|| (item.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
									|| (item.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES') || (item.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
						isTicRoomRedemptionFlow = true;
					}
				});
		var ticRoomRedemptionObject = {};
		ticRoomRedemptionObject.isTicRoomRedemptionFlow = isTicRoomRedemptionFlow;
		ticRoomRedemptionObject.selection = {};
		dataCache.session.setData("ticRoomRedemptionObject",
				ticRoomRedemptionObject);
	}

}
// end
