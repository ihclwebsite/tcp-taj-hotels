document.addEventListener( 'DOMContentLoaded', function() {
    var elShowHideHotelOffer = $( '.specific-hotels-offer-content > p' );
    if ( elShowHideHotelOffer.length > 0 ) {
        elShowHideHotelOffer.cmToggleText();
    }

    var locator = location.href;
    var theGlobalSearch = $( '.gb-search-con' );

    if ( locator.match( '/landing.html' ) ) {
        theGlobalSearch.addClass( 'cm-hide' );
    } else {
        if ( theGlobalSearch.hasClass( 'cm-hide' ) ) {
            theGlobalSearch.removeClass( 'cm-hide' );
        }
    }



    $( window ).resize( function() {
        // To handle tablet view changes
    } );
    $('.confirmation-experiences-wrap').customSlide(3);

    $('.limit-no-of-guest').keydown(function(e) {
        if ((e.which != 8) && (this.value.length >= 4)) {
            return false;
        }
    })

    $('.limit-phone-number').keydown(function(e) {
        if ((e.which != 8) && (this.value.length >= 15)) {
            return false;

        }
    })
} );
function siteMapPageInit() {
    $(document).ready(function() {
        var clickElement = $('.sitemap-category-heading');
        var expandBtnGeneral = $('.sitemap-category-expand-btn');
        var expansionGeneral = $('.sitemap-category-details');
        clickElement.each(function() {
            $(this).click(function() {
                var expandBtn = $(this).children();
                var expansion = $(this).siblings('.sitemap-category-details');
                if (expandBtn.html() == '+') {
                    expandBtnGeneral.html('+');
                    expandBtn.html('-');
                    expansionGeneral.removeClass('visible');
                    expansion.addClass('visible');
                } else {
                    expandBtn.html('+');
                    expansion.removeClass('visible');
                }
            })
        })
    });
}