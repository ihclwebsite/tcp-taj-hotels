$( document ).ready( function() {
    // numeric input validation
    $( '.only-numeric-input' ).keypress( function( e ) {
        if ( String.fromCharCode( e.keyCode || e.which ).match( /[^0-9\b\t]/g ) ) {            
            return false;
        }
    } );
    $(".only-numeric-input").on("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
         if(/[^0-9\b\t]+/g.test(pastedData) ){
            return false;
        }
    } );
    // aplhabet input validation
    $( '.only-alpha-input' ).keypress( function( e ) {
        if ( String.fromCharCode( e.keyCode || e.which ).match( /[^a-zA-Z \b\t]/g ) ) {
            return false;
        }
    } );
    $(".only-alpha-input").on("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
         if(/[^a-zA-Z \b\t]+/g.test(pastedData) ){
            return false;
        }
    } );


    // email validation
    function isEmail( email ) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test( email );
    }

    $( '.email-input' ).blur( function() {
        var email = $( this ).val();
        if ( !isEmail( email ) ) {
            $( this ).addClass( 'invalid-input' );
            invalidWarningMessage( $( this ) );
        } else {
            $( this ).removeClass( 'invalid-input' );
        }
    } );
    $(".email-input").on("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
         if(isEmail(pastedData) ){
            return false;
        }
    } );

    // Condition to toggle invalid-input based on value
	
	 $('select.sub-form-mandatory').change(function(){
        var targetInvalidInputWrapper = $( this ).closest('.sub-form-input-wrp');
        var targetInvalidInputWarningElement = targetInvalidInputWrapper.find('.sub-form-input-warning');
        targetInvalidInputWarningElement.hide(); 
    });

    $( '.sub-form-mandatory' ).not( '.email-input, .credit-card-number, #creditCardExpiryDate, .member-confirm-password, #gcenroll-countrySelectBoxIt, #epicurePersonalInfoCountrySelectBoxIt, #countrykitSelectBoxIt' ).blur( function() {
        if ( $( this ).val() != "" ) {
            $( this ).removeClass( 'invalid-input' );
        } else {
            $( this )
                .addClass( 'invalid-input' );
            invalidWarningMessage( $( this ) );
        }
    } );

    $('.sub-form-mandatory#gcenroll-countrySelectBoxIt').blur(function(){
        validateCountrySelected($(this));        
    })

    $('.sub-form-mandatory#epicurePersonalInfoCountrySelectBoxIt').blur(function(){
        validateCountrySelected($(this));
    })

    function validateCountrySelected(event){
        console.log("text is: " + event.text());
        if ( event.text() != "Select" ) {
            event.removeClass( 'invalid-input' );
        } else {
            event
                .addClass( 'invalid-input' );
            invalidWarningMessage( event );
        }
    }
    
    $( '.sub-form-mandatory' ).on('keyup blur', function(e) {
    	if($(this).attr('max')) {
    		if(($(this).val().length >= $(this).attr('max').length) && parseInt($(this).val()) > parseInt($(this).attr('max')) ){
    			$(this).addClass('invalid-input maxlength-error');    	
    			invalidWarningMessage($(this));
    		} else {
    			if(e.type != "blur") {
    				$(this).removeClass('invalid-input');
    			}
    			$(this).removeClass('maxlength-error');
    		}
    	}
    } );

    // Focusing input
    $( '.sub-form-input-element' ).on( 'focus', function() {
        this.focus( {
            preventScroll: false
        } );
    } );

    // Pancard Number Input validation
    function isPanCard( panNumber ) {
        var regex = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        return regex.test( panNumber );
    }

     $( '.panCard-input' ).blur( function() {
        var panCard = $( this ).val();
        if ( !isPanCard( panCard ) ) {
            $( this ).addClass( 'invalid-input' );
            invalidWarningMessage( $( this ) );
        } else {
            $( this ).removeClass( 'invalid-input' );
        }
    } );

    $(".panCard-input").on("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
         if(isPanCard(pastedData) ){
            return false;
        }
    } );

    // Onlhy Aplha Numric Input validation
    function isAplhaNumric( text ) {
        var regex = /^[a-z0-9]+$/i;
        return regex.test( text );
    }

     $( '.only-alphaNumeric-input' ).blur( function() {
        var text = $( this ).val();
        if ( !isAplhaNumric( text ) ) {
            $( this ).addClass( 'invalid-input' );
            invalidWarningMessage( $( this ) );
        } else {
            $( this ).removeClass( 'invalid-input' );
        }
    } );

    $(".only-alphaNumeric-input").on("paste", function(e){
        var pastedData = e.originalEvent.clipboardData.getData('text');
         if(isAplhaNumric(pastedData) ){
            return false;
        }
    } );

} );