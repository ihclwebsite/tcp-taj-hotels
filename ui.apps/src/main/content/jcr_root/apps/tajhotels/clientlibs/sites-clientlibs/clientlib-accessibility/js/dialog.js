var aria = aria || {};
var dialogClose;
aria.Utils = aria.Utils || {};
$('document')
        .ready(
                function() {
                    window.initDialog = function(dialogRef) {
                        var dialog = new aria.Dialog(dialogRef);
                    };
                    aria.Dialog = function(dialogRef) {
                        $(dialogRef).append("<div class = last-focus tabIndex = 0></div>");
                        $(dialogRef).prepend("<div class = first-focus tabIndex = 0></div>");
                        var tabPressed = false;
                        var shiftPressed = false;
                        // focus trapping
                        function dialogKeydownHandler(e) {
                            var evt = e || window.event;
                            var keyCode = evt.which || evt.keyCode;
                            if (keyCode == 27) {
                                evt.preventDefault();
                                $(dialogRef).find('.icon-close').first().click();
                            }
                            tabPressed = (keyCode === 9) ? true : false;
                            shiftPressed = (e.shiftKey) ? true : false;
                        }
                        $(dialogRef).on('keydown', dialogKeydownHandler);
                        var $firstFocusable = $(dialogRef).find('.first-focus');
                        var $lastFocusable = $(dialogRef).find('.last-focus');

                        $firstFocusable
                                .on(
                                        'focus',
                                        function(event) {
                                            if (tabPressed && shiftPressed) {
                                                var focusables = $(dialogRef)
                                                        .find(
                                                                'button:visible, [href]:visible, input:visible, select:visible, textarea:visible, [tabindex]:visible:not([tabindex="-1"])')
                                                        .not('.first-focus, .last-focus');
                                                focusables.last().focus();
                                            }
                                        });
                        $lastFocusable
                                .on(
                                        'focus',
                                        function(event) {
                                            var focusables = $(dialogRef)
                                                    .find(
                                                            'button:visible, [href]:visible, input:visible, select:visible, textarea:visible, [tabindex]:visible:not([tabindex="-1"])')
                                                    .not('.first-focus, .last-focus');
                                            if (tabPressed && !shiftPressed) {
                                                focusables.first().focus();
                                            }
                                        });
                    }
                    /*
                     * var element = $('[role = dialog]'); for(i=0;i<element.length;i++){
                     * window.initDialog(element[i]); } $('[role = dialog]').each(function() { window.initDialog(this); })
                     */
                })

$(document)
        .ready(
                function() {

                    // popup close

                    $('[data-dialog-close]').on('click', function() {
                        var dialogsName = $(this).data('dialogClose');
                        if (!((dialogsName === "jivaSpaCard") || (dialogsName === "galleryCarousel"))) {
                            $('[data-dialog-modal=' + dialogsName + ']').hide();
                        }
                        $('.first-focus').remove();
                        $(".last-focus").remove();
                        $('[data-dialog-modal=' + dialogsName + ']').attr('aria-expanded', 'false');
                        if (dialogClose !== undefined) {
                            dialogClose.focus();
                        }
                    });

                    // focus the first element in the popUp
                    var element = $('[data-dialog-btn]');
                    for (i = 0; i < element.length; i++) {
                        element[i]
                                .addEventListener(
                                        'click',
                                        function() {
                                            dialogClose = $(this);
                                            var dialogName = $(this).attr('data-dialog-btn');
                                            window.initDialog($('[data-dialog-modal=' + dialogName + ']'));
                                            if (!((dialogName === "jivaSpaCard") || (dialogName === "galleryCarousel"))) {
                                                $('[data-dialog-modal=' + dialogName + ']').show();
                                            }
                                            $('[data-dialog-modal=' + dialogName + ']').attr('aria-expanded', 'true');
                                            var focusElement = $('[data-dialog-modal=' + dialogName + ']')
                                                    .find(
                                                            'button:visible, [href]:visible, input:visible, select:visible, textarea:visible, [tabindex]:visible:not([tabindex="-1"])')
                                                    .not('.first-focus, .last-focus');
                                            if (focusElement.length != 0) {
                                                focusElement[0].focus();
                                            }
                                        })
                    }
                });
// focus traping for the dynamically created popUp

$(document).on(
        'click',
        '[data-dialog-btn]',
        function(e) {
            var dialogName = $(this).attr('data-dialog-btn');
            if (dialogName === 'WarningBox' || dialogName === 'showMapPopUp') {
                window.initDialog($('[data-dialog-modal=' + dialogName + ']'));
                dialogClose = $(this);
                $('[data-dialog-modal=' + dialogName + ']').attr('aria-expanded', 'true');
                var focusElement = $('[data-dialog-modal = ' + dialogName + ']').find(
                        'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])').not(
                        '.first-focus, .last-focus');
                if (focusElement.length != 0) {
                    focusElement[0].focus();
                }
            }
        })
