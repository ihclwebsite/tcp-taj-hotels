$.ajaxPrefilter(function(options, originalOptions, jqXHR) {
    // Modify options, control originalOptions, store jqXHR, etc
    var servletList = [ "rooms-prices", "rooms-availability", "destination-hotel-rates" ];
    var isCachable = false;
    servletList.forEach(function(servlet) {
        isCachable = isCachable ? isCachable : options.url.indexOf(servlet) !== -1;
    });

    if (isCachable) {
        options.url = options.url + (options.url.indexOf('rooms-prices') !== -1 ? '.' : '');
        if (options.data) {
            options.url = options.url + options.data.replace(/&/mg, '.');
        }
        if (!options.url.endsWith(".json")) {
            options.url = options.url + '.json';
        }
        options.data = null;
    }

});
