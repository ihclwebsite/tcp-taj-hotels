// Global data structure for gift cards
var dataGiftCardObj = {
    "giftCardValue": "",
    "giftCardQuantity": "",
    "giftCardType": ""
};

// Global data structure for offer details
var dataOfferObj = {
    "offerCategory": "",
    "offerName": "",
    "offerValidity": "",
    "offerCode": ""
};

// Global data structure for event details
var dataEventObj = {
    "eventType": "",
    "eventName": "",
    "eventPlace": "",
    "eventsTicketsQty": "",
    "eventDate": "",
}

// Global data Structure for points redemption
var dataPointsRedemptionObj = {
    "redemptionType": "",
    "redemptionName": "",
    "redemptionDescription": "",
    "pointsType": "",
    "ponitstobeRedeemed": "",
    "epicureLead": ""
}

// Global data structure for booking
var dataBookingObj = {
    "bookingType": "",
    "bookingPaymentType": ""
}


function getSpecificDataFields(){
	var specificDetails = Object.assign({}, dataGiftCardObj, dataOfferObj, dataEventObj, dataPointsRedemptionObj, dataBookingObj);
    return specificDetails;
}


function addSpecificDataToFinalDataLayer(generalDataLayerObj, availableObj) {
    for(let key in generalDataLayerObj){
        if(key == 'memberEmailId' || key == 'memberMobileNo'){
            availableObj[key] = availableObj[key] ? window.btoa(availableObj[key]) : null;
        }
        generalDataLayerObj[key] = availableObj[key] ? availableObj[key] : generalDataLayerObj[key];
    }
    return generalDataLayerObj;
}

function addParameterToDataLayerObj(event, availableObj) {
    var specificDetails = getSpecificDataFields();
    var memberDetails = getMmemberData();
	var hotelDetails = getHotelData();
    var pageDetails = getPageData();
    var deviceDetails = getDeviceData();
    var generalDataLayerObj = Object.assign({}, memberDetails, hotelDetails, pageDetails, deviceDetails, specificDetails);

    generalDataLayerObj.event = generalDataLayerObj.brandName == "TajInnerCircle" ? 'TIC_'+event : generalDataLayerObj.brandName+'_'+event;
	var finalDataLayerObj = addSpecificDataToFinalDataLayer(generalDataLayerObj, availableObj);

    console.log('finalDataLayerObj::: ', finalDataLayerObj);
    dataLayer.push(finalDataLayerObj);
}


function getMmemberData() {
	var user = getUserData();
    var dataMemberObj = {}
    dataMemberObj.isMembershipId  = user ? "yes" : "No";
    dataMemberObj.membershipId  = user ? user.membershipId : "";
    dataMemberObj.membershipType = user && user.card ? user.card.type : "";
    dataMemberObj.memberEmailId = user ? window.btoa(user.email) : "";
    dataMemberObj.memberMobileNo = user ? window.btoa(user.mobile) : "";
    dataMemberObj.memberName = user ? user.firstName + ' ' + user.lastName : "";
    dataMemberObj.memberGender = user ? user.gender : "";
    dataMemberObj.memberDOB = user ? user.dob : "";
    dataMemberObj.memberAge = getAgeFromDOB(user);
    dataMemberObj.memberCountry = getUserAddressData(user, "country");
    dataMemberObj.memberState = getUserAddressData(user, "state");
    dataMemberObj.memberCity = getUserAddressData(user, "city");
    dataMemberObj.memberPincode = getUserAddressData(user, "postalCode");
    dataMemberObj.userType = user ? "Returning" : "New";
    dataMemberObj.userStatus = user ? "LoggedIn" : "Not LoggedIn";
    dataMemberObj.visitSource = "Direct";
    dataMemberObj.existingCustomer = user ? "Yes" : "No";

    return dataMemberObj;
}


function getHotelData() {
    var hotelData = pageLevelData && pageLevelData.hotelName ? pageLevelData : null;
    var dataHotelObj = {}
    dataHotelObj.hotelCity = hotelData ? hotelData.hotelCity : "";
    dataHotelObj.hotelCode = hotelData ? hotelData.hotelCode : "";
    dataHotelObj.hotelCountry = hotelData ? hotelData.hotelCountry : "";
    dataHotelObj.hotelName = hotelData ? hotelData.hotelName : "";
    dataHotelObj.hotelPinCode = hotelData ? hotelData.hotelPinCode : "";
    dataHotelObj.hotelState = hotelData ? hotelData.hotelState : "";

    var dataBookingObj = dataCache ? dataCache.session.getData('bookingOptions') : null;
    dataHotelObj.checkInDate = dataBookingObj ? dataBookingObj.fromDate : "";
    dataHotelObj.checkOutDate = dataBookingObj ? dataBookingObj.toDate : "";
    dataHotelObj.numberofRooms = dataBookingObj ? dataBookingObj.rooms : "";
    dataHotelObj.adultsQty = dataBookingObj ? totalPersonCount(dataBookingObj.roomOptions, 'adults') : "";
    dataHotelObj.childQty = dataBookingObj ? totalPersonCount(dataBookingObj.roomOptions, 'children') : "";

    return dataHotelObj;
}


function totalPersonCount(roomOptions, key){
    var count = 0;
    if(roomOptions && roomOptions.length){
        roomOptions.forEach(function(room){
            if(room[key]){
            	count += room[key];
            }
        });
    }
    return count.toString();
}


function getPageData() {
    var dataPageObj = {}
    var h1Elem = $('h1');
    dataPageObj.brandName = getBrandName();
    dataPageObj.pageCategory = getPageCategory();
    dataPageObj.pageSubCategory = getPageCategory() != getPageSubCategory() ? getPageSubCategory() : "/";
    dataPageObj.pageName = h1Elem && h1Elem.text() ? h1Elem.text().trim() : "";
    dataPageObj.pageTitle = h1Elem && h1Elem.text() ? h1Elem.text().trim() : "";
    dataPageObj.pageLanguage = "en-in";

    return dataPageObj;
}

function getPageCategory(){
	var category = location.pathname.split('/en-in/');
    if(category && category.length  > 1){
		return category[1];
    }
    else {
		return "/";
    }
}


function getPageSubCategory(){
    var n = location.pathname.split("/");
    return n[n.length - 1] ? n[n.length - 1] : "/";
}

function getBrandName() {
	var hostname = location.href;
    var brand = "Taj"
    if(hostname.includes('tajinnercircle') || hostname.includes('taj-inner-circle')) {
		brand = "TajInnerCircle";
    } 
    else if(hostname.includes('ama')) {
		brand = "Ama"
    }
    else if(hostname.includes('vivantahotels') || hostname.includes('vivanta')) {
		brand = "Vivanta";
    }
    else if(hostname.includes('seleqtionshotels') || hostname.includes('seleqtions')) {
		brand = "Seleqtions"
    }
    return brand;
}


function getUserAddressData(user, key) {
    var value = "";
    if(user && user.addresses && user.addresses.length) {
        user.addresses.forEach(function(addr){
            if(addr.primary){
				value = addr[key];
            }
        });
    }
    return value;
}


function getAgeFromDOB(user){
    if(user && user.dob){

    }
	return '';
}


function getCookieDeviceId(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function getDeviceData() {
    var dataDeviceObj = {}
    dataDeviceObj.device = deviceType ? deviceType : "";
    dataDeviceObj.clientId = (getCookieDeviceId('_ga')).slice(6);
	return dataDeviceObj;
}


function getBrandNameForEvent() {
    var hostname = location.href;
    var brand = "Taj"
    if(hostname.includes('tajinnercircle') || hostname.includes('taj-inner-circle')) {
		brand = "TIC";
    } 
    else if(hostname.includes('ama')) {
		brand = "Ama"
    }
    else if(hostname.includes('vivantahotels') || hostname.includes('vivanta')) {
		brand = "Vivanta";
    }
    else if(hostname.includes('seleqtionshotels') || hostname.includes('seleqtions')) {
		brand = "Seleqtions"
    }
    return brand;
}