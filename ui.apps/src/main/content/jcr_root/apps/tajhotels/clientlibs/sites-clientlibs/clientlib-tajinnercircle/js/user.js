var warningShown = false;

// User related common functions
function userCacheExists() {
    var userDetails = dataCache.local.getData("userDetails");
    if (!$.isEmptyObject(userDetails)) {
        return userDetails;
    }
    return null;
}

function registerLoginListener(postLoginHandler) {
    $('body').on('taj:loginSuccess', postLoginHandler);
    allowPageScroll();
}

function profileFetchListener(postEventHandler) {
    $('body').on('taj:profile-fetch-success', postEventHandler);
}

function getUserData() {
    if (dataCache.local.hasData("tajData"))
        return dataCache.local.getData("userDetails");
    return null;
}

function setUserData(userData) {
    dataCache.local.setData("userDetails", userData);
}

// Call this function, only after login
function isEpicureMember() {
    var userCard = getUserData().card;
    if (!$.isEmptyObject(userCard)) {
        if (userCard.addOn && userCard.addOn.toLowerCase().includes("epicure")) {
            return true;
        }
    }
    return false;
}

function userLoggedIn() {
    if (getUserData()) {
        return !!getUserData().authToken;
    }
    return false;
}

function getPrimaryAddress(addressList) {
    if (addressList.length > 1) {
        return addressList.find(function(address) {
            return address.primary;
        });
    } else if (addressList.length === 1) {
        return addressList[0];
    }
}
function getMemberMobileNumber(userDetails) {
    var alternatePhoneNumber = "";
    var alternatePhoneNumbers = userDetails.alternatePhoneNumbers;
    if (alternatePhoneNumbers) {
        alternatePhoneNumbers.forEach(function(phone) {
            var primary = phone.primary;
            if (primary == true) {
                alternatePhoneNumber = phone.alternatePhone;
            }
        });
    }
    return alternatePhoneNumber;
}

function linkLoyaltyMembershipUser(membershipId) {
    if (getUserData() && getUserData().authToken) {
        var requestData = {
            authToken : getUserData().authToken,
            membershipId : membershipId
        };
        return $.ajax({
            type : 'post',
            url : '/bin/user/link-tic',
            data : JSON.stringify(requestData),
            contentType : 'application/json'
        });
    } else {
        return new $.Deferred().fail();
    }
}

// works with either membership id or email id
function sendOtp(loginId) {
    var dataInput = {
        username : loginId
    };

    return $.ajax({
        type : 'GET',
        cache: false,
        url : '/bin/send-activation-code',
        data : dataInput
    });
}

function validateOtp(otp, email) {
    var dataInput = {
        username : email,
        otp : otp,
        action : 'new-online-user'
    }
    return $.ajax({
        type : 'GET',
        cache: false,
        url : '/bin/validate-code',
        data : dataInput,
    });
}
function partnerValidation(formData) {

  return $.ajax({
        cache : false,
        contentType : false,
        processData : false,
        type : 'POST',
        url : '/bin/tic/partner/member/validation',
        data : formData,

  });
}

function enrolMember(inputData) {
    return $.ajax({
        type : "POST",
        cache: false,
        url : "/bin/tic/member-enrol",
        data : JSON.stringify(inputData),
        contentType : "application/json"
    });
}

function registerUser(inputData) {
    return $.ajax({
        type : "POST",
        cache: false,
        url : "/bin/user-register",
        data : JSON.stringify(inputData),
        contentType : "application/json"
    });
}

function fetchMemberPoints(membershipId) {
    var userDetailJson = getUserData();
    if(userDetailJson && userDetailJson.authToken) {
        authToken = userDetailJson.authToken;
        return $.ajax({
            type : "POST",
            cache : false,
            url : "/bin/points",
            data: {
                'membershipId': membershipId,
                'authToken': authToken
            },
    
        }).done(function(res) {
            try {
				if(res) {
					var data = JSON.parse(res);
					saveMemberPoints(data.epicureAvailablePoints, data.ticAvailablePoints, data.tapAvailablePoints,
									 data.tappmeAvailablePoints);
				}
            } catch (error) {
                console.error(error);
            }
         }).fail(function(res) {
            if (res.errorText) {
                console.log(res.errorText);
            }
            if(res.status === 401){
                forceLogoutAfterUnauthorized();
            }
        });
    }
}

function saveMemberPoints(epicurePoints, ticPoints, tapPoints, tappmePoints) {
    var userData = getUserData();
    userData.epicurePoints = epicurePoints;
    userData.ticPoints = ticPoints;
    userData.tapPoints = tapPoints;
    userData.tappmePoints = tappmePoints;
    setUserData(userData);
    $('body').trigger('taj:pointsUpdated');
}
function preventPageScroll() {
    $(".cm-page-container").addClass('prevent-page-scroll');
    $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');
}
function allowPageScroll() {
    $(".cm-page-container").removeClass('prevent-page-scroll');
    $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
}
function callwarningclose() {
    $('.warning-box-close').click(function() {
        allowPageScroll();
    });
}


function forceLogoutAfterUnauthorized(){
    var popupParams = {
        title: 'Session timed out',
        description : "Please Login again to continue."
    };
    if($('.cm-warning-box-main') && $('.cm-warning-box-main').length===0 && !warningShown){
        warningBox(popupParams);
        warningShown = true;
        $('.cm-warning-box-main > .cm-warning-box-inner-wrap')
        .append("<button style='margin-top: -15px;' class='btn cm-btn-secondary subscribe-btn tajhotel-subscribe-btn logout' data-dialog-btn='subscribeBox' onClick='unauthorizedLogout(true)'>Login again</button>");
        $(".cm-warning-box-main .warning-box-close").on('click', function(){
			unauthorizedLogout(false);
        });
    }
}


function unauthorizedLogout(type){
	var signOutRedirect = "/content/taj-inner-circle/en-in.html/";

    // removing data from bot
    //logoutBot();

    $('body').trigger('taj:sessionLogout');

    closeWarningPopup();
    if(type == true){
		signOutRedirect = signOutRedirect + "?popup=true";
		$('body').trigger('taj:sign-in');
    } else {
        logoutBot();
    	window.location.href = signOutRedirect;
    }
}

function closeWarningPopup(){
	var warningPopup = $( '.cm-warning-box-con' );
    warningPopup.remove();
    $(".cm-page-container").removeClass('prevent-page-scroll');
    $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
}


//Setting member data to bot after login
function dataToBot() {
    var user = getUserData();
	var iframe = document.getElementById('ymIframe');
    if(iframe && iframe.contentWindow){
        iframe.contentWindow.postMessage(JSON.stringify({
            event_code: 'ym-client-event', data: JSON.stringify({
                event: {
                    code: "login",
                    data: {
                        memberNumber: user.membershipId,
                        mobile: user.mobile,
                        name: user.name,
                        email: user.email,
                    },
                }
            })
        }), null);
    }
}


function logoutBot() {
	var iframe = document.getElementById('ymIframe');
    if(iframe && iframe.contentWindow){
        iframe.contentWindow.postMessage(JSON.stringify({
            event_code: 'ym-client-event', data: JSON.stringify({
                event: {
                    code: "logout",
                    data: {
                        memberNumber: '',
                        mobile: '',
                        name: '',
                        email: '',
                    },
                }
            })
        }), null);
    }
}