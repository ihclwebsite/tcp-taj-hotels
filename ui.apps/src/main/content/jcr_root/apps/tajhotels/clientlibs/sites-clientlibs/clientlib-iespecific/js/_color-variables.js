var gatewayThemeWrapper = document.querySelector( '.gateway-theme' );
var vivantaThemeWrapper = document.querySelector( '.vivanta-theme' );
var seleqtionsThemeWrapper = document.querySelector( '.seleqtions-theme' );
var ihclThemeWrapper = document.querySelector( '.ihcl-theme' );
if ( seleqtionsThemeWrapper ) {
    primaryColorValue = '#54575a';
    primaryColorLightValue = '#949599';
    primaryColorLighterValue = 'rgba(84, 87, 90, 0.2)';
    primaryColorDarkValue = '#54575a';
    linearGradientValue = 'linear-gradient(to top, #54575a, #54575a 52%, rgba(84, 87, 90, 0.7))';    
} else if ( vivantaThemeWrapper ) {
    primaryColorValue = '#333366';
    primaryColorLightValue = '#9090c5';
    primaryColorLighterValue = 'rgba(51, 51, 102, 0.2)';
    primaryColorDarkValue = '#333366';   
    linearGradientValue = 'linear-gradient(to bottom, rgba(144, 144, 197, 0.99), #4b4b85 49%, #333366)'; 
} else if ( gatewayThemeWrapper ) {
    primaryColorValue = '#d72741';
    primaryColorLightValue = '#f67d83';
    primaryColorLighterValue = 'rgba(215, 39, 65, 0.2)';
    primaryColorDarkValue = '#d5213d';  
    linearGradientValue = 'linear-gradient(to top, #d5213d, #f67d83)';  
} else if ( ihclThemeWrapper ) {
	primaryColorValue = '#002b49';
    primaryColorLightValue = 'rgba(84, 87, 90, 0.7)';
    primaryColorLighterValue = 'rgba(84, 87, 90, 0.2)';
    primaryColorDarkValue = '#54575a';   
    linearGradientValue = 'linear-gradient(to top, #54575a, #54575a 52%, rgba(84, 87, 90, 0.7))';
}	else {
    primaryColorValue = '#d29751';
    primaryColorLightValue = 'rgba(210, 151, 81, 0.8)';
    primaryColorLighterValue = 'rgba(210, 151, 81, 0.2)';
    primaryColorDarkValue = '#aa7938';
    linearGradientValue = 'linear-gradient(to top, #7b5519, #aa7938 52%, #d29851)';
}

cssVars( {
    variables: {
        primaryColor: primaryColorValue,
        primaryColorLight: primaryColorLightValue,
        primaryColorLighter: primaryColorLighterValue,
        primaryColorDark: primaryColorDarkValue,
        linearGradient: linearGradientValue
    }
} );