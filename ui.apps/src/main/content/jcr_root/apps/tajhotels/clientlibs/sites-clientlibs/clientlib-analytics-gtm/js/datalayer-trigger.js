var trigger_jiva_view = function(treatmentName) {
    dataLayer.push({
        'event' : 'jiva_page_view',
        'hotelOffering' : 'Jiva Spa',
        'hoteOfferingName' : treatmentName
    });
}

var tigger_jiva_book_appointment = function(bookingData) {
    dataLayer.push({
        'event' : 'jiva_booking_appointment',
        'hotelName' : bookingData.hotelName,
        'bookingDate' : bookingData.bookingDate,
        'city' : bookingData.city,
        'gender' : bookingData.gender,
        'timeToContact' : bookingData.timeToContact
    });
}

var tigger_meeting_quote = function(bookingData) {
    dataLayer.push({
        'event' : 'meeting_trigger_quote',
        'hotelName' : bookingData.hotelName,
        'bookingDate' : bookingData.bookingDate,
        'noOfGuest' : bookingData.numOfGuests,
    });
}

var trigger_experience_view = function(experienceName) {
    dataLayer.push({
        'event' : 'singature_experience_view',
        'hotelOffering' : 'Signature Experiences',
        'hoteOfferingName' : experienceName
    });
}

var trigger_hotelSearch = function(bookingData) {
    var roomDetails = [];
    roomOptions = bookingData.roomOptions;
    for (i = 0; i < roomOptions.length; i++) {
        var roomDetail = {};
        roomDetail["noOfAudults"] = roomOptions[i].adults;
        roomDetail["noOfChilds"] = roomOptions[i].children;
        roomDetails.push(roomDetail)
    }
    var hotelName = "";
    if (bookingData.targetEntity != undefined) {
        hotelName = bookingData.targetEntity;
    }
    if (hotelName == "" && dataLayerData != undefined) {
        hotelName = dataLayerData.hotelName;
    }
    dataLayer.push({
        'event' : 'hotel_search_booking',
        'hotelCode' : bookingData.hotelId,
        'hotelName' : hotelName,
        'arrival' : bookingData.fromDate,
        'departure' : bookingData.toDate,
        'no_Of_Rooms' : bookingData.rooms,
        'promoCode' : bookingData.promotionCode,
        'roomDetails' : roomDetails,
        'totalPrice' : bookingData.totalCartPrice
    });

}
function countTax(selection, i){
    var totalTaxPerRoom=0;
    var nightlyRatesArr=[];
    nightlyRatesArr=selection[i].nightlyRates
    if(nightlyRatesArr && nightlyRatesArr.length>0){
        $(nightlyRatesArr).each(function(index){
           totalTaxPerRoom=totalTaxPerRoom+parseInt(nightlyRatesArr[index].tax);
        });
    }
    return totalTaxPerRoom;
}
var trigger_roomSelection = function(bookingData) {
    var roomDetails = [];
    if (bookingData.selection) {
        selection = bookingData.selection;
        for (i = 0; i < selection.length; i++) {
            var roomDetail = {};
            roomDetail["noOfAudults"] = selection[i].adults;
            roomDetail["noOfChilds"] = selection[i].children;
            roomDetail["roomTypeCode"] = selection[i].roomTypeCode;
            roomDetail["roomCostAfterTax"] = selection[i].selectedRate+ countTax(selection, i);;
            roomDetail["ratePlanCode"] = selection[i].ratePlanCode;
            roomDetails.push(roomDetail)
        }
        dataLayer.push({
            'event' : 'trigger_roomSelection',
            'hotelCode' : bookingData.hotelId,
            'hotelName' : bookingData.targetEntity,
            'arrival' : bookingData.fromDate,
            'departure' : bookingData.toDate,
            'no_Of_Rooms' : bookingData.selection.length,
            'promoCode' : bookingData.promotionCode,
            'roomDetails' : roomDetails,
            'totalPrice' : bookingData.totalCartPrice
        });
    }
}

var trigger_guestDetails = function(bookingData, guestDetails) {
    console.log(bookingData);
    var selection = bookingData.selection;
    var roomDetails = [];
    for (i = 0; i < selection.length; i++) {
        var roomDetail = {};
        roomDetail["noOfAudults"] = selection[i].adults;
        roomDetail["noOfChilds"] = selection[i].children;
        roomDetail["roomTypeCode"] = selection[i].roomTypeCode;
        roomDetail["roomCostAfterTax"] = selection[i].selectedRate+ countTax(selection, i);
        roomDetail["ratePlanCode"] = selection[i].ratePlanCode;
        roomDetails.push(roomDetail)
    }
    dataLayer.push({
        'event' : 'trigger_guestDetails',
        'pageTitle' : 'booking-guestDetails',
        'hotelCode' : bookingData.hotelId,
        'hotelName' : bookingData.targetEntity,
        'arrival' : bookingData.fromDate,
        'departure' : bookingData.toDate,
        'no_Of_Rooms' : bookingData.selection.length,
        'promoCode' : bookingData.promotionCode,
        'roomDetails' : roomDetails,
        'totalPrice' : bookingData.totalCartPrice,
        'we_first_name' : guestDetails.guestFirstName,
        'we_last_name' : guestDetails.guestLastName,
        'we_email' : guestDetails.guestEmail,
        'we_phone' : guestDetails.guestPhoneNumber,
        'we_country' : guestDetails.guestCountry,
        'we_birth_date' : '',
        'specialRequests' : guestDetails.specialRequests,
        'TICMembershipNo' : guestDetails.guestMembershipNumber

    });
}


var trigger_bookingCheckout = function(bookingData, guestDetails, paymentsDetails) {
    var selection = bookingData.selection;
    var firstName = guestDetails.guestFirstName;
    var lastName = guestDetails.guestLastName;
    var fullName = firstName + " " + lastName;
    var nameArr = fullName.split(" ");
    var roomDetails = [];
    for (i = 0; i < selection.length; i++) {

        var roomDetail = {};
        roomDetail["noOfAudults"] = selection[i].adults;
        roomDetail["noOfChilds"] = selection[i].children;
        roomDetail["roomTypeCode"] = selection[i].roomTypeCode;

        roomDetail["roomCostAfterTax"] = selection[i].selectedRate+ countTax(selection, i);
        roomDetail["ratePlanCode"] = selection[i].ratePlanCode;
        roomDetails.push(roomDetail)
    }
    dataLayer.push({
        'event' : 'trigger_bookingCheckout',
        'pageTitle' : 'booking-checkout',
        'hotelCode' : bookingData.hotelId,
        'hotelName' : bookingData.targetEntity,
        'arrival' : bookingData.fromDate,
        'departure' : bookingData.toDate,
        'no_Of_Rooms' : bookingData.selection.length,
        'promoCode' : bookingData.promotionCode,
        'roomDetails' : roomDetails,
        'totalPrice' : bookingData.totalCartPrice,
        'creditCardType' : paymentsDetails.cardType,
        'we_first_name' : firstName,
        'we_last_name' : lastName,
        'we_email' : guestDetails.guestEmail,
        'we_phone' : guestDetails.guestPhoneNumber,
        'we_country' : guestDetails.guestCountry,
        'we_birth_date' : '',
        'specialRequests' : guestDetails.specialRequests,
        'TICMembershipNo' : guestDetails.guestMembershipNumber

    });
}

var trigger_bookingConfirm = function(bookingData) {
    console.log("booking confim event triggered")
    console.log(bookingData)
    dataLayer.push({
        'event' : 'trigger_bookingConfirm'
    });
}

var trigger_diningAvailability = function(diningBookingData) {
    dataLayer.push({
        'event' : 'trigger_diningAvailability',
        'mealType' : diningBookingData.mealType,
        'peopleCount' : diningBookingData.peopleCount,
        'diningID' : diningBookingData.diningID,
        'diningName' : diningBookingData.diningName,
        'bookingDate' : diningBookingData.bookingDate
    });

}

var trigger_tableReservation = function(diningBookingData) {
    dataLayer.push({
        'event' : 'trigger_diningReservation',
        'mealType' : diningBookingData.mealType,
        'peopleCount' : diningBookingData.peopleCount,
        'diningID' : diningBookingData.diningID,
        'diningName' : diningBookingData.diningName,
        'bookingDate' : diningBookingData.bookingDate,
        'slot' : diningBookingData.slot,
        'firstName' : diningBookingData.firstName,
        'eventName' : diningBookingData.eventName
    });
}



// updated for global data layer
function trigger_signin(event, obj){
	var reg = new RegExp('^[0-9]+$');
    if(reg.test(obj.username)){
        obj.membershipId = obj.username;
        addParameterToDataLayerObj(event, obj);
    } else {
        obj.memberEmailId = obj.username;
		addParameterToDataLayerObj(event, obj);
    }
}
