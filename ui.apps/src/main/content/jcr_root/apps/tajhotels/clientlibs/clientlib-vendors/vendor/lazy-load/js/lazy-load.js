(function($) {
    /**
     * Loads images as they come into view
     * loadNextViewportImages - set as true to load images in next window height also
     */
    var SCROLL_WINDOW_DEBOUNCE_RATE = 100;
    $.fn.infiniteScrollLazyLoad = function(loadNextViewportImages) {
        var images = $(this).find('img[data-src]');
        var lastScrollTop = 0;
        $(window).on('scroll', debounce(function(event) {
            var st = $(this).scrollTop();
            if(st > lastScrollTop) {
                for (var i=0; i < images.length; i++) {
                    if (images[i].getAttribute('data-src') && $(images[i]).is(':visible') && $(images[i]).isInViewport(loadNextViewportImages) ) {
                        images[i].setAttribute('src', images[i].getAttribute('data-src'));
                        images[i].removeAttribute('data-src');
                    }
                }
            }
            lastScrollTop = st;
        }, SCROLL_WINDOW_DEBOUNCE_RATE));
    }
})(jQuery);

$(document).ready(function(){
	//On page load functions only.
	try{
		$(".cm-page-container").infiniteScrollLazyLoad(true);
	}catch(error){
		console.error(error);
	}
});
