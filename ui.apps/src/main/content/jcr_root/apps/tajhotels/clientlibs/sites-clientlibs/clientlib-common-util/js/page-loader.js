(function($) {
    /**
     * Utility to add and remove a page loader componentLoader = true displays a loader only over the element passed.
     */
    var preventPageScrollInitial;
    $.fn.showLoader = function(componentLoader) {

        var loaderClass = "taj-page-loader";
        var componentId = "taj-page-loader";
        if (componentLoader) {
            loaderClass = "taj-comp-loader";
            componentId = "taj-comp-loader";
        }

        if ($(this).is("body")) {
            preventPageScrollInitial = $('.cm-page-container.prevent-page-scroll:visible').length;
            $('.cm-page-container').addClass("prevent-page-scroll");
        }

        if (!$(this).find('[data-component-id="' + componentId + '"]').length) {
            if (componentLoader) {
                $(this).css("position", "relative");
            }
            $(this).append(
                    '<div data-component-id="' + componentId + '" class="' + loaderClass + '">'
                            + '<div class="taj-loader spinner_wait_con check-wait-spinner">'
                            + '<div class="taj-loader-circle"></div>' + '<div class="taj-loader-circle"></div>'
                            + '<div class="taj-loader-circle"></div>' + '</div>' + '</div>');
        }
    }
    $.fn.hideLoader = function(componentLoader) {

        if ($(this).is("body") && !preventPageScrollInitial) {
            $('.cm-page-container').removeClass("prevent-page-scroll");
            preventPageScrollInitial = 0;
        }
        if (componentLoader) {
            $(this).find('[data-component-id="taj-comp-loader"]').remove();
        } else {
            $('[data-component-id="taj-page-loader"]').remove();
        }
    }
})(jQuery);
