	(function($) {
    $.fn.showMore = function(showMoreCardOffset,initialCardLoad) {
        var element = this;
        var count = element.children().children().length;             
        if ($(window).width() > 991) {
            showMoreBasic( (initialCardLoad || 3) ,showMoreCardOffset);
        } else if ($(window).width() > 767 && $(window).width() < 992) {
            showMoreBasic( (initialCardLoad || 2) ,showMoreCardOffset);
        } else if ($(window).width() < 768) {
            showMoreBasic( (initialCardLoad || 1) ,showMoreCardOffset);
        } 
        

        function showMoreBasic(initialCardLoad,showMoreCardOffset) {
        	showMoreCardOffset = showMoreCardOffset || 12; 
        	$(element.children().children()).css('display','none');
            for (i = 0; i < initialCardLoad; i++) {
                var currentCard = $(element.children().children().eq(i));
                lazyLoadCurrentElement(currentCard);
                currentCard.css('display', 'block');
            }
            if(element.next().hasClass('jiva-spa-show-more')){
                element.next().remove();
            }            
            if (count > initialCardLoad) {
                element.parent().append('<div class="jiva-spa-show-more"><button class="btn-only-focus">SHOW MORE<span class="inline-block icon-drop-down-arrow"></span></button></div>')
            } 
            
            var limit = Math.ceil(( count-initialCardLoad ) / showMoreCardOffset);
            var counter = 0;  
            (element.siblings('.jiva-spa-show-more')).on('click', function() {            	
                if ($(this).find('.icon-drop-down-arrow').hasClass('show-more-inverted')) {
                    for (i = initialCardLoad; i <= count; i++) {
                        $(element.children().children().eq(i)).css('display', 'none');
                    }                    
                    $(this).find('button').html('SHOW MORE<span class="inline-block icon-drop-down-arrow"></span>');
                    counter = 0;
                } else {
                    if (counter < limit) {
                    	var counterStartIndex = (counter * showMoreCardOffset) + initialCardLoad;                         
                        for (i = counterStartIndex; i < (counterStartIndex + showMoreCardOffset); i++) {
                            var currentCard = $(element.children().children().eq(i));
                            lazyLoadCurrentElement(currentCard);
                            currentCard.css('display', 'block');
                        }
                        counter++;
                        if (counter == limit){
                        	$(this).find('button').html('SHOW LESS<span class="show-more-inverted inline-block icon-drop-down-arrow"></span>');
                        }
                    }else{                        
                        counter = 0;
                    }                    
                }
            });

        }

        function lazyLoadCurrentElement(element) {
            var cardImages = $(element).find('img[data-src]');
            for (var i=0; i < cardImages.length; i++) {
                if (cardImages[i].getAttribute('data-src')) {
                    cardImages[i].setAttribute('src', cardImages[i].getAttribute('data-src'));
                    cardImages[i].removeAttribute('data-src');
                }
            }
        }

    }
}(jQuery));
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYXJrdXAvY29tcG9uZW50cy9jb3JwLVN1c3RhaW5hYmlsaXR5LWNhcmQvY29ycC1TdXN0YWluYWJpbGl0eS1jYXJkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGFib3V0VXNTdXN0YWluYWJpbGl0eUNhcmQoKSB7XHJcbiAgICAkKCBkb2N1bWVudCApLnJlYWR5KCBmdW5jdGlvbigpIHtcclxuICAgICAgICAkKCAnLnN1c3RhaW5hYmlsaXR5LWNhcmQtd3JhcCBwJyApLmVhY2goIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAkKCB0aGlzICkuY21Ub2dnbGVUZXh0KCB7XHJcbiAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDM1MFxyXG4gICAgICAgICAgICB9IClcclxuICAgICAgICB9ICk7XHJcbiAgICAgICAgaWYgKCBkZXZpY2VEZXRlY3Rvci5jaGVja0RldmljZSgpID09IFwic21hbGxcIiApIHtcclxuICAgICAgICAgICAgJCggJy5zdXN0YWluYWJpbGl0eS1jYXJkLXdyYXAgcCcgKS5lYWNoKCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgICQoIHRoaXMgKS5jbVRvZ2dsZVRleHQoIHtcclxuICAgICAgICAgICAgICAgICAgICBjaGFyTGltaXQ6IDIwMFxyXG4gICAgICAgICAgICAgICAgfSApXHJcbiAgICAgICAgICAgIH0gKTtcclxuICAgICAgICB9XHJcbiAgICB9ICk7XHJcbn0iXSwiZmlsZSI6Im1hcmt1cC9jb21wb25lbnRzL2NvcnAtU3VzdGFpbmFiaWxpdHktY2FyZC9jb3JwLVN1c3RhaW5hYmlsaXR5LWNhcmQuanMifQ==
$.fn.parallelShowMoreFn = function() {
	var descSection = this;
	var shortDesc = this.children().eq(0);
	var largeDesc = this.children().eq(1);
	if (largeDesc.children().length) {
		shortDesc.children('p').last().append('<span class="ihcl-show-more">Show More</span>');
		largeDesc.children('p').last().append('<span class="ihcl-show-less">Show Less</span>');
		largeDesc.css('display','none');
	}
	$('.ihcl-show-more').on('click',function() {
		$(this).parent().parent().css('display','none');
		$(this).parent().parent().siblings().css('display','block');
	});
	$('.ihcl-show-less').on('click',function() {
		largeDesc.css('display','none');
		shortDesc.css('display','block');
	});
	};
