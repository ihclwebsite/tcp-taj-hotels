$(document).ready(function() {
    try{
        var url_string = window.location.href;
        //var url = new URL(url_string);
        //var reqParam = url.searchParams.get("authToken");
		var reqParam = getQueryParameter('authToken');
        var authToken = encodeURIComponent(reqParam);
        if (reqParam) {
            validateUserDetails(authToken);
        }
    }catch(error){
        console.log(error);
    }
});

function validateUserDetails(authToken) {
    $.ajax({
        type : 'post',
        cache: false,
        url : '/bin/fetch-user-details-tic',
        data : "authToken=" + authToken,
        error : function(response) {
        },
        success : function(response) {
            if (response.data) {
                var memberData = response.data;
                var userDetails = {};
                userDetails.name = memberData.name;
                userDetails.firstName = memberData.firstName;
                userDetails.lastName = memberData.lastName;
                userDetails.email = memberData.email;
                userDetails.mobile = memberData.mobile;
                userDetails.cdmReferenceId = memberData.cdmReferenceId;
                userDetails.membershipId = memberData.membershipId;
                userDetails.googleLinked = memberData.googleLinked;
                userDetails.facebookLinked = memberData.facebookLinked;
                userDetails.title = memberData.title;
                saveUserDetails(authToken, userDetails);
                dataToBot();
            }
        }
    });
}

function saveUserDetails(authToken, userDetails) {
    var user = {
        authToken : authToken,
        name : userDetails.name,
        firstName : userDetails.firstName,
        lastName : userDetails.lastName,
        email : userDetails.email,
        mobile : userDetails.mobile,
        cdmReferenceId : userDetails.cdmReferenceId,
        membershipId : userDetails.membershipId,
        googleLinked : userDetails.googleLinked,
        facebookLinked : userDetails.facebookLinked,
        title : userDetails.title
    };
    dataCache.local.setData("userDetails", user);
}
