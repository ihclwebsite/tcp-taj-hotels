( function GlobalVariables() {
    this.dataCache = {};
} ).call( this );

( function dropdowns() {
    this.initDropdown = function( element, target ) {
        this.targetBlock = target;
        this.options = target.options;
        this.targetElem = element;
    };

    initDropdown.prototype.initialize = function() {
        if ( this.options ) {
            var itrLength = this.options.length;
            for ( i = 0; i < itrLength; i++ ) {
                this.targetElem.append( '<option>' + this.options[ i ] + '</option>' )
            };
        };

        // Initializes selectboxit on the rendered dropdown
        this.targetElem.selectBoxIt();

        // Add listeners to each selectbox change
        this.listenDDChange();
    };

    initDropdown.prototype.listenDDChange = function() {
        var dTarget = this.targetBlock;
        dTarget.elem.change( function() {
            var selectedOption = ( dTarget.elem ).find( "option:selected" );
            dTarget.selected = selectedOption.text();
            if ( dTarget.dependent.elem ) {
                dTarget.dependent.elem.selectBoxIt( 'selectOption', 0 );
                if ( dTarget.selected != dTarget.default ) {
                    dTarget.dependent.elem.prop( "disabled", false );
                } else {
                    dTarget.dependent.elem.prop( "disabled", true );
                }
            } else {
                if ( dTarget.selected != dTarget.default ) {
                    // "dropDowns" Gives the selected values for each dropdown options
                }
            }
        } );
    };
} ).call( this );


( function applicationStorage() {
    function Cache( storageType ) {
        if ( typeof( Storage ) !== "undefined" ) {
            this.data = JSON.parse( storageType.getItem( 'tajData' ) ) || {};
        }
        this.isEmpty = function(){
            if(storageType.length==0)
                return true;
            return false;
        }
        this.hasData = function(value){
            if(storageType[value])
                return true;
            return false;
        }
        this.setData = function( key, value ) {
            this.data[ key ] = value;
            if ( typeof( Storage ) !== "undefined" ) {
                storageType.setItem( 'tajData', JSON.stringify( this.data ) );
            }
        };
        this.getData = function( key ) {
            try {
                if ( typeof( Storage ) !== "undefined" ) {
                    return JSON.parse( storageType.getItem( 'tajData' ) )[ key ];
                }
            } catch ( e ) {
                console.error( e );
            }
        };
        this.getAllData = function() {
            if ( typeof( Storage ) !== "undefined" ) {
                return JSON.parse( storageType.getItem( 'tajData' ) );
            }
        };
        this.removeData = function( key ) {
            this.setData( key, null );
        };
        this.clearStorage = function() {
            if ( typeof( Storage ) !== "undefined" ) {
                storageType.clear();
            }
        };
    };

    function SessionCache() {
        return new Cache( sessionStorage );
    };

    function LocalCache() {
        return new Cache( localStorage );
    };

    this.dataCache.session = new SessionCache();
    this.dataCache.local = new LocalCache();

    /*
     * dataCache.session.setData("sampleData", "Hey There!! This is how you can set data");
     * dataCache.session.getData("sampleData");
     */

    dataCache.session.setData('sessionStorageEnabled',true);
    dataCache.local.setData('localStorageEnabled',true);
    
    $( window ).on( 'beforeunload', function() {
        dataCache.session.setData( "lastLocation", window.location.href );
    } );


} ).call( this );


( function appToaster() {
    this.tajToaster = function( message ) {
        var templateToaster = '<div class="cm-toaster">' +
            '<div class="cm-toaster-main">' +
            '<div class="toaster-logo-con">' +
            '<i class="toaster-logo icon-STAR-TAJ"></i>' +
            '<div class="warning-icon-con">' +
            '<img class="icon-warning" src="/content/dam/tajhotels/icons/style-icons/icon-warning.svg" alt = "warning icon"/>' +
            '</div></div>' +
            '<div class="cm-toaster-text">' + message + '</div><div class="toaster-close"><span class="toaster-close-icon icon-close"></span></div>' +
            '</div>' +
            '</div>';
        $( '.cm-page-container' ).append( templateToaster );
        var thisToaster = $( 'body' ).find( '.cm-toaster' ).last();
        setTimeout( function() {
            thisToaster.find( '.cm-toaster-main' ).addClass( 'active' );
        }, 10 );
        $( 'body' ).on( 'click', '.toaster-close', function( e ) {
            e.stopPropagation();
            var toasterParent = $( this ).closest( '.cm-toaster' )
            destroyToaster( toasterParent );
        } );
        $( 'body' ).on( 'click', '.cm-toaster', function( e ) {
            e.stopPropagation();
            /* destroyToaster( $( this ) ); */
        } );
        var toastrInterval = setTimeout( function() {
            // destroyToaster( thisToaster );
        }, 10000 );

        function destroyToaster( elem ) {
            clearTimeout( toastrInterval );
            elem.remove();
        };
    };
} ).call( this );

( function() {
    this.warningBox = function( params ) {

        // Default scenarion considers it to be an error scenario
        // Possible screnarios
        // Error - Dafault Scenarios
        // Error with Proceed or cancel btn- needsCta: true, callBack: function binded with the
        // scope from which function was called

        // Warning - isWarning: true
        // Warning with Proceed or cancel btn- isWarning: true, needsCta: true,
        // callBack: function binded with the scope from which function was called
        var defaultParams = {
            title: 'Error!',
            description: '',
            callBack: '',
            callBackSecondary: '',
            needsCta: false,
            isWarning: false,
            isForRoomAvailability: false
        };
        params = $.extend( {}, defaultParams, params );

        var _tempWarningBox = '<div class="cm-warning-box">' +
            '<div class="cm-warning-box-main">' +
            '<div class="cm-warning-box-inner-wrap">' +
            '<div class="warning-box-close"><span class="warning-box-close-icon icon-close"  tabindex = "0" data-dialog-close = "WarningBox"></span></div>' +
            '<div class="cm-warning-box-logo-con"><i class="toaster-logo icon-STAR-TAJ"></i>' +
            '<div class="warning-icon-con"><img class="icon-warning" src="/content/dam/tajhotels/icons/style-icons/icon-warning.svg" alt = "warning icon"/></div>' +
            '</div>' +
            '<div class="cm-warning-box-heading-text"></div>' +
            '<div class="cm-warning-box-description-text"></div>' +
            '<div class="cm-room-availability-description">' +
            '<div class="cm-room-availability-desc-wrap">' +
            '<table class="selected-rooms-details"></table>' +
            '</div>' +
            '</div>' +
            '<div class="cm-warning-box-btns">' +
            '<div class="cm-warning-box-proceed-btn">' +
            '<button class="cm-btn-secondary warning-proceed-btn"><span>Proceed</span><span class="dummy-to-middle-align"></span></button>' +
            '</div>' +
            '<div class="cm-warning-box-cancel-btn">' +
            '<button class="cm-btn-primary warning-cancel-btn"><span>Cancel</span><span class="dummy-to-middle-align"></span></button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';


        $( '.cm-page-container' ).append( '<div class="cm-warning-box-con warningBoxRemoveRef" role = "dialog" data-dialog-modal = "WarningBox" aria-expanded = "false">' + _tempWarningBox + '</div>' );
        var warningPopup = $( '.cm-warning-box-con' );
        var proceedBtn, cancelBtn, warningHeading, warningDescription, warningClose,
            btnsCon;
        var  preventPageScrollInitial = $('.cm-page-container.prevent-page-scroll:visible').length;
        // warningPopup.load("../../components/warning-popup/warning-popup.html", function() {
        proceedBtn = warningPopup.find( '.warning-proceed-btn' );
        cancelBtn = warningPopup.find( '.warning-cancel-btn' );
        warningHeading = warningPopup.find( '.cm-warning-box-heading-text' );
        warningDescription = warningPopup.find( '.cm-warning-box-description-text' );
        elRoomAvailability = warningPopup.find( '.cm-room-availability-description' );
        warningClose = warningPopup.find( '.warning-box-close' );
        btnsCon = warningPopup.find( '.cm-warning-box-btns' );

        warningHeading.html( params.title );
        warningDescription.html( params.description );


        if ( params.title ) {
            warningHeading.css( 'display', 'block' );
        }
        if ( params.description ) {
            warningDescription.css( 'display', 'block' );
        }
        if ( params.needsCta ) {
            btnsCon.css( 'display', 'block' );
        }

        if ( params.isWarning ) {
            warningPopup.addClass( 'rendered-as-warning' );
        }

        if ( params.isForRoomAvailability ) {
            warningPopup.addClass( 'rendered-as-room-availablity' );
            params.rooms.forEach( function( value, index ) {
            	var tempRoomStatus = '<tr class="selected-room-detail">' +
                '<td><img src="/content/dam/tajhotels/icons/style-icons/king-size-bed.jpg" alt = "king size bed icon"/></td>' +
                '<td><span class="number-of-room">' + ( index + 1 ) + 'ROOM:</span></td>' +
                '<td><span class="conf-room-type">' + value.type + '</span></td>' +
                '<td><span class="conf-guest-count">Guests:' + value.adults + '</span></td>' +
                '<td><span class="conf-children-count">Children:' + value.children + '</span></td>' +
                '<td><span class="conf-room-availability-status">' + value.status + '</span>';
	            /*
                 * if ( value.status == "AVAILABLE" ) { tempRoomStatus += '<td><span
                 * class="conf-room-availability-status"><img class="rooms-available-icon"
                 * src="../../../assets/images/tick.svg" alt = "tick icon"/>' + value.status + '</span>'; } else {
                 * tempRoomStatus += '<td><span class="conf-room-availability-status"><img
                 * class="rooms-unavailable-icon" src="../../../assets/images/wrong.jpg" alt = "wrong icon"/>' +
                 * value.status + '</span>'; }
                 */
	            tempRoomStatus += '</td></tr>';
                elRoomAvailability.find( 'table.selected-rooms-details' ).append( tempRoomStatus );
            } );

        }

        function warningPopupCloseScroll(){
            if(!preventPageScrollInitial){
             $(".cm-page-container").removeClass('prevent-page-scroll');
             $('.the-page-carousel').css('-webkit-overflow-scrolling', 'touch');
             $(window).scrollTop(presentScroll);                
            }

        }

        proceedBtn.on( 'click', function() {
            params.callBack();
            warningPopup.remove();   
            warningPopupCloseScroll();
        } );

        cancelBtn.on( 'click', function() {
            if ( params.callBackSecondary ) {
                params.callBackSecondary();
            }
            warningPopup.remove();
            warningPopupCloseScroll();
        } );
        
        var presentScroll = $(window).scrollTop();
        $(".cm-page-container").addClass('prevent-page-scroll');
        $('.the-page-carousel').css('-webkit-overflow-scrolling', 'unset');

        $(document).keydown(function(e) {
            if (($( '.cm-warning-box-con' ).length) && (e.which === 27)) {
                warningClose.trigger("click");
            }
        });
        warningClose.on( 'click', function() {
            if ( params.callBackSecondary ) {
                params.callBackSecondary();
            }
            warningPopupCloseScroll();
             warningPopup.remove();
        } );
        // });
    }

} ).call( this );

// Success pop up
( function() {
    this.successBox = function( params ) {
        var defaultParams = {
            title: 'Success!',
            description: '',
            callBack: '',
            callBackSecondary: '',
            needsCta: false,
        };
        params = $.extend( {}, defaultParams, params );

        var _tempSuccessBox = '<div class="cm-warning-box">' +
            '<div class="cm-warning-box-main">' +
            '<div class="cm-warning-box-inner-wrap">' +
            '<div class="warning-box-close"><span class="warning-box-close-icon icon-close" tabindex = "0" data-dialog-close = "WarningBox"></span></div>' +
            '<div class="cm-warning-box-logo-con"><i class="toaster-logo icon-STAR-TAJ"></i>' +
            '</div>' +
            '<div class="cm-success-box-heading-text"></div>' +
            '<div class="cm-success-box-description-text"></div>' +
            '<div class="cm-room-availability-description">' +
            '<div class="cm-room-availability-desc-wrap">' +
            '<table class="selected-rooms-details"></table>' +
            '</div>' +
            '</div>' +
            '<div class="cm-warning-box-btns">' +
            '<div class="cm-warning-box-proceed-btn">' +
            '<button class="cm-btn-secondary warning-proceed-btn"><span>Proceed</span><span class="dummy-to-middle-align"></span></button>' +
            '</div>' +
            '<div class="cm-warning-box-cancel-btn">' +
            '<button class="cm-btn-primary warning-cancel-btn"><span>Cancel</span><span class="dummy-to-middle-align"></span></button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';


        $( '.cm-page-container' ).append( '<div class="cm-warning-box-con successBoxRemoveRef" role = "dialog" data-dialog-modal = "WarningBox" aria-expanded = "false">' + _tempSuccessBox + '</div>' );
        var warningPopup = $( '.cm-warning-box-con' );
        var proceedBtn, cancelBtn, warningHeading, warningDescription, warningClose,
            btnsCon;

        proceedBtn = warningPopup.find( '.warning-proceed-btn' );
        cancelBtn = warningPopup.find( '.warning-cancel-btn' );
        warningHeading = warningPopup.find( '.cm-success-box-heading-text' );
        warningDescription = warningPopup.find( '.cm-success-box-description-text' );
        elRoomAvailability = warningPopup.find( '.cm-room-availability-description' );
        warningClose = warningPopup.find( '.warning-box-close' );
        btnsCon = warningPopup.find( '.cm-warning-box-btns' );

        warningHeading.html( params.title );
        warningDescription.html( params.description );


        if ( params.title ) {
            warningHeading.css( 'display', 'block' );
        }
        if ( params.description ) {
            warningDescription.css( 'display', 'block' );
        }
        if ( params.needsCta ) {
            btnsCon.css( 'display', 'block' );
        }

        proceedBtn.on( 'click', function() {
            params.callBack();
            warningPopup.remove();
        } );

        cancelBtn.on( 'click', function() {
            if ( params.callBackSecondary ) {
                params.callBackSecondary();
            }
            warningPopup.remove();
        } );
        warningClose.on( 'click', function() {
        	if ( params.callBackSecondary ) {
                params.callBackSecondary();
            }
            warningPopup.remove();
        } );
    }

} ).call( this );

( function applyCommaInNumbers() {
    $.fn.digits = function() {
        this.each( function() {
            $( this ).text( $( this ).text().replace( /(\d)(?=(\d\d\d)+(?!\d))/g, "$1," ) );
        } );
    }
} ).call( this );


// To find device type
( function DeviceDetector() {
    var windowInnerWidth;
    var maxSmallWidth = 767;
    var maxMediumWidth = 1023;
    var deviceType = '';
    var userAgent = window.navigator.userAgent;
    var iosDeviceType;

    this.deviceDetector = {};
    ( function() {
        this.checkDevice = function() {
            windowInnerWidth = window.innerWidth;
            if ( windowInnerWidth > maxMediumWidth ) {
                deviceType = 'large';
            } else if ( windowInnerWidth >= maxSmallWidth && windowInnerWidth <= maxMediumWidth ) {
                deviceType = 'medium';
            } else if ( windowInnerWidth < maxSmallWidth ) {
                deviceType = 'small';
            } else {
                console.log( 'Unrecognised device type' );
            }
            return deviceType;
        };
        this.isIphone = function() {
            if ( userAgent.indexOf( 'iPhone' ) >= 0 ) {
                return true;
            } else {
                return false;
            }

        };
        this.isIpad = function() {
            if ( userAgent.indexOf( 'iPad' ) >= 0 ) {
                return true;
            } else {
                return false;
            }
        };
        this.isIE = function() {
            /**
             * detect IE returns version of IE or false, if browser is not Internet Explorer
             */
            var msie = userAgent.indexOf( 'MSIE ' );
            if ( msie > 0 ) {
                // IE 10 or older => return version number
                return 'IE' + parseInt( userAgent.substring( msie + 5, userAgent.indexOf( '.', msie ) ), 10 );
            }

            var trident = userAgent.indexOf( 'Trident/' );
            if ( trident > 0 ) {
                // IE 11 => return version number
                var rv = userAgent.indexOf( 'rv:' );
                return 'IE' + parseInt( userAgent.substring( rv + 3, userAgent.indexOf( '.', rv ) ), 10 );
            }

            var edge = userAgent.indexOf( 'Edge/' );
            if ( edge > 0 ) {
                // Edge (IE 12+) => return version number
                return 'EDGE' + parseInt( userAgent.substring( edge + 5, userAgent.indexOf( '.', edge ) ), 10 );
            }

            // other browser
            return false;
        };
        this.getDeviceheight = function() {
            return window.innerHeight;
        };
        this.getDeviceWidth = function() {
            return window.innerWidth;
        };
    } ).call( this.deviceDetector );
} ).call( this );


// for selectbox in reserve table component

function selectReserveTable() {
    $( '#mr-reserveTable-number-of-people' ).selectBoxIt( {
        populate: [ '2', '3', '4', '5' ]
    } );

    $( '#mr-reserveTable-meal-type' ).selectBoxIt( {
        populate: [ 'Lunch', 'Dinner' ]
    } );
    $( '#jiva-spa-date.jiva-spa-date.dining-reserveTable-Date' ).datepicker( {
        startDate: new Date()
    } );
};

( function multipleDropDown() {
    $.fn.multiselectDropdown = function( options ) {

        var selectedOptionList = {};
        this.multiselect( {
            buttonClass: 'btn btn-link',
            nonSelectedText: 'select',
            allSelectedText: 'All Selected',
            numberDisplayed: 1,
            onChange: function() {
                var selectedOptn = this.$select.val();

                options.selectedOptionList = selectedOptn;

               
            }
        } );

        this.multiselect( 'dataprovider', options.option );
    }
    $.fn.multiselectDDreset = function() {
        this.multiselect( 'clearSelection' );
    }

} ).call( this );

// Detecting Credit Card Type
function getCreditCardType( creditCardNumber ) {
    var cards = {
        AMERICAN_EXPRESS: {
            length: [ 15 ],
            prefix: [ '34', '37' ]
        },
        DINERS_CLUB: {
            length: [ 14 ],
            prefix: [ '300', '301', '302', '303', '304', '305', '309', '36', '38', '39' ]
        },
        MASTERCARD: {
            length: [ 16 ],
            prefix: [ '2221', '2222', '2223', '2224', '2225', '2226', '2227', '2228', '2229',
                '223', '224', '225', '226', '227', '228', '229',
                '23', '24', '25', '26',
                '270', '271', '2720',
                '51', '52', '53', '54', '55'
            ]
        },
        VISA: {
            length: [ 13,16,19 ],
            prefix: [ '4' ]
        },
        DISCOVER: {
            length: [ 16 ],
            prefix: [ '6011', '622126', '622127', '622128', '622129', '62213',
                '62214', '62215', '62216', '62217', '62218', '62219',
                '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                '62290', '62291', '622920', '622921', '622922', '622923',
                '622924', '622925', '644', '645', '646', '647', '648',
                '649', '65'
            ]
        },
        JCB: {
            length: [ 16 ],
            prefix: [ '3528', '3529', '353', '354', '355', '356', '357', '358' ]
        },
        LASER: {
            length: [ 16, 17, 18, 19 ],
            prefix: [ '6304', '6706', '6771', '6709' ]
        },
        MAESTRO: {
            length: [ 12, 13, 14, 15, 16, 17, 18, 19 ],
            prefix: [ '5018', '5020', '5038', '6304', '6759', '6761', '6762', '6763', '6764', '6765', '6766' ]
        },        
        SOLO: {
            length: [ 16, 18, 19 ],
            prefix: [ '6334', '6767' ]
        },
        UNIONPAY: {
            length: [ 16, 17, 18, 19 ],
            prefix: [ '62' ]
        }
    };

    // Luhn algorithm to validate credit card
    var checksum = 0;    
    var j = 1;

    var calc;
    for (i = creditCardNumber.length - 1; i >= 0; i--) {
        calc = Number(creditCardNumber.charAt(i)) * j;
        if (calc > 9) {
            checksum = checksum + 1;
            calc = calc - 10;
        }
        checksum = checksum + calc;
        if (j ==1) {j = 2} else {j = 1};
    } 

    if (checksum % 10 != 0) { return false; }

    var type, i;
    for ( type in cards ) {
        for ( i in cards[ type ].prefix ) {
            if ( creditCardNumber.substr( 0, cards[ type ].prefix[ i ].length ) === cards[ type ].prefix[ i ] // Check
                                                                                                                // the
                                                                                                                // prefix
                &&
                $.inArray( creditCardNumber.length, cards[ type ].length ) !== -1 ) // and length
            {
                return {
                    valid: true,
                    type: type
                };
            }
        }
    }

    return false;
}

// code for get cardTypeCode using cardTypeName in paymentDetails JS
function creditCardTypeCode(cardType){
    var cardT = cardType;
    switch(cardT){
        case 'AMERICAN_EXPRESS':
            cardT = "AX"
            break;        
        case 'DINERS_CLUB':
            cardT = "DN";
            break;              
        case 'MASTERCARD':
            cardT = "MC";
            break;
        case 'VISA':
            cardT = "VI";
            break;
        default:
            cardT = "";
    }
    return cardT;
}

// code for get cardTypeName using cardTypeCode in Conformation Page JS
function creditCodeCardType(cardTypeCode){
    var cardT = cardTypeCode;
    switch(cardT){
        case "AX":
            cardT = 'AMERICAN_EXPRESS'
            break;        
        case "DN":
            cardT = 'DINERS_CLUB';
            break;              
        case "MC":
            cardT = 'MASTERCARD';
            break;
        case "VI":
            cardT = 'VISA';
            break;
        default:
            cardT = "";
    }
    return cardT;
}

( function() {
    this.invalidWarningMessage = function( targetInvalidInput ) {
        var targetInvalidInputValue = targetInvalidInput[ 0 ].value
        var invalidConditionText = "";
        if ( targetInvalidInput.length > 0 ) {
            invalidConditionText = " a valid";
        }
        var targetInvalidInputWrapper = targetInvalidInput.closest( '.sub-form-input-wrp' );
        var targetInvalidInputLabel = targetInvalidInputWrapper.find( '.sub-form-input-label' ).text();
        // remove * from the label
        var targetInvalidInputLabelLastChar = targetInvalidInputLabel[targetInvalidInputLabel.length - 1];
        if(targetInvalidInputLabelLastChar == "*"){
            targetInvalidInputLabel = targetInvalidInputLabel.slice( 0, -1 );
        }
        else{
            targetInvalidInputLabel = targetInvalidInputLabel;
        }
        var targetInvalidInputWarningElement = targetInvalidInputWrapper.find( '.sub-form-input-warning' );
        var invalidWarningtext = "Please enter" + invalidConditionText + " " + targetInvalidInputLabel; ;
        if(targetInvalidInput.is('select')){
            invalidWarningtext = invalidWarningtext.replace("enter","select");
        }
        
// if(targetInvalidInput.hasClass('maxlength-error') && targetInvalidInput.attr('max') != '') {
// invalidWarningtext = "Max no of guests cannot be greater than " + targetInvalidInput.attr('max');
// }
        $(targetInvalidInputWarningElement).show().text(invalidWarningtext);
        
    }
} ).call( this );


// email validation
function IsValidEmail( email ) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test( email );
}

// multiselect Filter
function globalMultipleFilter( elem, options ) {
    var selectedData = {};
    for ( var i = 0; i < options.length; i++ ) {
        if ( options[ i ].selectedOptionList && options[ i ].selectedOptionList.length > 0 )
            selectedData[ options[ i ].selectorValue ] = options[ i ].selectedOptionList;
    }

    elem.each( function() {
        var data = $( this ).data( 'key' );
        $( this ).toggleClass( 'cm-hide', false );
        for ( var key in selectedData ) {
            if ( selectedData[ key ].indexOf( data[ key ].toLowerCase() ) == -1 ) {
                $( this ).toggleClass( 'cm-hide', true );
                break;
            }
        }
    } )
    selectedData = {};
}

function createFilterOptions( filterOptionsWrap, options ) {
    for ( var i = 0; i < options.length; i++ ) {
        var temp = '<div class="filter-catagory" id="'+options[ i ].selector+'">' +
            '<div class="filter-header">' + options[ i ].selector + '</div>' +
            '<div class="filter-dropDD"><select aria-label="filter by" name="mutilpleFilterDropdown" class="cm-multi-dd" multiple="multiple"></select></div>' +
            '</div>';

        filterOptionsWrap.append( temp );
    }

    filterOptionsWrap.find( '.cm-multi-dd' ).each( function( i ) {
        $( this ).multiselectDropdown( options[ i ] )
    } );

}

function filterReset( selector, cardWrapper, optionList ) {
    $( '.filter-wrap-catagory' ).find( '.cm-multi-dd' ).each( function( i ) {
        $( this ).multiselectDDreset();
    } );
    $( '.mr-investors-report-documents-wrapper.content-active .mr-audit-statement-template' ).each( function() {
        $( this ).toggleClass( 'cm-hide', false );
    } )

    optionList.each( function( i ) {
        optionList[ i ].selectedOptionList = null;

    } )
}

(function($) {
    /**
     * Check if element is in view port Set nextScreenFold to true to check if exists in next screen fold
     */
    $.fn.isInViewport = function(nextScreenFold) {
        var elementTop = $(this).offset().top;
        var viewportTop = $(window).scrollTop();
        // double the window height to increase check radius
        var windowHeight = nextScreenFold ? $(window).height()*2 : $(window).height();
        var viewportBottom = viewportTop + $(window).height();
        return elementTop < viewportBottom;
    }
})(jQuery);

function setThemeValueInStorage(){
    dataCache.session.setData("themeValue",false);
    $($('.cm-page-container')[0].classList).each(function(){
        if(this.indexOf('theme')!=-1){
            dataCache.session.setData("themeValue",this);
            return;
        }
    })
}

function setThemeValueToPageContainer(){
    var themeValue = dataCache.session.getData("themeValue");
    if(themeValue){
    	$('.cm-page-container').removeClass("ihcl-theme");
        $('.cm-page-container').addClass(themeValue);
    }
}

function getQueryParameter(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

jQuery.fn.extend({
    tajPagenation: function(perPage) {
        var items = $(this).find(".contents-wrp").children();
        var numItems = items.length;
        items.slice(perPage).hide();
        var noOfPages = 5;
         deviceDetector.checkDevice() == "small"?noOfPages=2:noOfPages=5;
        $(this).find(".pagination-page").pagination({
            items: numItems,
            itemsOnPage: perPage,
            cssStyle: 'light-theme',
            displayedPages: noOfPages,
            onPageClick: function(pageNumber) {
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;
                items.hide().slice(showFrom, showTo).show();
            }
        });
    }
});