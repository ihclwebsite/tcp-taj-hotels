//[IHCL-CB START]
var ihclcbDashboardPage = "/en-in/corporate-booking/dashboard/";
$(document).ready(function() {
    if ($(".cm-page-container").hasClass("ihcl-theme")) {
        var ihclcbEntryPage = "/en-in/corporate-booking/";
        var userDetails = dataCache.local.getData('userDetails');
        var currentPage = window.location.pathname;
        if (!userDetails && currentPage != ihclcbEntryPage) {
            window.location.href = ihclcbEntryPage;
        } else if (userDetails && currentPage === ihclcbEntryPage) {
            window.location.href = ihclcbDashboardPage;
        }

        if (userDetails && currentPage == ihclcbDashboardPage) {
            if (userDetails.selectedEntity) {
                userDetails.selectedEntity = null;
            }
            if (userDetails.selectedEntityAgent) {
                userDetails.selectedEntityAgent = null;
            }
            dataCache.local.setData('userDetails', userDetails);
        }

        var ihclCbBookingObject = dataCache.session.getData('ihclCbBookingObject');
        if (!ihclCbBookingObject) {
            var ihclCbBookingObject = {};
            ihclCbBookingObject.isIHCLCBFlow = true;
            dataCache.session.setData("ihclCbBookingObject", ihclCbBookingObject);
        }
        $('.best-deal-banner img').attr('src', '/content/dam/tajhotels/ihclcb-icons/ic_best_deal.svg');

        // remove .html from the url
        $('a[href]').each(function() {
            var _this = $(this);
            var currentUrl = _this.attr('href');
            if ((!currentUrl.includes('/en-in/corporate-booking')) && currentUrl.includes('.html')) {
                currentUrl = currentUrl.replace(new RegExp('.html', 'g'), '');
                _this.attr('href', currentUrl);
            }
        });

        // removing ama content from dom
        $(".list-view-wrapper").each(function() {
            var hrefLink = $(this).find(".mr-list-hotel-price-tic").find("a").attr("href");
            if (hrefLink.includes("ama-trails")) {
                $(this).remove();
            }
        })

    } else if (window.location.hostname != "www.tajinnercircle.com" && !$(".error-page-component")[0]) {
        dataCache.session.removeData('ihclCbBookingObject');
        // dataCache.local.removeData('userDetails');
    }
});

function isIHCLCBSite() {
    var ihclCbBookingObject = dataCache.session.getData('ihclCbBookingObject');
    if (ihclCbBookingObject && ihclCbBookingObject.isIHCLCBFlow) {
        return true;
    }
    return false;
}

// [IHCL-CB END]
