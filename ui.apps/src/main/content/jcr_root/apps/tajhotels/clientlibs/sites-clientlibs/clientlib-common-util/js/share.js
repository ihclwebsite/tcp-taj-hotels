$(document).ready(function() {
    $('.share-btn-wrapper .share-btn-js').click(function(e) {
        e.stopPropagation();
        $(this).siblings('.share-options-list').toggle();
    });
    $('.share-btn-wrapper').click(function(e) {
        e.stopPropagation();
    });
    $(document).click(function() {
        $('.share-btn-wrapper .share-btn-js').siblings('.share-options-list').hide();
    });
});
