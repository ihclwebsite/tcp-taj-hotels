$(document).ready(function () {
  $(".ama-theme .content-wrapper").removeClass('container');
  $(".ama-theme .specific-hotels-breadcrumb").addClass('container');
  $(".ama-theme .hotel-banner").addClass('container');
  $(".ama-theme .cart-layout").addClass('container');
  $(".ama-theme .checkout-layout").addClass('container');
  if ($(".ama-theme .ama-content-wrapper .cm-content-blocks").hasClass('offers-room-container')) {
    $(".ama-theme .ama-content-wrapper").addClass('container');
  };
  $(".carousel").swipe({
    swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
      if (direction == 'left') $(this).carousel('next');
      if (direction == 'right') $(this).carousel('prev');
    },
    allowPageScroll: "vertical"
  });
});
