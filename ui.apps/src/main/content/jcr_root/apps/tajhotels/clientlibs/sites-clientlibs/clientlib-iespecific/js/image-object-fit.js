document.addEventListener( 'DOMContentLoaded', function() {
    function isIE() {
        ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf( "MSIE " ) > -1 || ua.indexOf( "Trident/" ) > -1;        
        return is_ie;
    }

    function styleObjectFit( imageElement ) {        
        var imageElementSource = imageElement.src;        
        if ( imageElementSource ) {
            imageElement.style.backgroundImage = 'url(' + imageElementSource + ')';
            imageElement.classList.add( 'object-fit-cover' );
            imageElement.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
        }
    }

    if ( isIE() ) {
        const imagesListObjectFit = document.querySelectorAll( '.hotel-banner-image-con img, .image-object-fit' );
        for ( var i = 0; i < imagesListObjectFit.length; i++ ) {
            styleObjectFit( imagesListObjectFit[ i ] )
        }
    }
} );