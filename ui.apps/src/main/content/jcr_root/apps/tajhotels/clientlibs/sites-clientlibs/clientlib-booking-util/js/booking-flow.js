var amatheme = "";
var onlyBungalow =  "";

function bookingFlow() {
    $(document).ready(function() {
        try {
            amatheme = $('.cm-page-container').hasClass('ama-theme');
            onlyBungalow =  getUrlParameter('onlyBungalows');
            defineBookingFlowVariables();
            setupDocReadyForBookingFlow();
        } catch(error) {
            console.log("Error on booking flow.js ", error);
        };   
    });
}

// Variables
var bookingOptions;
var selectionCount;
var roomCount;
var totalCartPrice;
var activeRateTabIndex;
var flaotingConWidth;

// Elements
var elmeSelInstruct;
var floatingCartPanel;
var checkoutContainer;
var elChoosenRoomsInCart;
var elSmRoomSelTrigger;
var elRatePlanTabs;
var elRoomsEachRateType;

var elRoomDetailsCon;

function defineBookingFlowVariables() {
    bookingOptions = dataCache.session.getData("bookingOptions") || {
        'selection' : []
    };
    selectionCount = bookingOptions.selectionCount ? parseInt(bookingOptions.selectionCount) : 0;
    roomCount = bookingOptions.roomCount ? parseInt(bookingOptions.roomCount) : 0;
    totalCartPrice = 0;
    activeRateTabIndex = bookingOptions.activeRateTabIndex ? parseFloat(bookingOptions.activeRateTabIndex) : 0;
    flaotingConWidth = 0;

    // Elements
    elmeSelInstruct = $('.room-selection-data');
    floatingCartPanel = $('.book-ind-container');
    checkoutContainer = floatingCartPanel.find('.checkout-container');
    elChoosenRoomsInCart = floatingCartPanel.find('.room-selected-options-container');
    elSmRoomSelTrigger = floatingCartPanel.find('.bic-info-selected-rooms');
    elRatePlanTabs = $('.rate-tab');
    elRoomsEachRateType = $('.cm-room-options');

    elRoomDetailsCon = elChoosenRoomsInCart.find('.room-details-container');
}


    function redirectToCart() {
        var redirectCartURL = $('.checkout-anchor').attr("href");
        window.location.href = redirectCartURL;

    }
function setupDocReadyForBookingFlow() {
    // set Theme value in browser storage
    setThemeValueInStorage();

    setupListenerForAddRoom();

    // Check whether user has been redirected on this page via Add Room
    hasInvokedViaAddRoom(bookingOptions);

    getRoomsForSelectedRatePlan();

    // Register the event hadlers to show/hide floating cart expanded state
    showCartSelOnSmallDevice();

    updateCheckAvailability();

    // checkout button click handler
    $('.checkout-anchor').click(function(e) {
        var dataFromStorage = dataCache.session.getData("bookingOptions");
        var roomOptionsLength = dataFromStorage.roomOptions.length;
        var packageSelectionLength = dataFromStorage.selection.length;
        if (roomOptionsLength > packageSelectionLength) {
            e.preventDefault();
            var _self = this;
            var popupParams = {
                    title : 'Not all rooms are selected.',
                    description : 'You have not selected packages for all the rooms. Do you still want to proceed?',
                    callBack : redirectToCart,
                    needsCta : true,
                    isWarning : true
            }
            warningBox(popupParams);
        }
    });

    // Rate tabs
    elRatePlanTabs.click(function() {
        $(this).siblings().removeClass('tab-selected');
        $(this).addClass('tab-selected');
        activeRateTabIndex = $(this).index();
        getRoomsForSelectedRatePlan();

    })

    // Display selection instruction panel

    if (bookingOptions.selection && bookingOptions.selection.length > 0) {
        // Update instruction to select room
        // updateSelectionInstruction( elmeSelInstruct,
		// bookingOptions.selection, true );

    } else {
        // Update instruction to select room
        ++roomCount;
        // updateSelectionInstruction( elmeSelInstruct,
		// bookingOptions.roomOptions );
    }

    updateSelectionInstruction();

    // Display floating cart panel if more than one selection is present -
    // condition
    // is handled in the function

    displayFloatingCart();
    // Render the room content in the floating cart
    /*
     * if ( bookingOptions.selection && bookingOptions.selection.length > 0 ) { renderFloatingCartOnLoad(); }
     */

    // Initial layout for package selections from floating cart
    pkgSelectionLayoutOnCart();

    promptToSelectDate();

};

function fetchRoomNameViaButton(clickedButton) {
    var roomName = '';
    var roomContainer = $(clickedButton).parents("[data-room-type-name]");
    var roomName = $(roomContainer).data("room-type-name");
    return roomName;
};

function fetchSelectionPriceViaButton(clickedButton) {
    var rateContainer = $(clickedButton).parents(".rate-info-wrp");
    var price = $(rateContainer).find(".present-rate .rc-total-amount").text();
    return price;
};

function askToInitiateSelection() {
    try {
        // Ask user to add another ROOM in BAS widget
        // if he is trying to select a package for a room that is not selected
        
			var popupParams = {
					title : 'Please add another room to select a package.',
					description : 'Do you want to add room?',
					callBack : popUpWidgetAndAddRoom,
					needsCta : true,
					isWarning : true
			}
		
        warningBox(popupParams);        
    } catch (error) {
        console.error(error);
    }
};
function closeWarningPopup() {
	$('.selection-delete').trigger('click');
}
function popUpWidgetAndAddRoom(){
    if(isAmaSite) {
        amaBookingObject = dataCache.session.getData("bookingOptions");
    }
    $('.book-stay-btn').trigger('click');
    setTimeout(function() {
        $('.bas-add-room').trigger('click');
    }, 100);    
}

function getRoomsForSelectedRatePlan() {
    elRoomsEachRateType.addClass('cm-active');
    elRoomsEachRateType.eq(activeRateTabIndex).siblings().removeClass('cm-active');

    // Update application storage object with active selection
    bookingOptions.activeRateTabIndex = activeRateTabIndex;
};

function updateSelectionInstruction(elem, selection, isDeletion) {
    try {
        var bookingOptions = dataCache.session.getData("bookingOptions");
        var elem = elmeSelInstruct;
        var adultsCount = 1, childCount = 0, adultSuffix = '', childSuffix = '';
        var tempRetIndexVal = roomIndexForCurrentPackage();
        var thisRoomIndex = tempRetIndexVal.indexCurrentRoom;
        var thisRoomNumber = parseInt(thisRoomIndex) + 1;

        if (bookingOptions && bookingOptions.roomOptions[thisRoomIndex]) {
            var parentOffset = $('.cm-page-container').offset();
            var targetOffset = elem.offset();
            $('html,body').animate({
                scrollTop : (parentOffset.top * -1) + targetOffset.top
            }, 'slow');

            elmeSelInstruct.css('display', 'block');

            var thisRoomValues = bookingOptions.roomOptions[thisRoomIndex];
            // if ( selection && selection.length > 0 && !isDeletion &&
			// roomCount <= selection.length ) {
            adultsCount = thisRoomValues.adults;
            childCount = thisRoomValues.children;
            adultSuffix = (adultsCount > 1) ? 's' : '';
            childSuffix = (childCount > 1) ? 'ren' : '';
            // }

            var displaText = "<span class='sel-ins-Select-room'>Select Room " + thisRoomNumber + "</span> <span>for "
            + adultsCount + " adult" + adultSuffix + " and " + childCount + " child" + childSuffix + "<span>";
            elmeSelInstruct.html(displaText);

        } else {
            elmeSelInstruct.css('display', 'none');
            return;
        }

    } catch (error) {
        console.error(error);
    }
};

function removeTargetPackage(bookingOptions, thisIndex) {
    var spliceIndex;
    bookingOptions.selection.forEach(function(value, index) {
        if (value.roomIndex == thisIndex) {
            spliceIndex = index;
        }
    });

    bookingOptions.selection.splice(spliceIndex, 1);

    delete bookingOptions.roomOptions[thisIndex].userSelection;

    // [TIC-FLOW] checking if even after rempving the room , it is still a TIC
	// Room redemption flow
    ticBookingFlow();

    // return spliceIndex;
}

// Register a click event on delete btn of each room card on floating cart
function deleteCardsOnFloatingCart(latestAddedRoom, value) {
    latestAddedRoom.find('.selection-delete').on('click', function() {

        var thisIndex = latestAddedRoom.closest(".fc-add-package-con").index();

        removeTargetPackage(bookingOptions, thisIndex);

        $(this).closest('.room-details-container').siblings('.fc-add-btn-con').show();
        $(this).closest('.room-details-container').remove();
        $('.rate-card-view-detials-container').removeClass('rate-card-view-detials-selected');
        $('.more-rates-button').removeClass('more-rates-button-selected');
        $('.more-rates-button').find('.icon-drop-down-arrow-white').removeClass('cm-rotate-icon-180');
        // Update instruction to select room
        --roomCount;
        updateSelectionInstruction(elmeSelInstruct, bookingOptions.selection, true);

        // Update the right side checkout box details on the floating cart.
        var selectedRate = parseFloat(value.selectedRate);
        // [TIC-FLOW]
        var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
            totalCartPrice = totalCartPrice - selectedRate - value.roomTaxRate;
        }else{
            totalCartPrice = totalCartPrice - selectedRate;
        }
        var totalCartAmount = getCommaFormattedNumber(totalCartPrice, "en-in");
        // totalCartPrice=totalCartAmount;

        updateCheckoutBox();

        displayFloatingCart();
        updateAppStorage();

        updateSelectionInstruction();

        // Update Room cards container on left side of the floating cart
        var lastRoomOfRooms = elChoosenRoomsInCart.children().last();
        flaotingConWidth = flaotingConWidth - parseFloat(lastRoomOfRooms.outerWidth());
        // elChoosenRoomsInCart.width( flaotingConWidth );
        upateRoomsCon();

        if (elChoosenRoomsInCart.children().length == 0) {
            flaotingConWidth = 0;
        }
        updateCheckAvailability();

        var tempRetIndexVal = roomIndexForCurrentPackage();

        var nextRoomNumber = parseInt(tempRetIndexVal.initialRoomIndex) + 1;
        if (ROOM_OCCUPANCY_RESPONSE[nextRoomNumber]) {
            processRoomOccupancyRates(nextRoomNumber);
        }
        // [TIC-FLOW]
        disablePackageInTicRoomRedemption();
    });
};

// TODO: Carousel behaviour on floating cart

// TODO: Prompt user to select package for all the rooms selected from the
// widget
// if he/she decides to proceed to next page without making all selections. Give
// him
// an option to "Proceed" or "Stay"

function updateCheckoutBox(selectedRoom) {
    // [TIC-FLOW]
    var userDetails = getUserData();

    var bookingOptions = dataCache.session.getData("bookingOptions");
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');

    var currencyData = dataCache.session.getData("selectedCurrency").trim();
    if (currencyData != undefined) {
        $(".cart-currency-symbol").text(currencyData);
    }
    
    var totalTicCartPrice = totalCartPrice;
    if (currencyData && (currencyData != 'INR' && currencyData != '₹')) {
        if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.currencyRateConversionString) {
            var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
            var conversionRate = parseFloat(currencyRateConversionString[currencyData + '_INR']);
            totalTicCartPrice = Math.ceil(totalTicCartPrice) * conversionRate;
        }
    }
    // [TIC-FLOW]
    if ((userDetails && userDetails.card && userDetails.card.tier && userDetails.card && userDetails.card.type.includes("TIC") && 
            ( (selectedRoom && selectedRoom.selectedFilterTitle && (selectedRoom.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES' || selectedRoom.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES' ))
                    || (userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow)))) {
        checkoutContainer.find('.cart-total-price').html('<div class="tic-points">'+roundPrice(Math.round(totalTicCartPrice)) +' TIC Points</div>');
        checkoutContainer.find('.tic-points').digits();
        checkoutContainer.find('.cart-currency-symbol').hide();
    } else if ((userDetails && userDetails.card && userDetails.card.tier && userDetails.card.type=='TAP' && 
            ((selectedRoom && selectedRoom.selectedFilterTitle && selectedRoom.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
                    || (userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow)))) {
        checkoutContainer.find('.cart-total-price').html(roundPrice(Math.round(totalTicCartPrice)) +' TAP ');
        checkoutContainer.find('.cart-total-price').digits();
        checkoutContainer.find('.cart-currency-symbol').hide();
    } else if ((userDetails && userDetails.card && userDetails.card.tier && userDetails.card.type=== 'TAPPMe' && 
            ((selectedRoom && selectedRoom.selectedFilterTitle && selectedRoom.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES')
                    || (userDetails && userDetails.card && userDetails.card.tier && ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow)))) {
        checkoutContainer.find('.cart-total-price').html(roundPrice(Math.round(totalTicCartPrice)) +' TAPPMe');
        checkoutContainer.find('.cart-total-price').digits();
        checkoutContainer.find('.cart-currency-symbol').hide();
    }else {
        checkoutContainer.find('.cart-total-price').html(roundPrice(totalCartPrice));
        checkoutContainer.find('.cart-total-price').digits();
        checkoutContainer.find('.cart-currency-symbol').show();
    }
    var roomsCountInCart = $('.room-selected-options-container .room-details-container').length;
    checkoutContainer.find('.checkout-num').html(roomsCountInCart);
    var roomSuffix = (roomsCountInCart > 1) ? 'S' : '';
    floatingCartPanel.find('.bic-rooms').html(roomsCountInCart + ' ROOM' + roomSuffix + ' SELECTED');
}

function upateRoomsCon() {
    if (elChoosenRoomsInCart.children().length > 4) {
        $('.message-container').addClass('add-shadow');
    } else {
        $('.message-container').removeClass('add-shadow');
    }
};

// Function to render each room on floating cart
function roomCardsOnFloatingCart(value, thisIndex) {

    // elChoosenRoomsInCart.append( value.floatingCart );

    $(".fc-add-package-wrap").eq(thisIndex).children().hide();
    $(".fc-add-package-wrap").eq(thisIndex).append(value.floatingCart);

    // var latestAddedRoom = elChoosenRoomsInCart.children().last();
    var latestAddedRoom = $(".fc-add-package-wrap").eq(thisIndex);

    latestAddedRoom.find('.fc-each-room-rate').digits();

    var thisOuterWidth = latestAddedRoom.outerWidth();

    // Set room cards container width
    flaotingConWidth = flaotingConWidth + parseFloat(latestAddedRoom.outerWidth()) + 10;
    // elChoosenRoomsInCart.width( flaotingConWidth );

    // Update Room cards container on left side of the floating cart
    upateRoomsCon();

    // Update the right side checkout box details on the floating cart.
    updateCheckoutBox(value);

    // Register deletion listeners on the freshly generated cart
    deleteCardsOnFloatingCart(latestAddedRoom, value);
};

// Function to display floating cart strip
function displayFloatingCart() {
    if (bookingOptions.selection && bookingOptions.selection.length >= 1) {
        floatingCartPanel.css('display', 'block');
        $('.cm-bas-con .cm-bas-content-con').css('bottom', '15%');
    } else {
        floatingCartPanel.css('display', 'none');
        $('.cm-bas-con .cm-bas-content-con').css('bottom', '4%');
    }
};

// Function to load floating cart on page load if the cart existed for that user
function renderFloatingCartOnLoad() {
    // [TIC-FLOW]
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    bookingOptions.selection.forEach(function(value) {
        if (value.floatingCart) {
            roomCardsOnFloatingCart(value, value.roomIndex);
            updateCheckAvailability();
            // [TIC-FLOW]
            if (ticRoomRedemptionObjectSession && ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow) {
                totalCartPrice = totalCartPrice + value.roomBaseRate + value.roomTaxRate;
            }else{
                totalCartPrice = totalCartPrice + value.roomBaseRate;
            }
            if ($("#tax-container").data("tax")) {
                totalCartPrice = totalCartPrice + value.roomTaxRate;
            }
        }
    });
    updateCheckoutBox();
};

function updateFloatingCart(targetParent, selectionView, thisIndex) {
    // Update floating cart price and selection values on each selection
    // var selectedRate = parseFloat( targetParent.find( '.present-rate
    // .rc-total-amount' ).html()
    // );
    var selectedRate = parseFloat((targetParent.find('.present-rate .rc-total-amount').html()).replace(/,/g, ""));

    // selectionView.selectedRate = selectedRate;

    var roomBaseRate = parseFloat((targetParent.find('.rate-base-price').html()).replace(/,/g, ""));
    var roomAvgTaxRate = parseFloat((targetParent.find('.room-avg-taxes-info').html()).replace(/,/g, ""));
    var symbol = targetParent.find('.present-rate').find('.rc-selected-currency').text();
    dataCache.session.setData("selectedCurrency", symbol);
    selectionView.roomBaseRate = roomBaseRate;
    selectionView.selectedRate = roomBaseRate;
    selectionView.roomAvgTaxRate  = roomAvgTaxRate;
    totalCartPrice = totalCartPrice + roomBaseRate;
    var roomTaxRate = parseFloat((targetParent.find('.rate-tax-info').html()).replace(/,/g, ""));
    selectionView.roomTaxRate = roomTaxRate;
    if ((targetParent.find('.rate-taxes-info').html() != null) && (targetParent.find('.rate-taxes-info').html() != '')
            && (targetParent.find('.rate-taxes-info').html() != 'undefined')) {
        var taxes = JSON.parse(targetParent.find('.rate-taxes-info').html());
        selectionView.taxes = taxes;
    }
    var nightlyRates = JSON.parse(targetParent.find('.nightly-rates-info').html());
    selectionView.nightlyRates = nightlyRates;
    if ($("#tax-container").data("tax")) {
        totalCartPrice = totalCartPrice + roomTaxRate;
        selectionView.selectedRate = selectionView.selectedRate + roomTaxRate;
    }
    /* selectionView.roomBedType = "king"; */

    var priceHtml = '';
    // [TIC-FLOW]
    var userDetails = getUserData();
    if (userDetails && userDetails.card && userDetails.card.tier && selectionView.selectedFilterTitle
            && ((selectionView.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES') 
                    || (selectionView.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES') 
                    || (selectionView.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES')
                    || (selectionView.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
        totalCartPrice = totalCartPrice + selectionView.roomTaxRate;

        var totalTicCartPrice = selectionView.selectedRate + selectionView.roomTaxRate;

        if (symbol != 'INR' && symbol != '₹') {
            var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
            if (ticRoomRedemptionObjectSession
                    && ticRoomRedemptionObjectSession.currencyRateConversionString) {
                var currencyRateConversionString = ticRoomRedemptionObjectSession.currencyRateConversionString;
                var conversionRate = parseFloat(currencyRateConversionString[symbol
                    + '_INR']);
                totalTicCartPrice = Math.round(totalTicCartPrice * conversionRate);
            }

        }

        var memberType=userDetails.card.type;
        if(memberType.includes("TIC")){
            priceHtml = '<span class="fc-tic-each-room-rate"><div class="book-ind-tic-points">' + roundPrice(Math.ceil(totalTicCartPrice)) +
            ' TIC Points';
        }else if (memberType === "TAP"){
            priceHtml = '<span class="fc-each-room-rate">' + roundPrice(totalTicCartPrice) + 'TAP';
        }else if (memberType === "TAPPMe"){
            priceHtml = '<span class="fc-each-room-rate">' + roundPrice(totalTicCartPrice) + 'TAPPMe';
        }
        if(memberType.includes("TIC")){
            priceHtml=priceHtml+' | ' + bookingOptions.nights + ' night</div></span>';
            
        }else{
            priceHtml=priceHtml+' | ' + bookingOptions.nights + ' night</span>';
        }
    }else {
        priceHtml = '<span class="fc-each-room-rate">' + roundPrice(selectionView.selectedRate)+' | ' + bookingOptions.nights + ' night</span>';
    }

    
    var _tempSelectedRoom = '<div class="room-details-container"><div class="fc-room-cards-con">'
        + '<div class="room-details-heading" title="' + selectionView.title + '">' + selectionView.title + '</div>'
        + '<div class="room-details-rate">' + priceHtml +'</div>'
        + '<div class="room-details-desc">1 Room | ' + (selectionView.adults) + ' Guests, '
        + (selectionView.children) + ' Child</div>' + '<button class="btn-only-focus icon-delete-icon selection-delete" aria-label = "delete icon"></button>'
        + '</div></div>';

    selectionView.floatingCart = _tempSelectedRoom;
    roomCardsOnFloatingCart(selectionView, thisIndex);
}

function showCartSelOnSmallDevice() {
    elSmRoomSelTrigger.on('click', function() {
        /* var arrowCon = $( this ).find( '.bic-arrow-con' ); */
        var selRoomOptionsCards = $(this).parent().siblings('.room-details-wrap');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            /* selRoomOptionsCards.css( 'z-index', -1 ); */
            selRoomOptionsCards.removeClass('active');
        } else {
            $(this).addClass('active');
            selRoomOptionsCards.addClass('active');
            /*
             * setTimeout( function() { selRoomOptionsCards.css( 'z-index', 1 ); }, 301 );
             */
        }
    });
};
// This function was previously name as navigateToCart
function updateAppStorage() {
	if($('.cm-page-container').hasClass('ama-theme')){
    bookingOptions.targetEntity = $('.text-container .text-title').text();
	}else{
		bookingOptions.targetEntity = $(".cm-header-label").html();
	}
    bookingOptions.totalCartPrice = totalCartPrice;
    // bookingOptions.selectionCount = selectionCount;
    bookingOptions.roomCount = roomCount;
    bookingOptions.activeRateTabIndex = activeRateTabIndex;
    dataCache.session.setData("bookingOptions", bookingOptions);
};

function hasInvokedViaAddRoom(bookingOptions) {

    // If ser has redirected on this page by clicking on Add Room+ on cart page,
    // The BAS widget
    // should open up an next room should be pre selected in the widget.

    if (bookingOptions.isAddRoomClicked) {
        // $( $( '.book-stay-btn' )[ 0 ] ).trigger( 'click' );
        // Call functions as on book-a-stay btn click

        // Pre populate the date picker with any previous selection, if there
        autoPopulateBookAStayWidget();

        initiateCalender();

        if ((bookingOptions.selection.length == bookingOptions.roomOptions.length)
                && (bookingOptions.roomOptions.length < 5)) {
            $('.cm-bas-con').addClass('active');
            $(".bas-add-room").trigger('click');
            bookingOptions.roomCount = bookingOptions.roomCount + 1;
        } else {
            var parentOffset = $('.cm-page-container').offset();
            var targetOffset = $('.room-selection-data').offset();
            $('html,body').animate({
                scrollTop : (parentOffset.top * -1) + targetOffset.top - 100
            }, 'slow');
        }
        // setting false to isAddRoomClicked to prevent popup on page refresh
        var addRoomBookingOptions = dataCache.session.getData("bookingOptions");
        addRoomBookingOptions.isAddRoomClicked = false;
        dataCache.session.setData("bookingOptions", addRoomBookingOptions);
    }
};

function updateCheckAvailability() {

    try {
        var bookingOptions = dataCache.session.getData("bookingOptions");
        var caTargetSelectionText = $(".banner-titles .cm-header-label").text();
        $('.ca-target-selection').html(caTargetSelectionText);

        if (bookingOptions.fromDate && bookingOptions.toDate) {
            var dateDisplayText = moment(bookingOptions.fromDate, "MMM Do YY").format('DD MMM YYYY') + ' - '
            + moment(bookingOptions.toDate, "MMM Do YY").format('DD MMM YYYY');
            $('.ca-booking-date-range').html(dateDisplayText);

            $('.mr-checkIn-checkOut-Date-hover').css('display', 'none');
        } else {
            $('.mr-checkIn-checkOut-Date-hover').css('display', 'block');
        }

        if (bookingOptions && bookingOptions.roomOptions.length > 0) {
            var selectionText = '';
            var totalGuests = 0;
            bookingOptions.roomOptions.forEach(function(value, index) {
                totalGuests = totalGuests + parseInt(value.adults);
                var guestWord = '';
                if (totalGuests > 1) {
                    guestWord = 'Guests';
                } else {
                    guestWord = 'Guest';
                }
                if (index < 2) {
                    var roomNumber = index + 1;
                    selectionText = selectionText + '<span class="roomCount">Room ' + roomNumber + '</span>' + '- '
                    + value.adults + ' ' + guestWord + ', ' + value.children + ' Child | ';
                }
                // totalGuests = totalGuests + parseInt(value.adults);
            });
            var roomLowerCaseSuffix = (bookingOptions.roomOptions.length > 1) ? 's' : '';
            var guestLowerCaseSuffix = (totalGuests > 1) ? 's' : '';
            var summarizedText = bookingOptions.roomOptions.length + ' Room' + roomLowerCaseSuffix + ' - '
            + totalGuests + ' Guest' + guestLowerCaseSuffix;
            $('.mr-mobile-roomGuestsCount').html(summarizedText);

            var lastOccurenceOfPipe = selectionText.lastIndexOf("|");
            if (lastOccurenceOfPipe != -1) {
                selectionText = selectionText.substr(0, lastOccurenceOfPipe);
            }

            if (bookingOptions.roomOptions.length > 2) {
                selectionText = selectionText + '...';
            }
            $('.ca-selections').html(selectionText);
            $('.mr-availability-check-hover').css('display', 'none');
        } else {
            $('.mr-availability-check-hover').css('display', 'block');
        }
    } catch (error) {
        console.error(error);
    }
}

var clickedButtonData;
function setupListenerForAddRoom() {
    var targetSelectBtn = $('.more-rates-select-btn');
    var room;
   targetSelectBtn.each(function() {
        $(this).on('click', function() {
            clickedButtonData = $(this);
            room = $(clickedButtonData).closest('.rate-card-wrap');
            if(rateTabCode && rateTabCode === 'TIC') {
                if(getUserData()){
                    if(amatheme && !onlyBungalow && room && room.data('room-type') == 'bungalow'){
                        handleClickOnSelectRoom();
                    }else{
                        handleClickOnSelectRoomProceding();
                    }
                } else {
                	promptTicLogin();
                }
            } 
            else {
                if(amatheme && !onlyBungalow && room && room.data('room-type') == 'bungalow'){
                    handleClickOnSelectRoom();
                }else{
                    handleClickOnSelectRoomProceding();
                }
            }
        });
    }); 
        targetSelectBtn.each(function() {
        $(this).on('click', function() {

            clickedButtonData = $(this);
            room = $(clickedButtonData).closest('.rate-card-wrap');
            if(amatheme && !onlyBungalow && room && room.data('room-type') == 'bungalow'){
                handleClickOnSelectRoom();
            }else if($($.find('.rate-tab.tab-selected')).attr('data-rate-filter-code') == "TIC"){
                memberRateSelected();
            }else{
                handleClickOnSelectRoomProceding();
            }
        });
    });

    // added dataLayerData to sessionLevelCache for analytics requirement
    var bOptions = dataCache.session.getData("bookingOptions");

    bOptions.dataLayerData = dataLayerData;
    dataCache.session.setData("bookingOptions", bOptions);

}

function handleClickOnSelectRoom() {
    var popupParams = {
        title: 'Bungalow selected!',
        description: "You have selected a Bungalow. Would you like to proceed with the booking?",
        callBack: setRoomOptionsTotal.bind(),
// callBackSecondary: testingCancel.bind(),
        needsCta: true,
    }
    warningBox( popupParams );
}

function changeRoomGuestToBungalow(rOptions) {
    var aud = 0; var chld = 0;
    for(var d = 0; d < rOptions.length; d++) {
        aud = aud + parseInt(rOptions[d].adults);
        chld = chld + parseInt(rOptions[d].children);
    }

    return {
        adults: aud+"",
        children: chld+"",
        initialRoomIndex: 0
    };  
}

function setRoomOptionsTotal(){
    var boptions = dataCache.session.getData("bookingOptions");
// var rOptions = boptions.roomOptions;
// var aud = 0; var chld = 0;
// for(var d = 0; d < rOptions.length; d++) {
// aud = aud + parseInt(rOptions[d].adults);
// chld = chld + parseInt(rOptions[d].children);
// }
//
// var roomOpt = {
// adults: aud+"",
// children: chld+"",
// initialRoomIndex: 0
// };
    
    var roomOpt = changeRoomGuestToBungalow(boptions.roomOptions);
    boptions.previousRooms = [];
    boptions.previousRooms.push(roomOpt);
    boptions.roomOptions = [];
    boptions.roomOptions.push(roomOpt);
    boptions.rooms = boptions.roomOptions.length;
    boptions.selection = [];
    boptions.BungalowType = "onlyBungalow";
    dataCache.session.setData("bookingOptions",boptions);
    handleClickOnSelectRoomProceding();
}

// function testingCancel(){
//    
// }

function handleClickOnSelectRoomProceding() {
    var clickedButton = clickedButtonData
    /*
     * Checks every time for booking options from cache as it is possible to modify the booking options through check
     * availability widget while on the page
     */
    var tempRetIndexVal;
    var guestRoomPath = window.location.href;
    dataCache.session.setData("guestRoomPath", guestRoomPath);
    bookingOptions = dataCache.session.getData("bookingOptions") || {
        'selection' : []
    };
    var roomCount = bookingOptions.roomCount ? parseInt(bookingOptions.roomCount) : 0;

    if (bookingOptions.selection.length == 5) {

        tajToaster('Please contact customer support for booking of more than 5 Rooms!');
    }

    // Case 1 - If no initiial selection, possibily user has landed on this page
    // directly - navigate him to the check_availability component.
    else if (bookingOptions.roomOptions.length == 0) {

        askToInitiateSelection();
    }

    // Case 2 - If packages for all roomOptions has been selected and there is
    // an
    // attempt for next selection
    else if (bookingOptions.selection.length == bookingOptions.roomOptions.length) {

        askToInitiateSelection();
    }

    // Case 3 - If user has choosen to select only 1 room - navigate to cart
    // page
    // &
    // Case 4 - If user has selected multiple rooms
    /* if ( selectionCount < bookingOptions.selection.length ) { */
    /* if ( bookingOptions.selection.length < 5 ) { */
    else if (bookingOptions.selection.length < bookingOptions.roomOptions.length) {
        // Update bookingOptions object
        var selectionView = {};

        // Index of the ROOM card whose one of the rate card has been clicked
        var _selectedRoomCard = $(clickedButton).closest('.rate-card-wrap');
        // clicked
        // Index of the ROOM card whose one of the rate card has been clicked
        var _indexRoomCard = $(clickedButton).closest('.rate-card-wrap').index();

        // Index of the rate card that has been clicked inside a Room card
        var _indexRateCard = $(clickedButton).closest('.more-rate-card-container').index();

        // Index of the Rate Types Tab (which list out the available rooms
        // for
        // choosen rate) whose Room option has been selected by clicking on
        // one of the
        // rate card for that room
        var _indRoomInRates = $(clickedButton).closest('.cm-room-options').index();

        // Target rate card in the cloned Room options(cloned at page load)
        // so that alteration of rate in the view by developer tool should
        // not
        // get reflected on the next page
        var allRoomsOnPgLoad = $('.cm-room-options').clone();
        var targetParent = allRoomsOnPgLoad.find('.rate-card-wrap').eq(_indexRoomCard)
        .find('.more-rate-card-container').eq(_indexRateCard);

        selectionView.details = (targetParent.html());
        selectionView.bedTypeOptions = _selectedRoomCard.data('bed-type-opts');
        // selectionView.standardDailyNightlyRate =
        // _selectedRoomCard.find('.current-currency-value.actual-rate').text();
        // selectionView.title = fetchRoomNameViaButton(clickedButton);
        // selectionView.price = fetchSelectionPriceViaButton(clickedButton);
        selectionView.title = _selectedRoomCard.data('room-type-name');
        if (selectionView.roomDescription == undefined) {
            selectionView.roomShortDescription = _selectedRoomCard.find(
            '.rate-card-room-details .rate-card-room-description span:first').text().replace("...Show More",
            "...");
            selectionView.roomLongDescription = _selectedRoomCard.find('.full-description-text.cm-hide span:first')
            .text()
            || selectionView.roomShortDescription;
        }
        if(_selectedRoomCard.data("room-type") == "bungalow"){
            selectionView.isBungalow = true;
        }
        selectionView.roomBedType = getDefaultBedType(selectionView.bedTypeOptions);
        selectionView.hotelId = $($.find('[data-hotel-id]')).data().hotelId;

        if($($.find("[data-hotel-locationid]")).data()){
            selectionView.hotelLocationId = $($.find('[data-hotel-locationid]')).data().hotelLocationid;
        }else{
            selectionView.hotelLocationId = $($.find('[data-hotel-id]')).data().hotelId;
        }

        var selectedRateTab = $.find('.rate-tab.tab-selected');
        selectionView.currencyString = $(selectedRateTab).find('[data-injector-key="currency-string"]').text();
		dataCache.session.setData("selectedCurrencyString", selectionView.currencyString);
        selectionView.roomTypeCode = selectionView.bedTypeOptions[selectionView.roomBedType];
        /*
         * selectionView.ratePlanCode = _selectedRoomCard.find('.more-rate-title').data('ratePlanCode');
         */
        if ($(selectedRateTab).attr('data-rate-filter-code')) {
            if ($(selectedRateTab).attr('data-rate-filter-code') == "TIC") {
                selectionView.promoCode = targetParent.find('.more-rate-title').data('rate-plan-code');
            } else {
                selectionView.promoCode = '';
            }

        } else if ($(selectedRateTab).attr('data-promo-code')) {
            selectionView.promoCode = $(selectedRateTab).attr('data-promo-code');
        } else if ($(selectedRateTab).attr('data-offer-rate-code')) {
            selectionView.promoCode = targetParent.find('.more-rate-title').data('rate-plan-code');
        } else if ($(selectedRateTab).attr('data-negotiation-code')) {
            selectionView.promoCode = $(selectedRateTab).attr('data-negotiation-code');
        }
        selectionView.ratePlanCode = targetParent.find('.more-rate-title').data('rate-plan-code');
        selectionView.rateName = targetParent.find('.more-rate-title').text();
        
        selectionView.rateDescription = targetParent.find('.more-rate-title').closest('.more-rate-card').find(
        '.rate-description .rate-description-content-first .rate-plan-description-txt').text()
        
        selectionView.dailyNightlyRate = targetParent.find('.more-rate-title').closest('.more-rate-card').find(
        '.present-rate .rc-total-amount').text();
        
        selectionView.dailyNightlyRateWithTax = parseInt(((targetParent.find('.more-rate-title').closest(
        '.more-rate-card').find('.rate-tax-info').text()) + "").replace(",", ""))
        + parseInt(((selectionView.dailyNightlyRate) + "").replace(",", ""));
        
        selectionView.guaranteeCode = targetParent.find('.more-rate-title').closest('.more-rate-card').find('.guarantee-code-info').text();
        
        var isCreditcardRequired = targetParent.find('.more-rate-title').closest('.more-rate-card').find('.credit-card-required').text();
        if(isCreditcardRequired){
            selectionView.isCreditCardRequired = true;
        }else{
            selectionView.isCreditCardRequired = false;
        }
        
        selectionView.guaranteeAmount = Number(targetParent.find('.more-rate-title').closest('.more-rate-card').find('.guarantee-amount-info').text().replace(",", ""));
        selectionView.guaranteePercentage = Number(targetParent.find('.more-rate-title').closest('.more-rate-card').find('.guarantee-percentage-info').text().replace(",", ""));
        selectionView.guaranteeDescription = targetParent.find('.more-rate-title').closest('.more-rate-card').find('.guarantee-description-info').text();
        var guaranteeAmountTaxInclusive = targetParent.find('.more-rate-title').closest('.more-rate-card').find('.guarantee-amount-tax-inclusive-info').text();
        if(guaranteeAmountTaxInclusive){
            guaranteeAmountTaxInclusive = true;
        }else{
            guaranteeAmountTaxInclusive = false;
        }
        selectionView.guaranteeAmountTaxInclusive = guaranteeAmountTaxInclusive;
        selectionView.selectedFilterCode = $('.rate-tab-container .rate-tab.tab-selected').data("rate-filter-code");
        selectionView.selectedFilterTitle = $('.rate-tab-container .rate-tab.tab-selected div').html();
        selectionView.avgTax = parseInt(((targetParent.find('.more-rate-title').closest(
        '.more-rate-card').find('.room-avg-taxes-info').text()) + "").replace(",", ""))

        tempRetIndexVal = roomIndexForCurrentPackage();

        var indexCurrentRoom = tempRetIndexVal.indexCurrentRoom;
        // [TIC-FLOW]
        ticBookingFlow(selectionView);
        bookingOptions.selection.push(selectionView);

        var tempSelIndex = bookingOptions.selection.length - 1;
        /*
         * if ( bookingOptions.roomOptions && bookingOptions.roomOptions[ tempSelIndex ] ) {
         */
        if (bookingOptions.roomOptions && bookingOptions.roomOptions[indexCurrentRoom]) {

            bookingOptions.selection[tempSelIndex].roomIndex = indexCurrentRoom;

            Object.assign(bookingOptions.selection[tempSelIndex], bookingOptions.roomOptions[indexCurrentRoom]);

            bookingOptions.roomOptions[indexCurrentRoom].userSelection = selectionView;

            // Update instruction to select room
            ++roomCount;

        } else {
            var tempObj = {
                    adults : 1,
                    children : 0,
                    isDefaultSelection : true
            };

            // updateSelectionInstruction();
            Object.assign(bookingOptions.selection[tempSelIndex], tempObj);

            // Update instruction to select room
            ++roomCount;
            // updateSelectionInstruction( elmeSelInstruct );
        }

        var tempRef = bookingOptions.selection[tempSelIndex];

        // ++selectionCount;
        // Update the floating cart values
        updateFloatingCart(targetParent, tempRef, indexCurrentRoom);

        // Update session cache
        updateAppStorage();

        updateSelectionInstruction();

        // Shows the cart after the first selection
        displayFloatingCart();

        updateCheckAvailability();

        // Express Selection - If opted for only one room then navigate to cart
        // page on addition of package
        if ((bookingOptions.roomOptions.length == 1) && (bookingOptions.selection.length == 1)) {

            checkoutContainer.find('a.checkout-anchor')[0].click();
        }

        var nextRoomNumber = parseInt(tempRetIndexVal.initialRoomIndex) + 2;

        if (ROOM_OCCUPANCY_RESPONSE[nextRoomNumber]) {
            processRoomOccupancyRates(nextRoomNumber);
        }

    } else {
        return;
    }
    /* processRoomOccupancyRates(); */

    // ////////////////
    $('.rate-card-details-section').removeClass('visible-table');
    $('.rate-card-view-detials-container').removeClass('rate-card-view-detials-selected')
    $('.more-rates-button').removeClass('more-rates-button-selected');
    $('.rate-card-more-rates-section').removeClass('visible');
    $('.rate-card-details-container').removeClass('select-border');
    $('.more-rates-button').find('.icon-drop-down-arrow-white').removeClass('cm-rotate-icon-180');

    // Analytic data call
    // prepareGlobalHotelListJS();
    ViewName = "rooms.add";
    event = "rooms.add";
    prepareOnRoomSelect(ViewName, event);
    pushEvent(event, ViewName, prepareGlobalHotelListJS())
    // ////////////////
}

function roomIndexForCurrentPackage() {
    var bookingOptions = dataCache.session.getData("bookingOptions");
    // var indexCurrentRoom;
    var indecies = {};
    bookingOptions.roomOptions.forEach(function(eachRoom, index) {
        if (!eachRoom.userSelection && (indecies.indexCurrentRoom == undefined)) {
            indecies.indexCurrentRoom = index;
            indecies.initialRoomIndex = eachRoom.initialRoomIndex || index;
        }
    });
    // return indexCurrentRoom;
    return indecies;
}

function getDefaultBedType(bedTypeOptions) {
    if (bedTypeOptions.king) {
        return "king";
    } else if (bedTypeOptions.queen) {
        return "queen";
    } else if (bedTypeOptions.double) {
        return "double";
    } else {
        return "twin";
    }
}

function setCartPanelRoomsConWidth() {
    var floatingCartPkgConWidth = 0;
    var bookingOptions = dataCache.session.getData("bookingOptions");

    var cartPanelRoomsCon = $('.room-selected-options-container');

    bookingOptions.roomOptions.forEach(function() {
        floatingCartPkgConWidth = floatingCartPkgConWidth + cartPanelRoomsCon.children().first().width();
        cartPanelRoomsCon.width(floatingCartPkgConWidth);
    });
}

function pkgSelectionLayoutOnCart() {
    displayFloatingCart();
    var bookingOptions = dataCache.session.getData("bookingOptions");
    var elAddRoomPackage = "<div class='fc-add-package-con clearfix'>" + "<div class='fc-add-package-wrap clearfix'>"
    + "<div class='fc-add-btn-con'><span class='fc-txt-add-room'>SELECT ROOM</span>"
    + "<span class='fc-add-plus-symbol'>+</span></div>" + "</div></div>";
    // elChoosenRoomsInCart.html('');
    bookingOptions.roomOptions.forEach(function(entity) {
        elChoosenRoomsInCart.append(elAddRoomPackage);
    });

    setCartPanelRoomsConWidth();

    // elChoosenRoomsInCart.children().first().addClass("active");

    if (bookingOptions.selection && bookingOptions.selection.length > 0) {
        elChoosenRoomsInCart.children().removeClass("active");
        renderFloatingCartOnLoad();
    }
}

function roundPrice(price) {
    var rateAfter = price;
    rateAfter = (Math.round(rateAfter) * 100) / 100;
    var selectedCurrency = dataCache.session.getData('selectedCurrencyString');
    if (selectedCurrency == null || selectedCurrency == undefined || selectedCurrency == "") {
        rateAfter = rateFormater(rateAfter);
    } else {
        rateAfter = rateFormater(rateAfter, selectedCurrency);
    }

    return rateAfter;
}

function returnSelectionCurrencyString() {
    var booking = dataCache.session.getData("bookingOptions");
    if (booking && booking.selection[0] && booking.selection[0].currencyString) {
        return booking.selection[0].currencyString;
    }
    return '';
}

function rateFormater(number, currency) {
    var formattedNumber;
    if (isNaN(number)) {
        formattedNumber = number;
    } else {
        if (currency == undefined || currency == "") {
            currency = dataCache.session.getData("selectedCurrencyString")
        }

        if (currency != '' && currency != 'INR') {
            formattedNumber = number.toLocaleString('en-US');
        } else {
            formattedNumber = number.toLocaleString('en-IN');
        }
    }
    return formattedNumber;
}

$(document).ready(
        function() {
            globalBookingOption = dataCache.session.getData("bookingOptions");
            if(globalBookingOption){
            globalBookingOption.totalCartTax = $(
            '.summary-charges-tax-con .summary-charges-item-value .cart-total-tax').html();

                Taxes = $('.summary-charges-tax-con .summary-charges-item-value .cart-total-tax').html();
                dataCache.session.setData("bookingOptions", globalBookingOption);
                // [TIC-FLOW]
                ticBookingFlow();
            }
        });
// [TIC-FLOW]
function ticBookingFlow(selectionView) {

    // [TIC-FLOW]
    var url = window.location.href;
    globalBookingOption = dataCache.session.getData("bookingOptions");
    var ticRoomRedemptionObjectSession = dataCache.session.getData('ticRoomRedemptionObject');
    var isTicRoomRedemptionFlow = false;

    // selectionView is valid when user clicks on "SELECT ROOM".
    if(selectionView){
        if (selectionView.selectedFilterTitle && ((selectionView.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES')
                || (selectionView.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
                || (selectionView.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES')
                || (selectionView.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
            $(".cm-page-container").addClass('tic-room-redemption-fix');
        }
    }

    if(ticRoomRedemptionObjectSession && url.indexOf('/booking-confirmation') == -1){
        globalBookingOption.selection.forEach(function(item, index) {
            if (item.selectedFilterTitle && ((item.selectedFilterTitle === 'TIC ROOM REDEMPTION RATES')
                    || (item.selectedFilterTitle === 'TAP ROOM REDEMPTION RATES')
                    || (item.selectedFilterTitle === 'TAPPMe ROOM REDEMPTION RATES')
                    || (item.selectedFilterTitle === 'TAJ HOLIDAY PACKAGES'))) {
                isTicRoomRedemptionFlow = true;
                $(".cm-page-container").addClass('tic-room-redemption-fix');
            }
        });

        ticRoomRedemptionObjectSession.isTicRoomRedemptionFlow = isTicRoomRedemptionFlow;
        ticRoomRedemptionObjectSession.selection =  globalBookingOption.selection;
        dataCache.session.setData("ticRoomRedemptionObject", ticRoomRedemptionObjectSession);

    }

}
