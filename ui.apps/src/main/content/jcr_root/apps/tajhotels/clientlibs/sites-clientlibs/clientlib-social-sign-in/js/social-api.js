// This file contains common methods for calling social apis

function performFbLogin(callback) {
    try {
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me?fields=id,first_name,last_name,picture,email,name', function(response) {
                    callback(response);
                });
            } else {
                console.error('User cancelled login or did not fully authorize.');
            }
        }, {
            scope : 'email'
        });
    } catch (error) {
        console.error("An error occurred while loading facebook login.");
        console.error(error);
    }
}

function loadGapi(callback) {
    try {
        gapi.load('auth2', function() {
            // Retrieve the singleton for the GoogleAuth library and set up the client.
            auth2 = gapi.auth2.init({
                client_id : '354167341738-acg2rm7cs2luemdi4hrpe5uokhu4j4q8.apps.googleusercontent.com',
                cookiepolicy : 'single_host_origin'
            });
            callback();
        });
    } catch (error) {
        console.error("An error occurred while loading gapi.");
        console.error(error);
    }
}